#if !defined(AFX_IPSETDLG_H__1FF552AC_CF80_4BB7_9010_2799A5D64476__INCLUDED_)
#define AFX_IPSETDLG_H__1FF552AC_CF80_4BB7_9010_2799A5D64476__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// IPSetDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CIPSetDlg dialog

class CIPSetDlg : public CDialog
{
// Construction
public:
	CString GetIPAddress();
	void SetIPAddress(CString strIP);
	CIPSetDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CIPSetDlg)
	enum { IDD = IDD_IP_SET_DLG };
	CIPAddressCtrl	m_ctrlIPAddress;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIPSetDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CString m_strIPAddress;
	// Generated message map functions
	//{{AFX_MSG(CIPSetDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IPSETDLG_H__1FF552AC_CF80_4BB7_9010_2799A5D64476__INCLUDED_)
