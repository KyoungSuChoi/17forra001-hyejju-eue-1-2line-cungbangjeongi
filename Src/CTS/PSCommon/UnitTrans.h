// UnitTrans.h: interface for the CUnitTrans class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_UNITTRANS_H__C557054C_7C76_4E80_A875_C953435608E2__INCLUDED_)
#define AFX_UNITTRANS_H__C557054C_7C76_4E80_A875_C953435608E2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class AFX_EXT_CLASS CUnitTrans  
{
public:
	CUnitTrans();
	virtual ~CUnitTrans();

	CString ValueString(double dData, int item, BOOL bUnit = FALSE);
	CString GetUnitString(int nItem);
	void UpdateUnitSetting();


protected:
	BOOL m_nVoltageUnitMode;
	BOOL m_nCurrentUnitMode;
	int m_nTimeUnit;
	CString m_strVUnit;
	int m_nVDecimal ;
	CString m_strIUnit;
	int m_nIDecimal;
	CString m_strCUnit;
	int m_nCDecimal;
	CString m_strWUnit;
	int m_nWDecimal;
	CString m_strWhUnit;
	int m_nWhDecimal;
};

#endif // !defined(AFX_UNITTRANS_H__C557054C_7C76_4E80_A875_C953435608E2__INCLUDED_)
