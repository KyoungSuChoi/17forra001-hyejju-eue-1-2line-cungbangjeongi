VB3 / VB4-16 Example to Print a chart with a clipping region

This is a supplement to "PRNTPAGE.TXT".  This example includes
additional references to CreateRectRgn, SelectClipRgn, and DeleteObject.

Declare Function CreateRectRgn Lib "GDI" (ByVal left%, ByVal top%, ByVal right%, ByVal bottom%) As Integer
Declare Function SelectClipRgn Lib "GDI" (ByVal hDC%, ByVal hRgn%) As Integer
Declare Function DeleteObject Lib "GDI" (ByVal hObj%) As Integer

Declare Function SetMapMode Lib "GDI" (ByVal hDC%, ByVal Mode%) As Integer
Declare Function SetViewportExt Lib "GDI" (ByVal hDC%, ByVal x%, ByVal y%) As Long
Declare Function SetViewportOrg Lib "GDI" (ByVal hDC%, ByVal x%, ByVal y%) As Long
Declare Function PlayMetafile Lib "GDI" (ByVal hDC%, ByVal hMeta%) As Integer
Declare Function PEgetmeta Lib "PEGRAPHS.DLL" (ByVal hObject%) As Integer
Declare Function PEresetimage Lib "PEGRAPHS.DLL" (ByVal hObject%, ByVal nLength%, ByVal nHeight%) As Integer

// To print an object called Pesgo1 //

Dim hMeta As Integer
Dim hRes As Integer
Dim vRes As Integer
Dim test As Integer
Dim oldMM As Integer
Dim lresult As Long

Dim hRgn As Integer

'** send printer something so VB knows to start page **'
Printer.Print " "

'** get size of page **'
Printer.ScaleMode = 3
hRes = Printer.ScaleWidth
vRes = Printer.ScaleHeight

'** set mapping mode MM_ANSIOTROPIC **'
oldMM = SetMapMode(Printer.hDC, 8)

'** Set viewport org and extents **'
lresult = SetViewportOrg(Printer.hDC, hRes * 0.25, vRes * 0.25)
lresult = SetViewportExt(Printer.hDC, hRes * 0.5, vRes * 0.5)

'** Create Clipping Region **'
hRgn = CreateRectRgn(hRes * 0.25, vRes * 0.25, hRes * 0.75, vRes * 0.75)
test = SelectClipRgn(Printer.hDC, hRgn)
test = DeleteObject(hRgn)

'** reset image shape to match shape defined by SetViewportExt **'
test = PEresetimage(Pesgo1, hRes, vRes)
hMeta = PEgetmeta(Pesgo1)
test = PlayMetafile(Printer.hDC, hMeta)

Printer.EndDoc

' ** reset mapping mode **'
test = SetMapMode(Printer.hDC, oldMM)
' ** reset image to current aspect ratio **'
test = PEresetimage(Pesgo1, 0, 0)

