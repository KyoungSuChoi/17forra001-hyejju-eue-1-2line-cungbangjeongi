// CpkViewDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "CpkViewDlg.h"
#include "CTSAnalView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCpkViewDlg dialog


bool CCpkViewDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCpkViewDlg"), _T("TEXT_CCpkViewDlg_CNT"), _T("TEXT_CCpkViewDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CCpkViewDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCpkViewDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}


CCpkViewDlg::CCpkViewDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCpkViewDlg::IDD, pParent)
{
	LanguageinitMonConfig();

	//{{AFX_DATA_INIT(CCpkViewDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_dDivision = 1.0f;
	m_pData = NULL;
	m_nCellCount = 0;
	m_dCp = 0.0;
	m_dCpk =0.0;
	m_bHighLimit = FALSE;
	m_bLowLimit = FALSE;
	m_dHighLimitVal = 0.0;
	m_dLowLimitVal = 0.0;
	m_strTitle =TEXT_LANG[0];//"분포도"
	m_strXAxisLable = "Value";
}

CCpkViewDlg::~CCpkViewDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

void CCpkViewDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCpkViewDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCpkViewDlg, CDialog)
	//{{AFX_MSG_MAP(CCpkViewDlg)
	ON_BN_CLICKED(IDC_SAVE_IMG_FILE, OnSaveImgFile)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_PRINT_IMG, OnPrintImg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////

BOOL CCpkViewDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CString str;
/*	str.Format("%f",m_pView->m_Cp);
	GetDlgItem(IDC_CP)->SetWindowText(str);
	str.Format("%f",m_pView->m_Cpk);
	GetDlgItem(IDC_CPK)->SetWindowText(str);
*/
	m_Graph.SubclassDlgItem(IDC_CP_GRAPH, this);
	m_Graph.SetDivision(m_dDivision);
	ASSERT(m_pData);
	m_Graph.SetData(m_nCellCount, m_pData);
	m_Graph.SetLimitVal(m_dHighLimitVal, m_dLowLimitVal);
	m_Graph.ProcessData();

//----------------------------------- 상,하한 설정시 ---------------------------------------//
	if(m_bHighLimit == TRUE && m_bLowLimit == TRUE)  
	{
		m_dCp = (m_dHighLimitVal - m_dLowLimitVal) / (6.0f * m_Graph.GetDeviation());
    	m_dCpk = m_dCp * fabs((1.0f-((m_dHighLimitVal + m_dLowLimitVal)/2.0f - m_Graph.GetMean())))
	    		   / ((m_dHighLimitVal - m_dLowLimitVal) / 2.0f);
	}
//---------------------------------- 상한 있는 경우 --------------------------------------//
	else if(m_bHighLimit == TRUE && m_bLowLimit == FALSE)  
	{
   		m_dCpk = m_dCp = (m_dHighLimitVal - m_Graph.GetMean()) / (3.0f * m_Graph.GetDeviation());
	}
	else if(m_bHighLimit == FALSE && m_bLowLimit == TRUE)  
	{
   		m_dCpk = m_dCp = (m_Graph.GetMean() - m_dLowLimitVal) / (3.0f * m_Graph.GetDeviation());
	}

	str.Format("Mean: %.3f , STD D: %.3f, Cp = %.3f, Cpk = %.3f", m_Graph.GetMean(), m_Graph.GetDeviation(), m_dCp, m_dCpk);
	m_Graph.SetTitle(m_strTitle, str);
	m_Graph.SetXAxisLabel(m_strXAxisLable);
	
	m_Graph.AddXAnnotation(m_Graph.GetMean(), "Mean");
	m_Graph.AddXAnnotation(m_dLowLimitVal, "Low Limit");
	m_Graph.AddXAnnotation(m_dHighLimitVal, "High Limit");

	GetDlgItem(IDC_STATIC_TITLE)->SetWindowText(m_strTitle);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CCpkViewDlg::SetHighLimit(BOOL bUse, double dHighLimit)
{
	m_bHighLimit = bUse;
	m_dHighLimitVal = dHighLimit;
}

void CCpkViewDlg::SetLowLimit(BOOL bUse, double dLowLimit)
{
	m_bLowLimit = bUse;
	m_dLowLimitVal = dLowLimit;
}

BOOL CCpkViewDlg::SetRowData(double *pData, int nCount, double dDivision)
{
	ASSERT(pData);
	if(nCount <= 0 || dDivision <= 0.0)		return FALSE;

	m_nCellCount = nCount;
	m_pData = pData;
	m_dDivision = dDivision;
	return TRUE;
}

void CCpkViewDlg::OnSaveImgFile() 
{
	// TODO: Add your control notification handler code here
	m_Graph.SaveFile();
}

void CCpkViewDlg::OnPrintImg() 
{
	// TODO: Add your control notification handler code here
	m_Graph.PrintData();
}
