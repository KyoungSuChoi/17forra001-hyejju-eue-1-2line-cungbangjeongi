// Ba_Tester.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "CTSGraphAnal.h"

#include "MainFrm.h"

#include "ChildFrm.h"
#include "CTSGraphAnalDoc.h"
#include "CTSGraphAnalView.h"

#include "DataFrame.h"
#include "DataDoc.h"
#include "DataView.h"

#include "Splash.h"
#include "Win32Process.h"

#include "UnitPathRecordset.h"

#define STRING_UNIT1			"Z:\\"
#define STRING_UNIT2			"Y:\\"
//#define STRING_UNIT1			"C:\\Unit1\\Share"
//#define STRING_UNIT2			"C:\\Unit2\\Share"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCTSGraphAnalApp

BEGIN_MESSAGE_MAP(CCTSGraphAnalApp, CWinApp)
	//{{AFX_MSG_MAP(CCTSGraphAnalApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)	
	ON_COMMAND(ID_SHOW_DATA, OnShowData)
	ON_COMMAND(ID_SHOW_GRAPH, OnShowGraph)	
	ON_UPDATE_COMMAND_UI(IDM_SHOW_HELP, OnUpdateShowHelp)	
	ON_UPDATE_COMMAND_UI(ID_SHOW_GRAPH, OnUpdateShowGraph)
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCTSGraphAnalApp construction

CCTSGraphAnalApp::CCTSGraphAnalApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance	
	m_pHelpTemplate    = NULL;
	m_bInitial         = TRUE;

}

CString CCTSGraphAnalApp::m_strOriginFolderPath = "";

/////////////////////////////////////////////////////////////////////////////
// The one and only CCTSGraphAnalApp object

CCTSGraphAnalApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CCTSGraphAnalApp initialization

BOOL CCTSGraphAnalApp::InitInstance()
{
//	AfxSocketInit();
	AfxGetModuleState()->m_dwVersion = 0x0601;
	
	AfxOleInit();		//for clipboard ftn
	GXInit();

	// 동일한 Program이 이미 실행되고 있는지 확인 
/*	CWin32Process win32proc;

	if(!win32proc.Init()) {
		AfxMessageBox(win32proc.GetLastError());
		return FALSE;
	}

	if(win32proc.IsWinNT() ) {
		//AfxMessageBox("This program cannot run on WinNT");
		//return FALSE;
	}

	if (!win32proc.EnumAllProcesses())
	{
		AfxMessageBox(win32proc.GetLastError());
		return FALSE;
	}


	int nCount = 0;
	CStringArray* pArray =win32proc.GetAllProcessesNames();
	for (int i=0;i<pArray->GetSize();i++){
		if(pArray->GetAt(i).CompareNoCase(m_pszAppName)==0) nCount++;
	}

	if(nCount>1){
		MessageBox(NULL, "이미 실행중입니다.", "Error", MB_ICONEXCLAMATION);
		//AfxMessageBox("This program is already running!");
		return FALSE;
	}
*/
	// CG: The following block was added by the Splash Screen component.
\
	{
\
		CCommandLineInfo cmdInfo;
\
		ParseCommandLine(cmdInfo);
\

\
//		CSplashWnd::EnableSplashScreen(cmdInfo.m_bShowSplash);
\
	}

	//
	TCHAR szBuff[_MAX_PATH];
	::GetModuleFileName(m_hInstance,szBuff,_MAX_PATH);
	CString path(szBuff);
	SetOriginFolderPath(path.Left(path.ReverseFind('\\')));

	//
	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	// Change the registry key under which our settings are stored.
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization.
	
	SetRegistryKey("PNE CTS");

	if( LanguageinitMonConfig() == false )
	{
		AfxMessageBox("Could not found [ GraphAnalyzer_Lang.ini ]");
		return false;
	}

	char szCurrentDirectory[_MAX_PATH];
	GetCurrentDirectory(_MAX_PATH, szCurrentDirectory);
	
	CString strCurrentPath = GetProfileString("Control", "ResultDataPath", szCurrentDirectory);
	if ( strCurrentPath.Compare(szCurrentDirectory) == 0 )
	{
		strcat(szCurrentDirectory, "\\Data");
		WriteProfileString("Control", "ResultDataPath", szCurrentDirectory);
	}
	strcpy(m_szCurrentDirectory, strCurrentPath);

	LoadStdProfileSettings();  // Load standard INI file options (including MRU)

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.

//	CMultiDocTemplate* pDocTemplate;
	m_pDataTemplate = new CMultiDocTemplate(
		IDR_DATA,
		RUNTIME_CLASS(CDataDoc),
		RUNTIME_CLASS(CDataFrame), // custom MDI child frame
		RUNTIME_CLASS(CDataView));
	AddDocTemplate(m_pDataTemplate);

	m_pGraphTemplate = new CMultiDocTemplate(
		IDR_BA_TESTYPE,
		RUNTIME_CLASS(CCTSGraphAnalDoc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(CCTSGraphAnalView));
	AddDocTemplate(m_pGraphTemplate);


	// create main MDI Frame window
	CMainFrame* pMainFrame = new CMainFrame;
	if (!pMainFrame->LoadFrame(IDR_MAINFRAME))
		return FALSE;
	m_pMainWnd = pMainFrame;

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

//	// Dispatch commands specified on the command line
//	if (!ProcessShellCommand(cmdInfo))
//		return FALSE;

//	// The main window has been initialized, so show and update it.
	pMainFrame->ShowWindow(SW_SHOWMAXIMIZED);
	pMainFrame->UpdateWindow();

	CString strArgu((char *)m_lpCmdLine);
	//strArgu
	//c:\data 저장위치\test명\M01Ch01\test명.rpt
	//								 \test명.cyc

	if(!strArgu.IsEmpty())
	{
		strArgu.TrimLeft("\"");
		strArgu.TrimRight("\"");
		CString strExt;
		int p1 = strArgu.ReverseFind('.');
		if(p1 > 0)								//파일 확장자 검색 
		{
			strExt =  strArgu.Mid(p1+1);
			strExt.MakeLower();
			if(strExt == PS_RAW_FILE_NAME_EXT || strExt == "rpt" || strExt == PS_RESULT_FILE_NAME_EXT)
			{
				p1 = strArgu.ReverseFind('\\');
				if(p1 >= 0)									//채널 폴더명 분리  
				{
					CString strChPathName, strTestName;
					strChPathName = strArgu.Left(p1);
					//AfxMessageBox(strTemp);
					
					p1 = strChPathName.ReverseFind('\\');	 
					if(p1 > 0)
					{
						CString strTemp;
						strTemp = strChPathName.Left(p1);		//test명 폴더 분리
						//AfxMessageBox(strChPathName);
					
						p1 = strTemp.ReverseFind('\\');
						if(p1 > 0)
						{
							strTestName = strTemp.Mid(p1+1);		//test명 분리 
							//AfxMessageBox(strTestName);
							pMainFrame->m_strCopyMsgString = strChPathName+"\n";
							m_pDataTemplate->OpenDocumentFile(strTestName);
						}
					}
				}
			}
		}
	}


	// Main Frame이 Drag되는 Files를 받아들이게 한다.
	pMainFrame->DragAcceptFiles(TRUE);

//	InitDBPath();
	
	//
	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

// Class definition

//---------------------------------------------------------------------------
//	Class Name: CAboutDlg
//
//	Comment:
//
//	Revision History:
//	Version   Name            Date        Reason
//	    1.0   Kim, Sung-Hoon  2001.06.01  Create
//---------------------------------------------------------------------------
class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
		// No message handlers
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CCTSGraphAnalApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// CCTSGraphAnalApp message handlers


int CCTSGraphAnalApp::ExitInstance() 
{
	// TODO: Add your specialized code here and/or call the base class

//	if(m_pDataTemplate != NULL)		delete m_pDataTemplate; 

//	if(m_pMonitorTemplate!=NULL) delete m_pMonitorTemplate;


//	if(m_pHelpTemplate!=NULL)    delete m_pHelpTemplate;

	//
	return CWinApp::ExitInstance();
}

BOOL CCTSGraphAnalApp::PreTranslateMessage(MSG* pMsg)
{
	// CG: The following lines were added by the Splash Screen component.
//	if (CSplashWnd::PreTranslateAppMessage(pMsg))
//		return TRUE;

	return CWinApp::PreTranslateMessage(pMsg);
}

void CCTSGraphAnalApp::OnShowGraph()
{
// TODO: Add your command handler code here
//	POSITION pos = GetFirstDocTemplatePosition();
//	CMultiDocTemplate *pTempate;
//	pTempate = (CMultiDocTemplate *)GetNextDocTemplate(pos);
//	pTempate->OpenDocumentFile(NULL);
	m_pGraphTemplate->OpenDocumentFile(NULL);
}

void CCTSGraphAnalApp::OnShowData()
{
	m_pDataTemplate->OpenDocumentFile(NULL);
}

//void CCTSGraphAnalApp::OnFileNew() 
//{
//	if(!m_bInitial) CWinApp::OnFileNew();
//}

void CCTSGraphAnalApp::InitDBPath()
{
	CUnitPathRecordset rs;
	rs.SetDefaultPath(m_strOriginFolderPath);

	if( rs.IsOpen() ) rs.Close();
	try{ rs.Open(); } catch(...) { return; }

	if( rs.IsBOF() || rs.IsEOF() )
	{
		for ( int nI = 1; nI <= 2; nI++ )
		{
			rs.AddNew();
			rs.m_No = nI;
			rs.m_Path = (nI == 1) ? _T(STRING_UNIT1) : _T(STRING_UNIT2);
			rs.m_Comm = TRUE;
			rs.Update();
		}
	}
	else
	{
		int nI = 1;
		rs.MoveFirst();

		CString strPath;
		while( !rs.IsEOF() )
		{
			strPath = (nI == 1) ? _T(STRING_UNIT1) : _T(STRING_UNIT2);
			if ( rs.m_No == nI )
			{
				if( !rs.m_Path.Compare(strPath) )
				{
					if ( rs.m_Comm == FALSE )
					{
						rs.Edit();
						rs.m_Comm = TRUE;
						rs.Update();
					}
				}
				else
				{
					rs.Edit();
					rs.m_Path = strPath;
					rs.m_Comm = TRUE;
					rs.Update();
				}
			}
			else
			{
				rs.Edit();
				rs.m_No = nI;
				rs.m_Path = strPath;
				rs.m_Comm = TRUE;
				rs.Update();
			}
			rs.MoveNext();
			nI++;
		}
	}
	rs.Close();
/*
	CString strUnitNo, strUnitPath;
	BOOL bCommState;

	CDaoDatabase aDB;
	aDB.Open(CCTSGraphAnalApp::m_strOriginFolderPath+"\\sysinfo.mdb");
		CDaoRecordset aRec(&aDB);
		aRec.Open(dbOpenTable, "UnitInfo");
		
		if( aRec.IsBOF() || aRec.IsEOF() )
		{
		}
		else
		{
			aRec.MoveFirst();
			int nI = 0;
			while( !aRec.IsEOF() )
			{
				COleVariant varValue;
				//
				aRec.GetFieldValue("No",varValue);
				if(varValue.vt!=VT_NULL) 
					//strUnitNo = (LPCTSTR)(LPTSTR)varValue.bstrVal;
					int nUnitNo = varValue.intVal;
				varValue.Clear();
				//
				aRec.GetFieldValue("Path",varValue);
				if(varValue.vt!=VT_NULL) 
					strUnitPath = (LPCTSTR)(LPTSTR)varValue.bstrVal;
				varValue.Clear();
				//
				aRec.GetFieldValue("Comm",varValue);
				bCommState=varValue.boolVal;
				//
				if ( atoi(strUnitNo) != nI + 1 )
				{
					CString strTmp;
					strTmp.Format("%d", nI + 1);
					varValue.Clear();
					varValue.intVal = nI + 1;
					aRec.SetFieldValue("No", varValue);
					varValue.SetString((nI == 0) ?_T("Z:\\") : _T("Y:\\"), VT_BSTRT);
					aRec.SetFieldValue("Path", varValue);
					varValue.boolVal = TRUE;
					aRec.SetFieldValue("Comm", varValue);
				}
				else 
				{
					if ( ! strUnitPath.Compare ( (nI == 0) ?_T("Z:\\") : _T("Y:\\") ) )
					{
						varValue.boolVal = TRUE;
						aRec.SetFieldValue("Comm", varValue);
					}
					else
					{
						varValue.SetString((nI == 0) ?_T("Z:\\") : _T("Y:\\"), VT_BSTRT);
						aRec.SetFieldValue("Path", varValue);
						//aRec.SetFieldValue("Path", (nI == 0) ?_T("Z:\\") : _T("Y:\\"));
						varValue.boolVal = TRUE;
						aRec.SetFieldValue("Comm", varValue);
					}
				}

				nI++;
				aRec.MoveNext();
			}
		}
		aRec.Close();
	*/
}

void CCTSGraphAnalApp::SetResultDataPath(CString strPath)
{
	strcpy(m_szCurrentDirectory, strPath);

	WriteProfileString("Control", "ResultDataPath", strPath);
}

void CCTSGraphAnalApp::OnUpdateShowHelp(CCmdUI* pCmdUI) 
{
	CString strHelpFilePath = m_strOriginFolderPath + "\\Manual.htm";
	CFileFind aFinder;
	
	if(aFinder.FindFile(strHelpFilePath))
		pCmdUI->Enable(TRUE);
	else
		pCmdUI->Enable(FALSE);

	aFinder.Close();
}

void CCTSGraphAnalApp::OnUpdateShowGraph(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
//	pCmdUI->Enable(FALSE);	
}

CString CCTSGraphAnalApp::GetExcelPath()
{
	CString strExcelPath = GetProfileString(REG_CONFIG_SECTION,"Excel Path");
	CFileFind finder;
	if(finder.FindFile(strExcelPath) == FALSE)
	{
		AfxMessageBox(TEXT_LANG[0]);
		CFileDialog pDlg(TRUE, "exe", strExcelPath, OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, "exe file(*.exe)|*.exe|All Files(*.*)|*.*|");
		pDlg.m_ofn.lpstrTitle = TEXT_LANG[1];
		if(IDOK != pDlg.DoModal())
		{
			return	"";
		}
		strExcelPath = pDlg.GetPathName();
		WriteProfileString(REG_CONFIG_SECTION,"Excel Path", strExcelPath);
	}
	return strExcelPath;
}

BOOL CCTSGraphAnalApp::ExecuteExcel(CString strFileName)
{
	STARTUPINFO	stStartUpInfo;
	PROCESS_INFORMATION	ProcessInfo;
	ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
	ZeroMemory(&ProcessInfo, sizeof(PROCESS_INFORMATION));
		
	stStartUpInfo.cb = sizeof(STARTUPINFO);
	stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
	stStartUpInfo.wShowWindow = SW_NORMAL;

	CString strTemp;
	strTemp = GetExcelPath();
	if(strTemp.IsEmpty())	return FALSE;

	char buff1[_MAX_PATH];
	char buff2[_MAX_PATH];
	ZeroMemory(buff1, _MAX_PATH);
	ZeroMemory(buff2, _MAX_PATH);

	int aa =0;
	do {
		//존재 여부 확인
		aa = GetShortPathName((LPSTR)(LPCTSTR)strTemp, buff2, _MAX_PATH);
		if(aa <= 0)
		{
			if(AfxMessageBox(TEXT_LANG[2], MB_ICONSTOP|MB_OKCANCEL) == IDCANCEL)
			{
				return FALSE;

			}
			strTemp = GetExcelPath();
			if(strTemp.IsEmpty())	return FALSE;
		}
		
	} while( aa <= 0 );	

	GetShortPathName((LPSTR)(LPCTSTR)strTemp, buff2, _MAX_PATH);
	GetShortPathName((LPSTR)(LPCTSTR)strFileName, buff1, _MAX_PATH);
	
	strTemp.Format("%s %s", buff2, buff1);
	
	BOOL bFlag = ::CreateProcess(NULL, (LPSTR)(LPCTSTR)strTemp, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &stStartUpInfo, &ProcessInfo);
	if(bFlag == FALSE)
	{
		strTemp.Format(TEXT_LANG[3], strFileName);
		AfxMessageBox(strTemp, MB_OK|MB_ICONSTOP);
	}
	return TRUE;
}

bool CCTSGraphAnalApp::LanguageinitMonConfig() 
{
	g_nLanguage = AfxGetApp()->GetProfileInt(REG_CONFIG_SECTION, "Language", 0);
	g_strLangPath.Format("%s\\Lang\\GraphAnalyzer_Lang.ini", GetProfileString(REG_CONFIG_SECTION ,"Path", "C:\Program Files\PNE CTS"));


	switch(g_nLanguage)
	{
	case 0:
		{
			SetThreadUILanguage( MAKELCID( MAKELANGID(LANG_KOREAN , SUBLANG_KOREAN) , SORT_DEFAULT));
			break;
		}

	case 1:
		{
			SetThreadUILanguage( MAKELCID( MAKELANGID(LANG_ENGLISH , SUBLANG_ENGLISH_US) , SORT_DEFAULT));
			break;
		}

	case 2:
		{
			SetThreadUILanguage( MAKELCID( MAKELANGID(LANG_CHINESE_SIMPLIFIED , SUBLANG_CHINESE_SIMPLIFIED) , SORT_DEFAULT));
			break;
		}
	}

	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCTSGraphAnalApp"), _T("TEXT_CCTSGraphAnalApp_CNT"), _T("TEXT_CCTSGraphAnalApp_CNT"));
	if( strTemp == "TEXT_CCTSGraphAnalApp_CNT" )
	{
		return false;
	}

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt];		

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CCTSGraphAnalApp_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCTSGraphAnalApp"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Languge error ====> " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}