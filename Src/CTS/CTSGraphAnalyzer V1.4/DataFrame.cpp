// DataFrame.cpp : implementation file
//

#include "stdafx.h"
#include "CTSGraphAnal.h"
#include "DataFrame.h"

#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDataFrame

IMPLEMENT_DYNCREATE(CDataFrame, CMDIChildWnd)

CDataFrame::CDataFrame()
{

}

CDataFrame::~CDataFrame()
{
}


BEGIN_MESSAGE_MAP(CDataFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CDataFrame)
	ON_WM_CREATE()
	ON_COMMAND(ID_DATA_SHEET, OnDataSheet)
	ON_UPDATE_COMMAND_UI(ID_DATA_SHEET, OnUpdateDataSheet)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDataFrame message handlers

void CDataFrame::OnUpdateFrameMenu(BOOL bActive, CWnd* pActivateWnd, HMENU hMenuAlt)
{
	if(bActive)             ((CMainFrame*)GetParentFrame())->LoadToolBar(CMainFrame::DATA_TOOLBAR);
	if(pActivateWnd==NULL) 	((CMainFrame*)GetParentFrame())->LoadToolBar(CMainFrame::MAIN_TOOLBAR);

	CMDIChildWnd::OnUpdateFrameMenu(bActive, pActivateWnd, hMenuAlt);
}

BOOL CDataFrame::PreCreateWindow(CREATESTRUCT& cs) 
{
	// TODO: Add your specialized code here and/or call the base class
	cs.style &= ~FWS_ADDTOTITLE;
//	cs.lpszName = "Channel Monitoring";
//	cs.cx = 790;
//	cs.cy = 500;
	
	return CMDIChildWnd::PreCreateWindow(cs);
}

void CDataFrame::ActivateFrame(int nCmdShow) 
{
	// TODO: Add your specialized code here and/or call the base class
//	nCmdShow = SW_SHOWMAXIMIZED;
	CMDIChildWnd::ActivateFrame(nCmdShow);
}


int CDataFrame::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	EnableDocking(CBRS_ALIGN_ANY);

	if (!m_wndGridBar.Create("DataSheet", this, 3000))
	{
		TRACE0("Failed to create dialog bar m_wndGridBar\n");
		return -1;		// fail to create
	}
	m_wndGridBar.SetBarStyle(m_wndGridBar.GetBarStyle() | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
	m_wndGridBar.EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndGridBar, AFX_IDW_DOCKBAR_BOTTOM);

	CString sProfile = _T("BarState1");
	if (VerifyBarState(sProfile))
	{
		CSizingControlBar::GlobalLoadState(this, sProfile);
		LoadBarState(sProfile);
	}
	
	return 0;
}

BOOL CDataFrame::VerifyBarState(LPCTSTR lpszProfileName)
{
    CDockState state;
    state.LoadState(lpszProfileName);

    for (int i = 0; i < state.m_arrBarInfo.GetSize(); i++)
    {
        CControlBarInfo* pInfo = (CControlBarInfo*)state.m_arrBarInfo[i];
        ASSERT(pInfo != NULL);
        int nDockedCount = pInfo->m_arrBarID.GetSize();
        if (nDockedCount > 0)
        {
            // dockbar
            for (int j = 0; j < nDockedCount; j++)
            {
                UINT nID = (UINT) pInfo->m_arrBarID[j];
                if (nID == 0) continue; // row separator
                if (nID > 0xFFFF)
                    nID &= 0xFFFF; // placeholder - get the ID
                if (GetControlBar(nID) == NULL)
                    return FALSE;
            }
        }
        
        if (!pInfo->m_bFloating) // floating dockbars can be created later
            if (GetControlBar(pInfo->m_nBarID) == NULL)
                return FALSE; // invalid bar ID
    }

    return TRUE;
}

BOOL CDataFrame::DestroyWindow() 
{
	// TODO: Add your specialized code here and/or call the base class
	CString sProfile = _T("BarState1");
	CSizingControlBar::GlobalSaveState(this, sProfile);
	SaveBarState(sProfile);
	
	return CMDIChildWnd::DestroyWindow();
}

CMyGridWnd * CDataFrame::GetGridWnd()
{
	return (CMyGridWnd *)m_wndGridBar.GetClientWnd();
}

void CDataFrame::OnDataSheet() 
{
	// TODO: Add your command handler code here
	ShowControlBar(&m_wndGridBar, !m_wndGridBar.IsVisible(), FALSE);	
}

void CDataFrame::OnUpdateDataSheet(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->SetCheck(m_wndGridBar.IsVisible());		
}

void CDataFrame::OnEditCopy() 
{
	// TODO: Add your command handler code here
	CWnd *pWnd = GetFocus();
	if(GetGridWnd() == pWnd)
	{
		GetGridWnd()->Copy();
	}
	else
	{
		((CDataView *)GetActiveView())->OnGridCopy();
	}
}
