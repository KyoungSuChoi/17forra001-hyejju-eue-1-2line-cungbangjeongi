#if !defined(AFX_UNITPATHRECORDSET_H__F52D8771_8C14_4D61_91CF_23D713229825__INCLUDED_)
#define AFX_UNITPATHRECORDSET_H__F52D8771_8C14_4D61_91CF_23D713229825__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UnitPathRecordset.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CUnitPathRecordset DAO recordset

class CUnitPathRecordset : public CDaoRecordset
{
public:
	void SetDefaultPath(CString strPath);
	CUnitPathRecordset(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CUnitPathRecordset)

	CString m_strPath;
// Field/Param Data
	//{{AFX_FIELD(CUnitPathRecordset, CDaoRecordset)
	BYTE	m_No;
	CString	m_Path;
	BOOL	m_Comm;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUnitPathRecordset)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UNITPATHRECORDSET_H__F52D8771_8C14_4D61_91CF_23D713229825__INCLUDED_)
