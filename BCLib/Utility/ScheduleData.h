// ScheduleData.h: interface for the CScheduleData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SCHEDULEDATA_H__707CB6FF_2156_4365_87AD_1D2295496A0E__INCLUDED_)
#define AFX_SCHEDULEDATA_H__707CB6FF_2156_4365_87AD_1D2295496A0E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Step.h"

#define SCHEDULE_FILE_ID		740721	//스케쥴 파일 ID
#define SCHEDULE_FILE_VER	0x00010000	//상위 2Byte는 Mager Version No, 하위 2Byte는 Minu Version No


class CScheduleData  
{
public:
	BOOL ExecuteEditor(long lModelPK, long lTestPK);
	CStep* GetStepData(UINT nStepIndex);
	BOOL SetSchedule(CString strDBName, long lModelPK, long lTestPK);
	void ResetData();
	UINT GetStepSize();
	BOOL SaveToFile(CString strFileName);
	BOOL SetSchedule(CString strFileName);
	CScheduleData();
	virtual ~CScheduleData();

protected:
	BOOL LoadGradeInfo(CDaoDatabase &db, CStep *pStepData);
	BOOL LoadStepInfo(CDaoDatabase &db, long lProcPK);
	BOOL LoadProcedureInfo(CDaoDatabase &db, long lModelPK, long lProcPK);
	BOOL LoadModelInfo(CDaoDatabase &db, long lPK);
	SCHDULE_INFORMATION_DATA	m_ModelData;
	SCHDULE_INFORMATION_DATA	m_ProcedureData;
	CELL_CHECK_DATA				m_CellCheck;
	CPtrArray	m_apStepArray;

};

#endif // !defined(AFX_SCHEDULEDATA_H__707CB6FF_2156_4365_87AD_1D2295496A0E__INCLUDED_)
