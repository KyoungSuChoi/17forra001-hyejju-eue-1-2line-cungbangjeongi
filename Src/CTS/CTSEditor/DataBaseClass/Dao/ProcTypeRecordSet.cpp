// ProcTypeRecordSet.cpp : implementation file
//

#include "stdafx.h"
#include "ProcTypeRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CProcTypeRecordSet

IMPLEMENT_DYNAMIC(CProcTypeRecordSet, CDaoRecordset)

extern CString GetDataBaseName();


CProcTypeRecordSet::CProcTypeRecordSet(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CProcTypeRecordSet)
	m_ProcType = 0;
	m_ProcID = 0;
	m_Description = _T("");
	m_nFields = 3;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString CProcTypeRecordSet::GetDefaultDBName()
{
	return _T(GetDataBaseName());
}

CString CProcTypeRecordSet::GetDefaultSQL()
{
	return _T("[ProcType]");
}

void CProcTypeRecordSet::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CProcTypeRecordSet)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Long(pFX, _T("[ProcType]"), m_ProcType);
	DFX_Long(pFX, _T("[ProcID]"), m_ProcID);
	DFX_Text(pFX, _T("[Description]"), m_Description);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CProcTypeRecordSet diagnostics

#ifdef _DEBUG
void CProcTypeRecordSet::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CProcTypeRecordSet::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
