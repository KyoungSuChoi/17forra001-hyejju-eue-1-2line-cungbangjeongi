/*************************************************************
	Copyrighted by LG Production engineering Research Center

	File Name: Data.h

	Written out by 
	Evaluation Thechnology Group
	Kim, Sung-Hoon

	Comment: CData class is defined

	Revision History:
	Version   Name            Date        Reason
	    1.0   Kim, Sung-Hoon  2001.06.01  Create
*************************************************************/

// Data.h: interface for the CData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DATA_H__40068522_EC8D_11D4_88DF_006008CEDA07__INCLUDED_)
#define AFX_DATA_H__40068522_EC8D_11D4_88DF_006008CEDA07__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// #include

#include "DataQueryCdn.h"

// #define

// Class definition
//struct fltPoint;  // pointer 사용을 위한 전방선언
//class  CChData;   // pointer 사용을 위한 전방선언
class  CPlane;    // pointer 사용을 위한 전방선언

//---------------------------------------------------------------------------
//	Class Name: CData
//
//	Comment:
//
//	Revision History:
//	Version   Name            Date        Reason
//	    1.0   Kim, Sung-Hoon  2001.06.01  Create
//---------------------------------------------------------------------------class AFX_EXT_CLASS CData  
class AFX_EXT_CLASS CData
{
////////////////
// Attributes //
////////////////

private:
	CDataQueryCondition                m_QueryCondition; // Data Query Condition

	CTypedPtrList <CPtrList, CChData*> m_ChDataList;     // List of Channel Data
	CTypedPtrList <CPtrList, CPlane*>  m_PlaneList;      // Plane List

//////////////////////////////////////////////////
// 2003-02-26 추가 요구사항
	CString m_strTestName;
public:
	void SetTestName(CString strName) { m_strTestName = strName; }
	CString GetTestName() { return m_strTestName; }
//////////////////////////////////////////////////

///////////////////////////////////////////////
// Operations: "Construction and destruction //
///////////////////////////////////////////////

public:

	CData();
	virtual ~CData();


////////////////////////////////////////////////////////
// Operations: "Functions to access member variables" //
////////////////////////////////////////////////////////

public:
	void	  ReloadDataAtPlane(CPlane* pPlane);

	// Set contents of m_QueryCondition by dialog with user
	BOOL      QueryData(LPCTSTR strPaths, BOOL bReload = FALSE);

	// Get a channel data from m_ChDataList
	CChData*  GetChData(LPCTSTR strChPath);

	// m_PlaneList
	int       GetPlaneCount() { return m_PlaneList.GetCount(); };
	POSITION  GetHeadPlanePos() { return m_PlaneList.GetHeadPosition(); }
	CPlane*   GetNextPlane(POSITION& pos) { return m_PlaneList.GetNext(pos); };
	CPlane*   GetPlaneAt(int iSel) { return m_PlaneList.GetAt(m_PlaneList.FindIndex(iSel)); }


///////////////////////////////////
// Operations: "Other functions" //
///////////////////////////////////

public:

	// Plane construction
	BOOL      IsTherePlaneWithThisYAxis(LPCTSTR strTitle);
	void      AddLinesToPlane(CPlane* pPlane);

	// Data acquisition
	BOOL      GetLastDataOfTable(fltPoint& pt, LPCTSTR strChPath, LONG lCycle, DWORD dwMode, LPCTSTR strTitle, WORD wPoint);
	fltPoint* GetDataOfTable(LONG& lDataNum, LPCTSTR strChPath, LONG lCycle, DWORD dwMode, LPCTSTR strXAxisTitle, LPCTSTR strYAxisTitle);
	LONG      GetTableIndex(LONG lCycle, DWORD dwMode);

	// Draw data
	void      DrawPlaneOnView(CDC* pDC, CRect ClientRect, BOOL bStairType);
	RECT      GetScopeRect(CDC *pDC, RECT ClientRect);	

	// Show an applied pattern to the channel data
	void      ShowPatternOfChData(LPCTSTR strChPath);

	// Show a Number of channel to mouse point it
	int		  CData::CheckPointInWhichChannel(CRect rect, POINT destPoint);

///////////////////////////////
// Operations: "Common used" //
///////////////////////////////

public:
	BOOL CheckPointWhichPlane(CRect rect, POINT destPoint, int &nPlanIndex, int &nLineIndex, fltPoint &fltDataPoint);
	BOOL ReLoadData();
	BOOL m_bShowTitle;
	COLORREF m_BackColor;

	// For Quick Sort
	static int  LongCompare( const void *arg1, const void *arg2 );
	static int  FloatCompare( const void *arg1, const void *arg2 );

protected:
	void DrawTitle(CDC *pDC, CRect scopeRect);

};

#endif // !defined(AFX_DATA_H__40068522_EC8D_11D4_88DF_006008CEDA07__INCLUDED_)
