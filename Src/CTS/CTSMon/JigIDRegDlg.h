#if !defined(AFX_JIGIDREGDLG_H__6FD05E24_8CF6_48D0_8BBA_8CB621BC2C1E__INCLUDED_)
#define AFX_JIGIDREGDLG_H__6FD05E24_8CF6_48D0_8BBA_8CB621BC2C1E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// JigIDRegDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CJigIDRegDlg dialog

class CJigIDRegDlg : public CDialog
{
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();
	virtual ~CJigIDRegDlg();

// Construction
public:
	CString SearchLocation(int nModuleID, int nJigID);
	BOOL SearchLocation(CString strID, int &nModuleID, int &nJigID);
	BOOL DeleteJigIDInfo(CString strJigID);
	BOOL SaveJigInfoToDB(CString strID, int nModuleID, int nJigNo);
	CJigIDRegDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CJigIDRegDlg)
	enum { IDD = IDD_JIG_LOCATION_DLG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CJigIDRegDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	CGXGridWnd	m_RegGrid;
	CWordArray m_aModuleList;

	// Generated message map functions
	//{{AFX_MSG(CJigIDRegDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	afx_msg LRESULT OnBcrscaned(UINT wParam,LONG lParam);
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_JIGIDREGDLG_H__6FD05E24_8CF6_48D0_8BBA_8CB621BC2C1E__INCLUDED_)
