// CTSAnalView.cpp : implementation of the CCTSAnalView class
//

#include "stdafx.h"
#include "CTSAnal.h"

#include "CTSAnalView.h"
#include "Winbase.h"
#include "ReportDataListRecordSet.h"
#include "LogInDlg.h"
#include "CpkSetDlg.h"

#include "ProgressProcDlg.h"
#include "SelectTrayDataDlg.h"
//#include "DataListDlg.h"
#include "CellListDlg.h"
#include "CellListSaveDlg.h"

#include "ModuleRunTimeDlg.h"
#include "TestLogEditDlg.h"

#ifdef _DEBUG

#include "Mmsystem.h"

#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCTSAnalView

IMPLEMENT_DYNCREATE(CCTSAnalView, CFormView)

BEGIN_MESSAGE_MAP(CCTSAnalView, CFormView)
	//{{AFX_MSG_MAP(CCTSAnalView)
	ON_CBN_SELCHANGE(IDC_COMBO1, OnSelchangeCombo1)
	ON_BN_CLICKED(IDC_BUTTON_SEARCH, OnButtonSearch)
	ON_BN_CLICKED(IDC_REPORT_VIEW1, OnReportView1)
	ON_CBN_SELCHANGE(IDC_COMBO_MONTH1, OnSelchangeComboMonth1)
	ON_CBN_SELCHANGE(IDC_COMBO_MONTH2, OnSelchangeComboMonth2)
	ON_CBN_SELCHANGE(IDC_COMBO_DAY1, OnSelchangeComboDay1)
	ON_CBN_SELCHANGE(IDC_COMBO_DAY2, OnSelchangeComboDay2)
	ON_CBN_SELCHANGE(IDC_COMBO_YEAR1, OnSelchangeComboYear1)
	ON_CBN_SELCHANGE(IDC_COMBO_YEAR2, OnSelchangeComboYear2)
	ON_COMMAND(ID_FILE_SAVE_AS, OnFileSaveAs)
	ON_COMMAND(ID_FILE_NEW, OnFileNew)
	ON_UPDATE_COMMAND_UI(ID_FILE_NEW, OnUpdateFileNew)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE_AS, OnUpdateFileSaveAs)
	ON_COMMAND(ID_DELETE, OnDelete)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_CHECK1, OnCheck1)
	ON_CBN_SELCHANGE(IDC_COMBO2, OnSelchangeCombo2)
	ON_COMMAND(ID_FIELD_SET, OnFieldSet)
	ON_BN_CLICKED(IDC_CPCPK_VIEW, OnCpcpkView)
	ON_BN_CLICKED(IDC_ANAL_PROC, OnAnalProc)
	ON_CBN_SELCHANGE(IDC_DATA_FILTER, OnSelchangeDataFilter)
	ON_BN_CLICKED(IDC_DATE_TIME_SEARCH, OnDateTimeSearch)
	ON_BN_CLICKED(IDC_DELETE_RECORD, OnDeleteRecord)
	ON_COMMAND(ID_FILE_SAVE, OnFileSave)
	ON_BN_CLICKED(IDC_CELL_LIST_SAVE, OnCellListSave)
	ON_BN_CLICKED(IDC_USE_COUNT_MODULE, OnUseCountModule)
	ON_COMMAND(ID_DELETE_TEST, OnDeleteTest)
	ON_COMMAND(ID_CELL_NO_EDIT, OnCellNoEdit)
	ON_COMMAND(ID_MODEL_NAME_EDIT, OnModelNameEdit)
	ON_COMMAND(ID_LOT_NO_EDIT, OnLotNoEdit)
	ON_COMMAND(ID_TRAY_NO_EDIT, OnTrayNoEdit)
	ON_UPDATE_COMMAND_UI(ID_CELL_NO_EDIT, OnUpdateCellNoEdit)
	ON_UPDATE_COMMAND_UI(ID_MODEL_NAME_EDIT, OnUpdateModelNameEdit)
	ON_UPDATE_COMMAND_UI(ID_LOT_NO_EDIT, OnUpdateLotNoEdit)
	ON_UPDATE_COMMAND_UI(ID_TRAY_NO_EDIT, OnUpdateTrayNoEdit)
	ON_COMMAND(ID_DETAIL_VIEW_TEST, OnDetailViewTest)
	ON_COMMAND(ID_DATA_SORT, OnDataSort)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CFormView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CFormView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CFormView::OnFilePrintPreview)
	ON_MESSAGE(WM_GRID_DOUBLECLICK, OnGridDoubleClick)
	ON_MESSAGE(WM_GRID_MOVECELL,OnMovedCurrentCell)
	ON_MESSAGE(WM_GRID_RIGHT_CLICK, OnRButtonClickedRowCol)

END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCTSAnalView construction/destruction

bool CCTSAnalView::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCTSAnalView"), _T("TEXT_CCTSAnalView_CNT"), _T("TEXT_CCTSAnalView_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CCTSAnalView_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCTSAnalView"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}


CCTSAnalView::CCTSAnalView()
	: CFormView(CCTSAnalView::IDD)
{
	LanguageinitMonConfig();

	//{{AFX_DATA_INIT(CCTSAnalView)
	m_strLotFrom = _T("");
	m_strLotTo = _T("");
	m_strTrayFrom = _T("");
	m_strTrayTo = _T("");
	m_dateFrom = 0;
	m_dateTo = 0;
	m_bDateTimeSerarch = FALSE;
	m_strModelName = _T("");
	//}}AFX_DATA_INIT
	// TODO: add construction code here
	m_pDlg = NULL;	
	m_pCpkViewDlg = NULL;//new CCpkViewDlg(this);
	m_bConnected = FALSE;
	m_SelNO = 3;
	m_SelNO2 =  1;
	m_Col   = 2;
	m_TotCol = 0;
	m_beforecasenum = 0;
	m_imsi  = -1;
//	m_C1 = FALSE;
//	m_D1 = FALSE;
	m_BackUp = FALSE;
	m_Save = FALSE;
	m_Clear = FALSE;
	m_Delete = FALSE;
	m_Check = FALSE;
	m_strLogInID = _T("");
	m_strPassWord = _T("");
	m_Remember = FALSE;
	m_DataType = 0;
	m_TestGridDisplay = FALSE;
	m_currentRow = 0;
	m_CellNo = 1;
	m_pFtpDlg = NULL;

	m_dHighLimit = 0.0;
	m_dLowLimit  = 0.0;
	m_dDivision  = 2.0;
	m_nCpItem = RPT_FORM_CHARGE_CAP1;
	
	m_nFilterType = DATA_LOT;
	m_nKeyColumn =  LIST_COLUMN_LOT_NO;
	m_strSearchTitle = TEXT_LANG[0];//"Lot 별 집계"

}

CCTSAnalView::~CCTSAnalView()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}

	STR_MSG_DATA *pCode;
	for( int i = 0; i< m_apDataList.GetSize(); i++)
	{
		pCode = (STR_MSG_DATA *)m_apDataList[i];
		if(pCode)
		{
			delete pCode;
			pCode = NULL;
		}
	}
	m_apDataList.RemoveAll();

	if(m_bConnected)
	{
	}
	
	if(m_pDlg)
	{
		delete	m_pDlg ;
		m_pDlg = NULL;
	}

	if(m_pCpkViewDlg)
	{
		delete  m_pCpkViewDlg;
		m_pCpkViewDlg = NULL;
	}
	
	if(m_ChValue.IsOpen())  	m_ChValue.Close();
	if(m_Test.IsOpen())         m_Test.Close();
	if(m_ChResult.IsOpen())     m_ChResult.Close();
	if(m_Module.IsOpen())    	m_Module.Close();
	if(m_TestLog.IsOpen())      m_TestLog.Close();

	if(m_pFtpDlg != NULL)
	{
		delete m_pFtpDlg;
		m_pFtpDlg = NULL;
	}
}

void CCTSAnalView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCTSAnalView)
	DDX_Control(pDX, IDC_SEARCH_COUNT, m_wndSearchCount);
//	DDX_Control(pDX, IDC_SEL_DATA_LABEL, m_ctrlSelDataLabel);
	DDX_Control(pDX, IDC_DATA_FILTER, m_ctrlFilterCombo);
	DDX_Control(pDX, IDC_COMBO_MONTH2, m_ComboMonth2);
	DDX_Control(pDX, IDC_COMBO_MONTH1, m_ComboMonth1);
	DDX_Control(pDX, IDC_COMBO_DAY2, m_ComboDay2);
	DDX_Control(pDX, IDC_COMBO_DAY1, m_ComboDay1);
	DDX_Control(pDX, IDC_COMBO1, m_Combo);
	DDX_Text(pDX, IDC_LOT_FROM, m_strLotFrom);
	DDX_Text(pDX, IDC_LOT_TO, m_strLotTo);
	DDX_Text(pDX, IDC_TRAY_FROM, m_strTrayFrom);
	DDX_Text(pDX, IDC_TRAY_TO, m_strTrayTo);
	DDX_DateTimeCtrl(pDX, IDC_DATETIME_FROM, m_dateFrom);
	DDX_DateTimeCtrl(pDX, IDC_DATETIME_TO, m_dateTo);
	DDX_Check(pDX, IDC_DATE_TIME_SEARCH, m_bDateTimeSerarch);
	DDX_Text(pDX, IDC_MODEL_NAME, m_strModelName);
	//}}AFX_DATA_MAP
}

BOOL CCTSAnalView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CCTSAnalView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();
	m_Combo.SetCurSel(2);

	m_btnSearch.AttachButton(IDC_BUTTON_SEARCH, stingray::foundation::SECBitmapButton::Al_Left, IDB_SEARCH_BITMAP, this);
//	m_btnModuleTimeLog.AttachButton(IDC_USE_COUNT_MODULE, SECBitmapButton::Al_Left, IDB_TIMER, this);
	m_ctrlFilterCombo.AddString(TEXT_LANG[1]);//"Model별 보기"
	m_ctrlFilterCombo.AddString(TEXT_LANG[2]);//"Lot별 보기"
	m_ctrlFilterCombo.AddString(TEXT_LANG[3]);//"Tray별 보기"
	m_ctrlFilterCombo.SetCurSel(m_nFilterType);

//--------------------DB Open --------------------------//
	try
	{
//		m_db.Open("TestLog");
		m_db.Open(ODBC_PROD_DB_NAME);
		
		m_TestLog.Open();
    	m_Module.Open();
    	m_Test.Open();
    	m_ChValue.Open();
		m_ChResult.Open();
	}
	catch (CDBException* e)
	{
    	AfxMessageBox(e->m_strError);
    	e->Delete();     //DataBase Open Fail
	}
//------------------------------------------------------//
	InitDataGrid();
	InitModuleGrid();
	InitTestGrid();

	GetDlgItem(IDC_COMBO_YEAR1)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_COMBO_MONTH1)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_COMBO_DAY1)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_COMBO_YEAR2)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_COMBO_MONTH2)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_COMBO_DAY2)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_SIMBOL1)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_SIMBOL2)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_SIMBOL3)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_SIMBOL4)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_SIMBOL6)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_SIMBOL7)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_SIMBOL8)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_SIMBOL9)->ShowWindow(SW_HIDE);
//------------------------- 2001.1.24 ------------------------------//
	GetDlgItem(IDC_COMBO_YEAR3)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_COMBO_MONTH3)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_COMBO_DAY3)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_COMBO_YEAR4)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_COMBO_MONTH4)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_COMBO_DAY4)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_SIMBOL10)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_SIMBOL11)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_SIMBOL12)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_SIMBOL13)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_SIMBOL14)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_SIMBOL15)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_SIMBOL16)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_SIMBOL5)->ShowWindow(SW_HIDE);
//	GetDlgItem(IDC_EDIT2)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_COMBO2)->ShowWindow(SW_HIDE);
//-----------------------------------------------------------------//
	RequeryDataSerarch(m_nFilterType);

#ifdef _DEBUG
//	GetDlgItem(IDC_EDIT1)->SetWindowText("LB-010");
//	OnButtonSearch();
//	OnCpcpkView(); 
#endif

	GetDlgItem(IDC_DATETIME_FROM)->EnableWindow(m_bDateTimeSerarch);
	GetDlgItem(IDC_DATETIME_TO)->EnableWindow(m_bDateTimeSerarch);
	m_dateFrom = CTime::GetCurrentTime();
	m_dateTo = CTime::GetCurrentTime();

//	GetDlgItem(IDC_BUTTON_SEARCH)->SetFocus();
	
	UpdateData(FALSE);
}

/////////////////////////////////////////////////////////////////////////////
// CCTSAnalView printing

BOOL CCTSAnalView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	pInfo->SetMaxPage(0xffff);
	pInfo->m_pPD->m_pd.Flags &= ~PD_NOSELECTION;
	pInfo->m_pPD->m_pd.hInstance = AfxGetInstanceHandle();
	return DoPreparePrinting(pInfo);
}

void CCTSAnalView::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	// TODO: add extra initialization before printing
	m_DataGrid.GetParam()->GetProperties()->SetMargins(100,10,50,10);
	m_DataGrid.GetParam()->GetProperties()->SetBlackWhite(FALSE);

//	m_DataGrid.SetStyleRange(CGXRange().SetTable(),
//				CGXStyle().SetFont(CGXFont().SetSize(7)));
	int nAWndGrid[] = {50, 50, 150, 0, 0, 150, 150, 0, 0, 150, 0 , 0};
	m_DataGrid.SetColWidth(1, 16, 0, nAWndGrid, GX_UPDATENOW);
	m_DataGrid.OnGridBeginPrinting(pDC, pInfo);
}

void CCTSAnalView::OnPrepareDC(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	m_DataGrid.OnGridPrepareDC(pDC, pInfo);
//	CFormView::OnPrepareDC(pDC, pInfo);
}

void CCTSAnalView::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	// TODO: add cleanup after printing
	m_DataGrid.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(9)));
	m_DataGrid.OnGridEndPrinting(pDC, pInfo);
}

void CCTSAnalView::OnPrint(CDC* pDC, CPrintInfo* pInfo)
{
	// TODO: add customized printing code here
	CString strLine;//, strLot, strCell, strTray, strTemp;
	strLine = _T("  ");
	for(int i =0; i<98; i++)
		strLine += _T("━");
	strLine += _T("  ");
//	strLot  = m_DataGrid.GetValueRowCol(m_currentRow, 5);
//	strCell = m_DataGrid.GetValueRowCol(m_currentRow, 6);
//	strTray = m_DataGrid.GetValueRowCol(m_currentRow, 7);
//	CString strTmp;//, strModuleNo, strTestName, strCycle, strStep, strTotalTime, strStepTime;
	CGXData& Header	= m_DataGrid.GetParam()->GetProperties()->GetDataHeader();
	CGXData& Footer	= m_DataGrid.GetParam()->GetProperties()->GetDataFooter();

	Header.StoreStyleRowCol(1, 1, CGXStyle().SetValue(""),gxOverride);
	Header.StoreStyleRowCol(1, 2, 
			CGXStyle().SetValue(TEXT_LANG[4]).SetFont(CGXFont().SetSize(14).SetBold(TRUE)), gxOverride);//"FORMATION DATA 검색 LIST"
	Header.StoreStyleRowCol(2, 1, CGXStyle().SetValue(""),gxOverride);
	Header.StoreStyleRowCol(2, 2, CGXStyle().SetValue(""),gxOverride);
	Header.StoreStyleRowCol(3, 1, CGXStyle().SetValue(""),gxOverride);
	Header.StoreStyleRowCol(3, 2, CGXStyle().SetValue(strLine), gxOverride);
	Header.StoreStyleRowCol(4, 1, CGXStyle().SetValue(""),gxOverride);
//	Header.StoreStyleRowCol(4, 2, CGXStyle().SetValue(""),gxOverride);
	
	if(m_strQuerySQL.IsEmpty())
	{
		if(m_nFilterType == DATA_LOT)
		{
			strLine = TEXT_LANG[5];//"검색 조건: Lot별 집계"
		}
		else if(m_nFilterType == DATA_MODEL)
		{
			strLine = TEXT_LANG[6];//"검색 조건: 모델별 집계"
		}
		else if(m_nFilterType == DATA_TRAY)
		{
			strLine = TEXT_LANG[7];//"검색 조건: Tray별 집계"
		}
	}
	else
		strLine.Format(TEXT_LANG[8]+": %s", m_strSearchTitle);//"검색 조건: %s"
	Header.StoreStyleRowCol(4, 2, CGXStyle().SetValue(strLine), gxOverride);

	Footer.StoreStyleRowCol(1, 1, CGXStyle().SetValue(""), gxOverride);
	Footer.StoreStyleRowCol(1, 2, CGXStyle().SetValue("$P/$N"),gxOverride);
	Footer.StoreStyleRowCol(1, 3, CGXStyle().SetValue("$d{%Y/%m/%d %H:%M:%S}"), gxOverride);
	m_DataGrid.OnGridPrint(pDC, pInfo);
}

/////////////////////////////////////////////////////////////////////////////
// CCTSAnalView diagnostics

#ifdef _DEBUG
void CCTSAnalView::AssertValid() const
{
	CFormView::AssertValid();
}

void CCTSAnalView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CCTSAnalDoc* CCTSAnalView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCTSAnalDoc)));
	return (CCTSAnalDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCTSAnalView message handlers
void CCTSAnalView::OnSelchangeCombo1() 
{
	// TODO: Add your control notification handler code here
	CComboBox * pCombo = (CComboBox*)GetDlgItem(IDC_COMBO1);
	ASSERT(pCombo !=NULL);

	m_SelNO = m_Combo.GetCurSel();    

	if(m_SelNO == 1)
	{
		GetDlgItem(IDC_COMBO_YEAR1)->ShowWindow(SW_SHOW);
    	GetDlgItem(IDC_COMBO_MONTH1)->ShowWindow(SW_SHOW);
    	GetDlgItem(IDC_COMBO_DAY1)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COMBO_YEAR2)->ShowWindow(SW_SHOW);
    	GetDlgItem(IDC_COMBO_MONTH2)->ShowWindow(SW_SHOW);
    	GetDlgItem(IDC_COMBO_DAY2)->ShowWindow(SW_SHOW);
    	GetDlgItem(IDC_SIMBOL1)->ShowWindow(SW_SHOW);
    	GetDlgItem(IDC_SIMBOL2)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_SIMBOL3)->ShowWindow(SW_SHOW);
    	GetDlgItem(IDC_SIMBOL4)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_SIMBOL6)->ShowWindow(SW_SHOW);
    	GetDlgItem(IDC_SIMBOL7)->ShowWindow(SW_SHOW);
    	GetDlgItem(IDC_SIMBOL8)->ShowWindow(SW_SHOW);
    	GetDlgItem(IDC_SIMBOL9)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_EDIT1)->ShowWindow(SW_HIDE);
		GetCurrentTime();                                     //2001.1.24
	}
	else
	{
        GetDlgItem(IDC_COMBO_YEAR1)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_COMBO_MONTH1)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_COMBO_DAY1)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMBO_YEAR2)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_COMBO_MONTH2)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_COMBO_DAY2)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_SIMBOL1)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_SIMBOL2)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SIMBOL3)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_SIMBOL4)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SIMBOL6)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_SIMBOL7)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_SIMBOL8)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_SIMBOL9)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_EDIT1)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_EDIT1)->SetWindowText("");
	}
}

void CCTSAnalView::OnSeekCellNo()
{
/*	CString str,str1,str2, str3, str4;
	int n,m;

	GetDlgItem(IDC_EDIT1)->GetWindowText(str);
	if(str.IsEmpty())
	{
    	m_TestLog.m_strFilter.Format(""); 
	}
	else
	{
    	n = str.Find("OR");
    	if(n != 0xffffffff)
		{
        	str1 = str.Mid(0,n-1);
        	str2 = str;
        	str2.Delete(0,n+3);
    		m = str2.Find("OR");
    		if(m != -1)
			{
    			str3 = str2.Mid(0,n-1);
    			str4 = str2;
    			str4.Delete(0,n+3);

    			m_TestLog.m_strFilter.Format("CellNo = %d OR CellNo = %d OR CellNo = %d",atoi(str1),atoi(str3),atoi(str4));
			}
    		else
        		m_TestLog.m_strFilter.Format("CellNo = %d OR CellNo = %d",atoi(str1),atoi(str2));
		}
    	else if(n == 0xffffffff)
		{
        	m_TestLog.m_strFilter.Format("CellNo = %d",atoi(str)); 
		}
	}

	m_TestLog.Requery();
	if(!m_BackUp)  	UpdateResource();
	*/
}
/*
void CCTSAnalView::OnSeekTestLogID()
{
	CString str,str1,str2, str3, str4;
	int n,m;

	GetDlgItem(IDC_EDIT1)->GetWindowText(str);
	if(str.IsEmpty())
	{
    	m_TestLog.m_strFilter.Format(""); 
	}
	else
	{
    	n = str.Find("OR");
    	if(n != 0xffffffff)
		{
        	str1 = str.Mid(0,n-1);
        	str2 = str;
        	str2.Delete(0,n+3);
    		m = str2.Find("OR");
    		if(m != -1)
			{
	    		str3 = str2.Mid(0,n-1);
    			str4 = str2;
    			str4.Delete(0,n+3);

    			m_TestLog.m_strFilter.Format("TestLogID = %d OR TestLogID = %d OR TestLogID = %d",atoi(str1),atoi(str3),atoi(str4));
			}
    		else
        		m_TestLog.m_strFilter.Format("TestLogID = %d OR TestLogID = %d",atoi(str1),atoi(str2));
		}
    	else if(n == 0xffffffff)
		{
        	m_TestLog.m_strFilter.Format("TestLogID = %d",atoi(str)); 
		}
	}

	m_TestLog.Requery();
	if(!m_BackUp)  	UpdateResource();
}
*/

/*
void CCTSAnalView::OnSeekTrayNo()
{
	CString str,str1,str2,str3, str4;
	int n,m;

	if(m_Check == TRUE)
	{
		GetDlgItem(IDC_EDIT2)->GetWindowText(str); 
		if(str.IsEmpty())
		{
    		m_TestLog.m_strFilter.Format(""); 
		}
		else
		{
			n = str.Find("OR");
			if(n != 0xffffffff)
			{
				str1 = str.Mid(0,n-1);
        		str2 = str;
        		str2.Delete(0,n+3);
    			m = str2.Find("OR");
    			if(m != -1)
				{
    				str3 = str2.Mid(0,n-1);
    				str4 = str2;
    				str4.Delete(0,n+3);
    
    				m_TestLog.m_strFilter.Format("TrayNo LIKE '%s' OR TrayNo LIKE '%s' OR TrayNo LIKE '%s'",str1,str3,str4);
				}
    			else
        			m_TestLog.m_strFilter.Format("TrayNo LIKE '%s' OR TrayNo LIKE '%s'",str1,str2);
			}
    		else if(n == 0xffffffff)
			{
        		m_TestLog.m_strFilter.Format("TrayNo LIKE '%s'",str); 
			}
		}
	}
	else
	{
    	GetDlgItem(IDC_EDIT1)->GetWindowText(str);
    	if(str.IsEmpty())
		{
    		m_TestLog.m_strFilter.Format(""); 
		}
    	else
		{
    		n = str.Find("OR");
    		if(n != 0xffffffff)
			{
        		str1 = str.Mid(0,n-1);
        		str2 = str;
        		str2.Delete(0,n+3);
    			m = str2.Find("OR");
    			if(m != -1)
				{
    				str3 = str2.Mid(0,n-1);
    				str4 = str2;
    				str4.Delete(0,n+3);

    				m_TestLog.m_strFilter.Format("TrayNo LIKE '%s' OR TrayNo LIKE '%s' OR TrayNo LIKE '%s'",str1,str3,str4);
				}
    			else
        			m_TestLog.m_strFilter.Format("TrayNo LIKE '%s' OR TrayNo LIKE '%s'",str1,str2);
			}
    		else if(n == 0xffffffff)
			{
        		m_TestLog.m_strFilter.Format("TrayNo LIKE '%s'",str); 
			}
		}
	}
	m_TestLog.Requery();
	if(!m_BackUp)  	UpdateResource();
}
*/

void CCTSAnalView::OnSeekDateTime()
{
	CString year, month, day, date;
	CString year1, month1, day1, date1;

	date.Format("%s/%s/%s",m_Year1,m_Month1,m_Day1);
	date1.Format("%s/%s/%s",m_Year2,m_Month2,m_Day2);
	m_TestLog.m_strFilter.Format("DateTime > #%s# AND DateTime < #%s#",date,date1);
	m_TestLog.Requery();
	if(!m_BackUp)  	UpdateResource();
}

/*
void CCTSAnalView::OnSeekTraySerialNo()
{
	CString str,str1,str2,str3,str4;
	int n,m;

	GetDlgItem(IDC_EDIT1)->GetWindowText(str);

	n = str.Find("OR");
	if(n != 0xffffffff)
	{
    	str1 = str.Mid(0,n-1);
    	str2 = str;
    	str2.Delete(0,n+3);
		m = str2.Find("OR");
		if(m != -1)
		{
			str3 = str2.Mid(0,n-1);
			str4 = str2;
			str4.Delete(0,n+3);

			m_TestLog.m_strFilter.Format("TraySerial LIKE '%s' OR TraySerial LIKE '%s' OR TraySerial LIKE '%s'",str1,str3,str4);
		}
		else
    		m_TestLog.m_strFilter.Format("TraySerial LIKE '%s' OR TraySerial LIKE '%s'",str1,str2);
	}
	else if(n == 0xffffffff)
	{
    	m_TestLog.m_strFilter.Format("TraySerial LIKE '%s'",str); 
	}
	m_TestLog.Requery();
	if(!m_BackUp)  	UpdateResource();
}
*/

void CCTSAnalView::OnSeekTestSerialNo()
{
	CString str,str1,str2,str3,str4;
	int n,m;

	GetDlgItem(IDC_EDIT1)->GetWindowText(str);

	n = str.Find("OR");
	if(n != 0xffffffff)
	{
    	str1 = str.Mid(0,n-1);
    	str2 = str;
    	str2.Delete(0,n+3);

		m = str2.Find("OR");
		if(m != -1)
		{
			str3 = str2.Mid(0,n-1);
			str4 = str2;
			str4.Delete(0,n+3);

			m_TestLog.m_strFilter.Format("TestSerialNo LIKE '%s' OR TestSerialNo LIKE '%s' OR TestSerialNo LIKE '%s'",str1,str3,str4);
		}
		else
    		m_TestLog.m_strFilter.Format("TestSerialNo LIKE '%s' OR TestSerialNo LIKE '%s'",str1,str2);
	}
	else if(n == 0xffffffff)
	{
    	m_TestLog.m_strFilter.Format("TestSerialNo LIKE '%s'",str); 
	}
	m_TestLog.Requery();
	if(!m_BackUp)  	UpdateResource();
}

void CCTSAnalView::OnSeekLotNo()
{
	CString str,str1,str2,str3,str4;
	int n,m;

	GetDlgItem(IDC_EDIT1)->GetWindowText(str);
	if(str.IsEmpty())
	{
   		m_TestLog.m_strFilter.Format(""); 
	}
   	else
	{
/*		str.Remove(',');
		m_TestLog.m_strFilter.Empty();
		
		char szbuff[64];
		while(1)
		{
			if(sscanf((LPSTR)(LPCTSTR)str, "%s", szbuff) <= 0)
				break;
			str1.Format("[LotNo] LIKE '%s'", szbuff);
			
			if(	m_TestLog.m_strFilter.IsEmpty())
				m_TestLog.m_strFilter += str1;
			else
				m_TestLog.m_strFilter = m_TestLog.m_strFilter+ " OR "+str1;
		}
*/
		n = str.Find("OR");
    	if(n != 0xffffffff)
		{
        	str1 = str.Mid(0,n-1);
        	str2 = str;
        	str2.Delete(0,n+3);

    		m = str2.Find("OR");
    		if(m != -1)
			{
    			str3 = str2.Mid(0,n-1);
    			str4 = str2;
    			str4.Delete(0,n+3);

    			m_TestLog.m_strFilter.Format("LotNo LIKE '%s' OR LotNo LIKE '%s' OR LotNo LIKE '%s'",str1,str3,str4);
			}
    		else
        		m_TestLog.m_strFilter.Format("LotNo LIKE '%s' OR LotNo LIKE '%s'",str1,str2);
		}
    	else if(n == 0xffffffff)
		{
        	m_TestLog.m_strFilter.Format("LotNo = '%s'",str); 
		}
	}
	m_TestLog.Requery();
	if(!m_BackUp)  	UpdateResource();
}

void CCTSAnalView::OnButtonSearch() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

#ifdef _DEBUG
	DWORD startTime = timeGetTime();
#endif

	CString strFilter, strTemp;
	m_strSearchTitle.Empty();
	if(!m_strLotFrom.IsEmpty() && !m_strLotTo.IsEmpty())
	{
		strFilter.Format("LotNo >= '%s' && LotNo <= '%s'", m_strLotFrom, m_strLotTo);
	}
	else
	{
		if(!m_strLotFrom.IsEmpty())
		{
			strFilter.Format("LotNo LIKE '%s'", m_strLotFrom);
			m_strSearchTitle += "LotNo = " + m_strLotFrom;
		}
		if(!m_strLotTo.IsEmpty())  
		{
			strFilter.Format("LotNo LIKE '%s'", m_strLotTo);
			m_strSearchTitle += "LotNo = " + m_strLotTo;
		}
	}


	if(!m_strTrayFrom.IsEmpty() && !m_strTrayTo.IsEmpty())
	{
		if(!strFilter.IsEmpty())		strFilter += " AND ";
		strTemp.Format("TrayNo >= '%s' && TrayNo <= '%s'", m_strTrayFrom, m_strTrayTo);
		strFilter += strTemp;
	}
	else
	{
		if(!m_strTrayFrom.IsEmpty())
		{
			if(!strFilter.IsEmpty())
			{
				m_strSearchTitle += ", ";
				strFilter += " AND ";
			}
			strTemp.Format("TrayNo LIKE '%s'", m_strTrayFrom);
			strFilter += strTemp;
			m_strSearchTitle += "TrayNo = " + m_strTrayFrom;
		}
		if(!m_strTrayTo.IsEmpty())
		{
			if(!strFilter.IsEmpty())
			{
				m_strSearchTitle += ", ";
				strFilter += " AND ";
			}
			strTemp.Format("TrayNo LIKE '%s'", m_strTrayTo);
			strFilter += strTemp;
			m_strSearchTitle += "TrayNo = " + m_strTrayTo;
		}
	}

	if(m_bDateTimeSerarch)
	{
		if(!strFilter.IsEmpty())
		{
			m_strSearchTitle += ", ";
			strFilter += " AND ";
		}
		m_strSearchTitle = m_strSearchTitle + m_dateFrom.Format("%Y/%m/%d") +"~"+m_dateTo.Format("%Y/%m/%d");

		strFilter += " DateTime >= #" + m_dateFrom.Format("%Y/%m/%d") +"# AND DateTime <= #" +m_dateTo.Format("%Y/%m/%d")+"#";

//		strFilter += " DateTime >= #" + m_dateFrom.Format("%Y/%m/%d") +"#";
//		strTemp.Format(" AND DateTime <= #%d/%d/%d#", m_dateTo.GetYear(), m_dateTo.GetMonth(), m_dateTo.GetDay()+1);
//		strFilter += " AND DateTime <= #" + m_dateTo.Format("%Y/%m/%d") + "#";
		strFilter += strTemp;
	}

	if(!m_strModelName.IsEmpty())
	{
		if(!strFilter.IsEmpty())
		{
			m_strSearchTitle += ", ";
			strFilter += " AND ";
		}

		m_strSearchTitle += TEXT_LANG[9]+" = " + m_strModelName;//"모델명"
		strFilter += " ModelName LIKE '" + m_strModelName+ "'";
	}

	if(strFilter.IsEmpty())		//모든 Data 검색 
	{
		strTemp = "SELECT TestLogID, TestSerialNo, TraySerialNo, LotNo, TrayNo, DateTime, ModelID, ModelName, TestDone, CellNo FROM Testlog order by TestLogID";
	}
	else
	{
		strTemp = "SELECT TestLogID, TestSerialNo, TraySerialNo, LotNo, TrayNo, DateTime, ModelID, ModelName, TestDone, CellNo FROM Testlog WHERE " + strFilter +" order by TestLogID";
	}

	m_strFilterString = strFilter;			//Where 절 
	m_strQuerySQL = strTemp;				//전체 SQL Query
	
	DisplayListData(m_strQuerySQL);

#ifdef _DEBUG
	TRACE("Search Time %dms\n", timeGetTime() - startTime);
#endif

	
//	UpdateResource();

//    m_TestLog.m_strFilter = strFilter; 
//	m_TestLog.Requery();
	
/*	m_SelNO = m_Combo.GetCurSel();
	
	switch(m_SelNO)
	{
	case 0:
		OnSeekCellNo();
		break;
	case 1:
		OnSeekDateTime();
		break;
	case 2:
		OnSeekLotNo();
		break;
	case 3:
		OnSeekTrayNo();
		break;
	}
	if(m_Check == TRUE)
	{
		switch(m_SelNO2)
		{
    	case 0:
    		OnSeekCellNo();
    		break;
    	case 1:
    		OnSeekDateTime();
    		break;
    	case 2:
    		OnSeekLotNo();
    		break;
    	case 3:
    		OnSeekTrayNo();
    		break;
 		}
	}
*/
}

void CCTSAnalView::InitDataGrid()
{
	m_DataGrid.SubclassDlgItem(IDC_DATALIST, this);
	m_DataGrid.m_bSameRowSize = FALSE;
	m_DataGrid.m_bSameColSize = FALSE;
//	m_DataGrid.m_bCustomColor = FALSE;
//---------------------------------------------------------------------//
	m_DataGrid.m_bCustomWidth = TRUE;
//---------------------------------------------------------------------//
	m_DataGrid.Initialize();

	m_DataGrid.LockUpdate();
	m_DataGrid.EnableCellTips();

	m_DataGrid.SetRowCount(0);
	m_DataGrid.SetColCount(10);
	m_DataGrid.SetDefaultRowHeight(22);

	//Enable Multi Channel Select
	m_DataGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	m_DataGrid.GetParam()->SetSortRowsOnDblClk(TRUE);
	m_DataGrid.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(9).SetFaceName(TEXT_LANG[20])).SetWrapText(FALSE).SetAutoSize(TRUE));//"굴림"

	//Enable Tooltips
//	m_DataGrid.EnableCellTips();

	m_DataGrid.SetValueRange(CGXRange(0, LIST_COLUMN_TEST_LOG_ID), TEXT_LANG[10]);//"ID"
	m_DataGrid.SetValueRange(CGXRange(0, LIST_COLUMN_NO), TEXT_LANG[11]);//"NO"
	m_DataGrid.SetValueRange(CGXRange(0, LIST_COLUMN_CELL_NO), TEXT_LANG[12]);//"Cell 번호"
	m_DataGrid.SetValueRange(CGXRange(0, LIST_COLUMN_TEST_SERIAL_NO), TEXT_LANG[13]);	//"TestSerialNo"
	m_DataGrid.SetValueRange(CGXRange(0, LIST_COLUMN_TRAY_SERIAL), TEXT_LANG[14]);//"TraySerialNo"
	m_DataGrid.SetValueRange(CGXRange(0, LIST_COLUMN_MODEL_NAME), TEXT_LANG[9]);		//"모델명"
	m_DataGrid.SetValueRange(CGXRange(0, LIST_COLUMN_RUN_MODULE), TEXT_LANG[15]);		//"진행수량"
	m_DataGrid.SetValueRange(CGXRange(0, LIST_COLUMN_TEST_DONE), TEXT_LANG[16]);//"완료"
	m_DataGrid.SetValueRange(CGXRange(0, LIST_COLUMN_LOT_NO), TEXT_LANG[17]);//"Batch"		
	m_DataGrid.SetValueRange(CGXRange(0, LIST_COLUMN_TRAY_NO), "Tray ID");
	m_DataGrid.SetValueRange(CGXRange(0, LIST_COLUMN_DATETIME), TEXT_LANG[19]);//"투입일"

	//Row Header Setting
	m_DataGrid.SetStyleRange(CGXRange().SetCols(1),
			CGXStyle().SetEnabled(TRUE).SetTextColor(RGB(0,0,0)).SetInterior(RGB(255,255,255)));
	m_DataGrid.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(9).SetFaceName(TEXT_LANG[20])));//"굴림"
	
	m_DataGrid.LockUpdate(FALSE);
	m_DataGrid.Redraw();
}

void CCTSAnalView::InitModuleGrid()
{
	m_ModuleGrid.SubclassDlgItem(IDC_MODULELIST, this);
	m_ModuleGrid.m_bSameRowSize = FALSE;
	m_ModuleGrid.m_bSameColSize = FALSE;
//	m_ModuleGrid.m_bCustomColor = TRUE;
//---------------------------------------------------------------------//
	m_ModuleGrid.m_bCustomWidth = TRUE;
//---------------------------------------------------------------------//
	m_ModuleGrid.Initialize();

	m_ModuleGrid.LockUpdate();

	m_ModuleGrid.SetRowCount(0);
	m_ModuleGrid.SetColCount(16);
	m_ModuleGrid.SetDefaultRowHeight(18);

	//Enable Multi Channel Select
	m_ModuleGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	m_ModuleGrid.GetParam()->SetSortRowsOnDblClk(TRUE);

/*	m_ModuleGrid.SetValueRange(CGXRange(0,0), "ProcedureID");
	m_ModuleGrid.SetValueRange(CGXRange(0,1), "TestLogID");		//Hide
	m_ModuleGrid.SetValueRange(CGXRange(0,2), "공정 순서");
	m_ModuleGrid.SetValueRange(CGXRange(0,3), "Rack");
	m_ModuleGrid.SetValueRange(CGXRange(0,4), "BoardIndex");		
	m_ModuleGrid.SetValueRange(CGXRange(0,5), "TrayNo");
	m_ModuleGrid.SetValueRange(CGXRange(0,6), "UserID");		
	m_ModuleGrid.SetValueRange(CGXRange(0,7), "GroupIndex");
	m_ModuleGrid.SetValueRange(CGXRange(0,8), "DataTime");
	m_ModuleGrid.SetValueRange(CGXRange(0,9), "TraySerial");
	m_ModuleGrid.SetValueRange(CGXRange(0,10), "TestSerialNo");
	m_ModuleGrid.SetValueRange(CGXRange(0,11), "State");
	m_ModuleGrid.SetValueRange(CGXRange(0,12), "LotNo");
	m_ModuleGrid.SetValueRange(CGXRange(0,13), "SystemIP");
	m_ModuleGrid.SetValueRange(CGXRange(0,14), "TestName");
	m_ModuleGrid.SetValueRange(CGXRange(0,15), "TestID");
	m_ModuleGrid.SetValueRange(CGXRange(0,16), "TestResultFileName");
*/
	m_ModuleGrid.SetValueRange(CGXRange( 0, 9), TEXT_LANG[21]);//"공정순서"
	m_ModuleGrid.SetValueRange(CGXRange( 0, 10), TEXT_LANG[22]);//"공정Type"
	m_ModuleGrid.SetValueRange(CGXRange( 0, 11), TEXT_LANG[23]);//"공정명"
	m_ModuleGrid.SetValueRange(CGXRange( 0, 12), TEXT_LANG[24]);//"입력수량"
	m_ModuleGrid.SetValueRange(CGXRange( 0, 13), TEXT_LANG[25]);//"정상수"
	m_ModuleGrid.SetValueRange(CGXRange( 0, 14), TEXT_LANG[26]);//"전체불량률"
	m_ModuleGrid.SetValueRange(CGXRange( 0, 15), TEXT_LANG[27]);//"공정불량률"
	m_ModuleGrid.SetValueRange(CGXRange( 0, 16), TEXT_LANG[28]);//"진행 Tray 수"


	
	//Row Header Setting
	m_ModuleGrid.SetStyleRange(CGXRange().SetCols(1),
			CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(0,0,0)).SetInterior(RGB(255,255,255)));

	m_ModuleGrid.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(9).SetFaceName(TEXT_LANG[20])).SetWrapText(FALSE).SetAutoSize(TRUE));//"굴림"
	
	m_ModuleGrid.EnableCellTips();
	
	m_ModuleGrid.LockUpdate(FALSE);
	m_ModuleGrid.Redraw();
    //EnableCellTips
}

void CCTSAnalView::InitTestGrid()
{
	FieldSelect();
 	
	m_TestGrid.SubclassDlgItem(IDC_TESTLIST, this);
	m_TestGrid.m_bSameRowSize = FALSE;
	m_TestGrid.m_bSameColSize = TRUE;
	m_TestGrid.m_bCustomWidth = FALSE;
	m_TestGrid.m_bCustomColor = FALSE;

	m_TestGrid.Initialize();
	m_TestGrid.LockUpdate();

	m_TestGrid.EnableCellTips();
	m_TestGrid.SetStyleRange(CGXRange().SetTable(), CGXStyle().SetWrapText(FALSE).SetAutoSize(TRUE));

	m_TestGrid.SetRowCount(135);
	m_TestGrid.SetColCount(3);
	m_TestGrid.SetRowHeight(0,0,0);
	m_TestGrid.SetDefaultRowHeight(18);
	m_TestGrid.SetScrollBarMode(SB_VERT, gxnEnabled | gxnEnhanced);

	m_TestGrid.SetValueRange(CGXRange(1,1),"Data Report");

	m_TestGrid.SetStyleRange(CGXRange().SetRows(2),
			CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(0,0,0)).SetInterior(RGB(192,192,192)));

	m_TestGrid.SetValueRange(CGXRange(132, 1), TEXT_LANG[29]);//"Ave"
	m_TestGrid.SetValueRange(CGXRange(133, 1), TEXT_LANG[30]);//"Max"
	m_TestGrid.SetValueRange(CGXRange(134, 1), TEXT_LANG[31]);//"Min"
	m_TestGrid.SetValueRange(CGXRange(135, 1), TEXT_LANG[32]);//"편차"

 	m_TestGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);

	//Row Header Setting
	m_TestGrid.SetStyleRange(CGXRange().SetRows(132, 135),
			CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(0,0,0)).SetInterior(RGB(192,192,192)));
	m_TestGrid.SetStyleRange(CGXRange().SetCols(1),
			CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(0,0,0)).SetInterior(RGB(192,192,192)));
	m_TestGrid.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(9).SetFaceName(TEXT_LANG[20])));//"굴림"
	
	m_TestGrid.SetFrozenRows(3,0);
	m_TestGrid.LockUpdate(FALSE);
	
	DrawTestGrid();
}

void CCTSAnalView::FieldSelect()
{
	CCTSAnalDoc  *pDoc = GetDocument();
//	int n = 0;
	m_TotCol = 0;
	int i = 0;

	for(i = 0; i < FIELDNO; i++)  
	{
		m_field[i].No = 0;
		m_field[i].FieldName = "";
		m_field[i].ID = 0;
		m_field[i].DataType = 0;
		m_field[i].bDisplay = FALSE;
	}

	for( i = 0; i < pDoc->m_TotCol; i++)
	{
		if(pDoc->m_field[i].bDisplay == TRUE)
		{
   			m_field[m_TotCol].No       = pDoc->m_field[i].No;
    		m_field[m_TotCol].FieldName = pDoc->m_field[i].FieldName;
	    	m_field[m_TotCol].ID = pDoc->m_field[i].ID;
   			m_field[m_TotCol].DataType = pDoc->m_field[i].DataType;
    		m_field[m_TotCol].bDisplay = pDoc->m_field[i].bDisplay;
			m_TotCol++;
		}
	}
}

void CCTSAnalView::DrawTestGrid()
{
	m_TestGrid.LockUpdate();

	m_TestGrid.SetColCount(m_TotCol+1);
	m_TestGrid.SetCoveredCellsRowCol(1, 1, 1, m_TotCol+1);
	
	m_TestGrid.SetValueRange(CGXRange(2,1), "Cell No");
	m_TestGrid.SetCoveredCellsRowCol(2, 1, 3, 1);

	for(int i = 1; i <= m_TotCol; i++)
	{
     	m_TestGrid.SetValueRange(CGXRange(2, i+1), m_field[i-1].FieldName);
		m_TestGrid.SetCoveredCellsRowCol(2, i+1, 3, i+1);
	}

	CString strTemp;
   	for (int nI = 4; nI <= 131; nI++)
	{
   		strTemp.Format("%d",m_CellNo++);
   		m_TestGrid.SetValueRange(CGXRange(nI, 1), strTemp);
	}

	m_TestGrid.LockUpdate(FALSE);
	m_TestGrid.Redraw();
}

void CCTSAnalView::UpdateResource()
{
	CString tmp;
	int    cnt = 0;

	cnt = m_DataGrid.GetRowCount();                    // 현재  Grid의 수를 읽어옴.
	if(cnt >= 1) m_DataGrid.RemoveRows(1, cnt);  

	int i = 1;
	while(!m_TestLog.IsEOF())
	{
		m_DataGrid.InsertRows(i,1);

		tmp.Format("%d",i);
		m_DataGrid.SetValueRange(CGXRange(i,1), tmp);
		m_DataGrid.SetValueRange(CGXRange(i,2), m_TestLog.m_TestLogID);
		m_DataGrid.SetValueRange(CGXRange(i,3), m_TestLog.m_TestSerialNo);
		m_DataGrid.SetValueRange(CGXRange(i,4), m_TestLog.m_TraySerialNo);
    	m_DataGrid.SetValueRange(CGXRange(i,5), m_TestLog.m_LotNo);
		m_DataGrid.SetValueRange(CGXRange(i,6), m_TestLog.m_CellNo);
    	m_DataGrid.SetValueRange(CGXRange(i,7), m_TestLog.m_TrayNo);
    	m_DataGrid.SetValueRange(CGXRange(i,8), m_TestLog.m_DateTime.Format("%Y-%m-%d %H:%M:%S"));

    	m_TestLog.MoveNext();
		i++;
		m_Clear = TRUE;
		m_Save = TRUE;
	}

	if(i == 1)
	{
		AfxMessageBox(TEXT_LANG[33],MB_OK|MB_ICONEXCLAMATION);//"검색하는 내용을 찾을수 없습니다."
		m_Save = FALSE;    
		m_Clear = FALSE;
	}
	else
	{
	}
}

void CCTSAnalView::UpdateResource1()
{
	CString   str;
	if(m_DataType == 1)            // 전   압 
	{
     	for(int i = 4; i <= 131; i++)
		{
    		str.Format("%8.3f",m_ChValue.m_ch[i-4]);
	    	m_TestGrid.SetValueRange(CGXRange(i,m_Col), str);
		}
		str.Format("%.3f",m_Test.m_Average);
		m_TestGrid.SetValueRange(CGXRange(132,m_Col), str);
		str.Format("%.3f",m_Test.m_MaxVal);
    	m_TestGrid.SetValueRange(CGXRange(133,m_Col), str);
		str.Format("%.3f",m_Test.m_MinVal);
    	m_TestGrid.SetValueRange(CGXRange(134,m_Col), str);
		str.Format("%.4f",m_Test.m_STDD);
    	m_TestGrid.SetValueRange(CGXRange(135,m_Col), str);
	}
	else if(m_DataType == 7)        // 시   간
	{
		for(int i = 4; i <= 131; i++)
		{
			str = Time(m_ChValue.m_ch[i-4]);
	    	m_TestGrid.SetValueRange(CGXRange(i,m_Col), str);
		}
		str = Time(m_Test.m_Average);
		m_TestGrid.SetValueRange(CGXRange(132,m_Col), str);
		str = Time(m_Test.m_MaxVal);
    	m_TestGrid.SetValueRange(CGXRange(133,m_Col), str);
		str = Time(m_Test.m_MinVal);
    	m_TestGrid.SetValueRange(CGXRange(134,m_Col), str);
//		str = Time(m_Test.m_STDD);
//    	m_TestGrid.SetValueRange(CGXRange(135,m_Col), str);
	}
	else
	{
		for(int i = 4; i <= 131; i++)
		{
    		str.Format("%8.1f",m_ChValue.m_ch[i-4]);
	    	m_TestGrid.SetValueRange(CGXRange(i,m_Col), str);
		}
		str.Format("%.1f",m_Test.m_Average);
		m_TestGrid.SetValueRange(CGXRange(132,m_Col), str);
		str.Format("%.1f",m_Test.m_MaxVal);
    	m_TestGrid.SetValueRange(CGXRange(133,m_Col), str);
		str.Format("%.1f",m_Test.m_MinVal);
    	m_TestGrid.SetValueRange(CGXRange(134,m_Col), str);
		str.Format("%.4f",m_Test.m_STDD);
    	m_TestGrid.SetValueRange(CGXRange(135,m_Col), str);
	}
}

LRESULT CCTSAnalView::OnMovedCurrentCell(WPARAM wParam, LPARAM lParam)
{
	ROWCOL nRow, nCol;
	CString str;

	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;
	nCol = LOWORD(wParam);
	nRow = HIWORD(wParam);

	if(nRow <1 || nCol <1 )		return 0;

	if(pGrid == (CMyGridWnd *)&m_DataGrid && m_strQuerySQL.IsEmpty())
	{
//		m_strFilterString = m_DataGrid.GetValueRowCol(nRow, m_nKeyColumn);
		str = m_DataGrid.GetValueRowCol(nRow, m_nKeyColumn);
		if(m_nFilterType == DATA_LOT)
		{
			m_strFilterString.Format("LotNo = '%s'", str);
			m_strSearchTitle.Format("LotNo = %s", str);
		}
		else if(m_nFilterType == DATA_TRAY)
		{
			m_strFilterString.Format("TrayNo = '%s'", str);
			m_strSearchTitle.Format("TrayNo = %s", str);
		}
		else
		{
			m_strFilterString.Format("ModelName = '%s'", str);
			m_strSearchTitle.Format("ModelName = %s", str);
		}
//		str = m_DataGrid.GetValueRowCol(nRow, 3);
//		DisplayModuleData(str, m_nFilterType, m_strFilterString);
	}
	return 1;
}

LRESULT CCTSAnalView::OnGridDoubleClick(WPARAM wParam, LPARAM lParam)
{
//	int cellno = 0;
	ROWCOL nRow, nCol;
	CString str,CellNO;

	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;
	nCol = LOWORD(wParam);
	nRow = HIWORD(wParam);

	if(nRow <1 || nCol <1)		return 0;
	
	m_currentRow = nRow;

//	m_C1 = FALSE;
//	m_D1 = FALSE;
	m_Col = 1;                                     //Colum를 Reset함.

	if(pGrid == (CMyGridWnd *)&m_DataGrid)
	{
/*		CCellListDlg *pDlg = new CCellListDlg;
	
		for(int i =0; i<m_TotCol; i++)
		{
			pDlg->m_Field[i].bDisplay = m_field[i].bDisplay;
			pDlg->m_Field[i].DataType = m_field[i].DataType;
			pDlg->m_Field[i].FieldName = m_field[i].FieldName;
			pDlg->m_Field[i].ID = m_field[i].ID;
			pDlg->m_Field[i].No = m_field[i].No;
		}

		if(m_strQuerySQL.IsEmpty())		//목록 Search
		{
//			m_strFilterString = m_DataGrid.GetValueRowCol(nRow, m_nKeyColumn);
			if(m_strFilterString.IsEmpty())
			{
				delete pDlg;
				pDlg = NULL;
				return 0;
			}
			CSelectTrayDataDlg	dlg;
			dlg.m_nFilterType =  m_nFilterType;
			dlg.m_strFilterString = m_strFilterString;
			if(dlg.DoModal() != IDOK)
			{
				delete pDlg;
				pDlg = NULL;
				return 0;
			}
			pDlg->m_strTestLogID = dlg.m_strTestLogID;
		}
		else
		{
			pDlg->m_strTestLogID = m_DataGrid.GetValueRowCol(nRow, LIST_COLUMN_TEST_LOG_ID);
		}
			
		pDlg->m_nFieldNo = m_TotCol;
		pDlg->m_pDBServer = &m_db;

		pDlg->m_pDoc = GetDocument();
		pDlg->DoModal();

		delete pDlg;
		pDlg = NULL;
*/
		ShowCellList(m_DataGrid.GetValueRowCol(nRow, LIST_COLUMN_TEST_LOG_ID));
//		DisplayChData(str);
	}
	else if(pGrid == (CMyGridWnd *)&m_ModuleGrid)
	{
//		m_strFilterString = m_DataGrid.GetValueRowCol(nRow, 5);
		if(m_strFilterString.IsEmpty())	return 0;

		CSelectTrayDataDlg	dlg;
		dlg.m_nFilterType =  m_nFilterType;
		dlg.m_strFilterString = m_strFilterString;
		if(dlg.DoModal() != IDOK)	return 0;
		str = dlg.m_strTestLogID;
		DisplayChData(str);
	}
	
	return 1;	
}

void CCTSAnalView::UpdateTestGrid()
{
	m_ChValue.m_strFilter.Format("TestIndex = %d", m_Test.m_TestIndex);
	m_ChValue.Requery();
	if(!m_ChValue.IsBOF())
	{
		UpdateResource1();
	}
	else
		AfxMessageBox(TEXT_LANG[34],MB_OK|MB_ICONEXCLAMATION);//"레코드가 없습니다."
}

void CCTSAnalView::OnReportView1() 
{
	// TODO: Add your control notification handler code here

	if(m_DataGrid.GetRowCount() <= 0)	return;
	if(m_strFilterString.IsEmpty())		return;

	BeginWaitCursor();
	if(m_pDlg)
	{
		delete m_pDlg;
		m_pDlg = NULL;
	}
	m_pDlg = new CDataDlg(this) ;
	for(int i =0; i<m_TotCol; i++)
	{
		m_pDlg->m_Field[i].bDisplay = m_field[i].bDisplay;
		m_pDlg->m_Field[i].DataType = m_field[i].DataType;
		m_pDlg->m_Field[i].FieldName = m_field[i].FieldName;
		m_pDlg->m_Field[i].ID = m_field[i].ID;
		m_pDlg->m_Field[i].No = m_field[i].No;
	}
	
	m_pDlg->m_nFieldNo = m_TotCol;
	m_pDlg->m_nFilterType = m_nFilterType;
	m_pDlg->m_strFilterString = m_strFilterString;
	
/*	if(m_strQuerySQL.IsEmpty())
	{
		if(m_nFilterType == DATA_LOT)
		{
			m_pDlg->m_strFilterString.Format("[LotNo] = '%s'", m_strFilterString);
		}
		else if(m_nFilterType == DATA_TRAY)
		{
			m_pDlg->m_strFilterString.Format("[TrayNo] = '%s'", m_strFilterString);
		}
		else
		{
			m_pDlg->m_strFilterString.Format("[ModelName] = '%s'", m_strFilterString);
		}
	}
	else
	{
		m_pDlg->m_strFilterString = m_strFilterString;
	}
*/
	m_pDlg->m_pDoc = GetDocument();
	m_pDlg->m_pDBServer = &m_db;
	m_pDlg->m_strTitle = m_strSearchTitle;
//	m_pDlg->DoModal();


	m_pDlg->Create() ;
	m_pDlg->ShowWindow(SW_SHOW);
	EndWaitCursor();
}

void CCTSAnalView::OnSelchangeComboMonth1() 
{
	// TODO: Add your control notification handler code here
	int     index;

	index = m_ComboMonth1.GetCurSel();
	m_ComboMonth1.GetLBText(index,m_Month1);
}

void CCTSAnalView::OnSelchangeComboMonth2() 
{
	// TODO: Add your control notification handler code here
	int     index;

	index = m_ComboMonth2.GetCurSel();
	m_ComboMonth2.GetLBText(index,m_Month2);
}

void CCTSAnalView::OnSelchangeComboDay1() 
{
	// TODO: Add your control notification handler code here
	int     index;

	index = m_ComboDay1.GetCurSel();
	m_ComboDay1.GetLBText(index,m_Day1);
}

void CCTSAnalView::OnSelchangeComboDay2() 
{
	// TODO: Add your control notification handler code here
	int     index;

	index = m_ComboDay2.GetCurSel();
	m_ComboDay2.GetLBText(index,m_Day2);
}

void CCTSAnalView::OnSelchangeComboYear1() 
{
	// TODO: Add your control notification handler code here
	int     index;
	CComboBox * pComboYear1 = (CComboBox*)GetDlgItem(IDC_COMBO_YEAR1);

	index = pComboYear1->GetCurSel();
	pComboYear1->GetLBText(index,m_Year1);
}

void CCTSAnalView::OnSelchangeComboYear2() 
{
	// TODO: Add your control notification handler code here
	int     index;
	CComboBox * pComboYear2 = (CComboBox*)GetDlgItem(IDC_COMBO_YEAR2);

	index = pComboYear2->GetCurSel();
	pComboYear2->GetLBText(index,m_Year2);
}

void CCTSAnalView::OnFileSaveAs() 
{
	// TODO: Add your command handler code here
	CString strFileName;
	strFileName.Format("%s.csv", TEXT_LANG[35]);//"검색목록"
		
	CFileDialog pDlg(FALSE, "", strFileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, TEXT_LANG[36]);//"csv 파일(*.csv)|*.csv|"

	if(IDOK == pDlg.DoModal())
	{
		strFileName = pDlg.GetPathName();
		if(strFileName.IsEmpty())	return ;

		FILE *fp = fopen(strFileName, "wt");
		if(fp == NULL)	return ;

		BeginWaitCursor();

		fprintf(fp, TEXT_LANG[4]+"\n");	// FileName	//"FORMATION DATA 검색 LIST"
		CString strTemp;
		if(m_strQuerySQL.IsEmpty())
		{
			if(m_nFilterType == DATA_LOT)
			{
				strTemp = TEXT_LANG[5];//"검색 조건: Lot별 집계"
			}
			else if(m_nFilterType == DATA_MODEL)
			{
				strTemp = TEXT_LANG[6];//"검색 조건: 모델별 집계"
			}
			else if(m_nFilterType == DATA_TRAY)
			{
				strTemp = TEXT_LANG[7];//"검색 조건: Tray별 집계"
			}
		}
		else
			strTemp.Format(TEXT_LANG[8]+": %s", m_strSearchTitle);//"검색 조건"
		
		fprintf(fp, "%s\n", strTemp);	// FileName
		fprintf(fp, "\n");

		for(int row = 0; row<m_DataGrid.GetRowCount()+1; row++)
		{
			for(int col = 1; col<m_DataGrid.GetColCount()+1; col++)
			{
				fprintf(fp, "%s,", m_DataGrid.GetValueRowCol(row, col));
			}
			fprintf(fp, "\n");
		}
		fclose(fp);
		EndWaitCursor();
	}		
}

void CCTSAnalView::OnFileNew() 
{
	// TODO: Add your command handler code here
//	Clear();
}

void CCTSAnalView::OnDelete() 
{
	// TODO: Add your command handler code here
	DeleteRecord();
}

void CCTSAnalView::BackUp() 
{
	// TODO: Add your control notification handler code here
	int cnt = m_DataGrid.GetRowCount();   //현재 표시된 레코드 수
	if(cnt == 0)                          //레코드가 있으면...
	{
		AfxMessageBox(TEXT_LANG[37]);//"선택된 레코드가 없습니다."
		return;
	}

	CFileDialog dlg(FALSE,"MDB","*.MDB",OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT,
		TEXT_LANG[38]+"(*.MDB)|*.MDB");//"데이터파일"

	if(dlg.DoModal() == IDOK)
	{
/*		m_BackUp =  TRUE;       
		PathName = dlg.GetPathName();

    	SHFILEOPSTRUCT  FileCopy;

    	FileCopy.hwnd   = NULL;
    	FileCopy.wFunc  = FO_COPY;
    	FileCopy.pFrom  = "C:\\Temp\\TestLog(원본).mdb";
    	FileCopy.pTo    = (LPCTSTR)PathName;
    	FileCopy.fFlags = FOF_NOCONFIRMATION | FOF_NOERRORUI | FOF_FILESONLY ;
    	FileCopy.fAnyOperationsAborted = FALSE;
    	FileCopy.hNameMappings         = NULL;
    	FileCopy.lpszProgressTitle     = NULL;

    	SHFileOperation(&FileCopy);

		if(!m_TestLogBk.IsOpen())   m_TestLogBk.Open();
		if(!m_ModuleBk.IsOpen())  	m_ModuleBk.Open();
		if(!m_TestBk.IsOpen())      m_TestBk.Open();
		if(!m_ChValueBk.IsOpen())   m_ChValueBk.Open();
		if(!m_ChResultBk.IsOpen())  m_ChResultBk.Open();
		OnButtonSearch();
		while(!m_Module.IsEOF())
		{
/*			m_ModuleBk.AddNew();

			m_ModuleBk.m_ProcedureID = m_Module.m_ProcedureID;
			m_ModuleBk.m_ModuleID = m_Module.m_ModuleID;
        	m_ModuleBk.m_GroupIndex = m_Module.m_GroupIndex;
            m_ModuleBk.m_TrayNo = m_Module.m_TrayNo;
//    		m_ModuleBk.m_DateTime.SetDateTime(m_Module.m_DateTime.GetYear(),m_Module.m_DateTime.GetMonth(),m_Module.m_DateTime.GetDay(),
//			           m_Module.m_DateTime.GetHour(),m_Module.m_DateTime.GetMinute(),m_Module.m_DateTime.GetSecond());
        	m_ModuleBk.m_LotNo = m_Module.m_LotNo;
        	m_ModuleBk.m_TestSerialNo = m_Module.m_TestSerialNo;
    		m_ModuleBk.m_UserID = m_Module.m_UserID;
    		m_ModuleBk.m_TraySerial = m_Module.m_TraySerial;

    		m_ModuleBk.Update();

			m_Test.m_strFilter.Format("ProcedureID = %d", m_Module.m_ProcedureID);
			m_Test.Requery();
			if(!m_Test.IsBOF())           //결과가 있으면...
			{
	    		while(!m_Test.IsEOF())
				{
	        		m_TestBk.AddNew();

	    			m_TestBk.m_TestIndex   = m_Test.m_TestIndex;
    		    	m_TestBk.m_ProcedureID = m_Test.m_ProcedureID;
    	    		m_TestBk.m_Average     = m_Test.m_Average;
     	    		m_TestBk.m_MaxVal      = m_Test.m_MaxVal;
    	    		m_TestBk.m_MaxChIndex  = m_Test.m_MaxChIndex;
    	    		m_TestBk.m_MinVal      = m_Test.m_MinVal;
    	    		m_TestBk.m_MinChIndex  = m_Test.m_MinChIndex;
    	    		m_TestBk.m_STDD        = m_Test.m_STDD;
    	    		m_TestBk.m_NormalNo    = m_Test.m_NormalNo;
    	    		m_TestBk.m_ProgressID  = m_Test.m_ProgressID;
    	    		m_TestBk.m_TestSerialNo= m_Test.m_TestSerialNo;

    	    		m_TestBk.Update();

					m_ChValue.m_strFilter.Format("TestIndex = %d", m_Test.m_TestIndex);
					m_ChValue.Requery();
					if(!m_ChValue.IsBOF()) //결과가 있으면...
					{
						while(!m_ChValue.IsEOF())
						{
							m_ChValueBk.AddNew();

							m_ChValueBk.m_Index     = m_ChValue.m_Index;
							m_ChValueBk.m_TestIndex = m_ChValue.m_TestIndex;
							for(int i = 0; i < 128; i++)
								m_ChValueBk.m_ch[i] = m_ChValue.m_ch[i];

							m_ChValueBk.Update();
							m_ChValue.MoveNext();
						}
					}
		    		m_Test.MoveNext();
				}
			}

			m_Module.MoveNext();
*/
/*			m_TestLogBk.AddNew();

			m_TestLogBk.m_TestLogID = m_TestLog.m_TestLogID;
			m_TestLogBk.m_TestSerialNo = m_TestLog.m_TestSerialNo;
        	m_TestLogBk.m_TraySerialNo = m_TestLog.m_TraySerialNo;
            m_TestLogBk.m_LotNo = m_TestLog.m_LotNo;
    		m_TestLogBk.m_CellNo = m_TestLog.m_CellNo;
        	m_TestLogBk.m_TrayNo = m_TestLog.m_TrayNo;
//			m_TestLogBk.m_DateTime.SetDateTime(m_TestLog.m_DateTime.GetYear(),m_TestLog.m_DateTime.GetMonth(),m_TestLog.m_DateTime.GetDay(),
//			           m_TestLog.m_DateTime.GetHour(),m_TestLog.m_DateTime.GetMinute(),m_TestLog.m_DateTime.GetSecond());

    		m_TestLogBk.Update();

			m_Module.m_strFilter.Format("TestLogID = %ld", m_TestLog.m_TestLogID);
			m_Module.Requery();
			if(!m_Module.IsBOF())           //결과가 있으면...
			{
	    		while(!m_Module.IsEOF())
				{
	        		m_ModuleBk.AddNew();

	    			m_ModuleBk.m_ProcedureID  = m_Module.m_ProcedureID;
    		    	m_ModuleBk.m_TestLogID    = m_Module.m_TestLogID;
    	    		m_ModuleBk.m_ModuleID     = m_Module.m_ModuleID;
     	    		m_ModuleBk.m_BoardIndex   = m_Module.m_BoardIndex;
    	    		m_ModuleBk.m_TrayNo       = m_Module.m_TrayNo;
    	    		m_ModuleBk.m_UserID       = m_Module.m_UserID;
    	    		m_ModuleBk.m_GroupIndex   = m_Module.m_GroupIndex;
    	    		m_ModuleBk.m_DateTime     = m_Module.m_DateTime;
    	    		m_ModuleBk.m_TraySerial   = m_Module.m_TraySerial;
    	    		m_ModuleBk.m_TestSerialNo = m_Module.m_TestSerialNo;
    	    		m_ModuleBk.m_State        = m_Module.m_State;
					m_ModuleBk.m_LotNo        = m_Module.m_LotNo;
					m_ModuleBk.m_SystemIP     = m_Module.m_SystemIP;
					m_ModuleBk.m_TestName     = m_Module.m_TestName;
					m_ModuleBk.m_TestID       = m_Module.m_TestID;
					m_ModuleBk.m_TestResultFileName = m_Module.m_TestResultFileName;

    	    		m_ModuleBk.Update();

					m_Test.m_strFilter.Format("[ProcedureID] = %ld", m_Module.m_ProcedureID);
					m_Test.Requery();
					if(!m_Test.IsBOF()) //결과가 있으면...
					{
						while(!m_Test.IsEOF())
						{
							m_TestBk.AddNew();

							m_TestBk.m_TestIndex     = m_Test.m_TestIndex;
							m_TestBk.m_ProcedureID   = m_Test.m_ProcedureID;
							m_TestBk.m_TestSerialNo  = m_Test.m_TestSerialNo;
							m_TestBk.m_StepNo        = m_Test.m_StepNo;
							m_TestBk.m_StepType      = m_Test.m_StepType;
//							m_TestBk.m_DateTime   = m_Test.m_ProcedureID;
							m_TestBk.m_TestResultFileName   = m_Test.m_TestResultFileName;
							m_TestBk.m_Average       = m_Test.m_Average;
							m_TestBk.m_MaxVal        = m_Test.m_MaxVal;
							m_TestBk.m_MaxChIndex    = m_Test.m_MaxChIndex;
							m_TestBk.m_MinVal        = m_Test.m_MinVal;
							m_TestBk.m_MinChIndex    = m_Test.m_MinChIndex;
							m_TestBk.m_STDD          = m_Test.m_STDD;
							m_TestBk.m_NormalNo      = m_Test.m_NormalNo;
							m_TestBk.m_ErrorRate     = m_Test.m_ErrorRate;
							m_TestBk.m_ProgressID    = m_Test.m_ProgressID;
							m_TestBk.m_TotalChannel  = m_Test.m_TotalChannel;
							m_TestBk.m_DataType      = m_Test.m_DataType;
							m_TestBk.m_CellNo        = m_Test.m_CellNo;
							m_TestBk.m_Temperature   = m_Test.m_Temperature;
							m_TestBk.m_Gas           = m_Test.m_Gas;							

							m_TestBk.Update();

							m_ChValue.m_strFilter.Format("TestIndex = %ld", m_Test.m_TestIndex);
	         				m_ChValue.Requery();
							if(!m_Test.IsBOF()) //결과가 있으면...
							{
								while(!m_ChValue.IsEOF())
								{
				        			m_ChValueBk.AddNew();

				        			m_ChValueBk.m_Index     = m_ChValue.m_Index;
				        			m_ChValueBk.m_TestIndex = m_ChValue.m_TestIndex;
//									m_ChValueBk.m_dataType  = m_ChValue.m_dataType;
				        			for(int i = 0; i < 128; i++)
				        				m_ChValueBk.m_ch[i] = m_ChValue.m_ch[i];

				            		m_ChValueBk.Update();
				            		m_ChValue.MoveNext();
								}
							}
							m_Test.MoveNext();
						}
					}
					m_ChResult.m_strFilter.Format("ProcedureID = %ld", m_Module.m_ProcedureID);
					m_ChResult.Requery();
					if(!m_ChResult.IsBOF()) //결과가 있으면...
					{
						while(!m_ChResult.IsEOF())
						{
							m_ChResultBk.AddNew();

							m_ChResultBk.m_Index        = m_ChResult.m_Index;
							m_ChResultBk.m_TestSerialNo = m_ChResult.m_TestSerialNo;
							m_ChResultBk.m_dataType     = m_ChResult.m_dataType;
							m_ChResultBk.m_ProcedureID  = m_ChResult.m_ProcedureID;
							for(int i = 0; i < 128; i++)
				       			m_ChResultBk.m_ch[i] = m_ChResult.m_ch[i];
							m_ChResultBk.Update();
							m_ChResult.MoveNext();
						}
					}
		    		m_Module.MoveNext();
				}
			}
			m_TestLog.MoveNext();
		}

		if(m_ChResultBk.IsOpen()) m_ChResultBk.Close();
		if(m_ChValueBk.IsOpen())  m_ChValueBk.Close();
		if(m_TestBk.IsOpen())     m_TestBk.Close();
		if(m_ModuleBk.IsOpen())   m_ModuleBk.Close();
		if(m_TestLogBk.IsOpen())  m_TestLogBk.Close();
*/	}
}		

void CCTSAnalView::Clear()
{
	int row = m_DataGrid.GetRowCount();
	m_DataGrid.RemoveRows(1, row);

	row = m_ModuleGrid.GetRowCount();
	if(row != 0)
		m_ModuleGrid.RemoveRows(1, row);

	int col = m_TestGrid.GetColCount();
   	m_TestGrid.SetValueRange(CGXRange(4, 2, 135, col),"");

	m_Clear = FALSE;
	m_Save = FALSE;
	m_imsi = 0;

	m_TestGridDisplay = TRUE;
}

void CCTSAnalView::DeleteRecord()
{
	int nSelNo;
	CString strQuery, strTemp;
	CString strKey;
	CString strColName, strWhere;
	CString  strTrayNo, strTestSerialNo;
	CString strSQL;

	CRowColArray	awRows;
	m_DataGrid.GetSelectedRows(awRows);
	nSelNo = awRows.GetSize();

	if(nSelNo == 0)  
	{
		AfxMessageBox(TEXT_LANG[39]);//"삭제할 레코드가 없습니다."
		return;
	}
	
	if(AfxMessageBox(TEXT_LANG[40],MB_YESNO|MB_ICONQUESTION) == IDNO)//"레코드가 삭제되어 복구되지 않습니다. 삭제 하시겠습니까?"
		return;

	CDatabase conditionDB;
	try
	{
		conditionDB.Open(ODBC_SCH_DB_NAME);
	}
	catch (CDBException* e)
	{
   		AfxMessageBox(e->m_strError);
   		e->Delete();     //DataBase Open Fail
		return;
	}



	for(int i = (nSelNo-1); i >= 0; i--)
	{
		int index = awRows[i];		//Current Selected Row
		if(index > 0)
		{
			if(m_strQuerySQL.IsEmpty())
			{
				strKey = m_DataGrid.GetValueRowCol(index, m_nKeyColumn);
				if(strKey.IsEmpty())
				{
					strWhere = "IS NULL";
				}
				else
				{
					strWhere.Format("= '%s'", strKey);
					if(m_nFilterType == DATA_LOT)
					{
						strColName = "LotNo";
					}
					else if(m_nFilterType == DATA_TRAY)
					{
						strColName = "TrayNo";
					}
					else
					{
						strColName = "ModelName";
					}
				}
				strTemp.Format(TEXT_LANG[41], strColName, strKey);	//"%s가 [%s]인 모든 Data를 삭제 하시겠습니까?"
				strQuery.Format("DELETE FROM TestLog WHERE %s %s", strColName, strWhere);

				int rtn = MessageBox(strTemp, TEXT_LANG[43], MB_ICONQUESTION|MB_YESNOCANCEL);//"삭제"
				if(rtn == IDCANCEL)		return;
				else if(rtn == IDNO)	continue;

				//삭제 하고자 하는 List의 traySerialNo와 TrayNo를 검색하여 Tray Table에 존재 하면 
				strSQL.Format("SELECT TestSerialNo, TrayNo FROM TestLog WHERE %s %s", strColName, strWhere); 
				CRecordset rs(&m_db);
				try
				{
					if(rs.Open(CRecordset::forwardOnly, strSQL) == FALSE)
						return;
				}
				catch(CDBException *e)
				{
					AfxMessageBox(e->m_strError);
					e->Delete();
					return;
				}
					
				while(!rs.IsEOF())
				{
					rs.GetFieldValue((short)0, strTestSerialNo);	//TestSerialNo
					rs.GetFieldValue((short)1, strTrayNo);			//TrayID
					rs.MoveNext();
					
					//유효한 정보이면 Tray Table에서 현재 진행 중인지 검사 
					if(!strTestSerialNo.IsEmpty() && !strTrayNo.IsEmpty())
					{
////
						strSQL.Format("SELECT count(*) FROM Tray WHERE TrayNo = '%s' AND TestSerialNo = '%s'", strTrayNo, strTestSerialNo);
							
						CRecordset countRs(&conditionDB);
						try
						{
							if(countRs.Open(CRecordset::forwardOnly, strSQL) == FALSE)
								return;
						}
						catch(CDBException *e)
						{
							AfxMessageBox(e->m_strError);
							e->Delete();
							continue;
						}

						CDBVariant va;
						if(!countRs.IsEOF())
						{
							countRs.GetFieldValue((short)0, va, SQL_C_SLONG);
						}
						countRs.Close();

						if(va.m_lVal > 0)
						{
							strTemp.Format(TEXT_LANG[42], strTrayNo);//"Tray [%s]는 현재 진행중인 공정이 있습니다. 삭제 하시겠습니까?"
							if(MessageBox(strTemp, TEXT_LANG[43], MB_ICONQUESTION|MB_YESNO) != IDYES)//"삭제"
							{
								continue;
							}
	/////
							
							//Tray 공정 정보 Reset
							strSQL = "UPDATE Tray SET TestKey = 0, NormalCount = 0, FailCount = 0, OperatorID = '',"; 
							strTemp = "InputCellNo = 0, ModuleID = 0, GroupIndex = 0, LotNo = ''";
							strSQL += strTemp;
							strTemp.Format(" WHERE TrayNo = '%s' AND TestSerialNo = '%s'", strTrayNo, strTestSerialNo);
							strSQL += strTemp;
							conditionDB.BeginTrans();
							conditionDB.ExecuteSQL(strSQL);
							conditionDB.CommitTrans();	//
						}
					}

				}	//while
				rs.Close();
			}
			else
			{
				strQuery.Format("DELETE FROM TestLog WHERE TestLogID = %s", m_DataGrid.GetValueRowCol(index, LIST_COLUMN_TEST_LOG_ID));
				strTestSerialNo = m_DataGrid.GetValueRowCol(index, LIST_COLUMN_TEST_SERIAL_NO);
				strTrayNo = m_DataGrid.GetValueRowCol(index, LIST_COLUMN_TRAY_NO);

				//현재 삭제 Data가 진행 중인 tray일 경우 Tray 상태를 Reset 시킨다.

				strSQL.Format("SELECT count(*) FROM Tray WHERE TrayNo = '%s' AND TestSerialNo = '%s'", strTrayNo, strTestSerialNo); 
				CRecordset countRs(&conditionDB);
				try
				{
					if(countRs.Open(CRecordset::forwardOnly, strSQL) == FALSE)
						return;
				}
				catch(CDBException *e)
				{
					AfxMessageBox(e->m_strError);
					e->Delete();
					continue;
				}

				CDBVariant va;
				if(!countRs.IsEOF())
				{
					countRs.GetFieldValue((short)0, va, SQL_C_SLONG);
				}
				countRs.Close();

				if(va.m_lVal > 0)
				{
					strTemp.Format(TEXT_LANG[42], strTrayNo);//"Tray [%s]는 현재 진행중인 공정이 있습니다. 삭제 하시겠습니까?"
					if(MessageBox(strTemp, TEXT_LANG[43], MB_ICONQUESTION|MB_YESNO) != IDYES)//"삭제"
					{
						continue;
					}
					//Tray 공정 정보 Reset
					if(!strTrayNo.IsEmpty() && !strTestSerialNo.IsEmpty())
					{
						strSQL = "UPDATE Tray SET TestKey = 0, NormalCount = 0, FailCount = 0, OperatorID = '',"; 
						strTemp = "InputCellNo = 0, ModuleID = 0, GroupIndex = 0, LotNo = ''";
						strSQL += strTemp;
						strTemp.Format(" WHERE TrayNo = '%s' AND TestSerialNo = '%s'", strTrayNo, strTestSerialNo);
						strSQL += strTemp;
						conditionDB.BeginTrans();
						conditionDB.ExecuteSQL(strSQL);
						conditionDB.CommitTrans();
					}
				}			
			}

			m_db.BeginTrans();
			m_db.ExecuteSQL( strQuery );
			m_db.CommitTrans();
			
			
			m_DataGrid.RemoveRows(index, index);
		}
	}
	conditionDB.Close();

}

void CCTSAnalView::OnUpdateFileNew(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(m_Clear);
}

void CCTSAnalView::OnUpdateFileSaveAs(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
//	pCmdUI->Enable(m_Save);
}

void CCTSAnalView::OnSize(UINT nType, int cx, int cy) 
{
	CFormView::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	if(::IsWindow(this->GetSafeHwnd()))
	{
		CFormView::ShowScrollBar(SB_HORZ,FALSE);
		CFormView::ShowScrollBar(SB_VERT,FALSE);
		RECT rect;
		::GetClientRect(m_hWnd, &rect);

		CRect rectGrid;
		float width;

/*		if(m_TestGrid.GetSafeHwnd())
		{
			m_TestGrid.GetWindowRect(rectGrid);	//Step Grid Window Position
			ScreenToClient(&rectGrid);
			m_TestGrid.MoveWindow(rectGrid.left, rectGrid.top, rect.right-rectGrid.left-10, rect.bottom - rectGrid.top - 5, FALSE);
		}
*/
		if(m_DataGrid.GetSafeHwnd())
		{
			m_DataGrid.GetWindowRect(rectGrid);	//Step Grid Window Position
			ScreenToClient(&rectGrid);
			m_DataGrid.MoveWindow(rectGrid.left, rectGrid.top, rect.right-rectGrid.left-10, rect.bottom - rectGrid.top -10, FALSE);

			m_DataGrid.GetClientRect(rectGrid);
			width = (float)rectGrid.Width()/100.0f;

			m_DataGrid.m_nWidth[LIST_COLUMN_NO]	= int(width* 8.0f);      //NO.
			m_DataGrid.m_nWidth[LIST_COLUMN_CELL_NO]	= int(width* 10.0f);		//0;//(int)(width* 25.0f);		//DateTime
			m_DataGrid.m_nWidth[LIST_COLUMN_MODEL_NAME]	= (int)(width* 20.0f);		//0;   	//ModelName
			m_DataGrid.m_nWidth[LIST_COLUMN_TEST_SERIAL_NO]	= 0;        				//TestSerialNo
			m_DataGrid.m_nWidth[LIST_COLUMN_TRAY_SERIAL]	= 0;	        			//TraySerialNo															//Watt Hour(mWh)
			m_DataGrid.m_nWidth[LIST_COLUMN_LOT_NO]	= int(width* 16.0f);		//(int)(width* 60.0f);		//LotNo
			m_DataGrid.m_nWidth[LIST_COLUMN_TRAY_NO]	= int(width* 16.0f);		//(int)(width* 30.0f);		//CellNo
			m_DataGrid.m_nWidth[LIST_COLUMN_DATETIME]	= int(width* 30.0f);		//0;//(int)(width* 25.0f);		//DateTime
			m_DataGrid.m_nWidth[LIST_COLUMN_RUN_MODULE]	= 0;						//(int)(width* 25.0f);		
			m_DataGrid.m_nWidth[LIST_COLUMN_TEST_DONE]	= 0;						//(int)(width* 25.0f);		
			m_DataGrid.Redraw();
		}

		if(::IsWindow(GetDlgItem(IDC_SEL_DATA_LABEL)->GetSafeHwnd()))
		{
			GetDlgItem(IDC_SEL_DATA_LABEL)->GetWindowRect(rectGrid);	//Step Grid Window Position
			ScreenToClient(&rectGrid);
			GetDlgItem(IDC_SEL_DATA_LABEL)->MoveWindow(rectGrid.left, rectGrid.top, rect.right-rectGrid.left-5, rect.bottom - rectGrid.top -5, FALSE);
		}


/*		if(m_ModuleGrid.GetSafeHwnd())
		{
			m_ModuleGrid.GetWindowRect(rectGrid);	//Step Grid Window Position
			ScreenToClient(&rectGrid);
			m_ModuleGrid.MoveWindow(rectGrid.left, rectGrid.top, rect.right-rectGrid.left-5, rectGrid.Height(), FALSE);
			
			m_ModuleGrid.GetClientRect(rectGrid);
			width = (float)rectGrid.Width()/100.0f;

/*			m_ModuleGrid.m_nWidth[1]	= 0;        		//ProcedureID
			m_ModuleGrid.m_nWidth[2]	= 0;        		//TestLogID
			m_ModuleGrid.m_nWidth[3]	= (int)(width* 15.0f);	   	//ModuleID															//Watt Hour(mWh)
			m_ModuleGrid.m_nWidth[4]	= 0;	        	//BoardIndex
			m_ModuleGrid.m_nWidth[5]	= 0;	        	//TrayNo
			m_ModuleGrid.m_nWidth[6]	= int(width* 15.0f);		//UserID
			m_ModuleGrid.m_nWidth[7]	= 0;        		//GroupIndex
			m_ModuleGrid.m_nWidth[8]	= int(width* 15.0f);		//DataTime
			m_ModuleGrid.m_nWidth[9]	= 0;            	//TraySerial
			m_ModuleGrid.m_nWidth[10]	= 0;        		//TestSerialNo
			m_ModuleGrid.m_nWidth[11]	= 0;	        	//State
			m_ModuleGrid.m_nWidth[12]	= 0;	        	//LotNo
			m_ModuleGrid.m_nWidth[13]	= 0;        		//SystemIP
			m_ModuleGrid.m_nWidth[14]	= int(width* 15.0f);		//TestName
			m_ModuleGrid.m_nWidth[15]	= 0;        		//TestID
			m_ModuleGrid.m_nWidth[16]	= int(width* 33.0f);  	//TestResultFileName
*/		
/*			m_ModuleGrid.m_nWidth[1]	= 0;        		//ProcedureID
			m_ModuleGrid.m_nWidth[2]	= 0;        		//TestLogID
			m_ModuleGrid.m_nWidth[3]	= 0;	   	//ModuleID															//Watt Hour(mWh)
			m_ModuleGrid.m_nWidth[4]	= 0;	        	//BoardIndex
			m_ModuleGrid.m_nWidth[5]	= 0;	        	//TrayNo
			m_ModuleGrid.m_nWidth[6]	= 0;		//UserID
			m_ModuleGrid.m_nWidth[7]	= 0;        		//GroupIndex
			m_ModuleGrid.m_nWidth[8]	= 0;		//DataTime
			m_ModuleGrid.m_nWidth[9]	= 130;            	//TraySerial
			m_ModuleGrid.m_nWidth[10]	= 130;        		//TestSerialNo
			m_ModuleGrid.m_nWidth[11]	= 130;	        	//State
			m_ModuleGrid.m_nWidth[12]	= 130;	        	//LotNo
			m_ModuleGrid.m_nWidth[13]	= 130;        		//SystemIP
			m_ModuleGrid.m_nWidth[14]	= 130;		//TestName
			m_ModuleGrid.m_nWidth[15]	= 130;        		//TestID
			m_ModuleGrid.m_nWidth[16]	= 130;  	//TestResultFileName
		}
*/
/*		if(::IsWindow(GetDlgItem(IDC_CH_DATA_TITLE)->GetSafeHwnd()))
		{	
			GetDlgItem(IDC_CH_DATA_TITLE)->MoveWindow(rectGrid.left-5, rectGrid.top-5, rect.right-rectGrid.left-5, rectGrid.Height(), FALSE);

		}
*/
	}
}
//------------------------------- 2001. 1. 24 ------------------------------------//
void CCTSAnalView::GetCurrentTime()
{
	CComboBox* pComboYear1 = (CComboBox*)GetDlgItem(IDC_COMBO_YEAR1);
	CComboBox* pComboYear2 = (CComboBox*)GetDlgItem(IDC_COMBO_YEAR2);
	CComboBox* pComboYear3 = (CComboBox*)GetDlgItem(IDC_COMBO_YEAR3);
	CComboBox* pComboYear4 = (CComboBox*)GetDlgItem(IDC_COMBO_YEAR4);
	CComboBox* pComboMonth3 = (CComboBox*)GetDlgItem(IDC_COMBO_MONTH3);
	CComboBox* pComboMonth4 = (CComboBox*)GetDlgItem(IDC_COMBO_MONTH4);
	CComboBox* pComboDay3  = (CComboBox*)GetDlgItem(IDC_COMBO_DAY3);
	CComboBox* pComboDay4  = (CComboBox*)GetDlgItem(IDC_COMBO_DAY4);
	int yearIndex, monthIndex, dayIndex;

	CTime time = CTime::GetCurrentTime();
	m_Year2.Format("%d",time.GetYear());
	m_Month2.Format("%d",time.GetMonth());
	m_Day2.Format("%d",time.GetDay());
	m_Year4.Format("%d",time.GetYear());
	m_Month4.Format("%d",time.GetMonth());
	m_Day4.Format("%d",time.GetDay());

	yearIndex = pComboYear2->FindStringExact(0,m_Year2);
	monthIndex = m_ComboMonth2.FindStringExact(0,m_Month2);
	dayIndex = m_ComboDay2.FindStringExact(0,m_Day2);

	pComboYear2->SetCurSel(yearIndex);
	m_ComboMonth2.SetCurSel(monthIndex);
	m_ComboDay2.SetCurSel(dayIndex);
	pComboYear4->SetCurSel(yearIndex);
	pComboMonth4->SetCurSel(monthIndex);
	pComboDay4->SetCurSel(dayIndex);

	if(m_Month2 == "1")
	{
		yearIndex = yearIndex - 1;
		pComboYear1->GetLBText(yearIndex,m_Year1);
		pComboYear3->GetLBText(yearIndex,m_Year3);
		monthIndex = 11;
		m_ComboMonth1.GetLBText(monthIndex,m_Month1);
		pComboMonth3->GetLBText(monthIndex,m_Month3);
	}
	else
	{
		pComboYear1->GetLBText(yearIndex,m_Year1);
		pComboYear3->GetLBText(yearIndex,m_Year3);
		monthIndex = monthIndex - 1;
		m_ComboMonth1.GetLBText(monthIndex,m_Month1);
		pComboMonth3->GetLBText(monthIndex,m_Month3);
	}
	m_ComboDay1.GetLBText(dayIndex,m_Day1);
	pComboDay3->GetLBText(dayIndex,m_Day3);

	pComboYear1->SetCurSel(yearIndex);
	m_ComboMonth1.SetCurSel(monthIndex);
	m_ComboDay1.SetCurSel(dayIndex);
	pComboYear3->SetCurSel(yearIndex);
	pComboMonth3->SetCurSel(monthIndex);
	pComboDay3->SetCurSel(dayIndex);
}

void CCTSAnalView::OnCheck1() 
{
	// TODO: Add your control notification handler code here
	if(!m_Check)
	{
		m_Check = TRUE;
		m_SelNO2 = 1;
		GetDlgItem(IDC_COMBO_YEAR3)->ShowWindow(SW_SHOW);
    	GetDlgItem(IDC_COMBO_MONTH3)->ShowWindow(SW_SHOW);
    	GetDlgItem(IDC_COMBO_DAY3)->ShowWindow(SW_SHOW);
    	GetDlgItem(IDC_COMBO_YEAR4)->ShowWindow(SW_SHOW);
     	GetDlgItem(IDC_COMBO_MONTH4)->ShowWindow(SW_SHOW);
    	GetDlgItem(IDC_COMBO_DAY4)->ShowWindow(SW_SHOW);
    	GetDlgItem(IDC_SIMBOL10)->ShowWindow(SW_SHOW);
    	GetDlgItem(IDC_SIMBOL11)->ShowWindow(SW_SHOW);
    	GetDlgItem(IDC_SIMBOL12)->ShowWindow(SW_SHOW);
    	GetDlgItem(IDC_SIMBOL13)->ShowWindow(SW_SHOW);
    	GetDlgItem(IDC_SIMBOL14)->ShowWindow(SW_SHOW);
    	GetDlgItem(IDC_SIMBOL15)->ShowWindow(SW_SHOW);
    	GetDlgItem(IDC_SIMBOL16)->ShowWindow(SW_SHOW);
    	GetDlgItem(IDC_SIMBOL5)->ShowWindow(SW_SHOW);
    	GetDlgItem(IDC_EDIT2)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_COMBO2)->ShowWindow(SW_SHOW);

		CComboBox * pCombo2 = (CComboBox*)GetDlgItem(IDC_COMBO2);
		pCombo2->SetCurSel(m_SelNO2);
		GetCurrentTime();
	}
	else
	{
		m_Check = FALSE;
		GetDlgItem(IDC_COMBO_YEAR3)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_COMBO_MONTH3)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_COMBO_DAY3)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_COMBO_YEAR4)->ShowWindow(SW_HIDE);
     	GetDlgItem(IDC_COMBO_MONTH4)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_COMBO_DAY4)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_SIMBOL10)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_SIMBOL11)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_SIMBOL12)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_SIMBOL13)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_SIMBOL14)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_SIMBOL15)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_SIMBOL16)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_SIMBOL5)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_EDIT2)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_COMBO2)->ShowWindow(SW_HIDE);
	}
}

void CCTSAnalView::OnSelchangeCombo2() 
{
	// TODO: Add your control notification handler code here
	CComboBox* pCombo = (CComboBox*)GetDlgItem(IDC_COMBO2);
	m_SelNO2 = pCombo->GetCurSel();
	if(m_SelNO2 == 1)
	{
		GetDlgItem(IDC_COMBO_YEAR3)->ShowWindow(SW_SHOW);
    	GetDlgItem(IDC_COMBO_MONTH3)->ShowWindow(SW_SHOW);
    	GetDlgItem(IDC_COMBO_DAY3)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COMBO_YEAR4)->ShowWindow(SW_SHOW);
    	GetDlgItem(IDC_COMBO_MONTH4)->ShowWindow(SW_SHOW);
    	GetDlgItem(IDC_COMBO_DAY4)->ShowWindow(SW_SHOW);
    	GetDlgItem(IDC_SIMBOL10)->ShowWindow(SW_SHOW);
    	GetDlgItem(IDC_SIMBOL11)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_SIMBOL12)->ShowWindow(SW_SHOW);
    	GetDlgItem(IDC_SIMBOL13)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_SIMBOL14)->ShowWindow(SW_SHOW);
    	GetDlgItem(IDC_SIMBOL15)->ShowWindow(SW_SHOW);
    	GetDlgItem(IDC_SIMBOL16)->ShowWindow(SW_SHOW);
    	GetDlgItem(IDC_SIMBOL5)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_EDIT2)->ShowWindow(SW_HIDE);
		GetCurrentTime();
	}
	else
	{
		GetDlgItem(IDC_COMBO_YEAR3)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_COMBO_MONTH3)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_COMBO_DAY3)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMBO_YEAR4)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_COMBO_MONTH4)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_COMBO_DAY4)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_SIMBOL10)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_SIMBOL11)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SIMBOL12)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_SIMBOL13)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SIMBOL14)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_SIMBOL15)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_SIMBOL16)->ShowWindow(SW_HIDE);
    	GetDlgItem(IDC_SIMBOL5)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_EDIT2)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_EDIT2)->SetWindowText("");
	}
}

BOOL CCTSAnalView::LogInCheck()
{
	CLogInDlg  *pLogInDlg;
	pLogInDlg = new CLogInDlg;

	ASSERT(pLogInDlg);

	if(m_Remember)
	{
		pLogInDlg->m_Remember = m_Remember;
		pLogInDlg->m_strLogInID = m_strLogInID;
		pLogInDlg->m_strPassWord = m_strPassWord;
	}
	if(pLogInDlg->DoModal() != IDOK)   //IDCANCEL
	{
		delete pLogInDlg;
		pLogInDlg = NULL;
		return FALSE;
	}
	else                               //IDOK
	{
 		m_Remember =  pLogInDlg->m_Remember;
		m_strLogInID = pLogInDlg->m_strLogInID;
		m_strPassWord = pLogInDlg->m_strPassWord;

		if(pLogInDlg->m_strLogInID == "elicopower" && pLogInDlg->m_strPassWord == "skc")
		{
			delete pLogInDlg;
	    	pLogInDlg = NULL;
			return TRUE;
		}
    	else
		{
    		AfxMessageBox(TEXT_LANG[44]);//"ID or Password가 다릅니다."
			delete pLogInDlg;
    		pLogInDlg = NULL;
			return FALSE;
		}
	}
}

CString CCTSAnalView::Time(float time)
{
	CString   str;

	int hour,hour1, min, sec;
	long lTime = (long)time;

	hour = lTime / 3600;        hour1 = lTime % 3600;
	min  = hour1  / 60;      
    sec  = hour1  % 60;

	str.Format("%d:%02d:%02d",hour,min,sec);
	return  str;
}

void CCTSAnalView::OnFieldSet() 
{
	// TODO: Add your control notification handler code here

	CCTSAnalDoc *pDoc = GetDocument();
	if(pDoc->SetDataField())
	{
		FieldSelect();				//선택된 Field를 구한다.
		DrawTestGrid();				//선택된 Field로 Grid를 다시 그린다.
	}
}

BOOL CCTSAnalView::DisplayModuleData(CString strTestSerial, int nType, CString strData)
{
/*	m_Module.m_strFilter.Format("TestSerialNo LIKE '%s'",strTestSerial); 
	m_Module.Requery();

	CString tmp;
	int    cnt = 0;

	cnt = m_ModuleGrid.GetRowCount();                    // 현재  Grid의 수를 읽어옮.
	if(cnt >= 1) m_ModuleGrid.RemoveRows(1, cnt);  

	int i = 1;
	while(!m_Module.IsEOF())
	{
		m_ModuleGrid.InsertRows(i,1);

		m_ModuleGrid.SetValueRange(CGXRange(i,1), m_Module.m_ProcedureID);
		m_ModuleGrid.SetValueRange(CGXRange(i,2), m_Module.m_TestLogID);
		if(m_Module.m_ModuleID == 33)
		{
			tmp  = "IR/OCV";
		}
		else
		{
			tmp.Format("Rack %d-%d", (m_Module.m_ModuleID-1)/3+1, (m_Module.m_ModuleID-1)%3+1);
		}

		m_ModuleGrid.SetValueRange(CGXRange(i,3), tmp);
    	m_ModuleGrid.SetValueRange(CGXRange(i,4), m_Module.m_BoardIndex);
		m_ModuleGrid.SetValueRange(CGXRange(i,5), m_Module.m_TrayNo);
    	m_ModuleGrid.SetValueRange(CGXRange(i,6), m_Module.m_UserID);
    	m_ModuleGrid.SetValueRange(CGXRange(i,7), m_Module.m_GroupIndex);
		m_ModuleGrid.SetValueRange(CGXRange(i,8), m_Module.m_DateTime.Format("%Y-%m-%d %H:%M:%S"));
		m_ModuleGrid.SetValueRange(CGXRange(i,9), m_Module.m_TraySerial);
		m_ModuleGrid.SetValueRange(CGXRange(i,10), m_Module.m_TestSerialNo);
		m_ModuleGrid.SetValueRange(CGXRange(i,11), m_Module.m_State);
		m_ModuleGrid.SetValueRange(CGXRange(i,12), m_Module.m_LotNo);
		m_ModuleGrid.SetValueRange(CGXRange(i,13), m_Module.m_SystemIP);
		m_ModuleGrid.SetValueRange(CGXRange(i,14), m_Module.m_TestName);
		m_ModuleGrid.SetValueRange(CGXRange(i,15), m_Module.m_TestID);
		m_ModuleGrid.SetValueRange(CGXRange(i,16), m_Module.m_TestResultFileName);

    	m_Module.MoveNext();
		i++;
	}

	if(i == 1)
	{
		AfxMessageBox("검색하는 내용을 찾을수 없습니다.",MB_OK|MB_ICONEXCLAMATION);
	}

	return TRUE;
*/

	int m_nDspType = 2;

	
	CTestLogSet			testLogSet;
	CModuleRecordSet    moduleLogSet;

	m_ptTestArray.RemoveAll();
		
	testLogSet.m_strSort.Format("[TestLogID]");
	if(nType == DATA_LOT)
	{
		testLogSet.m_strFilter.Format("[LotNo] = '%s'", strData);
	}
	else if(nType == DATA_TRAY)
	{
		testLogSet.m_strFilter.Format("[TrayNo] = '%s'", strData);
	}
	else
	{
		testLogSet.m_strFilter.Format("[ModelName] = '%s'", strData);
	}

	if(m_nDspType == 1)	//완료된 Data 보기
	{
		testLogSet.m_strFilter += " AND [TestDone] = TRUE";
	}
	else if(m_nDspType == 2)	//진행중인 Data
	{
		testLogSet.m_strFilter += " AND [TestDone] = FALSE";
	}

	moduleLogSet.m_strSort.Format("[ProcedureID] ASC");
	try
	{
		if(testLogSet.Open() == FALSE)	return FALSE;
		if(moduleLogSet.Open() == FALSE)	return FALSE;
	}
	catch(CDBException *e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();
		return FALSE;
	}

	CArray<PROC_LIST ,PROC_LIST> ptTempArray;

	while(!testLogSet.IsEOF())
	{
		moduleLogSet.m_strFilter.Format("[TestLogID] = %ld AND [TestSerialNo] = '%s'", testLogSet.m_TestLogID, testLogSet.m_TestSerialNo);
		moduleLogSet.Requery();

		ptTempArray.RemoveAll();

		int i = 0, j = 0;
		while(!moduleLogSet.IsEOF())
		{
			//각 공정이 중복된 시험이 있을 경우 최종 시험 Data를 선택하여 각 공정별 결과 값을 찾는다.
			for(i=0; i < ptTempArray.GetSize(); i++)
			{
				PROC_LIST& data = ptTempArray.ElementAt(i);
				
				if(data.dataID == moduleLogSet.m_TestID)	//같은 시험 이름  
				{
					data.count++;
				//	data.dataID = moduleLogSet.m_TestID;
					data.m_Name = moduleLogSet.m_TestName;
					break;
				}
			}
			
			if(i >= ptTempArray.GetSize())		
			{
				PROC_LIST newData;
				newData.m_Name = moduleLogSet.m_TestName;
				newData.dataID = moduleLogSet.m_TestID;
				newData.count = 1;
				newData.dataType = moduleLogSet.m_ProcedureType;
				newData.strSerial.Format("%s", moduleLogSet.m_TestSerialNo); 

				ptTempArray.Add(newData);
				TRACE("New Lot [%s] Added %d\n", newData.m_Name, newData.count);
			}	
			moduleLogSet.MoveNext();
		}
			
		for(i=0; i < ptTempArray.GetSize(); i++)
		{
			PROC_LIST& data1 = ptTempArray.ElementAt(i);
			for(j =0; j<m_ptTestArray.GetSize(); j++)
			{
				PROC_LIST& data2 = m_ptTestArray.ElementAt(j);
				if(data2.dataID == data1.dataID)	//같은 시험 이름  
				{
					data2.count++;
					RequeryCellCount(data2.dataID, data2.strSerial, data2.nInputNo, data2.nNormalNo);
					break;
				}
			}
			if(j >= m_ptTestArray.GetSize())
			{
				PROC_LIST newData;
				newData.m_Name = data1.m_Name;
				newData.dataID = data1.dataID;
				newData.count = 1;
				newData.dataType = data1.dataType;
				newData.strSerial = data1.strSerial;
				newData.nInputNo = 0;
				newData.nNormalNo = 0;
				RequeryCellCount(data1.dataID, data1.strSerial, newData.nInputNo, newData.nNormalNo);
		//		TRACE("New Lot [%s] Added %d\n", newData.m_Name, newData.count);
				m_ptTestArray.Add(newData);
			}	
		}
		testLogSet.MoveNext();
	}

	UpdateTestList();
	
	testLogSet.Close();
	moduleLogSet.Close();
	return TRUE;
}

BOOL CCTSAnalView::RequeryCellCount(long lTestID, CString strTestSerialNo, int &nInputCount, int &nNormalCount, long nProcdureID)
{
	CChResultSet	resultSet;
	
	if(nProcdureID == 0)
	{
		resultSet.m_strFilter.Format("[TestID] = %ld AND [TestSerialNo] LIKE %s", lTestID, strTestSerialNo);
	}
	else
	{
		resultSet.m_strFilter.Format("[ProcedureID] = %ld", nProcdureID);
	}
	
	resultSet.m_strSort.Format("[Index] DESC");
	resultSet.Open();

	int nNormal = 0, nInput = 0;
	if(!resultSet.IsBOF())
	{
		for(int i = 0; i<EP_BATTERY_PER_TRAY; i++)
		{
			if(!IsNonCell((BYTE)resultSet.m_ch[i]))		nInput++;
			if(IsNormalCell((BYTE)resultSet.m_ch[i]))		nNormal++;
		}
	}
	nInputCount += nInput;
	nNormalCount += nNormal;
	resultSet.Close();
	return TRUE;
}

void CCTSAnalView::UpdateTestList()
{
	int count = m_ModuleGrid.GetRowCount();
	if(count > 0)
	{
		m_ModuleGrid.RemoveRows(1, count);
	}

	if(m_ptTestArray.GetSize() <=0)		return;

	m_ModuleGrid.InsertRows(1, m_ptTestArray.GetSize());
	CString strTemp;
	int prevInput = 0;

	for (int i=0; i < m_ptTestArray.GetSize(); i++)
	{
		PROC_LIST& data = m_ptTestArray.ElementAt(i);
		TRACE("Lot [%s] Added %d\n", data.m_Name, data.count);
		m_ModuleGrid.SetValueRange(CGXRange(i+1, 9), (LONG)(i+1));
		switch(data.dataType)
		{
		case EP_PGS_PRECHARGE:		strTemp	= TEXT_LANG[45];	break;//"PreCharge"
		case EP_PGS_FORMATION:		strTemp	= TEXT_LANG[46];	break;//"Formation"
		case EP_PGS_IROCV:			strTemp	= TEXT_LANG[47];		break;//"IR/OCV"
		case EP_PGS_FINALCHARGE:	strTemp = TEXT_LANG[48];	break;//"출하충방"
		default :					strTemp	= TEXT_LANG[49];	break;//"관계없음"
		}		 
		m_ModuleGrid.SetValueRange(CGXRange(i+1, 10), strTemp);
		m_ModuleGrid.SetValueRange(CGXRange(i+1, 11), data.m_Name);
		m_ModuleGrid.SetValueRange(CGXRange(i+1, 12), (LONG)data.nInputNo);
		m_ModuleGrid.SetValueRange(CGXRange(i+1, 13), (LONG)data.nNormalNo);
		if(data.nInputNo == 0)
		{
			strTemp.Empty();
		}
		else
		{
			strTemp.Format("%.1f%%", (float)(data.nInputNo - data.nNormalNo)/(float)data.nInputNo*100.0f);
		}
		m_ModuleGrid.SetValueRange(CGXRange(i+1, 14), strTemp);
		
		if(i==0)
		{
			prevInput = data.nNormalNo;
		}
		
		if(prevInput == 0)
		{
			strTemp.Empty();
		}
		else
		{
			strTemp.Format("%.1f%%", (float)(prevInput-data.nNormalNo)/(float)prevInput*100.0f);
		}
		
		m_ModuleGrid.SetValueRange(CGXRange(i+1, 15), strTemp);
		m_ModuleGrid.SetValueRange(CGXRange(i+1, 16), (LONG)data.count);
		prevInput = data.nNormalNo;
	}
	
}

BOOL CCTSAnalView::DisplayChData(CString strTestserial)
{
	if(strTestserial.IsEmpty())		return FALSE;

	CWaitCursor wait;
	m_TestGrid.SetValueRange(CGXRange(4, 2, 135, m_TotCol+1),"");
	
	if(!m_Test.IsOpen())
	{
		m_Test.Open();
	}

	int nCol;
	for(int i =0; i<m_TotCol; i++)
	{
		if(m_field[i].ID == EP_REPORT_GRADING || m_field[i].ID == EP_REPORT_CH_CODE)
		{
			RequeryChResult(i+2, strTestserial, m_field[i].ID);
		}
		else
		{
//			m_Test.m_strFilter.Format("[TestSerialNo] = '%s' AND [ProgressID] = %ld", strTestserial, m_field[i].ID);
			//2002/5/1
			m_Test.m_strFilter.Format("[TestSerialNo] = '%s' AND [ProcType] = %ld AND [DataType] = %ld", strTestserial, m_field[i].ID, m_field[i].DataType);
   			m_Test.m_strSort = "[TestIndex] DESC";		//중복될 경우 가장 마지막 Data
			
			try
			{
   				m_Test.Requery();
			}
       		catch (CDBException* e)
			{
       			AfxMessageBox(e->m_strError);
       			e->Delete();
       			return FALSE;	//DataBase Open Fail
			}
		
			if(!m_Test.IsBOF() && !m_Test.IsEOF())                 //처음이 아니면..(선택된것이 있으면..)
			{
				nCol = i+2;
				if(RequeryChVal(nCol, m_Test.m_TestIndex, m_Test.m_DataType) == TRUE)
				{
					CString str;
					switch(m_Test.m_DataType)
					{
					case EP_VOLTAGE:
						str.Format("%.3f",m_Test.m_Average);
						m_TestGrid.SetValueRange(CGXRange(132, nCol), str);
						str.Format("%.3f",m_Test.m_MaxVal); 
    					m_TestGrid.SetValueRange(CGXRange(133, nCol), str);
						str.Format("%.3f",m_Test.m_MinVal);
    					m_TestGrid.SetValueRange(CGXRange(134, nCol), str);
						str.Format("%.4f",m_Test.m_STDD);
    					m_TestGrid.SetValueRange(CGXRange(135, nCol), str);
						break;

					case EP_CURRENT:
						str.Format("%.1f",m_Test.m_Average);
						m_TestGrid.SetValueRange(CGXRange(132, nCol), str);
						str.Format("%.1f",m_Test.m_MaxVal);
    					m_TestGrid.SetValueRange(CGXRange(133, nCol), str);
						str.Format("%.1f",m_Test.m_MinVal);
    					m_TestGrid.SetValueRange(CGXRange(134, nCol), str);
						str.Format("%.4f",m_Test.m_STDD);
    					m_TestGrid.SetValueRange(CGXRange(135, nCol), str);
						break;

					case EP_CAPACITY:
					case EP_IMPEDANCE:
						str.Format("%d", (int)m_Test.m_Average);
						m_TestGrid.SetValueRange(CGXRange(132, nCol), str);
						str.Format("%d", (int)m_Test.m_MaxVal);
    					m_TestGrid.SetValueRange(CGXRange(133, nCol), str);
						str.Format("%d", (int)m_Test.m_MinVal);
    					m_TestGrid.SetValueRange(CGXRange(134, nCol), str);
						str.Format("%.4f", m_Test.m_STDD);
    					m_TestGrid.SetValueRange(CGXRange(135, nCol), str);
						break;

					case EP_STEP_TIME:
					case EP_TOT_TIME:
						str = Time(m_Test.m_Average);
						m_TestGrid.SetValueRange(CGXRange(132, nCol), str);
						str = Time(m_Test.m_MaxVal);
    					m_TestGrid.SetValueRange(CGXRange(133, nCol), str);
						str = Time(m_Test.m_MinVal);
    					m_TestGrid.SetValueRange(CGXRange(134, nCol), str);
						str = Time(m_Test.m_STDD);
    					m_TestGrid.SetValueRange(CGXRange(135, nCol), str);
						break;

					default:
						str.Format("%.3f",m_Test.m_Average);
						m_TestGrid.SetValueRange(CGXRange(132, nCol), str);
						str.Format("%.3f",m_Test.m_MaxVal);
    					m_TestGrid.SetValueRange(CGXRange(133, nCol), str);
						str.Format("%.3f",m_Test.m_MinVal);
    					m_TestGrid.SetValueRange(CGXRange(134, nCol), str);
						str.Format("%.4f",m_Test.m_STDD);
    					m_TestGrid.SetValueRange(CGXRange(135, nCol), str);
						break;
					}
					m_TestGridDisplay = TRUE;
				}
			}
		}
	}
	return TRUE;
}

BOOL CCTSAnalView::RequeryChVal(int nCol, long testIndex, int nDataType)
{
//	m_ChValue.m_strFilter.Format("TestIndex = %d AND [dataType] = %ld", testIndex , nDataType);
	m_ChValue.m_strFilter.Format("[TestIndex] = %d", testIndex);
	m_ChValue.m_strSort = "[Index] DESC ";			//중복된 시험이 있을 경우 가장 마지막 시험을 Load
	m_ChValue.Requery();

	CString strTemp;
	if(!m_ChValue.IsBOF())
	{
//		if(!m_ChValue.IsEOF())	m_ChValue.MoveLast();	
		
    	for(int i = 4; i <= 131; i++)
		{
        	switch(nDataType)
			{
			case EP_VOLTAGE:
				strTemp.Format("%.3f", m_ChValue.m_ch[i-4]);
				break;
			case EP_CURRENT:
				strTemp.Format("%.1f", m_ChValue.m_ch[i-4]);
				break;
			case EP_STEP_TIME:
			case EP_TOT_TIME:
					strTemp = Time(m_ChValue.m_ch[i-4]);
				break;
			case EP_CAPACITY:
			case EP_IMPEDANCE:
				strTemp.Format("%.1f", m_ChValue.m_ch[i-4]);
			default:
				strTemp.Format("%.1f", m_ChValue.m_ch[i-4]);
				break;
			}
			m_TestGrid.SetValueRange(CGXRange(i, nCol), strTemp);
		}
		return TRUE;
	}

//	AfxMessageBox("Channel Value 레코드가 없습니다.",MB_OK|MB_ICONEXCLAMATION);
	m_TestGridDisplay = FALSE;
	return FALSE;
}

BOOL CCTSAnalView::RequeryChResult(int nCol, CString testSerial, int ID)
{
	CCTSAnalDoc *pDoc = (CCTSAnalDoc *)GetDocument();
	if(ID == RPT_CH_CODE)
	{
		m_ChResult.m_strFilter.Format("[TestSerialNo] = '%s' AND [dataType] = %d", testSerial, EP_CH_CODE);
	}
	else
	{
		m_ChResult.m_strFilter.Format("[TestSerialNo] = '%s' AND [dataType] = %d", testSerial, EP_GRADE_CODE);
	}

	m_ChResult.m_strSort = "[Index] DESC ";	//중복된 시험이 있을 경우 가장 마지막 시험을 Load
	try
	{
		m_ChResult.Requery();
	}
	catch( CDBException *e)
	{
		e->Delete();
		return FALSE;
	}

	
	CString strTemp;
	if(!m_ChResult.IsBOF())
	{
		for(int i = 4; i <= 131; i++)
		{
        	switch(ID)
			{
			case RPT_CH_CODE:
				strTemp = pDoc->ChCodeMsg((BYTE)m_ChResult.m_ch[i-4]);
				if(IsNormalCell((BYTE)m_ChResult.m_ch[i-4]))	// 정상
				{
					m_TestGrid.SetStyleRange(CGXRange(i,2,i,m_TotCol+1),
						CGXStyle().SetEnabled(TRUE).SetTextColor(RGB(0,0,0)).SetInterior(RGB(255,255,255)));
				}
   				else if(IsNonCell((BYTE)m_ChResult.m_ch[i-4]))  // NonCell
				{
					m_TestGrid.SetStyleRange(CGXRange(i,2,i,m_TotCol+1),
						CGXStyle().SetEnabled(TRUE).SetTextColor(RGB(0,0,255)).SetInterior(RGB(255,255,255)));
				}
				else	//IsFailCell(), IsSysFail()
				{
					m_TestGrid.SetStyleRange(CGXRange(i,2,i,m_TotCol+1),
						CGXStyle().SetEnabled(TRUE).SetTextColor(RGB(255,0,0)).SetInterior(RGB(255,255,255)));
				}
				break;
			case RPT_GRADING:
				strTemp.Format("%c", (char)m_ChResult.m_ch[i-4]);
				break;
			default:
//				strTemp.Format("%d", (int)m_ChResult.m_ch[i-9]);
				strTemp = "";
				break;
			}
			m_TestGrid.SetValueRange(CGXRange(i, nCol), strTemp);
		}
		return TRUE;
	}
//	AfxMessageBox("Channel Result 레코드가 없습니다.",MB_OK|MB_ICONEXCLAMATION);
	m_TestGridDisplay = FALSE;
	return FALSE;
}

/*
void CCTSAnalView::StateCheck()
{
	CString str;
	BYTE code;

	for(int i = 4; i <= 131; i++)
	{
    	m_TestGrid.SetStyleRange(CGXRange(i,2,i,m_TotCol+1),
	    	CGXStyle().SetEnabled(TRUE).SetTextColor(RGB(0,0,0)).SetInterior(RGB(255,255,255)));
	}

	m_ChResult.m_strFilter.Format("[TestSerialNo] = '%s'", m_DataGrid.GetValueRowCol(m_currentRow, 3));
	m_ChResult.Requery();

   	for(i = 4; i <= 131; i++)
	{		
   		code = (BYTE)m_ChResult.m_ch[i-4];
		
		if(IsNormalCell(code))	// 정상
		{
			m_TestGrid.SetStyleRange(CGXRange(i,2,i,m_TotCol+1),
				CGXStyle().SetEnabled(TRUE).SetTextColor(RGB(0,0,0)).SetInterior(RGB(255,255,255)));
		}
   		else if(IsNonCell(code))  // NonCell
		{
			m_TestGrid.SetStyleRange(CGXRange(i,2,i,m_TotCol+1),
				CGXStyle().SetEnabled(TRUE).SetTextColor(RGB(0,0,255)).SetInterior(RGB(255,255,255)));
		}
		else	//IsFailCell(), IsSysFail()
		{
			m_TestGrid.SetStyleRange(CGXRange(i,2,i,m_TotCol+1),
				CGXStyle().SetEnabled(TRUE).SetTextColor(RGB(255,0,0)).SetInterior(RGB(255,255,255)));
		}
	}
	m_TestGrid.Redraw();
}
*/

void CCTSAnalView::OnCpcpkView() 
{
	// TODO: Add your control notification handler code here
	long           lCount = 0;
	double         dMax = -10E+100, dMin = 10E+100;
	CString        str;

	int row = m_DataGrid.GetRowCount();
	if(row == 0)
	{
		AfxMessageBox(TEXT_LANG[50],MB_OK|MB_ICONWARNING);//"먼저 원하시는 조건을 검색하십시오!"
		return;
	}

	if(!m_strQuerySQL.IsEmpty())
	{
		DataCpCpk();
		return;
	}

//////Cp, Cpk Set Dlg
	CCpkSetDlg     dlgCpkSet;
	dlgCpkSet.m_strHighRange.Format("%f", m_dHighLimit);
	dlgCpkSet.m_strLowRange.Format("%f", m_dLowLimit);
	dlgCpkSet.m_strGraphGap.Format("%f", m_dDivision);
	dlgCpkSet.m_nItem = m_nCpItem;
   	
	for(int i =0; i<m_TotCol; i++)
	{
		dlgCpkSet.m_field[i].No = m_field[i].No;
		dlgCpkSet.m_field[i].FieldName = m_field[i].FieldName;
		dlgCpkSet.m_field[i].ID = m_field[i].ID;
   		dlgCpkSet.m_field[i].DataType = m_field[i].DataType;
		dlgCpkSet.m_field[i].bDisplay = m_field[i].bDisplay;
	}
	dlgCpkSet.m_nCount = m_TotCol;
	
	if(dlgCpkSet.DoModal() != IDOK)	return;
	
	m_dHighLimit = atof(dlgCpkSet.m_strHighRange);
	m_dLowLimit  = atof(dlgCpkSet.m_strLowRange);
	m_dDivision  = atof(dlgCpkSet.m_strGraphGap);
	m_nCpItem = dlgCpkSet.m_nItem;
//////
	int procType = m_field[m_nCpItem].ID;
	int dataType = m_field[m_nCpItem].DataType;
	if(m_ChResult.IsOpen())  m_ChResult.Close();	//시험 결과 DataBase Open
	m_ChResult.Open();
	
	if(m_strFilterString.IsEmpty())		return;

/*	if(m_nFilterType == DATA_LOT)
	{
		m_TestLog.m_strFilter.Format("[LotNo] = '%s'", m_strFilterString);	
	}
	else if(m_nFilterType == DATA_TRAY)
	{
		m_TestLog.m_strFilter.Format("[TrayNo] = '%s'", m_strFilterString);
	}
	else
	{
		m_TestLog.m_strFilter.Format("[ModelName] = '%s'", m_strFilterString);
	}
*/	m_TestLog.m_strFilter = m_strFilterString;
	m_TestLog.Requery();

	if(m_TestLog.IsBOF())	return;
	
	CWaitCursor wait;
	while(!m_TestLog.IsEOF())
	{
     	m_ChResult.m_strFilter.Format("[TestSerialNo] = '%s' AND [dataType] = %d", m_TestLog.m_TestSerialNo, EP_CH_CODE);
       	m_ChResult.m_strSort.Format("[Index] DESC");		//현재 Tray 최종 상태를 선택
//		m_Test.m_strFilter.Format( "[TestSerialNo] = '%s' AND [ProgressID] = %ld", m_TestLog.m_TestSerialNo, m_nCpItem);
		m_Test.m_strFilter.Format("[TestSerialNo] = '%s' AND [ProcType] = %ld AND [DataType] = %ld", m_TestLog.m_TestSerialNo, procType, dataType);
       	m_Test.m_strSort.Format("[TestIndex] DESC");		//중복된 시험이 있을 경우 가장 마지막 시험을 Load
   		try			
		{
			m_ChResult.Requery();
   			m_Test.Requery();
		}
   		catch (CDBException* e)
		{
      		AfxMessageBox(e->m_strError);
      		e->Delete();
     		return;	
		}

		if(!m_Test.IsBOF())
		{
//			m_ChValue.m_strFilter.Format("[TestIndex] = %d AND [DataType] = %d", m_Test.m_TestIndex, m_Test.m_DataType);	//현재 Test의 값을 Read
			m_ChValue.m_strFilter.Format("[TestIndex] = %d", m_Test.m_TestIndex);	//현재 Test의 값을 Read
	   		m_ChValue.m_strSort.Format("[Index] DESC");
			
			m_ChValue.Requery();
				
			if(!m_ChValue.IsBOF())
			{
				for(int i = 0; i < EP_BATTERY_PER_TRAY; i++)
				{						
					if(!IsNormalCell((BYTE)m_ChResult.m_ch[i]))	 continue; //정상이 아니면 계산 않함
					
   					lCount++;												//양품수
					if(dMin > (double)m_ChValue.m_ch[i])  dMin = (double)m_ChValue.m_ch[i];	//Min
					if(dMax < (double)m_ChValue.m_ch[i])  dMax = (double)m_ChValue.m_ch[i];	//Max
					
//					TRACE("Data %d: %f\n", lCount, m_ChValue.m_ch[i]);
				}
			}
		}
		m_TestLog.MoveNext();
//		m_Module.MoveNext();
	}

	TRACE("Total %d Cell Searched, Max %f, Min %f\n", lCount, dMax, dMin);

	if(lCount <= 0)
	{
		MessageBox(TEXT_LANG[51],TEXT_LANG[52] , MB_ICONSTOP|MB_OK);//"범위내의 해당하는 Cell이 없습니다.", "Data 오류"
		return;
	}
	//Memory 할당 받는다. (양품수로 해서 => 최대 가능 범위)
	double *pData = new double[lCount];
	ASSERT(pData);
	ZeroMemory(pData, sizeof(double)*lCount);
	
	lCount = 0;

	//선택된 Cp Setting에 해당 하는 Data만 검색 
//	for(i = 1; i <= row; i++)
//	{

//	m_Module.Requery();
//	if(m_Module.IsBOF())	return;

	m_TestLog.MoveFirst();
	while(!m_TestLog.IsEOF())
	{
     	m_ChResult.m_strFilter.Format("[TestSerialNo] = '%s' AND [dataType] = %d", m_TestLog.m_TestSerialNo, EP_CH_CODE);
       	m_ChResult.m_strSort.Format("[Index] DESC");		//현재 Tray 최종 상태를 선택
//		m_Test.m_strFilter.Format( "[TestSerialNo] = '%s' AND [ProgressID] = %ld", m_TestLog.m_TestSerialNo, m_nCpItem);
		m_Test.m_strFilter.Format("[TestSerialNo] = '%s' AND [ProcType] = %ld AND [DataType] = %ld", m_TestLog.m_TestSerialNo, procType, dataType);
      	m_Test.m_strSort.Format("[TestIndex] DESC");		//중복된 시험이 있을 경우 가장 마지막 시험을 Load
  	
		try			
		{
			m_ChResult.Requery();
			m_Test.Requery();
		}
		catch (CDBException* e)
		{
     		AfxMessageBox(e->m_strError);
     		e->Delete();
			return;	
		}
		if(!m_Test.IsBOF())	
		{
//			m_ChValue.m_strFilter.Format("[TestIndex] = %d AND [DataType] = %d", m_Test.m_TestIndex, m_Test.m_DataType);	//현재 Test의 값을 Read
			m_ChValue.m_strFilter.Format("[TestIndex] = %d", m_Test.m_TestIndex);	//현재 Test의 값을 Read
			m_ChValue.m_strSort = "[Index] DESC";					//중복된 시험이 있을 경우 가장 마지막 시험을 Load
			m_ChValue.Requery();

			if(m_ChValue.IsBOF())	continue;

			for(int i = 0; i < EP_BATTERY_PER_TRAY; i++)
			{						
				if(!IsNormalCell((BYTE)m_ChResult.m_ch[i]))  continue;					//정상이아니면  
				
	//			TRACE("Data %d: %f\n", lCount, m_ChValue.m_ch[i]);
				
				if(dlgCpkSet.m_bRange == TRUE)
				{
		   			if(dlgCpkSet.m_bHighRange == TRUE && dlgCpkSet.m_bLowRange == TRUE) //상하한 설정시  
					{
   		   				if(m_dLowLimit <= m_ChValue.m_ch[i] && m_ChValue.m_ch[i] <= m_dHighLimit)
						{
							pData[lCount++] = (double)m_ChValue.m_ch[i];
						}		    					
					}
					else if(dlgCpkSet.m_bHighRange == TRUE && dlgCpkSet.m_bLowRange == FALSE)//상한만
					{
						if(m_ChValue.m_ch[i] <= m_dHighLimit)
						{
							pData[lCount++] = (double)m_ChValue.m_ch[i];
						}
					}
					else if(dlgCpkSet.m_bHighRange == TRUE && dlgCpkSet.m_bLowRange == FALSE)//하한만
					{
			  			if(m_dLowLimit <= m_ChValue.m_ch[i])
						{
							pData[lCount++] = (double)m_ChValue.m_ch[i];
						}
					}
				}
				else   // m_pCpkSetDlg.m_bRange = FALSE
				{
					pData[lCount++] = (double)m_ChValue.m_ch[i];
				}
			}
		}
		m_TestLog.MoveNext();
//		m_Module.MoveNext();
	}
		
	ASSERT(lCount > 0);	
	//Cp, Cpk Graph  Display
	if(m_pCpkViewDlg)
	{
   		delete m_pCpkViewDlg;
   		m_pCpkViewDlg = NULL;
	}
   	m_pCpkViewDlg = new CCpkViewDlg(this) ;
	m_pCpkViewDlg->SetRowData(pData, lCount, m_dDivision);
	m_pCpkViewDlg->SetHighLimit(dlgCpkSet.m_bHighRange, m_dHighLimit);
	m_pCpkViewDlg->SetLowLimit(dlgCpkSet.m_bLowRange, m_dLowLimit);

	m_pCpkViewDlg->m_strTitle.Format(TEXT_LANG[53], m_strSearchTitle, m_field[m_nCpItem].FieldName);//"%s %s  분포도"

	m_pCpkViewDlg->DoModal();

	delete [] pData;
	pData = NULL;
		
}

void CCTSAnalView::OnAnalProc() 
{
	// TODO: Add your control notification handler code here
	CString strTestLogID;
	ROWCOL nRow, nCol;
	if(m_DataGrid.GetCurrentCell(nRow, nCol) == FALSE)	return;
	if(nRow <1 || nCol <1)		return ;
	
	if(m_strQuerySQL.IsEmpty())		//목록 Search
	{
		CSelectTrayDataDlg	*pSelDlg = new CSelectTrayDataDlg(this);
		pSelDlg->m_nFilterType =  m_nFilterType;
		pSelDlg->m_strFilterString = m_strFilterString;
		if(pSelDlg->DoModal() == IDOK)
		{
			strTestLogID = pSelDlg->m_strTestLogID;
		}
		delete pSelDlg;
	}
	else
	{
		strTestLogID = m_DataGrid.GetValueRowCol(nRow, LIST_COLUMN_TEST_LOG_ID);
	}

	if(strTestLogID.IsEmpty())	return;

	CProgressProcDlg *pDlg = new CProgressProcDlg(this);
	pDlg->SetDataBase(&m_db, strTestLogID);
	pDlg->DoModal();
	delete pDlg;
}

void CCTSAnalView::OnSelchangeDataFilter() 
{
	// TODO: Add your control notification handler code here
	m_nFilterType = m_ctrlFilterCombo.GetCurSel();
	if(m_nFilterType == DATA_LOT)		//Lot별 보기 
	{
		m_nKeyColumn =  LIST_COLUMN_LOT_NO;
	}
	else if(m_nFilterType == DATA_TRAY)	//Tray 별 보기 
	{
		m_nKeyColumn = LIST_COLUMN_TRAY_NO;
	}
	else					//Model 별 보기
	{
		m_nKeyColumn = LIST_COLUMN_MODEL_NAME;
	}
	m_strQuerySQL.Empty();
	

	RequeryDataSerarch(m_nFilterType);
	
}

BOOL CCTSAnalView::RequeryDataSerarch(int dataType)
{
	CWaitCursor wait;
	//
	CRecordset rs( &m_db );
	CDBVariant va;
	CString strQuery;

	if(dataType == DATA_LOT)		//Lot별 보기 
	{
		strQuery = "SELECT LotNo, COUNT(*) FROM Testlog GROUP BY LotNo";
	}
	else if(dataType == DATA_TRAY)	//Tray 별 보기 
	{
		strQuery = "SELECT TrayNo, COUNT(*)  FROM Testlog GROUP BY TrayNo";
	}
	else 
	{
		strQuery = "SELECT ModelName, COUNT(*)  FROM Testlog GROUP BY ModelName";
	}
	rs.Open( CRecordset::forwardOnly, strQuery);
	
	m_ptArray.RemoveAll();
	while(!rs.IsEOF())
	{
		DATA_LIST newData;
		rs.GetFieldValue((short)0, newData.m_Name);
		rs.GetFieldValue((short)1, va, SQL_C_SLONG);
		newData.count = va.m_lVal;
		m_ptArray.Add(newData);
		rs.MoveNext();
	}
	rs.Close();

	
/*	CTestLogSet testLogSet;
	testLogSet.m_strSort.Format("[TestLogID]");

	try
	{
		if(testLogSet.Open() == FALSE)
			return FALSE;
	}
	catch(CDBException *e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();
		return FALSE;
	}

	m_ptArray.RemoveAll();

	while(!testLogSet.IsEOF())
	{
		for(int i=0; i < m_ptArray.GetSize(); i++)
		{
			DATA_LIST& data = m_ptArray.ElementAt(i);
			
			if(dataType == DATA_LOT)		//Lot별 보기 
			{
				if(data.m_Name == testLogSet.m_LotNo)		//같은 Lot가 있으면 
				{
					data.count++;
					break;
				}
			}
			else if(dataType == DATA_TRAY)	//Tray 별 보기 
			{
				if(data.m_Name == testLogSet.m_TrayNo)		//같은 Tray 있으면 
				{
					data.count++;
					break;
				}
			}
			else					//Model 별 보기
			{
				if(data.m_Name == testLogSet.m_ModelName)	//같은 Model 
				{
					data.count++;
					break;
				}
			}
		}
		
		if(i >= m_ptArray.GetSize())
		{
			DATA_LIST newData;
			if(dataType == DATA_LOT)		//Lot별 보기 
			{
				newData.m_Name = testLogSet.m_LotNo;
			}
			else if(dataType == DATA_TRAY)	//Tray 별 보기 
			{
				newData.m_Name = testLogSet.m_TrayNo;
			}
			else					//Model 별 보기
			{
				newData.m_Name = testLogSet.m_ModelName;
			}
			newData.count = 1;
			m_ptArray.Add(newData);
//			TRACE("New Lot [%s] Added %d\n", newData.m_Name, newData.count);
		}
		testLogSet.MoveNext();
	}	
	testLogSet.Close();
*/	
	
	UpdateDataList();
	
	return TRUE;
}

void CCTSAnalView::UpdateDataList()
{
	int count = m_DataGrid.GetRowCount();
	if(count > 0)
	{
		m_DataGrid.RemoveRows(1, count);
	}

	if(m_ptArray.GetSize() <= 0)	return;

	m_DataGrid.InsertRows(1, m_ptArray.GetSize());

//	m_DataGrid.SetValueRange(CGXRange(0, 1), (LONG)(i+1));
	m_DataGrid.SetValueRange(CGXRange(0, LIST_COLUMN_DATETIME), TEXT_LANG[54]);//"투입 Tray 수"
	
	for (int i=0; i < m_ptArray.GetSize(); i++)
	{
		DATA_LIST& data = m_ptArray.ElementAt(i);
		TRACE("Lot [%s] Added %d\n", data.m_Name, data.count);
		m_DataGrid.SetValueRange(CGXRange(i+1, 1), (LONG)(i+1));
		m_DataGrid.SetValueRange(CGXRange(i+1, m_nKeyColumn), data.m_Name);
		m_DataGrid.SetValueRange(CGXRange(i+1, LIST_COLUMN_DATETIME), (LONG)data.count);
	}
	
//	m_DataGrid.SetCurrentCell(i, 5);
}


BOOL CCTSAnalView::DisplayListData(CString strSQL)
{
	if(strSQL.IsEmpty())		return FALSE;

	int    cnt = 0;
	cnt = m_DataGrid.GetRowCount();                    // 현재  Grid의 수를 읽어옮.
	if(cnt >= 1) m_DataGrid.RemoveRows(1, cnt);  

	CRecordset rs( &m_db );
	CDBVariant va;
	CString tmp;

	CWaitCursor wait;

	rs.Open( CRecordset::forwardOnly, strSQL );

	/*	0: TestLogID, 
		1: TestSerialNo, 
		2: TraySerialNo, 
		3: LotNo, 
		4: TrayNo, 
		5: DateTime, 
		6: ModelID, 
		7: ModelName, 
		8: TestDone ;
	*/
//	m_DataGrid.SetValueRange(CGXRange(0, LIST_COLUMN_TEST_LOG_ID), "ID");
//	m_DataGrid.SetValueRange(CGXRange(0, LIST_COLUMN_NO), "NO");
//	m_DataGrid.SetValueRange(CGXRange(0, LIST_COLUMN_CELL_NO), "Cell 번호");
//	m_DataGrid.SetValueRange(CGXRange(0, LIST_COLUMN_TEST_SERIAL_NO), "TestSerialNo");		
//	m_DataGrid.SetValueRange(CGXRange(0, LIST_COLUMN_TRAY_SERIAL), "TraySerialNo");
//	m_DataGrid.SetValueRange(CGXRange(0, LIST_COLUMN_MODEL_NAME), "모델명");		
//	m_DataGrid.SetValueRange(CGXRange(0, LIST_COLUMN_TEST_DONE), "완료");
//	m_DataGrid.SetValueRange(CGXRange(0, LIST_COLUMN_LOT_NO), "Batch");		
//	m_DataGrid.SetValueRange(CGXRange(0, LIST_COLUMN_TRAY_NO), "Tray ID");		
	m_DataGrid.SetValueRange(CGXRange(0, LIST_COLUMN_DATETIME), TEXT_LANG[55]);//"투입일"

	int i = 1;
	while(!rs.IsEOF())
	{
		m_DataGrid.InsertRows(1,1);
	
		tmp.Format("%d", i);
		m_DataGrid.SetValueRange(CGXRange(1, LIST_COLUMN_NO), tmp);

		rs.GetFieldValue( (short)0, va, SQL_C_SLONG );		//TestLogID
		m_DataGrid.SetValueRange(CGXRange(1, LIST_COLUMN_TEST_LOG_ID), va.m_lVal);

		rs.GetFieldValue( (short)1, tmp);					//TestSerialNo
		m_DataGrid.SetValueRange(CGXRange(1, LIST_COLUMN_TEST_SERIAL_NO), tmp);

		rs.GetFieldValue( (short)2, va, SQL_C_SLONG );		//TraySerialNo	
		m_DataGrid.SetValueRange(CGXRange(1, LIST_COLUMN_TRAY_SERIAL), m_TestLog.m_TraySerialNo);
  
		rs.GetFieldValue( (short)3, tmp);		//LotNo
	  	m_DataGrid.SetValueRange(CGXRange(1, LIST_COLUMN_LOT_NO), tmp);

		rs.GetFieldValue( (short)4, tmp );		//TrayNo
		m_DataGrid.SetValueRange(CGXRange(1, LIST_COLUMN_TRAY_NO), tmp);
 
		rs.GetFieldValue( (short)5, tmp);		//DateTime
	  	m_DataGrid.SetValueRange(CGXRange(1, LIST_COLUMN_DATETIME), tmp);
 
//		rs.GetFieldValue( (short)6, va, SQL_C_SLONG );		//ModelID
//		m_DataGrid.SetValueRange(CGXRange(i, LIST_COLUMN_TEST_DONE), va.m_lVal);

		rs.GetFieldValue( (short)7, tmp);		//ModelName
	  	m_DataGrid.SetValueRange(CGXRange(1, LIST_COLUMN_MODEL_NAME), tmp);

		rs.GetFieldValue( (short)8, va, SQL_C_SLONG );		//TestDone
	  	m_DataGrid.SetValueRange(CGXRange(1, LIST_COLUMN_TEST_DONE), va.m_lVal);
	
		rs.GetFieldValue( (short)9, va, SQL_C_SLONG );		//TestLogID
		m_DataGrid.SetValueRange(CGXRange(1, LIST_COLUMN_CELL_NO), va.m_lVal);
				
		tmp.Format(TEXT_LANG[56], i);//"전체 %d건의 Data가 조회 되었습니다."
		m_wndSearchCount.SetText(tmp);
		i++;
		rs.MoveNext();
	}
	rs.Close();

	if(i > 1)
		m_DataGrid.SetCurrentCell(1, LIST_COLUMN_NO);
	
	return FALSE;
}

void CCTSAnalView::OnDateTimeSearch() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	GetDlgItem(IDC_DATETIME_FROM)->EnableWindow(m_bDateTimeSerarch);
	GetDlgItem(IDC_DATETIME_TO)->EnableWindow(m_bDateTimeSerarch);	
}

BOOL CCTSAnalView::DataCpCpk()
{
	if(m_strQuerySQL.IsEmpty())		return FALSE;
	int nRow = m_DataGrid.GetRowCount();
	if(nRow <= 0)						return TRUE; 

//////Cp, Cpk Set Dlg
	CCpkSetDlg     dlgCpkSet;
	dlgCpkSet.m_strHighRange.Format("%f", m_dHighLimit);
	dlgCpkSet.m_strLowRange.Format("%f", m_dLowLimit);
	dlgCpkSet.m_strGraphGap.Format("%f", m_dDivision);
	dlgCpkSet.m_nItem = m_nCpItem;
	
	int i = 0;
   	
	for(i =0; i<m_TotCol; i++)
	{
		dlgCpkSet.m_field[i].No = m_field[i].No;
		dlgCpkSet.m_field[i].FieldName = m_field[i].FieldName;
		dlgCpkSet.m_field[i].ID = m_field[i].ID;
   		dlgCpkSet.m_field[i].DataType = m_field[i].DataType;
		dlgCpkSet.m_field[i].bDisplay = m_field[i].bDisplay;
	}
	dlgCpkSet.m_nCount = m_TotCol;
	
	if(dlgCpkSet.DoModal() != IDOK)	return	FALSE;
	
	m_dHighLimit = atof(dlgCpkSet.m_strHighRange);
	m_dLowLimit  = atof(dlgCpkSet.m_strLowRange);
	m_dDivision  = atof(dlgCpkSet.m_strGraphGap);
	m_nCpItem = dlgCpkSet.m_nItem;
//////

	int procType = m_field[m_nCpItem].ID;
	int dataType = m_field[m_nCpItem].DataType;

	if(m_ChResult.IsOpen())  m_ChResult.Close();	//시험 결과 DataBase Open
	m_ChResult.Open();
	
	CString strTestSerial;
	long           lCount = 0;
	double         dMax = -10E+100, dMin = 10E+100;
	CString        str;

#ifdef _DEBUG
	DWORD startTime = timeGetTime();
#endif
	
	CWaitCursor wait;

	for(i =0; i<nRow; i++)
	{	
		strTestSerial = m_DataGrid.GetValueRowCol(i+1, LIST_COLUMN_TEST_SERIAL_NO);
		if(strTestSerial.IsEmpty())  continue;

     	m_ChResult.m_strFilter.Format("[TestSerialNo] = '%s' AND [dataType] = %d", strTestSerial, EP_CH_CODE);
       	m_ChResult.m_strSort.Format("[Index] DESC");		//현재 Tray 최종 상태를 선택
		m_Test.m_strFilter.Format("[TestSerialNo] = '%s' AND [ProcType] = %ld AND [DataType] = %ld", strTestSerial, procType, dataType);
       	m_Test.m_strSort.Format("[TestIndex] DESC");		//중복된 시험이 있을 경우 가장 마지막 시험을 Load
   		try			
		{
			m_ChResult.Requery();
   			m_Test.Requery();
		}
   		catch (CDBException* e)
		{
      		AfxMessageBox(e->m_strError);
      		e->Delete();
      		return FALSE;	
		}

		if(!m_Test.IsBOF())
		{
			m_ChValue.m_strFilter.Format("[TestIndex] = %d", m_Test.m_TestIndex);	//현재 Test의 값을 Read
	   		m_ChValue.m_strSort.Format("[Index] DESC");
			
			m_ChValue.Requery();
				
			if(!m_ChValue.IsBOF())
			{
				for(int i = 0; i < EP_BATTERY_PER_TRAY; i++)
				{						
					if(!IsNormalCell((BYTE)m_ChResult.m_ch[i]))	 continue; //정상이 아니면 계산 않함
					
   					lCount++;												//양품수
					if(dMin > (double)m_ChValue.m_ch[i])  dMin = (double)m_ChValue.m_ch[i];	//Min
					if(dMax < (double)m_ChValue.m_ch[i])  dMax = (double)m_ChValue.m_ch[i];	//Max
					
//					TRACE("Data %d: %f\n", lCount, m_ChValue.m_ch[i]);
				}
			}
		}
		m_TestLog.MoveNext();
	}

	TRACE("Total %d Cell Searched, Max %f, Min %f\n", lCount, dMax, dMin);

	if(lCount <= 0)
	{
		MessageBox(TEXT_LANG[51],TEXT_LANG[52] , MB_ICONSTOP|MB_OK);//"범위내의 해당하는 Cell이 없습니다.", "Data 오류"
		return FALSE;
	}
	//Memory 할당 받는다. (양품수로 해서 => 최대 가능 범위)
	double *pData = new double[lCount];
	ASSERT(pData);
	ZeroMemory(pData, sizeof(double)*lCount);
	
	lCount = 0;

	m_TestLog.MoveFirst();

	for(i =0; i<nRow; i++)
	{	
 		strTestSerial = m_DataGrid.GetValueRowCol(i+1, LIST_COLUMN_TEST_SERIAL_NO);
		if(strTestSerial.IsEmpty())  continue;

    	m_ChResult.m_strFilter.Format("[TestSerialNo] = '%s' AND [dataType] = %d", strTestSerial, EP_CH_CODE);
       	m_ChResult.m_strSort.Format("[Index] DESC");		//현재 Tray 최종 상태를 선택
		m_Test.m_strFilter.Format("[TestSerialNo] = '%s' AND [ProcType] = %ld AND [DataType] = %ld", strTestSerial, procType, dataType);
      	m_Test.m_strSort.Format("[TestIndex] DESC");		//중복된 시험이 있을 경우 가장 마지막 시험을 Load
  	
		try			
		{
			m_ChResult.Requery();
			m_Test.Requery();
		}
		catch	(CDBException* e)
		{
     			AfxMessageBox(e->m_strError);
     			e->Delete();
				return FALSE;	
		}
		if(!m_Test.IsBOF())	
		{
			m_ChValue.m_strFilter.Format("[TestIndex] = %d", m_Test.m_TestIndex);	//현재 Test의 값을 Read
			m_ChValue.m_strSort = "[Index] DESC";					//중복된 시험이 있을 경우 가장 마지막 시험을 Load
			m_ChValue.Requery();

			if(m_ChValue.IsBOF())	continue;

			for(int i = 0; i < EP_BATTERY_PER_TRAY; i++)
			{						
				if(!IsNormalCell((BYTE)m_ChResult.m_ch[i]))  continue;					//정상이아니면  
				
	//			TRACE("Data %d: %f\n", lCount, m_ChValue.m_ch[i]);
				
				if(dlgCpkSet.m_bRange == TRUE)
				{
		   			if(dlgCpkSet.m_bHighRange == TRUE && dlgCpkSet.m_bLowRange == TRUE) //상하한 설정시  
					{
   		   				if(m_dLowLimit <= m_ChValue.m_ch[i] && m_ChValue.m_ch[i] <= m_dHighLimit)
						{
							pData[lCount++] = (double)m_ChValue.m_ch[i];
						}		    					
					}
					else if(dlgCpkSet.m_bHighRange == TRUE && dlgCpkSet.m_bLowRange == FALSE)//상한만
					{
						if(m_ChValue.m_ch[i] <= m_dHighLimit)
						{
							pData[lCount++] = (double)m_ChValue.m_ch[i];
						}
					}
					else if(dlgCpkSet.m_bHighRange == TRUE && dlgCpkSet.m_bLowRange == FALSE)//하한만
					{
			  			if(m_dLowLimit <= m_ChValue.m_ch[i])
						{
							pData[lCount++] = (double)m_ChValue.m_ch[i];
						}
					}
				}
				else   // m_pCpkSetDlg.m_bRange = FALSE
				{
					pData[lCount++] = (double)m_ChValue.m_ch[i];
				}
			}
		}
		m_TestLog.MoveNext();
	}
		
	ASSERT(lCount > 0);	
	
#ifdef _DEBUG
	TRACE("Search Time %dms\n", timeGetTime() - startTime);
#endif


	//Cp, Cpk Graph  Display
	if(m_pCpkViewDlg)
	{
   		delete m_pCpkViewDlg;
   		m_pCpkViewDlg = NULL;
	}
   	m_pCpkViewDlg = new CCpkViewDlg(this) ;
	
	m_pCpkViewDlg->SetRowData(pData, lCount, m_dDivision);
	m_pCpkViewDlg->SetHighLimit(dlgCpkSet.m_bHighRange, m_dHighLimit);
	m_pCpkViewDlg->SetLowLimit(dlgCpkSet.m_bLowRange, m_dLowLimit);
	m_pCpkViewDlg->m_strTitle.Format(TEXT_LANG[53], m_strSearchTitle, m_field[m_nCpItem].FieldName);//"%s %s  분포도"
	
	m_pCpkViewDlg->DoModal();

	delete [] pData;
	pData = NULL;

	
	return TRUE;		
}

void CCTSAnalView::OnDeleteRecord() 
{
	// TODO: Add your control notification handler code here
	DeleteRecord();
}

void CCTSAnalView::OnFileSave() 
{
	// TODO: Add your command handler code here
	OnFileSaveAs();
}

void CCTSAnalView::OnCellListSave() 
{
	if(m_strQuerySQL.IsEmpty())
	{
		AfxMessageBox(TEXT_LANG[57]);//"검색된 List가 없습니다."
		return;
	}
	
	CString strFileName;
	int nSelItem;
	int nOpType;
	float fVal;

	CCTSAnalDoc *pDoc = GetDocument();
	CCellListSaveDlg *pDlg = new CCellListSaveDlg(this);
	pDlg->SetField(m_field, m_TotCol);

	if(pDlg->DoModal() != IDOK)
	{
		delete pDlg;
		pDlg = NULL;
		return;
	}
	strFileName = pDlg->m_strFileName;
	nSelItem = pDlg->m_nSelItem;
	nOpType = pDlg->m_nOpType;
	fVal = pDlg->m_fVal1;

	delete pDlg;
	pDlg = NULL;
	
	CString strTemp, strSQL, strQuery, strTestLogID;

	if(strFileName.IsEmpty())	return;

	FILE *fp = fopen(strFileName, "wt");
	if(fp == NULL)
	{
		strTemp = strFileName +TEXT_LANG[58];//"을 생성할 수 없습니다."
		AfxMessageBox(strTemp, MB_ICONSTOP|MB_OK);
		return ;
	}

//	BeginWaitCursor();
	strTemp = TEXT_LANG[59];//"Cell List를 저장 중 입니다..."
	pDoc->SetProgressWnd(0, 1000, strTemp);

	fprintf(fp, TEXT_LANG[60]+":, %s\n", m_strFilterString);	// FileName		//"검색 범위"
	fprintf(fp, "\n");
	
	BYTE code[128], save[128];

	CString strLot, strTray, strModelName, strDateTime, strColumnHeader;
	int nModulleID, cellNo;
	short index;
	
	CRecordset rs(&m_db);
	CDBVariant val;

	strQuery = "SELECT a.ch1";
	for(int i = 2; i<=128; i++)
	{
		strTemp.Format(", a.ch%d", i);
		strQuery += strTemp;
	}

	int totCount = 0;
	CString *pStrBuff;
	pStrBuff = new CString[128];
	
	int nRowCount = m_DataGrid.GetRowCount();
	for(int nRow = 1; nRow <= nRowCount; nRow++)
	{
		pDoc->SetProgressPos( 1000/nRowCount*nRow);
		memset(code, 0, sizeof(code));		//
		memset(save, 0, sizeof(save));		// 
		
		strTestLogID = m_DataGrid.GetValueRowCol(nRow, LIST_COLUMN_TEST_LOG_ID);


		strTemp.Format(", b.TrayNo, b.LotNo, b.DateTime, b.ModelName, b.CellNo FROM ChResult a, TestLog b WHERE a.index = (SELECT MAX(index) FROM ChResult c WHERE c.TestSerialNo = b.TestSerialNo AND c.dataType = %d)", EP_CH_CODE);
		strSQL = strQuery + strTemp;
		strTemp.Format(" AND a.TestSerialNo = b.TestSerialNo AND b.TestLogID = %s", strTestLogID);
		strSQL += strTemp;

		//최종 불량 코드 검색 
		rs.Open(CRecordset::forwardOnly, strSQL);
		if(!rs.IsBOF())
		{
			rs.GetFieldValue((short)128, strTray);
			rs.GetFieldValue((short)129, strLot);
			rs.GetFieldValue((short)130, strDateTime);
			rs.GetFieldValue((short)131, strModelName);
			rs.GetFieldValue((short)132, val, SQL_C_SLONG);
			cellNo = val.m_lVal;
			for(index = 0; index<128; index++)
			{
				rs.GetFieldValue(index, val, SQL_C_FLOAT);
				code[index] = (BYTE)val.m_fltVal; 

				if(IsNormalCell(code[index]))			//기본으로 정상인 Cell에 대해서만 저장 
				{
					save[index] = 1;
					pStrBuff[index].Format("%d, %d, %s, %s, %s", index+1, cellNo+index, strTray, strLot, strModelName);
				}
			}
			strColumnHeader.Format("ChNo, CellNo, TrayNo, LotNo, "+TEXT_LANG[61]);//"모델명"
		}
		rs.Close();

		if(nSelItem >= 0)		//조건이 있으면 
		{
			memset(save, 0, sizeof(save));		 

			//조건 만족하는 Data 검색 
			strTemp.Format(", d.ModuleID, d.DateTime FROM ChValue a, TestLog b, Test c, Module d WHERE a.TestIndex = (SELECT MAX(e.TestIndex) FROM Test e WHERE e.TestSerialNo = b.TestSerialNo AND e.ProcType = %d AND e.DataType = %d)", m_field[nSelItem].ID, m_field[nSelItem].DataType);
			strSQL = strQuery + strTemp;
			strTemp.Format(" AND b.TestLogID= %s AND a.TestIndex = c.TestIndex AND d.ProcedureID = c.ProcedureID", strTestLogID);
			strSQL += strTemp;
			
			rs.Open(CRecordset::forwardOnly, strSQL);
			if(!rs.IsBOF())
			{
				rs.GetFieldValue(short(128), val, SQL_C_SLONG);				nModulleID = val.m_lVal;
				rs.GetFieldValue(short(129), strDateTime);
				for(index = 0; index<128; index++)
				{
					rs.GetFieldValue(index, val, SQL_C_FLOAT);

					if(m_field[nSelItem].DataType == EP_CH_CODE)		//Channel Code 일 경우
					{
						switch(nOpType)
						{
						case	0:	// >
							if(code[index] > (BYTE)fVal)
							{
								save[index] = 1;	totCount++;
							}
							break;
						case	1:	// >=
							if(code[index] >= (BYTE)fVal)
							{
								save[index] = 1;	totCount++;
							}
							break;
						case	2:	// <
							if(code[index] < (BYTE)fVal)
							{
								save[index] = 1;	totCount++;
							}
							break;
						case	3:	// <=
							if(code[index] <= (BYTE)fVal)
							{
								save[index] = 1;	totCount++;
							}
							break;
						case 4:
						default:
							if(code[index] == (BYTE)fVal)
							{
								save[index] = 1;	totCount++;
							}
						}
					}
					else
					{
						if(IsNormalCell(code[index]) )		//정상 Cell이고 조건 만족하는 Cell 
						{
							switch(nOpType)
							{
							case	0:	// >
								if(val.m_fltVal > fVal)
								{
									save[index] = 1;	totCount++;
								}
								break;
							case	1:	// >=
								if(val.m_fltVal >= fVal)
								{
									save[index] = 1;	totCount++;
								}
								break;
							case	2:	// <
								if(val.m_fltVal < fVal)
								{
									save[index] = 1;	totCount++;
								}
								break;
							case	3:	// <=
								if(val.m_fltVal <= fVal)
								{
									save[index] = 1;	totCount++;
								}
								break;
							case 4:
							default:
								if(val.m_fltVal == fVal)
								{
									save[index] = 1;	totCount++;
								}
							}
						}
					}
				}
			}
			rs.Close();
		}
		
		//전체 Data 검색 
		int prevProcType = 0;
		for(short field = 0; field<m_TotCol; field++)
		{
			strTemp.Format(", d.ModuleID, d.DateTime FROM ChValue a, TestLog b, Test c, Module d WHERE a.TestIndex = (SELECT MAX(e.TestIndex) FROM Test e WHERE e.TestSerialNo = b.TestSerialNo AND e.ProcType = %d AND e.DataType = %d)", m_field[field].ID, m_field[field].DataType);
			strSQL = strQuery + strTemp;
			strTemp.Format(" AND b.TestLogID= %s AND a.TestIndex = c.TestIndex AND d.ProcedureID = c.ProcedureID", strTestLogID);
			strSQL += strTemp;
			
			rs.Open(CRecordset::forwardOnly, strSQL);
			if(!rs.IsBOF())
			{
				rs.GetFieldValue(128, val, SQL_C_SLONG);		nModulleID = val.m_lVal;
				rs.GetFieldValue(129, strDateTime);
				for(index = 0; index < 128; index++)
				{
					if(save[index])
					{
						rs.GetFieldValue(index, val, SQL_C_FLOAT);
						if(prevProcType != m_field[field].ID)
						{
							if(m_field[field].DataType == EP_STEP_TIME)
								strTemp.Format(", Rack %d-%d, %s, %s", (nModulleID-1)/3+1, (nModulleID-1)%3+1, strDateTime, pDoc->ValueString(val.m_fltVal, EP_STEP_TIME));
							else
								strTemp.Format(", Rack %d-%d, %s, %f", (nModulleID-1)/3+1, (nModulleID-1)%3+1, strDateTime, val.m_fltVal);

						}
						else
						{
							if(m_field[field].DataType == EP_STEP_TIME)
								strTemp.Format(", %s", pDoc->ValueString(val.m_fltVal, EP_STEP_TIME));
							else
								strTemp.Format(", %f", val.m_fltVal);
						}					
						pStrBuff[index] += strTemp;
					}
				}
			}
			if(prevProcType != m_field[field].ID)
			{
				prevProcType = m_field[field].ID;
				strTemp.Format(", Stage, "+TEXT_LANG[62]+", %s", m_field[field].FieldName);//"작업시각"
			}
			else
			{
				strTemp.Format(", %s", m_field[field].FieldName);
			}
				
			strColumnHeader += strTemp;
			rs.Close();
		}

		if(nRow == 1)
			fprintf(fp, "%s\n", strColumnHeader);
		
		for(index = 0; index < 128; index++)
		{
			if(save[index])
			{
				fprintf(fp, "%s\n", pStrBuff[index]);
				totCount++;
			}
		}
	}
	delete[] pStrBuff;
	pStrBuff = NULL;
	
	fprintf(fp, TEXT_LANG[63], totCount);//"%d Cell이 검색 되었습니다.\n"
	fclose(fp);	

	pDoc->HideProgressWnd();
}

void CCTSAnalView::OnUseCountModule() 
{
	// TODO: Add your control notification handler code here
	
//	CModuleRunTimeDlg *m_pRunTimeDlg;
	CModuleRunTimeDlg *pDlg;
	pDlg = new CModuleRunTimeDlg();

	pDlg->SetDB(&m_db);
	pDlg->DoModal();

	delete pDlg;
	pDlg = NULL;

}

//Contol Context Menu를 보여 준다.
LRESULT CCTSAnalView::OnRButtonClickedRowCol(WPARAM wParam, LPARAM lParam)
{
	ROWCOL nRow, nCol;
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;
	nCol = LOWORD(wParam);
	nRow = HIWORD(wParam);
	
	ASSERT(pGrid);
	if(nCol<2 || nRow < 1)	return 0;
	
	CPoint point;
	::GetCursorPos(&point);
	
	if (point.x == -1 && point.y == -1)
	{
		//keystroke invocation
		CRect rect;
		pGrid->GetClientRect(rect);
		pGrid->ClientToScreen(rect);

		point = rect.TopLeft();
		point.Offset(5, 5);
	}

	CMenu menu, *pPopup;

	if(pGrid == (CMyGridWnd *)&m_DataGrid)
	{
		VERIFY(menu.LoadMenu(IDR_DB_EDIT_CONTEXT));
		pPopup = menu.GetSubMenu(0);
		ASSERT(pPopup != NULL);
		
		CWnd* pWndPopupOwner = this;
		while (pWndPopupOwner->GetStyle() & WS_CHILD)
			pWndPopupOwner = pWndPopupOwner->GetParent();

		pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, pWndPopupOwner);
		return 1;
	}
	return 1;
}

void CCTSAnalView::OnDeleteTest() 
{
	// TODO: Add your command handler code here
	DeleteRecord();
}


void CCTSAnalView::OnCellNoEdit() 
{
	// TODO: Add your command handler code here
	ROWCOL nRow, nCol;
	CString strSerial, strSQL, strValData;

	if(m_DataGrid.GetCurrentCell(nRow, nCol) == FALSE)	return;

	if(nRow <1 || nCol <1)		return;

	CTestLogEditDlg *pDlg = new CTestLogEditDlg(this);
	pDlg->SetTitle(TEXT_LANG[64]);//"Cell No를 수정 합니다."
	pDlg->m_strValue = m_DataGrid.GetValueRowCol(nRow, LIST_COLUMN_CELL_NO);

	if(pDlg->DoModal() != IDOK)
	{
		delete pDlg;
		pDlg = NULL;
		return;
	}

	strValData = pDlg->m_strValue;
	delete pDlg;
	pDlg = NULL;

	strSerial = m_DataGrid.GetValueRowCol(nRow, LIST_COLUMN_TEST_LOG_ID);


	//조건 DB를 Open하여 [tray] Table도 Update 시킴 

	if(!strValData.IsEmpty())
	{

		strSQL.Format("UPDATE testlog SET cellno = %d WHERE TestLogID = %s", atol(strValData), strSerial);
		m_db.BeginTrans();
		m_db.ExecuteSQL( strSQL );
		m_db.CommitTrans();
		m_DataGrid.SetValueRange(CGXRange(nRow, LIST_COLUMN_CELL_NO), strValData);

		///////현재 진행중인 Tray 정보를 Update 시킴 ////////
		//2004/5/6

		CDatabase conditionDB;
		try
		{
			conditionDB.Open(ODBC_SCH_DB_NAME);
		}
		catch (CDBException* e)
		{
   			AfxMessageBox(e->m_strError);
   			e->Delete();     //DataBase Open Fail
			return;
		}
		strSQL.Format("UPDATE Tray SET cellno = %d WHERE TrayNo = '%s' AND TestSerialNo = '%s'", 
						atol(strValData), 
						m_DataGrid.GetValueRowCol(nRow, LIST_COLUMN_TRAY_NO), 
						m_DataGrid.GetValueRowCol(nRow, LIST_COLUMN_TEST_SERIAL_NO)
					 );
		conditionDB.BeginTrans();
		conditionDB.ExecuteSQL(strSQL);
		conditionDB.CommitTrans();
		conditionDB.Close();
	}

}

void CCTSAnalView::OnModelNameEdit() 
{
	// TODO: Add your command handler code here
	ROWCOL nRow, nCol;
	if(m_DataGrid.GetCurrentCell(nRow, nCol) == FALSE)	return;

	if(nRow <1 || nCol <1)		return;

	CTestLogEditDlg *pDlg = new CTestLogEditDlg(this);
	pDlg->SetTitle(TEXT_LANG[65]);//"모델명을 수정 합니다."
	pDlg->m_strValue = m_DataGrid.GetValueRowCol(nRow, LIST_COLUMN_MODEL_NAME);

	CString strSerial, strSQL, strValData;

	if(pDlg->DoModal() != IDOK)
	{
		delete pDlg;
		pDlg = NULL;
		return;
	}

	strValData = pDlg->m_strValue;
	delete pDlg;
	pDlg = NULL;


	strSerial = m_DataGrid.GetValueRowCol(nRow, LIST_COLUMN_TEST_LOG_ID);

	if(!strValData.IsEmpty())
	{
		strSQL.Format("UPDATE TestLog SET ModelName = '%s' WHERE TestLogID = %s", strValData, strSerial);
		m_db.BeginTrans();
		m_db.ExecuteSQL( strSQL );
		m_db.CommitTrans();
		m_DataGrid.SetValueRange(CGXRange(nRow, LIST_COLUMN_MODEL_NAME), strValData);
	
		//결과 Table[Testlog]에서만 모델명 변경함 (진행 Model Key나 시험 조건 Table[Model]은 변경하지 않음)
	
	}
}

void CCTSAnalView::OnLotNoEdit() 
{
	// TODO: Add your command handler code here
	ROWCOL nRow, nCol;
	if(m_DataGrid.GetCurrentCell(nRow, nCol) == FALSE)	return;

	if(nRow <1 || nCol <1)		return;

	CTestLogEditDlg *pDlg = new CTestLogEditDlg(this);
	pDlg->SetTitle(TEXT_LANG[66]);//"Batch 를 수정 합니다."
	pDlg->m_strValue = m_DataGrid.GetValueRowCol(nRow, LIST_COLUMN_LOT_NO);

	CString strSerial, strSQL, strValData;

	if(pDlg->DoModal() != IDOK)
	{
		delete pDlg;
		pDlg = NULL;
		return;
	}

	strValData = pDlg->m_strValue;
	delete pDlg;
	pDlg = NULL;


	strSerial = m_DataGrid.GetValueRowCol(nRow, LIST_COLUMN_TEST_LOG_ID);

	if(!strValData.IsEmpty())
	{
		strSQL.Format("UPDATE TestLog a, Module b SET a.LotNo = '%s', b.LotNo = '%s' WHERE a.TestLogID = %s AND a.TestSerialNo = b.TestSerialNo", strValData, strValData, strSerial);
		m_db.BeginTrans();
		m_db.ExecuteSQL( strSQL );
		m_db.CommitTrans();
		
		m_DataGrid.SetValueRange(CGXRange(nRow, LIST_COLUMN_LOT_NO), strValData);

	
		///////현재 진행중인 Tray 정보를 Update 시킴 ////////
		//2004/5/6
		CDatabase conditionDB;
		try
		{
			conditionDB.Open(ODBC_SCH_DB_NAME);
		}
		catch (CDBException* e)
		{
   			AfxMessageBox(e->m_strError);
   			e->Delete();     //DataBase Open Fail
			return;
		}

		strSQL.Format("UPDATE Tray Set LotNo = '%s' WHERE TrayNo = '%s' AND TestSerialNo = '%s'", 
						strValData, 
						m_DataGrid.GetValueRowCol(nRow, LIST_COLUMN_TRAY_NO), 
						m_DataGrid.GetValueRowCol(nRow, LIST_COLUMN_TEST_SERIAL_NO)
					 );
		conditionDB.BeginTrans();
		conditionDB.ExecuteSQL(strSQL);
		conditionDB.CommitTrans();
		conditionDB.Close();

	}
}

void CCTSAnalView::OnTrayNoEdit() 
{
	// TODO: Add your command handler code here
	ROWCOL nRow, nCol;
	if(m_DataGrid.GetCurrentCell(nRow, nCol) == FALSE)	return;

	if(nRow <1 || nCol <1)		return;

	CTestLogEditDlg *pDlg = new CTestLogEditDlg(this);
	pDlg->SetTitle(TEXT_LANG[67]);//"Tray ID 를 수정 합니다."
	pDlg->m_strValue = m_DataGrid.GetValueRowCol(nRow, LIST_COLUMN_TRAY_NO);

	CString strSerial, strSQL, strValData;

	if(pDlg->DoModal() != IDOK)
	{
		delete pDlg;
		pDlg = NULL;
		return;
	}

	strValData = pDlg->m_strValue;
	delete pDlg;
	pDlg = NULL;

	strSerial = m_DataGrid.GetValueRowCol(nRow, LIST_COLUMN_TEST_LOG_ID);

	if(!strValData.IsEmpty())
	{
		strSQL.Format("UPDATE TestLog a, Module b SET a.TrayNo = '%s', b.TrayNo = '%s' WHERE a.TestLogID = %s AND a.TestSerialNo = b.TestSerialNo", strValData, strValData, strSerial);
		m_db.BeginTrans();
		m_db.ExecuteSQL( strSQL );
		m_db.CommitTrans();
		m_DataGrid.SetValueRange(CGXRange(nRow, LIST_COLUMN_TRAY_NO), strValData);

		//Tray 등록 Table[Tray]에는 변경 시키지 않고 결과 Table[TestLog],[Module]에서만 변경 가능
	
	}
}

void CCTSAnalView::OnUpdateCellNoEdit(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(!m_strQuerySQL.IsEmpty());		
	
}

void CCTSAnalView::OnUpdateModelNameEdit(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(!m_strQuerySQL.IsEmpty());		
	
}

void CCTSAnalView::OnUpdateLotNoEdit(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(!m_strQuerySQL.IsEmpty());		
	
}

void CCTSAnalView::OnUpdateTrayNoEdit(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(!m_strQuerySQL.IsEmpty());		
}

void CCTSAnalView::ShowCellList(CString strTestID)
{
	if(strTestID.IsEmpty())		return;

	CWaitCursor wait;
	CString strTestLogID;

	if(m_strQuerySQL.IsEmpty())		//목록 Search
	{
		if(m_strFilterString.IsEmpty())
		{
			return ;
		}
		CSelectTrayDataDlg	*pSelDlg= new CSelectTrayDataDlg(this);
		pSelDlg->m_nFilterType =  m_nFilterType;
		pSelDlg->m_strFilterString = m_strFilterString;
		if(pSelDlg->DoModal() == IDOK)
		{
			strTestLogID = pSelDlg->m_strTestLogID;
		}
		delete pSelDlg;
	}
	else
	{
		strTestLogID = strTestID;//;
	}
	
	if(strTestLogID.IsEmpty())	return;

	CCellListDlg *pDlg = new CCellListDlg(this);
	
	for(int i =0; i<m_TotCol; i++)
	{
		pDlg->m_Field[i].bDisplay = m_field[i].bDisplay;
		pDlg->m_Field[i].DataType = m_field[i].DataType;
		pDlg->m_Field[i].FieldName = m_field[i].FieldName;
		pDlg->m_Field[i].ID = m_field[i].ID;
		pDlg->m_Field[i].No = m_field[i].No;
	}
	
	pDlg->m_strTestLogID = strTestLogID;
	pDlg->m_nFieldNo = m_TotCol;
	pDlg->m_pDBServer = &m_db;
	pDlg->m_pDoc = GetDocument();

	pDlg->DoModal();

	delete pDlg;
	pDlg = NULL;
}

void CCTSAnalView::OnDetailViewTest() 
{
	// TODO: Add your command handler code here
	ROWCOL nRow, nCol;
	if(m_DataGrid.GetCurrentCell(nRow, nCol) == FALSE)	return;

	if(nRow > 0)	
	{
		ShowCellList(m_DataGrid.GetValueRowCol(nRow, LIST_COLUMN_TEST_LOG_ID));
	}	
}

BOOL CCTSAnalView::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message == WM_KEYDOWN)
	{
		if((IDC_MODEL_NAME == ::GetDlgCtrlID(pMsg->hwnd) || IDC_LOT_FROM == ::GetDlgCtrlID(pMsg->hwnd) || IDC_TRAY_FROM == ::GetDlgCtrlID(pMsg->hwnd))
 			&& pMsg->wParam == VK_RETURN)
		{
			OnButtonSearch(); 
		}
	}
	
	return CFormView::PreTranslateMessage(pMsg);
}

void CCTSAnalView::OnDataSort() 
{
	// TODO: Add your command handler code here
	if( IDYES != MessageBox(TEXT_LANG[68],TEXT_LANG[69] , MB_ICONQUESTION|MB_YESNO))//"Tray 공정 투입 이력은 있으나 공정 시행 결과 Data가 없는 목록을 삭제 합니다.", "목록 삭제"
	{
		return;
	}

	CString strSQL, strSerial;
	CDBVariant val;
	CRecordset	rs(&m_db);
	
	CWaitCursor wait;
	try
	{
		strSQL = "SELECT TestSerialNo FROM TestLog ORDER BY TestSerialNo";
		rs.Open(CRecordset::forwardOnly, strSQL);
		
		m_db.BeginTrans();
		while(!rs.IsEOF())
		{
			rs.GetFieldValue((short)0, strSerial);
			strSQL.Format("DELETE FROM TestLog a WHERE a.TestSerialNo = '%s' AND (SELECT count(*) FROM Test b WHERE a.TestSerialNo = b.TestSerialNo) < 1",
							strSerial);
			
/*			strSQL.Format("SELECT count(*) FROM Test WHERE TestSerialNo = '%s'", strSerial);
			rs1.Open(CRecordset::forwardOnly, strSQL);
			while(!rs1.IsEOF())
			{
				rs1.GetFieldValue(short(0), val, SQL_C_SLONG);
				if(val.m_lVal < 1)
				{
					TRACE("%s\n", strSerial);
				}
				rs1.MoveNext();
			}
			rs1.Close();
*/	
			
			m_db.ExecuteSQL(strSQL);
			rs.MoveNext();
		}
		m_db.CommitTrans();
	}
	catch (CDBException *e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();
	}
}

