// Label.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
#include "Label.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLabel

CLabel::CLabel()
{
	m_clLeft = GetSysColor(COLOR_3DFACE);
	m_clRight = GetSysColor(COLOR_BTNFACE);

	m_crText = GetSysColor(COLOR_WINDOWTEXT);
//	m_hBrush = ::CreateSolidBrush(GetSysColor(COLOR_3DFACE));

	::GetObject((HFONT)GetStockObject(DEFAULT_GUI_FONT),sizeof(m_lf),&m_lf);

	m_font.CreateFontIndirect(&m_lf);
	m_bTimer	= FALSE;
	m_bState	= FALSE;
	m_bLink		= TRUE;
	m_hCursor	= NULL;
	m_Type		= None;

//	m_hwndBrush = ::CreateSolidBrush(GetSysColor(COLOR_3DFACE));

	hinst_msimg32 = LoadLibrary( "msimg32.dll" );
	m_bCanDoGradientFill = FALSE;

	if(hinst_msimg32)
	{
		m_bCanDoGradientFill = TRUE;		
		dllfunc_GradientFill = ((LPFNDLLFUNC1) GetProcAddress( hinst_msimg32, "GradientFill" ));
	}	

	m_bUserGradient = FALSE;
	m_nGradientType = G_CENTER;
	m_dwTextAlign = DT_CENTER;
}

CLabel::~CLabel()
{
	m_font.DeleteObject();
//	::DeleteObject(m_hBrush);
//	::DeleteObject(m_hwndBrush);

	if(hinst_msimg32)
	{
		FreeLibrary( hinst_msimg32 );
	}
}

CLabel& CLabel::SetText(const CString& strText)
{
	SetWindowText(strText);
	m_strText = strText;
	RedrawWindow();
	return *this;
}

CLabel& CLabel::SetTextColor(COLORREF crText)
{
	m_crText = crText;
	RedrawWindow();
	return *this;
}

CLabel& CLabel::SetFontBold(BOOL bBold)
{	
	m_lf.lfWeight = bBold ? FW_BOLD : FW_NORMAL;
	ReconstructFont();
	RedrawWindow();
	return *this;
}

CLabel& CLabel::SetFontUnderline(BOOL bSet)
{	
	m_lf.lfUnderline = (BYTE)bSet;
	ReconstructFont();
	RedrawWindow();
	return *this;
}

CLabel& CLabel::SetFontItalic(BOOL bSet)
{
	m_lf.lfItalic = (BYTE)bSet;
	ReconstructFont();
	RedrawWindow();
	return *this;	
}

CLabel& CLabel::SetGradientColor(COLORREF crGdColor)
{
	m_clRight = crGdColor;
	RedrawWindow();
	return *this;	
}	

CLabel& CLabel::SetGradient(BOOL bActive)
{
	m_bUserGradient = bActive;
	RedrawWindow();
	return *this;	
}	

CLabel& CLabel::SetGradientType(INT nType)
{
	m_nGradientType = nType;
	RedrawWindow();
	return *this;	
}

CLabel& CLabel::SetFontAlign(DWORD dwAlign)
{
	m_dwTextAlign = dwAlign;
	RedrawWindow();
	return *this;	
}



CLabel& CLabel::SetSunken(BOOL bSet)
{
	if (!bSet)
		ModifyStyleEx(WS_EX_STATICEDGE,0,SWP_DRAWFRAME);
	else
		ModifyStyleEx(0,WS_EX_STATICEDGE,SWP_DRAWFRAME);
		
	return *this;	
}

CLabel& CLabel::SetBorder(BOOL bSet)
{
	if (!bSet)
		ModifyStyle(WS_BORDER,0,SWP_DRAWFRAME);
	else
		ModifyStyle(0,WS_BORDER,SWP_DRAWFRAME);
		
	return *this;	
}

CLabel& CLabel::SetFontSize(int nSize)
{
	nSize*=-1;
	m_lf.lfHeight = nSize;
	ReconstructFont();
	RedrawWindow();
	return *this;
}

CLabel& CLabel::SetBkColor(COLORREF crBkgnd)
{
	m_clLeft = crBkgnd;	
	RedrawWindow();
	return *this;
}

CLabel& CLabel::SetFontName(const CString& strFont)
{	
	strcpy(m_lf.lfFaceName,strFont);
	ReconstructFont();
	RedrawWindow();
	return *this;
}

BEGIN_MESSAGE_MAP(CLabel, CStatic)
	//{{AFX_MSG_MAP(CLabel)
//	ON_WM_CTLCOLOR_REFLECT()
	ON_WM_TIMER()
	ON_WM_LBUTTONDOWN()
	ON_WM_SETCURSOR()
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLabel message handlers
/*
HBRUSH CLabel::CtlColor(CDC* pDC, UINT nCtlColor) 
{
	// TODO: Change any attributes of the DC here
	// TODO: Return a non-NULL brush if the parent's handler should not be called

	if (CTLCOLOR_STATIC == nCtlColor)
	{
		pDC->SelectObject(&m_font);
		pDC->SetTextColor(m_crText);
		pDC->SetBkMode(TRANSPARENT);
	}

	if (m_Type == Background)
	{
		if (!m_bState)
			return m_hwndBrush;
	}

	return m_hBrush;
}
*/
void CLabel::ReconstructFont()
{
	m_font.DeleteObject();
	BOOL bCreated = m_font.CreateFontIndirect(&m_lf);

	ASSERT(bCreated);
}

void CLabel::ResetFont(const CString& strFont, int nSize, BOOL bBold) 
{
	m_lf.lfHeight = nSize * -1;
	m_lf.lfWeight = bBold ? FW_BOLD : FW_NORMAL;
	strcpy(m_lf.lfFaceName,strFont);

	m_font.DeleteObject();
	BOOL bCreated = m_font.CreateFontIndirect(&m_lf);

	ASSERT(bCreated);
	RedrawWindow();
}

void CLabel::ResetColor(COLORREF crText, COLORREF crBkgnd)
{
	m_crText = crText;
	m_clLeft = crBkgnd;
	RedrawWindow();
}

CLabel& CLabel::FlashText(BOOL bActivate)
{
	m_bState = FALSE;
	if (m_bTimer)
	{
		KillTimer(1);
		RedrawWindow();
	}

	if (bActivate)
	{
		m_bTimer = TRUE;
		SetTimer(1,500,NULL);
		m_Type = Text;
	}

	return *this;
}

CLabel& CLabel::FlashBackground(BOOL bActivate)
{
	m_bState = FALSE;

	if (m_bTimer)
	{
		KillTimer(1);
		RedrawWindow();
	}

	if (bActivate)
	{
		m_bTimer = TRUE;
		SetTimer(1,500,NULL);
		m_Type = Background;
	}

	return *this;
}

void CLabel::OnTimer(UINT nIDEvent) 
{
	m_bState = !m_bState;
	RedrawWindow();

	CStatic::OnTimer(nIDEvent);
}

CLabel& CLabel::SetLink(BOOL bLink)
{
	m_bLink = bLink;

	if (bLink)
		ModifyStyle(0,SS_NOTIFY);
	else
		ModifyStyle(SS_NOTIFY,0);

	return *this;
}

void CLabel::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CString strLink;

	GetWindowText(strLink);
	ShellExecute(NULL,"open",strLink,NULL,NULL,SW_SHOWNORMAL);
		
	CStatic::OnLButtonDown(nFlags, point);
}

BOOL CLabel::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	if (m_hCursor)
	{
		::SetCursor(m_hCursor);
		return TRUE;
	}

	return CStatic::OnSetCursor(pWnd, nHitTest, message);
}

CLabel& CLabel::SetLinkCursor(HCURSOR hCursor)
{
	m_hCursor = hCursor;
	return *this;
}

void CLabel::SetUserFont(LOGFONT *lf)
{
	if(lf)
	{
		memcpy(&m_lf, lf, sizeof(LOGFONT)); 
		ReconstructFont();
		RedrawWindow();
	}
}

void CLabel::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	CRect rect;
	GetClientRect(&rect);
	COLORREF color;

	CString strTitle;
	GetWindowText(strTitle);

	if(m_bCanDoGradientFill && m_bUserGradient)
	{	
		if(m_Type == Background && m_bState)
		{
			dc.FillSolidRect(&rect, GetSysColor(COLOR_3DFACE));
		}
		else
		{
			CRect dRect(rect);
			int iTemp = dRect.left;
			TRIVERTEX rcVertex[2];
			GRADIENT_RECT grect;
			//dRect.right--; // exclude this point, like FillRect does 
			//dRect.bottom--;
			
			if( m_nGradientType == G_CENTER)
			{
				dRect.left += dRect.Width()/2;
				rcVertex[0].x=dRect.left;
				rcVertex[0].y=dRect.top;
				rcVertex[0].Red=GetRValue(m_clRight)<<8;
				rcVertex[0].Green=GetGValue(m_clRight)<<8;
				rcVertex[0].Blue=GetBValue(m_clRight)<<8;
				rcVertex[0].Alpha=0x0000;
				rcVertex[1].x=dRect.right; 
				rcVertex[1].y=dRect.bottom;
				rcVertex[1].Red=GetRValue(m_clLeft)<<8;	// color values from 0x0000 to 0xff00 !!!!
				rcVertex[1].Green=GetGValue(m_clLeft)<<8;
				rcVertex[1].Blue=GetBValue(m_clLeft)<<8;
				rcVertex[1].Alpha=0;

				// fill the area 
				grect.UpperLeft=0;
				grect.LowerRight=1;
				dllfunc_GradientFill( dc,rcVertex,2,&grect,1,GRADIENT_FILL_RECT_H);
			
				dRect.left = iTemp;
				dRect.right -= rect.Width()/2;
			}
			
			rcVertex[0].x=dRect.left;
			rcVertex[0].y=dRect.top;
			rcVertex[0].Red=GetRValue(m_clLeft)<<8;	// color values from 0x0000 to 0xff00 !!!!
			rcVertex[0].Green=GetGValue(m_clLeft)<<8;
			rcVertex[0].Blue=GetBValue(m_clLeft)<<8;
			rcVertex[0].Alpha=0x0000;
			rcVertex[1].x=dRect.right; 
			rcVertex[1].y=dRect.bottom;
			rcVertex[1].Red=GetRValue(m_clRight)<<8;
			rcVertex[1].Green=GetGValue(m_clRight)<<8;
			rcVertex[1].Blue=GetBValue(m_clRight)<<8;
			rcVertex[1].Alpha=0;
		
			// fill the area 
			grect.UpperLeft=0;
			grect.LowerRight=1;
			dllfunc_GradientFill( dc,rcVertex,2, &grect,1,GRADIENT_FILL_RECT_H);
		}
		//Draw text
		::SetBkMode(dc, TRANSPARENT);
		::SetTextColor(dc, m_crText);
		if(m_Type == Text && m_bState)
		{
			strTitle.Empty();
		}

		HFONT hfontOld;
		hfontOld = (HFONT)SelectObject(dc.m_hDC, (HFONT)m_font.m_hObject);
		::DrawText(dc, strTitle, -1, &rect, DT_SINGLELINE|DT_VCENTER|m_dwTextAlign);
		if(hfontOld)
		{
			::SelectObject(dc.m_hDC, hfontOld);
		}	

	}
	else
	{
		//there is no gradient, so let's use standart color
		color = m_clLeft;
		if(m_Type == Background && m_bState)
		{
			color = GetSysColor(COLOR_3DFACE);
		}
		dc.FillSolidRect(&rect, color);
		
		//Draw text
		::SetBkMode(dc, TRANSPARENT);
		::SetTextColor(dc, m_crText);
		if(m_Type == Text && m_bState)
		{
			strTitle.Empty();
		}

		HFONT hfontOld;
		hfontOld = (HFONT)SelectObject(dc.m_hDC, (HFONT)m_font.m_hObject);
		::DrawText(dc, strTitle, -1, &rect, DT_SINGLELINE|DT_VCENTER|m_dwTextAlign);
		if(hfontOld)
		{
			::SelectObject(dc.m_hDC, hfontOld);
		}	
	}

}
