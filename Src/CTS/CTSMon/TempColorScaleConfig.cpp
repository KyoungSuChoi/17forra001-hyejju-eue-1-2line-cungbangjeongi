// TempColorScaleConfig.cpp : implementation file
//

#include "stdafx.h"
#include "ctsmon.h"
#include "TempColorScaleConfig.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTempColorScaleConfig dialog


CTempColorScaleConfig::CTempColorScaleConfig(CCTSMonDoc *pDoc, CWnd* pParent /*=NULL*/)
	: CDialog(CTempColorScaleConfig::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTempColorScaleConfig)
	m_fJigMax = 0.0f;
	m_fJigMin = 0.0f;
	m_fUnitMax = 0.0f;
	m_fUnitMin = 0.0f;
	//}}AFX_DATA_INIT
	m_pDoc = pDoc;
	LanguageinitMonConfig();
}


CTempColorScaleConfig::~CTempColorScaleConfig()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CTempColorScaleConfig::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CTempColorScaleConfig"), _T("TEXT_CTempColorScaleConfig_CNT"), _T("TEXT_CTempColorScaleConfig_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CTempColorScaleConfig_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CTempColorScaleConfig"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}


void CTempColorScaleConfig::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTempColorScaleConfig)
	DDX_Text(pDX, IDC_EIDT_JIGMAX, m_fJigMax);
	DDX_Text(pDX, IDC_EIDT_JIGMIN, m_fJigMin);
	DDX_Text(pDX, IDC_EIDT_UNITMAX, m_fUnitMax);
	DDX_Text(pDX, IDC_EIDT_UNITMIN, m_fUnitMin);
	//}}AFX_DATA_MAP	
}


BEGIN_MESSAGE_MAP(CTempColorScaleConfig, CDialog)
	//{{AFX_MSG_MAP(CTempColorScaleConfig)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTempColorScaleConfig message handlers

void CTempColorScaleConfig::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData( TRUE );
	float fGradeVal=0.0f;

	if( m_fJigMin > m_fJigMax || m_fJigMin < 0)
	{
		AfxMessageBox(TEXT_LANG[0]);//"Max 값이 Min 값보다 작거나 Min 값이 0보다 작습니다!"
		return;
	}

	if( m_fUnitMin > m_fUnitMax || m_fUnitMin < 0 )
	{
		AfxMessageBox(TEXT_LANG[0]);//"Max 값이 Min 값보다 작거나 Min 값이 0보다 작습니다!"
		return;
	}

	m_TempScaleConfig.m_fJiglevel1	= m_fJigMin;
	m_TempScaleConfig.m_fJiglevel8	= m_fJigMax;
	m_TempScaleConfig.m_fUnitlevel1	= m_fUnitMin;
	m_TempScaleConfig.m_fUnitlevel8	= m_fUnitMax;	

	fGradeVal = m_fJigMax - m_fJigMin;
	m_TempScaleConfig.m_fJiglevel2 = m_TempScaleConfig.m_fJiglevel1 + (fGradeVal/7);
	m_TempScaleConfig.m_fJiglevel3 = m_TempScaleConfig.m_fJiglevel1 + (fGradeVal/7)*2;
	m_TempScaleConfig.m_fJiglevel4 = m_TempScaleConfig.m_fJiglevel1 + (fGradeVal/7)*3;
	m_TempScaleConfig.m_fJiglevel5 = m_TempScaleConfig.m_fJiglevel1 + (fGradeVal/7)*4;
	m_TempScaleConfig.m_fJiglevel6 = m_TempScaleConfig.m_fJiglevel1 + (fGradeVal/7)*5;
	m_TempScaleConfig.m_fJiglevel7 = m_TempScaleConfig.m_fJiglevel1 + (fGradeVal/7)*6;
	
	fGradeVal = m_fUnitMax - m_fUnitMin;
	m_TempScaleConfig.m_fUnitlevel2 = m_TempScaleConfig.m_fUnitlevel1 + (fGradeVal/7);
	m_TempScaleConfig.m_fUnitlevel3 = m_TempScaleConfig.m_fUnitlevel1 + (fGradeVal/7)*2;
	m_TempScaleConfig.m_fUnitlevel4 = m_TempScaleConfig.m_fUnitlevel1 + (fGradeVal/7)*3;
	m_TempScaleConfig.m_fUnitlevel5 = m_TempScaleConfig.m_fUnitlevel1 + (fGradeVal/7)*4;
	m_TempScaleConfig.m_fUnitlevel6 = m_TempScaleConfig.m_fUnitlevel1 + (fGradeVal/7)*5;
	m_TempScaleConfig.m_fUnitlevel7 = m_TempScaleConfig.m_fUnitlevel1 + (fGradeVal/7)*6;

	m_pDoc->m_TempScaleConfig = m_TempScaleConfig;
	::WriteTempScaleConfig( m_TempScaleConfig );	
	CDialog::OnOK();
}

BOOL CTempColorScaleConfig::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_fJigMax = m_TempScaleConfig.m_fJiglevel8;
	m_fJigMin = m_TempScaleConfig.m_fJiglevel1;
	m_fUnitMax = m_TempScaleConfig.m_fUnitlevel8;
	m_fUnitMin = m_TempScaleConfig.m_fUnitlevel1;
	UpdateData(FALSE);	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
