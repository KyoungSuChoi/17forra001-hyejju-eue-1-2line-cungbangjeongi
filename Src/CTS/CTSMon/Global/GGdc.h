
/*#include <atlconv.h>

#define COLOR_YELLOW		RGB( 255, 255,   0 )
#define COLOR_RED			RGB( 255,   0,   0 )
#define COLOR_BLUE			RGB(   0,   0, 255 )
#define COLOR_BLACK			RGB(   0,   0,   0 )
#define COLOR_WHITE			RGB( 255, 255, 255 )
#define COLOR_DLG			RGB( 170, 170, 170 )
#define COLOR_DARKGRAY  	RGB( 100, 100, 100 )
#define COLOR_GRAY			RGB( 200, 200, 200 )
#define COLOR_LIGHTGRAY 	RGB( 235, 228, 228 )
#define COLOR_LIGHTGRAY2	RGB( 247, 247, 247 )
#define COLOR_PINK			RGB( 255,   0, 255 )

#ifndef GetRValue
	#define GetRValue(rgb)      ((BYTE)(rgb))                   // 적색
#endif
#ifndef GetGValue
	#define GetGValue(rgb)      ((BYTE)(((WORD)(rgb)) >> 8))    // 녹색
#endif
#ifndef GetBValue
	#define GetBValue(rgb)      ((BYTE)((rgb)>>16))             // 청색
#endif
#define REVRGB(A)				RGB(255-GetRValue(A),255-GetGValue(A),255-GetBValue(A))

//  these variables should have been defined in some standard header but is not
#ifndef WS_EX_LAYERED
	#define WS_EX_LAYERED 0x00080000 
#endif
// Use color as the transparency color.
#ifndef LWA_COLORKEY
	#define LWA_COLORKEY 0x00000001 
#endif
// Use bAlpha to determine the opacity of the layer
#ifndef LWA_ALPHA
	#define LWA_ALPHA    0x00000002 
#endif

///////////////////////////////////////////////////////////////////////////
#ifndef SAFE_RELEASE
	#define SAFE_RELEASE(x) { if (x) x->Release(); x = NULL; }
#endif
//////////////////////////////////////////////////////////////////////////
extern "C" {WINUSERAPI BOOL WINAPI SetLayeredWindowAttributes(HWND hwnd,COLORREF crKey,BYTE bAlpha,DWORD dwFlags);}
*/

class GGdc 
{
public:   
	GGdc();
    ~GGdc();
    		

	/*static CDC* gdc_HdcToCdc(HDC hdc)
	{
	    return CDC::FromHandle(hdc);
	}

	static void gdc_GetCBitmap(CBitmap &bitmap,HBITMAP hbitmap)
	{
		bitmap.Attach(hbitmap);
		//bitmap.Detach();
	}

	static SIZE gdc_TextExtent(HDC hDC,CString str) 
	{
		SIZE size;
		GetTextExtentPoint32(hDC,str, str.GetLength(), &size);   
		return size;
	}

	static void gdc_DrawText(HDC hDC,DWORD mode,int bcolor,int tcolor,CString str,CRect rect) 
	{
		::SetBkMode(hDC,mode); //OPAQUE,TRANSPARENT
		::SetBkColor(hDC,bcolor);//background
		::SetTextColor(hDC,tcolor);//text color
		::DrawText(hDC,str,str.GetLength(),rect,DT_VCENTER|DT_SINGLELINE);//검색어
	}

	static HANDLE gdc_DDBToDIB(CBitmap &bitmap, DWORD dwCompression, CPalette *pPal)
	{//CPalette pal;
	 //gdc_DDBToDIB(bitmap,BI_RGB, &pal);
		// Return Value : A HANDLE to the attached Windows GDI object
		ASSERT(bitmap.GetSafeHandle());

		// The function has no arg for bitfields
		if( dwCompression == BI_BITFIELDS ) return NULL;

		// If a palette has not been supplied use defaul palette
		HPALETTE hPal = (HPALETTE) pPal->GetSafeHandle();
		if (hPal==NULL)	hPal = (HPALETTE) GetStockObject(DEFAULT_PALETTE);

		// Get bitmap information
		BITMAP bm;
		bitmap.GetObject(sizeof(bm),(LPSTR)&bm);

		// Initialize the bitmapinfoheader ( Bitmap Information )
		BITMAPINFOHEADER bi;
		bi.biSize        = sizeof(BITMAPINFOHEADER);             // 비트맵 헤더크기
		bi.biWidth       = bm.bmWidth;                           // 비트맵의 가로 크기
		bi.biHeight      = bm.bmHeight;                          // 비트맵의 세로 크기
		bi.biPlanes      = 1;                                    // Plane 수 (1로 설정)
		bi.biBitCount    = bm.bmPlanes * bm.bmBitsPixel;         // 한 픽셀당 비트수
		bi.biCompression = dwCompression;                        // 압축 유무
		bi.biSizeImage   = 0;                                    // 그림 데이터 크기
		bi.biXPelsPerMeter = 0;                                  // 한 픽셀당 가로 미터
		bi.biYPelsPerMeter = 0;                                  // 한 픽셀당 세로 미터
		bi.biClrUsed       = 0;                                  // 그림에서 실제 사용되는 컬러수
		bi.biClrImportant  = 0;                                  // 중요하게 사용되는 컬러

		// Compute the size of the  infoheader and the color table
		int nColors = (1 << bi.biBitCount);
		if( nColors > 256 ) nColors = 0;
		DWORD dwLen  = bi.biSize + nColors * sizeof(RGBQUAD);

		// We need a device context to get the DIB from
		HDC hDC = ::GetDC(NULL);
		hPal = SelectPalette(hDC,hPal,FALSE);
		RealizePalette(hDC);

		// Allocate enough memory to hold bitmapinfoheader and color table
		HANDLE hDIB = GlobalAlloc(GMEM_FIXED,dwLen);

		if (!hDIB){
			SelectPalette(hDC,hPal,FALSE);
			::ReleaseDC(NULL,hDC);
			return NULL;
		}

		LPBITMAPINFOHEADER lpbi = (LPBITMAPINFOHEADER)hDIB;
		*lpbi = bi;

		// Call GetDIBits with a NULL lpBits param, so the device driver 
		// will calculate the biSizeImage field 
		GetDIBits(hDC,(HBITMAP)bitmap.GetSafeHandle(), 0L, (DWORD)bi.biHeight,
				  (LPBYTE)NULL, (LPBITMAPINFO)lpbi, (DWORD)DIB_RGB_COLORS);
		bi = *lpbi;

		// If the driver did not fill in the biSizeImage field, then compute it
		// Each scan line of the image is aligned on a DWORD (32bit) boundary
		if (bi.biSizeImage == 0)
		{
			 bi.biSizeImage = ((((bi.biWidth * bi.biBitCount) + 31) & ~31) / 8) 
			 *bi.biHeight;

			// If a compression scheme is used the result may infact be larger
			// Increase the size to account for this.
			if (dwCompression != BI_RGB)  bi.biSizeImage = (bi.biSizeImage * 3) / 2;
		}

		// Realloc the buffer so that it can hold all the bits
		dwLen += bi.biSizeImage;
		HANDLE handle;
		if (handle = GlobalReAlloc(hDIB, dwLen, GMEM_MOVEABLE)) hDIB = handle;
		else
		{			
			GlobalFree(hDIB);
			// Reselect the original palette
			SelectPalette(hDC,hPal,FALSE);
			::ReleaseDC(NULL,hDC);
			return NULL;
		}
		// Get the bitmap bits
		lpbi = (LPBITMAPINFOHEADER)hDIB;

		// FINALLY get the DIB
		BOOL bGotBits = GetDIBits(hDC, (HBITMAP)bitmap.GetSafeHandle(),
								  0L,    // Start scan line
								  (DWORD)bi.biHeight,  // # of scan lines
								  (LPBYTE)lpbi    // address for bitmap bits
								  +(bi.biSize + nColors * sizeof(RGBQUAD)),
								  (LPBITMAPINFO)lpbi,  // address of bitmapinfo
								  (DWORD)DIB_RGB_COLORS);  // Use RGB for color table
		if( !bGotBits )
		{			
			 GlobalFree(hDIB);
			 SelectPalette(hDC,hPal,FALSE);
			 ::ReleaseDC(NULL,hDC);
			 CloseHandle(handle);
			 return NULL;
		}
		SelectPalette(hDC,hPal,FALSE);
		::ReleaseDC(NULL,hDC);
		CloseHandle(handle);
		return hDIB;
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	// DIB로 바뀐 비트맵을 파일에 저장
	static BOOL gdc_WriteDIB(LPTSTR szFileName, HANDLE hDIB,BOOL bClose=TRUE)
	{		
		if (!hDIB) return FALSE;

		CFile file;
		if (!file.Open(szFileName, CFile::modeWrite | CFile::modeCreate)) return FALSE;

		LPBITMAPINFOHEADER lpbi = (LPBITMAPINFOHEADER)hDIB;

		// 한 픽셀당 비트수를 왼쪽으로 1 이동 (shift)
		int nColors = 1 << lpbi->biBitCount;

		// Fill in the fields of the file header ( Bitmap file header )
		BITMAPFILEHEADER      hdr;   
		hdr.bfType        = ((WORD) ('M' << 8) | 'B');         // is always "BM"   
		hdr.bfSize        = GlobalSize (hDIB) + sizeof(hdr);   // 비트맵 파일의 전체 크기
		hdr.bfReserved1   = 0;                                 // 예약변수 (0으로 설정)
		hdr.bfReserved2   = 0;                                 // 예약변수 (0으로 설정)
		// 파일에서 비트맵 데이터가 있는 위치
		hdr.bfOffBits     = (DWORD)(sizeof(hdr) + lpbi->biSize + nColors * sizeof(RGBQUAD));  

		// Write the file header 
		file.Write(&hdr, sizeof(hdr));
    
		// Write the DIB header and the bits 
		file.Write(lpbi, GlobalSize(hDIB));
		
		file.Close();
		if(bClose==TRUE) CloseHandle(hDIB);
		return TRUE;
	}


	static CFont* gdc_CFont(LOGFONT font)
	{
		CFont *cfont=new CFont;
		cfont->CreateFontIndirect(&font);
		
		return cfont;
	}

	static CString gdc_StrFont(LOGFONT font)
	{
		CString sfont;
		CString fontname=font.lfFaceName;
		  
		sfont.Format(_T("%d/%d/%d/%d/%d/%d/%d/%d/%d/%d/%d/%d/%d/%s"),
								font.lfHeight,font.lfWidth,font.lfEscapement,
								font.lfOrientation,font.lfWeight,font.lfItalic,
								font.lfUnderline,font.lfStrikeOut,font.lfCharSet,
								font.lfOutPrecision,font.lfClipPrecision,font.lfQuality,
								font.lfPitchAndFamily,fontname);    	 
		return sfont;
	}

	static LOGFONT gdc_LogFont(CString Value)
	{
		LOGFONT vFont;
		CString temp;
		memset(&vFont, 0, sizeof(LOGFONT)); 

		if(Value.IsEmpty() || Value==_T(""))
		{//50/0/0/0/900/0/0/0/0/0/0/0/2/Arial	  
		   vFont.lfHeight = 50;
		   vFont.lfWidth = 0;
		   vFont.lfEscapement = 0;	
		   vFont.lfOrientation = 0;
		   vFont.lfWeight = FW_HEAVY;
		   vFont.lfItalic = FALSE;
		   vFont.lfUnderline = FALSE;
		   vFont.lfStrikeOut = FALSE;
		   vFont.lfCharSet = ANSI_CHARSET;
		   vFont.lfOutPrecision = OUT_DEFAULT_PRECIS;
		   vFont.lfClipPrecision = CLIP_DEFAULT_PRECIS;
		   vFont.lfQuality = DEFAULT_QUALITY;
		   vFont.lfPitchAndFamily = VARIABLE_PITCH;	

		   #ifdef _UNICODE	
		        _tcscpy_s(vFont.lfFaceName, _countof(vFont.lfFaceName), _T("Arial"));
		   #else
				strcpy(vFont.lfFaceName, _T("Arial"));
		   #endif			   
		} 
		else 
		{		  
			AfxExtractSubString(temp,Value,0,'/');
			vFont.lfHeight = _ttoi(temp);
			AfxExtractSubString(temp,Value,1,'/');
			vFont.lfWidth = _ttoi(temp);
			AfxExtractSubString(temp,Value,2,'/');
			vFont.lfEscapement = _ttoi(temp);
			AfxExtractSubString(temp,Value,3,'/');
			vFont.lfOrientation = _ttoi(temp);
			AfxExtractSubString(temp,Value,4,'/');
			vFont.lfWeight = _ttoi(temp);
			AfxExtractSubString(temp,Value,5,'/');
			vFont.lfItalic = _ttoi(temp);
			AfxExtractSubString(temp,Value,6,'/');
			vFont.lfUnderline = _ttoi(temp);
			AfxExtractSubString(temp,Value,7,'/');
			vFont.lfStrikeOut = _ttoi(temp);
			AfxExtractSubString(temp,Value,8,'/');
			vFont.lfCharSet = _ttoi(temp);
			AfxExtractSubString(temp,Value,9,'/');
			vFont.lfOutPrecision = _ttoi(temp);
			AfxExtractSubString(temp,Value,10,'/');
			vFont.lfClipPrecision = _ttoi(temp);
			AfxExtractSubString(temp,Value,11,'/');
			vFont.lfQuality = _ttoi(temp);
			AfxExtractSubString(temp,Value,12,'/');
			vFont.lfPitchAndFamily = _ttoi(temp);	
			AfxExtractSubString(temp,Value,13,'/');
			
			#ifdef _UNICODE
				_tcscpy_s(vFont.lfFaceName, _countof(vFont.lfFaceName), temp);
			#else
			    strcpy(vFont.lfFaceName, temp);
			#endif	
		}	
		return vFont;	
	}

	static void gdc_Transparency(HWND hWnd,int value)
	{//max 255
		SetWindowLong(hWnd,GWL_EXSTYLE,GetWindowLong(hWnd,GWL_EXSTYLE)^WS_EX_LAYERED);
		SetLayeredWindowAttributes(hWnd,RGB(0,0,0),value,LWA_ALPHA);
	}
	
	static HBITMAP gdc_GetBitMap(CString file,LPPICTURE &pPic)
	{//2Mbyte이상 가능
		if(file.IsEmpty() || file==_T("")) return NULL;

		HBITMAP hBitmap; 		
		USES_CONVERSION;
		HRESULT hr = ::OleLoadPicturePath(const_cast<LPOLESTR>(T2COLE(file)),
								 NULL,0,0,IID_IPicture,reinterpret_cast<LPVOID *>(&pPic));
		if(S_OK!=hr || !pPic) return NULL;		
		if(S_OK!=pPic->get_Handle((UINT*)&hBitmap)) return NULL;
				
		HBITMAP hPicRet = (HBITMAP)CopyImage(hBitmap, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION); //LR_COPYRETURNORG);				
		DeleteObject(hBitmap);
		
		return hPicRet;
	}

	//메뉴에 아이콘 넣기 SetMenuBmp(m_Menu,AfxGetInstanceHandle(),0,IDB_BMP_MONITOR,IDB_BMP_MONITOR);
	static void gdc_SetMenuBmp(CMenu &menu,HINSTANCE gint,int pos,UINT BMP1,UINT BMP2) 
	{
		HBITMAP bmp1=NULL;
		HBITMAP bmp2=NULL;

		if(BMP1>0) bmp1=LoadBitmap(gint,MAKEINTRESOURCE(BMP1));  
		if(BMP2>0) bmp2=LoadBitmap(gint,MAKEINTRESOURCE(BMP2));

		::SetMenuItemBitmaps(menu,pos,MF_BYCOMMAND | MF_BYPOSITION, bmp1, bmp2);
	}

	static int gdc_GetColor(int color)
	{
		COLORREF nColorValue=color;
   		CColorDialog dlg(nColorValue);

		if(dlg.DoModal()==IDOK){
			return dlg.GetColor();	 	  	  
		}
		return color;
	}

	static CFont* gdc_CreateFont(char *pFontName, int nHeight, int nWeight=FW_NORMAL )
	{
		CFont *pFont=new CFont();

		#ifdef _UNICODE	
		    pFont->CreateFontW(nHeight, 0, 0, 0, nWeight, FALSE, FALSE, FALSE, 
								 DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, 
								 CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, 
								 FF_DONTCARE, (LPCTSTR)pFontName);
		#else
		    pFont->CreateFont(nHeight, 0, 0, 0, nWeight, FALSE, FALSE, FALSE, 
								 DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, 
								 CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, 
								 FF_DONTCARE, pFontName);	
		#endif	

		
		return pFont;
		
	}

	static HBITMAP gdc_BitmapFromIcon(HICON hIcon)
	{
	   HDC hDC = CreateCompatibleDC(NULL);
	   HBITMAP hBitmap = CreateCompatibleBitmap(hDC, GetSystemMetrics(SM_CXICON), GetSystemMetrics(SM_CYICON));
	   HBITMAP hOldBitmap = (HBITMAP)SelectObject(hDC, hBitmap);
	   DrawIcon(hDC, 0, 0, hIcon);
	   SelectObject(hDC, hOldBitmap);
	   DeleteDC(hDC);

	   return hBitmap;
	}

	static DWORD gdc_imgPicture(CString file,HWND hWnd,HDC hdc=NULL,CRect rect=CRect(0,0,0,0))
	{//2Mbyte이상은 로딩못함.     
		DWORD dwStart = GetTickCount();
				
		if(hWnd!=NULL && hdc==NULL) hdc=::GetDC(hWnd);			
		if(hWnd!=NULL && rect==CRect(0,0,0,0)) ::GetClientRect(hWnd,&rect);	
		
		LPPICTURE pPic;
		USES_CONVERSION;
		    	
		HRESULT hr= ::OleLoadPicturePath(const_cast<LPOLESTR>(T2COLE(file)),
					NULL,0,0,IID_IPicture,reinterpret_cast<LPVOID *>(&pPic));    
		if (pPic)
		{
		   OLE_XSIZE_HIMETRIC w;
		   OLE_YSIZE_HIMETRIC h;
		   pPic->get_Width(&w);
		   pPic->get_Height(&h);    	    
		   pPic->Render(hdc, //dc.GetSafeHdc(), // Render 로 화면 출력
						rect.left, // 출력할 위치 x
						rect.top, // 출력할 위치 y
						rect.Width(), // 출력할 크기 cx
						rect.Height(), // 출력할 크기 cy	   
						0,  h - 1, w, -h, NULL);    	   
		}

		SAFE_RELEASE(pPic);
		
		if(hWnd!=NULL)
		{
		  ::ReleaseDC(hWnd,hdc);
		  hdc=NULL;
		}

		return GetTickCount()-dwStart;		
	}

	static BOOL gdc_DrawBitmap(HBITMAP hBitmap,HWND hWnd,HDC hdc=NULL,CRect rect=CRect(0,0,0,0),BOOL bflag=TRUE,CString strText=_T(""))
	{			
		if(!hBitmap) return FALSE;

		BOOL bFree=FALSE;
		if(hWnd!=NULL && hdc==NULL){ hdc=::GetDC(hWnd);bFree=TRUE; }		
		if(hWnd!=NULL && rect==CRect(0,0,0,0)) ::GetClientRect(hWnd,&rect);	
		
		CDC *pDC=GGdc::gdc_HdcToCdc(hdc);
		if(!pDC) return FALSE;
		
		CBitmap  bmp;   
		CDC      BmpDC;
		bmp.Attach(hBitmap);
		BmpDC.CreateCompatibleDC(pDC);
		CBitmap *pOldBmp = (CBitmap *)BmpDC.SelectObject(&bmp);

		BITMAP bit;
		bmp.GetObject(sizeof(BITMAP),&bit);
		int bx=bit.bmWidth;
		int by=bit.bmHeight;	
		
		SetStretchBltMode(hdc,  COLORONCOLOR);
		pDC->StretchBlt(rect.left, rect.top, rect.Width(), rect.Height(), &BmpDC, 0, 0, bx, by, SRCCOPY);

		if(strText!=_T(""))
		{
			//pDC->SetBkMode(TRANSPARENT);
			//pDC->SetTextColor(RGB(255,0,0));
			pDC->DrawText(strText,strText.GetLength(),rect,DT_CENTER);
		}

		BmpDC.SelectObject(pOldBmp);
		BmpDC.DeleteDC();

		bmp.Detach();
		bmp.DeleteObject();

		pOldBmp->DeleteObject();

		if(bflag==TRUE) DeleteObject(hBitmap);
		if(bFree==TRUE)
		{
			::DeleteDC(hdc); // ::ReleaseDC(hWnd,hdc);
			pDC->DeleteDC();
		}
		
		return TRUE;
	}

	static void gdc_DrawLppicture(LPPICTURE &pPic,HDC hdc,CRect rect)
	{				
		if(!pPic) return;

		OLE_XSIZE_HIMETRIC w;
		OLE_YSIZE_HIMETRIC h;
		pPic->get_Width(&w);
		pPic->get_Height(&h);    	    
		pPic->Render(hdc,//dc.GetSafeHdc(), // Render 로 화면 출력
					 rect.left, // 출력할 위치 x
					 rect.top, // 출력할 위치 y
					 rect.Width(), // 출력할 크기 cx
					 rect.Height(), // 출력할 크기 cy	   
					 0,  h - 1, w, -h, NULL); 
	}

	static BOOL gdc_Capture(HWND hWnd,CBitmap &bitmap,int nPercent=100)
	{		
		CDC* windc = CDC::FromHandle(::GetDC(hWnd));
		if(!windc) return FALSE;
		
		CDC memdc;
		memdc.CreateCompatibleDC(windc);
	
		CRect rect;
		::GetClientRect(hWnd,&rect);

		if(nPercent<1) nPercent=1;

		int width=rect.Width()*nPercent/100;
		int height=rect.Height()*nPercent/100;
				
		CBitmap *pOldBmp = NULL;
		bitmap.CreateCompatibleBitmap(windc, width,height);
		pOldBmp = memdc.SelectObject(&bitmap);
		
		//memdc.BitBlt(0, 0, rect.Width(),rect.Height(),windc, rect.left, rect.top, SRCCOPY);

		SetStretchBltMode(memdc,  COLORONCOLOR);
		memdc.StretchBlt(0, 0, width,height,windc,
		                 0, 0, rect.Width(),rect.Height(),SRCCOPY);
	
		memdc.SelectObject(pOldBmp);
	
		pOldBmp->DeleteObject();
		memdc.DeleteDC();
		::ReleaseDC(hWnd,windc->m_hDC);	
		return TRUE;
	}

	static BOOL gdc_imgPictureSize(CString file,int &width,int &height)
	{//2Mbyte이상은 로딩못함.		
		LPPICTURE pPic;
		USES_CONVERSION;

		BOOL bflag=FALSE;
		width=height=0;

		HRESULT hr= ::OleLoadPicturePath(const_cast<LPOLESTR>(T2COLE(file)),
					NULL,0,0,IID_IPicture,reinterpret_cast<LPVOID *>(&pPic));    
		if (pPic)
		{		   
		   HBITMAP hbm = NULL;
           OLE_HANDLE handle;
		   if( SUCCEEDED( pPic->get_Handle( &handle ) ) )
		   {
				hbm = (HBITMAP)handle;
				if(hbm)
				{
					BITMAP bmp;
					GetObject(hbm, sizeof(BITMAP), (LPSTR)&bmp);

					bflag=TRUE;
					width = bmp.bmWidth;
					height = bmp.bmHeight;				
				}                
				DeleteObject(hbm);
		   }
		}

		SAFE_RELEASE(pPic);
		return bflag;
	}

	static void gdc_SetBGColor(CWnd *pWnd,CRect rect,int icolor)
	{
		if(!pWnd) return;
		if(!::IsWindow(pWnd->m_hWnd)) return;
		if(!::IsWindowVisible(pWnd->m_hWnd)) return;
		
		CDC *pDC=pWnd->GetDC();
		if(!pDC) return;
		if(!pDC->m_hDC) return;
		if(!pDC->m_hAttribDC) return;

		pDC->FillSolidRect(&rect,icolor);//FillRect error happen	
		
		::ReleaseDC(pWnd->m_hWnd,pDC->m_hDC);		
	}

	static HBRUSH gdc_SetBgBrush(int icolor)
	{
		HBRUSH hbr=CreateSolidBrush(icolor);
		return hbr;
	}

	static int gdc_GetBrushColor(HBRUSH bclrBrush)
	{
		LOGBRUSH logBrush;
	    GetObject(bclrBrush, sizeof(LOGBRUSH), &logBrush);	
	    return logBrush.lbColor;
	}

	static void gdc_SetBrushColor(CWnd *pWnd,int icolor)
	{
		if(!pWnd) return;

		CDC *pDC=pWnd->GetDC();
		CBrush brush = CBrush(icolor);
		
		CRect rcBounds;		
		::GetClientRect(pWnd->m_hWnd,&rcBounds);

		pDC->FillRect(&rcBounds, &brush);	
				
		::ReleaseDC(pWnd->m_hWnd,pDC->m_hDC);
		brush.DeleteObject();
	}

	static void gdc_SetBGClientColor(CWnd *pWnd,CRect rect,int icolor)
	{
		if(!pWnd) return;
		if(!::IsWindow(pWnd->m_hWnd)) return;
		if(!::IsWindowVisible(pWnd->m_hWnd)) return;
		
		CClientDC dc(pWnd);
		if(!dc.m_hDC) return;
		if(!dc.m_hAttribDC) return;

		dc.FillSolidRect(&rect,icolor);
	}*/
		
} ;