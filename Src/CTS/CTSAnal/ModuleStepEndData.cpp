// ModuleStepEndData.cpp: implementation of the CModuleStepEndData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ModuleStepEndData.h"
#include <afxstr.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CModuleStepEndData::CModuleStepEndData()
{

}

CModuleStepEndData::~CModuleStepEndData()
{
	LPEP_CH_DATA lpChData;
	POSITION pos = m_ChDataList.GetHeadPosition();
	while(pos)
	{
		lpChData = m_ChDataList.RemoveHead();
		delete lpChData;
		pos = m_ChDataList.GetHeadPosition();
	}
}

//Module에서 DownLoad한 복구용 StepEnd 파일을 Parsing한다.
//같은 Step의 번호가 중복될 경우 가장 마지막 Data 사용한다.
int CModuleStepEndData::LoadData(int nStepIndex, CString strEndFile)
{
	// for 손실데이터 복구 부분 에러 수정
	if(strEndFile.IsEmpty() || nStepIndex < 0)	
		return 0;
	
	//손실구간 복구 
	LPEP_CH_DATA lpChData, lpChTemp;
	//모든 step의 Data를 모두 재전송한다.
	//database server에 중복 step 전송시 최종 Data로 갱신여부 확인
	int nPrevStepNo = 0;		// 이전 스텝정보

	CStdioFile sourceFile;			
	CFileException ex;	
	CString saveFilePath;	
	CString strReadData;
	CString strData;

	CString strIndex;	
	int nSize = -1;

	if( !sourceFile.Open( strEndFile, CFile::modeRead, &ex ))
	{
		return FALSE;
	}

	int i=0;
	
	sourceFile.ReadString(strReadData);    // 항목의 제목을 읽는다.

	while(TRUE)	
	{			
		sourceFile.ReadString(strReadData);    // 한 문자열씩 읽는다.
		nSize = strReadData.Find(',');
		if( nSize > 0 )
		{		
			lpChData = new EP_CH_DATA;
			ZeroMemory(lpChData, sizeof(EP_CH_DATA));

			lpChData->wChIndex = nStepIndex;
			AfxExtractSubString(strData, strReadData, 2,',');
			lpChData->state = (WORD)atoi(strData);
			AfxExtractSubString(strData, strReadData, 3,',');
			lpChData->grade = (BYTE)atoi(strData);			
			AfxExtractSubString(strData, strReadData, 4,',');
			lpChData->channelCode = atoi(strData);			
			AfxExtractSubString(strData, strReadData, 5,',');
			lpChData->nStepNo = atoi(strData);
			AfxExtractSubString(strData, strReadData, 6,',');			
			lpChData->ulStepTime = atoi(strData)*10;			
			AfxExtractSubString(strData, strReadData, 7,',');
			lpChData->ulTotalTime = atoi(strData)*10;						
			
			AfxExtractSubString(strData, strReadData, 8,',');
			lpChData->lVoltage = (INT)(atof(strData)*10);
			AfxExtractSubString(strData, strReadData, 9,',');
			lpChData->lCurrent = (INT)(atof(strData)*100);
			AfxExtractSubString(strData, strReadData, 10,',');
			lpChData->lWatt = (INT)(atof(strData)*100);
			AfxExtractSubString(strData, strReadData, 11,',');
			lpChData->lWattHour = (INT)(atof(strData)*100);
			AfxExtractSubString(strData, strReadData, 12,',');
			lpChData->lCapacity = (INT)(atof(strData)*100);
			AfxExtractSubString(strData, strReadData, 13,',');
			lpChData->lImpedance = (INT)(atof(strData)*100);
						
			AfxExtractSubString(strData, strReadData, 14,',');
			lpChData->lCcCapacity = (INT)(atof(strData)*100);
			AfxExtractSubString(strData, strReadData, 15,',');
			lpChData->lCvCapacity = (INT)(atof(strData)*100);
			AfxExtractSubString(strData, strReadData, 16,',');			
			lpChData->ulCcRunTime = (INT)(atof(strData)*10);
			AfxExtractSubString(strData, strReadData, 17,',');			
			lpChData->ulCvRunTime = (INT)(atof(strData)*10);
			AfxExtractSubString(strData, strReadData, 18,',');
			lpChData->lDCIR_V1 = (INT)(atof(strData)*10);
			AfxExtractSubString(strData, strReadData, 19,',');
			lpChData->lDCIR_V2 = (INT)(atof(strData)*10);
			AfxExtractSubString(strData, strReadData, 20,',');
			lpChData->lDCIR_AvgCurrent = (INT)(atof(strData)*100);
			AfxExtractSubString(strData, strReadData, 21,',');
			lpChData->lDCIR_AvgTemp = (INT)(atof(strData)*100);
			AfxExtractSubString(strData, strReadData, 22,',');
			lpChData->lTimeGetChargeVoltage = (INT)(atof(strData)*10);
			AfxExtractSubString(strData, strReadData, 23,',');
			lpChData->lComDCIR = (INT)(atof(strData)*100);
			AfxExtractSubString(strData, strReadData, 24,',');
			lpChData->lComCapacity = (INT)(atof(strData)*100);

			AfxExtractSubString(strData, strReadData, 25,',');
			lpChData->currentFaultLevel3Cnt = atoi(strData);
			AfxExtractSubString(strData, strReadData, 26,',');
			lpChData->currentFaultLevel2Cnt = atoi(strData);
			AfxExtractSubString(strData, strReadData, 27,',');
			lpChData->currentFaultLevel1Cnt = atoi(strData);

			AfxExtractSubString(strData, strReadData, 28,',');
			lpChData->voltageFaultLevelV3Cnt = atoi(strData);
			AfxExtractSubString(strData, strReadData, 29,',');
			lpChData->voltageFaultLevelV2Cnt = atoi(strData);
			AfxExtractSubString(strData, strReadData, 30,',');
			lpChData->voltageFaultLevelV1Cnt = atoi(strData);

			AfxExtractSubString(strData, strReadData, 31,',');
			lpChData->ulTotalSamplingVoltageCnt = atoi(strData);
			AfxExtractSubString(strData, strReadData, 32,',');
			lpChData->ulTotalSamplingCurrentCnt = atoi(strData);


			for( i=0; i<6; i++ )
			{
				AfxExtractSubString(strData, strReadData, 33+i,',');
				lpChData->ulJigTemp[i] = (INT)(atof(strData)*100);
			}

			AfxExtractSubString(strData, strReadData, 39,',');
			lpChData->ulTempMax = (INT)(atof(strData)*100);

			AfxExtractSubString(strData, strReadData, 40,',');
			lpChData->ulTempMin = (INT)(atof(strData)*100);

			AfxExtractSubString(strData, strReadData, 41,',');
			lpChData->ulTempAvg = (INT)(atof(strData)*100);


			//Step 번호가 중복될 경우 최종 data로 갱신하도록 이전 data를 삭제한다.
			if(nPrevStepNo == lpChData->nStepNo)
			{
				if(m_ChDataList.GetCount() > 0)
				{
					lpChTemp = m_ChDataList.RemoveTail();
					delete lpChTemp;
				}
			}	
			
			m_ChDataList.AddTail(lpChData);			
		}			
		else
		{
			break;				
		}
	}			  
	sourceFile.Close();		
	return m_ChDataList.GetCount();
}

int CModuleStepEndData::GetDataSize()
{
	return m_ChDataList.GetCount();
}

LPEP_CH_DATA CModuleStepEndData::GetStepData(int nStepNo)
{
	LPEP_CH_DATA lpChData;
	POSITION pos = m_ChDataList.GetHeadPosition();
	while(pos)
	{
		lpChData = 	m_ChDataList.GetNext(pos);
		if(lpChData->nStepNo == nStepNo)
		{
			return lpChData;
		}
	}
	return NULL;
}
