// DataDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "DataDlg.h"
#include "CTSAnalView.h"

#include "TestLogSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDataDlg dialog

bool CDataDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CDataDlg"), _T("TEXT_CDataDlg_CNT"), _T("TEXT_CDataDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CDataDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CDataDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

CDataDlg::CDataDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDataDlg::IDD, pParent)
{
	LanguageinitMonConfig();

	//{{AFX_DATA_INIT(CDataDlg)
		// NOTE: the ClassWizard will add member initialization here
	m_pView = (CCTSAnalView *)pParent;
	//}}AFX_DATA_INIT
/*	for(int i = 0; i <= 255; i++)
	{
		m_Fault[i] = 0;
     	m_Error[i] = 0;
	}
*/
	m_Half    = 0;
	m_nTotInput = 0;
	m_nNormalcell = 0;


	for(int i = 0; i<FIELDNO; i++)   
	{
		m_dataField[i].dAve = 0.0;
		m_dataField[i].dMax = 0.0;
		m_dataField[i].dMin = 0.0;
		m_dataField[i].dSTD = 0.0;
		m_dataField[i].nCellNo = 0;
		m_dataField[i].Field.bDisplay = FALSE;
	}
	m_pDBServer = NULL;
}

CDataDlg::~CDataDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

void CDataDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDataDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CDataDlg, CDialog)
	//{{AFX_MSG_MAP(CDataDlg)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_SAVE_FILE, OnSaveFile)
	ON_CBN_SELCHANGE(IDC_PROC_SELECT, OnSelchangeProcSelect)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDataDlg message handlers

BOOL CDataDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
//--------------------DB Open --------------------------//
	m_Module.Open();
	m_Test.Open();
	m_ChValue.Open();
	m_ChResult.Open();
//------------------------------------------------------//
	InitField();

	InitDataGrid();
	
	InitErrorGrid();
	InitGraph();
	
	DataSearch();

//	ErrorSearch();
	UpdateFailGrid();
	
	GraphUpdate();
	ErrorSearch(5);

	GetDlgItem(IDC_TITLE_LABEL)->SetWindowText(m_strTitle);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDataDlg::InitDataGrid()
{
/*	int Col = 0, DisCol = 0;

	for(int i = 0; i < m_pView->m_TotCol ;i++)
	{
    	m_Name[i] = m_pView->m_field[i].FieldName;
	}
	Col = m_pView->m_TotCol + 2;
	m_Half = (int)Col / 2;
	DisCol = Col - 2;              //-2하는이유는 GradingCode,FailCode를 표시하지 않기때문에...
*/
	
	m_Half = m_nFieldNo / 2;
	m_DataGrid.SubclassDlgItem(IDC_DATA_LIST1, this);
	m_DataGrid.m_bSameRowSize = FALSE;
	m_DataGrid.m_bSameColSize = TRUE;
//---------------------------------------------------------------------//
	m_DataGrid.m_bCustomWidth = FALSE;
	m_DataGrid.m_bCustomColor = FALSE;

	m_DataGrid.Initialize();
	m_DataGrid.LockUpdate();

	m_DataGrid.SetRowCount(8);
	m_DataGrid.SetColCount(m_nFieldNo+1);        
	m_DataGrid.SetRowHeight( 0, 0, 0);
	m_DataGrid.SetRowHeight( 2, 2, 0);
	m_DataGrid.SetDefaultRowHeight(20);
	m_DataGrid.EnableGridToolTips();

	//Row Header Setting
	m_DataGrid.SetStyleRange(CGXRange().SetCols(1),
			CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(0,0,0)));
	m_DataGrid.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(9).SetFaceName(TEXT_LANG[0])));//"굴림"

	m_DataGrid.SetCoveredCellsRowCol(1, 1, 1, 2);
//	m_DataGrid.SetCoveredCellsRowCol(1, 3, 1, m_Half);
	m_DataGrid.SetCoveredCellsRowCol(1, m_Half+1, 1, m_Half+2);
//	m_DataGrid.SetCoveredCellsRowCol(1, m_Half+3, 1, m_nFieldNo);
	m_DataGrid.SetValueRange(CGXRange(1, 1), TEXT_LANG[1]);//"전 지   갯 수"
	m_DataGrid.SetValueRange(CGXRange(1, m_Half+1), TEXT_LANG[2]);//"비       고"
	m_DataGrid.SetStyleRange(CGXRange(1, m_Half+1),
			CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(0,0,0)));


	m_DataGrid.SetCoveredCellsRowCol( 1, 3, 1, m_nFieldNo+1);
	m_DataGrid.SetCoveredCellsRowCol( 2, 1, 2, m_nFieldNo+1);
	m_DataGrid.SetValueRange(CGXRange( 2, 1),"Data Report");

	m_DataGrid.SetRowHeight(3, 3, 40);
	m_DataGrid.SetStyleRange(CGXRange().SetRows(3),
			CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(0,0,0)));
	
	m_DataGrid.SetValueRange(CGXRange(3, 1), TEXT_LANG[3]);//"구 분"

	for(int i = 0; i < m_nFieldNo ; i++)
	{
		m_DataGrid.SetValueRange(CGXRange(3, i+2), m_Field[i].FieldName);
	}

//	m_DataGrid.SetValueRange(CGXRange(4 ,i-1), "비 고");

	m_DataGrid.SetValueRange(CGXRange( 4, 1), TEXT_LANG[4]);//"Ave"
	m_DataGrid.SetValueRange(CGXRange( 5, 1), TEXT_LANG[5]);//"Max"
	m_DataGrid.SetValueRange(CGXRange( 6, 1), TEXT_LANG[6]);//"Min"
	m_DataGrid.SetValueRange(CGXRange( 7, 1), "S");
	m_DataGrid.SetValueRange(CGXRange( 8, 1), TEXT_LANG[7]);//"진행된수"

	m_DataGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	
	m_DataGrid.SetScrollBarMode(SB_BOTH, gxnDisabled,TRUE);
	m_DataGrid.LockUpdate(FALSE);
	m_DataGrid.Redraw();
}

void CDataDlg::InitErrorGrid()
{
	m_ErrorGrid.SubclassDlgItem(IDC_DATA_LIST2, this);
	m_ErrorGrid.m_bSameRowSize = FALSE;
	m_ErrorGrid.m_bSameColSize = TRUE;
//---------------------------------------------------------------------//
//	m_ErrorGrid.m_bCustomWidth = FALSE;
	m_ErrorGrid.m_bCustomColor = FALSE;

	CRect rect;
	m_ErrorGrid.GetWindowRect(rect);		//Step Grid Window Position
	ScreenToClient(&rect);

	int width = rect.Width()/100;
	m_ErrorGrid.m_nWidth[1]	= (int)(width* 2.0f);
	m_ErrorGrid.m_nWidth[2]	= (int)(width* 2.0f);
	m_ErrorGrid.m_nWidth[3]	= (int)(width* 2.0f);
	m_ErrorGrid.m_nWidth[4]	= (int)(width* 2.0f);

	m_ErrorGrid.Initialize();
	m_ErrorGrid.LockUpdate();

	m_ErrorGrid.SetRowCount(0);
	m_ErrorGrid.SetColCount(4);
	m_ErrorGrid.SetDefaultRowHeight(18);

	m_ErrorGrid.SetValueRange(CGXRange(0,1), TEXT_LANG[3]);//"구 분"
	m_ErrorGrid.SetValueRange(CGXRange(0,2), TEXT_LANG[8]);//"불량수량"
	m_ErrorGrid.SetValueRange(CGXRange(0,3), TEXT_LANG[9]);//"불량비율"
	m_ErrorGrid.SetValueRange(CGXRange(0,4), TEXT_LANG[10]);//"전체비율"

	m_ErrorGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	//Row Header Setting
	m_ErrorGrid.SetStyleRange(CGXRange().SetCols(1),
			CGXStyle().SetEnabled(TRUE).SetTextColor(RGB(0,0,0)).SetInterior(RGB(192,192,192)));
	m_ErrorGrid.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(9).SetFaceName(TEXT_LANG[0])));//"굴림"
	m_ErrorGrid.EnableCellTips();
	m_ErrorGrid.SetStyleRange(CGXRange().SetTable(), CGXStyle().SetWrapText(FALSE).SetAutoSize(TRUE));
	
	m_ErrorGrid.LockUpdate(FALSE);
	m_ErrorGrid.Redraw();
}

void CDataDlg::InitGraph()
{
	CRect rect;
	GetDlgItem(IDC_ERROR_GRAPH)->GetWindowRect(rect);
	ScreenToClient(rect);
	m_hWndPE = PEcreate(PECONTROL_GRAPH, WS_BORDER, &rect, m_hWnd, IDC_ERROR_GRAPH);

	ASSERT(m_hWndPE);

	PEnset(m_hWndPE, PEP_nSUBSETS, 1);
	PEnset(m_hWndPE, PEP_nPOINTS,  256);

	PEnset(m_hWndPE, PEP_bPREPAREIMAGES, TRUE);
//	PEnset(m_hWndPE, PEP_nSCROLLINGSUBSETS , 1);		//동시에 보여지는 그래프 수를 나타 낸다. 
	PEnset(m_hWndPE, PEP_nDATAPRECISION, 0);			//테이블에서 소수점 아래 자리수 정하기
	PEnset(m_hWndPE, PEP_nGRIDLINECONTROL, PEGLC_YAXIS);	//Grid Line 표시 여부
//	PEnset(m_hWndPE, PEP_nALLOWZOOMING, PEAZ_HORZANDVERT);	//Zoom in/Out X,Y
	PEnset(m_hWndPE, PEP_bDATASHADOWS, FALSE);				//Data에 대한 그림자
	PEnset(m_hWndPE, PEP_nGRAPHPLUSTABLE, PEGPT_BOTH);

	PEszset(m_hWndPE, PEP_szMAINTITLEFONT, "굴림");//omit
	PEszset(m_hWndPE, PEP_szSUBTITLEFONT, "굴림");//omit


//	PEnset(m_hWndPE, PEP_bSHOWXAXISANNOTATIONS, TRUE);				//Annotation을 보여 준다.
//    PEnset(m_hWndPE, PEP_bSHOWANNOTATIONS, TRUE);					//Annotation을 사용 한다.
//	PEnset(m_hWndPE, PEP_bALLOWANNOTATIONCONTROL, TRUE);			//사용자가 선택 가능 
//	char MGText[] = "##########";
//	PEszset(m_hWndPE, PEP_szRIGHTMARGIN, MGText);
//	PEnset(m_hWndPE, PEP_nLINEANNOTATIONTEXTSIZE, 100);					//Annotation을 보여 준다.

	PEnset(m_hWndPE, PEP_bAUTOSCALEDATA, FALSE);					//This property controls whether the ProEssentials will automatically scale data that is very small or very large.
	

/*	int mas[2];
	mas[0] = 1;
	mas[1] = 1;
	int nYAxis = 2;
	PEvset(m_hWndPE, PEP_naMULTIAXESSUBSETS, mas, nYAxis);						//각 6개의 축에 보여줄 SubSet 갯수 지정(파일수/파일수/...) 
	PEvset(m_hWndPE, PEP_naOVERLAPMULTIAXES, &nYAxis, 1);						//1 Y축에 선택한 Item Y축을 한꺼번에 표시			
*/
//	PEnset(m_hWndPE, PEP_nALLOWZOOMING, TRUE);

	char szTitle[] = "Histogram\t";
	PEvset(m_hWndPE, PEP_szaSUBSETLABELS, szTitle, 1);			//4개의 그래프 라벨

	PEszset(m_hWndPE, PEP_szMAINTITLE, "불량 분포");//TEXT_LANG[11]
	PEszset(m_hWndPE, PEP_szSUBTITLE, "");
   
    PEszset(m_hWndPE, PEP_szXAXISLABEL, "불량");//TEXT_LANG[12]

	PEnset (m_hWndPE, PEP_nPLOTTINGMETHOD, PEGPM_BAR);
	PEszset(m_hWndPE, PEP_szYAXISLABEL, "Cell 수");//TEXT_LANG[13]

	PEreinitialize(m_hWndPE);
	PEresetimage(m_hWndPE, 0, 0);
}

void CDataDlg::InitField()
{
/*	ASSERT(m_ReportDataList.IsOpen());
	
	int Col = 0;
	
	if(!m_ReportDataList.IsBOF())
	{
		while(!m_ReportDataList.IsEOF())
		{
			m_field2[Col].No        = m_pDoc->m_field[Col].No;
			m_field2[Col].ID        = m_pDoc->m_field[Col].ID;
			m_field2[Col].FieldName = m_pDoc->m_field[Col].FieldName;
			m_field2[Col].DataType  = m_pDoc->m_field[Col].DataType;
			m_field2[Col].bDisplay  = m_pDoc->m_field[Col].bDisplay;
			m_field2[Col].Ave      = 0;
    		m_field2[Col].Max      = 0;
    		m_field2[Col].Min      = 0;
    		m_field2[Col].STD      = 0;
    		Col++;
			m_ReportDataList.MoveNext();
		}
		Col++;
		m_ReportDataList.Close();
	}
*/
}


BOOL CDataDlg::Create() 
{
	return CDialog::Create(CDataDlg::IDD);
}

/*
void CDataDlg::DataSearch()
{
	CString  str;
	int   i = 1, count = 0, col = 2;

	count = m_pView->m_DataGrid.GetRowCount();   //현재 선택된 Record수
	ASSERT(count > 0);

   	for(i = 1; i <= count; i++)
	{
       	str = m_pView->m_DataGrid.GetValueRowCol(i,3);
       	m_Test.m_strFilter.Format("TestSerialNo = '%s'",str);
       	m_Test.Requery();

       	while(!m_Test.IsEOF())
		{
       		TestTableSearch();
			m_Test.MoveNext();
		}
	}
	
	for(i = 0; i <= 12; i++)
	{
		if(m_Count[i] != 0)
		{
    		m_field2[i].Ave = m_field2[i].Ave / m_Count[i];
    		m_field2[i].Max = m_field2[i].Max / m_Count[i];
    		m_field2[i].Min = m_field2[i].Min / m_Count[i];
    		m_field2[i].STD = m_field2[i].STD / m_Count[i];
		}
	}
	

	for(i = 0; i <= 12; i++)
	{
		if(m_field2[i].bDisplay == 1)
		{
    		switch(m_field2[i].DataType)
			{
    		case EP_VOLTAGE:
    				str.Format("%.3f",m_field2[i].Ave);
	    			m_DataGrid.SetValueRange(CGXRange(5, col), str);
		    		str.Format("%.3f",m_field2[i].Max); 
    		    	m_DataGrid.SetValueRange(CGXRange(6, col), str);
    				str.Format("%.3f",m_field2[i].Min);
        			m_DataGrid.SetValueRange(CGXRange(7, col), str);
		    		str.Format("%.4f",m_field2[i].STD);
    		    	m_DataGrid.SetValueRange(CGXRange(8, col), str);
				break;
    		case EP_CURRENT:
		    		str.Format("%.1f",m_field2[i].Ave);
		    		m_DataGrid.SetValueRange(CGXRange(5, col), str);
	    			str.Format("%.1f",m_field2[i].Max);
   	    			m_DataGrid.SetValueRange(CGXRange(6, col), str);
		    		str.Format("%.1f",m_field2[i].Min);
   	        		m_DataGrid.SetValueRange(CGXRange(7, col), str);
		    		str.Format("%.4f",m_field2[i].STD);
   	    			m_DataGrid.SetValueRange(CGXRange(8, col), str);
				break;
    		case EP_CAPACITY:
    		case EP_IMPEDANCE:
		    		str.Format("%.1f", m_field2[i].Ave);
		    		m_DataGrid.SetValueRange(CGXRange(5, col), str);
		    		str.Format("%.1f", m_field2[i].Max);
   		    		m_DataGrid.SetValueRange(CGXRange(6, col), str);
		    		str.Format("%.1f", m_field2[i].Min);
   		    		m_DataGrid.SetValueRange(CGXRange(7, col), str);
		    		str.Format("%.4f", m_field2[i].STD);
   		    		m_DataGrid.SetValueRange(CGXRange(8, col), str);
				break;
    		case EP_STEP_TIME:
    		case EP_TOT_TIME:
		    		str = m_pView->Time(m_field2[i].Ave);
		    		m_DataGrid.SetValueRange(CGXRange(5, col), str);
	    			str = m_pView->Time(m_field2[i].Max);
   	    			m_DataGrid.SetValueRange(CGXRange(6, col), str);
	    			str = m_pView->Time(m_field2[i].Min);
   	    			m_DataGrid.SetValueRange(CGXRange(7, col), str);
	    			str = m_pView->Time(m_field2[i].STD);
   	    			m_DataGrid.SetValueRange(CGXRange(8, col), str);
				break;
    		default:
 	    			str.Format("%.3f",m_field2[i].Ave);
	    			m_DataGrid.SetValueRange(CGXRange(5, col), str);
	    			str.Format("%.3f",m_field2[i].Max);
   	    			m_DataGrid.SetValueRange(CGXRange(6, col), str);
	    			str.Format("%.3f",m_field2[i].Min);
   	    			m_DataGrid.SetValueRange(CGXRange(7, col), str);
	    			str.Format("%.4f",m_field2[i].STD);
   	     			m_DataGrid.SetValueRange(CGXRange(8, col), str);
				break;
			}
			col++;
		}
	}
}
*/



BOOL CDataDlg::DataSearch()
{
	if(m_strFilterString.IsEmpty() || m_pDBServer == NULL)		return FALSE;

	double dSum =0.0, dSquareSum = 0.0,  dMax = -10E+100, dMin = 10E+100;
	int nCount = 0, nInputCell = 0, nNormalCell = 0;
	ZeroMemory(m_Fault, sizeof(m_Fault));
	m_nTotInput = 0;
	m_nNormalcell = 0;

	
	CTestLogSet testLog;
	testLog.m_strSort.Format("[TestLogID] DESC");

/*	if(m_nFilterType == DATA_LOT)
	{
		testLog.m_strFilter.Format("[LotNo] = '%s'", m_strFilterString);	
	}
	else if(m_nFilterType == DATA_TRAY)
	{
		testLog.m_strFilter.Format("[TrayNo] = '%s'", m_strFilterString);
	}
	else
	{
		testLog.m_strFilter.Format("[ModelName] = '%s'", m_strFilterString);
	}
*/
	testLog.m_strFilter = m_strFilterString;
	testLog.Open();

	if(testLog.IsBOF())
	{
		testLog.Close();
		return FALSE;
	}
	
	for(int field = 0; field<m_nFieldNo; field++)					//현재 선택된 Field에 대해 구한다.
	{
		if(m_Field[field].ID < EP_PROC_TYPE_CHARGE_LOW)	continue;		//Grading or Fail Code

		dSum =0.0; dSquareSum = 0.0;  dMax = -10E+100; dMin = 10E+100;
		nCount = 0;
		
		testLog.MoveFirst();

		while(!testLog.IsEOF())
		{

//		for(int nRow = 0; nRow<nRowCount; nRow++)				//검색된 범위에서 전체 검색 
//		{
//			str = m_pView->m_DataGrid.GetValueRowCol( nRow+1, 3);
//			m_ChResult.m_strFilter.Format("[TestSerialNo] = '%s' AND [dataType] = %d", str, EP_CH_CODE);
			
			m_ChResult.m_strFilter.Format("[TestSerialNo] = '%s' AND [dataType] = %d", testLog.m_TestSerialNo, EP_CH_CODE);
	 		m_ChResult.m_strSort.Format("[Index] DESC");			//현재 Tray 최종 상태를 선택
//			m_Test.m_strFilter.Format( "[TestSerialNo] = '%s' AND [ProgressID] = %ld", str, m_Field[field].ID);
//			m_Test.m_strFilter.Format( "[TestSerialNo] = '%s' AND [ProgressID] = %ld", testLog.m_TestSerialNo, m_Field[field].ID);
			//2002/5/1
			m_Test.m_strFilter.Format("[TestSerialNo] = '%s' AND [ProcType] = %ld AND [DataType] = %ld", testLog.m_TestSerialNo, m_Field[field].ID, m_Field[field].DataType);
			m_Test.m_strSort.Format("[TestIndex] DESC");			//중복된 시험이 있을 경우 가장 마지막 시험을 Load
			
			try			
			{
				m_ChResult.Requery();
				m_Test.Requery();
			}
			catch (CDBException* e)
			{
     			AfxMessageBox(e->m_strError);
     			e->Delete();
				testLog.Close();
      			return FALSE;	
			}
			if(m_ChResult.IsBOF())
			{
				if(field == 0)		TRACE("Data Skipped 2 [%d]\n", m_ChResult.m_Index);
				testLog.MoveNext();
				continue;
			}
			
			if(field == 0)
			{
				nInputCell += EP_BATTERY_PER_TRAY;	
				
				for(int i = 0; i < EP_BATTERY_PER_TRAY; i++)
				{						
					ASSERT((BYTE)m_ChResult.m_ch[i]>=0 && (BYTE)m_ChResult.m_ch[i] < 256);

					if(IsNormalCell((BYTE)m_ChResult.m_ch[i]))				//정상 Cell
					{
						nNormalCell++;
						m_Fault[EP_CODE_NORMAL]++;
					}
					else if(IsNonCell((BYTE)m_ChResult.m_ch[i]))			//현재 Tray의 NonCell
					{
						nInputCell--;
						m_Fault[EP_CODE_NONCELL]++;
					}
					else if(IsCellCheckFail((BYTE)m_ChResult.m_ch[i]))		//Cell Check 불량 
					{
						m_Fault[EP_CODE_CELL_CHECK_FAIL]++;
					}
					else if(IsFailCell((BYTE)m_ChResult.m_ch[i]))			//불량 Cell
					{
						m_Fault[EP_CODE_CELL_FAIL]++;
					}
					else //if(IsSysFail((BYTE)m_ChResult.m_ch[i]))			//기타 Cell
					{
						m_Fault[EP_CODE_SYS_ERROR]++;
					}
					
					if((BYTE)m_ChResult.m_ch[i] > EP_SYS_CODE_HIGH)
						m_Fault[(BYTE)m_ChResult.m_ch[i]]++;
				}	
//				TRACE("Procedure ID = %d\n", m_Test.m_ProcedureID);
				//TRACE("1) [%s] INPUT Total Cell %d: Normal Cell %d, Tray %s\n", testLog.m_TestSerialNo, nInputCell, nNormalCell, testLog.m_TrayNo);
			}

			if(m_Test.IsBOF())
			{
//				if(field == 0)		TRACE("Data Skipped 1 [%d]\n", m_ChResult.m_Index);
				testLog.MoveNext();
				continue;
			}

//			m_ChValue.m_strFilter.Format("[TestIndex] = %d AND [DataType] = %d", m_Test.m_TestIndex, m_Test.m_DataType);	//현재 Test의 값을 Read
			m_ChValue.m_strFilter.Format("[TestIndex] = %d", m_Test.m_TestIndex);	//현재 Test의 값을 Read
			m_ChValue.m_strSort.Format("[Index] DESC");				//중복된 시험이 있을 경우 가장 마지막 시험을 Load
			m_ChValue.Requery();

			if(!m_ChValue.IsBOF())
			{
	//			TRACE("Data Serarched Serial %s, Data Type %d\n", str, m_Field[field].ID);
				for(int i = 0; i < EP_BATTERY_PER_TRAY; i++)
				{								
					if(!IsNormalCell((BYTE)m_ChResult.m_ch[i]))  continue;					//정상이아니면  
				
					nCount++;
					dSum += (double)m_ChValue.m_ch[i];
					dSquareSum += ((double)m_ChValue.m_ch[i]*(double)m_ChValue.m_ch[i]);

					if(dMin > (double)m_ChValue.m_ch[i])  dMin = (double)m_ChValue.m_ch[i];	//Min
					if(dMax < (double)m_ChValue.m_ch[i])  dMax = (double)m_ChValue.m_ch[i];	//Max
				}
			}
#if _DEBUG
			else
			{
//				if(field == 0)		TRACE("Data Skipped 3 [%d]\n", m_ChResult.m_Index);
			}
#endif
			testLog.MoveNext();
		}

		if(nCount == 0)
		{
			m_dataField[field].nCellNo = 0;
			m_dataField[field].dAve = 0.0;		//Mean
			m_dataField[field].dSTD = 0.0;		//Deviation
			m_dataField[field].dMax = 0.0;
			m_dataField[field].dMin = 0.0;
		}
		else
		{
			m_dataField[field].nCellNo = nCount;
			m_dataField[field].dAve  = dSum/(double)nCount;											//Mean
			m_dataField[field].dSTD = sqrt(fabs(dSquareSum/(double)nCount - m_dataField[field].dAve * m_dataField[field].dAve));		//Deviation
			m_dataField[field].dMax = dMax;
			m_dataField[field].dMin = dMin;
		}
	}
	testLog.Close();

#if _DEBUG
	for(int i =0; i<256; i++)
	{
		if(m_Fault[i] > 0)
		TRACE("%d:%d, ", i, m_Fault[i]);
	}
	TRACE("\n");
#endif
	
	m_nTotInput = nInputCell;
	m_nNormalcell = nNormalCell;

	TRACE("INPUT Total Cell %d: Normal Cell %d\n\n", nInputCell, nNormalCell);
	UpdateDisplayData(nInputCell, nNormalCell);
	return TRUE;
}

//각 공정별 불량 수량 검사 
BOOL CDataDlg::ErrorSearch()
{
	if(m_strFilterString.IsEmpty() == TRUE || m_pDBServer == NULL)	return FALSE;

	int nCount = 0, nInputCell = 0, nNormalCell = 0;

	ZeroMemory(m_Fault, sizeof(m_Fault));

	CTestLogSet testLog;
	testLog.m_strSort.Format("[TestLogID] DESC");

/*	if(m_nFilterType == DATA_LOT)
	{
		testLog.m_strFilter.Format("[LotNo] = '%s'", m_strFilterString);	
	}
	else if(m_nFilterType == DATA_TRAY)
	{
		testLog.m_strFilter.Format("[TrayNo] = '%s'", m_strFilterString);
	}
	else
	{
		testLog.m_strFilter.Format("[ModelName] = '%s'", m_strFilterString);
	}
*/	
	testLog.m_strFilter = m_strFilterString;
	testLog.Open();

	if(testLog.IsBOF())
	{
		testLog.Close();
		return FALSE;
	}

	while(!testLog.IsEOF())
	{
		m_Module.m_strFilter.Format("[TestSerialNo] = '%s'", testLog.m_TestSerialNo);
		m_Module.m_strSort.Format("[ProcedureID] DESC");		//전체 검색 일때는 공정의 가장 마지막 결과만 검색 

		try			
		{
			m_Module.Requery();
		}
		catch (CDBException* e)
		{
			AfxMessageBox(e->m_strError);
			e->Delete();
			testLog.Close();
			return FALSE;
		}

		if(m_Module.IsBOF() == TRUE)
		{
			testLog.MoveNext();
			continue;
		}
//		TRACE("Search %s Test\n", str);
		
//#if _DEBUG		
		m_ChResult.m_strFilter.Format("[TestSerialNo] = '%s' AND [dataType] = %d", testLog.m_TestSerialNo, EP_CH_CODE);
		m_ChResult.m_strSort.Format("[Index] DESC");
		try			
		{
			m_ChResult.Requery();
		}
		catch (CDBException* e)
		{
			AfxMessageBox(e->m_strError);
			e->Delete();
		}

		if(m_Module.m_ProcedureID != m_ChResult.m_ProcedureID)
		{
//			TRACE("Data Index Error [Module %d::Test %d]\n", m_Module.m_ProcedureID, m_ChResult.m_ProcedureID);
		}
//#endif

/*		m_ChResult.m_strFilter.Format("[ProcedureID] = %ld AND [dataType] = %d", m_Module.m_ProcedureID, EP_CH_CODE);	//마지막 시험 결과 의 Code
		m_ChResult.m_strSort.Format("[Index] DESC");
		try			
		{
			m_ChResult.Requery();
		}
		catch (CDBException* e)
		{
    		AfxMessageBox(e->m_strError);
    		e->Delete();
    		return FALSE;	
		}
*/
		
		//ChResult Table을 Serial No를 이용하여 직접 검색 하때는 필요 없다.(위의 debug Mode로 검색 할때는)
/*		while(m_ChResult.IsBOF())		//현재 공정의 결과가 기록되어 있지 않다면 이전 시험 결과 검색 
		{
			if(m_Module.IsEOF())	break;
			try	
			{
				m_Module.MoveNext();
				m_ChResult.m_strFilter.Format("[ProcedureID] = %ld AND [dataType] = %d", m_Module.m_ProcedureID, EP_CH_CODE);	//마지막 시험 결과 의 Code
				m_ChResult.m_strSort.Format("[Index] DESC");
				{
					m_ChResult.Requery();
				}
			}
			catch (CDBException* e)
			{
    			AfxMessageBox(e->m_strError);
    			e->Delete();
				testLog.Close();
    			return FALSE;	
			}
		}
*/
		if(!m_Module.IsBOF() && !m_ChResult.IsBOF())				//현재 공정의 결과를 하나도 찾을 수 없다.
		{
			nCount = 0;
			for(int i = 0; i < EP_BATTERY_PER_TRAY; i++)
			{						
				if(IsNonCell((BYTE)m_ChResult.m_ch[i]))			//현재 Tray의 NonCell 수 Count
				{
					nCount++;
				}
				
				if(IsNormalCell((BYTE)m_ChResult.m_ch[i])) 		//정상수 Count  
				{
					nNormalCell++;
				}
								
				m_Fault[(BYTE)m_ChResult.m_ch[i]]++;
			}
			nInputCell += (EP_BATTERY_PER_TRAY - nCount);		//NonCell 이 아닌 Cell은 전지가 투입된 Channel 이다. 
			TRACE("2) [%d] INPUT Total Cell %d: Normal Cell %d\n", m_ChResult.m_Index, nInputCell, nNormalCell);
		}
		
		testLog.MoveNext();
	}
	testLog.Close();

	CString str;
	str.Format(TEXT_LANG[14], nInputCell);//"%d 개"
	m_DataGrid.SetValueRange(CGXRange(1,3), str);
	str.Format(TEXT_LANG[15], nNormalCell);//"정상 Cell  =  %d개"
	m_DataGrid.SetValueRange(CGXRange(1, m_Half+3),str);

	TRACE("INPUT Total Cell %d: Normal Cell %d\n", nInputCell, nNormalCell);
	return TRUE;
	
}
/*
//각 공정별 불량 수량 검사 
//#define EP_PGS_FORMATION	0x03
//#define EP_PGS_IROCV		0x04
//#define EP_PGS_FINALCHARGE	0x05
BOOL CDataDlg::ErrorSearch(int nType)
{
	if(m_strFilterString.IsEmpty() == TRUE || m_pDBServer == NULL)	return FALSE;

	int nCount = 0, nInputCell = 0, nNormalCell = 0;
	ZeroMemory(m_Fault, sizeof(m_Fault));

	CString strQuery, strTemp, strSQL;
	strSQL = "SELECT a.ch1";
	for(int index = 2; index <= EP_BATTERY_PER_TRAY; index++)
	{
		strTemp.Format(", a.ch%d", index);
		strSQL += strTemp;
	}

	short field =0;
	CDBVariant val;
	long tempCode[256];
	long code[3][256];		//각 공정별 Code 집계 ==> Formation, IR/OCV, 출하 충전 
	ZeroMemory(code, sizeof(code));
	BYTE chCode;
	CRecordset testLog(m_pDBServer);

	int nNum = 0;
	CString strTestserial;
	long lProcedureID = 0;

	for(index = EP_PGS_FORMATION; index <= EP_PGS_FINALCHARGE; index++)
	{
		strTemp.Format(", a.testserialno FROM ChResult a, Module b WHERE b.%s AND b.ProcedureType = %d AND a.ProcedureID = b.ProcedureID AND a.DataType = %d ORDER BY a.testserialno, a.ProcedureID", m_strFilterString, index, EP_CH_CODE);
		strQuery = strSQL+strTemp;
		
		testLog.Open(CRecordset::forwardOnly, strQuery);


		while(!testLog.IsEOF())
		{
			field = 0;
			ZeroMemory(tempCode, sizeof(tempCode));
			
			nCount = testLog.GetODBCFieldCount()-1;
			while(field < nCount)
			{
				testLog.GetFieldValue(field++, val, SQL_C_SLONG);
				if(val.m_lVal < 0 || val.m_lVal >= 256)		//NonCell로 처리 
					chCode = EP_CODE_NONCELL;
				else 
					chCode = (BYTE)val.m_lVal;

				tempCode[chCode]++;
			}
//			testLog.GetFieldValue(field++, strTemp);
//			testLog.GetFieldValue(field, val, SQL_C_SLONG);
			
			if(nNum != 0 && strTestserial != strTemp && lProcedureID != val.m_lVal)		//이전 집계 Update
			{
				for(int k = 0; k<256; k++)
				{
					code[index-EP_PGS_FORMATION][k] += tempCode[k];
				}
			}
			strTestserial = strTemp;
			lProcedureID = val.m_lVal;
			nNum++;
			testLog.MoveNext();
		}
		TRACE("Total %d\n", nNum);
		nNum = 0;
		testLog.Close();
	}

#ifdef _DEBUG
	for(int j = 0; j<3; j++)
	{
		for(int i =0; i<EP_BATTERY_PER_TRAY; i++)
		{
			TRACE("%d ,", code[j][i]);
		}
		TRACE("\n");
	}
#endif
	
	strTemp.Format("%d 개", nInputCell);
//	m_DataGrid.SetValueRange(CGXRange(1,3), strTemp);
	strTemp.Format("정상 Cell  =  %d개", nNormalCell);
//	m_DataGrid.SetValueRange(CGXRange(1, m_Half+3),strTemp);

	return TRUE;
}
*/

//각 공정별 불량 수량 검사 
//#define EP_PGS_FORMATION	0x03
//#define EP_PGS_IROCV		0x04
//#define EP_PGS_FINALCHARGE	0x05
BOOL CDataDlg::ErrorSearch(int nType)
{
	if(m_strFilterString.IsEmpty() == TRUE || m_pDBServer == NULL)	return FALSE;

	int nCount = 0;				//, nInputCell = 0, nNormalCell = 0;
	ZeroMemory(m_FailCount, sizeof(m_FailCount));

	CString strQuery, strTemp, strSQL;
	int index = 0;

	strSQL = "SELECT a.ch1";
	for(index = 2; index <= EP_BATTERY_PER_TRAY; index++)
	{
		strTemp.Format(", a.ch%d", index);
		strSQL += strTemp;
	}

	short int field =0;
	CDBVariant val;
	BYTE chCode;
	long tempCode[256];
	ZeroMemory(tempCode, sizeof(tempCode));

	CRecordset testLog(m_pDBServer);

	int nNum = 0;
	long lProcedureID = 0;

	for(index = EP_PGS_FORMATION; index <= EP_PGS_FINALCHARGE; index++)
	{
		strTemp.Format(", b.ProcedureID FROM ChResult a, Module b WHERE b.%s AND b.ProcedureType = %d AND a.ProcedureID = b.ProcedureID AND a.DataType = %d ORDER BY a.testSerialno, a.ProcedureID", m_strFilterString, index, EP_CH_CODE);
		strQuery = strSQL+strTemp;
		
		testLog.Open(CRecordset::forwardOnly, strQuery);
		nNum = 0;
		
		while(!testLog.IsEOF())
		{
			testLog.GetFieldValue(128, val, SQL_C_SLONG);		//ProcedureID
			if(nNum != 0)
			{
				if(lProcedureID != val.m_lVal)		//이전 집계 Update
				{
					for(int k = 0; k<256; k++)
					{
						m_FailCount[index-EP_PGS_FORMATION][k] += tempCode[k];
					}
				}
			}
			lProcedureID = val.m_lVal;		
			ZeroMemory(tempCode, sizeof(tempCode));

			field = 0;		
			nCount = testLog.GetODBCFieldCount()-1;
			while(field < nCount)		//각 Code별 수량을 얻는다.
			{
				testLog.GetFieldValue(field++, val, SQL_C_SLONG);
				if(val.m_lVal < 0 || val.m_lVal >= 256)		//NonCell로 처리 
					chCode = EP_CODE_NONCELL;
				else 
					chCode = (BYTE)val.m_lVal;
				
				tempCode[chCode]++;
			}
			nNum++;
			testLog.MoveNext();
		}
		testLog.Close();
		
		if(nNum > 0)
		{
			for(int k = 0; k<256; k++)
			{
				m_FailCount[index-EP_PGS_FORMATION][k] += tempCode[k];
			}
		}	
	}

#ifdef _DEBUG
	for(int j = 0; j<3; j++)
	{
		for(int i =0; i<256; i++)
		{
			if(m_FailCount[j][i] != 0)
			TRACE("%d:%d ,", i, m_FailCount[j][i]);
		}
		TRACE("\n");
	}
#endif
	
//	strTemp.Format("%d 개", nInputCell);
//	m_DataGrid.SetValueRange(CGXRange(1,3), strTemp);
//	strTemp.Format("정상 Cell  =  %d개", nNormalCell);
//	m_DataGrid.SetValueRange(CGXRange(1, m_Half+3),strTemp);

	return TRUE;
}

void CDataDlg::GraphUpdate()
{
	//Fail Grid의 내용을 그래프로 그린다.
	
	int col = m_ErrorGrid.GetRowCount()-1;		//전체 수율 제외 
	PEnset(m_hWndPE, PEP_nPOINTS,  col);
	float data;
	CString str;
	PEszset(m_hWndPE, PEP_szMAINTITLE, (LPSTR)(LPCTSTR)m_strTitle);
	for(int i =0; i<col; i++)
	{
		str = m_ErrorGrid.GetValueRowCol(i+1, 1);
		PEvsetcellEx(m_hWndPE, PEP_szaPOINTLABELS, 0, i, (LPSTR)(LPCTSTR)str);//(void FAR*)(const char*)strTemp);
		str = m_ErrorGrid.GetValueRowCol(i+1, 2);
		data = (float)atof(str);
		PEvsetcellEx(m_hWndPE, PEP_faYDATA, 0, i, &data);
//		TRACE("Graph Point to %d: %s, %f\n", i, str, data);
	}
	PEreinitialize(m_hWndPE);
	PEresetimage(m_hWndPE, 0, 0);
}

void CDataDlg::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	m_Module.Close();
	m_Test.Close();
	m_ChValue.Close();
	m_ChResult.Close();
	
	PEdestroy(m_hWndPE);

	CDialog::OnClose();
}

BOOL CDataDlg::UpdateDisplayData(int nTotInput, int nNormal)
{
	CString str;
	str.Format(TEXT_LANG[16], nTotInput, nNormal, nTotInput-nNormal);//"입력 %d개  / 정상 %d개  /  불량 %d개"
	m_DataGrid.SetValueRange(CGXRange(1,3), str);

	int col = 0;
	for(int i = 0; i < m_nFieldNo; i++)
	{
   		col = i + 2;
		switch(m_Field[i].DataType)
		{
    	case EP_VOLTAGE:
    		str.Format("%.3f", m_dataField[i].dAve);
	    	m_DataGrid.SetValueRange(CGXRange(4, col), str);
			str.Format("%.3f", m_dataField[i].dMax); 
    	   	m_DataGrid.SetValueRange(CGXRange(5, col), str);
    		str.Format("%.3f", m_dataField[i].dMin);
        	m_DataGrid.SetValueRange(CGXRange(6, col), str);
			str.Format("%.4f", m_dataField[i].dSTD);
    	   	m_DataGrid.SetValueRange(CGXRange(7, col), str);
		break;
    	
		case EP_CURRENT:
			str.Format("%.1f", m_dataField[i].dAve);
			m_DataGrid.SetValueRange(CGXRange(4, col), str);
			str.Format("%.1f", m_dataField[i].dMax);
   	    	m_DataGrid.SetValueRange(CGXRange(5, col), str);
		    str.Format("%.1f", m_dataField[i].dMin);
   	        m_DataGrid.SetValueRange(CGXRange(6, col), str);
		    str.Format("%.2f", m_dataField[i].dSTD);
   	    	m_DataGrid.SetValueRange(CGXRange(7, col), str);
			break;
    	case EP_CAPACITY:
    	case EP_IMPEDANCE:
			str.Format("%.1f", m_dataField[i].dAve);
			m_DataGrid.SetValueRange(CGXRange(4, col), str);
		   	str.Format("%.1f", m_dataField[i].dMax);
   			m_DataGrid.SetValueRange(CGXRange(5, col), str);
		    str.Format("%.1f", m_dataField[i].dMin);
   		    m_DataGrid.SetValueRange(CGXRange(6, col), str);
		    str.Format("%.2f", m_dataField[i].dSTD);
   		    m_DataGrid.SetValueRange(CGXRange(7, col), str);
			break;

   		case EP_STEP_TIME:
    	case EP_TOT_TIME:
			str = m_pView->Time((float)m_dataField[i].dAve);
			m_DataGrid.SetValueRange(CGXRange(4, col), str);
	    	str = m_pView->Time((float)m_dataField[i].dMax);
   	    	m_DataGrid.SetValueRange(CGXRange(5, col), str);
	    	str = m_pView->Time((float)m_dataField[i].dMin);
   	    	m_DataGrid.SetValueRange(CGXRange(6, col), str);
	    	str = m_pView->Time((float)m_dataField[i].dSTD);
   	    	m_DataGrid.SetValueRange(CGXRange(7, col), str);
			break;
		case EP_CH_CODE:
		case EP_GRADE_CODE:
			str = "";
			m_DataGrid.SetValueRange(CGXRange(4, col, 7, col), str);
			break;

    	default:
 	    	str.Format("%.3f", m_dataField[i].dAve);
	    	m_DataGrid.SetValueRange(CGXRange(4, col), str);
	    	str.Format("%.3f", m_dataField[i].dMax);
   	    	m_DataGrid.SetValueRange(CGXRange(5, col), str);
	    	str.Format("%.3f", m_dataField[i].dMin);
   	    	m_DataGrid.SetValueRange(CGXRange(6, col), str);
	    	str.Format("%.4f", m_dataField[i].dSTD);
   	    	m_DataGrid.SetValueRange(CGXRange(7, col), str);
			break;
		}
   	    m_DataGrid.SetValueRange(CGXRange(8, col), m_dataField[i].nCellNo);
	}

	return TRUE;
}

void CDataDlg::UpdateFailGrid()
{
	int col = m_ErrorGrid.GetRowCount();
	CString str;
	float fFailCell =(float)(m_nTotInput - m_nNormalcell);
	
	if(col > 0)
	{
		m_ErrorGrid.RemoveRows(1, col);
	}
	
	int nTempCount = 0;
	
	//검사 전압 미달 표시
	col = 1;
	m_ErrorGrid.InsertRows(col, 1);
	m_ErrorGrid.SetValueRange(CGXRange(col, 1), m_pDoc->ChCodeMsg(EP_CODE_CHECK_VOLTAGE_LOW));
	m_ErrorGrid.SetValueRange(CGXRange(col, 2), m_Fault[EP_CODE_CHECK_VOLTAGE_LOW]);
	if(fFailCell > 0.0f)
		str.Format("%.1f%%", (float)m_Fault[EP_CODE_CHECK_VOLTAGE_LOW]/fFailCell * 100.0f);
	else
		str = "0.0%";

	m_ErrorGrid.SetValueRange(CGXRange(col, 3), str);
	if(m_nTotInput > 0)
		str.Format("%.1f%%", (float)nTempCount/(float)m_nTotInput * 100.0f);
	else
		str = "0.0%";
	m_ErrorGrid.SetValueRange(CGXRange(col, 4), str);

	//Cell Check 오류 표시 
	col++;
	m_ErrorGrid.InsertRows(col, 1);
	m_ErrorGrid.SetValueRange(CGXRange(col, 1), m_pDoc->ChCodeMsg(EP_CODE_CELL_CHECK_FAIL));
	nTempCount = m_Fault[EP_CODE_CELL_CHECK_FAIL] - m_Fault[EP_CODE_CHECK_VOLTAGE_LOW];
	m_ErrorGrid.SetValueRange(CGXRange(col, 2), (ROWCOL)nTempCount);
	if(fFailCell > 0.0f)
		str.Format("%.1f%%", (float)nTempCount/fFailCell * 100.0f);
	else
		str = "0.0%";

	m_ErrorGrid.SetValueRange(CGXRange(col, 3), str);
	if(m_nTotInput > 0)
		str.Format("%.1f%%", (float)nTempCount/(float)m_nTotInput * 100.0f);
	else
		str = "0.0%";
	m_ErrorGrid.SetValueRange(CGXRange(col, 4), str);

	for(BYTE i = EP_CELL_FAIL_CODE_LOW; i <= EP_CELL_FAIL_CODE_HIGH; i++)
	{
		if(m_Fault[i] >0)		//불량이 있는 것만 표시 
		{
			col++;
			m_ErrorGrid.InsertRows(col, 1);
			m_ErrorGrid.SetValueRange(CGXRange(col, 1), m_pDoc->ChCodeMsg(i));
			m_ErrorGrid.SetValueRange(CGXRange(col, 2), m_Fault[i]);
			if(fFailCell > 0.0f)
				str.Format("%.1f%%", (float)m_Fault[i]/fFailCell * 100.0f);
			else
				str = "0.0%";
			m_ErrorGrid.SetValueRange(CGXRange(col, 3), str);
			if(m_nTotInput > 0)
				str.Format("%.1f%%", (float)m_Fault[i]/(float)m_nTotInput * 100.0f);
			else
				str = "0.0%";
			m_ErrorGrid.SetValueRange(CGXRange(col, 4), str);
		}
	}
	
	col++;
	m_ErrorGrid.InsertRows(col, 1);
	m_ErrorGrid.SetCoveredCellsRowCol(col, 2, col, 4);
	m_ErrorGrid.SetValueRange(CGXRange(col, 1), TEXT_LANG[17]);//"전체 수율"
	
	if(m_nTotInput > 0)
		str.Format("%.1f%%", (float)m_nNormalcell/(float)m_nTotInput * 100.0f);
	else
		str = "0.0%";
	
	m_ErrorGrid.SetValueRange(CGXRange(col, 2), str);
}


void CDataDlg::OnSaveFile() 
{
	// TODO: Add your control notification handler code here
	CString strFileName;
	strFileName.Format("%s.csv", m_strTitle);
		
	CFileDialog pDlg(FALSE, "", strFileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "csv 파일(*.csv)|*.csv|");

	if(IDOK == pDlg.DoModal())
	{
		strFileName = pDlg.GetPathName();
		if(strFileName.IsEmpty())	return ;

		FILE *fp = fopen(strFileName, "wt");
		if(fp == NULL)	return ;

		BeginWaitCursor();
		int row = 0;

		fprintf(fp, TEXT_LANG[18]+":, %s\n", m_strTitle);	// FileName	//"검색 범위"
		fprintf(fp, "\n");
		
		fprintf(fp, TEXT_LANG[19]+"\n");	// FileName		//"Cell 수량 통계"
		for(row = 1; row<m_DataGrid.GetRowCount()+1; row++)
		{
			for(int col = 1; col<m_DataGrid.GetColCount()+1; col++)
			{
				fprintf(fp, "%s,", m_DataGrid.GetValueRowCol(row, col));
			}
			fprintf(fp, "\n");
		}

		fprintf(fp, "\n");
		fprintf(fp, TEXT_LANG[20]+"\n");	// FileName	//"불량 수량 통계"
		for(row = 0; row<m_ErrorGrid.GetRowCount()+1; row++)
		{
			for(int col = 1; col<m_ErrorGrid.GetColCount()+1; col++)
			{
				fprintf(fp, "%s,", m_ErrorGrid.GetValueRowCol(row, col));
			}
			fprintf(fp, "\n");
		}
		fclose(fp);
		EndWaitCursor();
	}		

}

void CDataDlg::OnSelchangeProcSelect() 
{
	// TODO: Add your control notification handler code here
	
}
