// UserAdminDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSEditor.h"
#include "UserAdminDlg.h"

#include "ChangeUserInfoDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUserAdminDlg dialog


CUserAdminDlg::CUserAdminDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUserAdminDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CUserAdminDlg)
	m_bUseLoginCheck = FALSE;
	//}}AFX_DATA_INIT
	m_pLoginData = NULL;
	LanguageinitMonConfig();
}

CUserAdminDlg::~CUserAdminDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CUserAdminDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CUserAdminDlg"), _T("TEXT_CUserAdminDlg_CNT"), _T("TEXT_CUserAdminDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CUserAdminDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CUserAdminDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}


void CUserAdminDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CUserAdminDlg)
	DDX_Control(pDX, IDC_COUNT_LABEL, m_ctrlCount);
	DDX_Check(pDX, IDC_USE_LOGIN_CHECK, m_bUseLoginCheck);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CUserAdminDlg, CDialog)
	//{{AFX_MSG_MAP(CUserAdminDlg)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_ADD_USER, OnAddUser)
	ON_BN_CLICKED(IDC_DELETE_USER, OnDeleteUser)
	ON_BN_CLICKED(IDOK, OnOk)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_GRID_DOUBLECLICK, OnGridDoubleClick)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUserAdminDlg message handlers

BOOL CUserAdminDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	ASSERT(	m_pLoginData );
	
	// TODO: Add extra initialization here
	InitUserListGrid();
	
//	m_recordSet.m_strFilter.Format("UserID = '%s' and Password = '%s'", m_strLoginID, m_strPassword);
	m_recordSet.m_strSort.Format("[UserID]");

	try
	{
		m_recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}

	RequeryUserDB();

	m_bUseLoginCheck = 	AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "UseLogin", 1);

	if((m_pLoginData->nPermission & PMS_USER_SETTING_CHANGE) == FALSE)
	{
		GetDlgItem(IDC_ADD_USER)->EnableWindow(FALSE);
		GetDlgItem(IDC_DELETE_USER)->EnableWindow(FALSE);
	}

	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CUserAdminDlg::InitUserListGrid()
{
	m_UserListGrid.SubclassDlgItem(IDC_USER_LIST_GIRD, this);
	m_UserListGrid.Initialize();
	m_UserListGrid.SetRowCount(10);
	m_UserListGrid.SetColCount(8);

	m_UserListGrid.SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetControl(GX_IDS_CTRL_PASSWORD));

	//Enable Tooltips
	CRect rect;
	m_UserListGrid.GetClientRect(&rect);
	float width = rect.Width()/100.0f;
	m_UserListGrid.SetColWidth(0, 0, (int)(width*5.0f));
	m_UserListGrid.SetColWidth(1, 1, (int)(width*10.0f));
	m_UserListGrid.SetColWidth(2, 2, (int)(width*10.0f));
	m_UserListGrid.SetColWidth(3, 3, (int)(width*15.0f));
	m_UserListGrid.SetColWidth(4, 4, (int)(width*10.0f));
	m_UserListGrid.SetColWidth(5, 5, 0);
	m_UserListGrid.SetColWidth(6, 6, 0);
	m_UserListGrid.SetColWidth(7, 7, (int)(width*25.0f));
	m_UserListGrid.SetColWidth(8, 8, (int)(width*30.0f));

	//Table setting
	m_UserListGrid.GetParam()->EnableTrackColWidth(TRUE);	//no Resize Col Width
//	m_UserListGrid.SetStyleRange(CGXRange().SetTable(),	CGXStyle().SetFont(CGXFont().SetSize(9).SetFaceName("굴림")));
	m_UserListGrid.SetStyleRange(CGXRange().SetCols(1), CGXStyle().SetInterior(RGB(192,192,192)));

	m_UserListGrid.SetStyleRange(CGXRange().SetCols(5),
		CGXStyle()
			.SetControl(GX_IDS_CTRL_CHECKBOX3D)
			.SetHorizontalAlignment(DT_CENTER)
	);

	//Table Name
	m_UserListGrid.SetStyleRange(CGXRange(0, 0), CGXStyle().SetValue("No"));
	m_UserListGrid.SetStyleRange(CGXRange(0, 1), CGXStyle().SetValue("ID"));
	m_UserListGrid.SetStyleRange(CGXRange(0, 2), CGXStyle().SetValue("Password"));
	m_UserListGrid.SetStyleRange(CGXRange(0, 3), CGXStyle().SetValue(TEXT_LANG[0]));//"이름"
	m_UserListGrid.SetStyleRange(CGXRange(0, 4), CGXStyle().SetValue(TEXT_LANG[1]));//"권한"
	m_UserListGrid.SetStyleRange(CGXRange(0, 5), CGXStyle().SetValue(TEXT_LANG[2]));//"자동Logout"
	m_UserListGrid.SetStyleRange(CGXRange(0, 6), CGXStyle().SetValue(TEXT_LANG[3]));//"자동Logout 대기시간"
	m_UserListGrid.SetStyleRange(CGXRange(0, 7), CGXStyle().SetValue(TEXT_LANG[4]));//"등록일"
	m_UserListGrid.SetStyleRange(CGXRange(0, 8), CGXStyle().SetValue(TEXT_LANG[0]));//"이름"
	return TRUE;
}

BOOL CUserAdminDlg::RequeryUserDB()
{
	int nCount = m_UserListGrid.GetRowCount();
	if(nCount >0)	m_UserListGrid.RemoveRows(1, nCount);

	m_recordSet.m_strFilter.Format("");
	m_recordSet.Requery();

	nCount = 0 ;
	while(!m_recordSet.IsEOF())
	{
		nCount++;
		m_UserListGrid.InsertRows(nCount, 1);
//		m_UserListGrid.SetValueRange(CGXRange(nCount, 1), (long)nCount);
		m_UserListGrid.SetValueRange(CGXRange(nCount, 1), m_recordSet.m_UserID);
		m_UserListGrid.SetValueRange(CGXRange(nCount, 2), m_recordSet.m_Password);
		m_UserListGrid.SetValueRange(CGXRange(nCount, 3), m_recordSet.m_Name);
		m_UserListGrid.SetValueRange(CGXRange(nCount, 4), m_recordSet.m_Authority);
		m_UserListGrid.SetValueRange(CGXRange(nCount, 5), (long)m_recordSet.m_AutoLogOut);
		m_UserListGrid.SetValueRange(CGXRange(nCount, 6), m_recordSet.m_AutoLogOutTime);
		m_UserListGrid.SetValueRange(CGXRange(nCount, 7), m_recordSet.m_RegistedDate.Format());
		m_UserListGrid.SetValueRange(CGXRange(nCount, 8), m_recordSet.m_Description);
		m_recordSet.MoveNext();
	}
	CString strTemp;
	strTemp.Format(TEXT_LANG[5], nCount);//"현재 등록된 사용자는 %d명 입니다."
	m_ctrlCount.SetText( strTemp );
	return TRUE;
}


void CUserAdminDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here
	if(m_recordSet.IsOpen())
		m_recordSet.Close();
}

void CUserAdminDlg::OnAddUser() 
{
	// TODO: Add your control notification handler code here
	CChangeUserInfoDlg *pDlg;
	pDlg = new CChangeUserInfoDlg(m_pLoginData, NULL, this);
	ASSERT(pDlg);
	if(pDlg->DoModal() ==IDOK)
	{
		STR_LOGIN	userData;
		memcpy(&userData, pDlg->GetUserInfo(), sizeof(userData));

		long nCount = m_UserListGrid.GetRowCount()+1;
		m_UserListGrid.InsertRows(nCount, 1);

//		m_UserListGrid.SetValueRange(CGXRange(nCount, 1), nCount);
		m_UserListGrid.SetValueRange(CGXRange(nCount, 1), userData.szLoginID);
		m_UserListGrid.SetValueRange(CGXRange(nCount, 2), userData.szPassword);
		m_UserListGrid.SetValueRange(CGXRange(nCount, 3), userData.szUserName);
		m_UserListGrid.SetValueRange(CGXRange(nCount, 4), userData.nPermission);
		m_UserListGrid.SetValueRange(CGXRange(nCount, 5), (long)userData.bUseAutoLogOut);
		m_UserListGrid.SetValueRange(CGXRange(nCount, 6), userData.lAutoLogOutTime);
		m_UserListGrid.SetValueRange(CGXRange(nCount, 7), userData.szRegistedDate);
		m_UserListGrid.SetValueRange(CGXRange(nCount, 8), userData.szDescription);
	}
	delete pDlg;
	pDlg = NULL;
}

BOOL CUserAdminDlg::SearchUser(CString strUserID, BOOL bPassWordCheck, CString strPassword, STR_LOGIN *pUserData)
{
	CUserRecordSet rs;
	
	rs.m_strFilter.Format("[UserID] = '%s'", strUserID);

	try
	{
		rs.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}

	if(rs.IsBOF() || rs.IsEOF())
	{
		rs.Close();
		return FALSE;
	}

	if(bPassWordCheck)
	{
		if(rs.m_Password != strPassword)
		{
			rs.Close();
			return FALSE;
		}
	}

	if(pUserData != NULL)
	{
		CString strTemp;
		pUserData->nPermission = rs.m_Authority;
		sprintf(pUserData->szDescription, rs.m_Description.operator const char*());
		sprintf(pUserData->szUserName, rs.m_Name.operator const char*());
		sprintf(pUserData->szPassword, rs.m_Password.operator const char*());
		strTemp = rs.m_RegistedDate.Format();
		sprintf(pUserData->szRegistedDate, strTemp.operator const char*());
		sprintf(pUserData->szLoginID, rs.m_UserID.operator const char*());
		pUserData->bUseAutoLogOut = rs.m_AutoLogOut;
		pUserData->lAutoLogOutTime = rs.m_AutoLogOutTime;
	}
	rs.Close();
	return TRUE;
}

void CUserAdminDlg::OnDeleteUser() 
{
	// TODO: Add your control notification handler code here
	int nTotRow = m_UserListGrid.GetRowCount();
	if(nTotRow <=0 )	return;

	CRowColArray	awRows;
	CString strTemp;

	m_UserListGrid.GetSelectedRows(awRows);
	int nSelNo = awRows.GetSize();
	
	if(nSelNo == 1)
	{
		if(awRows[0] >0 && awRows[0] <= nTotRow)
		{
			strTemp = m_UserListGrid.GetValueRowCol(awRows[0], 1);
			strTemp.Format(TEXT_LANG[6], strTemp);//"사용자 %s를 삭제 하시겠습니까?"
		}
		else
			return;
	}
	else
	{
		strTemp.Format(TEXT_LANG[7], nSelNo);//"선택한 %d명의 사용자를 삭제 하시겠습니까?"
	}
	
	if(MessageBox(strTemp, "User Delete", MB_ICONQUESTION|MB_YESNO) != IDYES)		return;

	for (int i = nSelNo-1; i>= 0; i--)
	{
		if(awRows[i] >0 && awRows[i] <= nTotRow)
		{
			strTemp.Format("%s", m_pLoginData->szLoginID);
			
			if(strTemp == m_UserListGrid.GetValueRowCol(awRows[i], 1))
			{
				strTemp.Format(TEXT_LANG[8], m_UserListGrid.GetValueRowCol(awRows[i], 1));//"%s는 현재 사용자 입니다. 삭제 하시면 다음부터는 Login 할 수 없습니다.  삭제 하시겠습니까?"
				if(AfxMessageBox(strTemp, MB_ICONQUESTION|MB_YESNO) == IDNO)
					continue;
			}

/*			CString aa;
			CString bb;
			aa = m_UserListGrid.GetValueRowCol(awRows[i], 1);
			bb = m_UserListGrid.GetValueRowCol(awRows[i], 2);
*/	
			m_recordSet.m_strFilter.Format("UserID = '%s' and Password = '%s'", 
							m_UserListGrid.GetValueRowCol(awRows[i], 1), 
							m_UserListGrid.GetValueRowCol(awRows[i], 2));
			m_recordSet.Requery();
			if(!m_recordSet.IsBOF() && !m_recordSet.IsEOF())
				m_recordSet.Delete();
		
			m_UserListGrid.RemoveRows(awRows[i], awRows[i]);
		}
	}

	strTemp.Format(TEXT_LANG[5], m_UserListGrid.GetRowCount());//"현재 등록된 사용자는 %d명 입니다."
	m_ctrlCount.SetText( strTemp );
}

void CUserAdminDlg::OnOk() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION, "UseLogin", m_bUseLoginCheck);
	CDialog::OnOK();
}

LONG CUserAdminDlg::OnGridDoubleClick(WPARAM wParam, LPARAM lParam)
{
	ROWCOL nRow, nCol;
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;
	nCol = LOWORD(wParam);
	nRow = HIWORD(wParam);
	
	CString strID, strPWD;
	STR_LOGIN	userData;
	strID = pGrid->GetValueRowCol(nRow, 1);
	strPWD = pGrid->GetValueRowCol(nRow, 2);
	if(SearchUser(strID, TRUE, strPWD, &userData) == FALSE)
	{
		AfxMessageBox(TEXT_LANG[9]);//"사용자를 찾을 수 없습니다."
		return 0;
	}

	BOOL	bCurrentUserFlag = FALSE;
	CChangeUserInfoDlg *pDlg;
	pDlg = new CChangeUserInfoDlg(m_pLoginData, &userData, this);
	ASSERT(pDlg);

	pDlg->m_bNewUser = FALSE;
	if((strcmp(m_pLoginData->szLoginID, userData.szLoginID) == 0) 
		 && (strcmp(m_pLoginData->szPassword, userData.szPassword) == 0))
	{
		//자기 자신은 변경 가능 
		bCurrentUserFlag = TRUE;		//Current Logined ID Changed
	}

	if(pDlg->DoModal() == IDOK)
	{
		memcpy(&userData, pDlg->GetUserInfo(), sizeof(userData));

		//현재 사용자 정보가 변경되었을 경우
		if(bCurrentUserFlag)
		{
			memcpy(m_pLoginData, &userData, sizeof(userData));

			//최종 Login 정보 Update
			UINT nSize = sizeof(STR_LOGIN);
			LPCTSTR lpData = (LPCTSTR)m_pLoginData;
			AfxGetApp()->WriteProfileBinary(EDITOR_REG_SECTION, "LastLogin", (BYTE *)lpData, nSize);
		}
		
		m_UserListGrid.SetValueRange(CGXRange(nRow, 1), userData.szLoginID);
		m_UserListGrid.SetValueRange(CGXRange(nRow, 2), userData.szPassword);
		m_UserListGrid.SetValueRange(CGXRange(nRow, 3), userData.szUserName);
		m_UserListGrid.SetValueRange(CGXRange(nRow, 4), userData.nPermission);
		m_UserListGrid.SetValueRange(CGXRange(nRow, 5), (long)userData.bUseAutoLogOut);
		m_UserListGrid.SetValueRange(CGXRange(nRow, 6), userData.lAutoLogOutTime);
		m_UserListGrid.SetValueRange(CGXRange(nRow, 7), userData.szRegistedDate);
		m_UserListGrid.SetValueRange(CGXRange(nRow, 8), userData.szDescription);
	}
	delete pDlg;
	pDlg = NULL;
	return 0;
	
}