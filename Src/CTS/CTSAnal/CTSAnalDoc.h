// CTSAnalDoc.h : interface of the CCTSAnalDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CTSAnalDOC_H__22AF37A5_1B3F_44E8_AD53_64B3D5589430__INCLUDED_)
#define AFX_CTSAnalDOC_H__22AF37A5_1B3F_44E8_AD53_64B3D5589430__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "parsingdata.h"
#include "mydefine.h"
//#include "FormResultFile.h"
//#include "../../../src/Utility/FormResultFile.h"

//class CProgressWnd;
class CCTSAnalDoc : public CDocument
{
protected: // create from serialization only
	CCTSAnalDoc();
	DECLARE_DYNCREATE(CCTSAnalDoc)

// Attributes
public:
	FIELD                    m_field[FIELDNO];
//	CReportDataListRecordSet m_ReportDataList;
	int                      m_TotCol;            //�� field ��

// Operations
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	void UpdateUnitSetting();
	
	int GetStepTypeID(char *lpStr);
	INT GetFieldAttribute(char *lpStr);
	int OnNewFileOpen(int nIndex);
	void SetFieldData(int nIndex, char *lpStr, int nItemIndex, int nItemLine);
	int ParsingData(char *pDest, int nIndex);
	int ParsingItem(char *pDest);
	CString GetFieldData(int nItemNum, int nFieldNum);
	CString GetStepType(int nID);

//==========================File Version 6============================//
	int m_nTrayColSize;
	CPtrArray m_apProcType;
	CString m_strDataBaseName;
	CString m_strLogDataBaseName;
	
	CString GetProcTypeName(int nType, int nStepType);
	CString StepEndString(STR_COMMON_STEP *pStep);
	BOOL RequeryProcType();

//==========================BTSGraph==================================//
#ifdef	_DEBUG
	FILE* f;
#endif//_DEBUG
	
	int m_nFileMode;
	int m_nXItem;
	int m_nMaxDataPoint;
	CString m_strConfigFileName;
	BOOL SaveGraphSetting();
	BOOL LoadGraphSetting();
	BYTE m_nMaxDataFileIndex;
	float *m_pData;
	CDataFormat* m_pParsingData;

	void RemoveFileData(int nFileCount = 0);
	int GetFileInfoDataA(int nIndex);
	int LoadFileDataA(BOOL bLoadFile, CString strFileName);
	int LoadFileData(BOOL bLoadFile, CString* strFileName, int nLoadNum);
	CString ValueString(double dData, int item, BOOL bUnit = FALSE);
	
	int m_nLoadedFileNum;
	STR_Y_AXIS_SET * GetAxisYSetting();
	COLORREF m_ChColor[MAX_CHANNEL_NUM];
	STR_Y_AXIS_SET	m_YSet[MAX_MUTI_AXIS];
	STR_GRID_SET	m_GraphLineSet;
	STR_Y_AXIS_SET	m_SelectedYSet[MAX_SEL_AXIS];
	STR_FILE_INFO_A	*m_pFileInfo;

	int GetGeneralResultData(int nIndex, float fInterVal, int nDataNo);
	int GetGeneralResultDataA(float fInterVal, int nDataNo);
	int	GetCapacityResultData(int nIndex,int nDataNo);
	int GetAxisCount();
	UINT m_nXMaxReadPoint;
	float m_fSecPerPoint;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCTSAnalDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual void OnCloseDocument();
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	//}}AFX_VIRTUAL

// Implementation
public:
	BOOL ExecuteGraphAnalyzer(CString strFileName);
	BOOL ShowLotAllData(CString strLot, CStringArray &aFile);
	BOOL ShowCpCpkData(CString strLot, CStringArray &aFile);
	CString GetTypeMsg(WORD State, BYTE &colorFlag);
	BOOL m_bHideNonCellData;
	STR_GRADE_COLOR_CONFIG m_gradeColorConfig;
	COLORREF GetGradeColor(CString strCode);
	BOOL SetDataField();
	BOOL ExecuteProgram(CString strProgram, CString strArgument = "", CString strClassName = "", CString strWindowName = "", BOOL bNewExe = FALSE, 	BOOL bWait = FALSE);
	BOOL DownLoadProfileData(CString strFileName);
	BOOL m_bUseRackIndex;
	int m_nModulePerRack;
	BOOL LoadSetting();
	BOOL LoadFieldList();
	BOOL m_bLoadField;
	BOOL m_bLoadCodeDB;
	STR_TOP_CONFIG m_TopConfig;
	CProgressWnd *m_pProgressWnd;
	void InitProgressWnd();
	BOOL m_bProgressInit;
	void SetProgressWnd(UINT nMin, UINT nMax, CString m_strTitle);
	void SetProgressPos(int nPos);
	void HideProgressWnd();	
	CString m_strCurFolder;
	CString m_strDataFolder;
	BOOL LoadCodeTable();
	CPtrArray m_apChCodeMsg;
	CString ChCodeMsg(WORD code, BOOL bDetail = FALSE);
	CString GetStateMsg(WORD State, BYTE &colorFlag);
//	CString StepEndString(LPVOID lpStep, int nType);
	CString ModeTypeMsg(int nMode);
	CString StepTypeMsg(int nType);
	BOOL	RemoveCondition(STR_CONDITION *pCondition);
	CString m_strModuleName;
	CString m_strGroupName;
	int ReadTestConditionFile(FILE *fp, STR_CONDITION *pCondition, char *fileID = NULL, char *fileVer = NULL);
	CString ModuleName(int nModuleID, int nGroupIndex = -1);
	STR_TOP_CONFIG * GetTopConfig();
	BOOL m_bUseGroupSet;
	virtual ~CCTSAnalDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	int m_nTimeUnit;
	CString m_strVUnit;
	int m_nVDecimal ;
	CString m_strIUnit;
	int m_nIDecimal;
	CString m_strCUnit;
	int m_nCDecimal;

protected:

// Generated message map functions
protected:


	//{{AFX_MSG(CCTSAnalDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CTSAnalDOC_H__22AF37A5_1B3F_44E8_AD53_64B3D5589430__INCLUDED_)
