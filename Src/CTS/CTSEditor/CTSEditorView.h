// CTSEditorView.h : interface of the CCTSEditorView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CTSEditorVIEW_H__1B7323B9_7330_4BD6_80E7_6B809D6197A2__INCLUDED_)
#define AFX_CTSEditorVIEW_H__1B7323B9_7330_4BD6_80E7_6B809D6197A2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "CTSEditorDoc.h"
#include "MyGridWnd.h"
#include "GridComboBox.h"
#include "RealEditCtrl.h"
#include "DateTimeCtrl.h"
#include "ModelSelDlg.h"
#include "afxwin.h"

//Grid Column 목록 
#define COL_MODEL_NO			0
#define COL_MODEL_NAME			1
#define COL_MODEL_CREATOR		2
#define COL_MODEL_DESCRIPTION	3
#define COL_MODEL_PRIMARY_KEY	4
#define COL_MODEL_EDIT_STATE	5

#define COL_TEST_NO				0
#define COL_TEST_PROC_TYPE		1
#define COL_TEST_RESET_PROC		2
#define COL_TEST_NAME			3
#define COL_TEST_CREATOR		4
#define COL_TEST_DESCRIPTION	5
#define COL_TEST_EDIT_TIME		6
#define COL_TEST_PRIMARY_KEY	7
#define COL_TEST_EDIT_STATE		8

#define COL_STEP_NO				0
#define COL_STEP_CYCLE			1
#define COL_STEP_PROCTYPE		2
#define COL_STEP_TYPE			3
#define COL_STEP_MODE			4
#define COL_STEP_REF_V			5
#define COL_STEP_REF_I			6
#define COL_STEP_REF_T			7
#define COL_STEP_END			8
#define COL_STEP_PREIMARY_KEY	9
#define COL_STEP_INDEX			10
#define COL_STEP_REF_P			11		// 파워
#define COL_STEP_END_V			12		// 종료 전압
#define COL_STEP_END_I			13		// 종료 전류
#define COL_STEP_END_DELTAVP	14		// 종료DeltaVp
#define COL_STEP_END_DAY		15		// 종료 시간
#define COL_STEP_END_TIME		16
#define COL_STEP_END_CAP		17		// 종료 용량
#define COL_STEP_END_TEMP		18		// 종료 온도
#define COL_STEP_END_SOC		19		// 종료 SOC

#define TAB_SAFT_VAL		0
#define TAB_RECORD_VAL		1
#define TAB_GRADE_VAL		2

#define CS_SAVED		"1"
#define CS_NEW			"2"
#define CS_EDIT			"3"


class CCTSEditorView : public CFormView
{
protected: // create from serialization only
	CCTSEditorView();
	DECLARE_DYNCREATE(CCTSEditorView)

public:
	//{{AFX_DATA(CCTSEditorView)
	enum { IDD = IDD_CTSEditor_FORM };
	CDateTimeCtrl	m_DeltaTime;
	CDateTimeCtrl	m_ReportTime;
	CTabCtrl	m_ctrlParamTab;
	CLabel	m_TotalStepCount;
	CLabel	m_TestNameLabel;
	CLabel	m_TotTestCount;
	CLabel	m_TotModelCount;
	CButton	m_PreTestCheck;
	CButton	m_GradeCheck;
	CLabel	m_strStepNumber;
	CLabel	m_strTestName;
	BOOL	m_bExtOption;	
	int		m_nCheckTime;
	CLabel	m_strWarning;
	//}}AFX_DATA

// Attributes
public:
	
private:
	enum {_COL_ITEM1_=1,_COL_GRADE_MIN1_ = 2, _COL_GRADE_MAX1_ = 3,_COL_GRADE_RELATION_=4, _COL_ITEM2_=5,_COL_GRADE_MIN2_ = 6, _COL_GRADE_MAX2_ = 7, _COL_GRADE_CODE_ = 8};
	CToolTipCtrl m_tooltip;

	CMyGridWnd m_wndStepGrid;
	CMyGridWnd m_wndTestListGrid;
	CMyGridWnd m_wndBatteryModelGrid;
	CMyGridWnd *m_pWndCurModelGrid;
	CMyGridWnd m_wndGradeGrid;

	CCTSEditorDoc* GetDocument();

	//Grid의 selection Combo
	CGridComboBox *m_pTestTypeCombo;
	CGridComboBox *m_pProcTypeCombo;
	CGridComboBox *m_pStepTypeCombo;
	CGridComboBox *m_pStepModeCombo;

	//Grade의 selection Combo
	CGridComboBox *m_pGradeItem1Combo;
	CGridComboBox *m_pGradeItem2Combo;
	CGridComboBox *m_pGradeRelationTypeCombo;

	//모델 조작 버튼
	stingray::foundation::SECBitmapButton m_btnModelNew;
	stingray::foundation::SECBitmapButton m_btnModelDelete;
	stingray::foundation::SECBitmapButton m_btnModelSave;
	stingray::foundation::SECBitmapButton m_btnModelCopy;
	stingray::foundation::SECBitmapButton m_btnModelEdit;

	//공정 조작 버튼
	stingray::foundation::SECBitmapButton m_btnProcNew;
	stingray::foundation::SECBitmapButton m_btnProcDelete;
	stingray::foundation::SECBitmapButton m_btnProcSave;
	stingray::foundation::SECBitmapButton m_btnProcEdit;
	stingray::foundation::SECBitmapButton m_btnProcCopy;
	
	//Step 조작 버튼
	stingray::foundation::SECBitmapButton m_btnStepNew;
	stingray::foundation::SECBitmapButton m_btnStepDelete;
	stingray::foundation::SECBitmapButton m_btnStepSave;

	stingray::foundation::SECBitmapButton m_btnLoad;
	stingray::foundation::SECBitmapButton m_btnModel;

	//Cell check 조건
	SECCurrencyEdit	m_PreMaxV;
	SECCurrencyEdit	m_PreMinV;
	SECCurrencyEdit	m_PreMaxI;
	SECCurrencyEdit	m_PreOCV;
	SECCurrencyEdit	m_TrickleI;
	SECCurrencyEdit	m_PreDeltaV;
	SECCurrencyEdit	m_PreReverseV;
	SECCurrencyEdit m_PreResistance;
	SECCurrencyEdit	m_PreContactUpperV;
	SECCurrencyEdit	m_PreContactLowerV;
	//20210223 KSJ
	SECCurrencyEdit	m_PreContactLimitV;
	SECCurrencyEdit	m_PreContactUpperI;
	SECCurrencyEdit	m_PreContactLowerI;
	SECCurrencyEdit	m_PreContactDeltaLimitV;

	//	CMyDateTimeCtrl	m_TrickleTime;
//	SECCurrencyEdit	m_FaultNo;


	//안전 조건 
	SECCurrencyEdit	m_VtgHigh;
	SECCurrencyEdit	m_VtgLow;
	SECCurrencyEdit	m_CrtHigh;
	SECCurrencyEdit	m_CrtLow;
	SECCurrencyEdit	m_CapHigh;
	SECCurrencyEdit	m_CapLow;
	SECCurrencyEdit	m_ImpHigh;
	SECCurrencyEdit	m_ImpLow;
	SECCurrencyEdit	m_TempHigh;
	SECCurrencyEdit	m_TempLow;
	SECCurrencyEdit	m_DeltaV;
	SECCurrencyEdit m_DeltaI;
	
//	CMyDateTimeCtrl	m_DeltaTime;

	//종지저류 검사
	SECCurrencyEdit	m_VtgEndHigh;
	SECCurrencyEdit	m_VtgEndLow;
	SECCurrencyEdit	m_CrtEndHigh;
	SECCurrencyEdit	m_CrtEndLow;

	//EDLC cap 측정 전압 
	SECCurrencyEdit	m_CapVtgLow;
	SECCurrencyEdit	m_CapVtgHigh;

	//전압 상승 검사
	SECCurrencyEdit	m_CompV[SCH_MAX_COMP_POINT];
	SECCurrencyEdit	m_CompV1[SCH_MAX_COMP_POINT];
	SECCurrencyEdit	m_CompTimeV[SCH_MAX_COMP_POINT];

	//전류 상승 검사
	SECCurrencyEdit	m_CompI[SCH_MAX_COMP_POINT];
	SECCurrencyEdit	m_CompI1[SCH_MAX_COMP_POINT];
	CMyDateTimeCtrl	m_CompTimeI[SCH_MAX_COMP_POINT];

	//Report 조건용 control
	SECCurrencyEdit	m_ReportTemp;
	SECCurrencyEdit	m_ReportV;
	SECCurrencyEdit m_ReportI;
	SECCurrencyEdit m_VIGetTime;
//	CMyDateTimeCtrl	m_ReportTime;

	//DCIR Setting control
	SECCurrencyEdit m_DCIR_RegTemp;				// 기준온도
	SECCurrencyEdit m_DCIR_ResistanceRate;		// 저항변화율

	SECCurrencyEdit	m_CompChgCcVtg;
	SECCurrencyEdit	m_CompChgCcTime;
	SECCurrencyEdit m_CompChgCcDeltaVtg;
	SECCurrencyEdit	m_CompChgCvCrt;
	SECCurrencyEdit	m_CompChgCvtTime;
	SECCurrencyEdit m_CompChgCvDeltaCrt;

	BOOL m_bShowEndDlgToggle;


// Operations
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCTSEditorView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView);
	//}}AFX_VIRTUAL

// Implementation
public:
	void DisplayStepEndStringAll();
	void DeleteSimulationFile(LONG lModelID);
	void DeleteSimulationFile(CString strFile);
	void DeleteSimulationFileTestData (LONG lTestID);	
	CString NewSimulationFile(CString strFile = "");
	void ModelSave();
	void ModelCopy();
	void ModelEdit();
	void ModelDelete();
	void NewModel();
	BOOL InitBatteryModelGrid(UINT nID, CWnd *pParent, CMyGridWnd *pGrid);
	BOOL CheckExecutableStep(int nTypeComboIndex, long lTestID, long lModelID);
	BOOL IsExecutableStep(int nTypeComboIndex, WORD type, WORD mode);
	void ApplySelectStep(int nStepType = -1);
	void SetApplyAllBtn(int nType);
	void AddUndoStep();
	CString ModeTypeMsg(int type, int mode);
	CString StepTypeMsg(int type);
	void SetStepCycleColumn();
	void SetDefultTest(CString strModel, CString strTest);
	void SetControlEnable(int nStepType);
	void DisplayStepOption( STEP *pStep = NULL);
	void ShowExtOptionCtrl(int nShow);
	void SetCompCtrlEnable(BOOL bEnable = TRUE, BOOL bAll = TRUE);
	BOOL ReadFile();
	BOOL WriteFile();
//	BOOL LoadTestList(CTestListRecordSet *pRecord);
	int GetTestIndexFromType(int nType);
	int GetStepIndexFromType(int nType);
	long GetModeIndex(int nMode);
	BOOL DeleteStep(CDWordArray &awSteps); 
	BOOL BackUpStep(CRowColArray &awRows, BOOL bEnableUndo = FALSE);
	BOOL m_bPasteUndo;
	BOOL m_bCopyed;
	BOOL CopyGradeStep(long lFromStepID, long lToStepID, long lModelID);
	BOOL CopyCheckParam(long lFromTestID, long lToTestID, long lModelID);
	BOOL CopyStepList(long lFromTestID, long lToTestID, long lModelID);
	BOOL CopyTestList(int lFromModelID, int lToModelID);
	BOOL CopyModel(ROWCOL nRow);
	void UpdateModelNo();
	BOOL UpdateTypeComboList();
	BOOL UpdateTestNo();
	int RequeryProcDataTypeID();
	void SetTestGridColumnWidth();
	void SetModelGridColumnWidth();
	BOOL StepSave();
	BOOL m_bStepDataSaved;
	CRect m_recDlgRect;
	void ShowEndTypeDlg(int nStep);
	void ClearStepState();
	LONG m_lLoadedBatteryModelID;
	LONG m_lLoadedTestID;

	void UpdateDspTest(CString strName, LONG lID);
	void HideEndDlg();
	void UpdateBatteryCountState(int nCount);
	int m_nDisplayStep;
	void UpdateDspModel(CString strName, LONG lID);
	void SettingStepWndType(int nStep, int nStepType, BOOL bGradeWnd = TRUE);
	void SettingGradeWndType(int nStepType, int nProcType = 0);
	BOOL ReLoadStepData(LONG lTestID, LONG lModelID);
	int UpdatePreTestParam();
	int DisplayPreTestParam();
	int UpdateStepGrade(int nStepNum);
	int DisplayStepGrade(STEP *pStep);
	void AddStepNew(int nStepNum = SCH_STEP_ALL, BOOL bAuto = TRUE, STEP *pCopyStep = NULL);
	BOOL UpdateStepGrid(int nStepNum = SCH_STEP_ALL);
	LONG m_lDisplayModelID;
	LONG m_lDisplayTestID;
	CString m_strDspModelName;
	CString m_strDspTestName;
	void Fun_LoadProcessSetting(LONG lTestID, LONG lModelID);
	int RequeryTestStep(LONG lTestID, LONG lModelID);
	BOOL RequeryTestList(LONG lModelID, CString strDefaultName = "");	
	BOOL DisplayStepGrid(int nStepNum = SCH_STEP_ALL);
	virtual ~CCTSEditorView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	CModelSelDlg *m_pModelDlg;
	int m_nTimer;
	CImageList	*m_pTabImageList;
	BOOL InitEditCtrl();
	void InitCtrl();
	void InitFont();
	BOOL InitGradeGrid();
	BOOL InitStepGrid();
	BOOL InitTestListGrid();
	void InitToolTips();
	void SetControlCellEdlc();
	BOOL	m_bVHigh;
	BOOL	m_bVLow;
	BOOL	m_bIHigh;
	BOOL	m_bILow;
	BOOL	m_bCHigh;
	BOOL	m_bCLow;
	float	m_fCLowVal;
	float	m_fCHighVal;
	float	m_fILowVal;
	float	m_fIHighVal;
	float	m_fVLowVal;
	float	m_fVHighVal;
	BOOL	m_bEditType;
	CArray<SCH_CODE_MSG ,SCH_CODE_MSG&> m_ptProcArray;

// Generated message map functions
protected:
	BOOL m_bUseModelInfo;
	CStringArray m_strTypeMaskArray;
	BOOL m_bUseTemp;
	UINT m_nCurTabIndex;
	void InitParamTab();
	BOOL ConvertStepData(CStep *pStepData, STEP *pStep);
//	BOOL ConvertCellCheckData( FILE_CELL_CHECK_PARAM &paramData, TEST_PARAM *pParam);
	BOOL ConvertStepFormat(STEP *pStep, FILE_STEP_PARAM &newStep);
//	BOOL ConvertCellCheckFormat(TEST_PARAM *pParam, FILE_CELL_CHECK_PARAM &newParam);
	//{{AFX_MSG(CCTSEditorView)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	afx_msg void OnLoadTest();
	afx_msg void OnModelNew();
	afx_msg void OnTestNew();
	afx_msg void OnModelDelete();
	afx_msg void OnTestDelete();
	afx_msg void OnModelSave();
	afx_msg void OnTestSave();
	afx_msg void OnStepSave();
	afx_msg void OnStepDelete();
	afx_msg void OnStepInsert();
	afx_msg void OnGradeCheck();
	afx_msg void OnPretestCheck();
	afx_msg void OnChangePretestMaxv();
	afx_msg void OnChangePretestDeltaV();
	afx_msg void OnChangePretestFaultNo();
	afx_msg void OnChangePretestMaxi();
	afx_msg void OnChangePretestMinv();
	afx_msg void OnChangePretestOcv();
	afx_msg void OnChangePretestTrckleI();
	afx_msg void OnChangeVtgHigh();
	afx_msg void OnChangeVtgLow();
	afx_msg void OnChangeCrtHigh();
	afx_msg void OnChangeCrtLow();
	afx_msg void OnChangeCapHigh();
	afx_msg void OnChangeCapLow();
	afx_msg void OnChangeImpHigh();
	afx_msg void OnChangeImpLow();
	afx_msg void OnChangeDeltaV();
	afx_msg void OnChangeDeltaI();
	afx_msg void OnChangeDeltaTime();
	afx_msg void OnOption();
	afx_msg void OnChangeCompV1();
	afx_msg void OnChangeCompV2();
	afx_msg void OnChangeCompV3();
	afx_msg void OnChangeCompTime1();
	afx_msg void OnChangeCompTime2();
	afx_msg void OnChangeCompTime3();
	afx_msg void OnEditCopy();
	afx_msg void OnUpdateEditCopy(CCmdUI* pCmdUI);
	afx_msg void OnEditPaste();
	afx_msg void OnUpdateEditPaste(CCmdUI* pCmdUI);
	afx_msg void OnEditCut();
	afx_msg void OnUpdateEditCut(CCmdUI* pCmdUI);
	afx_msg void OnEditUndo();
	afx_msg void OnUpdateEditUndo(CCmdUI* pCmdUI);
	afx_msg void OnModelCopyButton();
	afx_msg void OnExcelSave();
	afx_msg void OnUpdateFileSave(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFileOpen(CCmdUI* pCmdUI);
	afx_msg void OnFileSave();
	afx_msg void OnFileOpen();
	afx_msg void OnUpdateExcelSave(CCmdUI* pCmdUI);
	afx_msg void OnSelchangeParamTab(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnExtOptionCheck();
	afx_msg void OnModelEdit();
	afx_msg void OnTestEdit();
	afx_msg void OnFilePrint();
	afx_msg void OnInsertStep();
	afx_msg void OnDeleteStep();
	afx_msg void OnUpdateDeleteStep(CCmdUI* pCmdUI);
	afx_msg void OnUpdateInsertStep(CCmdUI* pCmdUI);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnTestCopyButton();
	afx_msg void OnEditInsert();
	afx_msg void OnUpdateEditInsert(CCmdUI* pCmdUI);
	afx_msg void OnChangeReportCurrent();
	afx_msg void OnChangeReportTemperature();
	afx_msg void OnChangeReportVoltage();
	afx_msg void OnDatetimechangeReportTime(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnChangeCapVtgLow();
	afx_msg void OnChangeCapVtgHigh();
	afx_msg void OnChangeCompITime1();
	afx_msg void OnChangeCompITime2();
	afx_msg void OnChangeCompITime3();
	afx_msg void OnChangeCompI1();
	afx_msg void OnChangeCompI2();
	afx_msg void OnChangeCompI3();
	afx_msg void OnChangeCompI4();
	afx_msg void OnChangeCompI5();
	afx_msg void OnChangeCompI6();
	afx_msg void OnChangeCompV4();
	afx_msg void OnChangeCompV5();
	afx_msg void OnChangeCompV6();
	afx_msg void OnDeltaposRptMsecSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnChangeCellCheckTimeEdit();
	afx_msg void OnAllStepButton();
	afx_msg void OnSameAllStepButton();
	afx_msg void OnChangeTempLow();
	afx_msg void OnChangeTempHigh();
	afx_msg void OnButtonModelSel();
	afx_msg void OnAdministration();
	afx_msg void OnUserSetting();
	afx_msg void OnUpdateAdministration(CCmdUI* pCmdUI);
	afx_msg void OnUpdateUserSetting(CCmdUI* pCmdUI);
	afx_msg void OnModelReg();
	afx_msg void OnUpdateModelReg(CCmdUI* pCmdUI);
	afx_msg void OnChangeReportTimeMili();
	afx_msg void OnKillFocusReportTimeMili();
	afx_msg void OnChangePretestOcvMinv();	
	//}}AFX_MSG
	afx_msg LRESULT OnGridDbClicked(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGridClicked(WPARAM wParam, LPARAM lParam);	
	afx_msg LRESULT OnMovedCurrentCell(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGridComboSelected(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGridComboDropDown(WPARAM wParam, LPARAM lParam);
	afx_msg	LRESULT OnRButtonClickedRowCol(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSelDragRowsDrop(WPARAM wParam, LPARAM lParam);	
	afx_msg LRESULT OnStartEditing(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnEndEditing(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGridBtnClicked(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnDateTimeChanged(WPARAM wParam, LPARAM lParam);
	
	
	DECLARE_MESSAGE_MAP()
public:
	CLabel m_Label1;
	CLabel m_LabelSelectInfo;
	afx_msg void OnEnChangeViGetTime();
	afx_msg void OnEnChangeEditRegTemp();
	afx_msg void OnEnChangeEditResistanceRate();
	afx_msg void OnEnChangeContackResistance();
	afx_msg void OnEnChangeCompChgCvDeltaCrt();
	afx_msg void OnEnChangeCompChgCvTime();
	afx_msg void OnEnChangeCompChgCvCrt();
	afx_msg void OnEnChangeCompChgCcDeltaVtg();
	afx_msg void OnEnChangeCompChgCcTime();
	afx_msg void OnEnChangeCompChgCcVtg();

	afx_msg void OnEnChangeContackLimitV();
	afx_msg void OnEnChangeContackUpperV();
	afx_msg void OnEnChangeContackLowerV();
	afx_msg void OnEnChangeContackUpperI();
	afx_msg void OnEnChangeContackLowerI();
	afx_msg void OnEnChangeContactDeltaLimitV();
};

#ifndef _DEBUG  // debug version in CTSEditorView.cpp
inline CCTSEditorDoc* CCTSEditorView::GetDocument()
   { return (CCTSEditorDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CTSEditorVIEW_H__1B7323B9_7330_4BD6_80E7_6B809D6197A2__INCLUDED_)
