// ModelNameDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSEditor.h"
#include "ModelNameDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CModelNameDlg dialog
extern CString GetDataBaseName();

CModelNameDlg::CModelNameDlg(BOOL bInsertMode /*= FALSE*/, CWnd* pParent /*=NULL*/)
	: CDialog(CModelNameDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CModelNameDlg)
	m_strName = _T("");
	m_strDescript = _T("");
	m_strUserID = _T("");
	//}}AFX_DATA_INIT
	m_bInsertMode = bInsertMode;

	LanguageinitMonConfig();
}

CModelNameDlg::~CModelNameDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CModelNameDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CModelNameDlg"), _T("TEXT_CModelNameDlg_CNT"), _T("TEXT_CModelNameDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CModelNameDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CModelNameDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

void CModelNameDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CModelNameDlg)
	DDX_Control(pDX, IDC_MODEL_COMBO, m_ctrlCombo);
	DDX_Control(pDX, IDC_MODEL_NAME_EDIT, m_ctrlName);
	DDX_Text(pDX, IDC_MODEL_NAME_EDIT, m_strName);
	DDV_MaxChars(pDX, m_strName, 63);
	DDX_Text(pDX, IDC_DESCRIP_EDIT, m_strDescript);
	DDV_MaxChars(pDX, m_strDescript, 127);
	DDX_Text(pDX, IDC_EDIT_ID, m_strUserID);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CModelNameDlg, CDialog)
	//{{AFX_MSG_MAP(CModelNameDlg)
	ON_EN_CHANGE(IDC_MODEL_NAME_EDIT, OnChangeModelNameEdit)
	ON_EN_CHANGE(IDC_DESCRIP_EDIT, OnChangeDescripEdit)
	ON_CBN_SELCHANGE(IDC_MODEL_COMBO, OnSelchangeModelCombo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CModelNameDlg message handlers

void CModelNameDlg::SetName(CString strName, CString strDescript)
{
	m_strName = strName;
	m_strDescript = strDescript;
}

CString CModelNameDlg::GetName()
{
	return m_strName;
}

CString CModelNameDlg::GetDescript()
{
	return m_strDescript;
}

BOOL CModelNameDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_ctrlName.SetSel(0, -1);
	GetDlgItem(IDC_MODEL_NAME_EDIT)->SetFocus();
	
	if(m_bInsertMode)
		GetDlgItem(IDC_INSERT_STATIC)->ShowWindow(SW_SHOW);
	else
		GetDlgItem(IDC_INSERT_STATIC)->ShowWindow(SW_HIDE);

	//Login Mode 사용시 사용자 ID는 자동 지정 
	if(AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "UseLogin", FALSE))
	{
		GetDlgItem(IDC_EDIT_ID)->EnableWindow(FALSE);
	}

#ifdef _FORMATION_
	GetDlgItem(IDC_DESC_STATIC)->SetWindowText(TEXT_LANG[0]);//"적용모델"
	GetDlgItem(IDC_MODEL_COMBO)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_DESCRIP_EDIT)->ShowWindow(SW_HIDE);

	//모델 목록이 있는지 검사후 없으며 등록하도록 한다.
	CString strSQL;
	CDaoDatabase  db;
	int nCnt = 0;
	COleVariant data;

	int nDefault = -1;
	strSQL = "SELECT Name FROM ModelInfo ORDER BY Name";
	try
	{
		db.Open(GetDataBaseName());
		CDaoRecordset rs(&db);
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	

		while(!rs.IsEOF())
		{
			data = rs.GetFieldValue(0);
			strSQL= data.pcVal;
			m_ctrlCombo.AddString(strSQL);
			if(strSQL ==  m_strDescript)
			{
				nDefault = nCnt;
			}
			nCnt++;
			rs.MoveNext();
		}
		rs.Close();
		db.Close();

		if(nCnt < 1)
		{
			AfxMessageBox(TEXT_LANG[1]);//"사용할 모델이 등록되어 있지 않습니다. 공정 목록 등록 이전에 사용할 모델을 등록하십시요."
		}
	}
	catch (CDaoException* e)
	{
		//Old Version
		TRACE("Model information tabe not exist\n");
		e->Delete();
	}

	if(	nDefault >= 0)
	{
		m_ctrlCombo.SetCurSel(nDefault);
	}

#endif

	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CModelNameDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData();
	if(m_strName.IsEmpty())
	{
		AfxMessageBox(TEXT_LANG[2]);//"목록명이 입력되지 않았습니다."
		GetDlgItem(IDC_MODEL_NAME_EDIT)->SetFocus();
		return;
	}

#ifdef _FORMATION_
	int nSel = m_ctrlCombo.GetCurSel();
	if(nSel == CB_ERR)
	{
		  AfxMessageBox(TEXT_LANG[3]);//"Model이 설정되지 않았습니다."
		  return;
	}
#endif

	CDialog::OnOK();
}


void CModelNameDlg::OnChangeModelNameEdit() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	UpdateData();
	GetDlgItem(IDOK)->EnableWindow(!m_strName.IsEmpty());

}

void CModelNameDlg::OnChangeDescripEdit() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
//	GetDlgItem(IDOK)->EnableWindow();	
}


void CModelNameDlg::OnSelchangeModelCombo() 
{
	// TODO: Add your control notification handler code here
	int nSel = m_ctrlCombo.GetCurSel();
	if(nSel != CB_ERR)
	{
		  m_ctrlCombo.GetLBText(nSel, m_strDescript);
	}
	UpdateData(FALSE);
}
