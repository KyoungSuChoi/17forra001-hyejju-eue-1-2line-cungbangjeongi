// CTSEditor.h : main header file for the CTSEditor application
//

#if !defined(AFX_CTSEditor_H__B028797E_F79E_413D_8E99_C6CA24E4C7A6__INCLUDED_)
#define AFX_CTSEditor_H__B028797E_F79E_413D_8E99_C6CA24E4C7A6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CCTSEditorApp:
// See CTSEditor.cpp for the implementation of this class
//

class CCTSEditorApp : public CWinApp
{
public:
	CCTSEditorApp();
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCTSEditorApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CCTSEditorApp)
	afx_msg void OnAppAbout();
	afx_msg void OnAppExit();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CTSEditor_H__B028797E_F79E_413D_8E99_C6CA24E4C7A6__INCLUDED_)
