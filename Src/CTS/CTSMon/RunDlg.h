#if !defined(AFX_RUNDLG_H__594ECFE1_784D_4B10_B721_7E5F3D16D1C1__INCLUDED_)
#define AFX_RUNDLG_H__594ECFE1_784D_4B10_B721_7E5F3D16D1C1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RunDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CRunDlg dialog
#include "CTSMonDoc.h"
#include "MyGridWnd.h"

#define _GRID_COL_JIGNO		1
#define _GRID_COL_INPUTCOUT	2
#define _GRID_COL_TRAYNO	3
#define _GRID_COL_LOTNO		4
#define _GRID_COL_CELLNO	5
#define _GRID_COL_FILENAME	6
#define _GRID_COL_TESTSERIAL 7		

#define _GRID_TOT_COL		7

class CRunDlg : public CDialog
{
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();
	virtual ~CRunDlg();

// Construction
public:
	CString RemakeFileName(int nTrayIndex=0);
	BOOL m_bNewTray;
//	BOOL m_bReadOk;
//	STR_CONDITION *m_pCondition;
	CRunDlg(CWnd* pParent = NULL);   // standard constructor
//	STR_GROUP_INFO *m_pGroupInfo;
	CFormModule *m_lpModuleInfo;
	CCTSMonDoc *m_pDoc;

// Dialog Data
	//{{AFX_DATA(CRunDlg)
	enum { IDD = IDD_RUN_MODULE_DLG };
	CXPButton	m_btnSelFile;
	CXPButton	m_btnOK;
	CXPButton	m_btnCancel;
	CXPButton	m_btnSensorSet;
	CXPButton	m_btnProcDel;
	CXPButton	m_btnDetalCon;
	CEdit	m_ctrlFileName;
	CLabel	m_ctrlModuleLabel;
	CLabel	m_ctrlTestName;
	CString	m_strLot;
	CString	m_strFileName;
	CString	m_strTrayNo;
	BOOL	m_bAutoSetData;
	CString	m_strTestSerialNo;
	CString	m_UserID;
	int		m_nCellNo;
	UINT	m_nInputCellCount;
	BOOL	m_bCellCodeCheck;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRunDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void UpdateBtnState();
	int SumUserInputCellCount();
	void InitTrayGrid();
	CMyGridWnd m_wndTrayGrid;
	// Generated message map functions
	//{{AFX_MSG(CRunDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDetailConBtn();
	virtual void OnOK();
	afx_msg void OnSelFile();
	afx_msg void OnChangeLotNo();
	afx_msg void OnChangeCellNo();
	afx_msg void OnChangeTrayno();
	afx_msg void OnProcDeleteButton();
	afx_msg void OnSensorSettingButton();
	//}}AFX_MSG
	afx_msg LRESULT OnMovedCurrentCell(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnStartEditing(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnEndEditing(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnModifyEdit(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnBcrscaned(UINT wParam, LONG lParam);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RUNDLG_H__594ECFE1_784D_4B10_B721_7E5F3D16D1C1__INCLUDED_)
