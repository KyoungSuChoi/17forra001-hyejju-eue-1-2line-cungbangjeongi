// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__DBE6A4EE_32CE_45E6_9EB3_3D9FB5F1DB57__INCLUDED_)
#define AFX_STDAFX_H__DBE6A4EE_32CE_45E6_9EB3_3D9FB5F1DB57__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes

#ifndef _AFX_NO_DAO_SUPPORT
#include <afxdao.h>			// MFC DAO database classes
#endif // _AFX_NO_DAO_SUPPORT

#ifndef _AFX_NO_DB_SUPPORT
#include <afxdb.h>			// MFC ODBC database classes
#endif // _AFX_NO_DB_SUPPORT

#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include "ColorButton2.h"

#define _DATABASE_CONNECTION_DAO	
#define TRAY_DATA_NVRAM		0

// DEFINE
enum RangeType{ High_Range=0, Middle_Range=1, Low_Range=2 };

#define MAX_ONLINE_UNIT		256
#define MAX_ONLINE_STEPDATA	10
#define JIGNUM		8
#define CODENUM		2
#define ONLINE_STEPTYPE_IDLE		0
#define ONLINE_STEPTYPE_CHARGE		1
#define ONLINE_STEPTYPE_DISCHARGE	2
#define ONLINE_STEPTYPE_REST		3
#define ONLINE_STEPTYPE_OCV			4
#define ONLINE_STEPTYPE_IMPEDANCE	5
#define ONLINE_STEPTYPE_END			6
#define ONLINE_STEPTYPE_ADV_CYCLE	7
#define ONLINE_STEPTYPE_LOOP		8
#define ONLINE_STEPSTART			0
#define ONLINE_STEPEND				1

#define RGB_MIDNIGHTBLUE	RGB(30,30,125)
#define RGB_WHITE			RGB(255,255,255)
#define RGB_CORNFLOWERBLUE	RGB(100,149,237)
#define RGB_DARKSLATEBLUE	RGB(72,61,141)
#define RGB_RED        RGB(220,  0,  0)
#define RGB_GREEN      RGB(  0,220,  0)
#define RGB_BLUE       RGB(  0,  0,127)
#define RGB_BLACK			RGB(0,0,0)
#define RGB_LTGRAY			RGB(192, 192, 192)
#define RGB_LIGHTRED   RGB(255,  0,  0)
#define RGB_LIGHTGREEN RGB(  0,255,  0)
#define RGB_LIGHTBLUE  RGB(  0,  0,255)
#define RGB_WHITE      RGB(255,255,255)
#define RGB_GRAY       RGB(192,192,192)
#define RGB_YELLOW	   RGB(255,255,0)

#define INFO_TYPE_NORMAL	0
#define INFO_TYPE_WARNNING	1
#define INFO_TYPE_FAULT		2

typedef double element;

typedef struct stackNode 
{			
	element data; 
	struct stackNode *link; 
}stackNode;

extern stackNode* top;

/*
#define RGB_LABEL_FONT_STAGENAME RGB(255, 255, 255)
#define RGB_LABEL_BACKGROUND_STAGENAME	RGB(120,170,255)
*/

#define RGB_LABEL_FONT_STAGENAME RGB(255, 255, 255)
#define RGB_LABEL_BACKGROUND_STAGENAME	RGB(0,0,0)

#define RGB_LABEL_FONT		RGB(255,255,255)
#define RGB_LABEL_BACKGROUND	RGB(72,61,141)

#define RGB_BTN_BACKGROUND	RGB(225, 225, 225)
#define RGB_BTN_FONT		RGB(0,0,0)
#define RGB_BTN_DISBKGND	RGB(255,255,255)

enum { 
	DEVICE_CHARGER1 = 0,
	DEVICE_FORMATION1,
	DEVICE_FORMATION2,
	DEVICE_FORMATION3,
	DEVICE_FORMATION4,
	DEVICE_FORMATION5,
	DEVICE_CHARGER2,
	DEVICE_CHARGER3,	
	DEVICE_DCIR1,		// 1Lane DCIR	
};

enum {
	TAB_INDEX_MANUALCONTROL = 0,
	TAB_INDEX_MONITORING,
	TAB_INDEX_CONNECTION,
	TAB_INDEX_OPERATION
};

#include "FMSGlobal.h"

#include "../Sqlite/sqlite3.h"

#define DATABASE_NAME	"CTSProduct"

#include "../../../Include/IniParser.h"	
extern int g_nLanguage;
extern CString g_strLangPath;
enum LanguageType {LANGUAGE_KOR=0, LANGUAGE_ENG, LANGUAGE_CHI };

#include "../../../Include/Formall.h"	
#include "../../UtilityClass/FormUtility.h"

#include <toolkit/secall.h>
#include <grid/gxall.h>

#include "FormationModule.h"

// 20130910 mysql 추가
#include <my_global.h>
#include <mysql.h>

typedef struct
{	
	MYSQL_RES*  res;
	MYSQL_ROW   row;
	MYSQL_FIELD *fields;
	int		    nfield;
	int         nrecord;
} MYSQLINFO;

// 20160618 PLC 통신 추가
#define WM_MESSAGE_PLCToCTSMON	(WM_USER + 5100)
#define WM_MESSAGE_CTSMONToPLC	(WM_USER + 5101)

#define EP_SIGNAL_ON	1
#define EP_SIGNAL_OFF	0

#include "NameInc.h"

// #pragma comment(lib, "C:\\Program Files (x86)\\mysql 5.6\\lib\\libmySQL.lib")
#pragma comment(lib, "../../../Lib/libmySQL.lib")

#ifdef _DEBUG
#pragma comment(lib, "../../../Lib/BFCommonD.lib")
#pragma message("Automatically linking with BFCommonD.lib By K.B.H ")
#else
#pragma comment(lib, "../../../Lib/BFCommon.lib")
#pragma message("Automatically linking with BFCommon.lib By K.B.H")
#endif	//_DEBUG

#ifdef _DEBUG
#pragma comment(lib, "../../../Lib/BFCtrlD.lib")
#pragma message("Automatically linking with BFCtrlD.lib By K.B.H ")
#else
#pragma comment(lib, "../../../Lib/BFCtrl.lib")
#pragma message("Automatically linking with BFCtrl.lib By K.B.H")
#endif	//_DEBUG

#ifdef _DEBUG
#pragma comment(lib, "../../../Lib/BFServerD.lib")
#pragma message("Automatically linking with BFServerD.lib By K.B.H ")
#else
#pragma comment(lib, "../../../Lib/BFServer.lib")
#pragma message("Automatically linking with BFServer.lib By K.B.H")
#endif	//_DEBUG
 
#ifdef _DEBUG
#pragma comment(lib, "../../../Lib/DataCltD.lib")
#pragma message("Automatically linking with DataCltD.lib By K.B.H ")
#else
#pragma comment(lib, "../../../Lib/DataClt.lib")
#pragma message("Automatically linking with DataClt.lib By K.B.H")
#endif	//_DEBUG

#pragma comment(lib, "../../../Lib/PEGRAP32.LIB")
#pragma message("Automatically linking with PEGRAP32.LIB By K.B.H ")

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__DBE6A4EE_32CE_45E6_9EB3_3D9FB5F1DB57__INCLUDED_)
