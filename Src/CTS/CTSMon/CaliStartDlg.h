#if !defined(AFX_CALISTARTDLG_H__A26F6C30_96B6_4071_8240_F81B1151FE4A__INCLUDED_)
#define AFX_CALISTARTDLG_H__A26F6C30_96B6_4071_8240_F81B1151FE4A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CaliStartDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCaliStartDlg dialog

class CCaliStartDlg : public CDialog
{
// Construction
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();
	virtual ~CCaliStartDlg();

	BOOL m_bChannelMode;
	CCaliStartDlg(int nModuleID, int nVRangeCnt = CAL_MAX_VOLTAGE_RANGE, int IRangeCnt = CAL_MAX_CURRENT_RANGE, int nTrayType = -1, CWnd* pParent = NULL);   // standard constructor
	int	m_unitno;
	
// Dialog Data
	//{{AFX_DATA(CCaliStartDlg)
	enum { IDD = IDD_CALCHK_START_DLG };
	CComboBox	m_ctrlRange;
	CComboBox	m_ctrlVI;
	CComboBox	m_ctrlMain;
	CComboBox	m_ctrlCali;
	CComboBox	m_ctrlBD;
	CString	m_strUserName;
	BOOL	m_bMultiMode;
	//}}AFX_DATA
	int n_main;
	int n_vi;
	int n_board;
	int n_cali;
	int n_range;
	int n_vRangeCnt;
	int n_iRangeCnt;
	int n_iTrayType;	//ljb 2011519 ����=0 /����=1

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCaliStartDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCaliStartDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnStartButton();
	afx_msg void OnSelchangeViCombo();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CALISTARTDLG_H__A26F6C30_96B6_4071_8240_F81B1151FE4A__INCLUDED_)
