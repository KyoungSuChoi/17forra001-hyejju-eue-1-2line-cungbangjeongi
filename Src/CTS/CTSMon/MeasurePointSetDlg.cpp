// MeasurePointSetDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "MeasurePointSetDlg.h"
#include "TextInputDlg.h"


// CMeasurePointSetDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CMeasurePointSetDlg, CDialog)

CMeasurePointSetDlg::CMeasurePointSetDlg(CMeasurePoint* pMeasPoint, CWnd* pParent /*=NULL*/)
	: CDialog(CMeasurePointSetDlg::IDD, pParent)
{
	m_pMeasPoint = pMeasPoint;
	m_nSelPresetNum = 0;
	LanguageinitMonConfig();
}

CMeasurePointSetDlg::~CMeasurePointSetDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CMeasurePointSetDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CMeasurePointSetDlg"), _T("TEXT_CMeasurePointSetDlg_CNT"), _T("TEXT_CMeasurePointSetDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CMeasurePointSetDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CMeasurePointSetDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}


void CMeasurePointSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_POINT_V, m_wndListPointV);
	DDX_Control(pDX, IDC_POINT_I, m_wndListPointI);
	DDX_Control(pDX, IDC_LABEL1, m_Label1);
	DDX_Control(pDX, IDC_SHUNT_T, m_wndShuntT);
	DDX_Control(pDX, IDC_SHUNT_R, m_wndShuntR);
	DDX_Control(pDX, IDC_SHUNT_SERIAL, m_wndShuntSerial);
	DDX_Control(pDX, IDC_POINT_V2, m_wndListPointV2);
	DDX_Control(pDX, IDC_POINT_V3, m_wndListPointV3);
	DDX_Control(pDX, IDC_POINT_I2, m_wndListPointI2);
	DDX_Control(pDX, IDC_POINT_I3, m_wndListPointI3);
}


BEGIN_MESSAGE_MAP(CMeasurePointSetDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CMeasurePointSetDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CMeasurePointSetDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BTN_PRESET_1, &CMeasurePointSetDlg::OnBnClickedBtnPreset1)
	ON_BN_CLICKED(IDC_BTN_PRESET_2, &CMeasurePointSetDlg::OnBnClickedBtnPreset2)
	ON_BN_CLICKED(IDC_BTN_PRESET_3, &CMeasurePointSetDlg::OnBnClickedBtnPreset3)
	ON_BN_CLICKED(IDC_BTN_PRESET_4, &CMeasurePointSetDlg::OnBnClickedBtnPreset4)
	ON_BN_CLICKED(IDC_BTN_PRESET_SAVE, &CMeasurePointSetDlg::OnBnClickedBtnPresetSave)
	ON_BN_CLICKED(IDC_BTN_PRESET_LOAD, &CMeasurePointSetDlg::OnBnClickedBtnPresetLoad)
END_MESSAGE_MAP()


// CMeasurePointSetDlg 메시지 처리기입니다.

void CMeasurePointSetDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	SaveMeasureData();

	OnOK();
}

void CMeasurePointSetDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}

void CMeasurePointSetDlg::InitLabel()
{
	m_Label1.SetFontSize(15)
		.SetTextColor(RGB(255,255,0))
		.SetBkColor(RGB(0,0,0))
		.SetSunken(TRUE)
		.SetFontBold(TRUE)		
		.SetText("None");
}

BOOL CMeasurePointSetDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	float fMaxBoundaryData;
	fMaxBoundaryData = 10000000.0f;		//Max 10000A;

	m_wndListPointI.SetBoundaryData(fMaxBoundaryData);
	m_wndListPointV.SetBoundaryData(fMaxBoundaryData);
	m_wndListPointI2.SetBoundaryData(fMaxBoundaryData);
	m_wndListPointV2.SetBoundaryData(fMaxBoundaryData);
	m_wndListPointI3.SetBoundaryData(fMaxBoundaryData);
	m_wndListPointV3.SetBoundaryData(fMaxBoundaryData);

	m_wndListPointI.SetDigitMode(TRUE);
	m_wndListPointV.SetDigitMode(TRUE);
	m_wndListPointI2.SetDigitMode(TRUE);
	m_wndListPointV2.SetDigitMode(TRUE);
	m_wndListPointI3.SetDigitMode(TRUE);
	m_wndListPointV3.SetDigitMode(TRUE);

	m_wndShuntT.m_bCheckCali = FALSE;
	m_wndShuntR.m_bCheckCali = FALSE;
	m_wndShuntSerial.m_bCheckCali = FALSE;

	m_wndShuntT.SetDigitMode(TRUE);
	m_wndShuntR.SetDigitMode(TRUE);
	m_wndShuntSerial.SetDigitMode(FALSE);

	CString strVItem, strIItem;
	CString strMsg;

	strVItem = TEXT_LANG[0];//"전압값(mV)"
	strIItem = TEXT_LANG[1];//"전류값(mA)"
	
	int nWidth = 95;
	m_wndListPointI.InsertColumn(0, "No", LVCFMT_RIGHT, 45);
	m_wndListPointI.InsertColumn(1, strIItem, LVCFMT_RIGHT, nWidth);
	m_wndListPointI2.InsertColumn(0, "No", LVCFMT_RIGHT, 35);
	m_wndListPointI2.InsertColumn(1, strIItem, LVCFMT_RIGHT, nWidth);
	m_wndListPointI3.InsertColumn(0, "No", LVCFMT_RIGHT, 35);
	m_wndListPointI3.InsertColumn(1, strIItem, LVCFMT_RIGHT, nWidth);

	DWORD dwExStyle = m_wndListPointI.GetExtendedStyle();
	dwExStyle |= LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES;
	m_wndListPointI.SetExtendedStyle( dwExStyle );
	m_wndListPointI2.SetExtendedStyle( dwExStyle );
	m_wndListPointI3.SetExtendedStyle( dwExStyle );

	m_wndListPointV.InsertColumn(0, "No", LVCFMT_RIGHT, 35);
	m_wndListPointV.InsertColumn(1, strVItem, LVCFMT_RIGHT, nWidth);
	m_wndListPointV2.InsertColumn(0, "No", LVCFMT_RIGHT, 35);
	m_wndListPointV2.InsertColumn(1, strVItem, LVCFMT_RIGHT, nWidth);
	m_wndListPointV3.InsertColumn(0, "No", LVCFMT_RIGHT, 35);
	m_wndListPointV3.InsertColumn(1, strVItem, LVCFMT_RIGHT, nWidth);

	dwExStyle = m_wndListPointV.GetExtendedStyle();
	dwExStyle |= LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES;
	m_wndListPointV.SetExtendedStyle( dwExStyle );
	m_wndListPointV2.SetExtendedStyle( dwExStyle );
	m_wndListPointV3.SetExtendedStyle( dwExStyle );
	
	int i = 0;
	for( i =0; i<MEAS_MAX_VOLTAGE_CNT; i++ )
	{
		strMsg.Format("%d", i+1);		
		m_wndListPointV.SetItemText(i, 0, strMsg);
		m_wndListPointV2.SetItemText(i, 0, strMsg);
		m_wndListPointV3.SetItemText(i, 0, strMsg);
	}

	for( i =0; i<MEAS_MAX_CURRENT_CNT; i++ )
	{
		strMsg.Format("%d", i+1);
		m_wndListPointI.SetItemText(i, 0, strMsg);
		m_wndListPointI2.SetItemText(i, 0, strMsg);
		m_wndListPointI3.SetItemText(i, 0, strMsg);
	}

	m_wndShuntT.InsertColumn(0, "No", LVCFMT_RIGHT, 45);
	m_wndShuntT.InsertColumn(1, "T", LVCFMT_RIGHT, nWidth);	
	dwExStyle = m_wndShuntT.GetExtendedStyle();
	dwExStyle |= LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES;
	m_wndShuntT.SetExtendedStyle( dwExStyle );

	m_wndShuntR.InsertColumn(0, "No", LVCFMT_RIGHT, 45);
	m_wndShuntR.InsertColumn(1, "R", LVCFMT_RIGHT, nWidth);	
	dwExStyle = m_wndShuntR.GetExtendedStyle();
	dwExStyle |= LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES;
	m_wndShuntR.SetExtendedStyle( dwExStyle );

	m_wndShuntSerial.InsertColumn(0, "No", LVCFMT_RIGHT, 45);
	m_wndShuntSerial.InsertColumn(1, "Serial", LVCFMT_CENTER, 130);	
	dwExStyle = m_wndShuntSerial.GetExtendedStyle();
	dwExStyle |= LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES;
	m_wndShuntSerial.SetExtendedStyle( dwExStyle );

	for(i=0; i<CAL_MAX_BOARD_NUM; i++ )
	{
		strMsg.Format("%d", i+1);
		m_wndShuntT.SetItemText(i, 0, strMsg);
		m_wndShuntR.SetItemText(i, 0, strMsg);
		m_wndShuntSerial.SetItemText(i, 0, strMsg);
	}

	LoadMeasureData();
	
	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CMeasurePointSetDlg::SaveMeasureData()
{
	int i = 0;
	CString strTemp;

	m_wndListPointV.UpdateText();
	m_wndListPointI.UpdateText();
	m_wndListPointV2.UpdateText();
	m_wndListPointI2.UpdateText();
	m_wndListPointV3.UpdateText();
	m_wndListPointI3.UpdateText();

	int nDataCnt = m_wndListPointV.GetItemCount();

	if( nDataCnt > MEAS_MAX_VOLTAGE_CNT )
	{
		nDataCnt = MEAS_MAX_VOLTAGE_CNT;
	}

	for( i=0; i<nDataCnt; i++ )
	{
		strTemp = m_wndListPointV.GetItemText(i, 1);
		m_pMeasPoint->SetVData(i,atof(strTemp), High_Range);
	}

	m_pMeasPoint->SetVMeasureCnt( nDataCnt, High_Range );

	nDataCnt = m_wndListPointV2.GetItemCount();

	if( nDataCnt > MEAS_MAX_VOLTAGE_CNT )
	{
		nDataCnt = MEAS_MAX_VOLTAGE_CNT;
	}

	for( i=0; i<nDataCnt; i++ )
	{
		strTemp = m_wndListPointV2.GetItemText(i, 1);
		m_pMeasPoint->SetVData(i,atof(strTemp), Middle_Range);
	}

	m_pMeasPoint->SetVMeasureCnt( nDataCnt, Middle_Range );

	nDataCnt = m_wndListPointV3.GetItemCount();

	if( nDataCnt > MEAS_MAX_VOLTAGE_CNT )
	{
		nDataCnt = MEAS_MAX_VOLTAGE_CNT;
	}

	for( i=0; i<nDataCnt; i++ )
	{
		strTemp = m_wndListPointV3.GetItemText(i, 1);
		m_pMeasPoint->SetVData(i,atof(strTemp), Low_Range);
	}

	m_pMeasPoint->SetVMeasureCnt( nDataCnt, Low_Range );

	nDataCnt = m_wndListPointI.GetItemCount();
	if( nDataCnt > MEAS_MAX_CURRENT_CNT )
	{
		nDataCnt = MEAS_MAX_CURRENT_CNT;
	}

	for( i=0; i<nDataCnt; i++ )
	{
		strTemp = m_wndListPointI.GetItemText(i, 1);
		m_pMeasPoint->SetIData(i,atof(strTemp), High_Range);
	}
	m_pMeasPoint->SetIMeasureCnt( nDataCnt, High_Range );

	nDataCnt = m_wndListPointI2.GetItemCount();
	if( nDataCnt > MEAS_MAX_CURRENT_CNT )
	{
		nDataCnt = MEAS_MAX_CURRENT_CNT;
	}

	for( i=0; i<nDataCnt; i++ )
	{
		strTemp = m_wndListPointI2.GetItemText(i, 1);
		m_pMeasPoint->SetIData(i,atof(strTemp), Middle_Range);
	}

	m_pMeasPoint->SetIMeasureCnt( nDataCnt, Middle_Range );

	nDataCnt = m_wndListPointI3.GetItemCount();
	if( nDataCnt > MEAS_MAX_CURRENT_CNT )
	{
		nDataCnt = MEAS_MAX_CURRENT_CNT;
	}

	for( i=0; i<nDataCnt; i++ )
	{
		strTemp = m_wndListPointI3.GetItemText(i, 1);
		m_pMeasPoint->SetIData(i,atof(strTemp), Low_Range);
	}

	m_pMeasPoint->SetIMeasureCnt( nDataCnt, Low_Range );

	double  shuntData[CAL_MAX_BOARD_NUM];
	ZeroMemory( shuntData, sizeof(shuntData));
	CHAR	shuntSerial[CAL_MAX_BOARD_NUM][CAL_MAX_SHUNT_SERIAL_SIZE];
	ZeroMemory( shuntSerial, sizeof(shuntSerial));

	m_wndShuntT.UpdateText();
	nDataCnt = m_wndShuntT.GetItemCount();
	if(nDataCnt > CAL_MAX_BOARD_NUM)
	{
		nDataCnt = CAL_MAX_BOARD_NUM;
	}

	for(i =0; i<CAL_MAX_BOARD_NUM; i++)
	{	
		strTemp = m_wndShuntT.GetItemText(i, 1);
		shuntData[i] = atof(strTemp);
	}

	m_pMeasPoint->SetShuntT(shuntData);

	m_wndShuntR.UpdateText();
	nDataCnt = m_wndShuntR.GetItemCount();
	if(nDataCnt > CAL_MAX_BOARD_NUM)
	{
		nDataCnt = CAL_MAX_BOARD_NUM;
	}

	for(i =0; i<CAL_MAX_BOARD_NUM; i++)
	{	
		strTemp = m_wndShuntR.GetItemText(i, 1);
		shuntData[i] = atof(strTemp);
	}

	m_pMeasPoint->SetShuntR(shuntData);

	m_wndShuntSerial.UpdateText();
	nDataCnt = m_wndShuntSerial.GetItemCount();
	if(nDataCnt > CAL_MAX_BOARD_NUM)
	{
		nDataCnt = CAL_MAX_BOARD_NUM;
	}

	for(i =0; i<CAL_MAX_BOARD_NUM; i++)
	{
		strTemp = m_wndShuntSerial.GetItemText(i, 1);
		sprintf(&shuntSerial[i][0], "%s", strTemp.Left(15));
		m_pMeasPoint->SetShuntSerial(i, &shuntSerial[i][0]);
	}

	AfxGetApp()->WriteProfileBinary(REG_CAL_SECTION, "MeasurePoint", (LPBYTE)m_pMeasPoint, sizeof(CMeasurePoint));
}

void CMeasurePointSetDlg::LoadMeasureData()
{
	int i=0;
	int nDataCnt = 0;
	CString strTemp;

	LV_ITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LV_ITEM));
	lvItem.mask = LVIF_TEXT;

	nDataCnt = m_pMeasPoint->GetVMeasureCnt(High_Range);

	for( i=0; i<nDataCnt; i++ )
	{
		lvItem.iItem = i;
		strTemp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTemp;
		m_wndListPointV.InsertItem(&lvItem);
		strTemp.Format("%.3f", m_pMeasPoint->GetVData(i, High_Range) );
		m_wndListPointV.SetItemText(i, 1, strTemp);
	}

	m_wndListPointV.UpdateText();

	nDataCnt = m_pMeasPoint->GetVMeasureCnt(Middle_Range);

	for( i=0; i<nDataCnt; i++ )
	{
		lvItem.iItem = i;
		strTemp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTemp;
		m_wndListPointV2.InsertItem(&lvItem);
		strTemp.Format("%.3f", m_pMeasPoint->GetVData(i, Middle_Range) );
		m_wndListPointV2.SetItemText(i, 1, strTemp);
	}

	m_wndListPointV2.UpdateText();

	nDataCnt = m_pMeasPoint->GetVMeasureCnt(Low_Range);

	for( i=0; i<nDataCnt; i++ )
	{
		lvItem.iItem = i;
		strTemp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTemp;
		m_wndListPointV3.InsertItem(&lvItem);
		strTemp.Format("%.3f", m_pMeasPoint->GetVData(i, Low_Range) );
		m_wndListPointV3.SetItemText(i, 1, strTemp);
	}

	m_wndListPointV3.UpdateText();	

	nDataCnt = m_pMeasPoint->GetIMeasureCnt(High_Range);

	for( i=0; i<nDataCnt; i++ )
	{
		lvItem.iItem = i;
		strTemp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTemp;
		m_wndListPointI.InsertItem(&lvItem);
		strTemp.Format("%.3f", m_pMeasPoint->GetIData(i,High_Range) );
		m_wndListPointI.SetItemText(i, 1, strTemp);
	}

	m_wndListPointI.UpdateText();

	nDataCnt = m_pMeasPoint->GetIMeasureCnt(Middle_Range);

	for( i=0; i<nDataCnt; i++ )
	{
		lvItem.iItem = i;
		strTemp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTemp;
		m_wndListPointI2.InsertItem(&lvItem);
		strTemp.Format("%.3f", m_pMeasPoint->GetIData(i, Middle_Range) );
		m_wndListPointI2.SetItemText(i, 1, strTemp);
	}

	m_wndListPointI2.UpdateText();

	nDataCnt = m_pMeasPoint->GetIMeasureCnt(Low_Range);

	for( i=0; i<nDataCnt; i++ )
	{
		lvItem.iItem = i;
		strTemp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTemp;
		m_wndListPointI3.InsertItem(&lvItem);
		strTemp.Format("%.3f", m_pMeasPoint->GetIData(i,Low_Range) );
		m_wndListPointI3.SetItemText(i, 1, strTemp);
	}

	m_wndListPointI3.UpdateText();

	m_wndShuntT.DeleteAllItems();	
	for( i=0; i<CAL_MAX_BOARD_NUM; i++)
	{
		if( m_pMeasPoint->GetShuntT(i) == 0 )
		{
			break;
		}

		lvItem.iItem = i;
		strTemp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTemp;
		m_wndShuntT.InsertItem(&lvItem);
		strTemp.Format("%.4f", m_pMeasPoint->GetShuntT(i));
		m_wndShuntT.SetItemText(i, 1, strTemp);	
	}	

	m_wndShuntR.DeleteAllItems();	
	for( i=0; i<CAL_MAX_BOARD_NUM; i++)
	{
		if( m_pMeasPoint->GetShuntR(i) == 0 )
		{
			break;
		}

		lvItem.iItem = i;
		strTemp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTemp;
		m_wndShuntR.InsertItem(&lvItem);
		strTemp.Format("%.4f", m_pMeasPoint->GetShuntR(i));
		m_wndShuntR.SetItemText(i, 1, strTemp);	
	}	

	m_wndShuntSerial.DeleteAllItems();	
	for( i=0; i<CAL_MAX_BOARD_NUM; i++)
	{
		strTemp.Format("%s", m_pMeasPoint->GetShuntSerial(i));
		if( strTemp.IsEmpty() || strTemp == "None" )
		{
			break;
		}

		lvItem.iItem = i;
		strTemp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTemp;
		m_wndShuntSerial.InsertItem(&lvItem);
		strTemp.Format("%s", m_pMeasPoint->GetShuntSerial(i));
		m_wndShuntSerial.SetItemText(i, 1, strTemp);	
	}
}

void CMeasurePointSetDlg::fnUpdatePreset()
{
	switch( m_nSelPresetNum )
	{
	case 0:
		m_Label1.SetText("None");
		break;
	case 1:
		m_Label1.SetText("===> 1");
		break;
	case 2:
		m_Label1.SetText("===> 2");
		break;
	case 3:
		m_Label1.SetText("===> 3");
		break;
	case 4:
		m_Label1.SetText("===> 4");
		break;
	}
}

void CMeasurePointSetDlg::OnBnClickedBtnPreset1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nSelPresetNum = 1;
	fnUpdatePreset();
}

void CMeasurePointSetDlg::OnBnClickedBtnPreset2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nSelPresetNum = 2;
	fnUpdatePreset();
}

void CMeasurePointSetDlg::OnBnClickedBtnPreset3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nSelPresetNum = 3;
	fnUpdatePreset();
}

void CMeasurePointSetDlg::OnBnClickedBtnPreset4()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nSelPresetNum = 4;
	fnUpdatePreset();
}

void CMeasurePointSetDlg::OnBnClickedBtnPresetSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( m_nSelPresetNum != 0 )
	{
		if( MessageBox("Save your current setting?","Confirm", MB_YESNO|MB_ICONQUESTION) == IDYES )
		{
			fnSavePreset( m_nSelPresetNum );

			m_nSelPresetNum = 0;

			fnUpdatePreset();
		}
	}
}

BOOL CMeasurePointSetDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	if( pMsg->message == WM_KEYDOWN )
	{
		switch( pMsg->wParam )
		{
		case VK_RETURN:
			return FALSE;			
		case VK_ESCAPE:
			return FALSE;
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL CMeasurePointSetDlg::fnLoadPreset( int nSelNum )
{
	UINT nSize;
	LPVOID* pData;
	CString strName;

	double  shuntT[CAL_MAX_BOARD_NUM];
	double	shuntR[CAL_MAX_BOARD_NUM];
	CHAR	shuntSerial[CAL_MAX_BOARD_NUM][CAL_MAX_SHUNT_SERIAL_SIZE];

	strName.Format("ShuntT_Preset%d", nSelNum);
	AfxGetApp()->GetProfileBinary(REG_CAL_SECTION, strName, (LPBYTE *)&pData, &nSize);
	if(nSize > 0)	memcpy(shuntT, pData, nSize);
	else			
	{
		memset(shuntT, 0, sizeof(shuntT));
	}	
	delete [] pData;

	strName.Format("ShuntR_Preset%d", nSelNum);
	AfxGetApp()->GetProfileBinary(REG_CAL_SECTION, strName, (LPBYTE *)&pData, &nSize);
	if(nSize > 0)	memcpy(shuntR, pData, nSize);
	else			
	{
		memset(shuntR, 0, sizeof(shuntR));
	}	
	delete [] pData;

	strName.Format("ShuntSerial_Preset%d", nSelNum);
	AfxGetApp()->GetProfileBinary(REG_CAL_SECTION, strName, (LPBYTE *)&pData, &nSize);
	if(nSize > 0)	memcpy(shuntSerial, pData, nSize);
	else			
	{
		memset(shuntSerial, 0, sizeof(shuntSerial));
	}	
	delete [] pData;

	LV_ITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LV_ITEM));
	lvItem.mask = LVIF_TEXT;

	int i=0;
	CString strTemp;	

	m_wndShuntT.DeleteAllItems();

	for( i=0; i<CAL_MAX_BOARD_NUM; i++)
	{
		if( shuntT[i] == 0 )
		{
			break;
		}

		lvItem.iItem = i;
		strTemp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTemp;
		m_wndShuntT.InsertItem(&lvItem);
		strTemp.Format("%.3f", shuntT[i]);
		m_wndShuntT.SetItemText(i, 1, strTemp);	
	}	

	m_wndShuntR.DeleteAllItems();	
	for( i=0; i<CAL_MAX_BOARD_NUM; i++)
	{
		if( shuntR[i] == 0 )
		{
			break;
		}

		lvItem.iItem = i;
		strTemp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTemp;
		m_wndShuntR.InsertItem(&lvItem);
		strTemp.Format("%.3f", shuntR[i]);
		m_wndShuntR.SetItemText(i, 1, strTemp);	
	}	

	m_wndShuntSerial.DeleteAllItems();	
	for( i=0; i<CAL_MAX_BOARD_NUM; i++)
	{
		strTemp.Format("%s", shuntSerial[i]);
		if( strTemp.IsEmpty() )
		{
			break;
		}

		lvItem.iItem = i;
		strTemp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTemp;
		m_wndShuntSerial.InsertItem(&lvItem);
		strTemp.Format("%s", shuntSerial[i]);
		m_wndShuntSerial.SetItemText(i, 1, strTemp);	
	}

	return TRUE;
}

BOOL CMeasurePointSetDlg::fnSavePreset( int nSelNum )
{
	CString strName = _T("");
	CString strTemp = _T("");
	int nDatacount = 0;
	int i=0;

	double  shuntData[CAL_MAX_BOARD_NUM];
	ZeroMemory( shuntData, sizeof(shuntData));
	CHAR	shuntSerial[CAL_MAX_BOARD_NUM][CAL_MAX_SHUNT_SERIAL_SIZE];
	ZeroMemory( shuntSerial, sizeof(shuntSerial));

	m_wndShuntT.UpdateText();
	nDatacount = m_wndShuntT.GetItemCount();
	if(nDatacount > CAL_MAX_BOARD_NUM)
	{
		nDatacount = CAL_MAX_BOARD_NUM;
	}

	for(i =0; i<CAL_MAX_BOARD_NUM; i++)
	{	
		strTemp = m_wndShuntT.GetItemText(i, 1);
		shuntData[i] = atof(strTemp);
	}

	strName.Format("ShuntT_Preset%d", nSelNum);
	AfxGetApp()->WriteProfileBinary(REG_CAL_SECTION, strName, (LPBYTE)shuntData, sizeof(shuntData));

	m_wndShuntR.UpdateText();
	nDatacount = m_wndShuntR.GetItemCount();
	if(nDatacount > CAL_MAX_BOARD_NUM)
	{
		nDatacount = CAL_MAX_BOARD_NUM;
	}

	for(i =0; i<CAL_MAX_BOARD_NUM; i++)
	{	
		strTemp = m_wndShuntR.GetItemText(i, 1);
		shuntData[i] = atof(strTemp);
	}

	strName.Format("ShuntR_Preset%d", nSelNum);
	AfxGetApp()->WriteProfileBinary(REG_CAL_SECTION, strName, (LPBYTE)shuntData, sizeof(shuntData));

	m_wndShuntSerial.UpdateText();
	nDatacount = m_wndShuntSerial.GetItemCount();
	if(nDatacount > CAL_MAX_BOARD_NUM)
	{
		nDatacount = CAL_MAX_BOARD_NUM;
	}

	for(i =0; i<CAL_MAX_BOARD_NUM; i++)
	{
		strTemp = m_wndShuntSerial.GetItemText(i, 1);
		sprintf(&shuntSerial[i][0], "%s", strTemp.Left(15));
	}

	strName.Format("ShuntSerial_Preset%d", nSelNum);
	AfxGetApp()->WriteProfileBinary(REG_CAL_SECTION, strName, (LPBYTE)shuntSerial, sizeof(shuntSerial));

	return TRUE;
}
void CMeasurePointSetDlg::OnBnClickedBtnPresetLoad()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( m_nSelPresetNum != 0 )
	{
		if( fnLoadPreset( m_nSelPresetNum ) == FALSE )
		{
			AfxMessageBox("Preset data loading fail!", MB_ICONASTERISK);
		}
	}
}
