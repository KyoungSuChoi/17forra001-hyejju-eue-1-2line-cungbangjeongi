// CTSMon.h : main header file for the CTSMon application
//

#if !defined(AFX_CTSMon_H__C2DE1ACB_822E_424A_95FA_D6D80981D056__INCLUDED_)
#define AFX_CTSMon_H__C2DE1ACB_822E_424A_95FA_D6D80981D056__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CCTSMonApp:
// See CTSMon.cpp for the implementation of this class
//
//////////////////////////////////////////////////////////////////////////
#include "FMSLog.h"
#include "FMSCriticalSection.h"
#include "FMSSyncParent.h"
#include "FMSStaticSyncParent.h"
#include "FMSMemoryPool.h"
#include "FMSManagedBuf.h"
#include "FMSCircularQueue.h"

#include "FMSPacketBox.h"

#include "FMSIocp.h"
#include "FMSNetObj.h"

#include "FMSNetIOCP.h"
#include "FMSRawServer.h"

#include "FMSObj.h"

#include "FMSServer.h"
//////////////////////////////////////////////////////////////////////////
class CCTSMonApp : public CWinApp
{
private:
	DWORD m_bySystemType;
public:
	CCTSMonApp();
	CString SetSystemType(DWORD lSystemID);
	DWORD GetSystemType();

	CString *TEXT_LANG;
	bool LanguageinitMonConfig();	

	CFMSServer m_FMSserver;
	CFMSCircularQueue<st_FMS_PACKET> m_FMS_RecvQ;
	CFMSCircularQueue<st_FMS_PACKET> m_FMS_SendQ;
	bool	m_bHeartbeatChk;

	CString m_strCurFolder;
	
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCTSMonApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CCTSMonApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

extern CCTSMonApp theApp;
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CTSMon_H__C2DE1ACB_822E_424A_95FA_D6D80981D056__INCLUDED_)
