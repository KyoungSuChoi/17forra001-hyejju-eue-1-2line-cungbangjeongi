// DataDown.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "DataDown.h"
#include "DataDownDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDataDownApp

BEGIN_MESSAGE_MAP(CDataDownApp, CWinApp)
	//{{AFX_MSG_MAP(CDataDownApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDataDownApp construction

CDataDownApp::CDataDownApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CDataDownApp object

CDataDownApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CDataDownApp initialization

BOOL CDataDownApp::InitInstance()
{
	AfxGetModuleState()->m_dwVersion = 0x0601;

	if (!AfxSocketInit())
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}

	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif
	
	SetRegistryKey(_T("PNE CTS"));

	if( LanguageinitMonConfig() == false )
	{
		AfxMessageBox("Could not found [ Datadown_Lang.ini ]");
		return false;
	}
	
	HWND FirsthWnd, FirstChildhWnd;
	if ((FirsthWnd = FindWindow(NULL, DATA_DOWN_TITLE_NAME)) != NULL)
	{
		FirstChildhWnd = GetLastActivePopup(FirsthWnd);
		SetActiveWindow(FirsthWnd);	
		SetForegroundWindow(FirsthWnd);
		if (FirsthWnd != FirstChildhWnd)
		{
			SetActiveWindow(FirstChildhWnd);	
			SetForegroundWindow(FirstChildhWnd);
		}

		ShowWindow(FirsthWnd, SW_SHOW); 	
		return FALSE;
	}
	
	
	char szBuff1[32];
	ZeroMemory(szBuff1, sizeof(szBuff1));
	sscanf(m_lpCmdLine, "%s", szBuff1);
	
	CString strOption(szBuff1);			//IPAddress

	CDataDownDlg dlg;
	if(strOption == "/start")
	{
		dlg.m_bHideMode = TRUE;
	}

	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

bool CDataDownApp::LanguageinitMonConfig() 
{
	g_nLanguage = AfxGetApp()->GetProfileInt(REG_SEC_NAME, "Language", 0);
	g_strLangPath.Format("%s\\Lang\\Datadown_Lang.ini", GetProfileString(REG_SEC_NAME ,"Path", "C:\\Program Files\\PNE CTS"));

	switch(g_nLanguage)
	{
	case 0:
		{
			SetThreadUILanguage( MAKELCID( MAKELANGID(LANG_KOREAN , SUBLANG_KOREAN) , SORT_DEFAULT));
			break;
		}

	case 1:
		{
			SetThreadUILanguage( MAKELCID( MAKELANGID(LANG_ENGLISH , SUBLANG_ENGLISH_US) , SORT_DEFAULT));
			break;
		}

	case 2:
		{
			SetThreadUILanguage( MAKELCID( MAKELANGID(LANG_CHINESE_SIMPLIFIED , SUBLANG_CHINESE_SIMPLIFIED) , SORT_DEFAULT));
			break;
		}
	}

	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CDataDownApp"), _T("TEXT_CDataDownApp_CNT"), _T("TEXT_CDataDownApp_CNT"));
	if( strTemp == "TEXT_CDataDownApp_CNT" )
	{
		return false;
	}

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt];		

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CDataDownApp_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CDataDownApp"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Languge error ====> " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}
