// RunDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSMon.h"
#include "RunDlg.h"
#include "UserAdminDlg.h"
#include "SensorMapDlg.h"
#include "MainFrm.h"
#include "FolderDialog.h"
#include "JigIDRegDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRunDlg dialog


CRunDlg::CRunDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CRunDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CRunDlg)
	m_strLot = _T("");
	m_strFileName = _T("");
	m_strTrayNo = _T("");
	m_bAutoSetData = FALSE;
	m_strTestSerialNo = _T("");
	m_UserID = _T("");
	m_nCellNo = 1;
	m_nInputCellCount = 0;
	m_bCellCodeCheck = TRUE;
	//}}AFX_DATA_INIT
	m_pDoc = NULL;
//	m_pCondition = NULL;
//	m_bReadOk = TRUE;
	m_bNewTray = TRUE;
	LanguageinitMonConfig();
}


CRunDlg::~CRunDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CRunDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CRunDlg"), _T("TEXT_CRunDlg_CNT"), _T("TEXT_CRunDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CRunDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CRunDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}


void CRunDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRunDlg)
	DDX_Control(pDX, IDC_SEL_FILE, m_btnSelFile);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDC_SENSOR_SETTING_BUTTON, m_btnSensorSet);
	DDX_Control(pDX, IDC_PROC_DELETE_BUTTON, m_btnProcDel);
	DDX_Control(pDX, IDC_DETAIL_CON_BTN, m_btnDetalCon);
	DDX_Control(pDX, IDC_RESULT_FILE_NAME, m_ctrlFileName);
	DDX_Control(pDX, IDC_MODULE_INFO_LABEL, m_ctrlModuleLabel);
	DDX_Control(pDX, IDC_TEST_NAME, m_ctrlTestName);
	DDX_Text(pDX, IDC_LOT_NO, m_strLot);
	DDV_MaxChars(pDX, m_strLot, EP_LOT_NAME_LENGTH);
	DDX_Text(pDX, IDC_RESULT_FILE_NAME, m_strFileName);
	DDX_Text(pDX, IDC_TRAYNO, m_strTrayNo);
	DDV_MaxChars(pDX, m_strTrayNo, EP_TRAY_NAME_LENGTH);
	DDX_Check(pDX, IDC_AUTO_SET, m_bAutoSetData);
	DDX_Text(pDX, IDC_TEST_SERIALNO, m_strTestSerialNo);
	DDV_MaxChars(pDX, m_strTestSerialNo, EP_TEST_SERIAL_LENGTH);
	DDX_Text(pDX, IDC_OPERATOR_NAME, m_UserID);
	DDV_MaxChars(pDX, m_UserID, EP_MAX_LONINID_LENGTH);
	DDX_Text(pDX, IDC_CELL_NO, m_nCellNo);
	DDX_Text(pDX, IDC_INPUT_CELL_COUNT, m_nInputCellCount);
//	DDV_MinMaxUInt(pDX, m_nInputCellCount, 0, EP_MAX_CH_PER_MD);
	DDX_Check(pDX, IDC_CODE_SET_CHECK, m_bCellCodeCheck);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CRunDlg, CDialog)
	//{{AFX_MSG_MAP(CRunDlg)
	ON_BN_CLICKED(IDC_DETAIL_CON_BTN, OnDetailConBtn)
	ON_BN_CLICKED(IDC_SEL_FILE, OnSelFile)
	ON_EN_CHANGE(IDC_LOT_NO, OnChangeLotNo)
	ON_EN_CHANGE(IDC_CELL_NO, OnChangeCellNo)
	ON_EN_CHANGE(IDC_TRAYNO, OnChangeTrayno)
	ON_BN_CLICKED(IDC_PROC_DELETE_BUTTON, OnProcDeleteButton)
	ON_BN_CLICKED(IDC_SENSOR_SETTING_BUTTON, OnSensorSettingButton)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_GRID_MOVECELL, OnMovedCurrentCell)
	ON_MESSAGE(WM_GRID_BEGINEDIT, OnStartEditing)
	ON_MESSAGE(WM_GRID_ENDEDIT, OnEndEditing)
	ON_MESSAGE(WM_GRID_EDIT_MIDIFY, OnModifyEdit)
	ON_MESSAGE(EPWM_BCR_SCANED, OnBcrscaned)
	ON_BN_CLICKED(IDOK, &CRunDlg::OnBnClickedOk)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRunDlg message handlers

BOOL CRunDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// TODO: Add extra initialization here
	InitTrayGrid();
	
	m_bAutoSetData = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "AutoRun", 0);

	CString strFileName;		
	strFileName.Format("%s %s", ::GetModuleName(m_lpModuleInfo->GetModuleID()),	GetStringTable(IDS_TEXT_WORK_INFO));
	m_ctrlModuleLabel.SetText(strFileName);
	m_ctrlModuleLabel.SetBkColor(RGB(255,255,255));
	m_ctrlModuleLabel.SetTextColor(RGB(255, 0, 0));
	m_ctrlModuleLabel.SetFontUnderline(TRUE);
	m_ctrlModuleLabel.SetFontBold(TRUE);
	m_ctrlModuleLabel.SetFontSize(15);
	
	strFileName.Format("%d : %s", m_lpModuleInfo->GetCondition()->GetTestInfo()->lNo, m_lpModuleInfo->GetTestName());
	m_ctrlTestName.SetText(strFileName);	
	m_ctrlTestName.SetTextColor(RGB(255, 0 ,0));
	m_ctrlTestName.SetBkColor(RGB(0, 0 ,0));
	m_ctrlTestName.SetFontSize(18);
	m_ctrlTestName.SetFontBold(TRUE);

//	m_strLot = m_lpModuleInfo->GetLotNo();			
//	m_strTrayNo = m_lpModuleInfo->GetTrayNo();
//	m_strTestSerialNo = m_lpModuleInfo->GetTestSerialNo();	
//	m_nCellNo = m_lpModuleInfo->GetCellNo();
//	m_strFileName = m_lpModuleInfo->GetResultFileName();

	m_strFileName = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"Data");
	if(m_nCellNo < 1)	m_nCellNo = 1;

	m_UserID =	m_lpModuleInfo->GetOperatorID();	
	
	
	m_nInputCellCount = 0;
	int nTrayCount = 0;
	BOOL bTestReserved =  m_lpModuleInfo->GetTestReserved();
	CTray *pTray;
	
	//대표 Lot번호 
/*	for(int t=0; t<m_lpModuleInfo->GetTotalJig(); t++)
	{
		pTray = m_lpModuleInfo->GetTrayInfo(t);
		if(pTray && pTray->strLotNo.IsEmpty() == FALSE)
		{
			m_strLot = pTray->strLotNo;
		}
	}
*/
	if(m_bNewTray && bTestReserved== FALSE)
	{
		COleDateTime curTime = COleDateTime::GetCurrentTime();
		int nChCount = m_lpModuleInfo->GetCellCountInTray();

		for(int t=0; t<m_lpModuleInfo->GetTotalJig(); t++)
		{
			pTray = m_lpModuleInfo->GetTrayInfo(t);

			m_strTestSerialNo.Format("%s%02d", curTime.Format("%Y%m%d%H%M%S"), t+1);			
			pTray->strTestSerialNo = m_strTestSerialNo;
			
			//기본 Cell 수량을 표시한다.
/*			if(bTrayBCRScaned == FALSE)				//BCR을 1개도 안읽었을 경우는 모든 Tray가 삽입될것으로 예측 
			{
				if(EPTrayState(m_lpModuleInfo->GetModuleID(), t) == EP_TRAY_LOAD)
				{
					pTray->lInputCellCount = nChCount;
					nTrayCount++;
				}
				else
				{
					pTray->lInputCellCount = 0;
				}
			}
			else
			{
				if(pTray->GetBcrRead())					//BCR이 읽힌 Tray는 모두 삽입될 것으로 예측 
				{
					pTray->lInputCellCount = nChCount;
					nTrayCount++;
				}
				else
				{
					pTray->lInputCellCount = 0;
				}
			}
*/

			//BCR을 읽은것과 상관없이 Tray가 Load되었으면 포함함 
			if(EPTrayState(m_lpModuleInfo->GetModuleID(), t) == EP_TRAY_LOAD)
			{
				pTray->lInputCellCount = nChCount;
				nTrayCount++;
			}
			else
			{
				pTray->lInputCellCount = 0;
			}
		}	
		m_nInputCellCount = nChCount*nTrayCount;
	}
	else
	{
		//연속 공정이거나 이전에 예약한 상태이면 
		for(int t=0; t<m_lpModuleInfo->GetTotalJig(); t++)
		{
			pTray = m_lpModuleInfo->GetTrayInfo(t);
			if(pTray && pTray->GetBcrRead())
			{
				m_nInputCellCount += m_lpModuleInfo->GetInputCellCount(t);	
				nTrayCount++;
			}

			if(pTray && pTray->strLotNo.IsEmpty() == FALSE)
			{
				m_strLot = pTray->strLotNo;
			}
		}
	}

	//Grid에 표시
	for(int t=0; t<m_lpModuleInfo->GetTotalJig(); t++)
	{
		pTray = m_lpModuleInfo->GetTrayInfo(t);
		
		m_wndTrayGrid.SetStyleRange(CGXRange(t+1, _GRID_COL_INPUTCOUT), CGXStyle().SetValue(pTray->lInputCellCount));
		m_wndTrayGrid.SetStyleRange(CGXRange(t+1, _GRID_COL_LOTNO), CGXStyle().SetValue(m_strLot));
		// if(pTray->GetBcrRead())
		// {
			m_wndTrayGrid.SetStyleRange(CGXRange(t+1, _GRID_COL_TRAYNO), CGXStyle().SetValue(pTray->GetTrayNo()));
		// }
	
		//결과 파일명 생성 
		strFileName = pTray->m_strFileName.Mid(pTray->m_strFileName.ReverseFind('\\')+1);
		m_wndTrayGrid.SetStyleRange(CGXRange(t+1, _GRID_COL_FILENAME), CGXStyle().SetValue(strFileName));
		m_wndTrayGrid.SetStyleRange(CGXRange(t+1, _GRID_COL_TESTSERIAL), CGXStyle().SetValue(pTray->strTestSerialNo));

		if(pTray->strLotNo.IsEmpty())
		{
			m_strLot = pTray->strLotNo;
		}
	}


	//Set Control state
	BOOL bAutoState		= ::EPGetAutoProcess(m_lpModuleInfo->GetModuleID());
	GetDlgItem(IDC_PROC_DELETE_BUTTON)->EnableWindow(bAutoState);
	if(bAutoState)
	{
		GetDlgItem(IDC_TRAY_NO)->EnableWindow(FALSE);
		GetDlgItem(IDC_AUTO_SET)->EnableWindow(TRUE);
	}
	//새로운 tray 구별 하여 Lot, test Serial No, Cell No 입력 
	GetDlgItem(IDC_LOT_NO)->EnableWindow(m_bNewTray);	
	GetDlgItem(IDC_CELL_NO)->EnableWindow(m_bNewTray);	
	GetDlgItem(IDC_INPUT_CELL_COUNT)->EnableWindow(m_bNewTray);

	UpdateData(FALSE);

	
	SumUserInputCellCount();
	UpdateBtnState();

	m_ctrlFileName.SetSel( 0, -1);//m_strFileName.GetLength()-1, m_strFileName.GetLength());
	
	return FALSE;	// return TRUE unless you set the focus to a control
					// EXCEPTION: OCX Property Pages should return FALSE
}

void CRunDlg::OnDetailConBtn() 
{
	m_pDoc->ShowTestCondition(m_lpModuleInfo->GetModuleID());
	// TODO: Add your control notification handler code here
}

void CRunDlg::OnOK() 
{
	// TODO: Add extra validation here
	CString strTemp;
	SumUserInputCellCount();
	UpdateData();

	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "AutoRun", m_bAutoSetData);

	if(AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "UseLogin", 1))
	{
		CUserAdminDlg	userSearch;
		STR_LOGIN	userData;
		if(userSearch.SearchUser(m_UserID, FALSE, "", &userData) == FALSE)	
		{
			strTemp.Format("%s [ID :: %s]", ::GetStringTable(IDS_TEXT_NOT_REGISTED_USER), m_UserID);
			GetDlgItem(IDC_OPERATOR_NAME)->SetFocus();
			AfxMessageBox(strTemp);
			return;
		}
		
		if(!(userData.nPermission & PMS_GROUP_CONTROL_CMD))
		{
			strTemp.Format("%s [ID :: %s]", ::GetStringTable(IDS_LANG_MSG_PERMISSION_ERROR), m_UserID);
			GetDlgItem(IDC_OPERATOR_NAME)->SetFocus();
			AfxMessageBox(strTemp);
			return;		
		}
	}

	//2004/5/8
	//Tray명이 리눅스에서 폴더명으로 사용되고 window에서 파일명에 포함되므로 특수 문자 제한
	//리눅스에서 폴더명에 공백이 허용 안됨
	//  \ / ? : " * < >  와 Space
	if(m_strLot.IsEmpty())
	{
		AfxMessageBox(TEXT_LANG[0]);//"Lot 번호가 입력되지 않았습니다."
		GetDlgItem(IDC_LOT_NO)->SetFocus();
		return;
	}
	if(m_strLot.FindOneOf("\\/?[]|=%.:\"*<> ") >= 0)
	{
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_ALERT_BLANK_FIELD));
		GetDlgItem(IDC_LOT_NO)->SetFocus();
		return;
	}

	if(m_nInputCellCount <= 0)
	{
		AfxMessageBox(TEXT_LANG[1]);//"투입된 Cell이 없습니다."
		return;
	}

	int nChCount= m_lpModuleInfo->GetCellCountInTray();

	CTray *pTray;
	int nInputCount;
	int nCnt = 0;
	CString strTrayNo;
	for(int t=0; t<m_lpModuleInfo->GetTotalJig(); t++)
	{
		pTray = m_lpModuleInfo->GetTrayInfo(t);
		strTrayNo = m_wndTrayGrid.GetValueRowCol(t+1, _GRID_COL_TRAYNO);
		strTrayNo.TrimLeft(' ');
		strTrayNo.TrimRight(' ');
		if(strTrayNo.FindOneOf("\\/?[]|=%.:\"*<> ") >= 0)			
		{
			strTemp.Format(TEXT_LANG[2], pTray->GetTrayNo());//"Tray명 [%s]는 입력할 수 없는 문자가 포함 되어 있습니다.(\\/?:\*<> 와  공백은 입력불가)"
			MessageBox(strTemp, "Error", MB_OK|MB_ICONSTOP);
			return;
		}
		
		//BCR을 수동으로 변경했을 수도 있으므로 재입력한다.  
		if(strTrayNo.IsEmpty() == FALSE)
		{
			if(((CMainFrame *)AfxGetMainWnd())->UserInputTrayNo(m_lpModuleInfo->GetModuleID(), t+1, strTrayNo) == FALSE)
			{
				return;
			}
			TRACE("%s Tray\n", strTrayNo);
			Sleep(100);
		}
		else
		{
			//작업에 사용하지 않는 TrayNo는 Reset한다. => TrayNo가 없고 입력수량이 없으면 작업을 안한 Tray로 판별 
			pTray->InitData();
			//수동으로 TrayNo를 입력하지 않았어도 BCR에 의해 입력 될 수 있다.
		}
		
		nInputCount = atol(m_wndTrayGrid.GetValueRowCol(t+1, _GRID_COL_INPUTCOUT));
		if(pTray->GetBcrRead())
		{
			if(nInputCount <= 0)
			{
				//BCR을 Read하고 입력 수량을 입력하지 않은 경우 
				strTemp.Format(TEXT_LANG[3], //"Tray [%s]의 투입 Cell 수량이 없습니다. Tray [%s]를 작업에서 제외하고 배출하시겠습니까?\n\nYes : Tray를 배출\nNo : 투입 Cell 수량 입력"
								pTray->GetTrayNo(), pTray->GetTrayNo());
				if(MessageBox(strTemp, "Error", MB_YESNO|MB_ICONQUESTION) == IDYES)
				{
					pTray->SetBcrRead(FALSE);		//Tray Read를 취소하던지
					m_wndTrayGrid.SetStyleRange(CGXRange(t+1, _GRID_COL_TRAYNO), CGXStyle().SetValue(""));

					//Cell을 Enable시켜야 함 
				}
				//수량을 입력하던지 
				return;
			}
		}

		if(nInputCount > nChCount)
		{
			strTemp.Format(TEXT_LANG[4], pTray->GetTrayNo(), nChCount);//"Tray [%s] Cell 최대 투입수량을 벗어났습니다.(최대 %d)"
			MessageBox(strTemp, "Error", MB_OK|MB_ICONSTOP);
			return;
		}


		if(nInputCount > 0)
		{
			pTray->SetTestReserved();
			pTray->ModuleID = m_lpModuleInfo->GetModuleID();
			pTray->GroupIndex = t;
			pTray->strJigID.Format("%d", t);
			pTray->lInputCellCount = nInputCount;
			pTray->strLotNo = m_wndTrayGrid.GetValueRowCol(t+1, _GRID_COL_LOTNO);				
			pTray->strOperatorID = m_UserID;
			pTray->strTestSerialNo = m_wndTrayGrid.GetValueRowCol(t+1, _GRID_COL_TESTSERIAL);	
			pTray->lTestKey = m_lpModuleInfo->GetCondition()->GetTestInfo()->lID;
			pTray->lModelKey = m_lpModuleInfo->GetCondition()->GetModelID();
			
			//Cell번호는 Newtray일 경우만 새로 지정한다.
			if(m_bNewTray)
			{
				//Cell 번호 지정
				pTray->lCellNo =  m_nCellNo + nCnt;
			}
			nCnt += nInputCount;
		}
		else
		{
			pTray->SetTestReserved(FALSE);
		}
		
		//수량으로 예약되거나 BCR로 예약된 Tray는 결과 파일명을 올바르게 입력해야 한다.
		//파일명검사 
		if(pTray->GetTestReserved() || pTray->GetBcrRead())
		{
			CString strFileName = _T("");
			CString strNewFileName = _T("");

			strFileName.Format("%s\\%s", 
							m_pDoc->CreateResultFilePath(m_lpModuleInfo->GetModuleID(), m_strFileName, pTray->strLotNo, pTray->GetTrayNo()), 
							m_wndTrayGrid.GetValueRowCol(t+1, _GRID_COL_FILENAME)
						);    
			
			m_lpModuleInfo->SetResultFileName(strFileName, t);

			if(m_pDoc->CheckFileNameValidate(strFileName, strNewFileName, m_lpModuleInfo->GetModuleID()) == FALSE)
			{
				strTemp.Format(TEXT_LANG[5], t+1, strFileName);//"%d번째 Tray 의 결과 파일명 %s는 올바르지 않습니다"
				MessageBox(strTemp, TEXT_LANG[6], MB_ICONSTOP|MB_OK);//"결과 파일 생성"
				return;
			}

			m_lpModuleInfo->SetResultFileName("", t);
			m_lpModuleInfo->SetResultFileName(strNewFileName, t);
			
		}
		else
		{
			m_lpModuleInfo->SetResultFileName("", t);
		}
	}
	CDialog::OnOK();
}

//Data 저장 위치 폴더 선택으로 변경 
void CRunDlg::OnSelFile() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	CFolderDialog dlg(m_strFileName);
	if( dlg.DoModal() == IDOK)
	{
		m_strFileName = dlg.GetPathName();
		UpdateData(FALSE);
	}	

/*
	CFileDialog pDlg(FALSE, "fmt", m_strFileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "Formation Data(*.fmt)|*.fmt|");
	pDlg.m_ofn.lpstrTitle = "Formation 결과 파일 저장";
	if(pDlg.DoModal() == IDOK)
	{
//		m_strFileName = pDlg.GetFolderPath();
		m_strFileName  = pDlg.GetPathName();
//		m_pDoc->m_strDataFolder = m_strFileName.Left(m_strFileName.ReverseFind('\\'));
		UpdateData(FALSE);
	}
	m_ctrlFileName.SetSel( 0, -1);//m_strFileName.GetLength()-1, m_strFileName.GetLength());
*/
}


void CRunDlg::OnChangeLotNo() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

	for(int i=0; i<m_wndTrayGrid.GetRowCount(); i++)
	{
		CTray *pTray = m_lpModuleInfo->GetTrayInfo(i);
		if(pTray)
		{
			pTray->strLotNo = m_strLot;
			m_wndTrayGrid.SetValueRange(CGXRange(i+1, _GRID_COL_FILENAME), RemakeFileName(i));
			m_wndTrayGrid.SetValueRange(CGXRange(i+1, _GRID_COL_LOTNO), m_strLot);
		}
	}
	UpdateData(FALSE);
}

void CRunDlg::OnChangeCellNo() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
//	m_strFileName = RemakeFileName();

	UpdateData(FALSE);	
	m_ctrlFileName.SetSel( 0, -1);//m_strFileName.GetLength()-1, m_strFileName.GetLength());
}

void CRunDlg::OnChangeTrayno() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

//	m_strFileName = RemakeFileName();

	UpdateData(FALSE);	
	m_ctrlFileName.SetSel( 0, -1);//m_strFileName.GetLength()-1, m_strFileName.GetLength());
}

void CRunDlg::OnProcDeleteButton() 
{
	// TODO: Add your control notification handler code here
	m_pDoc->SendInitCommand(m_lpModuleInfo->GetModuleID());

	CDialog::OnCancel();
}

void CRunDlg::OnSensorSettingButton() 
{
	// TODO: Add your control notification handler code here
	CSensorMapDlg dlg(m_pDoc, this);
	dlg.m_nModuleID = m_lpModuleInfo->GetModuleID();
	dlg.DoModal();
	return;	
}

CString CRunDlg::RemakeFileName(int nTrayIndex)
{
	CString strName;
	
	CTray *pTray = m_lpModuleInfo->GetTrayInfo(nTrayIndex);
	if(pTray)
	{

		CString strTray = m_wndTrayGrid.GetValueRowCol(nTrayIndex+1, _GRID_COL_TRAYNO);

		//strName.Format("%d_%s_%s_%s_T%02d.fmt", 
		//				m_lpModuleInfo->GetTestSequenceNo(),
		//				m_lpModuleInfo->GetTestName(),
		//				m_strLot,
		//				//m_lpModuleInfo->GetLotNo(nTrayIndex),
		//				strTray,	
		//				nTrayIndex+1
		//				);
		COleDateTime nowtime(COleDateTime::GetCurrentTime());													

		// SKI kky 결과파일 형식 변경( ProcessNo(00)_TrayId(000000)_일시(130826134333).fmt													
		strName.Format("%02d_%s_%s.fmt", 													
			m_lpModuleInfo->GetCondition()->GetModelID(),													
			strTray,													
			nowtime.Format("%y%m%d%H%M%S")													
			);
	}

	return strName;
}

void CRunDlg::InitTrayGrid()
{
	m_wndTrayGrid.SubclassDlgItem(IDC_TRAY_GRID, this);
	m_wndTrayGrid.m_bSameRowSize = FALSE;
	m_wndTrayGrid.m_bSameColSize = FALSE;
	m_wndTrayGrid.m_bCustomColor = FALSE;

	m_wndTrayGrid.Initialize();

	m_wndTrayGrid.SetRowCount(m_lpModuleInfo->GetTotalJig());
	m_wndTrayGrid.SetColCount(_GRID_TOT_COL);
	m_wndTrayGrid.SetDefaultRowHeight(22);
	m_wndTrayGrid.SetColWidth(_GRID_COL_JIGNO, _GRID_COL_JIGNO, 50);
	m_wndTrayGrid.SetColWidth(_GRID_COL_INPUTCOUT, _GRID_COL_INPUTCOUT, 50);
	m_wndTrayGrid.SetColWidth(_GRID_COL_TRAYNO, _GRID_COL_TRAYNO, 100);

#ifdef _DEBUG
	m_wndTrayGrid.SetColWidth(_GRID_COL_LOTNO, _GRID_COL_LOTNO, 50);
	m_wndTrayGrid.SetColWidth(_GRID_COL_CELLNO, _GRID_COL_CELLNO, 50);
#else
	m_wndTrayGrid.SetColWidth(_GRID_COL_LOTNO, _GRID_COL_LOTNO, 0);
	m_wndTrayGrid.SetColWidth(_GRID_COL_CELLNO, _GRID_COL_CELLNO, 0);
#endif
	CRect gridRect, cellRect;
	m_wndTrayGrid.GetWindowRect(gridRect);						//Get Step Grid Window Position
	cellRect = m_wndTrayGrid.CalcRectFromRowCol(0, 0, 0, _GRID_COL_FILENAME-1);
	m_wndTrayGrid.SetColWidth(_GRID_COL_FILENAME, _GRID_COL_FILENAME, gridRect.Width()-cellRect.Width()-5);
	m_wndTrayGrid.SetColWidth(_GRID_COL_TESTSERIAL, _GRID_COL_TESTSERIAL, 0);

	//Enable Multi Channel Select
	m_wndTrayGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	//Enable Tooltips
	m_wndTrayGrid.EnableGridToolTips();
//  m_wndTrayGrid.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, CGXStyle().SetWrapText(TRUE).SetAutoSize(FALSE));

	//Row Header Setting
	m_wndTrayGrid.SetStyleRange(CGXRange().SetCols(_GRID_COL_JIGNO), 	CGXStyle().SetEnabled(FALSE));
	m_wndTrayGrid.SetStyleRange(CGXRange().SetCols(_GRID_COL_TESTSERIAL),	CGXStyle().SetEnabled(FALSE));

	m_wndTrayGrid.SetStyleRange(CGXRange().SetCols(_GRID_COL_INPUTCOUT, _GRID_COL_FILENAME), 
								CGXStyle().SetControl(GX_IDS_CTRL_EDIT)
										.SetVerticalAlignment(DT_VCENTER)
										.SetHorizontalAlignment(DT_CENTER)
										.SetWrapText(FALSE)
								);

	//Table Name
	m_wndTrayGrid.SetStyleRange(CGXRange(0, _GRID_COL_JIGNO), CGXStyle().SetValue("Jig"));
	m_wndTrayGrid.SetStyleRange(CGXRange(0, _GRID_COL_INPUTCOUT), CGXStyle().SetValue(::GetStringTable(IDS_TEXT_INPUT_CELL)));
	m_wndTrayGrid.SetStyleRange(CGXRange(0, _GRID_COL_TRAYNO), CGXStyle().SetValue(::GetStringTable(IDS_LABEL_TRAY_NO)));
	m_wndTrayGrid.SetStyleRange(CGXRange(0, _GRID_COL_LOTNO), CGXStyle().SetValue(::GetStringTable(IDS_LABEL_LOT_NO )));
	m_wndTrayGrid.SetStyleRange(CGXRange(0, _GRID_COL_CELLNO), CGXStyle().SetValue(::GetStringTable(IDS_LABEL_CELL_NO)));
	m_wndTrayGrid.SetStyleRange(CGXRange(0, _GRID_COL_FILENAME), CGXStyle().SetValue(::GetStringTable(IDS_LABEL_FILE_NAME)));
	m_wndTrayGrid.SetStyleRange(CGXRange(0, _GRID_COL_TESTSERIAL), CGXStyle().SetValue("Test Serial"));

	CString str;
	for(int i=0; i<m_lpModuleInfo->GetTotalJig(); i++)
	{
		str.Format("%d", i+1);
		m_wndTrayGrid.SetStyleRange(CGXRange(i+1, _GRID_COL_JIGNO), CGXStyle().SetValue(str));
	}
}

LRESULT CRunDlg::OnMovedCurrentCell(WPARAM wParam, LPARAM lParam)
{
	return 1;
}


LRESULT CRunDlg::OnStartEditing(WPARAM wParam, LPARAM lParam)
{
//	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;				//Get Grid	
	ROWCOL nRow, nCol;			
	nCol = wParam & 0x0000ffff;		
	nRow = wParam >> 16;	//Get Current Row Col
	if(nRow < 1 || nCol < 1)	
		return 0;

	return 1;
}

LRESULT CRunDlg::OnEndEditing(WPARAM wParam, LPARAM lParam)
{
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;		//Get Grid	
	ROWCOL nRow, nCol;			
	nCol = LOWORD(wParam);  
	nRow = HIWORD(wParam);				//Get Current Row Col
	if(nRow < 1)	return 0;
	
	if(pGrid == (CMyGridWnd *)&m_wndTrayGrid)
	{
		if(nCol == _GRID_COL_INPUTCOUT)	//입력 수량 변경 
		{
			CString str;
			m_nInputCellCount = SumUserInputCellCount();
			str.Format("%d", m_nInputCellCount);
			GetDlgItem(IDC_INPUT_CELL_COUNT)->SetWindowText(str);
		}
		else if(nCol == _GRID_COL_TRAYNO)	//Tray명 변경시
		{
			m_wndTrayGrid.SetValueRange(CGXRange(nRow, _GRID_COL_FILENAME), RemakeFileName(nRow-1));
		}

		//Tray번호와 Cell 수량이 모두 입력되어 있으면 
		if(nCol == _GRID_COL_INPUTCOUT || nCol == _GRID_COL_TRAYNO)
		{
			UpdateBtnState();
		}
	}

	return 1;
}

LRESULT CRunDlg::OnModifyEdit(WPARAM wParam, LPARAM lParam)
{
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;		//Get Grid	
	ROWCOL nRow, nCol;			
	nCol = LOWORD(wParam);  
	nRow = HIWORD(wParam);				//Get Current Row Col
	if(pGrid == (CMyGridWnd *)&m_wndTrayGrid)
	{
		if(nCol == _GRID_COL_INPUTCOUT)	//입력 수량 변경 
		{
			CString str;
			m_nInputCellCount = SumUserInputCellCount();
			str.Format("%d", m_nInputCellCount);
			GetDlgItem(IDC_INPUT_CELL_COUNT)->SetWindowText(str);
		}
		else if(nCol == _GRID_COL_TRAYNO)	//Tray명 변경시
		{
			m_wndTrayGrid.SetValueRange(CGXRange(nRow, _GRID_COL_FILENAME), RemakeFileName(nRow-1));
		}

		//Tray번호와 Cell 수량이 모두 입력되어 있으면 
		if(nCol == _GRID_COL_INPUTCOUT || nCol == _GRID_COL_TRAYNO)
		{
			UpdateBtnState();
		}
	}

	return 1;
}

//모든 입력된 수량의 Cell을 더한다.
int CRunDlg::SumUserInputCellCount()
{
	long nCnt = 0;
	long nInput, nTraycount = 0;
	for(int row=0;  row<m_wndTrayGrid.GetRowCount(); row++)
	{
		nInput = atol(m_wndTrayGrid.GetValueRowCol(row+1, _GRID_COL_INPUTCOUT));
		if(nInput > 0)
		{
			nTraycount++;
			nCnt += nInput;

			m_wndTrayGrid.SetStyleRange(CGXRange(row+1, _GRID_COL_TRAYNO, row+1, _GRID_COL_FILENAME), CGXStyle().SetEnabled(TRUE));
		}
		else
		{
			m_wndTrayGrid.SetStyleRange(CGXRange(row+1, _GRID_COL_TRAYNO, row+1, _GRID_COL_FILENAME), CGXStyle().SetEnabled(FALSE));

		}
	}

	CString str;
	str.Format("Total %dEA TRAY", nTraycount);
	GetDlgItem(IDC_TRAY_STATIC)->SetWindowText(str);

	str.Format("%d", nCnt);
	GetDlgItem(IDC_INPUT_CELL_COUNT)->SetWindowText(str);

	return (int)nCnt;
}

void CRunDlg::UpdateBtnState()
{
	CString str;
	BOOL bStart = TRUE;
	//작업 시작으로 변경한다.
	for(int i=0; i<m_wndTrayGrid.GetRowCount(); i++)
	{
		if(atol(m_wndTrayGrid.GetValueRowCol(i+1, _GRID_COL_INPUTCOUT)) > 0)
		{
			str = m_wndTrayGrid.GetValueRowCol(i+1, _GRID_COL_TRAYNO);	//TrayNo 입력 완료 
			if(str.IsEmpty())
			{
				bStart = FALSE;
				}
		}
	}
	if(bStart)
	{
		GetDlgItem(IDOK)->SetWindowText("Start");
	}
	else
	{
		GetDlgItem(IDOK)->SetWindowText("Standby");
	}

}

LRESULT CRunDlg::OnBcrscaned(UINT wParam, LONG lParam)
{
	char *data = (char *)lParam;
	CString str(data);
	
	if(str.IsEmpty())	return 0;

	//Tray 위치 정보를 검색 
	if(wParam == TAG_TYPE_JIG)
	{
		CJigIDRegDlg *pDlg = new CJigIDRegDlg(this);
		int nModuleID, nJig;
		if(pDlg->SearchLocation(str, nModuleID, nJig))
		{
			if(nModuleID == m_lpModuleInfo->GetModuleID() && nJig <= m_wndTrayGrid.GetRowCount())
			{
				m_wndTrayGrid.SetCurrentCell(nJig, _GRID_COL_TRAYNO);	
			}
		}
		delete pDlg;
	}
	else if(wParam == TAG_TYPE_TRAY)
	{
		ROWCOL nRow, nCol;
		if(m_wndTrayGrid.GetCurrentCell(nRow, nCol))
		{
			m_wndTrayGrid.SetStyleRange(CGXRange(nRow, _GRID_COL_TRAYNO), CGXStyle().SetValue(str));	
		}
	}
	else if(wParam == TAG_TYPE_LOT)
	{
		GetDlgItem(IDC_LOT_NO)->SetWindowText(str);
	}
	else
	{
		//Unkonow
	}

	return 1;
}

void CRunDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnOK();
}
