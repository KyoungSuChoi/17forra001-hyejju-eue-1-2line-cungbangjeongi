#if !defined(AFX_USERADMINDLG_H__7179ABEA_A9D7_4384_9557_D0B2F841404D__INCLUDED_)
#define AFX_USERADMINDLG_H__7179ABEA_A9D7_4384_9557_D0B2F841404D__INCLUDED_

#include "MyGridWnd.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UserAdminDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CUserAdminDlg dialog
//#include "Label.h"

class CUserAdminDlg : public CDialog
{
// Construction
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();
	CUserAdminDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CUserAdminDlg();

	STR_LOGIN *m_pLoginData;
	CUserRecordSet	m_recordSet;
	BOOL RequeryUserDB();
	CMyGridWnd m_UserListGrid;
	BOOL InitUserListGrid();
	
	BOOL SearchUser(CString strUserID, BOOL bPassWordCheck = TRUE, CString strPassword = "" , STR_LOGIN *pUserData = NULL);

// Dialog Data
	//{{AFX_DATA(CUserAdminDlg)
	enum { IDD = IDD_USER_ADMIN_DLG };
	CLabel	m_ctrlCount;
	BOOL	m_bUseLoginCheck;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUserAdminDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CUserAdminDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnAddUser();
	afx_msg void OnDeleteUser();
	afx_msg void OnOk();
	//}}AFX_MSG
	afx_msg LONG OnGridDoubleClick(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_USERADMINDLG_H__7179ABEA_A9D7_4384_9557_D0B2F841404D__INCLUDED_)
