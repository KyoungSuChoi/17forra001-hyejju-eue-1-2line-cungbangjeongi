// ProgressProcDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "CTSAnalDoc.h"
#include "ProgressProcDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CProgressProcDlg dialog


bool CProgressProcDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CProgressProcDlg"), _T("TEXT_CProgressProcDlg_CNT"), _T("TEXT_CProgressProcDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CProgressProcDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CProgressProcDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

CProgressProcDlg::CProgressProcDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CProgressProcDlg::IDD, pParent)
{
	LanguageinitMonConfig();

	//{{AFX_DATA_INIT(CProgressProcDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_pDBServer = NULL;
}

CProgressProcDlg::~CProgressProcDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}



void CProgressProcDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CProgressProcDlg)
	DDX_Control(pDX, IDC_TYPE_SELECT, m_dataTypeSelect);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CProgressProcDlg, CDialog)
	//{{AFX_MSG_MAP(CProgressProcDlg)
	ON_WM_CLOSE()
	ON_CBN_SELCHANGE(IDC_TYPE_SELECT, OnSelchangeTypeSelect)
	ON_BN_CLICKED(IDC_ALL_DATA_RADIO, OnAllDataRadio)
	ON_BN_CLICKED(IDC_ENDED_DATA_RADIO, OnEndedDataRadio)
	ON_BN_CLICKED(IDC_WORKING_DATA_RADIO, OnWorkingDataRadio)
	ON_BN_CLICKED(IDC_DELETE_PROC_BUTTON, OnDeleteProcButton)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_GRID_MOVECELL, OnMovedCurrentCell)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CProgressProcDlg message handlers

BOOL CProgressProcDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	InitProcListGrid();
	InitDataListGrid();

	m_dataTypeSelect.AddString(TEXT_LANG[0]);//"Model별 보기"
	m_dataTypeSelect.AddString(TEXT_LANG[1]);//"Lot별 보기"
	m_dataTypeSelect.AddString(TEXT_LANG[2]);//"Tray별 보기"
	m_dataTypeSelect.SetCurSel(DATA_MODEL);

	if(m_pDBServer)
		RequeryDataSerarch(m_strTestLogID);
//		RequeryDataSerarch(DATA_MODEL);

	((CButton *)GetDlgItem(IDC_ALL_DATA_RADIO))->SetCheck(TRUE);
	m_nDspType =0;
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CProgressProcDlg::InitProcListGrid()
{
	m_wndProcList.SubclassDlgItem(IDC_PROC_LIST_GRID, this);
	m_wndProcList.m_bSameRowSize = FALSE;
	m_wndProcList.m_bSameColSize = FALSE;
//---------------------------------------------------------------------//
	m_wndProcList.m_bCustomWidth = TRUE;
	m_wndProcList.m_bCustomColor = FALSE;

	m_wndProcList.Initialize();
	m_wndProcList.LockUpdate();

	m_wndProcList.SetColCount(8);        
	m_wndProcList.SetDefaultRowHeight(20);
	m_wndProcList.EnableCellTips();

	//Row Header Setting
	m_wndProcList.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(9).SetFaceName(TEXT_LANG[3])).SetWrapText(FALSE).SetAutoSize(TRUE));//"굴림"
	m_wndProcList.SetStyleRange(CGXRange().SetCols(1),	CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(0,0,0)));
		
	m_wndProcList.m_nWidth[1] = 60;
	m_wndProcList.m_nWidth[2] = 0;			//공정명과 공정 Type명이 동일 하므로 표기 할 필요 없음 
	m_wndProcList.m_nWidth[3] = 150;
	m_wndProcList.m_nWidth[4] = 80;
	m_wndProcList.m_nWidth[5] = 80;
	m_wndProcList.m_nWidth[6] = 130;
	m_wndProcList.m_nWidth[7] = 130;
	m_wndProcList.m_nWidth[8] = 150;

	m_wndProcList.SetValueRange(CGXRange( 0, 1), TEXT_LANG[4]);//"공정순서"
	m_wndProcList.SetValueRange(CGXRange( 0, 2), TEXT_LANG[5]);//"공정명"
	m_wndProcList.SetValueRange(CGXRange( 0, 3), TEXT_LANG[6]);//"공정 Type"
	m_wndProcList.SetValueRange(CGXRange( 0, 4), TEXT_LANG[7]);//"입력수량"
	m_wndProcList.SetValueRange(CGXRange( 0, 5), TEXT_LANG[8]);//"정상수"
	m_wndProcList.SetValueRange(CGXRange( 0, 6), TEXT_LANG[9]);//"Stage"
	m_wndProcList.SetValueRange(CGXRange( 0, 7), TEXT_LANG[10]);//"작업자"
	m_wndProcList.SetValueRange(CGXRange( 0, 8), TEXT_LANG[11]);//"작업시각"

	m_wndProcList.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	
//	m_wndProcList.SetScrollBarMode(SB_BOTH, gxnDisabled,TRUE);
	m_wndProcList.LockUpdate(FALSE);
	m_wndProcList.Redraw();	
	
	return TRUE;
}

BOOL CProgressProcDlg::InitDataListGrid()
{
	m_wndDataList.SubclassDlgItem(IDC_ITEM_LIST_GRID, this);
	m_wndDataList.m_bSameRowSize = FALSE;
	m_wndDataList.m_bSameColSize = FALSE;
//---------------------------------------------------------------------//
	m_wndDataList.m_bCustomWidth = TRUE;
	m_wndDataList.m_bCustomColor = FALSE;

	m_wndDataList.Initialize();
	m_wndDataList.LockUpdate();

	m_wndDataList.SetColCount(11);        
	m_wndDataList.SetDefaultRowHeight(20);
	m_wndDataList.EnableCellTips();
	m_wndDataList.GetParam()->SetSortRowsOnDblClk(TRUE);
	
	m_wndDataList.m_nWidth[1] = 50;
	m_wndDataList.m_nWidth[2] = 50;			//공정명과 공정 Type명이 동일 하므로 표기 할 필요 없음 
	m_wndDataList.m_nWidth[3] = 120;
	m_wndDataList.m_nWidth[4] = 150;
	m_wndDataList.m_nWidth[5] = 80;
	m_wndDataList.m_nWidth[6] = 70;
	m_wndDataList.m_nWidth[7] = 70;
	m_wndDataList.m_nWidth[8] = 70;
	m_wndDataList.m_nWidth[9] = 60;
	m_wndDataList.m_nWidth[10] = 60;

	//Row Header Setting
	m_wndDataList.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(9).SetFaceName(TEXT_LANG[3])).SetWrapText(FALSE).SetAutoSize(TRUE));//"굴림"
	m_wndDataList.SetStyleRange(CGXRange().SetCols(1),	CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(0,0,0)));
		
	m_wndDataList.SetValueRange(CGXRange( 0, 1), TEXT_LANG[12]);//"순서"
	m_wndDataList.SetValueRange(CGXRange( 0, 2), TEXT_LANG[13]);//"StepNo"
	m_wndDataList.SetValueRange(CGXRange( 0, 3), TEXT_LANG[14]);//"StepType"
	m_wndDataList.SetValueRange(CGXRange( 0, 4), TEXT_LANG[15]);//"작업완료시간"
	m_wndDataList.SetValueRange(CGXRange( 0, 5), TEXT_LANG[16]);//"Data Type"
	m_wndDataList.SetValueRange(CGXRange( 0, 6), TEXT_LANG[17]);//"평균"
	m_wndDataList.SetValueRange(CGXRange( 0, 7), TEXT_LANG[18]);//"최소값"
	m_wndDataList.SetValueRange(CGXRange( 0, 8), TEXT_LANG[19]);//"최대값"
	m_wndDataList.SetValueRange(CGXRange( 0, 9), TEXT_LANG[8]);//"정상수"
	m_wndDataList.SetValueRange(CGXRange( 0, 10), TEXT_LANG[7]);//"입력수"

	m_wndDataList.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	
//	m_wndDataList.SetScrollBarMode(SB_BOTH, gxnDisabled,TRUE);
	m_wndDataList.LockUpdate(FALSE);
	m_wndDataList.Redraw();
	return TRUE;
}

void CProgressProcDlg::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
/*	try
	{
		m_TestLog.Close();
		m_Module.Close();
		m_Test.Close();
		m_ChValue.Close();
		m_ChResult.Close();
	}
	catch(CDBException *e)
	{
		e->Delete();
	}
*/
	CDialog::OnClose();
}

BOOL CProgressProcDlg::RequeryDataSerarch(int dataType)
{
	CTestLogSet testLogSet;
	testLogSet.m_strSort.Format("[TestLogID]");

	try
	{
		if(testLogSet.Open() == FALSE)
			return FALSE;
	}
	catch(CDBException *e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();
		return FALSE;
	}

	m_ptArray.RemoveAll();
	int i = 0;

	while(!testLogSet.IsEOF())
	{
		for(i=0; i < m_ptArray.GetSize(); i++)
		{
			DATA_LIST& data = m_ptArray.ElementAt(i);
			
			if(dataType == DATA_LOT)		//Lot별 보기 
			{
				if(data.m_Name == testLogSet.m_LotNo)		//같은 Lot가 있으면 
				{
					data.count++;
					break;
				}
			}
			else if(dataType == DATA_TRAY)	//Tray 별 보기 
			{
				if(data.m_Name == testLogSet.m_TrayNo)		//같은 Tray 있으면 
				{
					data.count++;
					break;
				}
			}
			else					//Model 별 보기
			{
				if(data.m_Name == testLogSet.m_ModelName)	//같은 Model 
				{
					data.count++;
					break;
				}
			}
		}
		
		if(i >= m_ptArray.GetSize())
		{
			DATA_LIST newData;
			if(dataType == DATA_LOT)		//Lot별 보기 
			{
				newData.m_Name = testLogSet.m_LotNo;
			}
			else if(dataType == DATA_TRAY)	//Tray 별 보기 
			{
				newData.m_Name = testLogSet.m_TrayNo;
			}
			else					//Model 별 보기
			{
				newData.m_Name = testLogSet.m_ModelName;
			}
			newData.count = 1;
			m_ptArray.Add(newData);
//			TRACE("New Lot [%s] Added %d\n", newData.m_Name, newData.count);
		}
		testLogSet.MoveNext();
	}
	
	UpdateDataList();
	
	testLogSet.Close();
	return TRUE;
}

void CProgressProcDlg::UpdateDataList()
{
	int count = m_wndDataList.GetRowCount();
	if(count > 0)
	{
		m_wndDataList.RemoveRows(1, count);
	}

	if(m_ptArray.GetSize() <= 0)	return;

	m_wndDataList.InsertRows(1, m_ptArray.GetSize());
	for (int i=0; i < m_ptArray.GetSize(); i++)
	{
		DATA_LIST& data = m_ptArray.ElementAt(i);
		TRACE("Lot [%s] Added %d\n", data.m_Name, data.count);
		m_wndDataList.SetValueRange(CGXRange(i+1, 1), (LONG)(i+1));
		m_wndDataList.SetValueRange(CGXRange(i+1, 2), data.m_Name);
		m_wndDataList.SetValueRange(CGXRange(i+1, 3), (LONG)data.count);
	}
}

void CProgressProcDlg::OnSelchangeTypeSelect() 
{
	// TODO: Add your control notification handler code here
	
	if(m_dataTypeSelect.GetCurSel() == DATA_TRAY)
	{
		
	}
	RequeryDataSerarch(m_dataTypeSelect.GetCurSel());
}

void CProgressProcDlg::RequeryTestList(int nType)
{
	CTestLogSet			testLogSet;
	CModuleRecordSet    moduleLogSet;

	m_ptTestArray.RemoveAll();
		
	testLogSet.m_strSort.Format("[TestLogID]");
	if(nType == DATA_LOT)
	{
		testLogSet.m_strFilter.Format("[LotNo] = '%s'", m_strData);
	}
	else if(nType == DATA_TRAY)
	{
		testLogSet.m_strFilter.Format("[TrayNo] = '%s'", m_strData);
	}
	else
	{
		testLogSet.m_strFilter.Format("[ModelID] = '%s'", m_strData);
	}

	if(m_nDspType == 1)	//완료된 Data 보기
	{
		testLogSet.m_strFilter += " AND [TestDone] = TRUE";
	}
	else if(m_nDspType == 2)	//진행중인 Data
	{
		testLogSet.m_strFilter += " AND [TestDone] = FALSE";
	}

	moduleLogSet.m_strSort.Format("[ProcedureID] ASC");
	try
	{
		if(testLogSet.Open() == FALSE)	return;
		if(moduleLogSet.Open() == FALSE)	return;
	}
	catch(CDBException *e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();
		return;
	}

	CArray<PROC_LIST ,PROC_LIST> ptTempArray;

	while(!testLogSet.IsEOF())
	{
		moduleLogSet.m_strFilter.Format("[TestLogID] = %ld AND [TestSerialNo] = '%s'", testLogSet.m_TestLogID, testLogSet.m_TestSerialNo);
		moduleLogSet.Requery();

		ptTempArray.RemoveAll();
		int i = 0, j = 0;

		while(!moduleLogSet.IsEOF())
		{
			//각 공정이 중복된 시험이 있을 경우 최종 시험 Data를 선택하여 각 공정별 결과 값을 찾는다.
			for(i=0; i < ptTempArray.GetSize(); i++)
			{
				PROC_LIST& data = ptTempArray.ElementAt(i);
				
				if(data.dataID == moduleLogSet.m_TestID)	//같은 시험 이름  
				{
					data.count++;
				//	data.dataID = moduleLogSet.m_TestID;
					data.m_Name = moduleLogSet.m_TestName;
					break;
				}
			}
			
			if(i >= ptTempArray.GetSize())		
			{
				PROC_LIST newData;
				newData.m_Name = moduleLogSet.m_TestName;
				newData.dataID = moduleLogSet.m_TestID;
				newData.count = 1;
				newData.strSerial.Format("%s", moduleLogSet.m_TestSerialNo); 

				ptTempArray.Add(newData);
				TRACE("New Lot [%s] Added %d\n", newData.m_Name, newData.count);
			}	
			moduleLogSet.MoveNext();
		}
			
		for(i=0; i < ptTempArray.GetSize(); i++)
		{
			PROC_LIST& data1 = ptTempArray.ElementAt(i);
			for(j =0; j<m_ptTestArray.GetSize(); j++)
			{
				PROC_LIST& data2 = m_ptTestArray.ElementAt(j);
				if(data2.dataID == data1.dataID)	//같은 시험 이름  
				{
					data2.count++;
					RequeryCellCount(data2.dataID, data2.strSerial, data2.nInputNo, data2.nNormalNo);
					break;
				}
			}
			if(j >= m_ptTestArray.GetSize())
			{
				PROC_LIST newData;
				newData.m_Name = data1.m_Name;
				newData.dataID = data1.dataID;
				newData.count = 1;
				newData.strSerial = data1.strSerial;
				newData.nInputNo = 0;
				newData.nNormalNo = 0;
				RequeryCellCount(data1.dataID, data1.strSerial, newData.nInputNo, newData.nNormalNo);
		//		TRACE("New Lot [%s] Added %d\n", newData.m_Name, newData.count);
				m_ptTestArray.Add(newData);
			}	
		}
		testLogSet.MoveNext();
	}

	UpdateTestList();
	
	testLogSet.Close();
	moduleLogSet.Close();
}

LONG CProgressProcDlg::OnMovedCurrentCell(WPARAM wParam, LPARAM lParam)
{
	ROWCOL nRow, nCol;
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;
	nCol = LOWORD(wParam);
	nRow = HIWORD(wParam);
	if(nRow<1 || nCol <1)		return 0;

	if(pGrid == &m_wndProcList)
	{
		CString ProcID = pGrid->GetValueRowCol(nRow, 0);
		RequeryTestList(ProcID);
	}
	
	return 0;
}

void CProgressProcDlg::UpdateTestList()
{
	int count = m_wndProcList.GetRowCount();
	if(count > 0)
	{
		m_wndProcList.RemoveRows(1, count);
	}

	if(m_ptTestArray.GetSize() <=0)		return;

	m_wndProcList.InsertRows(1, m_ptTestArray.GetSize());
	CString strTemp;
	int prevInput = 0;

	for (int i=0; i < m_ptTestArray.GetSize(); i++)
	{
		PROC_LIST& data = m_ptTestArray.ElementAt(i);
		TRACE("Lot [%s] Added %d\n", data.m_Name, data.count);
		m_wndProcList.SetValueRange(CGXRange(i+1, 1), (LONG)(i+1));
		m_wndProcList.SetValueRange(CGXRange(i+1, 2), data.m_Name);
		m_wndProcList.SetValueRange(CGXRange(i+1, 3), (LONG)data.nInputNo);
		m_wndProcList.SetValueRange(CGXRange(i+1, 4), (LONG)data.nNormalNo);
		if(data.nInputNo == 0)
		{
			strTemp.Empty();
		}
		else
		{
			strTemp.Format("%.1f%%", (float)(data.nInputNo - data.nNormalNo)/(float)data.nInputNo*100.0f);
		}
		m_wndProcList.SetValueRange(CGXRange(i+1, 5), strTemp);
		
		if(i==0)
		{
			prevInput = data.nNormalNo;
		}
		
		if(prevInput == 0)
		{
			strTemp.Empty();
		}
		else
		{
			strTemp.Format("%.1f%%", (float)(prevInput-data.nNormalNo)/(float)prevInput*100.0f);
		}
		
		m_wndProcList.SetValueRange(CGXRange(i+1, 6), strTemp);
		m_wndProcList.SetValueRange(CGXRange(i+1, 7), (LONG)data.count);
		prevInput = data.nNormalNo;
	}
	
}

BOOL CProgressProcDlg::RequeryCellCount(long lTestID, CString strTestSerialNo, int &nInputCount, int &nNormalCount, long nProcdureID)
{
	CChResultSet	resultSet;
	
	if(nProcdureID == 0)
	{
		resultSet.m_strFilter.Format("[TestID] = %ld AND [TestSerialNo] LIKE %s", lTestID, strTestSerialNo);
	}
	else
	{
		resultSet.m_strFilter.Format("[ProcedureID] = %ld", nProcdureID);
	}
	
	resultSet.m_strSort.Format("[Index] DESC");
	resultSet.Open();

	int nNormal = 0, nInput = 0;
	if(!resultSet.IsBOF())
	{
		for(int i = 0; i<EP_BATTERY_PER_TRAY; i++)
		{
			if(!IsNonCell((BYTE)resultSet.m_ch[i]))		nInput++;
			if(IsNormalCell((BYTE)resultSet.m_ch[i]))		nNormal++;
		}
	}
	nInputCount += nInput;
	nNormalCount += nNormal;
	resultSet.Close();
	return TRUE;
}

void CProgressProcDlg::OnAllDataRadio() 
{
	// TODO: Add your control notification handler code here
	m_nDspType = 0;
	RequeryTestList(m_dataTypeSelect.GetCurSel());
}

void CProgressProcDlg::OnEndedDataRadio() 
{
	// TODO: Add your control notification handler code here
	m_nDspType = 1;
	RequeryTestList(m_dataTypeSelect.GetCurSel());
}

void CProgressProcDlg::OnWorkingDataRadio() 
{
	// TODO: Add your control notification handler code here
	m_nDspType = 2;
	RequeryTestList(m_dataTypeSelect.GetCurSel());
}

void CProgressProcDlg::SetDataBase(CDatabase *pDB, CString strTestLogID)
{
	m_pDBServer = pDB;
	if(!strTestLogID.IsEmpty())
		m_strTestLogID = strTestLogID;
}

BOOL CProgressProcDlg::RequeryDataSerarch(CString strTestLogID)
{
	//삭제 하고자 하는 TestSerilNo 검색 
	CString strSQL, strQuery;
	strQuery = "SELECT a.ProcedureID, a.ModuleID, a.TestName, a.DateTime, a.TestSerialNo, b.Description, a.LotNo, a.TrayNo, a.UserID, c.ModelName, c.CellNo";
	strSQL += strQuery;
	strQuery.Format(" FROM Module a, ProcType b , TestLog c WHERE c.TestLogID = a.TestLogID AND a.TestLogID = %s AND a.ProcedureType = b.ProcType ORDER BY a.ProcedureID", strTestLogID);
	strSQL += strQuery;

	ASSERT(m_pDBServer);
	CRecordset rs(m_pDBServer);

	try
	{
		if(rs.Open(CRecordset::forwardOnly, strSQL) == FALSE)
			return FALSE;
	}
	catch(CDBException *e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();
		return FALSE;
	}

	int nData;
	if((nData = m_wndProcList.GetRowCount()) > 0)
		m_wndProcList.RemoveRows(1, nData);

	CDBVariant	va;
	CString str, strTestSerialNo;
	int nCount = 1;
	int nProcdureID, nInputCount = 0, nNormalCount = 0, lTestID = 0;
	int nPrevInput = -1;
	while(!rs.IsEOF())
	{
		nInputCount = 0, nNormalCount = 0;
		m_wndProcList.InsertRows(nCount, 1);
		str.Format("%3d", nCount);
		m_wndProcList.SetValueRange(CGXRange(nCount, 1), str);

		rs.GetFieldValue((short)0, va, SQL_C_SLONG);		//Procedure ID
		str.Format("%d", va.m_lVal);
		nProcdureID =  va.m_lVal;
		m_wndProcList.SetValueRange(CGXRange(nCount, 0), str);

		rs.GetFieldValue((short)1, va, SQL_C_SLONG);		//Module ID	
		str.Format("Rack %d-%d", (va.m_lVal-1)/3+1, (va.m_lVal-1)%3+1);
		m_wndProcList.SetValueRange(CGXRange(nCount, 6), str);

		rs.GetFieldValue((short)2, str);					//Test Name
		m_wndProcList.SetValueRange(CGXRange(nCount, 3), str);

		rs.GetFieldValue((short)3, str);					//Date Time
		m_wndProcList.SetValueRange(CGXRange(nCount, 8), str);

		rs.GetFieldValue((short)4, strTestSerialNo);					//TestSerialNo

		rs.GetFieldValue((short)5, str);					//Procedure Type
		m_wndProcList.SetValueRange(CGXRange(nCount, 2), str);
		
		rs.GetFieldValue((short)6, str);					//Lot
		GetDlgItem(IDC_LOT_NO_STATIC)->SetWindowText(str);

		rs.GetFieldValue((short)7, str);					//Tray
		GetDlgItem(IDC_TRAY_NO_STATIC)->SetWindowText(str);

		rs.GetFieldValue((short)8, str);					//User
		m_wndProcList.SetValueRange(CGXRange(nCount, 7), str);
		
		rs.GetFieldValue((short)9, str);					//ModelName
		GetDlgItem(IDC_MODEL_STATIC)->SetWindowText(str);

		rs.GetFieldValue((short)10, va, SQL_C_SLONG);		//Cell No

		RequeryCellCount(lTestID, strTestSerialNo, nInputCount, nNormalCount, nProcdureID);
		
		if(nPrevInput < 0)
		{
			m_wndProcList.SetValueRange(CGXRange(nCount, 4), (ROWCOL)nInputCount);
		}
		else
		{
			m_wndProcList.SetValueRange(CGXRange(nCount, 4), (ROWCOL)nPrevInput);
		}
		nPrevInput = nNormalCount;
		m_wndProcList.SetValueRange(CGXRange(nCount, 5), (ROWCOL)nNormalCount);


		nCount++;
		rs.MoveNext();
	}
	rs.Close();
	
	str.Format(TEXT_LANG[20], nCount-1);//"%d 공정이 진행 되었습니다."
	GetDlgItem(IDC_COUNT_STATIC)->SetWindowText(str);

	return TRUE;
}

BOOL CProgressProcDlg::RequeryTestList(CString strProcID)
{
	if(strProcID.IsEmpty())		return FALSE;
	
	CString strQuery, strSQL;
	strQuery = "SELECT StepNo, ProcType, DateTime, DataType, Average, MinVal, MaxVal, NormalNo, Totalchannel FROM Test WHERE";
	strSQL += strQuery;
	strQuery.Format(" ProcedureID =  %s ORDER BY StepNo", strProcID);
	strSQL += strQuery;

	ASSERT(m_pDBServer);
	CDatabase conditionDB;

	try
	{
		conditionDB.Open(ODBC_SCH_DB_NAME);
	}
	catch (CDBException* e)
	{
    	AfxMessageBox(e->m_strError);
    	e->Delete();     //DataBase Open Fail
		return FALSE;
	}

	CRecordset rs(m_pDBServer);
	CRecordset rs1(&conditionDB);

	try
	{
		if(rs.Open(CRecordset::forwardOnly, strSQL) == FALSE)
			return FALSE;
	}
	catch(CDBException *e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();
		return FALSE;
	}

	//Clear Data List
	int nData;
	if((nData = m_wndDataList.GetRowCount()) > 0)
		m_wndDataList.RemoveRows(1, nData);

	CDBVariant	va;
	int nCount = 1;
	long	nDataType;
	CString str, strAvg, strMin, strMax;
	float fAvg, fMin, fMax;
	while(!rs.IsEOF())
	{
		m_wndDataList.InsertRows(nCount, 1);
		
		str.Format("%3d", nCount);
		m_wndDataList.SetValueRange(CGXRange(nCount, 1), str);

		rs.GetFieldValue((short)0, va, SQL_C_SLONG);		//Step No
		str.Format("%d", va.m_lVal);
		m_wndDataList.SetValueRange(CGXRange(nCount, 2), str);

		rs.GetFieldValue((short)1, va, SQL_C_SLONG);		//ProcType	
		str.Format("%d", va.m_lVal);
		strSQL.Format("SELECT Description FROM ProcType WHERE ProcType = %d", va.m_lVal);
		
		try
		{
			rs1.Open(CRecordset::forwardOnly, strSQL);
		}
		catch(CDBException *e)
		{
			e->Delete();
		}
		
		if(!rs1.IsEOF())
		{
			rs1.GetFieldValue((short)0, str);
		}
		if(rs1.IsOpen())	rs1.Close();

		m_wndDataList.SetValueRange(CGXRange(nCount, 3), str);

		rs.GetFieldValue((short)2, str);					//DateTime
		m_wndDataList.SetValueRange(CGXRange(nCount, 4), str);

		rs.GetFieldValue((short)3, va, SQL_C_SLONG);		//Data Type
		nDataType = va.m_lVal;

		rs.GetFieldValue((short)4, va, SQL_C_FLOAT);		//Average
		fAvg = va.m_fltVal;
		rs.GetFieldValue((short)5, va, SQL_C_FLOAT);		//Min
		fMin = va.m_fltVal;
		rs.GetFieldValue((short)6, va, SQL_C_FLOAT);		//Max
		fMax = va.m_fltVal;

		switch(nDataType)
		{
		case EP_VOLTAGE		:	
			str = TEXT_LANG[21];			//"전압"
			strAvg.Format("%.3f", fAvg);		strMin.Format("%.3f", fMin);		strMax.Format("%.3f", fMax);
			break;
		case EP_CURRENT		:
			str = TEXT_LANG[22];//"전류"
			if(fAvg < 0)	fAvg = -fAvg;
			if(fMin < 0)	fMin = -fMin;
			if(fMax < 0)	fMax = -fMax;

			strAvg.Format("%.1f", fAvg);		strMin.Format("%.1f", fMin);		strMax.Format("%.1f", fMax);
			break;
		case EP_CAPACITY	:
			str = TEXT_LANG[23];			//"용량"
			strAvg.Format("%.1f", fAvg);		strMin.Format("%.1f", fMin);		strMax.Format("%.1f", fMax);			
			break;
		case EP_IMPEDANCE	:
			str = TEXT_LANG[24];		//"DCIR"
			strAvg.Format("%.1f", fAvg);		strMin.Format("%.1f", fMin);		strMax.Format("%.1f", fMax);			
			break;
		case EP_TOT_TIME	:
		case EP_STEP_TIME	:	
			str = TEXT_LANG[25];		//"진행시간"
			strAvg.Format("%.1f", fAvg);		strMin.Format("%.1f", fMin);		strMax.Format("%.1f", fMax);			
			break;
		case EP_WATT		:
			str = TEXT_LANG[26];//"Watt"
			strAvg.Format("%.1f", fAvg);		strMin.Format("%.1f", fMin);		strMax.Format("%.1f", fMax);			
			break;
		case EP_WATT_HOUR	:
			str = TEXT_LANG[27]; //"WattHour"
			strAvg.Format("%.1f", fAvg);		strMin.Format("%.1f", fMin);		strMax.Format("%.1f", fMax);			
			break;
//		case EP_OCV			:
//			str = "OCV"; 	
//			strAvg.Format("%.3f", fAvg);		strMin.Format("%.3f", fMin);		strMax.Format("%.3f", fMax);			
//			break;
		case EP_STATE		:
			str = TEXT_LANG[28];//"상태"
			strAvg.Empty();	strMin.Empty();	strMax.Empty();			
			break;				
		case EP_CH_CODE		:
			str = TEXT_LANG[29];//"Cell Code"
			strAvg.Empty();	strMin.Empty();	strMax.Empty();			
			break;
		case EP_GRADE_CODE	:
			str = TEXT_LANG[30];//"Grade Code"
			strAvg.Empty();	strMin.Empty();	strMax.Empty();			
			break;
		case EP_STEP_NO		:
			str = TEXT_LANG[31];//"Step No"
			strAvg.Empty();	strMin.Empty();	strMax.Empty();			
			break;
		default				:
			str.Format("%d", nDataType);
			strAvg.Empty();	strMin.Empty();	strMax.Empty();			
			break;
		}
		
		m_wndDataList.SetValueRange(CGXRange(nCount, 5), str);
		m_wndDataList.SetValueRange(CGXRange(nCount, 6), strAvg);
		m_wndDataList.SetValueRange(CGXRange(nCount, 7), strMin);
		m_wndDataList.SetValueRange(CGXRange(nCount, 8), strMax);

		
		rs.GetFieldValue((short)7, va, SQL_C_SLONG);		//normal count
		str.Format("%d", va.m_lVal);
		m_wndDataList.SetValueRange(CGXRange(nCount, 9), str);
		rs.GetFieldValue((short)8, va, SQL_C_SLONG);		//input Count
		str.Format("%d", va.m_lVal);
		m_wndDataList.SetValueRange(CGXRange(nCount, 10), str);
		
		nCount++;
		rs.MoveNext();
	}
	rs.Close();
	conditionDB.Close();

	return TRUE;
}

void CProgressProcDlg::OnDeleteProcButton() 
{
	// TODO: Add your control notification handler code here
	CString strTemp;

	CRowColArray	awRows;
	m_wndProcList.GetSelectedRows(awRows);
	if(awRows.GetSize() <=0)	return;


	CString strTestSerialNo, strLot, strTrayNo;
	CString strSQL;
	CDBVariant va;
	ASSERT(m_pDBServer);
	CRecordset rs(m_pDBServer);

	//삭제 하고자 하는 TestSerilNo 검색 
	strSQL.Format("SELECT TestSerialNo, LotNo, TrayNo FROM Module WHERE ProcedureID = %s", m_wndProcList.GetValueRowCol(awRows[0], 0));
	try
	{
		if(rs.Open(CRecordset::forwardOnly, strSQL) == FALSE)
			return;
	}
	catch(CDBException *e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();
		return;
	}
	
	if(!rs.IsEOF())		 
	{
		rs.GetFieldValue((short)0, strTestSerialNo);		
		rs.GetFieldValue((short)1, strLot);		
		rs.GetFieldValue((short)2, strTrayNo);		
	}
	rs.Close();



	strTemp.Format(TEXT_LANG[32], strTrayNo, strLot,awRows.GetSize());//"TrayNo = [%s] & LotNo = [%s]로 진행된 %d 개의 공정 결과를 삭제 하시겠습니까?"
	if(IDNO == MessageBox(strTemp,TEXT_LANG[33], MB_YESNO|MB_ICONQUESTION))	return;//"삭제"

	for (int i = awRows.GetSize()-1; i >=0 ; i--)
	{
		//공정 삭제 
		DeleteModuleLogList(m_wndProcList.GetValueRowCol(awRows[i], 0));

		//Row 삭제 
		m_wndProcList.RemoveRows(awRows[i], awRows[i]);
	}

	ROWCOL count = m_wndProcList.GetRowCount();
	if(count < 1)	//모든 공정 정보가 삭제 되어 해당 공정 Log가 하나도 없으므로 [TestLog] Table에서 공정 시작 Log를 삭제 한다.
	{				//Tray 공정 Data도 초기화 시킨다.
		if(!strTestSerialNo.IsEmpty())
		{
			CString strSQL;
			strSQL.Format("DELETE FROM TestLog WHERE TestSerialNo = '%s'", strTestSerialNo);
		
			m_pDBServer->BeginTrans();
			m_pDBServer->ExecuteSQL(strSQL);
			m_pDBServer->CommitTrans();

			CDatabase conditionDB;
			try
			{
				conditionDB.Open(ODBC_SCH_DB_NAME);
			}
			catch (CDBException* e)
			{
    			AfxMessageBox(e->m_strError);
    			e->Delete();     //DataBase Open Fail
				return ;
			}

			CString strQuery;
			//Tray 공정 정보 Reset
			strSQL = "UPDATE Tray SET TestKey = 0, NormalCount = 0, FailCount = 0, OperatorID = '',"; 
			strQuery = "InputCellNo = 0, ModuleID = 0, GroupIndex = 0, LotNo =''";
			strSQL += strQuery;
			strQuery.Format(" WHERE TrayNo = '%s' AND TestSerialNo = '%s'", strTrayNo, strTestSerialNo);
			strSQL += strQuery;
			conditionDB.BeginTrans();
			conditionDB.ExecuteSQL(strSQL);
			conditionDB.CommitTrans();
			conditionDB.Close();
		}
	}
	else
	{
		UpdateLastTrayState(m_wndProcList.GetValueRowCol(count, 0));
	}

	strTemp.Format(TEXT_LANG[20], count);//"%d 공정이 진행 되었습니다."
	GetDlgItem(IDC_COUNT_STATIC)->SetWindowText(strTemp);
}

BOOL CProgressProcDlg::UpdateLastTrayState(CString strProcedureID)
{
	if(strProcedureID.IsEmpty())	return FALSE;
	CString strQuery, strSQL;
	CDBVariant va;

	ASSERT(m_pDBServer);
	CRecordset rs(m_pDBServer);

	//Tray 최종 정보 검색 
	strQuery= "SELECT b.ModuleID, b.GroupIndex, b.UserID, b.TrayNo, b.TestID, a.NormalNo, a.TotalChannel, a.DateTime, b.TestSerialNo FROM Test a, Module b";
	strSQL += strQuery;
	strQuery.Format(" WHERE a.ProcedureID = b.ProcedureID AND a.TestIndex = (SELECT MAX(TestIndex) FROM Test WHERE ProcedureID = %s)", strProcedureID);
	strSQL += strQuery;

	try
	{
		if(rs.Open(CRecordset::forwardOnly, strSQL) == FALSE)
			return FALSE;
	}
	catch(CDBException *e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();
		return FALSE;
	}
	
	int nModuleID, GroupIndex, TestID, NormalNo, InputCellCount;
	float ErrorRate; 
	CString userID, strTrayID, strDateTime, strTestSerialNo;
	if(!rs.IsEOF())		//최종 공정의 Tray 이력 정보를 검색 한다. 
	{
		rs.GetFieldValue((short)0, va, SQL_C_SLONG);		//ModuleID
		nModuleID = va.m_lVal;
		rs.GetFieldValue((short)1, va, SQL_C_SLONG);		//GroupIndex	
		GroupIndex = va.m_lVal;
		rs.GetFieldValue((short)2, userID);					//UserID			
		rs.GetFieldValue((short)3, strTrayID);				//TrayNo	
		rs.GetFieldValue((short)4, va, SQL_C_SLONG);		//TestID	
		TestID = va.m_lVal;
		rs.GetFieldValue((short)5, va, SQL_C_SLONG);		//Normal Count			
		NormalNo = va.m_lVal;
		rs.GetFieldValue((short)6, va, SQL_C_SLONG);		//Input Cell Count			
		InputCellCount = va.m_lVal;
		ErrorRate = float(InputCellCount-NormalNo)/(float)InputCellCount*100.0f;
		rs.GetFieldValue((short)7, strDateTime);			//DateTime		
		rs.GetFieldValue((short)8, strTestSerialNo);		//TestSerialNo		
		rs.Close();
	}
	else	//이전 공정의 최종 Cellcode를 찾지 못하였을 경우 
	{
		rs.Close();
		return FALSE;
	}
	
	//최종 공정의 Cell Code 정보를 검색 한다.	
	strSQL = "SELECT";
	for(int i =1; i<=128; i++)
	{
		if(i == 1)
		{
			strQuery.Format(" ch%d", i);
		}
		else
		{
			strQuery.Format(", ch%d", i);
		}
		strSQL += strQuery;
	}
	strQuery.Format(" FROM ChResult WHERE Index = (SELECT MAX(Index) FROM ChResult WHERE ProcedureID = %s)", strProcedureID);
	strSQL += strQuery;

	try
	{
		if(rs.Open(CRecordset::forwardOnly, strSQL) == FALSE)
			return FALSE;
	}
	catch(CDBException *e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();
		return FALSE;
	}
	
	long code[256];
	if(!rs.IsEOF())		//최종 공정의 Tray 이력 정보를 검색 한다. 
	{
		for(int i =0; i<rs.GetODBCFieldCount(); i++)
		{
			rs.GetFieldValue((short)i, va, SQL_C_SLONG);
			code[i] = va.m_lVal;
		}
		rs.Close();
	}
	else	//이전 공정의 최종 Cellcode를 찾지 못하였을 경우 
	{
		rs.Close();
		return FALSE;
	}
	
	CDatabase conditionDB;
	try
	{
		conditionDB.Open(ODBC_SCH_DB_NAME);
	}
	catch (CDBException* e)
	{
    	AfxMessageBox(e->m_strError);
    	e->Delete();     //DataBase Open Fail
		return FALSE;
	}


	//현재 삭제 Data가 진행 중인 Tray일 경우 Tray 정보 Update
	strSQL = "UPDATE Tray SET";
	strQuery.Format(" TestKey = %d, TestDateTime = '%s',", TestID, strDateTime);
	strSQL += strQuery;
	strQuery.Format(" NormalCount = %d, FailCount = %d, OperatorID = '%s',", NormalNo, InputCellCount-NormalNo, userID); 
	strSQL += strQuery;
	strQuery.Format("InputCellNo = %d, ModuleID = %d, GroupIndex = %d", InputCellCount, nModuleID, GroupIndex);
	strSQL += strQuery;
	strQuery.Format(" WHERE TrayNo = '%s' AND TestSerialNo = '%s'", strTrayID, strTestSerialNo);
	strSQL += strQuery;

	conditionDB.BeginTrans();
	conditionDB.ExecuteSQL(strSQL);

	//현재 삭제 Data가 진행 중인 Tray일 경우 Tray의 CellState 정보를 변경 한다.
	try
	{
		for(int ch=0; ch<128; ch++)
		{
			strSQL.Format("UPDATE CellState a, Tray b SET a.CellCode = %d WHERE a.TraySerial = b.TraySerial AND b.TrayNo = '%s' AND a.ChNo = %d AND b.TestSerialNo = '%s'",
				code[ch], strTrayID, ch, strTestSerialNo);
			conditionDB.ExecuteSQL(strSQL);
		}
	}
    catch (CDBException* e)			
	{
		conditionDB.Close();
		AfxMessageBox(e->m_strError);
    	e->Delete();
		return FALSE;
	}
	
	conditionDB.CommitTrans();
	conditionDB.Close();

	return TRUE;		
}

BOOL CProgressProcDlg::DeleteModuleLogList(CString strProcedureID)
{
	if(strProcedureID.IsEmpty())	return FALSE;

	CString strSQL;

	ASSERT(m_pDBServer);

	//목록에서 삭제 
	strSQL.Format("DELETE FROM Module WHERE ProcedureID = %s", strProcedureID);
	m_pDBServer->BeginTrans();
	m_pDBServer->ExecuteSQL(strSQL);
	m_pDBServer->CommitTrans();
	
	return TRUE;
}
	