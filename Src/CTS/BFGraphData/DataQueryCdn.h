// DataQueryCdn.h: interface for the CDataQueryCondition class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DATAQUERYCDN_H__90DAB180_E17D_11D4_88DF_006008CEDA07__INCLUDED_)
#define AFX_DATAQUERYCDN_H__90DAB180_E17D_11D4_88DF_006008CEDA07__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// #include

#include "DataQueryDlg.h"	// Added by ClassView
#include "Axis.h"

class CDataQueryCondition  
{
////////////////
// Attributes //
////////////////
private:
	CStringList                     m_strlistChPath; // List of Channel Path
	CList <LONG, LONG&>             m_llistCycle;    // Cycle List
	CAxis                           m_XAxis;         // Selected X-Axis
	CTypedPtrList<CPtrList, CAxis*> m_YAxisList;     // List of Selected Y-Axis
	int                             m_iOutputOption;	// Step Output Option
	int								m_iStartOption;		//Start Type
	BOOL                            m_bModify;       // Whether it is Modifying Mode
public:
	CDataQueryDialog                m_dlgQuery;      // Dialog Box where Data Querying Condition is getted.
	CString							m_strTestName;
///////////////////////////////////////////////////////////
// Operations: "Construction, Destruction, and Creation" //
///////////////////////////////////////////////////////////
public:

	CDataQueryCondition();
	virtual ~CDataQueryCondition();

////////////////////////////////////////////////////////
// Operations: "Functions to access member variables" //
////////////////////////////////////////////////////////
public:

	// m_strlistChPath
	POSITION GetChPathHead() { return m_strlistChPath.GetHeadPosition(); };
	CString  GetNextChPath(POSITION& pos) { return m_strlistChPath.GetNext(pos); };
	void     ResetChPath() { m_strlistChPath.RemoveAll(); };
	void     AddChPath(LPCTSTR path) { m_strlistChPath.AddTail(path); };
	int      GetChPathCount() { return m_strlistChPath.GetCount(); };

	// m_llistCycle
	BOOL     UpdateCycleList(long* pCycle, int NumOfCycle);
	POSITION GetCycleHead() { return m_llistCycle.GetHeadPosition(); };
	LONG     GetNextCycle(POSITION& pos) { return m_llistCycle.GetNext(pos); };
	LONG     GetCycleCount() { return m_llistCycle.GetCount(); };

	// m_XAxisList
	CAxis*   GetXAxis() { return &m_XAxis; };

	// m_YAxisList
	void     ResetYAxisList();
	void     AddYAxisList(CAxis* pAxis) { m_YAxisList.AddTail(pAxis); };
	POSITION GetYAxisHead() { return m_YAxisList.GetHeadPosition(); };
	CAxis*   GetNextYAxis(POSITION& pos) { return m_YAxisList.GetNext(pos); };
	CAxis*   IsThereThisYAxis(LPCTSTR strTitle);

	// m_iOutputOption
	void SetOutputOption(int iOption) { m_iOutputOption = iOption; };		//Step Type�ִ�Flag
	int  GetOutputOption() { return m_iOutputOption; };

	// m_bModify
	BOOL IsModifyingMode() { return m_bModify; };
	void SetModifyingMode(BOOL bMode) { m_bModify = bMode; };

	//
	void	SetStartPointOption(int iOption)	{ m_iStartOption = iOption;	};
	int	GetStartPointOption()				{	return m_iStartOption;	};


///////////////////////////////////
// Operations: "Other functions" //
///////////////////////////////////
public:

	BOOL GetModeList(CList<DWORD, DWORD&>* pdwListMode,LPCTSTR strChPath, LPCTSTR strYAxisTitle);
	BOOL IsThereThisLine(LPCTSTR strChPath, LONG lCycle, DWORD dwMode, LPCTSTR strYAxisTitle);
	WORD GetVTMeasurePoint();
	BOOL GetUserSelection();

///////////////////////////////
// Operations: "Common used" //
///////////////////////////////
public:

	static CString GetModeName(DWORD mode);

//////////////////////////
// Enumerated Constants //
//////////////////////////
public:

	enum{
		MODE_NONE           = 0,
		MODE_CHARGE         = 1,
		MODE_CHARGE_OPEN    = 2,
		MODE_DISCHARGE      = 3,
		MODE_DISCHARGE_OPEN = 4
	};

};

#endif // !defined(AFX_DATAQUERYCDN_H__90DAB180_E17D_11D4_88DF_006008CEDA07__INCLUDED_)
