// CommandSocket.cpp : implementation file
//

#include "stdafx.h"
#include "CTSGraphAnal.h"
#include "CommandSocket.h"
#include "MonitorView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCommandSocket

CString MONITORING_BASIC_IP[] = {
	_T("61.77.154.228"),
	_T("61.77.154.228"),
	_T("61.77.154.228")
};

void WriteLog(CString strLog)
{
	CString strFile = _T("log.txt");
	int nID = 0;

	CFileFind aFinder;
	if( aFinder.FindFile(strFile) == FALSE)	nID = CFile::modeCreate;
	aFinder.Close();
	
	CStdioFile aFile(strFile, CFile::modeWrite|nID);
	
	if(!(nID&CFile::modeCreate))
		aFile.SeekToEnd();
	aFile.WriteString(CTime::GetCurrentTime().Format("%Y/%m/%d %H:%M:%S :: ")+strLog);
	aFile.Close();
}

UINT ThreadFunc(LPVOID pArg);

CCommandSocket::CCommandSocket(BYTE byUnitNo, bool bMode)
: m_byUnitNo(byUnitNo), m_bServer(bMode)
{
	MONITORING_PORT = MONITORING_BASIC_PORT + m_byUnitNo;
	MONITORING_IP	= MONITORING_BASIC_IP[m_byUnitNo-1];
}

CCommandSocket::CCommandSocket()
{
	m_byUnitNo = 0;
	m_bServer = false;
	m_nSockStatus = 0;
}

CCommandSocket::~CCommandSocket()
{
	LPEP_PACKAGE pPack = NULL;
	for( int nI = 0; nI < m_aPacket.GetSize(); nI++ )
	{
		pPack = (LPEP_PACKAGE)m_aPacket.GetAt(nI);
		delete pPack;
	}
	m_aPacket.RemoveAll();

	m_hSocket = INVALID_SOCKET;
	Close();
}

// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CCommandSocket, CAsyncSocket)
	//{{AFX_MSG_MAP(CCommandSocket)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// CCommandSocket member functions

void CCommandSocket::OnClose(int nErrorCode) 
{
	CString strLogMsg;
	strLogMsg.Format("** Unit %d : Close Connection!!\n", m_byUnitNo);
	WriteLog(strLogMsg);
	
	m_pDoc->DeleteSocket(m_byUnitNo);	
	CAsyncSocket::OnClose(nErrorCode);
}

void CCommandSocket::OnReceive(int nErrorCode) 
{
	CString strMsg;
	DWORD dwSocketRecv = 0;

	// 소켓 버퍼에 쌓인 데이터의 바이트 수를 읽어옴
	ioctlsocket(m_hSocket, FIONREAD, &dwSocketRecv);

	if( m_nSockStatus == 0 )
	{
		if( dwSocketRecv < sizeof(EP_CMD_PACKET) )
		{
			int nRetVal = Receive(m_pBuffer, dwSocketRecv);
			if( nRetVal == SOCKET_ERROR )
				return;
			
			m_nOffset = nRetVal;
			m_nSockStatus = 1;
			return;
		}

		int nRecvSize = Receive(m_pBuffer, sizeof(EP_CMD_PACKET));
		if( nRecvSize < sizeof(EP_CMD_PACKET) )
		{
			m_nOffset = nRecvSize;
			m_nSockStatus = 1;
			return;
		}

		m_nSockStatus = 0;
		m_nOffset = 0;
		ParsingCommand();
	}
	else
	{	
		if( m_nOffset + dwSocketRecv >= sizeof(EP_CMD_PACKET) )
		{
			int nRetVal = Receive(m_pBuffer+m_nOffset, sizeof(EP_CMD_PACKET));
			if( nRetVal == SOCKET_ERROR )
				return;
			if( nRetVal + m_nOffset < sizeof(EP_CMD_PACKET) )
			{
				m_nOffset += nRetVal;
				return;
			}
			m_nSockStatus = 0;
			m_nOffset = 0;
			ParsingCommand();
		}
	}
	
	CAsyncSocket::OnReceive(nErrorCode);
}

bool CCommandSocket::ParsingCommand()
{
	CString strLogMsg;

	EP_CMD_PACKET aPacket;
	::CopyMemory(&aPacket, m_pBuffer, sizeof(aPacket));

	switch( aPacket.byCmd )
	{
	case EP_PACKET_MODULE_ID :
		m_byUnitNo			= aPacket.byAckCode;
		aPacket.byCmd		= EP_PACKET_RESPONSE;
		aPacket.byAckCode	= EP_RESPONSE_ACK;
		aPacket.wdDataSize	= 0;
					
		Send(&aPacket, sizeof(EP_CMD_PACKET));

		strLogMsg.Format("**Unit %d Connected\n", m_byUnitNo);
		WriteLog(strLogMsg);

		m_pDoc->ConnectedUnit(m_byUnitNo);
		break;
	case EP_PACKET_RESPONSE :
		if( aPacket.byAckCode == EP_RESPONSE_ACK )
		{
			SetEvent(m_hSendComplete);
			m_pDoc->m_nReceiveAck[m_byUnitNo-1] = 0x01;
			//m_pDoc->m_wdModuleID[m_byUnitNo-1] = aPacket.wdModuleID;
			memcpy(m_pDoc->m_wdModuleID[m_byUnitNo-1], 
					aPacket.dwChannelLow, sizeof(aPacket.dwChannelLow));

			TRACE("***Unit %d Command Ack 수신\n", m_byUnitNo);
		}
		break;
	case EP_PACKET_POLLING:
		{

		}
		break;
	case EP_PACKET_EMERGENCY :
		{
			int nErrorCode = aPacket.byAckCode;
			int nModuleNo = aPacket.wdModuleID;
			m_pDoc->ReceiveErrorReport(m_byUnitNo, nModuleNo, nErrorCode);

			strLogMsg.Format("** Unit %d Module %02d Error Code(0x%04X) Received\n", 
				m_byUnitNo, nModuleNo, nErrorCode);
			WriteLog(strLogMsg);
		}
		break;
	}

	return true;
}

void CCommandSocket::SendCommand(BYTE byCmd, LPEP_CMD_PACKET pCmd, char* szTestName)
{
	bool bFound = false;
	pCmd->wdDataSize = (byCmd == EP_CMD_RUN) ? 64 : 0; //strlen(szTestName);
	int nSendByte = Send(pCmd, sizeof(EP_CMD_PACKET));
	if( nSendByte < 0 )
		return;

	if( byCmd == EP_CMD_RUN )
	{
		char szTest[64];
		sprintf(szTest, szTestName);
		int nSendByte = Send(szTest, 64); //pCmd->wdDataSize);
		TRACE("**Send Run Command : %d Byte\n", nSendByte);

		if( nSendByte < 0 )
			return;
	}
	m_hSendComplete = CreateEvent(NULL, TRUE, FALSE, NULL);
	AfxBeginThread(ThreadFunc, this);
}

UINT ThreadFunc(LPVOID pArg)
{
	CCommandSocket* pSock = (CCommandSocket *)pArg;
	return pSock->ReadAck();
}

UINT CCommandSocket::ReadAck()
{
	DWORD dwRetVal = WaitForSingleObject(m_hSendComplete, TIMEOUT);
	ResetEvent(m_hSendComplete);
	CloseHandle(m_hSendComplete);

	if( dwRetVal == WAIT_OBJECT_0 )
	{
		// Ack 수신 성공
		TRACE("*** Unit %d Receive Ack Success ***\n", m_byUnitNo);
		return 0;
	}

	// Ack 수신 실패
	TRACE("*** Unit %d Receive Ack Failed!! ***\n", m_byUnitNo);
	return 0;
}

void CCommandSocket::SendCommand(BYTE byCmd, BYTE ModuleNo, BYTE byChannelNum, DWORD dwChannel, char* szTestName)
{
/*	bool bFound = false;
	LPEP_PACKAGE pPackage = NULL;
	for( int nI = 0; nI < m_aPacket.GetSize(); nI++ )
	{
		pPackage = (LPEP_PACKAGE)m_aPacket.GetAt(nI);
		if( pPackage->aPacket.byModuleID == ModuleNo )
		{
			bFound = true;
			break;
		}
	}
	LPEP_CMD_PACKET aPacket = NULL;
	if( bFound == false )
	{
		pPackage = new EP_PACKAGE;
		ZeroMemory(&pPackage->aPacket, sizeof(EP_CMD_PACKET));
		m_aPacket.Add(pPackage);
		
	}
	aPacket = &pPackage->aPacket;

	aPacket->byCmd		= byCmd;
	aPacket->byChNum	= byChannelNum;
	aPacket->byModuleID = ModuleNo;
	aPacket->dwChannel	= dwChannel;

	Send(aPacket, sizeof(EP_CMD_PACKET));

	if( byCmd == EP_CMD_RUN )
	{
		sprintf(pPackage->szTestName, szTestName);
		int nSendByte = Send(pPackage->szTestName, 64);
	}
	m_byCurrentModuleID = ModuleNo;
	pPackage->hSendComplete = CreateEvent(NULL, TRUE, FALSE, NULL);
	AfxBeginThread(ThreadFunc, this);
*/
}
/*
UINT CCommandSocket::ReadAck()
{
	BYTE byModuleID = m_byCurrentModuleID;

	LPEP_PACKAGE pPackage = NULL;
	for( int nI = 0; nI < m_aPacket.GetSize(); nI++ )
	{
		pPackage = (LPEP_PACKAGE)m_aPacket.GetAt(nI);
		if( pPackage->aPacket.byModuleID == byModuleID )
		{
			TRACE("******** 진행하는 Module ID : %d / Detect Module HANDLE : %X\n", 
				byModuleID, pPackage->hSendComplete);
			break;
		}
	}
	
	DWORD dwRetVal = WaitForSingleObject(pPackage->hSendComplete, TIMEOUT);
	ResetEvent(pPackage->hSendComplete);
	CloseHandle(pPackage->hSendComplete);
	if( dwRetVal == WAIT_OBJECT_0 )
	{
		// Ack 수신 성공
		TRACE("*** Module %d Receive Ack Success ***\n", pPackage->aPacket.byModuleID);
		return 0;
	}

	// Ack 수신 실패
	TRACE("*** Module %d Receive Ack Failed!! ***\n", pPackage->aPacket.byModuleID);
	return 0;
}
*/
void CCommandSocket::ReceiveModuleNo()
{

}

