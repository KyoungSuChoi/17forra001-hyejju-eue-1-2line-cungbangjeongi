// CTSEditor.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "CTSEditor.h"

#include "MainFrm.h"
#include "CTSEditorDoc.h"
#include "CTSEditorView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCTSEditorApp

BEGIN_MESSAGE_MAP(CCTSEditorApp, CWinApp)
	//{{AFX_MSG_MAP(CCTSEditorApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	ON_COMMAND(ID_APP_EXIT, OnAppExit)
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
	// Standard print setup command
	ON_COMMAND(ID_FILE_PRINT_SETUP, CWinApp::OnFilePrintSetup)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCTSEditorApp construction

CCTSEditorApp::CCTSEditorApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CCTSEditorApp object

CCTSEditorApp theApp;
CString g_strDataBaseName;

/////////////////////////////////////////////////////////////////////////////
// CCTSEditorApp initialization

BOOL CCTSEditorApp::InitInstance()
{
	AfxGetModuleState()->m_dwVersion = 0x0601;

	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

	GXInit();
	
//	AfxGetModuleState()->m_dwVersion = 0x0601;
//	AfxDaoInit();

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	// Change the registry key under which our settings are stored.
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization.

#ifdef _CYCLER_		//Cycler version
	SetRegistryKey(_T("PNE CTSPro"));
#else
	SetRegistryKey(_T("PNE CTS"));		//formation version
#endif

	LoadStdProfileSettings();  // Load standard INI file options (including MRU)


	if( LanguageinitMonConfig() == false )// 삽입위치
	{
		AfxMessageBox("Could not found [ CTSEditor_Lang.ini ]");
		return false;
	}

	CString strCurFolder = GetProfileString(EDITOR_REG_SECTION ,"Path");	//Get Current Folder(PowerFormation Folde)
	if(strCurFolder.IsEmpty())
	{
		AfxMessageBox("설정 경로를 찾을 수 없습니다.");
		char szBuff[_MAX_PATH];
		if(GetCurrentDirectory(_MAX_PATH, szBuff) < 1)	//Get Current Direcotry Name
		{
			sprintf(szBuff, "File Path Name Read Error %d\n", GetLastError());
			AfxMessageBox(szBuff);
			return FALSE;	
		}
		strCurFolder = szBuff;
//		if(WriteProfileString(strSection, "CTSEditor", strCurFolder) == 0)	return FALSE;		//Install Program에서 Path를 기록한다.
	}

	//DataBase 이름을 지정한다.
	strCurFolder = GetProfileString(EDITOR_REG_SECTION ,"DataBase");	//Get Current Folder(PowerFormation Folde)
	if(strCurFolder.IsEmpty())
	{
		g_strDataBaseName.Format("%s\\DataBase\\%s", strCurFolder, PS_SCHEDULE_DATABASE_NAME);
	}
	else
	{
		g_strDataBaseName.Format("%s\\%s", strCurFolder, PS_SCHEDULE_DATABASE_NAME);
	}
//	CString strTemp;

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.

	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CCTSEditorDoc),
		RUNTIME_CLASS(CMainFrame),       // main SDI frame window
		RUNTIME_CLASS(CCTSEditorView));
	AddDocTemplate(pDocTemplate);

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	CString strArgu((char *)m_lpCmdLine);
	CString strModel, strTest;
	strModel = strArgu.Left(strArgu.Find(','));
	strTest = strArgu.Mid(strArgu.Find(',')+1);

	if(cmdInfo.m_nShellCommand == CCommandLineInfo::FileOpen && !cmdInfo.m_strFileName.IsEmpty())
	{
		cmdInfo.m_nShellCommand = CCommandLineInfo::FileNew;
		cmdInfo.m_strFileName.Empty();
	}

	// Dispatch commands specified on the command line
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// The one and only window has been initialized, so show and update it.
//	m_pMainWnd->ShowWindow(SW_SHOWMAXIMIZED);

	m_pMainWnd->CenterWindow();
#ifdef _DLG_MODEL_
	m_pMainWnd->ShowWindow(SW_HIDE);
#else
	m_pMainWnd->ShowWindow(SW_SHOW);
#endif

	//Argument로 넘어온 모델의 조건을 기본적으로 Loading 한다.
	if(strModel.GetLength() >0 && strTest.GetLength() > 0)
	{
		POSITION pos = pDocTemplate->GetFirstDocPosition();
		if(pos)
		{
			CCTSEditorDoc *myDoc =(CCTSEditorDoc *)(pDocTemplate->GetNextDoc(pos));
			if( myDoc != NULL )
			{
				CCTSEditorView *pView;
				POSITION pos1 = myDoc->GetFirstViewPosition();
				if(pos1)
				{
					pView = (CCTSEditorView *)myDoc->GetNextView(pos1);
					if(pView)
					{
						pView->SetDefultTest(strModel, strTest);
					}
				}
			}
		}
	}	


	m_pMainWnd->UpdateWindow();

	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CCTSEditorApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// CCTSEditorApp message handlers


int CCTSEditorApp::ExitInstance() 
{
	// TODO: Add your specialized code here and/or call the base class
	GXTerminate();
	return CWinApp::ExitInstance();
}

BOOL CAboutDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CString strMsg("CTSEditor");

#ifdef _CYCLER_
	strMsg = "PNE CTSPro Editor";
#else
	strMsg = "PNE CTS Editor";
#endif

#ifdef _EDLC_CELL_
	strMsg += " [Cell type EDLC]";
#else
	strMsg += " [Cell type Li-ion]";
#endif


	GetDlgItem(IDC_TYPE_STATIC)->SetWindowText(strMsg);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}



///////////////////////////////////
bool CCTSEditorApp::LanguageinitMonConfig() 
{
	g_nLanguage = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "Language", 0);
	g_strLangPath.Format("%s\\Lang\\CTSEditor_Lang.ini", GetProfileString(EDITOR_REG_SECTION ,"Path","C:\Program Files\PNE CTS"));

	switch(g_nLanguage)
	{
	case 0:
		{
			SetThreadUILanguage( MAKELCID( MAKELANGID(LANG_KOREAN , SUBLANG_KOREAN) , SORT_DEFAULT));
			break;
		}

	case 1:
		{
			SetThreadUILanguage( MAKELCID( MAKELANGID(LANG_ENGLISH , SUBLANG_ENGLISH_US) , SORT_DEFAULT));
			break;
		}

	case 2:
		{
			SetThreadUILanguage( MAKELCID( MAKELANGID(LANG_CHINESE_SIMPLIFIED , SUBLANG_CHINESE_SIMPLIFIED) , SORT_DEFAULT));
			break;
		}
	}

	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCTSEditorApp"), _T("TEXT_CCTSEditorApp_CNT"), _T("TEXT_CCTSEditorApp_CNT"));
	if( strTemp == "TEXT_CCTSMONAPP_CNT" )
	{
		return false;
	}


	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt];		

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CCTSEditorApp_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCTSEditorApp"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Languge error ====> " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

void CCTSEditorApp::OnAppExit() 
{
	// TODO: Add your command handler code here
	CWinApp::OnAppExit();
}