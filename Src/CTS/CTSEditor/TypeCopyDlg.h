#pragma once
#include "afxwin.h"


// CTypeCopyDlg 대화 상자입니다.

class CTypeCopyDlg : public CDialog
{
	DECLARE_DYNAMIC(CTypeCopyDlg)

public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	CTypeCopyDlg(long nSelectType, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CTypeCopyDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_TYPE_COPY_DLG };
	
public:
	void InitLabel();
	void InitFont();	
	
	long m_lSelectType;
	CFont font;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CLabel m_Label1;
	afx_msg void OnBnClickedOk();
	virtual BOOL OnInitDialog();
	CLabel m_Label2;	
	CLabel m_LabelSelectType;
	CString m_strTypeToCopy;
	afx_msg void OnBnClickedCancel();
};
