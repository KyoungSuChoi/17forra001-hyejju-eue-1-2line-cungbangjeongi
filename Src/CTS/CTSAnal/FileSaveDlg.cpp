// FileSaveDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "FileSaveDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFileSaveDlg dialog

bool CFileSaveDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CFileSaveDlg"), _T("TEXT_CFileSaveDlg_CNT"), _T("TEXT_CFileSaveDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CFileSaveDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CFileSaveDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}


CFileSaveDlg::CFileSaveDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFileSaveDlg::IDD, pParent)
{
	LanguageinitMonConfig();

	//{{AFX_DATA_INIT(CFileSaveDlg)
	m_bCycleSave = TRUE;
	m_bCannelSave = TRUE;
	m_bProcSave = TRUE;
	m_bChMode = TRUE;
	m_bChTime = TRUE;
	m_bChState = TRUE;
	m_bChGrade = TRUE;
	m_bChFailure = TRUE;
	m_bChVoltage = TRUE;
	m_bChCurrent = TRUE;
	m_bChWatt = TRUE;
	m_bChCapacity = TRUE;
	m_bChImpedance = TRUE;
	m_bStepTime = TRUE;
	m_bStepVoltage = TRUE;
	m_bStepCurrent = TRUE;
	m_bStepWatt = TRUE;
	m_bStepCapacity = TRUE;
	m_bStepImpedance = TRUE;
	m_bStepGrade = TRUE;
	m_bStepFailure = TRUE;
	m_bChType = TRUE;
	m_strSaveFileName = _T("");
	m_bCSVfileSave = TRUE;
	m_bTxtFileSave = TRUE;
	m_strChFileName = _T("");
	m_strStepFileName = _T("");
	//}}AFX_DATA_INIT
	m_strSection = "File Convert Set";
}

CFileSaveDlg::~CFileSaveDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


void CFileSaveDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFileSaveDlg)
	DDX_Control(pDX, IDC_ORGFILENAME, m_strFileNameLabel);
	DDX_Check(pDX, IDC_CYCLE_DATA_CHECK, m_bCycleSave);
	DDX_Check(pDX, IDC_CHANNEL_CHECK, m_bCannelSave);
	DDX_Check(pDX, IDC_PROC_CHECK, m_bProcSave);
	DDX_Check(pDX, IDC_MODE_CHECK1, m_bChMode);
	DDX_Check(pDX, IDC_STEPTIME_CHECK1, m_bChTime);
	DDX_Check(pDX, IDC_STATE_CHECK1, m_bChState);
	DDX_Check(pDX, IDC_GRADE_CHECK1, m_bChGrade);
	DDX_Check(pDX, IDC_FAILURE_CHECK1, m_bChFailure);
	DDX_Check(pDX, IDC_VTG_CHECK1, m_bChVoltage);
	DDX_Check(pDX, IDC_CURRENT_CHECK1, m_bChCurrent);
	DDX_Check(pDX, IDC_WATT_CHECK1, m_bChWatt);
	DDX_Check(pDX, IDC_CAP_CHECK1, m_bChCapacity);
	DDX_Check(pDX, IDC_IMP_CHECK1, m_bChImpedance);
	DDX_Check(pDX, IDC_VTG_CHECK2, m_bStepVoltage);
	DDX_Check(pDX, IDC_STEPTIME_CHECK2, m_bStepTime);
	DDX_Check(pDX, IDC_CURRENT_CHECK2, m_bStepCurrent);
	DDX_Check(pDX, IDC_WATT_CHECK2, m_bStepWatt);
	DDX_Check(pDX, IDC_CAP_CHECK2, m_bStepCapacity);
	DDX_Check(pDX, IDC_IMP_CHECK2, m_bStepImpedance);
	DDX_Check(pDX, IDC_GRADE_CHECK2, m_bStepGrade);
	DDX_Check(pDX, IDC_FAILURE_CHECK2, m_bStepFailure);
	DDX_Check(pDX, IDC_TYPE_CHECK1, m_bChType);
	DDX_Text(pDX, IDC_FILENAME_EDIT, m_strSaveFileName);
	DDX_Check(pDX, IDC_CSV_CHECK, m_bCSVfileSave);
	DDX_Check(pDX, IDC_TXT_SAVE, m_bTxtFileSave);
	DDX_Text(pDX, IDC_CH_NAME_EDIT, m_strChFileName);
	DDX_Text(pDX, IDC_STEP_LIST_EDIT, m_strStepFileName);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFileSaveDlg, CDialog)
	//{{AFX_MSG_MAP(CFileSaveDlg)
	ON_BN_CLICKED(IDC_PROC_CHECK, OnProcCheck)
	ON_BN_CLICKED(IDC_CHANNEL_CHECK, OnChannelCheck)
	ON_BN_CLICKED(IDOK, OnOk)
	ON_BN_CLICKED(IDC_FOLDER_SETTING_BTN, OnFolderSettingBtn)
	ON_BN_CLICKED(IDC_CYCLE_DATA_CHECK, OnCycleDataCheck)
	ON_BN_CLICKED(IDC_CH_LIST_FILE_BTN, OnChListFileBtn)
	ON_BN_CLICKED(IDC_STEP_LIST_FILE_BTN, OnStepListFileBtn)
	ON_BN_CLICKED(IDC_TXT_SAVE, OnTxtSave)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFileSaveDlg message handlers

BOOL CFileSaveDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	GetDlgItem( IDC_MODE_CHECK1)->ShowWindow(SW_HIDE);
	GetDlgItem( IDC_TYPE_CHECK1)->ShowWindow(SW_HIDE);


	// TODO: Add extra initialization here
	CString strTemp;
	strTemp = m_strOrgFileName.Left(m_strOrgFileName.ReverseFind('.'));
	if(m_bTxtFileSave== FALSE && m_bCSVfileSave== TRUE)
	{
		m_strSaveFileName =  strTemp + "(cap).csv";
		m_strChFileName = strTemp + "(ch).csv";
		m_strStepFileName = strTemp + "(step).csv";
	}
	else
	{
		m_strSaveFileName =  strTemp + "(cap).csv";
		m_strChFileName = strTemp + "(ch).csv";
		m_strStepFileName = strTemp + "(step).csv";
	}

	LoadSetting();
	UpdateData(FALSE);

	OnCycleDataCheck(); 
	ChannelStateCheck(TRUE);
	ProcStateCheck(TRUE); 

	m_strFileNameLabel.SetPath (TRUE);
	SetDlgItemText (IDC_ORGFILENAME, m_strOrgFileName);

	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CFileSaveDlg::OnProcCheck() 
{
	// TODO: Add your control notification handler code here
	ProcStateCheck();
}

void CFileSaveDlg::OnChannelCheck() 
{
	// TODO: Add your control notification handler code here
	ChannelStateCheck();
}

void CFileSaveDlg::ChannelStateCheck(BOOL bInit)
{
	if(!bInit)
		UpdateData(TRUE);

	if(m_bCannelSave)
	{
		GetDlgItem( IDC_TYPE_CHECK1)->EnableWindow(TRUE);
		GetDlgItem( IDC_MODE_CHECK1)->EnableWindow(TRUE);
		GetDlgItem( IDC_STEPTIME_CHECK1)->EnableWindow(TRUE);
		GetDlgItem( IDC_STATE_CHECK1)->EnableWindow(TRUE);
		GetDlgItem( IDC_GRADE_CHECK1)->EnableWindow(TRUE);
		GetDlgItem( IDC_FAILURE_CHECK1)->EnableWindow(TRUE);
		GetDlgItem( IDC_VTG_CHECK1)->EnableWindow(TRUE);
		GetDlgItem( IDC_CURRENT_CHECK1)->EnableWindow(TRUE);
		GetDlgItem( IDC_WATT_CHECK1)->EnableWindow(TRUE);
		GetDlgItem( IDC_CAP_CHECK1)->EnableWindow(TRUE);
		GetDlgItem( IDC_IMP_CHECK1)->EnableWindow(TRUE);
		GetDlgItem(IDC_CH_NAME_EDIT)->EnableWindow(TRUE);
		GetDlgItem(IDC_CH_LIST_FILE_BTN)->EnableWindow(TRUE);
		
	}
	else
	{
		GetDlgItem( IDC_TYPE_CHECK1)->EnableWindow(FALSE);
		GetDlgItem( IDC_MODE_CHECK1)->EnableWindow(FALSE);
		GetDlgItem( IDC_STEPTIME_CHECK1)->EnableWindow(FALSE);
		GetDlgItem( IDC_STATE_CHECK1)->EnableWindow(FALSE);
		GetDlgItem( IDC_GRADE_CHECK1)->EnableWindow(FALSE);
		GetDlgItem( IDC_FAILURE_CHECK1)->EnableWindow(FALSE);
		GetDlgItem( IDC_VTG_CHECK1)->EnableWindow(FALSE);
		GetDlgItem( IDC_CURRENT_CHECK1)->EnableWindow(FALSE);
		GetDlgItem( IDC_WATT_CHECK1)->EnableWindow(FALSE);
		GetDlgItem( IDC_CAP_CHECK1)->EnableWindow(FALSE);
		GetDlgItem( IDC_IMP_CHECK1)->EnableWindow(FALSE);
		GetDlgItem(IDC_CH_NAME_EDIT)->EnableWindow(FALSE);
		GetDlgItem(IDC_CH_LIST_FILE_BTN)->EnableWindow(FALSE);
	}
	
}
void CFileSaveDlg::ProcStateCheck(BOOL bInit) 
{
	if(!bInit)
		UpdateData(TRUE);
	
	if(m_bProcSave)
	{
		GetDlgItem( IDC_VTG_CHECK2)->EnableWindow(TRUE);
		GetDlgItem( IDC_CURRENT_CHECK2)->EnableWindow(TRUE);
		GetDlgItem( IDC_WATT_CHECK2)->EnableWindow(TRUE);
		GetDlgItem( IDC_CAP_CHECK2)->EnableWindow(TRUE);
		GetDlgItem( IDC_IMP_CHECK2)->EnableWindow(TRUE);
		GetDlgItem( IDC_GRADE_CHECK2)->EnableWindow(TRUE);
		GetDlgItem( IDC_FAILURE_CHECK2)->EnableWindow(TRUE);
		GetDlgItem( IDC_STEP_LIST_EDIT)->EnableWindow(TRUE);
		GetDlgItem( IDC_STEP_LIST_FILE_BTN)->EnableWindow(TRUE);
		GetDlgItem( IDC_STEPTIME_CHECK2)->EnableWindow(TRUE);
	}
	else
	{
		GetDlgItem( IDC_VTG_CHECK2)->EnableWindow(FALSE);
		GetDlgItem( IDC_CURRENT_CHECK2)->EnableWindow(FALSE);
		GetDlgItem( IDC_WATT_CHECK2)->EnableWindow(FALSE);
		GetDlgItem( IDC_CAP_CHECK2)->EnableWindow(FALSE);
		GetDlgItem( IDC_IMP_CHECK2)->EnableWindow(FALSE);
		GetDlgItem( IDC_GRADE_CHECK2)->EnableWindow(FALSE);
		GetDlgItem( IDC_FAILURE_CHECK2)->EnableWindow(FALSE);
		GetDlgItem( IDC_STEP_LIST_EDIT)->EnableWindow(FALSE);
		GetDlgItem( IDC_STEP_LIST_FILE_BTN)->EnableWindow(FALSE);
		GetDlgItem( IDC_STEPTIME_CHECK2)->EnableWindow(FALSE);
	}
}

void CFileSaveDlg::OnOk() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(m_bCSVfileSave == FALSE && m_bTxtFileSave == FALSE)
	{
		AfxMessageBox(TEXT_LANG[0]);//"저장 파일 형식이 선택되지 않았습니다."
		GetDlgItem(IDC_TXT_SAVE)->SetFocus();
		return;
	}

	if(m_bCannelSave == FALSE && m_bProcSave == FALSE && m_bCycleSave == FALSE)
	{
		AfxMessageBox(TEXT_LANG[1]);//"저장 Option이 선택되지 않았습니다."
		GetDlgItem(IDC_CHANNEL_CHECK)->SetFocus();
		return;
	}
	
	if(m_strSaveFileName.IsEmpty() && m_bCycleSave == TRUE)
	{
		AfxMessageBox(TEXT_LANG[2]);//"파일명이 입력 되지 않았습니다."
		GetDlgItem(IDC_FILENAME_EDIT)->SetFocus();
		return;
	}

	if(m_bCannelSave == TRUE && m_strChFileName.IsEmpty())
	{
		AfxMessageBox(TEXT_LANG[2]);//"파일명이 입력 되지 않았습니다."
		GetDlgItem(IDC_CH_NAME_EDIT)->SetFocus();
		return;
	}
	if(m_bCannelSave == TRUE &&
		m_bChType == FALSE &&
	m_bChMode == FALSE &&
	m_bChTime == FALSE &&
	m_bChState == FALSE &&
	m_bChGrade == FALSE &&
	m_bChFailure == FALSE &&
	m_bChVoltage == FALSE &&
	m_bChCurrent == FALSE &&
	m_bChWatt == FALSE &&
	m_bChCapacity == FALSE &&
	m_bChImpedance == FALSE)
	{
		AfxMessageBox(TEXT_LANG[3]);//"저장 항목이 하나도 선택되지 않았습니다."
		GetDlgItem(IDC_TYPE_CHECK1)->SetFocus();
		return;
	}

	if(m_strStepFileName.IsEmpty() && m_bProcSave == TRUE)
	{
		AfxMessageBox(TEXT_LANG[2]);//"파일명이 입력 되지 않았습니다."
		GetDlgItem(IDC_STEP_LIST_EDIT)->SetFocus();
		return;

	}

	if(m_bProcSave == TRUE &&
	m_bStepVoltage == FALSE &&
	m_bStepCurrent == FALSE &&
	m_bStepWatt == FALSE &&
	m_bStepCapacity == FALSE &&
	m_bStepImpedance == FALSE &&
	m_bStepGrade == FALSE &&
	m_bStepFailure == FALSE &&
	m_bStepTime == FALSE)
	{
		AfxMessageBox(TEXT_LANG[3]);//"저장 항목이 하나도 선택되지 않았습니다."
		GetDlgItem(IDC_VTG_CHECK2)->SetFocus();
		return;
	}
	
	SaveSetting();
	CDialog::OnOK();
}

void CFileSaveDlg::OnFolderSettingBtn() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	CFileDialog pDlg(FALSE, "", m_strSaveFileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, TEXT_LANG[4]);//"Text 파일(*.txt)|*.txt|"
	if(IDOK == pDlg.DoModal())
	{
		m_strSaveFileName = pDlg.GetPathName();
		UpdateData(FALSE);
	}
}

void CFileSaveDlg::OnCycleDataCheck() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(m_bCycleSave)
	{
		GetDlgItem( IDC_FILENAME_EDIT)->EnableWindow(TRUE);
		GetDlgItem( IDC_FOLDER_SETTING_BTN)->EnableWindow(TRUE);
	}
	else
	{
		GetDlgItem( IDC_FILENAME_EDIT)->EnableWindow(FALSE);
		GetDlgItem( IDC_FOLDER_SETTING_BTN)->EnableWindow(FALSE);
	}
}

void CFileSaveDlg::OnChListFileBtn() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	CFileDialog pDlg(FALSE, "", m_strChFileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, TEXT_LANG[5]);//"csv 파일(*.csv)|*.csv|"
	if(IDOK == pDlg.DoModal())
	{
		m_strChFileName = pDlg.GetPathName();
		UpdateData(FALSE);
	}
	
}

void CFileSaveDlg::OnStepListFileBtn() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	CFileDialog pDlg(FALSE, "", m_strStepFileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, TEXT_LANG[5]);//"csv 파일(*.csv)|*.csv|"
	if(IDOK == pDlg.DoModal())
	{
		m_strStepFileName = pDlg.GetPathName();
		UpdateData(FALSE);
	}
}

void CFileSaveDlg::LoadSetting()
{
//	m_bCSVfileSave =  AfxGetApp()->GetProfileInt(m_strSection, "csv File", TRUE);
//	m_bTxtFileSave =  AfxGetApp()->GetProfileInt(m_strSection, "txt File", TRUE);
	m_bCSVfileSave = FALSE;
	m_bTxtFileSave = TRUE;
	
	m_bCycleSave = AfxGetApp()->GetProfileInt(m_strSection, TEXT_LANG[6], TRUE);//"Cap Efficency"
	m_bCannelSave =  AfxGetApp()->GetProfileInt(m_strSection, TEXT_LANG[7], TRUE);//"Channel save"
	m_bProcSave =  AfxGetApp()->GetProfileInt(m_strSection, TEXT_LANG[8], TRUE);//"Step Save"

	m_bChType =  AfxGetApp()->GetProfileInt(m_strSection, TEXT_LANG[9], TRUE);//"Ch Type"
	m_bChMode =  AfxGetApp()->GetProfileInt(m_strSection, TEXT_LANG[10], TRUE);//"Ch Mode"
	m_bChTime =  AfxGetApp()->GetProfileInt(m_strSection, TEXT_LANG[11], TRUE);//"Ch Time"
	m_bChState =  AfxGetApp()->GetProfileInt(m_strSection, TEXT_LANG[12], TRUE);//"Ch State"
	m_bChGrade =  AfxGetApp()->GetProfileInt(m_strSection, TEXT_LANG[13], TRUE);//"Ch Grade"
	m_bChFailure =  AfxGetApp()->GetProfileInt(m_strSection, TEXT_LANG[14], TRUE);//"Ch Fail"
	m_bChVoltage =  AfxGetApp()->GetProfileInt(m_strSection, TEXT_LANG[15], TRUE);//"Ch Voltage"
	m_bChCurrent =  AfxGetApp()->GetProfileInt(m_strSection, TEXT_LANG[16], TRUE);//"Ch Current"
	m_bChWatt =  AfxGetApp()->GetProfileInt(m_strSection, TEXT_LANG[17], TRUE);//"Ch Watt"
	m_bChCapacity =  AfxGetApp()->GetProfileInt(m_strSection, TEXT_LANG[18], TRUE);//"Ch Capacity"
	m_bChImpedance =  AfxGetApp()->GetProfileInt(m_strSection, TEXT_LANG[19], TRUE);//"Ch Impedance"
	
	m_bStepTime = AfxGetApp()->GetProfileInt(m_strSection, TEXT_LANG[20], TRUE);//"Step Time"
	m_bStepVoltage =  AfxGetApp()->GetProfileInt(m_strSection, TEXT_LANG[21], TRUE);//"Step Voltage"
	m_bStepCurrent =  AfxGetApp()->GetProfileInt(m_strSection, TEXT_LANG[22], TRUE);//"Step Current"
	m_bStepWatt =  AfxGetApp()->GetProfileInt(m_strSection, TEXT_LANG[23], TRUE);//"Step Watt"
	m_bStepCapacity =  AfxGetApp()->GetProfileInt(m_strSection, TEXT_LANG[24], TRUE);//"Step Capacity"
	m_bStepImpedance =  AfxGetApp()->GetProfileInt(m_strSection, TEXT_LANG[25], TRUE);//"Step Impedance"
	m_bStepGrade =  AfxGetApp()->GetProfileInt(m_strSection, TEXT_LANG[26], TRUE);//"Step Grade"
	m_bStepFailure =  AfxGetApp()->GetProfileInt(m_strSection, TEXT_LANG[27], TRUE);//"Step Fail"
}

void CFileSaveDlg::SaveSetting()
{
	AfxGetApp()->WriteProfileInt(m_strSection, TEXT_LANG[28], m_bCSVfileSave);//"csv File"
	AfxGetApp()->WriteProfileInt(m_strSection, TEXT_LANG[29], m_bTxtFileSave);//"txt File"

	AfxGetApp()->WriteProfileInt(m_strSection, TEXT_LANG[6], m_bCycleSave);//"Cap Efficency"
	AfxGetApp()->WriteProfileInt(m_strSection, TEXT_LANG[7], m_bCannelSave);//"Channel save"
	AfxGetApp()->WriteProfileInt(m_strSection, TEXT_LANG[8], m_bProcSave);//"Step Save"

	AfxGetApp()->WriteProfileInt(m_strSection, TEXT_LANG[9], m_bChType);//"Ch Type"
	AfxGetApp()->WriteProfileInt(m_strSection, TEXT_LANG[10], m_bChMode);//"Ch Mode"
	AfxGetApp()->WriteProfileInt(m_strSection, TEXT_LANG[11], m_bChTime);//"Ch Time"
	AfxGetApp()->WriteProfileInt(m_strSection, TEXT_LANG[12], m_bChState);//"Ch State"
	AfxGetApp()->WriteProfileInt(m_strSection, TEXT_LANG[13], m_bChGrade);//"Ch Grade"
	AfxGetApp()->WriteProfileInt(m_strSection, TEXT_LANG[14], m_bChFailure);//"Ch Fail"
	AfxGetApp()->WriteProfileInt(m_strSection, TEXT_LANG[15], m_bChVoltage);//"Ch Voltage"
	AfxGetApp()->WriteProfileInt(m_strSection, TEXT_LANG[16], m_bChCurrent);//"Ch Current"
	AfxGetApp()->WriteProfileInt(m_strSection, TEXT_LANG[17], m_bChWatt);//"Ch Watt"
	AfxGetApp()->WriteProfileInt(m_strSection, TEXT_LANG[18], m_bChCapacity);//"Ch Capacity"
	AfxGetApp()->WriteProfileInt(m_strSection, TEXT_LANG[19], m_bChImpedance);//"Ch Impedance"
	
	AfxGetApp()->WriteProfileInt(m_strSection, TEXT_LANG[20], m_bStepTime);//"Step Time"
	AfxGetApp()->WriteProfileInt(m_strSection, TEXT_LANG[21], m_bStepVoltage);//"Step Voltage"
	AfxGetApp()->WriteProfileInt(m_strSection, TEXT_LANG[22], m_bStepCurrent);//"Step Current"
	AfxGetApp()->WriteProfileInt(m_strSection, TEXT_LANG[23], m_bStepWatt);//"Step Watt"
	AfxGetApp()->WriteProfileInt(m_strSection, TEXT_LANG[24], m_bStepCapacity);//"Step Capacity"
	AfxGetApp()->WriteProfileInt(m_strSection, TEXT_LANG[25], m_bStepImpedance);//"Step Impedance"
	AfxGetApp()->WriteProfileInt(m_strSection, TEXT_LANG[26], m_bStepGrade);//"Step Grade"
	AfxGetApp()->WriteProfileInt(m_strSection, TEXT_LANG[27], m_bStepFailure);//"Step Fail"
}

void CFileSaveDlg::OnTxtSave() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	
	if(m_bTxtFileSave== FALSE && m_bCSVfileSave== TRUE)
	{
		m_strSaveFileName =  m_strSaveFileName.Left(m_strSaveFileName.ReverseFind('.')) + TEXT_LANG[30];//".csv"
		m_strChFileName = m_strChFileName.Left(m_strChFileName.ReverseFind('.')) + TEXT_LANG[30];//".csv"
		m_strStepFileName = m_strStepFileName.Left(m_strStepFileName.ReverseFind('.')) + TEXT_LANG[30];//".csv"
	}
	else
	{
		m_strSaveFileName =  m_strSaveFileName.Left(m_strSaveFileName.ReverseFind('.')) + TEXT_LANG[31];//".txt"
		m_strChFileName = m_strChFileName.Left(m_strChFileName.ReverseFind('.')) + TEXT_LANG[31];//".txt"
		m_strStepFileName = m_strStepFileName.Left(m_strStepFileName.ReverseFind('.')) + TEXT_LANG[31];//".txt"
	}
	UpdateData(FALSE);
}
