// SortTypeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SortTypeDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSortTypeDialog dialog


CSortTypeDialog::CSortTypeDialog(int nMode /*=modeBasic=*/,
								 CWnd* pParent /*=NULL*/)
	: CDialog(CSortTypeDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSortTypeDialog)
	m_iFirst = 1;
	m_iSecond = 0;
	//}}AFX_DATA_INIT
	m_iType = 0;
	m_nMode = nMode;
}


void CSortTypeDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSortTypeDialog)
	DDX_Radio(pDX, IDC_FIRST1, m_iFirst);
	DDX_Radio(pDX, IDC_SECOND1, m_iSecond);
	//}}AFX_DATA_MAP
	if(pDX->m_bSaveAndValidate)
	{
		m_iType = m_iFirst*10+m_iSecond;
	}
}


BEGIN_MESSAGE_MAP(CSortTypeDialog, CDialog)
	//{{AFX_MSG_MAP(CSortTypeDialog)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSortTypeDialog message handlers

BOOL CSortTypeDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	if ( m_nMode == modeBasic )
	{
		GetDlgItem(IDC_EDIT_SIMPLE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_LABEL_TOP)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_FIRST1)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_FIRST2)->ShowWindow(SW_SHOW);
	}
	else
	{
		GetDlgItem(IDC_EDIT_SIMPLE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_LABEL_TOP)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_FIRST1)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_FIRST2)->ShowWindow(SW_HIDE);
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
