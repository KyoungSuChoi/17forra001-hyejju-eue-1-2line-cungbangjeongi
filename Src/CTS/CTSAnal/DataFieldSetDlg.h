#if !defined(AFX_DATAFIELDSETDLG_H__3EC31E2E_D511_4454_9FAA_F74CE097066A__INCLUDED_)
#define AFX_DATAFIELDSETDLG_H__3EC31E2E_D511_4454_9FAA_F74CE097066A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DataFieldSetDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDataFieldSetDlg dialog
#include "NewGridWnd.h"

class CDataFieldSetDlg : public CDialog
{
// Construction
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	CString GetProcTypeName(int nType);
	CPtrArray m_apProcType;
//	long FindMaxID();
	int m_nFieldCount;
	FIELD		 m_Field[FIELDNO];
	void InitFieldSetGrid();

	CDataFieldSetDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDataFieldSetDlg();

	CMyNewGridWnd	m_FieldGrid;
	BOOL RequeryProcType();
// Dialog Data
	//{{AFX_DATA(CDataFieldSetDlg)
	enum { IDD = IDD_FIELD_SET_DLG };
	BOOL	m_bGradingCode;
	BOOL	m_bCellCode;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDataFieldSetDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDataFieldSetDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnAddField();
	afx_msg void OnDeleteField();
	virtual void OnOK();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATAFIELDSETDLG_H__3EC31E2E_D511_4454_9FAA_F74CE097066A__INCLUDED_)
