// PasswordChkDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "PasswordChkDlg.h"


// CPasswordChkDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CPasswordChkDlg, CDialog)

CPasswordChkDlg::CPasswordChkDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPasswordChkDlg::IDD, pParent)
	, m_strPassword(_T(""))
	, m_strNewPassword(_T(""))
{
	m_bPasswordChagneChk = FALSE;
	m_bDiffPassword= false;
	m_strDiffPassword ="";
	m_strTitle = "";
}

CPasswordChkDlg::~CPasswordChkDlg()
{
}

void CPasswordChkDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_PASSWORD, m_strPassword);
	DDX_Text(pDX, IDC_NEW_PASSWORD, m_strNewPassword);
}


BEGIN_MESSAGE_MAP(CPasswordChkDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CPasswordChkDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BTN_PASSWORD_CHANGE, &CPasswordChkDlg::OnBnClickedBtnPasswordChange)
END_MESSAGE_MAP()


// CPasswordChkDlg 메시지 처리기입니다.

void CPasswordChkDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( m_bPasswordChagneChk == TRUE )
	{
		UpdateData(TRUE);
		AfxGetApp()->WriteProfileString(FORMSET_REG_SECTION, "Password", m_strNewPassword );
	}
	else
	{
		if( PasswordCheck() == FALSE )
		{
			AfxMessageBox(GetStringTable(IDS_TEXT_PASSWORD_FAIL), MB_ICONINFORMATION);
			return;
		}
	}

	OnOK();
}

BOOL CPasswordChkDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	InitFont();

	GetDlgItem(IDC_NEW_PASSWORD)->EnableWindow(FALSE);

	//20210105ksj
	if(m_bDiffPassword == TRUE)
	{
		SetWindowText(m_strTitle);
		GetDlgItem(IDC_BTN_PASSWORD_CHANGE)->EnableWindow(FALSE);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CPasswordChkDlg::OnBnClickedBtnPasswordChange()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( PasswordCheck() == TRUE )
	{		
		GetDlgItem(IDOK)->SetWindowText("CONFIRM");

		GetDlgItem(IDC_BTN_PASSWORD_CHANGE)->EnableWindow(FALSE);
		GetDlgItem(IDC_PASSWORD)->EnableWindow(FALSE);
		GetDlgItem(IDC_NEW_PASSWORD)->EnableWindow(TRUE);

		m_bPasswordChagneChk = TRUE;
	}
	else
	{
		AfxMessageBox(GetStringTable(IDS_TEXT_PASSWORD_FAIL), MB_ICONINFORMATION );
	}
}

BOOL CPasswordChkDlg::PasswordCheck()
{
	UpdateData(TRUE);

	if(m_bDiffPassword == TRUE)
	{
		if( m_strPassword != m_strDiffPassword )
		{
			return FALSE;
		}
	}
	else
	{
		CString strTempPassword = _T("");
		strTempPassword = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION, "Password", "");

		if( m_strPassword != strTempPassword )
		{
			return FALSE;
		}
	}

	return TRUE;
}

void CPasswordChkDlg::InitFont()
{
	LOGFONT	LogFont;

	GetDlgItem(IDC_STATIC1)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight = 1000;
	LogFont.lfHeight = 18;

	font.CreateFontIndirect( &LogFont );

	GetDlgItem(IDC_BTN_PASSWORD_CHANGE)->SetFont(&font);
	GetDlgItem(IDC_PASSWORD)->SetFont(&font);
	GetDlgItem(IDC_NEW_PASSWORD)->SetFont(&font);
}
/*
BOOL CPasswordChkDlg::PasswordCheck()
{
	UpdateData(TRUE);

	CString strTempPassword = _T("");
	strTempPassword = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION, "Password", "");

	if( m_strPassword != strTempPassword )
	{
		return FALSE;
	}
	
	return TRUE;
}

BOOL CPasswordChkDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	// m_strPassword = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION, "Password", m_strPassword);
	InitFont();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CPasswordChkDlg::OnBnClickedBtnPasswordChange()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( PasswordCheck() == TRUE )
	{
		if( m_bPasswordChagneChk == TRUE )
		{
			AfxGetApp()->WriteProfileString(FORMSET_REG_SECTION, "Password", m_strNewPassword );

			OnOK();
		}
		else
		{
			GetDlgItem(IDC_BTN_PASSWORD_CHANGE)->SetWindowText("CONFIRM");
			GetDlgItem(IDC_NEW_PASSWORD)->EnableWindow(TRUE);

			m_bPasswordChagneChk = TRUE;
		}		
	}
	else
	{
		AfxMessageBox(GetStringTable(IDS_TEXT_PASSWORD_FAIL), MB_ICONINFORMATION );
	}
}
*/
