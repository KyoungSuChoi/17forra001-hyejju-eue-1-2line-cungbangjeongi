#pragma once

typedef enum PACKET_ERROR
{
	PACKET_NOERROR
	, PACKET_NOTENOUGH_LENGTH
	, PACKET_HEAD_CHECK_FAIL
	, PACKET_PACKETNO_FAIL
	, PACKET_CHECKSUM_FAIL
	, PACKET_SYSTEM_FAIL

	, PACKET_END
};

static const TCHAR gszPACKET_ERROR[PACKET_END][32] = 
{
	_T("PACKET_NOERROR")
	, _T("PACKET_NOTENOUGH_LENGTH")
	, _T("PACKET_HEAD_CHECK_FAIL")
	, _T("PACKET_PACKETNO_FAIL")
	, _T("PACKET_CHECKSUM_FAIL")
	, _T("PACKET_SYSTEM_FAIL")
};

class CFMSPacketBox : public CFMSSyncParent<CFMSPacketBox>
{
public:
	CFMSPacketBox(void);
	~CFMSPacketBox(void);

	//	VOID Init(VOID) { CSyncObj Sync; m_dwCurSendNo = 0; m_dwLastRecvNo = 0; }
	INT strCutter(CHAR *msg, VOID * _dest, INT* mesmap);
	INT strLinker(CHAR *msg, VOID * _dest, INT* mesmap);
	BOOL WrapPacket(st_FMSMSGHEAD&, BYTE *, BYTE *, DWORD &rdwLen);
	PACKET_ERROR WrapOffPacket(st_FMSMSGHEAD&, BYTE *, BYTE *, DWORD &);
	//	USHORT fnCRC16_ccitt(const BYTE *buf, int len);

private:
	DWORD m_dwCurSendNo;
	DWORD m_dwLastRecvNo;
};
