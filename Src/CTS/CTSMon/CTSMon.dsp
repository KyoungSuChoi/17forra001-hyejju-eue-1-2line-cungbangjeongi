# Microsoft Developer Studio Project File - Name="CTSMon" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=CTSMon - Win32 DebugE
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "CTSMon.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "CTSMon.mak" CFG="CTSMon - Win32 DebugE"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "CTSMon - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "CTSMon - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE "CTSMon - Win32 DebugE" (based on "Win32 (x86) Application")
!MESSAGE "CTSMon - Win32 ReleaseE" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "CTSMon - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "../../../obj/CTSMon_Release"
# PROP Intermediate_Dir "../../../obj/CTSMon_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GR /GX /O2 /Op /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_AFXDLL" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x412 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x412 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 Winmm.lib /nologo /subsystem:windows /machine:I386 /out:"../../../bin/Release/CTSMon.exe"

!ELSEIF  "$(CFG)" == "CTSMon - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "../../../obj/CTSMon_Debug"
# PROP Intermediate_Dir "../../../obj/CTSMon_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W4 /Gm /GR /GX /Zi /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "_EDLC_" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x412 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x412 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 Winmm.lib /nologo /subsystem:windows /profile /debug /machine:I386 /out:"../../../bin/Debug/CTSMon.exe"

!ELSEIF  "$(CFG)" == "CTSMon - Win32 DebugE"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "CTSMon___Win32_DebugE"
# PROP BASE Intermediate_Dir "CTSMon___Win32_DebugE"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "../../../obj/CTSMon_DebugE"
# PROP Intermediate_Dir "../../../obj/CTSMon_DebugE"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W4 /Gm /GR /GX /Zi /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W4 /Gm /GR /GX /Zi /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "_EDLC_TEST_SYSTEM" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x412 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x412 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 BFServerD.lib BFCommonD.lib BFCtrlD.lib Winmm.lib PEGRAP32.LIB DataCltD.lib /nologo /subsystem:windows /profile /debug /machine:I386
# ADD LINK32 Winmm.lib /nologo /subsystem:windows /profile /debug /machine:I386 /out:"../../../bin/DebugE/CTSMon.exe"

!ELSEIF  "$(CFG)" == "CTSMon - Win32 ReleaseE"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "CTSMon___Win32_ReleaseE"
# PROP BASE Intermediate_Dir "CTSMon___Win32_ReleaseE"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "../../../obj/CTSMon_ReleaseE"
# PROP Intermediate_Dir "../../../obj/CTSMon_ReleaseE"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GR /GX /O2 /Op /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_AFXDLL" /FR /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GR /GX /O2 /Op /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_AFXDLL" /D "_EDLC_TEST_SYSTEM" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x412 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x412 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 BFServer.lib BFCommon.lib BFCtrl.lib PEGRAP32.LIB DataClt.lib Winmm.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 Winmm.lib /nologo /subsystem:windows /machine:I386 /out:"../../../bin/ReleaseE/CTSMon.exe"

!ENDIF 

# Begin Target

# Name "CTSMon - Win32 Release"
# Name "CTSMon - Win32 Debug"
# Name "CTSMon - Win32 DebugE"
# Name "CTSMon - Win32 ReleaseE"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\AccuracySetDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\AgingView.cpp
# End Source File
# Begin Source File

SOURCE=.\AllSystemView.cpp
# End Source File
# Begin Source File

SOURCE=.\CalibrationData.cpp
# End Source File
# Begin Source File

SOURCE=.\CalibratorDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CaliPoint.cpp
# End Source File
# Begin Source File

SOURCE=.\CaliSetDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CaliStartDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CellGradeListDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CellVoltageError.cpp
# End Source File
# Begin Source File

SOURCE=.\ChangeUserInfoDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ChCaliData.cpp
# End Source File
# Begin Source File

SOURCE=.\ChResultViewDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ClientSocket.cpp
# End Source File
# Begin Source File

SOURCE=.\ColorSelDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CTSMon.cpp
# End Source File
# Begin Source File

SOURCE=.\CTSMon.rc
# End Source File
# Begin Source File

SOURCE=.\CTSMonDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\CTSMonView.cpp
# End Source File
# Begin Source File

SOURCE=.\DetailChannelView.cpp
# End Source File
# Begin Source File

SOURCE=.\DetailTempCondition.cpp
# End Source File
# Begin Source File

SOURCE=.\DetailTestConditionDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\FailMsgDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\FormationModule.cpp
# End Source File
# Begin Source File

SOURCE=.\FormChannel.cpp
# End Source File
# Begin Source File

SOURCE=..\..\UtilityClass\src\FormResultFile.cpp
# End Source File
# Begin Source File

SOURCE=..\..\UtilityClass\src\FtpDownLoad.cpp
# End Source File
# Begin Source File

SOURCE=.\GradeColorDlg.cpp
# End Source File
# Begin Source File

SOURCE=..\..\UtilityClass\src\Grading.cpp
# End Source File
# Begin Source File

SOURCE=.\GroupSetDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\IconCombo.cpp
# End Source File
# Begin Source File

SOURCE=.\IPSetDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\JigIDInputDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\JigIDRegDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\LBEditorWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\ListEditCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\LoginDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\MatrixStatic.cpp
# End Source File
# Begin Source File

SOURCE=.\MeasureDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MeasureSetDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ModuleAddDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ModuleModifyDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ModuleSetDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ModuleStepEndData.cpp
# End Source File
# Begin Source File

SOURCE=.\MyGridWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\ParamSetDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\PrntScreen.cpp
# End Source File
# Begin Source File

SOURCE=.\ProcedureSelDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\RealEditCtrl.Cpp
# End Source File
# Begin Source File

SOURCE=.\RebootDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\RegTrayDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\RegulatorDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ResultLogRecordSet.cpp
# End Source File
# Begin Source File

SOURCE=.\RunDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SelEmgLogDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SelResultDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SelUnitDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SensorDataDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SensorMapDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SerialConfigDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SerialPort.cpp
# End Source File
# Begin Source File

SOURCE=.\ShowDetailResultDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=..\..\UtilityClass\src\Step.cpp
# End Source File
# Begin Source File

SOURCE=.\tabwnd.cpp
# End Source File
# Begin Source File

SOURCE=.\TempColorConfig.cpp
# End Source File
# Begin Source File

SOURCE=.\TempColorScaleConfig.cpp
# End Source File
# Begin Source File

SOURCE=.\TempView.cpp
# End Source File
# Begin Source File

SOURCE=..\..\UtilityClass\src\TestCondition.cpp
# End Source File
# Begin Source File

SOURCE=.\TestInfoDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\TestModuleRecordSet.cpp
# End Source File
# Begin Source File

SOURCE=.\TextInputDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\TopMonitoringConfigDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\TopView.cpp
# End Source File
# Begin Source File

SOURCE=..\..\UtilityClass\src\Tray.cpp
# End Source File
# Begin Source File

SOURCE=.\TrayInfoDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\TrayInputDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\UnitComLogDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\UpsSettingDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\UserAdminDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\WarnningDlg.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\AccuracySetDlg.h
# End Source File
# Begin Source File

SOURCE=.\AgingView.h
# End Source File
# Begin Source File

SOURCE=.\AllSystemView.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Include\BFCalibration.h
# End Source File
# Begin Source File

SOURCE=..\..\BFLib\Include\BFCtrlAll.h
# End Source File
# Begin Source File

SOURCE=.\CalibrationData.h
# End Source File
# Begin Source File

SOURCE=.\CalibratorDlg.h
# End Source File
# Begin Source File

SOURCE=.\CaliPoint.h
# End Source File
# Begin Source File

SOURCE=.\CaliSetDlg.h
# End Source File
# Begin Source File

SOURCE=.\CaliStartDlg.h
# End Source File
# Begin Source File

SOURCE=.\CellGradeListDlg.h
# End Source File
# Begin Source File

SOURCE=.\CellVoltageError.h
# End Source File
# Begin Source File

SOURCE=.\ChangeUserInfoDlg.h
# End Source File
# Begin Source File

SOURCE=.\ChCaliData.h
# End Source File
# Begin Source File

SOURCE=.\ChResultViewDlg.h
# End Source File
# Begin Source File

SOURCE=.\ClientSocket.h
# End Source File
# Begin Source File

SOURCE=.\ColorSelDlg.h
# End Source File
# Begin Source File

SOURCE=.\CTSMon.h
# End Source File
# Begin Source File

SOURCE=.\CTSMonDoc.h
# End Source File
# Begin Source File

SOURCE=.\CTSMonView.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Include\DataForm.h
# End Source File
# Begin Source File

SOURCE=.\DetailChannelView.h
# End Source File
# Begin Source File

SOURCE=.\DetailTempCondition.h
# End Source File
# Begin Source File

SOURCE=.\DetailTestConditionDlg.h
# End Source File
# Begin Source File

SOURCE=.\FailMsgDlg.h
# End Source File
# Begin Source File

SOURCE=.\FormationModule.h
# End Source File
# Begin Source File

SOURCE=.\FormChannel.h
# End Source File
# Begin Source File

SOURCE=..\..\UtilityClass\src\FormResultFile.h
# End Source File
# Begin Source File

SOURCE=..\..\UtilityClass\src\FtpDownLoad.h
# End Source File
# Begin Source File

SOURCE=.\GradeColorDlg.h
# End Source File
# Begin Source File

SOURCE=..\..\UtilityClass\src\Grading.h
# End Source File
# Begin Source File

SOURCE=.\GroupSetDlg.h
# End Source File
# Begin Source File

SOURCE=.\IconCombo.h
# End Source File
# Begin Source File

SOURCE=.\IPSetDlg.h
# End Source File
# Begin Source File

SOURCE=.\JigIDInputDlg.h
# End Source File
# Begin Source File

SOURCE=.\JigIDRegDlg.h
# End Source File
# Begin Source File

SOURCE=.\lbeditorwnd.h
# End Source File
# Begin Source File

SOURCE=.\ListEditCtrl.h
# End Source File
# Begin Source File

SOURCE=.\LoginDlg.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\MatrixStatic.h
# End Source File
# Begin Source File

SOURCE=.\MeasureDlg.h
# End Source File
# Begin Source File

SOURCE=.\MeasureSetDlg.h
# End Source File
# Begin Source File

SOURCE=.\ModuleAddDlg.h
# End Source File
# Begin Source File

SOURCE=.\ModuleModifyDlg.h
# End Source File
# Begin Source File

SOURCE=.\ModuleSetDlg.h
# End Source File
# Begin Source File

SOURCE=.\ModuleStepEndData.h
# End Source File
# Begin Source File

SOURCE=.\MyGridWnd.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Include\NewMsgDefine.h
# End Source File
# Begin Source File

SOURCE=.\ParamSetDlg.h
# End Source File
# Begin Source File

SOURCE=.\PrntScreen.h
# End Source File
# Begin Source File

SOURCE=.\ProcedureSelDlg.h
# End Source File
# Begin Source File

SOURCE=.\RealEditCtrl.h
# End Source File
# Begin Source File

SOURCE=.\RebootDlg.h
# End Source File
# Begin Source File

SOURCE=.\RegTrayDlg.h
# End Source File
# Begin Source File

SOURCE=.\RegulatorDlg.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\ResultLogRecordSet.h
# End Source File
# Begin Source File

SOURCE=.\RunDlg.h
# End Source File
# Begin Source File

SOURCE=.\SelEmgLogDlg.h
# End Source File
# Begin Source File

SOURCE=.\SelResultDlg.h
# End Source File
# Begin Source File

SOURCE=.\SelUnitDlg.h
# End Source File
# Begin Source File

SOURCE=.\SensorDataDlg.h
# End Source File
# Begin Source File

SOURCE=.\SensorMapDlg.h
# End Source File
# Begin Source File

SOURCE=.\serialconfigdlg.h
# End Source File
# Begin Source File

SOURCE=.\SerialPort.h
# End Source File
# Begin Source File

SOURCE=.\ShowDetailResultDlg.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=..\..\UtilityClass\src\Step.h
# End Source File
# Begin Source File

SOURCE=..\..\BFLib\src\DataBaseClass\Dao\StepRecordSet.h
# End Source File
# Begin Source File

SOURCE=.\tabwnd.h
# End Source File
# Begin Source File

SOURCE=.\TempColorConfig.h
# End Source File
# Begin Source File

SOURCE=.\TempColorScaleConfig.h
# End Source File
# Begin Source File

SOURCE=.\TempView.h
# End Source File
# Begin Source File

SOURCE=..\..\UtilityClass\src\TestCondition.h
# End Source File
# Begin Source File

SOURCE=.\TestInfoDlg.h
# End Source File
# Begin Source File

SOURCE=.\TestModuleRecordSet.h
# End Source File
# Begin Source File

SOURCE=.\TextInputDlg.h
# End Source File
# Begin Source File

SOURCE=.\TopMonitoringConfigDlg.h
# End Source File
# Begin Source File

SOURCE=.\TopView.h
# End Source File
# Begin Source File

SOURCE=..\..\UtilityClass\src\Tray.h
# End Source File
# Begin Source File

SOURCE=.\TrayInfoDlg.h
# End Source File
# Begin Source File

SOURCE=.\TrayInputDlg.h
# End Source File
# Begin Source File

SOURCE=.\UnitComLogDlg.h
# End Source File
# Begin Source File

SOURCE=.\UpsSettingDlg.h
# End Source File
# Begin Source File

SOURCE=.\UserAdminDlg.h
# End Source File
# Begin Source File

SOURCE=.\WarnningDlg.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=".\res\!Configure.ico"
# End Source File
# Begin Source File

SOURCE=".\res\ADP �ΰ�.bmp"
# End Source File
# Begin Source File

SOURCE=.\res\Apple.bmp
# End Source File
# Begin Source File

SOURCE=.\res\BadFace.bmp
# End Source File
# Begin Source File

SOURCE=".\res\Be Screen.ico"
# End Source File
# Begin Source File

SOURCE=.\res\bigmaus1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap2.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap3.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00001.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00002.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00003.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00004.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00005.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00006.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00007.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00008.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00009.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00010.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00011.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00012.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00013.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00014.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00015.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00016.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00017.bmp
# End Source File
# Begin Source File

SOURCE=.\ButtonsWin3000.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ButtonsWin3000.bmp
# End Source File
# Begin Source File

SOURCE=.\res\cali_res.bmp
# End Source File
# Begin Source File

SOURCE=.\res\CellVoltageError.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Charge.bmp
# End Source File
# Begin Source File

SOURCE=.\res\CHART6.ICO
# End Source File
# Begin Source File

SOURCE=".\res\ComputerMisc 156.ico"
# End Source File
# Begin Source File

SOURCE=".\res\ComputerMisc 163.ico"
# End Source File
# Begin Source File

SOURCE=".\res\ComputerMisc 164.ico"
# End Source File
# Begin Source File

SOURCE=".\res\ComputerMisc 165.ico"
# End Source File
# Begin Source File

SOURCE=".\res\ComputerMisc 169.ico"
# End Source File
# Begin Source File

SOURCE=".\res\ComputerMisc 189.ico"
# End Source File
# Begin Source File

SOURCE=.\res\config.ico
# End Source File
# Begin Source File

SOURCE=.\res\Connect.ico
# End Source File
# Begin Source File

SOURCE=.\res\CTSMon.ico
# End Source File
# Begin Source File

SOURCE=.\res\CTSMon.rc2
# End Source File
# Begin Source File

SOURCE=.\res\CTSMonDoc.ico
# End Source File
# Begin Source File

SOURCE=.\res\Diagram1.ico
# End Source File
# Begin Source File

SOURCE=.\res\Discharge.bmp
# End Source File
# Begin Source File

SOURCE=.\res\DisconUntitled.ico
# End Source File
# Begin Source File

SOURCE=".\res\Disk In.ico"
# End Source File
# Begin Source File

SOURCE=.\res\Door_Close.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Door_Open.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ElicoPower.bmp
# End Source File
# Begin Source File

SOURCE=.\res\EPLogo.bmp
# End Source File
# Begin Source File

SOURCE=.\res\EXC.ICO
# End Source File
# Begin Source File

SOURCE=.\res\Excell.ico
# End Source File
# Begin Source File

SOURCE=.\res\Form.bmp
# End Source File
# Begin Source File

SOURCE=.\res\GoodFace.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Green.bmp
# End Source File
# Begin Source File

SOURCE=.\res\greenButton.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Hardware.ico
# End Source File
# Begin Source File

SOURCE=.\res\hostmanager.ico
# End Source File
# Begin Source File

SOURCE=.\res\HTMLEditorPlain.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00001.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00002.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00003.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00004.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon1.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon2.ico
# End Source File
# Begin Source File

SOURCE=.\res\iserial_.ico
# End Source File
# Begin Source File

SOURCE=.\res\M_Connected.bmp
# End Source File
# Begin Source File

SOURCE=.\res\M_DisConnected.bmp
# End Source File
# Begin Source File

SOURCE=.\res\mainfram.bmp
# End Source File
# Begin Source File

SOURCE=.\res\matrixsetblue.bmp
# End Source File
# Begin Source File

SOURCE=.\res\matrixsetsmallblue.bmp
# End Source File
# Begin Source File

SOURCE=.\res\matrixsettinyblue.bmp
# End Source File
# Begin Source File

SOURCE=.\res\moduleru.bmp
# End Source File
# Begin Source File

SOURCE=.\res\MULTCAST_DLL_0.ico
# End Source File
# Begin Source File

SOURCE=".\res\Network Drive Offline.ico"
# End Source File
# Begin Source File

SOURCE=.\res\network.ico
# End Source File
# Begin Source File

SOURCE=.\res\next.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Pencil.ico
# End Source File
# Begin Source File

SOURCE=.\res\PHYSICS3.ICO
# End Source File
# Begin Source File

SOURCE=.\res\PNELogo.bmp
# End Source File
# Begin Source File

SOURCE=.\res\PowerForm.ico
# End Source File
# Begin Source File

SOURCE=.\res\prev.bmp
# End Source File
# Begin Source File

SOURCE=.\res\question.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Red.bmp
# End Source File
# Begin Source File

SOURCE=.\res\red_btn_.bmp
# End Source File
# Begin Source File

SOURCE=.\res\redButton.bmp
# End Source File
# Begin Source File

SOURCE=".\res\Shut Down.ico"
# End Source File
# Begin Source File

SOURCE=.\res\State16.bmp
# End Source File
# Begin Source File

SOURCE=.\res\state_ic.bmp
# End Source File
# Begin Source File

SOURCE=.\res\state_se.bmp
# End Source File
# Begin Source File

SOURCE=.\res\temp.bmp
# End Source File
# Begin Source File

SOURCE=.\res\TEMP.ICO
# End Source File
# Begin Source File

SOURCE=.\res\Temp_JigSettingReferense.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Temp_UnitSetting_Referense.bmp
# End Source File
# Begin Source File

SOURCE=.\res\tick.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\toolbar2.bmp
# End Source File
# Begin Source File

SOURCE=".\res\Trash Empty.ico"
# End Source File
# Begin Source File

SOURCE=.\res\tree_bit.bmp
# End Source File
# Begin Source File

SOURCE=.\res\tree_cha.bmp
# End Source File
# Begin Source File

SOURCE=.\res\tree_mod.bmp
# End Source File
# Begin Source File

SOURCE=".\res\Untitled-1 copy.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\Untitled-2 copy.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\Untitled-3 copy.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\Untitled-4 copy.bmp"
# End Source File
# Begin Source File

SOURCE=.\res\ups.bmp
# End Source File
# Begin Source File

SOURCE=.\res\usermanager.ico
# End Source File
# Begin Source File

SOURCE=.\res\Wanning.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Workdisk.ico
# End Source File
# Begin Source File

SOURCE=.\res\writingpad.ico
# End Source File
# Begin Source File

SOURCE=.\res\yell1_4.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Yellow.bmp
# End Source File
# Begin Source File

SOURCE=.\res\YellowBar.bmp
# End Source File
# End Group
# Begin Source File

SOURCE=..\BFCommon\BFCommon.dsp
# End Source File
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
# Section CTSMon : {72ADFD6C-2C39-11D0-9903-00A0C91BC942}
# 	1:21:CG_IDR_POPUP_TOP_VIEW:105
# End Section
