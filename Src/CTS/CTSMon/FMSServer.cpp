#include "StdAfx.h"
#include "CTSMon.h"

#include "FMSGlobal.h"
#include "FMSLog.h"
#include "FMSCriticalSection.h"
#include "FMSSyncParent.h"
#include "FMSStaticSyncParent.h"
#include "FMSMemoryPool.h"
#include "FMSManagedBuf.h"
#include "FMSCircularQueue.h"

#include "FMSPacketBox.h"

#include "FMSIocp.h"
#include "FMSNetObj.h"

#include "FMSNetIOCP.h"
#include "FMSRawServer.h"

#include "FMSObj.h"
#include "FMSLog.h"

#include "FMSServer.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CFMSServer::CFMSServer()
{
	mConnectCnt = 0;
	mHeartBeatCheck = 0;

	mKeepThreadHandle = NULL;
	mKeepThreadDestroyEvent = NULL;

	mObjDelete = NULL;
	m_bRunChk = FALSE;
}

CFMSServer::~CFMSServer()
{

}

DWORD WINAPI KeepThreadCallback(LPVOID parameter)
{
	CFMSServer *Owner = (CFMSServer*) parameter;
	Owner->KeepThreadCallback();

	return 0;
}

VOID CFMSServer::KeepThreadCallback(VOID)
{
	while (TRUE)
	{
		SetEvent(mObjDelete);
		DWORD Result = WaitForSingleObject(mKeepThreadDestroyEvent, 100);

		if (Result == WAIT_OBJECT_0)
			return;
			
		// 1. 전송할 CMD 데이터가 없으면 heartbeat 체크 신호를 ON 한다.
		// 2. Heartbeat 신호를 전송한 후 
		if( m_bRunChk == FALSE )
		{
			m_bRunChk = TRUE;

			if( theApp.m_FMS_SendQ.GetIsEmpty() )
			{	
				for (std::list<CFMSObj*>::iterator it=m_lstConnectedObj.begin();it!=m_lstConnectedObj.end();it++)
				{
					CFMSObj *poFMSObj = (CFMSObj*)(*it);
					if( poFMSObj->ChkHeartbeat() == TRUE )
					{
						// heartbeat 신호를 보낸다.
						CHAR cmd[5] = {0};
						sprintf(cmd, "%04s", "1001");
						fnMakeHead(m_Pack.head, cmd, 0, '0');
						memset(m_Pack.data, 0x20, sizeof(m_Pack.data));
						m_Pack.dataLen = 0;

						if( poFMSObj->Write(m_Pack.head, (BYTE*)m_Pack.data, m_Pack.dataLen) == FALSE )
						{
							if (poFMSObj->ForceClose())
							{
								OnDisconnected(poFMSObj);
							}
						}
					}
				}
			}
			else
			{					
				for (std::list<CFMSObj*>::iterator it=m_lstConnectedObj.begin();it!=m_lstConnectedObj.end();it++)				
				{
					// 1. Heartbeat 신호 초기화
					CFMSObj *poFMSObj = (CFMSObj*)(*it);
					poFMSObj->SetHeartbeatClear();
				}

				while( !theApp.m_FMS_SendQ.GetIsEmpty() )
				{
					st_FMS_PACKET pack;
					theApp.m_FMS_SendQ.Pop(pack);

					for (std::list<CFMSObj*>::iterator it=m_lstConnectedObj.begin();it!=m_lstConnectedObj.end();it++)
					{
						CFMSObj *poFMSObj = (CFMSObj*)(*it);				
						if( poFMSObj->Write(pack.head, (BYTE*)pack.data, pack.dataLen) == FALSE )
						{
							if (poFMSObj->ForceClose())
							{
								OnDisconnected(poFMSObj);
							}
						}
					}
				}
			}

			m_bRunChk = FALSE;
		}
	}
}

INT CFMSServer::strLinker(CHAR *msg, VOID * _dest, INT* mesmap)
{
	CHAR *stTemp = (CHAR *)_dest;

	UINT totlink = 0;
	UINT totidx = 0;
	UINT i = 0;
	while(mesmap[i])
	{
		INT iLink = mesmap[i++];
		memcpy(msg + totlink, stTemp + totidx, iLink); 

		totidx += iLink + 1;
		totlink += iLink;
	}

	return totidx;
}

INT CFMSServer::strCutter(CHAR *msg, VOID * _dest, INT* mesmap)
{
	CHAR *stTemp = (CHAR *)_dest;

	UINT totcut = 0;
	UINT totidx = 0;
	UINT szEndIdx = 0;
	UINT i = 0;
	while(mesmap[i])
	{
		INT iCut = mesmap[i++];
		memcpy(stTemp + totcut, msg + totidx, iCut);

		totidx += iCut;
		totcut += iCut;
		stTemp[totcut] = 0;
		totcut++;
	}

	return totidx;
}

BOOL CFMSServer::NetBegin(HWND mainframe)
{
	////////////////////////////////////////////////////////////////////////////
	////Test
	//st_FMSMSGHEAD* phead = new st_FMSMSGHEAD;
	//ZeroMemory(phead, sizeof(st_FMSMSGHEAD));
	//strCutter(g_Test_pMsg, phead, g_iHead_Map);
	//CHAR msg[1024] = {0,};
	//strLinker(msg, phead, g_iHead_Map);
	////////////////////////////////////////////////////////////////////////////
	INT idx = 0;
	INT lenidx = 0;
	UINT nPort = 7000;
	CHAR szhead[51] = {0};

	szhead[idx]  = '@';

	lenidx += g_iHead_Map[idx];//1
	memcpy(szhead + lenidx
		, AfxGetApp()->GetProfileString("FMS", "Line", "")
		, g_iHead_Map[++idx]);//4

	lenidx += g_iHead_Map[idx];
	memcpy(szhead + lenidx
		, AfxGetApp()->GetProfileString("FMS", "Sender", "")
		, g_iHead_Map[++idx]);//8

	lenidx += g_iHead_Map[idx];
	memcpy(szhead + lenidx
		, AfxGetApp()->GetProfileString("FMS", "Addressee", "")
		, g_iHead_Map[++idx]);//8

	szhead[50] = ':';

	ZeroMemory(&m_Pack, sizeof(st_FMS_PACKET));
	memset(&m_Pack.head, 0x20, sizeof(st_FMSMSGHEAD));		//Ox20 == Space
	strCutter(szhead, &m_Pack.head, g_iHead_Map);
		
	m_NotifyHwnd = mainframe;

	nPort = AfxGetApp()->GetProfileInt("FMS", "FMSPortNum", 7001);
	
	CFMSRawServer::Begin(nPort);

	mKeepThreadHandle = NULL;
	mKeepThreadDestroyEvent = NULL;

	mKeepThreadDestroyEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	if (!mKeepThreadDestroyEvent)
	{
		CFMSRawServer::End();

		return FALSE;
	}

	mKeepThreadHandle = CreateThread(NULL, 0, ::KeepThreadCallback, this, 0, NULL);
	if (!mKeepThreadHandle)
	{
		CFMSRawServer::End();

		return FALSE;
	}
	
	mObjDelete = CreateEvent(NULL, FALSE, FALSE, NULL);
	if (!mObjDelete)
	{
		CFMSRawServer::End();

		return FALSE;
	}

	return TRUE;
}
BOOL CFMSServer::NetEnd(VOID)
{
	if (mKeepThreadDestroyEvent && mKeepThreadHandle)
	{
		SetEvent(mKeepThreadDestroyEvent);

		WaitForSingleObject(mKeepThreadHandle, INFINITE);

		CloseHandle(mKeepThreadDestroyEvent);
		CloseHandle(mKeepThreadHandle);
	}

	CloseHandle(mObjDelete);

	CFMSRawServer::End();

	return TRUE;
}

VOID CFMSServer::OnConnected(CFMSObj *poNetObj)
{
	CFMSLog::WriteLog("FMS Connected Event => %d", ++mConnectCnt);
	
	if( mConnectCnt > 0 )
	{
		::PostMessage(m_NotifyHwnd, EPWM_FMS_CONNECTED, 0, 0);	
	}
	else
	{
		::PostMessage(m_NotifyHwnd, EPWM_FMS_CLOSED, 0, 0);
	}
}

VOID CFMSServer::OnDisconnected(CFMSObj *pmConnectCntoNetObj)
{
	CFMSLog::WriteLog("FMS Disconnected Event => %d", --mConnectCnt);
	
	if( mConnectCnt > 0 )
	{
		::PostMessage(m_NotifyHwnd, EPWM_FMS_CONNECTED, 0, 0);	
	}
	else
	{
		::PostMessage(m_NotifyHwnd, EPWM_FMS_CLOSED, 0, 0);
	}

	// WaitForSingleObject(mObjDelete, 5000);	
}

VOID CFMSServer::OnRead(CFMSObj *poNetObj, st_FMSMSGHEAD&_hdr, BYTE *pReadBuf, DWORD dwLen)
{
	TRACE("OnRead=====%x : %s %d========> \n", poNetObj, pReadBuf, dwLen);

	if(atoi(_hdr.Command) == E_CHARGER_INRESEVE)
	{
		st_FMS_PACKET Pack;
		ZeroMemory(&Pack, sizeof(st_FMS_PACKET));
		memcpy(&Pack.head, &_hdr, sizeof(st_FMSMSGHEAD));
		
		// CFMSLog::RecvFMSLog((CHAR*)pReadBuf);
		
		INT tempLen = 0;
		if(dwLen > 4096)
		{
			tempLen = 4095;
		}
		else
		{
			tempLen = dwLen;
		}
		memcpy(&Pack.data, pReadBuf, tempLen);
		Pack.dataLen = tempLen;
		
#ifdef _DCIR

		Pack.inreservCode = poNetObj->fnSaveDCIRWorkInfo(_hdr, pReadBuf, dwLen);

#else

		Pack.inreservCode = poNetObj->fnSaveWorkInfo(_hdr, pReadBuf, dwLen);
#endif

		theApp.m_FMS_RecvQ.Push(Pack);
	}
	else
	{
		st_FMS_PACKET Pack;
		ZeroMemory(&Pack, sizeof(st_FMS_PACKET));
		memcpy(&Pack.head, &_hdr, sizeof(st_FMSMSGHEAD));
		memcpy(&Pack.data, pReadBuf, dwLen);
		Pack.dataLen = dwLen;
		theApp.m_FMS_RecvQ.Push(Pack);
	}
}

VOID CFMSServer::OnWrite(CFMSObj *poNetObj, DWORD dwLen)
{
	TRACE("OnWrite=====%x %d========> \n", poNetObj, dwLen);
}

VOID CFMSServer::fnSendResult(const CHAR* _Longdata)
{	
	for (std::list<CFMSObj*>::iterator it=m_lstConnectedObj.begin();it!=m_lstConnectedObj.end();it++)
	{
		CFMSObj *poFMSObj = (CFMSObj*)(*it);
		poFMSObj->fnSendResult(_Longdata);
	}
}

VOID CFMSServer::fnKillNetwork()
{
	for (std::list<CFMSObj*>::iterator it=m_lstConnectedObj.begin();it!=m_lstConnectedObj.end();it++)
	{
		CFMSObj *poFMSObj = (CFMSObj*)(*it);
		poFMSObj->ForceClose();
	}
}

VOID CFMSServer::fnMakeHead(st_FMSMSGHEAD& _head, CHAR* _cmd, UINT _len, CHAR _result)
{
	strcpy_s(_head.Command, _cmd);

	COleDateTime dateTime = COleDateTime::GetCurrentTime();
	sprintf(_head.SendTime, "%s", dateTime.Format("%Y%m%d%H%M%S"));

	sprintf(_head.DataLength, "%06d", _len);

	memset(_head.ResultcCode, 0x20, 5);
	_head.ResultcCode[3] = _result;

	_head.HeadEnd[0] = ':';
}