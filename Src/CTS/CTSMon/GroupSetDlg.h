#include "afxwin.h"
#if !defined(AFX_GROUPSETDLG_H__8D7218CF_84DD_4647_B228_71897CB33FF5__INCLUDED_)
#define AFX_GROUPSETDLG_H__8D7218CF_84DD_4647_B228_71897CB33FF5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GroupSetDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CGroupSetDlg dialog

class CGroupSetDlg : public CDialog
{
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();
	virtual ~CGroupSetDlg();

	// Construction
public:
	int m_nTimeFolderInterval;
	void SetDoc(CCTSMonDoc *pDoc);
	CGroupSetDlg(CWnd* pParent = NULL);   // standard constructor
	CString m_strFontName;
//	UINT	m_nFontSize;
//	BOOL	m_bFontBold;
//	BOOL	m_bFontItalic;
//	BOOL	m_bFontUnderLine;
//	BOOL	m_bStrikeOut;
	BYTE	m_IPAddress[5];

	LOGFONT	m_afont;

// Dialog Data
	//{{AFX_DATA(CGroupSetDlg)
	enum { IDD = IDD_GROUP_SET_DLG };
	CComboBox	m_ctrlTimeCombo;
	CLabel	m_countString;
	CIPAddressCtrl	m_ctrlDataSvrIP;
	CSpinButtonCtrl	m_DataSaveSpin;
	int		m_nTopGridCol;
	BOOL	m_bAutoFileName;
	UINT	m_nDataSaveInterval;
	BOOL	m_bSaveData;
	BOOL	m_bUseDataServer;
	BOOL	m_bAutoProcDlg;
	BOOL	m_bAutoCellCheck;
	BOOL	m_bCapaSumDisplay;
	CString	m_strDataPath;
	BOOL	m_bModuleFolder;
	BOOL	m_bTimeFolder;
	CComboBox	m_ctrlTimeUnit;
	CComboBox	m_ctrlCDecimal;
	CComboBox	m_ctrlIDecimal;
	CComboBox	m_ctrlVDecimal;
	CComboBox	m_ctrlCUnit;
	CComboBox	m_ctrlIUnit;
	CComboBox	m_ctrlVUnit;
	CLabel		m_ctrFontResult;
	BOOL	m_bLotFolder;
	BOOL	m_bTrayFolder;
	CString	m_strOnlineDataPath;	
	BOOL	m_bOnlineDataErrChkforSave;
	int		m_nAfterSafeVoltageError;
	long	m_lWarnningChVoltage;
	long	m_lDangerChVoltage;
	BOOL	m_bJigTargetTempUse;
	BOOL	m_bChSafetyUse;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGroupSetDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CCTSMonDoc *m_pDoc;
	// Generated message map functions
	//{{AFX_MSG(CGroupSetDlg)
	afx_msg void OnOk();
	virtual BOOL OnInitDialog();
	afx_msg void OnRealTimeDataSave();
	afx_msg void OnFontSetBtn();
	afx_msg void OnFolderButton();
	afx_msg void OnTimeFolderCheck();
	afx_msg void OnDataServerCheck();
	afx_msg void OnButton1();
	afx_msg void OnOnlinedatafolderBtn();
	afx_msg void OnCheckJigTargettempUse();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	CComboBox m_ctrlTempPosition;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GROUPSETDLG_H__8D7218CF_84DD_4647_B228_71897CB33FF5__INCLUDED_)
