#if !defined(AFX_SAVEITEMSETDLG_H__5B6BEFF7_4C82_4F58_A4AF_B010B8329F7C__INCLUDED_)
#define AFX_SAVEITEMSETDLG_H__5B6BEFF7_4C82_4F58_A4AF_B010B8329F7C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SaveItemSetDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSaveItemSetDlg dialog
#define REG_SECTION	"SaveSetting"

class CSaveItemSetDlg : public CDialog
{
// Construction
public:
	CSaveItemSetDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSaveItemSetDlg)
	enum { IDD = IDD_SAVE_SET_DIALOG };
	BOOL	m_bState;
	BOOL	m_bTime;
	BOOL	m_bVoltage;
	BOOL	m_bCurrent;
	BOOL	m_bCapacity;
	BOOL	m_bCapEff;
	BOOL	m_bImpedance;
	BOOL	m_bWatt;
	BOOL	m_bWattHour;
	BOOL	m_bGradeCode;
	BOOL	m_bCellCode;
	BOOL	m_bType;
	BOOL	m_bSumCap;

	BOOL	m_bTimeGetChargeVoltage;
	BOOL	m_bComCapacity;
	BOOL	m_bCCTime;
	BOOL	m_bComDcir;
	BOOL	m_bDcir;
	BOOL	m_bV1;
	BOOL	m_bV2;
	BOOL	m_bAvgJigTemp;
	BOOL	m_bAvgCurrent;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSaveItemSetDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	CFont m_Font;
	void InitFont();

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSaveItemSetDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SAVEITEMSETDLG_H__5B6BEFF7_4C82_4F58_A4AF_B010B8329F7C__INCLUDED_)
