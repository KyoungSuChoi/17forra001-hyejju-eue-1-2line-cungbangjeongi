// GroupSetDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSMon.h"
#include "CTSMonDoc.h"
#include "GroupSetDlg.h"
//#include "ChDataSaveCfgDlg.h"

#include "FolderDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;

#endif

/////////////////////////////////////////////////////////////////////////////
// CGroupSetDlg dialog

CGroupSetDlg::CGroupSetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CGroupSetDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CGroupSetDlg)
	m_nTopGridCol = 0;
	m_bAutoFileName = FALSE;
	m_nDataSaveInterval = 2;
	m_bSaveData = FALSE;
	m_bUseDataServer = FALSE;
	m_bAutoProcDlg = FALSE;
	m_bAutoCellCheck = FALSE;
	m_bCapaSumDisplay = FALSE;
	m_strDataPath = _T("");
	m_bModuleFolder = FALSE;
	m_bTimeFolder = FALSE;
	m_bLotFolder = FALSE;
	m_bTrayFolder = FALSE;
	m_strOnlineDataPath = _T("");	
	m_bOnlineDataErrChkforSave = FALSE;
	m_nAfterSafeVoltageError = 0;
	m_lWarnningChVoltage = 0;
	m_lDangerChVoltage = 0;
	m_bJigTargetTempUse = FALSE;
	m_bChSafetyUse = FALSE;
	//}}AFX_DATA_INIT
//	m_nFontSize = 9;
//	m_bFontBold = FALSE;
//	m_bFontItalic = FALSE;
//	m_bFontUnderLine = FALSE;
//	m_strFontName = "Times New Roman";
//	m_bStrikeOut = FALSE;

	m_nTimeFolderInterval = 30;	
	m_pDoc = NULL;
	LanguageinitMonConfig();
}



CGroupSetDlg::~CGroupSetDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

bool CGroupSetDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CGroupSetDlg"), _T("TEXT_CGroupSetDlg_CNT"), _T("TEXT_CGroupSetDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CGroupSetDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CGroupSetDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

void CGroupSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGroupSetDlg)
	DDX_Control(pDX, IDC_TIME_COMBO, m_ctrlTimeCombo);
	DDX_Control(pDX, IDC_SEL_COUNT_STATIC, m_countString);
	DDX_Control(pDX, IDC_IPADDRESS1, m_ctrlDataSvrIP);
	DDX_Control(pDX, IDC_DATA_SAVE_SPIN, m_DataSaveSpin);
	DDX_Text(pDX, IDC_EDIT2, m_nTopGridCol);
	DDX_Check(pDX, IDC_AUTO_FILE_NAME, m_bAutoFileName);
	DDX_Text(pDX, IDC_DATA_SAVE_INTERVAL, m_nDataSaveInterval);
	DDV_MinMaxUInt(pDX, m_nDataSaveInterval, 0, 1000);
	DDX_Check(pDX, IDC_REAL_TIME_DATA_SAVE, m_bSaveData);
	DDX_Check(pDX, IDC_DATA_SERVER_CHECK, m_bUseDataServer);
	DDX_Check(pDX, IDC_AUTO_DLG, m_bAutoProcDlg);
	DDX_Check(pDX, IDC_CELL_CHECK_AUTO_START, m_bAutoCellCheck);
	DDX_Check(pDX, IDC_CAPA_SUM_DISPLAY_CHECK, m_bCapaSumDisplay);
	DDX_Text(pDX, IDC_DATA_FOLDER_EDIT, m_strDataPath);
	DDX_Check(pDX, IDC_MODULE_FOLDER_CHECK, m_bModuleFolder);
	DDX_Check(pDX, IDC_TIME_FOLDER_CHECK, m_bTimeFolder);
	DDX_Control(pDX, IDC_TIME_UNIT_COMBO, m_ctrlTimeUnit);
	DDX_Control(pDX, IDC_C_PRESI_COMBO, m_ctrlCDecimal);
	DDX_Control(pDX, IDC_I_PRESI_COMBO, m_ctrlIDecimal);
	DDX_Control(pDX, IDC_V_PRESI_COMBO, m_ctrlVDecimal);
	DDX_Control(pDX, IDC_C_UNIT_COMBO, m_ctrlCUnit);
	DDX_Control(pDX, IDC_I_UNIT_COMBO, m_ctrlIUnit);
	DDX_Control(pDX, IDC_V_UNIT_COMBO, m_ctrlVUnit);
	DDX_Control(pDX, IDC_FONT_NAME, m_ctrFontResult);
	DDX_Check(pDX, IDC_CHECK_LOT, m_bLotFolder);
	DDX_Check(pDX, IDC_CHECK_TRAY, m_bTrayFolder);
	DDX_Text(pDX, IDC_ONLINEDATA_FOLDER_EDIT, m_strOnlineDataPath);	
	DDX_Check(pDX, IDC_ONLINEDATA_ERRUNITCHK_SAVE, m_bOnlineDataErrChkforSave);
	DDX_CBIndex(pDX, IDC_AFTER_SAFE_VOLTAGEERROR, m_nAfterSafeVoltageError);
	DDX_Text(pDX, IDC_EDIT_WARNNING_CH_VOLTAGE, m_lWarnningChVoltage);
	DDX_Text(pDX, IDC_EDIT_DANGER_CH_VOLTAGE, m_lDangerChVoltage);
	DDX_Check(pDX, IDC_CHECK_JIG_TARGETTEMP_USE, m_bJigTargetTempUse);
	DDX_Check(pDX, IDC_CHECK_CH_SAFETY_USE, m_bChSafetyUse);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_TEMP_POSITION_COMBO, m_ctrlTempPosition);
}


BEGIN_MESSAGE_MAP(CGroupSetDlg, CDialog)
	//{{AFX_MSG_MAP(CGroupSetDlg)
	ON_BN_CLICKED(IDOK, OnOk)
	ON_BN_CLICKED(IDC_REAL_TIME_DATA_SAVE, OnRealTimeDataSave)
	ON_BN_CLICKED(IDC_FONT_SET_BTN, OnFontSetBtn)
	ON_BN_CLICKED(IDC_FOLDER_BUTTON, OnFolderButton)
	ON_BN_CLICKED(IDC_TIME_FOLDER_CHECK, OnTimeFolderCheck)
	ON_BN_CLICKED(IDC_DATA_SERVER_CHECK, OnDataServerCheck)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	ON_BN_CLICKED(IDC_ONLINEDATAFOLDER_BTN, OnOnlinedatafolderBtn)
	ON_BN_CLICKED(IDC_CHECK_JIG_TARGETTEMP_USE, OnCheckJigTargettempUse)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGroupSetDlg message handlers

void CGroupSetDlg::OnOk() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	CString strTemp;

//	m_DataSaveSpin.SetRange(1,1000);	
	AfxGetApp()->WriteProfileBinary(FORMSET_REG_SECTION, "GridFont",(LPBYTE)&m_afont, sizeof(LOGFONT));
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "SafeChWarnningVoltage", m_lWarnningChVoltage);
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "SafeChDangerVoltage", m_lDangerChVoltage);	
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "AfterSafeVoltageError", m_nAfterSafeVoltageError);
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "AutoRun", m_bAutoFileName);
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "DataSave", m_bSaveData);
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "SaveInterval", m_nDataSaveInterval);
	
	// AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "TopGridColCout", m_nTopGridCol);
//	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "TopGridFontSize", m_nFontSize);
//	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "TopGridFontBold", m_bFontBold);
//	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "TopGridFontItalic", m_bFontItalic);
//	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "TopGridFontUnderLine", m_bFontUnderLine);
//	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "TopGridFontStrikeOut", m_bStrikeOut);
//	AfxGetApp()->WriteProfileString(FORMSET_REG_SECTION ,"TopGridFontName", m_strFontName);

	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "Use Data Server", m_bUseDataServer);
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "AutoCellCheck", m_bAutoCellCheck);
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "Capacity Sum", m_bCapaSumDisplay);

	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "ModuleFolder", m_bModuleFolder);
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "TimeFolder", m_bTimeFolder);
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "LotFolder", m_bLotFolder);
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "TrayFolder", m_bTrayFolder);
	AfxGetApp()->WriteProfileString(FORM_PATH_REG_SECTION, "Data", m_strDataPath);
	AfxGetApp()->WriteProfileString(FORM_PATH_REG_SECTION, "OnlineData", m_strOnlineDataPath);
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "OnlineDataErrChkForSave", m_bOnlineDataErrChkforSave);
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "UseJigTargetTemp", m_bJigTargetTempUse);
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "UseChSafety", m_bChSafetyUse);

	
	int a = m_ctrlTimeCombo.GetCurSel();
	if(a <= 0)
	{
		m_nTimeFolderInterval = 1;
	}
	else 
	{
		m_nTimeFolderInterval = a*10;
	}
	
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "FolderTimeInterval", m_nTimeFolderInterval);

	char szBuff[32], szUnit[16], szDecimalPoint[16];
	m_ctrlVUnit.GetLBText(m_ctrlVUnit.GetCurSel(), szUnit);
	m_ctrlVDecimal.GetLBText(m_ctrlVDecimal.GetCurSel(), szDecimalPoint);
	sprintf(szBuff, "%s %s", szUnit, szDecimalPoint);
	AfxGetApp()->WriteProfileString(FORMSET_REG_SECTION, "V Unit", szBuff);

	m_ctrlIUnit.GetLBText(m_ctrlIUnit.GetCurSel(), szUnit);
	m_ctrlIDecimal.GetLBText(m_ctrlIDecimal.GetCurSel(), szDecimalPoint);
	sprintf(szBuff, "%s %s", szUnit, szDecimalPoint);
	AfxGetApp()->WriteProfileString(FORMSET_REG_SECTION, "I Unit", szBuff);

	m_ctrlCUnit.GetLBText(m_ctrlCUnit.GetCurSel(), szUnit);
	m_ctrlCDecimal.GetLBText(m_ctrlCDecimal.GetCurSel(), szDecimalPoint);
	sprintf(szBuff, "%s %s", szUnit, szDecimalPoint);
	AfxGetApp()->WriteProfileString(FORMSET_REG_SECTION, "C Unit", szBuff);

	sprintf(szBuff, "%d", m_ctrlTimeUnit.GetCurSel());
	AfxGetApp()->WriteProfileString(FORMSET_REG_SECTION, "Time Unit", szBuff);

	sprintf(szBuff, "%d", m_ctrlTempPosition.GetCurSel());
	AfxGetApp()->WriteProfileString(FORMSET_REG_SECTION, "Temp Measuring Position", szBuff);
	
	m_ctrlDataSvrIP.GetAddress(m_IPAddress[0], m_IPAddress[1], m_IPAddress[2], m_IPAddress[3] );
	strTemp.Format("%d.%d.%d.%d", m_IPAddress[0], m_IPAddress[1], m_IPAddress[2], m_IPAddress[3]);
	AfxGetApp()->WriteProfileString(FORMSET_REG_SECTION ,"Data Server IP", strTemp);
//	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "TestInfoDlg", m_bAutoProcDlg);
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "HideNonCellData", m_bAutoProcDlg);

	GetDlgItem(IDC_EDIT_EXCEL_PATH)->GetWindowText(strTemp);
	AfxGetApp()->WriteProfileString(FORM_PATH_REG_SECTION ,"Excel Path", strTemp);

	m_pDoc->m_bHideNonCellData = m_bAutoProcDlg;

	CDialog::OnOK();
}

BOOL CGroupSetDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
//	m_DataSaveSpin.ShowWindow(SW_HIDE);
//	m_DataSaveSpin.SetBuddy(GetDlgItem(IDC_DATA_SAVE_INTERVAL));

	UINT nSize;
	LPVOID* pData;
	BOOL nRtn = AfxGetApp()->GetProfileBinary(FORMSET_REG_SECTION, "GridFont", (LPBYTE *)&pData, &nSize);
	if(nRtn && nSize > 0)	
	{
		memcpy(&m_afont, pData, nSize);
	}
	else
	{
		CFont *pFont = GetFont();
		pFont->GetLogFont(&m_afont);
	}
	delete [] pData;

	m_lWarnningChVoltage = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "SafeChWarnningVoltage", 4400);
	m_lDangerChVoltage = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "SafeChDangerVoltage", 4700);
	m_nAfterSafeVoltageError = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "AfterSafeVoltageError", 0);
//	m_nTopGridCol = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "TopGridColCout", 8);
	m_bAutoFileName = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "AutoRun", 0);
	m_bSaveData = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "DataSave", 0);
	m_nDataSaveInterval = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "SaveInterval", 2);

//	m_nFontSize = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "TopGridFontSize", 9);
//	m_bFontBold = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "TopGridFontBold", 0);
//	m_bFontItalic = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "TopGridFontItalic", 0);
//	m_bFontUnderLine = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "TopGridFontUnderLine", 0);
//	m_bStrikeOut = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "TopGridFontStrikeOut", 0);
//	m_strFontName = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION ,"TopGridFontName", "Times New Roman");

	m_bUseDataServer = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION ,"Use Data Server", FALSE);
//	m_bAutoProcDlg = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "TestInfoDlg", FALSE);
	m_bAutoProcDlg = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "HideNonCellData", FALSE);

	m_bAutoCellCheck = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "AutoCellCheck", FALSE);
	m_bCapaSumDisplay = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "Capacity Sum", FALSE);

	m_bModuleFolder = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "ModuleFolder", FALSE);
	m_bTimeFolder = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "TimeFolder", FALSE);
	m_bLotFolder = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "LotFolder", FALSE);
	m_bTrayFolder = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "TrayFolder", FALSE);

	m_strDataPath = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION, "Data", "");
	m_strOnlineDataPath = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION, "OnlineData", "");
	m_bOnlineDataErrChkforSave = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "OnlineDataErrChkForSave", 1);
	m_bJigTargetTempUse = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "UseJigTargetTemp", 0);	
	m_bChSafetyUse = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "UseChSafety", 0);	
	
	m_nTimeFolderInterval = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "FolderTimeInterval", 30);
	int a = m_nTimeFolderInterval/10;
	m_ctrlTimeCombo.SetCurSel(a);
	GetDlgItem(IDC_TIME_COMBO)->EnableWindow(m_bTimeFolder);

	//////////////////////////////////////////////////////////////////////////
	char szBuff[32], szUnit[16], szDecimalPoint[16];
	sprintf(szBuff, AfxGetApp()->GetProfileString(FORMSET_REG_SECTION, "V Unit", "V 0"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_ctrlVUnit.SelectString(0, szUnit);
	m_ctrlVDecimal.SelectString(0,szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(FORMSET_REG_SECTION, "I Unit", "mA 0"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_ctrlIUnit.SelectString(0,szUnit);
	m_ctrlIDecimal.SelectString(0,szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(FORMSET_REG_SECTION, "C Unit", "mAh 0"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);

#ifdef _EDLC_TEST_SYSTEM
	m_ctrlCUnit.ResetContent();
	m_ctrlCUnit.AddString("F");
	m_ctrlCUnit.AddString("mF");
#endif

	m_ctrlCUnit.SelectString(0,szUnit);
	m_ctrlCDecimal.SelectString(0,szDecimalPoint);
	
	sprintf(szBuff, AfxGetApp()->GetProfileString(FORMSET_REG_SECTION, "Time Unit", "0"));
	m_ctrlTimeUnit.SetCurSel(atol(szBuff));

	sprintf(szBuff, AfxGetApp()->GetProfileString(FORMSET_REG_SECTION, "Temp Measuring Position", "0"));
	m_ctrlTempPosition.SetCurSel(atol(szBuff));
	//////////////////////////////////////////////////////////////////////////
	
	GetDlgItem(IDC_EDIT_EXCEL_PATH)->SetWindowText(AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"Excel Path", ""));
	sprintf(szBuff, "%d", AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION ,"System ID", 0));
	GetDlgItem(IDC_EDIT_SYTEM_ID)->SetWindowText(szBuff);

	CString strTemp;
	strTemp = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION ,"Data Server IP", "");
	strTemp.Replace('.', ' ');
	sscanf((LPSTR)(LPCTSTR)strTemp, "%d %d %d %d", &m_IPAddress[0], &m_IPAddress[1], &m_IPAddress[2], &m_IPAddress[3]);
	m_ctrlDataSvrIP.SetAddress(m_IPAddress[0], m_IPAddress[1], m_IPAddress[2], m_IPAddress[3]);

	// TODO: Add extra initialization here
	if(!m_bSaveData)
	{
		GetDlgItem(IDC_DATA_SAVE_INTERVAL)->EnableWindow(FALSE);
//		m_DataSaveSpin.EnableWindow(FALSE);
	}
//	m_ctrlFontName.SetText(m_strFontName);

/*	STR_GROUP_INFO *pData = NULL;
	int count = 0;
	int mdID;
	for (int nI = 0; nI < EPGetInstalledModuleNum(); nI++)
	{
		mdID = EPGetModuleID(nI);
		if(EPGetGroupState(mdID, 0) == EP_STATE_LINE_OFF)		continue;//Group State
		pData = m_pDoc->GetGroupData(mdID, 0);		//Group Data
		if(pData != NULL)
		{
//			count += pData->saveChList.GetSize();
		}
	}

	strTemp.Format(::GetStringTable(IDS_MSG_SEL_CHANNEL_COUNT), count);
	m_countString.SetText(strTemp);
*/	
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CGroupSetDlg::OnRealTimeDataSave() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

	if(!m_bSaveData)
	{
		GetDlgItem(IDC_DATA_SAVE_INTERVAL)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHANNEL_SELECT)->EnableWindow(FALSE);
//		m_DataSaveSpin.EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_DATA_SAVE_INTERVAL)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHANNEL_SELECT)->EnableWindow(FALSE);

//		GetDlgItem(IDC_CHANNEL_SELECT)->EnableWindow(TRUE);
//		m_DataSaveSpin.EnableWindow(TRUE);
	}	
}

void CGroupSetDlg::OnFontSetBtn() 
{
	// TODO: Add your control notification handler code here

	CFontDialog aDlg(&m_afont);
	if(aDlg.DoModal()==IDOK)
	{
		aDlg.GetCurrentFont(&m_afont);
		m_ctrFontResult.SetUserFont(&m_afont);
	}


/*	LOGFONT lf;
	memset(&lf, 0, sizeof(LOGFONT));

	CClientDC dc(this);
	lf.lfHeight = -MulDiv(m_nFontSize, dc.GetDeviceCaps(LOGPIXELSY), 72);
	sprintf(lf.lfFaceName, "%s", m_strFontName);
	lf.lfItalic = (BYTE)m_bFontItalic;
	lf.lfUnderline = (BYTE)m_bFontUnderLine;
	lf.lfStrikeOut = (BYTE)m_bStrikeOut;
	
	if(m_bFontBold)
	{
		lf.lfWeight = FW_BOLD;
	}
	else
	{
		lf.lfWeight = FW_NORMAL;
	}

	CFontDialog dlg(&lf);
	dlg.DoModal();

	m_strFontName = dlg.GetFaceName();
	m_nFontSize = dlg.GetSize()/10;
	m_bFontItalic = dlg.IsItalic();
	m_bFontUnderLine = dlg.IsUnderline();
	m_bFontBold = dlg.IsBold();
	m_bStrikeOut = dlg.IsStrikeOut();

	m_ctrlFontName.SetText(m_strFontName);
*/
}

/*
void CGroupSetDlg::OnChannelSelect() 
{
	// TODO: Add your control notification handler code here
/*	CChDataSaveCfgDlg *pDlg;
	pDlg = new CChDataSaveCfgDlg(this);

	pDlg->SetDoc(m_pDoc);

	pDlg->DoModal();

	delete pDlg;
	pDlg = NULL;
*/

/*	CString strTemp;

	strTemp.Format("현재 %d개의 Channel 이 선택되어 있습니다.", m_pDoc->ChSaveSetting());
	m_countString.SetText(strTemp);

}
*/
void CGroupSetDlg::SetDoc(CCTSMonDoc *pDoc)
{
	ASSERT(pDoc);
	m_pDoc = pDoc;
}

void CGroupSetDlg::OnFolderButton() 
{
	// TODO: Add your control notification handler code here
	CFolderDialog dlg(m_strDataPath);
	if( dlg.DoModal() == IDOK)
	{
		m_strDataPath = dlg.GetPathName();
		UpdateData(FALSE);
	}	
}

void CGroupSetDlg::OnTimeFolderCheck() 
{
	// TODO: Add your control notification handler code here
	UpdateData();

	GetDlgItem(IDC_TIME_COMBO)->EnableWindow(m_bTimeFolder);
}

void CGroupSetDlg::OnDataServerCheck() 
{
	// TODO: Add your control notification handler code here
	UpdateData();

	GetDlgItem(IDC_IPADDRESS1)->EnableWindow(m_bUseDataServer);
}

void CGroupSetDlg::OnButton1() 
{
	// TODO: Add your control notification handler code here
	CString str;
	GetDlgItem(IDC_EDIT_EXCEL_PATH)->GetWindowText(str);
	if(str.IsEmpty())
	{
		str = "C:\\Program Files\\Microsoft Office\\OFFICE11\\EXCEL.EXE";
	}
	
	CFileDialog pDlg(TRUE, "exe", str, OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, "exe file(*.exe)|*.exe|All Files(*.*)|*.*|");
	pDlg.m_ofn.lpstrTitle =TEXT_LANG[0] ;//"Excel 파일 위치"
	if(IDOK == pDlg.DoModal())
	{
		str = pDlg.GetPathName();
		GetDlgItem(IDC_EDIT_EXCEL_PATH)->SetWindowText(str);	
	}	
}

void CGroupSetDlg::OnOnlinedatafolderBtn() 
{
	// TODO: Add your control notification handler code here
	AfxMessageBox(TEXT_LANG[1]);//"Online Mode 에서 Summary 파일이 저장되는 폴더입니다."
	CFolderDialog dlg(m_strOnlineDataPath);	
	if( dlg.DoModal() == IDOK)
	{
		m_strOnlineDataPath = dlg.GetPathName();
		UpdateData(FALSE);
	}	
}

void CGroupSetDlg::OnCheckJigTargettempUse() 
{
	// TODO: Add your control notification handler code here
	AfxMessageBox(GetStringTable(IDS_TEXT_RESTART_ALERT));
}
