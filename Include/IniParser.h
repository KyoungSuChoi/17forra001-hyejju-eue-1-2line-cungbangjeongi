#ifndef _NS_INIPARSER_H_
#define _NS_INIPARSER_H_

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <sys/stat.h>
#include <locale.h>

#ifdef __cplusplus

class IniParser
{
public:
  	
    char    *mFileBuf;
    char    *mFilename;
    int     mFileBufSize;
    int     mError;
    int     mfWrite;

	int FindSection(char *aSection, char **aOutSecPtr);
    int FindKey(char *aSecPtr, char *aKey, char **aOutSecPtr);
    int GetValue(char *aSecPtr, char *aKey, char *aVal, int *aIOValSize);
    int GetAllKeys(char *aSecPtr, char *aVal, int *aIOValSize);

	void GetFileRead(char *aFilename);
    int GetString(char *aSection,char *aKey, char *aValBuf, int *aIOValBufSize );   
    int GetStringAlloc( char *aSection, char *aKey,char **aOutBuf, int *aOutBufSize );

    int GetError();
    static char *ResolveName(char *aINIRoot);

    int WriteString(char *aSection, char *aKey, char *aValBuf );
    void SetFileWrite();


	//file
	unsigned long GetPrivateProfileString(const char* szAppName,const char* szKeyName,
                                          const char* szDefault,const char* szReturnedString,
                                          int nSize,const char* szFileName);
    unsigned long WritePrivateProfileString(const char* szAppName,const char* szKeyName,
                                            const char* szValue,const char* szFileName);

	//str
	unsigned long GetPrivateProstrString(const char* szAppName,const char* szKeyName,
									     const char* szDefault,const char* szReturnedString,
										 int nSize,const char* szFileStr);
    //Errors
    enum
    {
        OK                  = 0,
        E_READ              = -701,
        E_MEM               = -702,
        E_PARAM             = -703,
        E_NO_SEC            = -704,
        E_NO_KEY            = -705,
        E_SEC_CORRUPT       = -706,
        E_SMALL_BUF         = -707
    };

	 IniParser();
    ~IniParser();
  
private:

};

#define INI_TMP_BUFSIZE  65536

class GIni 
{
public:   
	GIni();
    ~GIni();

	static int ini_GetCharCnt( CString session, char key )
	{
		int nCnt = 0;
		int nTemp = 0;
		int i=0;

		nTemp = session.GetLength();

		if( nTemp != 0 )
		{
			char *pBuf = new char[nTemp+1];
			sprintf( pBuf, "%s", session );			

			for( i=0; i<nTemp; i++ )
			{
				if( pBuf[i] == key )
				{
					nCnt++;
				}
			}

			delete[] pBuf;
		}

		return nCnt;
	}

	static CString ini_GetStr(CString sdata,CString session,CString key,CString basicv)
	{//스트링에서 구하기
		if(sdata.IsEmpty() || sdata==_T(""))     return basicv;
		if(session.IsEmpty() || session==_T("")) return basicv;
		if(key.IsEmpty() || key==_T(""))         return basicv;

		CString str=_T("");
		DWORD dwRead=0;
		IniParser parser;

		for(DWORD dwSize = INI_TMP_BUFSIZE-1;; dwSize = (dwSize+1)*2-1)
		{			
			#ifdef _UNICODE	
				dwRead = parser.GetPrivateProstrString(CStringA(session),CStringA(key),CStringA(basicv),
											           CStringA(str.GetBuffer(dwSize)),dwSize, CStringA(sdata));
				str.ReleaseBuffer();
			#else
				dwRead = parser.GetPrivateProstrString(session,key,basicv,
											       str.GetBuffer(dwSize),dwSize, sdata);
				str.ReleaseBuffer();
			#endif
			
		    if (dwRead < dwSize-1) break;    
		}
		str.TrimLeft();
		str.TrimRight();

		CString svalue;
		svalue.Format(_T("%s"),str);
		return svalue;
	}

	static void ini_GetMulstr(CString sdata,CString sort,CString key,CStringArray& Data)
	{//스트링에서 멀티 구하기
		if(sdata.IsEmpty() || sdata==_T("")) return;
		if(sort.IsEmpty() || sort==_T(""))   return;

		CString scount=_T("");
		CString str=_T("");
		Data.RemoveAll();
		int count=0;
		for(int i=0;;i++)
		{
		  scount.Format(_T("%s%d"),key,i);		  
		  scount.TrimLeft();
		  scount.TrimRight();

		  str=ini_GetStr(sdata,sort,scount,_T(""));		  
		  
		  if(str!=_T("")) Data.Add(str);
		  else            count=count+1;

		  if(count>10) break;
		}
	}

	static void ini_SetFile(CString file,CString session,CString key,CString value)
	{//파일 저장		
		WritePrivateProfileString(session,key,value,file);
	}

	static void ini_SetEmpty(CString file,CString sort)
	{//삭제
		WritePrivateProfileString(sort, NULL, NULL, file); //delete
	}

	static void ini_SetMuldata(CString file,CString sort,CString key,CStringArray& Data)
	{//파일 멀티 저장
		WritePrivateProfileString(sort, NULL, NULL, file); //delete

		CString scount=_T("");
		CString data=_T("");

		if(Data.GetSize()<1) return;
		for(int i=0;i<Data.GetSize();i++)
		{
		  scount.Format(_T("%s%d"),key,i);
		  data=Data.GetAt(i);
		  if(data!=_T("")) ini_SetFile(file,sort,scount,data);
		}
	}

	static BOOL ini_GetSNameFile(CStringArray &Data,CString file)
	{//파일에서 구하기
		CString tmpBuf=_T("");  
		Data.RemoveAll();
		LPTSTR szTmpBuf = tmpBuf.GetBuffer(INI_TMP_BUFSIZE);   
		tmpBuf.ReleaseBuffer();
		DWORD bufLen = ::GetPrivateProfileSectionNames(szTmpBuf,INI_TMP_BUFSIZE,file);

		if(bufLen < INI_TMP_BUFSIZE-2) 
		{
		  LPCTSTR bufPtr;
		  for (bufPtr=szTmpBuf ;bufPtr[0]!=_T('\0'); bufPtr+=_tcslen(bufPtr)+1)
		  {
			  Data.Add(bufPtr);		 
		  }
		  return TRUE;
		}
		return FALSE; 
	}

	static void ini_GetMuldata(CString file,CString sort,CString key,CStringArray& Data)
	{//파일에서 멀티 구하기
		CString scount=_T("");
		CString sdata=_T("");
		Data.RemoveAll();
		int count=0;

		for(int i=0;;i++)
		{
			scount.Format(_T("%s%d"),key,i);
			sdata=ini_GetFile(file,sort,scount,_T(""));

			if(sdata!=_T("")) Data.Add(sdata);
			else count=count+1;

			if(count>10) break;
		}
	}

	static CString ini_GetLangText(CString file, CString session, CString key, CString basicv, int nLanguageSelect = -1)
	{
		if(file.IsEmpty() || file==_T(""))       return basicv;
		if(session.IsEmpty() || session==_T("")) return basicv;
		if(key.IsEmpty() || key==_T(""))         return basicv;

		CString str=_T("");
		CString strReadData = _T("");
		CString svalue = _T("");

		DWORD dwRead;		
		int nCnt = 0;

		for(DWORD dwSize = INI_TMP_BUFSIZE-1;; dwSize = (dwSize+1)*2-1){
			dwRead = GetPrivateProfileString(session, key, basicv, str.GetBuffer(dwSize),dwSize, file);
			str.ReleaseBuffer();
			if (dwRead < dwSize-1) break;
		}
		str.TrimLeft();
		str.TrimRight();

		svalue = basicv;

		if( !str.IsEmpty() )
		{	
			if( nLanguageSelect == -1 )
			{
				svalue = str;
			}
			else
			{
				nCnt = ini_GetCharCnt( str, ',' );
				
				if( nLanguageSelect < nCnt )
				{
					AfxExtractSubString(strReadData ,str, nLanguageSelect, ',');
					strReadData.TrimLeft();
					strReadData.TrimRight();
					svalue.Format(_T("%s"), strReadData);
				}
			}
		}

		return svalue;
	}	

	static CString ini_GetFile(CString file, CString session, CString key, CString basicv)
	{//파일에서 구하기
		if(file.IsEmpty() || file==_T(""))       return basicv;
		if(session.IsEmpty() || session==_T("")) return basicv;
		if(key.IsEmpty() || key==_T(""))         return basicv;

		CString str=_T("");
		CString strReadData = _T("");
		DWORD dwRead;		

		for(DWORD dwSize = INI_TMP_BUFSIZE-1;; dwSize = (dwSize+1)*2-1){
		   dwRead = GetPrivateProfileString(session, key, basicv, str.GetBuffer(dwSize),dwSize, file);
		   str.ReleaseBuffer();
		   if (dwRead < dwSize-1) break;
		}
		str.TrimLeft();
		str.TrimRight();

		return str;
	}

	static CStringArray* ini_GetMuldata(CString file,CString sort,CString key)
	{//파일에서 멀티 구하기
		CString scount=_T("");
		CString sdata=_T("");
		CStringArray *Data=new CStringArray[1];
		int count=0;

		for(int i=0;;i++)
		{			
		  scount.Format(_T("%s%d"),key,i);		  
		  sdata=ini_GetFile(file,sort,scount,_T(""));

		  if(sdata!=_T("")) Data->Add(sdata);
		  else count=count+1;

		  if(count>10) break;
		}
		return Data;
	}
};


#define NL '\n'
#define NLSTRING "\n"
#define MAX_VAL_SIZE 512

#if defined(DUMP)
#undef DUMP
#endif
#if defined(DEBUG_sgehani) || defined(DEBUG_druidd) || defined(DEBUG_root)
#define DUMP(_msg) printf("%s %d: %s \n", __FILE__, __LINE__, _msg);
#else
#define DUMP(_msg)
#endif
#endif


#endif /*_NS_INIPARSER_H_ */


