// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__F4FB9D04_24EC_481C_A65F_6AB4A26C80FF__INCLUDED_)
#define AFX_MAINFRM_H__F4FB9D04_24EC_481C_A65F_6AB4A26C80FF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define SW_VERSION "v2.0.0.0.E2"

#include "TopView.h"
//#include "ProcesureFormView.h"
//#include "ResultView.h"
#include "AllSystemView.h"
#include "SerialPort.h"
#include "MeasureDlg.h"
#include "CalibratorDlg.h"
#include "SensorDataDlg.h"
#include "CalFileDlg.h"
#include "RegulatorDlg.h"
#include "TrayInputDlg.h"
#include "ManualControlView.h"
#include "ConnectionView.h"
#include "OperationView.h"
#include "SettingCheckDlg.h"
#include "VersionCheckDlg.h"
#include "ErcdWriteDlg.h"

#include "ExtSplitter.h"
#include "tabwnd.h"

#include "TestDlg.h"
#include "ShowEmgDlg.h"
#include "ContactCheckResultDlg.h"
#include "SystemsettingDlg.h"
#include "PrecisionDlg.h"
#include "SbcLogChkDlg.h"
#include "InformationDlg.h"
#include "Mmsystem.h"
#include "ClampCountChkDlg.h"
#include "FormulaDlg.h"

#define MAX_ONLINE_STEPDATASIZE		10		// ONLINE 모드 작동시 스텝의 수는 10개를 넘어가지 않는다.

class CMainFrame : public CFrameWnd
{
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();
	
protected: // create from serialization only
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// Attributes
public:
	BOOL m_bSensorData;		// SensorDlg 창 실행 여부
	CPtrArray m_apMeasureDlg;
	CPtrArray m_apCaliDlg;
	CPtrArray m_apEmgDlg;
	CPtrArray m_apInformationDlg;
	CPtrArray m_apContactCheckResultDlg;
	
	CPrecisionDlg *m_pPrecisionDlg;
	CSbcLogChkDlg *m_pSbcLogChkDlg;
	CClampCountChkDlg *m_pClampCountChkDlg;
	CSystemsettingDlg *m_pSystemSettingdlg;
	CFormulaDlg	*m_pFormulaDlg;
	
	BOOL m_bAlarmPlaying;
	int nModuleReadyToStart[EP_MAX_MODULE_NUM];	// 1:작업 시작

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	//}}AFX_VIRTUAL

// Implementation
public:
	BOOL			PasswordCheck();
	void ClearFrmDlg();
	VOID SendDataToODBCForPinLog( int nModuleID, int nChIndex, BYTE channelCode );		// ODBC 에 Pin Error 정보를 저장.
	BOOL Fun_FindJigID(CString strJig,int &nModuleID, int &nJigID);
	void BCRScaned(CString strCode);
	BOOL UserInputTrayNo(int nModuleID, int nJigNo = 0, CString strTrayNo = "");
	int m_nTimerArguModuleID;
	BOOL m_bSplitterCreated;
	CTabbedWnd* m_pTab;
	CExtSplitter m_wndSplitter;
	int	m_uSelect;		// SplitterWnd select num;

	BOOL CompareCurrentSelectTab(int nTabID);
	void OnTimerFuncTryToConnectToDBServer();

	BOOL WriteSerialSetting();
	BOOL LoadSerialSetting();
	BOOL m_bSerialConnected;
	BOOL CloseSerialPort();
	BOOL InitSerialPort();
	VOID SetAlarmOff();
	VOID SetAlarmOn();
	VOID ShowInformation(int nUnitNum, CString strMsg, int nMsgType = INFO_TYPE_NORMAL );
	CString MacroDateToCString(const char *MacroDate);	//20201030ksj
//	BOOL m_bWorking;
//	void SaveChData(int bSave, UINT nInterval);

	CAllSystemView		*m_pAllSystemView;
	CTopView			*m_pTopView;			// 전체 Module & Channel Monitoring
	CManualControlView	*m_pManualControlView;
	CConnectionView		*m_pConnectionView;	
	COperationView		*m_pOperationView;
	CSettingCheckDlg	*m_pSettingCheckDlg;	//Dialog
	CVersionCheckDlg	*m_pVersionCheckDlg;	//Dialog
	CErcdWriteDlg		*m_pErcdWriteDlg;		//Dialog
	
//	CResultView			*m_pResultView;
//	CProcesureFormView	*m_pConditionView;

// -----------------------------------------------------------------------
// For TabWnd on MainFrame
// -----------------------------------------------------------------------
// 	UINT	m_nCurTabIndex;
// 	UINT	m_nTabIndex_AllSystemView;
// 	UINT	m_nTabIndex_TopView;
// 	UINT	m_nTabIndex_ConditionView;
// 	UINT	m_nTabIndex_DetailView;
// 	UINT	m_nTabIndex_ResultView;
// 	CFont	m_ActiveFont;
// 	CFont	m_InactiveFont;
// 	SEC3DTabWnd m_tabWnd;

	BOOL	m_bDBSvrConnect;

	enum {
		TIMER_SAVE_CH_DATA = 501,
		TIMER_TRY_TO_CONNECT_TO_DBSERVER,
		TIMER_CHECK_RESULT,
		TIMER_1SEC_DEFAULT,
		TIMER_FMS_NET_ON,
		TIMER_500MSEC_DEFAULT
	};

	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
//	CCalFileDlg *m_pCalDlg;
	BOOL MakeFileStructure(int nModuleID, int nTrayIndex, int nStepIndex, LPVOID lpData, LPSTR_SAVE_CH_DATA lpSaveChData);
	BOOL MakeCellCheckFileStructure(int nModuleID, int nTrayIndex, LPVOID lpData, LPSTR_SAVE_CH_DATA lpSaveChData);
	CSensorDataDlg	*m_pSensorDlg;
	CMeasureDlg		*m_pAccurayDlg;
	
#ifdef USE_STATUS_BAR
	CStatusBar  m_wndStatusBar;
#endif
	CToolBar    m_wndToolBar;
//	CReBar      m_wndReBar;
//	CAnimateCtrl m_wndAnimate;

	//for serial comm.
	CSerialPort	m_SerialPort;
	SERIAL_CONFIG	m_SerialConfig;
	CString		m_strReceiveBuff;

	// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnConeditorRun();
	afx_msg void OnLogdlgCfg();
	afx_msg void OnBoardGroupSet();
	afx_msg void OnCfgTopConfig();
	afx_msg void OnChangeUser();
	afx_msg void OnUserSetting();
	afx_msg void OnAdministration();
	afx_msg void OnModuleSetting();
	afx_msg void OnClose();
	afx_msg void OnUpdateModuleSetting(CCmdUI* pCmdUI);
	afx_msg void OnUpdateAdministration(CCmdUI* pCmdUI);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnConnectDbsvr();
	afx_msg void OnDisconnectDbsvr();
	afx_msg void OnUpdateDisconnectDbsvr(CCmdUI* pCmdUI);
	afx_msg void OnUpdateConnectDbsvr(CCmdUI* pCmdUI);
	afx_msg void OnUpdateTrayReg(CCmdUI* pCmdUI);
	afx_msg void OnSensorDataView();
	afx_msg void OnSerialConfig();
	afx_msg void OnUpdateSerialConfig(CCmdUI* pCmdUI);
	afx_msg void OnIoTest();
	afx_msg void OnAccuracyTest();
	afx_msg void OnCalibrationSet();
	afx_msg void OnRefAdData();
	afx_msg void OnUpdateRefAdData(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCalibrationSet(CCmdUI* pCmdUI);
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnUpdateTraySerialInit(CCmdUI* pCmdUI);
	afx_msg void OnUpdateIoTest(CCmdUI* pCmdUI);
	afx_msg void OnJigAdd();
	afx_msg void OnJigDelete();
	afx_msg void OnUpdateUserSetting(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSensorDataView(CCmdUI* pCmdUI);
	afx_msg void OnViewEmgLog();
	afx_msg void OnUnitLog();
	afx_msg void OnTelnetCom();
	afx_msg void OnNetCheck();
	afx_msg void OnModuleBackup();
	afx_msg void OnBfcali();
	afx_msg void OnUpdateBfcali(CCmdUI* pCmdUI);
	afx_msg void OnUpdateAccuracyTest(CCmdUI* pCmdUI);
	
	afx_msg LRESULT OnModuleConnected(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnModuleDisConnected(WPARAM wParam, LPARAM lParam);

	afx_msg LRESULT OnModuleInfoChange(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnModuleStateChange(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSaveDataReceive(WPARAM wParam, LPARAM lParam);

	afx_msg LRESULT OnDBServerConnected(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnDBServerDisConnected(WPARAM wParam, LPARAM lParam);

	afx_msg LRESULT OnCheckEnded(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnTraySerialReceive(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnBarCodeReceive(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnModuleEmg(WPARAM wParam, LPARAM lParam);

	afx_msg LONG OnCommunication(WPARAM wParam, LPARAM lParam);
//	afx_msg void OnTabSelected(WPARAM wParam, LPARAM lParam);

	afx_msg LRESULT OnUserCmdReceive(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRealTimeDataReceive(WPARAM wParam, LPARAM lParam);

	afx_msg LRESULT OnOnlineStepDataReceive(WPARAM wParam, LPARAM lParam);
	
	afx_msg LRESULT OnCalResultReceive(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCalEndReceive(WPARAM wParam, LPARAM lParam);

	afx_msg LRESULT OnRealMeasEndReceive(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRealMeasWorkEndReceive(WPARAM wParam, LPARAM lParam);
	
	afx_msg LRESULT OnCMDResponse(WPARAM wParam, LPARAM lParam);
	
	afx_msg LRESULT OnFmsConnected(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnFmsClosed(WPARAM wParam, LPARAM lParam);
	
	afx_msg LRESULT OnSensorSettingDataRecive(WPARAM wParam, LPARAM lParam);	//20200821 ksj
	afx_msg LRESULT OnReciveSbcParam(WPARAM wParam, LPARAM lParam);				//20201030 ksj
	afx_msg LRESULT OnReciveFwVersion(WPARAM wParam, LPARAM lParam);			//20201224 ksj


	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnSystemSetting();
	afx_msg void OnPrecision();
	
	HANDLE mSBCPROCThreadHandle;
	HANDLE mSBCPROCThreadDestroyEvent;
	VOID SBCPROCThreadCallback(VOID);
	afx_msg void OnTestDlg();

	//////////////////////////////////////////////////////////////////////////
	CTestDlg* m_pTestDlg;
	afx_msg LRESULT OnStateChange(WPARAM, LPARAM);
	afx_msg void OnSbcLogchk();
	afx_msg void OnMisdataProcess();
	afx_msg void OnClampCntchk();
	afx_msg void OnViewSet();
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct);
	afx_msg void OnFormulaSetting();
	afx_msg void OnSbcSettingCheck();
	afx_msg void OnWriteErcdCode();
	afx_msg void OnVersionCheck();
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__F4FB9D04_24EC_481C_A65F_6AB4A26C80FF__INCLUDED_)
