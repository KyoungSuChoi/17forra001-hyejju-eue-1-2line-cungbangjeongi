// ConditionFile.cpp: implementation of the CConditionFile class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CTSEditor.h"
#include "ConditionFile.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CConditionFile::CConditionFile()
{

}

CConditionFile::~CConditionFile()
{

}

BOOL CConditionFile::WriteFile()
{
	//파일명 검사
	if(m_strFileName.IsEmpty())		return FALSE;
	
	//파일 생성
	FILE *fp = fopen(m_strFileName, "wt");
	
	//파일 생성 실패
	if(fp == NULL)
		return FALSE;

	//파일 Header 기록 
	LPPS_FILE_ID_HEADER	lpEpFileHeader;
	lpEpFileHeader = new PS_FILE_ID_HEADER;
	ASSERT(lpEpFileHeader);
	ZeroMemory(lpEpFileHeader, sizeof(PS_FILE_ID_HEADER));

	COleDateTime dateTime = COleDateTime::GetCurrentTime();
	sprintf(lpEpFileHeader->szCreateDateTime, "%s", dateTime.Format());	//기록 시간
	sprintf(lpEpFileHeader->szFileID, CONDITION_FILE_ID);				//파일 ID
	sprintf(lpEpFileHeader->szFileVersion, CONDITION_FILE_VER);			//파일 Version
	sprintf(lpEpFileHeader->szDescrition, "SFT cell test System");	//파일 서명 

	fwrite(lpEpFileHeader, sizeof(PS_FILE_ID_HEADER), 1, fp);				//기록 실시 
	
	delete lpEpFileHeader;		//Memory 해제 
	lpEpFileHeader = NULL;

	//모델 정보 기록
	fwrite(&m_modelData, sizeof(STR_CONDITION_HEADER), 1, fp);				//기록 실시 
	//공정 정보 기록 
	fwrite(&m_testData, sizeof(STR_CONDITION_HEADER), 1, fp);				//기록 실시 
	//Cell Check 조건 기록 
	fwrite(&m_cellCheckParam, sizeof(EP_PRETEST_PARAM), 1, fp);				//기록 실시 

	//스텝 정보 기록 

	while()
	{
		fwrite(&m_cellCheckParam, sizeof(EP_PRETEST_PARAM), 1, fp);				//기록 실시 
	}

	fclose(fp);
	return TRUE;
}
