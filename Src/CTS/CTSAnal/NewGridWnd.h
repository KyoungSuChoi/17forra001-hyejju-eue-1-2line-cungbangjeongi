// MyNewGridWnd.h: interface for the CMyGridWnd class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MYNEWGRIDWND_H__9855466B_D960_4A57_86EE_AA1873FB94EE__INCLUDED_)
#define AFX_MYNEWGRIDWND_H__9855466B_D960_4A57_86EE_AA1873FB94EE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include "..\\PowerFormation\\Msgdefine.h"
#include "StdAfx.h"
//Grid User Message define
#define WM_GRID_DOUBLECLICK		WM_USER+101
#define WM_TOOLBAR_COMBO1		WM_USER+102
#define WM_TOOLBAR_COMBO2		WM_USER+103
#define WM_COMBO_SELECT			WM_USER+104
#define WM_TOP_CHANNEL_REFLASH	WM_USER+105
#define WM_GRID_KILLFOCUS		WM_USER+106
#define WM_GRID_SETFOCUS		WM_USER+107
#define WM_CALLBACK_ERROR		WM_USER+108
#define WM_SEL_CHANGED			WM_USER+109
#define WM_GRID_MOVECELL		WM_USER+110
#define WM_GRID_BEGINEDIT		WM_USER+111
#define WM_GRID_ENDEDIT			WM_USER+112
#define WM_GRID_CANCELEDIT		WM_USER+113
#define WM_GRID_MOVEROW			WM_USER+114
#define WM_DATETIME_CHANGED		WM_USER+115
#define	WM_GRID_BTNCLICK		WM_USER+116
#define WM_GRID_CLICK			WM_USER+117
#define WM_GRID_RIGHT_CLICK		WM_USER+118
#define WM_GRID_ROW_DRAG_DROP	WM_USER+119
#define WM_GRID_MOUSE_MOVEOVER	WM_USER+122
#define WM_CHECKBOX_CLICKED		WM_USER+124


class CMyNewGridWnd : public CGXGridWnd  
{
public:
	CMyNewGridWnd();
	virtual ~CMyNewGridWnd();

	virtual	void OnInitialUpdate();

// Implementation
public:
	BOOL OnLButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	BOOL OnRButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	void OnMovedCurrentCell(ROWCOL nRow, ROWCOL nCol);
	BOOL OnSelDragRowsMove(ROWCOL nFirstRow, ROWCOL nLastRow, ROWCOL nDestRow);
	BOOL OnSelDragRowsDrop(ROWCOL nFirstRow, ROWCOL nLastRow, ROWCOL nDestRow);
	BOOL OnLeftCell(ROWCOL nRow, ROWCOL nCol, ROWCOL nNewRow, ROWCOL nNewCol);
	BOOL GetStyleRowCol(ROWCOL nRow, ROWCOL nCol, CGXStyle& style, GXModifyType mt, int nType);
	BOOL OnStartEditing(ROWCOL nRow, ROWCOL nCol);
	BOOL OnEndEditing(ROWCOL nRow, ROWCOL nCol);

protected:
	//{{AFX_MSG(CMyNewGridWnd)
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif // !defined(AFX_MYNEWGRIDWND_H__9855466B_D960_4A57_86EE_AA1873FB94EE__INCLUDED_)
