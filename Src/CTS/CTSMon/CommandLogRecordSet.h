#if !defined(AFX_COMMANDLOGRECORDSET_H__11BFEC9A_6DBD_41D6_8FE8_3A9DE821EDBF__INCLUDED_)
#define AFX_COMMANDLOGRECORDSET_H__11BFEC9A_6DBD_41D6_8FE8_3A9DE821EDBF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CommandLogRecordSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCommandLogRecordSet DAO recordset

class CCommandLogRecordSet : public CDaoRecordset
{
public:
	CCommandLogRecordSet(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CCommandLogRecordSet)

// Field/Param Data
	//{{AFX_FIELD(CCommandLogRecordSet, CDaoRecordset)
	long	m_Index;
	long	m_ProcedureID;
	CString	m_Action;
	COleDateTime	m_DateTime;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCommandLogRecordSet)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COMMANDLOGRECORDSET_H__11BFEC9A_6DBD_41D6_8FE8_3A9DE821EDBF__INCLUDED_)
