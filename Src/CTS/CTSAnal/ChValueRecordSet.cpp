// ChValueRecordSet.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "ChValueRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChValueRecordSet

IMPLEMENT_DYNAMIC(CChValueRecordSet, CRecordset)

CChValueRecordSet::CChValueRecordSet(CDatabase* pdb)
	: CRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CChValueRecordSet)
	m_Index = 0;
	m_TestIndex = 0;
//	m_dataType = 0;
	memset(m_ch, 0.0f,sizeof(m_ch));
	m_nFields = 130;
	//}}AFX_FIELD_INIT
	m_nDefaultType = snapshot;
}


CString CChValueRecordSet::GetDefaultConnect()
{
	return _T("ODBC;DSN=CTSProduct");
}

CString CChValueRecordSet::GetDefaultSQL()
{
	return _T("[ChValue]");
}

void CChValueRecordSet::DoFieldExchange(CFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CChValueRecordSet)
	CString   strTemp;
	pFX->SetFieldType(CFieldExchange::outputColumn);
	RFX_Long(pFX, _T("[Index]"), m_Index);
	RFX_Long(pFX, _T("[TestIndex]"), m_TestIndex);
//	RFX_Long(pFX, _T("[dataType]"), m_dataType);
	for(int i = 0; i < 128; i++)
	{
		strTemp.Format("[ch%d]", i+1);
    	RFX_Single(pFX, _T(strTemp),m_ch[i]);
	}
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CChValueRecordSet diagnostics

#ifdef _DEBUG
void CChValueRecordSet::AssertValid() const
{
	CRecordset::AssertValid();
}

void CChValueRecordSet::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG
