#if !defined(AFX_LOGVIEWDLG_H__0508AD9D_CC8A_4CB6_AA95_1F83DB4866BA__INCLUDED_)
#define AFX_LOGVIEWDLG_H__0508AD9D_CC8A_4CB6_AA95_1F83DB4866BA__INCLUDED_

//#include "DataForm.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LogViewDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLogViewDlg dialog

class CLogViewDlg : public CDialog
{
// Construction
public:
	CLogViewDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CLogViewDlg)
	enum { IDD = IDD_LOGVIEW_DLG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLogViewDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CLogViewDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LOGVIEWDLG_H__0508AD9D_CC8A_4CB6_AA95_1F83DB4866BA__INCLUDED_)
