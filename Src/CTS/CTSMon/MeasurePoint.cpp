#include "stdafx.h"
#include "MeasurePoint.h"

CMeasurePoint::CMeasurePoint(void)
{
	
}

CMeasurePoint::~CMeasurePoint(void)
{
	
}


void CMeasurePoint::SetVData( WORD wPoint, double dValue, RangeType nType )
{
	if( wPoint < MEAS_MAX_VOLTAGE_CNT )
	{
		m_VMeasureData[nType][wPoint] = dValue;
	}
}

void CMeasurePoint::SetIData( WORD wPoint, double dValue, RangeType nType )
{
	if( wPoint < MEAS_MAX_VOLTAGE_CNT )
	{
		m_IMeasureData[nType][wPoint] = dValue;
	}
}

double CMeasurePoint::GetShuntT( int nBoardIndexNum )
{
	if( nBoardIndexNum >= CAL_MAX_BOARD_NUM || nBoardIndexNum < 0 )
		return 0.0;

	return	m_ShuntT[nBoardIndexNum];
}

double CMeasurePoint::GetShuntR( int nBoardIndexNum )
{
	if( nBoardIndexNum >= CAL_MAX_BOARD_NUM || nBoardIndexNum < 0 )
		return 0.0;

	return	m_ShuntR[nBoardIndexNum];
}

CString CMeasurePoint::GetShuntSerial( int nBoardIndexNum )
{
	CString strSerial;
	strSerial.Empty();

	if( nBoardIndexNum >= CAL_MAX_BOARD_NUM || nBoardIndexNum < 0 )
		return strSerial;

	strSerial.Format("%s", m_ShuntSerial[nBoardIndexNum] );
	return strSerial;
}

BOOL CMeasurePoint::SetShuntT( double *pdData )
{
	ASSERT(pdData);

	memcpy( m_ShuntT, pdData, sizeof(double) * CAL_MAX_BOARD_NUM );

	return TRUE;
}

BOOL CMeasurePoint::SetShuntR( double *pdData )
{
	ASSERT(pdData);

	memcpy( m_ShuntR, pdData, sizeof(double) * CAL_MAX_BOARD_NUM );

	return TRUE;
}

BOOL CMeasurePoint::SetShuntSerial( int nBoardIndexNum, CHAR *pdData )
{
	ASSERT(pdData);	

	memcpy( &m_ShuntSerial[nBoardIndexNum][0], pdData, sizeof(CHAR) * CAL_MAX_SHUNT_SERIAL_SIZE);

	return TRUE;
}