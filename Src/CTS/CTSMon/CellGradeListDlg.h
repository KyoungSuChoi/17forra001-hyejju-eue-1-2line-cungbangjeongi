#if !defined(AFX_CELLGRADELISTDLG_H__E7941B2E_4E44_4308_B9FD_78BE0D0464F5__INCLUDED_)
#define AFX_CELLGRADELISTDLG_H__E7941B2E_4E44_4308_B9FD_78BE0D0464F5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CellGradeListDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCellGradeListDlg dialog
#include "MyGridWnd.h"

class CCellGradeListDlg : public CDialog
{
// Construction
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();
	virtual ~CCellGradeListDlg();

	int m_nChannelSize;
	UINT m_nTrayColSize;
	WORD m_cellStateCount[EP_MAX_CH_PER_MD];
	int m_nNormalCount;
	int m_nFailCount;
	int m_nNonCellCount;
	void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	BOOL DisplayData();
	BYTE cellState[EP_MAX_CH_PER_MD];
	void InitChannelGrid();
	CMyGridWnd m_ChannelGrid;
	int m_nTrayType;
	CCellGradeListDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCellGradeListDlg)
	enum { IDD = IDD_CELL_GRADE_LIST_DLG };
	CLabel	m_ctrlDeltaOCV2;
	CLabel	m_ctrlOCV4Fail;
	CLabel	m_ctrlImpFail;
	CLabel	m_ctrlDisCapFail;
	CLabel	m_ctrlDeltaOCV1Fail;
	CLabel	m_ctrlOCV1Fail;
	CLabel	m_ctrlInputCell;
	CLabel	m_faultCount;
	CLabel	m_normalCout;

	CString	m_strTrayNo;
	CString	m_strLotNo;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCellGradeListDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCellGradeListDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPrintResult();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CELLGRADELISTDLG_H__E7941B2E_4E44_4308_B9FD_78BE0D0464F5__INCLUDED_)
