
//GWin::win_SendCopyData("Debug Trace",0,sLog);

class GLog
{
public:   
	GLog();
    ~GLog();
    	   
	static CString log_GetFileName(CString stype)
	{		
		CString AppPath=GFile::file_GetAppPath();
		CString sFileName=GWin::win_CurrentTime(_T("filelog"));

//			2014-02-10			bung			:			MES 관련 log 수정.
		CString dbFolderName;
		dbFolderName.Format(_T("%s"),sFileName.Left(7));
		
		GFile::file_ForceDirectory(AppPath+_T("\\log\\")+ dbFolderName);//create log folder

		CString sext=_T("txt");
		CString spath=_T("");
		
		spath.Format(_T("%s\\log\\%s\\%s_%s.%s"),AppPath,sFileName.Left(7),stype,sFileName,sext);		
		return spath;		
	}

	static void log_TempSave(CString sLog, CString strPath,
		CWnd *pWnd=NULL, BOOL bflag=FALSE,CString sLogTime=_T(""), CString sTitleName=_T("") )
	{						
		if( sLog.IsEmpty() || sLog==_T("") || strPath==_T("") )   return;	
		if( sLogTime.IsEmpty() || sLogTime==_T("") )
		{
			sLogTime=GWin::win_CurrentTime(_T("fulllog"));
		}

		CString sData=_T("");

		strPath += "_Temperature.csv";

		if( GFile::file_Finder( strPath ) == FALSE )
		{
			sData.Format(_T("%s,%s\r"), sLogTime, sTitleName);
			GFile::file_AddWrite(strPath, sData);
		}

		sData.Format(_T("%s,%s\r"), sLogTime, sLog);
		if(bflag==TRUE) GFile::file_AddWrite(strPath,sData);

		if(!pWnd) return;
		if(!::IsWindow(pWnd->m_hWnd)) return;

		int nLength=sData.GetLength();

#ifdef _UNICODE			   									     
		wchar_t *lpszData=new wchar_t[nLength+1];						 
#else			
		char* lpszData = new char[nLength+1];
#endif	

		lstrcpy(lpszData, sData);
		::PostMessage(pWnd->m_hWnd,UM_LOGMESSAGE,0,(LPARAM)lpszData);
	}

	static void log_Save(CString sType,CString sTitle,CString sLog,
		                 CWnd *pWnd=NULL,BOOL bflag=FALSE,CString sLogTime=_T(""))
	{						
		if(sType.IsEmpty() || sType==_T("")) return;
		if(sLog.IsEmpty() || sLog==_T(""))   return;	
		if(sLogTime.IsEmpty() || sLogTime==_T(""))
		{
			sLogTime=GWin::win_CurrentTime(_T("fulllog"));
		}		
		CString strPath=log_GetFileName(sType);
		
		CString sData=_T("");
		sData.Format(_T("%s    %s %s\r\n"),sLogTime,GStr::str_Blank(sTitle,10,_T(" ")),sLog);
					
		if(bflag==TRUE) GFile::file_AddWrite(strPath,sData);

		if(!pWnd) return;
		if(!::IsWindow(pWnd->m_hWnd)) return;
		
		int nLength=sData.GetLength();

		#ifdef _UNICODE			   									     
		     wchar_t *lpszData=new wchar_t[nLength+1];						 
		#else			
		    char* lpszData = new char[nLength+1];
		#endif	
					
		lstrcpy(lpszData, sData);
	    ::PostMessage(pWnd->m_hWnd,UM_LOGMESSAGE,0,(LPARAM)lpszData);
	}
	static void log_TempFileSave(CString sLog, CString strPath,
		CWnd *pWnd=NULL, BOOL bflag=FALSE,CString sLogTime=_T(""), CString sTitleName=_T("") )
	{						
		if( sLog.IsEmpty() || sLog==_T("") || strPath==_T("") )   return;	
		if( sLogTime.IsEmpty() || sLogTime==_T("") )
		{
			sLogTime=GWin::win_CurrentTime(_T("fulllog"));
		}

		CString AppPath=GFile::file_GetAppPath();
		CString sFileName=GWin::win_CurrentTime(_T("filelog"));

		//			2014-02-10			bung			:			MES 관련 log 수정.
		CString dbFolderName;
		dbFolderName.Format(_T("%s"),sFileName.Left(10));

		GFile::file_ForceDirectory(strPath);//create log folder

		CString sData=_T("");

// 		strPath += "_Temperature.csv";
// 		strPath += "_Temperature.csv";
		CString strPath1 = _T("");

		strPath1 += strPath + "\\" + dbFolderName + ".csv";

		if( GFile::file_Finder( strPath1 ) == FALSE )
		{
			sData.Format(_T("%s,%s\r"), sLogTime, sTitleName);
			GFile::file_AddWrite(strPath1, sData);
		}

		sData.Format(_T("%s,%s\r"), sLogTime, sLog);
		if(bflag==TRUE) GFile::file_AddWrite(strPath1,sData);

		if(!pWnd) return;
		if(!::IsWindow(pWnd->m_hWnd)) return;

		int nLength=sData.GetLength();

#ifdef _UNICODE			   									     
		wchar_t *lpszData=new wchar_t[nLength+1];						 
#else			
		char* lpszData = new char[nLength+1];
#endif	

		lstrcpy(lpszData, sData);
		::PostMessage(pWnd->m_hWnd,UM_LOGMESSAGE,0,(LPARAM)lpszData);
	}
	static void log_Mesg(int nItem)
	{
		AfxMessageBox(GStr::str_IntToStr(nItem));
	}

	static void log_Mesg(CRect rect)
	{
		CString str=_T("");
		str.Format(_T("%d,%d,%d,%d"),rect.left,rect.top,rect.Width(),rect.Height());
		AfxMessageBox(str);
	}

	static void log_Mesg(CString title)
	{
		AfxMessageBox(title);
	}
} ;