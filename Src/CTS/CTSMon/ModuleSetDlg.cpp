// ModuleSetDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSMon.h"
#include "ModuleSetDlg.h"
#include "ModuleModifyDlg.h"

#include "ModuleAddDlg.h"
#include "CalFileDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CModuleSetDlg dialog

CModuleSetDlg::CModuleSetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CModuleSetDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CModuleSetDlg)
//	m_fVSpec = _T("");
	//}}AFX_DATA_INIT
	m_pDoc = NULL;
	m_nSelModuleID = 0;
	m_bChanged = FALSE;
	m_nNewModuleID = 0;
	m_fVSpec = 0.0f;
	m_fISpec = 0.0f;
	LanguageinitMonConfig();
}

CModuleSetDlg::~CModuleSetDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CModuleSetDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CModuleSetDlg"), _T("TEXT_CModuleSetDlg_CNT"), _T("TEXT_CModuleSetDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CModuleSetDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CModuleSetDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

void CModuleSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CModuleSetDlg)
	DDX_Control(pDX, IDC_MODULE_IP_LABEL, m_ctrlModuleIPLabel);
	DDX_Control(pDX, IDC_MODULE_ID_LABEL, m_ctrlModuleLabel);
	DDX_Control(pDX, IDC_TOTAL_CH_COUNT, m_ctrlTotalCh);
	DDX_Control(pDX, IDC_GROUP_LIST_COMBO, m_ctrlGroupList);
	DDX_Control(pDX, IDC_CH_IN_GROUP, m_ctrlChInGroup);
	DDX_Control(pDX, IDC_MODULE_IP, m_ctrlModuleIP);
	DDX_Control(pDX, IDC_MODULE_ID, m_ctrlModuleID);
//	DDX_Text(pDX, IDC_V_SPEC_EDIT, m_fVSpec);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CModuleSetDlg, CDialog)
	//{{AFX_MSG_MAP(CModuleSetDlg)
	ON_NOTIFY(TVN_GETDISPINFO, IDC_MODULE_TREE, OnGetdispinfoModuleTree)
	ON_NOTIFY(TVN_SELCHANGED, IDC_MODULE_TREE, OnSelchangedModuleTree)
	ON_BN_CLICKED(IDC_ADD_MODULE, OnAddModule)
	ON_BN_CLICKED(IDC_DELETE_MODULE, OnDeleteModule)
	ON_BN_CLICKED(IDC_CAL_FILE_UPDATE, OnCalFileUpdate)
	ON_BN_CLICKED(IDC_SHUT_DOWN, OnShutDown)
	ON_CBN_SELCHANGE(IDC_GROUP_LIST_COMBO, OnSelchangeGroupListCombo)
	ON_BN_CLICKED(IDOK, OnOk)
	ON_NOTIFY(NM_CLICK, IDC_MODULE_TREE, OnClickModuleTree)
	ON_BN_CLICKED(IDC_BTN_SAVE, OnBtnSave)
	ON_BN_CLICKED(IDC_BTN_EDIT, OnBtnEdit)
	ON_BN_CLICKED(IDC_MODIFY_MODULE, OnModifyModule)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CModuleSetDlg message handlers

BOOL CModuleSetDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	if(m_pDoc != NULL)
	{
		m_pDoc->InitTree(&m_TreeX, IDC_MODULE_TREE, this);
		m_nSelModuleID = EPGetModuleID(0);
	}

	CString strTemp;
//	strTemp.Format("%s를 추가 하거나 삭제 합니다. %s ID는 중복될수 없으며 추가 및 삭제 후에는 프로그램을 종료후 다시시작하여야 적용 됩니다."
//		,m_pDoc->m_strModuleName, m_pDoc->m_strModuleName);  ;
	GetDlgItem(IDC_MODULE_SET_LABEL)->SetWindowText(GetStringTable(IDS_LANG_TEXT_MODULE_ADD_WARN));
	strTemp.Format("%s IP", m_pDoc->m_strModuleName);
	m_ctrlModuleIPLabel.SetText(strTemp);
	strTemp.Format("%s ID", m_pDoc->m_strModuleName);		// 20090630 kky
	strTemp.Format("%s Name", m_pDoc->m_strModuleName);
	m_ctrlModuleLabel.SetText(strTemp);

	SetCurrentItem(m_TreeX.GetFirstVisibleItem());
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CModuleSetDlg::OnGetdispinfoModuleTree(NMHDR* pNMHDR, LRESULT* pResult) 
{
	TV_DISPINFO* pTVDispInfo = (TV_DISPINFO*)pNMHDR;
	// TODO: Add your control notification handler code here
	m_pDoc->UpdateGetdispinfoTree(pTVDispInfo, &m_TreeX);
	*pResult = 0;
}


void CModuleSetDlg::OnSelchangedModuleTree(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	ASSERT(pNMTreeView);
	SetCurrentItem(pNMTreeView->itemNew.hItem);
	*pResult = 0;
}

void CModuleSetDlg::OnAddModule() 
{
	// TODO: Add your control notification handler code here

	CString strTemp;
	//최대 설치 수 확인 
	int rsCount = m_TreeX.GetCount();
	if(rsCount > EP_MAX_MODULE_NUM)
	{
		strTemp.Format("%s [Max Count %d]", ::GetStringTable(IDS_TEXT_MAX_MD_ERROR), EP_MAX_MODULE_NUM);
		MessageBox(strTemp, "Error", MB_ICONSTOP|MB_OK);
		return;
	}
	
	//새로운 모듈 번호 입력 받음 
	CModuleAddDlg *pDlg;
	pDlg = new CModuleAddDlg;
	if(pDlg == NULL)	return;

	pDlg->m_fVSpec = m_fVSpec;
	pDlg->m_fISpec = m_fISpec;
	pDlg->m_nTrayCol = EP_DEFAULT_TRAY_COL_COUNT;

	if(m_nNewModuleID == 0)
	{
		pDlg->m_nModuleID = EPGetModuleID(m_pDoc->GetInstalledModuleNum()-1)+1;
		EP_SYSTEM_PARAM *pParam = EPGetSysParam(pDlg->m_nModuleID-1);
		if(pParam)
		{
			pDlg->m_fVSpec =  VTG_PRECISION(pParam->lMaxVoltage);
			pDlg->m_fISpec =  CRT_PRECISION(pParam->lMaxCurrent);
		}
	}
	else
	{
		pDlg->m_nModuleID = m_nNewModuleID+1;
	}

	if(IDCANCEL == pDlg->DoModal())	{
		delete pDlg;
		pDlg = NULL;
		return;
	}

	m_nNewModuleID = pDlg->m_nModuleID;
	m_nTrayType = pDlg->m_nModuleTrayType;	//Tray style 0,1:알파벳 타이틀,2,숫자 타이틀,3:폴리머형 알파벳 타이틀,4:폴리머형 숫자 타이클
	m_nTrayColCnt = pDlg->m_nTrayCol;
	if (pDlg->m_bUseModeChange)
		m_bTrayChange = TRUE;
	else
		m_bTrayChange = FALSE;
	m_fVSpec = pDlg->m_fVSpec;
	m_fISpec = pDlg->m_fISpec;
	int sysType = pDlg->m_nType;
	CString strMDName = pDlg->m_strName;
	delete pDlg;
	pDlg = NULL;
	
	CDaoDatabase  db;
	try
	{
 		db.Open(GetDataBaseName());
		CString strSQL,strAddMode;
		
// 		strSQL.Format("INSERT INTO SystemConfig(ModuleID, ModuleType, MaxVoltage, MinVoltage, MaxCurrent, MinCurrent, Data3) VALUES (%d, %d, %f, %f, %f, %f, '%s')", 
// 						m_nNewModuleID, sysType, m_fVSpec, -m_fVSpec, m_fISpec, -m_fISpec, strMDName);
		if (m_bTrayChange)
			strAddMode.Format("Type%d,%d,%d@Type%d,%d,%d",m_nTrayType,m_nTrayType,m_nTrayColCnt,m_nTrayType,m_nTrayType,m_nTrayColCnt);
		else
			strAddMode.Format("Type%d,%d,%d@",m_nTrayType,m_nTrayType,m_nTrayColCnt);

// 		strSQL.Format("INSERT INTO SystemConfig(ModuleID, ModuleType, MaxVoltage, MinVoltage, MaxCurrent, MinCurrent, Data3) VALUES (%d, %d, %f, %f, %f, %f, '%s')", 
// 			m_nNewModuleID, sysType, m_fVSpec, -m_fVSpec, m_fISpec, -m_fISpec, strMDName);
		strSQL.Format("INSERT INTO SystemConfig(ModuleID, ModuleType, MaxVoltage, MinVoltage, MaxCurrent, MinCurrent, Data3, Data4) VALUES (%d, %d, %f, %f, %f, %f, '%s', '%s')", 
			m_nNewModuleID, sysType, m_fVSpec, -m_fVSpec, m_fISpec, -m_fISpec, strMDName, strAddMode);
			
		db.Execute(strSQL);
		db.Close();
	}
	catch(CDaoException *e)
	{
      // Simply show an error message to the user.
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return;
	}

	//Tree에 생성 
	TV_INSERTSTRUCT		tvstruct;
	HTREEITEM	m_rghItem;
	strTemp.Format("%d", m_nNewModuleID);	
	tvstruct.hParent = NULL;
	tvstruct.hInsertAfter = TVI_LAST;
	tvstruct.item.iImage = I_IMAGECALLBACK;
	tvstruct.item.iSelectedImage = I_IMAGECALLBACK;
	tvstruct.item.pszText = (char *)(LPCTSTR)strTemp;		//Display Module ID String
	tvstruct.item.cchTextMax = 64;
	tvstruct.item.mask = TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_TEXT;
	m_rghItem = m_TreeX.InsertItem(&tvstruct);
	m_TreeX.SetItemData(m_rghItem, (DWORD)m_nNewModuleID);			//Set Tree Data to Module ID
	m_TreeX.Invalidate();

	m_bChanged = TRUE;
	return ;
}

void CModuleSetDlg::OnDeleteModule() 
{
	// TODO: Add your control notification handler code here	
	//  [6/30/2009 kky ]
	// for 기존 모듈을 선택 삭제시 모듈아이디가 중간에 끊기게 되면 프로그램 에러가 발생
	// 1,2,3,4,6,7 <= 이경우 5번 아이디가 없어서 에러.. 마지막 UnitID 정보부터 삭제
	CString strTemp;
	strTemp.Format(TEXT_LANG[0],::GetModuleName(m_TreeX.GetCount()) );	//"Unit 삭제시 가장 마지막 Unit부터 삭제 됩니다! %s를 삭제하시겠습니까?)"
	if(IDNO == MessageBox(strTemp, "Delete", MB_ICONQUESTION|MB_YESNO))
		return;
	
/*	CSystemParamRecordSet	recordSet;
	recordSet.m_strSort.Format("ModuleID");


	if(((CCTSMonApp*)AfxGetApp())->GetSystemType() != 0)
		recordSet.m_strFilter.Format("ModuleType=%d",((CCTSMonApp*)AfxGetApp())->GetSystemType());

	try
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return ;
	}

	if(recordSet.IsBOF())
	{
		strTemp.Format(GetStringTable(IDS_MSG_NOT_FOUND), ::GetModuleName(m_nSelModuleID));
		recordSet.Close();
		AfxMessageBox(strTemp);
		return;
	}
	else
	{
		recordSet.MoveLast();
		if(recordSet.GetRecordCount()<2)
		{
			strTemp.Format(GetStringTable(IDS_MSG_CAN_NOT_DELETE), m_pDoc->m_strModuleName);
			recordSet.Close();
			AfxMessageBox(strTemp);
			return;
		}
	}
	
	recordSet.m_strFilter.Format("ModuleID = %ld", m_nSelModuleID);
	
	if(((CCTSMonApp*)AfxGetApp())->GetSystemType() != 0)
	{
		 strTemp.Format(" AND ModuleType= %d", ((CCTSMonApp*)AfxGetApp())->GetSystemType());
		 recordSet.m_strFilter += strTemp;
	}
	recordSet.Requery();

	if(recordSet.IsBOF())
	{
		strTemp.Format(GetStringTable(IDS_MSG_NOT_FOUND), ::GetModuleName(m_nSelModuleID));
		AfxMessageBox(strTemp);
		recordSet.Close();
		return;
	}

	recordSet.Delete();
	recordSet.Close();
*/

	//최소 1개 이상의 모듈이 설치 되어 있어야 한다. (현재 남은 모듈이 1개 이하면 삭제 하지 못함)
	if(m_TreeX.GetCount() < 2)
	{
		strTemp.Format(GetStringTable(IDS_MSG_CAN_NOT_DELETE), m_pDoc->m_strModuleName);
		AfxMessageBox(strTemp);
		return;
	}

	//DB에서 삭제 
	CDaoDatabase  db;
	int sysType = ((CCTSMonApp*)AfxGetApp())->GetSystemType();

	try
	{
 		db.Open(GetDataBaseName());
		CString strSQL;
		// strSQL.Format("DELETE FROM SystemConfig WHERE ModuleID = %d", m_nSelModuleID);
		strSQL.Format("DELETE FROM SystemConfig WHERE ModuleID = %d", m_TreeX.GetCount());
		if(sysType != 0)
		{
			strTemp.Format(" AND ModuleType = %d", sysType);
			strSQL += strTemp;
		}
			
		db.Execute(strSQL);
		db.Close();
	}
	catch(CDaoException *e)
	{
      // Simply show an error message to the user.
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
	}


	//Tree에서 삭제 
	HTREEITEM	m_rghItem = m_TreeX.GetSelectedItem();
	if(m_nSelModuleID != m_TreeX.GetItemData(m_rghItem))
	{
		AfxMessageBox("ID Error");
	}

	HTREEITEM hItem;
	
	//다음 Module이 없으면 
	if(m_TreeX.GetNextVisibleItem(m_rghItem) == NULL)
	{
		hItem = m_TreeX.GetPrevVisibleItem(m_rghItem);
	}
	else
	{
		hItem = m_TreeX.GetNextVisibleItem(m_rghItem);		
	}

	m_TreeX.DeleteItem(m_rghItem);
	m_TreeX.Invalidate();

	SetCurrentItem(hItem);

//	strTemp.Format("%s 의 삭제 내용을 적용하시려면 프로그램을 종료 후 다시 시작하셔야 합니다.", m_pDoc->ModuleName(m_nSelModuleID));
//	AfxMessageBox(strTemp);
	m_bChanged = TRUE;

	return ;	
}

void CModuleSetDlg::OnCalFileUpdate() 
{
	// TODO: Add your control notification handler code here
	if(!PermissionCheck(PMS_MODULE_CAL_UPDATE))
	{
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_PERMISSION_ERROR)+ " [Calibration File Update]");
		return;
	}
/*	CCalFileDlg *pDlg = new CCalFileDlg(this);

	pDlg->SetModuleNum(m_pDoc->GetInstalledModuleNum());
	pDlg->DoModal();

	delete pDlg;
	pDlg = NULL;
*/
	CString strTemp;
	
	STARTUPINFO	stStartUpInfo;
	PROCESS_INFORMATION	ProcessInfo;
	ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
	ZeroMemory(&ProcessInfo, sizeof(PROCESS_INFORMATION));
	
	stStartUpInfo.cb = sizeof(STARTUPINFO);
	stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
	stStartUpInfo.wShowWindow = SW_NORMAL;

	CString strDirTemp;
	{
		TCHAR szCurDir[MAX_PATH];
		::GetModuleFileName(AfxGetApp()->m_hInstance, szCurDir, MAX_PATH);
		strDirTemp = CString(szCurDir).Mid(0, CString(szCurDir).ReverseFind('\\'));
	}

//	strDirTemp = ((CCTSMonDoc *)GetActiveDocument())->m_strCurFolder;
	strTemp.Format("%s\\CalFile.exe", strDirTemp);	//IP를 인자로 넘김 

	BOOL bFlag = CreateProcess(NULL, (LPSTR)(LPCTSTR)strTemp, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &stStartUpInfo, &ProcessInfo);
	if(bFlag == FALSE)
	{
/*		LPVOID lpMsgBuf;
		FormatMessage( 
			FORMAT_MESSAGE_ALLOCATE_BUFFER | 
			FORMAT_MESSAGE_FROM_SYSTEM | 
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			GetLastError(),
			0, // Default language
			(LPTSTR) &lpMsgBuf,
			0,
			NULL 
		);
		// Process any inserts in lpMsgBuf.
		// ...
		// Display the string.
		MessageBox( (LPCTSTR)lpMsgBuf, "Error", MB_OK | MB_ICONINFORMATION );
		// Free the buffer.
		LocalFree( lpMsgBuf );
*/
		strTemp.Format("%s\\CalFile.exe File not Found.", strDirTemp);	//IP를 인자로 넘김 
		MessageBox(strTemp, "Execute Error", MB_OK|MB_ICONSTOP); 
	}			
}

void CModuleSetDlg::OnShutDown() 
{
	// TODO: Add your control notification handler code here
	if(!PermissionCheck(PMS_MODULE_SHUT_DOWN))
	{
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_PERMISSION_ERROR) + " [System Shutdown]");
		return;
	}
	
	AfxMessageBox("Not support");
	
}

void CModuleSetDlg::OnSelchangeGroupListCombo() 
{
	// TODO: Add your control notification handler code here
	int nCurGroup = m_ctrlGroupList.GetCurSel();
	CString strTemp;
	strTemp.Format("%3d Ch", EPGetChInGroup(m_nSelModuleID, nCurGroup));
	m_ctrlChInGroup.SetText(strTemp);

}

/*
void CModuleSetDlg::OnAutoProcessing() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(EPSetAutoProcess(m_nSelModuleID, m_bAutoProcess) == FALSE)
	{
		CString strTemp;
		strTemp.Format("%s 의 Auto Processing 을 설정 할 수 없습니다.", m_pDoc->ModuleName(m_nSelModuleID));
		AfxMessageBox(strTemp);
		m_bAutoProcess = EPGetAutoProcess(m_nSelModuleID);
		UpdateData(FALSE);
	}
}
*/

void CModuleSetDlg::OnOk() 
{
	// TODO: Add your control notification handler code here
	if(m_bChanged)
	{
		AfxMessageBox(GetStringTable(IDS_TEXT_RESTART_ALERT));
	}
	CDialog::OnOK();
}

void CModuleSetDlg::OnClickModuleTree(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	
/*	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	ASSERT(pNMTreeView);
	HTREEITEM hItem = pNMTreeView->itemNew.hItem;
	ASSERT(hItem);

	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	ASSERT(pNMTreeView);
*/	
//	int nTmpPrevSelectedModuleID = m_nSelModuleID;

/*	HTREEITEM hItem = m_TreeX.GetSelectedItem();
	if(hItem == NULL)		return;
	m_nSelModuleID = m_TreeX.GetItemData(hItem);

	m_ctrlModuleIP.SetText(EPGetModuleIP(m_nSelModuleID));
	CString strTemp;
	strTemp.Format("%s", ::GetModuleName(m_nSelModuleID));
	m_ctrlModuleID.SetText(strTemp);

	m_ctrlGroupList.ResetContent();

	for(int i =0; i<EPGetGroupCount(m_nSelModuleID); i++)
	{
		strTemp.Format("G %d", i+1);
		m_ctrlGroupList.AddString(strTemp);
	}
	m_ctrlGroupList.SetCurSel(0);
	strTemp.Format("%3d Ch", EPGetChInGroup(m_nSelModuleID, 0));
	m_ctrlChInGroup.SetText(strTemp);

//	m_bAutoProcess = EPGetAutoProcess(m_nSelModuleID);
	strTemp = "알수 없음";
	EP_MD_SYSTEM_DATA *pData = EPGetModuleSysData(EPGetModuleIndex(m_nSelModuleID));
	if(pData != NULL)
	{
		if(pData->nControllerType == 0)
		{
			strTemp = "E669";
		}
		else 
		{
			strTemp = "Unknown";
		}
	}
	GetDlgItem(IDC_CPU_TYPE_STATIC)->SetWindowText(strTemp);


	UpdateData(FALSE);
	*pResult = 0;	

	GetDlgItem(IDC_EDIT_SAVE_INTERVAL)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_SAVE)->EnableWindow(FALSE);
*/
}

void CModuleSetDlg::SetCurrentItem(HTREEITEM hItem)
{
	if(hItem == NULL)	return;

	m_TreeX.SetItemState(hItem, TVIS_SELECTED, TVIS_SELECTED);
	m_TreeX.SetFocus();
		
	m_nSelModuleID = m_TreeX.GetItemData(hItem);

	m_ctrlModuleIP.SetText(EPGetModuleIP(m_nSelModuleID));
	CString strTemp;
	strTemp.Format("%s",::GetModuleName(m_nSelModuleID));
	m_ctrlModuleID.SetText(strTemp);

	EP_MD_SYSTEM_DATA *pData = EPGetModuleSysData(EPGetModuleIndex(m_nSelModuleID));
	if(pData != NULL)
	{
		strTemp.Format("BD %d X %d Ch : Tot %dCh", pData->wInstalledBoard, pData->wChannelPerBoard, pData->nTotalChNo);
		m_ctrlTotalCh.SetText(strTemp);

		m_ctrlGroupList.ResetContent();
		for(int i =0; i<EPGetGroupCount(m_nSelModuleID); i++)
		{
			strTemp.Format("G %d", i+1);
			m_ctrlGroupList.AddString(strTemp);
		}
		m_ctrlGroupList.SetCurSel(0);
		strTemp.Format("%3d Ch", EPGetChInGroup(m_nSelModuleID, 0));
		m_ctrlChInGroup.SetText(strTemp);
		
		if(pData->nControllerType == 0)
		{
			strTemp = "E669";
		}
		else 
		{
			strTemp = "Unknown";
		}
		GetDlgItem(IDC_CPU_TYPE_STATIC)->SetWindowText(strTemp);
	}

	strTemp = TEXT_LANG[1];//"알수 없음"
	EP_SYSTEM_PARAM *pParam = EPGetSysParam(m_nSelModuleID);
	if(pParam)
	{
		strTemp.Format("%dV/%dA", int(VTG_PRECISION(pParam->lMaxVoltage)/1000.0f), int(CRT_PRECISION(pParam->lMaxCurrent)/1000.0f));
	}
	GetDlgItem(IDC_VI_SPEC_STATIC)->SetWindowText(strTemp);

	//Tray 정보 표시
	

	CFormModule *pModule =  m_pDoc->GetModuleInfo(m_nSelModuleID);
	if(pModule)
	{
		if(pModule->m_iTrayTypeCount > 1)
		{
			GetDlgItem(IDC_LAB_TRAY_CHANGE)->SetWindowText(TEXT_LANG[2]);//"변경가능"
			strTemp.Format("%d",pModule->m_sTrayTypeData[0].iTrayCol);
			GetDlgItem(IDC_LAB_TRAY_COLUMN)->SetWindowText(strTemp);
			switch (pModule->m_sTrayTypeData[0].iTrayStyle)
			{
			case TRAY_TYPE_CUSTOMER:
			case TRAY_TYPE_LG_ROW_COL:
				strTemp=TEXT_LANG[3];//"일반형 1"
				break;
			case TRAY_TYPE_LG_COL_ROW:
				strTemp=TEXT_LANG[4];//"일반형 2"
				break;
			case TRAY_TYPE_PB5_ROW_COL:
				strTemp=TEXT_LANG[5];//"폴리머형 1"
				break;
			case TRAY_TYPE_PB5_COL_ROW:
				strTemp=TEXT_LANG[6];//"폴리머형 2"
				break;
			default :
				strTemp=TEXT_LANG[7];//"일반형"
				break;
			}
			GetDlgItem(IDC_LAB_TRAY_TYPE)->SetWindowText(strTemp);
		}
		else
		{
			GetDlgItem(IDC_LAB_TRAY_CHANGE)->SetWindowText(TEXT_LANG[8]);//"변경 불가능"
			GetDlgItem(IDC_LAB_TRAY_COLUMN)->SetWindowText(TEXT_LANG[9]);//"기본 설정값"
			GetDlgItem(IDC_LAB_TRAY_TYPE)->SetWindowText(TEXT_LANG[7]);//"일반형"
		}

	}
	UpdateSaveInterval(m_nSelModuleID);
	UpdateData(FALSE);
}

void CModuleSetDlg::OnBtnEdit() 
{
	if(m_nSelModuleID > 0)
	{
		GetDlgItem(IDC_EDIT_SAVE_INTERVAL)->EnableWindow(TRUE);
		GetDlgItem(IDC_BTN_SAVE)->EnableWindow(TRUE);
		GetDlgItem(IDC_BTN_EDIT)->EnableWindow(FALSE);
	}
}

void CModuleSetDlg::OnBtnSave() 
{
	int nSaveInterval = GetDlgItemInt(IDC_EDIT_SAVE_INTERVAL);
	if(nSaveInterval<2||nSaveInterval>3600)
	{
		MessageBox(GetStringTable(IDS_TEXT_VALUE_VALIDE) + " [2sec ~ 3600sec]");
		return;
	}

	CDaoDatabase  db;

	CString strDataBaseName = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION, "database");
	
	strDataBaseName = strDataBaseName+ "\\"+FORM_SET_DATABASE_NAME;
	
	try
	{
		db.Open(strDataBaseName);

		CString strSQL;
		strSQL.Format("UPDATE SystemConfig SET SaveInterval = %d WHERE ModuleID = %d", nSaveInterval, m_nSelModuleID);
		
		if(((CCTSMonApp*)AfxGetApp())->GetSystemType() != 0)
		{
			CString strTemp;
			strTemp.Format(" AND ModuleType = %d", ((CCTSMonApp*)AfxGetApp())->GetSystemType());
			strSQL += strTemp;
		}


		db.Execute(strSQL);
		db.Close();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return;
	}

	EP_SYSTEM_PARAM *pSysParam = EPGetSysParam(m_nSelModuleID);
//	pSysParam->nAutoReportInterval = nSaveInterval*1000;
	pSysParam->nAutoReportInterval = nSaveInterval;

	if(m_pDoc->SendSetSaveIntervalCommand(m_nSelModuleID) == FALSE)
	{
			
	}

	GetDlgItem(IDC_EDIT_SAVE_INTERVAL)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_SAVE)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_EDIT)->EnableWindow(TRUE);
	

/*	CDaoRecordset rs(&db);
	rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
	if(!rs.IsBOF())
	{
		COleVariant data = rs.GetFieldValue(0);
		address = data.pbVal;
	}

	rs.Close();
	db.Close();


//	CSystemParamRecordSet recordSet;
	recordSet.m_strSort.Format("ModuleID=%d", m_nSelModuleID);

	recordSet.m_strFilter.Format("ModuleID = %d AND ModuleType=%d", m_nSelModuleID, ((CCTSMonApp*)AfxGetApp())->GetSystemType());
	
	try
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return;
	}

	if(!recordSet.IsBOF()&&!recordSet.IsEOF())
	{
		recordSet.Edit();
		recordSet.m_SaveInterval=nSaveInterval;
		recordSet.Update();

		EP_SYSTEM_PARAM *pSysParam = EPGetSysParam(m_nSelModuleID);
		pSysParam->nAutoReportInterval = nSaveInterval*1000;

		if(m_pDoc->SendSetSaveIntervalCommand(m_nSelModuleID) == FALSE)
		{
			
		}

		GetDlgItem(IDC_EDIT_SAVE_INTERVAL)->EnableWindow(FALSE);
		GetDlgItem(IDC_BTN_SAVE)->EnableWindow(FALSE);
		GetDlgItem(IDC_BTN_EDIT)->EnableWindow(TRUE);
	}
	recordSet.Close();
*/
}

BOOL CModuleSetDlg::UpdateSaveInterval(int nModuleID)
{
	if(nModuleID <=0)		return FALSE;

	EP_SYSTEM_PARAM *pSysParam = EPGetSysParam(m_nSelModuleID);

	if(pSysParam == NULL)	return FALSE;	
//	SetDlgItemInt(IDC_EDIT_SAVE_INTERVAL, int(pSysParam->nAutoReportInterval/1000));
	SetDlgItemInt(IDC_EDIT_SAVE_INTERVAL, int(pSysParam->nAutoReportInterval));
	return TRUE;
}

//  [6/30/2009 kky ]
// for 모듈의 설정값을 수정하는 모드
// 모듈의 정보를 기존 이름에서 모듈 ID로 변경, 클릭시 정보창을 통해 모듈 이름을 볼 수 있게 수정
void CModuleSetDlg::OnModifyModule() 
{
	// TODO: Add your control notification handler code here	
	//최대 설치 수 확인 	
	int rsCount = m_TreeX.GetCount();
	if( rsCount == 0 )
	{
		return;
	}	

	CModuleModifyDlg *pDlg;
	pDlg = new CModuleModifyDlg;
	if( pDlg == NULL )
		return;

	CString strTemp;
	pDlg->m_nModuleID = m_nSelModuleID;

	if( pDlg->DoModal() == IDCANCEL )
	{
		delete pDlg;
		pDlg = NULL;
		return;
	}			
	
	m_nTrayType = pDlg->m_nModuleTrayType;	//Tray style 0,1:알파벳 타이틀,2,숫자 타이틀,3:폴리머형 알파벳 타이틀,4:폴리머형 숫자 타이클
	m_nTrayColCnt = pDlg->m_nTrayCol;
	if (pDlg->m_bUseModeChange)
		m_bTrayChange = TRUE;
	else
		m_bTrayChange = FALSE;
	m_fVSpec = pDlg->m_fVSpec;
	m_fISpec = pDlg->m_flSpec;
	int sysType = pDlg->m_nType;
	CString strMDName = pDlg->m_strName;
	delete pDlg;
	pDlg = NULL;
	
	CDaoDatabase  db;
	try
	{
		db.Open(GetDataBaseName());
		CString strSQL,strAddMode;
		
		if (m_bTrayChange)
			strAddMode.Format("Type%d,%d,%d@Type%d,%d,%d",m_nTrayType,m_nTrayType,m_nTrayColCnt,m_nTrayType,m_nTrayType,m_nTrayColCnt);
		else
			strAddMode.Format("Type%d,%d,%d@",m_nTrayType,m_nTrayType,m_nTrayColCnt);

		strSQL.Format( "UPDATE SystemConfig SET ModuleType = %d, MaxVoltage = %f, MinVoltage = %f, MaxCurrent = %f, MinCurrent = %f, Data3 = '%s', Data4 = '%s' WHERE ModuleID = %d", 
			sysType, m_fVSpec, -m_fVSpec, m_fISpec, -m_fISpec, strMDName, strAddMode, m_nSelModuleID);		
		
		db.Execute(strSQL);
		db.Close();
	}
	catch(CDaoException *e)
	{
		// Simply show an error message to the user.
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return;
	}	
	
	HTREEITEM	m_rghItem = m_TreeX.GetSelectedItem();
	if(m_nSelModuleID != m_TreeX.GetItemData(m_rghItem))
	{
		AfxMessageBox("ID Error");
	}
	
	HTREEITEM hItem;	
	//다음 Module이 없으면 
	if(m_TreeX.GetNextVisibleItem(m_rghItem) == NULL)
	{
		hItem = m_TreeX.GetPrevVisibleItem(m_rghItem);
	}
	else
	{
		hItem = m_TreeX.GetNextVisibleItem(m_rghItem);		
	}	
	m_TreeX.DeleteItem(m_rghItem);
	m_TreeX.Invalidate();
	
	//Tree에 생성 
	TV_INSERTSTRUCT		tvstruct;	
	strTemp.Format("%d", m_nSelModuleID);	
	tvstruct.hParent = NULL;
	tvstruct.hInsertAfter = TVI_LAST;
	tvstruct.item.iImage = I_IMAGECALLBACK;
	tvstruct.item.iSelectedImage = I_IMAGECALLBACK;
	tvstruct.item.pszText = (char *)(LPCTSTR)strTemp;		//Display Module ID String
	tvstruct.item.cchTextMax = 64;
	tvstruct.item.mask = TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_TEXT;
	m_rghItem = m_TreeX.InsertItem(&tvstruct);
	m_TreeX.SetItemData(m_rghItem, (DWORD)m_nSelModuleID);			//Set Tree Data to Module ID
	m_TreeX.Invalidate();
	m_bChanged = TRUE;
}
