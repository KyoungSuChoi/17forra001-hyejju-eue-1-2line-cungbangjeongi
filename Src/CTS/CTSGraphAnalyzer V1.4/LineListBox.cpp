// LineListBox.cpp : implementation file
//

#include "stdafx.h"
#include "CTSGraphAnal.h"
#include "LineListBox.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLineListBox
IMPLEMENT_DYNAMIC(CLineListBox,CListBox)

CLineListBox::CLineListBox()
{
}

CLineListBox::~CLineListBox()
{
}


BEGIN_MESSAGE_MAP(CLineListBox, CListBox)
	//{{AFX_MSG_MAP(CLineListBox)
	ON_WM_DRAWITEM_REFLECT()
	ON_WM_MEASUREITEM_REFLECT()
	ON_WM_CONTEXTMENU()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLineListBox message handlers

void CLineListBox::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	if(lpDrawItemStruct->itemID!=LB_ERR){
		CDC* pDC=CDC::FromHandle(lpDrawItemStruct->hDC);
		RECT ClientRect = lpDrawItemStruct->rcItem;
		//
		CString str, linename;
		GetText(lpDrawItemStruct->itemID,str);
		int p1=0, p2=0;
		p2=str.Find(',',p1);
		linename = str.Mid(p1,p2-p1);               //<*****
		BYTE red=0, green=0, blue=0;
		p1=str.Find(',',p2+1);
		red = (BYTE)atoi(str.Mid(p2+1,p1-p2-1));
		p2=str.Find(',',p1+1);
		green = (BYTE)atoi(str.Mid(p1+1,p2-p1-1));
		p1=str.Find(',',p2+1);
		blue = (BYTE)atoi(str.Mid(p2+1,p1-p2-1));
		int width=0, type=PS_SOLID;
		p2=str.Find(',',p1+1);
		width = atoi(str.Mid(p1+1,p2-p1-1));
		type  = atoi(str.Mid(p2+1));
		COLORREF color = RGB(red, green, blue);     //<*****
		//
		CPen apen, *oldpen;
		//
		if(GetSel(lpDrawItemStruct->itemID))
			apen.CreatePen(PS_SOLID,2,RGB(0,0,0));
		else
			apen.CreatePen(PS_SOLID,2,RGB(255,255,255));
		oldpen=pDC->SelectObject(&apen);
		pDC->Rectangle(&lpDrawItemStruct->rcItem);
		pDC->SelectObject(oldpen);
		apen.DeleteObject();
		//
		apen.CreatePen(type,width,color);
		oldpen   = pDC->SelectObject(&apen);

//		pDC->MoveTo(ClientRect.left+2, ClientRect.top+3);
//		pDC->LineTo(ClientRect.right-2,ClientRect.top+3);
		pDC->MoveTo(ClientRect.left+2, ClientRect.top+(ClientRect.bottom-ClientRect.top)/2);
		pDC->LineTo(ClientRect.left+42, ClientRect.top+(ClientRect.bottom-ClientRect.top)/2);

		pDC->SelectObject(oldpen);
		apen.DeleteObject();
		//
//		pDC->TextOut(ClientRect.left+1,ClientRect.top+6,linename);
//		pDC->SetTextAlign(TA_TOP|TA_LEFT);

		CFont afont, *oldfont;
		afont.CreateFont(
						   15,                        // nHeight
						   0,                         // nWidth
						   0,                         // nEscapement
						   0,                         // nOrientation
						   FW_NORMAL,                 // nWeight
						   FALSE,                     // bItalic
						   FALSE,                     // bUnderline
						   0,                         // cStrikeOut
						   ANSI_CHARSET,              // nCharSet
						   OUT_DEFAULT_PRECIS,        // nOutPrecision
						   CLIP_DEFAULT_PRECIS,       // nClipPrecision
						   DEFAULT_QUALITY,           // nQuality
						   DEFAULT_PITCH | FF_SWISS,  // nPitchAndFamily
						   "Arial"					// lpszFacename
						  );                 

		oldfont=pDC->SelectObject(&afont);
			
		pDC->TextOut(ClientRect.left+45,ClientRect.top+1,linename);

		pDC->SelectObject(oldfont);
		afont.DeleteObject();

	}
}

void CLineListBox::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct) 
{
	lpMeasureItemStruct->itemHeight=20;	
}

void CLineListBox::OnContextMenu(CWnd* pWnd, CPoint point) 
{
	// TODO: Add your message handler code here
	CMenu aMenu;
	aMenu.LoadMenu(IDR_CONTEXT_MENU);
	aMenu.GetSubMenu(1)->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON, point.x, point.y, AfxGetMainWnd());


}	
