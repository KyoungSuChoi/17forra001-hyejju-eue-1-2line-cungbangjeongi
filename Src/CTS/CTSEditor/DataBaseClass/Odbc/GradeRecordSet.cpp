// GradeRecordSet.cpp : implementation file
//

#include "stdafx.h"
#include "GradeRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGradeRecordSet

IMPLEMENT_DYNAMIC(CGradeRecordSet, CRecordset)

CGradeRecordSet::CGradeRecordSet(CDatabase* pdb)
	: CRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CGradeRecordSet)
	m_GradeID = 0;
	m_StepID = 0;
	m_GradeItem = 0;
	m_Value = 0.0f;
	m_Value1 = 0.0f;
	m_GradeCode = _T("");
	m_nFields = 6;
	//}}AFX_FIELD_INIT
	m_nDefaultType = snapshot;
}


CString CGradeRecordSet::GetDefaultConnect()
{
	return _T("ODBC;DSN=TestCondition");
}

CString CGradeRecordSet::GetDefaultSQL()
{
	return _T("[Grade]");
}

void CGradeRecordSet::DoFieldExchange(CFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CGradeRecordSet)
	pFX->SetFieldType(CFieldExchange::outputColumn);
	RFX_Long(pFX, _T("[GradeID]"), m_GradeID);
	RFX_Long(pFX, _T("[StepID]"), m_StepID);
	RFX_Long(pFX, _T("[GradeItem]"), m_GradeItem);
	RFX_Single(pFX, _T("[Value]"), m_Value);
	RFX_Single(pFX, _T("[Value1]"), m_Value1);
	RFX_Text(pFX, _T("[GradeCode]"), m_GradeCode);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CGradeRecordSet diagnostics

#ifdef _DEBUG
void CGradeRecordSet::AssertValid() const
{
	CRecordset::AssertValid();
}

void CGradeRecordSet::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG
