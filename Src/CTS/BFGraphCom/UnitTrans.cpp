// UnitTrans.cpp: implementation of the CUnitTrans class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "UnitTrans.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
#define CT_CONFIG_REG_SEC "Config"

CUnitTrans::CUnitTrans()
{
	UpdateUnitSetting();

	m_nVoltageUnitMode = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Vtg Unit Mode", 0);
	m_nCurrentUnitMode = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Crt Unit Mode", 0);
	LanguageinitMonConfig();
}

CUnitTrans::~CUnitTrans()
{
	if( TEXT_LANG != NULL )					
	{					
		delete[] TEXT_LANG;					
		TEXT_LANG = NULL;					
	}
}

bool CUnitTrans::LanguageinitMonConfig() 					
{	
	g_nLanguage = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Language", 0);
	g_strLangPath.Format("%s\\Lang\\GraphAnalyzer_Lang.ini", AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC ,"Path", "C:\Program Files\PNE CTS"));

	switch(g_nLanguage)
	{
	case 0:
		{
			SetThreadUILanguage( MAKELCID( MAKELANGID(LANG_KOREAN , SUBLANG_KOREAN) , SORT_DEFAULT));
			break;
		}

	case 1:
		{
			SetThreadUILanguage( MAKELCID( MAKELANGID(LANG_ENGLISH , SUBLANG_ENGLISH_US) , SORT_DEFAULT));
			break;
		}

	case 2:
		{
			SetThreadUILanguage( MAKELCID( MAKELANGID(LANG_CHINESE_SIMPLIFIED , SUBLANG_CHINESE_SIMPLIFIED) , SORT_DEFAULT));
			break;
		}
	}

	int i=0;				
	int nTextCnt = 0;				
	TEXT_LANG = NULL;				

	CString strErr = _T("");				
	CString strTemp = _T("");				
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_UnitTrans"), _T("TEXT_UnitTrans_CNT"), _T("TEXT_UnitTrans_CNT"));				

	nTextCnt = atoi(strTemp);				

	if( nTextCnt > 0 )				
	{				
		TEXT_LANG = new CString[nTextCnt];			

		for( i=0; i<nTextCnt; i++ )			
		{			
			strTemp.Format("TEXT_UnitTrans_%d", i);		
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_UnitTrans"), strTemp, strTemp, g_nLanguage);		

			if( TEXT_LANG[i] == strTemp )		
			{		
				if( strErr.IsEmpty() )	
				{	
					strErr = "Languge error " + strTemp; 
				}	
				else	
				{	
					strErr += "," + strTemp;
				}	
			}		
		}			

		if( !strErr.IsEmpty() )			
		{			
			AfxMessageBox(strErr);		
			return false;		
		}			
	}				

	return true;				
}					

void CUnitTrans::UpdateUnitSetting()
{
	char szBuff[32], szUnit[16], szDecimalPoint[16];

	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "V Unit", "V 3"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strVUnit = szUnit;
	m_nVDecimal = atoi(szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "I Unit", "mA 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strIUnit = szUnit;
	m_nIDecimal = atoi(szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "C Unit", "mAh 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strCUnit = szUnit;
	m_nCDecimal = atoi(szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "W Unit", "mW 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strWUnit = szUnit;
	m_nWDecimal = atoi(szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Wh Unit", "mWh 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strWhUnit = szUnit;
	m_nWhDecimal = atoi(szDecimalPoint);


	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Time Unit", "0"));
	m_nTimeUnit = atol(szBuff);

}

CString CUnitTrans::ValueString(double dData, int item, BOOL bUnit)
{
	CString strMsg, strTemp;
	double dTemp;
	char szTemp[8];

	dTemp = dData;
	switch(item)
	{
	case PS_STEP_TYPE:	strMsg = ::PSGetTypeMsg((WORD)dData);
		break;

	case PS_STATE:		strMsg = ::PSGetStateMsg((WORD)dData);
		break;
		
	case PS_VOLTAGE:		//voltage

		//mV단위로 변경 
		if(m_strVUnit == "V")
		{
			dTemp = dTemp / 1000.0f;	//LONG2FLOAT(Value);
		}
		if(m_strVUnit == "uV")
		{
			dTemp = dTemp * 1000.0f;	//LONG2FLOAT(Value);
		}
		//else //if(m_strVUnit == "mV")
		//{
		//}
		
		if(m_nVDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nVDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}		
		if(bUnit) strMsg+= (" "+m_strVUnit);
		
		break;

	case PS_CURRENT:		//current
//		if(dTemp < 0)	dTemp = -dTemp;			//양수 표기

		//m단위로 변경
		if(m_nCurrentUnitMode)		dTemp = dTemp /1000.0f;
		
		//current
		if(m_strIUnit == "A")
		{
			dTemp = dTemp/1000.0f;
		}  
		else if(m_strIUnit == "uA")
		{
			dTemp = dTemp * 1000.0f;		// LONG2FLOAT(Value)/1000.0f;
		}
		//else if(m_strIUnit == "mA")	//mA
		//{
		//}

		if(m_nIDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nIDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}
		if(bUnit) strMsg+= (" "+m_strIUnit);

		break;
			
	case PS_WATT	:
		if(dTemp < 0)	dTemp = -dTemp;			//양수 표기

		//m단위로 변경
		if(m_nCurrentUnitMode)		dTemp = dTemp /1000.0f;

		if(m_strWUnit == "W")
		{
			dTemp = dTemp/1000.0f;
		}
		else if(m_strWUnit == "KW")
		{
			dTemp = dTemp/1000000.0f;
		}
		else if(m_strWUnit == "uW")
		{
			dTemp = dTemp*1000.0f;
		}

		if(m_nWDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nWDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}
		if(bUnit) strMsg+= (" "+m_strWUnit);
		break;	
	
	case PS_WATT_HOUR:			
		if(dTemp < 0)	dTemp = -dTemp;			//양수 표기

		//m단위로 변경
		if(m_nCurrentUnitMode)		dTemp = dTemp /1000.0f;

		if(m_strWhUnit == "Wh")
		{
			dTemp = dTemp/1000.0f;
			strTemp = " Wh";
		}
		else if(m_strWhUnit == "KWh")
		{
			dTemp = dTemp/1000000.0f;
		}
		else if(m_strWhUnit == "uWh")
		{
			dTemp = dTemp*1000.0f;
		}

		if(m_nWhDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nWhDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}
		if(bUnit) strMsg+= (" "+m_strWhUnit);
		
		break;
		
	case PS_CAPACITY:		//capacity

		//m단위로 변경
		if(m_nCurrentUnitMode)		dTemp = dTemp / 1000.0f;

		if(m_strCUnit == "Ah" || m_strCUnit == "F")
		{
			dTemp = dTemp/1000.0f;
		}
		else if(m_strCUnit == "uAh" || m_strCUnit == "uF")
		{
			dTemp = dTemp*1000.0f;
		}

		if(m_nCDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nCDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}
		if(bUnit) strMsg+= (" "+m_strCUnit);
		break;

	case PS_IMPEDANCE:	
#ifdef _EDLC_TEST_SYSTEM
		strMsg.Format("%.3f", dTemp);
#else
		strMsg.Format("%.2f", dTemp);
#endif
		if(bUnit) strMsg+= " mOhm";
		break;

	case PS_CODE:	//failureCode
		::PSCellCodeMsg((BYTE)dData, strMsg, strTemp);	//BFGraphCom.dll API
		break;

	case PS_TOT_TIME:
	case PS_STEP_TIME:

		//sbc에서 100msec단위 전송되어 온다.
		if(m_nTimeUnit == 1)	//sec 표시
		{
#ifdef _EDLC_TEST_SYSTEM
			strMsg.Format("%.3f", dTemp);
#else
			strMsg.Format("%.1f", dTemp);
#endif
			if(bUnit) strMsg+= " sec";
		}
		else if(m_nTimeUnit == 2)	//Minute 표시
		{
			strMsg.Format("%.2f", dTemp/60.0f);
			if(bUnit) strMsg+= " Min";
		}
		else
		{
			CTimeSpan timeSpan((ULONG)dTemp);
			float aa = dTemp-(long)dTemp;
			if(timeSpan.GetDays() > 0)
			{
				strMsg.Format("%s.%02d", timeSpan.Format("%Dd %H:%M:%S"), MAKE2LONG(aa*100.0f));
			}
			else
			{
				strMsg.Format("%s.%02d", timeSpan.Format("%H:%M:%S"), MAKE2LONG(aa*100.0f));
			}
		}

/*		{
			char szTemp[32];
			ConvertTime(szTemp, Value);
			strMsg = szTemp;
		}
*/		break;

	case PS_GRADE_CODE:	
		strMsg.Format("%c", (BYTE)dData);
		break;
	case PS_PRESSURE:
	case PS_TEMPERATURE:
		if(0xFFFFFFFF == (float)dTemp)
		{
			strMsg = TEXT_LANG[31];
		}
		else
		{
			strMsg.Format("%.1f", dTemp);
			if(bUnit) strMsg+= " 'C";
		}
		
		break;

	case PS_STEP_NO:
	default:
		strMsg.Format("%d", (int)dTemp);
		break;
	}
	
	return strMsg;
}

CString CUnitTrans::GetUnitString(int nItem)
{
	CString	strTemp;

	switch(nItem)
	{
	case PS_VOLTAGE:		return	m_strVUnit;

	case PS_CURRENT:
		{
			strTemp = m_strIUnit;
/*			if(m_nCurrentUnitMode)
			{
				strTemp = "uA";			//11. Current	
			}
*/			return	strTemp;						
		}
			
	//capa 단위를 따라감
	case PS_WATT	:	return m_strWUnit;

/*		strTemp = " mW";
		if(m_strCUnit == "Ah" || m_strCUnit == "F")
		{
			strTemp = "W";
		}
		if(m_nCurrentUnitMode)
		{
			strTemp = "uW";			//11. Current	
		}
		return strTemp;
*/
	
	//capa 단위를 따라감
	case PS_WATT_HOUR:			return m_strWhUnit;
/*		strTemp = " mWh";
		if(m_strCUnit == "Ah" || m_strCUnit == "F")
		{
			strTemp = "Wh";
		}
		if(m_nCurrentUnitMode)
		{
			strTemp = "uWh";			//11. Current	
		}
		return strTemp;
*/		
	case PS_CAPACITY:
		strTemp = m_strCUnit;
/*		if(m_nCurrentUnitMode)	
		{
#ifdef EDLC_TEST_SYSTEM
			strTemp = "mF";		//12. Capacity,					
#else
			strTemp = "uAh";		//12. Capacity,					
#endif
		}
*/		return strTemp;

	case PS_IMPEDANCE:		strTemp = "mOhm";
		return strTemp;

	case PS_TOT_TIME:
	case PS_STEP_TIME:

		//sbc에서 100msec단위 전송되어 온다.
		if(m_nTimeUnit == 1)	//sec 표시
		{
			strTemp = "sec";
		}
		else if(m_nTimeUnit == 2)	//Minute 표시
		{
			strTemp = "Min";
		}
		return strTemp;

	case PS_TEMPERATURE:
		strTemp = "'C";
		return strTemp;

	case PS_STEP_TYPE:
	case PS_STATE:	
	case PS_CODE:	//failureCode
	case PS_GRADE_CODE:	
	case PS_STEP_NO:
	default:
		break;
	}

	return strTemp;
}
