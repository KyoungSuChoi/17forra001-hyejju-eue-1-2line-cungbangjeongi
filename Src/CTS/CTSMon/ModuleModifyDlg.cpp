// ModuleModifyDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ctsmon.h"
#include "ModuleModifyDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CModuleModifyDlg dialog
extern CString GetModuleName(int nModuleID, int nGroupIndex);


CModuleModifyDlg::CModuleModifyDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CModuleModifyDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CModuleModifyDlg)
	m_flSpec = 0.0f;
	m_fVSpec = 0.0f;
	m_nTrayCol = 0;
	m_bUseModeChange = FALSE;	
	m_nModuleID = 0;
	//}}AFX_DATA_INIT
	LanguageinitMonConfig();
}

CModuleModifyDlg::~CModuleModifyDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CModuleModifyDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CModuleModifyDlg"), _T("TEXT_CModuleModifyDlg_CNT"), _T("TEXT_CModuleModifyDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CModuleModifyDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CModuleModifyDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}


void CModuleModifyDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CModuleModifyDlg)
	DDX_Control(pDX, IDC_CBO_TRAY_TYPE, m_ctrlTrayType);
	DDX_Control(pDX, IDC_COMBO1, m_ctrlModuleType);
	DDX_Text(pDX, IDC_EDIT3, m_flSpec);
	DDX_Text(pDX, IDC_EDIT1, m_fVSpec);
	DDX_Text(pDX, IDC_EDIT_COL_CNT, m_nTrayCol);
	DDX_Check(pDX, IDC_CHK_CHANGE_MODE, m_bUseModeChange);		
	DDX_Text(pDX, IDC_MODULENUM, m_nModuleID);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CModuleModifyDlg, CDialog)
	//{{AFX_MSG_MAP(CModuleModifyDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CModuleModifyDlg message handlers
BOOL CModuleModifyDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
		
	//Registry에서 CTSMon DataBase 경로를 검색 
	long rtn;
	HKEY hKey = 0;
	BYTE buf[512], buf2[512];
	DWORD size = 511;
	DWORD type;

	rtn = ::RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\CTSMon\\FormSetting", 0, KEY_READ, &hKey);
	if(ERROR_SUCCESS == rtn)
	{
		rtn = ::RegQueryValueEx(hKey, "Module Name", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && (type == REG_SZ || type == REG_EXPAND_SZ))
		{
			m_strModuleName = buf;
		}
		size = 255;
		rtn = ::RegQueryValueEx(hKey, "Group Name", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && (type == REG_SZ || type == REG_EXPAND_SZ))
		{
			m_strGroupName = buf;
		}
		size = 255;
		rtn = ::RegQueryValueEx(hKey, "Module Per Rack", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && type == REG_DWORD)
		{
			sprintf((char *)buf2, "%ld", *(long *)buf);
			m_nModulePerRack = atol((LPCTSTR)buf2);
		}
		size = 255;
		rtn = ::RegQueryValueEx(hKey, "Use Rack Index", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && type == REG_DWORD)
		{
			sprintf((char *)buf2, "%ld", *(long *)buf);
			m_bUseRackIndex = atol((LPCTSTR)buf2);
		}
		size = 255;
		rtn = ::RegQueryValueEx(hKey, "Use Group", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && type == REG_DWORD)
		{
			sprintf((char *)buf2, "%ld", *(long *)buf);
			m_bUseGroupSet = atol((LPCTSTR)buf2);
		}
		::RegCloseKey(hKey);
	}

	GetDlgItem(IDC_NAME_EDIT)->SetWindowText(::GetModuleName(m_nModuleID, 0));	
	
	m_ctrlModuleType.AddString("Formation");
	m_ctrlModuleType.SetItemData(0, EP_ID_FORM_OP_SYSTEM);
	m_ctrlModuleType.AddString("IR/OCV");
	m_ctrlModuleType.SetItemData(1, EP_ID_IROCV_SYSTEM);
	m_ctrlModuleType.AddString("Grading");
	m_ctrlModuleType.SetItemData(2, EP_ID_GRADING_SYSTEM);
	m_ctrlModuleType.AddString("Aging");
	m_ctrlModuleType.SetItemData(3, EP_ID_AGIGN_SYSTEM);	
	m_ctrlModuleType.SetCurSel(0);
	
	//Tray Stype 추가 ljb 2008-12
	m_ctrlTrayType.AddString(TEXT_LANG[0]);//"기본형1"
	m_ctrlTrayType.AddString(TEXT_LANG[1]);//"기본형2"
	m_ctrlTrayType.AddString(TEXT_LANG[2]);//"폴리머형1"
	m_ctrlTrayType.AddString(TEXT_LANG[3]);	//"폴리머형2"
	m_ctrlTrayType.SetCurSel(0);

	SetCurrentModuleInfo( m_nModuleID );
	UpdateData(FALSE);
	return TRUE;
}

BOOL CModuleModifyDlg::SetCurrentModuleInfo( UINT nModuleID )
{
	CString strDBPath;
	long rtn;
	HKEY hKey = 0;
	BYTE buf[512];
	DWORD size = 511;
	DWORD type;

	m_bUseModeChange = FALSE;	
	CString strTemp;
	CString strData;
	CDaoDatabase db;

	rtn = ::RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\CTSMon\\Path", 0, KEY_READ, &hKey);
	if(ERROR_SUCCESS == rtn)
	{	
		//CTSMon DataBase 경로를 읽어온다. 
		rtn = ::RegQueryValueEx(hKey, "DataBase", NULL, &type, buf, &size);
		::RegCloseKey(hKey);
		
		//
		if(ERROR_SUCCESS == rtn && (type == REG_SZ || type == REG_EXPAND_SZ))
		{
			strDBPath.Format("%s\\%s", buf, FORM_SET_DATABASE_NAME);
		}
	}
	try
	{
		db.Open(strDBPath);
		// CString strSQL("SELECT * ModuleID FROM SystemConfig ORDER BY ModuleID");
		CString strSQL;
		strSQL.Format("SELECT * FROM SystemConfig WHERE ModuleID = %d", nModuleID);		
		
		CDaoRecordset rs(&db);
		rs.Open( dbOpenSnapshot, strSQL, dbReadOnly );
		COleVariant data = rs.GetFieldValue(0);
		strTemp.Format("%s", ::GetModuleName(data.lVal, 0));
			
		rs.GetFieldValue("ModuleType", data);				
		data.lVal = data.lVal - 2;
		m_ctrlModuleType.SetCurSel(data.lVal);

		rs.GetFieldValue("MaxVoltage", data);		
		m_fVSpec = data.fltVal;	

		rs.GetFieldValue("MaxCurrent", data);		
		m_flSpec = data.fltVal;

		rs.GetFieldValue("Data3", data);
		m_strName.Format("%s", data.lVal);		

		rs.GetFieldValue("Data4", data);
		strTemp.Format("%s", data.lVal);				
		AfxExtractSubString(strData, strTemp, 1, '@');		
		if( !strData.IsEmpty() )
		{
			m_bUseModeChange = TRUE;
		}
		AfxExtractSubString(strData, strTemp, 0, '@');		
		AfxExtractSubString(strTemp, strData, 1, ',');				
		m_nModuleTrayType = atoi(strTemp);
		if( m_nModuleTrayType > 0 )
		{
			m_nModuleTrayType--;
		}
		m_ctrlTrayType.SetCurSel(m_nModuleTrayType);
		AfxExtractSubString(strTemp, strData, 2, ',');		
		m_nTrayCol = atoi(strTemp);		
	}	
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	return TRUE;
}

void CModuleModifyDlg::OnCancel() 
{
	// TODO: Add extra cleanup here	
	CDialog::OnCancel();
}

void CModuleModifyDlg::OnOK() 
{
	// TODO: Add extra validation here
	int index = m_ctrlModuleType.GetCurSel();
	m_nType = m_ctrlModuleType.GetItemData(index);
		
	m_nModuleTrayType = m_ctrlTrayType.GetCurSel();
	if(index >= 0)
		m_nModuleTrayType++;			
	
	GetDlgItem(IDC_NAME_EDIT)->GetWindowText(m_strName);
	if(m_strName.IsEmpty())
	{
		AfxMessageBox(TEXT_LANG[4]);//"이름을 입력하십시요"
		GetDlgItem(IDC_NAME_STATIC)->SetFocus();
		return;
	}
	
	if(m_nModuleID <1 || m_nModuleID > 0x7FFF)
	{
		AfxMessageBox(GetStringTable(IDS_TEXT_VALUE_VALIDE));
		return;
	}	
	CDialog::OnOK();
}
