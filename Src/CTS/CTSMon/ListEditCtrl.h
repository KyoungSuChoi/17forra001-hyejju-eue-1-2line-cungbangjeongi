#if !defined(AFX_LISTEDITCTRL_H__2EC0BDCF_AE6E_4943_B8A4_1C77DF529E85__INCLUDED_)
#define AFX_LISTEDITCTRL_H__2EC0BDCF_AE6E_4943_B8A4_1C77DF529E85__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ListEditCtrl.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CListEditCtrl window

class CListEditCtrl : public CListCtrl
{
// Construction
public:
	CListEditCtrl();

// Attributes
private:
	BOOL	m_bDigitMode;
	float m_fMaxBoundData;
	int m_nCurSel;
	CEdit *m_pEdit;
// Operations
public:
	BOOL m_bCheckCali;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CListEditCtrl)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

// Implementation
public:
	void SetDigitMode(BOOL bSet)	{	m_bDigitMode = bSet;	}
	virtual ~CListEditCtrl();
	void SetBoundaryData(float fMaxBoundaryData) { m_fMaxBoundData = fMaxBoundaryData; }
	void UpdateText();

	// Generated message map functions
protected:
	bool IsDigit(CString strSource);
	BOOL MakeEditCtrl();
	//{{AFX_MSG(CListEditCtrl)
	afx_msg void OnDblclk(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnItemchanged(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LISTEDITCTRL_H__2EC0BDCF_AE6E_4943_B8A4_1C77DF529E85__INCLUDED_)
