// TrayInputDlg.cpp : implementation file
//
IDC_TITLE_STATIC
#include "stdafx.h"
#include "CTSMon.h"
#include "TrayInputDlg.h"
#include "JigIDRegDlg.h"
#include "CTSMonDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTrayInputDlg dialog


CTrayInputDlg::CTrayInputDlg(CCTSMonDoc *pDoc,CWnd* pParent /*=NULL*/)
	: CDialog(CTrayInputDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTrayInputDlg)
	m_strTrayNo = _T("");
	m_strUnitNo = _T("");
	m_strCellBCR = _T("");
	//}}AFX_DATA_INIT
	m_pDoc = pDoc;
	ASSERT(m_pDoc);
	m_nTimer = 0;
	m_ParentWnd = pParent;

	m_bModeLocation = FALSE;
	m_bReadCellStart = FALSE;
// 	m_bReadCellEnd =FALSE;

	m_nSelJigNo = 1;
	m_nSelModuleID = 0;
	m_nTopGridColCount = 1;
	LanguageinitMonConfig();
}



CTrayInputDlg::~CTrayInputDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CTrayInputDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CTrayInputDlg"), _T("TEXT_CTrayInputDlg_CNT"), _T("TEXT_CTrayInputDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CTrayInputDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CTrayInputDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

void CTrayInputDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTrayInputDlg)
	DDX_Control(pDX, IDC_JIG_LIST_COMBO, m_ctrlJigListCombo);
	DDX_Control(pDX, IDC_TITLE_STATIC, m_ctrlTitle);
	DDX_Text(pDX, IDC_EDIT1, m_strTrayNo);
	DDX_Text(pDX, IDC_EDIT2, m_strUnitNo);
	DDX_Text(pDX, IDC_EDIT_CELL_BCR, m_strCellBCR);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTrayInputDlg, CDialog)
	//{{AFX_MSG_MAP(CTrayInputDlg)
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_CBN_SELCHANGE(IDC_JIG_LIST_COMBO, OnSelchangeJigListCombo)
	ON_EN_CHANGE(IDC_EDIT1, OnChangeEdit1)
	ON_BN_CLICKED(IDC_BTN_INPUT, OnBtnInput)
	ON_BN_CLICKED(IDC_BTN_ALL_CLEAR, OnBtnAllClear)
	ON_MESSAGE(EPWM_BCR_SCANED, OnBcrscaned)
	ON_BN_CLICKED(IDC_BTN_CELL_CLEAR, OnBtnCellClear)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTrayInputDlg message handlers

//읽는 순서 Tray BCR => Jig BCR 
/*
BOOL CTrayInputDlg::ReadTrayNo(CString strData)
{
	ShowWindow(SW_SHOW);	

	if(m_nTimer > 0)
	{
		KillTimer(100);
	}


	CString strMsg;
	if(SearchTrayID(strData) == FALSE)
	{
		strMsg.Format("[%s]는 등록되어있지 않은 Tray입니다.\n먼저 등록하십시요." , strData);
		m_ctrlTitle.SetText(strMsg);
		m_ctrlTitle.SetTextColor(RGB(255, 0, 0));
		m_ctrlTitle.FlashText(TRUE);
		return FALSE;
	}

	m_strTrayNo = strData;
	m_strUnitNo.Empty();
	m_ctrlJigListCombo.SetCurSel(-1);

	m_ctrlTitle.FlashText(FALSE);
	m_ctrlTitle.SetTextColor(RGB(0, 0, 0));

	if(m_bModeLocation)
	{
		m_ctrlTitle.SetText("작업 위치를 선택하십시요.");
	}

	UpdateData(FALSE);
	return TRUE;
}

//BCR에 의해 지그ID가 읽히 경우 
BOOL CTrayInputDlg::ReadUnitNo(CString strData)
{
	if(m_bModeLocation == FALSE)	return FALSE;

	ShowWindow(SW_SHOW);	

	CString strMsg;
	int nModuleID = 0, nJigID = 0;
	if(SearchLocation(strData, nModuleID, nJigID) == FALSE)
	{
		strMsg.Format("[%s]는 등록되어있지 위치 정보입니다.\n위치정보를 먼저 등록하십시요." , strData);
		m_ctrlTitle.SetText(strMsg);
		m_ctrlTitle.SetTextColor(RGB(255, 0, 0));
		m_ctrlTitle.FlashText(TRUE);
		return FALSE;
	}
		
	//Set Combo Ctrl
	//
	int bFind = FALSE;
	DWORD nID = MAKELONG(nJigID, nModuleID) ;
	for(int i=0; i<m_ctrlJigListCombo.GetCount(); i++)
	{
		if(m_ctrlJigListCombo.GetItemData(i) == nID)
		{
			m_ctrlJigListCombo.SetCurSel(i);
			bFind = TRUE;
			break;
		}
	}
	if(bFind == FALSE)
	{
		m_ctrlJigListCombo.SetCurSel(-1);
		GetDlgItem(IDC_EDIT2)->SetWindowText("????????");
		
		AfxMessageBox("모듈에 존재하지 않는 위치 정보 입니다.");
		return FALSE;
	}
	//

	m_strUnitNo = strData;
	UpdateData(FALSE);

	if(m_strTrayNo.IsEmpty())
	{
		//Error
		m_ctrlTitle.SetText("Tray 번호를 먼저 입력하십시요.");
		m_ctrlTitle.SetTextColor(RGB(255, 0, 0));
		m_ctrlTitle.FlashText(TRUE);

		SetHideDelayTime(5000);

		m_strTrayNo.Empty();
		m_strUnitNo.Empty();
		return FALSE;
	}
	else
	{
		//DataBase에서 지정 위치 검색
		m_ctrlTitle.FlashText(FALSE);
		m_ctrlTitle.SetTextColor(RGB(0, 0, 0));
		m_ctrlTitle.SetText("입력이 완료 되었습니다.");
	
		SendTrayNoReadMsg(m_strTrayNo, nModuleID, nJigID);
		
		//reset data
		SetHideDelayTime(2000);
	}

	m_strTrayNo.Empty();
	m_strUnitNo.Empty();
	return TRUE;
}
*/
BOOL CTrayInputDlg::ReadCellNo(CString strData)
{
// 	int nIndex = m_ctrlJigListCombo.GetCurSel();
// 	DWORD dwID = 0;
// 	int nModuleID = 0, nJigID = 0;
// 	if(nIndex < 0)
// 	{
// 		m_strUnitNo.Empty();
// 		return FALSE;
// 	}
// 	else
// 	{
// 		dwID = m_ctrlJigListCombo.GetItemData(nIndex);
// 		nModuleID = HIWORD(dwID);
// 		nJigID = LOWORD(dwID);
// 	}
	CString strTemp;
	CTray	*pTrayData;
	CFormModule *pModule = m_pDoc->GetModuleInfo(m_nModuleID);
	if(pModule == NULL)	 return FALSE;
	
	strTemp.Format("%s :: Tray [%s] , Cell No [%s] Readed", ::GetModuleName(m_nModuleID),m_strTrayNo, strData);
	m_pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
	
	pTrayData = pModule->GetTrayInfo(m_nJigID-1);
	if(pTrayData == NULL)	return FALSE;

	int nCellNum;
	//Get Cell Write Position Num
	if (strData == "PNE_INIT")
	{
		//초기화
		
	}
	else
	{
		Fun_SetBarCodeToForcusCell(strData);	// Forcus 셀에 BarCode 쓰기 -> 다음 포지현으로 이동 ->  Write 한 nCellNum return
	}
	
// 	if (m_bReadCellEnd)
// 	{
// 		m_ctrlTitle.FlashText(FALSE);
// 		m_ctrlTitle.SetTextColor(RGB(0, 0, 0));
// 		m_ctrlTitle.SetText("입력이 완료 되었습니다.");
// 		
// 		//DataBase에서 지정 위치 검색
// 		m_ctrlTitle.FlashText(FALSE);
// 		m_ctrlTitle.SetTextColor(RGB(0, 0, 0));
// 		m_ctrlTitle.SetText("입력이 완료 되었습니다.");
// 		
// 		SendTrayNoReadMsg(m_strTrayNo, nModuleID, nJigID);
// 		
// 		SetHideDelayTime(2000);
// 	}
//	if (nCellNum < 0 || nCellNum > m_nTotalChannelNum) return FALSE;
	
// 	pTrayData->SetCellSerial(nCellNum,strData);		// CTray 에 Cell BarCode 입력
// 	strTemp.Format("Cell Num = %d, Cell BarCode = %s\n",nCellNum,strData);
// 	TRACE(strTemp);
	//AfxMessageBox(strData);
	m_wndCellGrid.Redraw();	
	return TRUE;
}

//읽는 순서 Jig BCR => Tray BCR => (if m_bReadCellStart == TRUE) Read Cell BCR
BOOL CTrayInputDlg::ReadTrayNo(CString strData)
{
	ShowWindow(SW_SHOW);	

	CString strMsg;
	if(SearchTrayID(strData) == FALSE)
	{
		strMsg.Format(TEXT_LANG[0] , strData);//"[%s]는 등록되어있지 않은 Tray입니다.\n먼저 등록하십시요."
		m_ctrlTitle.SetText(strMsg);
		m_ctrlTitle.SetTextColor(RGB(255, 0, 0));
		m_ctrlTitle.FlashText(TRUE);
		return FALSE;
	}
	m_strTrayNo = strData;

	int nIndex = m_ctrlJigListCombo.GetCurSel();
	DWORD dwID = 0;
	int nModuleID = 0, nJigID = 0;
	if(nIndex < 0)
	{
		m_strUnitNo.Empty();
	}
	else
	{
		dwID = m_ctrlJigListCombo.GetItemData(nIndex);
		nModuleID = HIWORD(dwID);
		nJigID = LOWORD(dwID);
	}

	UpdateData(FALSE);
	// ljb 2009925 S	/////////////////
	if(m_bModeLocation == TRUE && m_bUseCellBarCode == FALSE)
	{
		if(m_strUnitNo.IsEmpty())
		{
			//Error
			m_ctrlTitle.SetText(TEXT_LANG[1]);//"작업 위치(Jig Bar Code)를 먼저 지정하십시요."
			m_ctrlTitle.SetTextColor(RGB(255, 0, 0));
			m_ctrlTitle.FlashText(TRUE);

			SetHideDelayTime(5000);

			m_strTrayNo.Empty();
			m_strUnitNo.Empty();
			return FALSE;
		}
		else
		{
			//DataBase에서 지정 위치 검색
			m_ctrlTitle.FlashText(FALSE);
			m_ctrlTitle.SetTextColor(RGB(0, 0, 0));
			m_ctrlTitle.SetText(TEXT_LANG[2]);//"입력이 완료 되었습니다."
		
			SendTrayNoReadMsg(m_strTrayNo, nModuleID, nJigID);
			
			//reset data
			SetHideDelayTime(2000);
		}

		m_strTrayNo.Empty();
		m_strUnitNo.Empty();
	}

	if(m_bModeLocation == TRUE && m_bUseCellBarCode == TRUE)
	{
		if(m_strUnitNo.IsEmpty())
		{
			//Error
			m_ctrlTitle.SetText(TEXT_LANG[1]);//"작업 위치(Jig Bar Code)를 먼저 지정하십시요."
			m_ctrlTitle.SetTextColor(RGB(255, 0, 0));
			m_ctrlTitle.FlashText(TRUE);
			
			SetHideDelayTime(5000);
			
			m_strTrayNo.Empty();
			m_strUnitNo.Empty();
			return FALSE;
		}
		else
		{
//			m_bReadCellStart = TRUE;
//			m_bReadCellEnd = FALSE;
			//DataBase에서 지정 위치 검색
			m_ctrlTitle.FlashText(FALSE);
			m_ctrlTitle.SetTextColor(RGB(0, 0, 0));
			m_ctrlTitle.SetText(TEXT_LANG[3]);//"Cell 입력 하세요."
			
			//SendTrayNoReadMsg(m_strTrayNo, nModuleID, nJigID);
			if (!Fun_GetTrayAllData(nModuleID,nJigID)) AfxMessageBox("Tray Data Read Fail");	//ljb CTray에 있는 데이터를 표시 한다
			m_wndCellGrid.SetCurrentCell(1,2);
			m_wndCellGrid.SelectRange(CGXRange(1,2));	//Cell 입력 확인창 처음 셀 선택
			
		}
		
	}
	// ljb 2009925 E /////////////////
	return TRUE;
}

//BCR에 의해 지그ID가 읽히 경우 
BOOL CTrayInputDlg::ReadUnitNo(CString strData)
{
	if(m_bModeLocation == FALSE)	return FALSE;

	ShowWindow(SW_SHOW);	

	if(m_nTimer > 0)
	{
		KillTimer(100);
	}

	CString strMsg;
	m_nModuleID = 0, m_nJigID = 0;
	if(SearchLocation(strData, m_nModuleID, m_nJigID) == FALSE)
	{
		strMsg.Format(TEXT_LANG[4] , strData);//"[%s]는 등록되어있지 위치 정보입니다.\n위치정보를 먼저 등록하십시요."
		m_ctrlTitle.SetText(strMsg);
		m_ctrlTitle.SetTextColor(RGB(255, 0, 0));
		m_ctrlTitle.FlashText(TRUE);
		return FALSE;
	}

	//Set Combo Ctrl
	int bFind = FALSE;
	DWORD nID = MAKELONG(m_nJigID, m_nModuleID) ;
	for(int i=0; i<m_ctrlJigListCombo.GetCount(); i++)
	{
		if(m_ctrlJigListCombo.GetItemData(i) == nID)
		{
			m_ctrlJigListCombo.SetCurSel(i);
			bFind = TRUE;
			break;
		}
	}
	if(bFind == FALSE)
	{
		m_ctrlJigListCombo.SetCurSel(-1);
		GetDlgItem(IDC_EDIT2)->SetWindowText("????????");
		
		AfxMessageBox(TEXT_LANG[5]);//"모듈에 존재하지 않는 위치 정보 입니다."
		return FALSE;
	}
	//

	m_strUnitNo = strData;

	m_strTrayNo.Empty();
	m_ctrlTitle.FlashText(FALSE);
	m_ctrlTitle.SetTextColor(RGB(0, 0, 0));

	//m_ctrlTitle.SetText("Tray 번호를 입력 하십시요.");
	m_ctrlTitle.SetText(TEXT_LANG[6]);//"Cell BarCode를 입력 하십시요."

	UpdateData(FALSE);
	return TRUE;
	
}
//////////////////////////////////////////////////////////////////////////

void CTrayInputDlg::OnDestroy() 
{
//	delete [] m_pTopColorFlag;
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here
	
}

void CTrayInputDlg::SetHideDelayTime(int nMiliSec)
{
	if(m_nTimer > 0)
	{
		KillTimer(100);
	}
	m_nTimer = SetTimer(100, nMiliSec, NULL);
}

void CTrayInputDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	ShowWindow(SW_HIDE);

	//Reset Timer
	KillTimer(100);
	m_nTimer = 0;

	CDialog::OnTimer(nIDEvent);
}

LRESULT CTrayInputDlg::OnBcrscaned(UINT wParam, LONG lParam)
{
	char *data = (char *)lParam;
	CString str(data);
	
	if(str.IsEmpty())	
		return 0;

// 	if(wParam == TAG_TYPE_TRAY)
// 	{
// 		m_bReadCellStart = FALSE;
// 		m_bReadCellEnd = FALSE;
// 		ReadTrayNo(str);	
// 	}

	else if(wParam == TAG_TYPE_JIG)
	{
		UINT nRow=1,nCol=2;
		m_bReadCellStart = FALSE;
		DrawTopGrid();		//ljb Grid 다시 그림
		if (m_bUseCellBarCode)
		{
			m_wndCellGrid.SetCurrentCell(nRow,nCol);
			m_wndCellGrid.SelectRange(CGXRange(nRow,nCol));	//Cell 입력 확인창 처음 셀 선택
		}
		if (ReadUnitNo(str)) 	m_bReadCellStart = TRUE;
	}
	else
	{
		if(wParam == TAG_TYPE_NONE)
		{
			if (m_bReadCellStart)
			{
				ReadCellNo(str);
			}
			else
			{
				//Unknown tag id
				AfxMessageBox(TEXT_LANG[7], MB_OK|MB_ICONSTOP);//"알수 없는 코드입니다.(먼저 등록 후 사용하십시요)"
				return 0;
			}
		}
	}

	return 1;
}

void CTrayInputDlg::SendTrayNoReadMsg(CString strTray, int nModuleID, int nJigNo)
{
	if(m_ParentWnd != NULL)
	{
		static EP_TAG_INFOMATION tagData;
		memset(&tagData, 0, sizeof(tagData));
		sprintf(tagData.szTagID, m_strTrayNo);

		::PostMessage(m_ParentWnd->m_hWnd, WM_BCR_READED, 
			MAKELONG(nJigNo, nModuleID), (LPARAM)&tagData);			//Send to Parent Wnd to Module State change
	}
}

void CTrayInputDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
	CString strMsg;
	if(m_strTrayNo.IsEmpty())
	{
		AfxMessageBox(TEXT_LANG[8]);//"Tray 번호를 입력 하세요."
		GetDlgItem(IDC_EDIT1)->SetFocus();
		return;
	}

// 	if(SearchTrayID(m_strTrayNo) == FALSE)
// 	{
// 		strMsg.Format("[%s]는 등록되어있지 않은 Tray입니다.\n먼저 등록하십시요." , m_strTrayNo);
// 		//GetDlgItem(IDC_TITLE_STATIC)->SetWindowText(strMsg);
// 
// 		m_ctrlTitle.SetText(strMsg);
// 		m_ctrlTitle.SetTextColor(RGB(255, 0, 0));
// 		m_ctrlTitle.FlashText(TRUE);
// 		return;
// 	}

	if(m_bModeLocation)
	{
		int nSel = m_ctrlJigListCombo.GetCurSel();
		if(nSel != CB_ERR)
		{
			int nID = m_ctrlJigListCombo.GetItemData(nSel);
			m_nSelModuleID = HIWORD(nID);
			m_nSelJigNo = LOWORD(nID);
		}
		else
		{
			strMsg.Format(TEXT_LANG[9]);//"작업 위치가 선택되지 않았습니다."
			AfxMessageBox(strMsg);
			return;
		}

		m_bReadCellStart = FALSE;
		
		Fun_SetCellDataToCTray();		//CTray 에 Grid에 있는 데이터 셋팅
		SendTrayNoReadMsg(m_strTrayNo, m_nSelModuleID, m_nSelJigNo);
	}
	
// 	if (m_bUseCellBarCode && m_bReadCellStart)
// 	{
// 		strMsg.Format("CELL BarCode 를 입력 하세요.");
// 		AfxMessageBox(strMsg);
// 		return;
// 	}

	CDialog::OnOK();
}

BOOL CTrayInputDlg::SearchLocation(CString strID, int &nModuleID, int &nJigID)
{
	CJigIDRegDlg *pDlg = new CJigIDRegDlg(this);
	
	int nMD, nJig;
	if(pDlg->SearchLocation(strID, nMD, nJig))
	{
		delete pDlg;
		nModuleID = nMD;
		nJigID = nJig;
		return TRUE;
	}

	delete pDlg;
	nModuleID = 0;
	nJig = 0;
	return FALSE;
}

BOOL CTrayInputDlg::SearchTrayID(CString strID)
{
	CDaoDatabase  db;
	try
	{
		db.Open(::GetDataBaseName());
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	

	CString strSQL, strTemp;
	strSQL.Format("SELECT TraySerial FROM Tray WHERE TrayNo = '%s'", strID);

//	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return FALSE;
	}	
	
	if(rs.IsBOF() || rs.IsEOF())
	{
		rs.Close();
		db.Close();
	
		return FALSE;
	}
	rs.Close();
	db.Close();
	return TRUE;
}

void CTrayInputDlg::OnSelchangeJigListCombo() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

	if(m_nTimer > 0)
	{
		KillTimer(100);
	}
	
	int nSel = m_ctrlJigListCombo.GetCurSel();
	if(nSel != CB_ERR)
	{
		int nID = m_ctrlJigListCombo.GetItemData(nSel);
		int nMD = HIWORD(nID);
		int nJig = LOWORD(nID);

		CJigIDRegDlg *pDlg = new CJigIDRegDlg(this);
		CString strID = pDlg->SearchLocation(nMD, nJig);

		if(strID.IsEmpty())
		{
			//수동 입력된 상태(DB에 위치정보가 존재하지 않음)
			strID.Format(TEXT_LANG[10], ::GetModuleName(nMD), nJig);//"%s의 지그 %d에 위치 정보 ID가 존재하지 않습니다. 등록하십시요."
			//AfxMessageBox(strID);
			strID = "????????";
		}
		else
		{

		}

		m_strUnitNo = strID;
		UpdateData(FALSE);
		delete pDlg;
	}
}

BOOL CTrayInputDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CString str;

	int nInstallMD = EPGetInstalledModuleNum();

	int nCnt = 0;
	int nDefaultIndex = -1;
	m_warryChannelCnt.RemoveAll();
	for(int i=0; i<nInstallMD; i++)
	{
		int nModuleID = EPGetModuleID(i);
		EP_MD_SYSTEM_DATA *pSys = EPGetModuleSysData(i);

		for(int j=0; j<pSys->wTotalTrayNo; j++)
		{
			int nJigID = j+1;
			str.Format("%s - Jig %d", ::GetModuleName(nModuleID), j+1);
			m_ctrlJigListCombo.AddString(str);
			
			//default selection
			if(nModuleID == m_nSelModuleID && nJigID == m_nSelJigNo)
			{
				nDefaultIndex = nCnt;
			}
			long lID = MAKELONG(nJigID, nModuleID);
			m_ctrlJigListCombo.SetItemData(nCnt++, lID);
			m_warryChannelCnt.Add(pSys->awChInTray[j]);
		}
	}
	if(m_ctrlJigListCombo.GetCount() == 1)
	{
		nDefaultIndex = 0;
	}
	m_ctrlJigListCombo.SetCurSel(nDefaultIndex);
	OnSelchangeJigListCombo();

	GetDlgItem(IDC_JIG_LIST_COMBO)->EnableWindow(m_bModeLocation);
	
	if(m_strTitleText.IsEmpty() == FALSE)
	{
		GetDlgItem(IDC_TITLE_STATIC)->SetWindowText(m_strTitleText);
	}

	GetDlgItem(IDC_EDIT1)->SetFocus();
	
	// ljb 2009925 S	/////////////////
	CRect rect;
	GetClientRect(rect);
	m_bUseCellBarCode = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "USE CELL Bar Code", FALSE);
	if (m_bUseCellBarCode)
	{
		//Cell Input Grid 초기화
//		m_pConfig = m_pDoc->GetTopConfig();						//Top Config
//		m_pTopColorFlag = NULL;
		
		// 레지스트리 Setting 부분
		// m_nTopGridColCount = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "TopGridColCout", 16);
// 		m_nTopGridRowCount = EP_MAX_CH_PER_MD/m_nTopGridColCount;
// 		if(EP_MAX_CH_PER_MD % m_nTopGridColCount) m_nTopGridRowCount++;
		if (m_warryChannelCnt.GetSize() > 0)
		{
			m_nTotalChannelNum = m_warryChannelCnt.GetAt(0);
		}
		else
		{
			m_nTotalChannelNum = EP_MAX_CH_PER_MD;
		}
		m_nTopGridRowCount = m_nTotalChannelNum/m_nTopGridColCount;
		if(m_nTotalChannelNum % m_nTopGridColCount) m_nTopGridRowCount++;
		
		InitCellGrid();
	}
	else
	{
		//Dialog Size 조절
		//SetWindowPos(this,rect.left,rect.top,rect.Width(),rect.Height()-500,SW_SHOW);
		MoveWindow(rect.left,rect.top,rect.Width()-390,rect.Height()-610);
	}
	CenterWindow();

	// ljb 2009925 E /////////////////

	
	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CTrayInputDlg::SetTitle(CString str)
{
	m_strTitleText = str;
}

void CTrayInputDlg::SetUnitInput(BOOL bUnit)
{
	m_bModeLocation = bUnit;
}


void CTrayInputDlg::OnChangeEdit1() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	if(m_nTimer > 0)
	{
		KillTimer(100);
	}
}

void CTrayInputDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	m_bReadCellStart = FALSE;
	m_strTrayNo.Empty();
	m_strUnitNo.Empty();
	m_ctrlJigListCombo.SetCurSel(-1);

	m_ctrlTitle.FlashText(FALSE);
	m_ctrlTitle.SetTextColor(RGB(0, 0, 0));
	m_ctrlTitle.SetText(TEXT_LANG[11]);//"작업할 번호를 입력하십시요."

	UpdateData(FALSE);
	CDialog::OnCancel();
}
// ljb 2009925 S	/////////////////
void CTrayInputDlg::InitCellGrid()
{
	m_wndCellGrid.SubclassDlgItem(IDC_CELL_BCR_GRID, this);
	m_wndCellGrid.m_bSameRowSize = TRUE;
	m_wndCellGrid.m_bCustomColor = TRUE;
	
	m_wndCellGrid.Initialize();
	m_wndCellGrid.SetRowCount(1);
	m_wndCellGrid.SetColCount(1);
	m_wndCellGrid.SetDefaultRowHeight(26);
	
	//Enable Multi Channel Select
	m_wndCellGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	//Enable Tooltips
	m_wndCellGrid.EnableGridToolTips();
    m_wndCellGrid.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, 
		CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));
	//Use MemDc
	m_wndCellGrid.SetDrawingTechnique(gxDrawUsingMemDC);
	
	//Row Header Setting
	m_wndCellGrid.SetStyleRange(CGXRange().SetCols(1),
		CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(255,255,255)).SetInterior(RGB(192,192,192)));
	
	
	//Table Name
	m_wndCellGrid.SetStyleRange(CGXRange(0, 1), CGXStyle().SetValue("CH(/)"));
	
	DrawTopGrid();
}
// ljb 2009925 E /////////////////

void CTrayInputDlg::DrawTopGrid()
{
	Invalidate();
	BOOL bLock = m_wndCellGrid.LockUpdate();
	//ReDraw Grid

	m_wndCellGrid.SetRowCount(1);
	m_wndCellGrid.SetColCount(1);		//Delete Grid Attribute
	m_wndCellGrid.SetRowCount(m_nTopGridRowCount);		//ReDraw 
	m_wndCellGrid.SetColCount(m_nTopGridColCount+1);

//	if(m_pTopColorFlag)
// 	{
// 		delete[] m_pTopColorFlag;
// 		m_pTopColorFlag = NULL;
// 	}
// 
// 	m_pTopColorFlag = new char[m_nTopGridRowCount*m_nTopGridColCount];
// 
// 	//memset(m_pTopColorFlag, STATE_COLOR, m_nTopGridRowCount*m_nTopGridColCount);		//Default color Setting Array[15]
// 	memset(m_pTopColorFlag, 10, m_nTopGridRowCount*m_nTopGridColCount);		//Default color Setting Array[15]
// 	m_wndCellGrid.m_pCustomColorFlag = (char *)m_pTopColorFlag;
// 	m_wndCellGrid.m_CustomColorRange	= CGXRange(1, 2, m_nTopGridRowCount, m_nTopGridColCount+1);

	int nCount;

	//Row Header Label
	int nGroupIndex = 0,a,nI;
	nCount = 0;
	CString strBuff;

	WORD nTrayType = m_pDoc->m_nViewTrayType;
	switch (nTrayType)
	{
	case TRAY_TYPE_LG_COL_ROW:	//가로 번호 체계 
		//Column header Label
		for ( a = 1; a <= m_nTopGridColCount; a++)
		{
			m_wndCellGrid.SetStyleRange(CGXRange(0, a+1), CGXStyle().SetValue(ROWCOL(a)));
		}
		
		//Row Header Label
		for ( nI = 0; nI < m_nTopGridRowCount; nI++)
		{
			if(nI / 26 > 0)
			{
				strBuff.Format("%c%c (%3d)", 'A'+nI/26-1, 'A'+ nI%26, nCount++*m_nTopGridColCount+1);
			}
			else
			{
				strBuff.Format("%c (%3d)", 'A'+ nI, nCount++*m_nTopGridColCount+1);
			}
			
			m_wndCellGrid.SetStyleRange(CGXRange(nI+1, 1), CGXStyle().SetValue(strBuff).SetDraw3dFrame(gxFrameRaised));
// 			if(nCount*m_nTopGridColCount >= EPGetChInGroup(m_nSelModuleID, m_nSelJigNo))
// 			{
// 				nCount = 0;
// 				nGroupIndex++;
// 			}
		}		
		break;
	case TRAY_TYPE_PB5_COL_ROW:	//가로 번호 체계(각형)
		//Column header Label
		for ( a = 1; a <= m_nTopGridColCount; a++)
		{
			m_wndCellGrid.SetStyleRange(CGXRange(0, a+1), CGXStyle().SetValue(ROWCOL(a)));
		}
		
		//Row Header Label
		for ( nI = 0; nI < m_nTopGridRowCount; nI++)
		{
			if(nI == 0)
			{
				if(nI / 26 > 0)		//A~Z 초과시
				{
					strBuff.Format("%c%c (%3d)", 'A'+nI/26-1, 'A'+ nI%26, nCount++*m_nTopGridColCount+1);
				}
				else
				{
					strBuff.Format("%c (%3d)", 'A'+ nI, nCount++*m_nTopGridColCount+1);
				}
			}
			else
			{
				if(nI / 26 > 0)		//A~Z 초과시
				{
					strBuff.Format("%c%c (%3d)", 'A'+nI/26-1, 'A'+ nI%26, nCount++*m_nTopGridColCount-1);
				}
				else
				{
					strBuff.Format("%c (%3d)", 'A'+ nI, nCount++*m_nTopGridColCount+-1);
				}
			}
			
			m_wndCellGrid.SetStyleRange(CGXRange(nI+1, 1), CGXStyle().SetValue(strBuff).SetDraw3dFrame(gxFrameRaised));
// 			if(nCount*m_nTopGridColCount >= EPGetChInGroup(m_nSelModuleID, m_nSelJigNo))
// 			{
// 				nCount = 0;
// 				nGroupIndex++;
// 			}
		}		
		break;
	case TRAY_TYPE_PB5_ROW_COL:	//세로 번호 체계(각형)
		//Row header Lable
		m_wndCellGrid.SetStyleRange(CGXRange(0, 1), CGXStyle().SetValue("CH(/)"));
		
		for ( a = 1; a <=m_nTopGridRowCount; a++)
		{
			m_wndCellGrid.SetStyleRange(CGXRange(a, 1), CGXStyle().SetValue(ROWCOL(a)).SetDraw3dFrame(gxFrameRaised));
		}
		//Col Header Label
		for ( nI = 0; nI < m_nTopGridColCount; nI++)
		{
			if(nI == 0)
			{
				if(nI / 26 > 0)		//A~Z 초과시
				{
					strBuff.Format("%c%c (%3d)", 'A'+nI/26-1, 'A'+ nI%26, nCount++*m_nTopGridRowCount+1);
				}
				else
				{
					strBuff.Format("%c (%3d)", 'A'+ nI, nCount++*m_nTopGridRowCount+1);
				}
			}
			else
			{
				if(nI / 26 > 0)		//A~Z 초과시
				{
					strBuff.Format("%c%c (%3d)", 'A'+nI/26-1, 'A'+ nI%26, nCount++*m_nTopGridRowCount-1);
				}
				else
				{
					strBuff.Format("%c (%3d)", 'A'+ nI, nCount++*m_nTopGridRowCount+-1);
				}
			}
			m_wndCellGrid.SetStyleRange(CGXRange(0, nI+2), CGXStyle().SetValue(strBuff));
// 			if(nCount*m_nTopGridRowCount >= EPGetChInGroup(m_nSelModuleID, m_nSelJigNo))
// 			{
// 				nCount = 0;
// 				nGroupIndex++;
// 			}
		}
		break;
	case TRAY_TYPE_CUSTOMER:	//세로 번호 체계
	case TRAY_TYPE_LG_ROW_COL:	//세로 번호 체계
		//Row header Lable
		m_wndCellGrid.SetStyleRange(CGXRange(0, 1), CGXStyle().SetValue("CH(/)"));
		
		for (a = 1; a <=m_nTopGridRowCount; a++)
		{
			m_wndCellGrid.SetStyleRange(CGXRange(a, 1), CGXStyle().SetValue(ROWCOL(a)).SetDraw3dFrame(gxFrameRaised));
		}
		
		//Col Header Label
		for ( nI = 0; nI < m_nTopGridColCount; nI++)
		{
			if(nI / 26 > 0)		//A~Z 초과시
			{
				strBuff.Format("%c%c (%3d)", 'A'+nI/26-1, 'A'+ nI%26, nCount++*m_nTopGridRowCount+1);
			}
			else
			{
				strBuff.Format("%c (%3d)", 'A'+ nI, nCount++*m_nTopGridRowCount+1);
			}
			m_wndCellGrid.SetStyleRange(CGXRange(0, nI+2), CGXStyle().SetValue(strBuff));
// 			if(nCount*m_nTopGridRowCount >= EPGetChInGroup(m_nSelModuleID, m_nSelJigNo))
// 			{
// 				nCount = 0;
// 				nGroupIndex++;
// 			}
		}
		break;		
	}
	m_wndCellGrid.LockUpdate(bLock);
	m_wndCellGrid.Redraw();
}

void CTrayInputDlg::Fun_SetClearCellNum(int nRow,int nCol)
{
	CString strTemp;
	if (nRow <1 || nCol < 2)
	{
		AfxMessageBox(TEXT_LANG[12]);//"입력가능한 위치가 아닙니다"
		return ;	
	}
	strTemp="";

		m_wndCellGrid.SetValueRange(CGXRange(nRow, nCol), strTemp);		//clear
//		m_wndCellGrid.SetStyleRange(CGXRange(nRow, nCol),
//			CGXStyle().SetInterior(colRgbCell));
// 	BOOL bLock = m_wndCellGrid.LockUpdate();
// 	//Row Header Label
// 	int nCellPos = 0;
// 	int nGridColCount = m_wndCellGrid.GetColCount();
// 	int nGridRowCount = m_wndCellGrid.GetRowCount();
// 
// 	WORD nTrayType = m_pDoc->m_nViewTrayType;
// 	switch (nTrayType)
// 	{
// 	case TRAY_TYPE_LG_COL_ROW:	//가로 번호 체계 
// 		nCellPos = ((nRow-1)*(nGridColCount-1)) + (nCol-1); //CTray에 저장 할 셀 위치
// 		//Grid에서 다음 Row, Col 위치를 찾는다.////
// 		break;
// 	case TRAY_TYPE_PB5_COL_ROW:	//가로 번호 체계(각형)
// 		//Column header Label
// 		break;
// 	case TRAY_TYPE_PB5_ROW_COL:	//세로 번호 체계(각형)
// 		//Row header Lable
// 		break;
// 	case TRAY_TYPE_CUSTOMER:	//세로 번호 체계
// 	case TRAY_TYPE_LG_ROW_COL:	//세로 번호 체계
// 		nCellPos = ((nCol-2)*(nGridRowCount)) + (nRow-1); //CTray에 저장 할 셀 위치
// 		break;		
// 	}
// //	m_wndCellGrid.SetCurrentCell(nRow,nCol);
// //	m_wndCellGrid.SelectRange(CGXRange(nRow,nCol));
// 	m_wndCellGrid.LockUpdate(bLock);
// 	return nCellPos;
}
void CTrayInputDlg::Fun_SetBarCodeToForcusCell(CString strCellNo)
{
	COLORREF colRgbCell;
	COLORREF colRgbValue;
//	colRgbCell = RGB(243,248,84);
//	colRgbValue = RGB(211,208,154);

	ROWCOL nRow, nCol;
	m_wndCellGrid.GetCurrentCell(nRow,nCol);
	if (nRow <1 || nCol < 2)
	{
		AfxMessageBox(TEXT_LANG[12]);//"입력가능한 위치가 아닙니다"
		return;	
	}
	
// 	m_CellGrid.SetStyleRange(CGXRange().SetRows(2,3),
// 		CGXStyle().SetReadOnly(FALSE));
// 	
// 	m_CellGrid.SetStyleRange(CGXRange().SetRows(5,6),
// 		CGXStyle().SetReadOnly(FALSE));

//		m_wndCellGrid.SetValueRange(CGXRange(nRow, nCol), strCellNo);		//입력한 바코드 표시
//		m_wndCellGrid.SetStyleRange(CGXRange(nRow, nCol),
//			CGXStyle().SetInterior(colRgbCell));
//		m_wndCellGrid.SelectRange(CGXRange(nRow,nCol),FALSE);		//unselected
// 	m_wndCellGrid.SetStyleRange(CGXRange(nRow+1, nCol, nRow+2, nCol),
// 		CGXStyle().SetInterior(colRgbValue));	
	
// 	m_wndCellGrid.SetValueRange(CGXRange(nRow+1, nCol), strTestValue);
// 	m_wndCellGrid.SetValueRange(CGXRange(nRow+2, nCol), strLotValue);			
	
// 	m_wndCellGrid.SetStyleRange(CGXRange().SetRows(2,3),
// 		CGXStyle().SetReadOnly(TRUE));
// 	
// 	m_wndCellGrid.SetStyleRange(CGXRange().SetRows(5,6),
// 		CGXStyle().SetReadOnly(TRUE));
	
//	SetValueMiniMap(nRow, nCol);


	BOOL bLock = m_wndCellGrid.LockUpdate();
	
	//Row Header Label
//	int nGroupIndex = 0,a,nI;
//	int nCellPos = 0;
	int nGridColCount = m_wndCellGrid.GetColCount();
	int nGridRowCount = m_wndCellGrid.GetRowCount();
	CString strBuff;

	m_wndCellGrid.SelectRange(CGXRange(nRow,nCol),FALSE);		//unselected
	WORD nTrayType = m_pDoc->m_nViewTrayType;
	switch (nTrayType)
	{
	case TRAY_TYPE_LG_COL_ROW:	//가로 번호 체계 
		//nCellPos = ((nRow-1)*(nGridColCount-1)) + (nCol-1); //CTray에 저장 할 셀 위치
		//Grid에서 다음 Row, Col 위치를 찾는다.////
		if (strCellNo == "PNE_BACK")
		{
			nCol--;
			if(nCol < 2)
			{
				nRow--;
				nCol=nGridColCount;
			}
			if(nRow < 1)		
			{
				nRow = 1;		//처음 셀 위치
				nCol = 2;
			}			
			m_wndCellGrid.SelectRange(CGXRange(nRow,nCol));		//selected cell
			m_wndCellGrid.SetCurrentCell(nRow,nCol);
			m_wndCellGrid.LockUpdate(bLock);
			return ;
		}
		else if (strCellNo == "PNE_NEXT")
		{
			nCol++;
			if(nCol >= nGridColCount)
			{
				nRow++;
				nCol=2;
			}
			if(nRow > nGridRowCount)		
			{
				nRow = nGridRowCount;		//마지막 셀 위치
				nCol = nGridColCount;
			}			
			m_wndCellGrid.SelectRange(CGXRange(nRow,nCol));		//selected cell
			m_wndCellGrid.SetCurrentCell(nRow,nCol);
			m_wndCellGrid.LockUpdate(bLock);
			return ;
		}
		m_wndCellGrid.SetValueRange(CGXRange(nRow, nCol), strCellNo);		//입력한 바코드 표시
		if(nCol >= nGridColCount)
		{
			nRow++;
			nCol=2;
		}
		else
		{
			nCol++;
		}
		if(nRow > nGridRowCount)		
		{
			nRow = nGridRowCount;		//마지막 셀 위치
			nCol = nGridColCount;
		}
		break;
	case TRAY_TYPE_PB5_COL_ROW:	//가로 번호 체계(각형)
		//Column header Label
// 		for ( a = 1; a <= m_nTopGridColCount; a++)
// 		{
// 			m_wndCellGrid.SetStyleRange(CGXRange(0, a+1), CGXStyle().SetValue(ROWCOL(a)));
// 		}
// 		
// 		//Row Header Label
// 		for ( nI = 0; nI < m_nTopGridRowCount; nI++)
// 		{
// 			if(nI == 0)
// 			{
// 				if(nI / 26 > 0)		//A~Z 초과시
// 				{
// 					strBuff.Format("%c%c (%3d)", 'A'+nI/26-1, 'A'+ nI%26, nCount++*m_nTopGridColCount+1);
// 				}
// 				else
// 				{
// 					strBuff.Format("%c (%3d)", 'A'+ nI, nCount++*m_nTopGridColCount+1);
// 				}
// 			}
// 			else
// 			{
// 				if(nI / 26 > 0)		//A~Z 초과시
// 				{
// 					strBuff.Format("%c%c (%3d)", 'A'+nI/26-1, 'A'+ nI%26, nCount++*m_nTopGridColCount-1);
// 				}
// 				else
// 				{
// 					strBuff.Format("%c (%3d)", 'A'+ nI, nCount++*m_nTopGridColCount+-1);
// 				}
// 			}
// 			
// 			m_wndCellGrid.SetStyleRange(CGXRange(nI+1, 1), CGXStyle().SetValue(strBuff).SetDraw3dFrame(gxFrameRaised));
// 			if(nCount*m_nTopGridColCount >= EPGetChInGroup(m_nSelModuleID, m_nSelJigNo))
// 			{
// 				nCount = 0;
// 				nGroupIndex++;
// 			}
// 		}		
		break;
	case TRAY_TYPE_PB5_ROW_COL:	//세로 번호 체계(각형)
		//Row header Lable
// 		m_wndCellGrid.SetStyleRange(CGXRange(0, 1), CGXStyle().SetValue("CH(/)"));
// 		
// 		for ( a = 1; a <=m_nTopGridRowCount; a++)
// 		{
// 			m_wndCellGrid.SetStyleRange(CGXRange(a, 1), CGXStyle().SetValue(ROWCOL(a)).SetDraw3dFrame(gxFrameRaised));
// 		}
// 		//Col Header Label
// 		for ( nI = 0; nI < m_nTopGridColCount; nI++)
// 		{
// 			if(nI == 0)
// 			{
// 				if(nI / 26 > 0)		//A~Z 초과시
// 				{
// 					strBuff.Format("%c%c (%3d)", 'A'+nI/26-1, 'A'+ nI%26, nCount++*m_nTopGridRowCount+1);
// 				}
// 				else
// 				{
// 					strBuff.Format("%c (%3d)", 'A'+ nI, nCount++*m_nTopGridRowCount+1);
// 				}
// 			}
// 			else
// 			{
// 				if(nI / 26 > 0)		//A~Z 초과시
// 				{
// 					strBuff.Format("%c%c (%3d)", 'A'+nI/26-1, 'A'+ nI%26, nCount++*m_nTopGridRowCount-1);
// 				}
// 				else
// 				{
// 					strBuff.Format("%c (%3d)", 'A'+ nI, nCount++*m_nTopGridRowCount+-1);
// 				}
// 			}
// 			m_wndCellGrid.SetStyleRange(CGXRange(0, nI+2), CGXStyle().SetValue(strBuff));
// 			if(nCount*m_nTopGridRowCount >= EPGetChInGroup(m_nSelModuleID, m_nSelJigNo))
// 			{
// 				nCount = 0;
// 				nGroupIndex++;
// 			}
// 		}
		break;
	case TRAY_TYPE_CUSTOMER:	//세로 번호 체계
	case TRAY_TYPE_LG_ROW_COL:	//세로 번호 체계
		//nCellPos = ((nCol-2)*(nGridRowCount)) + (nRow-1); //CTray에 저장 할 셀 위치
		//Grid에서 다음 Row, Col 위치를 찾는다.////
		if (strCellNo == "PNE_BACK")
		{
			nRow--;
			if(nRow < 1)
			{
				nCol--;
				nRow=nGridRowCount;
			}
			if(nCol < 2)		
			{
				nRow = 1;		//처음 셀 위치
				nCol = 2;
			}			
			m_wndCellGrid.SelectRange(CGXRange(nRow,nCol));		//selected cell
			m_wndCellGrid.SetCurrentCell(nRow,nCol);
			m_wndCellGrid.LockUpdate(bLock);
			return ;
			
		}
		else if (strCellNo == "PNE_NEXT")
		{
			nRow++;
			if(nRow > nGridRowCount)
			{
				nCol++;
				nRow=1;
			}
			if(nCol > nGridColCount)		
			{
				nRow = nGridRowCount;		//마지막 셀 위치
				nCol = nGridColCount;
			}			
			m_wndCellGrid.SelectRange(CGXRange(nRow,nCol));		//selected cell
			m_wndCellGrid.SetCurrentCell(nRow,nCol);
			m_wndCellGrid.LockUpdate(bLock);
			return ;
		}

		m_wndCellGrid.SetValueRange(CGXRange(nRow, nCol), strCellNo);		//입력한 바코드 표시
		if(nRow >= m_wndCellGrid.GetRowCount())
		{
			nCol++;
			nRow=1;
		}
		else
		{
			nRow++;
		}
		if(nCol > m_wndCellGrid.GetColCount())		//Row가 최대 Row수보다 많으면 DB에 Update한다.
		{
			nRow = nGridRowCount;		//마지막 셀 위치
			nCol = nGridColCount;
		}
		break;		
	}
	m_wndCellGrid.SelectRange(CGXRange(nRow,nCol));		//selected cell
	m_wndCellGrid.SetCurrentCell(nRow,nCol);
	m_wndCellGrid.LockUpdate(bLock);
	//m_wndCellGrid.Redraw();	
	return ;
}

BOOL CTrayInputDlg::Fun_GetTrayAllData(int nModuleID, int nJigID)
{
	CString strTemp;
	CTray	*pTrayData;
	CFormModule *pModule = m_pDoc->GetModuleInfo(nModuleID);
	if(pModule == NULL)	 return FALSE;
	
	pTrayData = pModule->GetTrayInfo(nJigID-1);
	if(pTrayData == NULL)	return FALSE;
	

	for (int i=0; i <= m_nTotalChannelNum; i++)
	{
			strTemp = pTrayData->GetCellSerial(i);
			Fun_SetBarCodeToForcusCell(strTemp);	// Forcus 셀에 BarCode 쓰기 -> 다음 포지현으로 이동 ->  Write 한 nCellNum return
			strTemp.Empty();
//			if (m_bReadCellEnd) break;
	}
//	m_bReadCellEnd = FALSE;
	m_wndCellGrid.Redraw();	
	return TRUE;
}

void CTrayInputDlg::OnBtnInput() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
// 	int nIndex = m_ctrlJigListCombo.GetCurSel();
// 	DWORD dwID = 0;
// 	int nModuleID = 0, nJigID = 0;
// 	if(nIndex < 0)
// 	{
// 		m_strUnitNo.Empty();
// 		return ;
// 	}
// 	else
// 	{
// 		dwID = m_ctrlJigListCombo.GetItemData(nIndex);
// 		nModuleID = HIWORD(dwID);
// 		nJigID = LOWORD(dwID);
// 	}
// 	CString strTemp;
// 	CTray	*pTrayData;
// 	CFormModule *pModule = m_pDoc->GetModuleInfo(nModuleID);
// 	if(pModule == NULL)	 return ;
// 		
// 	pTrayData = pModule->GetTrayInfo(nJigID-1);
// 	if(pTrayData == NULL)	return ;

	Fun_SetBarCodeToForcusCell(m_strCellBCR);
	m_wndCellGrid.Redraw();	
	
}

void CTrayInputDlg::OnBtnAllClear() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	int nRtn = MessageBox(TEXT_LANG[13],TEXT_LANG[14],MB_YESNO);//"TRAY의 모든 셀 정보를 삭제 하시겠습니까"//"삭제 확인"
	if (nRtn == IDNO) return;
	m_wndCellGrid.ClearCells(CGXRange(1,2,m_wndCellGrid.GetRowCount(),m_wndCellGrid.GetColCount()));
	m_wndCellGrid.SetCurrentCell(1,2);	
//	
// 
// 	strTemp="";
// 	for (int i=0; i <= m_nTotalChannelNum; i++)
// 	{
// 		pTrayData->SetCellSerial(i,strTemp);
// 	}
	m_wndCellGrid.Redraw();
// 	Fun_GetTrayAllData(nModuleID,nJigID);
	
}

void CTrayInputDlg::OnBtnCellClear() 
{
	UpdateData();
	int nRtn = MessageBox(TEXT_LANG[15],TEXT_LANG[14],MB_YESNO);//"선택된 셀 정보를 삭제 하시겠습니까"//"삭제 확인"
	if (nRtn == IDNO) return;

// 	int nIndex = m_ctrlJigListCombo.GetCurSel();
// 	DWORD dwID = 0;
// 	int nModuleID = 0, nJigID = 0;
// 	if(nIndex < 0)
// 	{
// 		m_strUnitNo.Empty();
// 		return ;
// 	}
// 	else
// 	{
// 		dwID = m_ctrlJigListCombo.GetItemData(nIndex);
// 		nModuleID = HIWORD(dwID);
// 		nJigID = LOWORD(dwID);
// 	}
// 	CTray	*pTrayData;
// 	CFormModule *pModule = m_pDoc->GetModuleInfo(nModuleID);
// 	if(pModule == NULL)	 return ;
// 	
// 	pTrayData = pModule->GetTrayInfo(nJigID-1);
// 	if(pTrayData == NULL)	return ;

	CRowColArray awRows,awCols;
	ROWCOL nRowCount = m_wndCellGrid.GetSelectedRows(awRows);
	ROWCOL nColCount = m_wndCellGrid.GetSelectedCols(awCols);

	int iPos=0;
	for (ROWCOL nRow = 0; nRow < nRowCount; nRow++)
	{
		for (ROWCOL nCol = 0; nCol < nColCount; nCol++)
		{
			Fun_SetClearCellNum(awRows[nRow],awCols[nCol]);
			TRACE("Row = %ld , Column %ld is selected\n", awRows[nRow],awCols[nCol]);
		}
	}
//	m_wndCellGrid.SetCurrentCell(1,2);
//	Fun_GetTrayAllData(nModuleID,nJigID);
}

BOOL CTrayInputDlg::Fun_SetCellDataToCTray()
{
	CString strData;
	CTray	*pTrayData;
	CFormModule *pModule = m_pDoc->GetModuleInfo(m_nModuleID);
	if(pModule == NULL)	 return FALSE;
	
	pTrayData = pModule->GetTrayInfo(m_nJigID-1);
	if(pTrayData == NULL)	return FALSE;
	
	
	ROWCOL nRowCount = m_wndCellGrid.GetRowCount();
	ROWCOL nColCount = m_wndCellGrid.GetColCount();
	
	int iCh =0;
	for (ROWCOL nRow = 1; nRow <= nRowCount; nRow++)
	{
		for (ROWCOL nCol = 2; nCol <= nColCount; nCol++)
		{
			iCh = Fun_GetCellPosition(nRow,nCol);
			strData = m_wndCellGrid.GetValueRowCol(nRow,nCol);
			pTrayData->SetCellSerial(iCh,strData);
			iCh++;
			TRACE("Row = %ld , Col %ld is %d Position\n", nRow,nCol);
		}
	}
	
	return TRUE;
}

int CTrayInputDlg::Fun_GetCellPosition(ROWCOL nRow, ROWCOL nCol)
{
	int nCellPos=0;
	int nGridColCount = m_wndCellGrid.GetColCount();
	int nGridRowCount = m_wndCellGrid.GetRowCount();

	WORD nTrayType = m_pDoc->m_nViewTrayType;
	switch (nTrayType)
	{
	case TRAY_TYPE_LG_COL_ROW:	//가로 번호 체계 
		nCellPos = ((nRow-1)*(nGridColCount-1)) + (nCol-2); //CTray에 저장 할 셀 위치
		//Grid에서 다음 Row, Col 위치를 찾는다.////
		break;
	case TRAY_TYPE_PB5_COL_ROW:	//가로 번호 체계(각형)
		//Column header Label

		break;
	case TRAY_TYPE_PB5_ROW_COL:	//세로 번호 체계(각형)
		//Row header Lable

		break;
	case TRAY_TYPE_CUSTOMER:	//세로 번호 체계
	case TRAY_TYPE_LG_ROW_COL:	//세로 번호 체계
		nCellPos = ((nCol-2)*(nGridRowCount)) + (nRow-1); //CTray에 저장 할 셀 위치
		//Grid에서 다음 Row, Col 위치를 찾는다.////
		break;		
	}
	return nCellPos;
}
