// ClientSocket.h: interface for the CClientSocket class./////////////
//																	//	
//		Data Client Socket,		Copyright ElicoPower Co., LTD		//
//												Author	 B.H-Kim	//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CLIENTSOCKET_H__CF11FCAA_AD1E_4C51_82D6_8B2AC41CB1C2__INCLUDED_)
#define AFX_CLIENTSOCKET_H__CF11FCAA_AD1E_4C51_82D6_8B2AC41CB1C2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CClientSocket  
{
public:
	BOOL m_bRunThread;
	LPSTR GetClientIP();
//	UINT m_nChNum;
	UINT m_nSystemType;
	UINT m_nSystemID;
	void SetWnd(HWND hWnd);
	BOOL SendMsg(LPVOID pData, size_t nLength);
	void SetIpAddress(LPSTR szIP);
	int StopWork(BOOL bWait = TRUE);
	BOOL StartWork(LPSTR szIP);
	BOOL m_bConnected;
	CClientSocket(LPSTR szIpAddress = NULL);
	virtual ~CClientSocket();

	HANDLE m_hReadEvent;
	HANDLE m_hWriteEvent;
	HANDLE m_hShutDownEvent;
	char m_szServerIpAddress[EP_IP_NAME_LENGTH+1];
	char m_szClientIPAddress[EP_IP_NAME_LENGTH+1];

//	EP_FORMSERVER_VER m_sServer;
	EP_RESPONSE m_CommandAck;

	int m_nRxLength;
	int m_nTxLength;
	char m_szTxBuffer[_EP_TX_BUF_LEN];
	char m_szRxBuffer[_EP_RX_BUF_LEN];
	HWND m_hMsgWnd;

//	LPVOID m_lpOcvBuf;
//	LPVOID m_lpImpBuf;
//	LPVOID m_lpCapBuf;

protected:
	static UINT ClientRun(LPVOID pParam);
	CWinThread * m_pThread;
	BOOL WaitShutdown();
};

#endif // !defined(AFX_CLIENTSOCKET_H__CF11FCAA_AD1E_4C51_82D6_8B2AC41CB1C2__INCLUDED_)
