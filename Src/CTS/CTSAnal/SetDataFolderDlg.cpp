// SetDataFolderDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "SetDataFolderDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

///////////  //////////////////////////////////////////////////////////////////
// CSetDataFolderDlg dialog


CSetDataFolderDlg::CSetDataFolderDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSetDataFolderDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSetDataFolderDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CSetDataFolderDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSetDataFolderDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSetDataFolderDlg, CDialog)
	//{{AFX_MSG_MAP(CSetDataFolderDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSetDataFolderDlg message handlers

BOOL CSetDataFolderDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	char szBuff[512];
	GetCurrentDirectory(511, szBuff);

	CListBox* pLB = (CListBox*)GetDlgItem(IDC_DIRLIST);

	//default data
	CString str = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"Data");		//Get Data Folder
	if(str.IsEmpty())
	{
		str.Format("%s\\Data", szBuff);
	}
//	pLB->AddString(_T(str));

	//additional data folder
	CString strTmp;
	for(int i = 0; i<10; i++)
	{
		strTmp.Format("Data%d", i);
		str = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION , strTmp);
		if(str.IsEmpty() == FALSE)
		{
			pLB->AddString(_T(str));
		}
	}
	
	m_LBDirEditor.Initialize(IDC_DIRLIST, this, LBE_DEFAULT | LBE_BROWSE );
	// m_LBDirEditor.IsEditing();	
	m_LBDirEditor.SetWindowText(_T("&Directories:"));
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSetDataFolderDlg::OnOK() 
{
	// TODO: Add extra validation here
	CListBox* pLB = (CListBox*)GetDlgItem(IDC_DIRLIST);
	CString str, strTmp;
	int nCnt = 0;
	for(int i = 0; i<pLB->GetCount(); i++)
	{
		strTmp.Format("Data%d", nCnt++);
		pLB->GetText(i, str);
		if(str.IsEmpty() == FALSE)
		{
			str.TrimRight("\\");
			AfxGetApp()->WriteProfileString(FORM_PATH_REG_SECTION , strTmp, str);
		}
		else
		{
			AfxGetApp()->WriteProfileString(FORM_PATH_REG_SECTION , strTmp, "");
		}
	}
	CDialog::OnOK();
}
