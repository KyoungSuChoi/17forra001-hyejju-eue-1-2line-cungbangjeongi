#if !defined(AFX_MODELSELDLG_H__63CCF86C_DE4E_4910_BFF5_A371CB5BC6F2__INCLUDED_)
#define AFX_MODELSELDLG_H__63CCF86C_DE4E_4910_BFF5_A371CB5BC6F2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ModelSelDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CModelSelDlg dialog
#include "MyGridWnd.h"
#include "afxwin.h"
#include "StdAfx.h"

class CModelSelDlg : public CDialog
{
// Construction
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	CModelSelDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CModelSelDlg();
	
	BOOL SelectOK();	
	void InitLabel();
	void InitFont();
	void InitGridWnd();
	void InitColorBtn();
	void ChangeTypeValue();	
	void ChangeProcessInfo();
	void OnTestSave(LONG lModelID, LONG lTestId, LONG nProcType, CString strTestName);
	BOOL RequeryBatteryModel(LONG lModelID = 0, bool bSave = false);	// Process List	
	BOOL RequeryTestList(LONG lModelID = 0);		// Type List
	
	BOOL CopyModel(long lFromModelID, long lToModelID);
	BOOL CopyTestList(long lFromModelID, long lToModelID);
	BOOL CopyCheckParam(long lFromModelID, long lToModelID);
	BOOL CopyStepList(long lFromModelID, long lToModelID);
	BOOL CopyGradeStep(long lFromModelID, long lToModelID);
			
	BOOL DeleteTypeList( long lType);
	BOOL DeleteProcess( long lType, long lProcess);

public:
	LONG m_nSelectType;	
	LONG m_nSelectProcess;
	LONG m_nType[10];
	LONG m_nProcess[20];
// Dialog Data
	//{{AFX_DATA(CModelSelDlg)
	enum { IDD = IDD_MODEL_DIALOG };
	CLabel	m_LabelTypeNote;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CModelSelDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//�� ���� ��ư	
	CMyGridWnd m_wndBatteryModelGrid;
	CMyGridWnd m_wndSelectTypeGrid;
	CWnd *m_pView;

	// Generated message map functions
	//{{AFX_MSG(CModelSelDlg)
	virtual BOOL OnInitDialog();	
	afx_msg void OnButtonDelete();	
	afx_msg void OnButtonCopy();
	afx_msg void OnButtonSave();	
	virtual void OnCancel();
	
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	afx_msg LONG OnGridDbClicked(WPARAM wParam, LPARAM lParam);
	afx_msg LONG OnGridClicked(WPARAM wParam, LPARAM lParam);
public:
	afx_msg void OnBnClickedOk();
	CLabel m_Label1;
	afx_msg void OnBnClickedCancel();
	CLabel m_LabelSelectType;
	CLabel m_Label2;
	afx_msg void OnBnClickedButtonBack();
	afx_msg void OnBnClickedButtonNext();
	afx_msg void OnBnClickedButtonReset();	
	CString m_strTypeNote;	
	CColorButton2 m_Btn_Back;
	CColorButton2 m_Btn_Next;
	CColorButton2 m_Btn_Copy;
	CColorButton2 m_Btn_Save;
	CColorButton2 m_Btn_Reset;
	CColorButton2 m_Btn_Cancel;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MODELSELDLG_H__63CCF86C_DE4E_4910_BFF5_A371CB5BC6F2__INCLUDED_)
