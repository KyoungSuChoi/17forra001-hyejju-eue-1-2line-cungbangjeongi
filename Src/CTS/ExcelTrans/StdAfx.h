// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__728BFD7B_5FC5_4E70_925D_6A4FDC3B47A1__INCLUDED_)
#define AFX_STDAFX_H__728BFD7B_5FC5_4E70_925D_6A4FDC3B47A1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include "../../../Include/Formall.h"
#include "../../../Include/IniParser.h"

#ifdef _DEBUG
#pragma comment(lib, "../../../Lib/BFCommonD.lib")
#pragma message("Automatically linking with BFCommonD.lib By K.B.H ")
#else
#pragma comment(lib, "../../../Lib/BFCommon.lib")
#pragma message("Automatically linking with BFCommon.lib By K.B.H")
#endif	//_DEBUG


#include "../../UtilityClass/FormUtility.h"
#define MAX_EXCEL_COLUMN	256

extern int g_nLanguage;			
extern CString g_strLangPath;
enum LanguageType {LANGUAGE_KOR=0, LANGUAGE_ENG, LANGUAGE_CHI };

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__728BFD7B_5FC5_4E70_925D_6A4FDC3B47A1__INCLUDED_)
