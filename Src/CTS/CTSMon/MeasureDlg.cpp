// MeasureDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSMon.h"
#include "serialport.h"
#include "MeasureDlg.h"
// #include "MeasureSetDlg.h"
#include "MeasurePointSetDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMeasureDlg dialog


CMeasureDlg::CMeasureDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMeasureDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMeasureDlg)
	m_strFileName	= _T("test.csv");
	m_strChSelect	= _T("");
	m_nOptSelectChannel = -1;
	m_uiRecordTime = 5;
	m_nInterval		= 2000;
	m_nMeasureCount = 0;
	//}}AFX_DATA_INIT
	m_nCount		= 0;
	m_nModuleID		= 0;
	m_nChannelIndex = 0;

	m_bVCommOpen = FALSE;
	m_bICommOpen = FALSE;

	m_bAuto		= TRUE;
	m_nVPort	= 1;
	m_nIPort	= 2;
	m_fShunt	= 1.0f;	

//	m_RemainRepeatNum	= 0;
//	m_bCheckReceiveReadyFlag = FALSE;
//	m_nTotalCycleNum	= 0;
//	m_nRemainChNum		= 0;
	m_HeartBeat			= 0;
	m_bReceiveDataFlag	= FALSE;
	m_bExit = FALSE;
//	m_bPause = FALSE;
	LanguageinitMonConfig();
}

CMeasureDlg::~CMeasureDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CMeasureDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CMeasureDlg"), _T("TEXT_CMeasureDlg_CNT"), _T("TEXT_CMeasureDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CMeasureDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CMeasureDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}



void CMeasureDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMeasureDlg)
	DDX_Control(pDX, IDC_MEASURETYPE_SEL_COMBO, m_MeasureTypeSel);
	DDX_Control(pDX, IDC_MODULE_SEL_COMBO, m_ModuleList);
	DDX_Control(pDX, IDC_DATA_LIST, m_List);
	DDX_Text(pDX, IDC_EDIT2, m_strFileName);
	DDX_Text(pDX, IDC_CH_SELECT_EDIT, m_strChSelect);
	DDX_Radio(pDX, IDC_OPT_ALL_CHANNEL, m_nOptSelectChannel);
	DDX_Text(pDX, IDC_EDIT_RECORD_TIME, m_uiRecordTime);
	DDV_MinMaxUInt(pDX, m_uiRecordTime, 1, 600);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_RANGE_COMBO, m_ctrlRange);
}


BEGIN_MESSAGE_MAP(CMeasureDlg, CDialog)
	//{{AFX_MSG_MAP(CMeasureDlg)
	ON_BN_CLICKED(IDC_FILE_NAME, OnFileName)
	ON_BN_CLICKED(IDC_CLEAR, OnClear)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_START, OnStart)
	ON_BN_CLICKED(IDC_STOP, OnStop)
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_OPTION, OnOption)
	ON_CBN_SELCHANGE(IDC_MODULE_SEL_COMBO, OnSelchangeModuleSelCombo)
	ON_BN_CLICKED(IDC_SELECT_MEASURE_OPTION, OnSelectMeasureOptionManual)
	ON_BN_CLICKED(IDC_SELECT_MEASURE_OPTION_AUTO, OnSelectMeasureOptionAuto)
	ON_BN_CLICKED(IDC_OPT_ALL_CHANNEL, OnOptAllChannel)
	ON_BN_CLICKED(IDC_OPT_PART_CHANNEL, OnOptPartChannel)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, &CMeasureDlg::OnBnClickedOk)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMeasureDlg message handlers

void CMeasureDlg::OnFileName() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

	CFileDialog pDlg(FALSE, "", m_strFileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "Excel File(*.csv)|*.csv|");
	if(IDOK == pDlg.DoModal())
	{
		m_strFileName = pDlg.GetPathName();
		AfxGetApp()->WriteProfileString("Calibration", "MeasureResultDataSavePath", m_strFileName);
	}
	GetDlgItem(IDC_EDIT2)->SetWindowText(m_strFileName);
	UpdateData(FALSE);
}

void CMeasureDlg::OnClear() 
{
	// TODO: Add your control notification handler code here
	ResetData();
}

BOOL CMeasureDlg::ResetData()
{
	m_List.DeleteAllItems();
	m_nCount = 0;
//	unlink(m_strFileName);
	return TRUE;
}

void CMeasureDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	// 반복 횟수 만큼 실행
	// m_RemainRepeatNum = m_RemainRepeatNum - 1;

	switch( nIDEvent )
	{
	case 100:
		// 2012_01_13 kky0703 실측 값 교정 기능 추가로 시간 수정 10 => 30
		if( m_bReceiveDataFlag == FALSE )
		{
			if( m_HeartBeat == 60 )
			{	
				KillTimer(nIDEvent);

				m_HeartBeat = 0;

				GetDlgItem(IDC_START)->EnableWindow(TRUE);
				GetDlgItem(IDC_STOP)->EnableWindow(FALSE);

				AfxMessageBox(TEXT_LANG[0]);//"데이터가 수신되고 있지 않습니다. 네트워크 상태를 확인 하세요 !"
			}
			else
			{
				m_HeartBeat++;
			}
			
		}
		else
		{
			KillTimer(nIDEvent);
		}	
		break;
	}	
	CDialog::OnTimer(nIDEvent);
}

BOOL CMeasureDlg::bFileFinder(CString str_path)
{
	BOOL bWork;
	CFileFind finder;
    bWork=finder.FindFile(str_path);		
	finder.Close();

    return bWork;
}

BOOL CMeasureDlg::SendChDataToSbc()
{
	CString strLog;
	CString strTemp;
	int nRtn = 0;
	int nCh,i;
	POSITION pos;
	CString strCh;

	EP_REAL_MEASURE_START_INFO selInfo;
	ZeroMemory(&selInfo, sizeof(EP_REAL_MEASURE_START_INFO));

	selInfo.nType = m_MeasureTypeSel.GetCurSel();
	
	selInfo.measure_range = m_ctrlRange.GetCurSel();
	
	selInfo.wRecordTime = m_uiRecordTime;

	if (m_nOptSelectChannel == 0)
	{
		//전채널 선택
		for( i=0; i< EP_MAX_CH_PER_MD; i++ )	selInfo.bMonChNum[i] = 1;
	}
	else
	{
		//부분 채널 선택
		pos = m_ChList.GetHeadPosition();
		for( i=0; i<m_ChList.GetCount(); i++ )
		{
			nCh = m_ChList.GetNext(pos);
			selInfo.bMonChNum[nCh-1] = 1;
		}
		//m_pos = m_ChList.GetHeadPosition();	// 저장된 채널정보의 처음 위치
	}

	// 1. 측정 포인트 설정부분
	for( i=0; i<m_pMeasPoint.GetVMeasureCnt((RangeType)selInfo.measure_range); i++ )
	{
		selInfo.lVoltageSetVal[i] = (long)(m_pMeasPoint.GetVData(i, (RangeType)selInfo.measure_range)*1000.0f);
	}

	for( i=0; i<m_pMeasPoint.GetIMeasureCnt((RangeType)selInfo.measure_range); i++ )
	{
		selInfo.lCurrentSetVal[i] = (long)(m_pMeasPoint.GetIData(i, (RangeType)selInfo.measure_range)*1000.0f);
	}	

	for(i =0; i<CAL_MAX_BOARD_NUM; i++)
	{
		selInfo.lCaliShuntR[i] = m_pMeasPoint.GetShuntR(i)*10000.0f;
		selInfo.lCaliShuntT[i] = m_pMeasPoint.GetShuntT(i)*10000.0f;
		sprintf(&selInfo.byCaliShuntSerail[i][0], "%s", m_pMeasPoint.GetShuntSerial(i));
	}

	if((nRtn = EPSendCommand( m_nModuleID, 0, 0, EP_CMD_RM_START, &selInfo, sizeof(EP_REAL_MEASURE_START_INFO)))!= EP_ACK)
	{
		AfxMessageBox(TEXT_LANG[1]);//"[측정 시작] 명령 전송을 실패하였습니다."
		return FALSE;
	}

	// EPSendCommand(m_nModuleID);
	m_bReceiveDataFlag = FALSE;
	m_HeartBeat = 0;

	return TRUE;

//	strLog.Format("실측측정명령전송 ======> 모니터링->SBC 모듈:%d, TYPE:%d, CH:%d", m_nModuleID, selInfo.nType, selInfo.nMonChNum);
//	TRACE( strLog );	
}

BOOL CMeasureDlg::UpdateResultDataOnList( EP_REAL_MEASURE_RESULT_DATA * pRealMeasureData )
{
	m_bReceiveDataFlag = TRUE;

	EP_REAL_MEASURE_RESULT_DATA RealMeasureResultData;
	memcpy(&RealMeasureResultData, pRealMeasureData, sizeof(EP_REAL_MEASURE_RESULT_DATA));		

	BOOL	filefined = FALSE;
	CString SetAD_V;
	CString SetAD_I;
	CString SetData4;
	CString SetData5;
			
	CString SetData8;
	CString SetData9;
	CString SetData10;
	CString SetData11;
	
	CString strResultData;

	CString str;
	CString buf;
	COleDateTime datetime = COleDateTime::GetCurrentTime();

	int count = m_List.GetItemCount();
	float tempVal = 0.0f;
	
	CString strTemp = _T("");

	buf.Format(_T("%3d"), ++m_nCount);
	m_List.InsertItem(LVIF_TEXT|LVIF_STATE, count, buf, LVIS_SELECTED, LVIS_SELECTED, 0, 0);

	buf.Format("%s_CH%d", GetModuleName(m_nModuleID), RealMeasureResultData.nMonCh );
	m_List.SetItem(count, 1, LVIF_TEXT|LVIF_STATE, buf, 0, LVIS_SELECTED, LVIS_SELECTED, 0 );

	buf.Format("%d", RealMeasureResultData.nMeasureCnt );
	m_List.SetItem(count, 2, LVIF_TEXT|LVIF_STATE, buf, 0, LVIS_SELECTED, LVIS_SELECTED, 0 );

	if( RealMeasureResultData.nType == 0 )
	{
		str.Format("%s", m_pDoc->ValueString(RealMeasureResultData.lSetVal/1000.0f, EP_VOLTAGE) );
		SetAD_V = str;
		m_List.SetItem(count, 3, LVIF_TEXT|LVIF_STATE, str, 0, LVIS_SELECTED, LVIS_SELECTED, 0 );
		 
		str.Format("%s", m_pDoc->ValueString(RealMeasureResultData.lDMM/1000.0f, EP_VOLTAGE) );
		SetData4 = str;
		m_List.SetItem(count, 4, LVIF_TEXT|LVIF_STATE, str, 0, LVIS_SELECTED, LVIS_SELECTED, 0 );

		str.Format("%s", m_pDoc->ValueString((RealMeasureResultData.lSetVal/1000.0f)-(RealMeasureResultData.lDMM/1000.0f), EP_VOLTAGE ) );
		SetData5 = str;		
		m_List.SetItem(count, 5, LVIF_TEXT|LVIF_STATE, str, 0, LVIS_SELECTED, LVIS_SELECTED, 0 );

		/* 
		str.Format("%s", m_pDoc->ValueString(RealMeasureResultData.lAD/1000.0f, EP_VOLTAGE) );
		SetData5 = str;
		m_List.SetItem(count, 5, LVIF_TEXT|LVIF_STATE, str, 0, LVIS_SELECTED, LVIS_SELECTED, 0 );
		 
		str.Format("%s", m_pDoc->ValueString((RealMeasureResultData.lDMM/1000.0f)-(RealMeasureResultData.lAD/1000.0f), EP_VOLTAGE ) );
		SetData6 = str;		
		m_List.SetItem(count, 6, LVIF_TEXT|LVIF_STATE, str, 0, LVIS_SELECTED, LVIS_SELECTED, 0 );
		*/
	}
	// else if( RealMeasureResultData.nType == 1 )	//  전류
	else
	{
		str.Format("%s", m_pDoc->ValueString(RealMeasureResultData.lSetVal/1000.0f, EP_CURRENT) );
		SetAD_I = str;
		m_List.SetItem(count, 6, LVIF_TEXT|LVIF_STATE, str, 0, LVIS_SELECTED, LVIS_SELECTED, 0 );
		
		str.Format("%s", m_pDoc->ValueString(RealMeasureResultData.lDMM/1000.0f, EP_CURRENT) );
		SetData8 = str;
		m_List.SetItem(count, 7, LVIF_TEXT|LVIF_STATE, str, 0, LVIS_SELECTED, LVIS_SELECTED, 0 );
		
		str.Format("%s", m_pDoc->ValueString((RealMeasureResultData.lSetVal/1000.0f)-(RealMeasureResultData.lDMM/1000.0f), EP_CURRENT) );
		SetData9 = str;
		m_List.SetItem(count, 8, LVIF_TEXT|LVIF_STATE, str, 0, LVIS_SELECTED, LVIS_SELECTED, 0 );
		/*
		str.Format("%s", m_pDoc->ValueString(RealMeasureResultData.lAD/1000.0f, EP_CURRENT) );
		SetData9 = str;
		m_List.SetItem(count, 9, LVIF_TEXT|LVIF_STATE, str, 0, LVIS_SELECTED, LVIS_SELECTED, 0 );

		str.Format("%s", m_pDoc->ValueString((RealMeasureResultData.lDMM/1000.0f)-(RealMeasureResultData.lAD/1000.0f), EP_CURRENT) );
		SetData10 = str;
		m_List.SetItem(count, 10, LVIF_TEXT|LVIF_STATE, str, 0, LVIS_SELECTED, LVIS_SELECTED, 0 );
		*/
	}

	CFormModule		*pModule;
	_MAPPING_DATA	*pMapData;
	EP_GP_DATA gpData = EPGetGroupData(m_nModuleID, 0);

	str.Format("%.1f", (float)gpData.sensorData.sensorData1[6].lData/100.0f );
	SetData10 = str;
	m_List.SetItem(count, 9, LVIF_TEXT|LVIF_STATE, str, 0, LVIS_SELECTED, LVIS_SELECTED, 0 );

	str.Format("%.1f", (float)gpData.sensorData.sensorData1[7].lData/100.0f );
	SetData11 = str;
	m_List.SetItem(count, 10, LVIF_TEXT|LVIF_STATE, str, 0, LVIS_SELECTED, LVIS_SELECTED, 0 );


	if( count > MAX_LIST_VALUE )
	{
		m_List.DeleteItem(0);
	}

	if(m_strFileName.IsEmpty())	
		return FALSE;
	
	if( bFileFinder(m_strFileName) )
	{
		filefined = TRUE;		
	}
	
	FILE *fp;
	fp = fopen((LPSTR)(LPCTSTR)m_strFileName, "at+");
	if(fp == NULL)	
	{
		AfxMessageBox(TEXT_LANG[2]);//"파일 쓰기 에러 발생"
		return FALSE;
	}

	if( filefined == FALSE )
	{
		fprintf(fp,"Module, Chennel, Measuring Count, Voltage Setting value, Measuring Voltage, Voltage measurement tolerance, Current setting value, Measuring current, Current measurement tolerance, Temp1, Temp2\n");
		fprintf(fp,"%s,CH%d,%d,%s,%s,%s,%s,%s,%s,%s,%s\n", 
			// GetModuleName(m_nModuleID), RealMeasureResultData.nMonCh , datetime.Format("%m/%d_%H:%M:%S"), SetAD_I, SetData4, SetData5, SetData6);
			GetModuleName(m_nModuleID), RealMeasureResultData.nMonCh , RealMeasureResultData.nMeasureCnt, SetAD_V, SetData4, SetData5, SetAD_I, SetData8, SetData9, SetData10, SetData11);
	}
	else
	{
		fprintf(fp,"%s,CH%d,%d,%s,%s,%s,%s,%s,%s,%s,%s\n",
			// GetModuleName(m_nModuleID), RealMeasureResultData.nMonCh , datetime.Format("%m/%d_%H:%M:%S"), SetAD_I, SetData4, SetData5, SetData6);
			GetModuleName(m_nModuleID), RealMeasureResultData.nMonCh , RealMeasureResultData.nMeasureCnt, SetAD_V, SetData4, SetData5, SetAD_I, SetData8, SetData9, SetData10, SetData11);
	}
	
	fclose(fp);
	UpdateData(FALSE);
	
	return TRUE;	
}

BOOL CMeasureDlg::InitList()
{
	CRect rect;
	m_List.GetClientRect(&rect);
	int nColInterval = (rect.Width()-210)/8;

	m_List.SetRedraw(FALSE);
	m_List.InsertColumn(0, _T("No"), LVCFMT_LEFT, 50);
	m_List.InsertColumn(1, _T("Channel"), LVCFMT_LEFT, 80);
	m_List.InsertColumn(2, _T(TEXT_LANG[4]), LVCFMT_LEFT, 80);//"측정횟수"
 	m_List.InsertColumn(3, _T(TEXT_LANG[5]), LVCFMT_LEFT, nColInterval);//"전압설정값"
 	m_List.InsertColumn(4, _T(::GetStringTable(IDS_LABEL_METER_VOLTAGE)), LVCFMT_LEFT, nColInterval);
 	// m_List.InsertColumn(5, _T(::GetStringTable(IDS_LABEL_AD_VOLTAGE)), LVCFMT_LEFT, nColInterval);
 	m_List.InsertColumn(5, _T(::GetStringTable(IDS_LABEL_V_MEASURE_ERROR)), LVCFMT_LEFT, nColInterval);
	m_List.InsertColumn(6, _T(TEXT_LANG[6]), LVCFMT_LEFT, nColInterval);//"전류설정값"
	m_List.InsertColumn(7, _T(::GetStringTable(IDS_LABEL_METER_CURRENT)), LVCFMT_LEFT, nColInterval);
	// m_List.InsertColumn(9, _T(::GetStringTable(IDS_LABEL_AD_CURRENT)), LVCFMT_LEFT, nColInterval);
	m_List.InsertColumn(8, _T(::GetStringTable(IDS_LABEL_I_MEASURE_ERROR)), LVCFMT_LEFT, nColInterval);
	m_List.InsertColumn(9, _T("Temp7"), LVCFMT_LEFT, nColInterval);
	m_List.InsertColumn(10, _T("Temp8"), LVCFMT_LEFT, nColInterval);
	m_List.ModifyStyle(LVS_ICON | LVS_LIST | LVS_REPORT | LVS_SMALLICON , LVS_REPORT);
	//m_List.ModifyStyle(0, LVS_REPORT);
	m_List.SetRedraw(TRUE);
	m_List.DeleteAllItems();
	
	return TRUE;
}

BOOL CMeasureDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_nOptSelectChannel = 0;	//ljb 2011519 채널선택 모드
	GetDlgItem(IDC_CH_SELECT_EDIT)->EnableWindow(FALSE);	//ljb 2011519 

//	m_strModuleName = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Module Name", "Rack");	
//	m_strGroupName = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Group Name", "Group");	
//	m_nModulePerRack = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION , "Module Per Rack", 3);
//	m_bUseRackIndex = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION , "Use Rack Index", TRUE);
//	m_bUseGroupSet = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION , "Use Group", FALSE);

	// [8/28/2009 경여니]
	// MeasureDlg 위치 설정
	CRect dlgRect;
	dlgRect.left	= 200;	
	dlgRect.top		= 100; 
	CRect winRect;
	this->GetClientRect(&winRect);
	dlgRect.right = dlgRect.left + winRect.Width();
	dlgRect.bottom = dlgRect.top + winRect.Height();
	::SetWindowPos(m_hWnd, HWND_NOTOPMOST, dlgRect.left, dlgRect.top, dlgRect.Width(), dlgRect.Height(), SWP_SHOWWINDOW);	
	
	CString strTemp;
	int nModuleID;
	for(int i =0; i<EPGetInstalledModuleNum(); i++)
	{
		nModuleID = EPGetModuleID(i);
		strTemp.Format("%s", GetModuleName(nModuleID));
		m_ModuleList.AddString(strTemp);
		m_ModuleList.SetItemData(i, nModuleID);
	}

	m_ModuleList.SetCurSel(m_nModuleID-1);	
	m_ModuleList.GetItemData(m_nModuleID-1);
	
	m_ctrlRange.SetCurSel( AfxGetApp()->GetProfileInt(REG_CAL_SECTION, "MeasureRange", 0) );

	m_MeasureTypeSel.SetCurSel(0);

	m_strFileName = AfxGetApp()->GetProfileString("Calibration", "MeasureResultDataSavePath", "");

	InitList();


	UINT nSize;
	LPVOID* pData;

	AfxGetApp()->GetProfileBinary(REG_CAL_SECTION, "MeasurePoint", (LPBYTE *)&pData, &nSize);
	if( nSize > 0 )
	{
		memcpy(&m_pMeasPoint, pData, nSize);
	}
	else
	{
		memset(&m_pMeasPoint, 0, sizeof(CMeasurePoint));
	}
	delete [] pData;

	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

/*
inline CString CMeasureDlg::GetModuleName(int nModuleID, int nGroupIndex)
{
	CString strName;
	
	if(m_bUseRackIndex)
	{
		div_t div_result;
		div_result = div( nModuleID-1, m_nModulePerRack );

		if(m_bUseGroupSet && nGroupIndex >= 0)
		{
			strName.Format("%s %d-%d, %s %d", m_strModuleName, div_result.quot+1, div_result.rem+1, m_strGroupName, nGroupIndex+1);
		}
		else
		{
			strName.Format("%s %d-%d", m_strModuleName, div_result.quot+1, div_result.rem+1);
		}
	}
	else
	{
		if(m_bUseGroupSet && nGroupIndex >= 0)
		{
			strName.Format("%s %d, %s %d", m_strModuleName, nModuleID,  m_strGroupName, nGroupIndex+1);
		}
		else
		{
			strName.Format("%s %d", m_strModuleName, nModuleID);
		}
	}
	return strName;
}
*/

float CMeasureDlg::GetDVMData(int nType)
{
	CString buf;
	buf.Format("READ?\n");

	if(nType == EP_VOLTAGE && m_bVCommOpen)
	{
		m_PortV.WriteToPort((LPSTR)(LPCTSTR)buf);	
		buf = m_PortV.ReadString();
	}
	else if(nType == EP_CURRENT && m_bICommOpen)
	{
		m_PortI.WriteToPort((LPSTR)(LPCTSTR)buf);	
		buf = m_PortI.ReadString();
	}
	
	return (float)atof((LPSTR)(LPCTSTR)buf);
}

float CMeasureDlg::GetADVal(int nType)
{
	float data = 0.0f;
	data = EPGetChannelValue(m_nModuleID, 0, m_nChannelIndex, nType);
	return data;
}

BOOL CMeasureDlg::GetSelectdChNum()
{
	UpdateData(TRUE);
	
	if( m_strChSelect.IsEmpty() )
	{	return FALSE;	}

	int p1=0, p2=0, p3=0;
	
	m_ChList.RemoveAll();
			
	while(TRUE)
	{
		p2=m_strChSelect.Find(',', p1);
		p3=m_strChSelect.Find('-', p1);
		
		if(p2!=-1 && p3==-1)
		{
			long cyc = atoi(m_strChSelect.Mid(p1,p2-p1));
			if(m_ChList.Find(cyc)==NULL) m_ChList.AddTail(cyc);			
		}
		else if(p2!=-1 && p3!=-1)
		{
			if( p2 < p3 )
			{
				long cyc = atoi(m_strChSelect.Mid(p1,p2-p1));
				if(m_ChList.Find(cyc)==NULL) m_ChList.AddTail(cyc);
			}
			else if(p3<p2)
			{
				long from = atoi(m_strChSelect.Mid(p1, p3-p1));
				long to   = atoi(m_strChSelect.Mid(p3+1, p2-p3-1));
				for(long cyc=from; cyc<=to; cyc++)
				{
					if(m_ChList.Find(cyc)==NULL) m_ChList.AddTail(cyc);
				}
			}
		}
		else if(p2==-1 && p3==-1)
		{
			CString str = m_strChSelect.Mid(p1);
			if(!str.IsEmpty())
			{
				long cyc = atoi(str);
				if(m_ChList.Find(cyc)==NULL) m_ChList.AddTail(cyc);
			}
			break;
		}
		else if(p2==-1 && p3!=-1)
		{
			long from = atoi(m_strChSelect.Mid(p1, p3-p1));
			long to   = atoi(m_strChSelect.Mid(p3+1));
			for(long cyc=from; cyc<=to; cyc++)
			{
				if(m_ChList.Find(cyc)==NULL) m_ChList.AddTail(cyc);
			}
			break;
		}
		p1=p2+1;
	}
	return TRUE;
}

void CMeasureDlg::OnStart() 
{
	// TODO: Add your control notification handler code here	
	UpdateData();
	
	CString strID; 
	m_ModuleList.GetLBText( m_ModuleList.GetCurSel(), strID);
	m_nModuleID = m_ModuleList.GetCurSel() + 1;
	
	if( m_nModuleID < 1)
	{
		MessageBox("Invalid input value", "ID Error", MB_OK|MB_ICONSTOP);
		return;
	}

	if( m_nOptSelectChannel == 1 && GetSelectdChNum() == FALSE )
	{
		AfxMessageBox("No Selected Channel");
		return;
	}

	ResetData();		//ljb 2011519 그리드 초기화

	if (SendChDataToSbc())	//ljb 2011519 SBC로 명령 전송
	{
		m_bExit = TRUE;
		m_HeartBeat = 0;
		GetDlgItem(IDC_START)->EnableWindow(FALSE);
		GetDlgItem(IDC_STOP)->EnableWindow(TRUE);
		SetTimer(100, TIMER_SENDCHNUM_INTERVAL, NULL);
	}

	UpdateData(FALSE);
}

//DEL VOID CMeasureDlg::InitData()
//DEL {	
//DEL //	m_bPause = FALSE;		// PAUSE 상태 유무
//DEL 	m_bCheckReceiveReadyFlag = TRUE;	// 저장 플래그
//DEL 	m_pos = m_ChList.GetHeadPosition();	// 저장된 채널정보의 처음 위치
//DEL //	m_nRemainChNum = m_ChList.GetCount();
//DEL //	m_strSumChNum.Format("%d",m_ChList.GetCount());	
//DEL 
//DEL //	m_strRemainChNum.Format("%d",m_nRemainChNum);
//DEL //	m_nTotalCycleNum = m_nRepeatNum;
//DEL //	m_strRemainCycleNum.Format("%d", m_nTotalCycleNum );
//DEL 
//DEL 	GetDlgItem(IDC_START)->EnableWindow(FALSE);
//DEL 	GetDlgItem(IDC_STOP)->EnableWindow(TRUE);
//DEL }


void CMeasureDlg::OnStop() 
{
	// TODO: Add your control notification handler code here	
	m_bExit = FALSE;

	if(EPSendCommand( m_nModuleID, 0, 0, EP_CMD_RM_STOP) != EP_ACK)
	{
		AfxMessageBox(TEXT_LANG[7]);//"[정지] 명령 전송을 실패하였습니다."
		return;
	}
}


void CMeasureDlg::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	if (m_bExit == TRUE)
	{
		AfxMessageBox(TEXT_LANG[8]);//"작업 중 입니다. 작업 완료후 종료 하세요. "
		return;
	}
	
	CDialog::OnClose();
}

void CMeasureDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here
	KillTimer(100);
	if(m_bVCommOpen)
	{
		m_PortV.DisConnect();
	}
	if(m_bICommOpen)
	{
		m_PortI.DisConnect();
	}
}

void CMeasureDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	CRect ctrlRect;
	RECT rect;
	::GetClientRect(m_hWnd, &rect);
	
	if(m_List.GetSafeHwnd())
	{
		m_List.GetWindowRect(&ctrlRect);
		ScreenToClient(&ctrlRect);
		m_List.MoveWindow(ctrlRect.left, ctrlRect.top, rect.right-ctrlRect.left-10, rect.bottom - ctrlRect.top-10, FALSE);
		Invalidate();
	}
}

void CMeasureDlg::OnOption() 
{
	// TODO: Add your control notification handler code here
	CMeasurePointSetDlg dlg( &m_pMeasPoint );

	if( dlg.DoModal() == IDOK )
	{

	}
}	

void CMeasureDlg::OnSelchangeModuleSelCombo() 
{
	// TODO: Add your control notification handler code here

	int nIndex =  m_ModuleList.GetCurSel();
	if(nIndex >=0)
	{
		int nModuleID = m_ModuleList.GetItemData(nIndex);

		if( ::EPGetProtocolVer(nModuleID) < 0x1001)				//version check
		{
			CString strTemp;
			strTemp.Format(::GetStringTable(IDS_MSG_NOT_SUPPORT_VERSION), ::GetModuleName(nModuleID),  ::EPGetProtocolVer(nModuleID));
			AfxMessageBox(strTemp);
			
			//원래 상태로 되돌린다.
			for(int i=0; m_ModuleList.GetCount(); i++)
			{
				if(m_nModuleID == m_ModuleList.GetItemData(i))
				{
					m_ModuleList.SetCurSel(i);
				}
			}
			return;	
		}
		else
		{
			m_nModuleID = nModuleID;
		}
	}

	fnUpdatePointData( m_nModuleID );
}

// void CMeasureDlg::OnSelchangeChannelSelCombo() 
// {
// 	// TODO: Add your control notification handler code here
// 	m_nChannelIndex = m_ChannelList.GetCurSel();	
// }

void CMeasureDlg::OnSelectMeasureOptionManual() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);		
	
}

void CMeasureDlg::OnSelectMeasureOptionAuto() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
}

BOOL CMeasureDlg::InitComm()
{
	CString buf;

	if(m_nVPort > 0)
	{
		BOOL flag = m_PortV.InitPort(this,
					m_nVPort, 
					9600,
					'E',
					7,
					2,
					EV_CTS | EV_BREAK | EV_ERR | EV_RXCHAR | EV_RXFLAG,
					512
				);

		if( flag == FALSE )
		{
			buf.Format("Can't open voltage measure port %d.", m_nVPort);
			AfxMessageBox(buf);
			m_bVCommOpen = FALSE;
		}
		else
		{
		//	m_strReceived.Empty();
			m_PortV.StartMonitoring("\r\n");
			
			
			buf.Format("SYST:REM\n");
			m_PortV.WriteToPort((LPCTSTR)buf);
			Sleep(200);

		//	buf.Format("CONF:VOLT:DC DEF, DEF\n");
		//	m_Port.WriteToPort((LPCTSTR)buf);
		//	Sleep(200);

			buf.Format("SAMPLE:COUNT 1\n");
			m_PortV.WriteToPort((LPCTSTR)buf);
			m_bVCommOpen = TRUE;
		}
	}

	if(m_nIPort > 0)
	{

		BOOL flag = m_PortI.InitPort(this,
					m_nIPort, 
					9600,
					'E',
					7,
					2,
					EV_CTS | EV_BREAK | EV_ERR | EV_RXCHAR | EV_RXFLAG,
					512
				);

		if( flag == FALSE )
		{
			buf.Format("Can't open current measure port %d.", m_nIPort);
			AfxMessageBox(buf);
			m_bICommOpen = FALSE;
		}
		else
		{
		//	m_strReceived.Empty();
			m_PortI.StartMonitoring("\r\n");
			
			buf.Format("SYST:REM\n");
			m_PortI.WriteToPort((LPCTSTR)buf);
			Sleep(200);

		//	buf.Format("CONF:VOLT:DC DEF, DEF\n");
		//	m_Port.WriteToPort((LPCTSTR)buf);
		//	Sleep(200);

			buf.Format("SAMPLE:COUNT 1\n");
			m_PortI.WriteToPort((LPCTSTR)buf);
			m_bICommOpen = TRUE;	
		}
	}
	return (m_bVCommOpen | m_bICommOpen);
}

BOOL CMeasureDlg::AddLog()
{
	CString buf;
	COleDateTime datetime = COleDateTime::GetCurrentTime();

//	LVITEM lvi;
//	ZeroMemory(&lvi, sizeof(LVITEM));

//	lvi.cchTextMax = 64;
//	lvi.mask =  LVIF_TEXT;
//	lvi.state = LVIS_SELECTED;
//	lvi.iItem = m_nCount;

	//Count
//	lvi.iSubItem = 0;
//	buf.Format(_T("%-3d"), ++m_nCount);
//	lvi.pszText = (LPTSTR)(LPCTSTR)(buf);
//	m_List.InsertItem(&lvi);
//	lvi.iItem = count;
//	lvi.cchTextMax =64;
//	lvi.mask =  LVIF_TEXT|LVIF_STATE;
//	lvi.state = LVIS_SELECTED;

	int count =  m_List.GetItemCount();
	float data =0.0f;
	float data1 =0.0f;
	float data2 =0.0f;
	float data3 =0.0f;

	buf.Format(_T("%3d"), ++m_nCount);
	m_List.InsertItem(LVIF_TEXT|LVIF_STATE, count, buf, LVIS_SELECTED, LVIS_SELECTED, 0, 0);

	buf.Format("%s-CH%d", GetModuleName(m_nModuleID), m_nChannelIndex+1);
	m_List.SetItem(count, 1, LVIF_TEXT|LVIF_STATE, buf, 0, LVIS_SELECTED, LVIS_SELECTED, 0 );

	buf = datetime.Format("%m/%d %H:%M:%S");
	m_List.SetItem(count, 2, LVIF_TEXT|LVIF_STATE, buf, 0, LVIS_SELECTED, LVIS_SELECTED, 0 );
	
	data = GetDVMData(EP_VOLTAGE)*1000.0f;
	buf.Format("%.2f", data);
	m_List.SetItem(count, 3, LVIF_TEXT|LVIF_STATE, buf, 0, LVIS_SELECTED, LVIS_SELECTED, 0 );

	data1 = VTG_PRECISION(GetADVal(EP_VOLTAGE));
	buf.Format("%.2f", data1);
	m_List.SetItem(count, 4, LVIF_TEXT|LVIF_STATE, buf, 0, LVIS_SELECTED, LVIS_SELECTED, 0 );

	buf.Format("%.2f", data - data1);
	m_List.SetItem(count, 5, LVIF_TEXT|LVIF_STATE, buf, 0, LVIS_SELECTED, LVIS_SELECTED, 0 );

	data2 = CRT_PRECISION(GetDVMData(EP_CURRENT));
	buf.Format("%.2f", data2);
	m_List.SetItem(count, 6, LVIF_TEXT|LVIF_STATE, buf, 0, LVIS_SELECTED, LVIS_SELECTED, 0 );

	data3 = GetADVal(EP_CURRENT)*1000.0f;
	buf.Format("%.2f", data3);
	m_List.SetItem(count, 7, LVIF_TEXT|LVIF_STATE, buf, 0, LVIS_SELECTED, LVIS_SELECTED, 0 );

	buf.Format("%.2f", data2 - data3);
	m_List.SetItem(count, 8, LVIF_TEXT|LVIF_STATE, buf, 0, LVIS_SELECTED, LVIS_SELECTED, 0 );

	if( count > MAX_LIST_VALUE )
	{
		m_List.DeleteItem(0);
	}

//Date Time
//	lvi.iSubItem =1;
//	lvi.pszText = (LPTSTR)(LPCTSTR)(buf);
//	m_List.SetItem(&lvi);

	//AD Voltage
/*	lvi.iSubItem =2;
	buf.Format("%.4f", data);
	lvi.pszText = (LPTSTR)(LPCTSTR)(buf);
	m_List.SetItem(&lvi);

	//DVM Voltage
	lvi.iSubItem =3;
	buf.Format("%.4f", data);
	lvi.pszText = (LPTSTR)(LPCTSTR)(buf);
	m_List.SetItem(&lvi);
	
	//Voltage Error
	lvi.iSubItem =4;
	buf.Format("%.4f", data);
	lvi.pszText = (LPTSTR)(LPCTSTR)(buf);
	m_List.SetItem(&lvi);

	//AD Current
	lvi.iSubItem =5;
	buf.Format("%.4f", data);
	lvi.pszText = (LPTSTR)(LPCTSTR)(buf);
	m_List.SetItem(&lvi);

	//DVM Current
	lvi.iSubItem =6;
	buf.Format("%.4f", data);
	lvi.pszText = (LPTSTR)(LPCTSTR)(buf);
	m_List.SetItem(&lvi);

	//Current Error
	lvi.iSubItem =7;
	buf.Format("%.4f", data);
	lvi.pszText = (LPTSTR)(LPCTSTR)(buf);
	m_List.SetItem(&lvi);
*/
//	count =  m_List.GetItemCount();
//	m_List.SetSelectionMark(0);
	if(m_strFileName.IsEmpty())	return FALSE;

	FILE *fp;
	fp = fopen(m_strFileName, "at+");
	if(fp == NULL)	return FALSE;

	fprintf(fp, "%s,%d,%s,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f\n", GetModuleName(m_nModuleID), m_nChannelIndex+1, datetime.Format("%m/%d_%H:%M:%S"), data, data1, data - data1, data2, data3, data2-data3);
	fclose(fp);
	return TRUE;
}

void CMeasureDlg::OnOptAllChannel() 
{
	// TODO: Add your control notification handler code here
	GetDlgItem(IDC_CH_SELECT_EDIT)->EnableWindow(FALSE);
}

void CMeasureDlg::OnOptPartChannel() 
{
	// TODO: Add your control notification handler code here
	GetDlgItem(IDC_CH_SELECT_EDIT)->EnableWindow(TRUE);
}

void CMeasureDlg::FunWorkEndReveive()
{	
	m_bExit = FALSE;

	KillTimer(100);

	GetDlgItem(IDC_START)->EnableWindow(TRUE);
	GetDlgItem(IDC_STOP)->EnableWindow(FALSE);
}

void CMeasureDlg::fnUpdatePointData( int nModuleID )
{
	// 2. 실측 Shunt Point
	double shuntData[CAL_MAX_BOARD_NUM];

	int i=0;
	int nSize;
	LPVOID	pBuffer;
	EP_REAL_MEASURE_POINT_DATA MeasurePointData;
	nSize = sizeof(EP_REAL_MEASURE_POINT_DATA);

	if((pBuffer = EPSendDataCmd(nModuleID, 0, 0, EP_CMD_GET_RM_POINT_DATA, nSize)) != NULL)
	{
		memcpy(&MeasurePointData, pBuffer, sizeof(EP_REAL_MEASURE_POINT_DATA));

		for(i =0; i<CAL_MAX_BOARD_NUM; i++)
		{
			shuntData[i] = MeasurePointData.lCaliShuntR[i]/10000.0f;
		}
		m_pMeasPoint.SetShuntR(shuntData);

		for(i =0; i<CAL_MAX_BOARD_NUM; i++)
		{
			shuntData[i] = MeasurePointData.lCaliShuntT[i]/10000.0f;
		}
		m_pMeasPoint.SetShuntT(shuntData);

		for(i =0; i<CAL_MAX_BOARD_NUM; i++)
		{
			m_pMeasPoint.SetShuntSerial(i, &MeasurePointData.byCaliShuntSerail[i][0] );				
		}
	}
	else
	{
		AfxMessageBox(TEXT_LANG[3], MB_ICONWARNING);
	}
}

void CMeasureDlg::OnOK() 
{
	// TODO: Add extra validation here
// 	if (m_bExit == FALSE)
// 	{
// 		AfxMessageBox("작업 중 입니다. 작업 완료후 종료 하세요. ");
// 		return;
// 	}
	CString strTemp;
	strTemp = TEXT_LANG[9];//"정밀도 검증을 종료 하시겠습니까?"
	if(MessageBox(strTemp, TEXT_LANG[10], MB_ICONQUESTION|MB_YESNO) != IDYES)		return;//"종료 확인"

	if(EPSendCommand( m_nModuleID, 0, 0, EP_CMD_RM_STOP) != EP_ACK)
	{
// 		AfxMessageBox("[정지 명령] 전송 실패하였습니다.");
// 		return;
	}
	// EPSendCommand(m_nModuleID);

	CDialog::OnOK();
}

void CMeasureDlg::SetModuleID(int nModuleID)
{
//	m_nModuleID = nModuleID;
	if (nModuleID > 0)
	{	
		m_nModuleID = nModuleID;
		m_ModuleList.SetCurSel(m_nModuleID-1);

		fnUpdatePointData( m_nModuleID );
	}
}

void CMeasureDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if (m_bExit == TRUE)
	{
		m_bExit = FALSE;
		AfxMessageBox(TEXT_LANG[11]);//"실측 작업 중 입니다. 확인 후 종료 하세요. "
		return;
	}

	AfxGetApp()->WriteProfileInt(REG_CAL_SECTION, "MeasureRange", m_ctrlRange.GetCurSel());

	OnOK();
}
