#if !defined(AFX_AGINGVIEW_H__ACB7EADA_2777_4C47_9F71_7EE6A2808EE2__INCLUDED_)
#define AFX_AGINGVIEW_H__ACB7EADA_2777_4C47_9F71_7EE6A2808EE2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AgingView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CAgingView form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif
#include "MyGridWnd.h"	// Added by ClassView
#include "MatrixStatic.h"

#define TRAY_PER_MD		8
#define COL_PER_TRAY	5
#define	V_SENS_PER_TRAY	4
#define JIG_STACK_COL	2

class CAgingView : public CFormView
{
protected:
	CAgingView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CAgingView)

// Form Data
public:
	//{{AFX_DATA(CAgingView)
	enum { IDD = IDD_AGING_FORM };
	CLabel	m_CmdTarget;
	CLabel	m_StateStatic;
	CLedButton	m_DisChargeLamp;
	CLedButton	m_ChargeLamp;
//	CMatrixStatic	m_RemainTimeStatic;
//	CMatrixStatic	m_RunTimeStatic;
	CMatrixStatic	m_TempLed;
	CMatrixStatic	m_TempLed1;
	CLedButton	m_GreenLamp;
	CLedButton	m_YellowLamp;
	CLedButton	m_RedLamp;
	CLabel	m_DischargeLabel;
	CLabel	m_ChargeLabel;
	CLabel	m_RackBack;
	//}}AFX_DATA

// Attributes
public:

// Operations
public:
	void InitGrid2();
	void ModuleConnected(int nModuleID);
	void SetCurrentModule(int nModuleID);
	int m_nCurModuleID;
	void UpdateState();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAgingView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void ShowState();
	int m_nMonitoringTimer;
	void StopMonitoring();
	void StartMonitoring();
	void InitGrid();
	void ReConfigGrid();
	CMyGridWnd m_wndGrid;
	CMyGridWnd m_wndGrid2;
	virtual ~CAgingView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CAgingView)
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnDestroy();
	afx_msg void OnEditCopy();
	afx_msg void OnUpdateEditCopy(CCmdUI* pCmdUI);
	//}}AFX_MSG
	afx_msg LRESULT OnMovedCurrentCell(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AGINGVIEW_H__ACB7EADA_2777_4C47_9F71_7EE6A2808EE2__INCLUDED_)
