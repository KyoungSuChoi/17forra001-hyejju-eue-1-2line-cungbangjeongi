// ChCaliData.cpp: implementation of the CChCaliData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ChCaliData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CChCaliData::CChCaliData()
{
	m_nEdited = CAL_DATA_EMPTY;
	m_nBoardNo = 0;	//1Base
	m_nChannelNo = 0;	//1Base

	ZeroMemory(m_VCalData, sizeof(m_VCalData));
	ZeroMemory(m_VCheckData, sizeof(m_VCheckData));
	ZeroMemory(m_ICalData, sizeof(m_ICalData));
	ZeroMemory(m_ICheckData, sizeof(m_ICheckData));
}

CChCaliData::~CChCaliData()
{

}

void CChCaliData::GetVCalData(WORD wRange, WORD wPoint, double &dAdData, double &dMeterData)
{
	if(wRange >= CAL_MAX_VOLTAGE_RANGE || wPoint>= CAL_MAX_CALIB_SET_POINT)
	{
		dAdData = 0.0;
		dMeterData = 0.0;
	}
	
	dAdData = m_VCalData[wRange][wPoint].dAdData;
	dMeterData = m_VCalData[wRange][wPoint].dMeterData;
}

void CChCaliData::GetVCheckData(WORD wRange, WORD wPoint, double &dAdData, double &dMeterData)
{
	if(wRange >= CAL_MAX_VOLTAGE_RANGE || wPoint>= CAL_MAX_CALIB_CHECK_POINT)
	{
		dAdData = 0.0;
		dMeterData = 0.0;
	}
	
	dAdData = m_VCheckData[wRange][wPoint].dAdData;
	dMeterData = m_VCheckData[wRange][wPoint].dMeterData;
}

void CChCaliData::GetICalData(WORD wRange, WORD wPoint, double &dAdData, double &dMeterData)
{
	if(wRange >= CAL_MAX_CURRENT_RANGE || wPoint>= CAL_MAX_CALIB_SET_POINT)
	{
		dAdData = 0.0;
		dMeterData = 0.0;
	}
	
	dAdData = m_ICalData[wRange][wPoint].dAdData;
	dMeterData = m_ICalData[wRange][wPoint].dMeterData;
}

void CChCaliData::GetICheckData(WORD wRange, WORD wPoint, double &dAdData, double &dMeterData)
{
	if(wRange >= CAL_MAX_CURRENT_RANGE || wPoint>= CAL_MAX_CALIB_CHECK_POINT)
	{
		dAdData = 0.0;
		dMeterData = 0.0;
	}
	
	dAdData = m_ICheckData[wRange][wPoint].dAdData;
	dMeterData = m_ICheckData[wRange][wPoint].dMeterData;
}

void CChCaliData::GetVCalData(WORD wPoint, double &dAdData, double &dMeterData)
{
	if(wPoint>= CAL_MAX_CALIB_SET_POINT)
	{
		dAdData = 0.0;
		dMeterData = 0.0;
	}
	
	dAdData = m_VCalData[0][wPoint].dAdData;
	dMeterData = m_VCalData[0][wPoint].dMeterData;
}

void CChCaliData::GetVCheckData(WORD wPoint, double &dAdData, double &dMeterData)
{
	if(wPoint>= CAL_MAX_CALIB_CHECK_POINT)
	{
		dAdData = 0.0;
		dMeterData = 0.0;
	}
	
	dAdData = m_VCheckData[0][wPoint].dAdData;
	dMeterData = m_VCheckData[0][wPoint].dMeterData;
}

void CChCaliData::GetICalData(WORD wPoint, double &dAdData, double &dMeterData)
{
	if(wPoint>= CAL_MAX_CALIB_SET_POINT)
	{
		dAdData = 0.0;
		dMeterData = 0.0;
	}
	
	dAdData = m_ICalData[0][wPoint].dAdData;
	dMeterData = m_ICalData[0][wPoint].dMeterData;
}

void CChCaliData::GetICheckData(WORD wPoint, double &dAdData, double &dMeterData)
{
	if(wPoint>= CAL_MAX_CALIB_CHECK_POINT)
	{
		dAdData = 0.0;
		dMeterData = 0.0;
	}
	
	dAdData = m_ICheckData[0][wPoint].dAdData;
	dMeterData = m_ICheckData[0][wPoint].dMeterData;
}	 


void CChCaliData::SetVCalData(WORD wRange, WORD wPoint, double dAdData, double dMeterData)
{
	m_VCalData[wRange][wPoint].dAdData = dAdData;
	m_VCalData[wRange][wPoint].dMeterData = dMeterData;
}

void CChCaliData::SetVCheckData(WORD wRange, WORD wPoint, double dAdData, double dMeterData)
{
	m_VCheckData[wRange][wPoint].dAdData = dAdData;
	m_VCheckData[wRange][wPoint].dMeterData = dMeterData;
}

void CChCaliData::SetICalData(WORD wRange, WORD wPoint, double dAdData, double dMeterData)
{
	m_ICalData[wRange][wPoint].dAdData = dAdData;
	m_ICalData[wRange][wPoint].dMeterData = dMeterData;
}

void CChCaliData::SetICheckData(WORD wRange, WORD wPoint, double dAdData, double dMeterData)
{
	m_ICheckData[wRange][wPoint].dAdData = dAdData;
	m_ICheckData[wRange][wPoint].dMeterData = dMeterData;
}

	 

BOOL CChCaliData::ReadDataFromFile(FILE *fp)
{
	if(fp == NULL)	return FALSE;

	if(fread(&m_nBoardNo, sizeof(m_nBoardNo), 1, fp) < 1)
	{
		return FALSE;
	}

	if(fread(&m_nChannelNo, sizeof(m_nChannelNo), 1, fp) < 1)
	{
		return FALSE;
	}

	//여유공간 확보
	int nBuff[64];
	if(fread(nBuff, sizeof(nBuff), 1, fp) < 1)
	{
		return FALSE;
	}

	if(fread(m_VCalData, sizeof(m_VCalData), 1, fp) < 1)
	{
		return FALSE;
	}
	if(fread(m_VCheckData, sizeof(m_VCheckData), 1, fp) < 1)
	{
		return FALSE;
	}
	if(fread(m_ICalData, sizeof(m_ICalData), 1, fp) < 1)
	{
		return FALSE;
	}
	if(fread(m_ICheckData, sizeof(m_ICheckData), 1, fp) < 1)
	{
		return FALSE;
	}
	m_nEdited = CAL_DATA_SAVED;

	return TRUE;
}

BOOL CChCaliData::WriteDataToFile(FILE *fp)
{
	if(fp == NULL)	return FALSE;

	if(fwrite(&m_nBoardNo, sizeof(m_nBoardNo), 1, fp) < 1)
	{
		return FALSE;
	}

	if(fwrite(&m_nChannelNo, sizeof(m_nChannelNo), 1, fp) < 1)
	{
		return FALSE;
	}
	
	//여유공간 확보
	int nBuff[64];
	ZeroMemory(nBuff, sizeof(nBuff));
	if(fwrite(nBuff, sizeof(nBuff), 1, fp) < 1)
	{
		return FALSE;
	}

	if(fwrite(m_VCalData, sizeof(m_VCalData), 1, fp) < 1)
	{
		return FALSE;
	}
	if(fwrite(m_VCheckData, sizeof(m_VCheckData), 1, fp) < 1)
	{
		return FALSE;
	}
	if(fwrite(m_ICalData, sizeof(m_ICalData), 1, fp) < 1)
	{
		return FALSE;
	}
	if(fwrite(m_ICheckData, sizeof(m_ICheckData), 1, fp) < 1)
	{
		return FALSE;
	}
	m_nEdited = CAL_DATA_SAVED;

	return TRUE;
}
