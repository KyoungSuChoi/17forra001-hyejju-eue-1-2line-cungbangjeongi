/*************************************************************
	Copyrighted by LG Production engineering Research Center

	File Name: CTSGraphAnalDoc.h

	Written out by 
	Evaluation Technology Group
	Kim, Sung-Hoon

	Comment: CCTSGraphAnalDoc class is defined

	Revision History:
	Version   Name            Date        Reason
	    1.0   Kim, Sung-Hoon  2001.06.01  Create
*************************************************************/

// CTSGraphAnalDoc.h : interface of the CCTSGraphAnalDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CTSGraphAnalDOC_H__13FE5EAC_AB5E_11D4_88DF_006008CEDA07__INCLUDED_)
#define AFX_CTSGraphAnalDOC_H__13FE5EAC_AB5E_11D4_88DF_006008CEDA07__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// Class definition

//---------------------------------------------------------------------------
//	Class Name: CCTSGraphAnalDoc
//
//	Comment:
//
//	Revision History:
//	Version   Name            Date        Reason
//	    1.0   Kim, Sung-Hoon  2001.06.01  Create
//---------------------------------------------------------------------------
class CCTSGraphAnalDoc : public CDocument
{
protected: // create from serialization only
	CCTSGraphAnalDoc();
	DECLARE_DYNCREATE(CCTSGraphAnalDoc)

// Attributes
public:
	CData        m_Data;

// Operations
public:
	void  DrawView(CDC* pDC, CRect ClientRect);
	BOOL RequeryDataQuery();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCTSGraphAnalDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	//}}AFX_VIRTUAL

// Implementation
public:
	BOOL m_bLoadedSheet;
	void ReloadData();
	BOOL m_bStairLineType;
	int m_iCurSelPlaneIndex;
	virtual ~CCTSGraphAnalDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CCTSGraphAnalDoc)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CTSGraphAnalDOC_H__13FE5EAC_AB5E_11D4_88DF_006008CEDA07__INCLUDED_)
