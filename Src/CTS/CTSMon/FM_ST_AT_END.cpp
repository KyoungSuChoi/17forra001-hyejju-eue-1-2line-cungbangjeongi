#include "stdafx.h"
#include "CTSMon.h"
#include "FMS.h"
#include "FM_Unit.h"
#include "FM_STATE.h"
#include "MainFrm.h"

CFM_ST_AT_END::CFM_ST_AT_END(FM_STATUS_ID _Eqstid, FM_STATUS_ID _stid, CFM_Unit* _unit)
:CFM_ST_AUTO(_Eqstid, _stid, _unit)
{
	
}


CFM_ST_AT_END::~CFM_ST_AT_END(void)
{
}

VOID CFM_ST_AT_END::fnEnter()
{	
	m_nSeqNum = 1;

	m_Unit->fnGetFMS()->fnResultDataStateUpdate(NULL, RS_END);
	
	FMS_ERRORCODE _rec = FMS_ER_NONE;
	
	m_Unit->fnGetFMS()->fnSend_E_CHARGER_WORKEND(_rec);

	CHANGE_FNID(FNID_PROC);

	TRACE("CFM_ST_AT_END::fnEnter %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_AT_END::fnProc()
{
	if(-1 == m_iWaitCount)
	{
		fnSetFMSStateCode(FMS_ST_END);
	}
	
	if(1 == m_iWaitCount)
	{
		if( m_nSeqNum == 1 )
		{
			fnSetFMSStateCode(FMS_ST_RED_END);			
		}
		else
		{
			fnSetFMSStateCode(FMS_ST_BLUE_END);
		}

// 		if( !m_Unit->fnGetModuleTrayState() )
// 		{
// 			fnSetFMSStateCode(FMS_ST_RED_END);
// 		}
// 		else
// 		{
// 			fnSetFMSStateCode(FMS_ST_BLUE_END);
// 		}

		// 1. 타이머를 다시 시작
		m_iWaitCount = -1;
	}

	m_RetryMap |= RETRY_NOMAL_ON;

	TRACE("CFM_ST_AT_END::fnProc %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_AT_END::fnExit()
{	
	m_iWaitCount = -1;
	m_RetryMap |= RETRY_NOMAL_ON;

	TRACE("CFM_ST_AT_END::fnExit %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_AT_END::fnSBCPorcess(WORD _state)
{
	CFM_ST_AUTO::fnSBCPorcess(_state);

	TRACE("%s \n", g_strSBCState[m_Unit->fnGetModule()->GetState()]);
	
	if(m_Unit->fnGetFMS()->fnGetError())
	{
		CHANGE_STATE(AUTO_ST_ERROR);
	}
        
	switch(m_Unit->fnGetModule()->GetState())
	{
	case EP_STATE_IDLE:
		if(m_Unit->fnGetModuleTrayState())
		{
			CHANGE_STATE(AUTO_ST_TRAY_IN);
		}
		else
		{
			CHANGE_STATE(AUTO_ST_VACANCY);
		}
		break;
	case EP_STATE_STANDBY:
		{
			CHANGE_STATE(AUTO_ST_TRAY_IN);
		}
		break;
	case EP_STATE_PAUSE:
		{
			CHANGE_STATE(AUTO_ST_ERROR);
		}
		break;
	case EP_STATE_RUN:
		{
			if(m_Unit->fnGetModuleCheckState())
			{
				CHANGE_STATE(AUTO_ST_CONTACT_CHECK);
			}
			else
			{
				CHANGE_STATE(AUTO_ST_RUN);
			}
		}
		break;
	case EP_STATE_READY:
		{
			CHANGE_STATE(AUTO_ST_READY);
		}
		break;
	case EP_STATE_END:
		{

		}
		break;
	case EP_STATE_ERROR:
		{
			if( m_nSeqNum == 0 )
			{
				CHANGE_STATE(AUTO_ST_ERROR);
			}
		}
		break;
	}
	TRACE("CFM_ST_AT_END::fnSBCPorcess %d \n", m_Unit->fnGetModuleIdx());
}

FMS_ERRORCODE CFM_ST_AT_END::fnFMSPorcess(FMS_COMMAND _msgId, st_FMS_PACKET* _recvData)
{
	FMS_ERRORCODE _error = ER_NO_Process;
	//결과 요청
	//결과 수신 완료
	//출고 완료
	switch(_msgId)
	{

	case E_CHARGER_WORKEND_RESPONSE:
		{
			fnWaitInit();
		}
		break;
	case E_CHARGER_RESULT://결과 요청 - 블루 앤드
		{
			fnWaitInit();

			_error = ER_Data_Error;

			if(sizeof(st_CHARGER_RESULT) == _recvData->dataLen)
			{
				st_CHARGER_RESULT data;
				memset(&data, 0x20, sizeof(st_CHARGER_RESULT));
				m_Unit->fnGetFMS()->strCutter(_recvData->data, &data
					, g_iCHARGER_RESULT_Map);
				_error = ER_TrayID;
				if(strcmp(m_Unit->fnGetFMS()->fnGetTrayID(), data.Tray_ID) == 0
					&& m_Unit->fnGetFMS()->fnGetLineNo() == atoi(data.Equipment_Num))
				{
					_error = FMS_ER_NONE;
				}
			}
		}
		break;
	case E_CHARGER_RESULT_RECV://결과 요청 확인 - 블루 앤드 초기화
		{
			m_nSeqNum = 0;

			fnWaitInit();
			fnSetFMSStateCode(FMS_ST_END);
			CHANGE_FNID(FNID_EXIT);

			st_CHARGER_RESULT_RECV data;
			memset(&data, 0x20, sizeof(st_CHARGER_RESULT_RECV));
			m_Unit->fnGetFMS()->strCutter(_recvData->data, &data
				, g_iCHARGER_RESULT_RECV_Map);

			data.Notify_Mode;
			data.Tray_ID;

			_error = ER_TrayID;
			if(strcmp(m_Unit->fnGetFMS()->fnGetTrayID(), data.Tray_ID) == 0)
			{
				_error = FMS_ER_NONE;

				if(data.Notify_Mode[0] == '0')
				{
					m_Unit->fnGetFMS()->fnResultDataStateUpdate(NULL, RS_REPORT);
				}
			}
		}
		break;	

	case E_CHARGER_OUT:
		{	
			_error = FMS_ER_NONE;
		}
		break;

	default:
		{
		}
		break;
	}

	TRACE("CFM_ST_AT_END::fnFMSPorcess %d \n", m_Unit->fnGetModuleIdx());

	return _error;

}