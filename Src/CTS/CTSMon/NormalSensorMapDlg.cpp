// NormalSensorMapDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "NormalSensorMapDlg.h"


// CNormalSensorMapDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CNormalSensorMapDlg, CDialog)

CNormalSensorMapDlg::CNormalSensorMapDlg(int nDeviceID, CWnd* pParent /*=NULL*/)
	: CDialog(CNormalSensorMapDlg::IDD, pParent)
{
	m_nDeviceID = nDeviceID;
}

CNormalSensorMapDlg::~CNormalSensorMapDlg()
{
}

void CNormalSensorMapDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CNormalSensorMapDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CNormalSensorMapDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CNormalSensorMapDlg 메시지 처리기입니다.

void CNormalSensorMapDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	this->ShowWindow(SW_HIDE);
	OnOK();
}
BOOL CNormalSensorMapDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
// 	if( m_nDeviceID == DEVICE_FORMATION1 || m_nDeviceID == DEVICE_DCIR1 || m_nDeviceID == DEVICE_FORMATION2 )
// 	{
// 		GetDlgItem(IDC_SPIN_TYPE1)->ShowWindow(SW_SHOW);
// 		GetDlgItem(IDC_SPIN_TYPE2)->ShowWindow(SW_HIDE);
// 	}
// 	else
// 	{
// 		GetDlgItem(IDC_SPIN_TYPE1)->ShowWindow(SW_HIDE);
// 		GetDlgItem(IDC_SPIN_TYPE2)->ShowWindow(SW_SHOW);
// 	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
