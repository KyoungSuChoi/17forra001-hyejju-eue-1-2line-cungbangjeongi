#include "stdafx.h"

#include "CTSMon.h"

#include "FMS.h"
#include "FM_Unit.h"

#include "FM_STATE.h"

CFM_ST_MAINT::CFM_ST_MAINT(FM_STATUS_ID _Eqstid, FM_STATUS_ID _stid, CFM_Unit* _unit)
:CFM_STATE(_Eqstid, _stid, _unit)
, m_pstMAMaint(NULL)
, m_pstMAError(NULL)
{
}


CFM_ST_MAINT::~CFM_ST_MAINT(void)
{
	if(m_pstMAMaint)
	{
		delete m_pstMAMaint;
		m_pstMAMaint = NULL;
	}
	if(m_pstMAError)
	{
		delete m_pstMAError;
		m_pstMAError = NULL;
	}
}

VOID CFM_ST_MAINT::fnEnter()
{
	switch(m_Unit->fnGetModule()->GetState())
	{
	case EP_STATE_IDLE:
		if(m_Unit->fnGetModuleTrayState())
		{
			fnSetFMSStateCode(FMS_ST_TRAY_IN);
		}
		else
		{
			fnSetFMSStateCode(FMS_ST_VACANCY);
		}
		break;
	case EP_STATE_STANDBY:
		{
			fnSetFMSStateCode(FMS_ST_TRAY_IN);
		}
		break;
	case EP_STATE_PAUSE:
		{
			fnSetFMSStateCode(FMS_ST_ERROR);
		}
		break;
	case EP_STATE_RUN:
		{
			if(m_Unit->fnGetModuleCheckState())
			{
				fnSetFMSStateCode(FMS_ST_CONTACT_CHECK);
			}
			else
			{
				fnSetFMSStateCode(FMS_ST_RUNNING);
			}
		}
		break;
	case EP_STATE_READY:
		{
			fnSetFMSStateCode(FMS_ST_READY);
		}
		break;
	case EP_STATE_END:
		{
			fnSetFMSStateCode(FMS_ST_END);
		}
		break;
	case EP_STATE_ERROR:
		{
			fnSetFMSStateCode(FMS_ST_ERROR);
		}
		break;
	}

	if(m_Unit->fnGetFMS()->fnGetError() == FMS_ST_ERROR)
	{
		CHANGE_STATE(LOCAL_ST_ERROR);
	}

	TRACE("CFM_ST_MAINT::fnEnter %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_MAINT::fnProc()
{
	TRACE("CFM_ST_MAINT::fnProc %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_MAINT::fnExit()
{
	TRACE("CFM_ST_MAINT::fnExit %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_MAINT::fnSBCPorcess(WORD _state)
{
	if(m_Unit->fnGetModule()->GetState() == EP_STATE_LINE_OFF)
	{
		CHANGE_STATE(EQUIP_ST_OFF);
	}
	else
	{
		switch(m_Unit->fnGetModule()->GetOperationMode())
		{
		case EP_OPERATION_LOCAL:
			CHANGE_STATE(EQUIP_ST_LOCAL);
			break;
		case EP_OPEARTION_MAINTENANCE:
			//CHANGE_STATE(EQUIP_ST_MAINT);
			break;
		case EP_OPERATION_AUTO:
			CHANGE_STATE(EQUIP_ST_AUTO);
			break;
		}
	}
	TRACE("CFM_ST_MAINT::fnSBCPorcess %d \n", m_Unit->fnGetModuleIdx());
}

FMS_ERRORCODE CFM_ST_MAINT::fnFMSPorcess(FMS_COMMAND _msgId, st_FMS_PACKET* _recvData)
{
	FMS_ERRORCODE _error = ER_NO_Process;

	switch(_msgId)
	{
	default:
		{
		}
		break;
	}
	TRACE("CFM_ST_MAINT::fnFMSPorcess %d \n", m_Unit->fnGetModuleIdx());

	return _error;

}