// This file was created on March 21st 2001 by Robert Brault.
// I created this Class to be able change the Color of your Edit Box
// as well as your Edit Box Text. This is Derived from CEdit so you
// do not have all the overhead of a CRichEditCtrl.
//
// There are three functions available Currently:
// SetBkColor(COLORREF crColor)
// SetTextColor(COLORREF crColor)
// SetReadOnly(BOOL flag = TRUE)
//
// How To Use:
// Add three files to your project
// ColorEdit.cpp, ColorEdit.h and Color.h
// Color.h has (#define)'s for different colors (add any color you desire).
//
// Add #include "ColorEdit.h" to your Dialogs Header file.
// Declare an instance of CColorEdit for each edit box being modified.
// Ex. CColorEdit m_ebName;
//
// In your OnInitDialog() add a SubclassDlgItem for each CColorEdit member variable.
// Ex. m_ebName.SubclassDlgItem(IDC_EB_NAME, this);
// In this same function initialize your color for each box unless you want the default.


// ColorEdit.cpp : implementation file
//

#include "stdafx.h"
#include "ColorEdit.h"
//#include "Color.h" // File Holding (#define)'s for COLORREF Values

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CColorEdit

CColorEdit::CColorEdit()
{
	m_crBkColor = ::GetSysColor(COLOR_3DFACE); // Initializing background color to the system face color.
	m_crTextColor = BLACK; // Initializing text color to black
	m_brBkgnd.CreateSolidBrush(m_crBkColor); // Creating the Brush Color For the Edit Box Background

	::GetObject((HFONT)GetStockObject(DEFAULT_GUI_FONT),sizeof(m_lf),&m_lf);
	m_font.CreateFontIndirect(&m_lf);
}

CColorEdit::~CColorEdit()
{
	m_brBkgnd.DeleteObject();
	m_font.DeleteObject();
}


BEGIN_MESSAGE_MAP(CColorEdit, CEdit)
	//{{AFX_MSG_MAP(CColorEdit)
	ON_WM_CTLCOLOR_REFLECT()
	//}}AFX_MSG_MAP
//	ON_WM_DRAWITEM()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CColorEdit message handlers

void CColorEdit::SetTextColor(COLORREF crColor)
{
	m_crTextColor = crColor; // Passing the value passed by the dialog to the member varaible for Text Color
	RedrawWindow();
}

void CColorEdit::SetBkColor(COLORREF crColor)
{
	m_crBkColor = crColor; // Passing the value passed by the dialog to the member varaible for Backgound Color
	m_brBkgnd.DeleteObject(); // Deleting any Previous Brush Colors if any existed.
	m_brBkgnd.CreateSolidBrush(crColor); // Creating the Brush Color For the Edit Box Background
	RedrawWindow();
}



HBRUSH CColorEdit::CtlColor(CDC* pDC, UINT nCtlColor)
{
	HBRUSH hbr;
	hbr = (HBRUSH)m_brBkgnd;			// Passing a Handle to the Brush
	pDC->SetBkColor(m_crBkColor);		// Setting the Color of the Text Background to the one passed by the Dialog
	pDC->SetTextColor(m_crTextColor);	// Setting the Text Color to the one Passed by the Dialog
	pDC->SelectObject(&m_font);
	
	CString strCaption;
	CRect rcOffsetText;
	GetWindowText(strCaption);
	GetWindowRect(rcOffsetText);
	DrawEditText(pDC, rcOffsetText , strCaption);
	
	Invalidate();
	
	if (nCtlColor)       // To get rid of compiler warning
      nCtlColor += 0;

	return hbr;
}

BOOL CColorEdit::SetReadOnly(BOOL flag)
{
 //  if (flag == TRUE)
  //    SetBkColor(m_crBkColor);
//   else
//      SetBkColor(WHITE);

	 return CEdit::SetReadOnly(flag);
}


void CColorEdit::ReconstructFont()
{
	m_font.DeleteObject();
	BOOL bCreated = m_font.CreateFontIndirect(&m_lf);

	ASSERT(bCreated);
}

CColorEdit& CColorEdit::SetFontSize(int nSize)
{
	nSize*=-1;
	m_lf.lfHeight = nSize;
	ReconstructFont();
	Invalidate();	
	return *this;
}

CColorEdit& CColorEdit::SetFontBold(BOOL bBold)
{	
	m_lf.lfWeight = bBold ? FW_BOLD : FW_NORMAL;
	ReconstructFont();
	RedrawWindow();
	return *this;
}

void CColorEdit::DrawEditText(CDC *pDC, CRect rc, CString strCaption )
{
	DWORD uStyle = GetWindowLong(this->m_hWnd, GWL_STYLE);

	CArray<CString, CString> arLines;

	if((uStyle & BS_MULTILINE) == BS_MULTILINE)
	{
		int nIndex = 0;
		while(nIndex != -1)
		{
			nIndex = strCaption.Find('\n');
			if(nIndex>-1)
			{
				CString line = strCaption.Left(nIndex);
				arLines.Add(line);
				strCaption.Delete(0,nIndex+1);
			}
			else
				arLines.Add(strCaption);
		}
	}
	else
	{
		arLines.Add(strCaption);
	}

	CSize sizeText = pDC->GetOutputTextExtent( strCaption );

	COLORREF oldColour;

	int nStartPos = (rc.Height() - arLines.GetSize()*sizeText.cy)/2-1;
	if((uStyle & BS_TOP) == BS_TOP)
		nStartPos = rc.top+2;
	if((uStyle & BS_BOTTOM) == BS_BOTTOM)
		nStartPos = rc.bottom- arLines.GetSize()*sizeText.cy-2;
	if((uStyle & BS_VCENTER) == BS_VCENTER)
		nStartPos = (rc.Height() - arLines.GetSize()*sizeText.cy)/2-1;

	UINT uDrawStyles = 0;
	if((uStyle & BS_CENTER) == BS_CENTER)
		uDrawStyles |= DT_CENTER;
	else
	{
		if((uStyle & BS_LEFT) == BS_LEFT)
			uDrawStyles |= DT_LEFT;
		else
			if((uStyle & BS_RIGHT) == BS_RIGHT)
				uDrawStyles |= DT_RIGHT;
			else
				if(uDrawStyles == 0)
					uDrawStyles = DT_CENTER|DT_VCENTER | DT_SINGLELINE;
	}

	for(int i=0; i<arLines.GetSize(); i++)
	{
		CRect textrc = rc;
		textrc.DeflateRect(3,0,3,0);
		textrc.top = nStartPos + sizeText.cy*i;
		textrc.bottom = nStartPos + sizeText.cy*(i+1);
		CString line = arLines.GetAt(i);
		pDC->DrawText(line, line.GetLength(), textrc, uDrawStyles);
	}
}
