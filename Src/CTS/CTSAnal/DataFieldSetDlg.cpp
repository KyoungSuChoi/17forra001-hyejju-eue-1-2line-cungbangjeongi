// DataFieldSetDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "DataFieldSetDlg.h"

#include "ReportDataListRecordSet.h"
#include "FieldAddDlg.h"
#include "ProcTypeRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDataFieldSetDlg dialog

bool CDataFieldSetDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CDataFieldSetDlg"), _T("TEXT_CDataFieldSetDlg_CNT"), _T("TEXT_CDataFieldSetDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CDataFieldSetDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CDataFieldSetDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}


CDataFieldSetDlg::CDataFieldSetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDataFieldSetDlg::IDD, pParent)
{
	LanguageinitMonConfig();

	//{{AFX_DATA_INIT(CDataFieldSetDlg)
	m_bGradingCode = FALSE;
	m_bCellCode = FALSE;
	//}}AFX_DATA_INIT
	m_nFieldCount = 0;
}

CDataFieldSetDlg::~CDataFieldSetDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

void CDataFieldSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDataFieldSetDlg)
	DDX_Check(pDX, IDC_GRADING_CHECK, m_bGradingCode);
	DDX_Check(pDX, IDC_CELL_CODE_CHECK, m_bCellCode);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDataFieldSetDlg, CDialog)
	//{{AFX_MSG_MAP(CDataFieldSetDlg)
	ON_BN_CLICKED(IDC_ADD_FIELD, OnAddField)
	ON_BN_CLICKED(IDC_DELETE_FIELD, OnDeleteField)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDataFieldSetDlg message handlers

void CDataFieldSetDlg::InitFieldSetGrid()
{
	m_FieldGrid.SubclassDlgItem(IDC_FIELD_SET_GRID, this);
	m_FieldGrid.Initialize();
	m_FieldGrid.GetParam()->EnableUndo(FALSE);
//	m_FieldGrid.SetRowCount(10);
	m_FieldGrid.SetColCount(6);

	CRect rect;
	m_FieldGrid.GetClientRect(&rect);
	m_FieldGrid.SetColWidth(0, 0, int(rect.Width()*0.1));
	m_FieldGrid.SetColWidth(1, 2, 0); 
	m_FieldGrid.SetColWidth(3, 3, int(rect.Width()*0.3));
	m_FieldGrid.SetColWidth(4, 4, int(rect.Width()*0.3));
	m_FieldGrid.SetColWidth(5, 5, int(rect.Width()*0.3));
	m_FieldGrid.SetColWidth(6, 6, 0);

	m_FieldGrid.SetStyleRange(CGXRange(0, 0),	CGXStyle().SetValue(TEXT_LANG[0]));//"No"
	m_FieldGrid.SetStyleRange(CGXRange(0, 1),	CGXStyle().SetValue(TEXT_LANG[1]));//"ID"
	m_FieldGrid.SetStyleRange(CGXRange(0, 2),	CGXStyle().SetValue(TEXT_LANG[2]));//"DataType"
	m_FieldGrid.SetStyleRange(CGXRange(0, 3),	CGXStyle().SetValue(TEXT_LANG[3]));//"공정Type"
	m_FieldGrid.SetStyleRange(CGXRange(0, 4),	CGXStyle().SetValue(TEXT_LANG[2]));//"DataType"
	m_FieldGrid.SetStyleRange(CGXRange(0, 5),	CGXStyle().SetValue(TEXT_LANG[4]));//"Name"
	m_FieldGrid.SetStyleRange(CGXRange(0, 6),	CGXStyle().SetValue(TEXT_LANG[5]));//"Display"
	m_FieldGrid.SetStyleRange(CGXRange().SetCols(1, 2),	CGXStyle().SetEnabled(FALSE));
	m_FieldGrid.SetStyleRange(CGXRange().SetCols(1, 5), CGXStyle().SetControl(GX_IDS_CTRL_STATIC));
	m_FieldGrid.SetStyleRange(CGXRange().SetCols(6), CGXStyle().SetControl(GX_IDS_CTRL_CHECKBOX3D));
	
/*	CString strType(" 전압\n 전류\n 용량\n DCIR\n Step 시간\n Watt\n WattHour\n");
	m_FieldGrid.SetStyleRange(CGXRange().SetCols(2),
			CGXStyle()
				.SetControl(GX_IDS_CTRL_ZEROBASED_EX)
				.SetHorizontalAlignment(DT_LEFT)
				.SetChoiceList(strType)
	);
	
	m_FieldGrid.GetParam()->EnableUndo(TRUE);
*/
//	m_FieldGrid.SetStyleRange(CGXRange().SetCols(1, 2), CGXStyle().SetVertScrollBar(TRUE).SetWrapText(FALSE));

}

BOOL CDataFieldSetDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	InitFieldSetGrid();
	RequeryProcType();
	CString strTemp;
	
	for(int i =0; i<m_nFieldCount; i++)
	{
		if(m_Field[i].ID == EP_REPORT_GRADING)		//종합 등급 code
		{
			m_bGradingCode = TRUE;
			continue;
		}
		if(m_Field[i].ID == EP_REPORT_CH_CODE)		//최종 Cell Code
		{
			m_bCellCode = TRUE;
			continue;
		}

		m_FieldGrid.InsertRows(i+1, 1);
		switch(m_Field[i].DataType)
		{
		case EP_VOLTAGE		:	strTemp = TEXT_LANG[6];			break;//"전압"
		case EP_CURRENT		:	strTemp = TEXT_LANG[7];			break;//"전류"
		case EP_CAPACITY	:	strTemp = TEXT_LANG[8];			break;//"용량"
		case EP_IMPEDANCE	:	strTemp = TEXT_LANG[9];		break;//"DCIR"
		case EP_TOT_TIME	:	strTemp = TEXT_LANG[10];		break;//"전체 시간"
		case EP_STEP_TIME	:	strTemp = TEXT_LANG[11];			break;	//"시간"
		case EP_WATT		:	strTemp = TEXT_LANG[12];			break;//"Watt"
		case EP_WATT_HOUR	:	strTemp = TEXT_LANG[13];		break;//"WattHour"
//		case EP_OCV			:	strTemp = "OCV";			break;
		case EP_STATE		:	strTemp = TEXT_LANG[14];			break;	//			"상태"
		case EP_CH_CODE		:	strTemp = TEXT_LANG[15];		break;//"Cell Code"
		case EP_GRADE_CODE	:	strTemp = TEXT_LANG[16];		break;//"Grade Code"
		case EP_STEP_NO		:	strTemp = TEXT_LANG[17];		break;//"Step No"
		default				:	strTemp = TEXT_LANG[18];			break;//"오류"
		}

		m_FieldGrid.SetValueRange(CGXRange(i+1, 1), (long)m_Field[i].ID);
		m_FieldGrid.SetValueRange(CGXRange(i+1, 2), (long)m_Field[i].DataType);
		m_FieldGrid.SetValueRange(CGXRange(i+1, 3), GetProcTypeName(m_Field[i].ID));
		m_FieldGrid.SetValueRange(CGXRange(i+1, 4), strTemp);
		m_FieldGrid.SetValueRange(CGXRange(i+1, 5), m_Field[i].FieldName);
		m_FieldGrid.SetValueRange(CGXRange(i+1, 6), (long)m_Field[i].bDisplay);
	}
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDataFieldSetDlg::OnAddField() 
{
	// TODO: Add your control notification handler code here
	CFieldAddDlg	dlg;
	dlg.m_apProcType = &m_apProcType;
	if(dlg.DoModal() != IDOK)	return;
	
	int nRowCount = m_FieldGrid.GetRowCount();
	if(nRowCount >= FIELDNO)
	{
		MessageBox(TEXT_LANG[19],TEXT_LANG[20],  MB_ICONSTOP|MB_OK);//"입력한 Field 수가 너무 많습니다.", "입력 초과"
		return;
	}
	CString strTemp;
	m_FieldGrid.InsertRows(nRowCount+1, 1);
	m_FieldGrid.SetValueRange(CGXRange(nRowCount+1, 1), (LONG)dlg.m_ProcType.nCode);
	m_FieldGrid.SetValueRange(CGXRange(nRowCount+1, 2), (LONG)dlg.m_nDataType);
	m_FieldGrid.SetValueRange(CGXRange(nRowCount+1, 3), dlg.m_ProcType.szMessage);
	switch(dlg.m_nDataType)
	{
	case EP_VOLTAGE		:	strTemp = TEXT_LANG[6];			break;//"전압"
	case EP_CURRENT		:	strTemp = TEXT_LANG[7];			break;//"전류"
	case EP_CAPACITY	:	strTemp = TEXT_LANG[8];			break;//"용량"
	case EP_IMPEDANCE	:	strTemp = TEXT_LANG[9];		break;//"DCIR"
	case EP_TOT_TIME	:	strTemp = TEXT_LANG[10];		break;//"전체 시간"
	case EP_STEP_TIME	:	strTemp = TEXT_LANG[11];			break;	//"시간"
	case EP_WATT		:	strTemp = TEXT_LANG[12];			break;//"Watt"
	case EP_WATT_HOUR	:	strTemp = TEXT_LANG[13];		break;//"WattHour"
//	case EP_OCV			:	strTemp = "OCV";			break;
	case EP_STATE		:	strTemp = TEXT_LANG[14];			break;	//	"상태"		
	case EP_CH_CODE		:	strTemp = TEXT_LANG[15];		break;//"Cell Code"
	case EP_GRADE_CODE	:	strTemp = TEXT_LANG[16];		break;//"Grade Code"
	case EP_STEP_NO		:	strTemp = TEXT_LANG[17];		break;//"Step No"
	default				:	strTemp = TEXT_LANG[18];			break;//"오류"
	}
	m_FieldGrid.SetValueRange(CGXRange(nRowCount+1, 4), strTemp);
	m_FieldGrid.SetValueRange(CGXRange(nRowCount+1, 5), dlg.m_strFieldName);

}

void CDataFieldSetDlg::OnDeleteField() 
{
	// TODO: Add your control notification handler code here
	CString		strTemp;
	ROWCOL nRow = m_FieldGrid.GetRowCount();
	if(nRow <1)		return;

	CRowColArray	awRows;
	m_FieldGrid.GetSelectedRows(awRows);
	if(awRows.GetSize() <=0)	return;

	strTemp.Format(TEXT_LANG[21], awRows.GetSize());//"선택한 %d 개의 Data Field를 삭제 하시겠습니까?"
	if(IDNO == MessageBox(strTemp,TEXT_LANG[22], MB_YESNO|MB_ICONQUESTION))	return;//"삭제"
	
	for (int i = awRows.GetSize()-1; i >=0 ; i--)
	{
		m_FieldGrid.RemoveRows(awRows[i], awRows[i]);
	}
}

void CDataFieldSetDlg::OnOK() 
{
	// TODO: Add extra validation here
	m_nFieldCount = m_FieldGrid.GetRowCount();
	UpdateData(TRUE);

	int i = 0;
	
	for(i=0; i<m_nFieldCount; i++)
	{
		m_Field[i].No = i+1;//atol(m_FieldGrid.GetValueRowCol(i+1, 0));
		m_Field[i].ID = atol(m_FieldGrid.GetValueRowCol(i+1, 1));
		m_Field[i].DataType = atol(m_FieldGrid.GetValueRowCol(i+1, 2));
		m_Field[i].FieldName = m_FieldGrid.GetValueRowCol(i+1, 5);
		m_Field[i].bDisplay = TRUE;//atol(m_FieldGrid.GetValueRowCol(i+1, 6));
	}

	if(m_bGradingCode)			//종합 등급 표시 옵션
	{
		m_Field[m_nFieldCount].No = m_nFieldCount+1;
		m_Field[m_nFieldCount].ID = EP_REPORT_GRADING;
		m_Field[m_nFieldCount].DataType = EP_GRADE_CODE;
		m_Field[m_nFieldCount].FieldName = TEXT_LANG[16];//"등급 Code"
		m_Field[m_nFieldCount].bDisplay = TRUE;
		m_nFieldCount++;
	}

	if(m_bCellCode)				//최종 Cell code 표시 옵션
	{
		m_Field[m_nFieldCount].No = m_nFieldCount+1;
		m_Field[m_nFieldCount].ID = EP_REPORT_CH_CODE;
		m_Field[m_nFieldCount].DataType = EP_CH_CODE;
		m_Field[m_nFieldCount].FieldName = "Cell Code";
		m_Field[m_nFieldCount].bDisplay = TRUE;
		m_nFieldCount++;
	}

	CReportDataListRecordSet	rs;

	rs.Open();	
	while(!rs.IsEOF())
	{
		rs.Delete();
		rs.MoveNext();
	}

	for(i =0; i<m_nFieldCount; i++)
	{
		rs.AddNew();
		rs.m_No         = m_Field[i].No;
		rs.m_ProgressID = m_Field[i].ID;
		rs.m_Name       = m_Field[i].FieldName;
		rs.m_DataType   = m_Field[i].DataType;
		rs.m_Display    = m_Field[i].bDisplay;
	  	rs.Update();
	}

	rs.Close();
	
	CDialog::OnOK();
}

/*
long CDataFieldSetDlg::FindMaxID()
{
	int lMax = 99;
	for(int i = 0; i<m_nFieldCount; i++)
	{
		if(m_Field[i].ID != RPT_GRADING && m_Field[i].ID != RPT_CH_CODE)
		{
			if(lMax < m_Field[i].ID)
				lMax = m_Field[i].ID;
		}	
	}
	return lMax;
}
*/

BOOL CDataFieldSetDlg::RequeryProcType()
{
	CProcTypeRecordSet	rs;
	rs.m_strFilter.Format("[ProcType] >= %d", EP_PROC_TYPE_CHARGE_LOW);
	rs.m_strSort.Format("[ProcType]");

	try
	{
		rs.Open();
	}
	catch (CDaoException *e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);	
		e->Delete();
		return FALSE;
	}
	
	if(rs.IsBOF())
	{
		rs.Close();
		return FALSE;
	}

	STR_MSG_DATA	*pObject;
	pObject = new STR_MSG_DATA;
	pObject->nCode = 0;
	pObject->nData = 0;
	sprintf(pObject->szMessage, TEXT_LANG[23]);//"관계없음"
	m_apProcType.Add(pObject);

	while(!rs.IsEOF())
	{
		pObject = new STR_MSG_DATA;
		ASSERT(pObject);
		ZeroMemory(pObject, sizeof(STR_MSG_DATA));
		pObject->nCode = rs.m_ProcType;
		pObject->nData = rs.m_ProcID;
		m_apProcType.Add(pObject);
		sprintf(pObject->szMessage, "%s", rs.m_Description);
		TRACE("Proc Type Id %d : %s\n", rs.m_ProcType, rs.m_Description);
		rs.MoveNext();
	}
	rs.Close();
	return TRUE;
}

void CDataFieldSetDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here
	STR_MSG_DATA *pObject;
	for(int i = 0; i<m_apProcType.GetSize(); i++)
	{
		pObject = (STR_MSG_DATA *)m_apProcType[i];
		if(pObject)
		{
			delete pObject;
			pObject = NULL;
		}
	}
	m_apProcType.RemoveAll();	

}

CString CDataFieldSetDlg::GetProcTypeName(int nType)
{
	STR_MSG_DATA *pData;
	CString strTemp("UnKnown");
	for(int i =0; i<m_apProcType.GetSize(); i++)
	{
		pData = (STR_MSG_DATA *)m_apProcType[i];
		if(pData->nCode == nType)
		{
			strTemp = pData->szMessage;
			break;
		}
	}
	return strTemp;
}
