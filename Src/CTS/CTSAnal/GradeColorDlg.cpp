// GradeColorDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "GradeColorDlg.h"
#include "ColorSelDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGradeColorDlg dialog


CGradeColorDlg::CGradeColorDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CGradeColorDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CGradeColorDlg)
	//}}AFX_DATA_INIT
	ZeroMemory(&m_GradeColor, sizeof(STR_GRADE_COLOR_CONFIG));
}


void CGradeColorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGradeColorDlg)
	DDX_Control(pDX, IDC_LIST_COLOR, m_wndGradeColor);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CGradeColorDlg, CDialog)
	//{{AFX_MSG_MAP(CGradeColorDlg)
	ON_BN_CLICKED(IDC_BUTTON_ADD, OnButtonAdd)
	ON_BN_CLICKED(IDC_BUTTON_DELETE, OnButtonDelete)
	ON_BN_CLICKED(IDC_BUTTON3, OnButton3)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGradeColorDlg message handlers

void CGradeColorDlg::OnButtonAdd() 
{
	// TODO: Add your control notification handler code here

	CColorSelDlg dlg;
	if(dlg.DoModal() == IDOK)
	{
		m_wndGradeColor.AddString(dlg.m_strCode, dlg.m_Color);
		m_wndGradeColor.SetCurSel(m_wndGradeColor.GetCount()-1);
	}
}

void CGradeColorDlg::OnButtonDelete() 
{
	// TODO: Add your control notification handler code here
	int index = m_wndGradeColor.GetCurSel();
	if(index >= 0)
	{
		m_wndGradeColor.DeleteString(index);
		if(m_wndGradeColor.GetCount() > index)
		{
			m_wndGradeColor.SetCurSel(index);
		}
	}
}

void CGradeColorDlg::OnButton3() 
{
	// TODO: Add your control notification handler code here
	int index = m_wndGradeColor.GetCurSel();
	if(index >= 0)
	{
		CColorSelDlg dlg;
		dlg.m_Color = m_wndGradeColor.GetColor(index);
		m_wndGradeColor.GetText(index, dlg.m_strCode);
		if(dlg.DoModal() == IDOK)
		{
			m_wndGradeColor.DeleteString(index);
			m_wndGradeColor.InsertString(index, dlg.m_strCode, dlg.m_Color);
			m_wndGradeColor.SetCurSel(index);
		}
	}
}

BOOL CGradeColorDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	for(int i=0; i<MAX_GRADE_COLOR; i++)
	{
		if(m_GradeColor.colorData[i].bUse)
		{
			m_wndGradeColor.AddString(m_GradeColor.colorData[i].szCode, m_GradeColor.colorData[i].dwColor);
			m_wndGradeColor.SetCurSel(m_wndGradeColor.GetCount()-1);
		}
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CGradeColorDlg::OnOK() 
{
	// TODO: Add extra validation here
	ZeroMemory(&m_GradeColor, sizeof(STR_GRADE_COLOR_CONFIG));

	CString strCode;
	for(int i=0; i<m_wndGradeColor.GetCount(); i++)
	{
		m_GradeColor.colorData[i].dwColor = m_wndGradeColor.GetColor(i);
		m_wndGradeColor.GetText(i, strCode);
		sprintf(m_GradeColor.colorData[i].szCode, strCode);
		m_GradeColor.colorData[i].bUse = 1;

	}
	
	CDialog::OnOK();
}
