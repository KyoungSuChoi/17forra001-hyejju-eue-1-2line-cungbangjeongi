// PlaneSettingDlgBar.cpp : implementation file
//

#include "stdafx.h"
#include "CTSGraphAnal.h"
#include "PlaneSettingDlgBar.h"
#include "CTSGraphAnalDoc.h"
#include "CTSGraphAnalView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPlaneSettingDialogBar

CPlaneSettingDialogBar::CPlaneSettingDialogBar()
{
	//{{AFX_DATA_INIT(CPlaneSettingDialogBar)
	m_fltXAxisGrid = 0.0f;
	m_fltXAxisMax  = 0.0f;
	m_fltXAxisMin  = 0.0f;
	m_fltYAxisGrid = 0.0f;
	m_fltYAxisMax  = 0.0f;
	m_fltYAxisMin  = 0.0f;
	m_strTestName = _T("");
	//}}AFX_DATA_INIT

	m_iCurSelPlaneIndex = CB_ERR;
}

CPlaneSettingDialogBar::~CPlaneSettingDialogBar()
{
}


BEGIN_MESSAGE_MAP(CPlaneSettingDialogBar, CDialogBar)
	//{{AFX_MSG_MAP(CPlaneSettingDialogBar)
	ON_CBN_SELCHANGE(IDC_PLANE_COMBO, OnSelchangePlaneCombo)
	ON_BN_CLICKED(IDC_XAXIS_FONT_BTN, OnXaxisFontBtn)
	ON_BN_CLICKED(IDC_YAXIS_FONT_BTN, OnYaxisFontBtn)
	ON_WM_CONTEXTMENU()
	//}}AFX_MSG_MAP
	ON_WM_DRAWITEM()
	ON_UPDATE_COMMAND_UI(IDC_XAXIS_FONT_BTN, OnUpdateXAxisFont)
	ON_UPDATE_COMMAND_UI(IDC_YAXIS_FONT_BTN, OnUpdateYAxisFont)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CPlaneSettingDialogBar message handlers

void CPlaneSettingDialogBar::DoDataExchange(CDataExchange* pDX) 
{
	CDialogBar::DoDataExchange(pDX);

	//{{AFX_DATA_MAP(CPlaneSettingDialogBar)
	DDX_Control(pDX, IDC_LINE_LIST, m_LineListBox);
	DDX_Control(pDX, IDC_PLANE_COMBO, m_PlaneCombo);
	DDX_Text(pDX, IDC_XAXIS_GRID_EDIT, m_fltXAxisGrid);
	DDX_Text(pDX, IDC_XAXIS_MAX_EDIT,  m_fltXAxisMax);
	DDX_Text(pDX, IDC_XAXIS_MIN_EDIT,  m_fltXAxisMin);
	DDX_Text(pDX, IDC_YAXIS_GRID_EDIT, m_fltYAxisGrid);
	DDX_Text(pDX, IDC_YAXIS_MAX_EDIT,  m_fltYAxisMax);
	DDX_Text(pDX, IDC_YAXIS_MIN_EDIT,  m_fltYAxisMin);
	DDX_Text(pDX, IDC_EDIT_TESTNAME, m_strTestName);
	//}}AFX_DATA_MAP

	if(pDX->m_bSaveAndValidate)
	{

	}
}

BOOL CPlaneSettingDialogBar::Create(CWnd* pParentWnd, UINT nIDTemplate, UINT nStyle, UINT nID )
{
	// TODO: Add your specialized code here and/or call the base class
	
	if(!CDialogBar::Create(pParentWnd, nIDTemplate, nStyle, nID)) return FALSE;

	//
	UpdateData(FALSE);

	//
	return TRUE;
}

void CPlaneSettingDialogBar::AddPlaneIntoComboBox()
{
	CCTSGraphAnalDoc* pDoc = (CCTSGraphAnalDoc*)(GetParentFrame()->GetActiveDocument());
	//
	GetDlgItem(IDC_XAXIS_EDIT)->SetWindowText("");
	m_PlaneCombo.ResetContent();
	//
	POSITION pos = pDoc->m_Data.GetHeadPlanePos();
	BOOL bCheck = pos!=NULL;
	while(pos)
	{
		CPlane* pPlane = pDoc->m_Data.GetNextPlane(pos);
		CString str;
		str.Format("%s(%s)", pPlane->GetXAxis()->GetPropertyTitle(),
			                 pPlane->GetXAxis()->GetUnitNotation());
		GetDlgItem(IDC_XAXIS_EDIT)->SetWindowText(str);
		str.Format("%s(%s)", pPlane->GetYAxis()->GetPropertyTitle(),
							 pPlane->GetYAxis()->GetUnitNotation());
		m_PlaneCombo.AddString(str);
	}
	//
	if(bCheck)
	{
		m_PlaneCombo.SetCurSel(0);
		OnSelchangePlaneCombo();
	}

	SetTestName(pDoc->m_Data.GetTestName());
}

void CPlaneSettingDialogBar::OnSelchangePlaneCombo() 
{
	m_iCurSelPlaneIndex = m_PlaneCombo.GetCurSel();
	if(m_iCurSelPlaneIndex!=CB_ERR)
	{
		CCTSGraphAnalDoc* pDoc = (CCTSGraphAnalDoc*)(GetParentFrame()->GetActiveDocument());
		CPlane* pPlane     = pDoc->m_Data.GetPlaneAt(m_iCurSelPlaneIndex);
		//
		StandLinesInALine(pPlane);
		//
		m_fltXAxisMax  = pPlane->GetXAxis()->ConvertUnit(pPlane->GetXAxis()->GetMax());
		m_fltXAxisMin  = pPlane->GetXAxis()->ConvertUnit(pPlane->GetXAxis()->GetMin());
		m_fltXAxisGrid = pPlane->GetXAxis()->ConvertUnit(pPlane->GetXAxis()->GetGrid());
		m_fltYAxisMax  = pPlane->GetYAxis()->ConvertUnit(pPlane->GetYAxis()->GetMax());
		m_fltYAxisMin  = pPlane->GetYAxis()->ConvertUnit(pPlane->GetYAxis()->GetMin());
		m_fltYAxisGrid = pPlane->GetYAxis()->ConvertUnit(pPlane->GetYAxis()->GetGrid());
		UpdateData(FALSE);
		//
		Invalidate(TRUE);
	}
}

void CPlaneSettingDialogBar::StandLinesInALine(CPlane* pPlane)
{
	if(m_iCurSelPlaneIndex!=CB_ERR)
	{
		m_LineListBox.ResetContent();
		POSITION pos = pPlane->GetLineHead();
		while(pos)
		{
			CLine* pLine = pPlane->GetNextLine(pos);
			CString str;
			str.Format("%s,%d,%d,%d,%d,%d",
				pLine->GetName(),
				GetRValue(pLine->GetColor()),
				GetGValue(pLine->GetColor()),
				GetBValue(pLine->GetColor()),
				pLine->GetWidth(),
				pLine->GetType());
			m_LineListBox.AddString(str);
		}
	}
}

void CPlaneSettingDialogBar::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	if(m_iCurSelPlaneIndex!=CB_ERR)
	{
		CCTSGraphAnalDoc* pDoc = (CCTSGraphAnalDoc*)(GetParentFrame()->GetActiveDocument());
		CPlane* pPlane     = pDoc->m_Data.GetPlaneAt(m_iCurSelPlaneIndex);
		if(lpDrawItemStruct->CtlID==IDC_XAXIS_FONT_BTN)
		{
			CDC* pDC=CDC::FromHandle(lpDrawItemStruct->hDC);
			CFont aFont, *oldFont;
			aFont.CreateFontIndirect(pPlane->GetXAxis()->GetFont());
			oldFont=pDC->SelectObject(&aFont);
			pDC->Rectangle(&(lpDrawItemStruct->rcItem));
			pDC->SetTextColor(pPlane->GetXAxis()->GetLabelColor());
			pDC->DrawText("����ABab",&(lpDrawItemStruct->rcItem),DT_SINGLELINE|DT_CENTER|DT_VCENTER);
			pDC->SelectObject(oldFont);
			aFont.DeleteObject();
		}
		if(lpDrawItemStruct->CtlID==IDC_YAXIS_FONT_BTN)
		{
			CDC* pDC=CDC::FromHandle(lpDrawItemStruct->hDC);
			CFont aFont, *oldFont;
			aFont.CreateFontIndirect(pPlane->GetYAxis()->GetFont());
			oldFont=pDC->SelectObject(&aFont);
			pDC->Rectangle(&(lpDrawItemStruct->rcItem));
			pDC->SetTextColor(pPlane->GetYAxis()->GetLabelColor());
			pDC->DrawText("����ABab",&(lpDrawItemStruct->rcItem),DT_SINGLELINE|DT_CENTER|DT_VCENTER);
			pDC->SelectObject(oldFont);
			aFont.DeleteObject();
		}
	}
	//
	CDialogBar::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CPlaneSettingDialogBar::OnXaxisFontBtn() 
{
	if(m_iCurSelPlaneIndex!=CB_ERR)
	{
		CCTSGraphAnalDoc* pDoc = (CCTSGraphAnalDoc*)(GetParentFrame()->GetActiveDocument());
		CPlane* pPlane     = pDoc->m_Data.GetPlaneAt(m_iCurSelPlaneIndex);
		LOGFONT afont = *(pPlane->GetXAxis()->GetFont());
		CFontDialog aDlg(&afont);
		aDlg.m_cf.rgbColors = pPlane->GetYAxis()->GetLabelColor();
		if(aDlg.DoModal()==IDOK)
		{
			aDlg.GetCurrentFont(&afont);
			POSITION pos = pDoc->m_Data.GetHeadPlanePos();
			while(pos)
			{
				CPlane* pPlane = pDoc->m_Data.GetNextPlane(pos);
				*(pPlane->GetXAxis()->GetFont()) = afont;
				pPlane->GetXAxis()->SetLabelColor(aDlg.GetColor());
			}
			//
			pDoc->UpdateAllViews(NULL);
			Invalidate(TRUE);
		}
	}	
}

void CPlaneSettingDialogBar::OnYaxisFontBtn() 
{
	if(m_iCurSelPlaneIndex!=CB_ERR)
	{
		CCTSGraphAnalDoc* pDoc = (CCTSGraphAnalDoc*)(GetParentFrame()->GetActiveDocument());
		CPlane* pPlane     = pDoc->m_Data.GetPlaneAt(m_iCurSelPlaneIndex);
		LOGFONT afont = *(pPlane->GetYAxis()->GetFont());
		CFontDialog aDlg(&afont);
		aDlg.m_cf.rgbColors = pPlane->GetYAxis()->GetLabelColor();
		if(aDlg.DoModal()==IDOK)
		{
			aDlg.GetCurrentFont(&afont);
			*(pPlane->GetYAxis()->GetFont()) = afont;
			pPlane->GetYAxis()->SetLabelColor(aDlg.GetColor());
			//
			pDoc->UpdateAllViews(NULL);
			Invalidate(TRUE);
		}
	}	
}

void CPlaneSettingDialogBar::OnUpdateXAxisFont(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}

void CPlaneSettingDialogBar::OnUpdateYAxisFont(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}

BOOL CPlaneSettingDialogBar::PreTranslateMessage(MSG* pMsg) 
{
	if(pMsg->message==WM_KEYDOWN)
	{
		if(pMsg->wParam==VK_RETURN)		
		{
			if     (GetFocus()->GetDlgCtrlID()==IDC_XAXIS_MIN_EDIT)
			{
				GetDlgItem(IDC_XAXIS_MAX_EDIT)->SetFocus();
				((CEdit*)GetDlgItem(IDC_XAXIS_MAX_EDIT))->SetSel(0,-1);
				return TRUE;
			}
			else if(GetFocus()->GetDlgCtrlID()==IDC_XAXIS_MAX_EDIT)
			{
				GetDlgItem(IDC_XAXIS_GRID_EDIT)->SetFocus();
				((CEdit*)GetDlgItem(IDC_XAXIS_GRID_EDIT))->SetSel(0,-1);
				return TRUE;
			}
			else if(GetFocus()->GetDlgCtrlID()==IDC_XAXIS_GRID_EDIT)
			{
				if(UpdateData(TRUE))
				{
					if(m_iCurSelPlaneIndex!=CB_ERR)
					{
						CCTSGraphAnalDoc* pDoc = (CCTSGraphAnalDoc*)(GetParentFrame()->GetActiveDocument());
						POSITION pos = pDoc->m_Data.GetHeadPlanePos();
						while(pos)
						{
							CPlane* pPlane     = pDoc->m_Data.GetNextPlane(pos);
							//
							pPlane->GetXAxis()->SetMinMax( pPlane->GetXAxis()->ReverseUnit(m_fltXAxisMin),
								                           pPlane->GetXAxis()->ReverseUnit(m_fltXAxisMax),
														   pPlane->GetXAxis()->ReverseUnit(m_fltXAxisGrid));
						}
						//
						pDoc->UpdateAllViews(NULL);
					}	
				}
			}
			else if(GetFocus()->GetDlgCtrlID()==IDC_YAXIS_MIN_EDIT)
			{
				GetDlgItem(IDC_YAXIS_MAX_EDIT)->SetFocus();
				((CEdit*)GetDlgItem(IDC_YAXIS_MAX_EDIT))->SetSel(0,-1);
				return TRUE;
			}
			else if(GetFocus()->GetDlgCtrlID()==IDC_YAXIS_MAX_EDIT)
			{
				GetDlgItem(IDC_YAXIS_GRID_EDIT)->SetFocus();
				((CEdit*)GetDlgItem(IDC_YAXIS_GRID_EDIT))->SetSel(0,-1);
				return TRUE;
			}
			else if(GetFocus()->GetDlgCtrlID()==IDC_YAXIS_GRID_EDIT)
			{
				if(UpdateData(TRUE))
				{
					if(m_iCurSelPlaneIndex!=CB_ERR)
					{
						CCTSGraphAnalDoc* pDoc = (CCTSGraphAnalDoc*)(GetParentFrame()->GetActiveDocument());
						CPlane* pPlane     = pDoc->m_Data.GetPlaneAt(m_iCurSelPlaneIndex);
						//
						pPlane->GetYAxis()->SetMinMax( pPlane->GetYAxis()->ReverseUnit(m_fltYAxisMin),
							                           pPlane->GetYAxis()->ReverseUnit(m_fltYAxisMax),
													   pPlane->GetYAxis()->ReverseUnit(m_fltYAxisGrid));
						//
						pDoc->UpdateAllViews(NULL);
					}	
				}
			}
		}
	}

	return CDialogBar::PreTranslateMessage(pMsg);
}

void CPlaneSettingDialogBar::OnContextMenu(CWnd* pWnd, CPoint point) 
{
	if(m_iCurSelPlaneIndex==CB_ERR) return;

	if(m_LineListBox.GetSelCount()==0) return;

	if(pWnd->IsKindOf(RUNTIME_CLASS(CLineListBox)))
	{
		CMenu aMenu;
		aMenu.LoadMenu(IDR_CONTEXT_MENU);
		aMenu.GetSubMenu(1)->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON,point.x,point.y,AfxGetMainWnd());
	}
}

void CPlaneSettingDialogBar::SetLineWidth(int width)
{
	if(m_iCurSelPlaneIndex!=CB_ERR){
		CCTSGraphAnalDoc* pDoc = (CCTSGraphAnalDoc*)(GetParentFrame()->GetActiveDocument());
		CPlane* pPlane     = pDoc->m_Data.GetPlaneAt(m_iCurSelPlaneIndex);
		int N = m_LineListBox.GetCount();
		BOOL Check=FALSE;
		for(int i=0;i<N;i++){
			if(m_LineListBox.GetSel(i)){
				CLine *pLine = pPlane->GetLineAt(i);
				pLine->SetWidth(width);
				Check=TRUE;
			}
		}
		if(Check){
			pDoc->UpdateAllViews(NULL);
			StandLinesInALine(pPlane);
		}
	}
}

void CPlaneSettingDialogBar::SetLineType(int type)
{
	if(m_iCurSelPlaneIndex!=CB_ERR){
		CCTSGraphAnalDoc* pDoc = (CCTSGraphAnalDoc*)(GetParentFrame()->GetActiveDocument());
		CPlane* pPlane     = pDoc->m_Data.GetPlaneAt(m_iCurSelPlaneIndex);
		int N = m_LineListBox.GetCount();
		BOOL Check=FALSE;
		for(int i=0;i<N;i++){
			if(m_LineListBox.GetSel(i)){
				CLine *pLine = pPlane->GetLineAt(i);
				pLine->SetType(type);
				Check=TRUE;
			}
		}
		if(Check){
			pDoc->UpdateAllViews(NULL);
			StandLinesInALine(pPlane);
		}
	}
}

void CPlaneSettingDialogBar::SetLineColor()
{
	if(m_iCurSelPlaneIndex!=CB_ERR)
	{
		CCTSGraphAnalDoc* pDoc = (CCTSGraphAnalDoc*)(GetParentFrame()->GetActiveDocument());
		CPlane* pPlane     = pDoc->m_Data.GetPlaneAt(m_iCurSelPlaneIndex);
		int cursel=0;
		if(m_LineListBox.GetSelItems(1,&cursel)==1)
		{
			CLine *pLine = pPlane->GetLineAt(cursel);
			CColorDialog aDlg;
			aDlg.m_cc.rgbResult=pLine->GetColor();
			aDlg.m_cc.Flags|=CC_FULLOPEN|CC_RGBINIT;
			if(aDlg.DoModal()==IDOK)
			{
				pLine->SetColor(aDlg.GetColor());
				pDoc->UpdateAllViews(NULL);
				StandLinesInALine(pPlane);
			}
		}
	}
}

int CPlaneSettingDialogBar::GetSelLineCount()
{
	if(m_iCurSelPlaneIndex==CB_ERR) return 0;
	else                            return m_LineListBox.GetSelCount();
}

void CPlaneSettingDialogBar::TrackLine()
{
	if(m_iCurSelPlaneIndex!=CB_ERR)
	{
		int iCurSelLineIndex=0;
		if(m_LineListBox.GetSelItems(1,&iCurSelLineIndex)==1)
		{
			CCTSGraphAnalView* pView = (CCTSGraphAnalView*)(GetParentFrame()->GetActiveView());
			pView->OnToolsLinetrack(m_iCurSelPlaneIndex,iCurSelLineIndex);
			pView->SetFocus();
		}
	}
}

void CPlaneSettingDialogBar::UpdateAxesRange()
{
	if(m_iCurSelPlaneIndex!=CB_ERR)
	{
		CCTSGraphAnalDoc* pDoc = (CCTSGraphAnalDoc*)(GetParentFrame()->GetActiveDocument());
		CPlane* pPlane     = pDoc->m_Data.GetPlaneAt(m_iCurSelPlaneIndex);
		m_fltXAxisMax  = pPlane->GetXAxis()->ConvertUnit(pPlane->GetXAxis()->GetMax());
		m_fltXAxisMin  = pPlane->GetXAxis()->ConvertUnit(pPlane->GetXAxis()->GetMin());
		m_fltXAxisGrid = pPlane->GetXAxis()->ConvertUnit(pPlane->GetXAxis()->GetGrid());
		m_fltYAxisMax  = pPlane->GetYAxis()->ConvertUnit(pPlane->GetYAxis()->GetMax());
		m_fltYAxisMin  = pPlane->GetYAxis()->ConvertUnit(pPlane->GetYAxis()->GetMin());
		m_fltYAxisGrid = pPlane->GetYAxis()->ConvertUnit(pPlane->GetYAxis()->GetGrid());
		UpdateData(FALSE);
	}
}

void CPlaneSettingDialogBar::ShowPattern()
{
	if(m_iCurSelPlaneIndex!=CB_ERR)
	{
		int iCurSelLineIndex=0;
		if(m_LineListBox.GetSelItems(1,&iCurSelLineIndex)==1)
		{
			CCTSGraphAnalView* pView = (CCTSGraphAnalView*)(GetParentFrame()->GetActiveView());
			pView->OnToolsShowLinePattern(m_iCurSelPlaneIndex,iCurSelLineIndex);
		}
	}
}

void CPlaneSettingDialogBar::SetTestName(CString strTestName)
{
	strTestName.MakeUpper();
	m_strTestName = strTestName;
	UpdateData(FALSE);
}

CString CPlaneSettingDialogBar::GetTestName()
{
	UpdateData();
	return m_strTestName;
}
