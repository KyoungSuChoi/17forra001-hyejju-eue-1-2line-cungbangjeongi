#if !defined(AFX_DATADOC_H__D72D554C_2FD4_41A7_BC7E_3430D14DAFDF__INCLUDED_)
#define AFX_DATADOC_H__D72D554C_2FD4_41A7_BC7E_3430D14DAFDF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DataDoc.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDataDoc document
#include "TextParsing.h"

class CDataDoc : public CDocument
{
protected:
	CDataDoc();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CDataDoc)

// Attributes
public:

// Operations
public:

	CString *TEXT_LANG;	
	BOOL LanguageinitMonConfig();



// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDataDoc)
	public:
	virtual void Serialize(CArchive& ar);   // overridden for document i/o
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	protected:
	virtual BOOL OnNewDocument();
	//}}AFX_VIRTUAL

// Implementation
public:
	float CalImpedance(CString &strName, float fTime1, float fTime2);
//	CString GetExcelPath();
	BOOL DeleteFolder(CString strFolder);
	BOOL SelectTestDlg();
	CChData * GetChData();
	BOOL SetChPath(CString strChPath);
	BOOL GetCellLastState(CString strChPath = "");
	void SetTestList(CStringList *pstrList);
//	void SetDataPath(CString strPathName)	{ m_strDataPath = strPathName;	};
//	CData		m_Data;
	CChData		m_ChData;
//	CString GetDataPath()			{	return m_strDataPath;	};

	CStringList * GetSelFileList();
	virtual ~CDataDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
//	CString m_strDataPath;
	int FindPointData(CTextParsing *pSheet, float fTime, float &fData1, float &fData2, BOOL bUseFitLine = FALSE, BOOL bUsePrevData = TRUE);
	CStringList m_strFileList;
	//{{AFX_MSG(CDataDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATADOC_H__D72D554C_2FD4_41A7_BC7E_3430D14DAFDF__INCLUDED_)
