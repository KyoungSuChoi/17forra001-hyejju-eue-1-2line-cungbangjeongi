#pragma once

#include "FMSCriticalSection.h"
#include "FMSSyncParent.h"
#include "FMSCircularQueue.h"

class CFMS_SM : public CFMSSyncParent<CFMS_SM>
{
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

public:
	CFMS_SM(void);
	virtual ~CFMS_SM(void);

	BOOL		m_bRunChk;
	INT			m_nDeviceID;
	INT			fnGetDeviceID() { return m_nDeviceID; }
	INT			fnGetStageIdx( int nMachineID );

	VOID		FMS_MAINProcessCallback(VOID);
	VOID		FMS_SMProcessCallback(VOID);
	BOOL		fnBegin(HWND, CPtrArray&, int nDeviceID);
	BOOL		fnEnd();
	
	BOOL		fnSetEMG(INT moduleID, CString emgCode);
	BOOL		fnRsponseSet(INT moduleID, EP_RESPONSE& _response);

	FMS_STATE_CODE fnGetFMSStateForAllSystemView(INT moduleID);

	BOOL		fnClearReset(INT moduleID);
	BOOL		fnClearColor(INT moduleID);
	BOOL		fnTrayInStateReset(INT moduleID);
	BOOL		fnEndStateReset(INT moduleID);
	
	std::vector<CFM_Unit *> m_vUnit;
	BOOL		m_bMisSendWorking;
	INT			m_nMisWorkingUnitNum;
	
private:
	CFMS		m_Not_Module_Fms;
private:
	HWND		m_Papa;
	CString		m_strFMID;
	CString		m_strFMUnitID;
	BOOL		m_bWork;
private:

	BOOL		m_bTreadStop;
	INT			mTcpTheadIndex;
	std::vector<HANDLE> m_vUnitThread;
	std::vector<HANDLE> m_vUnitProcEvent;

	HANDLE		mFMS_StartProcessHandle;

	HANDLE		mFMSMainThreadHandle;
	HANDLE		mFMSMainThreadDestroyEvent;
private:
	CFMSCircularQueue<st_FMS_PACKET> m_FMS_Q[MAX_SBC_COUNT];

private:

};
