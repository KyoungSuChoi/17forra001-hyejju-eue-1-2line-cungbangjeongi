// SerarchDataDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "SerarchDataDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSerarchDataDlg dialog


bool CSerarchDataDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CSerarchDataDlg"), _T("TEXT_CSerarchDataDlg_CNT"), _T("TEXT_CSerarchDataDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CSerarchDataDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CSerarchDataDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

CSerarchDataDlg::CSerarchDataDlg(CCTSAnalDoc *pDoc, CWnd* pParent /*=NULL*/)
	: CDialog(CSerarchDataDlg::IDD, pParent)
{
	LanguageinitMonConfig();

	//{{AFX_DATA_INIT(CSerarchDataDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_pDoc = pDoc;
	m_aFile = NULL;
	m_strTitle = "Data List";
}
CSerarchDataDlg::~CSerarchDataDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


void CSerarchDataDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSerarchDataDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSerarchDataDlg, CDialog)
	//{{AFX_MSG_MAP(CSerarchDataDlg)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUTTON_EXCEL, OnButtonExcel)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSerarchDataDlg message handlers

void CSerarchDataDlg::InitGrid()
{
	m_wndDataListGrid.SubclassDlgItem(IDC_CUSTOM_DATA_GRID, this);
	//m_wndBoardListGrid.m_bRowSelection = TRUE;
	m_wndDataListGrid.m_bSameColSize  = FALSE;
	m_wndDataListGrid.m_bSameRowSize  = FALSE;
	//m_wndDataListGrid.m_bCustomWidth  = TRUE;

	m_wndDataListGrid.Initialize();
//	m_wndDataListGrid.SetRowCount(0);
	m_wndDataListGrid.SetColCount(m_pDoc->m_TotCol+3);	//Lot, Tray
	m_wndDataListGrid.SetDefaultRowHeight(18);
	m_wndDataListGrid.GetParam()->SetSortRowsOnDblClk(TRUE);
	m_wndDataListGrid.m_BackColor	= RGB(255,255,255);
	m_wndDataListGrid.SetColWidth(0,0, 50);

	m_wndDataListGrid.SetFrozenCols(3);
	m_wndDataListGrid.SetStyleRange(CGXRange().SetCols(3), CGXStyle().SetBorders(gxBorderRight, CGXPen().SetWidth(2).SetColor(RGB(0,0,255))));

	m_wndDataListGrid.m_bCustomColor 	= FALSE;
	for(int i=0; i<m_pDoc->m_TotCol; i++)
	{
		m_wndDataListGrid.SetValueRange(CGXRange(0, i+1+3),  m_pDoc->m_field[i].FieldName);
	}
	m_wndDataListGrid.SetValueRange(CGXRange(0, 1),  "LOT");
	m_wndDataListGrid.SetValueRange(CGXRange(0, 2),  "TRAY");
	m_wndDataListGrid.SetValueRange(CGXRange(0, 3),  "CellNo");

	m_wndDataListGrid.EnableCellTips();
	m_wndDataListGrid.SetStyleRange(CGXRange().SetTable(), CGXStyle().SetWrapText(FALSE));
//	m_wndDataListGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	m_wndDataListGrid.Redraw();
}

BOOL CSerarchDataDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	InitGrid();

	//Load Data
	SearchDataA();

	GetDlgItem(IDC_STATIC_TITLE)->SetWindowText(m_strTitle);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSerarchDataDlg::SetFileName(CStringArray *aFileName)
{
	m_aFile = aFileName;
}

void CSerarchDataDlg::SearchData()
{
	if(m_aFile == NULL)		return;

	STR_TOP_CONFIG *pConfig = m_pDoc->GetTopConfig();
	
	CString strRltFile, strTemp, strCode, strCode2;
	STR_STEP_RESULT *lpStepDataInfo;
	STR_SAVE_CH_DATA *lpChannelData;
	CFormResultFile *pRltFile = new CFormResultFile;
	
	BYTE colorFlag = 0;
	ROWCOL nRow, nCol, nTotRow;
	CWordArray aSelChArray;
	BOOL bSelChannel = FALSE;
	
	for(int s=0; s<m_aFile->GetSize(); s++)
	{
		strRltFile = m_aFile->GetAt(s);
		if(pRltFile->ReadFile(strRltFile))
		{
			//같은 Lot에서 같은 tray 번호는 한 공정을 진행중인 Tray 임 => 수정 필요 20070322

			BOOL bInsertRow = FALSE;
			bSelChannel = FALSE;
			nTotRow = m_wndDataListGrid.GetRowCount();
			for(int step = 0; step<pRltFile->GetStepSize(); step++)
			{
				lpStepDataInfo = pRltFile->GetStepData(step);
				if(lpStepDataInfo)
				{
					//표기할 Channel List를 구함 
					if(bSelChannel == FALSE)
					{
						bSelChannel = TRUE;
						aSelChArray.RemoveAll();
						for(int a = 0; a<lpStepDataInfo->aChData.GetSize(); a++)
						{
							lpChannelData = (STR_SAVE_CH_DATA *)lpStepDataInfo->aChData[a];
//							if(lpChannelData->channelCode != EP_CODE_NONCELL && lpChannelData->channelCode != EP_CODE_CELL_NONE)
//							{
								aSelChArray.Add(a);
//							}
						}
					}

					//find matting field
					for(int f = 0; f<m_pDoc->m_TotCol; f++)
					{
						nCol = f+1+3;
						//기본적으로 Grading code와 Channel Code 는 표기 한다.
						if( lpStepDataInfo->stepCondition.stepHeader.nProcType == m_pDoc->m_field[f].ID || 
							m_pDoc->m_field[f].ID == EP_REPORT_GRADING || m_pDoc->m_field[f].ID == EP_REPORT_CH_CODE)
					
						{
							for(int c = 0; c<aSelChArray.GetSize(); c++)
							{
								if(bInsertRow == FALSE)
								{
									bInsertRow = TRUE;
									m_wndDataListGrid.InsertRows(nTotRow+1, aSelChArray.GetSize());
								}
								
								nRow = nTotRow+c+1;
								lpChannelData = (STR_SAVE_CH_DATA *)lpStepDataInfo->aChData[aSelChArray.GetAt(c)];

								//find matting data type
								switch(m_pDoc->m_field[f].DataType)
								{
								case EP_VOLTAGE:
									strTemp.Format("%s", m_pDoc->ValueString(lpChannelData->fVoltage, m_pDoc->m_field[f].DataType));
									break;
								case EP_CURRENT:
									strTemp.Format("%s", m_pDoc->ValueString(lpChannelData->fCurrent, m_pDoc->m_field[f].DataType));
									break;
								case EP_CAPACITY:
									strTemp.Format("%s", m_pDoc->ValueString(lpChannelData->fCapacity, m_pDoc->m_field[f].DataType));
									break;
								case EP_STEP_TIME:
									strTemp.Format("%s", m_pDoc->ValueString(lpChannelData->fStepTime, m_pDoc->m_field[f].DataType));
									break;
								case EP_IMPEDANCE:
									strTemp.Format("%s", m_pDoc->ValueString(lpChannelData->fImpedance, m_pDoc->m_field[f].DataType));
									break;
								case EP_CH_CODE:
									strTemp.Format("%s", m_pDoc->ValueString(lpChannelData->channelCode, m_pDoc->m_field[f].DataType));
									break;
								case EP_GRADE_CODE:
									strCode = m_wndDataListGrid.GetValueRowCol(nRow, nCol);
									strCode2 = m_pDoc->ValueString(lpChannelData->grade, m_pDoc->m_field[f].DataType);
									if(strCode.IsEmpty())
									{
										strTemp.Format("%s", strCode2);
									}
									else
									{
										if(strCode2.IsEmpty())
										{
											strTemp.Format("%s", strCode);
										}
										else
										{
											strTemp.Format("%s-%s", strCode, strCode2);
										}
									}
									break;
								}

								m_wndDataListGrid.SetValueRange(CGXRange(nRow, 1), pRltFile->GetLotNo());
								m_wndDataListGrid.SetValueRange(CGXRange(nRow, 2), pRltFile->GetTrayNo());
								m_wndDataListGrid.SetValueRange(CGXRange(nRow, 3), (long)(lpChannelData->wChIndex+1));	
								m_pDoc->GetStateMsg(lpChannelData->state, colorFlag);
								if(m_pDoc->m_bHideNonCellData && (lpChannelData->channelCode == EP_CODE_CELL_NONE || lpChannelData->channelCode == EP_CODE_NONCELL ))
								{
									strTemp.Empty();
									m_wndDataListGrid.SetStyleRange(CGXRange(nRow, nCol), 
																		CGXStyle()
																		.SetValue(strTemp)
																		.SetTextColor(0)
																		.SetInterior(RGB(255,255,255)));
								}
								else
								{
									if(::IsNormalCell(lpChannelData->channelCode) == FALSE)
									{
										m_wndDataListGrid.SetStyleRange(CGXRange(nRow, nCol),					
																CGXStyle()
																.SetValue(strTemp)
																.SetTextColor(pConfig->m_TStateColor[colorFlag])
																.SetInterior(pConfig->m_BStateColor[colorFlag]));
									}
									else
									{
										m_wndDataListGrid.SetStyleRange(CGXRange(nRow, nCol), 
															CGXStyle()
															.SetValue(strTemp)
															.SetTextColor(0)
															.SetInterior(RGB(255,255,255)));
									}
								}
							}
						}
					}
				}
			}
		}
	}
	delete pRltFile;
	pRltFile = NULL;
}

void CSerarchDataDlg::SearchDataA()
{
	if(m_aFile == NULL)		return;

	CWaitCursor wait;

	CString strRltFile;
	CFormResultFile *pRltFile = new CFormResultFile;
	ROWCOL nStartRow;
	CStringArray aFileArray;
	aFileArray.Append(*m_aFile);
	CStringArray aTrayArray;
	CStringArray aLotArray;
	CString strLot, strTray;

	//각 파일의 Lot 번호와 Tray 번호 추출 
	for(int s=0; s<aFileArray.GetSize(); s++)
	{
		if(CFormResultFile::GetLotTrayNo(aFileArray.GetAt(s), strLot, strTray))
		{
			aTrayArray.Add(strTray);
			aLotArray.Add(strLot);
		}
		else
		{
			aTrayArray.Add("");
			aLotArray.Add("");
		}
	}
	//////////////////////////////////////////////////////////////////////////

	//같은 Lot에서 같은 tray 번호는 한 공정을 진행중인 Tray 임
	while(aFileArray.GetSize()>0)
	{
		ASSERT(aFileArray.GetSize() == aLotArray.GetSize());
		ASSERT(aLotArray.GetSize() == aTrayArray.GetSize());

		//Get First file name
		strRltFile = aFileArray.GetAt(0);
		if(pRltFile->ReadFile(strRltFile) == FALSE)		
		{
			aFileArray.RemoveAt(0);
			continue;
		}
		
		//Add Grid Row
		nStartRow = m_wndDataListGrid.GetRowCount()+1;
		m_wndDataListGrid.InsertRows(nStartRow, pRltFile->GetTotalCh());

		//1개의 Step도 진행 안한 Data도 Display 한다.
		for(int r =0; r<pRltFile->GetTotalCh(); r++)
		{
			m_wndDataListGrid.SetValueRange(CGXRange(nStartRow+r, 1), pRltFile->GetLotNo());
			m_wndDataListGrid.SetValueRange(CGXRange(nStartRow+r, 2), pRltFile->GetTrayNo());
			m_wndDataListGrid.SetValueRange(CGXRange(nStartRow+r, 3), (long)(r+1));	
		}

		//DisplayResultData(strRltFile)
		DisplayData(strRltFile, nStartRow);
		TRACE("Display file data %s, %s\n", pRltFile->GetLotNo(), pRltFile->GetTrayNo());

		//나머지 파일중에서 TrayNo가 같고 LotNo가 같은 파일은 같은 Schedule을 진행한 Tray이므로 같은 Row 상에 File을 찾아서 표시 
		if(aFileArray.GetSize() > 1)
		{
			for(int f=1; f<aFileArray.GetSize(); f++)
			{
				if(aLotArray.GetAt(f) == pRltFile->GetLotNo() && aTrayArray.GetAt(f) == pRltFile->GetTrayNo())
				{
					DisplayData(aFileArray.GetAt(f), nStartRow);
					TRACE("Display file data1 %s, %s\n", pRltFile->GetLotNo(), pRltFile->GetTrayNo());
				}
			}
		}

		//Remove data array
		//////////////////////////////////////////////////////////////////////////
		//1. Remove  current index
		aFileArray.RemoveAt(0);
		aLotArray.RemoveAt(0);
		aTrayArray.RemoveAt(0);

		//2. 중복된 Lot, Tray번호를 Array에서 삭제 한다.
		int nFile = aFileArray.GetSize();
		for(int n=nFile-1; n>=0; n--)
		{
			if(aLotArray[n] == pRltFile->GetLotNo() && aTrayArray[n] == pRltFile->GetTrayNo())
			{
				aFileArray.RemoveAt(n);
				aLotArray.RemoveAt(n);
				aTrayArray.RemoveAt(n);
			}
		}
		//////////////////////////////////////////////////////////////////////////
	}
	delete pRltFile;
	pRltFile = NULL;
}

void CSerarchDataDlg::DisplayData(CString strFile, int nStartRow)
{
	ASSERT(nStartRow > 0 && nStartRow <= m_wndDataListGrid.GetRowCount());

	STR_TOP_CONFIG *pConfig = m_pDoc->GetTopConfig();
	
	CString strRltFile, strTemp, strCode, strCode2;
	STR_STEP_RESULT *lpStepDataInfo;
	STR_SAVE_CH_DATA *lpChannelData;
	CFormResultFile *pRltFile = new CFormResultFile;
	
	BYTE colorFlag = 0;
	ROWCOL nRow, nCol;//, nTotRow;
	CWordArray aSelChArray;
	BOOL bSelChannel = FALSE;
	
	if(pRltFile->ReadFile(strFile))
	{
		for(int step = 0; step<pRltFile->GetStepSize(); step++)	
		{
			lpStepDataInfo = pRltFile->GetStepData(step);
			if(lpStepDataInfo)
			{
				//find matting field
				for(int f = 0; f<m_pDoc->m_TotCol; f++)
				{
					nCol = f+1+3;
					//기본적으로 Grading code와 Channel Code 는 표기 한다.
					if( lpStepDataInfo->stepCondition.stepHeader.nProcType == m_pDoc->m_field[f].ID || 
						m_pDoc->m_field[f].ID == EP_REPORT_GRADING || m_pDoc->m_field[f].ID == EP_REPORT_CH_CODE)
				
					{
						for(int c = 0; c<lpStepDataInfo->aChData.GetSize(); c++)
						{
							nRow = nStartRow+c;
							lpChannelData = (STR_SAVE_CH_DATA *)lpStepDataInfo->aChData[c];

							//find matting data type
							switch(m_pDoc->m_field[f].DataType)
							{
								case EP_VOLTAGE:
									strTemp.Format("%s", m_pDoc->ValueString(lpChannelData->fVoltage, m_pDoc->m_field[f].DataType));
									break;
								case EP_CURRENT:
									strTemp.Format("%s", m_pDoc->ValueString(lpChannelData->fCurrent, m_pDoc->m_field[f].DataType));
									break;
								case EP_CAPACITY:
									strTemp.Format("%s", m_pDoc->ValueString(lpChannelData->fCapacity, m_pDoc->m_field[f].DataType));
									break;
								case EP_STEP_TIME:
									strTemp.Format("%s", m_pDoc->ValueString(lpChannelData->fStepTime, m_pDoc->m_field[f].DataType));
									break;
								case EP_IMPEDANCE:
									strTemp.Format("%s", m_pDoc->ValueString(lpChannelData->fImpedance, m_pDoc->m_field[f].DataType));
									break;
								case EP_CH_CODE:
									strTemp.Format("%s", m_pDoc->ValueString(lpChannelData->channelCode, m_pDoc->m_field[f].DataType));
									break;
								case EP_GRADE_CODE:
									strCode = m_wndDataListGrid.GetValueRowCol(nRow, nCol);
									strCode2 = m_pDoc->ValueString(lpChannelData->grade, m_pDoc->m_field[f].DataType);
									if(strCode.IsEmpty())
									{
										strTemp.Format("%s", strCode2);
									}
									else
									{
										if(strCode2.IsEmpty())
										{
											strTemp.Format("%s", strCode);
										}
										else
										{
											strTemp.Format("%s-%s", strCode, strCode2);
										}
									}
									break;
							}

							m_wndDataListGrid.SetValueRange(CGXRange(nRow, 1), pRltFile->GetLotNo());
							m_wndDataListGrid.SetValueRange(CGXRange(nRow, 2), pRltFile->GetTrayNo());
							m_wndDataListGrid.SetValueRange(CGXRange(nRow, 3), (long)(lpChannelData->wChIndex+1));	
							m_pDoc->GetStateMsg(lpChannelData->state, colorFlag);
							if(m_pDoc->m_bHideNonCellData && (lpChannelData->channelCode == EP_CODE_CELL_NONE || lpChannelData->channelCode == EP_CODE_NONCELL))
							{
									strTemp.Empty();
									m_wndDataListGrid.SetStyleRange(CGXRange(nRow, nCol), 
																		CGXStyle()
																		.SetValue(strTemp)
																		.SetTextColor(0)
																		.SetInterior(RGB(255,255,255)));
							}
							else
							{
									if(::IsNormalCell(lpChannelData->channelCode) == FALSE)
									{
										m_wndDataListGrid.SetStyleRange(CGXRange(nRow, nCol),					
																CGXStyle()
																.SetValue(strTemp)
																.SetTextColor(pConfig->m_TStateColor[colorFlag])
																.SetInterior(pConfig->m_BStateColor[colorFlag]));
									}
									else
									{
										m_wndDataListGrid.SetStyleRange(CGXRange(nRow, nCol), 
															CGXStyle()
															.SetValue(strTemp)
															.SetTextColor(0)
															.SetInterior(RGB(255,255,255)));
									}
							}
							
						}
					}
				}
			}
		}
	}
	delete pRltFile;
	pRltFile = NULL;
}


void CSerarchDataDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	if(::IsWindow(this->GetSafeHwnd()))
	{
		RECT rect;
		::GetClientRect(m_hWnd, &rect);

		CRect rectGrid;
		if(m_wndDataListGrid.GetSafeHwnd())
		{
			m_wndDataListGrid.GetWindowRect(rectGrid);	//Step Grid Window Position
			ScreenToClient(&rectGrid);
			m_wndDataListGrid.MoveWindow(5, rectGrid.top, rect.right-10, rect.bottom-rectGrid.top-5, FALSE);				
			Invalidate();
		}
	}	
}

void CSerarchDataDlg::SetTitle(CString strTitle)
{
	m_strTitle = strTitle;
}

void CSerarchDataDlg::OnButtonExcel() 
{
	// TODO: Add your control notification handler code here
	CString strFileName;
	strFileName.Format("%s.csv", m_strTitle);
	CFileDialog pDlg(FALSE, "", strFileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, TEXT_LANG[0]);//"csv 파일(*.csv)|*.csv|"

	if(IDOK == pDlg.DoModal())
	{
		CWaitCursor wait;
		strFileName = pDlg.GetPathName();
		if(strFileName.IsEmpty())	return ;

		FILE *fp = fopen(strFileName, "wt");
		if(fp)
		{
			for(int row = 0; row<=m_wndDataListGrid.GetRowCount(); row++)
			{
				for(int col = 0; col<=m_wndDataListGrid.GetColCount(); col++)
				{
					fprintf(fp, "%s,", m_wndDataListGrid.GetValueRowCol(row, col));
				}
				fprintf(fp, "\n");
			}
			fclose(fp);
		}
	}		
}

