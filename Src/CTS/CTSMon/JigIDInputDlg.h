#if !defined(AFX_JIGIDINPUTDLG_H__DADC0090_06D2_4C3B_876F_BE008F7A1811__INCLUDED_)
#define AFX_JIGIDINPUTDLG_H__DADC0090_06D2_4C3B_876F_BE008F7A1811__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// JigIDInputDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CJigIDInputDlg dialog

class CJigIDInputDlg : public CDialog
{
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();
	virtual ~CJigIDInputDlg();

// Construction
public:
	CString GetJigID();
	CJigIDInputDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CJigIDInputDlg)
	enum { IDD = IDD_JIGID_DLG };
	CComboBox	m_ctrlJigCombo;
	CString	m_strJigID;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CJigIDInputDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CJigIDInputDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnRadio2();
	afx_msg void OnRadio1();
	afx_msg void OnSelchangeCombo1();
	virtual void OnOK();
	//}}AFX_MSG
	afx_msg LRESULT OnBcrscaned(UINT wParam, LONG lParam);
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_JIGIDINPUTDLG_H__DADC0090_06D2_4C3B_876F_BE008F7A1811__INCLUDED_)
