#ifndef __RW_GFA_H__
#define __RW_GFA_H__

CString strChar_(WORD DecimalZahl);
//CString MID(CString str, int nFirst, int nCount) const;
double arsinh(double x);
double arcosh(double x);
double artanh(double x);
double DEG(double x);
double RAD(double x);
#endif // __RW_GFA_H__