// ProcedureSelDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSMon.h"
#include "ProcedureSelDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CProcedureSelDlg dialog


CProcedureSelDlg::CProcedureSelDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CProcedureSelDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CProcedureSelDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_nModelIndex = 0L;
//	m_nFirstTestID = 0L;
	ZeroMemory(&m_TestHeader, sizeof(STR_CONDITION_HEADER));
//	m_strModuleName = "Module";
	m_lTestID = 0;
	m_bShowTestSelect = TRUE;
}


void CProcedureSelDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CProcedureSelDlg)
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDC_DETAIL_BUTTON, m_btnDetail);
	DDX_Control(pDX, IDC_TEST_NAME_STATIC, m_ctrlProLabel);
	DDX_Control(pDX, IDC_TITLE, m_ctrlTitle);
	DDX_Control(pDX, IDC_SEL_PROC_NAME, m_strSelProcName);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CProcedureSelDlg, CDialog)
	//{{AFX_MSG_MAP(CProcedureSelDlg)
	ON_BN_CLICKED(IDOK, OnOk)
	ON_BN_CLICKED(IDC_DETAIL_BUTTON, OnDetailButton)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_GRID_MOVECELL, OnMovedCurrentCell)
	ON_MESSAGE(WM_GRID_DOUBLECLICK, OnGridDoubleClick)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CProcedureSelDlg message handlers

BOOL CProcedureSelDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	CString strTemp;
	strTemp = ::GetStringTable(IDS_TEXT_APPLY_UNIT);
	CString str;
	str.Format(strTemp, AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Module Name", "Rack"));
	m_ctrlTitle.SetText(_T(str));
	m_strSelProcName.SetText("1.Select schedule.");
	m_strSelProcName.SetBkColor(RGB(255, 255, 220));
	m_ctrlProLabel.SetText("2.Select procedure.");
	m_ctrlProLabel.SetBkColor(RGB(255, 255, 220));

	InitModelGrid();
	InitTestListGrid();
	
//	RequeryTestList();

	//시험 목록은 표시하지 않음 
	if(!m_bShowTestSelect)
	{
		CRect rect;
		//GetClientRect(rect);
		GetWindowRect(rect);
		int nDecreaseSize  = rect.Width()*3/5;
		rect.right = rect.right - nDecreaseSize;
		MoveWindow(rect);

		GetDlgItem(IDOK)->GetWindowRect(&rect);
		ScreenToClient(&rect);
		GetDlgItem(IDOK)->MoveWindow(rect.left-nDecreaseSize , rect.top, rect.Width(), rect.Height(), FALSE);
		GetDlgItem(IDCANCEL)->GetWindowRect(&rect);
		ScreenToClient(&rect);
		GetDlgItem(IDCANCEL)->MoveWindow(rect.left-nDecreaseSize , rect.top, rect.Width(), rect.Height(), FALSE);
		
		int nRight = rect.left-nDecreaseSize;

		GetDlgItem(IDC_TITLE)->GetWindowRect(&rect);
		ScreenToClient(&rect);
		rect.right = nRight-5;
		GetDlgItem(IDC_TITLE)->MoveWindow(rect.left , rect.top, rect.Width(), rect.Height(), FALSE);
		
	}

	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CProcedureSelDlg::InitModelGrid()
{
	m_wndModelList.SubclassDlgItem(IDC_MODEL_LIST_GRID, this);
	m_wndModelList.m_bRowSelection = TRUE;
	m_wndModelList.m_bSameColSize  = FALSE;
	m_wndModelList.m_bSameRowSize  = FALSE;
	m_wndModelList.m_bCustomWidth  = TRUE;
//	m_wndModelList.m_nWidth[0]	 = 20;
	m_wndModelList.m_nWidth[1]	 = 0;
	m_wndModelList.m_nWidth[2]	 = 30;
	m_wndModelList.m_nWidth[3]	 = 150;
//	m_wndModelList.m_nWidth[4]	 = 180;
	m_wndModelList.Initialize();
	m_wndModelList.EnableCellTips();
	BOOL bLock = m_wndModelList.LockUpdate();
	m_wndModelList.GetParam()->SetSortRowsOnDblClk(TRUE);

	m_wndModelList.SetDefaultRowHeight(20);
	m_wndModelList.SetColCount(4);
	m_wndModelList.SetRowCount(0);
	m_wndModelList.SetStyleRange(CGXRange().SetRows(0), 
			CGXStyle().SetFont(CGXFont().SetSize(10).SetFaceName(GetStringTable(IDS_LANG_TEXT_FONT)).SetBold(FALSE) ));

	m_wndModelList.HideCols(1,1);
	m_wndModelList.SetValueRange(CGXRange(0, 1), "Model ID");
	m_wndModelList.SetValueRange(CGXRange(0, 2), "No");
	m_wndModelList.SetValueRange(CGXRange(0, 3), "Name");
	m_wndModelList.SetValueRange(CGXRange(0, 4), "Model");
	m_wndModelList.LockUpdate(bLock);

	m_wndModelList.SetStyleRange(CGXRange().SetCols(3, 4), CGXStyle().SetVertScrollBar(TRUE).SetWrapText(FALSE));
	RequeryBatteryModel();

}

/*****/
BOOL CProcedureSelDlg::RequeryBatteryModel()
{
/*	CBatteryModelRecordSet recordSet;
	recordSet.m_strSort.Format("[No]");

	try
	{
		recordSet.Open();
	}
	catch (CDBException* e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();
		return FALSE;
	}

	ROWCOL nRow = m_wndModelList.GetRowCount();
	if(nRow > 0)	m_wndModelList.RemoveRows(1, nRow);

	BOOL bLock = m_wndModelList.LockUpdate();
	nRow = 0;
	while(!recordSet.IsEOF())
	{
		nRow++;
		m_wndModelList.InsertRows(nRow, 1);
		m_wndModelList.SetValueRange(CGXRange(nRow, 1), recordSet.m_ModelID);
		m_wndModelList.SetValueRange(CGXRange(nRow, 2), nRow);
		m_wndModelList.SetValueRange(CGXRange(nRow, 3), recordSet.m_ModelName);
		m_wndModelList.SetValueRange(CGXRange(nRow, 4), recordSet.m_Description);
		recordSet.MoveNext();
	}
	recordSet.Close();

	
	m_wndModelList.LockUpdate(bLock);
	m_wndModelList.Redraw();
*/
	//20071030
	CDaoDatabase  db;
	int nInstallCount = 0;

	CString strDBName;
	strDBName = ::GetDataBaseName();
	if(strDBName.IsEmpty())		return FALSE;
	
	try
	{
		db.Open(strDBName);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	
	CString strSQL, strTemp;
	DWORD sysID = ((CCTSMonApp*)AfxGetApp())->GetSystemType();
	strSQL = "SELECT * FROM BatteryModel ORDER BY No";
	
	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return FALSE;
	}	
	
	ROWCOL nRow = m_wndModelList.GetRowCount();
	if(nRow > 0)	m_wndModelList.RemoveRows(1, nRow);

	BOOL bLock = m_wndModelList.LockUpdate();
	nRow = 0;
	while(!rs.IsEOF())
	{
		nRow++;
		m_wndModelList.InsertRows(nRow, 1);
		rs.GetFieldValue("ModelID", data);
		m_wndModelList.SetValueRange(CGXRange(nRow, 1), data.lVal);
		m_wndModelList.SetValueRange(CGXRange(nRow, 2), nRow);
		rs.GetFieldValue("ModelName", data);
		if(VT_NULL != data.vt) 		m_wndModelList.SetValueRange(CGXRange(nRow, 3), data.pbVal);
		rs.GetFieldValue("Description", data);
		if(VT_NULL != data.vt)		m_wndModelList.SetValueRange(CGXRange(nRow, 4), data.pbVal);
		rs.MoveNext();		
	}
	rs.Close();

	m_wndModelList.LockUpdate(bLock);
	m_wndModelList.Redraw();
	
	return TRUE;
}

LONG CProcedureSelDlg::OnMovedCurrentCell(WPARAM wParam, LPARAM lParam)
{
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;				//Get Grid	
//	ROWCOL nCol = LOWORD(wParam);		
	ROWCOL nRow = HIWORD(wParam);	//Get Current Row Col
	if(nRow < 1)	return 0L;

	CString strTemp;

	if(&m_wndModelList == (CMyGridWnd *)lParam)
	{
		m_nModelIndex = atol(pGrid->GetValueRowCol(nRow, 1));
		m_strSelProcName.SetText(pGrid->GetValueRowCol(nRow, 3));
		RequeryTestList();
	}
	else
	{
		m_lTestID = atol(pGrid->GetValueRowCol(nRow, 1));
		m_ctrlProLabel.SetText(pGrid->GetValueRowCol(nRow, 3));
	}
	return 0L;
}

void CProcedureSelDlg::OnOk() 
{
	// TODO: Add your control notification handler code here

	if(GetTestID() == FALSE)
	{
		AfxMessageBox(GetStringTable(IDS_TEXT_CONDITION_NOT_FOUND), MB_OK|MB_ICONERROR);
		return;
	}

	TRACE("%d,%s Select\n", m_TestHeader.lID, m_TestHeader.szName);
	CDialog::OnOK();
}

LONG CProcedureSelDlg::OnGridDoubleClick(WPARAM wParam, LPARAM lParam)
{
	ROWCOL nRow;//, nCol;
//	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;
//	nCol = LOWORD(wParam);
	nRow = HIWORD(wParam);
/*	if(nRow<1) 		return 0;

	if(GetTestID() == FALSE)
	{
		AfxMessageBox(GetStringTable(IDS_TEXT_CONDITION_NOT_FOUND), MB_OK|MB_ICONERROR);
		return 0;
	}
	CDialog::OnOK();
*/	
	if(&m_wndModelList == (CMyGridWnd *) lParam)
	{
		if(m_bShowTestSelect == FALSE)
		{
			if(GetTestID() == FALSE)
			{
				AfxMessageBox(GetStringTable(IDS_TEXT_CONDITION_NOT_FOUND), MB_OK|MB_ICONERROR);
				return 0;
			}

			TRACE("%d,%s Select\n", m_TestHeader.lID, m_TestHeader.szName);
			CDialog::OnOK();
		}
	}
	
	return 0;
}

BOOL CProcedureSelDlg::GetTestID()
{
	ROWCOL nRow, nCol;
	if(m_lTestID == 0)	//모델만 선택시 가장 처음 시험 선택 
	{
		if(m_wndTestList.GetRowCount() > 0)	
		{
			nRow = 1;
		}
		else
		{
			return FALSE;
		}
	}
	else				//선택한것이 있으면 적용
	{
		if(m_wndTestList.GetCurrentCell(&nRow, &nCol) == FALSE)		return FALSE;
		if(nRow < 1)	return FALSE;
	}

	m_TestHeader.lID = atol(m_wndTestList.GetValueRowCol(nRow, 1));
	m_TestHeader.lNo = atol(m_wndTestList.GetValueRowCol(nRow, 2));
	m_TestHeader.lType = atol(m_wndTestList.GetValueRowCol(nRow, 7));
	sprintf(m_TestHeader.szName, "%s", m_wndTestList.GetValueRowCol(nRow, 3));
	sprintf(m_TestHeader.szDescription, "%s", m_wndTestList.GetValueRowCol(nRow, 4));
	sprintf(m_TestHeader.szCreator, "%s", m_wndTestList.GetValueRowCol(nRow, 5));
	sprintf(m_TestHeader.szModifiedTime, "%s", m_wndTestList.GetValueRowCol(nRow, 6));


	return TRUE;
}


void CProcedureSelDlg::InitTestListGrid()
{
	m_wndTestList.SubclassDlgItem(IDC_TEST_LIST_GRID, this);
	m_wndTestList.m_bRowSelection = TRUE;
	m_wndTestList.m_bSameColSize  = FALSE;
	m_wndTestList.m_bSameRowSize  = FALSE;
	m_wndTestList.m_bCustomWidth  = TRUE;
//	m_wndTestList.m_nWidth[0]	 = 20;
	m_wndTestList.m_nWidth[1]	 = 0;
	m_wndTestList.m_nWidth[2]	 = 30;
	m_wndTestList.m_nWidth[3]	 = 100;
	m_wndTestList.m_nWidth[4]	 = 110;
	m_wndTestList.m_nWidth[5]	 = 50;
	m_wndTestList.Initialize();
	m_wndTestList.EnableCellTips();
	BOOL bLock = m_wndTestList.LockUpdate();
//	m_wndTestList.GetParam()->SetSortRowsOnDblClk(TRUE);

	m_wndTestList.SetDefaultRowHeight(20);
	m_wndTestList.SetColCount(7);
	m_wndTestList.SetRowCount(0);
	m_wndTestList.SetStyleRange(CGXRange().SetRows(0), 
			CGXStyle().SetFont(CGXFont().SetSize(10).SetFaceName(GetStringTable(IDS_LANG_TEXT_FONT)).SetBold(FALSE) ));

	m_wndTestList.HideCols(1,1);
	m_wndTestList.SetValueRange(CGXRange(0, 1), "Model ID");
	m_wndTestList.SetValueRange(CGXRange(0, 2), "No");
	m_wndTestList.SetValueRange(CGXRange(0, 3), ::GetStringTable(IDS_LABEL_MODEL_NAME));
	m_wndTestList.SetValueRange(CGXRange(0, 4), ::GetStringTable(IDS_LABEL_SUMMARY));
	m_wndTestList.SetValueRange(CGXRange(0, 5), ::GetStringTable(IDS_LABEL_DESCRIPTOR));
	m_wndTestList.SetValueRange(CGXRange(0, 6), ::GetStringTable(IDS_LABEL_CREATE_DATE));
	m_wndTestList.SetValueRange(CGXRange(0, 7), "Type");
	m_wndTestList.LockUpdate(bLock);

	m_wndTestList.SetStyleRange(CGXRange().SetCols(3, 6), CGXStyle().SetVertScrollBar(TRUE).SetWrapText(FALSE));
}

BOOL CProcedureSelDlg::RequeryTestList()
{

	if(m_wndTestList.GetRowCount() > 0)
	{
		m_wndTestList.RemoveRows(1, m_wndTestList.GetRowCount());
	}
		
	CDaoDatabase  db;
	try
	{
		db.Open(::GetDataBaseName());
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}
	
	CString strSQL, strTemp;
	strSQL.Format("SELECT * FROM TestName WHERE ModelID = %d ORDER BY TestNo", m_nModelIndex);

	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return FALSE;
	}	
	
	int ncount = 0;
	while(!rs.IsEOF())
	{
		ncount++;
		m_wndTestList.InsertRows(ncount, 1);
		rs.GetFieldValue("TestID", data);
		m_wndTestList.SetValueRange(CGXRange(ncount, 1), data.lVal);
		rs.GetFieldValue("TestNo", data);
		m_wndTestList.SetValueRange(CGXRange(ncount, 2), data.lVal);
		rs.GetFieldValue("TestName", data);
		if(VT_NULL != data.vt) 		m_wndTestList.SetValueRange(CGXRange(ncount, 3), data.pbVal);
		rs.GetFieldValue("Description", data);
		if(VT_NULL != data.vt)		m_wndTestList.SetValueRange(CGXRange(ncount, 4), data.pbVal);
		rs.GetFieldValue("Creator", data);
		if(VT_NULL != data.vt)		m_wndTestList.SetValueRange(CGXRange(ncount, 5), data.pbVal);
		rs.GetFieldValue("ModifiedTime", data);
		COleDateTime tt = data;
		if(tt.GetStatus() == COleDateTime::valid)
		{
			m_wndTestList.SetValueRange(CGXRange(ncount, 6), tt.Format());
		}

		rs.GetFieldValue("ProcTypeID", data);
		m_wndTestList.SetValueRange(CGXRange(ncount, 7), data.lVal);

		rs.MoveNext();
	}
	rs.Close();


/*	CTestListRecordSet	recordSet;
	recordSet.m_strFilter.Format("[ModelID] = %ld", m_nModelIndex);
	recordSet.m_strSort.Format("[TestNo]");
	try
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}
	
	if(recordSet.IsBOF())
	{
		recordSet.Close();
		return FALSE;
	}



	int ncount = 0;
	while(!recordSet.IsEOF())
	{
		ncount++;
		m_wndTestList.InsertRows(ncount, 1);
		m_wndTestList.SetValueRange(CGXRange(ncount, 1), recordSet.m_TestID);
		m_wndTestList.SetValueRange(CGXRange(ncount, 2), recordSet.m_TestNo);
		m_wndTestList.SetValueRange(CGXRange(ncount, 3), recordSet.m_TestName);
		m_wndTestList.SetValueRange(CGXRange(ncount, 4), recordSet.m_Description);
		m_wndTestList.SetValueRange(CGXRange(ncount, 5), recordSet.m_Creator);
		m_wndTestList.SetValueRange(CGXRange(ncount, 6), recordSet.m_ModifiedTime.Format());
		m_wndTestList.SetValueRange(CGXRange(ncount, 7), recordSet.m_ProcTypeID);

		recordSet.MoveNext();
	}
	recordSet.Close();
*/
	
//	if(ncount > 0)		m_wndTestList.SetCurrentCell(1, 2);

	return TRUE;
}

#include "MainFrm.h"
#include "CTSMonDoc.h"
void CProcedureSelDlg::OnDetailButton() 
{
	// TODO: Add your control notification handler code here
	CString strModelName, strTestName;
	m_strSelProcName.GetWindowText(strModelName);
	GetDlgItem(IDC_TEST_NAME_STATIC)->GetWindowText(strTestName);

//	m_pDoc->ExecuteEditor(strModelName, strTestName);

	((CCTSMonDoc *)((CMainFrame *)AfxGetMainWnd())->GetActiveDocument())->ExecuteEditor(strModelName, strTestName);
}
