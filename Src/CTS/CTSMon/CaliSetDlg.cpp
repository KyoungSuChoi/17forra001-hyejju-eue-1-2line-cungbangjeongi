// CaliSetDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSMon.h"
#include "CaliSetDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCaliSetDlg dialog


CCaliSetDlg::CCaliSetDlg(CCaliPoint* pCalPoint, 
						 int nMode,
						 CWnd* pParent /*=NULL*/)
	: CDialog(CCaliSetDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCaliSetDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_nMode = nMode;
	m_pCalPoint = pCalPoint;
	m_nVCalRange = CAL_RANGE1;	
	m_nVCheckRange = CAL_RANGE1;	
	m_nICalRange = CAL_RANGE1;	
	m_nICheckRange = CAL_RANGE1;
	m_nSelPresetNum = 0;

	LanguageinitMonConfig();

}

CCaliSetDlg::~CCaliSetDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

bool CCaliSetDlg::LanguageinitMonConfig()
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCaliSetDlg"), _T("TEXT_CCaliSetDlg_CNT"), _T("TEXT_CCaliSetDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CCaliSetDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCaliSetDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

void CCaliSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCaliSetDlg)
	DDX_Control(pDX, IDC_LABEL1, m_Label1);
	DDX_Control(pDX, IDC_SHUNT_T, m_wndShuntT);
	DDX_Control(pDX, IDC_SHUNT_R, m_wndShuntR);
	DDX_Control(pDX, IDC_SHUNT_SERIAL, m_wndShuntSerial);
	DDX_Control(pDX, IDC_I_CHECK_COMBO, m_ctrlICheck);
	DDX_Control(pDX, IDC_V_CHECK_COMBO, m_ctrlVCheck);
	DDX_Control(pDX, IDC_I_SET_COMBO, m_ctrlISet);
	DDX_Control(pDX, IDC_V_SET_COMBO, m_ctrlVSet);
	DDX_Control(pDX, IDC_POINT1, m_wndListPoint1);
	DDX_Control(pDX, IDC_POINT2, m_wndListPoint2);
	DDX_Control(pDX, IDC_CHECK1, m_wndListCheck1);
	DDX_Control(pDX, IDC_CHECK2, m_wndListCheck2);
	DDX_Control(pDX, IDC_CALIPOINT, m_ctrlSetCali);
	DDX_Control(pDX, IDC_CHECKPOINT, m_ctrlCheckCali);
	DDX_Check(pDX, IDC_CALIPOINT, m_bCaliPoint);
	DDX_Check(pDX, IDC_CHECKPOINT, m_bCheckPoint);
	DDX_Control(pDX, IDC_MAX_POINT_STATIC, m_cutionLabel);

	
	//}}AFX_DATA_MAP
}

	
BEGIN_MESSAGE_MAP(CCaliSetDlg, CDialog)
	//{{AFX_MSG_MAP(CCaliSetDlg)
	ON_CBN_SELCHANGE(IDC_V_SET_COMBO, OnSelchangeVSetCombo)
	ON_CBN_SELCHANGE(IDC_V_CHECK_COMBO, OnSelchangeVCheckCombo)
	ON_CBN_SELCHANGE(IDC_I_SET_COMBO, OnSelchangeISetCombo)
	ON_CBN_SELCHANGE(IDC_I_CHECK_COMBO, OnSelchangeICheckCombo)
	ON_BN_CLICKED(IDC_CALIPOINT, OnCalipoint)
	ON_BN_CLICKED(IDC_CHECKPOINT, OnCheckpoint)
	ON_BN_CLICKED(IDC_BUTTON_LOAD, OnButtonLoad)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BTN_PRESET_1, &CCaliSetDlg::OnBnClickedBtnPreset1)
	ON_BN_CLICKED(IDC_BTN_PRESET_2, &CCaliSetDlg::OnBnClickedBtnPreset2)
	ON_BN_CLICKED(IDC_BTN_PRESET_3, &CCaliSetDlg::OnBnClickedBtnPreset3)
	ON_BN_CLICKED(IDC_BTN_PRESET_4, &CCaliSetDlg::OnBnClickedBtnPreset4)
	ON_BN_CLICKED(IDC_BTN_PRESET_SAVE, &CCaliSetDlg::OnBnClickedBtnPresetSave)
	ON_BN_CLICKED(IDC_BTN_PRESET_LOAD, &CCaliSetDlg::OnBnClickedBtnPresetLoad)
	ON_BN_CLICKED(IDOK, &CCaliSetDlg::OnBnClickedOk)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCaliSetDlg message handlers

BOOL CCaliSetDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_toolTip.Create(this);
	m_toolTip.Activate(TRUE);

	m_toolTip.AddTool(GetDlgItem(IDC_CALIPOINT), TEXT_LANG[0]);//"입력값과 같은 -값을 자동 추가"
	m_toolTip.AddTool(GetDlgItem(IDC_CHECKPOINT), TEXT_LANG[0]);//"입력값과 같은 -값을 자동 추가"
	m_toolTip.AddTool(GetDlgItem(IDC_BUTTON_LOAD), TEXT_LANG[1]);//"가장 최근에 적용한 설정값 불러오기"

	m_cutionLabel.SetTextColor(RGB(255, 0, 0));
	m_bCaliPoint = AfxGetApp()->GetProfileInt("Calibration", "Set Cali", 0);
	m_bCheckPoint = AfxGetApp()->GetProfileInt("Calibration", "Check Cali", 0);
	m_ctrlSetCali.SetCheck(m_bCaliPoint);
	m_ctrlCheckCali.SetCheck(m_bCheckPoint);
	m_wndListPoint1.m_bCheckCali = FALSE;
	m_wndListCheck1.m_bCheckCali = FALSE;

	m_wndShuntT.m_bCheckCali = FALSE;
	m_wndShuntR.m_bCheckCali = FALSE;
	m_wndShuntSerial.m_bCheckCali = FALSE;

	{ //Set Max Boundary data with Cell Type
		float fMaxBoundaryData;
		fMaxBoundaryData = 10000000.0f;		//Max 10000A;
		m_wndListPoint1.SetBoundaryData(fMaxBoundaryData);
		m_wndListPoint2.SetBoundaryData(fMaxBoundaryData);
		m_wndListCheck1.SetBoundaryData(fMaxBoundaryData);
		m_wndListCheck2.SetBoundaryData(fMaxBoundaryData);

		m_wndListPoint1.SetDigitMode(TRUE);
		m_wndListPoint2.SetDigitMode(TRUE);
		m_wndListCheck1.SetDigitMode(TRUE);
		m_wndListCheck2.SetDigitMode(TRUE);

		m_wndShuntT.SetBoundaryData(fMaxBoundaryData);
		m_wndShuntT.SetDigitMode(TRUE);
		m_wndShuntR.SetDigitMode(TRUE);
		m_wndShuntSerial.SetDigitMode(FALSE);
	}

	int nVoltageUnitMode = AfxGetApp()->GetProfileInt("Calibration", "Vtg Unit Mode", 0);
	int nCurrentUnitMode = AfxGetApp()->GetProfileInt("Calibration", "Crt Unit Mode", 0);

	CString strVItem, strIItem;
	
		//List Property
		int nWidth = 95;
		m_wndListPoint1.InsertColumn(0, "No", LVCFMT_RIGHT, 45);
		strVItem = TEXT_LANG[2];//"전압값(mV)"
		strIItem = TEXT_LANG[3];//"전류값(mA)"
		if(nVoltageUnitMode)	strVItem = TEXT_LANG[4];//"전압값(uV)"
		if(nCurrentUnitMode)	strIItem = TEXT_LANG[5];//"전류값(uA)"

		m_wndListPoint1.InsertColumn(1, strVItem, LVCFMT_RIGHT, nWidth);
		DWORD dwExStyle = m_wndListPoint1.GetExtendedStyle();
		dwExStyle |= LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES;
		m_wndListPoint1.SetExtendedStyle( dwExStyle );

		m_wndListPoint2.InsertColumn(0, "No", LVCFMT_RIGHT, 45);
		m_wndListPoint2.InsertColumn(1, strIItem, LVCFMT_RIGHT, nWidth);
		dwExStyle = m_wndListPoint2.GetExtendedStyle();
		dwExStyle |= LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES;
		m_wndListPoint2.SetExtendedStyle( dwExStyle );

		m_wndListCheck1.InsertColumn(0, "No", LVCFMT_RIGHT, 45);
		m_wndListCheck1.InsertColumn(1, strVItem, LVCFMT_RIGHT, nWidth);
		dwExStyle = m_wndListCheck1.GetExtendedStyle();
		dwExStyle |= LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES;
		m_wndListCheck1.SetExtendedStyle( dwExStyle );

		m_wndListCheck2.InsertColumn(0, "No", LVCFMT_RIGHT, 45);
		m_wndListCheck2.InsertColumn(1, strIItem, LVCFMT_RIGHT, nWidth);
		dwExStyle = m_wndListCheck2.GetExtendedStyle();
		dwExStyle |= LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES;
		m_wndListCheck2.SetExtendedStyle( dwExStyle );

		m_wndShuntT.InsertColumn(0, "No", LVCFMT_RIGHT, 45);
		m_wndShuntT.InsertColumn(1, "T", LVCFMT_RIGHT, nWidth);	
		dwExStyle = m_wndShuntT.GetExtendedStyle();
		dwExStyle |= LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES;
		m_wndShuntT.SetExtendedStyle( dwExStyle );

		m_wndShuntR.InsertColumn(0, "No", LVCFMT_RIGHT, 45);
		m_wndShuntR.InsertColumn(1, "R", LVCFMT_RIGHT, nWidth);	
		dwExStyle = m_wndShuntR.GetExtendedStyle();
		dwExStyle |= LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES;
		m_wndShuntR.SetExtendedStyle( dwExStyle );

		m_wndShuntSerial.InsertColumn(0, "No", LVCFMT_RIGHT, 45);
		m_wndShuntSerial.InsertColumn(1, "Serial", LVCFMT_CENTER, 130);	
		dwExStyle = m_wndShuntSerial.GetExtendedStyle();
		dwExStyle |= LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES;
		m_wndShuntSerial.SetExtendedStyle( dwExStyle );	

	
	CString strMsg;
	int i = 0;
	for(i =0; i<CAL_MAX_CALIB_SET_POINT; i++)
	{
		strMsg.Format("%d", i+1);
		m_wndListPoint1.SetItemText(i, 0, strMsg);
		m_wndListPoint2.SetItemText(i, 0, strMsg);
	}
	for(i =0; i<CAL_MAX_CALIB_CHECK_POINT; i++)
	{
		strMsg.Format("%d", i+1);
		m_wndListCheck1.SetItemText(i, 0, strMsg);
		m_wndListCheck2.SetItemText(i, 0, strMsg);
	}

	for(i=0; i<CAL_MAX_BOARD_NUM; i++ )
	{
		strMsg.Format("%d", i+1);
		m_wndShuntT.SetItemText(i, 0, strMsg);
		m_wndShuntR.SetItemText(i, 0, strMsg);
		m_wndShuntSerial.SetItemText(i, 0, strMsg);
	}

	LoadCalConfigFile();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CCaliSetDlg::fnSavePreset( int nSelNum )
{
	CString strName = _T("");
	CString strTemp = _T("");
	int nDatacount = 0;
	int i=0;

	double  shuntData[CAL_MAX_BOARD_NUM];
	ZeroMemory( shuntData, sizeof(shuntData));
	CHAR	shuntSerial[CAL_MAX_BOARD_NUM][CAL_MAX_SHUNT_SERIAL_SIZE];
	ZeroMemory( shuntSerial, sizeof(shuntSerial));

	m_wndShuntT.UpdateText();
	nDatacount = m_wndShuntT.GetItemCount();
	if(nDatacount > CAL_MAX_BOARD_NUM)
	{
		nDatacount = CAL_MAX_BOARD_NUM;
	}

	for(i =0; i<CAL_MAX_BOARD_NUM; i++)
	{	
		strTemp = m_wndShuntT.GetItemText(i, 1);
		shuntData[i] = atof(strTemp);
	}

	strName.Format("ShuntT_Preset%d", nSelNum);
	AfxGetApp()->WriteProfileBinary(REG_CAL_SECTION, strName, (LPBYTE)shuntData, sizeof(shuntData));

	m_wndShuntR.UpdateText();
	nDatacount = m_wndShuntR.GetItemCount();
	if(nDatacount > CAL_MAX_BOARD_NUM)
	{
		nDatacount = CAL_MAX_BOARD_NUM;
	}

	for(i =0; i<CAL_MAX_BOARD_NUM; i++)
	{	
		strTemp = m_wndShuntR.GetItemText(i, 1);
		shuntData[i] = atof(strTemp);
	}

	strName.Format("ShuntR_Preset%d", nSelNum);
	AfxGetApp()->WriteProfileBinary(REG_CAL_SECTION, strName, (LPBYTE)shuntData, sizeof(shuntData));

	m_wndShuntSerial.UpdateText();
	nDatacount = m_wndShuntSerial.GetItemCount();
	if(nDatacount > CAL_MAX_BOARD_NUM)
	{
		nDatacount = CAL_MAX_BOARD_NUM;
	}

	for(i =0; i<CAL_MAX_BOARD_NUM; i++)
	{
		strTemp = m_wndShuntSerial.GetItemText(i, 1);
		sprintf(&shuntSerial[i][0], "%s", strTemp.Left(15));
	}

	strName.Format("ShuntSerial_Preset%d", nSelNum);
	AfxGetApp()->WriteProfileBinary(REG_CAL_SECTION, strName, (LPBYTE)shuntSerial, sizeof(shuntSerial));

	return TRUE;
}

BOOL CCaliSetDlg::fnLoadPreset( int nSelNum )
{
	UINT nSize;
	LPVOID* pData;
	CString strName;

	double  shuntT[CAL_MAX_BOARD_NUM];
	double	shuntR[CAL_MAX_BOARD_NUM];
	CHAR	shuntSerial[CAL_MAX_BOARD_NUM][CAL_MAX_SHUNT_SERIAL_SIZE];

	strName.Format("ShuntT_Preset%d", nSelNum);
	AfxGetApp()->GetProfileBinary(REG_CAL_SECTION, strName, (LPBYTE *)&pData, &nSize);
	if(nSize > 0)	memcpy(shuntT, pData, nSize);
	else			
	{
		memset(shuntT, 0, sizeof(shuntT));
	}	
	delete [] pData;

	strName.Format("ShuntR_Preset%d", nSelNum);
	AfxGetApp()->GetProfileBinary(REG_CAL_SECTION, strName, (LPBYTE *)&pData, &nSize);
	if(nSize > 0)	memcpy(shuntR, pData, nSize);
	else			
	{
		memset(shuntR, 0, sizeof(shuntR));
	}	
	delete [] pData;

	strName.Format("ShuntSerial_Preset%d", nSelNum);
	AfxGetApp()->GetProfileBinary(REG_CAL_SECTION, strName, (LPBYTE *)&pData, &nSize);
	if(nSize > 0)	memcpy(shuntSerial, pData, nSize);
	else			
	{
		memset(shuntSerial, 0, sizeof(shuntSerial));
	}	
	delete [] pData;

	LV_ITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LV_ITEM));
	lvItem.mask = LVIF_TEXT;

	int i=0;
	CString strTemp;	

	m_wndShuntT.DeleteAllItems();

	for( i=0; i<CAL_MAX_BOARD_NUM; i++)
	{
		if( shuntT[i] == 0 )
		{
			break;
		}

		lvItem.iItem = i;
		strTemp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTemp;
		m_wndShuntT.InsertItem(&lvItem);
		strTemp.Format("%.4f", shuntT[i]);
		m_wndShuntT.SetItemText(i, 1, strTemp);	
	}	

	m_wndShuntR.DeleteAllItems();	
	for( i=0; i<CAL_MAX_BOARD_NUM; i++)
	{
		if( shuntR[i] == 0 )
		{
			break;
		}

		lvItem.iItem = i;
		strTemp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTemp;
		m_wndShuntR.InsertItem(&lvItem);
		strTemp.Format("%.4f", shuntR[i]);
		m_wndShuntR.SetItemText(i, 1, strTemp);	
	}	

	m_wndShuntSerial.DeleteAllItems();	
	for( i=0; i<CAL_MAX_BOARD_NUM; i++)
	{
		strTemp.Format("%s", shuntSerial[i]);
		if( strTemp.IsEmpty() )
		{
			break;
		}

		lvItem.iItem = i;
		strTemp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTemp;
		m_wndShuntSerial.InsertItem(&lvItem);
		strTemp.Format("%s", shuntSerial[i]);
		m_wndShuntSerial.SetItemText(i, 1, strTemp);	
	}

	return TRUE;
}

void CCaliSetDlg::fnUpdatePreset()
{
	switch( m_nSelPresetNum )
	{
	case 0:
		m_Label1.SetText("None");
		break;
	case 1:
		m_Label1.SetText("===> 1");
		break;
	case 2:
		m_Label1.SetText("===> 2");
		break;
	case 3:
		m_Label1.SetText("===> 3");
		break;
	case 4:
		m_Label1.SetText("===> 4");
		break;
	}
}

void CCaliSetDlg::InitLabel()
{
	m_Label1.SetFontSize(15)
		.SetTextColor(RGB(255,255,0))
		.SetBkColor(RGB(0,0,0))
		.SetSunken(TRUE)
		.SetFontBold(TRUE)		
		.SetText("None");
}

BOOL CCaliSetDlg::PreTranslateMessage(MSG* pMsg) 
{
	if( pMsg->message == WM_KEYDOWN )
	{
		switch( pMsg->wParam )
		{
		case VK_RETURN:
			return FALSE;			
		case VK_ESCAPE:
			return FALSE;
		}
	}
	m_toolTip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL CCaliSetDlg::DestroyWindow() 
{
	return CDialog::DestroyWindow();
}

void CCaliSetDlg::OnOK() 
{
	if( SaveAll() == FALSE )
		return;
	
	CDialog::OnOK();
}

BOOL CCaliSetDlg::SaveAll()
{
	UpdatePointData();

	CString strMsg;

	int nVCalCnt = AfxGetApp()->GetProfileInt("Calibration", "V_CAL_POINT_COUNT", 2);
	int nICalCnt = AfxGetApp()->GetProfileInt("Calibration", "I_CAL_POINT_COUNT", 4);


	//전압 교정 Point 수량을 2개로 고정 
// 	for(int r=0; r<m_pCalPoint->GetVRangeCnt(); r++)
// 	{		
// 		if(m_pCalPoint->GetVtgSetPointCount(r) != nVCalCnt)
// 		{
// 			strMsg.Format("전압 교정 Point 수를 %d Point 지정 하십시요.", nVCalCnt);
// 			AfxMessageBox(strMsg);
// 			return FALSE;
// 		}
// 	}

// 	for(r=0; r<m_pCalPoint->GetIRangeCnt(); r++)
// 	{		
// 		//+ 2Point, - 2Point check
// 		int minus = 0, pluse = 0;
// 		for(int n = 0; n<m_pCalPoint->GetCrtSetPointCount(r); n++)
// 		{
// 			if(m_pCalPoint->GetISetPoint(r, n) < 0.0)
// 			{
// 				minus++;
// 			}
// 			else
// 			{
// 				pluse++;
// 			}
// 		}
// 		if(minus != int(nICalCnt/2) || pluse != int(nICalCnt/2))
// 		{
// 			strMsg.Format("전류 RANGE #%d 교정 Point를 + 영역 %dPoint, - 영역 %dPoint를 지정 하십시요.", r+1, nICalCnt/2, nICalCnt/2);
// 			AfxMessageBox(strMsg);
// 			return FALSE;
// 		}
//	}
	if(MessageBox(TEXT_LANG[6], TEXT_LANG[7], MB_YESNO|MB_ICONQUESTION) == IDYES)//"현재 교정 Point 설정을 최종 교정 설정으로 저장 하시겠습니까?"//"저장"
	{
		//pc에 저장 
		m_pCalPoint->SavePointData();
		return TRUE;
	}
	return FALSE;
}

void CCaliSetDlg::LoadCalConfigFile()
{
	CString strTmp;
	LV_ITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LV_ITEM));
	lvItem.mask = LVIF_TEXT;

	m_ctrlVCheck.ResetContent();
	m_ctrlVSet.ResetContent();
	m_ctrlICheck.ResetContent();
	m_ctrlISet.ResetContent();

	int i = 0;

	for(i=0; i<m_pCalPoint->GetVRangeCnt(); i++)
	{
		strTmp.Format("Range #%d", i+1);
		m_ctrlVCheck.AddString(strTmp);
		m_ctrlVSet.AddString(strTmp);;
		m_ctrlVCheck.SetItemData(i, i);
		m_ctrlVSet.SetItemData(i, i);
	}
	for(i =0; i<m_pCalPoint->GetIRangeCnt(); i++)
	{
		strTmp.Format("Range #%d", i+1);
		m_ctrlICheck.AddString(strTmp);
		m_ctrlISet.AddString(strTmp);
		m_ctrlICheck.SetItemData(i, i);
		m_ctrlISet.SetItemData(i, i);
	}
	m_ctrlVCheck.SetCurSel(0);
	m_ctrlVSet.SetCurSel(0);
	m_ctrlICheck.SetCurSel(0);
	m_ctrlISet.SetCurSel(0);

	m_wndListPoint1.DeleteAllItems();
	m_nVCalRange = m_ctrlVCheck.GetCurSel();
	for( i=0; i<m_pCalPoint->GetVtgSetPointCount((WORD)m_nVCalRange); i++)
	{
		lvItem.iItem = i;
		strTmp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTmp;
		m_wndListPoint1.InsertItem(&lvItem);
		strTmp.Format("%.3f", m_pCalPoint->GetVSetPoint((WORD)m_nVCalRange, (WORD)i));
		m_wndListPoint1.SetItemText(i, 1, strTmp);	
	}

	m_wndListCheck1.DeleteAllItems();
	m_nVCheckRange = m_ctrlVCheck.GetCurSel();
	for(i=0; i<m_pCalPoint->GetVtgCheckPointCount((WORD)m_nVCheckRange); i++)
	{
		lvItem.iItem = i;
		strTmp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTmp;
		m_wndListCheck1.InsertItem(&lvItem);
		strTmp.Format("%.3f", m_pCalPoint->GetVCheckPoint((WORD)m_nVCheckRange, (WORD)i));
		m_wndListCheck1.SetItemText(i, 1, strTmp);	
	}	

	m_wndListPoint2.DeleteAllItems();
	m_nICalRange = m_ctrlISet.GetCurSel();
	for(i=0; i<m_pCalPoint->GetCrtSetPointCount((WORD)m_nICalRange); i++)
	{
		lvItem.iItem = i;
		strTmp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTmp;
		m_wndListPoint2.InsertItem(&lvItem);
		strTmp.Format("%.3f", m_pCalPoint->GetISetPoint((WORD)m_nICalRange, (WORD)i));
		m_wndListPoint2.SetItemText(i, 1, strTmp);	

	}	

	m_wndListCheck2.DeleteAllItems();
	m_nICheckRange = m_ctrlICheck.GetCurSel();
	for(i=0; i<m_pCalPoint->GetCrtCheckPointCount((WORD)m_nICheckRange); i++)
	{
		lvItem.iItem = i;
		strTmp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTmp;
		m_wndListCheck2.InsertItem(&lvItem);
		strTmp.Format("%.3f", m_pCalPoint->GetICheckPoint((WORD)m_nICheckRange, (WORD)i));
		m_wndListCheck2.SetItemText(i, 1, strTmp);	
	}	

	m_wndShuntT.DeleteAllItems();	
	for( i=0; i<CAL_MAX_BOARD_NUM; i++)
	{
		if( m_pCalPoint->GetShuntT(i) == 0 )
		{
			break;
		}

		lvItem.iItem = i;
		strTmp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTmp;
		m_wndShuntT.InsertItem(&lvItem);
		strTmp.Format("%.4f", m_pCalPoint->GetShuntT(i));
		m_wndShuntT.SetItemText(i, 1, strTmp);	
	}	

	m_wndShuntR.DeleteAllItems();	
	for( i=0; i<CAL_MAX_BOARD_NUM; i++)
	{
		if( m_pCalPoint->GetShuntR(i) == 0 )
		{
			break;
		}

		lvItem.iItem = i;
		strTmp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTmp;
		m_wndShuntR.InsertItem(&lvItem);
		strTmp.Format("%.4f", m_pCalPoint->GetShuntR(i));
		m_wndShuntR.SetItemText(i, 1, strTmp);	
	}	

	m_wndShuntSerial.DeleteAllItems();	
	for( i=0; i<CAL_MAX_BOARD_NUM; i++)
	{
		strTmp.Format("%s", m_pCalPoint->GetShuntSerial(i));
		if( strTmp.IsEmpty() )
		{
			break;
		}

		lvItem.iItem = i;
		strTmp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTmp;
		m_wndShuntSerial.InsertItem(&lvItem);
		strTmp.Format("%s", m_pCalPoint->GetShuntSerial(i));
		m_wndShuntSerial.SetItemText(i, 1, strTmp);	
	}
}

//void SwapValue(long &lFirst, long &lSecond)
//{
//	long lTmp = lSecond;
//	lSecond = lFirst;
//	lFirst = lTmp;
//}

//void CCaliSetDlg::Sorting(CAL_POINT& aCalPoint)
//{
//	for( int nI = 0; nI < aCalPoint.byValidPointNum; nI++ )
//	{
//		for( int nX = nI+1; nX < aCalPoint.byValidPointNum; nX++ )
//		{
//			if( aCalPoint.lPoint[nI] > aCalPoint.lPoint[nX] )
//			{
//				SwapValue(aCalPoint.lPoint[nI], aCalPoint.lPoint[nX]);
//			}
//		}
//	}
//}

void CCaliSetDlg::OnSelchangeVSetCombo() 
{
	// TODO: Add your control notification handler code here

	ASSERT(m_pCalPoint);

	CString strTmp;
	UpdatePointData();

	LV_ITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LV_ITEM));
	lvItem.mask = LVIF_TEXT;

	m_wndListPoint1.DeleteAllItems();
	m_nVCalRange = m_ctrlVCheck.GetCurSel();
	for(int i=0; i<m_pCalPoint->GetVtgSetPointCount((WORD)m_nVCalRange); i++)
	{
		lvItem.iItem = i;
		strTmp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTmp;
		m_wndListPoint1.InsertItem(&lvItem);
		strTmp.Format("%.3f", m_pCalPoint->GetVSetPoint((WORD)m_nVCalRange, (WORD)i));
		m_wndListPoint1.SetItemText(i, 1, strTmp);	
	}

	
}

void CCaliSetDlg::OnSelchangeVCheckCombo() 
{
	// TODO: Add your control notification handler code here

	ASSERT(m_pCalPoint);
	CString strTmp;

	UpdatePointData();
	
	LV_ITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LV_ITEM));
	lvItem.mask = LVIF_TEXT;

	m_wndListCheck1.DeleteAllItems();
	m_nVCheckRange = m_ctrlVCheck.GetCurSel();
	for(int i=0; i<m_pCalPoint->GetVtgCheckPointCount((WORD)m_nVCheckRange); i++)
	{
		lvItem.iItem = i;
		strTmp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTmp;
		m_wndListCheck1.InsertItem(&lvItem);
		strTmp.Format("%.3f", m_pCalPoint->GetVCheckPoint((WORD)m_nVCheckRange, (WORD)i));
		m_wndListCheck1.SetItemText(i, 1, strTmp);	
	}	
}

void CCaliSetDlg::OnSelchangeISetCombo() 
{
	// TODO: Add your control notification handler code here

	ASSERT(m_pCalPoint);
	CString strTmp;

	UpdatePointData();

	LV_ITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LV_ITEM));
	lvItem.mask = LVIF_TEXT;

	m_wndListPoint2.DeleteAllItems();
	m_nICalRange = m_ctrlISet.GetCurSel();
	for(int i=0; i<m_pCalPoint->GetCrtSetPointCount((WORD)m_nICalRange); (WORD)i++)
	{
		lvItem.iItem = i;
		strTmp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTmp;
		m_wndListPoint2.InsertItem(&lvItem);
		strTmp.Format("%.3f", m_pCalPoint->GetISetPoint((WORD)m_nICalRange, (WORD)i));		
		m_wndListPoint2.SetItemText(i, 1, strTmp);
	}	
}

void CCaliSetDlg::OnSelchangeICheckCombo() 
{
	// TODO: Add your control notification handler code here

	ASSERT(m_pCalPoint);
	CString strTmp;

	UpdatePointData();
	
	LV_ITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LV_ITEM));
	lvItem.mask = LVIF_TEXT;

	m_wndListCheck2.DeleteAllItems();
	m_nICheckRange = m_ctrlICheck.GetCurSel();
	for(int i=0; i<m_pCalPoint->GetCrtCheckPointCount((WORD)m_nICheckRange); (WORD)i++)
	{
		lvItem.iItem = i;
		strTmp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTmp;
		m_wndListCheck2.InsertItem(&lvItem);
		strTmp.Format("%.3f", m_pCalPoint->GetICheckPoint((WORD)m_nICheckRange, (WORD)i));
		m_wndListCheck2.SetItemText(i, 1, strTmp);	

	}	
}

void CCaliSetDlg::UpdatePointData()
{
	AfxGetApp()->WriteProfileInt(REG_CAL_SECTION, "Set Cali", m_bCaliPoint);
	AfxGetApp()->WriteProfileInt(REG_CAL_SECTION, "Check Cali", m_bCheckPoint);
	//V Calibration Point Update
	CString strTmp;
	double data[CAL_MAX_CALIB_SET_POINT];
	double shuntData[CAL_MAX_BOARD_NUM];
	CHAR shuntDataSerial[CAL_MAX_BOARD_NUM][CAL_MAX_SHUNT_SERIAL_SIZE];

	int i=0;

	for( i=0; i<CAL_MAX_BOARD_NUM; i++)
	{
		ZeroMemory( &shuntDataSerial[i][0], CAL_MAX_SHUNT_SERIAL_SIZE );
	}	

	m_wndListPoint1.UpdateText();
	int nDatacount = m_wndListPoint1.GetItemCount();
	if(nDatacount > CAL_MAX_CALIB_SET_POINT)
	{
		nDatacount = CAL_MAX_CALIB_SET_POINT;
	}
	for(i =0; i<nDatacount; i++)
	{
		strTmp = m_wndListPoint1.GetItemText(i, 1);
		data[i] = atof(strTmp);
	}
	m_pCalPoint->SetVSetPointData(nDatacount, data, (WORD)m_nVCalRange);

	m_wndListCheck1.UpdateText();
	nDatacount = m_wndListCheck1.GetItemCount();
	if(nDatacount > CAL_MAX_CALIB_CHECK_POINT)
	{
		nDatacount = CAL_MAX_CALIB_CHECK_POINT;
	}
	for(i =0; i<nDatacount; i++)
	{
		strTmp = m_wndListCheck1.GetItemText(i, 1);
		data[i] = atof(strTmp);
	}
	m_pCalPoint->SetVCheckPointData(nDatacount, data, (WORD)m_nVCheckRange);

	m_wndListPoint2.UpdateText();
	nDatacount = m_wndListPoint2.GetItemCount();
	if(nDatacount > CAL_MAX_CALIB_SET_POINT)
	{
		nDatacount = CAL_MAX_CALIB_SET_POINT;
	}
	for(i =0; i<nDatacount; i++)
	{
		strTmp = m_wndListPoint2.GetItemText(i, 1);
		data[i] = atof(strTmp);
	}
	m_pCalPoint->SetISetPointData(nDatacount, data, (WORD)m_nICalRange);

	m_wndListCheck2.UpdateText();
	nDatacount = m_wndListCheck2.GetItemCount();
	if(nDatacount > CAL_MAX_CALIB_CHECK_POINT)
	{
		nDatacount = CAL_MAX_CALIB_CHECK_POINT;
	}
	for(i =0; i<nDatacount; i++)
	{
		strTmp = m_wndListCheck2.GetItemText(i, 1);
		data[i] = atof(strTmp);
	}
	m_pCalPoint->SetICheckPointData(nDatacount, data, (WORD)m_nICheckRange);

	m_pCalPoint->SortData();

	m_wndShuntT.UpdateText();
	nDatacount = m_wndShuntT.GetItemCount();
	if(nDatacount > CAL_MAX_BOARD_NUM)
	{
		nDatacount = CAL_MAX_BOARD_NUM;
	}

	for(i =0; i<CAL_MAX_BOARD_NUM; i++)
	{	
		strTmp = m_wndShuntT.GetItemText(i, 1);
		shuntData[i] = atof(strTmp);
	}

	m_pCalPoint->SetShuntT( shuntData );

	m_wndShuntR.UpdateText();
	nDatacount = m_wndShuntR.GetItemCount();
	if(nDatacount > CAL_MAX_BOARD_NUM)
	{
		nDatacount = CAL_MAX_BOARD_NUM;
	}

	for(i =0; i<CAL_MAX_BOARD_NUM; i++)
	{
		strTmp = m_wndShuntR.GetItemText(i, 1);
		shuntData[i] = atof(strTmp);
	}

	m_pCalPoint->SetShuntR( shuntData );

	m_wndShuntSerial.UpdateText();
	nDatacount = m_wndShuntSerial.GetItemCount();
	if(nDatacount > CAL_MAX_BOARD_NUM)
	{
		nDatacount = CAL_MAX_BOARD_NUM;
	}

	for(i =0; i<CAL_MAX_BOARD_NUM; i++)
	{
		strTmp = m_wndShuntSerial.GetItemText(i, 1);
		sprintf(&shuntDataSerial[i][0], "%s", strTmp.Left(15));

		m_pCalPoint->SetShuntSerial( i, &shuntDataSerial[i][0] );
	}
}

void CCaliSetDlg::OnCalipoint() 
{
	UpdateData();
	m_wndListPoint2.m_bCheckCali = m_bCaliPoint;
}

void CCaliSetDlg::OnCheckpoint() 
{
	UpdateData();
	m_wndListCheck2.m_bCheckCali = m_bCheckPoint;
}

/*
//	UINT nID = CFile::modeWrite | CFile::shareDenyNone;
//
//	CAL_POINT aCalPoint, aCheckPoint;
//	CString strTmp;
//	CListCtrl* pList = NULL;
//
//	CFile aFile;
//	CFileFind aFinder;
//
//	//////////////////////////////////////////////////////////////////////
//	// 1. Loading Calibration Point File
//	CString strFileName = _T("Cal.dat");
//	if( aFinder.FindFile(strFileName) == FALSE )
//		nID |= CFile::modeCreate;
//	aFinder.Close();
//	
//	TRY
//		aFile.Open(strFileName, nID );
//	CATCH( CFileException, ex )
//	{
//		ex->Delete();
//		return false;
//	}
//	END_CATCH
//
//	for( int nI = 0; nI < MAX_CALIB_RANGE; nI++ )
//	{
//		switch( nI )
//		{
//		case 0://Point1
//			{
//				pList = &m_wndListPoint1;
//				break;
//			}
//		case 1://:Point2
//			{
//				pList = &m_wndListPoint2;
//				break;
//			}
//		case 2://Point3
//			{
//				pList = &m_wndListPoint3;
//				break;
//			}
//		}
//
//		ZeroMemory(&aCalPoint, sizeof(CAL_POINT));
//		aCalPoint.byType = nI;
//		aCalPoint.byValidPointNum = pList->GetItemCount();
//		for( int nX = 0; nX < aCalPoint.byValidPointNum; nX++ )
//		{
//			strTmp = pList->GetItemText(nX, 0);
//			double dblTmp = atof(strTmp) * MULTI_LONG_2_FLOAT;
//			aCalPoint.lPoint[nX] = (long)dblTmp;
//		}
//
//		Sorting(aCalPoint);
//		aFile.Write(&aCalPoint, sizeof(CAL_POINT));
//
////		CopyMemory(&m_pCalPoint[nI], &aCalPoint, sizeof(CAL_POINT));
//	}
//	aFile.Close();
//
//	//////////////////////////////////////////////////////////////////////
//	// 2. Loading Check Point File
//	strFileName = _T("Check.dat");
//	if( aFinder.FindFile(strFileName) == FALSE )
//		nID |= CFile::modeCreate;
//	aFinder.Close();
//
//	TRY
//		aFile.Open(strFileName, nID );
//	CATCH( CFileException, ex )
//	{
//		ex->Delete();
//		return false;
//	}
//	END_CATCH
//
//	for( nI = 0; nI < MAX_CALIB_RANGE; nI++ )
//	{
//		switch( nI )
//		{
//		case 0://Point1
//			{
//				pList = &m_wndListCheck1;
//				break;
//			}
//		case 1://:Point2
//			{
//				pList = &m_wndListCheck2;
//				break;
//			}
//		case 2://Point3
//			{
//				pList = &m_wndListCheck3;
//				break;
//			}
//		}
//
////		ZeroMemory(&aCheckPoint, sizeof(CAL_POINT));
//		aCheckPoint.byType = nI;
//		aCheckPoint.byValidPointNum = pList->GetItemCount();
//		for( int nX = 0; nX < aCheckPoint.byValidPointNum; nX++ )
//		{
//			strTmp = pList->GetItemText(nX, 0);
//			double dblTmp = atof(strTmp) * MULTI_LONG_2_FLOAT;
//			aCheckPoint.lPoint[nX] = (long)dblTmp;
//		}
//
//		Sorting(aCheckPoint);
//		aFile.Write(&aCheckPoint, sizeof(CAL_POINT));
//
////		CopyMemory(&m_pCheckPoint[nI], &aCheckPoint, sizeof(CAL_POINT));
//	}
//	aFile.Close();
*/

/*
//	CAL_POINT aCalPoint, aCheckPoint;
//
//	CListCtrl* pList = NULL;
//	for( int nX = 0; nX < MAX_CALIB_RANGE; nX++ )
//	{
////		CopyMemory(&aCalPoint, &m_pCalPoint[nX], sizeof(CAL_POINT));
////		CopyMemory(&aCheckPoint, &m_pCheckPoint[nX], sizeof(CAL_POINT));
//
//		//////////////////////////////////////////////////////////////////////
//		// 1. Setting Calibration Point
//		switch( aCalPoint.byType )
//		{
//		case 0://Point1
//			{
//				pList = &m_wndListPoint1;
//				break;
//			}
//		case 1://:Point2
//			{
//				pList = &m_wndListPoint2;
//				break;
//			}
//		case 2://Point3
//			{
//				pList = &m_wndListPoint3;
//				break;
//			}
//		}
//
//		ZeroMemory(&lvItem, sizeof(LV_ITEM));
//
//		lvItem.mask = LVIF_TEXT;
//		for( int nI = 0; nI < aCalPoint.byValidPointNum; nI++ )
//		{
//			lvItem.iItem = nI;
//			strTmp.Format("%.3f", (float)aCalPoint.lPoint[nI]/DIVIDE_LONG_2_FLOAT);
//
//			lvItem.pszText = (LPSTR)(LPCTSTR)strTmp;
//			pList->InsertItem(&lvItem);
//		}
//
//		//////////////////////////////////////////////////////////////////////
//		// 2. Setting Check Point
//		switch( aCheckPoint.byType )
//		{
//		case 0://Point1
//			{
//				pList = &m_wndListCheck1;
//				break;
//			}
//		case 1://:Point2
//			{
//				pList = &m_wndListCheck2;
//				break;
//			}
//		case 2://Point3
//			{
//				pList = &m_wndListCheck3;
//				break;
//			}
//		}
//
//		ZeroMemory(&lvItem, sizeof(LV_ITEM));
//
//		lvItem.mask = LVIF_TEXT;
//		for( nI = 0; nI < aCheckPoint.byValidPointNum; nI++ )
//		{
//			lvItem.iItem = nI;
//			strTmp.Format("%.3f", (float)aCheckPoint.lPoint[nI]/DIVIDE_LONG_2_FLOAT);
//
//			lvItem.pszText = (LPSTR)(LPCTSTR)strTmp;
//			pList->InsertItem(&lvItem);
//		}
//	}
*/

void CCaliSetDlg::OnButtonLoad() 
{
	// TODO: Add your control notification handler code here
	m_pCalPoint->LoadPointData();
	LoadCalConfigFile();
}

void CCaliSetDlg::OnBnClickedBtnPreset1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nSelPresetNum = 1;
	fnUpdatePreset();
}

void CCaliSetDlg::OnBnClickedBtnPreset2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nSelPresetNum = 2;
	fnUpdatePreset();
}

void CCaliSetDlg::OnBnClickedBtnPreset3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nSelPresetNum = 3;
	fnUpdatePreset();
}

void CCaliSetDlg::OnBnClickedBtnPreset4()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nSelPresetNum = 4;
	fnUpdatePreset();
}

void CCaliSetDlg::OnBnClickedBtnPresetSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( m_nSelPresetNum != 0 )
	{
		if( MessageBox("Save your current setting?","Confirm", MB_YESNO|MB_ICONQUESTION) == IDYES )
		{
			fnSavePreset( m_nSelPresetNum );

			m_nSelPresetNum = 0;

			fnUpdatePreset();
		}
	}
}

void CCaliSetDlg::OnBnClickedBtnPresetLoad()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( m_nSelPresetNum != 0 )
	{
		if( fnLoadPreset( m_nSelPresetNum ) == FALSE )
		{
			AfxMessageBox("Preset data loading fail!", MB_ICONASTERISK);
		}
	}
}

void CCaliSetDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnOK();
}
