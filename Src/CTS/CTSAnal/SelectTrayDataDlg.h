#if !defined(AFX_SELECTTRAYDATADLG_H__F20AF5E1_7CD0_48B7_A44D_57CC6F25278B__INCLUDED_)
#define AFX_SELECTTRAYDATADLG_H__F20AF5E1_7CD0_48B7_A44D_57CC6F25278B__INCLUDED_

#include "MyGridWnd.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelectTrayDataDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSelectTrayDataDlg dialog

class CSelectTrayDataDlg : public CDialog
{
// Construction
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	CSelectTrayDataDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSelectTrayDataDlg();

	BOOL RequeryTrayList();
	int m_nFilterType;
	CString m_strFilterString;
	CString m_TestSerialNo;
	CString m_strTestLogID;
	BOOL InitTrayListGrid();
	

// Dialog Data
	//{{AFX_DATA(CSelectTrayDataDlg)
	enum { IDD = IDD_SELECT_TRAY_DLG };
	CLabel	m_ctrlDataTitleLabel;
	CLabel m_ctrlSearchNoLabel;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectTrayDataDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CMyGridWnd m_wndTrayListGrid;

	// Generated message map functions
	//{{AFX_MSG(CSelectTrayDataDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnListSave();
	//}}AFX_MSG
    afx_msg LONG OnGridDoubleClick(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELECTTRAYDATADLG_H__F20AF5E1_7CD0_48B7_A44D_57CC6F25278B__INCLUDED_)
