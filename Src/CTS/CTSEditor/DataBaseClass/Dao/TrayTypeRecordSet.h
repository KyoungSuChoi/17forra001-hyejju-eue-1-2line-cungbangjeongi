#if !defined(AFX_TRAYTYPERECORDSET_H__75B70A99_3DE2_49D1_BDCF_DF3C7FC209E8__INCLUDED_)
#define AFX_TRAYTYPERECORDSET_H__75B70A99_3DE2_49D1_BDCF_DF3C7FC209E8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TrayTypeRecordSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTrayTypeRecordSet DAO recordset

class CTrayTypeRecordSet : public CDaoRecordset
{
public:
	CTrayTypeRecordSet(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CTrayTypeRecordSet)

// Field/Param Data
	//{{AFX_FIELD(CTrayTypeRecordSet, CDaoRecordset)
	long	m_TrayType;
	CString	m_TypeName;
	long	m_Row;
	long	m_Col;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTrayTypeRecordSet)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TRAYTYPERECORDSET_H__75B70A99_3DE2_49D1_BDCF_DF3C7FC209E8__INCLUDED_)
