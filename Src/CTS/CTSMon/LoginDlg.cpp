// LoginDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSMon.h"
#include "LoginDlg.h"

//#include "UserRecordset.h"
#include "UserAdminDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLoginDlg dialog

CLoginDlg::CLoginDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLoginDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLoginDlg)
	m_bRemember = FALSE;
	m_strPassword = _T("");
	m_strLoginID = _T("guest");
	//}}AFX_DATA_INIT
	strcpy(m_LoginInfo.szLoginID, "kitsat");
	strcpy(m_LoginInfo.szPassword, "kitsat");
	m_LoginInfo.nPermission = PS_USER_SUPER;
	strcpy(m_LoginInfo.szRegistedDate, "Unknown");
	strcpy(m_LoginInfo.szUserName, "Kim Byung Hum");
	strcpy(m_LoginInfo.szDescription, "Supervisor");
	m_LoginInfo.lAutoLogOutTime = 0;
	m_LoginInfo.bUseAutoLogOut = FALSE;
}


void CLoginDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLoginDlg)
	DDX_Check(pDX, IDC_REMEMBER, m_bRemember);
	DDX_Text(pDX, IDC_PASSWORD, m_strPassword);
	DDX_Text(pDX, IDC_LOGINID, m_strLoginID);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CLoginDlg, CDialog)
	//{{AFX_MSG_MAP(CLoginDlg)
	ON_BN_CLICKED(IDOK, OnOk)
	ON_EN_CHANGE(IDC_PASSWORD, OnChangePassword)
	ON_EN_CHANGE(IDC_LOGINID, OnChangeLoginid)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLoginDlg message handlers

void CLoginDlg::OnOk() 
{
	// TODO: Add your control notification handler code here
	if(IDPasswordCheck() == FALSE)
	{
		AfxMessageBox(GetStringTable(IDS_TEXT_NOT_REGISTED_USER));
		return;	
	}
	CDialog::OnOK();
}

BOOL CLoginDlg::IDPasswordCheck()
{
	UpdateData(TRUE);
//	m_strLoginID.MakeUpper();
//	m_strPassword.MakeUpper();
	if(m_strLoginID == "kitsat" && m_strPassword == "kitsat")		//System master ID(Kim Byung Hum)
	{
		strcpy(m_LoginInfo.szLoginID, "kitsat");
		strcpy(m_LoginInfo.szPassword, "kitsat");
		m_LoginInfo.nPermission = PS_USER_SUPER;
		strcpy(m_LoginInfo.szRegistedDate, "Unknown");
		strcpy(m_LoginInfo.szUserName, "Kim Byung Hum");
		strcpy(m_LoginInfo.szDescription, "Supervisor");
		m_LoginInfo.lAutoLogOutTime = 0;
		m_LoginInfo.bUseAutoLogOut = FALSE;
		return TRUE;
	}

	if(m_strLoginID == "guest" && m_strPassword == "")				//Default Guest ID
	{
		strcpy(m_LoginInfo.szLoginID, "guest");
		strcpy(m_LoginInfo.szPassword, "");
		m_LoginInfo.nPermission = PS_USER_GUEST;
		strcpy(m_LoginInfo.szRegistedDate, "Unknown");
		strcpy(m_LoginInfo.szUserName, "defalult User");
		strcpy(m_LoginInfo.szDescription, "Guest");
		m_LoginInfo.lAutoLogOutTime = 0;
		m_LoginInfo.bUseAutoLogOut = FALSE;
		return TRUE;			
	}

	STR_LOGIN	loginData;
	CUserAdminDlg SeekUser;
	if(SeekUser.SearchUser(m_strLoginID, TRUE, m_strPassword, &loginData) == FALSE)
	{
		return FALSE;
	}
	memcpy(&m_LoginInfo, &loginData, sizeof(loginData));

	WriteLastLogID(m_LoginInfo);
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "Logset", m_bRemember);
	return TRUE;
}

BOOL CLoginDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// TODO: Add extra initialization here
	m_LoginInfo = ReadLastLogID();
	GetDlgItem(IDC_LOGINID)->SetFocus();
	
	m_strLoginID.Format("%s", m_LoginInfo.szLoginID);
	m_strPassword.Format("%s", m_LoginInfo.szPassword);

//	m_bRemember = GetPrivateProfileInt("FORMSET", "Logset", 0, INI_FILE);  // initialization file name
	
	m_bRemember = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "Logset", 0);

	if(m_bRemember == FALSE)
	{
		m_strPassword.Empty();
	}
		
	UpdateData(FALSE);
	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CLoginDlg::OnChangePassword() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(m_strPassword.GetLength() > EP_MAX_PASSWORD_LENGTH)	
	{
		m_strPassword.Delete(EP_MAX_PASSWORD_LENGTH);
		UpdateData(FALSE);
	}
}

void CLoginDlg::OnChangeLoginid() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(m_strLoginID.GetLength() > EP_MAX_LONINID_LENGTH)	
	{
		m_strLoginID.Delete(EP_MAX_LONINID_LENGTH);
		UpdateData(FALSE);
	}
}
