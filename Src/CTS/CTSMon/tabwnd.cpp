// TabbedWnd.cpp : implementation file
//

#include "stdafx.h"
#include "CTSMon.h"
#include "TabWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifdef WIN32
IMPLEMENT_DYNCREATE(CTabbedWnd, CWnd)
#else
IMPLEMENT_DYNAMIC(CTabbedWnd,CWnd)
#endif


/////////////////////////////////////////////////////////////////////////////
// CTabbedWnd

CTabbedWnd::CTabbedWnd()
{
	m_nCurTabIndex = 0;
	m_nTabIndex_ManualControlView = 1;
	m_nTabIndex_MonitoringView = 2;
	m_nTabIndex_ConnectionView = 3;
	m_nTabIndex_OperationView = 5;
	
	m_pManualControlView = NULL;
	m_pTopView		= NULL;			// 전체 Module & Channel Monitoring
	m_pConnectionView = NULL;
	m_pOperationView = NULL;

	LanguageinitMonConfig();
}

CTabbedWnd::~CTabbedWnd()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

BEGIN_MESSAGE_MAP(CTabbedWnd, CWnd)
	//{{AFX_MSG_MAP(CTabbedWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
	ON_MESSAGE(TCM_TABSEL, OnTabSelected)
END_MESSAGE_MAP()

/*
void CTabbedWnd::InitEmbeddedSplitter(CCreateContext* pContext,CWnd* pParent,int cx,int cy) {
	ASSERT(pContext);

	VERIFY(m_wndSplitter.CreateStatic(pParent, 1, 2));
	SIZE mySize;
	mySize.cx = cx/2;
	mySize.cy = cy;

	// Embed "view 3" as pane 0
	m_wndSplitter.CreateView ( 0, 0, RUNTIME_CLASS(CDemoView3), mySize, pContext );

	// Embed "view 4" as pane 1
	m_wndSplitter.CreateView ( 0, 1, RUNTIME_CLASS(CDemoView4), mySize, pContext );
}
*/

/////////////////////////////////////////////////////////////////////////////
// CTabbedWnd message handlers

int CTabbedWnd::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (m_tabWnd.Create(this, WS_CHILD | WS_VISIBLE | TWS_TABS_ON_TOP))
	{
		CCreateContext* pContext = (CCreateContext*)lpCreateStruct->lpCreateParams;
		ASSERT(pContext->m_pCurrentDoc);

	//	m_tabWnd.SetNotifyWnd(this);
		// m_tabWnd.SetTabStyle( TCS_TABS_ON_LEFT );
		m_tabWnd.SetTabStyle( TCS_TABS_ON_TOP );

		DWORD bySystemType;
		//Formation과 IROCV 구분
		bySystemType = ((CCTSMonApp*)AfxGetApp())->GetSystemType();

		switch(g_nLanguage)
		{
		case 0:
			{
				m_ActiveFont.CreateFont (18, 0, 0, 0, FW_BOLD, 0, 0, 0, 
					DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_CHARACTER_PRECIS, 
					DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, "");

				m_InactiveFont.CreateFont (14, 0, 0, 0, FW_NORMAL, 0, 0, 0, 
					DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_CHARACTER_PRECIS, 
					DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, "굴림");

				break;
			}

		case 1:
			{
				m_ActiveFont.CreateFont (18, 0, 0, 0, FW_BOLD, 0, 0, 0, 
					DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_CHARACTER_PRECIS, 
					DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, "");

				m_InactiveFont.CreateFont (14, 0, 0, 0, FW_NORMAL, 0, 0, 0, 
					DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_CHARACTER_PRECIS, 
					DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, "Verdana");
				break;
			}

		case 2:
			{
				m_ActiveFont.CreateFont (18, 0, 0, 0, FW_BOLD, 0, 0, 0, 
					DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_CHARACTER_PRECIS, 
					DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, "");

				m_InactiveFont.CreateFont (14, 0, 0, 0, FW_NORMAL, 0, 0, 0, 
					DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_CHARACTER_PRECIS, 
					DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, "SimSun-ExtB");
				break;
			}
		}

	

		SECTab* pTab = NULL;
		int index = 1;
		
		if(bySystemType == EP_ID_FORM_OP_SYSTEM || bySystemType == EP_ID_IROCV_SYSTEM)
		{
			pTab = m_tabWnd.AddTab( RUNTIME_CLASS(CManualControlView), TEXT_LANG[0], pContext );
			
			ASSERT(pTab->IsKindOf(RUNTIME_CLASS(SEC3DTab)));
			if (pTab == NULL)	return FALSE;
			m_pManualControlView = (CManualControlView*)pTab->m_pClient;
			// m_tabWnd.SetTabIcon( index++, IDI_CHANNELVIEW );			
			m_nTabIndex_ManualControlView = index;

			index++;

			// Monitor 화면 
			pTab = m_tabWnd.AddTab(RUNTIME_CLASS(CTopView), TEXT_LANG[1], pContext );
			
			ASSERT(pTab->IsKindOf(RUNTIME_CLASS(SEC3DTab)));
			if (pTab == NULL)	return FALSE;
			m_pTopView = (CTopView*)pTab->m_pClient;
			// m_tabWnd.SetTabIcon( index++, IDI_TOP );
			m_nTabIndex_MonitoringView = index;

			index++;			
				
			// Detail 화면 
			pTab = m_tabWnd.AddTab( RUNTIME_CLASS(CConnectionView), TEXT_LANG[2], pContext );
			
			ASSERT(pTab->IsKindOf(RUNTIME_CLASS(SEC3DTab)));
			if (pTab == NULL)	return FALSE;
			m_pConnectionView = (CConnectionView*)pTab->m_pClient;
			// m_tabWnd.SetTabIcon( index++, IDI_CHANNELVIEW );
			m_nTabIndex_DetailView = index;			

			index++;
		
			pTab = m_tabWnd.AddTab( RUNTIME_CLASS(COperationView), TEXT_LANG[3], pContext );
			
			ASSERT(pTab->IsKindOf(RUNTIME_CLASS(SEC3DTab)));
			if (pTab == NULL)	return FALSE;
			m_pOperationView = (COperationView*)pTab->m_pClient;
			// m_tabWnd.SetTabIcon( index++, IDI_CHANNELVIEW );
			m_nTabIndex_OperationView = index;			
		}

//		m_tabWnd.SetWindowPos(&wndTop, 0, 0, lpcs->cx, lpcs->cy, SWP_SHOWWINDOW );
		m_tabWnd.SetWindowPos( &wndTop, 0, 0, lpCreateStruct->cx, lpCreateStruct->cy, SWP_SHOWWINDOW );
		m_tabWnd.SetFontActiveTab(&m_ActiveFont);
		m_tabWnd.SetFontInactiveTab(&m_InactiveFont);
		m_tabWnd.ActivateTab(m_nCurTabIndex);
	}
	return 0;
}

void CTabbedWnd::OnSize(UINT nType, int cx, int cy) 
{
	CWnd::OnSize(nType, cx, cy);
	m_tabWnd.SetWindowPos( &wndTop, 0, 0, cx, cy, SWP_SHOWWINDOW );
}

void CTabbedWnd::StopGroupTimer()
{	
	if( m_pManualControlView )
	{
		m_pManualControlView->StopMonitoring();
	}
}


void CTabbedWnd::UpdateGroupState(int nModuleID, int nGroupIndex)
{
	if( nModuleID > 0 )
	{
		if( m_pManualControlView )
		{
			m_pManualControlView->UpdateGroupState(nModuleID);
		}

		if( m_pTopView )
		{
			m_pTopView->UpdateGroupState(nModuleID);
		}

		if( m_pConnectionView )
		{
			m_pConnectionView->UpdateGroupState(nModuleID);
		}

		if( m_pOperationView )
		{
			m_pOperationView->UpdateGroupState(nModuleID);
		}		
	}	
}

LRESULT CTabbedWnd::OnTabSelected(WPARAM wParam, LPARAM lParam)
{
	m_nCurTabIndex = wParam;

	SECTABINFO *lpTabInfo = (SECTABINFO *)lParam;

	switch( lpTabInfo->nIndexPrevTab )
	{
	case TAB_INDEX_MANUALCONTROL:
		{
			if( m_pManualControlView )
			{
				m_pManualControlView->StopMonitoring();
			}		
		}
		break;
	/*
	case TAB_INDEX_MONITORING:
		{
			if( m_pTopView )
			{
				m_pTopView->StopMonitoring();
			}
		}
		break;
	case TAB_INDEX_CONNECTION:
		{
			if( m_pConnectionView )
			{
				m_pConnectionView->StopMonitoring();
			}
		}
		break;
	case TAB_INDEX_OPERATION:
		{
			if( m_pOperationView )
			{
				m_pOperationView->StopMonitoring();
			}
		}
		break;
	*/
	}
		
	// 2. 선택한 창 활성화
	switch( lpTabInfo->nIndexActiveTab )
	{
	case TAB_INDEX_MANUALCONTROL:
		{
			if( m_pManualControlView )
			{
				m_pManualControlView->StartMonitoring();
			}
		}
		break;
	case TAB_INDEX_MONITORING:
		{
			if( m_pTopView )
			{
				m_pTopView->StartMonitoring();
			}
		}
		break;
	case TAB_INDEX_CONNECTION:
		{
			if( m_pConnectionView )
			{
				m_pConnectionView->StartMonitoring();
			}
		}
		break;
	case TAB_INDEX_OPERATION:
		{
			if( m_pOperationView )
			{
				m_pOperationView->StartMonitoring();
			}
		}
		break;
	}
	
	return 1;
}

bool CTabbedWnd::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CTabbedWnd"), _T("TEXT_CTabbedWnd_CNT"), _T("TEXT_CTabbedWnd_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CTabbedWnd_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CTabbedWnd"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}