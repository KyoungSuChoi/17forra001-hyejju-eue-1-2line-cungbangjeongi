// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "MainFrm.h"

#include "UserSetDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include "ModuleAddDlg.h"
#include "ModuleRunTimeDlg.h"

/////////////////////////////////////////////////////////////////////////////
// CMainFrame
GRAPH_CFG DEFAULT_GRAPH_CFG = 
{
	{ -5.0000, -2000.0, -2000.0,    0.0, -20.0,  0.0 },
	{  5.0000,  2000.0,  2000.0, 1000.0, 200.0, 40.0 },
	{ RGB(255,0,0),RGB(0,255,255),RGB(255,255,0),RGB(0,255,0),RGB(255,0,255),RGB(0,0,255) },
	1
};

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_UPDATE_COMMAND_UI(ID_FILE_OPEN, OnUpdateFileOpen)
	ON_WM_DROPFILES()
	ON_COMMAND(ID_FTP_DLG, OnFtpDlg)
	ON_WM_COPYDATA()
	ON_COMMAND(ID_USER_OPTION, OnUserOption)
	ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
	ON_WM_INITMENUPOPUP()
	ON_COMMAND(ID_OP_TIME, OnOpTime)
	//}}AFX_MSG_MAP
//	ON_MESSAGE(EPWM_MODULE_CONNECTED, OnConnected)

END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

bool CMainFrame::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CMainFrame"), _T("TEXT_CMainFrame_CNT"), _T("TEXT_CMainFrame_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CMainFrame_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CMainFrame"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

CMainFrame::CMainFrame()
{
	LanguageinitMonConfig();

	// TODO: add member initialization code here
	m_pDbView     = NULL;
	m_pResultView = NULL;
}

CMainFrame::~CMainFrame()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
/*	if(m_pFtpDlg != NULL)
	{
		delete m_pFtpDlg;
		m_pFtpDlg = NULL;
	}
*/
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	/*
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}
		
	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
	// m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	// EnableDocking(CBRS_ALIGN_ANY);
	// DockControlBar(&m_wndToolBar);

	BOOL ret = GetPrivateProfileStruct("CONFIG", "TOPCONFIG", &m_TopConfig, sizeof(STR_TOP_CONFIG), INI_FILE);
	if (ret == FALSE)
	{
		m_TopConfig = GetDefaultTopConfigSet();
		WriteTopConfig(m_TopConfig);
//		WritePrivateProfileStruct("CONFIG", "TOPCONFIG", &m_TopConfig, sizeof(STR_TOP_CONFIG), INI_FILE);
	}
*/
	m_GraphConfig = DEFAULT_GRAPH_CFG;	
	DragAcceptFiles();


	//20201030ksj
	CString strVer;
	strVer.Format("CTSAnal (SKEUE_%s_%s)",MacroDateToCString(__DATE__), __TIME__);
	SetWindowText(strVer);
	return 0;
}

CString CMainFrame::MacroDateToCString(const char *MacroDate)	//20201030ksj
{
	const int comfile_date_len = 12;

	// error check
	if (NULL == MacroDate || comfile_date_len - 1 != strlen(MacroDate))
		return "";

	const char month_names[] = "JanFebMarAprMayJunJulAugSepOctNovDec";

	char s_month[5] = {0,};
	int iyear = 0, iday = 0;

	sscanf(MacroDate, "%s %d %d", s_month, &iday, &iyear);
	int imonth = (strstr(month_names, s_month) - month_names) / 3 + 1;

	CString strDate;
	strDate.Format("%4d-%2d-%2d",iyear,imonth,iday);
	return strDate;
}
BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs
	cs.style &= ~FWS_ADDTOTITLE;

	WNDCLASS wc;
	::GetClassInfo( AfxGetInstanceHandle(), cs.lpszClass, &wc );
    wc.lpszClassName = cs.lpszClass = ANAL_CLASS_NAME;    
	wc.hIcon = ::LoadIcon(AfxGetInstanceHandle(), "IDR_MAINFRAME");
    ::RegisterClass( &wc );

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext) 
{
	// TODO: Add your specialized code here and/or call the base class
	if (!m_tabWnd.Create(this, WS_CHILD | WS_VISIBLE | TWS_TABS_ON_TOP))
	{
		return FALSE;
	}
	m_tabWnd.SetNotifyWnd(this);

//	m_tabWnd.SetTabStyle( TCS_TABS_ON_LEFT );

	m_ActiveFont.CreateFont (12, 0, 0, 0, FW_BOLD, 0, 0, 0, 
			DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_CHARACTER_PRECIS, 
			DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, TEXT_LANG[0]);//"굴림"
	m_InactiveFont.CreateFont (12, 0, 0, 0, FW_NORMAL, 0, 0, 0, 
			DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_CHARACTER_PRECIS, 
			DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, TEXT_LANG[0]);//"굴림"

	SECTab* pTab = NULL;
	int index = 0;

	pTab = m_tabWnd.AddTab(RUNTIME_CLASS(CResultView), _T("Data"), pContext );
	ASSERT(pTab->IsKindOf(RUNTIME_CLASS(SEC3DTab)));
	if (pTab == NULL)	return FALSE;
	m_pResultView = (CResultView*)pTab->m_pClient;
	m_tabWnd.SetTabIcon( index++, IDI_FILE_VIEW );

	/*
	// 1. LG 온라인 데이터 검색 부분
#ifdef _DEBUG
	pTab = m_tabWnd.AddTab(RUNTIME_CLASS(CSearchView), _T("Search"), pContext );
	ASSERT(pTab->IsKindOf(RUNTIME_CLASS(SEC3DTab)));
	if (pTab == NULL)	return FALSE;
	m_pSearchView = (CSearchView*)pTab->m_pClient;
	m_tabWnd.SetTabIcon( index++, IDI_ICON_MAGNIFY );

	pTab = m_tabWnd.AddTab(RUNTIME_CLASS(CSearchMDBView), _T("SearchDB"), pContext );
	ASSERT(pTab->IsKindOf(RUNTIME_CLASS(SEC3DTab)));
	if (pTab == NULL)	return FALSE;
	m_pSearchMDBView = (CSearchMDBView*)pTab->m_pClient;
	m_tabWnd.SetTabIcon( index++, IDI_ICON_MAGNIFY );

	pTab = m_tabWnd.AddTab(RUNTIME_CLASS(CCTSAnalView), _T("Tray"), pContext );
	ASSERT(pTab->IsKindOf(RUNTIME_CLASS(SEC3DTab)));
	if (pTab == NULL)	return FALSE;
	m_pDbView = (CCTSAnalView*)pTab->m_pClient;
	m_tabWnd.SetTabIcon( index++, IDI_DB_VIEW );

	pTab = m_tabWnd.AddTab(RUNTIME_CLASS(CResultGraphView), _T("Graph"), pContext );
	ASSERT(pTab->IsKindOf(RUNTIME_CLASS(SEC3DTab)));
	if (pTab == NULL)	return FALSE;
	m_pResultGraphView = (CResultGraphView*)pTab->m_pClient;
	m_tabWnd.SetTabIcon( index++, IDI_GRAPHICON );
#endif
	*/
	m_tabWnd.SetWindowPos(&wndTop, 0, 0, lpcs->cx, lpcs->cy, SWP_SHOWWINDOW );

	m_tabWnd.ActivateTab(0);
	m_tabWnd.SetFontActiveTab(&m_ActiveFont);
	m_tabWnd.SetFontInactiveTab(&m_InactiveFont);
	return TRUE;
//	return CFrameWnd::OnCreateClient(lpcs, pContext);
}

void CMainFrame::OnDropFiles(HDROP hDropInfo) 
{
	// TODO: Add your message handler code here and/or call default
	char str[_MAX_PATH] ={0};
	int cnt = 0xFFFFFFFF;
	//Drop된 파일의 수을 구한다.
	::DragQueryFile(hDropInfo, cnt, (LPTSTR)str, _MAX_PATH);

	if(cnt > 0)
	{
		//제일 첫번째 파일만 사용한다.
		ZeroMemory(str, _MAX_PATH);
		::DragQueryFile(hDropInfo, 0, (LPTSTR)str, _MAX_PATH); 
		CString strTemp, strFileName;
		strFileName = str;

		strTemp = strFileName.Mid(strFileName.ReverseFind('.')+1);
		strTemp.MakeLower();

		if(strTemp == "csv")
		{
			OpenGraphFile(strFileName);
		}
		else if(strTemp == BF_RESULT_FILE_EXT)
		{
			OpenDataFile(strFileName);
		}
		else
		{
			AfxMessageBox(TEXT_LANG[1]);//"지원되지 않는 파일 형식입니다."
			return;
		}
	}
	::DragFinish(hDropInfo);

//	CFrameWnd::OnDropFiles(hDropInfo);
}


void CMainFrame::OnFtpDlg() 
{
	// TODO: Add your command handler code here
/*		if(m_pFtpDlg != NULL)
		{
			delete m_pFtpDlg;
			m_pFtpDlg = NULL;
		}

		
   		m_pFtpDlg = new CFtpDlg;
       	m_pFtpDlg->Create(IDD_FTP_DLG, GetDesktopWindow());
       	m_pFtpDlg->SetOwner(this);
		m_pFtpDlg->ShowWindow(SW_SHOW);
       	m_pFtpDlg->InitDlg();
*/
	
/*	CModuleAddDlg *pDlg1;
	pDlg1 = new CModuleAddDlg();
	pDlg1->m_bAddDlg = FALSE;
	if(pDlg1->DoModal() != IDOK)	
	{
		delete pDlg1;
		pDlg1 = NULL;
		return;
	}
	int nModuleID = pDlg1->GetModuleID();
	delete pDlg1;
	pDlg1 =  NULL;
		
	CCTSAnalDoc *pDoc = (CCTSAnalDoc *)GetActiveDocument();
	ASSERT(pDoc);

	CString strIP;
	CString strTemp;

	CFtpDlg *pDlg;
	pDlg = new CFtpDlg();
	strIP = pDlg->GetIPAddress(nModuleID);
	delete pDlg;
	
	if(strIP.IsEmpty())
	{
		strIP.Format("%s 의 IP Address를 찾을 수 없습니다.", pDoc->ModuleName(nModuleID));
		AfxMessageBox(strIP);
		return;
	}
*/
/*	STARTUPINFO	stStartUpInfo;
	PROCESS_INFORMATION	ProcessInfo;
	ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
	ZeroMemory(&ProcessInfo, sizeof(PROCESS_INFORMATION));
	
	stStartUpInfo.cb = sizeof(STARTUPINFO);
	stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
	stStartUpInfo.wShowWindow = SW_NORMAL;

	CString strTemp;
	CString strDirTemp;
	{
		TCHAR szCurDir[MAX_PATH];
		::GetModuleFileName(AfxGetApp()->m_hInstance, szCurDir, MAX_PATH);
		strDirTemp = CString(szCurDir).Mid(0, CString(szCurDir).ReverseFind('\\'));
	}
	strTemp.Format("%s\\RealData.exe", strDirTemp);//, strIP, nModuleID);	//IP를 인자로 넘김 
	//strTemp.Format("%s\\RealData.exe %s %d", pDoc->m_strCurFolder, strIP, nModuleID);	//IP를 인자로 넘김 

	BOOL bFlag = CreateProcess(NULL, (LPSTR)(LPCTSTR)strTemp, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &stStartUpInfo, &ProcessInfo);
	if(bFlag == FALSE)
	{
		strTemp.Format("%s\\RealData.exe를 찾을 수 없습니다.", strDirTemp);	//IP를 인자로 넘김 
		//strTemp.Format("%s\\RealData.exe를 찾을 수 없습니다.", pDoc->m_strCurFolder);	//IP를 인자로 넘김 
		MessageBox(strTemp, "실행오류", MB_OK|MB_ICONSTOP);
	}		
*/	
	m_pResultView->DownLoadProfile();

}

BOOL CMainFrame::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct) 
{
	// TODO: Add your message handler code here and/or call default
	//CTSMon에서 전달된 Message
	if(pCopyDataStruct->dwData == 2)
	{
		if(pCopyDataStruct->cbData > 0)	//"\n"
		{
			char *pData = new char[pCopyDataStruct->cbData];
			memcpy(pData, pCopyDataStruct->lpData, pCopyDataStruct->cbData);
			CString strData(pData);
			delete [] pData;
			
/*			if(!strData.IsEmpty())
			{
				((CCTSAnalApp *)AfxGetApp())->m_pDataTemplate->OpenDocumentFile(strData);
			}

*/			//AfxMessageBox(strData);
			m_pResultView->DisplayFile(strData);
		}
	}
	return CFrameWnd::OnCopyData(pWnd, pCopyDataStruct);
}

BOOL CMainFrame::OpenGraphFile(CString strFileName)
{
	
	return FALSE;
}

BOOL CMainFrame::OpenDataFile(CString strFileName)
{
	if(m_pResultView)
	{
		m_tabWnd.ActivateTab(m_pResultView);
		return m_pResultView->DisplayFile(strFileName);
	}
	return FALSE;
}

void CMainFrame::OnUserOption() 
{
	// TODO: Add your command handler code here
	CUserSetDlg dlg;
	if(dlg.DoModal() == IDOK)
	{
		CCTSAnalDoc *pDoc = (CCTSAnalDoc *)GetActiveDocument();
		pDoc->UpdateUnitSetting();
		m_pResultView->UpdateDataUnit();
	}
}

void CMainFrame::OnFileOpen() 
{
	// TODO: Add your command handler code here
	CWnd *pWnd;
	if(m_tabWnd.GetActiveTab(pWnd))
	{
		if(m_pResultView == pWnd)
		{
			m_pResultView->OnRltFileOpen();
		}
	}
}

void CMainFrame::OnUpdateFileOpen(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	CWnd *pWnd;
	if(m_tabWnd.GetActiveTab(pWnd))
	{
		if( m_pResultView == pWnd )
		{
			pCmdUI->Enable(TRUE);
		}
		else
		{
			pCmdUI->Enable(FALSE);
		}
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}
}

void CMainFrame::OnOpTime() 
{
	// TODO: Add your command handler code here
	CModuleRunTimeDlg *pDlg;
	pDlg = new CModuleRunTimeDlg();
	pDlg->DoModal();

	delete pDlg;
	pDlg = NULL;	
}
