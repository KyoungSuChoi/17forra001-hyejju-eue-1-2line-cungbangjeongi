// FieldAddDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "FieldAddDlg.h"

#include "SimplexParserDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFieldAddDlg dialog

bool CFieldAddDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CFieldAddDlg"), _T("TEXT_CFieldAddDlg_CNT"), _T("TEXT_CFieldAddDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CFieldAddDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CFieldAddDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

CFieldAddDlg::CFieldAddDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFieldAddDlg::IDD, pParent)
{
	LanguageinitMonConfig();

	//{{AFX_DATA_INIT(CFieldAddDlg)
	m_strFieldName = _T("");
	//}}AFX_DATA_INIT
	ZeroMemory(&m_ProcType, sizeof(STR_MSG_DATA));
	m_nDataType = 0;
}

CFieldAddDlg::~CFieldAddDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


void CFieldAddDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFieldAddDlg)
	DDX_Control(pDX, IDC_COMBO2, m_ctrlDataTypeCombo);
	DDX_Control(pDX, IDC_COMBO1, m_ctrlProcTypeCombo);
	DDX_Text(pDX, IDC_EDIT1, m_strFieldName);
	DDV_MaxChars(pDX, m_strFieldName, 16);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFieldAddDlg, CDialog)
	//{{AFX_MSG_MAP(CFieldAddDlg)
	ON_WM_DESTROY()
	ON_CBN_SELCHANGE(IDC_COMBO1, OnSelchangeCombo1)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_EXTEND_FORM_BUTTON, OnExtendFormButton)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFieldAddDlg message handlers

BOOL CFieldAddDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	STR_MSG_DATA *pObject;
	int nIndex = 0;			//Display 할 Index
	int nComboIndex = 0;			//Combo Add Index

//	m_ctrlProcTypeCombo.AddString("사용자 수식입력");
	for(int i =0; i<m_apProcType->GetSize(); i++)
	{
		pObject = (STR_MSG_DATA *)m_apProcType->GetAt(i);
		if( pObject->nCode >= EP_PROC_TYPE_CHARGE_LOW)
		{
			m_ctrlProcTypeCombo.AddString(pObject->szMessage);
			m_ctrlProcTypeCombo.SetItemData(nComboIndex, i);				//Index를 Data로 지정 

			//Diaply할 항목 검사 
			if(strcmp(m_ProcType.szMessage, pObject->szMessage) == 0)	nIndex = nComboIndex;

			nComboIndex++;
		}
	}
	
	m_ctrlProcTypeCombo.SetCurSel(nIndex);						//현재 항목을 표시 
	nComboIndex = m_ctrlProcTypeCombo.GetItemData(nIndex);		//배열 Index를 구한다.
	pObject = (STR_MSG_DATA *)m_apProcType->GetAt(nComboIndex);	//배열에서 정보를 구함 
	
	
	int nType = GetStepTypeofProcType(pObject->nCode);
	
	SetDataTypeCombo(nType, m_nDataType);

	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CFieldAddDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here
}

BOOL CFieldAddDlg::SetDataTypeCombo(int stepType, int dataType)
{
	m_ctrlDataTypeCombo.ResetContent();
	switch(stepType)
	{
	case EP_TYPE_CHARGE:	
	case EP_TYPE_DISCHARGE:	
		{
			m_ctrlDataTypeCombo.AddString(TEXT_LANG[0]);//"용량등급 Code"
			m_ctrlDataTypeCombo.SetItemData(0, EP_GRADE_CODE);
			m_ctrlDataTypeCombo.AddString(TEXT_LANG[1]);//"전압"
			m_ctrlDataTypeCombo.SetItemData(1, EP_VOLTAGE);
			m_ctrlDataTypeCombo.AddString(TEXT_LANG[2]);//"전류"
			m_ctrlDataTypeCombo.SetItemData(2, EP_CURRENT);
			m_ctrlDataTypeCombo.AddString(TEXT_LANG[3]);//"용량"
			m_ctrlDataTypeCombo.SetItemData(3, EP_CAPACITY);
			m_ctrlDataTypeCombo.AddString(TEXT_LANG[4]);//"시간"
			m_ctrlDataTypeCombo.SetItemData(4, EP_STEP_TIME);
		}
		break;
	case EP_TYPE_REST:
		{
			m_ctrlDataTypeCombo.AddString(TEXT_LANG[1]);//"전압"
			m_ctrlDataTypeCombo.SetItemData(0, EP_VOLTAGE);			
		}
		break;
	case EP_TYPE_OCV:			
		m_ctrlDataTypeCombo.AddString(TEXT_LANG[5]);//"OCV 등급 Code"
		m_ctrlDataTypeCombo.SetItemData(0, EP_GRADE_CODE);
		m_ctrlDataTypeCombo.AddString(TEXT_LANG[6]);//"OCV"
		m_ctrlDataTypeCombo.SetItemData(1, EP_VOLTAGE);
		break;
	case EP_TYPE_IMPEDANCE:		
		m_ctrlDataTypeCombo.AddString(TEXT_LANG[7]);//"Impedance 등급 Code"
		m_ctrlDataTypeCombo.SetItemData(0, EP_GRADE_CODE);
		m_ctrlDataTypeCombo.AddString(TEXT_LANG[8]);//"Impedacne"
		m_ctrlDataTypeCombo.SetItemData(1, EP_IMPEDANCE);
		break;
	}

	int dataIndex = 0;
	switch(dataType)
	{
	case EP_GRADE_CODE:	dataIndex = 0;	break;
	case EP_VOLTAGE:	dataIndex =	1;	break;
	case EP_CURRENT:	dataIndex =	2;	break;
	case EP_CAPACITY:	dataIndex =	3;	break;
	case EP_STEP_TIME:	dataIndex =	4;	break;
//	case EP_OCV:		dataIndex =	1;	break;
	case EP_IMPEDANCE:	dataIndex =	1;	break;
	case 0:				dataIndex = 0;	break;
	default :			dataIndex =	-1;	break;
	}

	m_ctrlDataTypeCombo.SetCurSel(dataIndex);
	return TRUE;
}

void CFieldAddDlg::OnSelchangeCombo1() 
{
	// TODO: Add your control notification handler code here
	int index = m_ctrlProcTypeCombo.GetCurSel();
	if(index < 0)	return;
	
	int procCode = m_ctrlProcTypeCombo.GetItemData(index);

	STR_MSG_DATA *pObject;
	pObject = (STR_MSG_DATA *)m_apProcType->GetAt(procCode);

	SetDataTypeCombo(GetStepTypeofProcType(pObject->nCode));
}

void CFieldAddDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
	if(m_strFieldName.IsEmpty())	//Data 이름 검사 
	{
		AfxMessageBox(TEXT_LANG[9]);//"Data 이름이 입력되지 않았습니다."
		GetDlgItem(IDC_EDIT1)->SetFocus();
		return;
	}

	int index = m_ctrlProcTypeCombo.GetCurSel();		//현재 선택 Index를 구해 
	if(index < 0)
	{
		AfxMessageBox(TEXT_LANG[10]);//"공정 Type이 선택되지 않았습니다."
		GetDlgItem(IDC_COMBO1)->SetFocus();		
		return ;
	}
	
	index = m_ctrlProcTypeCombo.GetItemData(index);		//pObject 배열 Index를 구한다.

	STR_MSG_DATA *pObject = (STR_MSG_DATA *)m_apProcType->GetAt(index);
	if(pObject->nCode == 0)
	{
		AfxMessageBox(TEXT_LANG[10]);//"공정 Type이 선택되지 않았습니다."
		GetDlgItem(IDC_COMBO1)->SetFocus();		
		return ;
	}

	memcpy(&m_ProcType, pObject, sizeof(STR_MSG_DATA));

	int nType = GetStepTypeofProcType(pObject->nCode);
	int nIndex = m_ctrlDataTypeCombo.GetCurSel();		//Data Type을 확인 

	if(nIndex <0)
	{
		AfxMessageBox(TEXT_LANG[11]);//"Data 종류가 선택되지 않았습니다."
		GetDlgItem(IDC_COMBO2)->SetFocus();		
		return ;
	}

	switch(nType)
	{
	case EP_TYPE_CHARGE:	
	case EP_TYPE_DISCHARGE:		
			if(nIndex == 0)	m_nDataType = EP_GRADE_CODE;
			if(nIndex == 1)	m_nDataType = EP_VOLTAGE;
			if(nIndex == 2)	m_nDataType = EP_CURRENT;
			if(nIndex == 3)	m_nDataType = EP_CAPACITY;
			if(nIndex == 4)	m_nDataType = EP_STEP_TIME;
		break;
	case EP_TYPE_REST:
		{
			if(nIndex == 0)	m_nDataType = EP_VOLTAGE;
		}
		break;
	case EP_TYPE_OCV:
			if(nIndex == 0)	m_nDataType = EP_GRADE_CODE;
			if(nIndex == 1)	m_nDataType = EP_VOLTAGE;
		break;
	case EP_TYPE_IMPEDANCE:		
			if(nIndex == 0)	m_nDataType = EP_GRADE_CODE;
			if(nIndex == 1)	m_nDataType = EP_IMPEDANCE;
		break;
	}
	CDialog::OnOK();
}


void CFieldAddDlg::OnExtendFormButton() 
{
	// TODO: Add your control notification handler code here
	CSimplexParserDlg	dlg;
	static CString strExpression;

	dlg.m_strFormulainput = strExpression;
	dlg.m_apProcType = m_apProcType;
	if(IDOK != dlg.DoModal())
	{
		return;
	}

	strExpression = dlg.m_strFormulainput;

	TRACE("Input Fromulat Expressiion is %s\n", strExpression);
}
