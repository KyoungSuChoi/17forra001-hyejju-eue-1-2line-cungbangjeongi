#pragma once
#include <vector>
template <class T, int ALLOC_BLOCK_SIZE = 50>
class CFMSMemoryPool : public CFMSStaticSyncParent<T>
{
public:
	static VOID* operator new(size_t iAllocLen)
	{
		CFMSStaticSyncObj Sync2;

		m_isMemPoolEnd = FALSE;

		ASSERT(sizeof(T) == iAllocLen);
		ASSERT(sizeof(T) >= sizeof(UCHAR*));

		if (!m_pucFree)
			allocBlock();

		UCHAR *pReturn = m_pucFree;
		m_pucFree = *reinterpret_cast<UCHAR**>(pReturn);
		++m_iLength;
		////-------------------------------------------------------------------------
		//WCHAR tempStr[1024] = {0,};
		//_snwprintf_s(tempStr, 1024, L"Use Capacity %d m_iLength %d \n", m_iCapacity, m_iLength);
		//OutputDebugString(tempStr);
		////-------------------------------------------------------------------------

		return pReturn;
	}

	static VOID	operator delete(VOID* pDelete)
	{
		CFMSStaticSyncObj Sync2;

		if (m_iLength == 0) throw "delete invalid pointer";
		--m_iLength;

		*reinterpret_cast<UCHAR**>(pDelete) = m_pucFree;
		m_pucFree = static_cast<UCHAR*>(pDelete);
		//if(m_isMemPoolEnd && m_iCapacity)
		//{
		//	for (DWORD i = 0; i < m_dellist.size(); i++)
		//	{
		//		UCHAR *dellobj = m_dellist[i];
		//		delete dellobj;
		//	}
		//	m_iCapacity = 0;
		//}
		//-------------------------------------------------------------------------
		//WCHAR tempStr[1024] = {0,};
		//_snwprintf_s(tempStr, 1024, L"delete Capacity %d m_iLength %d \n", m_iCapacity, m_iLength);
		//OutputDebugString(tempStr);
		////-------------------------------------------------------------------------
	}

	static INT GetLength(VOID) { CFMSStaticSyncObj Sync2; return m_iLength; }
	static INT GetCapacity(VOID) { CFMSStaticSyncObj Sync2; return m_iCapacity;}
	static VOID SetMemPoolEND(VOID) { CFMSStaticSyncObj Sync2; m_isMemPoolEnd = TRUE;}

private:
	static VOID	allocBlock()
	{
		m_pucFree = new UCHAR[sizeof(T) * ALLOC_BLOCK_SIZE];

		UCHAR **ppCur = reinterpret_cast<UCHAR **>(m_pucFree);
		UCHAR *pNext = m_pucFree;

		for (INT i=0;i<ALLOC_BLOCK_SIZE-1;++i)
		{
			pNext += sizeof(T);
			*ppCur = pNext;
			ppCur = reinterpret_cast<UCHAR**>(pNext);
		}

		*ppCur = 0;
		m_iCapacity += ALLOC_BLOCK_SIZE;
		m_dellist.push_back(m_pucFree);
		//-------------------------------------------------------------------------
		TCHAR tempStr[1024] = {0,};
		OutputDebugString("-------------------------------------------------------\n");
		_sntprintf_s(tempStr, 1024, "---------------------------     allocBlock size %d / Capacity %d  \n", sizeof(T) * ALLOC_BLOCK_SIZE, m_iCapacity);
		OutputDebugString(tempStr);
		OutputDebugString("-------------------------------------------------------\n");
		//-------------------------------------------------------------------------
	}

private:
	static UCHAR	*m_pucFree;
	static INT		m_iLength;
	static INT		m_iCapacity;
	static BOOL		m_isMemPoolEnd;
	static std::vector<UCHAR *> m_dellist;

protected:
	~CFMSMemoryPool()
	{
		//-------------------------------------------------------------------------
		//WCHAR tempStr[1024] = {0,};
		//_snwprintf_s(tempStr, 1024, L"~CMemoryPool C : %d L : %d\n", m_iCapacity, m_iLength);
		//OutputDebugString(tempStr);
		//-------------------------------------------------------------------------
	}
};

template <class T, int ALLOC_BLOCK_SIZE>
UCHAR* CFMSMemoryPool<T, ALLOC_BLOCK_SIZE>::m_pucFree;
template <class T, int ALLOC_BLOCK_SIZE>
INT CFMSMemoryPool<T, ALLOC_BLOCK_SIZE>::m_iLength;
template <class T, int ALLOC_BLOCK_SIZE>
INT CFMSMemoryPool<T, ALLOC_BLOCK_SIZE>::m_iCapacity;
template <class T, int ALLOC_BLOCK_SIZE>
BOOL CFMSMemoryPool<T, ALLOC_BLOCK_SIZE>::m_isMemPoolEnd;
template <class T, int ALLOC_BLOCK_SIZE>
std::vector<UCHAR *> CFMSMemoryPool<T, ALLOC_BLOCK_SIZE>::m_dellist;
