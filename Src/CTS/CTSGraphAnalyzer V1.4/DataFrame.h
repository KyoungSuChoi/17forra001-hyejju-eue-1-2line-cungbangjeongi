#if !defined(AFX_DATAFRAME_H__93EB81C7_100B_4EE5_8228_16ACC0ECDD6D__INCLUDED_)
#define AFX_DATAFRAME_H__93EB81C7_100B_4EE5_8228_16ACC0ECDD6D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DataFrame.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDataFrame frame
#include "Gridbar.h"
#include "DataView.h"

class CDataFrame : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CDataFrame)
protected:
	CDataFrame();           // protected constructor used by dynamic creation

// Attributes
public:

// Operations
public:
	CMyGridWnd * GetGridWnd();
	CGridBar m_wndGridBar;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDataFrame)
	public:
	virtual void OnUpdateFrameMenu(BOOL bActive, CWnd* pActivateWnd, HMENU hMenuAlt);
	virtual void ActivateFrame(int nCmdShow = -1);
	virtual BOOL DestroyWindow();
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
protected:
	BOOL VerifyBarState(LPCTSTR lpszProfileName);
	virtual ~CDataFrame();

	// Generated message map functions
	//{{AFX_MSG(CDataFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDataSheet();
	afx_msg void OnUpdateDataSheet(CCmdUI* pCmdUI);
	afx_msg void OnEditCopy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATAFRAME_H__93EB81C7_100B_4EE5_8228_16ACC0ECDD6D__INCLUDED_)
