#include "stdafx.h"

#include "CTSMon.h"

#include "FMS.h"
#include "FM_Unit.h"

#include "FM_STATE.h"

CFM_ST_LOCAL::CFM_ST_LOCAL(FM_STATUS_ID _Eqstid, FM_STATUS_ID _stid, CFM_Unit* _unit)
:CFM_STATE(_Eqstid, _stid, _unit)
, m_pstLOLocal(NULL)
, m_pstLOError(NULL)
{
}


CFM_ST_LOCAL::~CFM_ST_LOCAL(void)
{
	if(m_pstLOLocal)
	{
		delete m_pstLOLocal;
		m_pstLOLocal = NULL;
	}
	if(m_pstLOError)
	{
		delete m_pstLOError;
		m_pstLOError = NULL;
	}
}

VOID CFM_ST_LOCAL::fnEnter()
{
	CHANGE_STATE(LOCAL_ST_LOCAL);
	TRACE("CFM_ST_LOCAL::fnEnter %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_LOCAL::fnProc()
{
	TRACE("CFM_ST_LOCAL::fnProc %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_LOCAL::fnExit()
{
	TRACE("CFM_ST_LOCAL::fnExit %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_LOCAL::fnSBCPorcess(WORD _state)
{
	if(m_Unit->fnGetModule()->GetState() == EP_STATE_LINE_OFF)
	{
		CHANGE_STATE(EQUIP_ST_OFF);
	}
	else
	{
		switch(m_Unit->fnGetModule()->GetOperationMode())
		{
		case EP_OPERATION_LOCAL:
			//CHANGE_STATE(EQUIP_ST_LOCAL);
			break;
		case EP_OPEARTION_MAINTENANCE:
			CHANGE_STATE(EQUIP_ST_MAINT);
			break;
		case EP_OPERATION_AUTO:
			CHANGE_STATE(EQUIP_ST_AUTO);
			break;
		}
	}
	//TRACE("CFM_ST_LOCAL::fnSBCPorcess %d \n", m_Unit->fnGetModuleIdx());
}

FMS_ERRORCODE CFM_ST_LOCAL::fnFMSPorcess(FMS_COMMAND _msgId, st_FMS_PACKET* _recvData)
{
	FMS_ERRORCODE _error = ER_NO_Process;

	switch(_msgId)
	{
	default:
		{
		}
		break;
	}
	TRACE("CFM_ST_LOCAL::fnFMSPorcess %d \n", m_Unit->fnGetModuleIdx());

	return _error;

}