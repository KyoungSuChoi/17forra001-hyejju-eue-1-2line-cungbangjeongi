#pragma once

#include "afxwin.h"
#include "MyGridWnd.h"
#include "CTSMonDoc.h"

// CErcdWriteDlg 대화 상자입니다.

class CErcdWriteDlg : public CDialog
{
	DECLARE_DYNAMIC(CErcdWriteDlg)

public:
	CErcdWriteDlg(CWnd* pParent = NULL , CCTSMonDoc* pDoc  = NULL);   // 표준 생성자입니다.
	virtual ~CErcdWriteDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_WRITE_ERCD_CODE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	void initCtrl();
	void initLabel();
	void initFont();
	void initGrid();
	void initCombo();

	CString GetTargetModuleName();
private:
	EP_ERCD_WRITE_VALUE m_SendERCDWriteValue;

	BOOL GetERCDValueFromUI(EP_ERCD_WRITE_VALUE& sendERCDWriteValue);
	BOOL AddERCDWriteValueEditBox(CString csLog);
	BOOL IsInteger(CString csValue);
public:	
	int m_nCurModuleID;
	int m_nMaxStageCnt;

	CMyGridWnd      m_grid;
	CLabel			m_LabelViewName;
	CLabel			m_CmdTarget;
	CComboBox		m_comboStageID;
	CFont			m_Font;

	CCTSMonDoc* m_pDoc;

	void SetCurrentModule(int nModuleID);
	void OnSBCResponse(int iModuleID, EP_ERCD_WRITE_VALUE ercdValue);
	afx_msg void OnCbnSelchangeComboStageChange();

	DECLARE_MESSAGE_MAP()
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBtnApply();
	afx_msg void OnBnClickedBtnApplyAll();	
	CEdit m_cEditBoxSBCResponseLog;
};