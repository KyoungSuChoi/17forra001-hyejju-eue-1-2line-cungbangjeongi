#pragma once
#include "afxwin.h"
#include "afxcmn.h"

#include "CTSMonDoc.h"

// CSystemsettingDlg 대화 상자입니다.
#define MAX_LABEL_CNT 16
#define LABEL_FONT_SIZE 20

class CSystemsettingDlg : public CDialog
{
	DECLARE_DYNAMIC(CSystemsettingDlg)
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

public:
	CSystemsettingDlg(CCTSMonDoc* pDoc, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSystemsettingDlg();

	// 대화 상자 데이터입니다.
	enum { IDD = IDD_SYSTEM_SETTING_DLG };
public:
	CCTSMonDoc* m_pDoc;

public:
	void InitFont();
	void InitLabel();
	void SaveSetting();
	bool LoadSetting();
	
	CFont	font;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
	
public:
	BYTE m_nCapaErrlimit;
	BYTE m_nContactErrlimit;
	BYTE m_nChErrlimit;
	UINT m_nStageLogSavePeriod;
	UINT m_nFMSLogSavePeriod;
	UINT m_nDelayBlueReady;
	UINT m_nDelayBlueEnd;
	UINT m_nDelayRedEnd;
	UINT m_nResultFileSavePeriod;
	UINT m_nPrecisionDataSavePeriod;
	UINT m_nDelayRedTrayIn;
	UINT m_nFMSPortNum;
	UINT m_nMonPortNum;
	CIPAddressCtrl m_ctrlMonIp;
	BOOL m_bUseMon;
	BOOL m_bUsePrecisionData;

public:
	CLabel m_LabelSystemSettingName;
	CLabel m_Label_Name[MAX_LABEL_CNT];
	CLabel m_Label_IMSSocket_Name1;
	CLabel m_Label_IMSSocket_Name2;
	CLabel m_Label_IMSSocket_Name3;
	
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBtnPasswordSetting();
	afx_msg void OnBnClickedOk();	
	
	BOOL m_bUseFaultAlarm;
	UINT m_nErrorHistorySavePeriod;
	CLabel m_LabelMonitoringConnectState;
	afx_msg void OnBnClickedUseMon();	
	UINT m_nImsRecordConditionTime;
};

