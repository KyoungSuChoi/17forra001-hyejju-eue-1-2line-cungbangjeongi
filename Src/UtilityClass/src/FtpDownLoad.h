// FtpDownLoad.h: interface for the CFtpDownLoad class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FTPDOWNLOAD_H__E9C1FD96_7D5F_48C4_AFF6_9A2611105C7C__INCLUDED_)
#define AFX_FTPDOWNLOAD_H__E9C1FD96_7D5F_48C4_AFF6_9A2611105C7C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxinet.h>

#define FTP_REG_CONFIG	"Ftp Set"
#define INTERNET_CONNECTION_TIMEOUT		5000

class CFtpDownLoad  
{
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();
	
	CFtpDownLoad();
	virtual ~CFtpDownLoad();

	CString GetLastErrorMsg();
	BOOL SetCurrentDirecotry(CString strCurDir);
	CString GetCurrentDirecotry();
	CFtpConnection   *m_pFtpConnection;         //Ftp 명령 연결 컨트롤 클래스
	BOOL ConnectFtpSession(CString strIPAddress, CString strID = "root", CString strPWD = "dusrnth");
	int DownLoadFile(CString strLoacalFile, CString strRemoteFile, BOOL bOverWrite = TRUE);
	
protected:
	CString m_strLastErrorMsg;
	CInternetSession *m_pInternetSession;          //인터넷 세션 클래스
};

#endif // !defined(AFX_FTPDOWNLOAD_H__E9C1FD96_7D5F_48C4_AFF6_9A2611105C7C__INCLUDED_)
