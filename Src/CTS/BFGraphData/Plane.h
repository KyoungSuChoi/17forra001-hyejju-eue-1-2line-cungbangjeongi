/*************************************************************
	Copyrighted by LG Production engineering Research Center

	File Name: Plane.h

	Written out by 
	Evaluation Thechnology Group
	Kim, Sung-Hoon

	Comment: CPlane class is defined

	Revision History:
	Version   Name            Date        Reason
	    1.0   Kim, Sung-Hoon  2001.06.01  Create
*************************************************************/

// Plane.h: interface for the CPlane class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PLANE_H__40068520_EC8D_11D4_88DF_006008CEDA07__INCLUDED_)
#define AFX_PLANE_H__40068520_EC8D_11D4_88DF_006008CEDA07__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CLine;
#include "Axis.h"

#define _YAXIS_LABEL_WIDTH	13

// Class definition

//---------------------------------------------------------------------------
//	Class Name: CPlane
//
//	Comment:
//
//	Revision History:
//	Version   Name            Date        Reason
//	    1.0   Kim, Sung-Hoon  2001.06.01  Create
//---------------------------------------------------------------------------
class AFX_EXT_CLASS CPlane
{
////////////////
// Attributes //
////////////////
private:
	CTypedPtrList <CPtrList, CLine*> m_LineList;
	CAxis                            m_XAxis;
	CAxis                            m_YAxis;
	int                              m_iSerialNo;
	COLORREF                         m_BackColor;
//	BOOL                             m_bNewScale;
	int                              m_iNumOfLineColors;
	COLORREF*                        m_pLineColor;
	int                              m_iCurLineColorIndex;

/////////////////////////////////////////////
// Operations: "Constructor and destructor //
/////////////////////////////////////////////
public:
	COLORREF                         m_GridColor;
	CPlane();
	virtual ~CPlane();

////////////////////////////////////////////////////////
// Operations: "Functions to access member variables" //
////////////////////////////////////////////////////////
public:
	int		 GetLineCount()	{	return m_LineList.GetCount();	}
	int IsPointInPoint(CRect ClientRect, POINT sourcepoint, fltPoint &fltDataPoint);	
	BOOL GetCrossTwoPosition(fltPoint prepoint, fltPoint curpoint, fltPoint &crsPoint1, fltPoint &crsPoint2);
	CAxis*   GetXAxis() { return &m_XAxis; };
	CAxis*   GetYAxis() { return &m_YAxis; };
	void     SetSerialNo(int no) { m_iSerialNo=no; };
	POSITION GetLineHead() { return m_LineList.GetHeadPosition(); };
	CLine*   GetNextLine(POSITION& pos) { return m_LineList.GetNext(pos); };
	CLine*   GetLineAt(int iIndex) { return m_LineList.GetAt(m_LineList.FindIndex(iIndex)); }
	CLine*   FindLineOf(LPCTSTR ChPath, LONG lCycle, BYTE byMode);
	void     RemoveLineAt(POSITION pos);
	void     AddLine(CLine* pLine) {  m_LineList.AddTail(pLine); }
	void     DeleteAllLines();
	BOOL     IsThereThisLine(LPCTSTR strChPath, LONG lCycle, DWORD dwMode, LPCTSTR strAxisName);

//	int      DrawScopeInView(CDC* pDC, int LeftSpace, int TopSpace, int LabelSpace, CRect ClientRect, BOOL bScope=FALSE);
	int      DrawScopeInView(CDC* pDC, int LabelSpace, CRect ScopeRect,CRect ClientRect, BOOL bScope=FALSE);
//	void     DrawLineInView(CDC* pDC, int LeftSpace, int TopSpace, CRect ClientRect);
//	void     DrawLineInView(CDC* pDC, CRect ScopeRect);
	void     DrawLineInView(CDC* pDC, CRect ScopeRect, BOOL bStairType = FALSE);
	
	POINT    GetViewPosition(fltPoint point, RECT viewrect);
	fltPoint GetRealPosition(POINT point, RECT viewrect);
	BOOL     IsInMinMax(fltPoint point);
	fltPoint GetCrossPosition(fltPoint prepoint, fltPoint curpoint, BOOL IsFromInToOut);
	void     AutoScaleYAxis(int iNumOfPlanes);
	BOOL     GetMinMaxValue(fltPoint& MinValue, fltPoint& MaxValue);
	BOOL     SetColorOption(BOOL IsEven);
	COLORREF GetNextLineColor();
	void     DrawAColumnData(CDC* pDC, CList<float,float&>* pList, CRect ClientRect);
	void     ZoomIn(fltPoint min, fltPoint max);
	void     ZoomOut();
	BOOL     IsZoomInMode();
	void     ZoomOff();
	BOOL	 GetModeListOfLines(CList<BYTE, BYTE&>* pbylistMode, LPCTSTR strYAxisName);
	
//	int		 IsPointInLine(CDC* pDC, int LeftSpace, int TopSpace, CRect ClientRect, POINT point);
	int		 IsPointInLine(CRect ClientRect, POINT point);

protected:
	void	DrawScopeRect(CDC *pDC, CRect ScopeRect);
	void	DrawXAxis(CDC* pDC, CRect ScopeRect, BOOL bScope);
	int		DrawYAxis(CDC *pDC, int LabelSpace, CRect ScopeRect, BOOL bScope);
	float	GetAutoGridGap(float fMin, float fMax);
};

#endif // !defined(AFX_PLANE_H__40068520_EC8D_11D4_88DF_006008CEDA07__INCLUDED_)
