// CTSMonView.cpp : implementation of the CCTSMonView class
//

#include "stdafx.h"
#include "CTSMon.h"

#include "CTSMonDoc.h"
#include "CTSMonView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCTSMonView

IMPLEMENT_DYNCREATE(CCTSMonView, CView)

BEGIN_MESSAGE_MAP(CCTSMonView, CView)
	//{{AFX_MSG_MAP(CCTSMonView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCTSMonView construction/destruction

CCTSMonView::CCTSMonView()
{
	// TODO: add construction code here

}

CCTSMonView::~CCTSMonView()
{
}

BOOL CCTSMonView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CCTSMonView drawing

void CCTSMonView::OnDraw(CDC* /*pDC*/)
{
//	CCTSMonDoc* pDoc = GetDocument();
//	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CCTSMonView printing

BOOL CCTSMonView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CCTSMonView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CCTSMonView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CCTSMonView diagnostics

#ifdef _DEBUG
void CCTSMonView::AssertValid() const
{
	CView::AssertValid();
}

void CCTSMonView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CCTSMonDoc* CCTSMonView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCTSMonDoc)));
	return (CCTSMonDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCTSMonView message handlers
