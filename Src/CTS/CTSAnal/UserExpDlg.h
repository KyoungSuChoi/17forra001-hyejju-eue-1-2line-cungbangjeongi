#if !defined(AFX_USEREXPDLG_H__8E98B1FC_A926_47E1_A66A_022DF198BB62__INCLUDED_)
#define AFX_USEREXPDLG_H__8E98B1FC_A926_47E1_A66A_022DF198BB62__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UserExpDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CUserExpDlg dialog

class CUserExpDlg : public CDialog
{
// Construction
public:
	CString getNextOP(CString &strExp, long &dataType);
	BOOL FormulaCheck(CString strExpression);
	CUserExpDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CUserExpDlg)
	enum { IDD = IDD_USER_EXPRESSION_DIALOG };
	CString	m_strExpression;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUserExpDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CUserExpDlg)
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_USEREXPDLG_H__8E98B1FC_A926_47E1_A66A_022DF198BB62__INCLUDED_)
