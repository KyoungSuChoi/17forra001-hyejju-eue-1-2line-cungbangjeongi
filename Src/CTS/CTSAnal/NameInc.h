//#pragma warning (disable: 0xE06D7363)

#include <atlbase.h>

#define UM_LOGMESSAGE		      WM_USER+101          //LOG Message
#define UM_LOGINSTAT		      WM_USER+102          //HSMS login
#define UM_STATTEXT  		      WM_USER+103          //HSMS login

//bung define
#define	TYPE_DEV					1
#define TYPE_QC						2

#define PROTOCOL_V8							1008
#define PROTOCOL_V_NORMAL					1009

#include <Winnetwk.h>
#pragma comment(lib,"Mpr.lib")

#include "./Global/Mesg.h"
#include "./Global/GStr.h"
#include "./Global/GMath.h"
#include "./Global/GReg.h"
#include "./Global/GWin.h"
#include "./Global/GFile.h"
#include "./Global/GShell.h"
#include "./Global/GTime.h"
#include "./Global/GGdc.h"
#include "./Global/GList.h"
#include "./Global/GNet.h"

#include "./Global/GLog.h"
// #include "./Global/IniParse/IniParser.h"