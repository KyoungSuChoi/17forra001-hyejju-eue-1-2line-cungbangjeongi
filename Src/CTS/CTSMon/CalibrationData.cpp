// CalibrationData.cpp: implementation of the CCalibrationData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CalibrationData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCalibrationData::CCalibrationData()
{
	m_nState = CAL_DATA_EMPTY;
	LanguageinitMonConfig();
}

CCalibrationData::~CCalibrationData()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

bool CCalibrationData::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCalibrationData"), _T("TEXT_CCalibrationData_CNT"), _T("TEXT_CCalibrationData_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CCalibrationData_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCalibrationData"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

//Load Calibration Data from file
BOOL CCalibrationData::LoadDataFromFile(CString strFileName)
{
	if(strFileName.IsEmpty())
	{
		TRACE(TEXT_LANG[0]);//"교정 파일을 발견할 수 없습니다.\n"
		return FALSE;
	}
	
	FILE *fp = fopen(strFileName, "rb");
	if(fp == NULL)
	{
		TRACE(TEXT_LANG[0]);//"교정 파일을 발견할 수 없습니다.\n"
		return FALSE;
	}

	CAL_FILE_ID_HEADER fileHeader;
	if(fread(&fileHeader, sizeof(CAL_FILE_ID_HEADER), 1, fp) < 1)
	{
		fclose(fp);
		return FALSE;
	}
	//file Header 검사
	if(fileHeader.nFileID != CAL_FILE_ID)
	{
		fclose(fp);
		return FALSE;
	}

//	//Calibration 포인트 정보 Read
	if(m_PointData.ReadDataFromFile(fp) == FALSE)
	{
		fclose(fp);
		return FALSE;
	}

	for(int i=0; i<EP_MAX_CH_PER_MD; i++)
	{
		//Calibraton값 Load
		if(m_ChCaliData[i].ReadDataFromFile(fp) == FALSE)
		{
			fclose(fp);
			return FALSE;
		}
		m_ChCaliData[i].SetState(CAL_DATA_SAVED);
	}
	fclose(fp);

	m_nState = CAL_DATA_SAVED;
	m_strCalTime.Format("%s", fileHeader.szCreateDateTime);
	m_strUserName = fileHeader.szUserName;

	return TRUE;
}

BOOL CCalibrationData::SaveDataToFile(CString strFileName, CWordArray *pawSelMonCh)
{

	if(strFileName.IsEmpty())
	{
		TRACE(TEXT_LANG[1]);//"교정 파일이름을 발견할 수 없습니다.\n"
		return FALSE;
	}
	
	FILE *fp = fopen(strFileName, "wb");
	if(fp == NULL)
	{
		TRACE(TEXT_LANG[0]);//"교정 파일을 발견할 수 없습니다.\n"
		return FALSE;
	}

	CAL_FILE_ID_HEADER fileHeader;
	COleDateTime curTime = COleDateTime::GetCurrentTime();
	ZeroMemory(&fileHeader, sizeof(CAL_FILE_ID_HEADER));
	fileHeader.nFileID = CAL_FILE_ID;
	fileHeader.nFileVersion = 0x1000;
	sprintf(fileHeader.szCreateDateTime, "%s", curTime.Format());
	strcpy(fileHeader.szDescrition, "PNE Calibraion File");
	sprintf(fileHeader.szUserName, "%s", m_strUserName);

	if(fwrite(&fileHeader, sizeof(CAL_FILE_ID_HEADER), 1, fp) < 1)
	{
		fclose(fp);
		return FALSE;
	}

	//Calibration 포인트 정보 Read
	if(m_PointData.WriteDataToFile(fp) == FALSE)
	{
		fclose(fp);
		return FALSE;
	}
	
	//Calibraton값 Load
	for(int i=0; i<EP_MAX_CH_PER_MD; i++)
	{
	 	if(m_ChCaliData[i].WriteDataToFile(fp) == FALSE)
	 	{
	 		fclose(fp);
	 		return FALSE;
	 	}
		m_ChCaliData[i].SetState(CAL_DATA_SAVED);
	}
	fclose(fp);

	m_nState = CAL_DATA_SAVED;
	
	m_strCalTime = curTime.Format();

	return TRUE;
}


CString CCalibrationData::GetUpdateTime()
{
	return m_strCalTime;
}

int CCalibrationData::GetState()
{
	return m_nState;
}

void CCalibrationData::SetEdited()
{
	m_nState = CAL_DATA_MODIFY;
}

BOOL CCalibrationData::IsLoadedData()
{
	if(	m_nState == CAL_DATA_EMPTY)		return FALSE;

	return TRUE;
}

CChCaliData * CCalibrationData::GetChCalData(int nChIndex)
{
	if(nChIndex< 0 || nChIndex >= EP_MAX_CH_PER_MD)	return NULL;

	return &m_ChCaliData[nChIndex];
}


int CCalibrationData::GetHWChCount(int nBoardNo)
{
	int nCount = 0;
	int nBdNo, nChNo;
	for(int i=0; i<EP_MAX_CH_PER_MD; i++)
	{
		m_ChCaliData[i].GetHWChNo( nBdNo, nChNo);
		if(nBoardNo == nBdNo)
		{
			nCount++;
		}
	}
	return nCount;
}

CString CCalibrationData::GetUserName()
{
	return m_strUserName;
}

void CCalibrationData::SetUserName(CString strUser)
{
	m_strUserName = strUser;
}
