// OCStatisticDisplay.cpp: implementation of the OCStatisticDisplay class.
//
//////////////////////////////////////////////////////////////////////
#ifdef _OBJCHART_UT_DLL
#undef AFXAPI_DATA
#define AFXAPI_DATA __based(__segname("_DATA"))
#endif

#include "ocutstd.h"
#include <math.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

OCStatisticDisplay::OCStatisticDisplay()
{
	m_dBarWidth = 6;
	m_Style.SetBreakLinesOnNull(TRUE);
}

OCStatisticDisplay::~OCStatisticDisplay()
{

}


void OCStatisticDisplay::DrawScatterData()
{
	if(m_ZoomScaleY.Max()==0 && m_ZoomScaleY.Min()==0 && m_ZoomScaleX.Max()==0 && m_ZoomScaleX.Min()==0)
		return;

	CScale ZoomScaleX=m_ZoomScaleX;
	CScale ZoomScaleY=m_ZoomScaleY;

	DWORD dwGraphStyle=m_Style.GetGraphStyle();
	BOOL bExtended=FALSE;

	if(	dwGraphStyle == CX_GRAPH_XYSCATTERA_EX ||
		dwGraphStyle == CX_GRAPH_XYSCATTERI_EX ||
		dwGraphStyle == CX_GRAPH_XYSCATTERG_EX )
		bExtended=TRUE;
	
	double XScalingFactor,YScalingFactor;

	// MSS 5/7/98 - this section has been modified
	if(bExtended)
	{
		if(GetStyle()->GetLog())
		{
			if(ZoomScaleY.Min()==0)
				ZoomScaleY.m_dMin=0.001;
			YScalingFactor=((double)m_DisplayRect.Height())/(log10(ZoomScaleY.Max())-log10(ZoomScaleY.Min()));
		}
		else
			YScalingFactor=((double)m_DisplayRect.Height())/ZoomScaleY.Range();

		if(GetStyle()->GetLogX())
		{
			if(ZoomScaleX.Min()==0)
				ZoomScaleX.m_dMin=0.001;
			XScalingFactor=((double)m_DisplayRect.Width())/(log10(ZoomScaleX.Max())-log10(ZoomScaleX.Min()));
		}
		else
			XScalingFactor=((double)m_DisplayRect.Width())/ZoomScaleX.Range();
	}
	else
	{
		if(GetStyle()->GetLog())
		{
			if(ZoomScaleY.Min()==0)
				ZoomScaleY.m_dMin=0.001;
			if(ZoomScaleX.Min()==0)
				ZoomScaleX.m_dMin=0.001;
			XScalingFactor=((double)m_DisplayRect.Width())/(log10(ZoomScaleX.Max())-log10(ZoomScaleX.Min()));
			YScalingFactor=((double)m_DisplayRect.Height())/(log10(ZoomScaleY.Max())-log10(ZoomScaleY.Min()));
		}
		else
		{
			XScalingFactor=((double)m_DisplayRect.Width())/ZoomScaleX.Range();
			YScalingFactor=((double)m_DisplayRect.Height())/ZoomScaleY.Range();
		}
	}
	// MSS - end of changed section

	int CurrentSet=0,x=0;
	BOOL bFirstPoint=TRUE,bLog=GetStyle()->GetLog(),bLogX=bExtended ? GetStyle()->GetLogX() : GetStyle()->GetLog();

	CPoint lastpoint(0,0);

	BOOL bLogData=GetStyle()->GetLog();

	switch(GetStyle()->GetGraphStyle())
	{
	case CX_GRAPH_XYSCATTERG:
	case CX_GRAPH_XYSCATTERG_EX:
		for(CurrentSet=0;CurrentSet<m_nSetCount;CurrentSet+=2)
		{
			bFirstPoint=TRUE;
			m_nCurrentIndex=CurrentSet + m_nMinSet;
			int nDivCount=GetParent()->GetGroup(CurrentSet+m_nMinSet)->GetDataCount();
			nDivCount-=m_nMinDiv!=-1 ? m_nMinDiv : 0;
			nDivCount=nDivCount>m_nMaxDiv-m_nMinDiv? ((m_nMaxDiv-m_nMinDiv)+1) : nDivCount;

			for(x=0;x<nDivCount;x++)
			{
				SRGraphData *pDX=GetParent()->GetSafeData(x+m_nMinDiv,CurrentSet+m_nMinSet);
				SRGraphData *pDY=GetParent()->GetSafeData(x+m_nMinDiv,CurrentSet+m_nMinSet+1);
				BOOL bXnull=pDX->GetNull(),bYnull=pDY->GetNull();

				if(bXnull || bYnull )
					continue;
				if(!bXnull && !bYnull)
				{
					double dValueX=pDX->GetValue(bLogX);
					double dValueY=pDY->GetValue(bLog);

					if(bExtended)
					{
						if(bLog)
							dValueY-=log10(ZoomScaleY.Min());
						else
							dValueY-=ZoomScaleY.Min();

						if(bLogX)
							dValueX-=log10(ZoomScaleX.Min());
						else
							dValueX-=ZoomScaleX.Min();
					}
					else
					{
						if(bLogData)
						{
							dValueX-=log10(ZoomScaleX.Min());
							dValueY-=log10(ZoomScaleY.Min());
						}
						else
						{
							dValueX-=ZoomScaleX.Min();
							dValueY-=ZoomScaleY.Min();
						}
					} 

					CPoint datapoint=CPoint((int)(m_DisplayRect.left+(XScalingFactor*dValueX)),(int)(m_DisplayRect.bottom-(YScalingFactor*dValueY)));
					GetParent()->LogPoint(datapoint,CurrentSet+m_nMinSet,x+m_nMinDiv);

					if( CurrentSet >= 2 ) // Draw line
					{
						if( bFirstPoint )
							bFirstPoint = FALSE;
						else
							DrawStyledLine(lastpoint,datapoint); //, NULL, m_nCurrentIndex, CX_FRAME_LINE);
						lastpoint=datapoint;
					}
					else  // Draw bar
					{
						DrawBar(datapoint + CPoint(-(int)(m_dBarWidth * XScalingFactor /2), 0),
							CPoint(datapoint.x + (int)(m_dBarWidth * XScalingFactor /2) ,m_DisplayRect.bottom),
							CurrentSet+m_nMinSet,x+m_nMinDiv,pDX->GetStyle(), 0);
					}
				}
				else
				{
					if(m_Style.GetBreakLinesOnNull())
						bFirstPoint=TRUE;
				}
			}
		}
		break;
	}
}
