#if !defined(AFX_ACCURACYSETDLG_H__E03967E8_B0A7_4A42_BDB5_5E087577F6EB__INCLUDED_)
#define AFX_ACCURACYSETDLG_H__E03967E8_B0A7_4A42_BDB5_5E087577F6EB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AccuracySetDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CAccuracySetDlg dialog
#include "CaliPoint.h"


class CAccuracySetDlg : public CDialog
{
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();
	virtual ~CAccuracySetDlg();

	// Construction
public:
	CAccuracySetDlg(CCaliPoint* pCalPoint, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CAccuracySetDlg)
	enum { IDD = IDD_ACCURACY_DIALOG };
	CListBox	m_IRangeList;
	float	m_fIRange;
	float	m_fVRange;
	CListBox	m_VRangeList;
	float	m_fIRange1;
	float	m_fVRange1;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAccuracySetDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CCaliPoint* m_pCalPoint;

	// Generated message map functions
	//{{AFX_MSG(CAccuracySetDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeIRangeList();
	afx_msg void OnSelchangeVRangeList();
	afx_msg void OnChangeVRangeEdit();
	afx_msg void OnChangeVRangeEdit2();
	afx_msg void OnChangeIRangeEdit();
	afx_msg void OnChangeIRangeEdit2();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ACCURACYSETDLG_H__E03967E8_B0A7_4A42_BDB5_5E087577F6EB__INCLUDED_)
