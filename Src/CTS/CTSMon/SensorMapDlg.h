#if !defined(AFX_SENSORMAPDLG_H__731052CD_34F0_4E22_B5FE_8FB875E9773B__INCLUDED_)
#define AFX_SENSORMAPDLG_H__731052CD_34F0_4E22_B5FE_8FB875E9773B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SensorMapDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSensorMapDlg dialog

#include "MyGridWnd.h"
#include "CTSMonDoc.h"	// Added by ClassView

class CSensorMapDlg : public CDialog
{
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();
	virtual ~CSensorMapDlg();

	// Construction
public:
	void UpdateMappingCh(int nModuleID, int nSensor = 0);
	void DisplayMapping(int nModuleID);
	CCTSMonDoc *m_pDoc;
	BOOL ApplyToModule(int nModule);
//	CFormModule * m_pModuleInfo;
	long m_nModuleID;
	CSensorMapDlg(CCTSMonDoc *pDoc, CWnd* pParent = NULL);   // standard constructor
	CMyGridWnd m_wndMapGrid;

	enum {SENSOR1 = 0, SENSOR2};


// Dialog Data
	//{{AFX_DATA(CSensorMapDlg)
	enum { IDD = IDD_SENSOR_MAP_DIALOG };
	CXPButton	m_Ok_Btn;
	CXPButton	m_Cancel_Btn;
	CXPButton	m_btnApplyAll;
	CComboBox	m_ctrlModuleSel;
	CLabel	m_ctrlGreenLamp;
	CLabel	m_ctrlYellowLamp;
	CLabel	m_ctrlRedLamp;
	
	UINT	m_nWaringRed;
	UINT	m_nWaringYellow;
	BOOL	m_bUseTrayError;
	BOOL	m_bUseTempWarning;
	int		m_sWarnTemp;
	int		m_sDangerTemp;
	BOOL	m_bUseJigTempWarning;
	int		m_sWarnJigTemp;
	int		m_sDangerJigTemp;
	BOOL	m_bUseJigTemp;
	int		m_nJigTargetTemp;
	int		m_nJigTempInterval;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSensorMapDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void InitMappingGrid();

	// Generated message map functions
	//{{AFX_MSG(CSensorMapDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeMdSelCombo();
	virtual void OnOK();
	afx_msg void OnApplyAllButton();
	afx_msg void OnRadio1();
	afx_msg void OnRadio2();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SENSORMAPDLG_H__731052CD_34F0_4E22_B5FE_8FB875E9773B__INCLUDED_)
