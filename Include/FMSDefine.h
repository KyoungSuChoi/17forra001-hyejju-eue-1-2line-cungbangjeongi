#pragma once

#define MSGSTR '@'
#define MSGEND "*;"

#define LOCALPC_COMMAND 0x1000

#define MAX_SBC_COUNT		64// 64// 100 // 270 // 51
#define MAX_SBC_DCIR_COUNT	2
#define MAX_CELL_COUNT		40
// #define MAX_SET_COUNT 32

#define FMS_HEAD_LEN 50

CONST INT MAX_BUFFER_LENGTH = 32768; 
// CONST INT MAX_BUFFER_LENGTH = 229376;
CONST INT MAX_RESULT_BUFFER_LENGTH = 210000; // 203282
//////////////////////////////////////////////////////////////////////////
//ST MA
//장비 내부 타이머 정의
#define FMSSM_MAIN_TIMER	1000 //변경 되면 FM_SECOND변경 한다.
#define FMSSM_SECOND		FMSSM_MAIN_TIMER / 1000
#define FMSSM_MINITE		FMSSM_SECOND * 60
#define FMSSM_HOUR			FMSSM_MINITE * 60

#define ELEMENT_VALUE_MAX_LEN	32
#define FMS_ERROR_MSG_LEN		256

#define RETRY_IMPORT_ON		0x80000000//재시도 후 DOWN 상태로
#define RETRY_NOMAL_ON		0x40000000//재시도
#define RETRY_ALARM_ON		0x20000000//타이머 완료시 알람창
#define RETRY_CHECK			0x10000000//재시도중 매번 체크
#define RETRY_OFF			0x7FFFFFFF

typedef enum _FMS_STATE_CODE
{
	FMS_ST_OFF
	, FMS_ST_VACANCY
	, FMS_ST_TRAY_IN
	, FMS_ST_RUNNING
	, FMS_ST_END
	, FMS_ST_ERROR
	, FMS_ST_READY
	, FMS_ST_CONTACT_CHECK
	, FMS_ST_LOCAL_OPERATION_NOT_USE
	, FMS_ST_RED_READY
	, FMS_ST_RED_END
	, FMS_ST_BLUE_END
	, FMS_ST_RED_TRAY_IN
	, FMS_ST_CODE_END
}FMS_STATE_CODE;

static TCHAR g_str_FMS_State[FMS_ST_CODE_END][ELEMENT_VALUE_MAX_LEN] = 
{
	TEXT("FMS_ST_OFF")
	, TEXT("FMS_ST_VACANCY")
	, TEXT("FMS_ST_TRAY_IN ")
	, TEXT("FMS_ST_RUNNING")
	, TEXT("FMS_ST_END")
	, TEXT("FMS_ST_ERROR")
	, TEXT("FMS_ST_READY")
	, TEXT("FMS_ST_CONTACT_CHECK")
	, TEXT("FMS_ST_LOCAL_OPERATION_NOT_USE")
	, TEXT("FMS_ST_RED_READY")
	, TEXT("FMS_ST_RED_END")
	, TEXT("FMS_ST_BLUE_END")
	, TEXT("FMS_ST_RED_TRAY_IN")
};
typedef enum _FM_STATUS_ID
{
	EQUIP_ST_OFF

	, EQUIP_ST_AUTO
	,	AUTO_ST_VACANCY
	,	AUTO_ST_READY
	,	AUTO_ST_TRAY_IN
	,	AUTO_ST_CONTACT_CHECK
	,	AUTO_ST_RUN
	,	AUTO_ST_ERROR
	,	AUTO_ST_END

	, EQUIP_ST_LOCAL
	,	LOCAL_ST_LOCAL
	,	LOCAL_ST_ERROR

	, EQUIP_ST_MAINT
	,	MAINT_ST_MAINT
	,	MAINT_ST_ERROR

	, EQIP_ST_END
}FM_STATUS_ID;

static TCHAR g_str_State[EQIP_ST_END][ELEMENT_VALUE_MAX_LEN] = 
{
	TEXT("OFF")

	, TEXT("AUTO")
	, TEXT("VACANCY")
	, TEXT("READY")
	, TEXT("TRAY_IN")
	, TEXT("CONTACT_CHECK")
	, TEXT("RUNNING")
	, TEXT("ERROR(AUTO)")
	, TEXT("END")

	, TEXT("LOCAL")
	, TEXT("LO_LOCAL")
	, TEXT("ERROR(LOCAL)")

	, TEXT("MAINT")
	, TEXT("MA_LOCAL")
	, TEXT("ERROR(MAINT)")
};
typedef enum _FM_TRAY_STATUS_ID
{
	TRAY_ST_NONE
	, TRAY_ST_IN
	, TRAY_ST_INED
	, TRAY_ST_OUT
	, TRAY_ST_OUTED
	, TRAY_ST_END
}FM_TRAY_STATUS_ID;

static TCHAR g_str_TRAY_STATUS[TRAY_ST_END][ELEMENT_VALUE_MAX_LEN] = 
{
	TEXT("NONE")
	, TEXT("TRAY_IN")
	, TEXT("TRAY_INED")
	, TEXT("TRAY_OUT")
	, TEXT("TRAY_OUTED")
};

typedef enum _PROC_FN_ID
{
	FNID_ENTER
	, FNID_PROC
	, FNID_EXIT
}PROC_FN_ID;

typedef enum tag_FMS_ERROR
{
	FR_NONE
	,FR_END

}MES_ERROR;
static const TCHAR g_str_MES_ERROR[FR_END][256] = 
{
	TEXT("")
};

//////////////////////////////////////////////////////////////////////////
#define HEAET_BEAT_F										"0001"
#define TIME_SET											"0002"
#define ERROR_NOTICE										"0011"	//FMS->PC
#define EQ_ERROR_RESPONSE									"0012"	//PC->FMS
//공통 명령

#ifdef _DCIR
	#define CHARGER_IMPOSSBLE									"0301"
	#define CHARGER_INRESEVE									"0302"
	#define CHARGER_IN											"0303"
	#define CHARGER_WORKEND_RESPONSE							"0304"
	#define CHARGER_RESULT										"0305"
	#define CHARGER_RESULT_RECV									"0306"
	#define CHARGER_OUT											"0307"
	#define CHARGER_MODE_CHANGE									"0308"
	#define CHARGER_STATE_RESPONSE								"0309"

	#define CHARGER_IMPOSSBLE_RESPONSE							"1301"
	#define CHARGER_INRESEVE_RESPONSE							"1302"
	#define CHARGER_IN_RESPONSE									"1303"
	#define CHARGER_WORKEND										"1304"
	#define CHARGER_RESULT_RESPONSE								"1305"
	#define CHARGER_RESULT_RECV_RESPONSE						"1306"
	#define CHARGER_OUT_RESPONSE								"1307"
	#define CHARGER_MODE_CHANGE_RESPONSE						"1308"
	#define CHARGER_STATE										"1309"
#else
	#define CHARGER_IMPOSSBLE									"0101"
	#define CHARGER_INRESEVE									"0102"
	#define CHARGER_IN											"0103"
	#define CHARGER_WORKEND_RESPONSE							"0104"
	#define CHARGER_RESULT										"0105"
	#define CHARGER_RESULT_RECV									"0106"
	#define CHARGER_OUT											"0107"
	#define CHARGER_MODE_CHANGE									"0108"
	#define CHARGER_STATE_RESPONSE								"0109"

	#define CHARGER_IMPOSSBLE_RESPONSE							"1101"
	#define CHARGER_INRESEVE_RESPONSE							"1102"
	#define CHARGER_IN_RESPONSE									"1103"
	#define CHARGER_WORKEND										"1104"
	#define CHARGER_RESULT_RESPONSE								"1105"
	#define CHARGER_RESULT_RECV_RESPONSE						"1106"
	#define CHARGER_OUT_RESPONSE								"1107"
	#define CHARGER_MODE_CHANGE_RESPONSE						"1108"
	#define CHARGER_STATE										"1109"
#endif


#define OCV_TRAY_ID_RESPONSE								"0201"
#define OCV_TEST_FINISH_RESPONSE							"0202"
#define OCV_RESULT_FILE_NOTIFY								"0203"
#define OCV_RESULT_FILE_RECEIVE_CHECK_NOTIFY				"0204"
#define OCV_MEASUREMENT_NOTIFY								"0205"

#define OCV_MODE_CHANGE_NOTIFY								"0208"
#define OCV_EQUIPMENT_STATE_NOTIFY_RESPONSE					"0209"

#define HEAET_BEAT_P										"1001"
#define TIME_SET_RESPONSE									"1002"
#define ERROR_NOTICE_RESPONSE								"1011"
#define EQ_ERROR											"1012"
//공통 명령                                	

#define OCV_TRAY_ID_NOTIFY									"1201"
#define OCV_TEST_FINISH_NOTIFY								"1202"
#define OCV_RESULT_FILE_RESPONSE							"1203"
#define OCV_RESULT_FILE_RECEIVE_CHECK_RESPONSE				"1204"
#define OCV_MEASUREMENT_RESPONSE							"1205"

#define OCV_MODE_CHANGE_RESPONSE							"1208"
#define OCV_EQUIPMENT_STATE_NOTIFY							"1209"

typedef enum tag_FMS_COMMAND
{
	E_HEAET_BEAT_F											= 1
	, E_TIME_SET											= 2
	, E_ERROR_NOTICE										= 11	//FMS->PC
	, E_EQ_ERROR_RESPONSE									= 12	//PC->FMS

	//공통 명령
#ifdef _DCIR
	, E_CHARGER_IMPOSSBLE									= 301
	, E_CHARGER_INRESEVE									= 302
	, E_CHARGER_IN											= 303
	, E_CHARGER_WORKEND_RESPONSE							= 304
	, E_CHARGER_RESULT										= 305
	, E_CHARGER_RESULT_RECV									= 306
	, E_CHARGER_OUT											= 307
	, E_CHARGER_MODE_CHANGE									= 308
	, E_CHARGER_STATE_RESPONSE								= 309

#else
	, E_CHARGER_IMPOSSBLE									= 101
	, E_CHARGER_INRESEVE									= 102
	, E_CHARGER_IN											= 103
	, E_CHARGER_WORKEND_RESPONSE							= 104
	, E_CHARGER_RESULT										= 105
	, E_CHARGER_RESULT_RECV									= 106
	, E_CHARGER_OUT											= 107
	, E_CHARGER_MODE_CHANGE									= 108
	, E_CHARGER_STATE_RESPONSE								= 109
#endif

	, E_OCV_TRAY_ID_RESPONSE								= 201
	, E_OCV_TEST_FINISH_RESPONSE							= 202
	, E_OCV_RESULT_FILE_NOTIFY								= 203
	, E_OCV_RESULT_FILE_RECEIVE_CHECK_NOTIFY				= 204
	, E_OCV_MEASUREMENT_NOTIFY								= 205

	, E_OCV_MODE_CHANGE_NOTIFY								= 208
	, E_OCV_EQUIPMENT_STATE_NOTIFY_RESPONSE					= 209
}FMS_COMMAND;

typedef enum tag_FMS_OP_COMMAND
{
	E_HEAET_BEAT_P											= 1001
	, E_TIME_SET_RESPONSE									= 1002
	, E_ERROR_NOTICE_RESPONSE								= 1011
	, E_EQ_ERROR											= 1012
	//공통 명령

#ifdef _DCIR
	, E_CHARGER_IMPOSSBLE_RESPONSE							= 1301
	, E_CHARGER_INRESEVE_RESPONSE							= 1302
	, E_CHARGER_IN_RESPONSE									= 1303
	, E_CHARGER_WORKEND										= 1304
	, E_CHARGER_RESULT_RESPONSE								= 1305
	, E_CHARGER_RESULT_RECV_RESPONSE						= 1306
	, E_CHARGER_OUT_RESPONSE								= 1307
	, E_CHARGER_MODE_CHANGE_RESPONSE						= 1308
	, E_CHARGER_STATE										= 1309
#else
	, E_CHARGER_IMPOSSBLE_RESPONSE							= 1101
	, E_CHARGER_INRESEVE_RESPONSE							= 1102
	, E_CHARGER_IN_RESPONSE									= 1103
	, E_CHARGER_WORKEND										= 1104
	, E_CHARGER_RESULT_RESPONSE								= 1105
	, E_CHARGER_RESULT_RECV_RESPONSE						= 1106
	, E_CHARGER_OUT_RESPONSE								= 1107
	, E_CHARGER_MODE_CHANGE_RESPONSE						= 1108
	, E_CHARGER_STATE										= 1109
#endif
	, E_OCV_TRAY_ID_NOTIFY									= 1201
	, E_OCV_TEST_FINISH_NOTIFY								= 1202
	, E_OCV_RESULT_FILE_RESPONSE							= 1203
	, E_OCV_RESULT_FILE_RECEIVE_CHECK_RESPONSE				= 1204
	, E_OCV_MEASUREMENT_RESPONSE							= 1205
	, E_OCV_MODE_CHANGE_RESPONSE							= 1208
	, E_OCV_EQUIPMENT_STATE_NOTIFY							= 1209
}FMS_OP_COMMAND;

static INT g_iHead_Map[] = 
{
	1
	, 4
	, 8
	, 8
	, 4
	, 14
	, 4
	, 6
	, 1
	, 0
};

typedef struct _st_FMSMSGHEAD
{
	CHAR MsgSTR[1+1];
	CHAR Line[4+1];
	CHAR Sender[8+1];
	CHAR Addressee[8+1];
	CHAR Command[4+1];
	CHAR SendTime[14+1];
	CHAR ResultcCode[4+1];
	CHAR DataLength[6+1];
	CHAR HeadEnd[1+1];

}st_FMSMSGHEAD;

//////////////////////////////////////////////////////////////////////////

// Error 통지 // 0011
static INT g_iERROR_NOTICE_Map[] = 
{
	3
	, 2
	, 0
};

typedef struct _st_ERROR_NOTICE
{
	CHAR Equipment_Num[3+1];
	CHAR Error_Code[2+1];
}st_ERROR_NOTICE;

// 설비 Error 통지 응답 // 0012
static INT g_iEQ_ERROR_RESPONSE_Map[] = 
{
	3
	, 0
};

typedef struct _st_EQ_ERROR_RESPONSE
{
	CHAR Equipment_Num[3+1];
}st_EQ_ERROR_RESPONSE;
//////////////////////////////////////////////////////////////////////////

// 시간 설정 응답 // 1002
static INT g_iTIME_SET_RESPONSE_Map[] = 
{
	3
	, 0
};

typedef struct _st_TIME_SET_RESPONSE
{
	CHAR Send_Fail_Count[3+1];
}st_TIME_SET_RESPONSE;

// Error 통지 응답 // 1011
static INT g_iERROR_NOTICE_RESPONSE_Map[] = 
{
	3
	, 0
};

typedef struct _st_ERROR_NOTICE_RESPONSE
{
	CHAR Equipment_Num[3+1];
}st_ERROR_NOTICE_RESPONSE;

// 설비 Error 통지 // 1012
static INT g_iEQ_ERROR_Map[] = 
{
	3
	, 2
	, 0
};

typedef struct _st_EQ_ERROR
{
	CHAR Equipment_Num[3+1];
	CHAR Error_Code[2+1];
}st_EQ_ERROR;

//////////////////////////////////////////////////////////////////////////


typedef struct _st_FMS_PACKET
{
	st_FMSMSGHEAD head;
	CHAR data[4096];
	UINT dataLen;
	INT64 inreservCode;
}st_FMS_PACKET;
//////////////////////////////////////////////////////////////////////////

typedef enum _FMS_Step_Type
{
	FMS_SKIP
	, FMS_CHG
	, FMS_DCHG
	, FMS_REST
	, FMS_OCV
	, FMS_DCIR
	, FMS_CC_CHARGE
	, FMS_OG_CHARGE
	, FMS_CC_CHARGE_FANOFF
	, FMS_COMMON			// 공통적용
}FMS_Step_Type;

//////////////////////////////////////////////////////////////////////////
#define RESULT_CODE_OK						'0'

#define RESULT_CODE_DATA_ERROR				'1'
#define RESULT_CODE_STATUS_ERROR			'2'

#define RESULT_CODE_ALREADY_RESERVED		'2'

#define RESULT_CODE_INSPECTION				'3'

#define RESULT_CODE_STEP_SEND				'5'
#define RESULT_CODE_ERROR_STATE				'6'

#define RESULT_CODE_RUN_FAIL				'6'

#define RESULT_CODE_CONTACT_ERROR			'4'
#define RESULT_CODE_CAPA_ERROR				'5'
#define RESULT_CODE_OVER_CHARGE				'6'

#define RESULT_CODE_TRAY_ID_ERROR			'4'

#define RESULT_CODE_MIS_SEND				'2'
#define RESULT_CODE_FILE_OPEN_ERROR			'3'

#define RESULT_CODE_SETTING_DATA_ERROR		'3'
#define RESULT_CODE_STAGE_SETTING_ERROR		'4'

#define RESULT_CODE_TIME_SET_FAIL			'1'

#define RESULT_CODE_TRAY_IN_ERROR			'3'

#define RESULT_UNKNOW_ERROR					'9'

typedef enum tag_FMS_ERRORCODE
{
	FMS_ER_NONE
	, ER_Data_Error
	, ER_Status_Error
	
	, ER_Already_Reserve
	, ER_fnLoadWorkInfo
	, ER_fnTransFMSWorkInfoToCTSWorkInfo
	, ER_fnMakeTrayInfo
	, ER_fnSendConditionToModule
	, ER_FMS_Recv_Error_State
	
	, ER_Run_Fail
	, ER_SBC_NetworkError	//20210225 ksj

	, ER_File_Not_Found
	, ER_File_Open_error
	, ER_Contact_Error
	, ER_Capa_Error
	, ER_Over_Charge
	
	, ER_TrayID
	
	, ER_Settiing_Data_error
	, ER_Stage_Setting_Error	
	
	, ER_NO_Process

	, ER_Tray_In
	, ER_fnRecipeOverC_Error	//20200411 엄륭 용량 상한

	, ER_END
}FMS_ERRORCODE;

static TCHAR g_str_FMS_ERROR[ER_END][FMS_ERROR_MSG_LEN] = 
{
	TEXT("")
	, TEXT("ER_Data_Error")
	, TEXT("ER_Status_Error")
	//////////////////////////////////////////////////////////////////////////
	, TEXT("ER_Already_Reserve")
	, TEXT("ER_fnLoadWorkInfo")
	, TEXT("ER_fnTransFMSWorkInfoToCTSWorkInfo")
	, TEXT("ER_fnMakeTrayInfo")
	, TEXT("ER_fnSendConditionToModule")
	, TEXT("ER_FMS_Recv_Error_State")
	//////////////////////////////////////////////////////////////////////////
	, TEXT("ER_Run_Fail")
	, TEXT("ER_SBC_NetworkError")
	//////////////////////////////////////////////////////////////////////////
	, TEXT("ER_File_Not_Found")
	, TEXT("ER_File_Open_error")
	, TEXT("ER_Contact_Error")
	, TEXT("ER_Capa_Error")
	, TEXT("ER_Over_Charge")
	//////////////////////////////////////////////////////////////////////////
	, TEXT("ER_TrayID")
	//////////////////////////////////////////////////////////////////////////
	, TEXT("ER_Settiing_Data_error")
	, TEXT("ER_Stage_Setting_Error")
	//////////////////////////////////////////////////////////////////////////
	, TEXT("ER_NO_Process")
	//////////////////////////////////////////////////////////////////////////
	, TEXT("ER_Tray_In")
	, TEXT("ER_fnRecipeOverC_Error")	//20200907ksj
};
typedef enum tag_STEP_VALUE_RANGE
{
	Range_S_I
	, Range_S_V
	, Range_I
	, Range_V
	, Range_SOC
	, Range_S_Min_Time
	, Range_Min_Time
	, Range_S_Sec_Time
	, Range_Sec_Time
	, Range_Capa
}STEP_VALUE_RANGE;

// RESULTDATA_STATE
// 값의 순서가 바뀌면 DB 정보와 매칭이 안됨
// 변경시 주의 - 마지막에 추가할것
// [2013/9/5 cwm]
typedef enum tag_RESULTDATA_STATE
{
	RS_NONE
	, RS_RUN
	, RS_END
	, RS_REPORT
	, RS_ERROR
}RESULTDATA_STATE;

typedef struct _st_SEND_MIS_INFO
{
	UINT64 idxKey;
	CHAR szFMTFilePath[256];
}st_SEND_MIS_INFO;

typedef enum tag_RESPONS_RESERVE_STATE
{
	RRS_NONE
	, RRS_RECVSET
	, RRS_TIME_OUT
	, RRS_DEL

}RESPONS_RESERVE_STATE;

typedef struct _st_RESPONS_RESERVE
{
	FMS_COMMAND fmsCommand;
	UINT timer;
	RESPONS_RESERVE_STATE RRState;
	FMS_ERRORCODE fmsERCode;

}st_RESPONS_RESERVE;