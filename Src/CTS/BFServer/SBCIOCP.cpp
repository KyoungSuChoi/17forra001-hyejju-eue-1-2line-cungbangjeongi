#include "StdAfx.h"
#include "SBCGlobal.h"
#include "SBCCriticalSection.h"
#include "SBCSyncParent.h"
#include "SBCStaticSyncParent.h"
#include "SBCMemoryPool.h"
#include "SBCCircularQueue.h"
#include "SBCNetObj.h"
#include "SBCIocp.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define THREAD_STOP_TIMEOUT 5000

DWORD WINAPI WorkerThreadCallback(LPVOID lpParam)
{
	CSBCIocp *pOwner = (CSBCIocp*) lpParam;
	pOwner->WorkerThreadCallback();
	return 0;
}

CSBCIocp::CSBCIocp(VOID)
{
	m_hIocp = NULL;
	m_dwWorkerThreadCnt = 0;
	mWorkIdx = 0;
}

CSBCIocp::~CSBCIocp(VOID)
{
}

BOOL CSBCIocp::RegToIocp(HANDLE hHandle, ULONG_PTR ulpCompletionKey)
{
	if (!hHandle || !ulpCompletionKey)
		return FALSE;

	m_hIocp = CreateIoCompletionPort((HANDLE) hHandle, m_hIocp, ulpCompletionKey, 0);
	if (!m_hIocp)
	{
		printf("GetLastError : %d\n", GetLastError());
		return FALSE;
	}

	return TRUE;
}

BOOL CSBCIocp::Begin(DWORD dwWorkerThreadCnt)
{
	m_hIocp = NULL;

	if (dwWorkerThreadCnt == 0)
	{
		SYSTEM_INFO si;
		GetSystemInfo(&si);

		m_dwWorkerThreadCnt = si.dwNumberOfProcessors * 2;
	}
	else
		m_dwWorkerThreadCnt = dwWorkerThreadCnt;


	m_hIocp = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, 0);

	if (!m_hIocp)
		return FALSE;

	for (DWORD i=0;i<m_dwWorkerThreadCnt;i++)
	{
		HANDLE hWorkerThread = CreateThread(NULL, 0, ::WorkerThreadCallback, this, 0, NULL);
		m_vWorkerThreadHandle.push_back(hWorkerThread);
	}

	return TRUE;
}

VOID CSBCIocp::End(VOID)
{
	for (DWORD i=0;i<m_vWorkerThreadHandle.size();i++)
	{
		PostQueuedCompletionStatus(m_hIocp, 0, 0, NULL);
		WaitForSingleObject(m_vWorkerThreadHandle[i], THREAD_STOP_TIMEOUT);
		TerminateThread(m_vWorkerThreadHandle[i], 0);
		CloseHandle(m_vWorkerThreadHandle[i]);
	}

	if (m_hIocp)
		CloseHandle(m_hIocp);

	m_vWorkerThreadHandle.clear();
}

VOID CSBCIocp::WorkerThreadCallback(VOID)
{
	INT idx = mWorkIdx++;
	UINT useCnt = 0;
	BOOL bSucc = FALSE;
	DWORD dwNumOfByteTransfered = 0;
	ULONG_PTR pCompletionKey = NULL;
	OVERLAPPED *pol = NULL;

	while (TRUE)
	{
		bSucc = GetQueuedCompletionStatus(
			m_hIocp,
			&dwNumOfByteTransfered,
			&pCompletionKey,
			&pol,
			INFINITE);

		if (!pCompletionKey)
			return;

		OnIo(bSucc, dwNumOfByteTransfered, pCompletionKey, pol);

		//TRACE("Work Tread IDX %d Use[%d]\n", idx, useCnt++);
	}
}