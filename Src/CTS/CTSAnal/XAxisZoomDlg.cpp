// XAxisZoomDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "XAxisZoomDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CXAxisZoomDlg dialog


CXAxisZoomDlg::CXAxisZoomDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CXAxisZoomDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CXAxisZoomDlg)
	m_nHscrollPonitNo = 0;
	//}}AFX_DATA_INIT
}


void CXAxisZoomDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CXAxisZoomDlg)
	DDX_Text(pDX, IDC_HSCROLL_PONIT, m_nHscrollPonitNo);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CXAxisZoomDlg, CDialog)
	//{{AFX_MSG_MAP(CXAxisZoomDlg)
	ON_BN_CLICKED(IDOK, OnOk)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CXAxisZoomDlg message handlers

void CXAxisZoomDlg::OnOk() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	CDialog::OnOK();
}

BOOL CXAxisZoomDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	GetDlgItem(IDC_HSCROLL_PONIT)->SetFocus();
	// TODO: Add extra initialization here
	
	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
