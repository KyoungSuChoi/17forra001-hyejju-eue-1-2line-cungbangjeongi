// ExcelTransDlg.h : header file
//

#if !defined(AFX_ExcelTransDLG_H__629B35F0_C6F7_46A7_A451_B67D256B7B47__INCLUDED_)
#define AFX_ExcelTransDLG_H__629B35F0_C6F7_46A7_A451_B67D256B7B47__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CExcelTransDlg dialog

class CExcelTransDlg : public CDialog
{
// Construction
public:
	CString *TEXT_LANG;	
	BOOL LanguageinitMonConfig();

	BOOL ConvertData();
	void UpdateFileList();
	int GetColumnCount(int nStepType);
	BOOL LoadCodeTable();
	CString GetStateMsg(WORD State);
	int m_nColCount;
	CString ValueString(double dData, int item, BOOL bUnit = FALSE);
	CString GetSaveFileName(CString strFileName);
	void SetFileName(const CStringArray &aSelFileName);
	BOOL WriteFileHeader(FILE *fp, CFormResultFile *pResultFile);
	BOOL SaveCellEfficiencyFile(CString strFile);
	BOOL SaveTrayFormFile(CString strFile);
	BOOL SaveChCellListFile(CString strFile);
	BOOL SaveStepChListFile(CString strFile);
	BOOL SaveChStepFile(CString strFile);
	int m_nConvertType;

	CExcelTransDlg(CWnd* pParent = NULL);	// standard constructor
	virtual ~CExcelTransDlg();

// Dialog Data
	//{{AFX_DATA(CExcelTransDlg)
	enum { IDD = IDD_ExcelTrans_DIALOG };
	CString	m_strSelFileName;
	BOOL	m_bFileAppend;
	BOOL	m_bExcelOpenFlag;
	CString	m_strSaveFileName;
	CString	m_strSaveFolderName;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CExcelTransDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	int m_nTimeUnit;
	CString m_strVUnit;
	int m_nVDecimal ;
	CString m_strIUnit;
	int m_nIDecimal;
	CString m_strCUnit;
	int m_nCDecimal;

	BOOL m_bSumCap;
	BOOL	m_bType;
	BOOL	m_bCapEff;
	BOOL	m_bState;
	BOOL	m_bTime;
	BOOL	m_bVoltage;
	BOOL	m_bCurrent;
	BOOL	m_bCapacity;
	BOOL	m_bImpedance;
	BOOL	m_bWatt;
	BOOL	m_bWattHour;
	BOOL	m_bGradeCode;
	BOOL	m_bCellCode;

	BOOL	m_bTimeGetChargeVoltage;
	BOOL	m_bComCapacity;
	BOOL	m_bCCTime;
	BOOL	m_bComDcir;
	BOOL	m_bDcir;
	BOOL	m_bV1;
	BOOL	m_bV2;
	BOOL	m_bAvgJigTemp;
	BOOL	m_bAvgCurrent;

	
	char m_aChMsg[256][EP_MAX_CH_CODE_LENGTH+1];
	CStringArray m_aSelFileName;
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CExcelTransDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnFileSelButton();
	afx_msg void OnSelSaveFile();
	afx_msg void OnChannelListRadio();
	afx_msg void OnStepListRadio();
	afx_msg void OnChannelProcessRadio();
	afx_msg void OnTrayFormRadio();
	afx_msg void OnCycleRadio();
	afx_msg void OnConvertRun();
	afx_msg void OnSaveItemButton();
	afx_msg void OnFileAddCheck();
	afx_msg void OnSaveFolderButton();
	afx_msg void OnSaveUnitButton();
	virtual void OnOK();
	//}}AFX_MSG
	afx_msg LRESULT OnAutoFileTrans(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnDestroy();	
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ExcelTransDLG_H__629B35F0_C6F7_46A7_A451_B67D256B7B47__INCLUDED_)
