#pragma once

class CMySql_DB
{
public:
	CMySql_DB(void);
	~CMySql_DB(void);
	
	
public:
	bool Fun_Connect();
	void Fun_DisConnect();
	bool IsMySqlLive();	

	void SetSendFlag( bool bFlag ) { m_bSendFlag = bFlag; }
	bool GetSendFlag() { return m_bSendFlag; }

	void fnSetConnState( bool bFlag ){ m_bConnectSql = bFlag; }
	bool fnGetConnState(){ return m_bConnectSql; }
	
	int	Fun_ExecuteQuery( CString strQuery );
	
private:
	bool m_bConnectSql;
	bool m_bSendFlag;
	
	MYSQL*	m_connection, m_dberrconn;
	MYSQL_RES *sql_result;
	MYSQL_ROW sql_row;
};