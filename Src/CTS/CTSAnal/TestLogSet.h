#if !defined(AFX_TESTLOGSET_H__8A12D9B4_3B03_4911_A636_D174C65EC052__INCLUDED_)
#define AFX_TESTLOGSET_H__8A12D9B4_3B03_4911_A636_D174C65EC052__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TestLogSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTestLogSet recordset


class CTestLogSet : public CRecordset
{
public:
	CTestLogSet(CDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CTestLogSet)

// Field/Param Data
	//{{AFX_FIELD(CTestLogSet, CRecordset)
	long	m_TestLogID;
	CString	m_TestSerialNo;
	long	m_TraySerialNo;
	CString	m_LotNo;
	long	m_CellNo;
	CString	m_TrayNo;
	CTime	m_DateTime;
	long	m_ModelID;
	CString m_ModelName;
	BOOL	m_TestDone;
	//}}AFX_FIELD


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestLogSet)
	public:
	virtual CString GetDefaultConnect();    // Default connection string
	virtual CString GetDefaultSQL();    // Default SQL for Recordset
	virtual void DoFieldExchange(CFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTLOGSET_H__8A12D9B4_3B03_4911_A636_D174C65EC052__INCLUDED_)
