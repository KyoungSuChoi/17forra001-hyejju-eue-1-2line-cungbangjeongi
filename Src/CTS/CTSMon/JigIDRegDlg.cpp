// JigIDRegDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSMon.h"
#include "JigIDRegDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CJigIDRegDlg dialog


CJigIDRegDlg::CJigIDRegDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CJigIDRegDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CJigIDRegDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	LanguageinitMonConfig();
}

CJigIDRegDlg::~CJigIDRegDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

bool CJigIDRegDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CJigIDRegDlg"), _T("TEXT_CJigIDRegDlg_CNT"), _T("TEXT_CJigIDRegDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CJigIDRegDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CJigIDRegDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}



void CJigIDRegDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CJigIDRegDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CJigIDRegDlg, CDialog)
	//{{AFX_MSG_MAP(CJigIDRegDlg)
	//}}AFX_MSG_MAP
	ON_MESSAGE(EPWM_BCR_SCANED, OnBcrscaned)

END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CJigIDRegDlg message handlers

BOOL CJigIDRegDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_RegGrid.SubclassDlgItem(IDC_JIG_REG_GRID, this);
	
	m_RegGrid.Initialize();
	m_RegGrid.EnableCellTips();
	m_RegGrid.EnableIntelliMouse();


	BOOL bLock= m_RegGrid.LockUpdate();

	m_RegGrid.SetDefaultRowHeight(18);
	m_RegGrid.SetRowCount(100);
	m_RegGrid.SetColCount(3);
	m_RegGrid.SetScrollBarMode(SB_VERT, gxnEnabled | gxnEnhanced);	//Only Vertial Scroll Bar

	m_RegGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);

	m_RegGrid.SetValueRange(CGXRange(0,1),"Tag ID");
	m_RegGrid.SetValueRange(CGXRange(0,2), TEXT_LANG[0]);//"Module번호"
	m_RegGrid.SetValueRange(CGXRange(0,3), TEXT_LANG[1]);//"지그번호"

	m_RegGrid.SetCurrentCell(1, 1);

//	m_RegGrid.SetStyleRange(CGXRange().SetCols(1),
//			CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(255,255,255)).SetInterior(RGB(192,192,192)));

	m_RegGrid.GetParam()->SetActivateCellFlags(GX_CAFOCUS_DBLCLICKONCELL);

	CString str;
	m_RegGrid.SetStyleRange(CGXRange().SetCols(1),
		CGXStyle()
			.SetControl(GX_IDS_CTRL_EDIT)
			.SetHorizontalAlignment(DT_LEFT)
			.SetMaxLength(EP_TRAY_NAME_LENGTH)				//TrayNo Lenghth
	);

	int nInstall = EPGetInstalledModuleNum();
	for(int n = 0; n<nInstall; n++)
	{
		str = str + GetModuleName(EPGetModuleID(n)) + "\n"; 
		m_aModuleList.Add(EPGetModuleID(n));

	}
	m_RegGrid.SetStyleRange(CGXRange().SetCols(2),
		CGXStyle()
			.SetControl(GX_IDS_CTRL_ZEROBASED_EX)
			.SetHorizontalAlignment(DT_LEFT)
			.SetChoiceList(str)
	);

	//???????????????????????
	str = "1\n2\n3\n4\n5\n6\n7\n8";
	m_RegGrid.SetStyleRange(CGXRange().SetCols(3),
		CGXStyle()
			.SetControl(GX_IDS_CTRL_CBS_DROPDOWNLIST)
			.SetHorizontalAlignment(DT_LEFT)
			.SetChoiceList(str)
	);


	m_RegGrid.LockUpdate(bLock);
	m_RegGrid.Redraw();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CJigIDRegDlg::OnOK() 
{
	// TODO: Add extra validation here
	int nTotRow = m_RegGrid.GetRowCount();

	int nJigNo, nModuleID;
	CString strData, strID;

	int nCount = 0;
	for(int a = 0; a<nTotRow; a++)
	{
		strData = m_RegGrid.GetValueRowCol(a+1, 1);		//Get Tag ID	
		if(strData.IsEmpty())	continue;
		strID = strData;
		
		strData = m_RegGrid.GetValueRowCol(a+1, 2);		//Get Unit ID	
		if(strData.IsEmpty())	continue;
		
		nModuleID = atol(strData);
		if(nModuleID < 0 && nModuleID >= m_aModuleList.GetSize())
		{
			continue;
		}
		nModuleID = m_aModuleList.GetAt(nModuleID);
		
		strData = m_RegGrid.GetValueRowCol(a+1, 3);		//Get Jig No	
		if(strData.IsEmpty())	continue;

		nJigNo = atol(strData);

		if(SaveJigInfoToDB(strID, nModuleID, nJigNo))
		{
			nCount++;
		}
	}

	if(nCount > 0)
	{
		
//		AfxMessageBox();
	}

	CDialog::OnOK();
}

//Save new jig mapping to database
BOOL CJigIDRegDlg::SaveJigInfoToDB(CString strID, int nModuleID, int nJigNo)
{
	CDaoDatabase  db;

	try
	{
		db.Open(::GetDataBaseName());
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	
	CString strSQL;
	strSQL.Format("SELECT ID FROM JigID WHERE ID = '%s'", strID);

	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return FALSE;
	}	
	
	if(!rs.IsBOF() && !rs.IsEOF())
	{
		//Update 
		strSQL.Format("UPDATE JigID SET ModuleID = %d, JigNo = %d WHERE ID = '%s'", nModuleID, nJigNo, strID);
	}
	else
	{
		//Insert
		strSQL.Format("INSERT INTO JigID VALUES ( '%s', %d, %d)", strID, nModuleID, nJigNo);
	}
	db.Execute(strSQL);
	rs.Close();
	db.Close();

	return TRUE;
}

LRESULT CJigIDRegDlg::OnBcrscaned(UINT wParam, LONG lParam)
{
	char *data = (char *)lParam;
	CString str(data);
	
	if(str.IsEmpty())	
		return 0;

	//Tray 위치 정보를 검색 
	if(wParam != TAG_TYPE_NONE && wParam != TAG_TYPE_JIG)
	{
		AfxMessageBox(str + TEXT_LANG[2], MB_ICONSTOP|MB_OK);//"는 다른곳에서 이미 사용중이므로 Jig 정보 ID로 사용할 수 없습니다."
		return 0;
	}

	ROWCOL nRow, nCol;
	if(m_RegGrid.GetCurrentCell(nRow, nCol) == FALSE)	
	{
		return 0;
	}
		
	//set current tag id
	m_RegGrid.SetValueRange(CGXRange(nRow, 1), str);

	//Move next row
	if(nRow < m_RegGrid.GetRowCount())
	{
		m_RegGrid.SetCurrentCell(nRow+1, 1);
	}

	return 1;
}

BOOL CJigIDRegDlg::DeleteJigIDInfo(CString strJigID)
{
	if(strJigID.IsEmpty())	return FALSE;
	
	CDaoDatabase  db;

	try
	{
		CString strSQL;
		strSQL.Format("DELETE FROM JigID WHERE ID = '%s'", strJigID);
		db.Open(::GetDataBaseName());
		db.Execute(strSQL);
		db.Close();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	return TRUE;

}

BOOL CJigIDRegDlg::SearchLocation(CString strID, int &nModuleID, int &nJigID)
{
	CDaoDatabase  db;
	nModuleID = 0;
	nJigID = 0;

	try
	{
		db.Open(::GetDataBaseName());
	}
	catch (CDaoException* e)
	{
//		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	
	CString strSQL, strTemp;
	strSQL.Format("SELECT ModuleID, JigNo FROM JigID WHERE ID = '%s'", strID);
//	strSQL.Format("SELECT ModuleID, No INTO FROM JigID WHERE ID = '%s'", strID);

	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
		if(!rs.IsBOF() && !rs.IsEOF())
		{
			COleVariant data;
			data = rs.GetFieldValue(0);
			nModuleID = data.lVal;
			data = rs.GetFieldValue(1);
			nJigID = data.lVal;
		}
		rs.Close();
		db.Close();
		
		if(nModuleID < 1 || nJigID < 1)
		{
			return FALSE;
			TRACE("Find error\n");
		}
	}
	catch (CDaoException* e)
	{
//		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return FALSE;
	}	
	

	return TRUE;
}

CString CJigIDRegDlg::SearchLocation(int nModuleID, int nJigID)
{
	CDaoDatabase  db;
	CString strSQL, strTemp;
	try
	{
		db.Open(::GetDataBaseName());
	}
	catch (CDaoException* e)
	{
//		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return strTemp;
	}	
	
	strSQL.Format("SELECT ID FROM JigID WHERE ModuleID = %d AND JigNo = %d", nModuleID, nJigID);
//	strSQL.Format("SELECT ModuleID, No INTO FROM JigID WHERE ID = '%s'", strID);

	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
		if(!rs.IsBOF() && !rs.IsEOF())
		{
			COleVariant data;
			data = rs.GetFieldValue(0);
			strTemp = data.pcVal;
		}
		rs.Close();
		db.Close();
	}
	catch (CDaoException* e)
	{
//		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
	}	
	
	return strTemp;
}
