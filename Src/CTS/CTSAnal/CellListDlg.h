#if !defined(AFX_CELLLISTDLG_H__80E0C13B_7CC1_4AA6_83DF_0DFBB75D6FE5__INCLUDED_)
#define AFX_CELLLISTDLG_H__80E0C13B_7CC1_4AA6_83DF_0DFBB75D6FE5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CellListDlg.h : header file
//

#include "MyGridWnd.h"
#include "CTSAnalDoc.h"

/////////////////////////////////////////////////////////////////////////////
// CCellListDlg dialog

#define GRID_ROW_OFFSET	7

class CCellListDlg : public CDialog
{
// Construction
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	BOOL UserUpdateCode(BYTE code);
	CString m_strTestSerialNo;
	int m_nCellNo;
	CString m_strModelName;
	int m_nFailCount;
	int m_nNormalCount;
	CString m_strLotNo;
	CString m_strTrayNo;
	void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	void SetGridType();
	int m_nTotalCh;
	CString m_strTestLogID;
	BOOL RequeryData(CString strTestSerialNo);
	int m_nFieldNo;
	CCellListDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CCellListDlg();
	void InitListGrid();
	FIELD		m_Field[FIELDNO];
	CDatabase *m_pDBServer;
	CCTSAnalDoc *m_pDoc;
	INT	m_nMode;
	
	stingray::foundation::SECBitmapButton m_btnSave;
	stingray::foundation::SECBitmapButton m_btnPrint;
	stingray::foundation::SECBitmapButton m_btnClose;

// Dialog Data
	//{{AFX_DATA(CCellListDlg)
	enum { IDD = IDD_CELL_LIST_DLG };
	CLabel	m_wndModel;
	CLabel	m_wndTray;
	CLabel	m_wndLot;
	CComboBox	m_ctrlDataCombo;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCellListDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CMyGridWnd	m_wndListGrid;

	// Generated message map functions
	//{{AFX_MSG(CCellListDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnCellListRadio();
	afx_msg void OnResultData();
	afx_msg void OnSelchangeDataSelectCombo();
	afx_msg void OnExcelSave();
	afx_msg void OnPrintData();
	afx_msg void OnProcedureHistory();
	afx_msg void OnTempGas();
	afx_msg void OnUserNonCellCode();
	afx_msg void OnUserFailCell();
	afx_msg void OnUserNormalCell();
	//}}AFX_MSG
	afx_msg	LRESULT OnRButtonClickedRowCol(WPARAM wParam, LPARAM lParam);

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CELLLISTDLG_H__80E0C13B_7CC1_4AA6_83DF_0DFBB75D6FE5__INCLUDED_)
