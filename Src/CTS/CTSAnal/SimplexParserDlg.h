#if !defined(AFX_SIMPLEXPARSERDLG_H__98EEC303_BFC2_11D1_B0F3_08002BBFDDC1__INCLUDED_)
#define AFX_SIMPLEXPARSERDLG_H__98EEC303_BFC2_11D1_B0F3_08002BBFDDC1__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// SimplexParserDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSimplexParserDlg dialog

class CSimplexParserDlg : public CDialog
{
// Construction
public:
	CSimplexParserDlg(CWnd* pParent = NULL);   // standard constructor
	CPtrArray *m_apProcType;

// Dialog Data
	//{{AFX_DATA(CSimplexParserDlg)
	enum { IDD = IDD_PARSER };
	CComboBox	m_Field10;
	CComboBox	m_Field9;
	CComboBox	m_Field8;
	CComboBox	m_Field7;
	CComboBox	m_Field6;
	CComboBox	m_Field5;
	CComboBox	m_Field4;
	CComboBox	m_Field3;
	CComboBox	m_Field2;
	CComboBox	m_Field1;
	CString	m_strFormulainput;
	CString	m_strFormulaoutput;
	double	m_dKonstB;
	double	m_dKonstF;
	double	m_dKonstG;
	double	m_dKonstH;
	double	m_dKonstJ;
	double	m_dKonstK;
	double	m_dKonstM;
	double	m_dKonstN;
	double	m_dKonstU;
	double	m_dKonstX;
	//}}AFX_DATA

	CString	m_strFormulainput_vorher;
	CString m_strFileName;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSimplexParserDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void SetEingabe(CString str);
	void SaveParameter();
	void LoadParameter();

	// Generated message map functions
	//{{AFX_MSG(CSimplexParserDlg)
	afx_msg void OnEnter();
	afx_msg void OnSin();
	afx_msg void OnPlus();
	afx_msg void OnMinus();
	afx_msg void OnMal();
	afx_msg void OnGeteilt();
	afx_msg void OnHoch();
	afx_msg void OnPkt();
	afx_msg void OnKlammerAuf();
	afx_msg void OnKlammerZu();
	afx_msg void OnArcsin();
	afx_msg void OnSinh();
	afx_msg void OnArsinh();
	afx_msg void OnCos();
	afx_msg void OnArccos();
	afx_msg void OnCosh();
	afx_msg void OnArcosh();
	afx_msg void OnTan();
	afx_msg void OnArctan();
	afx_msg void OnTanh();
	afx_msg void OnArtanh();
	afx_msg void OnExp();
	afx_msg void OnLn();
	afx_msg void OnInt();
	afx_msg void OnAbs();
	afx_msg void On10x();
	afx_msg void OnLog();
	afx_msg void OnRad();
	afx_msg void OnDeg();
	afx_msg void OnX2();
	afx_msg void OnSqr();
	afx_msg void OnX();
	afx_msg void OnDelete();
	afx_msg void OnUndo();
	afx_msg void OnB();
	afx_msg void OnF();
	afx_msg void OnG();
	afx_msg void OnH();
	afx_msg void OnJ();
	afx_msg void OnK();
	afx_msg void OnM();
	afx_msg void OnN();
	afx_msg void OnU();
	afx_msg void OnXb();
	afx_msg void OnSaveParameter();
	afx_msg void OnLoadParameter();
	afx_msg void OnClickedNumber(UINT nID);
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SIMPLEXPARSERDLG_H__98EEC303_BFC2_11D1_B0F3_08002BBFDDC1__INCLUDED_)
