#include "afxwin.h"
#if !defined(AFX_SHOWEMGDLG_H__3D6E2BEC_4E4D_4C0C_93A4_233F6FC3D1D6__INCLUDED_)
#define AFX_SHOWEMGDLG_H__3D6E2BEC_4E4D_4C0C_93A4_233F6FC3D1D6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ShowEmgDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// ShowEmgDlg dialog

class ShowEmgDlg : public CDialog
{
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();
	virtual ~ShowEmgDlg();

// Construction
public:
	CString m_strEmgMsg;
	CString strEmgValue;
	CString strMsg;
	int		m_nModuleID;
	void SetEmgData(CString strModuleName, CString strCodeMsg, long lValue );
	ShowEmgDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(ShowEmgDlg)
	enum { IDD = IDD_SHOW_EMG_DLG };	
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA
	
public:
	void DrawEmgInfo();
	void SetModuleID(int nModuleID);
	int	 GetModuleID();


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ShowEmgDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ShowEmgDlg)
	virtual BOOL OnInitDialog();		
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	CLabel m_ctrlEmgMsg;
	CLabel m_ctrlEmgLabel;	
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SHOWEMGDLG_H__3D6E2BEC_4E4D_4C0C_93A4_233F6FC3D1D6__INCLUDED_)
