#if !defined(AFX_GRADELISTRECORDSET_H__E4804141_B13B_4F51_AC9C_8CA80A204BF4__INCLUDED_)
#define AFX_GRADELISTRECORDSET_H__E4804141_B13B_4F51_AC9C_8CA80A204BF4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GradeRecordSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CGradeRecordSet recordset

class CGradeRecordSet : public CRecordset
{
public:
	CGradeRecordSet(CDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CGradeRecordSet)

// Field/Param Data
	//{{AFX_FIELD(CGradeRecordSet, CRecordset)
	long	m_GradeID;
	long	m_StepID;
	long	m_GradeItem;
	float	m_Value;
	float	m_Value1;
	CString	m_GradeCode;
	//}}AFX_FIELD


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGradeRecordSet)
	public:
	virtual CString GetDefaultConnect();    // Default connection string
	virtual CString GetDefaultSQL();    // Default SQL for Recordset
	virtual void DoFieldExchange(CFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRADELISTRECORDSET_H__E4804141_B13B_4F51_AC9C_8CA80A204BF4__INCLUDED_)
