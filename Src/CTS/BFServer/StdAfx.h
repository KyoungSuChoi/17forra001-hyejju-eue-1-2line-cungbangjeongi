// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__7A1C0696_EF97_47DD_9A8B_06B403A1F905__INCLUDED_)
#define AFX_STDAFX_H__7A1C0696_EF97_47DD_9A8B_06B403A1F905__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxole.h>         // MFC OLE classes
#include <afxodlgs.h>       // MFC OLE dialog classes
#include <afxdisp.h>        // MFC Automation classes
#endif // _AFX_NO_OLE_SUPPORT


#ifndef _AFX_NO_DB_SUPPORT
#include <afxdb.h>			// MFC ODBC database classes
#endif // _AFX_NO_DB_SUPPORT

#ifndef _AFX_NO_DAO_SUPPORT
#include <afxdao.h>			// MFC DAO database classes
#endif // _AFX_NO_DAO_SUPPORT

#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <winsock2.h>

#ifndef EXPORT_EPDLL_API
#define EXPORT_EPDLL_API
#endif

#include "../../../Include/epdlldef.h"
#ifdef _DEBUG
#pragma comment(lib, "../../../Lib/BFCommonD.lib")
#pragma message("Automatically linking with BFCommonD.lib By K.B.H ")
#else
#pragma comment(lib, "../../../Lib/BFCommon.lib")
#pragma message("Automatically linking with BFCommon.lib By K.B.H")
#endif	//_DEBUG

#define	 INSTANCE_NAME	"PNE battery charge/discharge system"


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__7A1C0696_EF97_47DD_9A8B_06B403A1F905__INCLUDED_)

#include "SBCGlobal.h"

#include <crtdbg.h>

#ifndef _MEMORYLEAK_H_

#endif