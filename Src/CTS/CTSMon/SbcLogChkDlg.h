#pragma once


// CSbcLogChkDlg 대화 상자입니다.
#include "stdafx.h"
#include "MyGridWnd.h"
#include "CTSMonDoc.h"
#include "afxwin.h"
#include "afxcmn.h"
#include "afxdtctl.h"

#define GRID_DATA_FONT_SIZE	15

class CSbcLogChkDlg : public CDialog
{
	DECLARE_DYNAMIC(CSbcLogChkDlg)

public:
	CSbcLogChkDlg(CCTSMonDoc *pDoc, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSbcLogChkDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_SBC_LOGCHK_DLG };
	
	CMyGridWnd	m_wndLayoutGrid;
	CCTSMonDoc	*m_pDoc;
	CFont		m_Font;
	bool		m_bRackIndexUp;
	int			m_nKeywordIndex;
	
	CLabel	m_LabelPrecisionName;
		
	int	m_nLayOutRow;
	int m_nLayOutCol;
	int m_nMaxStageCnt;
	
	BOOL GetModuleRowCol(int nModuleID, int &nRow, int &nCol);
	void InitGridWnd();	
	void InitFont();
	void InitLabel();
	void InitColorBtn();
	
	void Fun_ClearGrid();
	
	int KMP(char *s, char *p);			// 문자열 비교 알고리즘
	void make_f(char *p, int *f);	

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtnSearch();
	virtual BOOL OnInitDialog();
	CColorButton2 m_BtnSearch;
	CColorButton2 m_BtnCSVConvert;
	CColorButton2 m_BtnIDOK;
	afx_msg void OnBnClickedOk();
	CLabel m_LabelName;
	CComboBox m_ctrlKeyword;
	CProgressCtrl m_CtrlProgressGetLog;
	afx_msg void OnBnClickedCsvOutput();
	CDateTimeCtrl m_ctrlDateTime;
};
