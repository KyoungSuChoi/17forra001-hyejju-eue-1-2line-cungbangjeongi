// CTSEditorDoc.h : interface of the CCTSEditorDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CTSEditorDOC_H__DBF5825C_6297_46F0_8F69_38007690585C__INCLUDED_)
#define AFX_CTSEditorDOC_H__DBF5825C_6297_46F0_8F69_38007690585C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

typedef struct tagProcDataTypeIDMessage {
	int nCode;
	char szMessage[SCH_MAX_CODE_SIZE];
	int nDataType;
} DATA_CODE;

#define MAX_UNDO_COUNT	10

class CCTSEditorDoc : public CDocument
{
protected: // create from serialization only
	CCTSEditorDoc();
	DECLARE_DYNCREATE(CCTSEditorDoc)

// Attributes
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCTSEditorDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual void OnCloseDocument();
	//}}AFX_VIRTUAL

// Implementation
public:
	BOOL UpdateMainServer(long lModelNo, CString strModelName, CString strCreator, CString strState);
	STEP * GetStepDataGotoID(int nGotoID /*One Base*/);
	CString GetSimulationTitle(CString strFileName);
	int GetLastRefCapStepNo(int nCurStepNo);
	long m_lMaxCurrent1;
	long m_lLoadedProcType;
	STEP * GetLastStepData();
	float WattHourUnitTrans(float fData, BOOL bSave = TRUE);
	float WattUnitTrans(float fData, BOOL bSave = TRUE);
	float CUnitTrans(float fData, BOOL bSave = TRUE);
	float IUnitTrans(float fData, BOOL bSave = TRUE);
	float VUnitTrans(float fData, BOOL bSave = TRUE);
	long m_lVRefHigh;
	long m_lVRefLow;
	BOOL RedoStepEdit();
	void RemoveUndoStep();
	BOOL UpdoStepEdit(int &nStartStep, int &nEndStep);
	void MakeUndoStep();
	int		GetVtgUnitFloat()	{	return m_VtgUnitInfo.nFloatPoint;	}
	int		GetCrtUnitFloat()	{	return m_CrtUnitInfo.nFloatPoint;	}
	int		GetCapUnitFloat()	{	return m_CapUnitInfo.nFloatPoint;	}
	int		GetWattUnitFloat()	{	return m_WattUnitInfo.nFloatPoint;	}
	int		GetWattHourUnitFloat()	{	return m_WattHourUnitInfo.nFloatPoint;	}

	CString GetCrtUnit()	{	return m_CrtUnitInfo.szUnit;	};
	CString GetVtgUnit()	{	return m_VtgUnitInfo.szUnit;	};
	CString GetCapUnit()	{	return m_CapUnitInfo.szUnit;	};
	CString GetWattUnit()	{	return m_WattUnitInfo.szUnit;	};
	CString GetWattHourUnit()	{	return m_WattHourUnitInfo.szUnit;	};
	BOOL IsEdited();
	void RemoveInitStepArray();
//	BOOL m_bCycleType;
//	BOOL m_bEDLCType;
	BOOL m_bUseLogin;
	BOOL m_bEditedFlag;
	BOOL BackUpDataBase();
	BOOL InsertStep(int nStepIndex, STEP *pStep);
	BOOL PasteStep(int nStepNo);
	BOOL AddCopyStep(int nStepNo);
	BOOL ClearCopyStep();
	int m_nGradingStepLimit;		//Grading 입력 할 수 있는 최대수 
	CString m_strLastErrorString;
	int GetProcIndex(int nID);
	int GetProcID(int nIndex);
	BOOL RequeryProcType();
	BOOL RequeryGradeType();		//Grade 
	int PermissionCheck(int nAction);
//	CString m_strCurFolder;
//	CString m_strDBFolder;
	SCH_LOGIN_INFO m_LoginData;

	int RequeryStepGrade(STEP *pStep, LONG lStepID);
	int SavePreTestParam(LONG lTestID, LONG lModelID);
	int SaveStepGrade(STEP *pStep, LONG lStepID, LONG lModelID);
	int StepValidityCheck();
	int SaveStep(LONG lTestID, LONG lModelID);
	BOOL AddNewStep(int nStepNum, STEP *pCopyStep = NULL);
	BOOL RequeryPreTestParam(LONG lTestID, LONG lModelID);
	STEP * GetStepData(int nStepNum /*One Base*/);
	int GetTotalStepNum();
	CString MakeEndString(int nStepNum);
	
	BOOL ReMoveStepArray(int nStepNum = SCH_STEP_ALL);
	int RequeryStepData(LONG lTestID, LONG lModelID);
	CPtrArray m_apStep;
	CPtrArray m_apCopyStep;
	CPtrArray m_apUpdoStep;

	CPtrList m_aPtrArrayList;	//List of CPtrArray pointer
	POSITION m_posUndoList;


	CPtrArray m_apProcType;
	CPtrArray m_apGradeType;		//ljb 2008-11-00	Grade Type 추가

	TEST_PARAM m_sPreTestParam;
//	int m_VoltageUnitMode;
//	int m_CurrentUnitMode;

	UNIT_INFO m_CrtUnitInfo;
	UNIT_INFO m_VtgUnitInfo;
	UNIT_INFO m_CapUnitInfo;
	UNIT_INFO m_WattUnitInfo;
	UNIT_INFO m_WattHourUnitInfo;
	
	long	m_nMinRefI;
	long	m_nCCRefVoltage;
	LONG	m_lMaxCurrent;
	LONG	m_lMaxVoltage;

	virtual ~CCTSEditorDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	BOOL RemoveUndoList(POSITION pos);
	CPtrArray m_apInitStep;	//초기 Loading시 복사본 변경 여부 확인를 위해 
	TEST_PARAM m_sInitPreTestParam;


	//{{AFX_MSG(CCTSEditorDoc)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CTSEditorDOC_H__DBF5825C_6297_46F0_8F69_38007690585C__INCLUDED_)
