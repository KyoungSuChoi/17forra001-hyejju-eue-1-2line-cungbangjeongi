#if !defined(AFX_PROCEDURESELDLG_H__88D9CB56_F023_4F20_8060_17C1E5DC570E__INCLUDED_)
#define AFX_PROCEDURESELDLG_H__88D9CB56_F023_4F20_8060_17C1E5DC570E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ProcedureSelDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CProcedureSelDlg dialog
#include "MyGridWnd.h"

class CProcedureSelDlg : public CDialog
{
// Construction
public:
	BOOL	m_bShowTestSelect;
	long m_lTestID;
	BOOL RequeryTestList();
//	CString m_strModuleName;
	BOOL GetTestID();
//	long m_nFirstTestID;
	long m_nModelIndex;
	BOOL RequeryBatteryModel();
	void InitModelGrid();
	CProcedureSelDlg(CWnd* pParent = NULL);   // standard constructor
	STR_CONDITION_HEADER	m_TestHeader;
// Dialog Data
	//{{AFX_DATA(CProcedureSelDlg)
	enum { IDD = IDD_PROCEDURE_SEL_DLG };
	CXPButton	m_btnOK;
	CXPButton	m_btnCancel;
	CXPButton	m_btnDetail;
	CLabel	m_ctrlProLabel;
	CLabel	m_ctrlTitle;
	CLabel	m_strSelProcName;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CProcedureSelDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CMyGridWnd	m_wndModelList;
	CMyGridWnd	m_wndTestList;
	void InitTestListGrid();

	// Generated message map functions
	//{{AFX_MSG(CProcedureSelDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnOk();
	afx_msg void OnDetailButton();
	//}}AFX_MSG
	afx_msg LONG OnMovedCurrentCell(WPARAM wParam, LPARAM lParam);
	afx_msg LONG OnGridDoubleClick(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROCEDURESELDLG_H__88D9CB56_F023_4F20_8060_17C1E5DC570E__INCLUDED_)
