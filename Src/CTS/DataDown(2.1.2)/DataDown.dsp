# Microsoft Developer Studio Project File - Name="DataDown" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=DataDown - Win32 DebugE
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "DataDown.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "DataDown.mak" CFG="DataDown - Win32 DebugE"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "DataDown - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "DataDown - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE "DataDown - Win32 DebugE" (based on "Win32 (x86) Application")
!MESSAGE "DataDown - Win32 ReleaseE" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "DataDown - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "../../../obj/DataDown_Release"
# PROP Intermediate_Dir "../../../obj/DataDown_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GR /GX /O2 /D "_AFXDLL" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x412 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x412 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 /nologo /subsystem:windows /machine:I386 /out:"../../../bin/Release/DataDown.exe"

!ELSEIF  "$(CFG)" == "DataDown - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "../../../obj/DataDown_Debug"
# PROP Intermediate_Dir "../../../obj/DataDown_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GR /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x412 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x412 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /out:"../../../bin/Debug/DataDown.exe" /pdbtype:sept

!ELSEIF  "$(CFG)" == "DataDown - Win32 DebugE"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "DataDown___Win32_DebugE"
# PROP BASE Intermediate_Dir "DataDown___Win32_DebugE"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "../../../obj/DataDown_DebugE"
# PROP Intermediate_Dir "../../../obj/DataDown_DebugE"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GR /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GR /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x412 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x412 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 BFCtrlD.lib BFCommonD.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /out:"../../../bin/DebugE/DataDown.exe" /pdbtype:sept

!ELSEIF  "$(CFG)" == "DataDown - Win32 ReleaseE"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "DataDown___Win32_ReleaseE"
# PROP BASE Intermediate_Dir "DataDown___Win32_ReleaseE"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "../../../obj/DataDown_ReleaseE"
# PROP Intermediate_Dir "../../../obj/DataDown_ReleaseE"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GR /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GR /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x412 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x412 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 BFCtrl.lib BFCommon.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 /nologo /subsystem:windows /machine:I386 /out:"../../../bin/ReleaseE/DataDown.exe"

!ENDIF 

# Begin Target

# Name "DataDown - Win32 Release"
# Name "DataDown - Win32 Debug"
# Name "DataDown - Win32 DebugE"
# Name "DataDown - Win32 ReleaseE"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\DataDown.cpp
# End Source File
# Begin Source File

SOURCE=.\DataDown.rc
# End Source File
# Begin Source File

SOURCE=.\DataDownDlg.cpp
# End Source File
# Begin Source File

SOURCE=..\..\UtilityClass\src\FolderDialog.cpp
# End Source File
# Begin Source File

SOURCE=..\..\UtilityClass\src\FormResultFile.cpp
# End Source File
# Begin Source File

SOURCE=..\..\UtilityClass\src\FtpDownLoad.cpp
# End Source File
# Begin Source File

SOURCE=..\..\UtilityClass\src\Grading.cpp
# End Source File
# Begin Source File

SOURCE=.\IPSetDlg.cpp
# End Source File
# Begin Source File

SOURCE=..\..\UtilityClass\src\Markup.cpp
# End Source File
# Begin Source File

SOURCE=.\ModuleAddDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\NameInputDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ProfileListInfo.cpp
# End Source File
# Begin Source File

SOURCE=.\SaveOptionDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ShowDetailResultDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=..\..\UtilityClass\src\Step.cpp
# End Source File
# Begin Source File

SOURCE=..\..\UtilityClass\src\TestCondition.cpp
# End Source File
# Begin Source File

SOURCE=.\TrayIcon.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\DataDown.h
# End Source File
# Begin Source File

SOURCE=.\DataDownDlg.h
# End Source File
# Begin Source File

SOURCE=..\..\UtilityClass\src\FolderDialog.h
# End Source File
# Begin Source File

SOURCE=..\..\UtilityClass\src\FormResultFile.h
# End Source File
# Begin Source File

SOURCE=..\..\UtilityClass\src\FtpDownLoad.h
# End Source File
# Begin Source File

SOURCE=..\..\UtilityClass\src\Grading.h
# End Source File
# Begin Source File

SOURCE=.\IPSetDlg.h
# End Source File
# Begin Source File

SOURCE=..\..\UtilityClass\src\Markup.h
# End Source File
# Begin Source File

SOURCE=.\ModuleAddDlg.h
# End Source File
# Begin Source File

SOURCE=.\NameInputDlg.h
# End Source File
# Begin Source File

SOURCE=.\ProfileListInfo.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\SaveOptionDlg.h
# End Source File
# Begin Source File

SOURCE=.\ShowDetailResultDlg.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=..\..\BFLib\src\Utility\Step.h
# End Source File
# Begin Source File

SOURCE=..\..\UtilityClass\src\Step.h
# End Source File
# Begin Source File

SOURCE=..\..\UtilityClass\src\TestCondition.h
# End Source File
# Begin Source File

SOURCE=.\TrayIcon.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\DataDown.ico
# End Source File
# Begin Source File

SOURCE=.\res\hostmanager.ico
# End Source File
# Begin Source File

SOURCE=.\res\red_btn_.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Untitled.ico
# End Source File
# Begin Source File

SOURCE=.\res\YellowBar.bmp
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
