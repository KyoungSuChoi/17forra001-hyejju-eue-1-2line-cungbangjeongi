// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "CTSEditor.h"

#include "MainFrm.h"
#include "CTSEditorView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_COPYDATA()
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	LanguageinitMonConfig();	
}

CMainFrame::~CMainFrame()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

bool CMainFrame::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CMainFrame"), _T("TEXT_CMainFrame_CNT"), _T("TEXT_CMainFrame_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CMainFrame_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CMainFrame"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}


int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

// 	if (!m_wndStatusBar.Create(this) ||
// 		!m_wndStatusBar.SetIndicators(indicators,
// 		  sizeof(indicators)/sizeof(UINT)))
// 	{
// 		TRACE0("Failed to create status bar\n");
// 		return -1;      // fail to create
// 	}

	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);

	//20201030ksj
	CString strVer;
	strVer.Format("CTSEditor (SKEUE_%s_%s)",MacroDateToCString(__DATE__), __TIME__);
	SetWindowText(strVer);

	return 0;
}

CString CMainFrame::MacroDateToCString(const char *MacroDate)	//20201030ksj
{
	const int comfile_date_len = 12;

	// error check
	if (NULL == MacroDate || comfile_date_len - 1 != strlen(MacroDate))
		return "";

	const char month_names[] = "JanFebMarAprMayJunJulAugSepOctNovDec";

	char s_month[5] = {0,};
	int iyear = 0, iday = 0;

	sscanf(MacroDate, "%s %d %d", s_month, &iday, &iyear);
	int imonth = (strstr(month_names, s_month) - month_names) / 3 + 1;

	CString strDate;
	strDate.Format("%4d-%2d-%2d",iyear,imonth,iday);
	return strDate;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs
	
//	cs.style = WS_OVERLAPPED | WS_CAPTION | FWS_ADDTOTITLE
//		| WS_THICKFRAME | WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_MAXIMIZE;

	cs.style &= ~FWS_ADDTOTITLE;
	cs.cx = ::GetSystemMetrics(SM_CXSCREEN);
	cs.cy = ::GetSystemMetrics(SM_CYSCREEN);
	cs.lpszName = "CTSEditor";

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers
extern CString g_strDataBaseName;

CString GetDataBaseName()
{
/*	CString strTemp;
	strTemp = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");		//Get DataBase Folder
	if(strTemp.IsEmpty())
	{
		return strTemp;
	}
	
	strTemp = strTemp+ "\\" + FORM_SET_DATABASE_NAME;
	return strTemp;
*/
	return g_strDataBaseName;
}

BOOL CMainFrame::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct) 
{
	// TODO: Add your message handler code here and/or call default

	//CTSMonPro에서 전달된 Message
	if(pCopyDataStruct->dwData == 3)
	{
		if(pCopyDataStruct->cbData > 0)
		{
//			char *pData = new char[pCopyDataStruct->cbData];
//			memcpy(pData, pCopyDataStruct->lpData, pCopyDataStruct->cbData);
			
			CString strArgu((char *)pCopyDataStruct->lpData);

			CString strModel, strTest;
			strModel = strArgu.Left(strArgu.Find(','));
			strTest = strArgu.Mid(strArgu.Find(',')+1);
			
			//char szBuff1[64], szBuff2[64];
			//sscanf(pData, "%s %s", szBuff1, szBuff2);
//			delete [] pData;
			
			CCTSEditorView	*pView = (CCTSEditorView *)GetActiveView();

			if(pView)
				pView->SetDefultTest(strModel, strTest);
		}
	}	
	
	return CFrameWnd::OnCopyData(pWnd, pCopyDataStruct);
}

void CMainFrame::OnClose()
{
	CString strTemp;

	// TODO: Add your message handler code here and/or call default
	if(((CCTSEditorDoc *)GetActiveDocument())->IsEdited())
	{
		strTemp = TEXT_LANG[0];//"조건이 변경 되었습니다. 저장후 종료 하시겠습니까?"
		int nRtn = MessageBox(strTemp, TEXT_LANG[1], MB_ICONQUESTION|MB_YESNOCANCEL);//"저장"
		if(IDYES == nRtn)
		{
			if(((CCTSEditorView *)GetActiveView())->StepSave() == FALSE)
			{
				if(MessageBox(TEXT_LANG[2], TEXT_LANG[3], MB_ICONQUESTION|MB_YESNO) != IDYES)//"저장을 실패 하였습니다. 종료 하시겠습니까?"//"종료"
				{
					return;
				}
			}
		}
		else if(nRtn == IDCANCEL)
		{
			return;
		}

	}

	CFrameWnd::OnClose();
}