// PneDataTreeCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "PneDataTreeCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPneDataTreeCtrl

CPneDataTreeCtrl::CPneDataTreeCtrl()
{
}

CPneDataTreeCtrl::~CPneDataTreeCtrl()
{
}


BEGIN_MESSAGE_MAP(CPneDataTreeCtrl, CTreeCtrl)
	//{{AFX_MSG_MAP(CPneDataTreeCtrl)
	ON_NOTIFY_REFLECT(TVN_ITEMEXPANDING, OnItemexpanding)
	ON_NOTIFY_REFLECT(TVN_GETDISPINFO, OnGetdispinfo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPneDataTreeCtrl message handlers

void CPneDataTreeCtrl::OnItemexpanding(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
		
	HTREEITEM hItem = pNMTreeView->itemNew.hItem;

	ExpendTree(hItem);

	*pResult = 0;
}

void CPneDataTreeCtrl::SetRootDir(CString strRoot)
{
	m_strRootDir = strRoot;
	DeleteAllItems();

	HTREEITEM tItem;
	CString FolderName, strTemp;
	CFileFind ffind, subFind;

	FolderName.Format("%s\\*", m_strRootDir);
	BOOL   bWorking = ffind.FindFile(FolderName);		
	while (bWorking)		
	{
		bWorking	= ffind.FindNextFile();	
		if(ffind.IsDirectory())
		{
			if(!ffind.IsDots())
			{
				strTemp = ffind.GetFileName();
				tItem = InsertItem(strTemp);
				
				//Insert First client
				CString strFilter;
				strFilter.Format("%s\\*", ffind.GetFilePath());
				BOOL bWorking2 = subFind.FindFile(strFilter);
				while(bWorking2)
				{
					bWorking2	= subFind.FindNextFile();
					if(subFind.IsDirectory())
					{
						if(!subFind.IsDots())
						{
							strTemp = subFind.GetFileName();
							InsertItem(strTemp, tItem);
						}
					}
				}
			}
		}
	}

//	CShellPidl pidl((UINT)CSIDL_DESKTOP, m_hWnd);
//	char szPath[MAX_PATH];
  //	SHGetSpecialFolderLocation(m_hWnd, CSIDL_DESKTOP, szPath);
   
    SHFILEINFO sfi;
	ZeroMemory(&sfi, sizeof(SHFILEINFO));
	HIMAGELIST hSysImageList = (HIMAGELIST) SHGetFileInfo("",
		0, &sfi, sizeof(SHFILEINFO), SHGFI_PIDL | SHGFI_SYSICONINDEX | SHGFI_SMALLICON);

	//TreeView_SetImageList(m_hWnd, hSysImageList, TVSIL_NORMAL);
	// postpone imagelist attaching
	// (seems it doesn't like a sendmessage when dynamically created
	// maybe because it has not received the WM_CREATE message yet?)
	PostMessage(TVM_SETIMAGELIST, TVSIL_NORMAL, (LPARAM)hSysImageList);
	
}

void CPneDataTreeCtrl::ExpendTree(HTREEITEM hItem)
{
	CString strTemp, FolderName, strCur;

	HTREEITEM  rItem = hItem;
	while(rItem)
	{
		if(strCur.IsEmpty() ==FALSE)
		{
			strTemp.Format("%s\\%s", GetItemText(rItem),strCur);
		}
		else
		{
			strTemp = GetItemText(rItem);
		}
		strCur = strTemp;
		rItem = GetParentItem(rItem);
	}
	FolderName.Format("%s\\%s", m_strRootDir, strCur);
	//////////////////////////////////////////////////////////////////////////
	
	rItem = GetChildItem(hItem);
	while(rItem)
	{
		strTemp.Format("%s\\%s\\*", FolderName, GetItemText(rItem));
		
		CFileFind subFind;
		BOOL bWorking = subFind.FindFile(strTemp);
		while(bWorking)
		{
			bWorking = subFind.FindNextFile();
			if(subFind.IsDirectory())
			{
				if(!subFind.IsDots())
				{
					strTemp = subFind.GetFileName();
					InsertItem(strTemp, rItem);
				}
			}
		}		
		rItem = GetNextSiblingItem(rItem);
		subFind.Close();
	}
}

void CPneDataTreeCtrl::OnGetdispinfo(NMHDR* pNMHDR, LRESULT* pResult) 
{
	TV_DISPINFO* pTVDispInfo = (TV_DISPINFO*)pNMHDR;
	// TODO: Add your control notification handler code here

	if (pTVDispInfo->item.mask & (TVIF_IMAGE | TVIF_SELECTEDIMAGE))
	{
	}
	SHFILEINFO sfi;
	ZeroMemory(&sfi, sizeof(SHFILEINFO));
	UINT uFlags = SHGFI_PIDL | SHGFI_SYSICONINDEX;
	//DWORD_PTR SHGetFileInfo( LPCTSTR pszPath,  DWORD dwFileAttributes,   SHFILEINFO *psfi,   UINT cbFileInfo,   UINT uFlags);
//	SHGetFileInfo(0, &sfi, sizeof(SHFILEINFO), uFlags);
	
//	return sfi.iIcon;

	*pResult = 0;
}
