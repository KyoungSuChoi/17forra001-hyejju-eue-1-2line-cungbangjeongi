’
 TFORM7 0  TPF0TForm7Form7LeftŃ Top¾ Width®Height	CaptionForm7Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightó	Font.NameMS Sans Serif
Font.Style OnHideFormHideOnShowFormShowPixelsPerInchx
TextHeight PESGraph	PESGraph1LeftTopWidthHeight¹TabOrder ViewingStylesgColorPrepareImagesGraphForeColorclBlackGraphBackColorclWhite	DeskColorclSilver	TextColorclBlackShadowColorclGray	MainTitle
Main TitleSubTitle	Sub TitleSubsetsPointsDataPrecisionsgThreeDecimalsAllowCustomization	AllowExporting	AllowMaximization	
AllowPopup	AllowUserInterface	FontSizesgMediumMainTitleFontTimes New RomanMainTitleBoldMainTitleItalicMainTitleUnderlineSubTitleFontTimes New RomanSubTitleBoldSubTitleItalicSubTitleUnderline	LabelFontArial	LabelBoldLabelItalicLabelUnderline
XAxisLabelX Axis
YAxisLabelY AxisScrollingSubsets VBoundaryTypessgNoneUpperBoundTextUpperLowerBoundTextLowerXAxisScaleControlsgAutoYAxisScaleControlsgAutoPlottingMethodsgLineGridLineControlsgYPlusXAxisGridInFrontNoScrollingSubsetControlGraphDataLabelsDataShadows	MonoWithSymbolsThreeDDialogs	DefOrientationsgLandscapeManualScaleControlYsgAutoScale
ManualMinY    ś’’@
ManualMaxY     ³@ManualScaleControlXsgAutoScale
ManualMinX    ųef@
ManualMaxX   @ŠĢ,@AllowSubsetHotSpotsAllowGraphHotSpotsAllowDataHotSpotsAllowCoordPromptingMarkDataPointsRYAxisLabelRight Y AxisRYAxisScaleControlsgAutoManualScaleControlRYsgAutoScaleManualMinRY    ś’’@ManualMaxRY     ³@AllowPlotCustomization	AllowBubbleAllowBestFitCurve	AllowSpline	NegativeFromXAxis	ManualXAxisTicknLineManualYAxisTicknLineManualRYAxisTicknLine
BubbleSizesgMediumBubblesSpecificPlotModesgNormal	PointSizesgMediumPointsBestFitDegree	sgDegree2CurveGranularitysgMediumLinesAllowDataLabelssgDataValuesManualXAxisTick       ’?ManualXAxisLine       ’?ManualYAxisTick       ’?ManualYAxisLine       ’?ManualRYAxisTick       ’?ManualRYAxisLine       ’?	AllowLine	
AllowStick	AllowBar	
AllowPoint		AllowArea	AllowBestFitLine	AllowZoomingsgNoZoomingForceRightYAxisAllowPointsPlusLine	AllowPointsPlusSpline	SymbolFrequencysgEightPercentScrollingHorzZoom
CursorMode
sgNoCursorCursorPromptStylesgYValueCursorPromptTrackingMouseCursorControlShowAnnotationsAllowGraphAnnotHotSpotsGraphAnnotationTextSize2AllowAnnotationControlAnnotationsInFront	NoRandomPointsToExportCursorPageAmountXAxisVertNumberingLineGapThreshold     Ā½šy@ShowGraphAnnotationsShowXAxisAnnotationsShowYAxisAnnotationsShowHorzLineAnnotationsShowVertLineAnnotationsAllowXAxisAnnotHotSpotsAllowYAxisAnnotHotSpotsAllowHorzLineAnnotHotSpotsAllowVertLineAnnotHotSpotsAxesAnnotationTextSize2LineAnnotationTextSize2ZoomInterfaceOnlysgNormalZoomingVertOrient90DegreesLogScaleExpLabels	PlottingMethodIIsgLine2TXAxisLabel
Top X AxisTXAxisScaleControlsgAutoManualScaleControlTXsgAutoScaleManualMinTX    ųef@ManualMaxTX   @ŠĢ,@ManualTXAxisTicknLineManualTXAxisTick       ’?ManualTXAxisLine       ’?ForceTopXAxis
XAxisOnTopYAxisOnRight	FocalRect	FontSizeGlobalCntl       ’?FontSizeTitleCntl       ’?	ShowYAxissgGridPlusAxisLabels
ShowRYAxissgGridPlusAxisLabels	ShowXAxissgGridPlusAxisLabels
ShowTXAxissgGridPlusAxisLabels	GridStylesgThinInvertedYAxisInvertedRYAxisInvertedXAxisInvertedTXAxis
YAxisColorRYAxisColor
XAxisColorTXAxisColorFontSizeLegendCntl       ’?YAxisLongTicksRYAxisLongTicksXAxisLongTicksTXAxisLongTicksMultiAxesSeparatorssgNoSeparatorAllowOleExport	  TButtonButton1LeftTopČWidth© Height!CaptionLast 3 MinutesTabOrderOnClickButton1Click  TButtonButton2LeftšTopČWidth© Height!CaptionShow All DataTabOrderOnClickButton2Click  TTimerTimer1EnabledOnTimerTimer1TimerLeftTop   