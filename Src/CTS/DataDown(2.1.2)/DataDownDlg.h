// DataDownDlg.h : header file
//

#if !defined(AFX_DataDownDLG_H__07F717D8_436F_4946_8C8F_6F04B06F5719__INCLUDED_)
#define AFX_DataDownDLG_H__07F717D8_436F_4946_8C8F_6F04B06F5719__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


/////////////////////////////////////////////////////////////////////////////
// CDataDownDlg dialog
//#include <ProgressWnd.h>
#include <afxinet.h>
#include "TrayIcon.h"

#include "XPButton.h"
#include "Afxmt.h"

#define	WM_ICON_NOTIFY      WM_USER+10       // 사용자 정의 메세지
#define MAX_EXCEL_ROW		65536
//#define MAX_EXCEL_ROW		100
#define STR_BUFF_SIZE	4096
#define MAX_READ_PACKET_SIZE	100		//=> 5*1024byte

#include "SaveOptionDlg.h"
#include <winInet.h>
#pragma comment(lib, "Wininet.lib")

typedef struct tag_GraphResultData {
	char szResultFile[256];
	bool bExcuteGraphAnalyzer;
	int	 nRetryCnt;
} STR_GRAPH_DATA;

#define PUMPMESSAGE()\
		{\
			MSG msg;\
			while( PeekMessage( &msg, NULL, NULL, NULL, PM_REMOVE ) )\
			{\
				TranslateMessage(&msg);\
				DispatchMessage(&msg);\
			}\
		}


class CDataDownDlg : public CDialog
{
// Construction
public:

	//ysy			2015-06-12			다국어
	CString *TEXT_LANG;	
	bool LanguageinitMonConfig();

	BOOL DownLoadOnLineModeProfile(long lUnitID, CString strSaveDir, CString strTrayID, CString strUnitName);
	BOOL ExecuteProgram(CString strProgram, CString strArgument = "", CString strClassName = "", CString strWindowName = "", BOOL bNewExe = FALSE, 	BOOL bWait = FALSE);
	VOID ExecuteGraphAnalyzer(CString strFile);
	BOOL IfLocalFileExist(const char *pszFilePath, double *pdwFileSize);
	CString GetLastFtpErrorResponse();
	BOOL DownloadDataFile(CString strRemote, CString strSaveDir, CStringList &strServerFileList, CStringList &strLocalFileList, CString strIp, CString strID="root", CString strPassword="dusrnth");
	BOOL SetPrimary(CString strListFile);
	BOOL m_bWork;
//	CString ParsingResultFileName(CString strFileName, CString &strSavePath, CString &strIp, CString &strTestSerial, CString &strTray);
	CString m_strMonDir;
	int m_nOnLineSelModuleID;
	BOOL m_bSearchModeOffLine;
	CString GetRemoteFileList(CString strIp, CString strDir, CStringList & aFileList, CStringList & aSensorFileList, CStringList & aFadmFileList);
	BOOL ListUpAllRemote(CString strIp, int nGroupNo, CString strSearchTray = "");
	BOOL HandleMessage();
	CString ConvertDataString(CWordArray &awIndex, CString &strSrcData);
	BOOL ParsingString(CString strString, CStringArray &aStrArray);
	BOOL FindDataColumn(CWordArray &aChArray, CString &strColumn, int nSensorType, CFormResultFile *pRltFile);
	BOOL GetUsedSensorCh(CWordArray &aSelTrayCh, int nOption, CWordArray &awSensorCh, CFormResultFile *pRltFile);
	BOOL MakeSensorExcelFile(CStringList *pFileList, CString strFileName, CWordArray &awSensorCh);
	BOOL GetChannelList(CString strFileName);
	BOOL m_bHideMode;
	BOOL DeletePath(CString strPath);
	BOOL AddFolderList(CString strCurFolderName);
	BOOL DownLoadAutoMatic(CString strFileName, CString strSavePath = _T(""));
	CString LoadFormResultData(CString strFileName);
	void WriteLog(CString strMsg);
//	CString m_strAutoOpenFileName;
	CString m_strUserOpenFileName;
	BOOL DownLoadLocalData(CString strDir, CString strSaveDir, CWordArray &awSelTrayCh);
	BOOL DownLoadLocalDataAll(CString strDir, CString strSaveDir, CWordArray &awSelTrayCh);
	CString m_strFileLocation;
	BOOL m_bFindLocal;
	CString MakeMyFolder(CString strDir, BOOL bOverWrite = TRUE);
//	CString m_strDataTempPath;
	CString FindRealTimeDataFromRemote(CString strIp, int nGroupNo, CString strTestSerial, CStringList &aMdChannel, CStringList &aSensorCh, CStringList &aFdChannel);
	CString FindOnlineRealTimeDataFromRemote(CString strIp, int nGroupNo, CString strTestSerial, CStringList &aMdChannel, CStringList &aSensorCh, CStringList &aFdChannel);
	CString FindRealTimeDataFromLocal(CString strLocation, CString strTestSerial);
	BOOL GetRealTimeData(BOOL bLocal, CString strRemoteDir, CWordArray &awTraySelCh, CDWordArray &awFadmSelCh,  CWordArray &awSensorSelCh, CString strIp, CString strSelPath);
	BOOL DownLoadRemoteData( CString strIp, CString strRemote, CString strSaveDir, CDWordArray &aSelCh, CWordArray &awSensorCh, CDWordArray &awFadmCh);
	BOOL DownLoadRemoteDataA( CString strIp, CString strRemote, CString strSaveDir, CDWordArray &aSelCh, CWordArray &awSensorCh, CDWordArray &awFadmCh);
	CString GetRemoteLocation( CString strTestSerial, CString strIp, int nGroupNo = 1, int nLineMode = 0);
	BOOL DeleteData();
	BOOL SaveData();
	BOOL GetFolderList(CString strTestSerial);
	void SetModuleID(int nModuleID);
	void SetModuleIP(CString strIP);
	CDataDownDlg(CWnd* pParent = NULL);	// standard constructor
	~CDataDownDlg();
	CString GetIPAddress(int nModuleID,int &nGroupNo);

	/************************************************************************/
	/* File관련 함수 처리                                                   */
	/************************************************************************/
	char	szLogFileName[512];
	CString m_strCurFolder;
	char	m_szLogFileName[MAX_PATH];
	BOOL	WriteLog( CString szLog, int nMode );
	BOOL	WriteLog( char *szLog, int nMode );
	BOOL	WriteLogFile(char *szFileName, char *szData, int nMode);
	VOID	SetLogFileName(char *szFileName);
	BOOL	ForceDirectory(LPCTSTR lpDirectory);
	CString GetFilePath(LPCTSTR lpszFilePath);
	BOOL	DirectoryExists(LPCTSTR lpszDir);	
	INT		FileExists(LPCTSTR lpszName);
	

	enum { NOT_SAVE, ALL_SAVE, MAPPING_SAVE};
	enum { SENSOR1, SENSOR2, ALL_SENSOR};

// Dialog Data
	//{{AFX_DATA(CDataDownDlg)
	enum { IDD = IDD_DataDown_DIALOG };
	CListBox	m_ctrlProfileList;
	CLabel	m_ctrlSelModule;
	CListBox	m_ctrlRemoteList;
	CComboBox	m_ctrlUnitCombo;
	CXPButton	m_btnOK;
	CXPButton	m_btnShowFile;
	CXPButton	m_btnGetFile;
	CXPButton	m_btnDeSelAll;
	CXPButton	m_btnSelAll;
	CXPButton	m_btnSaveOption;
	CXPButton	m_btnSelSave;
	CProgressCtrl	m_downProgress;
	CListBox	m_LogList;
	CListBox	m_listBox;
	BOOL	m_bModuleFind;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDataDownDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CString GetNextTargetListFile();
	CString GetNextTargetListFile_new();
	CCriticalSection m_cs;
	BOOL MakeNextFile(FILE *fp,  int &nFileCount, CString strFile);

	CSaveOptionDlg m_SaveSetDlg;
	CTrayIcon m_TrayIcon;                         // CTrayIcon 를 멤버 변수로 선언
	CString m_strTestName;
	BOOL MakeExcelFile(CString strLocalFile, CStringList &aSourctFileList, CStringList &aSourctSensorFileList);
	CInternetSession m_InternetSession;          //인터넷 세션 클래스

	CString m_strPathName;
	CString m_strPassword;
	CString m_strID;
	CString m_strIPAddress;
	int m_nModuleID;
	HICON m_hIcon;
	CString	m_strDBPath;
	CFormResultFile m_resultData;
	CStringList m_strAutoFileList;
	CPtrArray m_apGraphResultList;

	// Generated message map functions
	//{{AFX_MSG(CDataDownDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnGetList();
	afx_msg void OnDblclkDataList();
	afx_msg void OnSaveDataButton();
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnDownloadData();
	afx_msg void OnUpdateDeleteData(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDownloadData(CCmdUI* pCmdUI);
	afx_msg void OnDeleteData();
	afx_msg void OnDelAllButton();
	afx_msg void OnSelectAllButton();
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnDeselAllButton();
	afx_msg void OnShowDlg();
	afx_msg void OnExit();
	virtual void OnCancel();
	afx_msg void OnOptionButton();
	afx_msg void OnDestroy();
	afx_msg void OnButton2();
	afx_msg void OnSelchangeDataList();
	afx_msg void OnRadio1();
	afx_msg void OnRadio2();
	afx_msg void OnOnButton();
	afx_msg void OnSelchangeRemoteList();
	afx_msg void OnSelchangeComboUnit();
	afx_msg void OnRadio3();
	afx_msg void OnRadio4();
	//}}AFX_MSG
	afx_msg LONG OnTrayNotification(UINT wParam, LONG lParam);
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DataDownDLG_H__07F717D8_436F_4946_8C8F_6F04B06F5719__INCLUDED_)
