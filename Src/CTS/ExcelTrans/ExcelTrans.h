// ExcelTrans.h : main header file for the ExcelTrans application
//

#if !defined(AFX_ExcelTrans_H__5BCA0A4C_72A1_43CD_B15C_EB4DDC8D023D__INCLUDED_)
#define AFX_ExcelTrans_H__5BCA0A4C_72A1_43CD_B15C_EB4DDC8D023D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#define REG_SECTION	"SaveSetting"

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CExcelTransApp:
// See ExcelTrans.cpp for the implementation of this class
//

class CExcelTransApp : public CWinApp
{
public:
	CExcelTransApp();

	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CExcelTransApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CExcelTransApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	virtual int ExitInstance();
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ExcelTrans_H__5BCA0A4C_72A1_43CD_B15C_EB4DDC8D023D__INCLUDED_)
