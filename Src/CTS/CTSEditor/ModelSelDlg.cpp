#include "stdafx.h"
#include "ctseditor.h"
#include "ModelSelDlg.h"
#include "CTSEditorView.h"
#include "TestNameDlg.h"
#include "ColorButton2.h"
#include "TypeCopyDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CModelSelDlg dialog


CModelSelDlg::CModelSelDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CModelSelDlg::IDD, pParent)
	, m_strTypeNote(_T(""))
{
	//{{AFX_DATA_INIT(CModelSelDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_pView = pParent;
	ASSERT(m_pView);
	
	m_nSelectType = 0;
	m_nSelectProcess = -1;
	
	ZeroMemory(m_nType, sizeof(m_nType));	
	ZeroMemory(m_nProcess, sizeof(m_nProcess));
	
	LanguageinitMonConfig();
}

CModelSelDlg::~CModelSelDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CModelSelDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CModelSelDlg"), _T("TEXT_CModelSelDlg_CNT"), _T("TEXT_CModelSelDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CModelSelDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CModelSelDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

void CModelSelDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CModelSelDlg)	
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_LABEL1, m_Label1);
	DDX_Control(pDX, IDC_LABEL_SELECT_TYPE, m_LabelSelectType);
	DDX_Control(pDX, IDC_LABEL2, m_Label2);	
	DDX_Text(pDX, IDC_EDIT_TYPENOTE, m_strTypeNote);
	DDX_Control(pDX, IDC_BUTTON_BACK, m_Btn_Back);
	DDX_Control(pDX, IDC_BUTTON_NEXT, m_Btn_Next);
	DDX_Control(pDX, IDC_BUTTON_COPY, m_Btn_Copy);
	DDX_Control(pDX, IDC_BUTTON_SAVE, m_Btn_Save);
	DDX_Control(pDX, IDC_BUTTON_RESET, m_Btn_Reset);
	DDX_Control(pDX, IDCANCEL, m_Btn_Cancel);
}


BEGIN_MESSAGE_MAP(CModelSelDlg, CDialog)
	//{{AFX_MSG_MAP(CModelSelDlg)	
	ON_BN_CLICKED(IDC_BUTTON_COPY, OnButtonCopy)
	ON_BN_CLICKED(IDC_BUTTON_SAVE, OnButtonSave)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_GRID_DOUBLECLICK, OnGridDbClicked)
	ON_MESSAGE(WM_GRID_CLICK, OnGridClicked)	
	ON_BN_CLICKED(IDCANCEL, &CModelSelDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON_BACK, &CModelSelDlg::OnBnClickedButtonBack)
	ON_BN_CLICKED(IDC_BUTTON_NEXT, &CModelSelDlg::OnBnClickedButtonNext)
	ON_BN_CLICKED(IDC_BUTTON_RESET, &CModelSelDlg::OnBnClickedButtonReset)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CModelSelDlg message handlers

BOOL CModelSelDlg::OnInitDialog()
{
	CDialog::OnInitDialog();	
	// TODO: Add extra initialization here
	InitLabel();
	InitGridWnd();	
	InitFont();
	InitColorBtn();
	
	int i=0;
	CString strTemp = _T("");
	
	for(i=0; i<10; i++ )
	{
		m_nType[i] = i;		
	}
	
	for(i=0;i<20;i++)
	{
		m_nProcess[i] = i;
	}
	
	strTemp.Format("Type %ld", m_nSelectType);
	m_LabelSelectType.SetText(strTemp);
	
	if( RequeryBatteryModel(m_nSelectType) == true )
	{
		ChangeProcessInfo();	
	}	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CModelSelDlg::InitGridWnd()
{	
	int i=0;
	CString strTemp = _T("");
	
	m_wndBatteryModelGrid.SubclassDlgItem(IDC_MODEL_GRID_CUSTOM, this);
	m_wndBatteryModelGrid.Initialize();
	m_wndBatteryModelGrid.SetColCount(6);
	m_wndBatteryModelGrid.SetRowCount(10);
	
	m_wndBatteryModelGrid.SetScrollBarMode(SB_BOTH,gxnDisabled,TRUE);	
	//Use MemDc
	m_wndBatteryModelGrid.SetDrawingTechnique(gxDrawUsingMemDC);
	
	// SetModelGridColumnWidth();	

	int nGridWidth = 0;
	CRect ctrlRect;
	m_wndBatteryModelGrid.GetWindowRect(&ctrlRect);
	nGridWidth = ctrlRect.Width();



	m_wndBatteryModelGrid.SetDefaultColWidth(200);
	m_wndBatteryModelGrid.SetDefaultRowHeight(60);
	m_wndBatteryModelGrid.SetColWidth(0,0,0);			
	m_wndBatteryModelGrid.SetColWidth(2,2,400);	
	m_wndBatteryModelGrid.SetColWidth(3,3,0);	
	m_wndBatteryModelGrid.SetColWidth(5,5,400);		
	m_wndBatteryModelGrid.SetColWidth(6,6,0);	
	
	m_wndBatteryModelGrid.SetStyleRange(CGXRange(0, 1),	CGXStyle().SetValue("Process"));	
	m_wndBatteryModelGrid.SetStyleRange(CGXRange(0, 2),	CGXStyle().SetValue("Device"));
	m_wndBatteryModelGrid.SetStyleRange(CGXRange(0, 3),	CGXStyle().SetValue("Proctype"));
	m_wndBatteryModelGrid.SetStyleRange(CGXRange(0, 4),	CGXStyle().SetValue("Process"));
	m_wndBatteryModelGrid.SetStyleRange(CGXRange(0, 5),	CGXStyle().SetValue("Device"));
	m_wndBatteryModelGrid.SetStyleRange(CGXRange(0, 6),	CGXStyle().SetValue("Proctype"));
	
	for( i=0; i<10; i++ )
	{
		strTemp.Format("%d", i);
		m_wndBatteryModelGrid.SetStyleRange(CGXRange(i+1, 1), CGXStyle().SetHorizontalAlignment(DT_CENTER).SetInterior(RGB_BTN_BACKGROUND).SetFont(CGXFont().SetSize(20).SetBold(TRUE).SetWeight(1000)).SetValue(strTemp));
		m_wndBatteryModelGrid.SetStyleRange(CGXRange(i+1, 2), CGXStyle().SetHorizontalAlignment(DT_CENTER).SetFont(CGXFont().SetSize(20).SetBold(TRUE).SetWeight(1000)).SetValue("No setting"));
		strTemp.Format("%d", i+10);
		m_wndBatteryModelGrid.SetStyleRange(CGXRange(i+1, 4), CGXStyle().SetHorizontalAlignment(DT_CENTER).SetInterior(RGB_BTN_BACKGROUND).SetFont(CGXFont().SetSize(20).SetBold(TRUE).SetWeight(1000)).SetValue(strTemp));
		m_wndBatteryModelGrid.SetStyleRange(CGXRange(i+1, 5), CGXStyle().SetHorizontalAlignment(DT_CENTER).SetFont(CGXFont().SetSize(20).SetBold(TRUE).SetWeight(1000)).SetValue("No setting"));
	}
	
	
	/*	
	m_pWndCurModelGrid->SetStyleRange(CGXRange(0, COL_MODEL_NAME),	CGXStyle().SetValue("이름"));
	m_pWndCurModelGrid->SetStyleRange(CGXRange(0, COL_MODEL_CREATOR),	CGXStyle().SetValue("작성자"));

#ifdef _FORMATION_
	//formatio시는 적용 모델명이 기록된다.
	m_pWndCurModelGrid->SetStyleRange(CGXRange(0, COL_MODEL_DESCRIPTION),	CGXStyle().SetValue("적용모델"));
#else
	m_pWndCurModelGrid->SetStyleRange(CGXRange(0, COL_MODEL_DESCRIPTION),	CGXStyle().SetValue("설명"));
#endif
	m_pWndCurModelGrid->SetStyleRange(CGXRange(0, COL_MODEL_PRIMARY_KEY),	CGXStyle().SetValue("Primary Key"));
	m_pWndCurModelGrid->SetStyleRange(CGXRange(0, COL_MODEL_EDIT_STATE),	CGXStyle().SetValue("Edit_State"));
	m_pWndCurModelGrid->SetStyleRange(CGXRange().SetCols(COL_MODEL_PRIMARY_KEY, COL_MODEL_EDIT_STATE),	CGXStyle().SetEnabled(FALSE));
	m_pWndCurModelGrid->SetStyleRange(CGXRange().SetCols(COL_MODEL_PRIMARY_KEY, COL_MODEL_EDIT_STATE), CGXStyle().SetControl(GX_IDS_CTRL_STATIC));
	m_pWndCurModelGrid->GetParam()->EnableUndo(TRUE);
	m_pWndCurModelGrid->GetParam()->SetSortRowsOnDblClk(TRUE);
	m_pWndCurModelGrid->SetStyleRange(CGXRange().SetCols(COL_MODEL_NAME, COL_MODEL_DESCRIPTION), CGXStyle().SetVertScrollBar(TRUE).SetWrapText(FALSE));
	*/	
	
	m_wndSelectTypeGrid.SubclassDlgItem(IDC_GRID_SELECT_TYPE, this);
	m_wndSelectTypeGrid.Initialize();
	m_wndSelectTypeGrid.SetRowCount(1);
	m_wndSelectTypeGrid.SetColCount(14);
	m_wndSelectTypeGrid.SetDefaultRowHeight(60);
	m_wndSelectTypeGrid.SetFrozenRows(1);
	m_wndSelectTypeGrid.SetRowHeight(0,0,0);
	m_wndSelectTypeGrid.SetColWidth(0,0,0);
	
	m_wndSelectTypeGrid.SetColWidth(1,2,60);
	m_wndSelectTypeGrid.SetColWidth(3,12,100);
	m_wndSelectTypeGrid.SetColWidth(13,14,60);	
	
	// Disable scrollbar
	m_wndSelectTypeGrid.SetScrollBarMode(SB_BOTH,gxnDisabled,TRUE);	
	//Use MemDc
	m_wndSelectTypeGrid.SetDrawingTechnique(gxDrawUsingMemDC);

	m_wndSelectTypeGrid.SetStyleRange(CGXRange(1,1), CGXStyle().SetHorizontalAlignment(DT_CENTER).SetFont(CGXFont().SetSize(20).SetBold(TRUE).SetWeight(1000)).SetValue("<<"));
	m_wndSelectTypeGrid.SetStyleRange(CGXRange(1,2), CGXStyle().SetHorizontalAlignment(DT_CENTER).SetFont(CGXFont().SetSize(20).SetBold(TRUE).SetWeight(1000)).SetValue("<"));	
	m_wndSelectTypeGrid.SetStyleRange(CGXRange(1,3), CGXStyle().SetHorizontalAlignment(DT_CENTER).SetFont(CGXFont().SetSize(20).SetBold(TRUE).SetWeight(1000)).SetValue("0"));
	m_wndSelectTypeGrid.SetStyleRange(CGXRange(1,4), CGXStyle().SetHorizontalAlignment(DT_CENTER).SetFont(CGXFont().SetSize(20).SetBold(TRUE).SetWeight(1000)).SetValue("1"));
	m_wndSelectTypeGrid.SetStyleRange(CGXRange(1,5), CGXStyle().SetHorizontalAlignment(DT_CENTER).SetFont(CGXFont().SetSize(20).SetBold(TRUE).SetWeight(1000)).SetValue("2"));
	m_wndSelectTypeGrid.SetStyleRange(CGXRange(1,6), CGXStyle().SetHorizontalAlignment(DT_CENTER).SetFont(CGXFont().SetSize(20).SetBold(TRUE).SetWeight(1000)).SetValue("3"));
	m_wndSelectTypeGrid.SetStyleRange(CGXRange(1,7), CGXStyle().SetHorizontalAlignment(DT_CENTER).SetFont(CGXFont().SetSize(20).SetBold(TRUE).SetWeight(1000)).SetValue("4"));
	m_wndSelectTypeGrid.SetStyleRange(CGXRange(1,8), CGXStyle().SetHorizontalAlignment(DT_CENTER).SetFont(CGXFont().SetSize(20).SetBold(TRUE).SetWeight(1000)).SetValue("5"));
	m_wndSelectTypeGrid.SetStyleRange(CGXRange(1,9), CGXStyle().SetHorizontalAlignment(DT_CENTER).SetFont(CGXFont().SetSize(20).SetBold(TRUE).SetWeight(1000)).SetValue("6"));
	m_wndSelectTypeGrid.SetStyleRange(CGXRange(1,10), CGXStyle().SetHorizontalAlignment(DT_CENTER).SetFont(CGXFont().SetSize(20).SetBold(TRUE).SetWeight(1000)).SetValue("7"));
	m_wndSelectTypeGrid.SetStyleRange(CGXRange(1,11), CGXStyle().SetHorizontalAlignment(DT_CENTER).SetFont(CGXFont().SetSize(20).SetBold(TRUE).SetWeight(1000)).SetValue("8"));
	m_wndSelectTypeGrid.SetStyleRange(CGXRange(1,12), CGXStyle().SetHorizontalAlignment(DT_CENTER).SetFont(CGXFont().SetSize(20).SetBold(TRUE).SetWeight(1000)).SetValue("9"));
	m_wndSelectTypeGrid.SetStyleRange(CGXRange(1,13), CGXStyle().SetHorizontalAlignment(DT_CENTER).SetFont(CGXFont().SetSize(20).SetBold(TRUE).SetWeight(1000)).SetValue(">"));
	m_wndSelectTypeGrid.SetStyleRange(CGXRange(1,14), CGXStyle().SetHorizontalAlignment(DT_CENTER).SetFont(CGXFont().SetSize(20).SetBold(TRUE).SetWeight(1000)).SetValue(">>"));
	
	m_wndSelectTypeGrid.SetStyleRange(CGXRange().SetRows(1),
		CGXStyle().SetControl(GX_IDS_CTRL_STATIC).SetTextColor(RGB_BTN_FONT).SetInterior(RGB_BTN_BACKGROUND));	
}

void CModelSelDlg::InitColorBtn()
{
	m_Btn_Back.SetFontStyle(30,1);
	m_Btn_Back.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	m_Btn_Next.SetFontStyle(30,1);
	m_Btn_Next.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	m_Btn_Copy.SetFontStyle(30,1);
	m_Btn_Copy.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	m_Btn_Save.SetFontStyle(30,1);
	m_Btn_Save.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	m_Btn_Reset.SetFontStyle(30,1);
	m_Btn_Reset.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	m_Btn_Cancel.SetFontStyle(30,1);
	m_Btn_Cancel.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
}

void CModelSelDlg::InitFont()
{
	static CFont	font;
	LOGFONT			LogFont;

	GetDlgItem(IDC_EDIT_TYPENOTE)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight = 1000;
	LogFont.lfHeight = EDIT_FONT_SIZE;

	font.CreateFontIndirect( &LogFont );

	GetDlgItem(IDC_EDIT_TYPENOTE)->SetFont(&font);
}

void CModelSelDlg::ChangeTypeValue()
{
	CString strTemp = _T("");
	int i=0;
	int nPostion=3;
	
	for( i=0; i<10; i++ )
	{
		nPostion = i + 3;
		strTemp.Format("%ld", m_nType[i] );		
		m_wndSelectTypeGrid.SetStyleRange(CGXRange(1,nPostion), CGXStyle().SetValue(strTemp));
	}
}

void CModelSelDlg::ChangeProcessInfo()
{
	CString strTemp = _T("");
	int i=0;

	for( i=0; i<10; i++ )
	{	
		strTemp.Format("%ld", m_nProcess[i]);
		m_wndBatteryModelGrid.SetStyleRange(CGXRange(i+1,1), CGXStyle().SetValue(strTemp));
		m_wndBatteryModelGrid.SetStyleRange(CGXRange(i+1,2), CGXStyle().SetTextColor(RGB_BLACK).SetValue(NODEVICE));
		m_wndBatteryModelGrid.SetStyleRange(CGXRange(i+1,3), CGXStyle().SetValue(""));
		strTemp.Format("%ld", m_nProcess[i+10]);
		m_wndBatteryModelGrid.SetStyleRange(CGXRange(i+1,4), CGXStyle().SetValue(strTemp));		
		m_wndBatteryModelGrid.SetStyleRange(CGXRange(i+1,5), CGXStyle().SetTextColor(RGB_BLACK).SetValue(NODEVICE));
		m_wndBatteryModelGrid.SetStyleRange(CGXRange(i+1,6), CGXStyle().SetValue(""));
	}
	
	// 1. TestID 정보를 가져와서 화면에 표시	
	int nTotModelNum = 0;
	CTestListRecordSet	recordSet;

	recordSet.m_strFilter.Format("[ModelID] = %ld AND [TestID] >= %ld AND [TestID] <= %ld", m_nSelectType, m_nProcess[0], m_nProcess[19] );
	recordSet.m_strSort.Format("[TestID]");	

	try
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return;
	}

	if(recordSet.IsBOF())
	{
		nTotModelNum = 0;
	}
	else
	{
		recordSet.MoveLast();
		nTotModelNum = recordSet.GetRecordCount();
		recordSet.MoveFirst(); 
	}	
	
	bool bFound = false;
	while(!recordSet.IsEOF())
	{
		for( i=0; i<20; i++ )
		{
			if( m_nProcess[i] == recordSet.m_TestID )
			{
				bFound = true;			
				break;
			}
		}
		
		if( bFound )
		{				
			if( i < 10 )
			{
				m_wndBatteryModelGrid.SetStyleRange(CGXRange(i+1, 2),	CGXStyle().SetTextColor(RGB_RED).SetValue(recordSet.m_TestName));
				m_wndBatteryModelGrid.SetStyleRange(CGXRange(i+1, 3),	CGXStyle().SetValue(recordSet.m_ProcTypeID));
			}
			else
			{
				m_wndBatteryModelGrid.SetStyleRange(CGXRange(i-9, 5),	CGXStyle().SetTextColor(RGB_RED).SetValue(recordSet.m_TestName));
				m_wndBatteryModelGrid.SetStyleRange(CGXRange(i-9, 6),	CGXStyle().SetValue(recordSet.m_ProcTypeID));						
			}
		}
		bFound = false;		
		recordSet.MoveNext();
	}
	recordSet.Close();
}

void CModelSelDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	((CCTSEditorApp *)AfxGetApp())->OnAppExit();

}

LONG CModelSelDlg::OnGridDbClicked(WPARAM wParam, LPARAM lParam)
{
	CString strTemp = _T("");	
	int i=0;
	ROWCOL nRow, nCol;
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;				//Get Grid	
	nCol = LOWORD(wParam);		
	nRow = HIWORD(wParam);	//Get Current Row Col	
	
	if( pGrid == &m_wndSelectTypeGrid )
	{
		if(nCol > 2 && nCol < 13 )
		{	
			m_nSelectType = atoi(pGrid->GetValueRowCol(nRow, nCol));
			strTemp.Format("Type %d", m_nSelectType);
			m_LabelSelectType.SetText(strTemp);
			
			// 1. Type 변경에 따른 Process 정보 초기화 진행
			// 2. 화면 갱신
			for(i=0;i<20;i++)
			{
				m_nProcess[i] = i;
			}
			
			if( RequeryBatteryModel(m_nSelectType) == true )
			{
				ChangeProcessInfo();	
			}				
		}				
	}
	return 0;
}

LONG CModelSelDlg::OnGridClicked(WPARAM wParam, LPARAM lParam)
{
	ROWCOL nRow, nCol;			
	CString strTemp = _T("");
	int i = 0;
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;				//Get Grid	
	nCol = LOWORD(wParam);		
	nRow = HIWORD(wParam);	//Get Current Row Col	

	if( pGrid == &m_wndSelectTypeGrid )
	{		
		if(nCol > 2 && nCol < 13 )
		{
			
		}
		else
		{				
			if( nCol == 1 )	// "<<"
			{
				if( m_nType[0] >= 100 )
				{
					for( i=0; i<10; i++ )
					{
						m_nType[i] -= 100;
					}

					ChangeTypeValue();
				}				
			}
			else if( nCol == 2 )	// "<"
			{
				if( m_nType[0] >= 10 )
				{
					for( i=0; i<10; i++ )
					{
						m_nType[i] -= 10;
					}

					ChangeTypeValue();
				}
			}
			else if( nCol == 13 )	// ">"
			{
				if( m_nType[0] < 990 )
				{
					for( i=0; i<10; i++ )
					{
						m_nType[i] += 10;
					}

					ChangeTypeValue();
				}
			}
			else if( nCol == 14 )	// ">>"
			{
				if( m_nType[0] < 900 )
				{
					for( i=0; i<10; i++ )
					{
						m_nType[i] += 100;
					}

					ChangeTypeValue();
				}
			}
		
		}		
	}
	else if( pGrid == &m_wndBatteryModelGrid )
	{
		if( nRow > 0 && ( nCol == 2 || nCol == 5 ))
		{
			int nTestID = nRow-1;
			if( nCol == 5 )
			{
				nTestID = nTestID + 10;
			}
			
			m_nSelectProcess = m_nProcess[nTestID];
		
			CTestNameDlg dlg;			
			dlg.m_lTestTypeID = atol(m_wndBatteryModelGrid.GetValueRowCol( nRow, nCol+1));
			dlg.m_nProcess = m_nProcess[nTestID];
			
			if(dlg.DoModal() != IDOK)
				return 1;
			
			if( dlg.m_bProcessSetting == true )
			{					
				// 1. 테스트 이름을 선택하면 공정 편집하면을 바로 실행하고
				// 2. 테스트 이름을 선택하지 않으면 기존에 저장된 데이터를 삭제한다.	
				m_wndBatteryModelGrid.SetStyleRange(CGXRange(nRow, nCol),	CGXStyle().SetTextColor(RGB_RED).SetValue(dlg.m_strName));			
				m_wndBatteryModelGrid.SetStyleRange(CGXRange(nRow, nCol+1),	CGXStyle().SetTextColor(RGB_RED).SetValue(dlg.m_lTestTypeID));			
				
				WPARAM value = ((WPARAM)(m_nSelectProcess) << 16) | ((WPARAM)(m_nSelectType));
				((CCTSEditorView *)m_pView)->PostMessage(WM_GRID_MOVECELL, (WPARAM) value, NULL);

				OnTestSave( m_nSelectType, m_nProcess[nTestID], dlg.m_lTestTypeID, dlg.m_strName);

				AfxGetMainWnd()->ShowWindow(SW_SHOW);
				ShowWindow(SW_HIDE);
			}
			else
			{
				m_wndBatteryModelGrid.SetStyleRange(CGXRange(nRow, nCol),	CGXStyle().SetTextColor(RGB_BLACK).SetValue(dlg.m_strName));
				m_wndBatteryModelGrid.SetStyleRange(CGXRange(nRow, nCol+1),	CGXStyle().SetTextColor(RGB_BLACK).SetValue(dlg.m_lTestTypeID));
				
				DeleteProcess( m_nSelectType, m_nProcess[nTestID]);
			}
		}
		else if( nRow > 0 && ( nCol == 1 || nCol == 4 ))
		{
			int nTestID = nRow-1;
			if( nCol == 4 )
			{
				nTestID = nTestID + 10;
			}
			
			long nProcType = atol(m_wndBatteryModelGrid.GetValueRowCol( nRow, nCol+2));
			if( nProcType != 0 )
			{
				WPARAM value = ((WPARAM)(m_nProcess[nTestID]) << 16) | ((WPARAM)(m_nSelectType));
				((CCTSEditorView *)m_pView)->PostMessage(WM_GRID_MOVECELL, (WPARAM) value, NULL);

				OnTestSave( m_nSelectType, m_nProcess[nTestID], nProcType, m_wndBatteryModelGrid.GetValueRowCol( nRow, nCol+1));

				AfxGetMainWnd()->ShowWindow(SW_SHOW);
				ShowWindow(SW_HIDE);	
			}
		}
	}
	
	return 0;
}

void CModelSelDlg::OnTestSave(LONG lModelID, LONG lTestId, LONG nProcType, CString strTestName) 
{	
	// TODO: Add your control notification handler code here
	CString		strTemp, strState;
	CTestListRecordSet	recordSet;		

	ROWCOL i = 0;
	int nLastEditedModel = 0;

	try
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return ;
	}
	
	strTemp.Format("TestID = %ld AND ModelID = %ld", lTestId, lModelID);
	if(recordSet.Find(AFX_DAO_FIRST, strTemp))	//Find Model Name Data54
	{
		recordSet.Edit();
	}
	else
	{
		recordSet.AddNew();			
	}

	recordSet.m_TestID		= lTestId;
	recordSet.m_TestNo		= lTestId; 
	recordSet.m_ModelID		= lModelID;
	recordSet.m_ProcTypeID  = nProcType;
	recordSet.m_TestName    = strTestName;
	recordSet.Update();
	recordSet.Close();
		
		
		/*
	recordSet.m_strFilter.Format("[ModelID] = %ld", lModelID);
	recordSet.m_strSort.Format("[TestNo]");

	i = 0;
	recordSet.Requery();
	if(!recordSet.IsBOF())
	{
		recordSet.MoveLast();
		i = recordSet.GetRecordCount();
		recordSet.MoveFirst();
	}
	nRow -= i;
	
	if(nRow>0)
	{
		m_wndTestListGrid.RemoveRows(i+1, i+nRow);		//Row가 RecordSet  보다 많으면 잉여 Row를 Delete
	}
	else if(nRow<0)
	{
		m_wndTestListGrid.InsertRows(i+1, -nRow);		//Row가 RescoedSet 보다 작으면 부족 Row Insert
	}
	
	nRow = 0;
	while(!recordSet.IsEOF())
	{	
		nRow++;
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_PROC_TYPE), (ROWCOL)GetTestIndexFromType(recordSet.m_ProcTypeID));
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_NAME), recordSet.m_TestName);
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_CREATOR), recordSet.m_Creator);
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_DESCRIPTION), recordSet.m_Description);
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_EDIT_TIME), recordSet.m_ModifiedTime.Format());
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_PRIMARY_KEY), recordSet.m_TestID);
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_EDIT_STATE), CS_SAVED);
	
		if(nRow == nLastEditedModel)				//Last Edited Battery Model is current selected Model
		{
			UpdateDspTest(recordSet.m_TestName,  recordSet.m_TestID);
			m_wndTestListGrid.SetCurrentCell(nRow, COL_TEST_PROC_TYPE);
		}
		recordSet.MoveNext();
	}
	
	recordSet.Close();
	strTemp.Format("%d", nRow);
	m_TotTestCount.SetText(strTemp);
	
	if(m_wndTestListGrid.GetCurrentCell(nRow, nCol))
	{
		if(nRow>0)
			GetDlgItem(IDC_LOAD_TEST)->EnableWindow(TRUE);
	}
	GetDlgItem(IDC_TEST_SAVE)->EnableWindow(FALSE);		
	GetDocument()->m_bEditedFlag = TRUE;					//DataBase 변경 Flag (BackUp을 위해)
	*/
}

void CModelSelDlg::InitLabel()
{
	m_Label1.SetBkColor(RGB_MIDNIGHTBLUE)
		.SetTextColor(RGB_WHITE)
		.SetFontSize(24)
		.SetFontBold(TRUE)
		.SetText("Type Setting");
		
	m_LabelSelectType.SetFontSize(20)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND_STAGENAME)
		.SetFontBold(TRUE)
		.SetFontName("HY헤드라인M")
		.SetText("-");
		
	m_Label2.SetBkColor(RGB_MIDNIGHTBLUE)
		.SetTextColor(RGB_WHITE)
		.SetFontSize(24)
		.SetFontBold(TRUE)
		.SetText("Note");
}

BOOL CModelSelDlg::SelectOK()
{
	return TRUE;
}

void CModelSelDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}

//Load Battery Model Information from data base.
BOOL CModelSelDlg::RequeryBatteryModel( LONG lModelID, bool bSave )
{	
	UpdateData(TRUE);
	
	CBatteryModelRecordSet	recordSet;
	bool bFindModel = false;

	//사용자에 맞는 모델만 Loading	
	recordSet.m_strFilter.Format("[ModelID] = %ld", lModelID);

	try
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return false;
	}
	
	if(recordSet.IsBOF())
	{	
		m_strTypeNote = _T("");		
		recordSet.AddNew();
	}
	else 
	{	
		recordSet.Edit();		
		
		if( bSave == false )
		{
			m_strTypeNote = recordSet.m_Description;
		}
	}
	
	recordSet.m_ModelID = lModelID;
	recordSet.m_Description = m_strTypeNote;
	
	recordSet.Update();
	recordSet.Close();
	
	UpdateData( FALSE );
	return true;
}

BOOL CModelSelDlg::RequeryTestList(LONG lModelID )
{
/*
	int nTotModelNum = 0;
	CTestListRecordSet	recordSet;

	recordSet.m_strFilter.Format("[ModelID] = %ld", lModelID);
	recordSet.m_strSort.Format("[TestNo]");

	try
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}

	ROWCOL nRow = m_wndTestListGrid.GetRowCount();

	if(recordSet.IsBOF())
	{
		nTotModelNum = 0;
	}
	else
	{
		recordSet.MoveLast();
		nTotModelNum = recordSet.GetRecordCount();
		recordSet.MoveFirst(); 
	}

	nTotModelNum -= nRow;

	if(nTotModelNum > 0)
	{	
		m_wndTestListGrid.InsertRows(nRow+1, nTotModelNum);
	}
	else if(nTotModelNum <0)
	{
		m_wndTestListGrid.RemoveRows(nRow+nTotModelNum+1, nRow);
	}

	nRow = 0;
	int nSelRow = 0;
	int nSelID  = 0;
	CString strSelName;

	while(!recordSet.IsEOF())
	{	
		nRow++;
		//if(strDefaultName == recordSet.m_TestName)
		if(strDefaultName == recordSet.m_Description)
		{
			nSelRow = nRow;
			nSelID  = recordSet.m_TestID;
			//strSelName = recordSet.m_TestName;
			strSelName = recordSet.m_Description;
		}
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_PROC_TYPE), (ROWCOL)GetTestIndexFromType(recordSet.m_ProcTypeID));	
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_NAME), recordSet.m_TestName);
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_CREATOR), recordSet.m_Creator);
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_DESCRIPTION), recordSet.m_Description);
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_EDIT_TIME), recordSet.m_ModifiedTime.Format());
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_PRIMARY_KEY), recordSet.m_TestID);
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_RESET_PROC), (long)recordSet.m_PreTestCheck);
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_EDIT_STATE), CS_SAVED);
		recordSet.MoveNext();
	}

	if(nSelRow == 0)
	{
		if(nRow >0)	//가장 마지막 Recoedset을 보여 준다.
		{
			recordSet.MovePrev();
			nSelID = recordSet.m_TestID;
			strSelName = recordSet.m_TestName;
			nSelRow = nRow;
		}
	}
	recordSet.Close();

	//default 
	UpdateDspTest(strSelName, nSelID);
	m_wndTestListGrid.SetCurrentCell(nSelRow, 1);

	CString strTemp;
	strTemp.Format("%d", nRow);
	m_TotTestCount.SetText(strTemp);
*/
	return TRUE;
}

/*
void CModelSelDlg::OnButtonNew() 
{
	// TODO: Add your control notification handler code here
	((CCTSEditorView *)m_pView)->NewModel();
}
void CModelSelDlg::OnButtonEdit() 
{
	// TODO: Add your control notification handler code here
	((CCTSEditorView *)m_pView)->ModelEdit();
}
void CModelSelDlg::OnButtonDelete() 
{
	// TODO: Add your control notification handler code here
	((CCTSEditorView *)m_pView)->ModelDelete();
}
void CModelSelDlg::OnOK() 
{
	// TODO: Add extra validation here
	SelectOK();
//	CDialog::OnOK();
}
*/
void CModelSelDlg::OnBnClickedButtonBack()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int i = 0;
	if( m_nProcess[0] >= 20 )
	{
		for( i=0; i<20; i++ )
		{
			m_nProcess[i] -= 20;
		}

		ChangeProcessInfo();
	}
}

void CModelSelDlg::OnBnClickedButtonNext()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int i = 0;
	if( m_nProcess[0] < 180 )
	{
		for( i=0; i<20; i++ )
		{
			m_nProcess[i] += 20;
		}

		ChangeProcessInfo();
	}			
}

void CModelSelDlg::OnButtonCopy() 
{
	// TODO: Add your control notification handler code here
	CTypeCopyDlg dlg( m_nSelectType );
	
	if( dlg.DoModal() == IDOK )
	{
		long lTypeToCopy = atol(dlg.m_strTypeToCopy);
	
		if( DeleteTypeList( lTypeToCopy ) )
		{
			if( CopyModel( m_nSelectType, lTypeToCopy ) == true )
			{
				if( CopyTestList( m_nSelectType, lTypeToCopy ) == true )
				{
					if(CopyCheckParam( m_nSelectType, lTypeToCopy ) == true )
					{
						if( CopyStepList( m_nSelectType, lTypeToCopy ) == true )
						{
						
						}
					}				
				}
			}
		}
	}	
	return;
}

void CModelSelDlg::OnButtonSave() 
{
	// TODO: Add your control notification handler code here
	RequeryBatteryModel( m_nSelectType, true );
	return;
}

void CModelSelDlg::OnBnClickedButtonReset()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.	
	CString strTemp;
	strTemp.Format(TEXT_LANG[0], m_nSelectType);//"Type [%d] 정보를 삭제 하시겠습니까?"

	if(IDNO == MessageBox(strTemp, "TYPE DELETE", MB_YESNO|MB_ICONQUESTION))	
	{
		return;
	}
	
	DeleteTypeList( m_nSelectType );	
}


BOOL CModelSelDlg::DeleteTypeList( long lType )
{	
	CString strTemp, strSQL1, strSQL2, strSQL3, strSQL4;

	CBatteryModelRecordSet	recordSet;

	CDaoDatabase  db;

	try
	{
		db.Open(g_strDataBaseName);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return false;
	}

	strSQL1.Format("Delete from BatteryModel where ModelID = %ld ", lType );	
	strSQL2.Format("Delete from TestName where ModelID = %ld ", lType );
	strSQL3.Format("Delete from Step where ModelID = %ld", lType);
	strSQL4.Format("Delete from Check where ModelID = %ld", lType);

	try
	{
		db.Execute(strSQL1);

		db.Execute(strSQL2);

		db.Execute(strSQL3);
		
		db.Execute(strSQL4);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return false;	
	}

	db.Close();

	// 1. Type 정보 삭제 후 해당 정보를 Update 한다.
	if( RequeryBatteryModel(lType) == true )
	{
		ChangeProcessInfo();	
	}
	else
	{
		return false;
	}
	
	return true;
}

BOOL CModelSelDlg::DeleteProcess( long lType, long lProcess)
{
	CString strTemp, strSQL1, strSQL2, strSQL3, strSQL4;

	CBatteryModelRecordSet	recordSet;

	CDaoDatabase  db;

	try
	{
		db.Open(g_strDataBaseName);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return false;
	}

	strSQL2.Format("Delete from TestName where ModelID = %ld AND TestID = %ld", lType, lProcess );
	strSQL3.Format("Delete from Step where ModelID = %ld AND TestID = %ld", lType, lProcess);
	strSQL4.Format("Delete from Check where ModelID = %ld AND TestID = %ld", lType, lProcess);

	try
	{
		db.Execute(strSQL2);

		db.Execute(strSQL3);

		db.Execute(strSQL4);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return false;	
	}

	db.Close();

	// 1. Type 정보 삭제 후 해당 정보를 Update 한다.
	if( RequeryBatteryModel(lType) == true )
	{
		ChangeProcessInfo();	
	}
	else
	{
		return false;
	}

	return true;
}

BOOL CModelSelDlg::CopyModel(long lFromModelID, long lToModelID)
{
	UpdateData(TRUE);

	bool bFindModel = false;
	
	CDaoDatabase  db;
	
	db.Open(g_strDataBaseName);
	
	CString name, descript, creator;
	CString strSQL;
	bool bFindData = false;
	strSQL.Format("SELECT ModelName, Description, Creator FROM BatteryModel WHERE ModelID = %d", lFromModelID);
	COleVariant data;
	CDaoRecordset rs(&db);
	rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
	
	while(!rs.IsEOF())
	{
		bFindData = true;
		data = rs.GetFieldValue(0);	    name = data.pbVal;
		data = rs.GetFieldValue(1);		descript = data.pbVal; 
		data = rs.GetFieldValue(2);		creator = data.pbVal;
		rs.MoveNext();
	}
	
	rs.Close();
	
	if( bFindData )
	{
		COleDateTime createTime;
		createTime = COleDateTime::GetCurrentTime();

		strSQL.Format("UPDATE BatteryModel SET ModelName = '%s', Description = '%s', CreatedTime = '%s', Creator = '%s' WHERE ModelID = %ld",
			name, descript, createTime.Format(), creator, lToModelID );

		db.Execute(strSQL);	
		db.Close();	
	}
	return TRUE;
}

//TestName 목록 복사 
BOOL CModelSelDlg::CopyTestList(long lFromModelID, long lToModelID)
{
	CDaoDatabase  db;
	
	db.Open(g_strDataBaseName);

	CString name, descript, creator;
	long check, typeID, testNo, lTestID, lFromTestID, lModelID;
	COleDateTime modtime;

	CString strSQL;
	strSQL.Format("SELECT TestID, ModelID, TestName, Description, Creator, PreTestCheck, ProcTypeID, TestNo, ModifiedTime FROM TestName WHERE ModelID = %d ORDER BY TestNo", 
					lFromModelID);
					
	COleVariant data;
	CDaoRecordset rs(&db);
	rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
	while(!rs.IsEOF())
	{
		data = rs.GetFieldValue(0);		lFromTestID = data.lVal;
		data = rs.GetFieldValue(1);	    lModelID = data.lVal;
		data = rs.GetFieldValue(2);		name = data.pbVal;
		data = rs.GetFieldValue(3);		descript = data.pbVal;
		data = rs.GetFieldValue(4);		creator = data.pbVal;
		data = rs.GetFieldValue(5);		check = data.lVal;
		data = rs.GetFieldValue(6);		typeID = data.lVal;
		data = rs.GetFieldValue(7);		testNo = data.lVal;
		modtime = COleDateTime::GetCurrentTime();		
		
		strSQL.Format("INSERT INTO TestName(TestID, ModelID, TestName, Description, Creator, PreTestCheck, ProcTypeID, TestNo, ModifiedTime) VALUES (%d, %d, '%s', '%s', '%s', %d, %d, %d, '%s')",
			lFromTestID, lToModelID, name, descript, creator, check, typeID, testNo, modtime.Format());

		db.Execute(strSQL);		
		rs.MoveNext();
	}
	rs.Close();
	db.Close();
	return TRUE;
}

BOOL CModelSelDlg::CopyStepList(long lFromModelID, long lToModelID)
{	
	CDaoDatabase  db;
	db.Open(g_strDataBaseName);

	CString strSQL;
	CStepRecordSet rs, rs1;
	rs.m_strFilter.Format("[ModelID] = %ld", lFromModelID);
	rs.m_strSort = "[StepNo]";

	rs.Open();
	rs1.Open();

	while(!rs.IsEOF())
	{
		rs1.AddNew();					
		rs1.m_ModelID = lToModelID;
		rs1.m_TestID = rs.m_TestID;
		rs1.m_StepNo = rs.m_StepNo;
		rs1.m_StepProcType = rs.m_StepProcType;
		rs1.m_StepType = rs.m_StepType;
		rs1.m_StepMode = rs.m_StepMode;
		rs1.m_Vref = rs.m_Vref;
		rs1.m_Iref = rs.m_Iref;
		rs1.m_EndTime = rs.m_EndTime;
		rs1.m_EndV = rs.m_EndV;
		rs1.m_EndI = rs.m_EndI;		
		rs1.m_EndCapacity = rs.m_EndCapacity;
		rs1.m_End_dV = rs.m_End_dV;
		rs1.m_End_dI = rs.m_End_dI;
		rs1.m_OverV = rs.m_OverV;
		rs1.m_LimitV = rs.m_LimitV;
		rs1.m_OverI = rs.m_OverI;
		rs1.m_LimitI = rs.m_LimitI;
		rs1.m_OverCapacity = rs.m_OverCapacity;
		rs1.m_LimitCapacity = rs.m_LimitCapacity;
		rs1.m_OverImpedance = rs.m_OverImpedance;
		rs1.m_LimitImpedance = rs.m_LimitImpedance;
		rs1.m_DeltaTime = rs.m_DeltaTime;
		rs1.m_DeltaV = rs.m_DeltaV;
		rs1.m_DeltaTime1 = rs.m_DeltaTime1;
		rs1.m_DeltaI = rs.m_DeltaI;
		rs1.m_Grade = rs.m_Grade;
		rs1.m_CompTimeV1 = rs.m_CompTimeV1;
		rs1.m_CompTimeV2 = rs.m_CompTimeV2;		//30
		rs1.m_CompTimeV3 = rs.m_CompTimeV3;
		rs1.m_CompVLow1 = rs.m_CompVLow1;
		rs1.m_CompVLow2 = rs.m_CompVLow2;
		rs1.m_CompVLow3 = rs.m_CompVLow3;
		rs1.m_CompVHigh1 = rs.m_CompVHigh1;
		rs1.m_CompVHigh2 = rs.m_CompVHigh2;
		rs1.m_CompVHigh3 = rs.m_CompVHigh3;
		rs1.m_CompTimeI1 = rs.m_CompTimeI1;
		rs1.m_CompTimeI2 = rs.m_CompTimeI2;
		rs1.m_CompTimeI3 = rs.m_CompTimeI3;		//40
		rs1.m_CompILow1 = rs.m_CompILow1;
		rs1.m_CompILow2 = rs.m_CompILow2;
		rs1.m_CompILow3 = rs.m_CompILow3;
		rs1.m_CompIHigh1 = rs.m_CompIHigh1;
		rs1.m_CompIHigh2 = rs.m_CompIHigh2;
		rs1.m_CompIHigh3 = rs.m_CompIHigh3;
		rs1.m_RecordTime	= rs.m_RecordTime;
		rs1.m_RecordVIGetTime = rs.m_RecordVIGetTime;
		rs1.m_RecordDeltaV	= rs.m_RecordDeltaV;		//50
		rs1.m_RecordDeltaI	= rs.m_RecordDeltaI;
		rs1.m_CapVLow		= rs.m_CapVLow;
		rs1.m_CapVHigh		= rs.m_CapVHigh;			
		rs1.m_EndCheckVLow  = rs.m_EndCheckVLow ;
		rs1.m_EndCheckVHigh = rs.m_EndCheckVHigh ;
		rs1.m_EndCheckILow  = rs.m_EndCheckILow ;
		rs1.m_EndCheckIHigh	= rs.m_EndCheckIHigh;
		rs1.m_strGotoValue  = rs.m_strGotoValue;
		rs1.m_strReportTemp = rs.m_strReportTemp;
		rs1.m_strSocEnd  = rs.m_strSocEnd;
		rs1.m_strEndWatt  = rs.m_strEndWatt;
		rs1.m_strTempLimit  = rs.m_strTempLimit;		
		rs1.m_strSimulFile = rs.m_strSimulFile;
		rs1.m_strValueLimit = rs.m_strValueLimit;
		rs1.m_strEndVGotoValue  = rs.m_strEndVGotoValue;
		rs1.m_strEndTGotoValue  = rs.m_strEndTGotoValue; //rs1.m_Value8  = rs.m_Value8;
		rs1.m_strEndCGotoValue  = rs.m_strEndCGotoValue; //rs1.m_Value9  = rs.m_Value9;
		
		rs1.m_fDCIR_RegTemp = rs.m_fDCIR_RegTemp;
		rs1.m_fDCIR_ResistanceRate = rs.m_fDCIR_ResistanceRate;

		rs1.m_fCompChgCcVtg  = rs.m_fCompChgCcVtg;
		rs1.m_ulCompChgCcTime  = rs.m_ulCompChgCcTime;
		rs1.m_fCompChgCcDeltaVtg  = rs.m_fCompChgCcDeltaVtg;
		rs1.m_fCompChgCvCrt  = rs.m_fCompChgCvCrt;
		rs1.m_ulCompChgCvtTime  = rs.m_ulCompChgCvtTime;
		rs1.m_fCompChgCvDeltaCrt  = rs.m_fCompChgCvDeltaCrt;
		
		rs1.Update();		
		rs.MoveNext();
	}
	rs.Close();
	rs1.Close();
	
	db.Close();
	return TRUE;
}

BOOL CModelSelDlg::CopyCheckParam(long lFromModelID, long lToModelID)
{
	//	CCTSEditorDoc *pDoc =  GetDocument();

	CPreTestCheckRecordSet rs, rs1;
	rs.m_strFilter.Format("[ModelID] = %ld", lFromModelID); 
	rs.m_strSort = "[CheckID]";
	
	rs.Open();
	rs1.Open();

	while(!rs.IsEOF())
	{
		rs1.AddNew();					
		rs1.m_ModelID = lToModelID;
		
		rs1.m_TestID = rs.m_TestID;
		rs1.m_MaxV = rs.m_MaxV;
		rs1.m_MinV = rs.m_MinV;
		rs1.m_CurrentRange = rs.m_CurrentRange;
		rs1.m_OCVLimit = rs.m_OCVLimit;
		rs1.m_TrickleCurrent = rs.m_TrickleCurrent;
		rs1.m_TrickleTime = rs.m_TrickleTime;
		rs1.m_DeltaVoltage = rs.m_DeltaVoltage;
		rs1.m_MaxFaultBattery = rs.m_MaxFaultBattery;
		rs1.m_AutoTime = rs.m_AutoTime;
		rs1.m_AutoProYN = rs.m_AutoProYN;
		rs1.m_PreTestCheck = rs.m_PreTestCheck;
		
		rs1.Update();		
		rs.MoveNext();
	}

	rs1.Close();
	rs.Close();
	return TRUE;
}

BOOL CModelSelDlg::CopyGradeStep(long lFromModelID, long lToModelID)
{
	CGradeRecordSet rs, rs1;
	rs.m_strFilter.Format("[ModelID] = %ld", lFromModelID);
	rs.m_strSort = "[GradeID]";

	rs.Open();
	rs1.Open();

	while(!rs.IsEOF())
	{
		rs1.AddNew();
		rs1.m_ModelID = lToModelID;		
		rs1.m_StepID = rs.m_StepID;
		rs1.m_GradeItem = rs.m_GradeItem;
		rs1.m_Value = rs.m_Value;
		rs1.m_Value1 = rs.m_Value1;
		rs1.m_GradeCode = rs.m_GradeCode;
		rs1.Update();
		rs.MoveNext();
	}
	rs.Close();
	rs1.Close();
	return TRUE;
}