// TextParsing.cpp: implementation of the CTextParsing class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TextParsing.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define START_SIMULATION_POINT "STX"

#define WM_SET_RANGE		WM_USER + 100
#define WM_STEP_IT			WM_USER + 101
#define WM_PROGRESS_SHOW	WM_USER + 102
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTextParsing::CTextParsing()
{
	m_nRecordCount = 0;
	m_nColumnSize = 0;
	m_ppData = NULL;
	
	LanguageinitMonConfig();
}

CTextParsing::~CTextParsing()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
	Clear();
}

bool CTextParsing::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CTextParsing"), _T("TEXT_CTextParsing_CNT"), _T("TEXT_CTextParsing_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CTextParsing_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CTextParsing"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}


BOOL CTextParsing::SetFile(CString strFileName)
{
	Clear();

	CString strTemp, str;
	CStdioFile aFile;
	CFileException e;

	m_strFileName = strFileName;
	if( !aFile.Open( strFileName, CFile::modeRead|CFile::shareDenyWrite, &e ) )
	{
		#ifdef _DEBUG
		   afxDump << "File could not be opened " << e.m_cause << "\n";
		#endif
		   return FALSE;
	}

	CStringList		strDataList;
	while(aFile.ReadString(strTemp))
	{
		if(!strTemp.IsEmpty())	
		{
			strDataList.AddTail(strTemp);
		}
	}
	aFile.Close();
	
	m_nRecordCount = strDataList.GetCount()-1;
	if(m_nRecordCount < 0)		return FALSE;

	int p1=0, s=0;
	POSITION pos = strDataList.GetHeadPosition();

	strTemp = strDataList.GetNext(pos);		//Title
	while(p1!=-1)
	{
		p1 = strTemp.Find(',', s);
		if(p1!=-1)
		{
			str = strTemp.Mid(s, p1-s);
			m_TitleList.AddTail(str);
			s  = p1+1;
		}
	}
	str = strTemp.Mid(s);
	str.TrimLeft(' '); str.TrimRight(' ');
	if(str.IsEmpty() == FALSE)	m_TitleList.AddTail(str);

	m_nColumnSize = m_TitleList.GetCount();

	m_ppData = new float*[m_nColumnSize];
	for(int i =0; i<m_nColumnSize; i++)
	{
		m_ppData[i] = new float[m_nRecordCount];
	}
	
	CStringList strColList;
	int nRecord = 0;
	while(pos)
	{
		strTemp = strDataList.GetNext(pos);
		//////////////////////////////////////////////////////////////////////////
		p1=0, s=0;
		strColList.RemoveAll();
		while(p1!=-1)
		{
			p1 = strTemp.Find(',', s);
			if(p1!=-1)
			{
				str = strTemp.Mid(s, p1-s);
				strColList.AddTail(str);
				s  = p1+1;
			}
		}
		str = strTemp.Mid(s);
		str.TrimLeft(' '); str.TrimRight(' ');
		if(str.IsEmpty() == FALSE)	strColList.AddTail(str);

		//////////////////////////////////////////////////////////////////////////
		POSITION pos1 = strColList.GetHeadPosition();
		int nC = 0;
		while(pos1 && nC < m_nColumnSize)
		{
			str = strColList.GetNext(pos1);
			m_ppData[nC][nRecord] = atof(str);
			nC++;
		}
		nRecord++;
	}
	return TRUE;
}

float * CTextParsing::GetColumnData(int nColIndex)
{
	if(nColIndex < 0 || nColIndex >= m_nColumnSize)		return NULL;

	return m_ppData[nColIndex];
}

CString CTextParsing::GetColumnHeader(int nColIndex)
{
	if(nColIndex< 0 || nColIndex >= m_nColumnSize)		return "";

	POSITION pos = m_TitleList.GetHeadPosition();
	CString str;
	int nCnt = 0;
	while(pos)
	{
		str = m_TitleList.GetNext(pos);	
		if(nCnt == nColIndex)
		{
			break;
		}
		nCnt++;
	}
	return str;
}

BOOL CTextParsing::SetSimulationFile(CString strFileName, CString &strErrorMsg)
{
	CTimeSpan CheckTime;
	Clear();

	CString strTemp, str;
	CStdioFile aFile;
	CFileException e;

	CStringList strColList;

	ZeroMemory(&oldTime, sizeof(PSTimeSet));

	m_strFileName = strFileName;
	if( !aFile.Open( strFileName, CFile::modeRead|CFile::shareDenyWrite, &e ) )
	{
		#ifdef _DEBUG
		   afxDump << "File could not be opened " << e.m_cause << "\n";
		#endif
		   strErrorMsg.Format(TEXT_LANG[0], strFileName);//"%s 파일을 열 수 없습니다."
		   return FALSE;
	}

	CStringList		strDataList;
	while(aFile.ReadString(strTemp))
	{
		if(strTemp.Left(3) == START_SIMULATION_POINT)			//여기서부터 실제 Simulation Data -> 이 문자 위로는 주석문(처리안함)
		{
			int p1=0, s=0;
			//1. Title//////////////////////////////////////////////////////////////
			if(aFile.ReadString(strTemp))
			{
				while(p1!=-1)
				{
					p1 = strTemp.Find(',', s);
					if(p1!=-1)
					{
						str = strTemp.Mid(s, p1-s);
						m_TitleList.AddTail(str);
						s  = p1+1;
					}
				}
				str = strTemp.Mid(s);
				str.TrimLeft(' '); str.TrimRight(' ');
				if(str.IsEmpty() == FALSE)	m_TitleList.AddTail(str);
			}
			else
			{
				AfxMessageBox(TEXT_LANG[1]);//"파일 데이터가 올바르지 않습니다."
				return -1;
			}
			m_nColumnSize = m_TitleList.GetCount();
			
			////////////////////////////////////////////////////////////////////////

			while(aFile.ReadString(strTemp))
			{
				p1 = s = 0;
				while(p1!=-1)
				{
					p1 = strTemp.Find(',', s);
					if(p1!=-1)
					{
						str = strTemp.Mid(s, p1-s);
						CString strMakeTime;
						if(GetColumnHeader(0) == "T2")		//동작 시간일 경우 누적시간으로 변경한다.
							strMakeTime = GetStringTimePoint(str, TRUE);
						else
							strMakeTime = GetStringTimePoint(str);
						
						strColList.AddTail(strMakeTime);					
						s = p1+1;
					}
				}
				str = strTemp.Mid(s);
				str.TrimLeft(' '); str.TrimRight(' ');
				if(str.IsEmpty() == FALSE)	strColList.AddTail(str);
			}

		}	
	}
	
	
	aFile.Close();

	m_nRecordCount = strColList.GetCount() / 2;
	if(m_nRecordCount < 0)		
	{
		strErrorMsg.Format(TEXT_LANG[2]);//"데이터가 올바르지 않습니다."
		return FALSE;
	}

	m_ppData = new float*[m_nColumnSize];
	for(int i =0; i<m_nColumnSize; i++)
	{
		m_ppData[i] = new float[m_nRecordCount];
	}

	POSITION pos1 = strColList.GetHeadPosition();
	int nC = 0;
	int nRecord = 0;
	float fSec, fMs;
	CString strTemp1;
	int nIndex = 0;
	while(pos1 && nC < m_nColumnSize)
	{
		str = strColList.GetNext(pos1);
		nIndex = str.Find('.');
		fSec = atof(str.Left(nIndex));
		fMs = atof(str.Mid(nIndex, str.GetLength() - nIndex ));

		if(fSec >= 0.0f)
			m_ppData[nC][nRecord] = fSec+fMs;
		else
			m_ppData[nC][nRecord] = fSec-fMs;


		if(nC%m_nColumnSize == 0)
		{		
			TRACE("Time : %s ==> %.3f\r\n", str, m_ppData[nC][nRecord]);
			nC = 1;
		}
		else
		{
			TRACE("Value : %s ==> %.3f\r\n", str, m_ppData[nC][nRecord]);
			nRecord++;
			nC = 0;			
		}	
		
		
	}
	

	
/*	
	int nRecord = 0;
	while(pos)
	{
		strTemp = strDataList.GetNext(pos);
		//////////////////////////////////////////////////////////////////////////
		p1=0, s=0;
		strColList.RemoveAll();
		while(p1!=-1)
		{
			p1 = strTemp.Find(',', s);
			if(p1!=-1)
			{
				str = strTemp.Mid(s, p1-s);
				//str.TrimLeft(':'); str.TrimRight(':');
				int p2 = 0;
				int s2 = 0;
				nHour = nMin = nSec = nMs = -1;
				
				while(p2!= -1)
				{
					CString strTime;
					p2 = str.Find(':', s2);
					if(p2!= -1)
					{
						strTime = str.Mid(s2, p2-s2);
						s2 = p2 + 1;

						if(nHour < 0)
							nHour = atoi(strTime);
						else if(nMin < 0)
							nMin = atoi(strTime);
					}
					else
					{
						if(s2 < str.GetLength())
						{
							if(nHour < 0)
								nHour = 0;
							if(nMin < 0)
								nMin = 0;
							strTime = str.Mid(s2, str.Find('.'));
							if(strTime == "")
							{
								strTime = str.Mid(str.ReverseFind(':')+1, str.GetLength());								
							}
							nSec = atoi(strTime);
							int nMsFind1, nMsFind2;
							nMsFind1 = str.Find('.');
							nMsFind2 = str.GetLength() - str.Find('.');
							
							if(nMsFind1 < 0)
								strTime = "00";
							else
								strTime = str.Mid(nMsFind1,nMsFind2);
							
							float fTemp = atof(strTime);
							nMs = fTemp * 100;
						}
					}
				
				}
				//str.Remove(':');
				if(GetColumnHeader(0) == "T2")		//동작 시간일 경우 누적시간으로 변경한다.
				{
					nHour	+= nTempHour;
					nMin	+= nTempMin;
					nSec	+= nTempSec;
					nMs		+= nTempMs;

					if(nMs > 99)					//990ms 보다 클경우
					{
						nSec += 1;
						nMs -= 100;

					}
					if(nSec > 59)
					{
						nMin += 1;
						nSec -= 60;
					}
					if(nMin > 59)
					{
						nHour += 1;
						nMin -= 60;
					}
				}

				
				CTimeSpan ts(0, nHour, nMin,nSec);
				CTimeSpan ts2(0, nTempHour, nTempMin, nTempSec);
				if(ts.GetTotalSeconds() < ts2.GetTotalSeconds())
				{
					strErrorMsg.Format("시간 데이터가 올바르지 않습니다.%d:%d:%d / %d:%d:%d");
					return FALSE;
				}
				if(ts.GetTotalSeconds() == ts2.GetTotalSeconds())
				{
					if(nTempMs > nMs)
					{
						strErrorMsg.Format("시간 데이터가 올바르지 않습니다.%d:%d:%d / %d:%d:%d");
						return FALSE;
					}
					
				}
				
				str.Format("%d.%02d", ts.GetTotalSeconds(), nMs);
				strColList.AddTail(str);
				
				nTempHour	= nHour;
				nTempMin	= nMin;
				nTempSec	= nSec;
				nTempMs		= nMs;
				CheckTime = ts;
				s			= p1+1;
			}
		}
		str = strTemp.Mid(s);
		str.TrimLeft(' '); str.TrimRight(' ');
		if(str.IsEmpty() == FALSE)	strColList.AddTail(str);
		pWnd->SendMessage(WM_STEP_IT, m_nRecordCount, 0);

		//////////////////////////////////////////////////////////////////////////
		POSITION pos1 = strColList.GetHeadPosition();
		int nC = 0;
		while(pos1 && nC < m_nColumnSize)
		{
			str = strColList.GetNext(pos1);
			m_ppData[nC][nRecord] = atof(str);
			nC++;
		}
		nRecord++;
	}
	pWnd->SendMessage(WM_PROGRESS_SHOW, SW_HIDE, 0);*/
	return TRUE;
}
/*
BOOL CTextParsing::SetSimulationFile(CString strFileName, CString &strErrorMsg)
{
	int nTempHour, nTempMin, nTempSec, nTempMs;
	nTempHour = nTempMin = nTempSec = nTempMs = 0;

	int nHour, nMin, nSec, nMs;				
	nHour = nMin = nSec = nMs = -1;

	CTimeSpan CheckTime;
	Clear();

	CString strTemp, str;
	CStdioFile aFile;

	m_strFileName = strFileName;
	if( !aFile.Open( strFileName, CFile::modeRead|CFile::shareDenyWrite, &e ) )
	{
		#ifdef _DEBUG
		   afxDump << "File could not be opened " << e.m_cause << "\n";
		#endif
		   strErrorMsg.Format("%s 파일을 열 수 없습니다.", strFileName);
		   return FALSE;
	}

	CStringList		strDataList;
	while(aFile.ReadString(strTemp))
	{
		if(strTemp.Left(3) == START_SIMULATION_POINT)			//여기서부터 실제 Simulation Data -> 이 문자 위로는 주석문(처리안함)
		{
			while(aFile.ReadString(strTemp))
			{
				if(!strTemp.IsEmpty())	
				{
					strDataList.AddTail(strTemp);
				}
			}

		}	
	}

	
	aFile.Close();
	
	
	m_nRecordCount = strDataList.GetCount()-1;
	pWnd->SendMessage(WM_SET_RANGE, m_nRecordCount, 0);
	pWnd->SendMessage(WM_PROGRESS_SHOW, SW_SHOW, 0);
	if(m_nRecordCount < 0)		
	{
		strErrorMsg.Format("데이터가 올바르지 않습니다.");
		return FALSE;
	}

	int p1=0, s=0;
	POSITION pos = strDataList.GetHeadPosition();

	strTemp = strDataList.GetNext(pos);		//Title
	while(p1!=-1)
	{
		p1 = strTemp.Find(',', s);
		if(p1!=-1)
		{
			str = strTemp.Mid(s, p1-s);
			m_TitleList.AddTail(str);
			s  = p1+1;
		}
	}
	str = strTemp.Mid(s);
	str.TrimLeft(' '); str.TrimRight(' ');
	if(str.IsEmpty() == FALSE)	m_TitleList.AddTail(str);

	m_nColumnSize = m_TitleList.GetCount();

	m_ppData = new float*[m_nColumnSize];
	for(int i =0; i<m_nColumnSize; i++)
	{
		m_ppData[i] = new float[m_nRecordCount];
	}
	
	CStringList strColList;
	int nRecord = 0;
	while(pos)
	{
		strTemp = strDataList.GetNext(pos);
		//////////////////////////////////////////////////////////////////////////
		p1=0, s=0;
		strColList.RemoveAll();
		while(p1!=-1)
		{
			p1 = strTemp.Find(',', s);
			if(p1!=-1)
			{
				str = strTemp.Mid(s, p1-s);
				//str.TrimLeft(':'); str.TrimRight(':');
				int p2 = 0;
				int s2 = 0;
				nHour = nMin = nSec = nMs = -1;
				
				while(p2!= -1)
				{
					CString strTime;
					p2 = str.Find(':', s2);
					if(p2!= -1)
					{
						strTime = str.Mid(s2, p2-s2);
						s2 = p2 + 1;

						if(nHour < 0)
							nHour = atoi(strTime);
						else if(nMin < 0)
							nMin = atoi(strTime);
					}
					else
					{
						if(s2 < str.GetLength())
						{
							if(nHour < 0)
								nHour = 0;
							if(nMin < 0)
								nMin = 0;
							strTime = str.Mid(s2, str.Find('.'));
							if(strTime == "")
							{
								strTime = str.Mid(str.ReverseFind(':')+1, str.GetLength());								
							}
							nSec = atoi(strTime);
							int nMsFind1, nMsFind2;
							nMsFind1 = str.Find('.');
							nMsFind2 = str.GetLength() - str.Find('.');
							
							if(nMsFind1 < 0)
								strTime = "00";
							else
								strTime = str.Mid(nMsFind1,nMsFind2);
							
							float fTemp = atof(strTime);
							nMs = fTemp * 100;

	CFileException e;
						}
					}
				
				}
				//str.Remove(':');
				if(GetColumnHeader(0) == "T2")		//동작 시간일 경우 누적시간으로 변경한다.
				{
					nHour	+= nTempHour;
					nMin	+= nTempMin;
					nSec	+= nTempSec;
					nMs		+= nTempMs;

					if(nMs > 99)					//990ms 보다 클경우
					{
						nSec += 1;
						nMs -= 100;

					}
					if(nSec > 59)
					{
						nMin += 1;
						nSec -= 60;
					}
					if(nMin > 59)
					{
						nHour += 1;
						nMin -= 60;
					}
				}

				
				CTimeSpan ts(0, nHour, nMin,nSec);
				CTimeSpan ts2(0, nTempHour, nTempMin, nTempSec);
				if(ts.GetTotalSeconds() < ts2.GetTotalSeconds())
				{
					strErrorMsg.Format("시간 데이터가 올바르지 않습니다.%d:%d:%d / %d:%d:%d");
					return FALSE;
				}
				if(ts.GetTotalSeconds() == ts2.GetTotalSeconds())
				{
					if(nTempMs > nMs)
					{
						strErrorMsg.Format("시간 데이터가 올바르지 않습니다.%d:%d:%d / %d:%d:%d");
						return FALSE;
					}
					
				}
				
				str.Format("%d.%02d", ts.GetTotalSeconds(), nMs);
				strColList.AddTail(str);
				
				nTempHour	= nHour;
				nTempMin	= nMin;
				nTempSec	= nSec;
				nTempMs		= nMs;
				CheckTime = ts;
				s			= p1+1;
			}
		}
		str = strTemp.Mid(s);
		str.TrimLeft(' '); str.TrimRight(' ');
		if(str.IsEmpty() == FALSE)	strColList.AddTail(str);
		pWnd->SendMessage(WM_STEP_IT, m_nRecordCount, 0);

		//////////////////////////////////////////////////////////////////////////
		POSITION pos1 = strColList.GetHeadPosition();
		int nC = 0;
		while(pos1 && nC < m_nColumnSize)
		{
			str = strColList.GetNext(pos1);
			m_ppData[nC][nRecord] = atof(str);
			nC++;
		}
		nRecord++;
	}
	pWnd->SendMessage(WM_PROGRESS_SHOW, SW_HIDE, 0);
	return TRUE;
}
*/
void CTextParsing::SetParent(CWnd *pWnd)
{
	this->pWnd = pWnd;
}

void CTextParsing::Clear()
{
	if(m_ppData)
	{
		for(int i=0; i<m_nColumnSize; i++)
		{
			if(m_ppData[i] != NULL)
			{
				delete[] m_ppData[i];
				m_ppData[i] = NULL;
			}
		}
		delete[]	m_ppData; 
		m_ppData = NULL;
		m_TitleList.RemoveAll();
	}
}

CString CTextParsing::GetStringTimePoint(CString strTime, BOOL IsRunningTime)
{
	int nTempHour, nTempMin, nTempSec, nTempMs;
	nTempHour = nTempMin = nTempSec = nTempMs = 0;

	int nHour, nMin, nSec, nMs;				
	nHour = nMin = nSec = nMs = -1;
	
	int nIndex, nPos = 0;
	CString strTemp;

	nIndex = strTime.Find(":", nPos);
	if(nIndex != -1)
	{
		while(nIndex!= -1)
		{
			strTemp = strTime.Mid(nPos, nIndex - nPos);
			if(nHour < 0)
				nHour = atoi(strTemp);
			else if(nMin < 0)
				nMin = atoi(strTemp);
			else if(nSec < 0)
				nSec = atoi(strTemp);
			
			nPos = nIndex + 1;
			nIndex = strTime.Find(":", nPos);

		}
	}
	else
	{
		nHour = nMin = 0;
	}
	
	if(nSec < 0)
	{
		strTemp = strTime.Mid(nPos, strTime.GetLength() - nPos);
		nIndex = strTemp.Find(".");
		if(nIndex > 0)
		{
			double fTemp = atof(strTemp.Right(strTemp.GetLength() - nIndex));
			nMs = fTemp * 100;
			strTemp = strTemp.Left(nIndex);
		}
		nSec = atoi(strTemp);
	}

	if(IsRunningTime == TRUE)	//동작시간일 경우
	{
		nHour	+= oldTime.nHour;
		nMin	+= oldTime.nMin;
		nSec	+= oldTime.nSec;
		nMs		+= oldTime.nMs;

		if(nMs > 99)					//990ms 보다 클경우
		{
			nSec += 1;
			nMs -= 100;

		}
		if(nSec > 59)
		{
			nMin += 1;
			nSec -= 60;
		}
		if(nMin > 59)
		{
			nHour += 1;
			nMin -= 60;
		}

		oldTime.nHour	= nHour;
		oldTime.nMin	= nMin;
		oldTime.nSec	= nSec;
		oldTime.nMs		= nMs;
	}
	
	CTimeSpan ts(0, nHour, nMin,nSec);
	CString strReturn;

	if(nMs < 0)
		strReturn.Format("%d.0", ts.GetTotalSeconds());	
	else
		strReturn.Format("%d.%02d", ts.GetTotalSeconds(), nMs);	
	
	return strReturn;

	
	
/*	while(p2!= -1)
	{
		p2 = str.Find(':', s2);
		if(p2!= -1)
		{
			strTime = str.Mid(s2, p2-s2);
			s2 = p2 + 1;

			if(nHour < 0)
				nHour = atoi(strTime);
			else if(nMin < 0)
				nMin = atoi(strTime);
		}
		else
		{
			if(s2 < str.GetLength())
			{
				if(nHour < 0)
					nHour = 0;
				if(nMin < 0)
					nMin = 0;
				strTime = str.Mid(s2, str.Find('.'));
				if(strTime == "")
				{
					strTime = str.Mid(str.ReverseFind(':')+1, str.GetLength());								
				}
				nSec = atoi(strTime);
				int nMsFind1, nMsFind2;
				nMsFind1 = str.Find('.');
				nMsFind2 = str.GetLength() - str.Find('.');
				
				if(nMsFind1 < 0)
					strTime = "00";
				else
					strTime = str.Mid(nMsFind1,nMsFind2);
				
				float fTemp = atof(strTime);
				nMs = fTemp * 100;
			}
		}
	
	}
	//str.Remove(':');
	if(GetColumnHeader(0) == "T2")		//동작 시간일 경우 누적시간으로 변경한다.
	{
		nHour	+= nTempHour;
		nMin	+= nTempMin;
		nSec	+= nTempSec;
		nMs		+= nTempMs;

		if(nMs > 99)					//990ms 보다 클경우
		{
			nSec += 1;
			nMs -= 100;

		}
		if(nSec > 59)
		{
			nMin += 1;
			nSec -= 60;
		}
		if(nMin > 59)
		{
			nHour += 1;
			nMin -= 60;
		}
	}

	
	CTimeSpan ts(0, nHour, nMin,nSec);
	CTimeSpan ts2(0, nTempHour, nTempMin, nTempSec);
	if(ts.GetTotalSeconds() < ts2.GetTotalSeconds())
	{
		strErrorMsg.Format("시간 데이터가 올바르지 않습니다.%d:%d:%d / %d:%d:%d");
		return FALSE;
	}
	if(ts.GetTotalSeconds() == ts2.GetTotalSeconds())
	{
		if(nTempMs > nMs)
		{
			strErrorMsg.Format("시간 데이터가 올바르지 않습니다.%d:%d:%d / %d:%d:%d");
			return FALSE;
		}
		
	}
	
	str.Format("%d.%02d", ts.GetTotalSeconds(), nMs);

	return strTime2;*/
}
