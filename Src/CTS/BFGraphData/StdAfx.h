// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__26B8E346_D902_11D4_88DF_006008CEDA07__INCLUDED_)
#define AFX_STDAFX_H__26B8E346_D902_11D4_88DF_006008CEDA07__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxole.h>         // MFC OLE classes
#include <afxodlgs.h>       // MFC OLE dialog classes
#include <afxdisp.h>        // MFC Automation classes
#endif // _AFX_NO_OLE_SUPPORT


#ifndef _AFX_NO_DB_SUPPORT
#include <afxdb.h>			// MFC ODBC database classes
#endif // _AFX_NO_DB_SUPPORT

#ifndef _AFX_NO_DAO_SUPPORT
#include <afxdao.h>			// MFC DAO database classes
#endif // _AFX_NO_DAO_SUPPORT

#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#include <afxtempl.h>
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <float.h>
#include "../../../Include/PSCommonAll.h"
#include "../../../Include/IniParser.h"
#define REG_CONFIG_SECTION	"Config"

extern int g_nLanguage;			
extern CString g_strLangPath;
enum LanguageType {LANGUAGE_KOR=0, LANGUAGE_ENG, LANGUAGE_CHI };

#define FORM_SET_DATABASE_NAME		"Schedule.mdb"

#ifdef _DEBUG
#pragma comment(lib, "../../../Lib/BFGraphComD.lib ")
#pragma message("Automatically linking with BFGraphComD.lib By K.B.H ")
#else
#pragma comment(lib, "../../../Lib/BFGraphCom.lib")
#pragma message("Automatically linking with BFGraphCom.lib By K.B.H")
#endif	//_DEBUG


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__26B8E346_D902_11D4_88DF_006008CEDA07__INCLUDED_)
