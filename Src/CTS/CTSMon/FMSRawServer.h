#pragma once

#define MAX_ACCEPT_POOL_CNT 1

template <class TNetObj>
class CFMSRawServer : public CFMSIocp
{
public:
	CFMSRawServer(VOID) { m_dwAcceptPoolCnt = MAX_ACCEPT_POOL_CNT; }

	BOOL Begin(USHORT usListenPort)
	{
		m_poNetIocp = new CFMSNetIocp<TNetObj>();
		m_poListenObj = new TNetObj();

		for (DWORD i=0;i<m_dwAcceptPoolCnt;i++)
		{
			TNetObj *poAcceptObj = new TNetObj();
			m_lstAcceptObj.push_back(poAcceptObj);
		}

		m_poNetIocp->SetOwnerIocp(this);
		
		if (!m_poNetIocp->Begin(0))
			return FALSE;
		
		// Backlog와 AcceptPool의 비율은 10:1로 정의한다.
		// if (!m_poListenObj->Listen(usListenPort, m_dwAcceptPoolCnt*10))
		if (!m_poListenObj->Listen(usListenPort, m_dwAcceptPoolCnt))
			return FALSE;

		if (!m_poNetIocp->RegSocketToIocp(m_poListenObj->GetSocket(), (ULONG_PTR) m_poListenObj))
			return FALSE;
					
		if (!CFMSIocp::Begin(1))
			return FALSE;
		
		for (std::list<TNetObj*>::iterator it=m_lstAcceptObj.begin();it!=m_lstAcceptObj.end();it++)
		{
			TNetObj *poAcceptObj = (TNetObj*)(*it);
			if (!poAcceptObj->Accept(m_poListenObj->GetSocket()))
				return FALSE;
		}

		return TRUE;
	}

	VOID End(VOID)
	{
		CFMSIocp::End();
		TRACE("Work Thread Stop !!!!!!! \n");

		for (std::list<TNetObj*>::iterator it=m_lstConnectedObj.begin();it!=m_lstConnectedObj.end();it++)
		{
			TNetObj *poFMSObj = (TNetObj*)(*it);
			if (poFMSObj->ForceClose())
			{
				OnDisconnected(poFMSObj);
			}
		}

		for (std::list<TNetObj*>::iterator it=m_lstAcceptObj.begin();it!=m_lstAcceptObj.end();it++)
		{
			TNetObj *poAcceptObj = (TNetObj*)(*it);
			if ( poAcceptObj->ForceClose() )
				delete poAcceptObj;
		}

		m_poNetIocp->End();
		TRACE("Recv Thread Stop !!!!!!! \n");

		m_poListenObj->ForceClose();
		TRACE("ListenObj ForceClose !!!!!!! \n");

		delete m_poNetIocp;
		delete m_poListenObj;
		
		m_lstAcceptObj.clear();
		m_lstConnectedObj.clear();
	}

	SOCKET GetListenSocket(VOID) { return m_poListenObj->GetSocket(); }
	
protected:
	std::list<TNetObj*> m_lstAcceptObj;
	std::list<TNetObj*> m_lstConnectedObj;
	TNetObj *m_poListenObj;
	CFMSNetIocp<TNetObj> *m_poNetIocp;
	DWORD m_dwAcceptPoolCnt;

protected:
	virtual VOID OnConnected(TNetObj *poNetObj) = 0;
	virtual VOID OnDisconnected(TNetObj *poNetObj) = 0;
	virtual VOID OnRead(TNetObj *poNetObj, st_FMSMSGHEAD &_hdr, BYTE *pReadBuf, DWORD dwLen) = 0;
	virtual VOID OnWrite(TNetObj *poNetObj, DWORD dwLen) = 0;

	VOID OnIo(BOOL bSucc, DWORD dwNumOfByteTransfered, ULONG_PTR pCompletionKey, OVERLAPPED *pol)
	{
		CFMSNetIoStatus *ponios = (CFMSNetIoStatus*) pol;

		if (ponios)
		{
			TNetObj *poNetObj = (TNetObj*) ponios->m_poObject;			
			DWORD dwDataLen = 0;			
			BOOL bReadBufRet = FALSE;

			st_FMSMSGHEAD hdr;
			memset(&hdr, 0x00, sizeof(st_FMSMSGHEAD));
			switch (ponios->m_eIO)
			{
			case IO_ACCEPT:
				CFMSLog::WriteLog("=> IO_ACCEPT <=");				

				for (std::list<TNetObj*>::iterator it=m_lstConnectedObj.begin();it!=m_lstConnectedObj.end();it++)
				{
					TNetObj *poFMSObj = (TNetObj*)(*it);
					if( poFMSObj->GetIsWork() )
					{	
						CFMSLog::WriteLog("=> m_lstConnectedObj remove <=");
						if (poFMSObj->ForceClose())
						{	
							OnDisconnected(poFMSObj);							
						}
					}
				}

				poNetObj->SetIsWork(TRUE);
				m_lstAcceptObj.remove(poNetObj);
				m_lstConnectedObj.push_back(poNetObj);
				OnConnected(poNetObj);			
				break;

			case IO_NEW_ACCEPTOBJ:
				CFMSLog::WriteLog("=> IO_NEW_ACCEPTOBJ <=");
				m_lstAcceptObj.push_back(poNetObj);
				break;

			case IO_DISCONNECT:
				CFMSLog::WriteLog("=> IO_DISCONNECT <=");				
				m_lstConnectedObj.remove(poNetObj);
				OnDisconnected(poNetObj);
				delete poNetObj;
				break;

			case IO_READ:
				{
					CFMSManagedBufSP Buf;

					while (bReadBufRet = poNetObj->ReadPacket(hdr, Buf->m_aucBuf
						, ponios->m_dwNumOfByteTransfered, dwDataLen))
					{
						if (dwDataLen > 0)
						{
							OnRead(poNetObj, hdr, Buf->m_aucBuf, dwDataLen);
						}
						else
						{
							// 패킷이 변조되었을 때의 처리 (접속 종료)
							CFMSLog::WriteLog("IO_READ - ReadPacket - dwDataLen <= 0");
							poNetObj->ForceClose();
							break;
						}

						ZeroMemory(&hdr, sizeof(st_FMSMSGHEAD));
					}

					//if(poNetObj->getRecvTotlen() == poNetObj->getProcTotlen())
					//{
					//	TRACE("\n\n TOT [RECV]%d/[PROC]%d \n\n"
					//		, poNetObj->getRecvTotlen()
					//		,  poNetObj->getProcTotlen());

					//	poNetObj->ResetTotlen();
					//}

				}
				break;
			case IO_WRITE:				
				OnWrite(poNetObj, ponios->m_dwNumOfByteTransfered);
				delete ponios;
				break;
			}
		}
	}
};
