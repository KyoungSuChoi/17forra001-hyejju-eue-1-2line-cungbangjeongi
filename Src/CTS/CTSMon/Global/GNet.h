/*#include <winsock2.h>
#include <afxsock.h>		

#include <Iphlpapi.h>//GetIMacAddr()
#include <lm.h>//GetNMacAddr()
#include <objbase.h>
#include <netcon.h>
#include <wininet.h>

#pragma comment(lib, "rpcrt4.lib")//GetUMacAddr()
#pragma comment(lib, "iphlpapi.lib")//GetIMacAddr()
#pragma comment(lib, "Netapi32.lib")//GetNMacAddr()
#pragma comment(lib, "ws2_32.lib")

#define ALLOCATE_FROM_PROCESS_HEAP( bytes )		::HeapAlloc( ::GetProcessHeap(), HEAP_ZERO_MEMORY, bytes )
#define DEALLOCATE_FROM_PROCESS_HEAP( ptr )		if( ptr ) ::HeapFree( ::GetProcessHeap(), 0, ptr )
#define REALLOC_FROM_PROCESS_HEAP( ptr, bytes )	::HeapReAlloc( ::GetProcessHeap(), HEAP_ZERO_MEMORY, ptr, bytes )
*/

typedef struct 
{	
	UINT type;
	CString guid;	
	CString desc;	
	CString mac;
	CString ip;	
	CString subnet;
	CString gw;
	BOOL    bDhcpEnabled ;
	BOOL    bDnsEnabled ;
	CString dns1;
	CString dns2;
} LANINFO;//lancard info

class GNet
{
public:   
	GNet();
    ~GNet();
    		
	/*static void net_closesocket(SOCKET TempSocket)
	{
		//shutdown(TempSocket,SD_BOTH); 
		//closesocket(TempSocket);

		DWORD code = ::shutdown( TempSocket, SD_BOTH ); // connect end singal send

		if( code != SOCKET_ERROR )  // Wait for socket to fail (ie closed by other end) 
		{
		  fd_set readfds; 
		  fd_set errorfds; 
		  timeval timeout; 
	      
		  FD_ZERO( &readfds ); 
		  FD_ZERO( &errorfds ); 
		  FD_SET( TempSocket, &readfds ); 
		  FD_SET( TempSocket, &errorfds ); 
		  timeout.tv_sec  = 0; 
		  timeout.tv_usec = 0; 
		  ::select( 1, &readfds, NULL, &errorfds, &timeout ); 
		}  
		code = ::closesocket( TempSocket ); 
		TempSocket = INVALID_SOCKET; 
	}*/

	static CString net_GetIpA()
	{//local ip
		WORD wVersionRequested; 
		WSADATA wsaData; 
		char name[255]; 
		CString ip=_T("");
		PHOSTENT hostinfo; 
		wVersionRequested = MAKEWORD(2,0); 
		if (WSAStartup(wVersionRequested,&wsaData)==0)
		{
			if(gethostname(name,sizeof(name))==0)
			{
			  if((hostinfo=gethostbyname(name))!=NULL)
			  {
				 ip=inet_ntoa(*(struct in_addr *)*hostinfo->h_addr_list); 
			  }
			} 
		}
		WSACleanup(); 
		return ip;
	}

	/*
	static CString net_GetIpB(CStringArray &Data)
	{
		Data.RemoveAll();
		WORD wVersionRequested;
		WSADATA wsaData;
		char name[255];
		PHOSTENT hostinfo;
		wVersionRequested = MAKEWORD( 1, 1 );
		char *ip;

		if ( WSAStartup( wVersionRequested, &wsaData ) == 0 )
		{
		  if( gethostname ( name, sizeof(name)) == 0)
		  {		   
			   if((hostinfo = gethostbyname(name)) != NULL)
			   {
					 int nCount = 0;
					 while(hostinfo->h_addr_list[nCount])
					 {				
					   ip = inet_ntoa(*(struct in_addr *)hostinfo->h_addr_list[nCount]);			  			   
					   Data.Add((CString)ip);//ip	
					   nCount=nCount+1;
					 }
			   }
		 }
		}
		WSACleanup(); 
		return (CString)name;//Host name
	}

	static BOOL net_GetIpC(CString &strMac,CString &strIp)
	{//IP	
		strMac=strIp=_T("");
		PIP_ADAPTER_INFO pAdapterInfo;
		PIP_ADAPTER_INFO pAdapter = NULL;
		DWORD dwRetVal = 0;
		
		pAdapterInfo = (IP_ADAPTER_INFO*)malloc(sizeof(IP_ADAPTER_INFO));
		ULONG ulOutBufLen = sizeof(IP_ADAPTER_INFO);

		if (::GetAdaptersInfo(pAdapterInfo, &ulOutBufLen) == ERROR_BUFFER_OVERFLOW)
		{
			free(pAdapterInfo);
			pAdapterInfo = (IP_ADAPTER_INFO*)malloc(ulOutBufLen); 
		}

		if ((dwRetVal = ::GetAdaptersInfo(pAdapterInfo, &ulOutBufLen)) == NO_ERROR)
		{
			pAdapter = pAdapterInfo;
			while (pAdapter)
			{//pAdapter->Type(MIB_IF_TYPE_ETHERNET)
								
				CString mac=net_StrMacAddress(pAdapter->Address);
				
				CString ip=_T("");
				CString gw=_T("");
				CString sguid=_T("");

				#ifdef _UNICODE			   												   				   
					USES_CONVERSION;
					sguid = A2T( pAdapter->AdapterName );					
					ip=pAdapter->IpAddressList.IpAddress.String;					
					gw=pAdapter->GatewayList.IpAddress.String;
				#else				    
					sguid=pAdapter->AdapterName;
					ip=pAdapter->IpAddressList.IpAddress.String;
					gw=pAdapter->GatewayList.IpAddress.String;
				#endif	

				//GNet::net_NetIsConnect(sguid)==TRUE)
				if(mac!=_T("00-00-00-00-00-00") && ip!=_T("0.0.0.0"))
				{					
					strMac=mac;
					strIp=ip;
					break;
				}
				pAdapter = pAdapter->Next;
			}
		}
		free(pAdapterInfo);
		return TRUE;
	}

	static CString net_GetUMacAddr()
	{//#pragma comment(lib, "rpcrt4.lib")
		unsigned char MACData[6];

		UUID uuid;
		UuidCreateSequential( &uuid );    // Ask OS to create UUID

		for (int i=2; i<8; i++){  // Bytes 2 through 7 inclusive                           
			MACData[i - 2] = uuid.Data4[i];// are MAC address
		}
		CString strMac=_T("");
		strMac.Format(_T("%02X-%02X-%02X-%02X-%02X-%02X"), 
				   MACData[0], MACData[1], MACData[2], MACData[3], MACData[4], MACData[5]);
		return strMac;
	}

	static CString net_GetIMacAddr()
	{//#include <Iphlpapi.h> #pragma comment(lib, "iphlpapi.lib")
		CString strMac=_T("");
		DWORD size = sizeof(PIP_ADAPTER_INFO);

		PIP_ADAPTER_INFO Info;
		ZeroMemory( &Info, size );

		int result = GetAdaptersInfo( Info, &size );        // 첫번째 랜카드 MAC address 가져오기
		if (result == ERROR_BUFFER_OVERFLOW)    // GetAdaptersInfo가 메모리가 부족하면 재 할당하고 재호출
		{
			Info = (PIP_ADAPTER_INFO)malloc(size);
			GetAdaptersInfo( Info, &size );
		}
		if(!Info) return strMac;

		strMac.Format(_T("%0.2X-%0.2X-%0.2X-%0.2X-%0.2X-%0.2X"), 
					  Info->Address[0], Info->Address[1], Info->Address[2], 
					  Info->Address[3], Info->Address[4], Info->Address[5] );
		return strMac;
	}

	static CString net_GetNMacAddr()
	{//#include <lm.h> #pragma comment(lib, "Netapi32.lib")

		unsigned char MACData[8];      // Allocate data structure for MAC (6 bytes needed)                                 
		WKSTA_TRANSPORT_INFO_0 *pwkti; // Allocate data structure  for NetBIOS                                 
		DWORD dwEntriesRead;
		DWORD dwTotalEntries;
		BYTE *pbBuffer;

		// Get MAC address via NetBIOS's enumerate function
		NET_API_STATUS dwStatus = NetWkstaTransportEnum(
							NULL,                 // [in]  server name
							0,                    // [in]  data structure to return
							&pbBuffer,            // [out] pointer to buffer
						   MAX_PREFERRED_LENGTH, // [in]  maximum length
						   &dwEntriesRead,       // [out] counter of elements
												 //       actually enumerated
						   &dwTotalEntries,      // [out] total number of elements
												 //       that could be enumerated
						   NULL);                // [in/out] resume handle
		if(dwStatus != NERR_Success) return _T("");

		pwkti = (WKSTA_TRANSPORT_INFO_0 *)pbBuffer; // type cast the buffer

		CString strMac=_T("");
		for(DWORD i=1; i< dwEntriesRead; i++)  // first address is 00000000, skip it enumerate MACs & print
		{
			swscanf((wchar_t *)pwkti[i].wkti0_transport_address,
					L"%2hx%2hx%2hx%2hx%2hx%2hx",
					&MACData[0],&MACData[1],&MACData[2],&MACData[3],&MACData[4],&MACData[5]);
			
			strMac.Format(_T("%02X-%02X-%02X-%02X-%02X-%02X\n"), 
						  MACData[0],MACData[1],MACData[2],MACData[3],MACData[4],MACData[5]);		
	  }
	  dwStatus = NetApiBufferFree(pbBuffer);//Release pbBuffer allocated by above function
	  return strMac;
	}

	static BOOL net_ChkNameServer(CString sguid)
	{
		if(sguid.IsEmpty() || sguid==_T("")) return FALSE;

		HKEY iphKey=GReg::reg_GetKey(HKLM,
									_T("SYSTEM\\CurrentControlSet\\Services\\TcpIp\\Parameters\\Interfaces"));			
		if(!iphKey) return FALSE;
		CString dns=GReg::reg_GetStrValue(iphKey,sguid,_T("NameServer"));
		
		if(dns.GetLength()>3) return TRUE;
		return FALSE;
	}		

	static CPtrArray* net_GetLanCardB()
	{
		HKEY phKey=GReg::reg_GetKey(HKLM,
									_T("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\NetworkCards"));		
		if(!phKey) return NULL;
		CPtrArray *lanlist=new CPtrArray;

		CStringArray list;
		GReg::reg_GetKeyList(phKey,list);
		for(int i=0;i<list.GetSize();i++)
		{
			LANINFO *LanInfo=new LANINFO;
			CString sguid=list.GetAt(i);

			LanInfo->desc=GReg::reg_GetStrValue(phKey,sguid,_T("Description"));
			LanInfo->guid=GReg::reg_GetStrValue(phKey,sguid,_T("ServiceName"));
					
			HKEY iphKey=GReg::reg_GetKey(HKLM,
									_T("SYSTEM\\CurrentControlSet\\Services\\TcpIp\\Parameters\\Interfaces"));			
			LanInfo->ip=GReg::reg_GetStrValue(iphKey,sguid,_T("IPAddress"));
			LanInfo->subnet=GReg::reg_GetStrValue(iphKey,sguid,_T("SubnetMask"));
			LanInfo->gw=GReg::reg_GetStrValue(iphKey,sguid,_T("DefaultGateway"));
			LanInfo->bDhcpEnabled=_ttoi(GReg::reg_GetStrValue(iphKey,sguid,_T("EnableDHCP")));		
			CString dns=GReg::reg_GetStrValue(iphKey,sguid,_T("NameServer"));
			LanInfo->dns1=GStr::str_GetSplit(dns,0,',');
			LanInfo->dns2=GStr::str_GetSplit(dns,1,',');
			
			if(LanInfo->dns1!=_T("") || LanInfo->dns1!=_T("")) LanInfo->bDnsEnabled=TRUE;	
			else LanInfo->bDnsEnabled=FALSE;	

			lanlist->Add(LanInfo);
		}
		return lanlist;
	}	

	static BOOL net_SetNetwork(CString sguid,int idhcp,int idns,
		                       CString ip=_T(""),CString subnet=_T(""),CString gw=_T(""),
							   CString dns1=_T(""),CString dns2=_T(""))
	{
		HKEY phKey=GReg::reg_GetKey(HKLM,
									_T("SYSTEM\\CurrentControlSet\\Services\\TcpIp\\Parameters\\Interfaces"));			
		if(!phKey) return FALSE;
		
		GReg::reg_SetDwordValue(phKey,sguid,_T("EnableDHCP"),idhcp);//dword		
		if(idhcp==0)
		{			
			GReg::reg_SetMultiValue(phKey,sguid,_T("IPAddress"),ip);
			GReg::reg_SetMultiValue(phKey,sguid,_T("SubnetMask"),subnet);
			GReg::reg_SetMultiValue(phKey,sguid,_T("DefaultGateway"),gw);				
		}
		else
		{
			GReg::reg_SetMultiValue(phKey,sguid,_T("DefaultGateway"),_T(""));
			GReg::reg_SetMultiValue(phKey,sguid,_T("DefaultGatewayMetric"),_T(""));			
		}
		if(idns==0)
		{			
			GReg::reg_SetStrValue(phKey,sguid,_T("NameServer"),dns1+_T(",")+dns2);
		}
		else
		{
			GReg::reg_SetStrValue(phKey,sguid,_T("NameServer"),_T(""));
		}
		
		RegCloseKey(phKey);
		return TRUE;
	}
			

	static CPtrArray* net_GetLanCardA()
	{
		BOOL	bResult = FALSE;
		PIP_ADAPTER_INFO pAdapterInfo;
		PIP_ADAPTER_INFO pAdapter = NULL;
		DWORD dwRetVal = 0;

		pAdapterInfo = (IP_ADAPTER_INFO*)malloc(sizeof(IP_ADAPTER_INFO));
		ULONG ulOutBufLen = sizeof(IP_ADAPTER_INFO);

		if (::GetAdaptersInfo(pAdapterInfo, &ulOutBufLen) == ERROR_BUFFER_OVERFLOW)
		{
			free(pAdapterInfo);
			pAdapterInfo = (IP_ADAPTER_INFO*)malloc(ulOutBufLen); 
		}

		CPtrArray *list=new CPtrArray;
		if ((dwRetVal = ::GetAdaptersInfo(pAdapterInfo, &ulOutBufLen)) == NO_ERROR)
		{
			pAdapter = pAdapterInfo;
			while (pAdapter)
			{					
				if(pAdapter->Type==MIB_IF_TYPE_ETHERNET)
				{								
					LANINFO *LanInfo=new LANINFO;				
					LanInfo->type=pAdapter->Type;
					LanInfo->desc=pAdapter->Description;   
					LanInfo->guid=pAdapter->AdapterName;//classid
					LanInfo->mac=net_StrMacAddress(pAdapter->Address);
					LanInfo->ip=pAdapter->IpAddressList.IpAddress.String;
					LanInfo->subnet=pAdapter->IpAddressList.IpMask.String;
					LanInfo->gw=pAdapter->GatewayList.IpAddress.String;
					LanInfo->dns1=_T("");
					LanInfo->dns2=_T("");
					LanInfo->bDhcpEnabled=pAdapter->DhcpEnabled;
					
					//dns
					LanInfo->bDnsEnabled=TRUE;//dynamic
					if(net_ChkNameServer(LanInfo->guid)==TRUE)
					{//name server가 있으면
						net_GetDnsB(pAdapter->Index,LanInfo->bDnsEnabled,
									LanInfo->dns1,LanInfo->dns2);
					}
					list->Add(LanInfo);	
				}
				pAdapter = pAdapter->Next;
			}
		}
		free(pAdapterInfo);
		return list;
	}

	static void net_GetDnsB(DWORD Index,BOOL &bflag,CString &dns1,CString &dns2)
	{//dns 가져오기
	    bflag=TRUE;
        dns1=dns2=_T("");

		ULONG ulLen  ;
		IP_PER_ADAPTER_INFO* pPerAdapt    = NULL;
		DWORD err=GetPerAdapterInfo(Index, pPerAdapt, &ulLen );		
		if( err == ERROR_BUFFER_OVERFLOW ) 
		{ 
			pPerAdapt = (IP_PER_ADAPTER_INFO*)ALLOCATE_FROM_PROCESS_HEAP( ulLen ); 			
			err = ::GetPerAdapterInfo(Index, pPerAdapt, &ulLen ); 
			if( err == ERROR_SUCCESS ) 
			{ 				
			  IP_ADDR_STRING* pNext = NULL;
			  pNext = &(pPerAdapt->DnsServerList);			  
			  while(pNext) 
			  { 			
				  bflag=FALSE;
				  if(dns1.IsEmpty() || dns1==_T(""))      dns1=pNext->IpAddress.String;
				  else if(dns2.IsEmpty() || dns2==_T("")) dns2=pNext->IpAddress.String;
				  
			      pNext = pNext->Next; 
			  } 
			} 						
			DEALLOCATE_FROM_PROCESS_HEAP(pPerAdapt); 					 
		 } 
	}

	static BOOL net_GetDnsA(BOOL &bflag,CString &dns1,CString &dns2)
	{//dns 
		DWORD    dwRetVal;
		IP_ADDR_STRING * pIPAddr;
        
		FIXED_INFO *FixedInfo = (FIXED_INFO *) GlobalAlloc( GPTR, sizeof( FIXED_INFO ) );
		ULONG ulOutBufLen = sizeof( FIXED_INFO );

		dns1=dns2=_T("");
		if( ERROR_BUFFER_OVERFLOW == GetNetworkParams( FixedInfo, &ulOutBufLen ) ) 
		{
			GlobalFree( FixedInfo );
			FixedInfo = (FIXED_INFO*)GlobalAlloc( GPTR, ulOutBufLen );
		}
		if ( dwRetVal = GetNetworkParams( FixedInfo, &ulOutBufLen ) ) 
		{
			return FALSE;
		}
		else 
		{						
			bflag=FixedInfo->EnableDns;
            dns1=FixedInfo->DnsServerList.IpAddress.String;
	        pIPAddr = FixedInfo -> DnsServerList.Next;
			if(pIPAddr)
			{
	           dns2=pIPAddr ->IpAddress.String;
			}			
		}
		return TRUE;
	}

	static BOOL net_GetMacAddressA(BYTE* byteMacAddress)
	{//mac addr
		BOOL	bResult = FALSE;
		PIP_ADAPTER_INFO pAdapterInfo;
		PIP_ADAPTER_INFO pAdapter = NULL;
		DWORD dwRetVal = 0;

		pAdapterInfo = (IP_ADAPTER_INFO*)malloc(sizeof(IP_ADAPTER_INFO));
		ULONG ulOutBufLen = sizeof(IP_ADAPTER_INFO);

		if (::GetAdaptersInfo(pAdapterInfo, &ulOutBufLen) == ERROR_BUFFER_OVERFLOW)
		{
			free(pAdapterInfo);
			pAdapterInfo = (IP_ADAPTER_INFO*)malloc(ulOutBufLen); 
		}

		if ((dwRetVal = ::GetAdaptersInfo(pAdapterInfo, &ulOutBufLen)) == NO_ERROR)
		{
			pAdapter = pAdapterInfo;
			while (pAdapter)
			{											
				CString mac=net_StrMacAddress(pAdapter->Address);
				
				CString ip;
				ip.Format(_T("%s"),pAdapter->IpAddressList.IpAddress.String);

				CString sguid;
				sguid.Format(_T("%s"),pAdapter->AdapterName);
								
			    if(mac!=_T("00-00-00-00-00-00") && ip!=_T("0.0.0.0"))
				{
                    memcpy(byteMacAddress,pAdapter->Address,6);					
					bResult = TRUE;
					break;
				}
				pAdapter = pAdapter->Next;
			}
		}
		free(pAdapterInfo);
		return bResult;
	}

	static BOOL net_GetMacAddressB(BYTE* byteMacAddress, int nbMAlen)
	{//맥주소
		BOOL	bResult = FALSE;
		PIP_ADAPTER_INFO pAdapterInfo;
		PIP_ADAPTER_INFO pAdapter = NULL;
		DWORD dwRetVal = 0;

		pAdapterInfo = (IP_ADAPTER_INFO*)malloc(sizeof(IP_ADAPTER_INFO));
		ULONG ulOutBufLen = sizeof(IP_ADAPTER_INFO);

		if (::GetAdaptersInfo(pAdapterInfo, &ulOutBufLen) == ERROR_BUFFER_OVERFLOW)
		{
			free(pAdapterInfo);
			pAdapterInfo = (IP_ADAPTER_INFO*)malloc(ulOutBufLen); 
		}

		if ((dwRetVal = ::GetAdaptersInfo(pAdapterInfo, &ulOutBufLen)) == NO_ERROR)
		{
			pAdapter = pAdapterInfo;
			int iMac = 0;
			while (pAdapter)
			{
				CString mac=net_StrMacAddress(pAdapter->Address);

				CString ip;
				ip.Format(_T("%s"),pAdapter->IpAddressList.IpAddress.String);

				CString sguid;
				sguid.Format(_T("%s"),pAdapter->AdapterName);

				if(mac!=_T("00-00-00-00-00-00")  && ip!=_T("0.0.0.0"))
				{				
					if(iMac*6 >= nbMAlen) break;
					memcpy(byteMacAddress,pAdapter->Address,6);					
					bResult = TRUE;				
				}
				pAdapter = pAdapter->Next;
				iMac++;
			}
		}
		free(pAdapterInfo);
		return bResult;
	}

	static CString net_StrMacAddress(BYTE* byteMacAddress)
	{
		CString strMac=_T("");
		strMac.Format(_T("%02x-%02x-%02x-%02x-%02x-%02x"), byteMacAddress[0],
													       byteMacAddress[1],
													       byteMacAddress[2],
													       byteMacAddress[3],
													       byteMacAddress[4],
													       byteMacAddress[5]);
		return strMac.MakeUpper();		
	}

	static CString net_GetStrMac(BYTE *mac,int nbMAlen)
	{
		CString strMac=_T("");
		for(int i=0; i<nbMAlen; i++)
		{
			CString temp=_T("");
			temp.Format(_T("%02x"),mac[i]);
			temp.MakeUpper();

			if(strMac==_T("")) strMac.Append(temp);
			else
			{
				strMac.Append(_T("-"));
				strMac.Append(temp);
			}
		}	
		return strMac;
	}

	static int net_ConvertIP(CString pIP)
	{
		int sub0 = 0;
		int sub1 = 0;
		int sub2 = 0;
		int sub3 = 0;

		#ifdef _UNICODE
			swscanf(pIP, _T("%d.%d.%d.%d"), &sub0, &sub1, &sub2, &sub3);
		#else
			sscanf(pIP, _T("%d.%d.%d.%d"), &sub0, &sub1, &sub2, &sub3);
		#endif
		
		int ip = 0;
		ip = ((sub0 & 0xff) << 24);
		ip += ((sub1 & 0xff) << 16);
		ip += ((sub2 & 0xff) << 8);
		ip += (sub3 & 0xff);;
		return ip;
	}
	
	static CString net_GetHostName()
	{//Local PC Name
		WORD wVersionRequested; 
		WSADATA wsaData; 
		char name[255]; 
		wVersionRequested = MAKEWORD(2,0); 
		if (WSAStartup(wVersionRequested,&wsaData)==0)
		{
			if(gethostname(name,sizeof(name))==0)
			{
			} 
		}
		WSACleanup(); 
		return (CString)name;
	}


	static CString net_GetIpFromDomain(CString domain)
	{//get ip from domain
		WORD wVersionRequested; 
		WSADATA wsaData; 
		CString ip=_T("");
		PHOSTENT hostinfo; 
		wVersionRequested = MAKEWORD(2,0); 
		if (WSAStartup(wVersionRequested,&wsaData)==0)
		{
			  if((hostinfo=gethostbyname((LPSTR)(LPCTSTR)domain))!=NULL)
			  {
				 ip=inet_ntoa(*(struct in_addr *)*hostinfo->h_addr_list); 
		      }
		}
		WSACleanup(); 
		return ip;
	}

	static BOOL net_SetHostName(CString hostname)
	{
		//hostname
		//GReg::reg_SetStrValue(HKCU,_T("SYSTEM\\CurrentControlSet\\Services\\TcpIp\\Parameters"),T("HostName"),hostname);
		//GReg::reg_SetStrValue(HKCU,T("SYSTEM\\CurrentControlSet\\Control\\ComputerName\\ComputerName"),T("ComputerName"),hostname);
		
		return SetComputerNameEx(ComputerNamePhysicalDnsHostname, hostname);
	}


	static BOOL net_SetWorkGroupName(CString workgroup)
	{		
		LPCWSTR pServer = NULL;
		LPCWSTR pDomain = workgroup.AllocSysString();
		LPCWSTR pAccountOU = NULL;
		LPCWSTR pAccount = NULL;
		LPCWSTR pPassword = NULL;
		DWORD nJoinOptions = NETSETUP_DOMAIN_JOIN_IF_JOINED;
		NET_API_STATUS nStatus =
		NetJoinDomain(pServer, pDomain, pAccountOU, pAccount, pPassword,nJoinOptions);
	    
		return nStatus;
	}

	static CString net_GetWorkGroupName()
	{//그룹명가져오기
		CString hostname,workgroup;
		if(GetVersion()<0x80000000)                
		{// WinNT series
			const    DWORD    dwLevel = 102;
			LPWSTR   pszServerName=NULL;
			BOOL     fIsDLLLoaded = FALSE;
			{
				WKSTA_INFO_102    *pBuf = NULL;
				NET_API_STATUS    nStatus;
				nStatus = NetWkstaGetInfo( pszServerName, dwLevel, (LPBYTE*)&pBuf );
				
				if (nStatus == NERR_Success)
				{					
					hostname=(CString)pBuf->wki102_computername;
					workgroup=(CString)pBuf->wki102_langroup;
				}
				if (pBuf != NULL) NetApiBufferFree(pBuf);				
			}
		}
		else
		{// 9X에서는 레지스트리에서 검색을 해야함.						
			workgroup=GReg::reg_GetStrValue(HKLM,
				      _T("System\\CurrentControlSet\\Services\\VxD\\VNETSUP"),_T("Workgroup"));			
		}	
		return workgroup;
	}

	static BOOL net_NetIsConnect(CString sClassid)
	{//connect check
		if(sClassid.IsEmpty() || sClassid==_T("")) return FALSE;
		
		CoInitialize(0); 
		INetConnectionManager *pMan = 0; 
		int status = FALSE; //1:connect, 0:disconnect 
		HRESULT hr = CoCreateInstance(CLSID_ConnectionManager, 
										0, 
										CLSCTX_ALL, 
										__uuidof(INetConnectionManager), 
										(void**)&pMan); 																	
		if (SUCCEEDED(hr)) 
		{ 
			IEnumNetConnection *pEnum = 0; 
			if(SUCCEEDED(pMan->EnumConnections(NCME_DEFAULT, &pEnum)))
			{ 
				INetConnection *pCon = 0; 
				ULONG count; 				
				while (pEnum->Next(1, &pCon, &count) == S_OK ) 
				{ 					
					NETCON_PROPERTIES *pProps = 0; 					
					if (SUCCEEDED(pCon->GetProperties(&pProps))) 
					{ 
						CString sGuid=GStr::str_GetGuid(pProps->guidId);
						NETCON_STATUS Status=pProps->Status;

						if (pProps) 
						{
							CoTaskMemFree(pProps->pszwName); 
							CoTaskMemFree(pProps->pszwDeviceName); 
							CoTaskMemFree(pProps); 
						}
						if(sClassid==sGuid)
						{
							if (Status == NCS_DISCONNECTED)   status = FALSE;
							else if (Status == NCS_CONNECTED) status = TRUE;
							break;
						}
					} 
					pCon->Release(); 
				} 
				pEnum->Release(); 
			} 
			pMan->Release(); 
		} 		 
		CoUninitialize(); 
		return status; 
	}

	static HRESULT net_NetStatus(CString sClassid,BOOL bEnable)
	{ 
		HRESULT hr = E_FAIL; 
		CoInitialize(NULL); 
		INetConnectionManager *pNetConnectionManager = NULL; 
		hr = CoCreateInstance(CLSID_ConnectionManager,
							  NULL,
							  CLSCTX_LOCAL_SERVER | CLSCTX_NO_CODE_DOWNLOAD, 
							  IID_INetConnectionManager, 
							  reinterpret_cast<LPVOID *>(&pNetConnectionManager) ); 
		if (FAILED(hr))
		{
			pNetConnectionManager->Release(); 
			CoUninitialize(); 
			return hr;
		}
		
		//Get an enumurator for the set of connections on the system 
		IEnumNetConnection* pEnumNetConnection; 
		pNetConnectionManager->EnumConnections(NCME_DEFAULT, &pEnumNetConnection);
		ULONG ulCount = 0;
		BOOL fFound = FALSE; 
		hr = HRESULT_FROM_WIN32(ERROR_FILE_NOT_FOUND); 
		HRESULT hrT = S_OK; 
		
		do
		{
			NETCON_PROPERTIES* pProps = NULL;
			INetConnection *pConn; 
			
			hrT = pEnumNetConnection->Next(1, &pConn, &ulCount); 
			if (SUCCEEDED(hrT) && 1 == ulCount)
			{
				hrT = pConn->GetProperties(&pProps); 
				if (S_OK == hrT)
				{						
					CString sName=(CString)pProps->pszwName;//로컬 영역 연결
					CString sGuid=GStr::str_GetGuid(pProps->guidId);
                    CString sDevice=(CString)pProps->pszwDeviceName;
															
					CoTaskMemFree (pProps->pszwName);
					CoTaskMemFree (pProps->pszwDeviceName); CoTaskMemFree (pProps); 
					
					if(sClassid==sGuid)
					{
					 	if (bEnable) hr = pConn->Connect(); 
						else 		 hr = pConn->Disconnect();						
					}
				} 
				pConn->Release();
				pConn = NULL;
			} 
		} while (SUCCEEDED(hrT) && 1 == ulCount && !fFound); 

		if (FAILED(hrT))
		{
			hr = hrT;
		} 
 	    pEnumNetConnection->Release();
		
		if (FAILED(hr) && hr != HRESULT_FROM_WIN32(ERROR_RETRY)) 
		{ 
			//printf("Could not enable or disable connection (0x%08x)\r\n", hr); 
		} 
		pNetConnectionManager->Release(); 
		CoUninitialize(); 
		return hr; 
	}

	static BOOL net_PortScanA(CString ip,CString port)
	{	
		WSADATA wsadata;
		SOCKADDR_IN	addr;
		SOCKET sSocket = INVALID_SOCKET;
		LINGER		LingerStruct;
		int         nZero=0;

		if(WSAStartup(MAKEWORD(2,2), &wsadata) != 0)
		{
			WSACleanup();
			return FALSE;
		}
		if(INVALID_SOCKET==(sSocket=socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)))
		{		

			WSACleanup();
			return FALSE;
		}
		memset( &addr, 0, sizeof(SOCKADDR_IN));
		addr.sin_family = AF_INET;
		addr.sin_port = htons(_ttoi(port));  
		addr.sin_addr.s_addr = inet_addr((LPSTR)(LPCTSTR)ip);
		
		//if( (fcntl(sSocket, F_GETFL, NULL)) < 0) 
		//{ 
		//	WSACleanup();
		//   return FALSE; 
		//}
		
		if(SOCKET_ERROR == ioctlsocket(sSocket, FIONBIO, (ULONG *)&nZero ))
		{
		    WSACleanup();
		    return FALSE;
		}

		LingerStruct.l_onoff  = 0;//1,SO_LINGER
		LingerStruct.l_linger = 0;
		nZero = 0; 
		if(SOCKET_ERROR == setsockopt(sSocket, SOL_SOCKET, SO_LINGER,(char*) &LingerStruct, sizeof(LingerStruct))){
			WSACleanup(); //SO_LINGER  SO_DONTLINGER
			return FALSE;
		}

		int nValue = 1;
		if(SOCKET_ERROR==setsockopt(sSocket, IPPROTO_TCP, TCP_NODELAY,(char*)&nValue, sizeof(nValue) )){
			WSACleanup();
			return FALSE;
		}

		if(SOCKET_ERROR==setsockopt(sSocket, SOL_SOCKET, SO_OOBINLINE,(char *)&nValue, sizeof(nValue))){
			WSACleanup();
			return FALSE;
		}

		int timeout=1;
		if (SOCKET_ERROR==setsockopt(sSocket, SOL_SOCKET, SO_RCVTIMEO,(char*)&timeout, sizeof(timeout)))
		{
			WSACleanup();
			return FALSE;
		}
		
		if (SOCKET_ERROR==setsockopt(sSocket, SOL_SOCKET, SO_SNDTIMEO,(char*)&timeout, sizeof(timeout)))
		{
			WSACleanup();
			return FALSE;
		}

		struct timeval Timeout;
		fd_set readfds;
		readfds.fd_count = 1;
		readfds.fd_array[0] = sSocket;
		Timeout.tv_sec = 1;//3
		Timeout.tv_usec = 0;
		
		if(SOCKET_ERROR==select(1, &readfds, NULL, NULL, &Timeout))
		{
			WSACleanup();
			return FALSE;
		}
		
		BOOL flag=FALSE;
		if(SOCKET_ERROR!=connect(sSocket, (LPSOCKADDR)&addr,sizeof(addr)))
		{		 	    
			flag=TRUE;
		}

		WSACleanup();
		return flag;
	}

	static BOOL net_PortScanB(CString ip,CString port)
	{	
		WSADATA wsadata;
		SOCKADDR_IN	addr;		
		SOCKET sSocket = INVALID_SOCKET;		

		if(WSAStartup(MAKEWORD(2,2), &wsadata) != 0)
		{
			WSACleanup();
			return FALSE;
		}
		if(INVALID_SOCKET==(sSocket=socket(AF_INET, SOCK_STREAM, 0)))
		{
			WSACleanup();
			return FALSE;
		}

		memset( &addr, 0, sizeof(SOCKADDR_IN));
		addr.sin_family = AF_INET;
		addr.sin_port = htons(_ttoi(port));  
		addr.sin_addr.s_addr = inet_addr((LPSTR)(LPCTSTR)ip);	

		/////////////////////////////
		//LINGER		LingerStruct;
		//LingerStruct.l_onoff  = 0;//1,SO_LINGER
		//LingerStruct.l_linger = 0;		
		//if(SOCKET_ERROR == setsockopt(sSocket, SOL_SOCKET, SO_LINGER,(char*) &LingerStruct, sizeof(LingerStruct))){
		//	WSACleanup(); //SO_LINGER  SO_DONTLINGER
		//	return FALSE;
		//}
		/////////////////////////////

		sSocket = socket(AF_INET, SOCK_STREAM, 0);
		if(sSocket < 0)
		{
			WSACleanup();
			return FALSE;
		}

		BOOL flag=FALSE;
		if(SOCKET_ERROR!=connect(sSocket, (struct sockaddr *)&addr, sizeof(addr)))
		{ 
		  flag=TRUE;
		} 
		WSACleanup();    
		return flag;
	}

	static BOOL net_PortScanC(CString ip,CString port)
	{
		int  err, nret;
		SOCKET sock;
		SOCKADDR_IN Info;
		WSADATA wsadata;
  
		err = WSAStartup(MAKEWORD(2, 0), &wsadata);
		if(err != 0) return FALSE;
		
		BOOL flag=FALSE;
		if(INVALID_SOCKET==(sock=socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)))
		{
			WSACleanup();  
			return FALSE;
		}

		Info.sin_family = AF_INET;
		Info.sin_port = htons(_ttoi(port));
		Info.sin_addr.s_addr = inet_addr((LPSTR)(LPCTSTR)ip);

		struct timeval Timeout;
		fd_set readfds;

		readfds.fd_count = 1;
		readfds.fd_array[0] = sock;
		Timeout.tv_sec = 0;//3
		Timeout.tv_usec = 0;

		if(SOCKET_ERROR==select(1, &readfds, NULL, NULL, &Timeout))
		{
			WSACleanup();  
			return FALSE;
		}
		if(SOCKET_ERROR!=(nret=connect(sock, (struct sockaddr *)&Info, sizeof(Info))))
		{
		  flag=TRUE;
		}     
		closesocket(sock);
   
		WSACleanup();  
		return flag;
	}

	static BOOL net_LanWakeOnA(CString strMac,CString sPort=_T("40000")) 
	{//WOL AfxSocketInit()		
		CAsyncSocket s;//Socket to send magic packet		
		BYTE magicP[102];//Buffer for packet
			
		//fill 6 Bytes with 0xFF
		for (int i=0;i<6;i++) magicP[i] = 0xff;

		//First 6 bytes (these must be repeated!!)
		//fill bytes 6-12
		for (int i=0;i<6;i++) 
		{	
			CString shex=GStr::str_GetSplit(strMac,i,'-');
			if(shex.IsEmpty() || shex==_T("")) continue;
	
			magicP[i+6] = GStr::str_Hex2IntB(shex);
		}

		//fill remaining 90 bytes (15 time repeat)
		for (int i=0;i<15;i++) memcpy(&magicP[(i+2)*6],&magicP[6],6);
		
		if(s.Create(_ttoi(sPort),SOCK_DGRAM)==FALSE)//Create a socket to send data
		{
			return FALSE;
		}
		
		//Customize socket to BROADCAST
		BOOL bOptVal = TRUE;
		if (s.SetSockOpt(SO_BROADCAST,(char*)&bOptVal,sizeof(BOOL))==SOCKET_ERROR) 
		{			
			return FALSE;
		}
		
		//Broadcast Magic Packet, Hope appropriate NIC will take it ;)
		BOOL bflag=FALSE;
		if(SOCKET_ERROR!=s.SendTo(magicP,102,_ttoi(sPort)))
		{
			bflag=TRUE;
		}
				
		s.Close();//Close the socket and release all buffers		
		return bflag;
	}

	static BOOL net_LanWakeOnB(CString strMac,CString sPort=_T("40000")) 
	{//WOL	  
	    WSADATA wsaData;
	    if(WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
	    {
		  //"WSAStartup() error!"; 
		  return FALSE;
	    }
		  
	    SOCKET hSendSock=socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP); //브로드캐스트를 위해 UDP소켓 생성 
	    if(hSendSock == INVALID_SOCKET)
	    {
		  //"socket() error");
		  return FALSE;
	    }
		
	    SOCKADDR_IN broadAddr;
	    memset(&broadAddr, 0, sizeof(broadAddr));
	    broadAddr.sin_family=AF_INET;
		
		#ifdef _UNICODE	
			broadAddr.sin_addr.s_addr=inet_addr("255.255.255.255");
		#else
		    broadAddr.sin_addr.s_addr=inet_addr(_T("255.255.255.255"));
		#endif

	    broadAddr.sin_port=htons(_ttoi(sPort));
	  
	    int so_broadcast=TRUE;
	    int nResult=setsockopt(hSendSock, SOL_SOCKET, SO_BROADCAST, 
		                     (char*)&so_broadcast, sizeof(so_broadcast));
	    if(nResult==SOCKET_ERROR)
	    {
		  //"setsockopt() error");
		  return FALSE;
	    }

		BYTE magicP[102]={0};//Buffer for packet
			
	   //fill 6 Bytes with 0xFF
	   for (int i=0;i<6;i++) magicP[i] = 0xff;

	   //First 6 bytes (these must be repeated!!)
	   //fill bytes 6-12
	   for (int i=0;i<6;i++) 
	   {	
	 	 CString shex=GStr::str_GetSplit(strMac,i,'-');
		 if(shex.IsEmpty() || shex==_T("")) continue;
	
		 magicP[i+6] = GStr::str_Hex2IntB(shex);
	   }

	   //fill remaining 90 bytes (15 time repeat)
	   for (int i=0;i<15;i++) memcpy(&magicP[(i+2)*6],&magicP[6],6);
		
	   BOOL bflag=FALSE;
	   nResult=sendto(hSendSock, (char*)magicP, 102, 0, (SOCKADDR*)&broadAddr, sizeof(broadAddr));	  
	   if(SOCKET_ERROR!=nResult) bflag=TRUE;

	   closesocket(hSendSock);
	   WSACleanup();
	   return bflag;
	}

	static int net_ChkLanModem() 
	{//
		DWORD Status;
		BOOL bConnectedStatus = InternetGetConnectedState(&Status, NULL);

		
		if( Status & INTERNET_CONNECTION_MODEM)  return 1;//모뎀 연결된 경우		
		else if( Status&INTERNET_CONNECTION_LAN) return 2;//랜 연결된 경우
  
		return -1;
	}

	static BOOL net_ConnLanModem() 
	{//
		DWORD Status;
		BOOL bConnectedStatus = InternetGetConnectedState(&Status, NULL);

		return bConnectedStatus;//인터넷 연결여부
	}*/

} ;