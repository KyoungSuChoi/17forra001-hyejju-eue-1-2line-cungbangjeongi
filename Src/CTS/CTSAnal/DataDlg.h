#if !defined(AFX_DATADLG_H__43D93504_DE61_11D5_A021_00010282CC7B__INCLUDED_)
#define AFX_DATADLG_H__43D93504_DE61_11D5_A021_00010282CC7B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DataDlg.h : header file
//

#include "CTSAnalDoc.h"
#include "GraphWnd.h"
#include "ModuleRecordSet.h"
#include "TestRecordSet.h"
#include "ChValueRecordSet.h"
#include "ChResultSet.h"
#include "MyGridWnd.h"
#include "PEGRPAPI.h"
#include "pemessag.h"

class CCTSAnalView;

#define  FAILCODENO    30
#define  FAILCODE      1

typedef struct tagField{
	FIELD	Field;
	long	nCellNo;
	double  dAve;
	double  dMax;
	double  dMin;
	double  dSTD;
}DATAFIELD;
/////////////////////////////////////////////////////////////////////////////
// CDataDlg dialog

class CDataDlg : public CDialog
{
// Construction
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	CString m_strTitle;
	CDatabase *m_pDBServer;
	CString m_strFilterString;
	int m_nFilterType;
	void UpdateFailGrid();
	BOOL UpdateDisplayData(int nTotInput, int nNormal);

	CDataDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDataDlg();

// Dialog Data
	//{{AFX_DATA(CDataDlg)
	enum { IDD = IDD_DATA_FORM };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA
	CModuleRecordSet         m_Module;
	CTestRecordSet           m_Test;
	CChValueRecordSet        m_ChValue;
	CChResultSet             m_ChResult;
	CMyGridWnd               m_DataGrid;
	CMyGridWnd               m_ErrorGrid;
//	CGraphWnd                m_GraphWnd;
    CCTSAnalView         *m_pView;
	CCTSAnalDoc          *m_pDoc;
	long				m_nTotInput;
	long                m_nNormalcell;
//	double              m_Error[256];
	int                m_Half;                 //총 Colum수의 반
	
	HWND        m_hWndPE;
	long        m_Fault[256];
	FIELD		m_Field[FIELDNO];
	DATAFIELD   m_dataField[FIELDNO];
	int			m_nFieldNo;
	LONG		m_FailCount[3][256];					//각 공정별 Code 집계 ==> Formation, IR/OCV, 출하 충전 

	BOOL   Create();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDataDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	void   InitDataGrid();
	void   InitErrorGrid();
	void   InitGraph();
	void   InitField();
	BOOL   ErrorSearch();
	BOOL   DataSearch();
	void   GraphUpdate();
	BOOL   ErrorSearch(int nType = 3);

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDataDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnClose();
	afx_msg void OnSaveFile();
	afx_msg void OnSelchangeProcSelect();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATADLG_H__43D93504_DE61_11D5_A021_00010282CC7B__INCLUDED_)
