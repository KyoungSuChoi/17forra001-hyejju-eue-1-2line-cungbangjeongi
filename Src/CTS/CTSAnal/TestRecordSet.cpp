// TestRecordSet.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "TestRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTestRecordSet

IMPLEMENT_DYNAMIC(CTestRecordSet, CRecordset)

CTestRecordSet::CTestRecordSet(CDatabase* pdb)
	: CRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CTestRecordSet)
	m_TestIndex = 0;
	m_ProcedureID = 0;
	m_TestSerialNo = _T("");
	m_StepNo = 0;
	m_StepType = 0;
//	m_TestResultFileName = _T("");
	m_Average = 0.0f;
	m_MaxVal = 0.0f;
	m_MaxChIndex = 0;
	m_MinVal = 0.0f;
	m_MinChIndex = 0;
	m_STDD = 0.0f;
	m_NormalNo = 0;
//	m_ErrorRate = 0.0f;
	m_ProgressID = 0;
	m_TotalChannel = 0;
	m_DataType = 0;
//	m_CellNo = 0;
	m_Temperature = 0;
	m_Gas = 0;
	m_ProcType = 0;
	m_nFields = 19;
	//}}AFX_FIELD_INIT
	m_nDefaultType = snapshot;
}


CString CTestRecordSet::GetDefaultConnect()
{
	return _T("ODBC;DSN=CTSProduct");
}

CString CTestRecordSet::GetDefaultSQL()
{
	return _T("[Test]");
}

void CTestRecordSet::DoFieldExchange(CFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CTestRecordSet)
	pFX->SetFieldType(CFieldExchange::outputColumn);
	RFX_Long(pFX, _T("[TestIndex]"), m_TestIndex);
	RFX_Long(pFX, _T("[ProcedureID]"), m_ProcedureID);
	RFX_Text(pFX, _T("[TestSerialNo]"), m_TestSerialNo);
	RFX_Long(pFX, _T("[StepNo]"), m_StepNo);
	RFX_Long(pFX, _T("[StepType]"), m_StepType);
	RFX_Date(pFX, _T("[DateTime]"), m_DateTime);
//	RFX_Text(pFX, _T("[TestResultFileName]"), m_TestResultFileName);
	RFX_Single(pFX, _T("[Average]"), m_Average);
	RFX_Single(pFX, _T("[MaxVal]"), m_MaxVal);
	RFX_Long(pFX, _T("[MaxChIndex]"), m_MaxChIndex);
	RFX_Single(pFX, _T("[MinVal]"), m_MinVal);
	RFX_Long(pFX, _T("[MinChIndex]"), m_MinChIndex);
	RFX_Single(pFX, _T("[STDD]"), m_STDD);
	RFX_Long(pFX, _T("[NormalNo]"), m_NormalNo);
//	RFX_Single(pFX, _T("[ErrorRate]"), m_ErrorRate);
	RFX_Long(pFX, _T("[ProgressID]"), m_ProgressID);
	RFX_Long(pFX, _T("[TotalChannel]"), m_TotalChannel);
	RFX_Long(pFX, _T("[DataType]"), m_DataType);
//	RFX_Long(pFX, _T("[CellNo]"), m_CellNo);
	RFX_Long(pFX, _T("[Temperature]"), m_Temperature);
	RFX_Long(pFX, _T("[Gas]"), m_Gas);
	RFX_Long(pFX, _T("[Gas]"), m_ProcType);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CTestRecordSet diagnostics

#ifdef _DEBUG
void CTestRecordSet::AssertValid() const
{
	CRecordset::AssertValid();
}

void CTestRecordSet::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG
