#if !defined(AFX_DETAILCHANNELVIEW_H__A8B5D777_2D75_11D4_850D_0060083FBBB6__INCLUDED_)
#define AFX_DETAILCHANNELVIEW_H__A8B5D777_2D75_11D4_850D_0060083FBBB6__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// DetailChannelView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDetailChannelView form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

#include "MyGridWnd.h"

class CDetailChannelView : public CFormView
{
protected:
	CDetailChannelView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CDetailChannelView)

// Form Data
public:
	//{{AFX_DATA(CDetailChannelView)
	enum { IDD = IDD_DETAIL_CHANNEL_VIEW };
	CLabel	m_TestUserT;
	CLabel	m_TestEditDayT;
	CLabel	m_TestDescriptionT;
	CXPButton	m_btnResetAutoProcess;
	CXPButton	m_btnRegTray;
	CXPButton	m_btnFileOptn;
	CXPButton	m_btnTestConView;
	CLabel		m_ctrlGas;
	CLabel		m_ctrlTemp;
	CLabel		m_AutoProcD;
	CLabel		m_AutoProcT;
	CLabel		m_ctrlErrorRate;
	CLabel		m_ctrlError;
	CLabel		m_ctrlNormal;
	CLabel		m_ModuleNoT;
	CLabel		m_ModuleNoD;
	CLabel		m_StateT;
	CLabel		m_StateD;
	CLabel		m_DoorStateT;
	CLabel		m_DoorStateD;
	CLabel		m_TestNameT;
	CLabel		m_TestNameD;
	CLabel		m_TrayNoT;
	CLabel		m_TrayNoD;
	CLabel		m_TotalStepT;
	CLabel		m_TotalStepD;
	CLabel		m_LotNoT;
	CLabel		m_LotNoD;
	CLabel		m_UserIDT;
	CLabel		m_UserIDD;
	CLabel		m_CellNumT;
	CLabel		m_CellNumD;
	CLabel		m_TotalTimeT;
	CLabel		m_TotalTimeD;
	CLabel		m_FileNameT;
	CLabel		m_FileNameD;
	BOOL	m_bAutoProcess;
	//}}AFX_DATA

// Attributes
public:
	CCTSMonDoc* GetDocument();
	void UpdateTreeState(int nModuleID, int nGroupIndex);
	void ModuleConnected(int nModuleID);
	void TopConfigChanged();

// Operations
public:
	int m_nCurTrayIndex;
	int m_nCurModuleID;
	void DataUnitChanged();
//	int m_nCurStep;
	void ActiveAutoReport();
	void StopMonitoring();
	void StartMonitoring();
	int GetCurModuleID();
	BOOL SetCurrentModule(int nModuleID, int nGroupIndex = 0);
	BOOL UpdateModuleSet();
	UINT m_nTrayColCount;
	UINT m_nTrayRowCount;
	void GroupSetChanged(int nColCount);
	BOOL ResetCmdFlag(int cmd);
	BOOL SetCmdFlag(int cmd);
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDetailChannelView)
	public:
	virtual void OnInitialUpdate();
	virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo = NULL);
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
protected:
	void GroupCellSetting(int nFromCh, int nToCh, COLORREF color = RGB(255, 0, 0), int nWidth = 2);
	int m_nCmdTarget;
	BOOL SendCommand(int nCmd, BOOL bChCmd = FALSE);
	UINT m_nDisplayTimer;
	void DrawDetailGrid();
	void SetGridColor();
	CMyGridWnd		m_wndDetailGrid;
	SECTreeCtrl		*m_pTreeCtrlX;
	STR_TOP_CONFIG *m_pConfig;
	void SetModuleData();
	int m_nCurGroup;
	void SetChannelData();
	void InitDetailChannelGrid();
	char *m_pDetalChColorFlag;
	virtual ~CDetailChannelView();

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CDetailChannelView)
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnChannelConditionButton();
	afx_msg void OnDestroy();
	afx_msg void OnRunModule();
	afx_msg void OnPause();
	afx_msg void OnContinueModule();
	afx_msg void OnStop();
	afx_msg void OnClear();
	afx_msg void OnStepover();
	afx_msg void OnUpdateRunModule(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePause(CCmdUI* pCmdUI);
	afx_msg void OnUpdateContinueModule(CCmdUI* pCmdUI);
	afx_msg void OnUpdateStop(CCmdUI* pCmdUI);
	afx_msg void OnUpdateClear(CCmdUI* pCmdUI);
	afx_msg void OnUpdateStepover(CCmdUI* pCmdUI);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnGetdispinfoChannelModuleTree(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnUpdateFileOpen(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFileSaveAs(CCmdUI* pCmdUI);
	afx_msg void OnAutoProcess();
	afx_msg void OnInitAtuoProcess();
	afx_msg void OnRegTray();
	afx_msg void OnReconnect();
	afx_msg void OnUpdateReconnect(CCmdUI* pCmdUI);
	afx_msg void OnTraynoUserInput();
	afx_msg void OnUpdateTraynoUserInput(CCmdUI* pCmdUI);
	afx_msg void OnMaintenanceMode();
	afx_msg void OnUpdateMaintenanceMode(CCmdUI* pCmdUI);
	afx_msg void OnReboot();
	afx_msg void OnFileSave();
	afx_msg void OnRefAdValueUpdate();
	afx_msg void OnUpdateRefAdValueUpdate(CCmdUI* pCmdUI);
	afx_msg void OnRclickChannelModuleTree(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDataViewButton();
	afx_msg void OnGetProfileData();
	afx_msg void OnUpdateGetProfileData(CCmdUI* pCmdUI);
	afx_msg void OnRadio1();
	afx_msg void OnRadio2();
	afx_msg void OnRadio6();
	afx_msg void OnRadio7();
	afx_msg void OnEditCopy();
	afx_msg void OnUpdateEditCopy(CCmdUI* pCmdUI);
	afx_msg void OnViewResult();
	afx_msg void OnUpdateViewResult(CCmdUI* pCmdUI);
	//}}AFX_MSG
	afx_msg LONG OnGridDoubleClick(WPARAM wParam, LPARAM lParam);
	afx_msg	LRESULT OnRButtonClickedRowCol(WPARAM wParam, LPARAM lParam);
//	afx_msg LONG OnGridClicked(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in CTSMonView.cpp
inline CCTSMonDoc* CDetailChannelView::GetDocument()
   { return (CCTSMonDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DETAILCHANNELVIEW_H__A8B5D777_2D75_11D4_850D_0060083FBBB6__INCLUDED_)
