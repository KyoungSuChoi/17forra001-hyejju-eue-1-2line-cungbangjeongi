// DataView.cpp : implementation file
//

#include "stdafx.h"
#include "CTSGraphAnal.h"
#include "DataDoc.h"
#include "DataView.h"

#include "MainFrm.h"
#include "DataFrame.h"
#include "UserSetDlg.h"

#include <math.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDataView

IMPLEMENT_DYNCREATE(CDataView, CFormView)

CDataView::CDataView()
	: CFormView(CDataView::IDD)
{
	//{{AFX_DATA_INIT(CDataView)
	//}}AFX_DATA_INIT
	m_pImagelist = NULL;
//	m_bShowChList = FALSE;
//	m_nRawGridWidth = -1;

	m_bInitedTooltip = FALSE;

	LanguageinitMonConfig();
}

CDataView::~CDataView()
{
	if( TEXT_LANG != NULL )					
	{					
		delete[] TEXT_LANG;					
		TEXT_LANG = NULL;
	}
	delete m_pImagelist;
	m_pImagelist = NULL;
}
void CDataView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDataView)
	DDX_Control(pDX, IDC_STEP_RAW_STATIC, m_ctrlCurStep);
	DDX_Control(pDX, IDC_COMBO1, m_StepSelCombo);
	DDX_Control(pDX, IDC_CH_TEST_INFO_LIST, m_ctrlTestInfoList);
	DDX_Control(pDX, IDC_TEST_TREE, m_TestTree);
	DDX_Control(pDX, IDC_STEP_LIST_CUSTOM, m_wndStepGrid);             // associate the grid window with a C++ object
	//}}AFX_DATA_MAP
//	DDX_Control(pDX, IDC_CH_LIST_CUSTOM, m_wndChListGrid);             // associate the grid window with a C++ object
	
}


BEGIN_MESSAGE_MAP(CDataView, CFormView)
	//{{AFX_MSG_MAP(CDataView)
	ON_WM_SIZE()
	ON_NOTIFY(TVN_SELCHANGED, IDC_TEST_TREE, OnSelchangedTestTree)
	ON_NOTIFY(NM_DBLCLK, IDC_STEP_LIST_CUSTOM, OnGridLButtonDblClk)
	ON_NOTIFY(NM_RCLICK, IDC_STEP_LIST_CUSTOM, OnGridRButtonUp)
	ON_BN_CLICKED(IDC_TEST_SEL_BUTTON, OnTestSelButton)
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, OnUpdateEditCopy)
	ON_COMMAND(ID_SELECT_ALL, OnSelectAll)
	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
	ON_NOTIFY(NM_RCLICK, IDC_TEST_TREE, OnRclickTestTree)
	ON_UPDATE_COMMAND_UI(ID_SELECT_ALL, OnUpdateSelectAll)
	ON_COMMAND(ID_EDIT_CLEAR, OnEditClear)
	ON_UPDATE_COMMAND_UI(ID_EDIT_CLEAR, OnUpdateEditClear)	
	ON_COMMAND(ID_EXCEL_STEP_END, OnExcelStepEnd)
	ON_COMMAND(ID_SEL_STEP, OnExcelSelStep)
	ON_UPDATE_COMMAND_UI(ID_SEL_STEP, OnUpdateExcelSelStep)
	ON_COMMAND(ID_ALL_STEP_DATA, OnExcelAllStepData)
	ON_UPDATE_COMMAND_UI(ID_ALL_STEP_DATA, OnUpdateExcelAllStepData)
	ON_COMMAND(ID_GRAPH_VIEW, OnGraphView)
	ON_BN_CLICKED(IDC_APPLY_BUTTON, OnApplyButton)
	ON_COMMAND(ID_STEP_START_DETAIL, OnStepStartDetail)
	ON_UPDATE_COMMAND_UI(ID_STEP_START_DETAIL, OnUpdateStepStartDetail)
	ON_COMMAND(ID_USER_OPTION, OnUserOption)
	ON_BN_CLICKED(IDC_EXCEL_DETAIL_BUTTON, OnExcelDetailButton)	
	ON_NOTIFY(NM_DBLCLK, IDC_TEST_TREE, OnDblclkTestTree)
	ON_NOTIFY(NM_RCLICK, IDC_STEP_RAW_CUSTOM, OnGridRButtonUp)
	//}}AFX_MSG_MAP

	ON_NOTIFY(NM_RCLICK, IDC_CH_LIST_CUSTOM, OnGridRButtonUp)
	ON_NOTIFY(GVN_SELCHANGED, IDC_CH_LIST_CUSTOM, OnGridSelectChanged)

END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDataView diagnostics

#ifdef _DEBUG
void CDataView::AssertValid() const
{
	CFormView::AssertValid();
}

void CDataView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CDataView message handlers

void CDataView::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();
	
	// TODO: Add your specialized code here and/or call the base class
//	m_bShowChList = AfxGetApp()->GetProfileInt(REG_CONFIG_SECTION, "CellListGrid", FALSE);
	m_nCurrentUnitMode = AfxGetApp()->GetProfileInt(REG_CONFIG_SECTION, "Crt Unit Mode", 0);

	CRect rect;
	GetClientRect(rect);
	OnSize(SIZE_RESTORED,rect.right,rect.bottom);

	
//	UpdateUnitSetting();

	m_pImagelist = new CImageList;
	ASSERT(m_pImagelist);
	m_pImagelist->Create(IDB_TEST_LIST_TREE_BITMAP, 18, 6, RGB(255,255,255));
	m_TestTree.SetImageList(m_pImagelist, TVSIL_NORMAL);

	m_StepSelCombo.AddString(TEXT_LANG[0]); //000599
	m_StepSelCombo.AddString(TEXT_LANG[1]); //000600
	m_StepSelCombo.AddString(TEXT_LANG[2]);	//000601
	m_StepSelCombo.AddString(TEXT_LANG[3]);	//000602
	m_StepSelCombo.AddString(TEXT_LANG[4]);	//000603
	m_StepSelCombo.AddString(TEXT_LANG[5]); //000604
	m_StepSelCombo.AddString(TEXT_LANG[6]);	//000605

	m_StepSelCombo.SetCurSel(0);
	
	InitTestInfoList();
	InitStepListCtrl();
//	InitRawDataGrid();
//	InitChListGrid();
	InitTooltip();
//	InitStepEndGrid();
	

	ListUpTree();
}

void CDataView::InitStepListCtrl()
{
//	m_pChStsLargeImage    = new CImageList;
//	m_pChStsSmallImage    = new CImageList;
//	m_pChStsLargeImage->Create(IDB_CH_STS_BIG_SIGN,32,5,RGB(255,255,255));
//	m_pChStsSmallImage->Create(IDB_CH_STS_SMALL_SIGN,16,6,RGB(255,255,255));
//	m_StepListCtrl.SetImageList(m_pChStsLargeImage,LVSIL_NORMAL);
// 	m_StepListCtrl.SetImageList(m_pChStsSmallImage,LVSIL_SMALL);

	//
	CString strTemp;
/*	m_StepListCtrl.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_TRACKSELECT);
//	m_StepListCtrl.SetHoverTime(10000);

	// Column 삽입

	m_StepListCtrl.InsertColumn(0, "Step No", LVCFMT_RIGHT, 50);
	m_StepListCtrl.InsertColumn(1, "Type", LVCFMT_RIGHT,	100);
	m_StepListCtrl.InsertColumn(2, "Votlage(mV)", LVCFMT_RIGHT, 70);
	m_StepListCtrl.InsertColumn(3, "Current(mA)", LVCFMT_RIGHT, 70);

#ifdef _EDLC_TEST_SYSTEM
	strTemp = "Capacity(F)";
#else
	strTemp = "Capacity(mAh)";
#endif
	m_StepListCtrl.InsertColumn(4, strTemp, LVCFMT_RIGHT, 70);
	m_StepListCtrl.InsertColumn(5, "Watt(mW)", LVCFMT_RIGHT, 70);
*/

	
// 	m_ImageList.Create(MAKEINTRESOURCE(IDB_IMAGES), 16, 1, RGB(255,255,255));
//	m_wndStepGrid.SetImageList(&m_ImageList);

	m_wndStepGrid.EnableDragAndDrop(FALSE);
 	m_wndStepGrid.SetEditable(FALSE);
	m_wndStepGrid.SetVirtualMode(FALSE);
	
	//Cell의 색상 
	m_wndStepGrid.GetDefaultCell(FALSE, FALSE)->SetBackClr(RGB(0xFF, 0xFF, 0xE0));
//	m_wndStepGrid.GetDefaultCell(TRUE, FALSE)->SetBackClr(RGB(0xFF, 0xFF, 0xE0));
//	m_wndStepGrid.GetDefaultCell(FALSE, TRUE)->SetBackClr(RGB(0xFF, 0xFF, 0xE0));
//	m_wndStepGrid.GetDefaultCell(TRUE, TRUE)->SetBackClr(RGB(0xFF, 0xFF, 0xE0));

   
	//여러 Row와 Column을 동시에 선택 가능하도록 한다.
	m_wndStepGrid.SetSingleRowSelection(FALSE);
	m_wndStepGrid.SetSingleColSelection(FALSE);

	m_wndStepGrid.SetRowCount(1); 
    m_wndStepGrid.SetColumnCount(9); 

	//Header 부분을 각각 1칸씩 사용한다.
	m_wndStepGrid.SetFixedRowCount(1); 
    m_wndStepGrid.SetFixedColumnCount(1); 

	//크기 조정을 사용자가 못하도록 한다.
//	m_wndStepGrid.SetColumnResize(FALSE);
//	m_wndStepGrid.SetRowResize(FALSE);

	m_wndStepGrid.SetItemText(0, 0, TEXT_LANG[7]); //000606				//1. No, 2.IndexFrom, 3.IndexTo,										
	m_wndStepGrid.SetItemText(0, 1, TEXT_LANG[8]);	//000607			//4. CurCycle,//5. TotalCycle,
	m_wndStepGrid.SetItemText(0, 2, TEXT_LANG[9]);		//000608		//6. Type,
	m_wndStepGrid.SetItemText(0, 3, TEXT_LANG[10]);	    //000609			//8. Code, 7, State,

	strTemp = GetUnitString(PS_STEP_TIME);
	if(strTemp.IsEmpty())
	{
		strTemp = "Time";
	}
	else
	{
		strTemp.Format("Time(%s)", GetUnitString(PS_STEP_TIME)); 
	}
	m_wndStepGrid.SetItemText(0, 4, strTemp);			//8, Time,		//9. TotTime,
	
	strTemp.Format(TEXT_LANG[11], GetUnitString(PS_VOLTAGE)); //000610
	m_wndStepGrid.SetItemText(0, 5, strTemp);			//10. Voltage,

	strTemp.Format(TEXT_LANG[12], GetUnitString(PS_CURRENT)); //000611
	m_wndStepGrid.SetItemText(0, 6, strTemp);			//11. Current
		
	strTemp.Format(TEXT_LANG[13], GetUnitString(PS_CAPACITY)); //000612
	m_wndStepGrid.SetItemText(0, 7, strTemp);			//11. Capacity
		
	strTemp.Format(TEXT_LANG[14], GetUnitString(PS_WATT_HOUR)); //000613
	m_wndStepGrid.SetItemText(0, 8, strTemp);		//13. WattHour,		


	/*
#ifdef _EDLC_TEST_SYSTEM
	m_wndStepGrid.SetItemText(0, 9, "ESR(mOhm)");			//14. IR,										
#else
	m_wndStepGrid.SetItemText(0, 9, "Imp(mOhm)");			//14. IR,										
#endif


	m_wndStepGrid.SetItemText(0, 10, "등급");				//9. Grade,										
	*/														//15. Temp,Press,AvgVoltage,AvgCurrent,

/*	CString str; 
	m_wndStepGrid.SetItemFormat(0, 0, DT_CENTER|DT_VCENTER|DT_SINGLELINE );
	m_wndStepGrid.SetColumnWidth(0, 20);
	for (int col = 1; col < 11; col++)
    {
		m_wndStepGrid.SetItemFormat(0, col, DT_RIGHT|DT_VCENTER|DT_SINGLELINE );
    }
*/	
	m_wndStepGrid.SetHeaderSort(FALSE);	//Sort기능 사용안함 
	m_wndStepGrid.ExpandColumnsToFit();	//폭을 균일 분배로 맞춤
//	m_wndStepGrid.ExpandRowsToFit();	//높이를 균일 분배로 맞춤

	m_wndStepGrid.Refresh();

//	m_wndStepGrid.SetDefCellMargin(30);
//	m_wndStepGrid.AutoSize();
}

void CDataView::OnSize(UINT nType, int cx, int cy) 
{
	CFormView::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here

	CRect rect, rectCtrl, rawGridRect;
	GetClientRect(&rect);

/*	int nOrgXPoint = 0;		//최소 기준 좌표 (설명 static의 우측 좌표 이용)
	if(GetDlgItem(IDC_DESCRIPT_STATIC)->GetSafeHwnd())
	{
		GetDlgItem(IDC_DESCRIPT_STATIC)->GetWindowRect(rectCtrl);
		ScreenToClient(&rectCtrl);
		nOrgXPoint = rectCtrl.left + rectCtrl.Width();
	}
	//일정크기 이상이 되면 m_wndRawDataGrid를 비례적으로 키워간다.
	TRACE("Width %d\n", rect.Width());
	if(rect.Width() > 1200)
	{
		if(m_wndRawDataGrid.GetSafeHwnd())
		{
			m_wndRawDataGrid.GetWindowRect(rawGridRect);
			ScreenToClient(&rawGridRect);
			m_wndRawDataGrid.MoveWindow(rawGridRect.left, rawGridRect.top, rawGridRect.Width()+(rect.Width()-1200)/10, rawGridRect.Height());		
		}
	}
	if(m_wndRawDataGrid.GetSafeHwnd())
	{
		m_wndRawDataGrid.GetWindowRect(rawGridRect);
		ScreenToClient(&rawGridRect);

		if(m_nRawGridWidth <= 0)
		{
			m_nRawGridWidth = rawGridRect.Width();
		}
		m_wndRawDataGrid.ExpandColumnsToFit();
	}


	int nOffSet = 150;
	if(m_wndStepGrid.GetSafeHwnd())
	{
		m_wndStepGrid.GetWindowRect(rectCtrl);
		ScreenToClient(&rectCtrl);

		m_wndStepGrid.MoveWindow(rectCtrl.left, rectCtrl.top, rect.right-rectCtrl.left- (nRawGridWidth + 10 +nOffSet), rect.bottom-rectCtrl.top-5);
	
		if(rect.right - (m_nRawGridWidth + 10 + 150) > nOrgXPoint)	nOffSet = 150;
		else if (rect.right - (m_nRawGridWidth + 10 + 100) > nOrgXPoint)	nOffSet = 100;
		else nOffSet = 0;

		
		if(rect.right - (m_nRawGridWidth + 10 + nOffSet) > nOrgXPoint)
		{
			m_wndStepGrid.MoveWindow(rectCtrl.left, rectCtrl.top, rect.right-rectCtrl.left- (m_nRawGridWidth + 10 +nOffSet), rectCtrl.Height());
				
			if(m_wndRawDataGrid.GetSafeHwnd())
			{
				m_wndRawDataGrid.GetWindowRect(rectCtrl);
				ScreenToClient(&rectCtrl);
				m_wndRawDataGrid.MoveWindow(rect.right-m_nRawGridWidth-5-nOffSet, rectCtrl.top, m_nRawGridWidth+nOffSet, rectCtrl.Height());
				
				GetDlgItem(IDC_STEP_RAW_STATIC)->GetWindowRect(rectCtrl);
				ScreenToClient(&rectCtrl);
				GetDlgItem(IDC_STEP_RAW_STATIC)->MoveWindow(rect.right-m_nRawGridWidth-5-nOffSet, rectCtrl.top, rectCtrl.Width(), rectCtrl.Height());
				GetDlgItem(IDC_DETAIL_STEP_START_BUTTON)->GetWindowRect(rectCtrl);
				ScreenToClient(&rectCtrl);
				GetDlgItem(IDC_DETAIL_STEP_START_BUTTON)->MoveWindow(rect.right-rectCtrl.Width()-5, rectCtrl.top, rectCtrl.Width(), rectCtrl.Height());
			}
					
			if(GetDlgItem(IDC_CROSS_BAR_STATIC)->GetSafeHwnd())
			{
				GetDlgItem(IDC_CROSS_BAR_STATIC)->GetWindowRect(rectCtrl);
				ScreenToClient(&rectCtrl);
				GetDlgItem(IDC_CROSS_BAR_STATIC)->MoveWindow(rectCtrl.left, rectCtrl.top, rect.right-m_nRawGridWidth-10-15-nOffSet, rectCtrl.Height());
			}
		}
		m_wndStepGrid.ExpandColumnsToFit();
	}

	if(m_wndChListGrid.GetSafeHwnd())
	{
		m_wndChListGrid.GetWindowRect(rectCtrl);
		ScreenToClient(&rectCtrl);
		if(rect.right-m_nRawGridWidth-10-nOffSet > nOrgXPoint)
			m_wndChListGrid.MoveWindow(rectCtrl.left, rectCtrl.top, rect.right-m_nRawGridWidth-10-rectCtrl.left-nOffSet, rectCtrl.Height());
		
		m_wndChListGrid.GetWindowRect(rectCtrl);
		ScreenToClient(&rectCtrl);
		if( rect.bottom-rectCtrl.top-5 > 100)
		{
			m_wndChListGrid.MoveWindow(rectCtrl.left, rectCtrl.top, rectCtrl.Width(), rect.bottom-rectCtrl.top-5);
			
			m_wndRawDataGrid.GetWindowRect(rectCtrl);
			ScreenToClient(&rectCtrl);
			m_wndRawDataGrid.MoveWindow(rectCtrl.left, rectCtrl.top, rectCtrl.Width(), rect.bottom-rectCtrl.top-5);
		}
	}	
*/
	if(m_TestTree.GetSafeHwnd())// && m_bShowChList == FALSE)
	{
		m_TestTree.GetWindowRect(rectCtrl);
		ScreenToClient(&rectCtrl);
		m_TestTree.MoveWindow(rectCtrl.left, rectCtrl.top, rectCtrl.Width(), rect.bottom-rectCtrl.top-5);
	}

	if(m_wndStepGrid.GetSafeHwnd())// && m_bShowChList == FALSE)
	{
		m_wndStepGrid.GetWindowRect(rectCtrl);
		ScreenToClient(&rectCtrl);
		m_wndStepGrid.MoveWindow(rectCtrl.left, rectCtrl.top, rect.right-rectCtrl.left-5, rect.bottom-rectCtrl.top-5);
	}
}

BOOL CDataView::UpdateDataList()
{
//	CData *pData = &(GetDocument()->m_Data);
//	LONG	lDataCount;
//	pData->GetDataOfTable(lDataCount, );
//
	return TRUE;
}

void CDataView::ListUpTree()
{

//	CProgressWnd *pWndProgress = new CProgressWnd;
//	pWndProgress->Create(AfxGetApp()->m_pMainWnd, "Test", TRUE);
//	pWndProgress->Show();

	TV_INSERTSTRUCT		tvstruct;
	ZeroMemory(&tvstruct, sizeof(TV_INSERTSTRUCT));
	HTREEITEM	hItem1, hItem2;
	char szName[_MAX_PATH];

	m_TestTree.DeleteAllItems();

//	BOOL bContinueChNo = AfxGetApp()->GetProfileInt(REG_CONFIG_SECTION, "ChNumberMode", 0);
	CFileFind chFinder;
	CString strName, strTemp;
	int nCount = 0, nCount2 = 0;

	CDataDoc *pDoc = (CDataDoc *)GetDocument();
	CStringList *pSelTestList = pDoc->GetSelFileList();		//FullPathName
	if(pSelTestList == NULL)	return;

	POSITION pos = pSelTestList->GetHeadPosition();
	BeginWaitCursor();
		
	while (pos)
	{
		strName = pSelTestList->GetNext(pos);
	
		tvstruct.hParent = NULL;
		tvstruct.hInsertAfter = TVI_LAST;
		tvstruct.item.iImage = 0;
		tvstruct.item.iSelectedImage = 1;
		sprintf(szName, "%s", strName.Mid(strName.ReverseFind('\\')+1));		//뒷부분 시험명만 표시한다.
		tvstruct.item.pszText = szName;		//Display Module ID String
		tvstruct.item.cchTextMax = 64;
		tvstruct.item.mask = TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_TEXT;
		hItem1 = m_TestTree.InsertItem(&tvstruct);
		if(hItem1)
			m_TestTree.SetItemData(hItem1, (DWORD)nCount++);			//Set Tree Data to File List Index

		nCount2 = 0;
		ZeroMemory(&tvstruct, sizeof(TV_INSERTSTRUCT));

		strTemp.Format("%s\\%s", strName, FILE_FIND_FORMAT);
		
		BOOL bWorking1 = chFinder.FindFile(strTemp);
		
		while (bWorking1 && hItem1)
		{
//			pWndProgress->SetPos(nCount2);

			bWorking1 = chFinder.FindNextFile();
			if(chFinder.IsDots())	continue;
			if (chFinder.IsDirectory())	continue;

			
				sprintf(szName, "%s", chFinder.GetFilePath());
				//bCellOK = ((CDataDoc *)GetDocument())->GetCellLastState(szName);

				tvstruct.hParent = hItem1;
				tvstruct.hInsertAfter = TVI_LAST;
				//if(bCellOK)
				//{
					tvstruct.item.iImage = 6;
					tvstruct.item.iSelectedImage = 7;
				//}
				//else
				//{
				//	tvstruct.item.iImage = 4;
				//	tvstruct.item.iSelectedImage = 5;
				//}
				
				strTemp = chFinder.GetFileName();
				sprintf(szName, "%s", strTemp);
				tvstruct.item.pszText = szName;		//Display Module ID String
				tvstruct.item.cchTextMax = 64;
				tvstruct.item.mask = TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_TEXT;
				hItem2 = m_TestTree.InsertItem(&tvstruct);
				if(hItem2)
					m_TestTree.SetItemData(hItem2, (DWORD)nCount2++);			//Set Tree Data to Module ID
			
		}
	}
	
	hItem1 = m_TestTree.GetFirstVisibleItem();
	if(hItem1)	m_TestTree.Expand(hItem1, TVE_EXPAND);
	EndWaitCursor();

//	delete pWndProgress;


//	int		iItem, jItem;
//	TV_INSERTSTRUCT		tvstruct;
//	HTREEITEM	hMdItem, hGpItem, hTempItem;
//	int nModuleID;
//	CString		strTreeTitle;
//	pTreeX->DeleteAllItems();
//	ASSERT(pTreeX->GetCount() == m_nInstalledModuleNum);
//	m_rghItem = pTreeX->GetNextVisibleItem(m_rghItem);	//Select Next Group
//	TVGN_ROOT   
//	m_hItem = pTreeX->GetNextItem(m_hItem,  TVGN_FIRSTVISIBLE );
//	hMdItem = pTreeX->GetRootItem();
//	while(hMdItem)
//	{
//		nModuleID = pTreeX->GetItemData(hMdItem);
//		if(nSelModuleID < 0 || nSelModuleID == nModuleID)
//		{
//			int nGroupCount = EPGetGroupCount(nModuleID);
//		
//			hGpItem = pTreeX->GetChildItem(hMdItem);
//			
//			for(int i =0; i<nGroupCount; i++)
//			{
//				if(hGpItem == NULL)
//				{
//					strTreeTitle.Format("Group %d ", i+1);	
//					tvstruct.hParent = hMdItem;
//					tvstruct.hInsertAfter = TVI_LAST;
//					tvstruct.item.pszText = (char *)(LPCTSTR)strTreeTitle;
//					tvstruct.item.cchTextMax = 64;
//					tvstruct.item.lParam = i;
//					tvstruct.item.iImage = I_IMAGECALLBACK;
//					tvstruct.item.iSelectedImage = I_IMAGECALLBACK;
//					tvstruct.item.mask = TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_TEXT;
//					hGpItem = pTreeX->InsertItem(&tvstruct);
//					pTreeX->SetItemData(hGpItem, (DWORD)i);
//				}
//				else
//				{
//					pTreeX->SetItemData(hGpItem, (DWORD)i);
//				}
//				pTreeX->HideItem(hGpItem, !m_bUseGroupSet);
//				
//				hGpItem = pTreeX->GetNextItem(hGpItem, TVGN_NEXT);
//			}
//			
//			if(hGpItem)
//			{
//				hTempItem = pTreeX->GetNextItem(hGpItem, TVGN_NEXT);
//				while(hTempItem)
//				{
//					pTreeX->DeleteItem(hTempItem);
//					hTempItem = pTreeX->GetNextItem(hGpItem, TVGN_NEXT);
//				}
//				if(pTreeX->DeleteItem(hGpItem) ==0)
//				{
//					TRACE("Tree Item Delete Error\n");
//				}
//			}
//		}
//		hMdItem = pTreeX->GetNextItem(hMdItem,  TVGN_NEXT );
//	}
//	pTreeX->Invalidate();
}

//		POSITION ChPos = m_QueryCondition.GetChPathHead();
//		while(ChPos)
//		{
//			CString str;
//			//채널 번호 기록 
//			CString ChPath = m_QueryCondition.GetNextChPath(ChPos);
//			str.Format("\n%s,\n",ChPath.Mid(ChPath.ReverseFind('\\')+1));
//			afile.WriteString(str);
//
//			// 해당채널의 라인을 각 Plane에서 찾아서 List를 만든다.
//			CTypedPtrList<CPtrList, CLine*> TempLineList;
//			POSITION PlanePos = m_PlaneList.GetHeadPosition();
//			int n=0;
//			while(PlanePos)
//			{
//				CPlane* pPlane = m_PlaneList.GetNext(PlanePos);
//				if(n==0)
//				{
//					str.Format("%s(%s),", pPlane->GetXAxis()->GetTitle(), pPlane->GetXAxis()->GetUnitNotation());
//					afile.WriteString(str);
//				}
//				str.Format("%s(%s),", pPlane->GetYAxis()->GetTitle(), pPlane->GetYAxis()->GetUnitNotation());
//				afile.WriteString(str);
//						//
//				TempLineList.AddTail(pPlane->FindLineOf(ChPath, 0, 0));
//				n++;
//			}
//			afile.WriteString("\n");
//
//			// Line 내 Data 개수 검증
//			// 제일 첫번째 라인 Data수와 나머지 라인 Data수 가 같은지 확인 한다.
//			int N = 0;
//			BOOL bCheck = TRUE;
//			POSITION TempPos = TempLineList.GetHeadPosition();
//			n = 0;
//			while(TempPos)
//			{
//				CLine* pLine = TempLineList.GetNext(TempPos);
//				if(pLine!=NULL)
//				{
//					if(n==0) N = pLine->GetDataCount();
//					else
//					{
//						if(N!=pLine->GetDataCount())
//						{
//							bCheck = FALSE;
//							break;
//						}
//					}
//					n++;
//				}
//			}
//			
//			
//			//
//			if(N>0&&bCheck)
//			{
//				for(int i=0; i<N; i++)
//				{
//					if( (i % nSaveInterval != 0) && i )
//							continue;
//
//							// X축 값 적기
//					TempPos = TempLineList.GetHeadPosition();
//					n=0;
//					while(TempPos)
//					{
//						CLine* pLine = TempLineList.GetNext(TempPos);
//						if(pLine!=NULL)
//						{
//							str.Format("%.3f,",m_PlaneList.GetAt(m_PlaneList.FindIndex(n))->GetXAxis()->ConvertUnit(pLine->GetDataAt(pLine->FindDataIndex(i)).x));
//							afile.WriteString(str);
//							break;
//						}
//						n++;
//					}
//					// 각 Plane의 Y축 값 적기
//					TempPos = TempLineList.GetHeadPosition();
//					n=0;
//					while(TempPos)
//					{
//						CLine* pLine = TempLineList.GetNext(TempPos);
//						if(pLine!=NULL)
//						{
//							str.Format("%.3f,",m_PlaneList.GetAt(m_PlaneList.FindIndex(n))->GetYAxis()->ConvertUnit(pLine->GetDataAt(pLine->FindDataIndex(i)).y));
//							afile.WriteString(str);
//						}
//						else afile.WriteString(",");
//							n++;
//					}
//							//
//					afile.WriteString("\n");
//				}
//			}
//		}


void CDataView::UpdateStepData(CString strChPath)
{
	CChData *pData = ((CDataDoc *)GetDocument())->GetChData();
	if(pData->IsLoadedData() == FALSE)	return;

	CString strName;
	CString strTemp;
	char szBuff[128];
	

/*	strTemp = GetUnitString(PS_STEP_TIME);
	if(strTemp.IsEmpty())
	{
		strTemp = "Time";
	}
	else
	{
		strTemp.Format("Time(%s)", GetUnitString(PS_VOLTAGE));
	}
	m_wndStepGrid.SetItemText(0, 4, strTemp);			//8, Time,		//9. TotTime,
	
	strTemp.Format("Volt(%s)", GetUnitString(PS_VOLTAGE));
	m_wndStepGrid.SetItemText(0, 5, strTemp);			//10. Voltage,

	strTemp.Format("Crt(%s)", GetUnitString(PS_CURRENT));
	m_wndStepGrid.SetItemText(0, 6, strTemp);			//11. Current
		
	strTemp.Format("Capa(%s)", GetUnitString(PS_CAPACITY));
	m_wndStepGrid.SetItemText(0, 7, strTemp);			//11. Current
		
	strTemp.Format("WattHour(%s)", GetUnitString(PS_WATT_HOUR));
	m_wndStepGrid.SetItemText(0, 8, strTemp);		//13. WattHour,		
*/
	
	//Schedule Information display
	CScheduleData *pSchData = pData->GetPattern();
	BOOL bShowImpColumn = TRUE;
	BOOL bShowGradeColumn = FALSE;
	if(pSchData)
	{	
		//Impedance와 Grading은 해당 step이 존재할 경우만 Column을 표시한다.
		if(!pSchData->IsThereImpStep())
		{
			bShowImpColumn = FALSE;
		}
		if(!pSchData->IsThereGradingStep())
		{
			bShowGradeColumn = FALSE;
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//remake grid
	m_wndStepGrid.DeleteAllItems();
	m_wndStepGrid.SetRowCount(1); 
    m_wndStepGrid.SetColumnCount(9); 

	//Header 부분을 각각 1칸씩 사용한다.
	m_wndStepGrid.SetFixedRowCount(1); 
    m_wndStepGrid.SetFixedColumnCount(1); 

	m_wndStepGrid.SetItemText(0, 0, "Step");				//1. No, 2.IndexFrom, 3.IndexTo,										
	m_wndStepGrid.SetItemText(0, 1, "RunTime");				//4. CurCycle,//5. TotalCycle,
	m_wndStepGrid.SetItemText(0, 2, "Type");				//6. Type,
	m_wndStepGrid.SetItemText(0, 3, "State");				//8. Code, 7, State,

	strTemp = GetUnitString(PS_STEP_TIME);
	if(strTemp.IsEmpty())
	{
		sprintf(szBuff, "%s", "Time");
	}
	else
	{
		sprintf(szBuff, "Time(%s)", GetUnitString(PS_STEP_TIME));
	}
	m_wndStepGrid.SetItemText(0, 4, szBuff);			//8, Time,		//9. TotTime,
	
	sprintf(szBuff, "Voltage(%s)", GetUnitString(PS_VOLTAGE));
	m_wndStepGrid.SetItemText(0, 5, szBuff);			//10. Voltage,

	sprintf(szBuff, "Current(%s)", GetUnitString(PS_CURRENT));
	m_wndStepGrid.SetItemText(0, 6, szBuff);			//11. Current
		
	sprintf(szBuff, "Capacity(%s)", GetUnitString(PS_CAPACITY));
	m_wndStepGrid.SetItemText(0, 7, szBuff);			//11. Capacity
		
	sprintf(szBuff, "WattHour(%s)", GetUnitString(PS_WATT_HOUR));
	m_wndStepGrid.SetItemText(0, 8, szBuff);		//13. WattHour,	


	float fImpTime1 = atof(AfxGetApp()->GetProfileString(REG_CONFIG_SECTION, "ImpTime1", "0.0"));
	float fImpTime2 = atof(AfxGetApp()->GetProfileString(REG_CONFIG_SECTION, "ImpTime2", "0.01"));
	BOOL bUserImpTime = AfxGetApp()->GetProfileInt(REG_CONFIG_SECTION, "UserImp", FALSE);
	
	bShowImpColumn = FALSE;		//formation
	if(bShowImpColumn)
	{
		#ifdef _EDLC_TEST_SYSTEM
			strTemp = "ESR(mOhm)";			//14. IR,										
		#else
			strTemp = "Imp(mOhm)";			//14. IR,										
		#endif
		m_wndStepGrid.InsertColumn(strTemp);
		
	}

	if(bShowGradeColumn)
	{
		m_wndStepGrid.InsertColumn("Grade");
	}


	m_wndStepGrid.ExpandColumnsToFit();
//////////////////////////////////////////////////////////////////////////

//	int nDataCount = pData->m_TableList.GetCount();
	int nDisplayStep = m_StepSelCombo.GetCurSel();
	
//	m_wndStepGrid.SetRowCount(nDataCount+1);
	int nTableSize = pData->m_TableList.GetCount();

	float fData, fDataVtg, fDataCrt, fCapacitySum = 0.0f;
	long nStepNo;
	WORD type;
	long totCycle;
	COLORREF textColor, bkColor;
	
	int nRow = 0;
	int nColumn = 0;

	CProgressWnd *pProgressWnd = new CProgressWnd;
	pProgressWnd->Create(this, "Loading...", TRUE);
//	pProgressWnd->SetColor(RGB(100,100,100));
//	pProgressWnd->Clear();
	pProgressWnd->SetRange(0, 100);
	pProgressWnd->SetText("Data Loading...");
	pProgressWnd->SetPos(0);
	pProgressWnd->Show();
	pProgressWnd->GoModal();
	
	m_wndStepGrid.DeleteNonFixedRows();
	
//	DWORD dwstart = GetTickCount();
//	dwstart = GetTickCount();

	//검색 속도 향상 version
// 	/*♠*/ CStatusBar* pStatusBar = (CStatusBar*)(((CMDIFrameWnd*)AfxGetMainWnd())->GetDlgItem(AFX_IDW_STATUS_BAR));
// 	/*♠*/ CProgressCtrl ProgressCtrl;
// 	/*♠*/ CRect rect;
// 	/*♠*/ pStatusBar->GetItemRect(0,rect);
// 	/*♠*/ rect.left = rect.Width()/2;
// 	/*♠*/ ProgressCtrl.Create(WS_CHILD|WS_VISIBLE,rect,pStatusBar,777);
// 	/*♠*/ ProgressCtrl.SetRange(0, 100);
// 	/*♠*/ ((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText("Index searching......");

	CDWordArray	tableList;
	POSITION pos = pData->GetFirstTablePosition();
	long a = 0;
	CTable *tb;
	while(pos)
	{
//		ProgressCtrl.SetPos(int(float(a)/(float)pData->GetTableSize()*100.0f));

		tb = pData->GetNextTable(pos);
		type = tb->GetType();		
		nStepNo = tb->GetStepNo();
		totCycle = (ULONG)tb->GetCycleNo();
		
		if(nDisplayStep == 1)	//충전 
		{
			if(type == PS_STEP_CHARGE)		tableList.Add(a);		
		}
		else if(nDisplayStep == 2 )	//방전
		{
			if(type == PS_STEP_DISCHARGE)	tableList.Add(a);
		}
		else if(nDisplayStep == 3 )	//충전&방전
		{
			if(type == PS_STEP_CHARGE || type == PS_STEP_DISCHARGE)	tableList.Add(a);
		}
		else if(nDisplayStep == 4 )	//Rest
		{
			if(type == PS_STEP_REST)				tableList.Add(a);
		}
		else if(nDisplayStep == 5 )	//Impedance
		{
			if(type == PS_STEP_IMPEDANCE)			tableList.Add(a);
		}
		else if(nDisplayStep == 6 )	//OCV
		{
			if(type == PS_STEP_OCV)					tableList.Add(a);
		}
		else
		{
			tableList.Add(a);
		}
		a++;
	}
	/*♠*/ ((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText("");

//	TRACE("5: %dmsec\n", GetTickCount()-dwstart);
//	dwstart = GetTickCount();

	int nSelTableSize = tableList.GetSize();
	//Table size가 너무 길어 쪼개서 출력 
	if(nSelTableSize > MAX_RECORD_ROW_COUNT)
	{
		pProgressWnd->Hide();
		
		CString msg;

		strTemp.Format(TEXT_LANG[16]); //000615
		msg += strTemp;
		strTemp.Format(TEXT_LANG[17], 	nSelTableSize, MAX_RECORD_ROW_COUNT);	//000616											
		msg += strTemp;
		strTemp.Format(TEXT_LANG[18], MAX_RECORD_ROW_COUNT); //000617											
		msg += strTemp;
		strTemp.Format(TEXT_LANG[19], MAX_RECORD_ROW_COUNT); //000618																
		msg += strTemp;
		strTemp.Format(TEXT_LANG[20]);	//000619														
		msg += strTemp;
		strTemp.Format(TEXT_LANG[21]);		//000620													 
		msg += strTemp;
	

		int nRtn = MessageBox(msg,	"OK", MB_YESNOCANCEL|MB_ICONWARNING);
		if(nRtn == IDYES)
		{
			for(int q=MAX_RECORD_ROW_COUNT; q<nSelTableSize; q++)
			{
				tableList.RemoveAt(MAX_RECORD_ROW_COUNT);
			}
			nSelTableSize = tableList.GetSize();
		
			//구간을 선택
			//////////////////////////////////////////////////////////////////////////
			strTemp.Format("1-%d", MAX_RECORD_ROW_COUNT);
			//////////////////////////////////////////////////////////////////////////
		}
		else if(nRtn == IDNO)
		{
			//////////////////////////////////////////////////////////////////////////
			m_StepSelCombo.SetCurSel(0);
			//////////////////////////////////////////////////////////////////////////
		}
		else 
		{
			delete 	pProgressWnd;
			return;
		}
		pProgressWnd->Show();
	}
	
	strTemp.Format("Step(%d/%d)", nSelTableSize, nTableSize);
	m_wndStepGrid.SetItemText(0, 0, strTemp);

	//중간 redraw message 처리시 닫기를 누르면 
	//프로그램 오류가 생기므로 닫기 버튼을 disable 시킴 
// 	CMenu* pMainSysMenu = AfxGetMainWnd()->GetSystemMenu(FALSE);
//     if (pMainSysMenu != NULL)
// 	{
//       pMainSysMenu->EnableMenuItem(SC_CLOSE,MF_BYCOMMAND | MF_DISABLED); // Disable
// 	}
// 
// 	CMenu* pSubSysMenu = ((CDataFrame *)GetParentFrame())->GetSystemMenu(FALSE);
// 	if(pSubSysMenu)
// 	{
//       pSubSysMenu->EnableMenuItem(SC_CLOSE,MF_BYCOMMAND | MF_DISABLED); // Disable
// 	}

	int i;
	for(int nSeqCount = 0; nSeqCount<nSelTableSize; nSeqCount++)
	{
		i = tableList.GetAt(nSeqCount);
		TRACE("Table %d\n", i);
		tb = pData->GetTableAt(i);
		if(tb == NULL)	continue;

		type = (WORD)tb->GetType();
		nStepNo = tb->GetStepNo();
		totCycle = (ULONG)tb->GetCycleNo();
		
		strTemp.Format("결과 Data를 검색하여 Loading 합니다.\n\nTable %d(Step %d) data loading...", i+1, nStepNo); //000621
		pProgressWnd->SetText(strTemp);
		pProgressWnd->SetPos(int((float)nSeqCount/(float)nSelTableSize*100.0f));
		
		//중간 redraw message 처리 
		//Loading과정을 보여줌 
		//중복 Loading시 문제가 있어 Commet처리함 2006/8/19
		if(pProgressWnd->PeekAndPump() == FALSE)
		{
			//Cancel button 처리
			break;
		}


/*		if(nDisplayStep == 1)	//충전 
		{
			if(type != PS_STEP_CHARGE)					continue;
		}
		else if(nDisplayStep == 2 )	//방전
		{
			if(type != PS_STEP_DISCHARGE)				continue;
		}
		else if(nDisplayStep == 3 )	//충전&방전
		{
			if(type != PS_STEP_CHARGE && type != PS_STEP_DISCHARGE)	continue;
		}
		else if(nDisplayStep == 4 )	//Rest
		{
			if(type != PS_STEP_REST)					continue;
		}
		else if(nDisplayStep == 5 )	//Impedance
		{
			if(type != PS_STEP_IMPEDANCE)				continue;
		}
		else if(nDisplayStep == 6)	//Step Range
		{
			if(m_RangeList.Find(nStepNo)==NULL)			continue;
		}
		else if(nDisplayStep == 7)	//Cycle Range
		{
			if(m_RangeList.Find(totCycle)==NULL)		continue;
		}
		else if(nDisplayStep == 8)	//Index range
		{
			lTemp = i+1;
			if(m_RangeList.Find(lTemp)==NULL)				continue;
		}
*/		
		//No
		nColumn = 0;
//#ifdef _DEBUG
		sprintf(szBuff, "%d(%d)", nStepNo, (ULONG)tb->GetLastData(RS_COL_SEQ));
//#else
//		sprintf(szBuff, "%d", nStepNo);
//#endif
		m_wndStepGrid.InsertRow(szBuff);
		nRow++;
		m_wndStepGrid.SetItemData(nRow, 0, i);
		m_wndStepGrid.SetItemFormat(nRow, nColumn, DT_CENTER|DT_VCENTER|DT_SINGLELINE );

		//cycle//CurCycle,TotalCycle
		nColumn++;
//		sprintf(szBuff, "%d/%d", (ULONG)tb->GetLastData(RS_COL_CUR_CYCLE), totCycle);	//PScommon.dll
		sprintf(szBuff, "%s", ValueString(tb->GetLastData(RS_COL_TOT_TIME), PS_STEP_TIME));	//PScommon.dll
		m_wndStepGrid.SetItemText(nRow, nColumn, szBuff);
		m_wndStepGrid.SetItemFormat(nRow, nColumn, DT_CENTER|DT_VCENTER|DT_SINGLELINE );

		//Type
		nColumn++;
		m_wndStepGrid.SetItemText(nRow, nColumn, ::PSGetTypeMsg(type));
		m_wndStepGrid.SetItemFormat(nRow, nColumn, DT_RIGHT|DT_VCENTER|DT_SINGLELINE );
		::PSGetTypeColor(type, textColor, bkColor);		//pscommon.dll
		m_wndStepGrid.SetItemBkColour(nRow, nColumn, bkColor);
		m_wndStepGrid.SetItemFgColour(nRow, nColumn, textColor);
		
		//Code
		nColumn++;
//		::PSCellCodeMsg((BYTE)tb->GetLastData(RS_COL_CODE), strName, strTemp);	//Pscommon.dll API
//		strName = ::PSGetStateMsg((WORD)tb->GetLastData(RS_COL_STATE), (WORD)tb->GetLastData(RS_COL_TYPE));	//Pscommon.dll API
//		if(type == PS_STEP_END)		
			strName.Empty();
		m_wndStepGrid.SetItemText(nRow, nColumn, strName);
		m_wndStepGrid.SetItemFormat(nRow, nColumn, DT_RIGHT|DT_VCENTER|DT_SINGLELINE );

		//Time
		nColumn++;
		strName = ValueString(tb->GetLastData("Time"), PS_STEP_TIME);
		
		if(type == PS_STEP_END)
		{
			strName.Format("Sum: %s", ValueString(tb->GetLastData(RS_COL_TOT_TIME), PS_STEP_TIME));
		}
		m_wndStepGrid.SetItemText(nRow, nColumn, strName);
		m_wndStepGrid.SetItemFormat(nRow, nColumn, DT_RIGHT|DT_VCENTER|DT_SINGLELINE );
		
		//Voltage
		nColumn++;
		fDataVtg = tb->GetLastData(RS_COL_VOLTAGE);
		strName = ValueString(fDataVtg, PS_VOLTAGE);
			
		if(type == PS_STEP_END)		strName.Empty();
		m_wndStepGrid.SetItemText(nRow, nColumn, strName);
		m_wndStepGrid.SetItemFormat(nRow, nColumn, DT_RIGHT|DT_VCENTER|DT_SINGLELINE );

		//Current
		nColumn++;
		fDataCrt = tb->GetLastData(RS_COL_CURRENT);
		if(type == PS_STEP_REST || type == PS_STEP_OCV)
		{
			fDataCrt = 0;
#ifdef _DEBUG
			sprintf(szBuff, "%s(%.2f)", ValueString(fDataCrt, PS_CURRENT), fDataCrt);
#else
			sprintf(szBuff, "%s", ValueString(fDataCrt, PS_CURRENT));
#endif
		}
		else if(type == PS_STEP_CHECK || type == PS_STEP_CHARGE || type== PS_STEP_DISCHARGE || type == PS_STEP_IMPEDANCE || type == PS_STEP_BALANCE || type == PS_STEP_PATTERN)
		{
			sprintf(szBuff, "%s", ValueString(fDataCrt, PS_CURRENT));
		}
		else //if(type == PS_STEP_END)
		{
			sprintf(szBuff, "");
		}
		m_wndStepGrid.SetItemText(nRow, nColumn, szBuff);
		m_wndStepGrid.SetItemFormat(nRow, nColumn, DT_RIGHT|DT_VCENTER|DT_SINGLELINE );
		
		//Capacity
		nColumn++;
		fData = tb->GetLastData(RS_COL_CAPACITY);
		if(type == PS_STEP_CHARGE)
		{
			fCapacitySum += fData;
		}
		else if(type == PS_STEP_DISCHARGE)
		{
			fCapacitySum -= fData;
		}
		else if(type== PS_STEP_IMPEDANCE)
		{
			fCapacitySum -= fData;
		}

		if(type == PS_STEP_END)
		{
#ifndef _EDLC_TEST_SYSTEM
			sprintf(szBuff, "Sum: %s", ValueString(fCapacitySum, PS_CAPACITY));
#endif
		}
		else if(type == PS_STEP_CHARGE || type== PS_STEP_DISCHARGE)
		{
			sprintf(szBuff, "%s", ValueString(fData, PS_CAPACITY));
		}
		else
		{
#ifdef _DEBUG
			sprintf(szBuff, "0.0(%s)", ValueString(fData, PS_CAPACITY));
#else
			sprintf(szBuff, "0.0");
#endif
		}
		m_wndStepGrid.SetItemText(nRow, nColumn, szBuff);
		m_wndStepGrid.SetItemFormat(nRow, nColumn, DT_RIGHT|DT_VCENTER|DT_SINGLELINE );
		
		//WattHour
		nColumn++;
		fData = tb->GetLastData(RS_COL_WATT_HOUR);
		if(type == PS_STEP_END)
		{
			sprintf(szBuff, "");
		}
		else if(type == PS_STEP_CHARGE || type== PS_STEP_DISCHARGE || type == PS_STEP_BALANCE || type == PS_STEP_PATTERN)
		{
			sprintf(szBuff, "%s", ValueString(fData, PS_WATT_HOUR));
		}
		else
		{
#ifdef _DEBUG
			sprintf(szBuff, "0.0(%.1f)", fData);
#else
			sprintf(szBuff, "0.0");
#endif
		}
		m_wndStepGrid.SetItemText(nRow, nColumn, szBuff);
		m_wndStepGrid.SetItemFormat(nRow, nColumn, DT_RIGHT|DT_VCENTER|DT_SINGLELINE );
	
		if(bShowImpColumn)
		{
			nColumn++;
			//IR
			fData = tb->GetLastData(RS_COL_IR);
			if(type == PS_STEP_END)
			{
				sprintf(szBuff, "");
			}
			else if(type == PS_STEP_IMPEDANCE || type == PS_STEP_DISCHARGE || type== PS_STEP_CHARGE)
			{
				if(fData > 1000000 || fData < 0)		//이상 data시 표기 이상 처리 
				{
					sprintf(szBuff, "0.0");
				}
				else
				{
					//SBC data
					sprintf(szBuff, "%s", ValueString(fData, PS_IMPEDANCE));

					//Get User define time impedance value
					//Edited by KBH 2007/2/26 (사용자가 지정한 Time으로 Impdeance를 재계산 한다.)
					if(bUserImpTime /*DCIR*/)
					{
						GetDlgItem(IDC_TEST_NAME_STATIC)->GetWindowText(strName);
						strTemp.Format("%s\\StepStart\\*_C%06d_S%02d.csv", pData->GetPath(),  
										int(totCycle),
										nStepNo
										);					
						//파일이 많으면 오래 걸림
						CFileFind fFind;
						if(fFind.FindFile(strTemp))
						{
							fFind.FindNextFile();
							strTemp = fFind.GetFilePath();
							TRACE("===========> User point impedance value\n");
							fData = ((CDataDoc *)GetDocument())->CalImpedance(strTemp, fImpTime1, fImpTime2);
							if(fData >= 0.0f)
							{
#ifdef _DEBUG
								sprintf(szBuff, "%s", strTemp);
#else
								sprintf(szBuff, "%s", ValueString(fData, PS_IMPEDANCE));
#endif
							}
						}
					}
				}
			}
			else
			{
				sprintf(szBuff, "0.0");
			}
#ifdef _DEBUG
			CString str;
			str.Format("%s(%s)", szBuff, ValueString(tb->GetLastData(RS_COL_IR), PS_IMPEDANCE));
			m_wndStepGrid.SetItemText(nRow, nColumn, str);
#else
			m_wndStepGrid.SetItemText(nRow, nColumn, szBuff);
#endif
			m_wndStepGrid.SetItemFormat(nRow, nColumn, DT_RIGHT|DT_VCENTER|DT_SINGLELINE );
		}
		
		if(bShowGradeColumn)
		{
			//Grading
			nColumn++;
			fData = tb->GetLastData(RS_COL_GRADE);
			//strName.Format("%c", (BYTE)fData);
			strName = ValueString(fData, PS_GRADE_CODE);
			if(type == PS_STEP_END)		strName.Empty();
			m_wndStepGrid.SetItemText(nRow, nColumn, strName);
			m_wndStepGrid.SetItemFormat(nRow, nColumn, DT_RIGHT|DT_VCENTER|DT_SINGLELINE );
		}
	}

	pProgressWnd->SetPos(100);
//	Sleep(100);
	pProgressWnd->Hide();
	delete pProgressWnd;

//     if (pMainSysMenu != NULL)
// 	{
//        pMainSysMenu->EnableMenuItem(SC_CLOSE,MF_BYCOMMAND | MF_ENABLED); // Enable
// 	}
//     if (pSubSysMenu != NULL)
// 	{
//        pSubSysMenu->EnableMenuItem(SC_CLOSE,MF_BYCOMMAND | MF_ENABLED); // Enable
// 	}
	m_wndStepGrid.Refresh();
}

void CDataView::OnSelchangedTestTree(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here

	HTREEITEM hItem = pNMTreeView->itemNew.hItem;
//	hItem =m_TestTree.GetSelectedItem();
	
	DisplayData(hItem);

	*pResult = 0;	
}

void CDataView::OnTestSelButton() 
{
	// TODO: Add your control notification handler code here
//	CResultTestSelDlg	dlg;
	CDataDoc *pDoc = (CDataDoc *)GetDocument();
//
//	dlg.SetTestList(pDoc->GetSelFileList());
//	if( dlg.DoModal() == IDCANCEL)		return;
//	
//	pDoc->SetDataPath(dlg.m_strFileFolder);	
//	pDoc->SetTestList(dlg.GetSelList());
	
	pDoc->SelectTestDlg();

	ListUpTree();

}

//#include <afxadv.h>
//#include <afxole.h>

void CDataView::CopyDataToClipBoard(CListCtrl *pListCtrl)
{
//	int nItem;
//	CString str;
//	char szBuff[128];
//	
//	if(pListCtrl == NULL)	return;
//
//	POSITION pos = pListCtrl->GetFirstSelectedItemPosition();

//	while(pos)
//	{
//		nItem = pListCtrl->GetNextSelectedItem(pos);
//		
//		for(int j=0; j<5; j++)
//		{
//			pListCtrl->GetItemText(nItem, j, szBuff, 128);	
//			str += szBuff;
//			if(j != 4)
//				str += "\t";
//		}
//		str += "\n";
//	}
//
//	HGLOBAL hGlobal = GlobalAlloc(GHND | GMEM_SHARE, str.GetLength()+1);
//	PTSTR pGlobal = (char *)GlobalLock(hGlobal);
//
//	lstrcpy(pGlobal, TEXT(str.GetBuffer(1)));
//	str.ReleaseBuffer();
//
//	GlobalUnlock(hGlobal);
//
//	OpenClipboard();
//
//	EmptyClipboard();
//
//	SetClipboardData(CF_TEXT, hGlobal);
//
//	CloseClipboard();

	//CString 변수는 64K 길이 제한이 있으므로 메모리를 이용하여 복사한다.(OLE)
//	CSharedFile sf(GMEM_MOVEABLE|GMEM_DDESHARE|GMEM_ZEROINIT);

//	UINT cellState;
//	CString strData;
//	CCellRange cellRange = pGridCtrl->GetSelectedCellRange();
//	
//	for(int row= cellRange.GetMinRow(); row<=cellRange.GetMaxRow(); row++)
//	{
//		for(int col = cellRange.GetMinCol(); col<= cellRange.GetMaxCol(); col++)
//		{
//			if(m_wndStepGrid.IsCellSelected(row, col))
//			{
//				strData = m_wndStepGrid.GetItemText(row, col);
//			}
//			else
//			{
//				strData.Empty();
//			}
//			str += strData;
//			if(col != cellRange.GetMaxCol())
//			{
//				str += "\t";
//			}
//		}
//		if(row != cellRange.GetMaxRow())
//			str += "\r\n";
//
//		sf.Write(str.GetBuffer(1), str.GetLength());
//		str.ReleaseBuffer();
//		str.Empty();
//	}



//	while(pos)
//	{
//		nItem = pListCtrl->GetNextSelectedItem(pos);
//		
//		for(int j=0; j<6; j++)
//		{
//			pListCtrl->GetItemText(nItem, j, szBuff, 128);	
//			str += szBuff;
//			if(j != 5)
//				str += "\t";
//		}
//		str += "\n";
//		sf.Write(str.GetBuffer(1), str.GetLength());
//		str.ReleaseBuffer();
//		str.Empty();
//	}
//
//
//	// 제일 마지막에 NULL문자를 기록하고...
//	char c = '\0';
//	sf.Write(&c, 1);
//
//	// SharedFile의 메모리 핸들을 구합니다...
//	DWORD dwLen = sf.GetLength();
//	HGLOBAL hMem = sf.Detach();
//	if (!hMem) return;
//
//	hMem = ::GlobalReAlloc(hMem, dwLen, GMEM_MOVEABLE|GMEM_DDESHARE|GMEM_ZEROINIT);
//	if (!hMem) return;
//
//
//	// OleDataSource를 만들고, 그 곳에 메모리 핸들의 Data를 잡습니다... 
//	COleDataSource* pSource = new COleDataSource();
//	pSource->CacheGlobalData(CF_TEXT, hMem);
//	if (!pSource) return;
//
//	// 그리고, 그 OleDataSource를 Clipboard에 붙여 넣습니다.....
//	pSource->SetClipboard();    
}

void CDataView::OnGridCopy() 
{
	// TODO: Add your command handler code here
	CWnd *pWnd= GetFocus();

	if(&m_wndStepGrid == pWnd)
	{
		m_wndStepGrid.OnEditCopy();	
//		m_wndStepGrid.CopyTextFromGrid();
	}
/*	else if(&m_wndRawDataGrid == pWnd)
	{
		m_wndRawDataGrid.OnEditCopy();	
//		m_wndRawDataGrid.CopyTextFromGrid();
	}
	else if(&m_wndChListGrid == pWnd)
	{
		m_wndChListGrid.OnEditCopy();
	}
*/
}

void CDataView::OnGridLButtonDblClk(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_GRIDVIEW *pItem = (NM_GRIDVIEW *)pNMHDR;
    *pResult = 0;
    int nRow = pItem->iRow;
    int nCol = pItem->iColumn;
	
	if(pItem->hdr.hwndFrom == m_wndStepGrid.m_hWnd)
	{
		TRACE("Cell (%d, %d) left button double clicked\n", nRow, nCol);
		if(nRow > 0 && nCol >0)
		{
			//CString strStep = m_wndStepGrid.GetItemText(nRow, 0);
			CChData *pChData =((CDataDoc *)GetDocument())->GetChData();
			if(pChData == NULL)		return;

			int nTableIndex= m_wndStepGrid.GetItemData(nRow, 0);
			CString strName, strName2, strTemp;
			int nStepNo = pChData->GetTableNo(nTableIndex);
			WORD type = (WORD)pChData->GetLastDataOfTable(nTableIndex, RS_COL_TYPE);
			strName.Format("Step %d : %s", nStepNo, ::PSGetTypeMsg(type));
			//GetDlgItem(IDC_STEP_RAW_STATIC)->SetWindowText(strName);

			COLORREF textColor, bkColor;
			::PSGetTypeColor(type, textColor, bkColor);		//pscommon.dll

			m_ctrlCurStep.SetText(strName);
			m_ctrlCurStep.SetTextColor(textColor);
			m_ctrlCurStep.SetBkColor(bkColor);

			strName = m_wndStepGrid.GetItemText(nRow, 1);
			GetDlgItem(IDC_TEST_NAME_STATIC)->GetWindowText(strTemp);
			
//			int nCyc = (int)pChData->GetLastDataOfTable(nTableIndex, RS_COL_TOT_CYCLE);
			strName2.Format("%s\\StepStart\\*_C%06d_S%02d.csv", pChData->GetPath(),  
							int(pChData->GetLastDataOfTable(nTableIndex, RS_COL_TOT_CYCLE)),
							nStepNo
							);
		
			
			
			//파일이 많으면 오래 걸림
			CFileFind fFind;
//			BOOL bF = fFind.FindFile(strName2);		

			UpdateStepRawData(nTableIndex);
		}
	}
}


void CDataView::OnGridRButtonUp(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_GRIDVIEW *pItem = (NM_GRIDVIEW *)pNMHDR;
    *pResult = 0;
//    int nRow = pItem->iRow;
//    int nCol = pItem->iColumn;
	
	if( pItem->hdr.hwndFrom == m_wndStepGrid.m_hWnd)// || 
//		pItem->hdr.hwndFrom == m_wndRawDataGrid.m_hWnd ||
//		pItem->hdr.hwndFrom == m_wndChListGrid.m_hWnd)
	{
//		TRACE("Cell (%d, %d) left button double clicked\n", nRow, nCol);
//		if(nRow > 0 && nCol >0)
//		{
			CMenu menu; 
			menu.LoadMenu(IDR_CONTEXT_MENU); 
			CMenu* popmenu = menu.GetSubMenu(4); 
			CPoint ptMouse; 
			GetCursorPos(&ptMouse); 
			popmenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, ptMouse.x, ptMouse.y, GetParent()); 

//			CCellList *cellList = m_wndStepGrid.GetSelectedCellList();
//			int count = cellList->GetCount();
//			POSITION pos = cellList->GetHeadPosition();
//			CCellID cellID;
//			while(pos)
//			{
//				cellID = cellList->GetNext(pos);
//				TRACE("(%d, %d) selected\n", cellID.row, cellID.col);
//			}
//			TRACE("Selectd Total Cell is %d\n", count);
				
//		}
	}
//	else
//	{
//			TRACE("Selectd Total Cell is %d\n", 1);
//
//	}
}

void CDataView::UpdateStepRawData(LONG	lTableIndex, BOOL bShowStepStartData)
{
	CChData *pChData =((CDataDoc *)GetDocument())->GetChData();	
	CMyGridWnd *pGrid = ((CDataFrame *)GetParentFrame())->GetGridWnd();

	// TODO: Add your control notification handler code here
	LONG lDataCount;
	CString strName, strTemp;

	CProgressWnd *pProgressWnd = new CProgressWnd;
	pProgressWnd->Create(this, "Loading...", TRUE);
	strTemp.Format("Table %d의 data를 loading합니다.", lTableIndex+1); //000622
	pProgressWnd->SetText(strTemp);
	pProgressWnd->Show();
	
	if(pChData->IsLoadedData() == FALSE)	
	{
		pProgressWnd->Hide();
		delete pProgressWnd;
		pProgressWnd = NULL;
		return;
	}

	WORD type = (WORD)pChData->GetLastDataOfTable(lTableIndex, RS_COL_TYPE);

	pGrid->SetRowCount(0);

	int nColCnt = 3;
	if(type == PS_STEP_CHARGE || type == PS_STEP_DISCHARGE|| type == PS_STEP_BALANCE || type == PS_STEP_PATTERN)
	{
		nColCnt = 6;
	}
	pGrid->SetColCount(nColCnt);
	pGrid->SetColWidth(1, nColCnt, 50);
	pGrid->SetRowHeight(0, 0, 24);
	
	//File Field 정보를 이용하여야 한다.
	//Remake column
	{
		pGrid->SetValueRange(CGXRange(0, 0), "No(index)");
		pGrid->SetValueRange(CGXRange(0, 1), "Time");
		strName.Format("V(%s)", GetUnitString(PS_VOLTAGE));
		pGrid->SetValueRange(CGXRange(0, 2), strName);
		strName.Format("I(%s)", GetUnitString(PS_CURRENT));
		pGrid->SetValueRange(CGXRange(0, 3), strName);
		
		//충방전 Step에서만 용량과 WattHour를 표시한다.
		if(type == PS_STEP_CHARGE || type == PS_STEP_DISCHARGE|| type == PS_STEP_BALANCE || type == PS_STEP_PATTERN)
		{
			strName.Format("C(%s)", GetUnitString(PS_CAPACITY));
			pGrid->SetValueRange(CGXRange(0, 4), strName);

			strName.Format("W(%s)", GetUnitString(PS_WATT));
			pGrid->SetValueRange(CGXRange(0, 5), strName);
			
			strName.Format("Wh(%s)", GetUnitString(PS_WATT_HOUR));
			pGrid->SetValueRange(CGXRange(0, 6), strName);
		}

		pProgressWnd->PeekAndPump();
	}

	//전압 전류는 반드시 기록하여야 한다.
	ULONG nMinData = 0;
	ULONG nMaxData = 0;
	//Voltage
	fltPoint *pVData = NULL; 
	pVData =  pChData->GetDataOfTable(lTableIndex, lDataCount, "Time", RS_COL_VOLTAGE);
	if(pVData == NULL)
	{
		pProgressWnd->Hide();
		delete pProgressWnd;
		pProgressWnd = NULL;
		return;
	}
	nMinData = lDataCount;
	nMaxData = lDataCount;

	//Current
	fltPoint *pIData= NULL; 
	pIData =  pChData->GetDataOfTable(lTableIndex, lDataCount, "Time", RS_COL_CURRENT);
	if(pIData == NULL)
	{
		delete[] pVData;
		pProgressWnd->Hide();
		delete pProgressWnd;
		pProgressWnd = NULL;
		return;
	}
	if(nMinData > lDataCount)		nMinData = lDataCount;
	if(nMaxData < lDataCount)		nMaxData = lDataCount;

	fltPoint *pCData = NULL; 
	fltPoint *pWattData = NULL; 
	
	//충방전 Step에서만 용량과 WattHour를 표시한다.
	if(type == PS_STEP_CHARGE || type == PS_STEP_DISCHARGE || type == PS_STEP_BALANCE || type == PS_STEP_PATTERN)
	{
		pCData =  pChData->GetDataOfTable(lTableIndex, lDataCount, "Time", RS_COL_CAPACITY);
		if(pCData)
		{
			if(nMinData > lDataCount)		nMinData = lDataCount;
			if(nMaxData < lDataCount)		nMaxData = lDataCount;
		}

		pWattData =  pChData->GetDataOfTable(lTableIndex, lDataCount, "Time", RS_COL_WATT_HOUR);
		if(pWattData)
		{
			if(nMinData > lDataCount)		nMinData = lDataCount;
			if(nMaxData < lDataCount)		nMaxData = lDataCount;
		}
	}

	fltPoint *pIndexData = NULL; 
	pIndexData =  pChData->GetDataOfTable(lTableIndex, lDataCount, "Time", RS_COL_SEQ);
	if(pIndexData)
	{
		if(nMinData > lDataCount)		nMinData = lDataCount;
		if(nMaxData < lDataCount)		nMaxData = lDataCount;
	}

	TRACE("Min data size %d/ Max data size %d\n",  nMinData, nMaxData);

	int nStep, nCycNo;
	float fMaxDetailDataTime = 0.0f, fData, fStepTime, fVoltage, fCurrent;
	float fCapacity = 0.0f, fWattHour = 0.0f;
	CFileFind chFinder;
	nStep =  pChData->GetTableNo(lTableIndex);
	nCycNo = int(pChData->GetLastDataOfTable(lTableIndex, RS_COL_TOT_CYCLE));
	GetDlgItem(IDC_TEST_NAME_STATIC)->GetWindowText(strTemp);
	strName.Format("%s\\StepStart\\*_C%06d_S%02d.csv", pChData->GetPath(), nCycNo, nStep);	
	int nRowCount = pGrid->GetRowCount();
	int nColumn = 0;
	//step start data exsist

	//Display step start data
	bShowStepStartData = TRUE;
	if(chFinder.FindFile(strName) && bShowStepStartData)
	{
		//start file
		chFinder.FindNextFile();
		strName = chFinder.GetFilePath();
		//display step start data;		
		FILE *fp = fopen(strName, "rt");
		if(fp)
		{

			//Save format
/*
#ifdef _EDLC_TEST_SYSTEM
		strLine.Format("time(sec),S%d_V(mV),S%d_I(mA),\n", pMiliSecInfo->lStepNo, pMiliSecInfo->lStepNo, pMiliSecInfo->lStepNo);
#else
		strLine.Format("time(sec),S%d_V(mV),S%d_I(mA),S%d_C(mAh),S%d_WH(mWh)\n", pMiliSecInfo->lStepNo, pMiliSecInfo->lStepNo, pMiliSecInfo->lStepNo, pMiliSecInfo->lStepNo);
#endif
*/
			char szBuff[64];
#ifdef _EDLC_TEST_SYSTEM
			if(fscanf(fp, "%s,%s,%s,", szBuff, szBuff, szBuff) > 0)	//Skip Header
			{
				while(fscanf(fp, "%f,%f,%f,", &fStepTime, &fVoltage, &fCurrent) > 0)
				{
#else
			if(fscanf(fp, "%s,%s,%s,%s,%s", szBuff, szBuff, szBuff, szBuff, szBuff) > 0)	//Skip Header
			{
				while(fscanf(fp, "%f,%f,%f,%f,%f", &fStepTime, &fVoltage, &fCurrent, &fCapacity, &fWattHour) > 0)
				{
#endif
					//No
					pGrid->InsertRows(++nRowCount, 1);
					nColumn = 0;
					
					strName.Format("%d(!)", nRowCount);		//! Step Start data(1초 이하 저장 Data)임을 표시 
					pGrid->SetValueRange(CGXRange(nRowCount, nColumn++), strName);

					//Time
/*
#ifdef _EDLC_TEST_SYSTEM
					strName.Format("%.3f", fStepTime);
#else
					strName.Format("%.2f", fStepTime);
#endif
*/
					//20080826 Edited by KBH (Step Raw Data 도 시간 설정 Format으로 표시되도록)
					strName = ValueString(fStepTime, PS_STEP_TIME);
					pGrid->SetValueRange(CGXRange(nRowCount, nColumn++), strName);

					//Voltage
					strName = ValueString(fVoltage, PS_VOLTAGE);
					pGrid->SetValueRange(CGXRange(nRowCount, nColumn++), strName);

					//Current
					if(type == PS_STEP_CHARGE || type == PS_STEP_DISCHARGE || type == PS_STEP_IMPEDANCE || type == PS_STEP_PATTERN || type == PS_STEP_BALANCE)
					{
						fData = fCurrent;
						strName = ValueString(fData, PS_CURRENT);
					}
					else
					{
						fData = 0;
#ifdef _DEBUG
						strName.Format("%s(%s)", ValueString(fData, PS_CURRENT), ValueString(fCurrent, PS_CURRENT));
#else
						strName = ValueString(fData, PS_CURRENT);
#endif
					}
					pGrid->SetValueRange(CGXRange(nRowCount, nColumn++), strName);

					//충방전 Step에서만 용량과 WattHour를 표시한다.
					if(type == PS_STEP_CHARGE || type == PS_STEP_DISCHARGE|| type == PS_STEP_BALANCE || type == PS_STEP_PATTERN)
					{

						//Capacity
						strName = ValueString(fCapacity, PS_CAPACITY);
						pGrid->SetValueRange(CGXRange(nRowCount, nColumn++), strName);

						//Watt
						strName = ValueString(fVoltage*fCurrent/1000.0f, PS_WATT);
						pGrid->SetValueRange(CGXRange(nRowCount, nColumn++), strName);

						//WattHour
						strName = ValueString(fWattHour, PS_WATT_HOUR);
						pGrid->SetValueRange(CGXRange(nRowCount, nColumn++), strName);
					}
					fMaxDetailDataTime = fStepTime;
				}
			}

			fclose(fp);
		}
	}
	//////////////////////////////////////////////////////////////////////////
	
	//set row count
	nColumn = 0;
	nRowCount = pGrid->GetRowCount();
	
	int nDataSeqFrom = 0;
	if(lTableIndex > 0)
	{
		nDataSeqFrom = (long)pChData->GetLastDataOfTable(lTableIndex-1, RS_COL_SEQ)+1;
	}
	int nDataSeqTo = (long)pChData->GetLastDataOfTable(lTableIndex, RS_COL_SEQ);

	if(nDataSeqTo > 0)		//Version 호환성 위해 
	{
		if((nDataSeqTo - nDataSeqFrom+1) != nMinData)
		{
			AfxMessageBox(TEXT_LANG[22]); //000624
		}
	}

	//Added 2006/08/18
	long lStartIndex = 0, lEndNo = nMinData; 
	if(nMinData > MAX_RAW_DATA_COUNT)
	{
		pProgressWnd->Hide();
		CRangeSelDlg *pSelDlg = new  CRangeSelDlg(this);
		pSelDlg->m_lMax = nMinData;
		if(pSelDlg->DoModal() ==IDOK)
		{
			lStartIndex = pSelDlg->m_lFrom-1;
			lEndNo = pSelDlg->m_lTo;
		}
		else
		{
			//Cancel 처리 default 구간 입력 
			lStartIndex = 0;
			lEndNo = MAX_RAW_DATA_COUNT;
		}
		delete pSelDlg;
			
		if(lStartIndex >= nMinData || lStartIndex >= lEndNo || lEndNo > nMinData)
		{
			//잘못된 Index 입력시 default 구간 
			lStartIndex = 0;
			lEndNo = MAX_RAW_DATA_COUNT;
		}
		pProgressWnd->Show();
	}
	lDataCount = lEndNo-lStartIndex;
	//////////////////////////////////////////////////////////////////////////


	for(int i=lStartIndex; i<nMinData && i< lEndNo; i++)
	{
		strTemp.Format(TEXT_LANG[23], lTableIndex+1, i); //000624
		pProgressWnd->SetText(strTemp);
		pProgressWnd->SetPos(int(float(i)/(float)lDataCount*100.0f));
		
		//PeekAndPump시 Windows Close하면 오류가 발생 
		if(pProgressWnd->PeekAndPump() == FALSE)
		{
			if(pVData)		delete [] pVData;
			if(pIData)		delete [] pIData;
			if(pCData)		delete [] pCData;
			if(pIndexData)	delete [] pIndexData;
			if(pWattData)	delete [] pWattData;
			
		//////////////////////////////////////////////////////////////////////////
			pProgressWnd->SetPos(100);
			pProgressWnd->Hide();
			delete pProgressWnd;
			pProgressWnd = NULL;
			return;
		}

		//step detail에서 이미 표시한 data임
//		if( fMaxDetailDataTime >= pVData[i].x)	continue;

		//No
		nColumn = 0;
		pGrid->InsertRows(++nRowCount, 1);

		if(pIndexData != NULL)	
		{
			if(nDataSeqFrom < int(pIndexData[i].y))
			{
				//nLossCount개의 Data가 손실되었슴.
				int nLossCount = 0;
				while(int(pIndexData[i].y) >= nDataSeqFrom)
				{
					nDataSeqFrom++;
					nLossCount++;
				}
			}
			nDataSeqFrom++;
			strName.Format("%d(%d)", nRowCount,long(pIndexData[i].y));
		}
		else
		{
			strName.Format("%d", nRowCount);
		}
		pGrid->SetValueRange(CGXRange(nRowCount, nColumn++), strName);
		
		//Step end data를 다른 색으로 표시
		if(pIndexData)
		{
			if(nDataSeqTo == long(pIndexData[i].y))
			{
				pGrid->SetStyleRange(CGXRange().SetRows( nRowCount), CGXStyle().SetInterior(RGB(255, 192, 0)));
			}
		}

		//20080826 Edited by KBH (Step Raw Data 도 시간 설정 Format으로 표시되도록)
		strName = ValueString(pVData[i].x, PS_STEP_TIME);
		pGrid->SetValueRange(CGXRange(nRowCount, nColumn++), strName);

		//Voltage
		//strName.Format("%.4f", pVData[i].y/1000.0f);
		strName = ValueString(pVData[i].y, PS_VOLTAGE);
		pGrid->SetValueRange(CGXRange(nRowCount, nColumn++), strName);

		//Current
		if(type == PS_STEP_CHARGE || type == PS_STEP_DISCHARGE || type == PS_STEP_IMPEDANCE|| type == PS_STEP_BALANCE || type == PS_STEP_PATTERN)
		{
			fData = pIData[i].y;
//			strName.Format("%.2f", pIData[i].y);
			strName = ValueString(fData, PS_CURRENT);
		}
		else
		{
			fData = 0;
#ifdef _DEBUG
			strName.Format("%s(%s)", ValueString(fData, PS_CURRENT), ValueString(pIData[i].y, PS_CURRENT));
#else
			strName =  ValueString(fData, PS_CURRENT);
#endif
		}
		pGrid->SetValueRange(CGXRange(nRowCount, nColumn++), strName);

		//충방전 Step에서만 용량과 WattHour를 표시한다.
		if(type == PS_STEP_CHARGE || type == PS_STEP_DISCHARGE|| type == PS_STEP_BALANCE || type == PS_STEP_PATTERN)
		{

			//Capacity
			if(pCData != NULL)
			{
				//strName.Format("%.2f", pCData[i].y);
				strName = ValueString(pCData[i].y, PS_CAPACITY);
				pGrid->SetValueRange(CGXRange(nRowCount, nColumn++), strName);
			}
			//Watt
			strName = ValueString(fabs(pIData[i].y*pVData[i].y/1000.0f), PS_WATT);
			pGrid->SetValueRange(CGXRange(nRowCount, nColumn++), strName);

			//WattHour
			if(pWattData)
			{
				//strName.Format("%.2f", fabs(pWattData[i].y));
				strName = ValueString(fabs(pWattData[i].y), PS_WATT_HOUR);
				pGrid->SetValueRange(CGXRange(nRowCount, nColumn++), strName);
			}
		}
	}

	pGrid->Redraw();

	if(pVData)		delete [] pVData;
	if(pIData)		delete [] pIData;
	if(pCData)		delete [] pCData;
	if(pIndexData)	delete [] pIndexData;
	if(pWattData)	delete [] pWattData;
	
//////////////////////////////////////////////////////////////////////////
	pProgressWnd->SetPos(100);
	pProgressWnd->Hide();
	delete pProgressWnd;
	pProgressWnd = NULL;
//////////////////////////////////////////////////////////////////////////
}
/*
void CDataView::UpdateStepRawData(LONG	lTableIndex, BOOL bShowStepStartData)
{
	// TODO: Add your control notification handler code here
	CChData *pChData =((CDataDoc *)GetDocument())->GetChData();

	LONG lDataCount;
	CString strName, strTemp;

	CProgressWnd *pProgressWnd = new CProgressWnd;
	pProgressWnd->Create(this, "Loading...", TRUE);
	strTemp.Format("Table %d의 data를 loading합니다.", lTableIndex+1);
	pProgressWnd->SetText(strTemp);
	pProgressWnd->Show();
	
	if(pChData->IsLoadedData() == FALSE)	
	{
		pProgressWnd->Hide();
		delete pProgressWnd;
		pProgressWnd = NULL;
		return;
	}

	WORD type = (WORD)pChData->GetLastDataOfTable(lTableIndex, RS_COL_TYPE);
	
	//File Field 정보를 이용하여야 한다.
	//Remake column
	{
		m_wndRawDataGrid.DeleteAllItems();
		m_wndRawDataGrid.InsertColumn("No(index)");
		m_wndRawDataGrid.InsertColumn("Time(sec)");
		
		strName.Format("V(%s)", GetUnitString(PS_VOLTAGE));
		m_wndRawDataGrid.InsertColumn(strName);
		
		strName.Format("I(%s)", GetUnitString(PS_CURRENT));
		m_wndRawDataGrid.InsertColumn(strName);


		//충방전 Step에서만 용량과 WattHour를 표시한다.
		if(type == PS_STEP_CHARGE || type == PS_STEP_DISCHARGE)
		{
//#ifndef _EDLC_TEST_SYSTEM
			strName.Format("C(%s)", GetUnitString(PS_CAPACITY));
			m_wndRawDataGrid.InsertColumn(strName);
//#endif
			strName.Format("W(%s)", GetUnitString(PS_WATT));
			m_wndRawDataGrid.InsertColumn(strName);
			
			strName.Format("Wh(%s)", GetUnitString(PS_WATT_HOUR));
			m_wndRawDataGrid.InsertColumn(strName);

//			strName = "Temp(℃)";
//			m_wndRawDataGrid.InsertColumn(strName);
		}
		m_wndRawDataGrid.SetFixedRowCount(1); 
		m_wndRawDataGrid.SetFixedColumnCount(1); 
		m_wndRawDataGrid.ExpandColumnsToFit();
		pProgressWnd->PeekAndPump();
	}



//	Grid 속성을 Reset 시키기 위해 다시 그린다.
//////////////////////////////////////////////////////////////////////////
//	int nTotRowCount = m_wndRawDataGrid.GetRowCount();
//	TRACE("Step raw data Size Grid %d/Data %d\n", nTotRowCount, lDataCount);
//	if(nTotRowCount > lDataCount)	//Delete Row
//	{
//		for(int  row = nTotRowCount; row >lDataCount ; row--)
//		{
//			m_wndRawDataGrid.DeleteRow(row);
//		}
//	}
//	else		//Insert Row
//	{
//		for(int  row = nTotRowCount; row <=lDataCount ; row++)
//		{
//			strName.Format("%d", row);
//			m_wndRawDataGrid.InsertRow(strName);
//		}
//	}	
//////////////////////////////////////////////////////////////////////////
	
//	m_wndRawDataGrid.SetRowCount(1);

	//전압 전류는 반드시 기록하여야 한다.
	ULONG nMinData = 0;
	ULONG nMaxData = 0;
	//Voltage
	fltPoint *pVData = NULL; 
	pVData =  pChData->GetDataOfTable(lTableIndex, lDataCount, "Time", RS_COL_VOLTAGE);
	if(pVData == NULL)
	{
		pProgressWnd->Hide();
		delete pProgressWnd;
		pProgressWnd = NULL;
		return;
	}
	nMinData = lDataCount;
	nMaxData = lDataCount;

	//Current
	fltPoint *pIData= NULL; 
	pIData =  pChData->GetDataOfTable(lTableIndex, lDataCount, "Time", RS_COL_CURRENT);
	if(pIData == NULL)
	{
		delete[] pVData;
		pProgressWnd->Hide();
		delete pProgressWnd;
		pProgressWnd = NULL;
		return;
	}
	if(nMinData > lDataCount)		nMinData = lDataCount;
	if(nMaxData < lDataCount)		nMaxData = lDataCount;

	fltPoint *pCData = NULL; 
	fltPoint *pWattData = NULL; 
//	fltPoint *pTempData = NULL; 
	
	//충방전 Step에서만 용량과 WattHour를 표시한다.
	if(type == PS_STEP_CHARGE || type == PS_STEP_DISCHARGE)
	{
		pCData =  pChData->GetDataOfTable(lTableIndex, lDataCount, "Time", RS_COL_CAPACITY);
		if(pCData)
		{
			if(nMinData > lDataCount)		nMinData = lDataCount;
			if(nMaxData < lDataCount)		nMaxData = lDataCount;
		}

		pWattData =  pChData->GetDataOfTable(lTableIndex, lDataCount, "Time", RS_COL_WATT_HOUR);
		if(pWattData)
		{
			if(nMinData > lDataCount)		nMinData = lDataCount;
			if(nMaxData < lDataCount)		nMaxData = lDataCount;
		}
	
//		pTempData =  pChData->GetDataOfTable(lTableIndex, lDataCount, "Time", "Temperature");
//		if(nMinData > lDataCount)		nMinData = lDataCount;
//		if(nMaxData < lDataCount)		nMaxData = lDataCount;
	}

//#ifdef _DEBUG
	fltPoint *pIndexData = NULL; 
	pIndexData =  pChData->GetDataOfTable(lTableIndex, lDataCount, "Time", RS_COL_SEQ);
	if(pIndexData)
	{
		if(nMinData > lDataCount)		nMinData = lDataCount;
		if(nMaxData < lDataCount)		nMaxData = lDataCount;
	}
//#endif

	TRACE("Min data size %d/ Max data size %d\n",  nMinData, nMaxData);

	int nStep, nCycNo;
	float fMaxDetailDataTime = 0.0f, fData, fStepTime, fVoltage, fCurrent;
	float fCapacity = 0.0f, fWattHour = 0.0f;
	CFileFind chFinder;
	nStep =  pChData->GetTableNo(lTableIndex);
	nCycNo = int(pChData->GetLastDataOfTable(lTableIndex, RS_COL_TOT_CYCLE));
	GetDlgItem(IDC_TEST_NAME_STATIC)->GetWindowText(strTemp);
	strName.Format("%s\\StepStart\\%s_C%06d_S%02d.csv", pChData->GetPath(), strTemp, nCycNo, nStep);	
	int nRowCount = m_wndRawDataGrid.GetRowCount();
	int nColumn = 0;
	//step start data exsist

	//Display step start data
	bShowStepStartData = TRUE;
	if(chFinder.FindFile(strName) && bShowStepStartData)
	{
		//start file
		chFinder.FindNextFile();
		strName = chFinder.GetFilePath();
		//display step start data;		
		FILE *fp = fopen(strName, "rt");
		if(fp)
		{
			char szBuff[64];
			if(fscanf(fp, "%s,%s,%s,%s,%s", szBuff, szBuff, szBuff, szBuff, szBuff) > 0)	//Skip Header
			{
				while(fscanf(fp, "%f,%f,%f,%f,%f", &fStepTime, &fVoltage, &fCurrent, &fCapacity, &fWattHour) > 0)
				{
					//No
					nColumn = 0;
					strName.Format("%d(!)", nRowCount);		//! Step Start data임을 표시 
					nColumn++;
					m_wndRawDataGrid.InsertRow(strName);

					//Time
					//0.3 sec 이하 Data에 SBC에서 이전 Data가 전송되므로 표시하지 않음(SBC 수정시 삭제 해도 됨)
// 					if(fStepTime < 0.3f && type != PS_STEP_IMPEDANCE)
// 					{
// 			#ifdef _DEBUG
// 						strName.Format("0.0(%.3f)", fStepTime);
// 			#else
// 						strName.Format("0.0");
// 			#endif
// 
// 					}
// 					else
// 					{
#ifdef _EDLC_TEST_SYSTEM
						strName.Format("%.3f", fStepTime);
#else
						strName.Format("%.2f", fStepTime);
#endif
//					}
					m_wndRawDataGrid.SetItemText(nRowCount, nColumn++, strName);

					//Voltage
					strName = ValueString(fVoltage, PS_VOLTAGE);
					m_wndRawDataGrid.SetItemText(nRowCount, nColumn++, strName);

					//Current
					if(type == PS_STEP_CHARGE || type == PS_STEP_DISCHARGE || type == PS_STEP_IMPEDANCE)
					{
						fData = fCurrent;
						strName = ValueString(fData, PS_CURRENT);
					}
					else
					{
						fData = 0;
#ifdef _DEBUG
						strName.Format("%s(s)", ValueString(fData, PS_CURRENT), ValueString(fCurrent, PS_CURRENT));
#else
						strName = ValueString(fData, PS_CURRENT);
#endif
					}
					m_wndRawDataGrid.SetItemText(nRowCount, nColumn++, strName);

					//충방전 Step에서만 용량과 WattHour를 표시한다.
					if(type == PS_STEP_CHARGE || type == PS_STEP_DISCHARGE)
					{

//#ifndef _EDLC_TEST_SYSTEM
						//Capacity
//						fCapacity = fCapacity + fCurrent* (fStepTime-fMaxDetailDataTime)/3600.0f;
						strName = ValueString(fCapacity, PS_CAPACITY);
						m_wndRawDataGrid.SetItemText(nRowCount, nColumn++, strName);
//#endif

//						fWattHour = fWattHour + (fVoltage*fCurrent/1000.0f)*(fStepTime-fMaxDetailDataTime)/3600.0f;
						//Watt
						strName = ValueString(fVoltage*fCurrent/1000.0f, PS_WATT);
						m_wndRawDataGrid.SetItemText(nRowCount, nColumn++, strName);

						//WattHour
						strName = ValueString(fWattHour, PS_WATT_HOUR);
						m_wndRawDataGrid.SetItemText(nRowCount, nColumn++, strName);
					}
					fMaxDetailDataTime = fStepTime;
					nRowCount++;	
				}
			}

			fclose(fp);
		}
	}
	//////////////////////////////////////////////////////////////////////////
	
	//set row count
	nColumn = 0;
	nRowCount = m_wndRawDataGrid.GetRowCount();
	
	int nDataSeqFrom = 1;
	if(lTableIndex > 0)
	{
		nDataSeqFrom = (long)pChData->GetLastDataOfTable(lTableIndex-1, RS_COL_SEQ)+1;
	}
	int nDataSeqTo = (long)pChData->GetLastDataOfTable(lTableIndex, RS_COL_SEQ);

	if(nDataSeqTo > 0)		//Version 호환성 위해 
	{
		if((nDataSeqTo - nDataSeqFrom+1) != nMinData)
		{
			AfxMessageBox("일부 data가 손실구간이 존재 합니다.");
		}
	}

	//Added 2006/08/18
	long lStartIndex = 0, lEndNo = nMinData; 
	if(nMinData > MAX_RAW_DATA_COUNT)
	{
		pProgressWnd->Hide();
		CRangeSelDlg *pSelDlg = new  CRangeSelDlg(this);
		pSelDlg->m_lMax = nMinData;
		if(pSelDlg->DoModal() ==IDOK)
		{
			lStartIndex = pSelDlg->m_lFrom-1;
			lEndNo = pSelDlg->m_lTo;
		}
		else
		{
			//Cancel 처리 default 구간 입력 
			lStartIndex = 0;
			lEndNo = MAX_RAW_DATA_COUNT;
		}
		delete pSelDlg;
			
		if(lStartIndex >= nMinData || lStartIndex >= lEndNo || lEndNo > nMinData)
		{
			//잘못된 Index 입력시 default 구간 
			lStartIndex = 0;
			lEndNo = MAX_RAW_DATA_COUNT;
		}
		pProgressWnd->Show();
	}
	lDataCount = lEndNo-lStartIndex;
	//////////////////////////////////////////////////////////////////////////

	//Disable Close Button
// 	CMenu* pMainSysMenu = AfxGetMainWnd()->GetSystemMenu(FALSE);
//     if (pMainSysMenu != NULL)
// 	{
//       pMainSysMenu->EnableMenuItem(SC_CLOSE,MF_BYCOMMAND | MF_DISABLED); // Disable
// 	}
// 
// 	CMenu* pSubSysMenu = ((CDataFrame *)GetParentFrame())->GetSystemMenu(FALSE);
// 	if(pSubSysMenu)
// 	{
//       pSubSysMenu->EnableMenuItem(SC_CLOSE,MF_BYCOMMAND | MF_DISABLED); // Disable
// 	}


	for(int i=lStartIndex; i<nMinData && i< lEndNo; i++)
//	for(int i=0; i<nMinData; i++)
	{
		strTemp.Format("Table %d의 data를 loading합니다.\n\nData index %d loading...", lTableIndex+1, i);
		pProgressWnd->SetText(strTemp);
		pProgressWnd->SetPos(int(float(i)/(float)lDataCount*100.0f));
		
		//PeekAndPump시 Windows Close하면 오류가 발생 
		if(pProgressWnd->PeekAndPump() == FALSE)	return;

		//step detail에서 이미 표시한 data임
		if( fMaxDetailDataTime >= pVData[i].x)	continue;

		//No
		nColumn = 0;
//#ifdef _DEBUG
		if(pIndexData != NULL)	
		{
			if(nDataSeqFrom < int(pIndexData[i].y))
			{
				//nLossCount개의 Data가 손실되었슴.
				int nLossCount = 0;
				while(int(pIndexData[i].y) >= nDataSeqFrom)
				{
					nDataSeqFrom++;
					nLossCount++;
				}
			}
			nDataSeqFrom++;
			strName.Format("%d(%d)", nRowCount,long(pIndexData[i].y));
		}
		else
		{
			strName.Format("%d", nRowCount);
		}
//#else
//		strName.Format("%d", nRowCount);
//#endif
//		m_wndRawDataGrid.SetItemText(i+1, nColumn++, strName);
		nColumn++;
		m_wndRawDataGrid.InsertRow(strName);
		
		//Step end data를 다른 색으로 표시
		if(pIndexData)
		{
			if(nDataSeqTo == long(pIndexData[i].y))
			{
				for(int j =1; j<m_wndRawDataGrid.GetColumnCount(); j++)
				{
					m_wndRawDataGrid.SetItemBkColour(nRowCount, j, RGB(255, 192, 0));
				}
			}
		}

		//Time
		//0.3 sec 이하 Data에 SBC에서 이전 Data가 전송되므로 표시하지 않음(SBC 수정시 삭제 해도 됨)
//		if(pVData[i].x < 0.3f && (type == PS_STEP_CHARGE || type == PS_STEP_DISCHARGE || type == PS_STEP_IMPEDANCE))
// 		if(pVData[i].x < 0.3f && type != PS_STEP_IMPEDANCE)
// 		{
// #ifdef _DEBUG
// 			strName.Format("0.0(%.3f)", pVData[i].x);
// #else
// 			strName.Format("0.0");
// #endif
// 
// 		}
// 		else
// 		{
#ifdef _EDLC_TEST_SYSTEM
			strName.Format("%.3f", pVData[i].x);
#else
			strName.Format("%.2f", pVData[i].x);
#endif
//		}
		m_wndRawDataGrid.SetItemText(nRowCount, nColumn++, strName);

		//Voltage
		//strName.Format("%.4f", pVData[i].y/1000.0f);
		strName = ValueString(pVData[i].y, PS_VOLTAGE);
		m_wndRawDataGrid.SetItemText(nRowCount, nColumn++, strName);

		//Current
		if(type == PS_STEP_CHARGE || type == PS_STEP_DISCHARGE || type == PS_STEP_IMPEDANCE)
		{
			fData = pIData[i].y;
//			strName.Format("%.2f", pIData[i].y);
			strName = ValueString(fData, PS_CURRENT);
		}
		else
		{
			fData = 0;
#ifdef _DEBUG
			strName.Format("%s(%s)", ValueString(fData, PS_CURRENT), ValueString(pIData[i].y, PS_CURRENT));
#else
			strName =  ValueString(fData, PS_CURRENT);
#endif
		}
		m_wndRawDataGrid.SetItemText(nRowCount, nColumn++, strName);

		//충방전 Step에서만 용량과 WattHour를 표시한다.
		if(type == PS_STEP_CHARGE || type == PS_STEP_DISCHARGE)
		{

//#ifndef _EDLC_TEST_SYSTEM
			//Capacity
			if(pCData != NULL)
			{
				//strName.Format("%.2f", pCData[i].y);
				strName = ValueString(pCData[i].y, PS_CAPACITY);
				m_wndRawDataGrid.SetItemText(nRowCount, nColumn++, strName);
//				delete [] pData;
			}
//#endif
			//Watt
			strName = ValueString(fabs(pIData[i].y*pVData[i].y/1000.0f), PS_WATT);
			m_wndRawDataGrid.SetItemText(nRowCount, nColumn++, strName);

			//WattHour
			if(pWattData)
			{
				//strName.Format("%.2f", fabs(pWattData[i].y));
				strName = ValueString(fabs(pWattData[i].y), PS_WATT_HOUR);
				m_wndRawDataGrid.SetItemText(nRowCount, nColumn++, strName);
			}

//			if(pTempData)
//			{
//				strName = ValueString(fabs(pTempData[i].y), PS_TEMPERATURE);
//				m_wndRawDataGrid.SetItemText(nRowCount, nColumn++, strName);
//			}
		}
		nRowCount++;
	}

	m_wndRawDataGrid.Refresh();

//     if (pMainSysMenu != NULL)
// 	{
//       pMainSysMenu->EnableMenuItem(SC_CLOSE,MF_BYCOMMAND | MF_ENABLED); // Disable
// 	}
// 
// 	if(pSubSysMenu)
// 	{
//       pSubSysMenu->EnableMenuItem(SC_CLOSE,MF_BYCOMMAND | MF_ENABLED); // Disable
// 	}


	if(pVData)		delete [] pVData;
	if(pIData)		delete [] pIData;
	if(pCData)		delete [] pCData;
//#ifdef _DEBUG
	if(pIndexData)	delete [] pIndexData;
//#endif
	if(pWattData)	delete [] pWattData;
//	if(pTempData)	delete [] pTempData;
	
//////////////////////////////////////////////////////////////////////////
	pProgressWnd->SetPos(100);
//	Sleep(200);
	pProgressWnd->Hide();
	delete pProgressWnd;
	pProgressWnd = NULL;
//////////////////////////////////////////////////////////////////////////

}
*/
/*
void CDataView::UpdateStepRawData(LONG	lTableIndex, BOOL bShowStepStartData)
{
	// TODO: Add your control notification handler code here
	CChData *pChData =((CDataDoc *)GetDocument())->GetChData();

	LONG lDataCount;
	CString strName, strTemp;


	CProgressWnd *pProgressWnd = new CProgressWnd;
	pProgressWnd->Create(this, "Loading...", TRUE);
	pProgressWnd->SetColor(RGB(100,100,100));
	pProgressWnd->Clear();
	pProgressWnd->SetRange(0, 100);
	pProgressWnd->SetText("Data Loading...");
	pProgressWnd->SetPos(0);
	pProgressWnd->Show();
	
	if(pChData->IsLoadedData() == FALSE)	
	{
		pProgressWnd->Hide();
		delete pProgressWnd;
		pProgressWnd = NULL;
		return;
	}

	CWordArray *pColumnList = pChData->GetRecordItemList(lTableIndex);
	if(pColumnList == NULL)	
	{
		pProgressWnd->Hide();
		delete pProgressWnd;
		pProgressWnd = NULL;
		return;
	}




	WORD type = (WORD)pChData->GetLastDataOfTable(lTableIndex, "Type");
	
	//File Field 정보를 이용하여야 한다.
	//Remake column
	m_wndRawDataGrid.DeleteAllItems();

	m_wndRawDataGrid.InsertColumn("No");
	for(int n =0; n<pColumnList->GetSize(); n++)
	{
		strName.Format("%s", GetUnitString(pColumnList->GetAt(n)));
		m_wndRawDataGrid.InsertColumn(strName);
	}
		
	m_wndRawDataGrid.SetFixedRowCount(1); 
	m_wndRawDataGrid.SetFixedColumnCount(1); 
	m_wndRawDataGrid.ExpandColumnsToFit();
	pProgressWnd->PeekAndPump();

	fltPoint *paData[10];
	//Voltage
	fltPoint *pVData = NULL; 
	pVData =  pChData->GetDataOfTable(lTableIndex, lDataCount, "Time", "Voltage");

	//Current
	fltPoint *pIData= NULL; 
	pIData =  pChData->GetDataOfTable(lTableIndex, lDataCount, "Time", "Current");

	fltPoint *pCData = NULL; 
	pCData =  pChData->GetDataOfTable(lTableIndex, lDataCount, "Time", "Capacity");
	
	fltPoint *pWattData = NULL; 
	pWattData =  pChData->GetDataOfTable(lTableIndex, lDataCount, "Time", "WattHour");

	fltPoint *pTempData = NULL; 
	pTempData =  pChData->GetDataOfTable(lTableIndex, lDataCount, "Time", "Temperature");
	
#ifdef _DEBUG
	fltPoint *pIndexData = NULL; 
	pIndexData =  pChData->GetDataOfTable(lTableIndex, lDataCount, "Time", "Index");
	if(nMinData > lDataCount)		nMinData = lDataCount;
	if(nMaxData < lDataCount)		nMaxData = lDataCount;
#endif

	TRACE("Min data size %d/ Max data size %d\n",  nMinData, nMaxData);

	int nStep, nCycNo;
	float fMaxDetailDataTime = 0.0f, fData, fStepTime, fVoltage, fCurrent;
	float fCapacity = 0.0f, fWattHour = 0.0f;
	CFileFind chFinder;
	nStep =  pChData->GetTableNo(lTableIndex);
	nCycNo = pChData->GetLastDataOfTable(lTableIndex, "TotalCycle");
	GetDlgItem(IDC_TEST_NAME_STATIC)->GetWindowText(strTemp);
	strName.Format("%s\\StepStart\\%s_C%06d_S%02d.csv", pChData->GetPath(), strTemp, nCycNo, nStep);	
	int nRowCount = m_wndRawDataGrid.GetRowCount();
	int nColumn = 0;
	//step start data exsist

	bShowStepStartData = TRUE;
	if(chFinder.FindFile(strName) && bShowStepStartData)
	{
		//start file
		chFinder.FindNextFile();
		strName = chFinder.GetFilePath();
		//display step start data;		
		FILE *fp = fopen(strName, "rt");
		if(fp)
		{
			char szBuff[64];
			if(fscanf(fp, "%s,%s,%s,%s,%s", szBuff, szBuff, szBuff, szBuff, szBuff) > 0)	//Skip Header
			{
				while(fscanf(fp, "%f,%f,%f,%f,%f", &fStepTime, &fVoltage, &fCurrent, &fCapacity, &fWattHour) > 0)
				{
					//No
					nColumn = 0;
					strName.Format("%d !", nRowCount);
					nColumn++;
					m_wndRawDataGrid.InsertRow(strName);

					//Time
					if(fStepTime < 0.3f)
					{
			#ifdef _DEBUG
						strName.Format("0.0(%.1f)", fStepTime);
			#else
						strName.Format("0.0");
			#endif

					}
					else
					{
						strName.Format("%.1f", fStepTime);
					}
					m_wndRawDataGrid.SetItemText(nRowCount, nColumn++, strName);

					//Voltage
					strName = ValueString(fVoltage, PS_VOLTAGE);
					m_wndRawDataGrid.SetItemText(nRowCount, nColumn++, strName);

					//Current
					if(type == PS_STEP_CHARGE || type == PS_STEP_DISCHARGE || type == PS_STEP_IMPEDANCE)
					{
						fData = fCurrent;
						strName = ValueString(fData, PS_CURRENT);
					}
					else
					{
						fData = 0;
#ifdef _DEBUG
						strName.Format("%s(s)", ValueString(fData, PS_CURRENT), ValueString(fCurrent, PS_CURRENT));
#else
						strName = ValueString(fData, PS_CURRENT);
#endif
					}
					m_wndRawDataGrid.SetItemText(nRowCount, nColumn++, strName);

					//충방전 Step에서만 용량과 WattHour를 표시한다.
					if(type == PS_STEP_CHARGE || type == PS_STEP_DISCHARGE)
					{

#ifndef _EDLC_TEST_SYSTEM
						//Capacity
//						fCapacity = fCapacity + fCurrent* (fStepTime-fMaxDetailDataTime)/3600.0f;
						strName = ValueString(fCapacity, PS_CAPACITY);
						m_wndRawDataGrid.SetItemText(nRowCount, nColumn++, strName);
#endif

//						fWattHour = fWattHour + (fVoltage*fCurrent/1000.0f)*(fStepTime-fMaxDetailDataTime)/3600.0f;
						//Watt
						strName = ValueString(fVoltage*fCurrent/1000.0f, PS_WATT);
						m_wndRawDataGrid.SetItemText(nRowCount, nColumn++, strName);

						//WattHour
						strName = ValueString(fWattHour, PS_WATT_HOUR);
						m_wndRawDataGrid.SetItemText(nRowCount, nColumn++, strName);
					}
					fMaxDetailDataTime = fStepTime;
					nRowCount++;	
				}
			}

			fclose(fp);
		}
	}
	
	//set row count
	nColumn = 0;
	nRowCount = m_wndRawDataGrid.GetRowCount();
	
	for(int i=0; i<nMinData; i++)
	{
		pProgressWnd->SetPos(int(float(i)/(float)lDataCount*100.0f));
		
		//PeekAndPump시 Windows Close하면 오류가 발생 
//		if(pProgressWnd->PeekAndPump() == FALSE)	return;

		//step detail에서 이미 표시한 data임
		if( fMaxDetailDataTime >= pVData[i].x)	continue;

		//No
		nColumn = 0;
#ifdef _DEBUG
		if(pIndexData != NULL)	
		{
			strName.Format("%d(%d)", nRowCount,int(pIndexData[i].y));
		}
		else
		{
			strName.Format("%d", nRowCount);
		}
#else
		strName.Format("%d", nRowCount);
#endif
//		m_wndRawDataGrid.SetItemText(i+1, nColumn++, strName);
		nColumn++;
		m_wndRawDataGrid.InsertRow(strName);

		//Time
//		if(pVData[i].x < 0.3f && (type == PS_STEP_CHARGE || type == PS_STEP_DISCHARGE || type == PS_STEP_IMPEDANCE))
		if(pVData[i].x < 0.3f)
		{
#ifdef _DEBUG
			strName.Format("0.0(%.1f)", pVData[i].x);
#else
			strName.Format("0.0");
#endif

		}
		else
		{
			strName.Format("%.1f", pVData[i].x);
		}
		m_wndRawDataGrid.SetItemText(nRowCount, nColumn++, strName);

		//Voltage
		//strName.Format("%.4f", pVData[i].y/1000.0f);
		strName = ValueString(pVData[i].y, PS_VOLTAGE);
		m_wndRawDataGrid.SetItemText(nRowCount, nColumn++, strName);

		//Current
		if(type == PS_STEP_CHARGE || type == PS_STEP_DISCHARGE || type == PS_STEP_IMPEDANCE)
		{
			fData = pIData[i].y;
//			strName.Format("%.2f", pIData[i].y);
			strName = ValueString(fData, PS_CURRENT);
		}
		else
		{
			fData = 0;
#ifdef _DEBUG
			strName.Format("%s(%s)", ValueString(fData, PS_CURRENT), ValueString(pIData[i].y, PS_CURRENT));
#else
			strName =  ValueString(fData, PS_CURRENT);
#endif
		}
		m_wndRawDataGrid.SetItemText(nRowCount, nColumn++, strName);

		//충방전 Step에서만 용량과 WattHour를 표시한다.
		if(type == PS_STEP_CHARGE || type == PS_STEP_DISCHARGE)
		{

#ifndef _EDLC_TEST_SYSTEM
			//Capacity
			if(pCData != NULL)
			{
				//strName.Format("%.2f", pCData[i].y);
				strName = ValueString(pCData[i].y, PS_CAPACITY);
				m_wndRawDataGrid.SetItemText(nRowCount, nColumn++, strName);
//				delete [] pData;
			}
#endif
			//Watt
			strName = ValueString(fabs(pIData[i].y*pVData[i].y/1000.0f), PS_WATT);
			m_wndRawDataGrid.SetItemText(nRowCount, nColumn++, strName);

			//WattHour
			if(pWattData)
			{
				//strName.Format("%.2f", fabs(pWattData[i].y));
				strName = ValueString(fabs(pWattData[i].y), PS_WATT_HOUR);
				m_wndRawDataGrid.SetItemText(nRowCount, nColumn++, strName);
			}

//			if(pTempData)
//			{
//				strName = ValueString(fabs(pTempData[i].y), PS_TEMPERATURE);
//				m_wndRawDataGrid.SetItemText(nRowCount, nColumn++, strName);
//			}
		}
		nRowCount++;
	}

	m_wndRawDataGrid.Refresh();

	if(pVData)		delete [] pVData;
	if(pIData)		delete [] pIData;
	if(pCData)		delete [] pCData;
#ifdef _DEBUG
	if(pIndexData)	delete [] pIndexData;
#endif
	if(pWattData)	delete [] pWattData;
//	if(pTempData)	delete [] pTempData;
	
//////////////////////////////////////////////////////////////////////////
	pProgressWnd->SetPos(100);
	Sleep(200);
	pProgressWnd->Hide();
	delete pProgressWnd;
	pProgressWnd = NULL;
//////////////////////////////////////////////////////////////////////////

}
*/
/*
void CDataView::InitRawDataGrid()
{

// 	m_ImageList.Create(MAKEINTRESOURCE(IDB_IMAGES), 16, 1, RGB(255,255,255));
//	m_wndRawDataGrid.SetImageList(&m_ImageList);

	m_wndRawDataGrid.EnableDragAndDrop(FALSE);
 	m_wndRawDataGrid.SetEditable(FALSE);
	m_wndRawDataGrid.SetVirtualMode(FALSE);
	
	//Cell의 색상 
	m_wndRawDataGrid.GetDefaultCell(FALSE, FALSE)->SetBackClr(RGB(0xFF, 0xFF, 0xE0));
//	m_wndRawDataGrid.GetDefaultCell(TRUE, FALSE)->SetBackClr(RGB(0xFF, 0xFF, 0xE0));
//	m_wndRawDataGrid.GetDefaultCell(FALSE, TRUE)->SetBackClr(RGB(0xFF, 0xFF, 0xE0));
//	m_wndRawDataGrid.GetDefaultCell(TRUE, TRUE)->SetBackClr(RGB(0xFF, 0xFF, 0xE0));

   
	//여러 Row와 Column을 동시에 선택 가능하도록 한다.
	m_wndRawDataGrid.SetSingleRowSelection(FALSE);
	m_wndRawDataGrid.SetSingleColSelection(FALSE);

	//5*9 Grid 생성
	m_wndRawDataGrid.SetRowCount(1); 
	int nColCount;
	CString	strTemp;

#ifdef _EDLC_TEST_SYSTEM
    nColCount = 6; 
#else
	nColCount = 7;
#endif

    m_wndRawDataGrid.SetColumnCount(nColCount); 

	//Header 부분을 각각 1칸씩 사용한다.
	m_wndRawDataGrid.SetFixedRowCount(1); 
    m_wndRawDataGrid.SetFixedColumnCount(1); 

	//크기 조정을 사용자가 못하도록 한다.
//	m_wndRawDataGrid.SetColumnResize(FALSE);
//	m_wndRawDataGrid.SetRowResize(FALSE);

	int nCol =1;
	m_wndRawDataGrid.SetItemText(0, 0, "No(index)");
	m_wndRawDataGrid.SetItemText(0, nCol++, "Time(sec)");

	strTemp.Format("V(%s)", GetUnitString(PS_VOLTAGE));
	m_wndRawDataGrid.SetItemText(0, nCol++, strTemp);

	strTemp.Format("I(%s)", GetUnitString(PS_CURRENT));
	m_wndRawDataGrid.SetItemText(0, nCol++, strTemp);


#ifndef _EDLC_TEST_SYSTEM
	strTemp.Format("C(%s)", GetUnitString(PS_CAPACITY));
	m_wndRawDataGrid.SetItemText(0, nCol++, strTemp);
#endif

	strTemp.Format("W(%s)", GetUnitString(PS_WATT));
	m_wndRawDataGrid.SetItemText(0, nCol++, strTemp);

	strTemp.Format("Wh(%s)", GetUnitString(PS_WATT_HOUR));
	m_wndRawDataGrid.SetItemText(0, nCol++, strTemp);
//	m_wndRawDataGrid.SetHeaderSort(FALSE);	//Sort기능 사용안함 
	m_wndRawDataGrid.ExpandColumnsToFit();	//폭을 균일 분배로 맞춤
//	m_wndRawDataGrid.ExpandRowsToFit();		//높이를 균일 분배로 맞춤

	m_wndRawDataGrid.Refresh();

//	m_wndRawDataGrid.SetDefCellMargin(30);
//	m_wndRawDataGrid.AutoSize();
}
*/
void CDataView::InitTestInfoList()
{
	m_ctrlTestInfoList.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_TRACKSELECT);
	
	// Column 삽입
	m_ctrlTestInfoList.InsertColumn(0, TEXT_LANG[24], LVCFMT_LEFT, 150); //000625
	m_ctrlTestInfoList.InsertColumn(1, TEXT_LANG[25], LVCFMT_LEFT,	300); //000626

	CString strName;
	LVITEM lvItem; 
	::ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.mask = LVIF_TEXT;
	lvItem.iItem = 0;
	lvItem.iSubItem = 0;
	lvItem.pszText = "Name of Test"; //000249
	m_ctrlTestInfoList.InsertItem(&lvItem);

	lvItem.iItem = 1;
	lvItem.pszText = "Start time"; //000251
	m_ctrlTestInfoList.InsertItem(&lvItem);

	lvItem.iItem = 2;
	lvItem.pszText = "Complete visual"; //000629
	m_ctrlTestInfoList.InsertItem(&lvItem);
		
	lvItem.iItem = 3;
	lvItem.pszText = "Serial"; //000630
	m_ctrlTestInfoList.InsertItem(&lvItem);

	lvItem.iItem = 4;
	lvItem.pszText = "Name of Channel"; //000651
	m_ctrlTestInfoList.InsertItem(&lvItem);
}

void CDataView::UpdateTestInfoList(CString strChName)
{

}

void CDataView::OnUpdateEditCopy(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	CWnd *pWnd= GetFocus();
	if(&m_wndStepGrid == (CGridCtrl *)pWnd)
	{
		pCmdUI->Enable(m_wndStepGrid.GetSelectedCount() > 0);	
	}
/*	else if(&m_wndRawDataGrid == (CGridCtrl *)pWnd)
	{
		pCmdUI->Enable(m_wndRawDataGrid.GetSelectedCount() > 0);	
	}
	else if(&m_wndChListGrid == (CGridCtrl *)pWnd)
	{
		pCmdUI->Enable(m_wndChListGrid.GetSelectedCount() > 0);	
	}
	else
	{
		pCmdUI->Enable(FALSE);	
	}
*/
}

void CDataView::OnSelectAll() 
{
	// TODO: Add your command handler code here
	CWnd *pWnd= GetFocus();
	if(&m_wndStepGrid == (CGridCtrl *)pWnd)
	{
		m_wndStepGrid.OnEditSelectAll();
	}
/*	if(&m_wndRawDataGrid == (CGridCtrl *)pWnd)
	{
		m_wndRawDataGrid.OnEditSelectAll();
	}	
*/
}

void CDataView::OnUpdateSelectAll(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	CWnd *pWnd= GetFocus();
	if(&m_wndStepGrid == (CGridCtrl *)pWnd)
	{
		pCmdUI->Enable(m_wndStepGrid.GetSelectedCount() > 0);	
	}
/*	else if(&m_wndRawDataGrid == (CGridCtrl *)pWnd)
	{
		pCmdUI->Enable(m_wndRawDataGrid.GetSelectedCount() > 0);	
	}
*/	else
	{
		pCmdUI->Enable(FALSE);	
	}	
}

void CDataView::OnEditClear() 
{
	// TODO: Add your command handler code here
	HTREEITEM 	hItem =m_TestTree.GetSelectedItem();

	CString strTestName, strTemp;
	CDataDoc *pDoc = (CDataDoc *)GetDocument();
	CStringList *pSelTestList = pDoc->GetSelFileList();		//FullPathName

	if(m_TestTree.ItemHasChildren(hItem))						//Test Select
	{
		//선택명 변경
		strTestName = m_TestTree.GetItemText(hItem);			//Get Test Name

		//휴지통으로 이동 
		strTemp.Format(TEXT_LANG[26], strTestName); //000632
		if( IDYES == MessageBox(strTemp, "Delete", MB_ICONQUESTION|MB_YESNO) )
		{
			int nIndex = m_TestTree.GetItemData(hItem);				//Data Folder Index
			strTestName = pSelTestList->GetAt( pSelTestList->FindIndex(nIndex));
			if(pDoc->DeleteFolder(strTestName))
			{
				m_TestTree.DeleteItem(hItem);
			}
		}
	}
	else												//Channel Select			
	{
		HTREEITEM hParent = m_TestTree.GetParentItem(hItem);
		if(hParent == FALSE)	return;
		
		strTestName = m_TestTree.GetItemText(hParent);			//Get Test Name
		strTemp.Format(TEXT_LANG[27], strTestName, m_TestTree.GetItemText(hItem)); //000633
		//휴지통으로 이동 
		if( IDYES == MessageBox(strTemp, "Delete", MB_ICONQUESTION|MB_YESNO) )
		{
			int nIndex = m_TestTree.GetItemData(hParent);			//Data Folder Index
			strTestName = pSelTestList->GetAt( pSelTestList->FindIndex(nIndex));
			CString strPath;
			strPath.Format("%s\\%s", strTestName, m_TestTree.GetItemText(hItem));
			
			if(pDoc->DeleteFolder(strPath))
			{
				m_TestTree.DeleteItem(hItem);
			}
		}
	}

}

void CDataView::OnUpdateEditClear(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	CWnd *pWnd= GetFocus();
	if(&m_TestTree == (CTreeCtrl *)pWnd)
	{
		pCmdUI->Enable(TRUE);	
	}
	else
	{
		pCmdUI->Enable(FALSE);	
	}		
}


void CDataView::OnFilePrint() 
{
	// TODO: Add your command handler code here
	CWnd *pWnd= GetFocus();
	if(&m_wndStepGrid == (CGridCtrl *)pWnd)
	{
		m_wndStepGrid.Print();
	}
/*	if(&m_wndRawDataGrid == (CGridCtrl *)pWnd)
	{
		m_wndRawDataGrid.Print();
	}
*/	
}

void CDataView::OnRclickTestTree(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here

	// Select the item that is at the point myPoint.
	// The point to test.
	
	CPoint myPoint;
	
	GetCursorPos(&myPoint); 
	
	m_TestTree.ScreenToClient(&myPoint);
	HTREEITEM hit = m_TestTree.HitTest(myPoint);
	
	// 선택되게 하는 법
	m_TestTree.SelectItem(hit);
	this->SetFocus();	

	// 이미 선택되어 있을 경우만 메뉴 띠우는 법
	if(m_TestTree.GetSelectedItem() == hit)
	{
		 // 메뉴를 띠운다.
		CMenu menu; 
		menu.LoadMenu(IDR_CONTEXT_MENU); 
		CMenu* popmenu = menu.GetSubMenu(4); 
		GetCursorPos(&myPoint); 
		popmenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, myPoint.x, myPoint.y, GetParent()); 
	}
	

	*pResult = 0;
}

void CDataView::OnExcelStepEnd() 
{
	// TODO: Add your command handler code here

	HTREEITEM hItem = m_TestTree.GetSelectedItem();
	ASSERT(hItem);

	//1. 선택한 Test명 채널명 구함
	CString strTestPath, strChName, strTestName, strTemp1;
	int nIndex;
	CStringList *pSelTestList = ((CDataDoc *)GetDocument())->GetSelFileList();
	if(m_TestTree.ItemHasChildren(hItem))								//Test Select
	{
		//선택명 변경
		nIndex = m_TestTree.GetItemData(hItem);
		ASSERT(pSelTestList->FindIndex(nIndex));
		strTestPath = pSelTestList->GetAt( pSelTestList->FindIndex(nIndex));			//FullPathName
	}
	else																				//Channel Select			
	{
		HTREEITEM hParent = m_TestTree.GetParentItem(hItem);
		nIndex = m_TestTree.GetItemData(hParent);										//Data Folder Index
		ASSERT(pSelTestList->FindIndex(nIndex));
		strTestPath = pSelTestList->GetAt( pSelTestList->FindIndex(nIndex));			//FullPathName
		strChName = m_TestTree.GetItemText(hItem);				//Get Ch Name
	}

	if(strTestPath.IsEmpty())	return;
	strTestName = strTestPath.Mid(strTestPath.ReverseFind('\\')+1);

	CString strSchFile, strCSVFile, strPath;
	CFileFind chFinder, schFinder;
	BOOL bWorking1;
	CScheduleData schData;//, *pSchData;

	//2. 파일명 입력 받음	//기본 파일명 생성	// 기본 파일명을 스케쥴명_작업명_채널명.csv로 설정
	if(strChName.IsEmpty() && !strTestPath.IsEmpty())		//Test Select
	{
		//가장 상위 채널의 스케쥴 파일을 구한다.
		strTemp1.Format("%s\\%s", strTestPath, FILE_FIND_FORMAT);
		bWorking1 = chFinder.FindFile(strTemp1);
		while(bWorking1)
		{
			bWorking1 = chFinder.FindNextFile();
			if (chFinder.IsDirectory())
			{
				strTemp1 = chFinder.GetFilePath();
				strSchFile.Format("%s\\*.sch", strTemp1);
				if(schFinder.FindFile(strSchFile))
				{
					schFinder.FindNextFile();
					//schFinder.GetFileName();
					strSchFile = schFinder.GetFilePath();
					schData.SetSchedule(strSchFile);
					break;
				}
			}
		}
		strCSVFile.Format("%s_%s.csv", schData.GetScheduleName() , strTestName);
	}
	else
	{
		strSchFile.Format("%s\\%s\\*.sch", strTestPath, strChName);
		if(schFinder.FindFile(strSchFile))
		{
			schFinder.FindNextFile();
			strSchFile = schFinder.GetFilePath();
			schData.SetSchedule(strSchFile);
			strCSVFile.Format("%s_%s_%s.csv", schData.GetScheduleName(), strTestName, strChName);
		}
		else
		{
			strCSVFile.Format("%s_%s.csv", strTestName, strChName);
		}
	}

	//File 명에 []가 포함된 파일을 Excel로 Open하면 Sheet명이 이상(Excel Bug)하여 참조오류가 발생하여 그래프를 그릴수 없다.
	strCSVFile.Replace('[', '(');
	strCSVFile.Replace(']', ')');

	//사용자 입력 받음 
	CFileDialog pDlg(FALSE, "csv", strCSVFile, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, "csv file(*.csv)|*.csv|");
	if(IDOK != pDlg.DoModal())
	{
		return;
	}
	strCSVFile = pDlg.GetPathName();
	
	if(strChName.IsEmpty())			//Test 선택시 Test를 실시한 모든 채널을 1개 파일에 저장한다.
	{
		strPath.Format("%s\\%s", strTestPath, FILE_FIND_FORMAT);
	}
	else
	{
		strPath.Format("%s\\%s", strTestPath, strChName);
	}
	
	//3. 파일 저장 
/*	CString strHeaderString;
	if(m_nTimeUnit == 1)	//sec 표시
	{
		strTemp1 = "sec";
	}
	else if(m_nTimeUnit == 2)	//Minute 표시
	{
		strTemp1 = "Min";
	}
	else
	{
		strTemp1.Empty();
	}
	if(strTemp1.IsEmpty())
	{
		strHeaderString.Format("채널,작업명,스케쥴명,Step,Type,코드,시작시간,종료시간,총시간,스텝시간,총 Cycle,현재 Cycle,전압(%s),전류(%s),합산용량(%s),충전용량(%s), 방전용량(%s),Power(%s),WattHour(%s),평균전압(%s),평균전류(%s),IR(mOhm),Grade\n", 
								m_strVUnit, m_strIUnit, m_strCUnit, m_strCUnit, m_strCUnit, m_strWUnit, m_strWhUnit, m_strVUnit, m_strIUnit);
	}
	else
	{
		strHeaderString.Format("채널,작업명,스케쥴명,Step,Type,코드,시작시간,종료시간,총시간(%s),스텝시간(%s),총 Cycle,현재 Cycle,전압(%s),전류(%s),합산용량(%s),충전용량(%s), 방전용량(%s), Power(%s),WattHour(%s),평균전압(%s),평균전류(%s),IR(mOhm),Grade\n", 
								strTemp1, strTemp1, m_strVUnit, m_strIUnit, m_strCUnit, m_strCUnit, m_strCUnit, m_strWUnit, m_strWhUnit, m_strVUnit, m_strIUnit);
	}

	float fData, fDataVtg,fDataCrt;
	int nStepNo;
	FILE *fp = NULL;
	WORD wType;
	float	fCapacitySum = 0.0f;
	float	fTotTime = 0.0f;
	COleDateTime	timeStart, timeStepStart;
	COleDateTimeSpan	timeRun;
	long lDay, lHour, lMinute, lSecond;

	CWaitCursor wait;
*/

	int nFileCount = SaveToExcelFile(strPath, strCSVFile);
	if(nFileCount < 1)
	{
		strTemp1.Format(TEXT_LANG[29], strCSVFile);
		AfxMessageBox(strTemp1, MB_ICONSTOP|MB_OK);
	}
		
/*	bWorking1 = chFinder.FindFile(strPath);
	while (bWorking1)
	{
			bWorking1 = chFinder.FindNextFile();
			if(chFinder.IsDots())	continue;

			if (chFinder.IsDirectory())
			{
				strPath = chFinder.GetFilePath();
				CChData data(strPath);
				if(data.Create() == TRUE)
				{
					//최초일 경우 파일을 Open한다.
					if(	fp == NULL)
					{
						fp = fopen(strCSVFile, "wt");
						if(fp == NULL)
						{
							EndWaitCursor();
							strTemp1.Format("%s 파일을 생성할 수 없습니다.", strCSVFile);
							AfxMessageBox(strTemp1, MB_ICONSTOP|MB_OK);
							return;
						}
						fprintf(fp, strHeaderString);//column header 기록
					}
					
//					fprintf(fp, "작업명,%s\n", strTestName);
//					fprintf(fp, "채널,%s\n", strPath.Mid(strPath.ReverseFind('\\')+1));
//					fprintf(fp, "시작시각,%s\n", data.GetStartTime());
//					fprintf(fp, "완료시각,%s\n", data.GetEndTime());
//					fprintf(fp, "작업번호,%s\n", data.GetTestSerial());
					pSchData = data.GetPattern();
					timeStart.ParseDateTime(data.GetStartTime());
					timeStepStart.ParseDateTime(data.GetStartTime());

					for(int i=0; i<data.m_TableList.GetCount(); i++)
					{
						//Channel No
						fprintf(fp, "%s", strPath.Mid(strPath.ReverseFind('\\')+1));
						
						//test Name
						fprintf(fp, ",%s", strTestName);

						//Schedule name
						if(pSchData)
						{
							fprintf(fp, ",%s", pSchData->GetScheduleName());
						}
						else
						{
							fprintf(fp, ",정보를 찾을 수 없음");
						}



						//No
						nStepNo = data.GetTableNo(i);
						fprintf(fp, ",%d", nStepNo);

						//Type
						wType = (WORD)data.GetLastDataOfTable(i, "Type");
						fprintf(fp, ",%s", ::PSGetTypeMsg(wType));	//PScommon.dll
						
						//Code
						::PSCellCodeMsg((BYTE)data.GetLastDataOfTable(i, "Code"), strTemp1, strTemp2);	//Pscommon.dll API
						fprintf(fp, ",%s", strTemp1);	//PScommon.dll

						fTotTime = data.GetLastDataOfTable(i, "TotTime");
						//StartTime
						fprintf(fp, ",%s", timeStepStart.Format("%m/%d %H:%M:%S"));

						//EndTime
						lDay = (long)fTotTime/86400;		lHour = (long)fTotTime%86400;
						lMinute = lHour % 3600;				
						lHour = lHour / 3600;				
						lSecond = lMinute % 60;
						lMinute = lMinute / 60;
						

						timeRun.SetDateTimeSpan(lDay, lHour, lMinute, lSecond);
						timeStepStart = timeStart + timeRun;
						fprintf(fp, ",%s", timeStepStart.Format("%m/%d %H:%M:%S"));
						
						//TotTime
						//fprintf(fp, ",%d", (long)data.GetLastDataOfTable(i, "TotTime"));
						fprintf(fp, ",%s", ValueString(fTotTime, PS_TOT_TIME));
						
						//Time
						//fprintf(fp, ",%d", long(data.GetLastDataOfTable(i, "Time")));
						fprintf(fp, ",%s", ValueString(data.GetLastDataOfTable(i, "Time"), PS_STEP_TIME));
						
						//tot cycle
						fprintf(fp, ",%d", (int)data.GetLastDataOfTable(i, "TotalCycle"));

						//step cycle
						fprintf(fp, ",%d", (int)data.GetLastDataOfTable(i, "CurCycle"));
						
						//Voltage
						fDataVtg = data.GetLastDataOfTable(i, "Voltage");
						//fprintf(fp, ",%.4f", fDataVtg/1000.0f);
						fprintf(fp, ",%s", ValueString(fDataVtg, PS_VOLTAGE));
						
						//Current
						fDataCrt = data.GetLastDataOfTable(i, "Current");
						//fprintf(fp, ",%.2f", fDataCrt);
						fprintf(fp, ",%s", ValueString(fDataCrt, PS_CURRENT));
						
						//Capacity
						fData = data.GetLastDataOfTable(i, "Capacity");
						//fprintf(fp,",%.3f", fData);

						//용량을 총용량, 충전용량, 방전용량으로 구별하여 저장 
						strTemp1 = ValueString(0.0, PS_CAPACITY);
						if(wType == PS_STEP_CHARGE)
						{
							fCapacitySum += fData;
							fprintf(fp, ",%s,%s,%s", ValueString(fCapacitySum, PS_CAPACITY), ValueString(fData, PS_CAPACITY), strTemp1);
						}
						else if(wType == PS_STEP_DISCHARGE || wType == PS_STEP_IMPEDANCE)
						{
							fCapacitySum -= fData;
							fprintf(fp, ",%s,%s,%s", ValueString(fCapacitySum, PS_CAPACITY), strTemp1, ValueString(fData, PS_CAPACITY));
						}
						else 
						{
							fprintf(fp, ",%s,%s,%s", ValueString(fCapacitySum, PS_CAPACITY), strTemp1, strTemp1);
						}
						
						//Watt
						//fprintf(fp,",%.3f", fabs(fDataCrt*fDataVtg/1000.0f));
						fData = fabs(fDataCrt*fDataVtg/1000.0f);
						fprintf(fp,",%s", ValueString(fData, PS_WATT));
						
						//WattHour
						fData = data.GetLastDataOfTable(i, "WattHour");
						//fprintf(fp,",%.1f", fData);
						fprintf(fp,",%s", ValueString(fData, PS_WATT_HOUR));
					
						//Average Voltage
						fDataVtg = data.GetLastDataOfTable(i, "AvgVoltage");
						fprintf(fp, ",%s", ValueString(fDataVtg, PS_VOLTAGE));
						
						//Average Current
						fDataCrt = data.GetLastDataOfTable(i, "AvgCurrent");
						fprintf(fp, ",%s", ValueString(fDataCrt, PS_CURRENT));

						//IR
						fData = data.GetLastDataOfTable(i, "IR");
						fprintf(fp,",%.1f", fData);
						
						//Grading
						fData = data.GetLastDataOfTable(i, "Grade");
						fprintf(fp, ",%c", (BYTE)fData);
						
						fprintf(fp, "\n");
					}	//for					
				}//	if(data.Create() == TRUE)				
			} //if (chFinder.IsDirectory())
			
			//if(fp != NULL)			fprintf(fp, "\n");		//channel과 channel 사이 한줄 삽입 
		
	}// while (bWorking1)

	if(fp != NULL)	fclose(fp);
*/

/*	}

	else	//1 Channel Select
	{
		strPath.Format("%s\\%s", strTestPath, strChName);
		CChData data(strPath);
		if(data.Create() == TRUE)
		{
			fp = fopen(strCSVFile, "wt");
			if(fp)
			{
				fprintf(fp, "작업명,%s\n", strTestName);
				fprintf(fp, "채널,%s\n", strChName);
				fprintf(fp, "시작시각,%s\n", data.GetStartTime());
				fprintf(fp, "완료시각,%s\n", data.GetEndTime());
				fprintf(fp, "작업번호,%s\n", data.GetTestSerial());

				pSchData = data.GetPattern();
				if(pSchData)
				{
					fprintf(fp, "스케쥴명,%s\n", schData.GetScheduleName());
				}
				else
				{
					fprintf(fp, "스케쥴명,정보를 찾을 수 없음\n");
				}


				fprintf(fp, szHeaderString);//column header 기록

				for(int i=0; i<data.m_TableList.GetCount(); i++)
				{
					//No
					nStepNo = data.GetTableNo(i);
					fprintf(fp, "%d", nStepNo);

					//Type
					fprintf(fp, ",%s", ::PSGetTypeMsg((WORD)data.GetLastDataOfTable(i, "Type")));	//PScommon.dll
					
					//Code
					::PSCellCodeMsg((BYTE)data.GetLastDataOfTable(i, "Code"), strTemp1, strTemp2);	//Pscommon.dll API
					fprintf(fp, ",%s", strTemp1);	//PScommon.dll

					//Time
					//fprintf(fp, ",%d", (long)data.GetLastDataOfTable(i, "Time"));
					fprintf(fp, ",%s", ValueString(data.GetLastDataOfTable(i, "Time"), PS_STEP_TIME));
					
					//TotTime
					//fprintf(fp, ",%d", (long)data.GetLastDataOfTable(i, "TotTime"));
					fprintf(fp, ",%s", ValueString(data.GetLastDataOfTable(i, "TotTime"), PS_TOT_TIME));
					
					//Voltage
					fDataVtg = data.GetLastDataOfTable(i, "Voltage");
					//fprintf(fp, ",%.4f", fDataVtg/1000.0f);
					fprintf(fp, ",%s", ValueString(fDataVtg, PS_VOLTAGE));
					
					//Current
					fDataCrt = data.GetLastDataOfTable(i, "Current");
					//fprintf(fp, ",%.3f", fDataCrt);
					fprintf(fp, ",%s", ValueString(fDataCrt, PS_CURRENT));
					
					//Capacity
					fData = data.GetLastDataOfTable(i, "Capacity");
					//fprintf(fp,",%.3f", fData);
					fprintf(fp, ",%s", ValueString(fData, PS_CAPACITY));
					
					//Watt
					//fprintf(fp,",%.3f", fabs(fDataCrt*fDataVtg)/1000.0f);
					fprintf(fp, ",%s", ValueString(fabs(fDataCrt*fDataVtg)/1000.0f, PS_WATT));
					
					//WattHour
					fData = data.GetLastDataOfTable(i, "WattHour");
					//fprintf(fp,",%.1f", fData);
					fprintf(fp, ",%s", ValueString(fData, PS_WATT_HOUR));
				
					//IR
					fData = data.GetLastDataOfTable(i, "IR");
					fprintf(fp,",%.1f", fData);
					
					//Grading
					fData = data.GetLastDataOfTable(i, "Grade");
					fprintf(fp, ",%c", (BYTE)fData);
					
					fprintf(fp, "\n");
				}
				fclose(fp);
			}
			else
			{	
				EndWaitCursor();
				strTemp1.Format("%s 파일을 생성할 수 없습니다.", strCSVFile);
				AfxMessageBox(strTemp1, MB_ICONSTOP|MB_OK);
				return;
			}
		}
	}		
	EndWaitCursor();
*/
	//Excel로 Open 여부를 묻는다.
	if(nFileCount > 1)
	{
		strTemp1.Format(TEXT_LANG[30], nFileCount, strCSVFile);//000635
	}
	else
	{
		strTemp1.Format(TEXT_LANG[31], strCSVFile); //000636
	}
	if(MessageBox(strTemp1, "File Open", MB_ICONQUESTION|MB_YESNO) != IDNO)
	{
		((CCTSGraphAnalApp *)AfxGetApp())->ExecuteExcel(strCSVFile);
	}
}
/*
BOOL CDataView::ExecuteExcel(CString strFileName)
{
	STARTUPINFO	stStartUpInfo;
	PROCESS_INFORMATION	ProcessInfo;
	ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
	ZeroMemory(&ProcessInfo, sizeof(PROCESS_INFORMATION));
		
	stStartUpInfo.cb = sizeof(STARTUPINFO);
	stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
	stStartUpInfo.wShowWindow = SW_NORMAL;

	CString strTemp;
	strTemp = ((CCTSGraphAnalApp *)AfxGetApp())->GetExcelPath();
//	strTemp = ((CDataDoc *)GetDocument())->GetExcelPath();
	if(strTemp.IsEmpty())	return FALSE;

	char buff1[_MAX_PATH];
	char buff2[_MAX_PATH];

	int aa =0;
	do {
		//존재 여부 확인
		aa = GetShortPathName((LPSTR)(LPCTSTR)strTemp, buff2, _MAX_PATH);
		if(aa <= 0)
		{
			if(AfxMessageBox("Excel 실행파일 경로가 잘못되었습니다. 경로를 설정 하십시요.", MB_ICONSTOP|MB_OKCANCEL) == IDCANCEL)
			{
				return FALSE;

			}
			strTemp = ((CCTSGraphAnalApp *)AfxGetApp())->GetExcelPath();
			if(strTemp.IsEmpty())	return FALSE;
		}
		
	} while( aa <= 0 );	

	GetShortPathName((LPSTR)(LPCTSTR)strTemp, buff2, _MAX_PATH);
	GetShortPathName((LPSTR)(LPCTSTR)strFileName, buff1, _MAX_PATH);
	
	strTemp.Format("%s %s", buff2, buff1);
	
	BOOL bFlag = ::CreateProcess(NULL, (LPSTR)(LPCTSTR)strTemp, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &stStartUpInfo, &ProcessInfo);
	if(bFlag == FALSE)
	{
		strTemp.Format("%s를 Open할 수 없습니다.", strFileName);
		MessageBox(strTemp, "Error", MB_OK|MB_ICONSTOP);
	}
	return TRUE;
}
*/

void CDataView::OnExcelSelStep() 
{	
	// TODO: Add your command handler code here
	CChData *pData = ((CDataDoc *)GetDocument())->GetChData();
	if(pData->IsLoadedData() == FALSE)		return;

	ExcelAllStepData(TRUE);

/*

	CString strPath, strTestName, strChName, strTemp1, strTemp2, strSchFile;
	CString strCSVFile;	

	strPath = pData->GetPath();
	//GetChannelName
	strChName = strPath.Mid(strPath.ReverseFind('\\')+1);
	
	//GetTestName
	strTemp1 = strPath.Left(strPath.ReverseFind('\\'));
	strTestName = strTemp1.Mid(strTemp1.ReverseFind('\\')+1);

	//Find schedule File
	CScheduleData *pSchData = pData->GetPattern();
	if(pSchData)
	{
		strCSVFile.Format("%s_%s_%s.csv", pSchData->GetScheduleName(), strTestName, strChName);
	}
	else
	{
		strCSVFile.Format("%s_%s.csv",strTestName, strChName);
	}

	//File 명에 []가 포함된 파일을 Excel로 Open하면 Sheet명이 이상(Excel Bug)하여 참조오류가 발생하여 그래프를 그릴수 없다.
	strCSVFile.Replace('[', '(');
	strCSVFile.Replace(']', ')');

	//2. 파일명 입력 받음
	CFileDialog pDlg(FALSE, "csv", strCSVFile, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, "csv file(*.csv)|*.csv|");
	if(IDOK != pDlg.DoModal())
	{
		return;
	}
	strCSVFile = pDlg.GetPathName();
	
	//3. 파일 저장 
	float fData, fDataVtg,fDataCrt, fTotTime, fCapacitySum, fStepTime, fWattHour;
	int nStepNo;
	LONG	lDataNum;
	fltPoint *pfTimeData, *pfVtgData, *pfCrtData, *pfCapData, *pfWattHour, *pfTemp, *pfAvgVtg, *pfAvgCrt;
	FILE *fp = NULL;
	CString	strWriteBuff;
	int nTotCycle, nCurCycle;
	COleDateTime timeStart, timeStepStart;
	COleDateTimeSpan timeRun;
	long lDay, lHour, lMinute, lSecond;

	//Make Column Header
	CString strHeaderString;
	strTemp1 = m_UnitTrans.GetUnitString(PS_STEP_TIME);
	if(strTemp1.IsEmpty())
	{
		strHeaderString.Format("채널,작업명,스케쥴명,Step,Type,코드,시작시간,종료시간,총시간,스텝시간,총 Cycle,현재 Cycle,전압(%s),전류(%s),합산용량(%s),충전용량(%s),방전용량(%s),Power(%s),WattHour(%s),평균전압(%s),평균전류(%s),IR(mOhm),온도(℃),Grade\n", 
								m_UnitTrans.GetUnitString(PS_VOLTAGE), 
								m_UnitTrans.GetUnitString(PS_CURRENT),
								m_UnitTrans.GetUnitString(PS_CAPACITY),
								m_UnitTrans.GetUnitString(PS_CAPACITY),
								m_UnitTrans.GetUnitString(PS_CAPACITY),
								m_UnitTrans.GetUnitString(PS_WATT),
								m_UnitTrans.GetUnitString(PS_WATT_HOUR),
								m_UnitTrans.GetUnitString(PS_VOLTAGE),
								m_UnitTrans.GetUnitString(PS_CURRENT)
								);
	}
	else
	{
		strHeaderString.Format("채널,작업명,스케쥴명,Step,Type,코드,시작시간,종료시간,총시간(%s),스텝시간(%s),총 Cycle,현재 Cycle,전압(%s),전류(%s),합산용량(%s),충전용량(%s),방전용량(%s), Power(%s),WattHour(%s),평균전압(%s),평균전류(%s),IR(mOhm),온도(℃),Grade\n", 
								strTemp1, strTemp1, 
								m_UnitTrans.GetUnitString(PS_VOLTAGE), 
								m_UnitTrans.GetUnitString(PS_CURRENT),
								m_UnitTrans.GetUnitString(PS_CAPACITY),
								m_UnitTrans.GetUnitString(PS_CAPACITY),
								m_UnitTrans.GetUnitString(PS_CAPACITY),
								m_UnitTrans.GetUnitString(PS_WATT),
								m_UnitTrans.GetUnitString(PS_WATT_HOUR),
								m_UnitTrans.GetUnitString(PS_VOLTAGE),
								m_UnitTrans.GetUnitString(PS_CURRENT)
								);
	}

	CWaitCursor wait;

	fp = fopen(strCSVFile, "wt");
	if(fp)
	{
		fprintf(fp, strHeaderString);//column header 기록

		//Selected stepNo List
		CRowColArray awRows;
		int i;
		WORD wType;
		m_wndStepGrid.GetSelectedRows(awRows);
		
		pSchData = pData->GetPattern();
		timeStart.ParseDateTime(pData->GetStartTime());
		timeStepStart.ParseDateTime(pData->GetStartTime());

		//이전 Stpe의 data를 정리한다.
		fCapacitySum = 0;
		fTotTime = 0;
	
		int nRowSize = m_wndStepGrid.GetRowCount();
		BOOL bSelected = FALSE;
		for(int row =0; row<nRowSize; row++)
		{
			bSelected = FALSE;
			
			for(int findindex = 0; findindex < awRows.GetSize(); findindex++)
			{
				if(awRows[findindex] == row+1)
				{
					bSelected = TRUE;
					break;
				}
			}

			i = pData->GetTableIndex(atol(m_wndStepGrid.GetItemText(row+1, 0)));
			if(i < 0)	continue;

			//last total time
			if(i > 0)
			{
				fTotTime = pData->GetLastDataOfTable(i-1, "TotTime");
			}
			wType = (WORD)pData->GetLastDataOfTable(i, "Type");
			//이전 Step의 용량 총합을 구한다.
			fData = pData->GetLastDataOfTable(i, "Capacity");
			if(wType == PS_STEP_CHARGE)
			{
				fCapacitySum = fCapacitySum + fData;
			}
			else if(wType == PS_STEP_DISCHARGE || wType == PS_STEP_IMPEDANCE)
			{
				fCapacitySum = fCapacitySum - fData;
			}
			
			//Selection is Skiped
			if(bSelected == FALSE)	continue;

			TRACE("Selected Row %d\n", row+1);
					
			fStepTime = 0.0f;
			nStepNo = pData->GetTableNo(i);
			nTotCycle = (int)pData->GetLastDataOfTable(i, "TotalCycle");
			nCurCycle = (int)pData->GetLastDataOfTable(i, "CurCycle");

			//1. Save Step Start Data(1sec 이하 data 저장 )
			//////////////////////////////////////////////////////////////////////////
			strTemp1.Format("%s\\StepStart\\%s_C%06d_S%02d.csv", strPath, strTestName, nTotCycle, nStepNo);	
			FILE *fpCSV = fopen(strTemp1, "rt");
			if(fpCSV)
			{
				char szBuff[64];
				if(fscanf(fpCSV, "%s,%s,%s,%s,%s", szBuff, szBuff, szBuff, szBuff, szBuff) > 0)	//Skip Header
				{
					while(fscanf(fpCSV, "%f,%f,%f,%f,%f", &fStepTime, &fDataVtg, &fDataCrt, &fData, &fWattHour) > 0)
					{
						strWriteBuff.Format("%s,%s,%s,%d,%s,%s,,,%s", 
												strPath.Mid(strPath.ReverseFind('\\')+1),		//Channel
												strTestName,									//Test Name
												pSchData->GetScheduleName(),					//Schedule Name
												nStepNo,										//Step
												::PSGetTypeMsg(wType),							//Type
												"작업중",										//Code
																								//Start Time
																								//End Time
												ValueString(fTotTime+fStepTime, PS_TOT_TIME)	//TotalTime (이전 Step 총시간 + 현재 진행시간)
											);		

							//Time
						if(fStepTime < 0.3f)
						{
#ifdef _DEBUG
							strTemp1.Format("0.0(%.1f)", fStepTime);
#else
							strTemp1.Format("0.0");
#endif
						}
						else
						{
							strTemp1.Format("%.1f", fStepTime);
						}

						float fSum, fChargeCapa, fDischargeCapa;
						if(wType == PS_STEP_CHARGE)
						{
							fChargeCapa = fData; 
							fDischargeCapa = 0.0f;
							fSum = fCapacitySum + fChargeCapa;
						}
						else if(wType == PS_STEP_DISCHARGE || wType == PS_STEP_IMPEDANCE)
						{
							fChargeCapa = 0.0f; 
							fDischargeCapa = fData;
							fSum = fCapacitySum - fDischargeCapa;
						}
						else
						{
							fChargeCapa = 0.0f; 
							fDischargeCapa = 0.0f;
							fSum = fCapacitySum;
						}
								
						strTemp2.Format(",%s,%d,%d,%s,%s,%s,%s,%s,%s,%s,%d,%d,%d,%d,,", 
												strTemp1,											//Step Time	
												nTotCycle,											//TotCycle
												nCurCycle,											//CurCycle
												ValueString(fDataVtg, PS_VOLTAGE),					//Voltage
												ValueString(fDataCrt, PS_CURRENT),					//Current
												ValueString(fSum, PS_CAPACITY),						//Capacity Sum
												ValueString(fChargeCapa, PS_CAPACITY),				//Charge Capacity
												ValueString(fDischargeCapa, PS_CAPACITY),			//Discharge Capacity
												ValueString(fDataVtg*fDataCrt/1000.0f, PS_WATT),	//Power
												ValueString(fWattHour, PS_WATT_HOUR),			//WattHour
												0,													//Average Voltage
												0,													//Average Current
												0,													//IR
												0													//Temperature
																									//Grade
										);
						strWriteBuff += strTemp2;
						fprintf(fp, "%s\n", strWriteBuff);
					}
				}
				fclose(fpCSV);
			}
			//////////////////////////////////////////////////////////////////////////


			//raw data save
			pfTimeData = pData->GetDataOfTable(i, lDataNum, "Time", "Time");
			pfVtgData = pData->GetDataOfTable(i, lDataNum, "Time", "Voltage");
			pfCrtData = pData->GetDataOfTable(i, lDataNum, "Time", "Current");
			pfCapData = pData->GetDataOfTable(i, lDataNum, "Time", "Capacity");
			pfWattHour = pData->GetDataOfTable(i, lDataNum, "Time", "WattHour");
			
			//Option data
			long lTempData;
			pfTemp = pData->GetDataOfTable(i, lTempData, "Time", "Temperature");		
			pfAvgVtg = pData->GetDataOfTable(i, lTempData, "Time", "VoltageAverage");	
			pfAvgCrt = pData->GetDataOfTable(i, lTempData, "Time", "CurrentAverage");	
		
			//2. Save saved option data
			//////////////////////////////////////////////////////////////////////////
			for(int index = 0; index<lDataNum-1; index++)	//최종 data 1개는 제외한다.(하단에서 별도 저장한다.)
			{
				//Skip step start data
				if(pfTimeData[index].y < fStepTime)		continue;

				//Time
				if(pfTimeData[index].y < 0.3f)
				{
#ifdef _DEBUG
					strTemp1.Format("0.0(%.1f)", pfTimeData[index].y);
#else
					strTemp1 = "0.0";
#endif
				}
				else
				{
					strTemp1.Format("%.1f", pfTimeData[index].y);
				}
				
				strWriteBuff.Format("%s,%s,%s,%d,%s,%s,,,%s", 
									strPath.Mid(strPath.ReverseFind('\\')+1),		//Channel
									strTestName,									//Test Name
									pSchData->GetScheduleName(),					//Schedule Name
									nStepNo,										//Step
									::PSGetTypeMsg(wType),							//Type
									"작업중",										//Code
																			//Start Time
																			//End Time
									ValueString(fTotTime+pfTimeData[index].y, PS_TOT_TIME)	//TotalTime (이전 Step 총시간 + 현재 진행시간)
									);		
				float fSum, fChargeCapa, fDischargeCapa;
				if(wType == PS_STEP_CHARGE)
				{
					fChargeCapa = pfCapData[index].y; 
					fDischargeCapa = 0.0f;
					fSum = fCapacitySum + fChargeCapa;

				}
				else if(wType == PS_STEP_DISCHARGE || wType == PS_STEP_IMPEDANCE)
				{
					fChargeCapa = 0.0f; 
					fDischargeCapa = pfCapData[index].y;
					fSum = fCapacitySum - pfCapData[index].y;
				}
				else
				{
					fChargeCapa = 0.0f; 
					fDischargeCapa = 0.0f;
					fSum = fCapacitySum;
				}
					
				strTemp2.Format(",%s,%d,%d,%s,%s,%s,%s,%s,%s,%s", 
									strTemp1,											//Step Time	
									nTotCycle,											//TotCycle
									nCurCycle,											//CurCycle
									ValueString(pfVtgData[index].y, PS_VOLTAGE),		//Voltage
									ValueString(pfCrtData[index].y, PS_CURRENT),		//Current
									ValueString(fSum, PS_CAPACITY),						//Capacity Sum
									ValueString(fChargeCapa, PS_CAPACITY),				//Charge Capacity
									ValueString(fDischargeCapa, PS_CAPACITY),			//Discharge Capacity
									ValueString(pfVtgData[index].y*pfCrtData[index].y/1000.0f, PS_WATT),	//Power
									ValueString(pfWattHour[index].y, PS_WATT_HOUR)		//WattHour
								);
				strWriteBuff += strTemp2;
				
				//Average Voltage
				if(pfAvgVtg)
				{
					strTemp2.Format(",%s", ValueString(pfAvgVtg[index].y, PS_VOLTAGE));
				}
				else
				{
					strTemp2 = ",0";
				}
				strWriteBuff += strTemp2;
					
				//Average Current
				if(pfAvgCrt)
				{
					strTemp2.Format(",%s", ValueString(pfAvgCrt[index].y, PS_CURRENT));
				}
				else
				{
					strTemp2 = ",0";
				}
				strWriteBuff += strTemp2;
				
				//Temperature
				if(pfTemp)
				{
					strTemp2.Format(",%s", ValueString(pfTemp[index].y, PS_TEMPERATURE));
				}
				else
				{
					strTemp2 = ",0";
				}
				strWriteBuff += strTemp2;
					
				//IR
				strTemp2 = ",0";
				strWriteBuff += strTemp2;

				//Grade
				strTemp2 = ",0";
				strWriteBuff += strTemp2;
				
				fprintf(fp, "%s\n", strWriteBuff);
			}
			
			if(pfTimeData != NULL)	delete [] pfTimeData;
			if(pfVtgData != NULL)	delete [] pfVtgData;
			if(pfCrtData != NULL)	delete [] pfCrtData;
			if(pfCapData != NULL)	delete [] pfCapData;
			if(pfWattHour != NULL)	delete [] pfWattHour;

			if(pfTemp != NULL)		delete [] pfTemp;
			if(pfAvgCrt != NULL)	delete [] pfAvgCrt;
			if(pfAvgVtg != NULL)	delete [] pfAvgVtg;

			//3. Save Step end data
			//////////////////////////////////////////////////////////////////////////
			strWriteBuff.Empty();
			
			//Channel No
			//fprintf(fp, "%s", strPath.Mid(strPath.ReverseFind('\\')+1));
			strTemp1.Format("%s", strPath.Mid(strPath.ReverseFind('\\')+1));
			strWriteBuff += strTemp1;
			
			//test Name
			//fprintf(fp, ",%s", strTestName);
			strTemp1.Format(",%s", strTestName);
			strWriteBuff += strTemp1;

			//Schedule name
			if(pSchData)
			{
				//fprintf(fp, ",%s", pSchData->GetScheduleName());
				strTemp1.Format(",%s", pSchData->GetScheduleName());
			}
			else
			{
				//fprintf(fp, ",정보를 찾을 수 없음");
				strTemp1 = ",정보를 찾을 수 없음";
			}
			strWriteBuff += strTemp1;

			//No
			strTemp1.Format(",%d", nStepNo);
			strWriteBuff += strTemp1;

			//Type
			strTemp1.Format(",%s", ::PSGetTypeMsg(wType));
			strWriteBuff += strTemp1;
			
			//Code
			::PSCellCodeMsg((BYTE)pData->GetLastDataOfTable(i, "Code"), strTemp1, strTemp2);	//Pscommon.dll API
			strWriteBuff += (","+strTemp1);

			//StartTime
			strWriteBuff += timeStepStart.Format(",%m/%d %H:%M:%S");

			//EndTime
			fTotTime = pData->GetLastDataOfTable(i, "TotTime");
			lDay = (long)fTotTime/86400;		lHour = (long)fTotTime%86400;
			lMinute = lHour % 3600;				
			lHour = lHour / 3600;				
			lSecond = lMinute % 60;
			lMinute = lMinute / 60;
			

			timeRun.SetDateTimeSpan(lDay, lHour, lMinute, lSecond);
			timeStepStart = timeStart + timeRun;
			strWriteBuff += timeStepStart.Format(",%m/%d %H:%M:%S");
			
			//TotTime
			strWriteBuff += (","+ValueString(fTotTime, PS_TOT_TIME));
			
			//Time
			strTemp1.Format(",%.1f", pData->GetLastDataOfTable(i, "Time"));
			strWriteBuff += strTemp1;
			
			//tot cycle
			strTemp1.Format(",%d", nTotCycle);
			strWriteBuff += strTemp1;

			//step cycle
			strTemp1.Format(",%d", nCurCycle);
			strWriteBuff += strTemp1;
			
			//Voltage
			fDataVtg = pData->GetLastDataOfTable(i, "Voltage");
			strWriteBuff += (","+ValueString(fDataVtg, PS_VOLTAGE));
			
			//Current
			fDataCrt = pData->GetLastDataOfTable(i, "Current");
			strWriteBuff += (","+ValueString(fDataCrt, PS_CURRENT));
			
			//Capacity
			fData = pData->GetLastDataOfTable(i, "Capacity");

			//용량을 총용량, 충전용량, 방전용량으로 구별하여 저장 
			strTemp2 = ValueString(0.0, PS_CAPACITY);
			if(wType == PS_STEP_CHARGE)
			{
				fCapacitySum += fData;
				strTemp1.Format(",%s,%s,%s", ValueString(fCapacitySum, PS_CAPACITY), ValueString(fData, PS_CAPACITY), strTemp2);
			}
			else if(wType == PS_STEP_DISCHARGE || wType == PS_STEP_IMPEDANCE)
			{
				fCapacitySum -= fData;
				strTemp1.Format(",%s,%s,%s", ValueString(fCapacitySum, PS_CAPACITY), strTemp2, ValueString(fData, PS_CAPACITY));
			}
			else 
			{
				strTemp1.Format(",%s,%s,%s", ValueString(fCapacitySum, PS_CAPACITY), strTemp2, strTemp2);
			}
			strWriteBuff += strTemp1;
			
			//Watt
			fData = fabs(fDataCrt*fDataVtg/1000.0f);
			strWriteBuff += (","+ValueString(fData, PS_WATT));
			
			//WattHour
			fData = pData->GetLastDataOfTable(i, "WattHour");
			strWriteBuff += (","+ValueString(fData, PS_WATT_HOUR));
		
			//Average Voltage
			fData = pData->GetLastDataOfTable(i, "VoltageAverage");
			strWriteBuff += (","+ValueString(fData, PS_VOLTAGE));
			
			//Average Current
			fData = pData->GetLastDataOfTable(i, "CurrentAverage");
			strWriteBuff += (","+ValueString(fData, PS_CURRENT));

			//IR
			fData = pData->GetLastDataOfTable(i, "IR");
			strTemp1.Format(",%.1f", fData);
			strWriteBuff += strTemp1;
			
			//Temperature
			fData = pData->GetLastDataOfTable(i, "Temperature");
			strTemp1.Format(",%s", ValueString(fData, PS_TEMPERATURE));
			strWriteBuff += strTemp1;
			
			//Grading
			fData = pData->GetLastDataOfTable(i, "Grade");
			strTemp1.Format(",%c", (BYTE)fData);
			strWriteBuff += strTemp1;
			
			fprintf(fp, "%s\n", strWriteBuff);

		}
		fclose(fp);
	}
	else
	{	
		strTemp1.Format("%s 파일을 생성할 수 없습니다.", strCSVFile);
		AfxMessageBox(strTemp1, MB_ICONSTOP|MB_OK);
		return;
	}
	
	//Excel로 Open 여부를 묻는다.
	strTemp1.Format("%s를 지금 Open하시겠습니까?", strCSVFile);
	if(MessageBox(strTemp1, "File Open", MB_ICONQUESTION|MB_YESNO) != IDNO)
	{
		((CCTSGraphAnalApp *)AfxGetApp())->ExecuteExcel(strCSVFile);
	}
*/
}

void CDataView::OnUpdateExcelSelStep(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	CWnd *pWnd= GetFocus();
	if(&m_wndStepGrid == (CGridCtrl *)pWnd)
	{
		pCmdUI->Enable(m_wndStepGrid.GetSelectedCount() > 0);	
	}
	else
	{
		pCmdUI->Enable(FALSE);	
	}	
}
/*
void CDataView::InitChListGrid()
{
	CString strTemp;
// 	m_ImageList.Create(MAKEINTRESOURCE(IDB_IMAGES), 16, 1, RGB(255,255,255));
//	m_wndChListGrid.SetImageList(&m_ImageList);

	m_wndChListGrid.EnableDragAndDrop(FALSE);
 	m_wndChListGrid.SetEditable(FALSE);
	m_wndChListGrid.SetVirtualMode(FALSE);

	
	//Cell의 색상 
	m_wndChListGrid.GetDefaultCell(FALSE, FALSE)->SetBackClr(RGB(0xFF, 0xFF, 0xE0));
//	m_wndChListGrid.GetDefaultCell(TRUE, FALSE)->SetBackClr(RGB(0xFF, 0xFF, 0xE0));
//	m_wndChListGrid.GetDefaultCell(FALSE, TRUE)->SetBackClr(RGB(0xFF, 0xFF, 0xE0));
//	m_wndChListGrid.GetDefaultCell(TRUE, TRUE)->SetBackClr(RGB(0xFF, 0xFF, 0xE0));

   
	//여러 Row와 Column을 동시에 선택 가능하도록 한다.
	m_wndChListGrid.SetSingleRowSelection(FALSE);
	m_wndChListGrid.SetSingleColSelection(FALSE);

	m_wndChListGrid.SetRowCount(1); 
    m_wndChListGrid.SetColumnCount(1); 

	//Header 부분을 각각 1칸씩 사용한다.
	m_wndChListGrid.SetFixedRowCount(1); 
    m_wndChListGrid.SetFixedColumnCount(1); 

	m_wndChListGrid.SetHeaderSort(TRUE);	//Sort기능 사용안함 

	m_wndChListGrid.Refresh();
}
*/
/*
void CDataView::UpdateChListData(CString strPath)
{
	
//	m_wndChListGrid.DeleteAllItems();

	m_wndChListGrid.SetRowCount(1);
	m_wndChListGrid.SetColumnCount(1);
	m_wndChListGrid.SetItemText(0, 0, "Channel");

	m_wndChListGrid.SetFixedRowCount(1); 
    m_wndChListGrid.SetFixedColumnCount(1); 

	int nCount = 0;
	CString strName, strTemp;
		
	CFileFind chFinder, schFinder;

//	m_wndChListGrid.InsertColumn("Name");
	
	//같은 이름으로 진행된 채널들은 모두 같은 시험조건을 시행을 한다.
	BeginWaitCursor();
	BOOL bSameSchedule = FALSE;
	CString strModel, strTest;
	CScheduleData schData, *pSchData;

	strTemp.Format("%s\\%s", strPath, FILE_FIND_FORMAT);
	BOOL bWorking = chFinder.FindFile(strTemp);
	while (bWorking)
	{
		bWorking = chFinder.FindNextFile();
		if(chFinder.IsDots())	continue;
		if (chFinder.IsDirectory())
		{
			nCount++;
			m_wndChListGrid.InsertRow(chFinder.GetFileName());

			//스케쥴 파일 검색
			strTemp.Format("%s\\*.sch", chFinder.GetFilePath());
			if(schFinder.FindFile(strTemp))
			{
				schFinder.FindNextFile();
				strTemp = schFinder.GetFilePath();
				schData.SetSchedule(strTemp);

				//Model 명이나 스케쥴명이 다르면 다른 시험조건을 시행한 폴더이다.
				if(!strModel.IsEmpty() && !strTest.IsEmpty() && (strModel != schData.GetModelName() || strTest != schData.GetScheduleName()))
				{
					bSameSchedule = FALSE;
					TRACE("%s/%s, %s/%s\n", strModel, strTest, schData.GetModelName(), schData.GetScheduleName());
					break;
				}
				else
				{
					strModel = schData.GetModelName();
					strTest = schData.GetScheduleName();
					bSameSchedule = TRUE;
				}
			}
		}
		

	}
	EndWaitCursor();
	
	//표기할 결과가 없을 경우
	if(nCount <= 0)	
	{
		m_wndChListGrid.SetItemText(0, 0, "시행한 채널 결과가 없습니다.");
		return;
	}
	//다른 시험을 진행한 채널이 있을 경우 
	if(bSameSchedule == FALSE)
	{
		m_wndChListGrid.SetItemText(0, 0, "서로 다른 스캐쥴을 시행한 결과이므로 표시 할 수 없습니다.");
		return;
	}

	//Data를 표기 한다.
	CChData *chData = new CChData[nCount];

	BeginWaitCursor();
	for(int i =0; i< nCount; i++)
	{
		strTemp.Format("%s\\%s", strPath, m_wndChListGrid.GetItemText(i+1, 0));
		chData[i].SetPath(strTemp);
		chData[i].Create();
	}

	//채널들이 같은 공정을 시행하지 않았으면 채널 리스트 형태로 표기 할 수 없다.
	pSchData = chData[0].GetPattern();
	if(pSchData == NULL)	bSameSchedule = FALSE;

	if(bSameSchedule )
	{
		CStep *pStep = NULL;
		int nStepNo;
		//Column 삽입 
		for(int i=0; i<pSchData->GetStepSize(); i++)
		{
			pStep = (CStep *)(pSchData->GetStepData(i));
			if(pStep == NULL)	break;

			nStepNo = pStep->m_StepIndex+1;// pData->GetTableNo(i);
			switch(pStep->m_type)
			{
			case PS_STEP_CHARGE:
			case PS_STEP_DISCHARGE:
				strTemp.Format("%d:전압", nStepNo);
				m_wndChListGrid.InsertColumn(strTemp);
				strTemp.Format("%d:전류", nStepNo);
				m_wndChListGrid.InsertColumn(strTemp);
				strTemp.Format("%d:용량", nStepNo);
				m_wndChListGrid.InsertColumn(strTemp);
				if(pStep->m_bGrade)
				{
					strTemp.Format("%d:등급", nStepNo);
					m_wndChListGrid.InsertColumn(strTemp);
				}
				break;

			case PS_STEP_IMPEDANCE:
				strTemp.Format("%d:Imp", nStepNo);
				m_wndChListGrid.InsertColumn(strTemp);
				if(pStep->m_bGrade)
				{
					strTemp.Format("%d:등급", nStepNo);
					m_wndChListGrid.InsertColumn(strTemp);
				}
				break;

			case PS_STEP_OCV:
				strTemp.Format("%d:전압", nStepNo);
				m_wndChListGrid.InsertColumn(strTemp);
				if(pStep->m_bGrade)
				{
					strTemp.Format("%d:등급", nStepNo);
					m_wndChListGrid.InsertColumn(strTemp);
				}
				break;
			}
		}

		//Data 표기 
		int nColCount;
		for(i =0; i< nCount; i++)
		{
			nColCount = 1;
			for(int step=0; step<pSchData->GetStepSize(); step++)
			{
				pStep = pSchData->GetStepData(step);
				if(pStep == NULL)	break;
				nStepNo = pStep->m_StepIndex+1;// pData->GetTableNo(i);
				
				//아직 시행하지 않았거나 표시 할 수 없는 Step Display하지 않는다.
				if(chData[i].GetTableIndex(nStepNo) < 0)	continue;	

				switch(pStep->m_type)
				{
				case PS_STEP_CHARGE:
				case PS_STEP_DISCHARGE:
					strTemp.Format("%.4f", chData[i].GetLastDataOfTable(chData[i].GetTableIndex(nStepNo), "Voltage")/1000.0f);
					m_wndChListGrid.SetItemText(i+1, nColCount++, strTemp);
					strTemp.Format("%.2f", chData[i].GetLastDataOfTable(chData[i].GetTableIndex(nStepNo), "Current"));
					m_wndChListGrid.SetItemText(i+1, nColCount++, strTemp);
					strTemp.Format("%.2f", chData[i].GetLastDataOfTable(chData[i].GetTableIndex(nStepNo), "Capacity"));
					m_wndChListGrid.SetItemText(i+1, nColCount++, strTemp);
					if(pStep->m_bGrade)
					{
						strTemp.Format("%c", (char)(chData[i].GetLastDataOfTable(chData[i].GetTableIndex(nStepNo), "Grade")));
						m_wndChListGrid.SetItemText(i+1, nColCount++, strTemp);
					}
					break;

				case PS_STEP_IMPEDANCE:
						strTemp.Format("%.2f", chData[i].GetLastDataOfTable(chData[i].GetTableIndex(nStepNo), "IR"));
						m_wndChListGrid.SetItemText(i+1, nColCount++, strTemp);
					if(pStep->m_bGrade)
					{
						strTemp.Format("%c", (char)(chData[i].GetLastDataOfTable(chData[i].GetTableIndex(nStepNo), "Grade")));
						m_wndChListGrid.SetItemText(i+1, nColCount++, strTemp);
					}
					break;

				case PS_STEP_OCV:
						strTemp.Format("%.4f", chData[i].GetLastDataOfTable(chData[i].GetTableIndex(nStepNo), "Voltage")/1000.0f);
						m_wndChListGrid.SetItemText(i+1, nColCount++, strTemp);
					if(pStep->m_bGrade)
					{
						strTemp.Format("%c", (char)(chData[i].GetLastDataOfTable(chData[i].GetTableIndex(nStepNo), "Grade")));
						m_wndChListGrid.SetItemText(i+1, nColCount++, strTemp);
					}
					break;
				}
			}
		}
	}

	delete[] chData;
	chData = NULL;
	
	EndWaitCursor();
}
*/
/*
void CDataView::UpdateChListData(CString strPath)
{
//	CWaitCursor wait;
//	m_wndChListGrid.DeleteAllItems();

	m_wndChListGrid.SetRowCount(1);
	m_wndChListGrid.SetColumnCount(1);
	m_wndChListGrid.SetItemText(0, 0, "Channel");

	m_wndChListGrid.SetFixedRowCount(1); 
    m_wndChListGrid.SetFixedColumnCount(1); 

	int nCount = 0;
	CString strName, strTemp;
		
	CFileFind chFinder, schFinder;

//	m_wndChListGrid.InsertColumn("Name");
	
	//같은 이름으로 진행된 채널들은 모두 같은 시험조건을 시행을 한다.
	BOOL bSameSchedule = FALSE;
	CString strModel, strTest;
	CScheduleData schData, *pSchData;

//	CString strTestName;
//	GetDlgItem(IDC_TEST_NAME_STATIC)->GetWindowText(strTestName);
//	strTestName = strPath.Mid(strPath.ReverseFind('\\')+1);
	strTemp.Format("%s\\%s", strPath, FILE_FIND_FORMAT);
	
	BOOL bWorking = chFinder.FindFile(strTemp);
	while (bWorking)
	{
		bWorking = chFinder.FindNextFile();
		if(chFinder.IsDots())	continue;
		if (chFinder.IsDirectory())
		{
			nCount++;
			m_wndChListGrid.InsertRow(chFinder.GetFileName());

			//스케쥴 파일 검색
			strTemp.Format("%s\\*.sch", chFinder.GetFilePath());
//			strTemp.Format("%s\\%s.sch", chFinder.GetFilePath(), strTestName);

			if(schFinder.FindFile(strTemp))
			{
				schFinder.FindNextFile();
				strTemp = schFinder.GetFilePath();
				schData.SetSchedule(strTemp);

				//Model 명이나 스케쥴명이 다르면 다른 시험조건을 시행한 폴더이다.
				if(!strModel.IsEmpty() && !strTest.IsEmpty() && (strModel != schData.GetModelName() || strTest != schData.GetScheduleName()))
				{
					bSameSchedule = FALSE;
					TRACE("%s/%s, %s/%s\n", strModel, strTest, schData.GetModelName(), schData.GetScheduleName());
					break;
				}
				else
				{
					strModel = schData.GetModelName();
					strTest = schData.GetScheduleName();
					bSameSchedule = TRUE;
				}
			}
		}
	}
	
	//표기할 결과가 없을 경우
	if(nCount <= 0)	
	{
		m_wndChListGrid.SetItemText(0, 0, "시행한 채널 결과가 없습니다.");
		return;
	}
	//다른 시험을 진행한 채널이 있을 경우 
	if(bSameSchedule == FALSE)
	{
		m_wndChListGrid.SetItemText(0, 0, "서로 다른 스캐쥴을 시행한 결과이므로 표시 할 수 없습니다.");
		return;
	}

	CProgressWnd *pProgressWnd = new CProgressWnd;
	pProgressWnd->Create(this, "Loading...", TRUE, TRUE);
//	pProgressWnd->SetColor(RGB(100,100,100));
//	pProgressWnd->Clear();
	pProgressWnd->SetRange(0, 100);
	pProgressWnd->SetText("Data Loading...");
	pProgressWnd->SetPos(0);
	pProgressWnd->Show();
	
	//Data를 표기 한다.
	CChData *chData = new CChData[nCount];
	CChData maxChData;

	int nMaxTableSize = 0;
	for(int i =0; i< nCount; i++)
	{
		strTemp.Format("Data Loading %s.", m_wndChListGrid.GetItemText(i+1, 0));
		pProgressWnd->SetText(strTemp);
		pProgressWnd->SetPos(int((float)i/((float)nCount*2.0f)*100.0f));
		if(pProgressWnd->PeekAndPump() == FALSE)		//Message 처리 
		{
			//Loading Canceled
			nMaxTableSize = 0;
			break;
		}

		strTemp.Format("%s\\%s", strPath, m_wndChListGrid.GetItemText(i+1, 0));
		chData[i].SetPath(strTemp);
		chData[i].Create();
		if(nMaxTableSize < chData[i].GetTableSize())
		{
			maxChData.SetPath(strTemp);
			maxChData.Create();
			nMaxTableSize = chData[i].GetTableSize();
		}
	}

	if(nMaxTableSize < 1)
	{
		m_wndChListGrid.SetItemText(0, 0, "저장된 채널 결과가 없습니다.");
		delete [] chData;

		pProgressWnd->Hide();
		delete pProgressWnd;
		pProgressWnd = NULL;
		return;
	}

	//채널들이 같은 공정을 시행하지 않았으면 채널 리스트 형태로 표기 할 수 없다.
	pSchData = chData[0].GetPattern();
	if(pSchData == NULL)	bSameSchedule = FALSE;
	int nDisplayStep = m_StepSelCombo.GetCurSel();

	if(bSameSchedule )
	{
		CStep *pStep = NULL;
		long nStepNo, totCycle;


		//Column 삽입 
		m_wndChListGrid.InsertColumn("Code");

		for(int i=0; i<nMaxTableSize; i++)	//가장 많이 실행한 채널 수 많큼 
		//pos = tableList.GetHeadPosition();
		//while(pos)
		{
			nStepNo = maxChData.GetTableNo(i);
			totCycle = (long)maxChData.GetLastDataOfTable(i, RS_COL_TOT_CYCLE);
			pStep = (CStep *)(pSchData->GetStepData(nStepNo-1));
			if(pStep == NULL)	break;

			if(nDisplayStep == 1)	//충전 
			{
				if(pStep->m_type != PS_STEP_CHARGE)	continue;
			}
			else if(nDisplayStep == 2 )	//방전
			{
				if(pStep->m_type != PS_STEP_DISCHARGE)	continue;
			}
			else if(nDisplayStep == 3 )	//충전&방전
			{
				if(pStep->m_type != PS_STEP_CHARGE && pStep->m_type != PS_STEP_DISCHARGE)	continue;
			}
			else if(nDisplayStep == 4 )	//Rest
			{
				if(pStep->m_type != PS_STEP_REST)		continue;
			}
			else if(nDisplayStep == 5 )	//Impedance
			{
				if(pStep->m_type != PS_STEP_IMPEDANCE)	continue;
			}
			else if(nDisplayStep == 6)	//Step Range
			{
				if(m_RangeList.Find(nStepNo)==NULL)		continue;
			}
			else if(nDisplayStep == 7)	//Cycle Range
			{
				if(m_RangeList.Find(totCycle)==NULL)	continue;
			}
			else if(nDisplayStep == 8)	//Index Range
			{
				long lTemp = i+1;
				if(m_RangeList.Find(lTemp) == NULL)				continue;
			}

			switch(pStep->m_type)
			{
			case PS_STEP_CHARGE:
				strTemp.Format("%d:Cha_V", nStepNo);
				m_wndChListGrid.InsertColumn(strTemp);
				strTemp.Format("%d:Cha_I", nStepNo);
				m_wndChListGrid.InsertColumn(strTemp);
				strTemp.Format("%d:Cha_C", nStepNo);
				m_wndChListGrid.InsertColumn(strTemp);
				if(pStep->m_bGrade)
				{
					strTemp.Format("%d:Ch_G", nStepNo);
					m_wndChListGrid.InsertColumn(strTemp);
				}
				break;

			case PS_STEP_DISCHARGE:
				strTemp.Format("%d:Dis_V", nStepNo);
				m_wndChListGrid.InsertColumn(strTemp);
				strTemp.Format("%d:Dis_I", nStepNo);
				m_wndChListGrid.InsertColumn(strTemp);
				strTemp.Format("%d:Dis_C", nStepNo);
				m_wndChListGrid.InsertColumn(strTemp);
				if(pStep->m_bGrade)
				{
					strTemp.Format("%d:Dis_G", nStepNo);
					m_wndChListGrid.InsertColumn(strTemp);
				}
				break;

			case PS_STEP_IMPEDANCE:
				strTemp.Format("%d:Imp", nStepNo);
				m_wndChListGrid.InsertColumn(strTemp);
				if(pStep->m_bGrade)
				{
					strTemp.Format("%d:Imp_G", nStepNo);
					m_wndChListGrid.InsertColumn(strTemp);
				}
				break;

			case PS_STEP_OCV:
				strTemp.Format("%d:OCV", nStepNo);
				m_wndChListGrid.InsertColumn(strTemp);
				if(pStep->m_bGrade)
				{
					strTemp.Format("%d:OCV_G", nStepNo);
					m_wndChListGrid.InsertColumn(strTemp);
				}
				break;
			}

			//max column num
			if(m_wndChListGrid.GetColumnCount() > 255)
			{
				break;
			}
		}

		//Data 표기 
		int nColCount;
		COLORREF bkColorRef[2] = { RGB(255, 240, 240), RGB(240, 240, 255)};
		COLORREF bkColor, textColor;
		BYTE cellCode = 0;
		for(i =0; i< nCount; i++)
		{
			strTemp.Format("Data Loading %s.", m_wndChListGrid.GetItemText(i+1, 0));
			pProgressWnd->SetText(strTemp);
			pProgressWnd->SetPos(int((float)(i+nCount)/((float)nCount*2.0f)*100.0f));
			
			nColCount = 1;
	
			//Last Code
			cellCode = (BYTE)chData[i].GetLastDataOfTable(chData[i].GetTableSize()-1, RS_COL_CODE);
			if(::PSIsCellOk(cellCode) || ::PSIsNonCell(cellCode))
			{
				::PSGetTypeColor(PS_STATE_END, textColor, bkColor);		//pscommon.dll
				m_wndChListGrid.SetItemFgColour(i+1, nColCount, textColor);
				m_wndChListGrid.SetItemBkColour(i+1, nColCount, bkColor);
			}
			else
			{
				::PSGetTypeColor(PS_STATE_FAIL, textColor, bkColor);		//pscommon.dll
				m_wndChListGrid.SetItemFgColour(i+1, nColCount, textColor);
				m_wndChListGrid.SetItemBkColour(i+1, nColCount, bkColor);
			}
			::PSCellCodeMsg(cellCode, strTemp, strName);	//Pscommon.dll API
			m_wndChListGrid.SetItemText(i+1, nColCount++, strTemp);

			//for(int step=0; step<pSchData->GetStepSize(); step++)
			for(int tb = 0; tb<chData[i].GetTableSize(); tb++)
			{
				nStepNo = chData[i].GetTableNo(tb);
				pStep = pSchData->GetStepData(nStepNo-1);
				totCycle = (long)maxChData.GetLastDataOfTable(i, RS_COL_TOT_CYCLE);
				if(pStep == NULL)	break;

				if(nDisplayStep == 1)	//충전 
				{
					if(pStep->m_type != PS_STEP_CHARGE)	continue;
				}
				else if(nDisplayStep == 2 )	//방전
				{
					if(pStep->m_type != PS_STEP_DISCHARGE)	continue;
				}
				else if(nDisplayStep == 3 )	//충전&방전
				{
					if(pStep->m_type != PS_STEP_CHARGE && pStep->m_type != PS_STEP_DISCHARGE)	continue;
				}
				else if(nDisplayStep == 4 )	//Rest
				{
					if(pStep->m_type != PS_STEP_REST)	continue;
				}
				else if(nDisplayStep == 5 )	//Impedance
				{
					if(pStep->m_type != PS_STEP_IMPEDANCE)	continue;
				}
				else if(nDisplayStep == 6)	//Step Range
				{
					if(m_RangeList.Find(nStepNo)==NULL)		continue;
				}
				else if(nDisplayStep == 7)	//Cycle Range
				{
					if(m_RangeList.Find(totCycle)==NULL)		continue;
				}

				
				//아직 시행하지 않았거나 표시 할 수 없는 Step Display하지 않는다.
//				if(chData[i].GetTableIndex(nStepNo) < 0)	continue;	

				switch(pStep->m_type)
				{
				case PS_STEP_CHARGE:
					bkColor = bkColorRef[0];
					//strTemp.Format("%.4f", chData[i].GetLastDataOfTable(tb, "Voltage")/1000.0f);
					strTemp = ValueString(chData[i].GetLastDataOfTable(tb, RS_COL_VOLTAGE), PS_VOLTAGE);
					m_wndChListGrid.SetItemBkColour(i+1, nColCount, bkColor);
					m_wndChListGrid.SetItemText(i+1, nColCount++, strTemp);
				
					//strTemp.Format("%.2f", chData[i].GetLastDataOfTable(tb, "Current"));
					strTemp = ValueString(chData[i].GetLastDataOfTable(tb, RS_COL_CURRENT), PS_CURRENT);
					m_wndChListGrid.SetItemBkColour(i+1, nColCount, bkColor);
					m_wndChListGrid.SetItemText(i+1, nColCount++, strTemp);
					
					//strTemp.Format("%.2f", chData[i].GetLastDataOfTable(tb, "Capacity"));
					strTemp = ValueString(chData[i].GetLastDataOfTable(tb, RS_COL_CAPACITY), PS_CAPACITY);
					m_wndChListGrid.SetItemBkColour(i+1, nColCount, bkColor);
					m_wndChListGrid.SetItemText(i+1, nColCount++, strTemp);
					if(pStep->m_bGrade)
					{
						strTemp.Format("%c", (char)(chData[i].GetLastDataOfTable(tb, RS_COL_GRADE)));
						m_wndChListGrid.SetItemBkColour(i+1, nColCount, bkColor);
						m_wndChListGrid.SetItemText(i+1, nColCount++, strTemp);
					}
					break;
				case PS_STEP_DISCHARGE:
					bkColor = bkColorRef[1];
					//strTemp.Format("%.4f", chData[i].GetLastDataOfTable(tb, "Voltage")/1000.0f);
					strTemp = ValueString(chData[i].GetLastDataOfTable(tb, RS_COL_VOLTAGE), PS_VOLTAGE);
					m_wndChListGrid.SetItemBkColour(i+1, nColCount, bkColor);
					m_wndChListGrid.SetItemText(i+1, nColCount++, strTemp);

					//strTemp.Format("%.2f", chData[i].GetLastDataOfTable(tb, "Current"));
					strTemp = ValueString(chData[i].GetLastDataOfTable(tb, RS_COL_CURRENT), PS_CURRENT);
					m_wndChListGrid.SetItemBkColour(i+1, nColCount, bkColor);
					m_wndChListGrid.SetItemText(i+1, nColCount++, strTemp);
					
					//strTemp.Format("%.2f", chData[i].GetLastDataOfTable(tb, "Capacity"));
					strTemp = ValueString(chData[i].GetLastDataOfTable(tb, RS_COL_CAPACITY), PS_CAPACITY);					
					m_wndChListGrid.SetItemBkColour(i+1, nColCount, bkColor);
					m_wndChListGrid.SetItemText(i+1, nColCount++, strTemp);
					if(pStep->m_bGrade)
					{
						strTemp.Format("%c", (char)(chData[i].GetLastDataOfTable(tb, RS_COL_GRADE)));
						m_wndChListGrid.SetItemBkColour(i+1, nColCount, bkColor);
						m_wndChListGrid.SetItemText(i+1, nColCount++, strTemp);
					}
					break;

				case PS_STEP_IMPEDANCE:
					//strTemp.Format("%.2f", chData[i].GetLastDataOfTable(tb, "IR"));
					strTemp = ValueString(chData[i].GetLastDataOfTable(tb, RS_COL_IR), PS_IMPEDANCE);					
					//m_wndChListGrid.SetItemBkColour(i+1, nColCount, bkColor);
					m_wndChListGrid.SetItemText(i+1, nColCount++, strTemp);
					if(pStep->m_bGrade)
					{
						strTemp.Format("%c", (char)(chData[i].GetLastDataOfTable(tb, RS_COL_GRADE)));
						//m_wndChListGrid.SetItemBkColour(i+1, nColCount, bkColor);
						m_wndChListGrid.SetItemText(i+1, nColCount++, strTemp);
					}
					break;

				case PS_STEP_OCV:
					//strTemp.Format("%.4f", chData[i].GetLastDataOfTable(tb, "Voltage")/1000.0f);
					strTemp = ValueString(chData[i].GetLastDataOfTable(tb, RS_COL_VOLTAGE), PS_VOLTAGE);
					//m_wndChListGrid.SetItemBkColour(i+1, nColCount, bkColor);
					m_wndChListGrid.SetItemText(i+1, nColCount++, strTemp);
					if(pStep->m_bGrade)
					{
						strTemp.Format("%c", (char)(chData[i].GetLastDataOfTable(tb, RS_COL_GRADE)));
						//m_wndChListGrid.SetItemBkColour(i+1, nColCount, bkColor);
						m_wndChListGrid.SetItemText(i+1, nColCount++, strTemp);
					}
					break;
				}

				//max column num
				if(m_wndChListGrid.GetColumnCount() > 255)
				{
					break;
				}
			}
		}
	}

	m_wndChListGrid.Refresh();
	delete[] chData;
	chData = NULL;

	pProgressWnd->SetPos(100);
//	Sleep(200);
	pProgressWnd->Hide();
	delete pProgressWnd;
	pProgressWnd = NULL;
}
*/
void CDataView::OnExcelAllStepData() 
{

	ExcelAllStepData();

/*	return;


	// TODO: Add your command handler code here
	HTREEITEM hItem =m_TestTree.GetSelectedItem();
	ASSERT(hItem);

	//1. 선택한 Test명 채널명 구함
	CString strTestPath, strChName, strTemp1, strTemp2, strTestName;
	int nIndex;
	CStringList *pSelTestList = ((CDataDoc *)GetDocument())->GetSelFileList();
	if(m_TestTree.ItemHasChildren(hItem))								//Test Select
	{
		//선택명 변경
		nIndex = m_TestTree.GetItemData(hItem);
		ASSERT(pSelTestList->FindIndex(nIndex));
		strTestPath = pSelTestList->GetAt( pSelTestList->FindIndex(nIndex));			//FullPathName
	}
	else																				//Channel Select			
	{
		HTREEITEM hParent = m_TestTree.GetParentItem(hItem);
		if(hParent == NULL)		return;
		nIndex = m_TestTree.GetItemData(hParent);										//Data Folder Index
		ASSERT(pSelTestList->FindIndex(nIndex));
		strTestPath = pSelTestList->GetAt( pSelTestList->FindIndex(nIndex));			//FullPathName
		strChName = m_TestTree.GetItemText(hItem);				//Get Ch Name
	}

	if(strTestPath.IsEmpty())	return;
	strTestName = strTestPath.Mid(strTestPath.ReverseFind('\\')+1);

	CString strSchFile;
	CFileFind chFinder, schFinder;
	BOOL bWorking1;
	CScheduleData schData, *pSchData;

	//2. 파일명 입력 받음	
	CString strCSVFile;	
	//기본 파일명 생성	// 기본 파일명을 스케쥴명_작업명_채널명.csv로 설정
	if(strChName.IsEmpty() && !strTestPath.IsEmpty())		//Test Select
	{
		//가장 상위 채널의 스케쥴 파일을 구한다.
		strTemp1.Format("%s\\%s", strTestPath, FILE_FIND_FORMAT);
		bWorking1 = chFinder.FindFile(strTemp1);
		while(bWorking1)
		{
			bWorking1 = chFinder.FindNextFile();
			if (chFinder.IsDirectory())
			{
				strTemp1 = chFinder.GetFilePath();
	//			strSchFile.Format("%s\\%s.sch", strTemp1, strTestName);
				strSchFile.Format("%s\\*.sch", strTemp1);
				if(schFinder.FindFile(strSchFile))
				{
					schFinder.FindNextFile();
					//schFinder.GetFileName();
					strSchFile = schFinder.GetFilePath();
					schData.SetSchedule(strSchFile);
					break;
				}
			}
		}
		strCSVFile.Format("%s_%s.csv", schData.GetScheduleName() , strTestName);
	}
	else
	{
		strSchFile.Format("%s\\%s\\*.sch", strTestPath, strChName);
//		strSchFile.Format("%s\\*.sch", strChName);
		if(schFinder.FindFile(strSchFile))
		{
			schFinder.FindNextFile();
			strSchFile = schFinder.GetFilePath();
			schData.SetSchedule(strSchFile);
			strCSVFile.Format("%s_%s_%s.csv", schData.GetScheduleName(), strTestName, strChName);
		}
		else
		{
			strCSVFile.Format("%s_%s.csv", strTestName, strChName);
		}
	}

	//File 명에 []가 포함된 파일을 Excel로 Open하면 Sheet명이 이상(Excel Bug)하여 참조오류가 발생하여 그래프를 그릴수 없다.
	strCSVFile.Replace('[', '(');
	strCSVFile.Replace(']', ')');
	//사용자 입력 받음 
	CFileDialog pDlg(FALSE, "csv", strCSVFile, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, "csv file(*.csv)|*.csv|");
	if(IDOK != pDlg.DoModal())
	{
		return;
	}
	strCSVFile = pDlg.GetPathName();
	
	//3. 파일 저장 
	CString strPath, strWriteBuff;
	float fData, fStepTime, fDataVtg,fDataCrt, fTotTime, fWattHour, fCapacitySum = 0;
	long lDay, lHour, lMinute, lSecond;
	int nStepNo;
	fltPoint *pfTimeData, *pfVtgData, *pfCrtData, *pfCapData, *pfWattHour, *pfTemp, *pfAvgVtg, *pfAvgCrt;
	FILE *fp = NULL;
	COleDateTime timeStart, timeStepStart;
	COleDateTimeSpan timeRun;
	WORD wType;
	int nTotCycle, nCurCycle;

	//Make Column Header
	CString strHeaderString;
	strTemp1 = m_UnitTrans.GetUnitString(PS_STEP_TIME);
	if(strTemp1.IsEmpty())
	{
		strHeaderString.Format("채널,작업명,스케쥴명,Step,Type,코드,시작시간,종료시간,총시간,스텝시간,총 Cycle,현재 Cycle,전압(%s),전류(%s),합산용량(%s),충전용량(%s),방전용량(%s),Power(%s),WattHour(%s),평균전압(%s),평균전류(%s),IR(mOhm),온도(℃),Grade\n", 
								m_UnitTrans.GetUnitString(PS_VOLTAGE), 
								m_UnitTrans.GetUnitString(PS_CURRENT),
								m_UnitTrans.GetUnitString(PS_CAPACITY),
								m_UnitTrans.GetUnitString(PS_CAPACITY),
								m_UnitTrans.GetUnitString(PS_CAPACITY),
								m_UnitTrans.GetUnitString(PS_WATT),
								m_UnitTrans.GetUnitString(PS_WATT_HOUR),
								m_UnitTrans.GetUnitString(PS_VOLTAGE),
								m_UnitTrans.GetUnitString(PS_CURRENT)
								);
	}
	else
	{
		strHeaderString.Format("채널,작업명,스케쥴명,Step,Type,코드,시작시간,종료시간,총시간(%s),스텝시간(%s),총 Cycle,현재 Cycle,전압(%s),전류(%s),합산용량(%s),충전용량(%s),방전용량(%s), Power(%s),WattHour(%s),평균전압(%s),평균전류(%s),IR(mOhm),온도(℃),Grade\n", 
								strTemp1, strTemp1, 
								m_UnitTrans.GetUnitString(PS_VOLTAGE), 
								m_UnitTrans.GetUnitString(PS_CURRENT),
								m_UnitTrans.GetUnitString(PS_CAPACITY),
								m_UnitTrans.GetUnitString(PS_CAPACITY),
								m_UnitTrans.GetUnitString(PS_CAPACITY),
								m_UnitTrans.GetUnitString(PS_WATT),
								m_UnitTrans.GetUnitString(PS_WATT_HOUR),
								m_UnitTrans.GetUnitString(PS_VOLTAGE),
								m_UnitTrans.GetUnitString(PS_CURRENT)
								);
	}



	CWaitCursor wait;

	if(strChName.IsEmpty())			//Test 선택시 Test를 실시한 모든 채널을 1개 파일에 저장한다.
	{
		strPath.Format("%s\\%s", strTestPath, FILE_FIND_FORMAT);
	}
	else
	{
		strPath.Format("%s\\%s", strTestPath, strChName);
	}
	
	bWorking1 = chFinder.FindFile(strPath);
	if(bWorking1 == FALSE)
	{
		AfxMessageBox("Data를 찾을 수 없습니다.");
		return;
	}

	while (bWorking1)
	{
		bWorking1 = chFinder.FindNextFile();
		if(chFinder.IsDots())			continue;
		if (!chFinder.IsDirectory())	continue;
			
		strPath = chFinder.GetFilePath();
		CChData data(strPath);
		if(data.Create() == TRUE)
		{
			//최초일 경우 파일을 Open한다.
			if(	fp == NULL)
			{
				fp = fopen(strCSVFile, "wt");
				if(fp == NULL)
				{
					strTemp1.Format("%s 파일을 생성할 수 없습니다.", strCSVFile);
					AfxMessageBox(strTemp1, MB_ICONSTOP|MB_OK);
					return;
				}
				fprintf(fp, strHeaderString);//column header 기록
			}

			timeStart.ParseDateTime(data.GetStartTime());
			timeStepStart.ParseDateTime(data.GetStartTime());

			pSchData = data.GetPattern();
			fCapacitySum = 0;
			fTotTime = 0;
			
			for(int i =0; i<data.GetTableSize(); i++)
			{
				fStepTime = 0.0f;
				nStepNo = data.GetTableNo(i);
				wType = (WORD)data.GetLastDataOfTable(i, "Type");
				nTotCycle = (int)data.GetLastDataOfTable(i, "TotalCycle");
				nCurCycle = (int)data.GetLastDataOfTable(i, "CurCycle");

				//1. Save Step Start Data(1sec 이하 data 저장 )
				//////////////////////////////////////////////////////////////////////////
				strTemp1.Format("%s\\StepStart\\%s_C%06d_S%02d.csv", strPath, strTestName, nTotCycle, nStepNo);	
				FILE *fpCSV = fopen(strTemp1, "rt");
				if(fpCSV)
				{
					char szBuff[64];
					if(fscanf(fpCSV, "%s,%s,%s,%s,%s", szBuff, szBuff, szBuff, szBuff, szBuff) > 0)	//Skip Header
					{
						while(fscanf(fpCSV, "%f,%f,%f,%f,%f", &fStepTime, &fDataVtg, &fDataCrt, &fData, &fWattHour) > 0)
						{
							strWriteBuff.Format("%s,%s,%s,%d,%s,%s,,,%s", 
													strPath.Mid(strPath.ReverseFind('\\')+1),		//Channel
													strTestName,									//Test Name
													pSchData->GetScheduleName(),					//Schedule Name
													nStepNo,										//Step
													::PSGetTypeMsg(wType),							//Type
													"작업중",										//Code
																									//Start Time
																									//End Time
													ValueString(fTotTime+fStepTime, PS_TOT_TIME)	//TotalTime (이전 Step 총시간 + 현재 진행시간)
												);		

							//Time
							if(fStepTime < 0.3f)
							{
#ifdef _DEBUG
								strTemp1.Format("0.0(%.1f)", fStepTime);
#else
								strTemp1.Format("0.0");
#endif
							}
							else
							{
								strTemp1.Format("%.1f", fStepTime);
							}

							float fSum, fChargeCapa, fDischargeCapa;
							if(wType == PS_STEP_CHARGE)
							{
								fChargeCapa = fData; 
								fDischargeCapa = 0.0f;
								fSum = fCapacitySum + fChargeCapa;
							}
							else if(wType == PS_STEP_DISCHARGE || wType == PS_STEP_IMPEDANCE)
							{
								fChargeCapa = 0.0f; 
								fDischargeCapa = fData;
								fSum = fCapacitySum - fDischargeCapa;
							}
							else
							{
								fChargeCapa = 0.0f; 
								fDischargeCapa = 0.0f;
								fSum = fCapacitySum;
							}
									
							strTemp2.Format(",%s,%d,%d,%s,%s,%s,%s,%s,%s,%s,%d,%d,%d,%d,,", 
													strTemp1,											//Step Time	
													nTotCycle,											//TotCycle
													nCurCycle,											//CurCycle
													ValueString(fDataVtg, PS_VOLTAGE),					//Voltage
													ValueString(fDataCrt, PS_CURRENT),					//Current
													ValueString(fSum, PS_CAPACITY),						//Capacity Sum
													ValueString(fChargeCapa, PS_CAPACITY),				//Charge Capacity
													ValueString(fDischargeCapa, PS_CAPACITY),			//Discharge Capacity
													ValueString(fDataVtg*fDataCrt/1000.0f, PS_WATT),	//Power
													ValueString(fWattHour, PS_WATT_HOUR),				//WattHour
													0,													//Average Voltage
													0,													//Average Current
													0,													//IR
													0													//Temperature
																										//Grade
											);
							strWriteBuff += strTemp2;
							fprintf(fp, "%s\n", strWriteBuff);
						}
					}
					fclose(fpCSV);
				}
				//////////////////////////////////////////////////////////////////////////


				//2. Save Saved data
				//////////////////////////////////////////////////////////////////////////
				//raw data save 
				long lMaxDataCount = 0;
				long nTimeDataCnt = 0, nVtgDataCnt = 0, nCrtDataCnt = 0, nCapDataCnt = 0;
				long nWattHDataCnt = 0, nTempDataCnt = 0, nAvgCrtCnt = 0, nAvgVtgCnt = 0;
				pfTimeData = data.GetDataOfTable(i, nTimeDataCnt, "Time", "Time");			if(nTimeDataCnt > lMaxDataCount)	lMaxDataCount = nTimeDataCnt;
				pfVtgData = data.GetDataOfTable(i, nVtgDataCnt, "Time", "Voltage");			if(nVtgDataCnt > lMaxDataCount)		lMaxDataCount = nVtgDataCnt;
				pfCrtData = data.GetDataOfTable(i, nCrtDataCnt, "Time", "Current");			if(nCrtDataCnt > lMaxDataCount)		lMaxDataCount = nCrtDataCnt;
				pfCapData = data.GetDataOfTable(i, nCapDataCnt, "Time", "Capacity");		if(nCapDataCnt > lMaxDataCount)		lMaxDataCount = nCapDataCnt;
				pfWattHour = data.GetDataOfTable(i, nWattHDataCnt, "Time", "WattHour");		if(nWattHDataCnt > lMaxDataCount)	lMaxDataCount = nWattHDataCnt;
				pfTemp = data.GetDataOfTable(i, nTempDataCnt, "Time", "Temperature");		if(nTempDataCnt > lMaxDataCount)	lMaxDataCount = nTempDataCnt;
				pfAvgVtg = data.GetDataOfTable(i, nAvgVtgCnt, "Time", "VoltageAverage");	if(nAvgVtgCnt > lMaxDataCount)		lMaxDataCount = nAvgVtgCnt;
				pfAvgCrt = data.GetDataOfTable(i, nAvgCrtCnt, "Time", "CurrentAverage");	if(nAvgCrtCnt > lMaxDataCount)		lMaxDataCount = nAvgCrtCnt;
			
				for(int index = 0; index<lMaxDataCount-1; index++)	//최종 종료값 1개는 빼고 저장한다.//밑에서 최종 종료값은 별도로 저장한다.
				{
					//Step start data와 중복된 data이므로 생략 
					if(pfTimeData[index].y < fStepTime  )	continue;

					//Time
					if(pfTimeData[index].y < 0.3f)
					{
#ifdef _DEBUG
						strTemp1.Format("0.0(%.1f)", pfTimeData[index].y);
#else
						strTemp1 = "0.0";
#endif
					}
					else
					{
						strTemp1.Format("%.1f", pfTimeData[index].y);
					}
					
					strWriteBuff.Format("%s,%s,%s,%d,%s,%s,,,%s", 
											strPath.Mid(strPath.ReverseFind('\\')+1),		//Channel
											strTestName,									//Test Name
											pSchData->GetScheduleName(),					//Schedule Name
											nStepNo,										//Step
											::PSGetTypeMsg(wType),							//Type
											"작업중",										//Code
																							//Start Time
																							//End Time
											ValueString(fTotTime+pfTimeData[index].y, PS_TOT_TIME)	//TotalTime (이전 Step 총시간 + 현재 진행시간)
										);		
					float fSum, fChargeCapa, fDischargeCapa;
					if(wType == PS_STEP_CHARGE)
					{
						fChargeCapa = pfCapData[index].y; 
						fDischargeCapa = 0.0f;
						fSum = fCapacitySum + fChargeCapa;
					}
					else if(wType == PS_STEP_DISCHARGE || wType == PS_STEP_IMPEDANCE)
					{
						fChargeCapa = 0.0f; 
						fDischargeCapa = pfCapData[index].y;
						fSum = fCapacitySum - fDischargeCapa;
					}
					else
					{
						fChargeCapa = 0.0f; 
						fDischargeCapa = 0.0f;
						fSum = fCapacitySum;
					}
							
					strTemp2.Format(",%s,%d,%d,%s,%s,%s,%s,%s,%s,%s", 
											strTemp1,											//Step Time	
											nTotCycle,											//TotCycle
											nCurCycle,											//CurCycle
											ValueString(pfVtgData[index].y, PS_VOLTAGE),		//Voltage
											ValueString(pfCrtData[index].y, PS_CURRENT),		//Current
											ValueString(fSum, PS_CAPACITY),						//Capacity Sum
											ValueString(fChargeCapa, PS_CAPACITY),				//Charge Capacity
											ValueString(fDischargeCapa, PS_CAPACITY),			//Discharge Capacity
											ValueString(pfVtgData[index].y*pfCrtData[index].y/1000.0f, PS_WATT),	//Power
											ValueString(pfWattHour[index].y, PS_WATT_HOUR)		//WattHour
									);
					strWriteBuff += strTemp2;

					//Average Voltage
					if(pfAvgVtg)
					{
						strTemp2.Format(",%s", ValueString(pfAvgVtg[index].y, PS_VOLTAGE));
					}
					else
					{
						strTemp2 = ",0";
					}
					strWriteBuff += strTemp2;
					
					//Average Current
					if(pfAvgCrt)
					{
						strTemp2.Format(",%s", ValueString(pfAvgCrt[index].y, PS_CURRENT));
					}
					else
					{
						strTemp2 = ",0";
					}
					strWriteBuff += strTemp2;
					
					//Temperature
					if(pfTemp)
					{
						strTemp2.Format(",%s", ValueString(pfTemp[index].y, PS_TEMPERATURE));
					}
					else
					{
						strTemp2 = ",0";
					}
					strWriteBuff += strTemp2;
					
					//IR
					strTemp2 = ",0";
					strWriteBuff += strTemp2;

					//Grade
					strTemp2 = ",0";
					strWriteBuff += strTemp2;
					
					fprintf(fp, "%s\n", strWriteBuff);
				}
					
				if(pfTimeData != NULL)	delete [] pfTimeData;
				if(pfVtgData != NULL)	delete [] pfVtgData;
				if(pfCrtData != NULL)	delete [] pfCrtData;
				if(pfCapData != NULL)	delete [] pfCapData;
				if(pfWattHour != NULL)	delete [] pfWattHour;
				if(pfTemp != NULL)		delete [] pfTemp;
				if(pfAvgVtg != NULL)	delete [] pfAvgVtg;
				if(pfAvgCrt != NULL)	delete [] pfAvgCrt;

				//3. Save step End data
				//////////////////////////////////////////////////////////////////////////
				//Save Step End Value
				strWriteBuff.Empty();
				//Channel No
				strTemp1.Format("%s", strPath.Mid(strPath.ReverseFind('\\')+1));
				strWriteBuff += strTemp1;
					
				//test Name
				strTemp1.Format(",%s", strTestName);
				strWriteBuff += strTemp1;

				//Schedule name
				if(pSchData)
				{
					strTemp1.Format(",%s", pSchData->GetScheduleName());
				}
				else
				{
					strTemp1 = ",정보를 찾을 수 없음";
				}
				strWriteBuff += strTemp1;

				//No
				strTemp1.Format(",%d", nStepNo);
				strWriteBuff += strTemp1;

				//Type
				strTemp1.Format(",%s", ::PSGetTypeMsg(wType));
				strWriteBuff += strTemp1;
					
				//Code
				::PSCellCodeMsg((BYTE)data.GetLastDataOfTable(i, "Code"), strTemp1, strTemp2);	//Pscommon.dll API
				strWriteBuff += (","+strTemp1);

				//StartTime
				strWriteBuff += timeStepStart.Format(",%m/%d %H:%M:%S");

				//EndTime
				fTotTime = data.GetLastDataOfTable(i, "TotTime");
				lDay = (long)fTotTime/86400;		lHour = (long)fTotTime%86400;
				lMinute = lHour % 3600;				
				lHour = lHour / 3600;				
				lSecond = lMinute % 60;
				lMinute = lMinute / 60;
					

				timeRun.SetDateTimeSpan(lDay, lHour, lMinute, lSecond);
				timeStepStart = timeStart + timeRun;
				strWriteBuff += timeStepStart.Format(",%m/%d %H:%M:%S");
					
				//TotTime
				strWriteBuff += (","+ValueString(fTotTime, PS_TOT_TIME));
					
				//Time
				strTemp1.Format(",%.1f", data.GetLastDataOfTable(i, "Time"));
				strWriteBuff += strTemp1;
					
				//tot cycle
				strTemp1.Format(",%d", nTotCycle);
				strWriteBuff += strTemp1;

				//step cycle
				strTemp1.Format(",%d", nCurCycle);
				strWriteBuff += strTemp1;
					
				//Voltage
				fDataVtg = data.GetLastDataOfTable(i, "Voltage");
				strWriteBuff += (","+ValueString(fDataVtg, PS_VOLTAGE));
					
				//Current
				fDataCrt = data.GetLastDataOfTable(i, "Current");
				strWriteBuff += (","+ValueString(fDataCrt, PS_CURRENT));
					
				//Capacity
				fData = data.GetLastDataOfTable(i, "Capacity");

				//용량을 총용량, 충전용량, 방전용량으로 구별하여 저장 
				strTemp2 = ValueString(0.0, PS_CAPACITY);
				if(wType == PS_STEP_CHARGE)
				{
					fCapacitySum += fData;
					//fprintf(fp, ",%s,%s,%s", ValueString(fCapacitySum, PS_CAPACITY), ValueString(fData, PS_CAPACITY), strTemp2);
					strTemp1.Format(",%s,%s,%s", ValueString(fCapacitySum, PS_CAPACITY), ValueString(fData, PS_CAPACITY), strTemp2);
				}
				else if(wType == PS_STEP_DISCHARGE || wType == PS_STEP_IMPEDANCE)
				{
					fCapacitySum -= fData;
					//fprintf(fp, ",%s,%s,%s", ValueString(fCapacitySum, PS_CAPACITY), strTemp2, ValueString(fData, PS_CAPACITY));
					strTemp1.Format(",%s,%s,%s", ValueString(fCapacitySum, PS_CAPACITY), strTemp2, ValueString(fData, PS_CAPACITY));
				}
				else 
				{
					//fprintf(fp, ",%s,%s,%s", ValueString(fCapacitySum, PS_CAPACITY), strTemp2, strTemp2);
					strTemp1.Format(",%s,%s,%s", ValueString(fCapacitySum, PS_CAPACITY), strTemp2, strTemp2);
				}
				strWriteBuff += strTemp1;
				
				//Watt
				//fprintf(fp,",%.3f", fabs(fDataCrt*fDataVtg/1000.0f));
				fData = fabs(fDataCrt*fDataVtg/1000.0f);
				//fprintf(fp,",%s", ValueString(fData, PS_WATT));
				strWriteBuff += (","+ValueString(fData, PS_WATT));
				
				//WattHour
				fData = data.GetLastDataOfTable(i, "WattHour");
				//fprintf(fp,",%.1f", fData);
				//fprintf(fp,",%s", ValueString(fData, PS_WATT_HOUR));
				strWriteBuff += (","+ValueString(fData, PS_WATT_HOUR));
			
				//Average Voltage
				fData = data.GetLastDataOfTable(i, "VoltageAverage");
				//fprintf(fp, ",%s", ValueString(fData, PS_VOLTAGE));
				strWriteBuff += (","+ValueString(fData, PS_VOLTAGE));
				
				//Average Current
				fData = data.GetLastDataOfTable(i, "CurrentAverage");
				//fprintf(fp, ",%s", ValueString(fData, PS_CURRENT));
				strWriteBuff += (","+ValueString(fData, PS_CURRENT));

				//IR
				fData = data.GetLastDataOfTable(i, "IR");
				//fprintf(fp,",%.1f", fData);
				strTemp1.Format(",%.1f", fData);
				strWriteBuff += strTemp1;

				//Temperature
				fData = data.GetLastDataOfTable(i, "Temperature");
				//fprintf(fp,",%.1f", fData);
				strTemp1.Format(",%s", ValueString(fData, PS_TEMPERATURE));
				strWriteBuff += strTemp1;
				
				//Grading
				fData = data.GetLastDataOfTable(i, "Grade");
				//fprintf(fp, ",%c", (BYTE)fData);
				strTemp1.Format(",%c", (BYTE)fData);
				strWriteBuff += strTemp1;
				
				fprintf(fp, "%s\n", strWriteBuff);
				
			}	//for(int i =0; i<data.GetTableSize(); i++)
		}//	if(data.Create() == TRUE)				
	}// while (bWorking1)

	if(fp != NULL)	fclose(fp);

	//Excel로 Open 여부를 묻는다.
	strTemp1.Format("%s를 지금 Open하시겠습니까?", strCSVFile);
	if(MessageBox(strTemp1, "File Open", MB_ICONQUESTION|MB_YESNO) != IDNO)
	{
		((CCTSGraphAnalApp *)AfxGetApp())->ExecuteExcel(strCSVFile);
	}
*/	
}

void CDataView::OnUpdateExcelAllStepData(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	
}

// default file name
// scheduleName_testname_chname.csv
CString CDataView::GetDefaultExcelFileName(CString strTestPath, CString strChName)
{
	CString strSchFile, strTemp1;
	CFileFind chFinder, schFinder;
	BOOL bWorking1;
	CScheduleData schData;

	CString strTestName = strTestPath.Mid(strTestPath.ReverseFind('\\')+1);

	//2. 파일명 입력 받음	
	CString strCSVFile;	
	//기본 파일명 생성	// 기본 파일명을 스케쥴명_작업명_채널명.csv로 설정
	if(strChName.IsEmpty() && !strTestPath.IsEmpty())		//Test Select
	{
		//가장 상위 채널의 스케쥴 파일을 구한다.
		strTemp1.Format("%s\\%s", strTestPath, FILE_FIND_FORMAT);
		bWorking1 = chFinder.FindFile(strTemp1);
		while(bWorking1)
		{
			bWorking1 = chFinder.FindNextFile();
			if (chFinder.IsDirectory())
			{
				strTemp1 = chFinder.GetFilePath();
	//			strSchFile.Format("%s\\%s.sch", strTemp1, strTestName);
				strSchFile.Format("%s\\*.sch", strTemp1);
				if(schFinder.FindFile(strSchFile))
				{
					schFinder.FindNextFile();
					schFinder.GetFileName();
					strSchFile = schFinder.GetFilePath();
					break;
				}
			}
		}
		schData.SetSchedule(strSchFile);
		strCSVFile.Format("%s_%s.csv", schData.GetScheduleName() , strTestName);
	}
	else
	{
		strSchFile.Format("%s\\%s\\*.sch", strTestPath, strChName);
//		strSchFile.Format("%s\\*.sch", strChName);
		if(schFinder.FindFile(strSchFile))
		{
			schFinder.FindNextFile();
			strSchFile = schFinder.GetFilePath();
			schData.SetSchedule(strSchFile);
			strCSVFile.Format("%s_%s_%s.csv", schData.GetScheduleName(), strTestName, strChName);
		}
		else
		{
			strCSVFile.Format("%s_%s.csv", strTestName, strChName);
		}
	}

	return strCSVFile;
}	

//Grid에서 채널 선택이 바뀌면 
void CDataView::OnGridSelectChanged(NMHDR* pNMHDR, LRESULT* pResult)
{
//	NM_GRIDVIEW *pItem = (NM_GRIDVIEW *)pNMHDR;
//   int nRow = pItem->iRow;
//  int nCol = pItem->iColumn;
	
/*	if(pItem->hdr.hwndFrom == m_wndChListGrid.m_hWnd)
	{
		if(nRow < 1 || nCol <1)		return;
		TRACE("Grid selection changed(%d,%d)\n", nRow, nCol);

		CString strChName = m_wndChListGrid.GetItemText(nRow, 0);

		HTREEITEM hItem, hTreeItem;
		
		hItem = m_TestTree.GetSelectedItem();
		if(hItem == NULL)	return;

		//다시 Parent 부터 검색한다. 
		if(m_TestTree.ItemHasChildren(hItem))			//Channel Select
		{
		   hTreeItem = m_TestTree.GetChildItem(hItem);
		}
		else
		{
			hItem = m_TestTree.GetParentItem(hItem);
			hTreeItem = m_TestTree.GetChildItem(hItem);
		}
  
		while (hTreeItem != NULL)
		{
			if(strChName == m_TestTree.GetItemText(hTreeItem))
			{
				m_TestTree.SelectItem(hTreeItem);
				break;
			}
			hTreeItem = m_TestTree.GetNextItem(hTreeItem, TVGN_NEXT);
		}
	}
*/
    *pResult = 0;
}

void CDataView::LoadSelChData()
{
	HTREEITEM hItem;

	hItem =m_TestTree.GetSelectedItem();
	if(hItem == NULL)	return;

	int nIndex;
	CString strTestName;
	CStringList *pSelTestList;
	
	BeginWaitCursor();
	if(m_TestTree.ItemHasChildren(hItem))			//Test Select
	{
		//선택명 변경
		strTestName = m_TestTree.GetItemText(hItem);			//Get Test Name
		GetDlgItem(IDC_TEST_NAME_STATIC)->SetWindowText(strTestName);

		//Frame Title 변경 
//		POSITION pos = AfxGetApp()->GetFirstDocTemplatePosition();
//		CMultiDocTemplate *pTempate;
//		pTempate = (CMultiDocTemplate *)AfxGetApp()->GetNextDocTemplate(pos);
		CMainFrame *pMainFrm = (CMainFrame *)AfxGetMainWnd();
		if(pMainFrm == NULL)	return ;
		CDataFrame *pMonFrm = (CDataFrame *)pMainFrm->GetActiveFrame();
		if(pMonFrm == NULL)		return;
		pMonFrm->SetWindowText(strTestName);

		nIndex = m_TestTree.GetItemData(hItem);									//Data Folder Index
		pSelTestList = ((CDataDoc *)GetDocument())->GetSelFileList();	
/*		if(m_bShowChList && nIndex < pSelTestList->GetCount())
		{
			strTestName = pSelTestList->GetAt( pSelTestList->FindIndex(nIndex));			//FullPathName
			
			//시행한 스케쥴이 같을 경우 한채널 Step List가 아닌 시행한 채널 리스트 형태로 Data를 표기한다. 
			UpdateChListData(strTestName);
		}
*/		
	}
	else												//Channel Select			
	{
		HTREEITEM hParent = m_TestTree.GetParentItem(hItem);
		if(hParent)
		{
			nIndex = m_TestTree.GetItemData(hParent);									//Data Folder Index
			pSelTestList = ((CDataDoc *)GetDocument())->GetSelFileList();		
			if(nIndex < pSelTestList->GetCount())
			{
				strTestName = pSelTestList->GetAt( pSelTestList->FindIndex(nIndex));			//FullPathName

				CString strChName = m_TestTree.GetItemText(hItem);				//Get Ch Name
				CString strPath;
				strPath.Format("%s\\%s", strTestName, strChName);
			
				//Step File Load & Display
				if(((CDataDoc *)GetDocument())->SetChPath(strPath))		//Load Step Data File
				{
					UpdateStepData(strPath);								//Display Step Data
				}
				
				GetDlgItem(IDC_SEL_CH_TITLE_STATIC)->SetWindowText(strChName);
			}		
		}
	}

	EndWaitCursor();
}

void CDataView::OnGraphView() 
{
	// TODO: Add your command handler code here	
	//선택채널의 결과 Data 폴더명을 모은다.
	//폴더명1\n폴더명2\n....으로 전송('\n'으로 폴더들을 구별하여 보낸다.)
	HTREEITEM hItem =m_TestTree.GetSelectedItem();
	ASSERT(hItem);

	//1. 선택한 Test명 채널명 구함
	CString strTestPath, strChName, strData, strTemp;
	int nIndex;
	CFileFind chFinder;
	BOOL bWorking1;
	
	CStringList *pSelTestList = ((CDataDoc *)GetDocument())->GetSelFileList();	
	
	if(m_TestTree.ItemHasChildren(hItem))								//Test Select
	{
		//선택명 변경
		nIndex = m_TestTree.GetItemData(hItem);
		ASSERT(pSelTestList->FindIndex(nIndex));
		strTestPath = pSelTestList->GetAt( pSelTestList->FindIndex(nIndex));			//FullPathName

		strTemp.Format("%s\\%s", strTestPath, FILE_FIND_FORMAT);
		bWorking1 = chFinder.FindFile(strTemp);
		while(bWorking1)
		{
			bWorking1 = chFinder.FindNextFile();
			if (chFinder.IsDirectory())
			{
				strData = strData+chFinder.GetFilePath()+"\n";
			}
		}
	}
	else																				//Channel Select			
	{
		HTREEITEM hParent = m_TestTree.GetParentItem(hItem);
		nIndex = m_TestTree.GetItemData(hParent);										//Data Folder Index
		ASSERT(pSelTestList->FindIndex(nIndex));
		strTestPath = pSelTestList->GetAt( pSelTestList->FindIndex(nIndex));			//FullPathName
		strChName = m_TestTree.GetItemText(hItem);				//Get Ch Name
		
		strData.Format("%s\\%s\n", strTestPath, strChName);
	}

	if(strData.IsEmpty())
	{
		AfxMessageBox(TEXT_LANG[32]); //000637
		return;
	}

	nIndex = strData.GetLength()+1;
	char *pData = new char[nIndex];
	sprintf(pData, "%s", strData);
	pData[nIndex-1] = '\0';
	COPYDATASTRUCT CpStructData;
	CpStructData.dwData = 4;		//CTSMonPro Program 구별 Index
	CpStructData.cbData = nIndex;
	CpStructData.lpData = pData;

//	TRACE("Send File %s\n", pData);
		
	::SendMessage(AfxGetMainWnd()->m_hWnd, WM_COPYDATA, 0, (LPARAM)&CpStructData);			
	delete [] pData;
}

//  [6/22/2009 kky ]
// CSTAnal에서 가져온 데이터 경로를 이용해서 선택채널의 결과 Data 폴더명을 모은다.
void CDataView::OnAnalToGraphDataView( CString strPath ) 
{
	// TODO: Add your command handler code here
	//폴더명1\n폴더명2\n....으로 전송('\n'으로 폴더들을 구별하여 보낸다.)
	//1. 선택한 Test명 채널명 구함
	CString strTestPath, strChName, strData, strTemp;
	int nIndex;
	CFileFind FileFinder;
	BOOL bWorking1;
	
	BOOL bFind = FileFinder.FindFile(strPath+"\\*");
	while(bFind)
	{
		bFind = FileFinder.FindNextFile();
		if(FileFinder.IsDots())
		{
			TRACE( FileFinder.GetFilePath() );
		}		
	}

// 	nIndex = strData.GetLength()+1;
// 	char *pData = new char[nIndex];
// 	sprintf(pData, "%s", strData);
// 	pData[nIndex-1] = '\0';
// 	COPYDATASTRUCT CpStructData;
// 	CpStructData.dwData = 4;		//CTSMonPro Program 구별 Index
// 	CpStructData.cbData = nIndex;
// 	CpStructData.lpData = pData;
// 	//	TRACE("Send File %s\n", pData);
// 	::SendMessage(AfxGetMainWnd()->m_hWnd, WM_COPYDATA, 0, (LPARAM)&CpStructData);
// 	delete [] pData;
}

//update all grid
//1. update step end grid
//2. update cell list grid
void CDataView::DisplayCurSelCh()
{
	HTREEITEM hItem =m_TestTree.GetSelectedItem();
	ASSERT(hItem);

	int nIndex = 0;
	CString strTestName, strChName;
	CStringList *pSelTestList;
	
	CWaitCursor wait;

	if(m_TestTree.ItemHasChildren(hItem))			//Test Select
	{
		strTestName = m_TestTree.GetItemText(hItem);
		nIndex = m_TestTree.GetItemData(hItem);									//Data Folder Index
	}
	else
	{
		HTREEITEM hParent = m_TestTree.GetParentItem(hItem);
		if(hParent)
		{
			strTestName = m_TestTree.GetItemText(hParent);
			nIndex = m_TestTree.GetItemData(hParent);
		}
		strChName = m_TestTree.GetItemText(hItem);				//Get Ch Name
	}						//Data Folder Index

	pSelTestList = ((CDataDoc *)GetDocument())->GetSelFileList();	
	if(nIndex < pSelTestList->GetCount() && !strTestName.IsEmpty())
	{
		//시행한 스케쥴이 같을 경우 한채널 Step List가 아닌 시행한 채널 리스트 형태로 Data를 표기한다. 
		strTestName = pSelTestList->GetAt( pSelTestList->FindIndex(nIndex));			//FullPathName
		
//		if(m_bShowChList)
//			UpdateChListData(strTestName);
		
		if(!strChName.IsEmpty())
		{
			CString strPath;
			strPath.Format("%s\\%s", strTestName, strChName);
			
			//Step File Load & Display
			if(((CDataDoc *)GetDocument())->SetChPath(strPath))		//Load Step Data File
			{
				UpdateStepData(strPath);								//Display Step Data	
			}
			
			//Channel List Grid에서 현재 선택한 채널을 찾아 선택한 상태로 표시한다.
/*			for(int row =0; row < m_wndChListGrid.GetRowCount(); row++)
			{
				if(m_wndChListGrid.GetItemText(row+1, 0) == strChName)
				{
					m_wndChListGrid.SetSelectedRange(row+1, 0, row+1, m_wndChListGrid.GetColumnCount()-1);
					break;
				}
			}
*/
		}
		else
		{
			m_wndStepGrid.DeleteNonFixedRows();
			m_wndStepGrid.Refresh();
		}
	}


}


// BOOL CDataView::UpdateRangeList()
// {
// 
// 	CWaitCursor wait;
// 
// 	UpdateData();
// 
// 	int p1=0, p2=0, p3=0;
// 	m_RangeList.RemoveAll();
// 
// 	if(m_strRangeString.IsEmpty())	
// 	{
// 		return TRUE;
// 	}
// 
// // 	// 같으면, Main Frame의 상태바에 Progress바를 생성하여 데이터 loading상황을 보여줄 준비를 한다.
// // 	/*♠*/ CStatusBar* pStatusBar = (CStatusBar*)(((CMDIFrameWnd*)AfxGetMainWnd())->GetDlgItem(AFX_IDW_STATUS_BAR));
// // 	/*♠*/ CProgressCtrl ProgressCtrl;
// // 	/*♠*/ CRect rect;
// // 	/*♠*/ pStatusBar->GetItemRect(0,rect);
// // 	/*♠*/ rect.left = rect.Width()/2;
// // 	/*♠*/ ProgressCtrl.Create(WS_CHILD|WS_VISIBLE,rect,pStatusBar,777);
// // 	/*♠*/ ProgressCtrl.SetRange(0, 100);
// // 	/*♠*/ ((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText("Index searching......");
// 
// 	
// 	//개체수가 많아질 경우는 CList의 Find 속도가 많이 느림 
// 	//속도 개선 필요
// 
// 	CString str, str1;
// 	long cyc;
// 	while(TRUE)
// 	{
// 		p2 = m_strRangeString.Find(',',p1);
// 		if(p2 > 0)
// 		{
// 			str = m_strRangeString.Mid(p1, p2-p1);
// 			p3 = str.Find('-');
// 			if(p3 > 0)
// 			{
// 				str1 = str.Left(p3);
// 				
// 				//CheckDigit(str1);
// 				long from = atoi(str1);
// 
// 				str1 = str.Mid(p3+1);
// 				//CheckDigit(str);
// 				long to   = atoi(str1);
// 				
// 				if(from <= to)
// 				{
// 					//최초의 연속된 번호는 무조건 넣는다.
// 					if(m_RangeList.GetCount() < 1)	// ex) 1-10000만 입력이 되었을 경우 속도 개선
// 					{
// 						for(long cyc=from; cyc<=to; cyc++)
// 						{
// 							m_RangeList.AddTail(cyc);
// 						}
// 					}
// 					else
// 					{
// 						for(long cyc=from; cyc<=to; cyc++)
// 						{
// 							if(m_RangeList.Find(cyc)==NULL) m_RangeList.AddTail(cyc);
// 						}			
// 					}
// 				}	
// 			}
// 			else
// 			{
// 				//CheckDigit(str);
// 				cyc = atol(str);
// 				if(m_RangeList.Find(cyc)==NULL) m_RangeList.AddTail(cyc);
// 			}
// 		}
// 		else 
// 		{
// 			str = m_strRangeString.Mid(p1);
// 			if(!str.IsEmpty())
// 			{
// 				p3 = str.Find('-');
// 				if(p3 > 0)
// 				{
// 					str1 = str.Left(p3);
// 					//CheckDigit(str1);
// 					long from = atoi(str1);
// 
// 					str1 = str.Mid(p3+1);
// 					//CheckDigit(str);
// 					long to   = atoi(str1);
// 					
// 					if(from <= to)
// 					{
// 						//최초의 연속된 번호는 무조건 넣는다.
// 						if(m_RangeList.GetCount() < 1)	// ex) 1-10000만 입력이 되었을 경우 속도 개선
// 						{
// 							for(long cyc=from; cyc<=to; cyc++)
// 							{
// 								m_RangeList.AddTail(cyc);
// 							}
// 						}
// 						else 
// 						{
// 							for(long cyc=from; cyc<=to; cyc++)
// 							{
// 								if(m_RangeList.Find(cyc)==NULL) m_RangeList.AddTail(cyc);
// 							}
// 						}						
// 					}	
// 				}
// 				else
// 				{
// 					//CheckDigit(str);
// 					cyc = atol(str);
// 					if(m_RangeList.Find(cyc)==NULL) m_RangeList.AddTail(cyc);
// 				}				
// 			}
// 			break;
// 		}
// 		p1=p2+1;
// 	}
// 
// //	((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText("");
// 
// 
// /*	POSITION pos;
// 	pos = m_RangeList.GetHeadPosition();
// 	while(pos)
// 	{
// 		TRACE("%d\n", m_RangeList.GetNext(pos));
// 	}
// 
// 	TRACE("\n");
// */	return TRUE;
// }

void CDataView::OnApplyButton() 
{
	// TODO: Add your control notification handler code here
	DisplayCurSelCh();	

	this->SetFocus();		//메인 화면이 닫히는 것을 방지하기 위해
}

void CDataView::OnStepStartDetail() 
{
	// TODO: Add your command handler code here
	HTREEITEM hItem =m_TestTree.GetSelectedItem();
	if(hItem == NULL)	return;

	CWaitCursor wait;

	//1. 선택한 Test명 채널명 구함
	CString strTestPath, strChName, strTemp1, strTemp2, strTestName;
	int nIndex;
	CStringList *pSelTestList = ((CDataDoc *)GetDocument())->GetSelFileList();
	if(m_TestTree.ItemHasChildren(hItem))								//Test Select
	{
		//선택명 변경
		nIndex = m_TestTree.GetItemData(hItem);
		ASSERT(pSelTestList->FindIndex(nIndex));
		strTestPath = pSelTestList->GetAt( pSelTestList->FindIndex(nIndex));			//FullPathName
	}
	else																				//Channel Select			
	{
		HTREEITEM hParent = m_TestTree.GetParentItem(hItem);
		nIndex = m_TestTree.GetItemData(hParent);										//Data Folder Index
		ASSERT(pSelTestList->FindIndex(nIndex));
		strTestPath = pSelTestList->GetAt( pSelTestList->FindIndex(nIndex));			//FullPathName
		strChName = m_TestTree.GetItemText(hItem);				//Get Ch Name
	}

	if(strTestPath.IsEmpty())	return;
	strTestName = strTestPath.Mid(strTestPath.ReverseFind('\\')+1);

//	CString strSchFile;
	CFileFind chFinder2;
//	CScheduleData schData;//, *pSchData;
	CString strCSVFile;	

	//2. 파일명 입력 받음	
	//기본 파일명 생성	// 기본 파일명을 스케쥴명_작업명_채널명.csv로 설정
/*	if(strChName.IsEmpty() && !strTestPath.IsEmpty())		//Test Select
	{
		//가장 상위 채널의 스케쥴 파일을 구한다.
		strTemp1.Format("%s\\%s", strTestPath, FILE_FIND_FORMAT);
		bWorking1 = chFinder.FindFile(strTemp1);
		while(bWorking1)
		{
			bWorking1 = chFinder.FindNextFile();
			if (chFinder.IsDirectory())
			{
				strTemp1 = chFinder.GetFilePath();
	//			strSchFile.Format("%s\\%s.sch", strTemp1, strTestName);
	//			chFinder2.Format("%s\\*.sch", strTemp1);
				if(chFinder2.FindFile(strSchFile))
				{
					chFinder2.FindNextFile();
					//schFinder.GetFileName();
					strSchFile = chFinder2.GetFilePath();
					schData.SetSchedule(strSchFile);
					break;
				}
			}
		}
		strCSVFile.Format("%s_%s.csv", schData.GetScheduleName() , strTestName);
	}
	else
	{
		strSchFile.Format("%s\\%s\\*.sch", strTestPath, strChName);
//		strSchFile.Format("%s\\*.sch", strChName);
		if(chFinder2.FindFile(strSchFile))
		{
			chFinder2.FindNextFile();
			strSchFile = chFinder2.GetFilePath();
			schData.SetSchedule(strSchFile);
			strCSVFile.Format("%s_%s_%s.csv", schData.GetScheduleName(), strTestName, strChName);
		}
		else
		{
			strCSVFile.Format("%s_%s.csv", strTestName, strChName);
		}
	}
	chFinder2.Close();
*/
	//사용자 입력 받음 
	CString strPath;
/*	CFileDialog pDlg(FALSE, "csv", strCSVFile, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, "csv file(*.csv)|*.csv|");
	if(IDOK != pDlg.DoModal())
	{
		return;
	}
	strCSVFile = pDlg.GetPathName();
	
	//3. 파일 저장 
	char *szHeaderString = "No,Type,Code,Time,TotTime,Voltage,Current,Capacity,Power,WattHour,IR,Grade\n";
	float fData, fDataVtg,fDataCrt;
	int nStepNo;
//	FILE *fp = NULL;

	if(strChName.IsEmpty())			//Test 선택시 Test를 실시한 모든 채널을 1개 파일에 저장한다.
	{
		strChName.Format("%s\\%s", strTestPath, FILE_FIND_FORMAT);
		bWorking1 = chFinder.FindFile(strChName);
		while (bWorking1)
		{
			bWorking1 = chFinder.FindNextFile();
			if(chFinder.IsDots())	continue;
			
			if (chFinder.IsDirectory())
			{
				//Channel Path(Directory)
				strPath = chFinder.GetFilePath();
				CChData data(strPath);
				if(data.Create() == TRUE)
				{
					//Selected stepNo List
//					CRowColArray awRows;
//					m_wndStepGrid.GetSelectedRows(awRows);
//					for(int row =0; row<awRows.GetSize(); row++)
//					{
//						TRACE("Selected Row %d\n", awRows[row]);
//						i = awRows[row]-1;
//					}
//					
					int nSelStep = 1;
					

					strTemp2.Format("%s\\%s_S%02d_C??????.csv", strPath, strTestName, nSelStep);
					if(chFinder2.FindFile(strTemp2))
					{
						//start file
						chFinder2.FindNextFile();
						strPath = chFinder2.GetFilePath();

						
						//최초일 경우 파일을 Open한다.
						if(	fp == NULL)
						{
							fp = fopen(strCSVFile, "wt");
							if(fp == NULL)
							{
								strTemp1.Format("%s 파일을 생성할 수 없습니다.", strCSVFile);
								AfxMessageBox(strTemp1, MB_ICONSTOP|MB_OK);
								return;
							}
						}
						
						fprintf(fp, "작업명,%s\n", strTestName);
						fprintf(fp, "채널,%s\n", strPath.Mid(strPath.ReverseFind('\\')+1));
						fprintf(fp, "시작시각,%s\n", data.GetStartTime());
						fprintf(fp, "완료시각,%s\n", data.GetEndTime());
						fprintf(fp, "작업번호,%s\n", data.GetTestSerial());
						pSchData = data.GetPattern();
						if(pSchData)
						{
							fprintf(fp, "스케쥴명,%s\n", schData.GetScheduleName());
						}
						else
						{
							fprintf(fp, "스케쥴명,정보를 찾을 수 없음\n");
						}
						
//						fprintf(fp, szHeaderString);//column header 기록
//						for(int i=0; i<data.m_TableList.GetCount(); i++)
//						{
//
//						}//for	
						
					}
				}//	if(data.Create() == TRUE)				
			} //if (chFinder.IsDirectory())
			
			if(fp != NULL)			fprintf(fp, "\n");		//channel과 channel 사이 한줄 삽입 
		
		}// while (bWorking1)

		if(fp != NULL)	fclose(fp);
	}
	else	//1 Channel Select
	{
*/		
		strPath.Format("%s\\%s", strTestPath, strChName);
		//Selected stepNo List
		int nSelStep = 0, nCycNo = 0;

		CRowColArray awRows;		//표기된 Step과 선택 Step이 다를 경우???
		m_wndStepGrid.GetSelectedRows(awRows);

		CChData *pChData = ((CDataDoc *)GetDocument())->GetChData();
		if(pChData == NULL)		return;
				
		for(int row =0; row<awRows.GetSize(); row++)
		{
			TRACE("Selected Row %d\n", awRows[row]);
			//nSelStep = atoi(m_wndStepGrid.GetItemText(awRows[row], 0));	//=> STEP No
			int nSelStepIndex = m_wndStepGrid.GetItemData(awRows[row], 0);
			nSelStep = pChData->GetTableNo(nSelStepIndex);

//			strTemp1 = m_wndStepGrid.GetItemText(awRows[row], 1);
//			nCycNo = atoi(strTemp1.Mid(strTemp1.ReverseFind('/')+1));	//=> Cycle No
			nCycNo = (long)pChData->GetLastDataOfTable(nSelStepIndex, RS_COL_TOT_CYCLE);

			strTemp2.Format("%s\\StepStart\\*_C%06d_S%02d.csv", strPath, nCycNo, nSelStep);
			strCSVFile.Empty();
			if(chFinder2.FindFile(strTemp2))
			{
				//start file
				chFinder2.FindNextFile();
				strCSVFile = chFinder2.GetFilePath();
			}

			//Excel로 Open 여부를 묻는다.
		//	strTemp1.Format("%s를 지금 Open하시겠습니까?", strCSVFile);
		//	if(MessageBox(strTemp1, "File Open", MB_ICONQUESTION|MB_YESNO) != IDNO)
		//	{
				if(strCSVFile.IsEmpty())
				{
					strCSVFile.Format("Step %d의 시작 상세 정보를 찾을 수 없습니다.", nSelStep);
					AfxMessageBox(strCSVFile);
				}
				else
				{
//#ifdef _EDLC_TEST_SYSTEM
					STARTUPINFO	stStartUpInfo;
					PROCESS_INFORMATION	ProcessInfo;
					ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
					ZeroMemory(&ProcessInfo, sizeof(PROCESS_INFORMATION));
							
					stStartUpInfo.cb = sizeof(STARTUPINFO);
					stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
					stStartUpInfo.wShowWindow = SW_NORMAL;

					char szBuff[MAX_PATH], szBuff1[MAX_PATH];
					::GetModuleFileName(AfxGetApp()->m_hInstance,szBuff,_MAX_PATH);
					CString strTemp3(szBuff);
					//strTemp1.Format("%s\\DCRScope.exe %s", strTemp3.Left(strTemp3.ReverseFind('\\')), strCSVFile);
					strTemp1.Format("%s\\DCRScope.exe", strTemp3.Left(strTemp3.ReverseFind('\\')));

					GetShortPathName((LPSTR)(LPCTSTR)strTemp1, szBuff, _MAX_PATH);
					GetShortPathName((LPSTR)(LPCTSTR)strCSVFile, szBuff1, _MAX_PATH);
	
					strTemp1.Format("%s \"%s\"", szBuff, strCSVFile);
					//strTemp1.Format("%s %s", szBuff, szBuff1);
		
					BOOL bFlag = ::CreateProcess(NULL, (LPSTR)(LPCTSTR)strTemp1, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &stStartUpInfo, &ProcessInfo);
					if(bFlag == FALSE)
					{
						strTemp1.Format(TEXT_LANG[33], strCSVFile);
						AfxMessageBox(strTemp1, MB_OK|MB_ICONSTOP);
					}	
/*
#else
					((CCTSGraphAnalApp *)AfxGetApp())->ExecuteExcel(strCSVFile);
#endif
*/
				}
		//	}	
		}

//	}		
}

void CDataView::OnUpdateStepStartDetail(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	CWnd *pWnd= GetFocus();
	if(&m_wndStepGrid == (CGridCtrl *)pWnd)
	{
		pCmdUI->Enable(m_wndStepGrid.GetSelectedCount() > 0);	
	}
	else
	{
		pCmdUI->Enable(FALSE);	
	}		
}

void CDataView::OnUserOption() 
{
	// TODO: Add your command handler code here
	CUserSetDlg	dlg;
	if(dlg.DoModal() == IDOK)
	{
	//	UpdateUnitSetting();
		m_UnitTrans.UpdateUnitSetting();

		DisplayCurSelCh();
		this->SetFocus();		//메인 화면이 닫히는 것을 방지하기 위해
	}
}

/*void CDataView::UpdateUnitSetting()
{
	m_UnitTrans.UpdateUnitSetting();
	char szBuff[32], szUnit[16], szDecimalPoint[16];

	sprintf(szBuff, AfxGetApp()->GetProfileString(REG_CONFIG_SECTION, "V Unit", "V 3"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strVUnit = szUnit;
	m_nVDecimal = atoi(szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(REG_CONFIG_SECTION, "I Unit", "mA 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strIUnit = szUnit;
	m_nIDecimal = atoi(szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(REG_CONFIG_SECTION, "C Unit", "mAh 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strCUnit = szUnit;
	m_nCDecimal = atoi(szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(REG_CONFIG_SECTION, "W Unit", "mW 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strWUnit = szUnit;
	m_nWDecimal = atoi(szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(REG_CONFIG_SECTION, "Wh Unit", "mWh 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strWhUnit = szUnit;
	m_nWhDecimal = atoi(szDecimalPoint);


	sprintf(szBuff, AfxGetApp()->GetProfileString(REG_CONFIG_SECTION, "Time Unit", "0"));
	m_nTimeUnit = atol(szBuff);


}
*/
CString CDataView::ValueString(double dData, int item, BOOL bUnit)
{
	return m_UnitTrans.ValueString(dData, item, bUnit);

/*	CString strMsg, strTemp;
	double dTemp;
	char szTemp[8];

	dTemp = dData;
	switch(item)
	{
	case PS_STEP_TYPE:	strMsg = ::PSGetTypeMsg((WORD)dData);
		break;

	case PS_STATE:		strMsg = ::PSGetStateMsg((WORD)dData);
		break;
		
	case PS_VOLTAGE:		//voltage

		//mV단위로 변경 
		if(m_strVUnit == "V")
		{
			dTemp = dTemp / 1000.0f;	//LONG2FLOAT(Value);
		}
		if(m_strVUnit == "uV")
		{
			dTemp = dTemp * 1000.0f;	//LONG2FLOAT(Value);
		}
		//else //if(m_strVUnit == "mV")
		//{
		//}
		
		if(m_nVDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nVDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}		
		if(bUnit) strMsg+= (" "+m_strVUnit);
		
		break;

	case PS_CURRENT:		//current
//		if(dTemp < 0)	dTemp = -dTemp;			//양수 표기

		//m단위로 변경
		if(m_nCurrentUnitMode)		dTemp = dTemp /1000.0f;
		
		//current
		if(m_strIUnit == "A")
		{
			dTemp = dTemp/1000.0f;
		}  
		else if(m_strIUnit == "uA")
		{
			dTemp = dTemp * 1000.0f;		// LONG2FLOAT(Value)/1000.0f;
		}
		//else if(m_strIUnit == "mA")	//mA
		//{
		//}

		if(m_nIDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nIDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}
		if(bUnit) strMsg+= (" "+m_strIUnit);

		break;
			
	case PS_WATT	:
		if(dTemp < 0)	dTemp = -dTemp;			//양수 표기

		//m단위로 변경
		if(m_nCurrentUnitMode)		dTemp = dTemp /1000.0f;

		if(m_strWUnit == "W")
		{
			dTemp = dTemp/1000.0f;
		}
		else if(m_strWUnit == "KW")
		{
			dTemp = dTemp/1000000.0f;
		}
		else if(m_strWUnit == "uW")
		{
			dTemp = dTemp*1000.0f;
		}

		if(m_nWDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nWDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}
		if(bUnit) strMsg+= (" "+m_strWUnit);
		break;	
	
	case PS_WATT_HOUR:			
		if(dTemp < 0)	dTemp = -dTemp;			//양수 표기

		//m단위로 변경
		if(m_nCurrentUnitMode)		dTemp = dTemp /1000.0f;

		if(m_strWhUnit == "Wh")
		{
			dTemp = dTemp/1000.0f;
			strTemp = " Wh";
		}
		else if(m_strWhUnit == "KWh")
		{
			dTemp = dTemp/1000000.0f;
		}
		else if(m_strWhUnit == "uWh")
		{
			dTemp = dTemp*1000.0f;
		}

		if(m_nWhDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nWhDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}
		if(bUnit) strMsg+= (" "+m_strWhUnit);
		
		break;
		
	case PS_CAPACITY:		//capacity

		//m단위로 변경
		if(m_nCurrentUnitMode)		dTemp = dTemp / 1000.0f;

		if(m_strCUnit == "Ah" || m_strCUnit == "F")
		{
			dTemp = dTemp/1000.0f;
		}
		else if(m_strCUnit == "uAh" || m_strCUnit == "uF")
		{
			dTemp = dTemp*1000.0f;
		}

		if(m_nCDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nCDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}
		if(bUnit) strMsg+= (" "+m_strCUnit);
		break;

	case PS_IMPEDANCE:	
		strMsg.Format("%.1f", dTemp);
		if(bUnit) strMsg+= " mOhm";
		break;

	case PS_CODE:	//failureCode
		::PSCellCodeMsg((BYTE)dData, strMsg, strTemp);	//Pscommon.dll API
		break;

	case PS_TOT_TIME:
	case PS_STEP_TIME:

		//sbc에서 100msec단위 전송되어 온다.
		if(m_nTimeUnit == 1)	//sec 표시
		{
			strMsg.Format("%.1f", dTemp);
			if(bUnit) strMsg+= " sec";
		}
		else if(m_nTimeUnit == 2)	//Minute 표시
		{
			strMsg.Format("%.2f", dTemp/60.0f);
			if(bUnit) strMsg+= " Min";
		}
		else
		{
			CTimeSpan timeSpan((ULONG)dTemp);
			if(timeSpan.GetDays() > 0)
			{
				strMsg =  timeSpan.Format("%Dd %H:%M:%S");
			}
			else
			{
				strMsg = timeSpan.Format("%H:%M:%S");
			}
		}

		break;

	case PS_GRADE_CODE:	
		strMsg.Format("%c", (BYTE)dData);
		break;
	
	case PS_TEMPERATURE:
		if(0xFFFFFFFF == (float)dTemp)
		{
			strMsg = "미사용";
		}
		else
		{
			strMsg.Format("%.1f", dTemp);
			if(bUnit) strMsg+= " ℃";
		}
		
		break;

	case PS_STEP_NO:
	default:
		strMsg.Format("%d", (int)dTemp);
		break;
	}
	
	return strMsg;
*/
}

CString CDataView::GetUnitString(int nItem)
{
	return m_UnitTrans.GetUnitString(nItem);

/*	CString	strTemp;

	switch(nItem)
	{
	case PS_VOLTAGE:		return	m_strVUnit;

	case PS_CURRENT:
		{
			strTemp = m_strIUnit;
			return	strTemp;						
		}
			
	//capa 단위를 따라감
	case PS_WATT	:	return m_strWUnit;


	
	//capa 단위를 따라감
	case PS_WATT_HOUR:			return m_strWhUnit;
		
	case PS_CAPACITY:
		strTemp = m_strCUnit;
	return strTemp;

	case PS_IMPEDANCE:		strTemp = "mOhm";
		return strTemp;

	case PS_TOT_TIME:
	case PS_STEP_TIME:

		//sbc에서 100msec단위 전송되어 온다.
		if(m_nTimeUnit == 1)	//sec 표시
		{
			strTemp = "sec";
		}
		else if(m_nTimeUnit == 2)	//Minute 표시
		{
			strTemp = "Min";
		}
		return strTemp;

	case PS_TEMPERATURE:
		strTemp = "℃";
		return strTemp;

	case PS_STEP_TYPE:
	case PS_STATE:	
	case PS_CODE:	//failureCode
	case PS_GRADE_CODE:	
	case PS_STEP_NO:
	default:
		break;
	}

	return strTemp;
*/
}

BOOL CDataView::AddStepStartData()
{
/*	HTREEITEM hItem =m_TestTree.GetSelectedItem();
	ASSERT(hItem);

	CWaitCursor wait;

	//1. 선택한 Test명 채널명 구함
	CString strTestPath, strChName, strTemp1, strTemp2, strTestName;
	int nIndex;
	CStringList *pSelTestList = ((CDataDoc *)GetDocument())->GetSelFileList();
	if(m_TestTree.ItemHasChildren(hItem))								//Test Select
	{
		//선택명 변경
		nIndex = m_TestTree.GetItemData(hItem);
		ASSERT(pSelTestList->FindIndex(nIndex));
		strTestPath = pSelTestList->GetAt( pSelTestList->FindIndex(nIndex));			//FullPathName
	}
	else																				//Channel Select			
	{
		HTREEITEM hParent = m_TestTree.GetParentItem(hItem);
		nIndex = m_TestTree.GetItemData(hParent);										//Data Folder Index
		ASSERT(pSelTestList->FindIndex(nIndex));
		strTestPath = pSelTestList->GetAt( pSelTestList->FindIndex(nIndex));			//FullPathName
		strChName = m_TestTree.GetItemText(hItem);				//Get Ch Name
	}

	if(strTestPath.IsEmpty())	return;
	strTestName = strTestPath.Mid(strTestPath.ReverseFind('\\')+1);

	CString strSchFile;
	CFileFind chFinder, chFinder2;
	CScheduleData schData;//, *pSchData;
	CString strCSVFile;	


	strPath.Format("%s\\%s", strTestPath, strChName);
	//Selected stepNo List
	int nSelStep = 0, nCycNo = 0;
	CRowColArray awRows;
	m_wndStepGrid.GetSelectedRows(awRows);
			
	for(int row =0; row<awRows.GetSize(); row++)
	{
		TRACE("Selected Row %d\n", awRows[row]);
		nSelStep = atoi(m_wndStepGrid.GetItemText(awRows[row], 0));	//=> STEP No
		strTemp1 = m_wndStepGrid.GetItemText(awRows[row], 1);
		nCycNo = atoi(strTemp1.Mid(strTemp1.ReverseFind('/')+1));	//=> Cycle No

		strTemp2.Format("%s\\StepStart\\%s_C%06d_S%02d.csv", strPath, strTestName, nCycNo, nSelStep);
			
		strCSVFile.Empty();
		if(chFinder2.FindFile(strTemp2))
		{
			//start file
			chFinder2.FindNextFile();
			strCSVFile = chFinder2.GetFilePath();

		}
	}
	
	if(strCSVFile.IsEmpty())	return;

	NM_GRIDVIEW *pItem = (NM_GRIDVIEW *)pNMHDR;
    *pResult = 0;
    int nRow = pItem->iRow;
    int nCol = pItem->iColumn;
	
	if(pItem->hdr.hwndFrom == m_wndStepGrid.m_hWnd)
	{
		TRACE("Cell (%d, %d) left button double clicked\n", nRow, nCol);
		if(nRow > 0 && nCol >0)
		{
			//CString strStep = m_wndStepGrid.GetItemText(nRow, 0);
			CString strName, strName2, strTemp;
			strName.Format("Step %s : %s", m_wndStepGrid.GetItemText(nRow, 0), m_wndStepGrid.GetItemText(nRow, 2));
			GetDlgItem(IDC_STEP_RAW_STATIC)->SetWindowText(strName);

			strName = m_wndStepGrid.GetItemText(nRow, 1);
			GetDlgItem(IDC_TEST_NAME_STATIC)->GetWindowText(strTemp);
			CChData *pChData =((CDataDoc *)GetDocument())->GetChData();
			
			strName2.Format("%s\\StepStart\\%s_C%06d_S%02d.csv", pChData->GetPath(), strTemp, 
							atoi(strName.Mid(strName.ReverseFind('/')+1)),
							atol(m_wndStepGrid.GetItemText(nRow, 0))
							);
			
			
			
			CFileFind fFind;
			GetDlgItem(IDC_DETAIL_STEP_START_BUTTON)->EnableWindow(fFind.FindFile(strName2));

			int nTableIndex = m_wndStepGrid.GetItemData(nRow, 0);
			UpdateStepRawData(nTableIndex);
		}
	}
*/
	return TRUE;
}

void CDataView::OnExcelDetailButton() 
{
	// TODO: Add your control notification handler code here
	CChData *pChData = ((CDataDoc *)GetDocument())->GetChData();
	if(pChData == NULL)		return;

	CString strTemp = _T("");

	/*
	//File 명에 []가 포함된 파일을 Excel로 Open하면 Sheet명이 이상(Excel Bug)하여 참조오류가 발생하여 그래프를 그릴수 없다.
	strChName.Replace('[', '(');
	strChName.Replace(']', ')');
	strCSVFile.Format("%s\\%s_%s.csv", pChData->GetPath(), strTestName, strChName);
	*/

	if(SaveToExcelFile(pChData->GetPath(), pChData->GetPath()) < 1)
	{
		AfxMessageBox("IDS_DATAVIEW_47"); //000638
		return;
	}

	((CCTSGraphAnalApp *)AfxGetApp())->ExecuteExcel(pChData->GetPath());
}

//Save Step End data to excel file
int CDataView::SaveToExcelFile(CString strFilter, CString strSaveFile)
{
	float fData, fDataVtg,fDataCrt;
	int nStepNo;
	long lCheckCycle = 0;
	FILE *fp = NULL;
	WORD wType;
	float	fCapacitySum = 0.0f, fCapacityChar= 0.0f, fCapacityDisChar = 0.0f;
	float	fTotTime = 0.0f;
	COleDateTime	timeStart, timeStepStart;
	COleDateTimeSpan	timeRun;
	long lDay, lHour, lMinute, lSecond;

	CWaitCursor wait;
	CString strTemp, strName;

	//3. 파일 저장 
	CString strHeaderString, strTemp1, strTemp2, strPath, strTestName;
	strTemp1 = m_UnitTrans.GetUnitString(PS_STEP_TIME);
	if(strTemp1.IsEmpty())
	{
		strHeaderString.Format(TEXT_LANG[34], 
								m_UnitTrans.GetUnitString(PS_VOLTAGE), 
								m_UnitTrans.GetUnitString(PS_CURRENT),
								m_UnitTrans.GetUnitString(PS_CAPACITY),
								m_UnitTrans.GetUnitString(PS_CAPACITY),
								m_UnitTrans.GetUnitString(PS_CAPACITY),
								m_UnitTrans.GetUnitString(PS_CAPACITY),
								m_UnitTrans.GetUnitString(PS_CAPACITY),
								m_UnitTrans.GetUnitString(PS_WATT),
								m_UnitTrans.GetUnitString(PS_WATT_HOUR),
								m_UnitTrans.GetUnitString(PS_VOLTAGE),
								m_UnitTrans.GetUnitString(PS_CURRENT)
								); //000640
	}
	else
	{
		strHeaderString.Format(TEXT_LANG[35], 
								strTemp1, strTemp1, 
								m_UnitTrans.GetUnitString(PS_VOLTAGE), 
								m_UnitTrans.GetUnitString(PS_CURRENT),
								m_UnitTrans.GetUnitString(PS_CAPACITY),
								m_UnitTrans.GetUnitString(PS_CAPACITY),
								m_UnitTrans.GetUnitString(PS_CAPACITY),
								m_UnitTrans.GetUnitString(PS_CAPACITY),
								m_UnitTrans.GetUnitString(PS_CAPACITY),
								m_UnitTrans.GetUnitString(PS_WATT),
								m_UnitTrans.GetUnitString(PS_WATT_HOUR),
								m_UnitTrans.GetUnitString(PS_VOLTAGE),
								m_UnitTrans.GetUnitString(PS_CURRENT)
								); //000641
	}


	CFileFind chFinder;
	CScheduleData *pSchData;
	BOOL bWorking1 = chFinder.FindFile(strFilter);
	GetDlgItem(IDC_TEST_NAME_STATIC)->GetWindowText(strTestName);

	if(bWorking1 == FALSE)	return 0;

	// 같으면, Main Frame의 상태바에 Progress바를 생성하여 데이터 loading상황을 보여줄 준비를 한다.
	/*♠*/ CStatusBar* pStatusBar = (CStatusBar*)(((CMDIFrameWnd*)AfxGetMainWnd())->GetDlgItem(AFX_IDW_STATUS_BAR));
	/*♠*/ CProgressCtrl ProgressCtrl;
	/*♠*/ CRect rect;
	/*♠*/ pStatusBar->GetItemRect(0,rect);
	/*♠*/ rect.left = rect.Width()/2;
	/*♠*/ ProgressCtrl.Create(WS_CHILD|WS_VISIBLE,rect,pStatusBar,777);
	/*♠*/ ((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText("data loading...");

	float fImpTime1 = atof(AfxGetApp()->GetProfileString(REG_CONFIG_SECTION, "ImpTime1", "0.0"));
	float fImpTime2 = atof(AfxGetApp()->GetProfileString(REG_CONFIG_SECTION, "ImpTime2", "0.01"));
	BOOL bUserImpTime = AfxGetApp()->GetProfileInt(REG_CONFIG_SECTION, "UserImp", FALSE);

	long nRowCount = 0;
	int nFileCount = 1;
	while (bWorking1)
	{
			bWorking1 = chFinder.FindNextFile();
			if(chFinder.IsDots())	continue;

			if (chFinder.IsDirectory())
			{
				strPath = chFinder.GetFilePath();
				CChData data(strPath);
				if(data.Create() == TRUE)
				{
					//최초일 경우 파일을 Open한다.
					if(	fp == NULL)
					{
						fp = fopen(strSaveFile, "wt");
						if(fp == NULL)
						{
							/*♠*/ ((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText("");
							return 0;
						}
						fprintf(fp, strHeaderString);//column header 기록
						nRowCount++;
					}
					
					pSchData = data.GetPattern();
					timeStart.ParseDateTime(data.GetStartTime());
					timeStepStart.ParseDateTime(data.GetStartTime());
					
					POSITION pos = data.GetFirstTablePosition();
					int nTableSize = data.GetTableSize();
					int nCount = 0;
					//for(int i=0; i<data.m_TableList.GetCount(); i++)

					ProgressCtrl.SetRange(0, data.GetTableSize());

					while(pos)
					{
						CTable *tb = data.GetNextTable(pos);
						
						// ProgressCtrl.SetPos(int(float(nCount++)/float(nTableSize)*100.0f));
						ProgressCtrl.SetPos(nCount++);

						//Channel No
						fprintf(fp, "%s", strPath.Mid(strPath.ReverseFind('\\')+1));
						
						//test Name
						fprintf(fp, ",%s", strTestName);

						//Schedule name
						if(pSchData)
						{
							fprintf(fp, ",%s", pSchData->GetScheduleName());
						}
						else
						{
							fprintf(fp, ",%s", TEXT_LANG[36]); // 000641
						}

						//No
						nStepNo = tb->GetStepNo();
						fprintf(fp, ",%d", nStepNo);

						//Type
						wType = tb->GetType();
						fprintf(fp, ",%s", ::PSGetTypeMsg(wType));	//PScommon.dll
						
						//Code
						
						::PSCellCodeMsg((BYTE)tb->GetLastData(RS_COL_CODE), strTemp1, strTemp2);	//Pscommon.dll API
						fprintf(fp, ",%s", strTemp1);	//PScommon.dll

						fTotTime = tb->GetLastData(RS_COL_TOT_TIME);
						//StartTime
						fprintf(fp, ",%s", timeStepStart.Format("%m/%d %H:%M:%S"));

						//EndTime
						lDay = (long)fTotTime/86400;		lHour = (long)fTotTime%86400;
						lMinute = lHour % 3600;				
						lHour = lHour / 3600;				
						lSecond = lMinute % 60;
						lMinute = lMinute / 60;
						
						timeRun.SetDateTimeSpan(lDay, lHour, lMinute, lSecond);
						timeStepStart = timeStart + timeRun;
						fprintf(fp, ",%s", timeStepStart.Format("%m/%d %H:%M:%S"));
						
						//TotTime
						//fprintf(fp, ",%d", (long)data.GetLastDataOfTable(i, "TotTime"));
						fprintf(fp, ",%s", ValueString(fTotTime, PS_TOT_TIME));
						
						//Time
						//fprintf(fp, ",%d", long(data.GetLastDataOfTable(i, "Time")));
						fprintf(fp, ",%s", ValueString(tb->GetLastData("Time"), PS_STEP_TIME));
						
						//tot cycle
						fprintf(fp, ",%d", tb->GetCycleNo());
						if(lCheckCycle != tb->GetCycleNo())
						{
							lCheckCycle = tb->GetCycleNo();
							fCapacityChar = 0.0f;
							fCapacityDisChar = 0.0f;
						}
						

						//step cycle
						fprintf(fp, ",%d", tb->GetLastData(RS_COL_CUR_CYCLE));
						
						//Voltage
						fDataVtg = tb->GetLastData(RS_COL_VOLTAGE);
						//fprintf(fp, ",%.4f", fDataVtg/1000.0f);
						fprintf(fp, ",%s", ValueString(fDataVtg, PS_VOLTAGE));
						
						//Current
						fDataCrt = tb->GetLastData(RS_COL_CURRENT);
						//fprintf(fp, ",%.2f", fDataCrt);
						fprintf(fp, ",%s", ValueString(fDataCrt, PS_CURRENT));
						
						//Capacity
						fData = tb->GetLastData(RS_COL_CAPACITY);
						//fprintf(fp,",%.3f", fData);

						//용량을 총용량, 충전용량, 방전용량으로 구별하여 저장 
						strTemp1 = ValueString(0.0, PS_CAPACITY);
						
						if(wType == PS_STEP_CHARGE)
						{
							fCapacitySum += fData;
							fCapacityChar += fData;
							fprintf(fp, ",%s,%s,%s,%s,%s", ValueString(fCapacitySum, PS_CAPACITY), ValueString(fCapacityChar, PS_CAPACITY), ValueString(fCapacityDisChar, PS_CAPACITY), ValueString(fData, PS_CAPACITY), strTemp1);
							//fprintf(fp, ",%s,%s,%s", ValueString(fCapacitySum, PS_CAPACITY), ValueString(fData, PS_CAPACITY), strTemp1);
						}
						else if(wType == PS_STEP_DISCHARGE || wType == PS_STEP_IMPEDANCE)
						{
							fCapacitySum -= fData;
							fCapacityDisChar += fData;
							fprintf(fp, ",%s,%s,%s,%s,%s", ValueString(fCapacitySum, PS_CAPACITY), ValueString(fCapacityChar, PS_CAPACITY), ValueString(fCapacityDisChar, PS_CAPACITY), strTemp1, ValueString(fData, PS_CAPACITY));
							//fprintf(fp, ",%s,%s,%s", ValueString(fCapacitySum, PS_CAPACITY), strTemp1, ValueString(fData, PS_CAPACITY));
						}
						else 
						{
							fprintf(fp, ",%s,%s,%s,%s,%s", ValueString(fCapacitySum, PS_CAPACITY), ValueString(fCapacityChar, PS_CAPACITY), ValueString(fCapacityDisChar, PS_CAPACITY), strTemp1, strTemp1);
							//fprintf(fp, ",%s,%s,%s", ValueString(fCapacitySum, PS_CAPACITY), strTemp1, strTemp1);
						}
						
						//Watt
						//fprintf(fp,",%.3f", fabs(fDataCrt*fDataVtg/1000.0f));
						fData = (float)fabs(fDataCrt*fDataVtg/1000.0f);
						fprintf(fp,",%s", ValueString(fData, PS_WATT));
						
						//WattHour
						fData = tb->GetLastData(RS_COL_WATT_HOUR);
						//fprintf(fp,",%.1f", fData);
						fprintf(fp,",%s", ValueString(fData, PS_WATT_HOUR));
					
						//Average Voltage
						fData = tb->GetLastData(RS_COL_AVG_VTG);
						fprintf(fp, ",%s", ValueString(fData, PS_VOLTAGE));
						
						//Average Current
						fData = tb->GetLastData(RS_COL_AVG_CRT);
						fprintf(fp, ",%s", ValueString(fData, PS_CURRENT));

						//IR //DCIR
						fData = tb->GetLastData(RS_COL_IR);
						if(wType == PS_STEP_IMPEDANCE && bUserImpTime)
						{
							//Edited by KBH 2007/2/26 (사용자가 지정한 Time으로 Impdeance를 재계산 한다.)
							GetDlgItem(IDC_TEST_NAME_STATIC)->GetWindowText(strName);
							strTemp.Format("%s\\StepStart\\*_C%06d_S%02d.csv", data.GetPath(),  
											tb->GetCycleNo(),
											nStepNo
											);
					
							//파일이 많으면 오래 걸림
							CFileFind fFind;
							if(fFind.FindFile(strTemp))
							{
								fFind.FindNextFile();
								strTemp = fFind.GetFilePath();
								float fImpData = ((CDataDoc *)GetDocument())->CalImpedance(strTemp , fImpTime1, fImpTime2);
								if(fImpData >= 0.0f)
								{
									fData = fImpData;
								}
							}
						}
						fprintf(fp,",%.3f", fData);

						//Temperature
						fData = tb->GetLastData(RS_COL_TEMPERATURE);
						fprintf(fp,",%s", ValueString(fData, PS_TEMPERATURE));
						
						//Grading
						fData = tb->GetLastData(RS_COL_GRADE);
						fprintf(fp, ",%c", (BYTE)fData);
						
						fprintf(fp, "\n");

						nRowCount++;
						//최대 Excel row수 초과시 다음 파일 Open
						if(nRowCount >= MAX_EXCEL_ROW)
						{
							if(MakeNextFile(fp, nFileCount, strSaveFile) == FALSE)
							{
								/*♠*/ ((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText("");
								strTemp1.Format(TEXT_LANG[37], strSaveFile);
								AfxMessageBox(strTemp1, MB_ICONSTOP|MB_OK);
								return nFileCount;
							}

							//Excel Open
//							((CCTSGraphAnalApp *)AfxGetApp())->ExecuteExcel(strSaveFile);
								
							ASSERT(fp);
							fprintf(fp, "%s\n", strHeaderString);//column header 기록
							nRowCount = 1;		//Header 기록 
						}
					}	//for					
				}//	if(data.Create() == TRUE)				
			} //if (chFinder.IsDirectory())
			
			//if(fp != NULL)			fprintf(fp, "\n");		//channel과 channel 사이 한줄 삽입 
		
	}// while (bWorking1)

	if(fp != NULL)	fclose(fp);
	/*♠*/ ((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText("");
	return nFileCount;
}

void CDataView::OnDblclkTestTree(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here

	HTREEITEM hItem = pNMTreeView->itemNew.hItem;
	hItem =m_TestTree.GetSelectedItem();

	DisplayData(hItem);

	*pResult = 0;	
}

void CDataView::DisplayData(HTREEITEM hItem)
{
	if(hItem == NULL)	return;

	int nIndex;
	CString strTestName;
	CStringList *pSelTestList;
	
//	BeginWaitCursor();
	if(m_TestTree.ItemHasChildren(hItem))			//Test Select
	{
		//선택명 변경
		GetDlgItem(IDC_TEST_NAME_STATIC)->GetWindowText(strTestName);
		if(strTestName != m_TestTree.GetItemText(hItem))	//변경되었을 경우
		{
			strTestName = m_TestTree.GetItemText(hItem);
			GetDlgItem(IDC_TEST_NAME_STATIC)->SetWindowText(strTestName);

			//Frame Title 변경 
//			POSITION pos = AfxGetApp()->GetFirstDocTemplatePosition();
//			CMultiDocTemplate *pTempate;
//			pTempate = (CMultiDocTemplate *)AfxGetApp()->GetNextDocTemplate(pos);
			CMainFrame *pMainFrm = (CMainFrame *)AfxGetMainWnd();
			if(pMainFrm == NULL)	return ;
			CDataFrame *pMonFrm = (CDataFrame *)pMainFrm->GetActiveFrame();
			if(pMonFrm == NULL)		return;
			pMonFrm->SetWindowText(strTestName);

			nIndex = m_TestTree.GetItemData(hItem);										//Data Folder Index
			pSelTestList = ((CDataDoc *)GetDocument())->GetSelFileList();	
			
/*			if(m_bShowChList && nIndex < pSelTestList->GetCount())
			{
				strTestName = pSelTestList->GetAt( pSelTestList->FindIndex(nIndex));	//FullPathName
				//시행한 스케쥴이 같을 경우 한채널 Step List가 아닌 시행한 채널 리스트 형태로 Data를 표기한다. 
				UpdateChListData(strTestName);
			}
*/		}
	}
	else												//Channel Select			
	{
		HTREEITEM hParent = m_TestTree.GetParentItem(hItem);
		if(hParent)
		{
			strTestName = m_TestTree.GetItemText(hParent);
			GetDlgItem(IDC_TEST_NAME_STATIC)->SetWindowText(strTestName);

			nIndex = m_TestTree.GetItemData(hParent);									//Data Folder Index
			pSelTestList = ((CDataDoc *)GetDocument())->GetSelFileList();		
			if(nIndex < pSelTestList->GetCount())
			{
				strTestName = pSelTestList->GetAt( pSelTestList->FindIndex(nIndex));			//FullPathName

				CString strChName = m_TestTree.GetItemText(hItem);				//Get Ch Name
				CString strPath;
				strPath.Format("%s\\%s", strTestName, strChName);
			
				//Step File Load & Display
				if(((CDataDoc *)GetDocument())->SetChPath(strPath) == TRUE)		//Load Step Data File
				{
//					DWORD dwStart = GetTickCount();
					UpdateStepData(strPath);								//Display Step Data
//					UpdateStepDataA(strPath);
//					TRACE("%d\n", GetTickCount()-dwStart);
					if(((CDataDoc *)GetDocument())->GetCellLastState())
					{
						m_TestTree.SetItemImage(hItem, 2, 3);
					}
					else
					{
						m_TestTree.SetItemImage(hItem, 4, 5);
					}
				}

				GetDlgItem(IDC_SEL_CH_TITLE_STATIC)->SetWindowText(strChName);
				
				//Channel List Grid에서 현재 선택한 채널을 찾아 선택한 상태로 표시한다.
/*				for(int row =0; row < m_wndChListGrid.GetRowCount(); row++)
				{
					if(m_wndChListGrid.GetItemText(row+1, 0) == strChName)
					{
						m_wndChListGrid.SetSelectedRange(row+1, 0, row+1, m_wndChListGrid.GetColumnCount()-1);
						break;
					}
				}
*/			}
		}
	}

//	EndWaitCursor();
}

//저장된 모든 Step의 raw data를 Excel로 Exporting한다.
//1.Step start data
//2.Step raw data
//3.Step End data

BOOL CDataView::ExcelAllStepData(BOOL bSelStep)
{
	HTREEITEM hItem =m_TestTree.GetSelectedItem();
	ASSERT(hItem);

	//1. 선택한 Test명 채널명 구함
	CString strTestPath, strChName, strTemp1, strTemp2, strTestName;
	int nIndex;
	CStringList *pSelTestList = ((CDataDoc *)GetDocument())->GetSelFileList();
	if(m_TestTree.ItemHasChildren(hItem))								//Test Select
	{
		//선택명 변경
		nIndex = m_TestTree.GetItemData(hItem);
		ASSERT(pSelTestList->FindIndex(nIndex));
		strTestPath = pSelTestList->GetAt( pSelTestList->FindIndex(nIndex));			//FullPathName
	}
	else																				//Channel Select			
	{
		HTREEITEM hParent = m_TestTree.GetParentItem(hItem);
		if(hParent == NULL)		return	FALSE;
		nIndex = m_TestTree.GetItemData(hParent);										//Data Folder Index
		ASSERT(pSelTestList->FindIndex(nIndex));
		strTestPath = pSelTestList->GetAt( pSelTestList->FindIndex(nIndex));			//FullPathName
		strChName = m_TestTree.GetItemText(hItem);				//Get Ch Name
	}

	if(strTestPath.IsEmpty())	return	FALSE;
	strTestName = strTestPath.Mid(strTestPath.ReverseFind('\\')+1);

	CString strSchFile;
	CFileFind chFinder, schFinder;
	BOOL bWorking1;
	CScheduleData schData, *pSchData;

	//2. 파일명 입력 받음	
	CString strCSVFile;	
	//기본 파일명 생성	// 기본 파일명을 스케쥴명_작업명_채널명.csv로 설정
	if(strChName.IsEmpty() && !strTestPath.IsEmpty())		//Test Select
	{
		//가장 상위 채널의 스케쥴 파일을 구한다.
		strTemp1.Format("%s\\%s", strTestPath, FILE_FIND_FORMAT);
		bWorking1 = chFinder.FindFile(strTemp1);
		while(bWorking1)
		{
			bWorking1 = chFinder.FindNextFile();
			if (chFinder.IsDirectory())
			{
				strTemp1 = chFinder.GetFilePath();
	//			strSchFile.Format("%s\\%s.sch", strTemp1, strTestName);
				strSchFile.Format("%s\\*.sch", strTemp1);
				if(schFinder.FindFile(strSchFile))
				{
					schFinder.FindNextFile();
					//schFinder.GetFileName();
					strSchFile = schFinder.GetFilePath();
					schData.SetSchedule(strSchFile);
					break;
				}
			}
		}
		strCSVFile.Format("%s_%s.csv", schData.GetScheduleName() , strTestName);
	}
	else
	{
		strSchFile.Format("%s\\%s\\*.sch", strTestPath, strChName);
//		strSchFile.Format("%s\\*.sch", strChName);
		if(schFinder.FindFile(strSchFile))
		{
			schFinder.FindNextFile();
			strSchFile = schFinder.GetFilePath();
			schData.SetSchedule(strSchFile);
			strCSVFile.Format("%s_%s_%s.csv", schData.GetScheduleName(), strTestName, strChName);
		}
		else
		{
			strCSVFile.Format("%s_%s.csv", strTestName, strChName);
		}
	}

	//???File 명에 []가 포함된 파일을 Excel로 Open하면 Sheet명이 이상(Excel Bug)하여 참조오류가 발생하여 그래프를 그릴수 없다.
	strCSVFile.Replace('[', '(');
	strCSVFile.Replace(']', ')');
	//사용자 입력 받음 
	CFileDialog pDlg(FALSE, "csv", strCSVFile, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, "csv file(*.csv)|*.csv|");
	if(IDOK != pDlg.DoModal())
	{
		return FALSE;
	}
	strCSVFile = pDlg.GetPathName();
	
	//3. 파일 저장 
	CString strPath, strWriteBuff;
	float fStepTime, fDataVtg, fDataCrt, fDataCap, fTotTime, fWattHour, fAvgVtg, fAvgCrt, fTemper, fMeter, fImp;
	float fCapacitySum = 0, fSum = 0, fChargeCapa = 0, fDischargeCapa = 0;
	long lDay, lHour, lMinute, lSecond;
	int nStepNo;
	fltPoint *pfTimeData = NULL, *pfVtgData = NULL, *pfCrtData= NULL, *pfCapData=NULL;
	fltPoint *pfWattHour=NULL, *pfTemp=NULL, *pfAvgVtg=NULL, *pfAvgCrt=NULL, *pfMeter=NULL;
	FILE *fp = NULL;
	COleDateTime timeStart, timeStepStart;
	COleDateTimeSpan timeRun;
	WORD wType;
	int nTotCycle, nCurCycle;
	int nDataSequence = 1;
	CString strStartTime, strEndTime, strCode;
	BYTE bGrade;

	CWaitCursor wait;

	//////////////////////////////////////////////////////////////////////////
	//설정 Item Loading
	UINT nSize;
	DWORD dwItem[PS_MAX_ITEM_NUM] = {0,};
	LPVOID pData = NULL;
	int nRtn = AfxGetApp()->GetProfileBinary(REG_CONFIG_SECTION, "SaveItem", (LPBYTE *)&pData, &nSize);
	if(nSize > 0 && nRtn == TRUE)	
	{
		memcpy(dwItem, pData, min(sizeof(dwItem), nSize));
	}
	else
	{
		//default list
		for( int nI = 0; nI < PS_MAX_ITEM_NUM; nI++ )
		{
			strTemp1 = ::PSGetItemName(nI);	
			if(strTemp1.IsEmpty())
			{
				dwItem[nI] = MAKELONG(nI , FALSE);
			}
			else
			{
				dwItem[nI] = MAKELONG(nI , TRUE);
			}
		}
	}
	if(pData) delete [] pData;
	//////////////////////////////////////////////////////////////////////////

	//Serarch selected file
	if(strChName.IsEmpty())			//Test 선택시 Test를 실시한 모든 채널을 1개 파일에 저장한다.
	{
		strPath.Format("%s\\%s", strTestPath, FILE_FIND_FORMAT);
	}
	else
	{
		strPath.Format("%s\\%s", strTestPath, strChName);
	}
	
	bWorking1 = chFinder.FindFile(strPath);
	if(bWorking1 == FALSE)
	{
		AfxMessageBox(TEXT_LANG[41]); //000645
		return FALSE;
	}
	//////////////////////////////////////////////////////////////////////////

	//Make Column Header
	CString strHeaderString;
	for(int n=0; n<PS_MAX_ITEM_NUM; n++)
	{
		BOOL bSave = HIWORD(dwItem[n]);
		WORD item = LOWORD(dwItem[n]);
		strTemp1 = m_UnitTrans.GetUnitString(item);
		if(!strTemp1.IsEmpty())
		{
			strTemp2.Format("%s(%s),", ::PSGetItemName(item), strTemp1);
		}
		else
		{
			strTemp2.Format("%s,", ::PSGetItemName(item));

		}
		if(bSave)	strHeaderString += strTemp2;
	}
	//////////////////////////////////////////////////////////////////////////
	float fImpTime1 = atof(AfxGetApp()->GetProfileString(REG_CONFIG_SECTION, "ImpTime1", "0.0"));
	float fImpTime2 = atof(AfxGetApp()->GetProfileString(REG_CONFIG_SECTION, "ImpTime2", "0.01"));
	BOOL bUserImpTime = AfxGetApp()->GetProfileInt(REG_CONFIG_SECTION, "UserImp", FALSE);

	//file로 저장한다.
	CRowColArray awRows;
	m_wndStepGrid.GetSelectedRows(awRows);

	int nFileCount = 0;
	int nRowCount = 0;	//저장된 Row Count 만약 Data row 수가 Excel에서 볼수 있는 최대 수를 벗어나면 새로운 파일을 만듬 
	while (bWorking1)
	{
		bWorking1 = chFinder.FindNextFile();
		if(chFinder.IsDots())			continue;
		if (!chFinder.IsDirectory())	continue;
			
		strPath = chFinder.GetFilePath();
		CChData data(strPath);
		if(data.Create() == TRUE)
		{
			//최초일 경우 파일을 Open한다.
			if(	fp == NULL)
			{
				fp = fopen(strCSVFile, "wt");
				if(fp == NULL)
				{
					strTemp1.Format(TEXT_LANG[42], strCSVFile); //000646
					AfxMessageBox(strTemp1, MB_ICONSTOP|MB_OK);
					return FALSE;
				}
				fprintf(fp, "%s\n", strHeaderString);//column header 기록
				nRowCount++;
			}

			timeStart.ParseDateTime(data.GetStartTime());			//작업 시작 시간
			timeStepStart.ParseDateTime(data.GetStartTime());		//Step시작 시간 

			pSchData = data.GetPattern();
			fCapacitySum = 0;
			fTotTime = 0.0f;

			/*♠*/ CStatusBar* pStatusBar = (CStatusBar*)(((CMDIFrameWnd*)AfxGetMainWnd())->GetDlgItem(AFX_IDW_STATUS_BAR));
			/*♠*/ CProgressCtrl ProgressCtrl;
			/*♠*/ CRect rect;
			/*♠*/ pStatusBar->GetItemRect(0,rect);
			/*♠*/ rect.left = rect.Width()/2;
			/*♠*/ ProgressCtrl.Create(WS_CHILD|WS_VISIBLE,rect,pStatusBar,777);
			/*♠*/ ((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText(strChName+ " data 저장중..."); //000647
			
			BOOL bSelected = FALSE;
			POSITION pos = data.GetFirstTablePosition();
			int nTotSize =  data.GetTableSize();

			//for(int i =0; i<data.GetTableSize(); i++)
			int cnt = 0;
			int nMaxIndex = -1;

			ProgressCtrl.SetRange(0, nTotSize);

			while(pos)
			{
				ProgressCtrl.SetPos(cnt);

				CTable *table = data.GetNextTable(pos);
				//이전 step까지 용량 합산
				if(cnt > 0)
				{
					wType = (WORD)data.GetLastDataOfTable(cnt-1, RS_COL_TYPE);
					fTotTime = data.GetLastDataOfTable(cnt-1, RS_COL_TOT_TIME);			
					fDataCap = data.GetLastDataOfTable(cnt-1, RS_COL_CAPACITY);
					if(wType == PS_STEP_CHARGE)
					{
						fCapacitySum = fCapacitySum + fDataCap;
					}
					else if(wType == PS_STEP_DISCHARGE || wType == PS_STEP_IMPEDANCE)
					{
						fCapacitySum = fCapacitySum - fDataCap;
					}
				}

				//선택된 Step만 
				if(bSelStep)
				{
					bSelected = FALSE;
					//Selection is Skiped
					int iSel = -1;
					//Step 선택 여부 검사 
					for(int findindex = 0; findindex < awRows.GetSize(); findindex++)
					{
//						iSel = data.GetTableIndex(atol(m_wndStepGrid.GetItemText(awRows[findindex], 0)));
						iSel = m_wndStepGrid.GetItemData(awRows[findindex], 0);
						if(iSel > nMaxIndex)	nMaxIndex = iSel;
						if(cnt == iSel)
						{
							bSelected = TRUE;
							break;
						}
					}

					if(nMaxIndex < cnt )
					{
						break;
					}

					//현재 table이 선택 List에 존재 하지 않음
					if(bSelected == FALSE)	
					{
						cnt++;
						continue;
					}

					//선택한 table의 최대 Index보다 크면 더이상 해당하는 table이 없음 
				}

				fStepTime = 0.0f;
				nStepNo = table->GetStepNo();
				wType = (WORD)table->GetType();
				nTotCycle = table->GetCycleNo();
				nCurCycle = (int)table->GetLastData(RS_COL_CUR_CYCLE);

				//1. Save Step Start Data(1sec 이하 data 저장 )
				//////////////////////////////////////////////////////////////////////////
				strTemp1.Format("%s\\StepStart\\*_C%06d_S%02d.csv", strPath, nTotCycle, nStepNo);	
				CFileFind fFind;
				if(fFind.FindFile(strTemp1))
				{
					fFind.FindNextFile();
					strTemp1 = fFind.GetFilePath();
				}

				FILE *fpCSV = fopen(strTemp1, "rt");
				if(fpCSV)
				{
					char szBuff[64];
					if(fscanf(fpCSV, "%s,%s,%s,%s,%s", szBuff, szBuff, szBuff, szBuff, szBuff) > 0)	//Skip Header
					{
						while(fscanf(fpCSV, "%f,%f,%f,%f,%f", &fStepTime, &fDataVtg, &fDataCrt, &fDataCap, &fWattHour) > 0)
						{
							
							//Time
							if(fStepTime < 0.3f)
							{
#ifdef _DEBUG
								strTemp1.Format("0.0(%.1f)", fStepTime);
#else
								strTemp1.Format("0.0");
#endif
							}
							else
							{
								strTemp1.Format("%.1f", fStepTime);
							}

							if(wType == PS_STEP_CHARGE)
							{
								fChargeCapa = fDataCap; 
								fDischargeCapa = 0.0f;
								fSum = fCapacitySum + fChargeCapa;
							}
							else if(wType == PS_STEP_DISCHARGE || wType == PS_STEP_IMPEDANCE)
							{
								fChargeCapa = 0.0f; 
								fDischargeCapa = fDataCap;
								fSum = fCapacitySum - fDischargeCapa;
							}
							else
							{
								fChargeCapa = 0.0f; 
								fDischargeCapa = 0.0f;
								fSum = fCapacitySum;
							}

							//지정 순서대로 저장 실시 
							strWriteBuff.Empty();
							for(int n=0; n<PS_MAX_ITEM_NUM; n++)
							{
								BOOL bSave = HIWORD(dwItem[n]);
								WORD item = LOWORD(dwItem[n]);
								if(bSave)
								{
									switch(item)
									{
									case	PS_STATE			:	strTemp2 = "작업중,";	break;
									case	PS_VOLTAGE			:	strTemp2.Format("%s,", ValueString(fDataVtg, PS_VOLTAGE));	break;
									case	PS_CURRENT			:	strTemp2.Format("%s,", ValueString(fDataCrt, PS_CURRENT));	break;
									case	PS_CAPACITY			:	strTemp2.Format("%s,", ValueString(fDataCap, PS_CAPACITY));	break;
									case	PS_IMPEDANCE		:	strTemp2 = ",";			break;
									case	PS_CODE				:	strTemp2 = "정상,";		break;
									case	PS_STEP_TIME		:	strTemp2.Format("%s,", strTemp1);		break;
									case	PS_TOT_TIME			:	strTemp2.Format("%s,", ValueString(fTotTime+fStepTime, PS_TOT_TIME));	break;
									case	PS_GRADE_CODE		:	strTemp2 = "," ;		break;
									case	PS_STEP_NO			:	strTemp2.Format("%d,", nStepNo);			break;
									case	PS_WATT				:	strTemp2.Format("%s,", ValueString(fDataVtg*fDataCrt/1000.0f, PS_WATT));	break;
									case	PS_WATT_HOUR		:	strTemp2.Format("%s,", ValueString(fWattHour, PS_WATT_HOUR));		break;
									case	PS_TEMPERATURE		:	strTemp2 = ",";			break;
									case	PS_PRESSURE			:	strTemp2 = ",";			break;
									case	PS_STEP_TYPE		:	strTemp2.Format("%s,", ::PSGetTypeMsg(wType));			break;
									case	PS_CUR_CYCLE		:	strTemp2.Format("%d,", nCurCycle);		break;
									case	PS_TOT_CYCLE		:	strTemp2.Format("%d,", nTotCycle);		break;
									case	PS_TEST_NAME		:	strTemp2.Format("%s,", strTestName);	break;
									case	PS_SCHEDULE_NAME	:	strTemp2.Format("%s,", pSchData->GetScheduleName());	break;
									case	PS_CHANNEL_NO		:	strTemp2.Format("%s,", strPath.Mid(strPath.ReverseFind('\\')+1));	break;
									case	PS_MODULE_NO		:	strTemp2.Format("%s,", strPath.Mid(strPath.ReverseFind('\\')+1));		break;
									case	PS_LOT_NO			:	strTemp2.Format("%s,", data.GetTestSerial());		break;
									case	PS_DATA_SEQ			:	strTemp2.Format("%d,", nDataSequence++);			break;
									case	PS_AVG_CURRENT		:	strTemp2 = ",";			break;
									case	PS_AVG_VOLTAGE		:	strTemp2 = ",";			break;
									case	PS_CAPACITY_SUM		:	strTemp2.Format("%s,", ValueString(fSum, PS_CAPACITY));			break;  
									case	PS_CHARGE_CAP		:	strTemp2.Format("%s,", ValueString(fChargeCapa, PS_CAPACITY));	break;  
									case	PS_DISCHARGE_CAP	:	strTemp2.Format("%s,", ValueString(fDischargeCapa, PS_CAPACITY));	break;
									case	PS_METER_DATA		:	strTemp2 = ",";			break;
									case	PS_START_TIME		:	strTemp2 = ",";			break;
									case	PS_END_TIME			:	strTemp2 = ",";			break;
									default:							strTemp2 = ",";			break;
									}
									strWriteBuff += strTemp2;
								}
							}
							fprintf(fp, "%s\n", strWriteBuff);
							nRowCount++;
							//최대 Excel row수 초과시 다음 파일 Open
							if(nRowCount >= MAX_EXCEL_ROW)
							{
								if(MakeNextFile(fp, nFileCount, strCSVFile) == FALSE)
								{
									strTemp1.Format(TEXT_LANG[42], strCSVFile);
									AfxMessageBox(strTemp1, MB_ICONSTOP|MB_OK);
									return FALSE;
								}
								
								ASSERT(fp);
								fprintf(fp, "%s\n", strHeaderString);//column header 기록
								nRowCount = 1;		//Header 기록 
							}
						}
					}
					fclose(fpCSV);
				}
				//////////////////////////////////////////////////////////////////////////

				//2. Save Saved data
				//////////////////////////////////////////////////////////////////////////
				//raw data save 
				long lMaxDataCount = 0;
				long nTimeDataCnt = 0, nVtgDataCnt = 0, nCrtDataCnt = 0, nCapDataCnt = 0;
				long nWattHDataCnt = 0, nTempDataCnt = 0, nAvgCrtCnt = 0, nAvgVtgCnt = 0, nMeterCnt;
				pfTimeData = data.GetDataOfTable(cnt, nTimeDataCnt, "Time", "Time");			if(nTimeDataCnt > lMaxDataCount)	lMaxDataCount = nTimeDataCnt;
				pfVtgData = data.GetDataOfTable(cnt, nVtgDataCnt, "Time", RS_COL_VOLTAGE);			if(nVtgDataCnt > lMaxDataCount)		lMaxDataCount = nVtgDataCnt;
				pfCrtData = data.GetDataOfTable(cnt, nCrtDataCnt, "Time", RS_COL_CURRENT);			if(nCrtDataCnt > lMaxDataCount)		lMaxDataCount = nCrtDataCnt;
				pfCapData = data.GetDataOfTable(cnt, nCapDataCnt, "Time", RS_COL_CAPACITY);		if(nCapDataCnt > lMaxDataCount)		lMaxDataCount = nCapDataCnt;
				pfWattHour = data.GetDataOfTable(cnt, nWattHDataCnt, "Time", RS_COL_WATT_HOUR);		if(nWattHDataCnt > lMaxDataCount)	lMaxDataCount = nWattHDataCnt;
				pfTemp = data.GetDataOfTable(cnt, nTempDataCnt, "Time", RS_COL_TEMPERATURE);		if(nTempDataCnt > lMaxDataCount)	lMaxDataCount = nTempDataCnt;
				pfAvgVtg = data.GetDataOfTable(cnt, nAvgVtgCnt, "Time", RS_COL_AVG_VTG);	if(nAvgVtgCnt > lMaxDataCount)		lMaxDataCount = nAvgVtgCnt;
				pfAvgCrt = data.GetDataOfTable(cnt, nAvgCrtCnt, "Time", RS_COL_AVG_CRT);	if(nAvgCrtCnt > lMaxDataCount)		lMaxDataCount = nAvgCrtCnt;
				pfMeter = data.GetDataOfTable(cnt, nMeterCnt, "Time", "Meter1");				
				for(int index = 0; index<lMaxDataCount-1; index++)	//최종 종료값 1개는 빼고 저장한다.//밑에서 최종 종료값은 별도로 저장한다.
				{
					//Step start data와 중복된 data이므로 생략 
					if(pfTimeData[index].y < fStepTime  )	continue;

					//Time
					if(pfTimeData[index].y < 0.3f)
					{
#ifdef _DEBUG
						strTemp1.Format("0.0(%.1f)", pfTimeData[index].y);
#else
						strTemp1 = "0.0";
#endif
					}
					else
					{
						strTemp1.Format("%.1f", pfTimeData[index].y);
					}
					
					fDataCap = pfCapData[index].y;
					if(wType == PS_STEP_CHARGE)
					{
						fChargeCapa = fDataCap; 
						fDischargeCapa = 0.0f;
						fSum = fCapacitySum + fChargeCapa;
					}
					else if(wType == PS_STEP_DISCHARGE || wType == PS_STEP_IMPEDANCE)
					{
						fChargeCapa = 0.0f; 
						fDischargeCapa = fDataCap;
						fSum = fCapacitySum - fDischargeCapa;
					}
					else
					{
						fChargeCapa = 0.0f; 
						fDischargeCapa = 0.0f;
						fSum = fCapacitySum;
					}

					fDataVtg = pfVtgData[index].y;							//Voltage
					fDataCrt = pfCrtData[index].y;							//Current
					fDataCap = pfCapData[index].y;							//Capa
					fWattHour = pfWattHour[index].y;						//WattHour

					//Average Voltage
					if(pfAvgVtg)
					{
						fAvgVtg = pfAvgVtg[index].y;
					}
					else
					{
						fAvgVtg = 0.0f;
					}
					
					//Average Current
					if(pfAvgCrt)
					{
						fAvgCrt = pfAvgCrt[index].y;
					}
					else
					{
						fAvgCrt = 0.0f;
					}
					
					//Temperature
					if(pfTemp)
					{
						fTemper = pfTemp[index].y;
					}
					else
					{
						fTemper = 0.0f;
					}

					if(pfMeter)
					{
						fMeter = pfMeter[index].y;
					}
					else
					{
						fMeter = 0.0f;
					}

					//지정 순서대로 저장 실시 
					strWriteBuff.Empty();
					for(int n=0; n<PS_MAX_ITEM_NUM; n++)
					{
						BOOL bSave = HIWORD(dwItem[n]);
						WORD item = LOWORD(dwItem[n]);
						if(bSave)
						{
							switch(item)
							{
							case	PS_STATE			:	strTemp2 = "작업중,";	break;
							case	PS_VOLTAGE			:	strTemp2.Format("%s,", ValueString(fDataVtg, PS_VOLTAGE));	break;
							case	PS_CURRENT			:	strTemp2.Format("%s,", ValueString(fDataCrt, PS_CURRENT));	break;
							case	PS_CAPACITY			:	strTemp2.Format("%s,", ValueString(fDataCap, PS_CAPACITY));	break;
							case	PS_IMPEDANCE		:	strTemp2 = ",";			break;
							case	PS_CODE				:	strTemp2 = "정상,";		break;
							case	PS_STEP_TIME		:	strTemp2.Format("%s,", strTemp1);		break;
							case	PS_TOT_TIME			:	strTemp2.Format("%s,", ValueString(fTotTime+pfTimeData[index].y, PS_TOT_TIME));	break;
							case	PS_GRADE_CODE		:	strTemp2 = "," ;		break;
							case	PS_STEP_NO			:	strTemp2.Format("%d,", nStepNo);		break;
							case	PS_WATT				:	strTemp2.Format("%s,", ValueString(fDataVtg*fDataCrt/1000.0f, PS_WATT));	break;
							case	PS_WATT_HOUR		:	strTemp2.Format("%s,", ValueString(fWattHour, PS_WATT_HOUR));		break;
							case	PS_TEMPERATURE		:	strTemp2.Format("%s,", ValueString(fTemper, PS_TEMPERATURE));		break;
							case	PS_PRESSURE			:	strTemp2 = ",";			break;
							case	PS_STEP_TYPE		:	strTemp2.Format("%s,", ::PSGetTypeMsg(wType));			break;
							case	PS_CUR_CYCLE		:	strTemp2.Format("%d,", nCurCycle);		break;
							case	PS_TOT_CYCLE		:	strTemp2.Format("%d,", nTotCycle);		break;
							case	PS_TEST_NAME		:	strTemp2.Format("%s,", strTestName);	break;
							case	PS_SCHEDULE_NAME	:	strTemp2.Format("%s,", pSchData->GetScheduleName());	break;
							case	PS_CHANNEL_NO		:	strTemp2.Format("%s,", strPath.Mid(strPath.ReverseFind('\\')+1));	break;
							case	PS_MODULE_NO		:	strTemp2.Format("%s,", strPath.Mid(strPath.ReverseFind('\\')+1));		break;
							case	PS_LOT_NO			:	strTemp2.Format("%s,", data.GetTestSerial());		break;
							case	PS_DATA_SEQ		:	strTemp2.Format("%d,", nDataSequence++);			break;
							case	PS_AVG_CURRENT		:	strTemp2.Format("%s,", ValueString(fAvgCrt, PS_CURRENT));			break;
							case	PS_AVG_VOLTAGE		:	strTemp2.Format("%s,", ValueString(fAvgVtg, PS_VOLTAGE));			break;
							case	PS_CAPACITY_SUM		:	strTemp2.Format("%s,", ValueString(fSum, PS_CAPACITY));				break;  
							case	PS_CHARGE_CAP		:	strTemp2.Format("%s,", ValueString(fChargeCapa, PS_CAPACITY));		break;  
							case	PS_DISCHARGE_CAP	:	strTemp2.Format("%s,", ValueString(fDischargeCapa, PS_CAPACITY));	break;
							case	PS_METER_DATA		:	strTemp2.Format("%s,", ValueString(fMeter, PS_METER_DATA));			break;
							case	PS_START_TIME		:	strTemp2 = ",";			break;
							case	PS_END_TIME			:	strTemp2 = ",";			break;
							default:							strTemp2 = ",";			break;
							}
							strWriteBuff += strTemp2;
						}
					}
					fprintf(fp, "%s\n", strWriteBuff);
					nRowCount++;
					//최대 Excel row수 초과시 다음 파일 Open
					if(nRowCount >= MAX_EXCEL_ROW)
					{
						if(MakeNextFile(fp, nFileCount, strCSVFile) == FALSE)
						{
							strTemp1.Format("%s 파일을 생성할 수 없습니다.", strCSVFile);
							AfxMessageBox(strTemp1, MB_ICONSTOP|MB_OK);

							//release memory
							if(pfTimeData != NULL)	delete [] pfTimeData;
							if(pfVtgData != NULL)	delete [] pfVtgData;
							if(pfCrtData != NULL)	delete [] pfCrtData;
							if(pfCapData != NULL)	delete [] pfCapData;
							if(pfWattHour != NULL)	delete [] pfWattHour;
							if(pfTemp != NULL)		delete [] pfTemp;
							if(pfAvgVtg != NULL)	delete [] pfAvgVtg;
							if(pfAvgCrt != NULL)	delete [] pfAvgCrt;
							if(pfMeter != NULL)		delete [] pfMeter;
													
							return FALSE;
						}
								
						ASSERT(fp);
						fprintf(fp, "%s\n", strHeaderString);//column header 기록
						nRowCount = 1;		//Header 기록 
					}
				}
				
				//release memory
				if(pfTimeData != NULL)	delete [] pfTimeData;
				if(pfVtgData != NULL)	delete [] pfVtgData;
				if(pfCrtData != NULL)	delete [] pfCrtData;
				if(pfCapData != NULL)	delete [] pfCapData;
				if(pfWattHour != NULL)	delete [] pfWattHour;
				if(pfTemp != NULL)		delete [] pfTemp;
				if(pfAvgVtg != NULL)	delete [] pfAvgVtg;
				if(pfAvgCrt != NULL)	delete [] pfAvgCrt;
				if(pfMeter != NULL)		delete [] pfMeter;

				//3. Save step End data
				//////////////////////////////////////////////////////////////////////////

				//Code
				::PSCellCodeMsg((BYTE)table->GetLastData(RS_COL_CODE), strCode, strTemp2);	//Pscommon.dll API

				//StartTime
				strStartTime = timeStepStart.Format("%m/%d %H:%M:%S");

				//EndTime
				fTotTime = table->GetLastData(RS_COL_TOT_TIME);
				lDay = (long)fTotTime/86400;		lHour = (long)fTotTime%86400;
				lMinute = lHour % 3600;				
				lHour = lHour / 3600;				
				lSecond = lMinute % 60;
				lMinute = lMinute / 60;
				timeRun.SetDateTimeSpan(lDay, lHour, lMinute, lSecond);
				timeStepStart = timeStart + timeRun;
				strEndTime = timeStepStart.Format("%m/%d %H:%M:%S");
					
				//Time
				fStepTime = table->GetLastData("Time");
					
				//Voltage
				fDataVtg = table->GetLastData(RS_COL_VOLTAGE);
					
				//Current
				fDataCrt = table->GetLastData(RS_COL_CURRENT);
					
				//Capacity
				fDataCap = table->GetLastData(RS_COL_CAPACITY);

				//용량을 총용량, 충전용량, 방전용량으로 구별하여 저장 
				if(wType == PS_STEP_CHARGE)
				{
					fSum= fCapacitySum + fDataCap;
					fChargeCapa= fDataCap;
					fDischargeCapa = 0.0f;
				}
				else if(wType == PS_STEP_DISCHARGE || wType == PS_STEP_IMPEDANCE)
				{
					fSum = fCapacitySum - fDataCap;
					fChargeCapa= 0.0f;
					fDischargeCapa = fDataCap;
				}
				else 
				{
					fSum = fCapacitySum;
					fChargeCapa= 0.0f;
					fDischargeCapa = 0.0f;
				}
				
				//WattHour
				fWattHour = table->GetLastData(RS_COL_WATT_HOUR);
			
				//Average Voltage
				fAvgVtg = table->GetLastData(RS_COL_AVG_VTG);
				
				//Average Current
				fAvgCrt = table->GetLastData(RS_COL_AVG_CRT);

				//IR
				fImp = table->GetLastData(RS_COL_IR);
				if(wType == PS_STEP_IMPEDANCE && bUserImpTime)
				{
					CString strA, strB;
					//Edited by KBH 2007/2/26 (사용자가 지정한 Time으로 Impdeance를 재계산 한다.)
					GetDlgItem(IDC_TEST_NAME_STATIC)->GetWindowText(strA);
					strB.Format("%s\\StepStart\\*_C%06d_S%02d.csv", data.GetPath(),  
									table->GetCycleNo(),
									nStepNo
									);
					//파일이 많으면 오래 걸림
					CFileFind fFind;
					if(fFind.FindFile(strB))
					{
						fFind.FindNextFile();
						strB = fFind.GetFilePath();
						float fImpData = ((CDataDoc *)GetDocument())->CalImpedance(strB , fImpTime1, fImpTime2);
						if(fImpData >= 0.0f)
						{
							fImp = fImpData;
						}
					}
				}

				//Temperature
				fTemper = table->GetLastData(RS_COL_TEMPERATURE);
				
				//Grading
				bGrade = (BYTE)table->GetLastData( RS_COL_GRADE);

				//Meter
				fMeter = table->GetLastData("Meter1");
				
				strWriteBuff.Empty();
				for(int n=0; n<PS_MAX_ITEM_NUM; n++)
				{
					BOOL bSave = HIWORD(dwItem[n]);
					WORD item = LOWORD(dwItem[n]);
					if(bSave)
					{
						switch(item)
						{
						case	PS_STATE			:	strTemp2.Format("%s,", ValueString(table->GetLastData(RS_COL_STATE), PS_STATE));	break;
						case	PS_VOLTAGE			:	strTemp2.Format("%s,", ValueString(fDataVtg, PS_VOLTAGE));	break;
						case	PS_CURRENT			:	strTemp2.Format("%s,", ValueString(fDataCrt, PS_CURRENT));	break;
						case	PS_CAPACITY			:	strTemp2.Format("%s,", ValueString(fDataCap, PS_CAPACITY));	break;
						case	PS_IMPEDANCE		:	strTemp2.Format("%s,", ValueString(fImp, PS_IMPEDANCE));	break;
						case	PS_CODE				:	strTemp2.Format("%s,", strCode);		break;
						case	PS_STEP_TIME		:	strTemp2.Format("%.1f,", fStepTime);	break;
						case	PS_TOT_TIME			:	strTemp2.Format("%s,", ValueString(fTotTime, PS_TOT_TIME));	break;
						case	PS_GRADE_CODE		:	strTemp2.Format("%s,", ValueString(bGrade, PS_GRADE_CODE));	break;
						case	PS_STEP_NO			:	strTemp2.Format("%d,", nStepNo);		break;
						case	PS_WATT				:	strTemp2.Format("%s,", ValueString(fDataVtg*fDataCrt/1000.0f, PS_WATT));	break;
						case	PS_WATT_HOUR		:	strTemp2.Format("%s,", ValueString(fWattHour, PS_WATT_HOUR));		break;
						case	PS_TEMPERATURE		:	strTemp2.Format("%s,", ValueString(fTemper, PS_TEMPERATURE));		break;
						case	PS_PRESSURE			:	strTemp2 = ",";			break;
						case	PS_STEP_TYPE		:	strTemp2.Format("%s,", ::PSGetTypeMsg(wType));			break;
						case	PS_CUR_CYCLE		:	strTemp2.Format("%d,", nCurCycle);		break;
						case	PS_TOT_CYCLE		:	strTemp2.Format("%d,", nTotCycle);		break;
						case	PS_TEST_NAME		:	strTemp2.Format("%s,", strTestName);	break;
						case	PS_SCHEDULE_NAME	:	strTemp2.Format("%s,", pSchData->GetScheduleName());	break;
						case	PS_CHANNEL_NO		:	strTemp2.Format("%s,", strPath.Mid(strPath.ReverseFind('\\')+1));	break;
						case	PS_MODULE_NO		:	strTemp2.Format("%s,", strPath.Mid(strPath.ReverseFind('\\')+1));	break;
						case	PS_LOT_NO			:	strTemp2.Format("%s,", data.GetTestSerial());		break;
						case	PS_DATA_SEQ			:	strTemp2.Format("%d,", nDataSequence++);			break;
						case	PS_AVG_CURRENT		:	strTemp2.Format("%s,", ValueString(fAvgCrt, PS_CURRENT));			break;
						case	PS_AVG_VOLTAGE		:	strTemp2.Format("%s,", ValueString(fAvgVtg, PS_VOLTAGE));			break;
						case	PS_CAPACITY_SUM		:	strTemp2.Format("%s,", ValueString(fSum, PS_CAPACITY));				break;  
						case	PS_CHARGE_CAP		:	strTemp2.Format("%s,", ValueString(fChargeCapa, PS_CAPACITY));		break;  
						case	PS_DISCHARGE_CAP	:	strTemp2.Format("%s,", ValueString(fDischargeCapa, PS_CAPACITY));	break;
						case	PS_METER_DATA		:	strTemp2.Format("%s,", ValueString(fMeter, PS_METER_DATA));			break;
						case	PS_START_TIME		:	strTemp2.Format("%s,", strStartTime);		break;
						case	PS_END_TIME			:	strTemp2.Format("%s,", strEndTime);			break;
						default:							strTemp2 = ",";								break;
						}
						strWriteBuff += strTemp2;
					}
				}

				fprintf(fp, "%s\n", strWriteBuff);
				nRowCount++;
				
				//최대 Excel row수 초과시 다음 파일 Open
				if(nRowCount >= MAX_EXCEL_ROW)
				{
					if(MakeNextFile(fp, nFileCount, strCSVFile) == FALSE)
					{
						strTemp1.Format(TEXT_LANG[42], strCSVFile); //000648
						AfxMessageBox(strTemp1, MB_ICONSTOP|MB_OK);
						return FALSE;
					}
								
					ASSERT(fp);
					fprintf(fp, "%s\n", strHeaderString);//column header 기록
					nRowCount = 1;		//Header 기록 
				}
				cnt++;
			}	//for(int i =0; i<data.GetTableSize(); i++)

			/*♠*/ ((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText("");

		}//	if(data.Create() == TRUE)				
	}// while (bWorking1)

	if(fp != NULL)	fclose(fp);

	//Excel로 Open 여부를 묻는다.
	if(nFileCount > 1)
	{
		strTemp1.Format(TEXT_LANG[43], nFileCount, strCSVFile); //000649
	}
	else
	{
		strTemp1.Format(TEXT_LANG[44], strCSVFile); //000650
	}
	if(MessageBox(strTemp1, "File Open", MB_ICONQUESTION|MB_YESNO) != IDNO)
	{
		((CCTSGraphAnalApp *)AfxGetApp())->ExecuteExcel(strCSVFile);
	}	
	return TRUE;
}


BOOL CDataView::MakeNextFile(FILE *fp,  int &nFileCount, CString strFile)
{
	ASSERT(fp);

	fclose(fp);
	fp = NULL;
	
	//파일 이름에 (1)을 붙인다.
	//1) 파일 이름 추출 

	CString strTemp;
	int n = strFile.ReverseFind('\\');
	CString strFileName = strFile.Mid(n+1);
	CString strPathName = strFile.Left(n+1);
	
	int m = strFileName.ReverseFind('.');
	if(m<0)
	{
		strTemp.Format("%s(%d)", strFileName, nFileCount);
	}
	else
	{
		strTemp.Format("%s(%d)%s", strFileName.Left(m), nFileCount, strFileName.Mid(m));
	}
	
	CString str;
	str.Format("%s%s", strPathName,strTemp);
	
	fp = fopen(str, "wt");

	if(fp == NULL)	return FALSE;

	nFileCount++;
	
	return TRUE;
}

/*
void CDataView::InitStepEndGrid()
{
	m_wndStepEndGrid.SubclassDlgItem(IDC_STEP_END_GRID, this);
	m_wndStepEndGrid.Initialize();

	m_wndStepEndGrid.SetRowCount(1); 
    m_wndStepEndGrid.SetColCount(9); 

}
*/

/*
void CDataView::UpdateStepDataA(CString strChPath)
{
	CChData *pData = ((CDataDoc *)GetDocument())->GetChData();
	if(pData->IsLoadedData() == FALSE)	return;

	strTemp = GetUnitString(PS_STEP_TIME);
	if(strTemp.IsEmpty())
	{
		strTemp = "Time";
	}
	else
	{
		strTemp.Format("Time(%s)", GetUnitString(PS_VOLTAGE));
	}
	m_wndStepEndGrid.SetValueRange(CGXRange(0, 4), strTemp);			//8, Time,		//9. TotTime,
	
	strTemp.Format("Volt(%s)", GetUnitString(PS_VOLTAGE));
	m_wndStepEndGrid.SetValueRange(CGXRange(0, 5), strTemp);			//10. Voltage,

	strTemp.Format("Crt(%s)", GetUnitString(PS_CURRENT));
	m_wndStepEndGrid.SetValueRange(CGXRange(0, 6), strTemp);			//11. Current
		
	strTemp.Format("Capa(%s)", GetUnitString(PS_CAPACITY));
	m_wndStepEndGrid.SetValueRange(CGXRange(0, 7), strTemp);			//11. Current
		
	strTemp.Format("WattHour(%s)", GetUnitString(PS_WATT_HOUR));
	m_wndStepEndGrid.SetValueRange(CGXRange(0, 8), strTemp);		//13. WattHour,		

	
	//Schedule Information display
	CScheduleData *pSchData = pData->GetPattern();
	BOOL bShowImpColumn = TRUE;
	BOOL bShowGradeColumn = FALSE;
	if(pSchData)
	{
		//Impedance와 Grading은 해당 step이 존재할 경우만 Column을 표시한다.
		if(!pSchData->IsThereImpStep())
		{
			bShowImpColumn = FALSE;
		}
		if(!pSchData->IsThereGradingStep())
		{
			bShowGradeColumn = FALSE;
		}
	}

	//////////////////////////////////////////////////////////////////////////
	//remake grid
//	m_wndStepGrid.DeleteAllItems();
//	m_wndStepGrid.SetRowCount(1); 
//  m_wndStepGrid.SetColumnCount(9); 

	//Header 부분을 각각 1칸씩 사용한다.
//	m_wndStepGrid.SetFixedRowCount(1); 
//    m_wndStepGrid.SetFixedColumnCount(1); 

	m_wndStepEndGrid.SetValueRange(CGXRange(0, 0), "Step");				//1. No, 2.IndexFrom, 3.IndexTo,										
	m_wndStepEndGrid.SetValueRange(CGXRange(0, 1), "Cur/Tot Cyc");				//4. CurCycle,//5. TotalCycle,
	m_wndStepEndGrid.SetValueRange(CGXRange(0, 2), "Type");				//6. Type,
	m_wndStepEndGrid.SetValueRange(CGXRange(0, 3), "상태");				//8. Code, 7, State,

	strTemp = GetUnitString(PS_STEP_TIME);
	if(strTemp.IsEmpty())
	{
		strTemp = "Time";
	}
	else
	{
		strTemp.Format("Time(%s)", GetUnitString(PS_STEP_TIME));
	}
	m_wndStepEndGrid.SetValueRange(CGXRange(0, 4), strTemp);			//8, Time,		//9. TotTime,
	
	strTemp.Format("전압(%s)", GetUnitString(PS_VOLTAGE));
	m_wndStepEndGrid.SetValueRange(CGXRange(0, 5), strTemp);			//10. Voltage,

	strTemp.Format("전류(%s)", GetUnitString(PS_CURRENT));
	m_wndStepEndGrid.SetValueRange(CGXRange(0, 6), strTemp);			//11. Current
		
	strTemp.Format("용량(%s)", GetUnitString(PS_CAPACITY));
	m_wndStepEndGrid.SetValueRange(CGXRange(0, 7), strTemp);			//11. Capacity
		
	strTemp.Format("WattHour(%s)", GetUnitString(PS_WATT_HOUR));
	m_wndStepEndGrid.SetValueRange(CGXRange(0, 8), strTemp);		//13. WattHour,	


	if(bShowImpColumn)
	{
		#ifdef _EDLC_TEST_SYSTEM
			strTemp = "ESR(mOhm)";			//14. IR,										
		#else
			strTemp = "Imp(mOhm)";			//14. IR,										
		#endif
//		m_wndStepEndGrid.InsertCols(m_wndStepEndGrid.GetColCount()+1, 1);
//			.InsertColumn(strTemp);
	}

	if(bShowGradeColumn)
	{
//		m_wndStepEndGrid.InsertColumn("등급");
	}
//	m_wndStepGrid.ExpandColumnsToFit();
//////////////////////////////////////////////////////////////////////////

//	int nDataCount = pData->m_TableList.GetCount();
	int nDisplayStep = m_StepSelCombo.GetCurSel();
	
//	m_wndStepGrid.SetRowCount(nDataCount+1);

	float fData, fDataVtg, fDataCrt, fCapacitySum = 0.0f;
	long nStepNo;
	WORD type;
	long totCycle;
	COLORREF textColor, bkColor;
	
	int nRow = 0;
	int nColumn = 0;

	CProgressWnd *pProgressWnd = new CProgressWnd;
	pProgressWnd->Create(this, "Loading...", TRUE);
	pProgressWnd->SetColor(RGB(100,100,100));
	pProgressWnd->Clear();
	pProgressWnd->SetRange(0, 100);
	pProgressWnd->SetText("Data Loading...");
	pProgressWnd->SetPos(0);
	pProgressWnd->Show();
	
//	m_wndStepGrid.DeleteNonFixedRows();

	int nTableSize = pData->m_TableList.GetCount();

	strTemp.Format("Step(%d)", nTableSize);
	m_wndStepEndGrid.SetValueRange(CGXRange(0, 0), strTemp);
	
	//Table size가 너무 길어 쪼개서 출력 

	for(int i=0; i<nTableSize; i++)
	{

		type = (WORD)pData->GetLastDataOfTable(i, RS_COL_TYPE);
		nStepNo = pData->GetTableNo(i);
		totCycle = (ULONG)pData->GetLastDataOfTable(i, RS_COL_TOT_CYCLE);
		
		strTemp.Format("Table %d(Step %d) data loading...", i+1, nStepNo);
		pProgressWnd->SetText(strTemp);
		pProgressWnd->SetPos(int((float)i/(float)nTableSize*100.0f));
//		pProgressWnd->PeekAndPump();

		if(nDisplayStep == 1)	//충전 
		{
			if(type != PS_STEP_CHARGE)					continue;
		}
		else if(nDisplayStep == 2 )	//방전
		{
			if(type != PS_STEP_DISCHARGE)				continue;
		}
		else if(nDisplayStep == 3 )	//충전&방전
		{
			if(type != PS_STEP_CHARGE && type != PS_STEP_DISCHARGE)	continue;
		}
		else if(nDisplayStep == 4 )	//Rest
		{
			if(type != PS_STEP_REST)					continue;
		}
		else if(nDisplayStep == 5 )	//Impedance
		{
			if(type != PS_STEP_IMPEDANCE)				continue;
		}
		else if(nDisplayStep == 6)	//Step Range
		{
			if(m_RangeList.Find(nStepNo)==NULL)			continue;
		}
		else if(nDisplayStep == 7)	//Cycle Range
		{
			if(m_RangeList.Find(totCycle)==NULL)		continue;
		}
		
		//No
		nColumn = 0;
#ifdef _DEBUG
		strName.Format("%d(%d)", nStepNo, (long)pData->GetLastDataOfTable(i, RS_COL_SEQ));
#else
		strName.Format("%d", nStepNo);
#endif
		nRow++;
		m_wndStepEndGrid.InsertRows(nRow, 1);
		m_wndStepEndGrid.SetValueRange(CGXRange(nRow, 0), strName);
//		m_wndStepGrid.SetItemData(nRow, 0, i);

//		m_wndStepGrid.SetItemText(i+1, 0, strName);
//		m_wndStepGrid.SetItemFormat(nRow, nColumn, DT_CENTER|DT_VCENTER|DT_SINGLELINE );

		//cycle//CurCycle,TotalCycle
		nColumn++;
		strName.Format("%d/%d", (ULONG)pData->GetLastDataOfTable(i, RS_COL_CUR_CYCLE), totCycle);	//PScommon.dll
			m_wndStepEndGrid.SetValueRange(CGXRange(nRow, nColumn), strName);
//		m_wndStepGrid.SetItemFormat(nRow, nColumn, DT_CENTER|DT_VCENTER|DT_SINGLELINE );

		//Type
		nColumn++;
		strName.Format("%s", ::PSGetTypeMsg(type));	//PScommon.dll
//		m_StepListCtrl.SetItemText(lvItem.iItem, 1, strName);
			m_wndStepEndGrid.SetValueRange(CGXRange(nRow, nColumn), strName);
//		m_wndStepGrid.SetItemFormat(nRow, nColumn, DT_RIGHT|DT_VCENTER|DT_SINGLELINE );
		::PSGetTypeColor(type, textColor, bkColor);		//pscommon.dll
//		m_wndStepGrid.SetItemBkColour(nRow, nColumn, bkColor);
//		m_wndStepGrid.SetItemFgColour(nRow, nColumn, textColor);
		
		//Code
		nColumn++;
		::PSCellCodeMsg((BYTE)pData->GetLastDataOfTable(i, RS_COL_CODE), strName, strTemp);	//Pscommon.dll API
		if(type == PS_STEP_END)		strName.Empty();
			m_wndStepEndGrid.SetValueRange(CGXRange(nRow, nColumn), strName);
//		m_wndStepGrid.SetItemFormat(nRow, nColumn, DT_RIGHT|DT_VCENTER|DT_SINGLELINE );

		//Time
		nColumn++;

		strName = ValueString(pData->GetLastDataOfTable(i, "Time"), PS_STEP_TIME);
		
		if(type == PS_STEP_END)
		{
			strName.Format("총 %s", ValueString(pData->GetLastDataOfTable(i, RS_COL_TOT_TIME), PS_STEP_TIME));
		}
			m_wndStepEndGrid.SetValueRange(CGXRange(nRow, nColumn), strName);
//		m_wndStepGrid.SetItemFormat(nRow, nColumn, DT_RIGHT|DT_VCENTER|DT_SINGLELINE );
		
		//Voltage
		nColumn++;
		fDataVtg = pData->GetLastDataOfTable(i, RS_COL_VOLTAGE);
//		strName.Format("%.4f", fDataVtg/1000.0f);
		strName = ValueString(pData->GetLastDataOfTable(i, RS_COL_VOLTAGE), PS_VOLTAGE);
			
		if(type == PS_STEP_END)		strName.Empty();
			m_wndStepEndGrid.SetValueRange(CGXRange(nRow, nColumn), strName);
//		m_wndStepGrid.SetItemFormat(nRow, nColumn, DT_RIGHT|DT_VCENTER|DT_SINGLELINE );

		//Current
		nColumn++;
		fDataCrt = pData->GetLastDataOfTable(i, RS_COL_CURRENT);
		if(type == PS_STEP_REST || type == PS_STEP_OCV)
		{
			fDataCrt = 0;
#ifdef _DEBUG
			strName.Format("%s(%.2f)", ValueString(fDataCrt, PS_CURRENT), fDataCrt);
#else
			strName.Format("%s", ValueString(fDataCrt, PS_CURRENT));
#endif
		}
		else if(type == PS_STEP_CHARGE || type== PS_STEP_DISCHARGE || type == PS_STEP_IMPEDANCE)
		{
			//strName.Format("%.2f", fDataCrt);
			strName = ValueString(fDataCrt, PS_CURRENT);
		}
		else //if(type == PS_STEP_END)
		{
			strName.Empty();
		}
			m_wndStepEndGrid.SetValueRange(CGXRange(nRow, nColumn), strName);
//		m_wndStepGrid.SetItemFormat(nRow, nColumn, DT_RIGHT|DT_VCENTER|DT_SINGLELINE );
		
		//Capacity
		nColumn++;
		fData = pData->GetLastDataOfTable(i, RS_COL_CAPACITY);
		if(type == PS_STEP_CHARGE)
		{
			fCapacitySum += fData;
		}
		else if(type == PS_STEP_DISCHARGE || type== PS_STEP_IMPEDANCE)
		{
			fCapacitySum -= fData;
		}

		if(type == PS_STEP_END)
		{
			strName.Format("총 %s", ValueString(fCapacitySum, PS_CAPACITY));
		}
		else if(type == PS_STEP_CHARGE || type== PS_STEP_DISCHARGE)
		{
//			strName.Format("%.1f", fData);
			strName = ValueString(fData, PS_CAPACITY);
		}
		else
		{
#ifdef _DEBUG
			strName.Format("0.0(%.1f)", fData);
#else
			strName = "0.0";
#endif
		}
		m_wndStepEndGrid.SetValueRange(CGXRange(nRow, nColumn), strName);
//		m_wndStepGrid.SetItemFormat(nRow, nColumn, DT_RIGHT|DT_VCENTER|DT_SINGLELINE );
		
		//Watt
//		nColumn++;
////	strName.Format("%.3f", fabs(fDataCrt*fDataVtg/1000.0f));
//		strName = ValueString(fabs(fDataCrt*fDataVtg/1000.0f), PS_WATT);
//		if(type == PS_STEP_END)		strName.Empty();
//		m_wndStepGrid.SetItemText(i+1, 7, strName);
//		m_wndStepGrid.SetItemFormat(i+1, 7, DT_RIGHT|DT_VCENTER|DT_SINGLELINE );
		
		//WattHour
		nColumn++;
		fData = pData->GetLastDataOfTable(i, RS_COL_WATT_HOUR);
		if(type == PS_STEP_END)
		{
			strName.Empty();
		}
		else if(type == PS_STEP_CHARGE || type== PS_STEP_DISCHARGE)
		{
			//strName.Format("%.1f", fData);
			strName = ValueString(fData, PS_WATT_HOUR);
		}
		else
		{
#ifdef _DEBUG
			strName.Format("0.0(%.1f)", fData);
#else
			strName = "0.0";
#endif
		}
			m_wndStepEndGrid.SetValueRange(CGXRange(nRow, nColumn), strName);
//		m_wndStepGrid.SetItemFormat(nRow, nColumn, DT_RIGHT|DT_VCENTER|DT_SINGLELINE );
	
		if(bShowImpColumn)
		{
			nColumn++;
			//IR
			fData = pData->GetLastDataOfTable(i, RS_COL_IR);
			if(type == PS_STEP_END)
			{
				strName.Empty();
			}
			else if(type == PS_STEP_IMPEDANCE || type == PS_STEP_DISCHARGE)
			{
				if(fData > 1000000 || fData < 0)		//이상 data시 표기 이상 처리 
				{
					strName = "0.0";
				}
				else
				{
					strName = ValueString(fData, PS_IMPEDANCE);
				}
			}
			else
			{
				strName = "0.0";
			}
#ifdef _DEBUG
			CString str;
			str.Format("(%s)", ValueString(fData, PS_IMPEDANCE));
				m_wndStepEndGrid.SetValueRange(CGXRange(nRow, nColumn), strName+str);
#else
				m_wndStepEndGrid.SetValueRange(CGXRange(nRow, nColumn), strName);
#endif
//			m_wndStepGrid.SetItemFormat(nRow, nColumn, DT_RIGHT|DT_VCENTER|DT_SINGLELINE );
		}
		
		if(bShowGradeColumn)
		{
			//Grading
			nColumn++;
			fData = pData->GetLastDataOfTable(i, RS_COL_GRADE);
			//strName.Format("%c", (BYTE)fData);
			strName = ValueString(fData, PS_GRADE_CODE);
			if(type == PS_STEP_END)		strName.Empty();
				m_wndStepEndGrid.SetValueRange(CGXRange(nRow, nColumn), strName);
//			m_wndStepGrid.SetItemFormat(nRow, nColumn, DT_RIGHT|DT_VCENTER|DT_SINGLELINE );
		}
	}

	pProgressWnd->SetPos(100);
	Sleep(100);
	pProgressWnd->Hide();
	delete pProgressWnd;

	m_wndStepGrid.Refresh();

}
*/

void CDataView::InitTooltip()
{
	m_tooltip.Create(this);
	m_tooltip.Activate(TRUE);

	m_tooltip.AddTool(GetDlgItem(IDC_COMBO1), "표시할 Type을 선택"); //000651		
	m_tooltip.AddTool(GetDlgItem(IDC_TEST_SEL_BUTTON), "작업 및 채널 목록 선택"); //000654
	m_tooltip.AddTool(GetDlgItem(IDC_EXCEL_DETAIL_BUTTON), "Step의 종료값을 Excel로 보기"); //000655
	// m_tooltip.AddTool(GetDlgItem(IDC_DETAIL_STEP_START_BUTTON), "Step진입 초기구간을 Excel로 표시");

	m_bInitedTooltip = TRUE;
}

BOOL CDataView::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CDataView"), _T("TEXT_CDataView_CNT"), _T("TEXT_CDataView_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt];

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CDataView_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CDataView"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Languge error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}