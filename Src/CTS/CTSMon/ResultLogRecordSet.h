#if !defined(AFX_RESULTLOGRECORDSET1_H__923651FA_1698_4564_9A16_3314C5AB1A9A__INCLUDED_)
#define AFX_RESULTLOGRECORDSET1_H__923651FA_1698_4564_9A16_3314C5AB1A9A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ResultLogRecordSet1.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CResultLogRecordSet DAO recordset

class CResultLogRecordSet : public CDaoRecordset
{
public:
	CResultLogRecordSet(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CResultLogRecordSet)

// Field/Param Data
	//{{AFX_FIELD(CResultLogRecordSet, CDaoRecordset)
	long	m_TestIndex;
	COleDateTime	m_DateTime;
	long	m_ProcedureID;
	CString	m_TestResultFileName;
	float	m_Average;
	float	m_MaxVal;
	long	m_MaxChIndex;
	float	m_MinVal;
	long	m_MinChIndex;
	float	m_STDD;
	long	m_NormalNo;
	float	m_ErrorRate;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CResultLogRecordSet)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RESULTLOGRECORDSET1_H__923651FA_1698_4564_9A16_3314C5AB1A9A__INCLUDED_)
