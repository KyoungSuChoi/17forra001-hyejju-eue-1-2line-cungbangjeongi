// SystemsettingDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "SystemsettingDlg.h"


// CSystemsettingDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSystemsettingDlg, CDialog)

CSystemsettingDlg::CSystemsettingDlg(CCTSMonDoc* pDoc, CWnd* pParent /*=NULL*/)
: CDialog(CSystemsettingDlg::IDD, pParent)
, m_nCapaErrlimit(0)
, m_nContactErrlimit(0)
, m_nStageLogSavePeriod(0)
, m_nFMSLogSavePeriod(0)
, m_nDelayBlueReady(0)
, m_nDelayBlueEnd(0)
, m_nDelayRedEnd(0)
, m_nResultFileSavePeriod(0)
, m_nPrecisionDataSavePeriod(0)
, m_nDelayRedTrayIn(0)
, m_nFMSPortNum(0)
, m_nMonPortNum(0)
, m_bUseMon(FALSE)
, m_bUsePrecisionData(FALSE)
, m_bUseFaultAlarm(FALSE)
, m_nErrorHistorySavePeriod(0)
, m_nChErrlimit(0)
, m_nImsRecordConditionTime(0)
{
	m_pDoc = pDoc;
	LanguageinitMonConfig();
}

CSystemsettingDlg::~CSystemsettingDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CSystemsettingDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CSystemsettingDlg"), _T("TEXT_CSystemsettingDlg_CNT"), _T("TEXT_CSystemsettingDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CSystemsettingDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CSystemsettingDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

void CSystemsettingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, ID_DLG_LABEL_SYSTEMSETTING_NAME, m_LabelSystemSettingName);
	DDX_Control(pDX, IDC_LABEL1, m_Label_Name[0]);
	DDX_Control(pDX, IDC_LABEL2, m_Label_Name[1]);
	DDX_Control(pDX, IDC_LABEL3, m_Label_Name[2]);
	DDX_Control(pDX, IDC_LABEL4, m_Label_Name[3]);
	DDX_Control(pDX, IDC_LABEL5, m_Label_Name[4]);
	DDX_Control(pDX, IDC_LABEL6, m_Label_Name[5]);
	DDX_Control(pDX, IDC_LABEL7, m_Label_Name[6]);
	DDX_Control(pDX, IDC_LABEL8, m_Label_Name[7]);
	DDX_Control(pDX, IDC_LABEL9, m_Label_Name[8]);
	DDX_Control(pDX, IDC_LABEL10, m_Label_Name[9]);	
	DDX_Control(pDX, IDC_LABEL11, m_Label_Name[10]);	
	DDX_Control(pDX, IDC_LABEL12, m_Label_Name[11]);	
	DDX_Control(pDX, IDC_LABEL13, m_Label_Name[12]);	
	DDX_Control(pDX, IDC_LABEL14, m_Label_Name[13]);	
	DDX_Control(pDX, IDC_LABEL16, m_Label_Name[14]);	
	DDX_Control(pDX, IDC_LABEL17, m_Label_Name[15]);	
	DDX_Text(pDX, IDC_EDIT_CAPA_ERROR, m_nCapaErrlimit);
	DDX_Text(pDX, IDC_EDIT_CONTACT_ERROR, m_nContactErrlimit);
	DDX_Text(pDX, IDC_EDIT_STAGE_LOG_FILE_SAVE_PERIOD, m_nStageLogSavePeriod);
	DDX_Text(pDX, IDC_EDIT_FMS_LOG_FILE_SAVE_PERIOD, m_nFMSLogSavePeriod);
	DDX_Text(pDX, IDC_EDIT_DELAY_BLUE_READY, m_nDelayBlueReady);
	DDX_Text(pDX, IDC_EDIT_DELAY_BLUE_END, m_nDelayBlueEnd);
	DDX_Text(pDX, IDC_EDIT_DELAY_RED_END, m_nDelayRedEnd);
	DDX_Text(pDX, IDC_EDIT_RESULT_FILE_SAVE_PERIOD, m_nResultFileSavePeriod);
	DDX_Text(pDX, IDC_EDIT_PC_FILE_SAVE_PERIOD, m_nPrecisionDataSavePeriod);
	DDX_Text(pDX, IDC_EDIT_DELAY_RED_TRAYIN, m_nDelayRedTrayIn);
	DDX_Text(pDX, IDC_EDIT_FMS_PORT_NUM, m_nFMSPortNum);
	DDX_Text(pDX, IDC_EDIT_MON_PORT_NUM, m_nMonPortNum);
	DDX_Control(pDX, IDC_MON_IP, m_ctrlMonIp);
	DDX_Check(pDX, IDC_USE_MON, m_bUseMon);
	DDX_Check(pDX, IDC_USE_PRECISION_DATA, m_bUsePrecisionData);
	DDX_Check(pDX, IDC_USE_FAULT_ALARM, m_bUseFaultAlarm);
	DDX_Text(pDX, IDC_EDIT_ERROR_HISTORY_SAVE_PERIOD, m_nErrorHistorySavePeriod);
	DDX_Control(pDX, IDC_LABEL_MONITORING_CONNET_STATE, m_LabelMonitoringConnectState);
	DDX_Text(pDX, IDC_EDIT_CHANNEL_ERROR, m_nChErrlimit);
	DDX_Text(pDX, IDC_EDIT_IMS_RECORD_CONDITION_TIME, m_nImsRecordConditionTime);
}


BEGIN_MESSAGE_MAP(CSystemsettingDlg, CDialog)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BTN_PASSWORD_SETTING, &CSystemsettingDlg::OnBnClickedBtnPasswordSetting)
	ON_BN_CLICKED(IDOK, &CSystemsettingDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_USE_MON, &CSystemsettingDlg::OnBnClickedUseMon)
END_MESSAGE_MAP()


// CSystemsettingDlg 메시지 처리기입니
void CSystemsettingDlg::InitLabel()
{
	m_LabelSystemSettingName.SetBkColor(RGB_MIDNIGHTBLUE)
		.SetTextColor(RGB_WHITE)
		.SetFontSize(20)
		.SetFontBold(TRUE)
		.SetText("System Setting");
		
	m_LabelMonitoringConnectState.SetBkColor(RGB_RED)
		.SetTextColor(RGB_WHITE)
		.SetFontSize(24)
		.SetFontBold(TRUE)
		.SetText(TEXT_LANG[0]);//"연결끊김"
	
	int i = 0;
	for( i=0; i<MAX_LABEL_CNT; i++ )
	{
		m_Label_Name[i].SetBkColor(RGB_LABEL_BACKGROUND);
		m_Label_Name[i].SetTextColor(RGB_WHITE);
		m_Label_Name[i].SetFontSize(LABEL_FONT_SIZE);
		m_Label_Name[i].SetFontBold(TRUE);
		m_Label_Name[i].SetFontAlign(DT_LEFT);
	}
}

BOOL CSystemsettingDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	
	InitFont();

	InitLabel();	

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CSystemsettingDlg::InitFont()
{	
	LOGFONT			LogFont;

	GetDlgItem(IDC_EDIT_CONTACT_ERROR)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight = 1000;
	LogFont.lfHeight = 20;

	font.CreateFontIndirect( &LogFont );

	GetDlgItem(IDC_EDIT_CONTACT_ERROR)->SetFont(&font);
	GetDlgItem(IDC_EDIT_CAPA_ERROR)->SetFont(&font);
	GetDlgItem(IDC_EDIT_CHANNEL_ERROR)->SetFont(&font);
	GetDlgItem(IDC_EDIT_DELAY_BLUE_READY)->SetFont(&font);
	GetDlgItem(IDC_EDIT_DELAY_RED_TRAYIN)->SetFont(&font);	
	
	GetDlgItem(IDC_EDIT_DELAY_BLUE_END)->SetFont(&font);
	GetDlgItem(IDC_EDIT_DELAY_RED_END)->SetFont(&font);
	GetDlgItem(IDC_EDIT_RESULT_FILE_SAVE_PERIOD)->SetFont(&font);
	GetDlgItem(IDC_EDIT_STAGE_LOG_FILE_SAVE_PERIOD)->SetFont(&font);
	GetDlgItem(IDC_EDIT_FMS_LOG_FILE_SAVE_PERIOD)->SetFont(&font);
	GetDlgItem(IDC_EDIT_PC_FILE_SAVE_PERIOD)->SetFont(&font);
	GetDlgItem(IDC_EDIT_ERROR_HISTORY_SAVE_PERIOD)->SetFont(&font);
	
	GetDlgItem(IDC_EDIT_FMS_PORT_NUM)->SetFont(&font);	
	GetDlgItem(IDC_EDIT_MON_PORT_NUM)->SetFont(&font);

	GetDlgItem(IDC_EDIT_IMS_RECORD_CONDITION_TIME)->SetFont(&font);	
	
	GetDlgItem(IDC_MON_IP)->SetFont(&font);	
	GetDlgItem(IDC_USE_MON)->SetFont(&font);
}

void CSystemsettingDlg::OnBnClickedBtnPasswordSetting()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

}

void CSystemsettingDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	SaveSetting();

	OnOK();
}

bool CSystemsettingDlg::LoadSetting()
{
	m_nCapaErrlimit = m_pDoc->m_nCapaErrlimit;
	m_nChErrlimit = m_pDoc->m_nChErrlimit;
	m_nContactErrlimit = m_pDoc->m_nContactErrlimit;
	m_bUseMon = m_pDoc->m_bUsePneMonitoringSystem;

	m_nImsRecordConditionTime = AfxGetApp()->GetProfileInt("FMS", "IMSRecordConditionTime", 1);	
	m_nStageLogSavePeriod = AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "StageLogSavePeriod", 30);
	m_nFMSLogSavePeriod = AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "FMSLogSavePeriod", 30);
	m_nDelayBlueReady = AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "DelayBlueReady", 5);
	m_nDelayRedTrayIn = AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "DelayRedTrayIn", 5);
	m_nDelayBlueEnd = AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "DelayBlueEnd", 5);
	m_nDelayRedEnd = AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "DelayRedEnd", 5);
	
	m_nResultFileSavePeriod = AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "ResultFileSavePeriod", 30);
	m_nPrecisionDataSavePeriod = AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "PrecisionDataSavePeriod", 30);
	m_nErrorHistorySavePeriod = AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "ErrorHistorySavePeriod", 30);	
	
	m_nFMSPortNum = AfxGetApp()->GetProfileInt("FMS", "FMSPortNum", 7001);
	m_nMonPortNum = AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "MonPortNum", 7000);
	
	m_bUsePrecisionData = AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "UsePrecisionData", FALSE);	
	m_bUseFaultAlarm = AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "UseFaultAlarm", FALSE);	
			
	//입력
	BYTE szField0 = 127 , szField1 = 0 , szField2 = 0 , szField3 = 1;	
	CString strTemp = _T("");
	CString strData = _T("");
	strTemp = AfxGetApp()->GetProfileString(FROM_SYSTEM_SECTION, "MonIP", "127.0.0.1");
	if( strTemp.Find('.') > 0 )
	{
		AfxExtractSubString(strData, strTemp, 0,'.');
		szField0 = atoi(strData);
		AfxExtractSubString(strData, strTemp, 1,'.');
		szField1 = atoi(strData);
		AfxExtractSubString(strData, strTemp, 2,'.');
		szField2 = atoi(strData);
		AfxExtractSubString(strData, strTemp, 3,'.');
		szField3 = atoi(strData);		
	}
	
	m_ctrlMonIp.SetAddress( szField0, szField1, szField2, szField3 );

	if( m_bUseMon )
	{
		m_LabelMonitoringConnectState.SetBkColor(RGB_GREEN)
			.SetTextColor(RGB_BLACK)
			.SetFontSize(28)
			.SetFontBold(TRUE)
			.SetText(TEXT_LANG[1]);		//"연결중"
	}
	else
	{
		m_LabelMonitoringConnectState.SetBkColor(RGB_RED)
			.SetTextColor(RGB_WHITE)
			.SetFontSize(28)
			.SetFontBold(TRUE)
			.SetText(TEXT_LANG[0]);	//"연결끊김"
	}
	
	UpdateData(FALSE);
	return true;
}

void CSystemsettingDlg::SaveSetting()
{
	UpdateData(TRUE);

	AfxGetApp()->WriteProfileInt(FROM_SYSTEM_SECTION, "CapaErrlimit", m_nCapaErrlimit);
	AfxGetApp()->WriteProfileInt(FROM_SYSTEM_SECTION, "ChErrlimit", m_nChErrlimit);
	AfxGetApp()->WriteProfileInt(FROM_SYSTEM_SECTION, "ContactCheckErrlimit", m_nContactErrlimit);
	
	AfxGetApp()->WriteProfileInt(FROM_SYSTEM_SECTION, "StageLogSavePeriod", m_nStageLogSavePeriod);
	AfxGetApp()->WriteProfileInt(FROM_SYSTEM_SECTION, "FMSLogSavePeriod", m_nFMSLogSavePeriod);
	AfxGetApp()->WriteProfileInt(FROM_SYSTEM_SECTION, "DelayBlueReady", m_nDelayBlueReady);
	AfxGetApp()->WriteProfileInt(FROM_SYSTEM_SECTION, "DelayRedTrayIn", m_nDelayRedTrayIn);
	AfxGetApp()->WriteProfileInt(FROM_SYSTEM_SECTION, "DelayBlueEnd", m_nDelayBlueEnd);
	AfxGetApp()->WriteProfileInt(FROM_SYSTEM_SECTION, "DelayRedEnd", m_nDelayRedEnd);

	AfxGetApp()->WriteProfileInt(FROM_SYSTEM_SECTION, "ResultFileSavePeriod", m_nResultFileSavePeriod);
	AfxGetApp()->WriteProfileInt(FROM_SYSTEM_SECTION, "PrecisionDataSavePeriod", m_nPrecisionDataSavePeriod);
	AfxGetApp()->WriteProfileInt(FROM_SYSTEM_SECTION, "ErrorHistorySavePeriod", m_nErrorHistorySavePeriod);

	AfxGetApp()->WriteProfileInt("FMS", "FMSPortNum", m_nFMSPortNum);
	AfxGetApp()->WriteProfileInt(FROM_SYSTEM_SECTION, "MonPortNum", m_nMonPortNum);
	AfxGetApp()->WriteProfileInt(FROM_SYSTEM_SECTION, "UseMon", m_bUseMon);
	AfxGetApp()->WriteProfileInt(FROM_SYSTEM_SECTION, "UsePrecisionData", m_bUsePrecisionData);
	AfxGetApp()->WriteProfileInt(FROM_SYSTEM_SECTION, "UseFaultAlarm", m_bUseFaultAlarm);

	AfxGetApp()->WriteProfileInt("FMS", "IMSRecordConditionTime", m_nImsRecordConditionTime);
	
	//입력
	CString strTemp = _T("");
	BYTE szField0 = 127 , szField1 = 0 , szField2 = 0 , szField3 = 1;
	m_ctrlMonIp.GetAddress( szField0, szField1, szField2, szField3 );
	
	strTemp.Format("%d.%d.%d.%d", szField0, szField1, szField2, szField3 );
	AfxGetApp()->WriteProfileString(FROM_SYSTEM_SECTION, "MonIP", strTemp);

	m_pDoc->m_bUsePneMonitoringSystem = m_bUseMon;
	m_pDoc->m_nContactErrlimit = m_nContactErrlimit; 
	m_pDoc->m_nCapaErrlimit = m_nCapaErrlimit;
	m_pDoc->m_nChErrlimit = m_nChErrlimit;
	m_pDoc->m_bUseFaultAlarm = m_bUseFaultAlarm;
	
}
void CSystemsettingDlg::OnBnClickedUseMon()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);	

	if( m_bUseMon )
	{
		m_LabelMonitoringConnectState.SetBkColor(RGB_GREEN)
			.SetTextColor(RGB_BLACK)
			.SetFontSize(28)
			.SetFontBold(TRUE)
			.SetText(TEXT_LANG[1]);//"연결중"
	}
	else
	{
		m_LabelMonitoringConnectState.SetBkColor(RGB_RED)
			.SetTextColor(RGB_WHITE)
			.SetFontSize(28)
			.SetFontBold(TRUE)
			.SetText(TEXT_LANG[0]);		//"연결끊김"
	}
}