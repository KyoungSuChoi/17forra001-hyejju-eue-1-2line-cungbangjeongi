/*************************************************************
	Copyrighted by LG Production engineering Research Center

	File Name: ChildFrm.h

	Written out by 
	Evaluation Technology Group
	Kim, Sung-Hoon

	Comment: CChildFrame class is defined

	Revision History:
	Version   Name            Date        Reason
	    1.0   Kim, Sung-Hoon  2001.06.01  Create
*************************************************************/

// ChildFrm.h : interface of the CChildFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CHILDFRM_H__13FE5EAA_AB5E_11D4_88DF_006008CEDA07__INCLUDED_)
#define AFX_CHILDFRM_H__13FE5EAA_AB5E_11D4_88DF_006008CEDA07__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// #include
#include "PlaneSettingDlgBar.h"

// #define

// Class definition

//---------------------------------------------------------------------------
//	Class Name: CChildFrame
//
//	Comment:
//
//	Revision History:
//	Version   Name            Date        Reason
//	    1.0   Kim, Sung-Hoon  2001.06.01  Create
//---------------------------------------------------------------------------

#include "Gridbar.h"
#include "TreeBar.h"


class CChildFrame : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CChildFrame)
public:
	CChildFrame();

// Attributes
public:
	CMyGridWnd * GetGridWnd();
	CTreeCtrl * GetTreeWnd();

// Operations
public:
	CPlaneSettingDialogBar m_wndDialogBar;
	CGridBar m_wndGridBar;
	CTreeBar m_wndLineListBar;
	CTreeBar m_wndGraphSetBar;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChildFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnUpdateFrameMenu(BOOL bActive, CWnd* pActivateWnd, HMENU hMenuAlt);
	virtual BOOL DestroyWindow();
	//}}AFX_VIRTUAL

// Implementation
public:
	BOOL VerifyBarState(LPCTSTR lpszProfileName);
	virtual ~CChildFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
protected:
	CImageList  *m_pmyImageList;
	//{{AFX_MSG(CChildFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDataSheet();
	afx_msg void OnUpdateDataSheet(CCmdUI* pCmdUI);
	afx_msg void OnLineTree();
	afx_msg void OnUpdateLineTree(CCmdUI* pCmdUI);
	afx_msg void OnEditCopy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHILDFRM_H__13FE5EAA_AB5E_11D4_88DF_006008CEDA07__INCLUDED_)
