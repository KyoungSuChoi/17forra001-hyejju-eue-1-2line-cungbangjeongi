#if !defined(AFX_MODELINFODLG_H__CB54F011_78CF_4629_9605_D52D2993771A__INCLUDED_)
#define AFX_MODELINFODLG_H__CB54F011_78CF_4629_9605_D52D2993771A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ModelInfoDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CModelInfoDlg dialog
#include "NewGridWnd.h"
#include "CTSEditorDoc.h"	// Added by ClassView

class CModelInfoDlg : public CDialog
{
// Construction
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();
	virtual ~CModelInfoDlg();

	CCTSEditorDoc *m_pDoc;
	BOOL QueryList();
	void InitGrid();
	CModelInfoDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CModelInfoDlg)
	enum { IDD = IDD_MODEL_INFO_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CModelInfoDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CMyGridWnd	m_wndModelGrid;

	// Generated message map functions
	//{{AFX_MSG(CModelInfoDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonNew();
	afx_msg void OnButtonDelete();
	afx_msg void OnButtonEdit();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MODELINFODLG_H__CB54F011_78CF_4629_9605_D52D2993771A__INCLUDED_)
