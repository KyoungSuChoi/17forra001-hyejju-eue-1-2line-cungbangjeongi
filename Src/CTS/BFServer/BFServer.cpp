// BFServer.cpp : Defines the initialization routines for the DLL.

#include "stdafx.h"
#include "BFServer.h"

#include "Worker.h"
#include "ThreadDispatcher.h"
#include "afxmt.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//
//	Note!
//
//		If this DLL is dynamically linked against the MFC
//		DLLs, any functions exported from this DLL which
//		call into MFC must have the AFX_MANAGE_STATE macro
//		added at the very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//

/////////////////////////////////////////////////////////////////////////////
// CBFServerApp

BEGIN_MESSAGE_MAP(CBFServerApp, CWinApp)
	//{{AFX_MSG_MAP(CBFServerApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBFServerApp construction

CBFServerApp::CBFServerApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CBFServerApp object

CBFServerApp theApp;

//Shared Data 
#pragma data_seg("SHARDATA")
int		m_nInstalledModuleNum = 0;
int		nErrorNumber = EP_ERR_EMPTY;
int		m_Initialized = FALSE;
BOOL	m_bServerOpen = FALSE;
EP_STR_MODULE_DATA	 *g_pstModule = NULL;			//Module Struct
#pragma data_seg()

CSBCCircularQueue <SBCQueue_DATA> g_Que;
CSBCCircularQueue <SBCQueue_DATA> g_DlgQue;

static	HINSTANCE	hInstance;				
BOOL	bWriteLog = TRUE;
char	szLogFileName[512] = {""};
char	szErrorString[512];
static	Worker		m_worker;
static	CWizThreadDispatcher	*m_pDispather = NULL;
HANDLE	hMutexInstance;
int		g_channelMap[EP_MAX_CH_PER_MD];
char	crcCheckSendBuf[_EP_TX_BUF_LEN];	
char	crcCheckRecvBuf[_EP_TX_BUF_LEN];
int		g_nChannelIndexType	= EP_CH_INDEX_MEMBER;	//Channel Mapping type of Monitoring Data 
													//0: Use wChIndex( the member variable of EP_CH_DATA)
													//1: Network incomming Squence 								
													//2: Special Type 											 
													//3: Read Channel Mapping Table from "mapping.dat" File
CSBCServer *g_pSBCServer = NULL;
BOOL	InitGroupData (int nModuleIndex);
BOOL	CloseGroupData (int nModuleIndex);
inline BOOL ChannelIndexCheck(int nModuleIndex, int nGroupIndex, int nChIndex);
BOOL	ModuleIndexCheck(int nModuleIndex);
BOOL	GroupIndexCheck(int nModuleIndex, int nGroupIndex);
BOOL	LoadChMapFile();

BOOL WriteLog( char *szLog )
{
	if(bWriteLog)
	{
		//TRACE("%s\n", szLog);
		if(szLog == NULL)					return FALSE;
		if(strlen(szLogFileName) == 0)		return FALSE;

		FILE *fp = fopen(szLogFileName, "ab+");
		if(fp == NULL)		return FALSE;
		
		COleDateTime nowtime(COleDateTime::GetCurrentTime());
		fprintf(fp, "%s :: %s\r\n", nowtime.Format(), szLog);
		fclose(fp);
		
//		WriteLogFile(szLogFileName, szLog, FILE_APPEND);
		return TRUE;
	}
	return FALSE;
}

//Only One Time Execute
BOOL IsFirstInstance()
{
	hMutexInstance = ::OpenMutex(MUTEX_ALL_ACCESS|SYNCHRONIZE, FALSE, _T(INSTANCE_NAME));
	if(hMutexInstance)
		return FALSE;
	
	hMutexInstance = ::CreateMutex(NULL, TRUE, _T(INSTANCE_NAME));
//	BOOL fMutexOwned = FALSE;
	if(hMutexInstance != NULL)
	{
		return TRUE;
	}
	return FALSE;
		
/*		if(GetLastError() == ERROR_ALREADY_EXISTS)
		{
			DWORD dwWaitResult = ::WaitForSingleObject(hMutexInstance, INFINITE);
			if(dwWaitResult == WAIT_OBJECT_0)
			{
				fMutexOwned = TRUE;
			}
		}
	}

//	if(hMutexInstance)	::ReleaseMutex(hMutexInstance);
//	::CloseHandle(hMutexInstance);
	return fMutexOwned;
*/
}

//Open Formation Control Server
EPDLL_API	BOOL EPOpenFormServer(int nInstalledModuleNo,  HWND hMsgWnd)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	
	if(!IsFirstInstance())			//중복 실행 방지 
	{
		WriteLog("Initialize Socket Port Error(Use By other App)");
		nErrorNumber = EP_ERR_WINSOCK_INITIALIZE;
		return FALSE;
	}

	//Module Number Check
	if(nInstalledModuleNo <= 0 || nInstalledModuleNo > EP_MAX_MODULE_NUM)
	{
		WriteLog("Initialize Module Number Error");
		nErrorNumber = EP_ERR_INVALID_ARGUMENT;
		return FALSE;
	}

	//랜카드(NIC) 설치 유무
	//iphlpapi.h lib 를 include 시켜야 합니다. windows core sdk 안에 포함되어있습니다.

/*	IP_ADAPTER_INFO info;
	unsigned long buf;
	buf = sizeof(info);

	if ( GetAdaptersInfo(&info, &buf) == ERROR_SUCCESS ) {
	   printf("you are installed Network Interface Card");
	   printf("NIC Name : %s", info.Description);
	}
 */
 
	LoadChMapFile();

	WSADATA wsaData;
	WORD wVersionRequested = MAKEWORD( 2, 2 );		//WinSock Version Greater than 2.0
	int  err = ::WSAStartup( wVersionRequested, &wsaData );
	if (err != 0) 
	{
		// Tell the user that we couldn't find a usable WinSock DLL.
		sprintf(szErrorString, "Couldn't find a usable WinSock DLL. %d\n", err);
		WriteLog(szErrorString);
		nErrorNumber = EP_ERR_WINSOCK_STARTUP;
		return FALSE;
	}

/*	/* Confirm that the WinSock DLL supports 2.0.*/
/*	if ((LOBYTE( wsaData.wVersion ) == LOBYTE(wVersionRequested) &&
			HIBYTE( wsaData.wVersion ) < HIBYTE(wVersionRequested)) ||
			LOBYTE( wsaData.wVersion ) < LOBYTE(wVersionRequested))
	{
		WSACleanup();
//		sprintf(szErrorString, "%s. Win Socket Ver %d.%d require. Installed Ver %d.%d\n",
//			wsaData.szDescription,
//			LOBYTE(wVersionRequested), HIBYTE(wVersionRequested),
//			LOBYTE( wsaData.wVersion), HIBYTE( wsaData.wVersion));
		nErrorNumber = EP_ERR_WINSOCK_VERSION;
		return FALSE; 
	}
*/

	//Get Server IP
/*	struct hostent *serverHostent;
	struct in_addr sin_addr;
	::gethostname( szServerIP,100 );
	serverHostent = ::gethostbyname( szServerIP );
	strcpy( szServerIP, serverHostent->h_name );
	memcpy( &sin_addr, serverHostent->h_addr, serverHostent->h_length);
	strcpy( szServerIP, inet_ntoa( sin_addr ) );
*/
	m_nInstalledModuleNum = nInstalledModuleNo;
	g_pstModule = new EP_STR_MODULE_DATA[m_nInstalledModuleNum];
	ASSERT(g_pstModule);

//	ZeroMemory(g_pstModule, sizeof(EP_STR_MODULE_DATA)*m_nInstalledModuleNum);

	for(INDEX i =0; i<m_nInstalledModuleNum; i++)
	{
		InitGroupData (i);

		g_pstModule[i].state = EP_STATE_LINE_OFF;
		g_pstModule[i].nLineMode = EP_OFFLINE_MODE;
		g_pstModule[i].nOperationMode = EP_OPERATION_LOCAL;
		g_pstModule[i].nWorkType = 0;

		g_pstModule[i].m_hWriteEvent = ::CreateEvent(NULL, TRUE, FALSE, NULL);
		g_pstModule[i].m_hReadEvent  = ::CreateEvent(NULL, TRUE, FALSE, NULL);

		g_pstModule[i].lpDataBuff = NULL;

		//Default Module system Data
		g_pstModule[i].sysData.nModuleID = i+1;
		g_pstModule[i].sysData.nVersion = _EP_PROTOCOL_VERSION;
		g_pstModule[i].sysData.wInstalledBoard = EP_DEFAULT_BD_PER_MD;
		g_pstModule[i].sysData.wChannelPerBoard = EP_DEFAULT_CH_PER_BD;
		
		g_pstModule[i].sysData.nModuleGroupNo = 0;
		g_pstModule[i].sysData.nTotalChNo = EP_DEFAULT_CH_PER_MD;
		g_pstModule[i].sysData.wTrayType = 0;
		g_pstModule[i].sysData.wTotalTrayNo = 1;
		g_pstModule[i].sysData.awChInTray[0] = EP_DEFAULT_CH_PER_MD;

		//Set Default Parameter
		g_pstModule[i].sysParam.nModuleID = i+1;
		g_pstModule[i].sysParam.lMaxVoltage =  ExpFloattoLong(EP_MAX_VOLTAGE, EP_VTG_FLOAT);
		g_pstModule[i].sysParam.lMinVoltage =  ExpFloattoLong(EP_MIN_VOLTAGE, EP_VTG_FLOAT);
		g_pstModule[i].sysParam.lMaxCurrent =  ExpFloattoLong(EP_MAX_CURRENT, EP_CRT_FLOAT);
		g_pstModule[i].sysParam.lMinCurrent =  ExpFloattoLong(EP_MIN_CURRENT, EP_CRT_FLOAT);
		g_pstModule[i].sysParam.sWanningTemp = EP_MAX_TEMPERATURE*(long)EP_ETC_EXP;		
		g_pstModule[i].sysParam.sCutTemp	= EP_MAX_TEMPERATURE*(long)EP_ETC_EXP;		
		g_pstModule[i].sysParam.sJigWanningTemp = EP_MAX_TEMPERATURE*(long)EP_ETC_EXP;		
		g_pstModule[i].sysParam.sJigCutTemp = EP_MAX_TEMPERATURE*(long)EP_ETC_EXP;		
		
		g_pstModule[i].sysParam.bUseTempLimit = FALSE;
		g_pstModule[i].sysParam.bUseJigTempLimit = FALSE;

		g_pstModule[i].sysParam.lV24Over = ExpFloattoLong(EP_MAX_V24_IN, EP_VTG_FLOAT);
		g_pstModule[i].sysParam.lV24Limit =  ExpFloattoLong(EP_MIN_V24_IN, EP_VTG_FLOAT);
		g_pstModule[i].sysParam.lV12Over =  ExpFloattoLong(EP_MAX_V12_OUT, EP_VTG_FLOAT);
		g_pstModule[i].sysParam.lV12Limit =  ExpFloattoLong(EP_MIN_V12_OUT, EP_VTG_FLOAT);
		g_pstModule[i].sysParam.nAutoReportInterval = 2000;

		g_pstModule[i].CommandAck.nCmd = 0;
		g_pstModule[i].CommandAck.nCode = EP_NACK;
//		strcpy(g_pstModule[i].sysParam.IPAddr, szServerIP);
	}

	if(strlen(szLogFileName) <= 0)	//create Log File
	{
		SYSTEMTIME time;
		::GetSystemTime(&time);
		sprintf(szLogFileName, "%d%02d%02d.log", time.wYear, time.wMonth, time.wDay);
	}

	m_worker.SetMsgWnd(hMsgWnd);
	m_Initialized = TRUE;

	return m_Initialized;
}

BOOL	EPServerStart()
{
	//Server 초기화가 되어 있지 않거나 이미 Open 되어 있을 경우 
	if(m_Initialized == FALSE || m_pDispather != NULL || m_bServerOpen == TRUE) 
		return FALSE;
	
	m_pDispather = new CWizThreadDispatcher(m_worker, m_nInstalledModuleNum);	
	m_pDispather->Start();
	m_bServerOpen = TRUE;
	WriteLog("Formation Control Server Initilaized");

	return TRUE;
}

BOOL LoadChMapFile()
{
	SHORT *pTemp = NULL;

	BOOL bRtn = TRUE;
	FILE *fp = fopen(EP_CHANNEL_MAP_FILE_NAME, "rt");
	int nTemp;
	if(fp != NULL)
	{
		char buff[128];
		long ver;
		INDEX i = 0, n = 0;

		if(fscanf(fp, "%s", buff) > 0 && fscanf(fp, "%d", &ver) > 0)
		{
			int count = EP_MAX_CH_PER_MD;
			if(fscanf(fp, "%d", &count) > 0 && count > 0)
			{
				pTemp = new SHORT[count];		//중복된 Index가 있는지 검사 하기 위해 
				ASSERT(pTemp);
				for(n = 0; n<count; n++)	pTemp[n] = -1;

				for(i =0; i<count; i++)
				{				
					if(fscanf(fp, "%d", &nTemp) < 1)		//Read Fail Default Mapping
					{
						break;
					}

					if(nTemp < 0 || nTemp >= count)			//채널 범위를 벗어난 숫자 Default Mapping
					{
						sprintf(szErrorString, "Channel Mapping File Index Error (INDEX = %d)", i);
						WriteLog( szErrorString );
						break;
					}
					
					if(pTemp[nTemp] != -1 )					//현재 Index에 이미 값이 할당되어 있는데 또 있을 경우 
					{
						sprintf(szErrorString, "Channel Mapping File Index Error (INDEX = %d)", i);
						WriteLog( szErrorString );
						break;
					}

					g_channelMap[i] = nTemp;
					pTemp[nTemp] = (SHORT)nTemp;
				}
				
				if(count == i)	bRtn = FALSE;		//Read Success
			}
		}
		fclose(fp);
	}

	if(bRtn)
	{
		for(INDEX i =0; i<EP_MAX_CH_PER_MD; i++)
		{
			g_channelMap[i] = i;
		}
	}

	if( pTemp != NULL)
	{
		delete pTemp;
		pTemp = NULL;
	}
	return TRUE;
}

BOOL	ModuleIndexCheck(int nModuleIndex)
{
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return FALSE;
	}
	return TRUE;
}

BOOL	GroupIndexCheck(int nModuleIndex, int /*nGroupIndex*/)
{
	if(ModuleIndexCheck(nModuleIndex) == FALSE)	return FALSE;

	return TRUE;
}

inline BOOL ChannelIndexCheck(int nModuleIndex, int nGroupIndex, int nChIndex)
{
	if(GroupIndexCheck(nModuleIndex, nGroupIndex) == FALSE)	return FALSE;
	if(nChIndex <0 || nChIndex >= g_pstModule[nModuleIndex].sysData.nTotalChNo)
	{
		nErrorNumber = EP_ERR_CHANNEL_NOT_EXIST;
		return FALSE;
	}
	return TRUE;
}
/*
int EPGetGroupNo(int nModuleIndex)
{
	if(g_pstModule == NULL)
	{
		nErrorNumber = EP_ERR_NOT_INITIALIZED;
		return -1;
	}
	if(ModuleIndexCheck(nModluleIndex) == FALSE)	return -1;
	return g_pstModule[nModuleIndex].sysData.nTotalGroupNo;
}
*/

BOOL	CloseGroupData (int nModuleIndex)
{
	if(ModuleIndexCheck(nModuleIndex) == FALSE)	return FALSE;
	
	int nGroupNo =  g_pstModule[nModuleIndex].gpData.GetSize();
	LPEP_GROUP_INFO	pGroup;
	LPEP_CH_DATA	pChannel;

	INDEX i=0, j=0;

	for( i =0; i<nGroupNo; i++ )
	{
		pGroup = (EP_GROUP_INFO *)g_pstModule[nModuleIndex].gpData[i];	
		ASSERT(pGroup);
		
		for( j =0; j<pGroup->chData.GetSize(); j++ )
		{
			pChannel = (EP_CH_DATA *)pGroup->chData[j];
			delete pChannel;
			pChannel = NULL;
//			TRACE("Release channel end data %d\n", j);
		}
		pGroup->chData.RemoveAll();
		
		for(j =0; j<pGroup->chRealData.GetSize(); j++)
		{
			EP_REAL_TIME_DATA *pRltData = (EP_REAL_TIME_DATA *)pGroup->chRealData[j];
			delete pRltData;
			pRltData = NULL;
//			TRACE("Release channel real data %d\n", j);
		}
		pGroup->chRealData.RemoveAll();

		delete pGroup;
		pGroup = NULL;
	}
	g_pstModule[nModuleIndex].gpData.RemoveAll();
	return TRUE;
}

EPDLL_API EP_MD_SYSTEM_DATA * EPGetModuleSysData(int nModuleIndex)
{
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)	return NULL;
	return &g_pstModule[nModuleIndex].sysData;
}

EPDLL_API EP_MD_FW_VER_DATA * EPGetModuleVersionData(int nModuleIndex)
{
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)	return NULL;
	return &g_pstModule[nModuleIndex].fwVerData;
}


EPDLL_API EP_MD_SBC_PARAM * EPGetModuleSbcParam(int nModuleIndex)
{
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)	return NULL;
	return &g_pstModule[nModuleIndex].sbcParam;
}


BOOL InitGroupData (int nModuleIndex)
{
	if(ModuleIndexCheck(nModuleIndex) == FALSE)	return FALSE;
	if(CloseGroupData(nModuleIndex) == FALSE)	return FALSE;	
	
	ASSERT(g_pstModule[nModuleIndex].gpData.GetSize() == 0);

	LPEP_GROUP_INFO		pGroup;
	LPEP_CH_DATA		pChannel;
	LPEP_MD_SYSTEM_DATA	pSysData;
	
	pSysData = EPGetModuleSysData(nModuleIndex);
	ASSERT(pSysData);

	pGroup = new EP_GROUP_INFO;
	ASSERT(pGroup);
	ZeroMemory(&pGroup->gpData, sizeof(EP_GP_DATA));
	g_pstModule[nModuleIndex].gpData.Add(pGroup);	

	pGroup->gpData.gpState.state = EP_STATE_LINE_OFF;	
	for(INDEX j =0; j<pSysData->nTotalChNo; j++)
	{
		pChannel = new EP_CH_DATA;
		ZeroMemory(pChannel, sizeof(EP_CH_DATA));
		pGroup->chData.Add(pChannel);

		EP_REAL_TIME_DATA *pRltData = new EP_REAL_TIME_DATA;
		ZeroMemory(pRltData, sizeof(EP_REAL_TIME_DATA));
		pGroup->chRealData.Add(pRltData);
	}

	return TRUE;
}

EPDLL_API	EP_GROUP_INFO* EPGetGroupInfo(int nModuleID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	int nModuleIndex = EPGetModuleIndex(nModuleID);
	if(g_pstModule == NULL)
	{
		nErrorNumber = EP_ERR_NOT_INITIALIZED;
		return NULL;
	}

	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return NULL;
	}

	return (EP_GROUP_INFO*)g_pstModule[nModuleIndex].gpData[0];
}

//Close Formation Control Server
EPDLL_API	BOOL EPCloseFormServer()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if(m_Initialized)
	{
		//1. Close Sever
		if (m_bServerOpen)				
		{
			m_pDispather->Stop(TRUE);
			delete m_pDispather;
			m_pDispather = NULL;
			m_bServerOpen = FALSE;
		}
		::WSACleanup();
	
		//2. Close Handle
		::ReleaseMutex(hMutexInstance);
		::CloseHandle(hMutexInstance);

		//3. Release group structure; 
		for (INDEX nI = 0; nI < m_nInstalledModuleNum; nI++)		//Close Event
		{	
			::CloseHandle(g_pstModule[nI].m_hWriteEvent);
			::CloseHandle(g_pstModule[nI].m_hReadEvent);

			if(g_pstModule[nI].lpDataBuff != NULL)
			{
				delete [] g_pstModule[nI].lpDataBuff;
				g_pstModule[nI].lpDataBuff = NULL;
			}
			CloseGroupData(nI);
		}

		//4. Release Module structure
		if(g_pstModule)					//Close Module Data Structure
		{
			delete []	g_pstModule;
			g_pstModule = NULL;
		}
	}
	m_Initialized= FALSE;
	WriteLog("Formation Control Server Closed");
	return TRUE;
}

//Get Last Error Code
EPDLL_API	int EPGetLastError()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return nErrorNumber;
}

//Get Installed Module Number
EPDLL_API	int EPGetInstalledModuleNum()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return m_nInstalledModuleNum;
}

//Get Module ID
EPDLL_API inline int EPGetModuleID(int nModuleIndex)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
//#ifdef _DEBUG
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return -1;
	}
//#endif	

	if(g_pstModule == NULL)
	{
		nErrorNumber = EP_ERR_NOT_INITIALIZED;
		return -1;
	}
	return g_pstModule[nModuleIndex].sysParam.nModuleID;
}

//Get Module Index
EPDLL_API inline int EPGetModuleIndex(int nModuleID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
//#ifdef _DEBUG
	if(g_pstModule == NULL)
	{
		nErrorNumber = EP_ERR_NOT_INITIALIZED;
		return -1;
	}
//#endif

	for(INDEX i=0; i<m_nInstalledModuleNum; i++)
	{
		if(g_pstModule[i].sysParam.nModuleID == nModuleID)	return i;
	}

	nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
	return -2;	
}

//Get Module Ip Address
EPDLL_API	LPSTR EPGetModuleIP(int nModuleID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	int nModuleIndex = EPGetModuleIndex(nModuleID);

	if(g_pstModule == NULL)
	{
		nErrorNumber = EP_ERR_NOT_INITIALIZED;
		return "Not Init.";
	}

/*	if(g_pstModule[nModuleIndex].state == EP_STATE_LINE_OFF)
	{
		nErrorNumber = EP_ERR_LINE_OFF;
		return "LineOff";
	}
*/	
//#ifdef _DEBUG
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return "MD Index Error";
	}
//#endif

	return g_pstModule[nModuleIndex].sysParam.szIPAddr;
}

EPDLL_API BOOL EPInitSysParam(int nModuleIndex,  EP_SYSTEM_PARAM *pParam)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
//#ifdef _DEBUG
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return FALSE;
	}

	if(pParam == NULL)
	{
		nErrorNumber = EP_ERR_INVALID_ARGUMENT;
		return FALSE;
	}
//#endif

	if(g_pstModule == NULL)
	{
		nErrorNumber = EP_ERR_NOT_INITIALIZED;
		return FALSE;
	}

	memcpy(&g_pstModule[nModuleIndex].sysParam, pParam, sizeof(EP_SYSTEM_PARAM));
//	strcpy(g_pstModule[nModuleIndex].szIPAddress, szServerIP);
	strcpy(g_pstModule[nModuleIndex].sysParam.szIPAddr, pParam->szIPAddr);
	
	return TRUE;
}

//Set Module System Parameter
EPDLL_API	BOOL EPSetSysParam(int nModuleID, EP_SYSTEM_PARAM *pParam)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	int nModuleIndex = EPGetModuleIndex(nModuleID);
	return EPInitSysParam(nModuleIndex,  pParam);
}

EPDLL_API	EP_SYSTEM_PARAM * EPGetSysParam(int nModuleID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	int nModuleIndex = EPGetModuleIndex(nModuleID);

//#ifdef _DEBUG
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return NULL;
	}
//#endif

	if(g_pstModule == NULL)
	{
		nErrorNumber = EP_ERR_NOT_INITIALIZED;
		return NULL;
	}
	return &g_pstModule[nModuleIndex].sysParam;
}

EPDLL_API	WORD EPGetModuleState(int nModuleID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	WORD state = EP_STATE_LINE_OFF;

	if(g_pstModule == NULL)
	{
		nErrorNumber = EP_ERR_NOT_INITIALIZED;
//		sprintf(szErrorString, "Network is Not Initialized\n");
		return state;
	}

	int nModuleIndex = EPGetModuleIndex(nModuleID);

//#ifdef _DEBUG	
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return state;
	}
//#endif

	return	g_pstModule[nModuleIndex].state;
}

EPDLL_API	WORD EPGetGroupState(int nModuleID, int /*nGroupIndex*/)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	
	WORD state = EP_STATE_ERROR;
	if(g_pstModule == NULL)
	{
		nErrorNumber = EP_ERR_NOT_INITIALIZED;
		return state;
	}

	int nModuleIndex = EPGetModuleIndex(nModuleID);

//#ifdef _DEBUG
	//문제 있는 부분 수정이 필요
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return state;
	}
//#endif

	if(g_pstModule[nModuleIndex].gpData.GetSize() <= 0)
		return state;

	EP_GROUP_INFO *pGroup =  (EP_GROUP_INFO*)g_pstModule[nModuleIndex].gpData[0];
	if(pGroup != NULL)
	{
		state = pGroup->gpData.gpState.state;
		if( state != EP_STATE_LINE_OFF )
		{
			if(g_pstModule[nModuleIndex].nOperationMode  == EP_OPEARTION_MAINTENANCE)
			{
				state = EP_STATE_MAINTENANCE;
			}		
		}
	}
	
	return state;
}

EPDLL_API	BOOL EPSetGroupState(int nModuleID, int /*nGroupIndex*/, WORD newState)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	
//	WORD state = EP_STATE_ERROR;
	if(g_pstModule == NULL)
	{
		nErrorNumber = EP_ERR_NOT_INITIALIZED;
		return FALSE;
	}

	int nModuleIndex = EPGetModuleIndex(nModuleID);

//#ifdef _DEBUG
	//문제 있는 부분 수정이 필요
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return FALSE;
	}
//#endif
	EP_GROUP_INFO *pGroup =  (EP_GROUP_INFO*)g_pstModule[nModuleIndex].gpData[0];
	pGroup->gpData.gpState.state = newState;
	
	return TRUE;
}

EPDLL_API	BOOL EPSetGroupState(int nModuleID, EP_GP_STATE_DATA& _state)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	//	WORD state = EP_STATE_ERROR;
	if(g_pstModule == NULL)
	{
		nErrorNumber = EP_ERR_NOT_INITIALIZED;
		return FALSE;
	}

	int nModuleIndex = EPGetModuleIndex(nModuleID);

	//#ifdef _DEBUG
	//문제 있는 부분 수정이 필요
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return FALSE;
	}
	//#endif
	EP_GROUP_INFO *pGroup =  (EP_GROUP_INFO*)g_pstModule[nModuleIndex].gpData[0];
	memcpy(&pGroup->gpData.gpState, &_state, sizeof(EP_GP_STATE_DATA));

	return TRUE;
}

EPDLL_API EP_GP_DATA EPGetGroupData(int nModuleID, int /*nGroupIndex*/)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	
	EP_GP_DATA gpData;
	ZeroMemory(&gpData, sizeof(EP_GP_DATA));
	
	if(g_pstModule == NULL)
	{
		nErrorNumber = EP_ERR_NOT_INITIALIZED;
		return gpData;
	}
	
	int nModuleIndex = EPGetModuleIndex(nModuleID);

//#ifdef _DEBUG
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return gpData;
	}
	
//#endif

	EP_GROUP_INFO *pGroup =  (EP_GROUP_INFO*)g_pstModule[nModuleIndex].gpData[0];
	if(pGroup != NULL)
	{
		gpData = pGroup->gpData;
	}
	return gpData;
}

EPDLL_API	WORD EPGetChannelState(int nModuleID, int /*nGroupIndex*/, int nChannelIndex)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	WORD state = EP_STATE_ERROR;
	if(g_pstModule == NULL)
	{
		nErrorNumber = EP_ERR_NOT_INITIALIZED;
//		sprintf(szErrorString, "Network is Not Initialized\n");
		return state;
	}

	int nModuleIndex = EPGetModuleIndex(nModuleID);

//#ifdef _DEBUG
	if(nModuleIndex<0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return state;
	}
	if(nChannelIndex<0 || nChannelIndex >= g_pstModule[nModuleIndex].sysData.nTotalChNo)
	{
		nErrorNumber = EP_ERR_CHANNEL_NOT_EXIST;
		return state;
	}
//#endif
	EP_GROUP_INFO *pGroup =  (EP_GROUP_INFO*)g_pstModule[nModuleIndex].gpData[0];
	if(pGroup != NULL)
	{
		EP_CH_DATA *pCh = (EP_CH_DATA *)pGroup->chData[nChannelIndex];
		if(pCh != NULL)
		{
			state = pCh->state;
		}
	}
	return state;
}

EPDLL_API int EPGetGroupCount(int /*nModuleID*/)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if(g_pstModule == NULL)
	{
		nErrorNumber = EP_ERR_NOT_INITIALIZED;
		return 0;
	}

	return 1;
//	int nModuleIndex = EPGetModuleIndex(nModuleID);
//	if(nModuleIndex < 0)	return 0;
//	return  g_pstModule[nModuleIndex].sysData.nTotalGroupNo;
}

EPDLL_API	int EPGetChInGroup(int nModuleID, int /*nGroupIndex*/)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if(g_pstModule == NULL)
	{
		nErrorNumber = EP_ERR_NOT_INITIALIZED;
		return 0;
	}
	int nModuleIndex = EPGetModuleIndex(nModuleID);
	return g_pstModule[nModuleIndex].sysData.nTotalChNo;
}

EPDLL_API	long EPGetChannelValue(int nModuleID, int /*GroupIndex*/, int nChannelIndex, int nItem)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	
	long lValue =0;
	if(g_pstModule == NULL)
	{
		nErrorNumber = EP_ERR_NOT_INITIALIZED;
		return lValue;
	}

	int nModuleIndex = EPGetModuleIndex(nModuleID);

//#ifdef _DEBUG
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return lValue;
	}

	if(nChannelIndex < 0 || nChannelIndex >= g_pstModule[nModuleIndex].sysData.nTotalChNo)
	{
		nErrorNumber = EP_ERR_CHANNEL_NOT_EXIST;
		return lValue;
	}
//#endif
	
	LPEP_CH_DATA  lpChannel;
	LPEP_GROUP_INFO lpGroup;
	lpGroup = (EP_GROUP_INFO *)g_pstModule[nModuleIndex].gpData[0];
	ASSERT(lpGroup);
	lpChannel = (EP_CH_DATA *)lpGroup->chData[nChannelIndex];
	ASSERT(lpChannel);

	switch(nItem)
	{
	case EP_STATE		:	//Channel State
		lValue =  (long)lpChannel->state;		break;
	case EP_VOLTAGE	:		//Voltage
		lValue =  lpChannel->lVoltage;			break;
	case EP_CURRENT	:		//Current
		lValue =  lpChannel->lCurrent;			break;
	case EP_CAPACITY	:	//Capacity
		lValue =  lpChannel->lCapacity;			break;
	case EP_CH_CODE:	//Failure Code
		lValue =  (long)lpChannel->channelCode;	break;
	case EP_TOT_TIME	:	//Total Time
		lValue =  (long)lpChannel->ulTotalTime;	break;
	case EP_STEP_TIME	:	//Step Time
		lValue =  lpChannel->ulStepTime;		break;	
	case EP_GRADE_CODE	:	//Grade Code
		lValue =  (long)lpChannel->grade;		break;
	case EP_STEP_NO	:		//Current Step Number
		lValue =  (long)lpChannel->nStepNo;		break;	
	case EP_WATT		:	//Watt
		lValue =  lpChannel->lWatt;				break;	
	case EP_WATT_HOUR	:	//Watt per Hour
		lValue =  lpChannel->lWattHour;			break;
	case EP_IMPEDANCE:		//Impedance
		lValue =  lpChannel->lImpedance;		break;
	}
	return lValue;
}

EPDLL_API EP_CH_DATA  EPGetChannelData(int nModuleID, int /*nGroupIndex*/, int nChannelIndex)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	
	EP_CH_DATA	chData;
	ZeroMemory(&chData, sizeof(EP_CH_DATA));
	if(g_pstModule == NULL)
	{
		nErrorNumber = EP_ERR_NOT_INITIALIZED;
		return chData;
	}

	int nModuleIndex = EPGetModuleIndex(nModuleID);

	if(EPGetModuleState(nModuleID) == EP_STATE_LINE_OFF)
	{
		nErrorNumber = EP_ERR_NOT_INITIALIZED;
		return chData;
	}

//#ifdef _DEBUG
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return chData;
	}

	if(nChannelIndex < 0 || nChannelIndex >= g_pstModule[nModuleIndex].sysData.nTotalChNo)
	{
		nErrorNumber = EP_ERR_CHANNEL_NOT_EXIST;
		return chData;
	}
//#endif
	
	LPEP_GROUP_INFO lpGroup;
	lpGroup = (EP_GROUP_INFO *)g_pstModule[nModuleIndex].gpData[0];
	ASSERT(lpGroup);
	memcpy(&chData, lpGroup->chData[nChannelIndex], sizeof(EP_CH_DATA));

	return chData;
}

EPDLL_API BYTE EPGetChCode(int nModuleID, int nGroupIndex, int nChannelIndex)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	
	BYTE code = 0;
	
	if(g_pstModule == NULL)
	{
		nErrorNumber = EP_ERR_NOT_INITIALIZED;
		return code;
	}

	int nModuleIndex = EPGetModuleIndex(nModuleID);

//#ifdef _DEBUG
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return code;
	}

	if(nChannelIndex < 0 || nChannelIndex >= g_pstModule[nModuleIndex].sysData.nTotalChNo)
	{
		nErrorNumber = EP_ERR_CHANNEL_NOT_EXIST;
		return code;
	}
//#endif
	
	LPEP_CH_DATA  lpChannel;
	LPEP_GROUP_INFO lpGroup;
	lpGroup = (EP_GROUP_INFO *)g_pstModule[nModuleIndex].gpData[nGroupIndex];
	ASSERT(lpGroup);
	lpChannel = (EP_CH_DATA *)lpGroup->chData[nChannelIndex];
	ASSERT(lpChannel);
	code =  (BYTE)lpChannel->channelCode;
	return code;
}

/*
EPDLL_API WORD	EPGetDoorState(int nModuleID, int nGroupIndex)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	WORD state = EP_STATE_ERROR;

	if(g_pstModule == NULL)
	{
		nErrorNumber = EP_ERR_NOT_INITIALIZED;
		return state;
	}

	int nModuleIndex = EPGetModuleIndex(nModuleID);

//#ifdef _DEBUG
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return state;
	}
	
	if(nGroupIndex < 0 || nGroupIndex >= g_pstModule[nModuleIndex].sysData.nTotalGroupNo)
	{
		nErrorNumber = EP_ERR_GROUP_NOT_EXIST;
		return state;
	}
//#endif

	EP_GROUP_INFO *pGroup =  (EP_GROUP_INFO*)g_pstModule[nModuleIndex].gpData[nGroupIndex];
	if(pGroup != NULL)
	{
		state = (WORD)pGroup->gpData.gpState.doorState;
	}
	return state;	
}
*/

EPDLL_API WORD	EPJigState(int nModuleID, int nJigIndex)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	WORD state = EP_STATE_ERROR;

	if(g_pstModule == NULL)
	{
		nErrorNumber = EP_ERR_NOT_INITIALIZED;
		return state;
	}

	int nModuleIndex = EPGetModuleIndex(nModuleID);

//#ifdef _DEBUG
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return state;
	}
//#endif

	EP_GROUP_INFO *pGroup =  (EP_GROUP_INFO*)g_pstModule[nModuleIndex].gpData[0];
	if(pGroup != NULL)
	{
		state = (WORD)pGroup->gpData.gpState.sensorState[0];
		if((state & (0x01<<nJigIndex)) != 0)
		{
			state = EP_JIG_DOWN;
		}
		else
		{
			state = EP_JIG_UP;
		}
	}
	return state;	
}


EPDLL_API WORD	EPTrayState(int nModuleID, int nJigIndex)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	WORD state = EP_TRAY_ERROR;

	int nTrayIndex =  nJigIndex;

	if(g_pstModule == NULL)
	{
		nErrorNumber = EP_ERR_NOT_INITIALIZED;
		return state;
	}

	int nModuleIndex = EPGetModuleIndex(nModuleID);

	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return state;
	}
	
	if(nTrayIndex < 0 || nTrayIndex >= g_pstModule[nModuleIndex].sysData.wTotalTrayNo)
	{
		nErrorNumber = EP_ERR_GROUP_NOT_EXIST;
		return state;
	}

	EP_GROUP_INFO *pGroup =  (EP_GROUP_INFO*)g_pstModule[nModuleIndex].gpData[0];
	if(pGroup != NULL)
	{
		state = (WORD)pGroup->gpData.gpState.sensorState[1];

		if(state&(0x01<<nTrayIndex))
		{
			state = EP_TRAY_LOAD;
		}
		else
		{
			state = EP_TRAY_NONE;
		}
		//////////////////////////////////////////////////////////////////////////
	}
	
	return state;
}

EPDLL_API WORD	EPStopperState(int nModuleID, int nJigIndex)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	WORD state = 0;

	if(g_pstModule == NULL)
	{
		nErrorNumber = EP_ERR_NOT_INITIALIZED;
		return state;
	}

	int nModuleIndex = EPGetModuleIndex(nModuleID);

//#ifdef _DEBUG
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return state;
	}
	
	if(nJigIndex < 0 || nJigIndex >= g_pstModule[nModuleIndex].sysData.wTotalTrayNo)
	{
		nErrorNumber = EP_ERR_GROUP_NOT_EXIST;
		return state;
	}
//#endif

	EP_GROUP_INFO *pGroup =  (EP_GROUP_INFO*)g_pstModule[nModuleIndex].gpData[0];
	if(pGroup != NULL)
	{
		state = (WORD)pGroup->gpData.gpState.sensorState[2];
		
		//Bit Flag 정보로 변경 
		//2006.10.11
		//////////////////////////////////////////////////////////////////////////
		if(state&(0x01<<nJigIndex))
		{
			state = EP_ON;
		}
		else
		{
			state = EP_OFF;
		}
		//////////////////////////////////////////////////////////////////////////
	}
	return state;	
}

EPDLL_API WORD	EPDoorState(int nModuleID, int nJigIndex)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	WORD state = EP_STATE_ERROR;

	if(g_pstModule == NULL)
	{
		nErrorNumber = EP_ERR_NOT_INITIALIZED;
		return state;
	}

	int nModuleIndex = EPGetModuleIndex(nModuleID);
//#ifdef _DEBUG
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return state;
	}
	
	if(nJigIndex < 0 || nJigIndex >= g_pstModule[nModuleIndex].sysData.wTotalTrayNo)
	{
		nErrorNumber = EP_ERR_GROUP_NOT_EXIST;
		return state;
	}
//#endif

	EP_GROUP_INFO *pGroup =  (EP_GROUP_INFO*)g_pstModule[nModuleIndex].gpData[0];
	if(pGroup != NULL)
	{
		state = (WORD)pGroup->gpData.gpState.sensorState[3];
		if(state & (0x01<<nJigIndex))
		{
			state = EP_DOOR_CLOSE;
		}
		else
		{
			state = EP_DOOR_OPEN;
		}
	}

	return state;
}

/*
int WriteToModule(int nModuleIndex, LPVOID pData, int nLen)
{
	ASSERT(g_pstModule && pData);
	ASSERT(nModuleIndex>=0 && nModuleIndex < m_nInstalledModuleNum);

	memcpy(g_pstModule[nModuleIndex].szTxBuffer, pData, nLen);
	g_pstModule[nModuleIndex].nTxLength = nLen;

	::SetEvent(g_pstModule[nModuleIndex].m_hWriteEvent);
	return nLen;
}

//Read Data to Buffer From Module  
int ReadFromModule(int nModuleIndex, LPVOID pData, int nLen)
{
	ASSERT(g_pstModule && pData);
	ASSERT(nModuleIndex>=0 && nModuleIndex < m_nInstalledModuleNum);

	int nRtn = -1;
	if (g_pstModule[nModuleIndex].nRxLength >= nLen )		//Received
	{
		memcpy(pData, g_pstModule[nModuleIndex].szRxBuffer, nLen);
		nRtn = nLen;
	} 
	else 
	{
		::ResetEvent(g_pstModule[nModuleIndex].m_hReadEvent);
		
		if (::WaitForSingleObject(g_pstModule[nModuleIndex].m_hReadEvent, _EP_MSG_TIMEOUT) == WAIT_OBJECT_0 )
		{
			if (g_pstModule[nModuleIndex].nRxLength >= nLen ) 
			{
				memcpy(pData, g_pstModule[nModuleIndex].szRxBuffer, nLen);
				nRtn = nLen;
			}
			else
			{
				nErrorNumber = EP_ERR_MODULE_READ_FAIL;
			}
		}
		else
		{
			nErrorNumber = EP_ERR_RECEIVE_TIMEOUT;
		}
	}
	g_pstModule[nModuleIndex].nRxLength = 0; 
	return nRtn;
}
*/

//Read Data From module
int ReadModuleData(int nModuleIndex, int nLen)
{
	ASSERT(g_pstModule);
	ASSERT(nModuleIndex>=0 && nModuleIndex < m_nInstalledModuleNum);

	int nRtn = -1;
	bool bRecieve = false;
	
	while( !bRecieve )
	{		
		if (::WaitForSingleObject(g_pstModule[nModuleIndex].m_hReadEvent, _EP_MSG_TIMEOUT) == WAIT_OBJECT_0 )
		{
			if (g_pstModule[nModuleIndex].nRxLength >= nLen ) 
			{
				bRecieve = true;
				nRtn = g_pstModule[nModuleIndex].nRxLength;
			}
			else
			{
				::ResetEvent(g_pstModule[nModuleIndex].m_hReadEvent);
			}
		}
		else
		{
			bRecieve = true;
			nErrorNumber = EP_ERR_RECEIVE_TIMEOUT;
		}
	}
	/*
	if (::WaitForSingleObject(g_pstModule[nModuleIndex].m_hReadEvent, _EP_MSG_TIMEOUT) == WAIT_OBJECT_0 )
	{
		if (g_pstModule[nModuleIndex].nRxLength >= nLen ) 
		{
			nRtn = g_pstModule[nModuleIndex].nRxLength;
		}
	}
	else
	{
		nErrorNumber = EP_ERR_RECEIVE_TIMEOUT;
	}
	*/
	
	::ResetEvent(g_pstModule[nModuleIndex].m_hReadEvent);
	g_pstModule[nModuleIndex].nRxLength = 0; 
	return nRtn;
}

//read From Command Response (ACK/NACK/FAIL)
int ReadAck(int nModuleIndex)
{
	ASSERT(g_pstModule);
	ASSERT(nModuleIndex>=0 && nModuleIndex < m_nInstalledModuleNum);
	int nAck = EP_FAIL;
	int bRtn = false;

	if (::WaitForSingleObject(g_pstModule[nModuleIndex].m_hReadEvent, /*INFINITE*/_EP_MSG_TIMEOUT) == WAIT_OBJECT_0 )
	{
//		::ResetEvent(g_pstModule[nModuleIndex].m_hReadEvent);
		nAck = g_pstModule[nModuleIndex].CommandAck.nCode;
	}
	else
	{
		nAck =  EP_TIMEOUT;
	}
	
	::ResetEvent(g_pstModule[nModuleIndex].m_hReadEvent);
	g_pstModule[nModuleIndex].CommandAck.nCmd = 0;
	g_pstModule[nModuleIndex].CommandAck.nCode = EP_FAIL;
	//TRACE("Read Response of Command %d\n", nAck);
	return nAck;
}


//Send Command to Module and Read response Data
EPDLL_API LPVOID EPSendDataCmd(int nModuleID, int /*nGroupNo*/, int nChannelNo, int nCmd, int &nSize)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	
	if(m_Initialized == FALSE)
	{
		nErrorNumber = EP_ERR_NOT_INITIALIZED;
		return NULL;
	}

	int nModuleIndex = EPGetModuleIndex(nModuleID);
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return NULL;
	}

	if(nChannelNo < 0 || nChannelNo > g_pstModule[nModuleIndex].sysData.nTotalChNo)
	{
		nErrorNumber = EP_ERR_CHANNEL_NOT_EXIST;
		return NULL;
	}

	int nPacketSize = SizeofHeader();
	ASSERT(nPacketSize < _EP_TX_BUF_LEN);

//	m_worker.m_TxBuffCritic.Lock();					//Tx Buffer Sync.

	LPEP_MSG_HEADER pMsgHeader;
	pMsgHeader = (LPEP_MSG_HEADER)g_pstModule[nModuleIndex].szTxBuffer;
	ASSERT(pMsgHeader);
	pMsgHeader->nID = EP_ID_FORM_OP_SYSTEM;
	pMsgHeader->wGroupNum = (WORD)1;			//0:module, 1~:Board

	if(nChannelNo > 0)
		pMsgHeader->wChannelNum = (WORD)EPGetHWChIndex(nChannelNo-1);		//0:Board,  1~:Channel
	else
		pMsgHeader->wChannelNum = 0;		//0:Board,  1~:Channel

	pMsgHeader->nCommand = nCmd;
    // pMsgHeader->nLength = nPacketSize;
    pMsgHeader->nLength = nPacketSize + 2;		// CRC16 Add
	g_pstModule[nModuleIndex].nTxLength = nPacketSize;

//	m_worker.m_TxBuffCritic.Unlock();
	if(EPGetModuleState(nModuleID) == EP_STATE_LINE_OFF)
	{
		nErrorNumber = EP_ERR_LINE_OFF;
		return NULL;
	}
	
	bool bRtn = false;
	int nAck = EP_FAIL;
	
	bRtn = ::SetEvent(g_pstModule[nModuleIndex].m_hWriteEvent);	
	if( bRtn == false )
	{
		g_pstModule[nModuleIndex].CommandAck.nCode = nAck;
		nErrorNumber = EP_ERR_MODULE_READ_FAIL;
		return NULL;
	}
	else
	{	
		nPacketSize = nSize;
		
		if (::WaitForSingleObject(g_pstModule[nModuleIndex].m_hReadEvent, _EP_MSG_TIMEOUT) == WAIT_OBJECT_0 )
		{			
			nSize = g_pstModule[nModuleIndex].nRxLength;
		}
		else
		{
			nErrorNumber = EP_ERR_RECEIVE_TIMEOUT;
		}
	}
	
	if( !::ResetEvent(g_pstModule[nModuleIndex].m_hReadEvent) )
	{
		char msg[512];
		sprintf(msg, "Module %d :: ResetEvent failed(%d)\n", g_pstModule[nModuleIndex].sysParam.nModuleID, GetLastError());
		WriteLog(msg);
	}

	g_pstModule[nModuleIndex].CommandAck.nCmd = 0;
	g_pstModule[nModuleIndex].CommandAck.nCode = nAck;

	if( nSize != nPacketSize + 2 )
	{
		nErrorNumber = EP_ERR_MODULE_READ_FAIL;
		return NULL;	
	}
	
	return	g_pstModule[nModuleIndex].szRxBuffer;
}

// EPDLL_API BOOL EPNewSendDataCmd(HWND _noti, int nModuleID, int /*nGroupNo*/, int nChannelNo, int nCmd)
// {
// 	AFX_MANAGE_STATE(AfxGetStaticModuleState());
// 
// 	if(m_Initialized == FALSE)
// 	{
// 		nErrorNumber = EP_ERR_NOT_INITIALIZED;
// 		return NULL;
// 	}
// 
// 	int nModuleIndex = EPGetModuleIndex(nModuleID);
// 	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
// 	{
// 		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
// 		return NULL;
// 	}
// 
// 	if(nChannelNo < 0 || nChannelNo > g_pstModule[nModuleIndex].sysData.nTotalChNo)
// 	{
// 		nErrorNumber = EP_ERR_CHANNEL_NOT_EXIST;
// 		return NULL;
// 	}
// 
// 	int nPacketSize = SizeofHeader();
// 	ASSERT(nPacketSize < _EP_TX_BUF_LEN);
// 
// 	//	m_worker.m_TxBuffCritic.Lock();					//Tx Buffer Sync.
// 
// 	LPEP_MSG_HEADER pMsgHeader;
// 	pMsgHeader = (LPEP_MSG_HEADER)g_pstModule[nModuleIndex].szTxBuffer;
// 	ASSERT(pMsgHeader);
// 	pMsgHeader->nID = EP_ID_FORM_OP_SYSTEM;
// 	pMsgHeader->wGroupNum = (WORD)1;			//0:module, 1~:Board
// 
// 	if(nChannelNo > 0)
// 		pMsgHeader->wChannelNum = (WORD)EPGetHWChIndex(nChannelNo-1);		//0:Board,  1~:Channel
// 	else
// 		pMsgHeader->wChannelNum = 0;		//0:Board,  1~:Channel
// 
// 	pMsgHeader->nCommand = nCmd;
// 	pMsgHeader->nLength = nPacketSize;
// 	g_pstModule[nModuleIndex].nTxLength = nPacketSize;
// 
// 	//	m_worker.m_TxBuffCritic.Unlock();
// 	if(EPGetModuleState(nModuleID) == EP_STATE_LINE_OFF)
// 	{
// 		nErrorNumber = EP_ERR_LINE_OFF;
// 		return NULL;
// 	}
// 
// 	BOOL _error = FALSE;
// 	if(g_pSBCServer)
// 		_error = g_pSBCServer->SavePacket(nModuleID, pMsgHeader, (BYTE*)NULL, 0);
// 	if(_error)
// 	{
// 		_error = EPSendCommand(nModuleID);
// 		int responseCmd = 0;
// 		switch(nCmd)
// 		{
// 		case EP_CMD_LINE_MODE_REQUEST:
// 			responseCmd = EP_CMD_LINE_MODE_DATA;
// 			break;
// 		case EP_CMD_MAPPING_INFO:
// 			responseCmd = EP_CMD_MAPPING_DATA;
// 			break;
// 		case EP_CMD_GET_CH_CAL_POINT:
// 			responseCmd = 0x0001300D;
// 			break;
// 		}
// 		g_pSBCServer->SetNotiHWND(nModuleID, _noti, responseCmd);
// 	}
// 
// 	 return _error;
// 
// 	//::SetEvent(g_pstModule[nModuleIndex].m_hWriteEvent);
// 
// 	//nPacketSize = nSize;
// 
// 	//nSize = ReadModuleData(nModuleIndex, nPacketSize);
// 	//if(nSize != nPacketSize)		//Receive Data Error
// 	//{
// 	//	nErrorNumber = EP_ERR_MODULE_READ_FAIL;
// 	//	return NULL;
// 	//} 
// 
// 	//return	g_pstModule[nModuleIndex].szRxBuffer;
// }


//Send Command to Module and Read response 
//nGroupNo and nChannelNo is one base Index;
//nGroup == 0 => Modlule Command
//nChannelNo == 0 => Group Command
// EPDLL_API int EPSaveCommand(int nModuleID, int /*nGroupNo*/, int nChannelNo, UINT nCmd, LPVOID lpData, UINT nSize)
// {
// 	//제어가 바뀐다(DLL <-> EXE)
// 	AFX_MANAGE_STATE(AfxGetStaticModuleState());
// 	
// 	if(m_Initialized == FALSE)
// 	{
// 		nErrorNumber = EP_ERR_NOT_INITIALIZED;
// 		return EP_FAIL;
// 	}
// 
// 	if(nSize >0)
// 	{
// 		if(lpData == NULL)
// 		{
// 			nErrorNumber = EP_ERR_INVALID_ARGUMENT;
// 			return EP_FAIL;
// 		}
// 	}
// 
// 	int nModuleIndex = EPGetModuleIndex(nModuleID);
// 	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
// 	{
// 		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
// 		return EP_FAIL;
// 	}
// 
// 	if(nChannelNo < 0 || nChannelNo > g_pstModule[nModuleIndex].sysData.nTotalChNo)
// 	{
// 		nErrorNumber = EP_ERR_CHANNEL_NOT_EXIST;
// 		return EP_FAIL;
// 	}
// 
// /*	if(nCmd > EP_MODULE_CMD)			//Module Command이면 Board나 Channel Index를 0으로 한다.
// 	{
// 		nGroupNo = 0;
// 		nChannelNo = 0;
// 	}
// 	else if(nCmd > EP_GROUP_CMD)		//Board에만 해당 하는 Command
// 	{
// 		nGroupNo++;
// 		nChannelNo = 0;
// 	}
// 	else								//Channel Command	
// 	{
// 		nGroupNo++;
// 		nChannelNo++;				//Module과의 통신은 0이면 전체 적용 Command
// 	}
// */	
// 
// //	UINT nBodySize = GetCmdDataSize(nCmd);				//Get Write Body Data Size 
// //	ASSERT(nSize == nLength);
// //	nLength = nSize;
// 
// 	UINT nBodySize = nSize;
// 	int nPacketSize = nBodySize + SizeofHeader();		//Total Frame Size
// 	if(nPacketSize >= _EP_TX_BUF_LEN)
// 	{
// 		g_pstModule[nModuleIndex].CommandAck.nCode = EP_TX_BUFF_OVER_FLOW;
// 		return EP_TX_BUFF_OVER_FLOW;
// 	}
// 
// //	m_worker.m_TxBuffCritic.Lock();						//Tx Buffer Sync.
// 
// 	LPEP_MSG_HEADER pMsgHeader;
// 	pMsgHeader = (LPEP_MSG_HEADER)g_pstModule[nModuleIndex].szTxBuffer;
// 	ASSERT(pMsgHeader);
// 	pMsgHeader->nID = EP_ID_FORM_OP_SYSTEM;
// 	pMsgHeader->wGroupNum = (WORD)1;			//0:module, 1~:Board
// 	if(nChannelNo > 0)
// 		pMsgHeader->wChannelNum = (WORD)EPGetHWChIndex(nChannelNo-1);		//0:Board,  1~:Channel
//     else
// 		pMsgHeader->wChannelNum = 0;
// 	pMsgHeader->nCommand = nCmd;
//     pMsgHeader->nLength = nPacketSize;
// 
// 	if(EPGetModuleState(nModuleID) == EP_STATE_LINE_OFF)
// 	{
// 		nErrorNumber = EP_ERR_LINE_OFF;
// 		return EP_FAIL;
// 	}
// 
// 	BOOL _error = FALSE;
// 	if(g_pSBCServer)
// 		_error = g_pSBCServer->SavePacket(nModuleID, pMsgHeader, (BYTE*)lpData, nBodySize);
// 
// 	//::SetEvent(g_pstModule[nModuleIndex].m_hWriteEvent);
// 	return _error;//ReadAck(nModuleIndex);
// }

/*
EPDLL_API BOOL EPSendCommand(int nModuleID)
{
	BOOL _error = FALSE;
	if(g_pSBCServer)
		_error = g_pSBCServer->SendCommand(nModuleID);

	return _error;
}*/

//Send Command to Module and Read response 
//nGroupNo and nChannelNo is one base Index;
//nGroup == 0 => Modlule Command
//nChannelNo == 0 => Group Command



EPDLL_API int EPJustSendCommand(int nModuleID, int /*nGroupNo*/, int nChannelNo, UINT nCmd, LPVOID lpData, UINT nSize)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if(m_Initialized == FALSE)
	{
		nErrorNumber = EP_ERR_NOT_INITIALIZED;
		return EP_FAIL;
	}

	if(nSize >0)
	{
		if(lpData == NULL)
		{
			nErrorNumber = EP_ERR_INVALID_ARGUMENT;
			return EP_FAIL;
		}
	}

	int nModuleIndex = EPGetModuleIndex(nModuleID);
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return EP_FAIL;
	}

	if(nChannelNo < 0 || nChannelNo > g_pstModule[nModuleIndex].sysData.nTotalChNo)
	{
		nErrorNumber = EP_ERR_CHANNEL_NOT_EXIST;
		return EP_FAIL;
	}

	UINT nBodySize = nSize;
	int nPacketSize = nBodySize + SizeofHeader();		//Total Frame Size
	if(nPacketSize >= _EP_TX_BUF_LEN)
	{
		g_pstModule[nModuleIndex].CommandAck.nCode = EP_TX_BUFF_OVER_FLOW;
		return EP_TX_BUFF_OVER_FLOW;
	}

	LPEP_MSG_HEADER pMsgHeader;
	pMsgHeader = (LPEP_MSG_HEADER)g_pstModule[nModuleIndex].szTxBuffer;
	ASSERT(pMsgHeader);
	pMsgHeader->nID = EP_ID_FORM_OP_SYSTEM;
	pMsgHeader->wGroupNum = (WORD)1;			//0:module, 1~:Board
	if(nChannelNo > 0)
		pMsgHeader->wChannelNum = (WORD)EPGetHWChIndex(nChannelNo-1);		//0:Board,  1~:Channel
	else
		pMsgHeader->wChannelNum = 0;
	pMsgHeader->nCommand = nCmd;
	pMsgHeader->nLength = nPacketSize + 2;		// CRC16 Add

	if(nBodySize>0)		
		memcpy((char *)g_pstModule[nModuleIndex].szTxBuffer + SizeofHeader(), lpData, nBodySize);

	g_pstModule[nModuleIndex].nTxLength = nPacketSize;
	//	m_worker.m_TxBuffCritic.Unlock();

	//	TRACE("Set Write Socket Event : 0x%08x\n", pMsgHeader->nCommand);
	if(EPGetModuleState(nModuleID) == EP_STATE_LINE_OFF)
	{
		nErrorNumber = EP_ERR_LINE_OFF;
		return EP_FAIL;
	}

	BOOL bRtn = FALSE;
	int nAck = EP_FAIL;

	bRtn = ::ResetEvent(g_pstModule[nModuleIndex].m_hReadEvent);
	if( bRtn == FALSE )
	{
		char msg[512];
		sprintf(msg, "Module %d :: ResetEvent failed(%d)\n", g_pstModule[nModuleIndex].sysParam.nModuleID, GetLastError());
		WriteLog(msg);
	}	

	bRtn = ::SetEvent(g_pstModule[nModuleIndex].m_hWriteEvent);
	if( bRtn == FALSE )
	{
		nAck =  EP_NACK;
	}
	else
	{	
// 		if (::WaitForSingleObject(g_pstModule[nModuleIndex].m_hReadEvent, /*INFINITE*/_EP_MSG_TIMEOUT) == WAIT_OBJECT_0 )
// 		{
// 			nAck = g_pstModule[nModuleIndex].CommandAck.nCode;
// 		}
// 		else
// 		{
// 			nAck =  EP_TIMEOUT;
// 		}
	}

	bRtn = ::ResetEvent(g_pstModule[nModuleIndex].m_hReadEvent);
	if( bRtn == FALSE )
	{
		char msg[512];
		sprintf(msg, "Module %d :: ResetEvent failed(%d)\n", g_pstModule[nModuleIndex].sysParam.nModuleID, GetLastError());
		WriteLog(msg);
	}	

	g_pstModule[nModuleIndex].CommandAck.nCmd = 0;
	g_pstModule[nModuleIndex].CommandAck.nCode = EP_NACK;

	return nAck;
}

EPDLL_API int EPSendCommand(int nModuleID, int /*nGroupNo*/, int nChannelNo, UINT nCmd, LPVOID lpData, UINT nSize)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	
	if(m_Initialized == FALSE)
	{
		nErrorNumber = EP_ERR_NOT_INITIALIZED;
		return EP_FAIL;
	}

	if(nSize >0)
	{
		if(lpData == NULL)
		{
			nErrorNumber = EP_ERR_INVALID_ARGUMENT;
			return EP_FAIL;
		}
	}

	int nModuleIndex = EPGetModuleIndex(nModuleID);
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return EP_FAIL;
	}

	if(nChannelNo < 0 || nChannelNo > g_pstModule[nModuleIndex].sysData.nTotalChNo)
	{
		nErrorNumber = EP_ERR_CHANNEL_NOT_EXIST;
		return EP_FAIL;
	}

	UINT nBodySize = nSize;
	int nPacketSize = nBodySize + SizeofHeader();		//Total Frame Size
	if(nPacketSize >= _EP_TX_BUF_LEN)
	{
		g_pstModule[nModuleIndex].CommandAck.nCode = EP_TX_BUFF_OVER_FLOW;
		return EP_TX_BUFF_OVER_FLOW;
	}

	LPEP_MSG_HEADER pMsgHeader;
	pMsgHeader = (LPEP_MSG_HEADER)g_pstModule[nModuleIndex].szTxBuffer;
	ASSERT(pMsgHeader);
	pMsgHeader->nID = EP_ID_FORM_OP_SYSTEM;
	pMsgHeader->wGroupNum = (WORD)1;			//0:module, 1~:Board
	if(nChannelNo > 0)
		pMsgHeader->wChannelNum = (WORD)EPGetHWChIndex(nChannelNo-1);		//0:Board,  1~:Channel
    else
		pMsgHeader->wChannelNum = 0;
	pMsgHeader->nCommand = nCmd;
    pMsgHeader->nLength = nPacketSize + 2;		// CRC16 Add

	if(nBodySize>0)		
		memcpy((char *)g_pstModule[nModuleIndex].szTxBuffer + SizeofHeader(), lpData, nBodySize);

	g_pstModule[nModuleIndex].nTxLength = nPacketSize;
//	m_worker.m_TxBuffCritic.Unlock();

//	TRACE("Set Write Socket Event : 0x%08x\n", pMsgHeader->nCommand);
	if(EPGetModuleState(nModuleID) == EP_STATE_LINE_OFF)
	{
		nErrorNumber = EP_ERR_LINE_OFF;
		return EP_FAIL;
	}

	BOOL bRtn = FALSE;
	int nAck = EP_FAIL;

	if( !::ResetEvent(g_pstModule[nModuleIndex].m_hReadEvent) )
	{
		char msg[512];
		sprintf(msg, "Module %d :: ResetEvent failed(%d)\n", g_pstModule[nModuleIndex].sysParam.nModuleID, GetLastError());
		WriteLog(msg);
	}
	

	bRtn = ::SetEvent(g_pstModule[nModuleIndex].m_hWriteEvent);
	if( bRtn == FALSE )
	{
		nAck =  EP_NACK;
	}
	else
	{	
		if (::WaitForSingleObject(g_pstModule[nModuleIndex].m_hReadEvent, /*INFINITE*/_EP_MSG_TIMEOUT) == WAIT_OBJECT_0 )
		{
			nAck = g_pstModule[nModuleIndex].CommandAck.nCode;
		}
		else
		{
			nAck =  EP_TIMEOUT;
		}
	}
	
	if( !::ResetEvent(g_pstModule[nModuleIndex].m_hReadEvent) )
	{
		char msg[512];
		sprintf(msg, "Module %d :: ResetEvent failed(%d)\n", g_pstModule[nModuleIndex].sysParam.nModuleID, GetLastError());
		WriteLog(msg);
	}

	g_pstModule[nModuleIndex].CommandAck.nCmd = 0;
	g_pstModule[nModuleIndex].CommandAck.nCode = EP_NACK;
		
	return nAck;
}

EPDLL_API void EPSetLogFileName(char *szFileName)
{
	if(szFileName == NULL)	return;
	strcpy(szLogFileName, szFileName);
}

EPDLL_API int EPGetInstalledBoard(int nModuleID)
{
	int nModuleIndex = EPGetModuleIndex(nModuleID);
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return 0;
	}
	
	return g_pstModule[nModuleIndex].sysData.wInstalledBoard;
}

EPDLL_API int EPGetChPerBoard(int nModuleID)
{
	int nModuleIndex = EPGetModuleIndex(nModuleID);
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return 0;
	}
	return g_pstModule[nModuleIndex].sysData.wChannelPerBoard;
}

//Module Version
EPDLL_API UINT EPGetModuleVersion(int nModuleID)
{
	int nModuleIndex = EPGetModuleIndex(nModuleID);
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return 0;
	}
	return g_pstModule[nModuleIndex].sysData.nVersion;
}

// 
EPDLL_API UINT EPInstledlTotalGroup()
{
	int count = 0;
	for(int i =0; i<m_nInstalledModuleNum; i++)
	{
		count += 1;
	}
	return count;
}

//Setting Current Working Type (Auto /Normal)
EPDLL_API BOOL	EPSetAutoProcess(int nModuleID, BOOL bEnable)
{
	int nModuleIndex = EPGetModuleIndex(nModuleID);
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return FALSE;
	}
	g_pstModule[nModuleIndex].sysParam.bAutoProcess = bEnable;
	return TRUE;	
}

//Current Working Type (Auto/Manual)
EPDLL_API BOOL	EPGetAutoProcess(int nModuleID)
{
	int nModuleIndex = EPGetModuleIndex(nModuleID);
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return FALSE;
	}
	return g_pstModule[nModuleIndex].sysParam.bAutoProcess;
}

//Group Fail Code
EPDLL_API WORD EPGetFailCode(int nModuleID, int /*nGroupIndex*/)
{
	EP_GP_DATA gpData = EPGetGroupData(nModuleID, 0);
	return gpData.gpState.failCode;
}

//Setting Monitoring Channel Mapping Type
EPDLL_API void EPSetChIndexType(int nType)
{
	if(nType > 0)	
		g_nChannelIndexType = nType;
}

// Convert Monitoring Channel to Hardware Channel
// input  UINT : Monitoring Channel Index
// return UINT : Hardware Channel
EPDLL_API UINT EPGetHWChIndex(UINT nIndex)
{
	if(g_nChannelIndexType == EP_CH_INDEX_MAPPING)		
	{
		for(UINT i =0; i<EP_MAX_CH_PER_MD; i++)
		{
			if((UINT)g_channelMap[i] == nIndex)
			{
				return i;
			}
		}
	}
	return nIndex;
}

//Command Fail Message
EPDLL_API CString EPCmdFailMsg(int nCode)
{
	//2004/3/25 KBH
	//EMG Code 사용 재정의 
	
	//0x0000 0000
	//상위 2Byte는 EMG Code Flag => EMG Table에서 Code 정보 Read 
	//하위 2Byte는 code Defile 정의 

	CString strMsg;
	switch(nCode)
	{
	case EP_ACK:				strMsg = "정상";					break;	//0x00000001
	case EP_TIMEOUT:			strMsg = "응답 없음";				break;	//0x00000002
	case EP_SIZE_MISMATCH:		strMsg = "Data Size 오류";			break;	//0x00000003
	case EP_RX_BUFF_OVER_FLOW:	strMsg = "Rx Buffer Over Flow";		break;	//0x00000004
	case EP_TX_BUFF_OVER_FLOW:	strMsg = "Tx Buffer Over Flow";		break;	//0x00000005
	case EP_FAIL:				strMsg.Format("Fail Code %d", EPGetLastError());	break;

	case 0x011:		strMsg = "정의되지 않은 명령";					break;
	case 0x012:		strMsg = "장비 ID가 맞지 않음";					break;
	case 0x013:		strMsg = "알수 없는 Code";						break;
	case 0x014:		strMsg = "Group ID 오류";						break;
	case 0x015:		strMsg = "알수 없는 Step Type";					break;
	case 0x016:		strMsg = "공정 정보가 전송되지 않았음";			break;
	case 0x017:		strMsg = "비교 Flag Disable";					break;

	case 0x0100:	strMsg = "Vacancy 상태가 아님";					break;
	case 0x0101:	strMsg = "Standby나 End 상태가 아님";			break;
	case 0x0102:	strMsg = "Run이나 Pause 상태가 아님 ";			break;
	case 0x0103:	strMsg = "Run 상태가 아님";						break;
	case 0x0104:	strMsg = "Pause 상태가 아님";					break;
	case 0x0105:	strMsg = "모두 Fault 상태임";					break;
	case 0x0106:	strMsg = "전송 가능한 상태가 아님(Vacancy, Standby)";	break;	
	case 0x0107:	strMsg = "전송 가능한 상태가 아님(Vacancy, Standby, Run)";		break;
	case 0x0108:	strMsg = "전송 가능한 상태가 아님(Standby, End, Run)";		break;
	case 0x0109:	strMsg = "전송 가능한 상태가 아님(Standby or Run)";			break;
	case 0x010A:	strMsg = "상태전이 중";							break;

	case 0x0200:	strMsg = "Channel이  Run 상태가 아님";			break;

	case 0x0300:	strMsg = "Jig가 준비 되지 않았음";				break;
	case 0x0301:	strMsg = "Local Mode로 설정 되어 있음";			break;
	case 0x0302:	strMsg = "Door Open";							break;
	case 0x0303:	strMsg = "자동공정이 설정되어 있지 않음";		break;
	case 0x0304:	strMsg = "Jig의 Cell 높이 설정이 입력 Tray와 맞지 않음";	break;



	default:	strMsg.Format("Code %d", nCode);	break;
	}
	return strMsg;
}

//2003/1/14
EPDLL_API int EPDllGetVersion()
{
	return _EP_FORM_VERSION;
}

EPDLL_API BOOL EPSetAutoReport(int nModuleID, int /*nGroupNo*/, UINT nInterval, UINT nSaveInterval)
{
	int nModuleIndex = EPGetModuleIndex(nModuleID);

	if(ModuleIndexCheck(nModuleIndex) == FALSE)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return FALSE;
	}

/*	if(GroupIndexCheck(nModuleIndex, nGroupIndex) == FALSE)
	{
		nErrorNumber = EP_ERR_GROUP_NOT_EXIST;
		return FALSE;
	}
*/
/*	if(m_Initialized && g_pstModule[nModuleIndex].sysData.nVersion <= 0x3000)
	{
		TRACE("Module %d는 자동 보고 Setting 명령을 지원하지 않음(0x%x)\n", nModuleID,  g_pstModule[nModuleIndex].sysData.nVersion);
		return FALSE;
	}
*/
	EP_AUTO_REPORT_SET autoSet;
	ZeroMemory(&autoSet, sizeof(autoSet));

/*	EP_SYSTEM_PARAM *pParam = EPGetSysParam(nModuleID);

	if(pParam == NULL)
	{
		autoSet.nInterval = EP_AUTO_REPORT_INTERVAL;
	}
	else
	{
		autoSet.nInterval = pParam->nAutoReportInterval;
	}
*/
	autoSet.nInterval = nInterval;
	autoSet.nSaveInterval = nSaveInterval;
	int nRtn;
	// if(EP_ACK != (nRtn = EPSaveCommand(nModuleID, 0, 0, EP_CMD_SET_AUTO_REPORT, &autoSet, sizeof(EP_AUTO_REPORT_SET))))
	if(EP_ACK != (nRtn = EPSendCommand(nModuleID, 0, 0, EP_CMD_SET_AUTO_REPORT, &autoSet, sizeof(EP_AUTO_REPORT_SET))))	
	{
		TRACE("Auto Report Interval Setting Fail %d\n", nRtn);
		return FALSE;
	}
	
	// EPSendCommand(nModuleID);

	TRACE("Send Auto Report Interval Setting %d\n", nInterval);
	return TRUE;
}



EPDLL_API BOOL EPSetOperationMode(int nModuleID, int /*nGroupNo*/, int nModeData)
{
	int nModuleIndex = EPGetModuleIndex(nModuleID);

	if(ModuleIndexCheck(nModuleIndex) == FALSE)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return FALSE;
	}
	
	g_pstModule[nModuleIndex].nOperationMode = nModeData;

	return TRUE;
}


//2003/1/14
EPDLL_API BOOL EPSetLineMode(int nModuleID, int /*nGroupNo*/, int nModeType, int nModeData)
{
	int nModuleIndex = EPGetModuleIndex(nModuleID);

	if(ModuleIndexCheck(nModuleIndex) == FALSE)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return FALSE;
	}

	EP_LINE_MODE_SET lineSet;
	ZeroMemory(&lineSet, sizeof(lineSet));

	switch( nModeType )
	{
	case EP_WORKTYPE_MODE:
		{
			lineSet.nWorkType = nModeData;
			lineSet.nOperationMode = g_pstModule[nModuleIndex].nOperationMode;
			lineSet.nOnLineMode = g_pstModule[nModuleIndex].nLineMode;
		}
		break;

	case EP_LINE_MODE:
		{
			lineSet.nOnLineMode = nModeData;
			lineSet.nOperationMode = g_pstModule[nModuleIndex].nOperationMode;
			lineSet.nWorkType = g_pstModule[nModuleIndex].nWorkType;
		}
		break;

	case EP_OPERATION_MODE:
		{
			lineSet.nOperationMode = nModeData;
			lineSet.nWorkType = g_pstModule[nModuleIndex].nWorkType;			
			lineSet.nOnLineMode = g_pstModule[nModuleIndex].nLineMode;
		}
		break;
	}	
	
	// 1. Ack 메세지를 받으면 Local 의 상태 정보를 갱신한다.
	//g_pstModule[nModuleIndex].poLineModeSet.nOperationMode = lineSet.nOperationMode;
	//g_pstModule[nModuleIndex].poLineModeSet.nWorkType = lineSet.nWorkType;
	//g_pstModule[nModuleIndex].poLineModeSet.nOnLineMode = lineSet.nOnLineMode;

	if(EP_ACK != EPSendCommand(nModuleID, 0, 0, EP_CMD_SET_LINE_MODE, &lineSet, sizeof(lineSet)))
	{
		return FALSE;
	}
	
	// EPSendCommand(nModuleID);
	
	/*
	switch( nModeType )
	{
	case EP_LINE_MODE:
		{
			g_pstModule[nModuleIndex].nLineMode = nModeData;
		}
		break;
	case EP_OPERATION_MODE:
		{
			g_pstModule[nModuleIndex].nOperationMode = nModeData;
		}
		break;	
	case EP_WORKTYPE_MODE:
		{
			g_pstModule[nModuleIndex].nWorkType = nModeData;
		}
		break;
	}
	*/

	return TRUE;
}

// 2003/1/14
// 2013/06/24
EPDLL_API BOOL EPGetLineMode(int nModuleID, int /*nGroupNo*/, int &nOnLineMode, int &nOperationMode, int &nCellTypeMode )
{
	int nModuleIndex = EPGetModuleIndex(nModuleID);

	if(ModuleIndexCheck(nModuleIndex) == FALSE)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return FALSE;
	}

	nOperationMode = g_pstModule[nModuleIndex].nOperationMode;
	nOnLineMode = g_pstModule[nModuleIndex].nLineMode;
	nCellTypeMode = g_pstModule[nModuleIndex].nWorkType;
	return TRUE;
}

//2007/6/7
EPDLL_API BOOL EPUpdateModuleLineMode(int nModuleID)
{	
	int nModuleIndex = EPGetModuleIndex(nModuleID);

	if(ModuleIndexCheck(nModuleIndex) == FALSE)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return FALSE;
	}
	
	EP_LINE_MODE_SET lineSet;
	ZeroMemory(&lineSet, sizeof(lineSet));
	int nSize = sizeof(EP_LINE_MODE_SET);
	LPVOID lpData = EPSendDataCmd(nModuleID, 0, 0, EP_CMD_LINE_MODE_REQUEST, nSize);
	if(lpData == NULL)	
	{
		return FALSE;
	}
	else
	{
		memcpy(&lineSet, lpData, sizeof(lineSet));
		g_pstModule[nModuleIndex].nOperationMode = lineSet.nOperationMode;
		
		char szLog[128];
		if( lineSet.nOperationMode == EP_OPERATION_AUTO )
		{
			sprintf(szLog, "Module %d operation mode [AUTO] received!", nModuleID );		
		}
		else
		{
			sprintf(szLog, "Module %d operation mode [LOCAL] received!", nModuleID );				
		}		
		WriteLog(szLog);
	}
	return TRUE;	
}


EPDLL_API UINT EPGetProtocolVer(int nModuleID)
{
	int nModuleIndex = EPGetModuleIndex(nModuleID);

	if(ModuleIndexCheck(nModuleIndex) == FALSE)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return 0;
	}
	if(m_Initialized == FALSE)	return 0;

	return g_pstModule[nModuleIndex].sysData.nVersion;
}

//Send Command to Module and Read response Data
EPDLL_API int EPSendResponse(int nModuleID, int nCmd, int nCode)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	
	if(m_Initialized == FALSE)
	{
		nErrorNumber = EP_ERR_NOT_INITIALIZED;
		return EP_FAIL;
	}

	int nModuleIndex = EPGetModuleIndex(nModuleID);
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return EP_FAIL;
	}

	LPEP_MSG_HEADER pMsgHeader;
	pMsgHeader = (LPEP_MSG_HEADER)g_pstModule[nModuleIndex].szTxBuffer;
	ASSERT(pMsgHeader);
	pMsgHeader->nID = EP_ID_FORM_OP_SYSTEM;
	pMsgHeader->wGroupNum = 0;			//0:module, 1~:Board
	pMsgHeader->wChannelNum = 0;
	pMsgHeader->nCommand = EP_CMD_RESPONSE;
    pMsgHeader->nLength = SizeofHeader()+sizeof(EP_RESPONSE);

	EP_RESPONSE sResponse;
	::ZeroMemory(&sResponse, sizeof(EP_RESPONSE));
	sResponse.nCmd = nCmd;
	sResponse.nCode = nCode;

	memcpy((char *)g_pstModule[nModuleIndex].szTxBuffer + SizeofHeader(), &sResponse, pMsgHeader->nLength);

	g_pstModule[nModuleIndex].nTxLength = pMsgHeader->nLength;
//	m_worker.m_TxBuffCritic.Unlock();

//	TRACE("Set Write Socket Event : 0x%08x\n", pMsgHeader->nCommand);
	if(EPGetModuleState(nModuleID) == EP_STATE_LINE_OFF)
	{
		nErrorNumber = EP_ERR_LINE_OFF;
		return EP_FAIL;
	}

	::SetEvent(g_pstModule[nModuleIndex].m_hWriteEvent);
	return EP_ACK;
}

EPDLL_API BOOL EPQueueDataPop(SBCQueue_DATA& _data)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());	

	g_Que.Pop(_data);

	return TRUE; 
}

EPDLL_API BOOL EPDlgQueueDataIsEmpty()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	return g_DlgQue.GetIsEmpty();
}

EPDLL_API BOOL EPDlgQueueDataPop(SBCQueue_DATA& _data)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	g_DlgQue.Pop(_data);

	return TRUE; 
}

EPDLL_API BOOL EPQueueDataIsEmpty()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	return g_Que.GetIsEmpty();
}

EPDLL_API BOOL EPNewServerStart(HWND papa)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	g_pSBCServer = new CSBCServer();

	return g_pSBCServer->NetBegin(papa);
}

EPDLL_API BOOL EPNewServerStop()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	g_pSBCServer->NetEnd();

	delete g_pSBCServer;

	return TRUE;
}

EPDLL_API BOOL EPTestSetLineMode(int nModuleID, int /*nGroupNo*/, int nModeType, int nModeData)
{
	int nModuleIndex = EPGetModuleIndex(nModuleID);

	if(ModuleIndexCheck(nModuleIndex) == FALSE)
	{
		nErrorNumber = EP_ERR_MODULE_NOT_EXIST;
		return FALSE;
	}

	EP_LINE_MODE_SET lineSet;
	ZeroMemory(&lineSet, sizeof(lineSet));

	switch( nModeType )
	{
	case EP_WORKTYPE_MODE:
		{
			lineSet.nWorkType = nModeData;
			lineSet.nOperationMode = g_pstModule[nModuleIndex].nOperationMode;
			lineSet.nOnLineMode = g_pstModule[nModuleIndex].nLineMode;
		}
		break;

	case EP_LINE_MODE:
		{
			lineSet.nOnLineMode = nModeData;
			lineSet.nOperationMode = g_pstModule[nModuleIndex].nOperationMode;
			lineSet.nWorkType = g_pstModule[nModuleIndex].nWorkType;
		}
		break;

	case EP_OPERATION_MODE:
		{
			lineSet.nOperationMode = nModeData;
			lineSet.nWorkType = g_pstModule[nModuleIndex].nWorkType;			
			lineSet.nOnLineMode = g_pstModule[nModuleIndex].nLineMode;
		}
		break;
	}	
	
	// 1. Ack 메세지를 받으면 Local 의 상태 정보를 갱신한다.
	g_pstModule[nModuleIndex].nOperationMode = lineSet.nOperationMode;
	g_pstModule[nModuleIndex].nWorkType = lineSet.nWorkType;
	g_pstModule[nModuleIndex].nLineMode = lineSet.nOnLineMode;

	return TRUE;
}
EPDLL_API BOOL EPTestSetState(int nModuleID, WORD _sate)
{
	int nModuleIndex = EPGetModuleIndex(nModuleID);
	g_pstModule[nModuleIndex].state = _sate;


	LPEP_GROUP_INFO lpGroup;
	lpGroup = (EP_GROUP_INFO *)g_pstModule[nModuleIndex].gpData[0];
	ASSERT(lpGroup);
	lpGroup->chData;


	LPEP_CH_DATA pChannel;

	for(INDEX j =0; j<EP_DEFAULT_CH_PER_MD; j++)
	{
		pChannel = new EP_CH_DATA;
		ZeroMemory(pChannel, sizeof(EP_CH_DATA));
		lpGroup->chData.Add(pChannel);

		EP_REAL_TIME_DATA *pRltData = new EP_REAL_TIME_DATA;
		ZeroMemory(pRltData, sizeof(EP_REAL_TIME_DATA));
		lpGroup->chRealData.Add(pRltData);
	}



	return TRUE;
}