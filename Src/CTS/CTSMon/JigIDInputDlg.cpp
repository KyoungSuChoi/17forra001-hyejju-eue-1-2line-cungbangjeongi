// JigIDInputDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ctsmon.h"
#include "JigIDInputDlg.h"
#include "JigIDRegDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CJigIDInputDlg dialog


CJigIDInputDlg::CJigIDInputDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CJigIDInputDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CJigIDInputDlg)
	m_strJigID = _T("");
	//}}AFX_DATA_INIT
	LanguageinitMonConfig();
}



CJigIDInputDlg::~CJigIDInputDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

bool CJigIDInputDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CJigIDInputDlg"), _T("TEXT_CJigIDInputDlg_CNT"), _T("TEXT_CJigIDInputDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CJigIDInputDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CJigIDInputDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

void CJigIDInputDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CJigIDInputDlg)
	DDX_Control(pDX, IDC_COMBO1, m_ctrlJigCombo);
	DDX_Text(pDX, IDC_EDIT1, m_strJigID);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CJigIDInputDlg, CDialog)
	//{{AFX_MSG_MAP(CJigIDInputDlg)
	ON_BN_CLICKED(IDC_RADIO2, OnRadio2)
	ON_BN_CLICKED(IDC_RADIO1, OnRadio1)
	ON_CBN_SELCHANGE(IDC_COMBO1, OnSelchangeCombo1)
	//}}AFX_MSG_MAP
	ON_MESSAGE(EPWM_BCR_SCANED, OnBcrscaned)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CJigIDInputDlg message handlers

BOOL CJigIDInputDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	int nInstallMD = EPGetInstalledModuleNum();
	int nInstallJig = 8;

	CString str;
	int nCnt = 0;
	int nDefaultIndex = -1;

	for(int i=0; i<nInstallMD; i++)
	{
		int nModuleID = EPGetModuleID(i);
		for(int j=0; j<nInstallJig; j++)
		{
			int nJigID = j+1;
			str.Format("Module %d - Jig %d", i+1, j+1);
			m_ctrlJigCombo.AddString(str);
			
			//default selection
/*			if(nModuleID == m_nSelModuleID && nJigID == m_nSelJigNo)
			{
				nDefaultIndex = nCnt;
			}
*/			long lID = MAKELONG(nJigID, nModuleID);
			m_ctrlJigCombo.SetItemData(nCnt++, lID);
		}
	}
	m_ctrlJigCombo.SetCurSel(nDefaultIndex);
	
	((CButton *)GetDlgItem(IDC_RADIO2))->SetCheck(TRUE);
	OnRadio2();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

CString CJigIDInputDlg::GetJigID()
{
	return m_strJigID;
}

void CJigIDInputDlg::OnRadio2() 
{
	// TODO: Add your control notification handler code here
	GetDlgItem(IDC_COMBO1)->EnableWindow(TRUE);
	GetDlgItem(IDC_EDIT1)->EnableWindow(FALSE);
}

void CJigIDInputDlg::OnRadio1() 
{
	// TODO: Add your control notification handler code here
	GetDlgItem(IDC_COMBO1)->EnableWindow(FALSE);
	GetDlgItem(IDC_EDIT1)->EnableWindow(TRUE);	
}

void CJigIDInputDlg::OnSelchangeCombo1() 
{
	// TODO: Add your control notification handler code here
	int nSel = m_ctrlJigCombo.GetCurSel();
	if(nSel != CB_ERR)
	{
		int nID = m_ctrlJigCombo.GetItemData(nSel);
		int nMD = HIWORD(nID);
		int nJig = LOWORD(nID);

		CJigIDRegDlg *pDlg = new CJigIDRegDlg(this);
		GetDlgItem(IDC_EDIT1)->SetWindowText(pDlg->SearchLocation(nMD, nJig));
		delete pDlg;
	}	
}

void CJigIDInputDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData();

/*	CJigIDRegDlg *pDlg = new CJigIDRegDlg(this);
	int nModuleID, nJigNo; 
	if(pDlg->SearchLocation(str, nModuleID, nJigNo) == FALSE)
	{

	}
*/
	CDialog::OnOK();
}


LRESULT CJigIDInputDlg::OnBcrscaned(UINT wParam, LONG lParam)
{
	char *data = (char *)lParam;
	CString str(data);
	
	CJigIDRegDlg *pDlg = new CJigIDRegDlg(this);
	int nModuleID, nJigNo; 
	if(pDlg->SearchLocation(str, nModuleID, nJigNo))
	{
		DWORD dwData = MAKELONG(nJigNo, nModuleID);

		for(int i=0; i<m_ctrlJigCombo.GetCount(); i++)
		{
			if(m_ctrlJigCombo.GetItemData(i) == dwData)
			{
				m_ctrlJigCombo.SetCurSel(i);
				break;
			}
		}
		m_strJigID = str;

		GetDlgItem(IDC_TITLE_STATIC)->SetWindowText(str+ TEXT_LANG[0]);//" 등록 정보를 찾았습니다."
	}
	else
	{
		m_ctrlJigCombo.SetCurSel(-1);
		m_strJigID.Empty();
		GetDlgItem(IDC_TITLE_STATIC)->SetWindowText(str+ TEXT_LANG[1]);//" 등록 정보를 찾을 수 없습니다."
	}
	delete pDlg;
	
	UpdateData(FALSE);

	return 1;
}