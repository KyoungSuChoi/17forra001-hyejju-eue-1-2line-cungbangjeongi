// ResultLogRecordSet1.cpp : implementation file
//

#include "stdafx.h"
#include "CTSMon.h"
#include "ResultLogRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CResultLogRecordSet

IMPLEMENT_DYNAMIC(CResultLogRecordSet, CDaoRecordset)

CResultLogRecordSet::CResultLogRecordSet(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CResultLogRecordSet)
	m_TestIndex = 0;
	m_DateTime = (DATE)0;
	m_ProcedureID = 0;
	m_TestResultFileName = _T("");
	m_Average = 0.0f;
	m_MaxVal = 0.0f;
	m_MaxChIndex = 0;
	m_MinVal = 0.0f;
	m_MinChIndex = 0;
	m_STDD = 0.0f;
	m_NormalNo = 0;
	m_ErrorRate = 0.0f;
	m_nFields = 12;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString CResultLogRecordSet::GetDefaultDBName()
{
	return _T(GetLogDataBaseName());
}

CString CResultLogRecordSet::GetDefaultSQL()
{
	return _T("[Test]");
}

void CResultLogRecordSet::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CResultLogRecordSet)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Long(pFX, _T("[TestIndex]"), m_TestIndex);
	DFX_DateTime(pFX, _T("[DateTime]"), m_DateTime);
	DFX_Long(pFX, _T("[ProcedureID]"), m_ProcedureID);
	DFX_Text(pFX, _T("[TestResultFileName]"), m_TestResultFileName);
	DFX_Single(pFX, _T("[Average]"), m_Average);
	DFX_Single(pFX, _T("[MaxVal]"), m_MaxVal);
	DFX_Long(pFX, _T("[MaxChIndex]"), m_MaxChIndex);
	DFX_Single(pFX, _T("[MinVal]"), m_MinVal);
	DFX_Long(pFX, _T("[MinChIndex]"), m_MinChIndex);
	DFX_Single(pFX, _T("[STDD]"), m_STDD);
	DFX_Long(pFX, _T("[NormalNo]"), m_NormalNo);
	DFX_Single(pFX, _T("[ErrorRate]"), m_ErrorRate);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CResultLogRecordSet diagnostics

#ifdef _DEBUG
void CResultLogRecordSet::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CResultLogRecordSet::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
