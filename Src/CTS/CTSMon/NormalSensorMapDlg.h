#pragma once


// CNormalSensorMapDlg 대화 상자입니다.

class CNormalSensorMapDlg : public CDialog
{
	DECLARE_DYNAMIC(CNormalSensorMapDlg)

public:
	CNormalSensorMapDlg(int nDeviceID, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CNormalSensorMapDlg();

	int m_nDeviceID;

// 대화 상자 데이터입니다.
	enum { IDD = IDD_NORMAL_SENSOR_PIC_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	virtual BOOL OnInitDialog();
};
