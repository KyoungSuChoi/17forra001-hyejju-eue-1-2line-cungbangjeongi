#pragma once

#include "MySql.h"

//! 데이터베이스 쿼리 클래스
class PneApp
{
public:
    
	MySql m_MySql;
	CTime m_StartTime;

	bool m_bSendFlag;

	CStringList mesData;

	bool GetSendFlag() { return m_bSendFlag; }
	void SetSendFlag( bool bFlag ) { m_bSendFlag = bFlag; }

	void ReConnectTime();
	void ReConnectError();

	BOOL CreateTbCellTrayMap();
	BOOL InsertCellTrayMap( CString trayno, CString lotid, int nChannelnum, CString cellid );

	BOOL CreateTbTrayWorkinfo();
	BOOL InsertTrayWorkinfo(CString trayno, CString lotid, int nTypeId, int nProcessId);
	BOOL UpdateTrayWorkinfo(CString strTrayNo, int nProcessID, int nWorkingSeq=0);

	BOOL IsMySqlLive();

	MYSQLINFO* GetTrayMappingInfo(CString strTrayNo);
	MYSQLINFO* GetTrayProcessInfo(CString strTrayNo);

	BOOL SetClearTrayMainppingInfo(CString strTrayNo);

	BOOL GetCellTrayMappingInfo(CString strCellNo, CString lotid, CString &strTrayNo);
	BOOL GetWorkinfoTbIndex( CString &strTbName, CString &strIdx );

	CString Time_Conversion(CString str_Time);

	/*
	BOOL InsertWorkinfo(CString deviceid,CString blockid,CString lotid,CString cellid,
						CString nChannel,CString moduleid,CString channelidx,
						CString nmmodule,CString nowdt,CString nmwork,
						CString worker,CString ip,CString schednm,CString schedfile);	
	
	MYSQLINFO* GetWorkInfo(CString dtstart,CString dtend,CString stat);
	
	BOOL SetStateInsert(CTypedPtrList<CPtrList, CHINFO*> &ChList);
	BOOL SetMonitoring(CHINFO *chinfo);

	

	


	
	BOOL InsertWorkinfo(PS_TEST_RESULT_FILE_HEADER *pHeader,CString ip,
					    CString nmdir,CString fileTitle,CString nmwork,CString nmnode,
		                CString deviceid,CString blockid,
					    CString channelid,CString &tbraw,CString &tbsrc);

	BOOL CreateTbRaw(CString table);
	BOOL CreateTbSrc(CString table);
	BOOL CreateTbMonitor();

	BOOL InsertWorkevent(CString deviceid,CString blockid,CString lotid,CString cellid,
	CString channel, CString moduleid,CString channelidx,
	CString sampleno,CString nmwork,CString tpstate);

	BOOL InsertWorkstate(CString deviceid,CString blockid,CString lotid,CString cellid,
	CString channel, CString moduleid,CString channelidx,
	CString tpstate);	


	BOOL InsertRaw(PS_STEP_END_RECORD *pRecord,LONGLONG noidx,CString tbraw); 
	BOOL InsertSrc(PS_STEP_SRC_RECORD *pRecord,CString tbsrc);

	BOOL InsertRaw2(PS_STEP_END_RECORD2 *pRecord,LONGLONG noidx,CString tbraw);
	BOOL InsertSrc2(PS_STEP_SRC_RECORD2 *pRecord,CString tbsrc);

	void ListWorkInfo(CListCtrl *list,CString deviceid,CString blockid,CString nmwork,CString sstart,CString send,CString estart,CString eend);
	
	CStringArray* GetListTreeNode(CString strName);
	CStringArray* GetChData(CString nmwork,CString nmnode);
	CString       GetSchdData(CString nowork);
	CStringArray* GetRawData(CString tbraw);
	MYSQLINFO*    GetSrcData(CString tbsrc,CString snIndexFrom,CString snIndexTo);
	void          GetChList(CString sName,CListCtrl *list);
	void          GetSchedList(CString sName,CStringArray &DataList);
	CString       GetWorkChTable(CString strChPath,CString sfield);
	long          GetFirstTableIndexOfCycle(CString tbraw,LONG lCycleID); 
	MYSQLINFO*    GetTableCycleList(CString strChPath,LONG lCycleID);
	MYSQLINFO*    GetWorkChListTable(CString strTestName,CString strChName);
	MYSQLINFO*    GetSrcCycleData(CString tbsrc,CString sCycle);

	CString   GetRawIndexData(CStringArray *rawData,int nIdx,CString sfield);
	fltPoint* GetSrcDataList(CStringArray *rawlist,CString tbsrc,int nItem,LONG &nTimeDataCnt,CString strXAxisTitle, CString strYAxisTitle);
	fltPoint* GetSrcCycleDataList(CStringArray *rawlist,CString tbsrc,LONG nCycle,LONG &nTimeDataCnt,CString strXAxisTitle, CString strYAxisTitle);

	BOOL SetStateInsert(CTypedPtrList<CPtrList, CHINFO*> &ChList);
	BOOL SetMonitoring(CHINFO *chinfo);*/

public:
	PneApp(void);
	virtual ~PneApp(void);

};

extern PneApp mainPneApp;