#if !defined(AFX_SELRESULTDLG_H__A7C4759D_FD98_43A8_A643_E1E422037CB7__INCLUDED_)
#define AFX_SELRESULTDLG_H__A7C4759D_FD98_43A8_A643_E1E422037CB7__INCLUDED_

#include "FormationModule.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelResultDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSelResultDlg dialog

class CSelResultDlg : public CDialog
{
// Construction
public:
	CFormModule *m_pModuleInfo;
	int m_nTrayIndex;
	CSelResultDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSelResultDlg)
	enum { IDD = IDD_SEL_RLT_DIALOG };
	CListCtrl	m_wndSelList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelResultDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSelResultDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDblclkSelList(NMHDR* pNMHDR, LRESULT* pResult);
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELRESULTDLG_H__A7C4759D_FD98_43A8_A643_E1E422037CB7__INCLUDED_)
