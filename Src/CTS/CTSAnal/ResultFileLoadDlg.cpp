// ResultFileLoadDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "CTSAnalDoc.h"
#include "ResultFileLoadDlg.h"
#include "CpkSetDlg.h"
#include "CpkViewDlg.h"
#include "SerarchDataDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CResultFileLoadDlg dialog

typedef struct tagThreadParam {
	CMyGridWnd	*pGrid;
	char szFolderName[512];
	BOOL bStopFlag;
	BOOL bAllFile;	
} THREAD_PARAM;

UINT	LoadFileList(LPVOID lParam);
volatile BOOL g_bFileLoadThreadRun = FALSE;

bool CResultFileLoadDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CResultFileLoadDlg"), _T("TEXT_CResultFileLoadDlg_CNT"), _T("TEXT_CResultFileLoadDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CResultFileLoadDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CResultFileLoadDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

CResultFileLoadDlg::CResultFileLoadDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CResultFileLoadDlg::IDD, pParent)
{
	LanguageinitMonConfig();

	//{{AFX_DATA_INIT(CResultFileLoadDlg)
	m_strFileFolder = _T("");
	m_strLotFilter = _T("");
	m_strTrayFilter = _T("");
	m_bTodayWork = FALSE;
	m_fromTime = CTime::GetCurrentTime();
	m_toTime = CTime::GetCurrentTime();
	//}}AFX_DATA_INIT
//	m_hThread = NULL;
	m_pThread = NULL;
	m_bStopThreadFlag = FALSE;
	m_pDoc = NULL;
}

CResultFileLoadDlg::~CResultFileLoadDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


void CResultFileLoadDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CResultFileLoadDlg)
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDC_REFRESH, m_btnRefresh);
	DDX_Control(pDX, IDC_FOLDER_SEL, m_btnFolderSel);
	DDX_Control(pDX, IDC_DELETE_FILE, m_btnDelFile);	
	DDX_Control(pDX, IDC_BUTTON_CPCPK, m_btnCpCpk);
	DDX_Control(pDX, IDC_BUTTON_FAIL, m_btnAllDataList);
	DDX_Control(pDX, IDC_TOTAL_FILE_NO, m_totalFileCount);
	DDX_Text(pDX, IDC_LOT_FILTER, m_strLotFilter);
	DDX_Text(pDX, IDC_TRAY_FILTER, m_strTrayFilter);
	DDX_Check(pDX, IDC_CHECK_TODAY, m_bTodayWork);
	DDX_DateTimeCtrl(pDX, IDC_DATETIMEPICKER1, m_fromTime);
	DDX_Text(pDX, IDC_CUR_FOLDER, m_strSelFileName);
	DDX_DateTimeCtrl(pDX, IDC_DATETIMEPICKER3, m_toTime);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CResultFileLoadDlg, CDialog)
	//{{AFX_MSG_MAP(CResultFileLoadDlg)
	ON_BN_CLICKED(IDC_REFRESH, OnRefresh)
	ON_BN_CLICKED(IDC_DELETE_FILE, OnDeleteFile)
	ON_BN_CLICKED(IDC_FOLDER_SEL, OnFolderSel)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_COMMAND(ID_RELOAD, OnReload)
	ON_COMMAND(ID_SELECT_ALL, OnSelectAll)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_COMMAND(ID_DELETE, OnDelete)
	ON_BN_CLICKED(IDC_BUTTON1, OnToDayButton)
	ON_BN_CLICKED(IDC_CHECK_TODAY, OnCheckToday)
	ON_BN_CLICKED(IDC_BUTTON_CPCPK, OnButtonCpcpk)
	ON_BN_CLICKED(IDC_BUTTON_FAIL, OnButtonFail)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_GRID_DOUBLECLICK, OnGridDoubleClick)
	ON_MESSAGE(WM_GRID_MOVECELL, OnMovedCurrentCell)
	ON_MESSAGE(WM_GRID_RIGHT_CLICK, OnRButtonClickedRowCol)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CResultFileLoadDlg message handlers

BOOL CResultFileLoadDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	InitFileListGrid();
	
	m_totalFileCount.SetTextColor(RGB(255, 0, 0));

/*	m_pThread = AfxBeginThread(LoadFileList, this);		//Start Save Thread
	if (m_pThread == NULL)	AfxThrowMemoryException();
	m_pThread->m_bAutoDelete = FALSE; 
*/
	if(m_strSelFileName.IsEmpty() == FALSE)
	{
		LoadFileList(this);
	}

/*	CString strTemp;
	strTemp.Format("%d", m_wndFileListGrid.GetRowCount());
	m_totalFileCount.SetText(strTemp);
*/
	UpdateData(FALSE);
	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CResultFileLoadDlg::InitFileListGrid()
{
	m_wndFileListGrid.SubclassDlgItem(IDC_RESULT_FILE_LIST_GIRD, this);
	//m_wndBoardListGrid.m_bRowSelection = TRUE;
	m_wndFileListGrid.m_bSameColSize  = FALSE;
	m_wndFileListGrid.m_bSameRowSize  = FALSE;
	m_wndFileListGrid.m_bCustomWidth  = TRUE;

	CRect rect;
	m_wndFileListGrid.GetWindowRect(rect);	//Step Grid Window Position
	ScreenToClient(&rect);

	float fWidth = float(rect.Width()-50)/100.0f;
	m_wndFileListGrid.m_nWidth[1]	= fWidth* 15.0f;
	m_wndFileListGrid.m_nWidth[2]	= fWidth* 15.0f;
	m_wndFileListGrid.m_nWidth[3]	= fWidth* 15.0f;
	m_wndFileListGrid.m_nWidth[4]	= fWidth* 20.0f;
	m_wndFileListGrid.m_nWidth[5]	= fWidth* 15.0f;
	m_wndFileListGrid.m_nWidth[6]	= fWidth* 10.0f;
	m_wndFileListGrid.m_nWidth[7]	= fWidth* 10.0f;
	m_wndFileListGrid.m_nWidth[8]	= 0;

	m_wndFileListGrid.Initialize();
//	m_wndFileListGrid.SetRowCount(0);
	m_wndFileListGrid.SetColCount(8);
	m_wndFileListGrid.SetDefaultRowHeight(18);
	m_wndFileListGrid.GetParam()->SetSortRowsOnDblClk(TRUE);
	m_wndFileListGrid.m_BackColor	= RGB(255,255,255);
	m_wndFileListGrid.SetColWidth(0, 0, 50);

	m_wndFileListGrid.m_bCustomColor 	= FALSE;

	m_wndFileListGrid.SetValueRange(CGXRange(0,1),  TEXT_LANG[0]);//"공정명"
	m_wndFileListGrid.SetValueRange(CGXRange(0,2),  TEXT_LANG[1]);//"LotNo"
	m_wndFileListGrid.SetValueRange(CGXRange(0,3),  TEXT_LANG[2]);//"TrayNo"
	m_wndFileListGrid.SetValueRange(CGXRange(0,4),  TEXT_LANG[3]);//"Date_Time"
	m_wndFileListGrid.SetValueRange(CGXRange(0,5),  TEXT_LANG[4]);//"모델명"
	m_wndFileListGrid.SetValueRange(CGXRange(0,6),  m_pDoc->m_strModuleName);
	m_wndFileListGrid.SetValueRange(CGXRange(0,7),  TEXT_LANG[5]);//"Operator"
	m_wndFileListGrid.SetValueRange(CGXRange(0,8),  TEXT_LANG[6]);//"FileName"

	m_wndFileListGrid.GetParam()->SetSortRowsOnDblClk(TRUE);	
	m_wndFileListGrid.EnableCellTips();
	m_wndFileListGrid.SetStyleRange(CGXRange().SetTable(), CGXStyle().SetWrapText(FALSE));

	m_wndFileListGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	m_wndFileListGrid.Redraw();
}

UINT LoadFileList(LPVOID lParam)
{
	g_bFileLoadThreadRun = TRUE;
//	THREAD_PARAM *pParam = (THREAD_PARAM *)lParam;
	CResultFileLoadDlg *pParam = (CResultFileLoadDlg *)lParam;
	ASSERT(pParam);

	CFileFind FileFinder;
	CString strFileName, strTemp;

	strTemp = pParam->m_strSelFileName;
	if(strTemp.IsEmpty())
	{
		return 0;
	}
	strTemp = pParam->m_strSelFileName.Left(pParam->m_strSelFileName.ReverseFind('\\'));

	CString FolderName = strTemp;
	FolderName = FolderName+"\\*."+BF_RESULT_FILE_EXT;

	BOOL   bWorking = FileFinder.FindFile(FolderName);		
	TRACE("File Read Thread Started %s(%d)\n", FolderName, bWorking);
	
	int nFileCount = 0;		//Count File
	while (bWorking)		
	{
		bWorking	= FileFinder.FindNextFile();
		nFileCount++;
	}
	
	bWorking = FileFinder.FindFile(FolderName);		

///////
	CProgressWnd *pProgressWnd = new CProgressWnd;
	pProgressWnd->Create(AfxGetApp()->m_pMainWnd, "Progress", TRUE);
//	pProgressWnd->SetColor(RGB(0,0,192));
	pProgressWnd->Clear();
	pProgressWnd->SetRange(0, 1000);
	pProgressWnd->SetText("결과 파일 List를 읽고 있습니다.");//omit
	pProgressWnd->SetPos(0);
	pProgressWnd->Show();


	FILE *fp = NULL;
	RESULT_FILE_HEADER	 pResultFileHeader;

//	pResultFileHeader = new RESULT_FILE_HEADER;
//	ASSERT(pResultFileHeader);

	int nRow = pParam->m_wndFileListGrid.GetRowCount();
	if(nRow >0)
		pParam->m_wndFileListGrid.RemoveRows(1, nRow);

	COleDateTime fileTime, curTime = COleDateTime::GetCurrentTime();

	EP_FILE_HEADER lpFileHeader;
	STR_FILE_EXT_SPACE extSpace;
//	lpFileHeader = new EP_FILE_HEADER;
//	ASSERT(lpFileHeader);

	BOOL bLock = pParam->m_wndFileListGrid.LockUpdate();

	STR_CONDITION_HEADER conditionHeader, modelHeader;
	nRow =0;
	while (bWorking && !pParam->m_bStopThreadFlag)			//Find Board Test Result File
	{
///
		pProgressWnd->SetPos(int((float)nRow/(float)nFileCount*1000.0f));
		pProgressWnd->PeekAndPump(FALSE);
///
		bWorking	= FileFinder.FindNextFile();
		strFileName = FileFinder.GetFilePath();

		if(strFileName.IsEmpty())
		{
			TRACE("Result File Name is Empty\n");
			continue;			//File Name Check;
		}
		
		fp = fopen(strFileName, "rb");
		if(fp == NULL)
		{
			TRACE("Result File Open Fail\n");
			continue;
		}

//		TRACE("%s File Read\n", strFileName);
	
		fread(&lpFileHeader, sizeof(EP_FILE_HEADER), 1, fp);
		if(strcmp(lpFileHeader.szFileID, RESULT_FILE_ID) != 0 && 
		   strcmp(lpFileHeader.szFileID, IROCV_FILE_ID)  != 0 &&
		   strcmp(lpFileHeader.szFileID, PRODUCT_RESULT_FILE_ID) !=0)
		{
			fclose(fp);
			fp = NULL;
			TRACE("Not Support Result File\n");
			continue;
		}

		fread(&pResultFileHeader, sizeof(RESULT_FILE_HEADER), 1, fp);				//File Header Read

		//Lot Filter
		if(!pParam->m_strLotFilter.IsEmpty())
		{
			strTemp = pResultFileHeader.szLotNo;
			if(strTemp != pParam->m_strLotFilter)
			{
				fclose(fp);
				fp = NULL;
				continue;
			}
		}

		//Tray Filter
		if(!pParam->m_strTrayFilter.IsEmpty())
		{
			strTemp = pResultFileHeader.szTrayNo;
			if(strTemp != pParam->m_strTrayFilter)
			{
				fclose(fp);
				fp = NULL;
				continue;
			}
		}
		
		//date Filter
		if(pParam->m_bTodayWork)
		{
			fileTime.ParseDateTime(pResultFileHeader.szDateTime);
			if(fileTime.GetYear() != curTime.GetYear()
				|| fileTime.GetMonth() != curTime.GetMonth()
				|| fileTime.GetDay() != curTime.GetDay()
			)
			{
				fclose(fp);
				fp = NULL;
				continue;
			}
		}

		fseek(fp, sizeof(EP_MD_SYSTEM_DATA), SEEK_CUR);

		if(fread(&extSpace, sizeof(STR_FILE_EXT_SPACE), 1, fp) != 1)
		{
			TRACE("Extra space data read fail..\n");
			fclose(fp);
			fp = NULL;
			break;		//File Header Read
		}

		if(atol(lpFileHeader.szFileVersion) >= atol(RESULT_FILE_VER3))
		{
			fseek(fp, sizeof(PNE_RESULT_CELL_SERIAL), SEEK_CUR);
		}
		
		fseek(fp, sizeof(SENSOR_MAPPING_SAVE_TABLE), SEEK_CUR);
	
		if(fread(&modelHeader, sizeof(STR_CONDITION_HEADER), 1, fp) != 1)
		{
			TRACE("Model Header Read Fail..\n");
			fclose(fp);
			fp = NULL;
			break;		//File Header Read
		}

		if(fread(&conditionHeader, sizeof(STR_CONDITION_HEADER), 1, fp) != 1)
		{
			TRACE("Test Header Read Fail..\n");
			fclose(fp);
			fp = NULL;
			break;		//File Header Read
		}

//		fseek(fp, sizeof(EP_TEST_HEADER), SEEK_CUR);
//		strTemp.Format("%d byte" , FileFinder.GetLength());
		//Add to Board Test List
//		nRow = pParam->m_wndFileListGrid.GetRowCount()+1;

		nRow++;
		pParam->m_wndFileListGrid.InsertRows(nRow, 1);	
		pParam->m_wndFileListGrid.SetValueRange(CGXRange(nRow, 1),  conditionHeader.szName);
		pParam->m_wndFileListGrid.SetValueRange(CGXRange(nRow, 2),  pResultFileHeader.szLotNo);
		pParam->m_wndFileListGrid.SetValueRange(CGXRange(nRow, 3),  pResultFileHeader.szTrayNo);
		pParam->m_wndFileListGrid.SetValueRange(CGXRange(nRow, 4),  pResultFileHeader.szDateTime);
		pParam->m_wndFileListGrid.SetValueRange(CGXRange(nRow, 5),  modelHeader.szName);
		pParam->m_wndFileListGrid.SetValueRange(CGXRange(nRow, 6),  extSpace.szModuleName);	
		pParam->m_wndFileListGrid.SetValueRange(CGXRange(nRow, 7),  pResultFileHeader.szOperatorID);
		pParam->m_wndFileListGrid.SetValueRange(CGXRange(nRow, 8),  FileFinder.GetFilePath());
		

		
		fclose(fp);
		fp = NULL;

		strTemp.Format("%d", nRow);
		pParam->m_totalFileCount.SetText(strTemp);

//		pParam->HandleIdleMessage();
	}
	
	pParam->m_wndFileListGrid.LockUpdate(bLock);
	pProgressWnd->SetPos(1000);
	pProgressWnd->PeekAndPump(FALSE);
	
	if(!pParam->m_bStopThreadFlag)
	{
		CGXSortInfoArray sortInfo;
		sortInfo.SetSize(1);			// 1 key only (you can also have more keys)
		sortInfo[0].nRC = 4;			// column nCol is the key
		sortInfo[0].sortType = CGXSortInfo::autodetect;	// the grid
				// will determine if the key is a date, numeric or 
				// alphanumeric value
		sortInfo[0].sortOrder = CGXSortInfo::descending;	
				// sort ascending
		
		if(nRow>1)
		{
			pParam->m_wndFileListGrid.SortRows(CGXRange().SetTable(), sortInfo);
			pParam->m_wndFileListGrid.SetCurrentCell(1, 1);
		}
		pParam->m_wndFileListGrid.Redraw();
	}

/*	delete lpFileHeader;
	lpFileHeader = NULL;

	delete pResultFileHeader;
	pResultFileHeader = NULL;
*/
	pParam->m_bStopThreadFlag = FALSE;
	g_bFileLoadThreadRun = FALSE;

	if(pProgressWnd != NULL)
	{
		delete pProgressWnd;
		pProgressWnd = NULL;
	}

	return 0;
}

BOOL CResultFileLoadDlg::WaitThreadClose()
{
	DWORD	dwExitCode;
	int		state;

	if(m_pThread == NULL)	return TRUE;

	m_bStopThreadFlag = TRUE;
	state= ::WaitForSingleObject(m_pThread->m_hThread, INFINITE);

	if(state == WAIT_OBJECT_0)
	{
		TRACE("File List Read Thread Closed\n");
	}
	else
	{
		TRACE("File List Read Thread Closed Fail\n");
	}

	::GetExitCodeThread(m_pThread->m_hThread, &dwExitCode);
	
	if(dwExitCode == STILL_ACTIVE)
	{
		TRACE("Client Connection Thread Terminate Fail. Code %d\n", dwExitCode); 
		return FALSE;
	}
		
	if(m_pThread != NULL)
	{
		delete m_pThread;
		m_pThread = NULL;
	}

	m_bStopThreadFlag = FALSE;
	return TRUE;
}



void CResultFileLoadDlg::OnDestroy() 
{
//Thread가 안전하게 종료된 후에 닫아야 할것 갔은데...
	if(WaitThreadClose() == FALSE)
	{
		AfxMessageBox("Thread Close Fail");
	}

	CDialog::OnDestroy();
}

CString CResultFileLoadDlg::GetSelFileName()
{
	return m_strSelFileName;
}

LONG CResultFileLoadDlg::OnGridDoubleClick(WPARAM wParam, LPARAM /*lParam*/)
{
	ROWCOL nCol = LOWORD(wParam);
	ROWCOL nRow = HIWORD(wParam);
	
	if(nCol > 0 && nRow > 0)
	{
		UpdateData(TRUE);
		m_strSelFileName = m_wndFileListGrid.GetValueRowCol(nRow, 8); 
		UpdateData(FALSE);
		OnOK();
	}
	return 0;
}

LONG CResultFileLoadDlg::OnMovedCurrentCell(WPARAM wParam, LPARAM /*lParam*/)
{
	ROWCOL nCol = LOWORD(wParam);
	ROWCOL nRow = HIWORD(wParam);

	if(nCol > 0 && nRow > 0)
	{
		UpdateData(TRUE);
		m_strSelFileName = m_wndFileListGrid.GetValueRowCol(nRow, 8); 
		UpdateData(FALSE);
	}
	return 0;
}


void CResultFileLoadDlg::OnRefresh() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

	if(m_strLotFilter.IsEmpty() && m_strTrayFilter.IsEmpty())
	{
		if(MessageBox(TEXT_LANG[8],TEXT_LANG[9], MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2) !=IDYES)//"검색 조건이 설정되어 있지 않습니다. 모든 Data를 검색하시겠습니까?\nData량에 따라 지연될 수 있습니다.", "검색에러"
		{
			return;
		}
	}

	SearchFile();
//	LoadFileList(this);

	CString strTemp;
	strTemp.Format("%d", m_wndFileListGrid.GetRowCount());
	m_totalFileCount.SetText(strTemp);
/*
	if(	g_bFileLoadThreadRun)	return;
	
	if(m_pThread)
	{
		delete m_pThread;
		m_pThread = NULL;
	}

	m_pThread = AfxBeginThread(LoadFileList, this);		//Start Save Thread
	if (m_pThread == NULL)	AfxThrowMemoryException();
	m_pThread->m_bAutoDelete = FALSE; 
*/
}


void CResultFileLoadDlg::OnDeleteFile() 
{
	// TODO: Add your control notification handler code here
	int nTotRow = m_wndFileListGrid.GetRowCount();
	if(nTotRow <=0 )	return;

	CRowColArray	awRows;
	CString strTemp;

	m_wndFileListGrid.GetSelectedRows(awRows);
	int nSelNo = awRows.GetSize();
	if(nSelNo == 1)
	{
		if(awRows[0] >0)
		{
			strTemp = m_wndFileListGrid.GetValueRowCol(awRows[0], 1);
			strTemp += TEXT_LANG[10];//" 파일을 삭제 하시겠습니까?"
		}
		else
			return;
	}
	else
	{
		strTemp.Format(TEXT_LANG[11], nSelNo);//"선택한 %d개의 파일을 삭제 하시겠습니까?"
	}
	
	if(MessageBox(strTemp, TEXT_LANG[12], MB_ICONQUESTION|MB_YESNO) != IDYES)		return;//"파일 삭제"

	char szFrom[256], szTo[256];
	LPSHFILEOPSTRUCT lpFileOp = new SHFILEOPSTRUCT;
	for (int i = nSelNo-1; i>= 0; i--)
	{
		if(awRows[i] >0 && awRows[i] <= nTotRow)
		{
			strTemp =  m_wndFileListGrid.GetValueRowCol(awRows[i], 8);
			
			//휴지통으로 
/*			if(_unlink((LPSTR)(LPCTSTR)strTemp) != 0)
			{
				strTemp =  strTemp+" 파일을 삭제 할 수 없습니다."; 
				AfxMessageBox(strTemp);
				continue;
			}
*/			//
			ZeroMemory(szFrom, 256);	//Double NULL Terminate
			ZeroMemory(szTo, 256);		//Double NULL Terminate
				
			//pFrom에 Full Path 제공하고 
			//pTo에 Path명을 제공하지 않고 
			//fFlags Option에 FOF_ALLOWUNDO 부여하고 
			//Delete 동작 하면 휴지통으로 삭제 된다.

			sprintf(szFrom, "%s", strTemp);
			ZeroMemory(lpFileOp, sizeof(SHFILEOPSTRUCT));
			lpFileOp->hwnd = NULL; 
			lpFileOp->wFunc = FO_DELETE ; 
			lpFileOp->pFrom = szFrom; 
			lpFileOp->pTo = szTo; 
			lpFileOp->fFlags = FOF_FILESONLY|FOF_NOCONFIRMATION|FOF_ALLOWUNDO ;//FOF_NOCONFIRMATION | FOF_NOERRORUI | FOF_FILESONLY ; 
			lpFileOp->fAnyOperationsAborted = FALSE; 
			lpFileOp->hNameMappings = NULL; 
			lpFileOp->lpszProgressTitle = NULL; 

			if(SHFileOperation(lpFileOp) != 0)		//Move File
			{
				delete	lpFileOp;
				lpFileOp = NULL;
			}
			else
			{
				m_wndFileListGrid.RemoveRows(awRows[i], awRows[i]);
			}

		}
	}
	delete	lpFileOp;
	lpFileOp = NULL;

	strTemp.Format("%d", m_wndFileListGrid.GetRowCount());
	m_totalFileCount.SetText(strTemp);
}

void CResultFileLoadDlg::OnFolderSel() 
{
	UpdateData();
	//Folder explore
/*	CString strTmp=AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION, "LastSelDir", NULL);
	if(strTmp.IsEmpty())
	{
		TCHAR szLastSelDir[MAX_PATH];
		GetModuleFileName(AfxGetApp()->m_hInstance, szLastSelDir, MAX_PATH);
		strTmp = CString(szLastSelDir).Mid(0, CString(szLastSelDir).ReverseFind('\\')) + "\\Data";
	}
	CFolderDialog dlg(strTmp);
	if( dlg.DoModal() == IDOK)
	{
		m_strFileFolder = dlg.GetPathName();
		AfxGetApp()->WriteProfileString(FORM_PATH_REG_SECTION, "LastSelDir", m_strFileFolder);
		UpdateData(FALSE);
		OnRefresh();
	}
*/

	//file explore
	CString strFilter;
	strFilter.Format(TEXT_LANG[13], BF_RESULT_FILE_EXT, BF_RESULT_FILE_EXT);//"CTS 결과 파일(*.%s)|*.%s|"
	CFileDialog dlgFile(TRUE, BF_RESULT_FILE_EXT, m_strSelFileName, OFN_HIDEREADONLY|OFN_FILEMUSTEXIST, strFilter);
	dlgFile.m_ofn.lpstrTitle = TEXT_LANG[14];//"Cell test system 결과 파일"
			
	if(dlgFile.DoModal() == IDOK)
	{
		m_strSelFileName = dlgFile.GetPathName();
		UpdateData(FALSE);
		InsertFormFileList(m_strSelFileName);
		return ;
	}

}

void CResultFileLoadDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	if(::IsWindow(this->GetSafeHwnd()))
	{
		RECT rect;
		::GetClientRect(m_hWnd, &rect);

		CRect rectGrid;
		float width;
		if(m_wndFileListGrid.GetSafeHwnd())
		{
			m_wndFileListGrid.GetWindowRect(rectGrid);	//Step Grid Window Position
			ScreenToClient(&rectGrid);
			m_wndFileListGrid.MoveWindow(5, rectGrid.top, rect.right-10, rect.bottom-rectGrid.top-5, FALSE);
			
			m_wndFileListGrid.GetClientRect(rectGrid);
			width = (float)(rectGrid.Width()-50)/100.0f;
				
			m_wndFileListGrid.m_nWidth[1]	= width* 15.0f;
			m_wndFileListGrid.m_nWidth[2]	= width* 15.0f;
			m_wndFileListGrid.m_nWidth[3]	= width* 15.0f;
			m_wndFileListGrid.m_nWidth[4]	= width* 20.0f;
			m_wndFileListGrid.m_nWidth[5]	= width* 15.0f;
			m_wndFileListGrid.m_nWidth[6]	= width* 10.0f;
			m_wndFileListGrid.m_nWidth[7]	= width* 10.0f;
//			m_wndFileListGrid.Redraw();
			Invalidate();
		}
	}
}

LRESULT CResultFileLoadDlg::OnRButtonClickedRowCol(WPARAM wParam, LPARAM lParam)
{
	CMenu menu, *popmenu; 
	VERIFY(menu.LoadMenu(IDR_POP_UP)); 
	popmenu = menu.GetSubMenu(0); 
	CPoint ptMouse; 
	GetCursorPos(&ptMouse); 
	popmenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, ptMouse.x, ptMouse.y, this); 				
	
	return 1;
}


void CResultFileLoadDlg::OnReload() 
{
	// TODO: Add your command handler code here
	OnRefresh() ;
}

void CResultFileLoadDlg::OnSelectAll() 
{
	// TODO: Add your command handler code here
	m_wndFileListGrid.SelectRange(CGXRange().SetTable());

}

void CResultFileLoadDlg::OnEditCopy() 
{
	// TODO: Add your command handler code here
	m_wndFileListGrid.Copy();
	
}

void CResultFileLoadDlg::OnDelete() 
{
	// TODO: Add your command handler code here
	OnDeleteFile();
}

BOOL CResultFileLoadDlg::SearchFile()
{
	CWaitCursor wait;
	int nRow = m_wndFileListGrid.GetRowCount();
	if(nRow >0)
	{
		m_wndFileListGrid.RemoveRows(1, nRow);
	}	
	m_strSelFileName.Empty();
	
	CString strTemp, strTemp1, strFolder;
	CFileFind FileFinder, subFileFind;

	// MAX Data Foler를 10개 설정 할 수 있다. 
	// 레지스트리에는 10개 이후의 경로도 저장된다.
	for(int i =0; i<10; i++)
	{
		strTemp.Format("Data%d", i);
		strFolder = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,strTemp);		//Get Data Folder
		if(strFolder.IsEmpty())	continue;

		FindResultFile(strFolder);

		//recursive function
/*		BOOL bFind = FileFinder.FindFile(strFolder+"\\*");
		while(bFind)
		{
			bFind = FileFinder.FindNextFile();
			
			if(FileFinder.IsDots())
			{
				continue;
			}
			else if(FileFinder.IsDirectory())
			{
				strTemp = FileFinder.GetFileName();

				//Foler로 비교 
				if(m_strLotFilter.IsEmpty() || ((m_strLotFilter.IsEmpty() == FALSE) && strTemp == m_strLotFilter))
				{
					BOOL bFind1 = subFileFind.FindFile(FileFinder.GetFilePath()+"\\*");
					while(bFind1)
					{
						bFind1 = subFileFind.FindNextFile();
						if(subFileFind.IsDirectory() && !subFileFind.IsDots())
						{
							strTemp1 = subFileFind.GetFileName();
							//strTemp1.MakeLower();
							if(m_strTrayFilter.IsEmpty() || ((m_strTrayFilter.IsEmpty() == FALSE) && strTemp1 == m_strTrayFilter))
							{
								AddFileList(subFileFind.GetFilePath());
							}
						}
					}			
				}
			}
			else	//File
			{
				//Add File list
			}
		}
*/
	}
		
	if(m_wndFileListGrid.GetRowCount()>1)
	{
		CGXSortInfoArray sortInfo;
		sortInfo.SetSize(1);			// 1 key only (you can also have more keys)
		sortInfo[0].nRC = 4;			// column nCol is the key
		sortInfo[0].sortType = CGXSortInfo::autodetect;	// the grid
				// will determine if the key is a date, numeric or 
					// alphanumeric value
		sortInfo[0].sortOrder = CGXSortInfo::descending;	
				// sort ascending
		m_wndFileListGrid.SortRows(CGXRange().SetTable(), sortInfo);


		m_wndFileListGrid.SetCurrentCell(1, 1);
		m_strSelFileName = m_wndFileListGrid.GetValueRowCol(1, 8); 
		
	}
	m_wndFileListGrid.Redraw();

	UpdateData(FALSE);

	return TRUE;
}

//주어지 폴더와 하위 폴더에서 Match file을 찾는다.
BOOL CResultFileLoadDlg::FindResultFile(CString strFolder, BOOL bFindSubFolder)
{
	//recursive function
	CFileFind FileFinder;
	BOOL bFind = FileFinder.FindFile(strFolder+"\\*");
	while(bFind)
	{
		bFind = FileFinder.FindNextFile();
		
		if(FileFinder.IsDots())
		{
			continue;
		}
		else if(FileFinder.IsDirectory())
		{
			if(bFindSubFolder)
			{
				FindResultFile(FileFinder.GetFilePath());
			}
		}
		else	//File Match check  1.lot	2. tray	3.date
		{
			//Add File list
			//*.fmt file
			CString strFileName(FileFinder.GetFilePath());
			CString strExt(strFileName.Mid(strFileName.ReverseFind('.')+1));
			strExt.MakeLower();
			if(strExt == BF_RESULT_FILE_EXT)
			{
//				TRACE("%s\n", strFileName);
				InsertFormFileList(strFileName);
			}
		}
	}

	return TRUE;
}

BOOL CResultFileLoadDlg::AddFileList(CString strFolder)
{
	CFileFind FileFinder;
	CString FolderName, strFileName, strTemp;
	FolderName = strFolder +"\\*."+BF_RESULT_FILE_EXT;

	BOOL   bWorking = FileFinder.FindFile(FolderName);		
	TRACE("File Read Thread Started %s(%d)\n", FolderName, bWorking);

	
	int nFileCount = 0;		//Count File
	while (bWorking)		
	{
		bWorking	= FileFinder.FindNextFile();
		nFileCount++;
	}

///////
	CProgressWnd *pProgressWnd = new CProgressWnd;
	pProgressWnd->Create(AfxGetApp()->m_pMainWnd, "Progress", TRUE);
	pProgressWnd->Clear();
	pProgressWnd->SetRange(0, 100);
	pProgressWnd->SetText(TEXT_LANG[7]);//"결과 파일 List를 읽고 있습니다."
	pProgressWnd->SetPos(0);
	pProgressWnd->Show();

	int nRow =m_wndFileListGrid.GetRowCount();
	bWorking = FileFinder.FindFile(FolderName);		
	BOOL bLock = m_wndFileListGrid.LockUpdate();

	int nCnt = 0;
	while (bWorking && !m_bStopThreadFlag)			//Find Board Test Result File
	{
		pProgressWnd->SetPos(int((float)nCnt++/(float)nFileCount*100.0f));
		pProgressWnd->PeekAndPump(FALSE);
		if(pProgressWnd->Cancelled())	break;
///
		bWorking	= FileFinder.FindNextFile();
		strFileName = FileFinder.GetFilePath();

		if(strFileName.IsEmpty())
		{
			TRACE("Result File Name is Empty\n");
			continue;			//File Name Check;
		}		
		InsertFormFileList(strFileName);
	}
	m_wndFileListGrid.LockUpdate(bLock);
	
	strTemp.Format("%d", nRow);
	m_totalFileCount.SetText(strTemp);

	pProgressWnd->SetPos(100);
	pProgressWnd->PeekAndPump(FALSE);
	
/*	if(!m_bStopThreadFlag)
	{
		CGXSortInfoArray sortInfo;
		sortInfo.SetSize(1);			// 1 key only (you can also have more keys)
		sortInfo[0].nRC = 4;			// column nCol is the key
		sortInfo[0].sortType = CGXSortInfo::autodetect;	// the grid
				// will determine if the key is a date, numeric or 
				// alphanumeric value
		sortInfo[0].sortOrder = CGXSortInfo::descending;	
				// sort ascending
		
		if(nRow>1)
		{
			m_wndFileListGrid.SortRows(CGXRange().SetTable(), sortInfo);
			m_wndFileListGrid.SetCurrentCell(1, 1);
		}
		m_wndFileListGrid.Redraw();
	}
	m_bStopThreadFlag = FALSE;
*/
	
	if(pProgressWnd != NULL)
	{
		delete pProgressWnd;
		pProgressWnd = NULL;
	}

	return TRUE;
}

BOOL CResultFileLoadDlg::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message == WM_KEYDOWN )
	{
		switch( pMsg->wParam )
		{
		case VK_RETURN:
			{
				if(GetFocus() == GetDlgItem(IDC_LOT_FILTER) || GetFocus() == GetDlgItem(IDC_TRAY_FILTER))
				{
					OnRefresh();
				}
				return TRUE;
			}
		}
	}
	
	return CDialog::PreTranslateMessage(pMsg);
}

void CResultFileLoadDlg::OnToDayButton() 
{
	// TODO: Add your control notification handler code here
	m_fromTime = CTime::GetCurrentTime();
	m_toTime = CTime::GetCurrentTime();
	UpdateData(FALSE);

}

void CResultFileLoadDlg::OnCheckToday() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	GetDlgItem(IDC_DATETIMEPICKER1)->EnableWindow(m_bTodayWork);
	GetDlgItem(IDC_DATETIMEPICKER3)->EnableWindow(m_bTodayWork);
}

void CResultFileLoadDlg::OnButtonCpcpk() 
{
	// TODO: Add your control notification handler code here

	CStringArray aFile;
	for(int row=0; row<m_wndFileListGrid.GetRowCount(); row++)
	{
		aFile.Add(m_wndFileListGrid.GetValueRowCol(row+1, 8));
	}
	m_pDoc->ShowCpCpkData(m_strLotFilter, aFile);
	return;

/*	double         dMax = -10E+100, dMin = 10E+100;
	CString        str;
	double			dData1;

	static double m_dDivision = 1;
	static double m_dLowLimit = 0;
	static double m_dHighLimit = 0;
	static int m_nCpItem = 0;

//////Cp, Cpk Set Dlg
	CCpkSetDlg     dlgCpkSet;
	dlgCpkSet.m_strHighRange.Format("%.4f", m_dHighLimit);
	dlgCpkSet.m_strLowRange.Format("%.4f", m_dLowLimit);
	dlgCpkSet.m_strGraphGap.Format("%.4f", m_dDivision);
	dlgCpkSet.m_nItem = m_nCpItem;
	if(m_strLotFilter.IsEmpty() == FALSE)
	{
		dlgCpkSet.m_strTitle.Format("Lot [%s] 분포도 보기", m_strLotFilter);
	}
   	
	dlgCpkSet.m_nCount = m_pDoc->m_TotCol;
	for(int i =0; i<m_pDoc->m_TotCol; i++)
	{
		dlgCpkSet.m_field[i].No = m_pDoc->m_field[i].No;
		dlgCpkSet.m_field[i].FieldName = m_pDoc->m_field[i].FieldName;
		dlgCpkSet.m_field[i].ID = m_pDoc->m_field[i].ID;
   		dlgCpkSet.m_field[i].DataType = m_pDoc->m_field[i].DataType;
		dlgCpkSet.m_field[i].bDisplay = m_pDoc->m_field[i].bDisplay;
	}
	
	if(dlgCpkSet.DoModal() != IDOK)	return;
	
	m_dHighLimit = atof(dlgCpkSet.m_strHighRange);
	m_dLowLimit  = atof(dlgCpkSet.m_strLowRange);
	m_dDivision  = atof(dlgCpkSet.m_strGraphGap);
	m_nCpItem = dlgCpkSet.m_nItem;
//////
	int procType = m_pDoc->m_field[m_nCpItem].ID;
	int dataType = m_pDoc->m_field[m_nCpItem].DataType;

	CList<double, double&> dDataBuffer;   
	CString strRltFile, strTemp;
	STR_STEP_RESULT *lpStepDataInfo;
	STR_SAVE_CH_DATA *lpChannelData;
	CFormResultFile *pRltFile = new CFormResultFile;
	
	for( row=0; row<m_wndFileListGrid.GetRowCount(); row++)
	{
		strRltFile = m_wndFileListGrid.GetValueRowCol(row+1, 8);
		if(pRltFile->ReadFile(strRltFile))
		{
			for(int step = 0; step<pRltFile->GetStepSize(); step++)
			{
				lpStepDataInfo = pRltFile->GetStepData(step);
				if(lpStepDataInfo)
				{
					//find matting field
//					for(int f = 0; f<m_pDoc->m_TotCol; f++)
//					{
						if(lpStepDataInfo->stepCondition.stepHeader.nProcType == procType)// || pDoc->m_field[f].ID == RPT_CH_CODE)
						{
							for(int c = 0; c<lpStepDataInfo->aChData.GetSize(); c++)
							{
								lpChannelData = (STR_SAVE_CH_DATA *)lpStepDataInfo->aChData[c];
								if(!IsNormalCell((BYTE)lpChannelData->channelCode))	 continue;	//정상이 아니면 계산 않함

								//find matting data type
								switch(dataType)
								{
								case EP_VOLTAGE:
									dData1 = atof(m_pDoc->ValueString(lpChannelData->fVoltage, dataType));
									if(dMin > dData1)  dMin = dData1;	//Min
									if(dMax < dData1)  dMax = dData1;	//Max
									dDataBuffer.AddTail(dData1);
									break;
								case EP_CURRENT:
									dData1 = atof(m_pDoc->ValueString(lpChannelData->fCurrent, dataType));
									if(dMin > dData1)  dMin = dData1;	//Min
									if(dMax < dData1)  dMax = dData1;	//Max
									dDataBuffer.AddTail(dData1);
									break;
								case EP_CAPACITY:
									dData1 = atof(m_pDoc->ValueString(lpChannelData->fCapacity, dataType));
									if(dMin > dData1)  dMin = dData1;	//Min
									if(dMax < dData1)  dMax = dData1;	//Max
									dDataBuffer.AddTail(dData1);
									break;
								case EP_STEP_TIME:
									dData1 = atof(m_pDoc->ValueString(lpChannelData->fStepTime, dataType));
									if(dMin > dData1)  dMin = dData1;	//Min
									if(dMax < dData1)  dMax = dData1;	//Max
									dDataBuffer.AddTail(dData1);
									break;
								case EP_IMPEDANCE:
									dData1 = atof(m_pDoc->ValueString(lpChannelData->fImpedance, dataType));
									if(dMin > dData1)  dMin = dData1;	//Min
									if(dMax < dData1)  dMax = dData1;	//Max
									dDataBuffer.AddTail(dData1);
									break;
								}
							}
						}
//					}			
				}
			}
		}
	}
	delete pRltFile;
	pRltFile = NULL;

	long lCount = dDataBuffer.GetCount();
	if(lCount <= 0)
	{
		MessageBox("범위내의 해당하는 Cell이 없습니다.", "Data 오류", MB_ICONSTOP|MB_OK);
		return;
	}

	//Memory 할당 받는다. (양품수로 해서 => 최대 가능 범위)
	double *pData = new double[lCount];
	ASSERT(pData);
	ZeroMemory(pData, sizeof(double)*lCount);	
	lCount = 0;

	POSITION pos = dDataBuffer.GetHeadPosition();
	while(pos)
	{
		dData1 = dDataBuffer.GetNext(pos);
		if(dlgCpkSet.m_bRange == TRUE)
		{
   			if(dlgCpkSet.m_bHighRange == TRUE && dlgCpkSet.m_bLowRange == TRUE) //상하한 설정시  
			{
   				if(m_dLowLimit <= dData1 && dData1 <= m_dHighLimit)
				{
					pData[lCount++] = dData1;
				}		    					
			}
			else if(dlgCpkSet.m_bHighRange == TRUE && dlgCpkSet.m_bLowRange == FALSE)//상한만
			{
				if(dData1 <= m_dHighLimit)
				{
					pData[lCount++] = dData1;
				}
			}
			else if(dlgCpkSet.m_bHighRange == TRUE && dlgCpkSet.m_bLowRange == FALSE)//하한만
			{
	  			if(m_dLowLimit <= dData1)
				{
					pData[lCount++] = dData1;
				}
			}
		}
		else   // m_pCpkSetDlg.m_bRange = FALSE
		{
			pData[lCount++] = dData1;
		}
	}
		
	ASSERT(lCount > 0);	
	//Cp, Cpk Graph  Display
	CCpkViewDlg*	pCpkViewDlg = new CCpkViewDlg(this);
	pCpkViewDlg->SetRowData(pData, lCount, m_dDivision);
	pCpkViewDlg->SetHighLimit(dlgCpkSet.m_bHighRange, m_dHighLimit);
	pCpkViewDlg->SetLowLimit(dlgCpkSet.m_bLowRange, m_dLowLimit);
	pCpkViewDlg->m_strXAxisLable =m_pDoc->m_field[m_nCpItem].FieldName; 
	if(m_strLotFilter.IsEmpty() == FALSE)
	{
		pCpkViewDlg->m_strTitle.Format("%s %s 분포도(%d개)", m_strLotFilter, m_pDoc->m_field[m_nCpItem].FieldName, lCount);
	}
	else
	{
		pCpkViewDlg->m_strTitle.Format("%s 분포도(%d개)", m_pDoc->m_field[m_nCpItem].FieldName, lCount);
	}
	pCpkViewDlg->DoModal();
	delete pCpkViewDlg;

	delete [] pData;
	pData = NULL;	
*/
}

void CResultFileLoadDlg::OnButtonFail() 
{
	// TODO: Add your control notification handler code here
	if(m_wndFileListGrid.GetRowCount() <= 0)	return;

	CStringArray aStrFile;
	for(int i=0; i<m_wndFileListGrid.GetRowCount(); i++)
	{
		aStrFile.Add(m_wndFileListGrid.GetValueRowCol(i+1, 8));
	}

/*	CSerarchDataDlg dlg(m_pDoc, this);
	dlg.SetFileName(&aStrFile);
	CString str;
	if(m_strLotFilter.IsEmpty())
	{
		str = "검색 Tray data 보기";
	}
	else
	{
		str.Format("Lot %s 검색 Tray data 보기", m_strLotFilter);
	}
	dlg.SetTitle(str);
	dlg.DoModal();
*/
	m_pDoc->ShowLotAllData(m_strLotFilter, aStrFile);
}

BOOL CResultFileLoadDlg::InsertFormFileList(CString strFile)
{
	CString strTemp;
	FILE *fp = NULL;
	RESULT_FILE_HEADER	 pResultFileHeader;
	COleDateTime fileTime;//, curTime = COleDateTime::GetCurrentTime();
	EP_FILE_HEADER lpFileHeader;
	STR_CONDITION_HEADER conditionHeader, modelHeader;
	STR_FILE_EXT_SPACE extSpace;

	//속도 향상을 위해 CFormResult Class 사용하지 않고 직접 읽음 
	fp = fopen(strFile, "rb");
	if(fp == NULL)
	{
		TRACE("Result File Open Fail\n");
		return FALSE;
	}

//		TRACE("%s File Read\n", strFileName);
	
	fread(&lpFileHeader, sizeof(EP_FILE_HEADER), 1, fp);
	if(strcmp(lpFileHeader.szFileID, RESULT_FILE_ID) != 0 && 
	   strcmp(lpFileHeader.szFileID, IROCV_FILE_ID)  != 0 &&
	   strcmp(lpFileHeader.szFileID, PRODUCT_RESULT_FILE_ID) !=0)
	{
		fclose(fp);
		fp = NULL;
		TRACE("Not Support Result File\n");
		return FALSE;
	}

	fread(&pResultFileHeader, sizeof(RESULT_FILE_HEADER), 1, fp);				//File Header Read

	//Lot Filter
	if(!m_strLotFilter.IsEmpty())
	{
		strTemp = pResultFileHeader.szLotNo;
		if(strTemp != m_strLotFilter)
		{
			fclose(fp);
			fp = NULL;
			return FALSE;
		}
	}

	//Tray Filter
	if(!m_strTrayFilter.IsEmpty())
	{
		strTemp = pResultFileHeader.szTrayNo;
		if(strTemp != m_strTrayFilter)
		{
			fclose(fp);
			fp = NULL;
			return FALSE;
		}
	}
	
	//date Filter
	if(m_bTodayWork)
	{
		fileTime.ParseDateTime(pResultFileHeader.szDateTime);
		if(fileTime.m_status == COleDateTime::valid)
		{
			CTime fTime(fileTime.GetYear(), fileTime.GetMonth(), fileTime.GetDay(), 0, 0, 0);
			CTime fmTime(m_fromTime.GetYear(), m_fromTime.GetMonth(), m_fromTime.GetDay(), 0, 0, 0);
			CTime tTime(m_toTime.GetYear(), m_toTime.GetMonth(), m_toTime .GetDay(), 0, 0, 0);
			
			TRACE("%s<%s<%s\n", fmTime.Format("%Y%m%d"), fTime.Format("%Y%m%d"), tTime.Format("%Y%m%d"));

			if(fTime < fmTime || tTime < fTime)
			{
				fclose(fp);
				fp = NULL;
				return FALSE;
			}
		}
		else
		{
			fclose(fp);
			fp = NULL;
			return FALSE;
		}
	}

	fseek(fp, sizeof(EP_MD_SYSTEM_DATA), SEEK_CUR);

	if(fread(&extSpace, sizeof(STR_FILE_EXT_SPACE), 1, fp) != 1)
	{
		TRACE("Extra space data read fail..\n");
		fclose(fp);
		fp = NULL;
		return FALSE;
	}
	
	if(atol(lpFileHeader.szFileVersion) >= atol(RESULT_FILE_VER3))
	{
		fseek(fp, sizeof(PNE_RESULT_CELL_SERIAL), SEEK_CUR);
	}

	fseek(fp, sizeof(SENSOR_MAPPING_SAVE_TABLE), SEEK_CUR);

	if(fread(&modelHeader, sizeof(STR_CONDITION_HEADER), 1, fp) != 1)
	{
		TRACE("Model Header Read Fail..\n");
		fclose(fp);
		fp = NULL;
		return FALSE;
	}

	if(fread(&conditionHeader, sizeof(STR_CONDITION_HEADER), 1, fp) != 1)
	{
		TRACE("Test Header Read Fail..\n");
		fclose(fp);
		fp = NULL;
		return FALSE;
	}

	fseek(fp, sizeof(EP_TEST_HEADER), SEEK_CUR);

	ROWCOL nRow = m_wndFileListGrid.GetRowCount()+1;
	m_wndFileListGrid.InsertRows(nRow, 1);	
	m_wndFileListGrid.SetValueRange(CGXRange(nRow, 1),  conditionHeader.szName);
	m_wndFileListGrid.SetValueRange(CGXRange(nRow, 2),  pResultFileHeader.szLotNo);
	m_wndFileListGrid.SetValueRange(CGXRange(nRow, 3),  pResultFileHeader.szTrayNo);
	m_wndFileListGrid.SetValueRange(CGXRange(nRow, 4),  pResultFileHeader.szDateTime);
	m_wndFileListGrid.SetValueRange(CGXRange(nRow, 5),  modelHeader.szName);
	m_wndFileListGrid.SetValueRange(CGXRange(nRow, 6),  extSpace.szModuleName);	
	m_wndFileListGrid.SetValueRange(CGXRange(nRow, 7),  pResultFileHeader.szOperatorID);
	m_wndFileListGrid.SetValueRange(CGXRange(nRow, 8),  strFile);
	
	fclose(fp);
	fp = NULL;

	return TRUE;
}
