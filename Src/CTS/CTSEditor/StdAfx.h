// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//



#if !defined(AFX_STDAFX_H__9E473411_6574_4AA2_9224_16E9CF02DB45__INCLUDED_)
#define AFX_STDAFX_H__9E473411_6574_4AA2_9224_16E9CF02DB45__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#define _AFXDLL

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes

#ifndef _AFX_NO_DB_SUPPORT
#include <afxdb.h>			// MFC ODBC database classes
#endif // _AFX_NO_DB_SUPPORT

#ifndef _AFX_NO_DAO_SUPPORT
#include <afxdao.h>			// MFC DAO database classes
#endif // _AFX_NO_DAO_SUPPORT

#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <grid/gxall.h>
#include <toolkit/secall.h>

#include "ColorButton2.h"

extern CString g_strDataBaseName;
extern CString GetDataBaseName();

#include "../../../BCLib/Include/DataEditor.h"
#include "../../../BCLib/Include/PSCommonAll.h"
//#include <DataEditor.h>			//For Editor data structure defie
#include "./DataBaseClass/DataBaseAll.h"		//For DAO DataBase Class
//#include <PSCommonAll.h>		//for PSCommon.dll


#define NODEVICE "No Setting"
#define EDITOR_REG_SECTION	"Config"

#define RGB_MIDNIGHTBLUE	RGB(25,25,112)
#define RGB_WHITE			RGB(255,255,255)
#define RGB_CORNFLOWERBLUE	RGB(100,149,237)
#define RGB_DARKSLATEBLUE	RGB(72,61,141)
#define RGB_RED				RGB(255,0,0)
#define RGB_BLACK			RGB(0,0,0)
#define RGB_LTGRAY			RGB(192, 192, 192)

#define RGB_LABEL_FONT_STAGENAME RGB(255, 255, 255)
#define RGB_LABEL_BACKGROUND_STAGENAME	RGB(0,0,0)

#define RGB_LABEL_FONT		RGB(255,255,255)
#define RGB_LABEL_BACKGROUND	RGB(72,61,141)

#define RGB_BTN_BACKGROUND	RGB(210, 210, 210)
#define RGB_BTN_FONT		RGB(0,0,0)
#define RGB_BTN_DISBKGND	RGB(255,255,255)

#define EDIT_FONT_SIZE 30

#ifdef _DEBUG
#pragma comment(lib, "../../../Lib/PSCommonD.lib")
#pragma message("Automatically linking with PSCommonD.lib By K.B.H ")
#else
#pragma comment(lib, "../../../Lib/PSCommon.lib")
#pragma message("Automatically linking with PSCommon.lib By K.B.H")
#endif	//_DEBUG

//Graph Class ��� 
#include "../../../BCLib/Include/PEMESSAG.H"
#include "../../../BCLib/Include/PEGRPAPI.H"
#pragma comment(lib, "../../../Lib/PEGRAP32.LIB")
#pragma message("Automatically linking with PEGRAPH32.lib By K.B.H ")


#include "../../../Include/IniParser.h"
extern int g_nLanguage;			
extern CString g_strLangPath;
enum LanguageType {LANGUAGE_KOR=0, LANGUAGE_ENG, LANGUAGE_CHI };

//Cell ����
//#define _NIMH_CELL_		//Cell Type //	_EDLC_CELL_, _NIMH_CELL_, _LIION_
//#define _EDLC_CELL_		//Cell Type //	_EDLC_CELL_, _NIMH_CELL_, _LIION_
#define _LIION_

//#define _PACK_MODEL_TYPE

#ifdef _PACK_MODEL_TYPE
#define MIN_TIME_INTERVAL	50
#else
#define MIN_TIME_INTERVAL	100
#endif

#ifdef _FORMATION_
#define _AUTO_PROCESS_
#endif

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__9E473411_6574_4AA2_9224_16E9CF02DB45__INCLUDED_)
