#if !defined(AFX_SELECTSIMULDATADLG_H__53D4C731_5789_44CE_BD38_476A7E11E974__INCLUDED_)
#define AFX_SELECTSIMULDATADLG_H__53D4C731_5789_44CE_BD38_476A7E11E974__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "GraphWnd.h"
#include "TextParsing.h"
#include "ProgressDlg.h"

// SelectSimulDataDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSelectSimulDataDlg dialog

class CSelectSimulDataDlg : public CDialog
{
// Construction
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	CSelectSimulDataDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSelectSimulDataDlg();

	BOOL IsChange;
	CString m_strErrorMsg;
	BOOL MakeSimulationFile(CString strExistingFileName, CString strNexFileName);
	char m_chMode;	
	long lLimitLow;
	long lLimitHigh;
	BOOL DeleteTempFile(CString strFile = "");
	BOOL isChangeSelFile;
	CString m_strNewSelFile;
	BOOL DrawGraph();
	BOOL LoadData(CString strFile);
	BOOL SetDataSheet(CString strFile);
	BOOL UpdateRawDataInfo(CString strFile);
	CString SelectRawFile(CString strFile = "");
	CString GetDataTitle();
	CString GetPathName();
	STEP * pStep;

	LONG m_lMaxRefI;
	int m_nMinRefI;
	CProgressDlg * m_pProgress;

// Dialog Data
	//{{AFX_DATA(CSelectSimulDataDlg)
	enum { IDD = IDD_SIMUL_DATA_DLG };
	CString	m_strDataTitle;
	CString m_strEndTimeCycle;
	BOOL	m_bCheck;
	//}}AFX_DATA
	CString	m_strSelFile;
	CDCRScopeWnd m_wndGraph;
	CTextParsing m_SheetData;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectSimulDataDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CString m_strTimeEndCycle;
	// Generated message map functions
	//{{AFX_MSG(CSelectSimulDataDlg)	
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnChangeEditDataTitle();
	afx_msg void OnButtonOpenExcel();
	afx_msg void OnButtonDir();

	afx_msg LRESULT OnProgressSetRange(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnProgressStepIt(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnProgressShow(WPARAM wParam, LPARAM lParam);
	
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELECTSIMULDATADLG_H__53D4C731_5789_44CE_BD38_476A7E11E974__INCLUDED_)
