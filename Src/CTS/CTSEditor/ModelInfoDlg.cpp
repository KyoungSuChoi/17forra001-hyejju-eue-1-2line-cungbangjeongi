// ModelInfoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ctseditor.h"
#include "ModelInfoDlg.h"
#include "NewModelInfoDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CModelInfoDlg dialog
extern CString GetDataBaseName();

CModelInfoDlg::CModelInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CModelInfoDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CModelInfoDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CModelInfoDlg::~CModelInfoDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CModelInfoDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CModelInfoDlg"), _T("TEXT_CModelInfoDlg_CNT"), _T("TEXT_CModelInfoDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CModelInfoDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CModelInfoDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}


void CModelInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CModelInfoDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CModelInfoDlg, CDialog)
	//{{AFX_MSG_MAP(CModelInfoDlg)
	ON_BN_CLICKED(IDC_BUTTON_NEW, OnButtonNew)
	ON_BN_CLICKED(IDC_BUTTON_DELETE, OnButtonDelete)	
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CModelInfoDlg message handlers

void CModelInfoDlg::InitGrid()
{
	m_wndModelGrid.SubclassDlgItem(IDC_MODEL_LIST_GRID, this);
	m_wndModelGrid.Initialize();
	m_wndModelGrid.GetParam()->EnableUndo(FALSE);
	m_wndModelGrid.SetColCount(8);

	CString strTemp;
	CRect rect;
	m_wndModelGrid.GetClientRect(&rect);

#ifdef _AUTO_PROCESS_
	m_wndModelGrid.SetColWidth(0, 0, rect.Width()*0.1f);
	m_wndModelGrid.SetColWidth(1, 1, rect.Width()*0.20f);
	m_wndModelGrid.SetColWidth(2, 2, rect.Width()*0.13f);
	m_wndModelGrid.SetColWidth(3, 3, rect.Width()*0.13f);
	m_wndModelGrid.SetColWidth(4, 4, rect.Width()*0.13f);
	m_wndModelGrid.SetColWidth(5, 5, rect.Width()*0.13f);
	m_wndModelGrid.SetColWidth(6, 6, rect.Width()*0.18f);
	m_wndModelGrid.SetColWidth(7, 7, 0);
	m_wndModelGrid.SetColWidth(8, 8, 0);
#else
	m_wndModelGrid.SetColWidth(0, 0, rect.Width()*0.1f);
	m_wndModelGrid.SetColWidth(1, 1, rect.Width()*0.3f);
	m_wndModelGrid.SetColWidth(2, 2, rect.Width()*0.15f);
	m_wndModelGrid.SetColWidth(3, 3, rect.Width()*0.15f);
	m_wndModelGrid.SetColWidth(4, 4, rect.Width()*0.15f);
	m_wndModelGrid.SetColWidth(5, 5, rect.Width()*0.15f);
	m_wndModelGrid.SetColWidth(6, 6, 0);
	m_wndModelGrid.SetColWidth(7, 7, 0);
	m_wndModelGrid.SetColWidth(8, 8, 0);
#endif

	m_wndModelGrid.SetStyleRange(CGXRange(0, 0),	CGXStyle().SetValue("No"));
	m_wndModelGrid.SetStyleRange(CGXRange(0, 1),	CGXStyle().SetValue(TEXT_LANG[0]));//"모델명"
	strTemp.Format(TEXT_LANG[1], m_pDoc->GetCapUnit());//"용량(%s)"
	m_wndModelGrid.SetStyleRange(CGXRange(0, 2),	CGXStyle().SetValue(strTemp));
	m_wndModelGrid.SetStyleRange(CGXRange(0, 3),	CGXStyle().SetValue(TEXT_LANG[2]));//"폭(W)/지름(φ)"
	m_wndModelGrid.SetStyleRange(CGXRange(0, 4),	CGXStyle().SetValue(TEXT_LANG[3]));//"높이(H)"
	m_wndModelGrid.SetStyleRange(CGXRange(0, 5),	CGXStyle().SetValue(TEXT_LANG[4]));//"두께(D)"
	m_wndModelGrid.SetStyleRange(CGXRange(0, 6),	CGXStyle().SetValue(TEXT_LANG[5]));//"바코드"
	m_wndModelGrid.SetStyleRange(CGXRange(0, 7),	CGXStyle().SetValue(TEXT_LANG[6]));//"등록자"
	m_wndModelGrid.SetStyleRange(CGXRange(0, 8),	CGXStyle().SetValue(TEXT_LANG[7]));//"설명"

	m_wndModelGrid.SetStyleRange(CGXRange().SetCols(2, 8), CGXStyle().SetHorizontalAlignment(DT_CENTER));

	m_wndModelGrid.GetParam()->EnableUndo(TRUE);
	m_wndModelGrid.GetParam()->SetSortRowsOnDblClk(TRUE);
	m_wndModelGrid.SetStyleRange(CGXRange().SetTable(), CGXStyle().SetVertScrollBar(TRUE).SetWrapText(FALSE));
}

BOOL CModelInfoDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	ASSERT(m_pDoc);
	
	// TODO: Add extra initialization here
	InitGrid();

	QueryList();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CModelInfoDlg::OnButtonNew() 
{
	// TODO: Add your control notification handler code here
	//새로운 모델 등록
	CString strTemp;
	CNewModelInfoDlg *pDlg = new CNewModelInfoDlg(this);
	
	LPVOID* pData;
	UINT nSize;
	if(AfxGetApp()->GetProfileBinary(EDITOR_REG_SECTION, "LastLogin", (LPBYTE *)&pData, &nSize))
	{
		SCH_LOGIN_INFO loginData;
		if(nSize == sizeof(loginData))
		{
			memcpy(&loginData, pData, nSize);
			pDlg->m_strID = loginData.szLoginID;
		}
		delete [] pData;
	}

	if(pDlg->DoModal() == IDOK)
	{
		//새로운 모델등록 
		CString strSQL;
		CDaoDatabase  db;
		COleVariant data;

				strSQL.Format("SELECT Name FROM ModelInfo WHERE Name = '%s'", pDlg->m_strName);
		try
		{
			db.Open(GetDataBaseName());
			CDaoRecordset rs(&db);
			rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
			if(!rs.IsBOF()&&!rs.IsEOF())
			{
				//존재하는 경우 
				strTemp.Format(TEXT_LANG[8], pDlg->m_strName);//"%s 모델은 이미 등록되어 있습니다."
				AfxMessageBox(strTemp);
			}
			else
			{
				strSQL.Format("INSERT INTO ModelInfo(Name, Width, Height, Depth, Capacity, BCRMask, Description, Creator) VALUES('%s', %f, %f, %f, %f, '%s', '%s', '%s')", 
								pDlg->m_strName, pDlg->m_fWidth, pDlg->m_fHeight, pDlg->m_fDepth, m_pDoc->CUnitTrans(pDlg->m_fCapacity), 
								pDlg->m_strBarCode, pDlg->m_strDescript, pDlg->m_strID);
				db.Execute(strSQL);
			}
			rs.Close();
			db.Close();

			//Grid에 추가 
			int nRow = m_wndModelGrid.GetRowCount()+1;
			m_wndModelGrid.InsertRows(nRow, 1);
			m_wndModelGrid.SetValueRange(CGXRange(nRow, 1), pDlg->m_strName);
			m_wndModelGrid.SetValueRange(CGXRange(nRow, 2), pDlg->m_fCapacity);
			m_wndModelGrid.SetValueRange(CGXRange(nRow, 3), pDlg->m_fWidth);
			m_wndModelGrid.SetValueRange(CGXRange(nRow, 4), pDlg->m_fHeight);
			m_wndModelGrid.SetValueRange(CGXRange(nRow, 5), pDlg->m_fDepth);
			m_wndModelGrid.SetValueRange(CGXRange(nRow, 6), pDlg->m_strBarCode);
			m_wndModelGrid.SetValueRange(CGXRange(nRow, 7), pDlg->m_strID);
			m_wndModelGrid.SetValueRange(CGXRange(nRow, 8), pDlg->m_strDescript);
		}
		catch (CDaoException* e)
		{
			AfxMessageBox(e->m_pErrorInfo->m_strDescription);
			e->Delete();
		}		
	}
	delete pDlg;

	strTemp.Format("%d", m_wndModelGrid.GetRowCount());
	GetDlgItem(IDC_NUM_STATIC)->SetWindowText(strTemp);

}

BOOL CModelInfoDlg::QueryList()
{
		//새로운 모델등록 
	CString strSQL;
	CDaoDatabase  db;
	COleVariant data;
	int nRow = m_wndModelGrid.GetRowCount()+1;

	strSQL = "SELECT * FROM ModelInfo ORDER BY Name";
	try
	{
		db.Open(GetDataBaseName());
		CDaoRecordset rs(&db);
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	

		while(!rs.IsEOF())
		{
				m_wndModelGrid.InsertRows(nRow, 1);
				
				data = rs.GetFieldValue("Name");
				m_wndModelGrid.SetValueRange(CGXRange(nRow, 1), CString(data.pcVal));
				data = rs.GetFieldValue("Capacity");
				m_wndModelGrid.SetValueRange(CGXRange(nRow, 2), m_pDoc->CUnitTrans(data.fltVal, FALSE));
				data = rs.GetFieldValue("Width");
				m_wndModelGrid.SetValueRange(CGXRange(nRow, 3), data.fltVal);
				data = rs.GetFieldValue("Height");
				m_wndModelGrid.SetValueRange(CGXRange(nRow, 4), data.fltVal);
				data = rs.GetFieldValue("Depth");
				m_wndModelGrid.SetValueRange(CGXRange(nRow, 5), data.fltVal);
				data = rs.GetFieldValue("BCRMask");
				m_wndModelGrid.SetValueRange(CGXRange(nRow, 6), CString(data.pcVal));
				data = rs.GetFieldValue("Creator");
				m_wndModelGrid.SetValueRange(CGXRange(nRow, 7), CString(data.pcVal));
				data = rs.GetFieldValue("Description");
				m_wndModelGrid.SetValueRange(CGXRange(nRow, 8), CString(data.pcVal));
				//Grid에 추가 

				rs.MoveNext();
		}
		rs.Close();
		db.Close();
		
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}

	strSQL.Format("%d", m_wndModelGrid.GetRowCount());
	GetDlgItem(IDC_NUM_STATIC)->SetWindowText(strSQL);
	
	return TRUE;
}

void CModelInfoDlg::OnButtonDelete() 
{
	// TODO: Add your control notification handler code here
	CString		strTemp;
	ROWCOL nRow = m_wndModelGrid.GetRowCount();
	if(nRow <1)		return;				//Test Condition is not exist

	CRowColArray	awRows;
	m_wndModelGrid.GetSelectedRows(awRows);		//Get Selected Test Condition
	if(awRows.GetSize() <=0)	return;
	
	if(awRows.GetSize() == 1)
	{
		strTemp.Format(TEXT_LANG[9], m_wndModelGrid.GetValueRowCol(awRows[0], 1) );//"%s 를 삭제 하시겠습니까?"
	}
	else
	{
		strTemp = TEXT_LANG[10];//"선택한 모델들을 삭제 하시겠습니까?."
	}

	if(IDNO == AfxMessageBox(strTemp, MB_YESNO|MB_ICONQUESTION))	
		return;


	CString strSQL;
	CDaoDatabase  db;
	COleVariant data;

	try
	{
		db.Open(GetDataBaseName());
		for(ROWCOL row = awRows.GetSize(); row >0; row--)
		{
			strSQL.Format("DELETE FROM ModelInfo WHERE Name = '%s'", m_wndModelGrid.GetValueRowCol(awRows[row-1], 1));
			db.Execute(strSQL);
			
			m_wndModelGrid.RemoveRows(awRows[row-1], awRows[row-1]);
		}
	
		db.Close();

	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
	}	
	
	strTemp.Format("%d", m_wndModelGrid.GetRowCount());
	GetDlgItem(IDC_NUM_STATIC)->SetWindowText(strTemp);

}

void CModelInfoDlg::OnButtonEdit() 
{
	// TODO: Add your control notification handler code here

	ROWCOL nRow,nCol;
	if(m_wndModelGrid.GetCurrentCell(nRow, nCol) == FALSE)
	{
		return;
	}

	if(nRow < 1)
	{
		return;
	}

	CString strTemp;
	CNewModelInfoDlg *pDlg = new CNewModelInfoDlg(this);
	
	LPVOID* pData;
	UINT nSize;
	if(AfxGetApp()->GetProfileBinary(EDITOR_REG_SECTION, "LastLogin", (LPBYTE *)&pData, &nSize))
	{
		SCH_LOGIN_INFO loginData;
		if(nSize == sizeof(loginData))
		{
			memcpy(&loginData, pData, nSize);
			pDlg->m_strID = loginData.szLoginID;
		}
		delete [] pData;
	}
	CString strOrgName = m_wndModelGrid.GetValueRowCol(nRow, 1);
	pDlg->m_strName = strOrgName;
	pDlg->m_fCapacity = atof(m_wndModelGrid.GetValueRowCol(nRow, 2));
	pDlg->m_fWidth	= atof(m_wndModelGrid.GetValueRowCol(nRow, 3));
	pDlg->m_fHeight = atof(m_wndModelGrid.GetValueRowCol(nRow, 4));
	pDlg->m_fDepth	= atof(m_wndModelGrid.GetValueRowCol(nRow, 5));
	pDlg->m_strBarCode = m_wndModelGrid.GetValueRowCol(nRow, 6);
	pDlg->m_strID = m_wndModelGrid.GetValueRowCol(nRow, 7);
	pDlg->m_strDescript = m_wndModelGrid.GetValueRowCol(nRow, 8);
	
	if(pDlg->DoModal() == IDOK)
	{
		//새로운 모델등록 
		CString strSQL;
		CDaoDatabase  db;
		COleVariant data;
		strSQL.Format("SELECT Name FROM ModelInfo WHERE Name = '%s'", pDlg->m_strName);
		try
		{
			db.Open(GetDataBaseName());
			
/*			CDaoRecordset rs(&db);
			rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
			if(!rs.IsBOF() && !rs.IsEOF())
			{
				//존재하는 경우 
				strTemp.Format("%s 모델은 이미 등록되어 있습니다.", pDlg->m_strName);
				AfxMessageBox(strTemp);
			}
			else
			{
*/				strSQL.Format("UPDATE ModelInfo SET Name = '%s', Width = %f, Height=%f, Depth=%f, Capacity=%f, BCRMask = '%s', Description ='%s', Creator='%s' WHERE Name = '%s'", 
							pDlg->m_strName, pDlg->m_fWidth, pDlg->m_fHeight, pDlg->m_fDepth, m_pDoc->CUnitTrans(pDlg->m_fCapacity), 
							pDlg->m_strBarCode, pDlg->m_strDescript, pDlg->m_strID, strOrgName);
				db.Execute(strSQL);
//			}
//			rs.Close();
			db.Close();

			//Grid에 추가 
			m_wndModelGrid.SetValueRange(CGXRange(nRow, 1), pDlg->m_strName);
			m_wndModelGrid.SetValueRange(CGXRange(nRow, 2), pDlg->m_fCapacity);
			m_wndModelGrid.SetValueRange(CGXRange(nRow, 3), pDlg->m_fWidth);
			m_wndModelGrid.SetValueRange(CGXRange(nRow, 4), pDlg->m_fHeight);
			m_wndModelGrid.SetValueRange(CGXRange(nRow, 5), pDlg->m_fDepth);
			m_wndModelGrid.SetValueRange(CGXRange(nRow, 6), pDlg->m_strBarCode);
			m_wndModelGrid.SetValueRange(CGXRange(nRow, 7), pDlg->m_strID);
			m_wndModelGrid.SetValueRange(CGXRange(nRow, 8), pDlg->m_strDescript);
		}
		catch (CDaoException* e)
		{
			AfxMessageBox(e->m_pErrorInfo->m_strDescription);
			e->Delete();
		}		
	}
	delete pDlg;
	
	strTemp.Format("%d", m_wndModelGrid.GetRowCount());
	GetDlgItem(IDC_NUM_STATIC)->SetWindowText(strTemp);
}
