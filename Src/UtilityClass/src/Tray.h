// Tray.h: interface for the CTray class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TRAY_H__853B6F3B_7B61_4D8C_80EE_6BABA2032F4D__INCLUDED_)
#define AFX_TRAY_H__853B6F3B_7B61_4D8C_80EE_6BABA2032F4D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CTray  
{
public:
	CString GetCellSerial(int nChIndex);
	void	SetCellSerial(int nChIndex, CString str);	
	PNE_RESULT_CELL_SERIAL * GetResultCellSerial();
	BOOL	GetBcrRead();					
	void	SetBcrRead(BOOL bRead = TRUE)	{	m_bBCRScaned = bRead;	}
	BOOL	GetTestReserved()			{	return m_bTestReserved;	}
	void	SetTestReserved(BOOL bReserved = TRUE)	{	m_bTestReserved = bReserved;	}	
	void	SetFirstProcess(BOOL bFirst = TRUE)			{ m_bNewProcTray = bFirst;	}
	BOOL	IsWorkingTray();
	void	SetTrayData(CTray &tray);
	BOOL	UpdateStartData();
	void	SetNewTray();
	BOOL	IsNewProcedureTray();
	BOOL	DeleteTray(CString strDestTrayNo = "");
	BOOL	AddNewTray(CString trayNo, CString trayName, CString UserName, CString descrip, int trayType = 0);
	BOOL	WriteTrayData();
	BOOL	LoadTrayData(CString strLoadTrayNo = "", BOOL bAllData = TRUE);
	BOOL	ResetTrayData();
	void	InitData(BOOL	bAllData = TRUE);
	long	GetProcessSeqNo()	{	return lTestKey;		}

	void	SetTrayNo(CString strNo)	{	m_strTrayNo = strNo;	}
	CString GetTrayNo()	{	return m_strTrayNo;	}
	long	GetTraySerialNo()	{	return m_lTraySerial;		}
	CString	GetTrayUserName()	{	return m_strTrayName;		}
	int		GetInputCellCnt()   {	return lInputCellCount;		}
	void    SetInputCellCnt( int nInputCellCnt ) { lInputCellCount = nInputCellCnt; }  

	CTray(CString trayNo);
	CTray(long traySerial);
	CTray();
	virtual ~CTray();
	void	operator = (const CTray &tray); 

//	CString			strCellCode;
//	CString			strCellGradeCode;
//	int				m_nTrayCellType;

	//작업 시작시 결정되는 정보
	long			ModuleID;
	long			GroupIndex;
	CString			strJigID;
	long			lModelKey;					//Battery Model DB Primary Key
	long			lTestKey;					//Test	Paramary Key
	CString			strLotNo;					//사용자 입력 Lot No  Max 31Byte
	CString			strTypeSel;					//20201211 ksj
	CString			strTestSerialNo;			//현재 공정에 대한 고유 번호 23Byte(날짜+Random()수를 이용한 고유 번호)
	long			lCellNo;
	long			lInputCellCount;			//초기 공정에 입력수량 
	CString			strOperatorID;
	CString			m_strFileName;

	//시험후 결과로 산출되는 data
	WORD			nNormalCount;					//각step에서 입력되는 정상인 수 
	WORD			nFailCount;						//각 Step을 완료후 불량은 수 
	COleDateTime	testDateTime;
	BYTE			cellCode[EP_MAX_CH_PER_MD];			//256
	BYTE			cellGradeCode[EP_MAX_CH_PER_MD];	//256
	
	BYTE			m_nTabDeepth;
	BYTE			m_nTrayHeight;
	BYTE			m_nTrayType;
	BYTE			m_nContactErrlimit;			// Cell 체크 에러 카운터
	BYTE			m_nCapaErrlimit;			// 채널 Cut-off 확인
	BYTE			m_nChErrlimit;				// 채널 에러 카운터
		
	// 1. Unit 에서 EMG가 발생하면 다음번에 들어오는 결과데이터 저장 후 
	// 2. 파일이름을 XXX_EMG.fmt 로 저장한다.	
	// 3. 사용자가 Stop 한 경우 파일명에 STP를 붙인다.
	int				m_nResultFileType;			// 0:Normal, 1:EMG, 2:User Stop

protected:
	//등록정보
	long			m_TrayType;					//Tray Type
	CString			m_strTrayName;
	CString			m_strUserName;				//Tray 등록자 
	CString			m_strDescription;
	long			m_lTraySerial;
	CString			m_strTrayNo;
	COleDateTime	m_registedTime;	

	//작업 관련  Flag
	BOOL			m_bNewProcTray;
	BOOL			m_bBCRScaned;
	BOOL			m_bTestReserved;

	//20090920
	PNE_RESULT_CELL_SERIAL m_ResultCellSerial;		//각 Cell의 Serial No

};

#endif // !defined(AFX_TRAY_H__853B6F3B_7B61_4D8C_80EE_6BABA2032F4D__INCLUDED_)
