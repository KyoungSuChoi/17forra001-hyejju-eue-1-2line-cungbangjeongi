// UserExpDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "UserExpDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUserExpDlg dialog


CUserExpDlg::CUserExpDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUserExpDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CUserExpDlg)
	m_strExpression = _T("");
	//}}AFX_DATA_INIT
}


void CUserExpDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CUserExpDlg)
	DDX_Text(pDX, IDC_EXPRESSION_EDIT, m_strExpression);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CUserExpDlg, CDialog)
	//{{AFX_MSG_MAP(CUserExpDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUserExpDlg message handlers

void CUserExpDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);

	FormulaCheck(m_strExpression);

	m_strExpression.Empty();
	UpdateData(FALSE);
	return;

	CDialog::OnOK();
}

BOOL prcd(CString a, CString b)
{
	if(a == "(")	return FALSE;

	if(b == "(")
	{
		if(a == ")")	return TRUE;	//괄호 사이 연산자가 없다.
		else return FALSE;
	}
	else if(b == ")")
	{
		return TRUE;
	}
	if(a == "+" || a == "-")
	{
		if(b == "+" || b == "-")	return TRUE;
		else return FALSE;
	}

/*	if(a == "*" || a == "/")
	{
		if("b" == "*" || "b" == "/")	return TRUE;
	}
*/

	return TRUE;
}

CString GetTopOperator(CStringArray &strOperandArray)
{
	int size = strOperandArray.GetSize();
	if(size < 1)	return "";

	CString data = strOperandArray[size-1];
	strOperandArray.RemoveAt(size-1);
	return data;
}


BOOL CUserExpDlg::FormulaCheck(CString strExpression)
{
	if(strExpression.IsEmpty())		return	 FALSE;
	
	long dataType;
	CString strRemain(strExpression);
	CString strOperand, topOperator;

	CStringArray strPostFixArray;
	CStringArray strOperandArray;

	int i = 0;

	//conver to infix to postfix expression
	while(!strRemain.IsEmpty())
	{
		strOperand = getNextOP(strRemain, dataType);
		TRACE("strOperand >> %s\n", strOperand);

		if(dataType == 1 || dataType == 2)	//operand  digit or 
		{
			strPostFixArray.Add(strOperand);
		}
		else  //operator or parentheses
		{
			topOperator = GetTopOperator(strOperandArray);
			while(!topOperator.IsEmpty() && prcd(topOperator, strOperand))
			{
				strPostFixArray.Add(topOperator);
				topOperator = GetTopOperator(strOperandArray);
			}
			
			if(!topOperator.IsEmpty())		//prcd(topOperator, strOperand) == FALSE;
			{
				strOperandArray.Add(topOperator);
			}

			if(topOperator.IsEmpty() || strOperand != ")")
			{
				strOperandArray.Add(strOperand);
			}
		}

		TRACE("PostFix >> ");
		for(i =0; i<strPostFixArray.GetSize(); i++)
		{
			TRACE("[%s]", strPostFixArray[i]);
		}
		TRACE("\n");

		TRACE("Operand >> ");
		for(i =0; i<strOperandArray.GetSize(); i++)
		{
			TRACE("[%s]", strOperandArray[i]);
		}
		TRACE("\n");
	}

	int count = strOperandArray.GetSize();
	while(count > 0)
	{
		CString data = strOperandArray[count-1];
		strPostFixArray.Add(data);
		count--;
	}

	TRACE("PostFix >> ");
	for(int i =0; i<strPostFixArray.GetSize(); i++)
	{
		TRACE("[%s]", strPostFixArray[i]);
	}
	TRACE("\n");
	
	return TRUE;
}


CString CUserExpDlg::getNextOP(CString &strExp, long &dataType)
{
	CString strReturn;
	
	CString strExpression(strExp);
	if(strExpression.IsEmpty())
	{
		dataType =0;
		return strReturn;
	}

	strExpression.TrimLeft(" ");

	CString first;
	first = strExpression.Left(1);
	if(first == "(" || first == ")" || first == "+" || first == "-" || first == "*" || first == "/")
	{
		strExp = strExpression.Mid(1);
		strReturn = first;
		dataType = 3;
	}
	else 
	{
		int i = 0;
		char szName[128];
		for(i=0; i<strExpression.GetLength(); i++)
		{
			first = strExpression[i];
			if(first == "(" || first == ")" || first == "+" || first == "-" || first == "*" || first == "/")
			{
				break;
			}
			else
			{
				szName[i] = strExpression[i];
			}
		}
		szName[i] = '\0';
		strExp = strExpression.Mid(i);
		strReturn = szName;
		dataType = 2;
	}
	return strReturn;
}
