#pragma once

typedef enum PACKET_ERROR
{
	PACKET_NOERROR
	, PACKET_NOTENOUGH_LENGTH
	, PACKET_HEAD_CHECK_FAIL
	, PACKET_PACKETNO_FAIL
	, PACKET_CHECKSUM_FAIL
	, PACKET_SYSTEM_FAIL

	, PACKET_END
};

static const TCHAR gszPACKET_ERROR[PACKET_END][32] = 
{
	_T("PACKET_NOERROR")
	, _T("PACKET_NOTENOUGH_LENGTH")
	, _T("PACKET_HEAD_CHECK_FAIL")
	, _T("PACKET_PACKETNO_FAIL")
	, _T("PACKET_CHECKSUM_FAIL")
	, _T("PACKET_SYSTEM_FAIL")
};

class CSBCPacketBox : public CSBCSyncParent<CSBCPacketBox>
{
public:
	CSBCPacketBox(void);
	~CSBCPacketBox(void);

//	VOID Init(VOID) { CSyncObj Sync; m_dwCurSendNo = 0; m_dwLastRecvNo = 0; }
	BOOL WrapPacket(EP_MSG_HEADER&, BYTE *, BYTE *, DWORD &rdwLen);
	PACKET_ERROR WrapOffPacket(EP_MSG_HEADER&, BYTE *, BYTE *, DWORD &);
//	USHORT fnCRC16_ccitt(const BYTE *buf, int len);

private:
	DWORD m_dwCurSendNo;
	DWORD m_dwLastRecvNo;
};
