// ClientSocket.cpp: implementation of the CClientSocket class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DataClt.h"
#include "ClientSocket.h"

#include "RawSocket.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
extern BOOL WriteLog(char *szLog);
BOOL ParsingCommand(CWizReadWriteSocket *pSock, LPVOID lpMsgHeader, LPVOID lpReadBuff, CClientSocket *lpParent);

CClientSocket::CClientSocket(char *szIpAddress)
: m_bConnected(FALSE), m_pThread(NULL)
{
	if(szIpAddress)
	{
		sprintf(m_szServerIpAddress, szIpAddress);
	}

	m_hShutDownEvent = ::CreateEvent(NULL, FALSE, FALSE, NULL);	//Create Shutdown Event
	::ResetEvent(m_hShutDownEvent);
	m_hWriteEvent = ::CreateEvent(NULL, TRUE, FALSE, NULL);		//Create Write Event
	::ResetEvent(m_hWriteEvent);
	m_hReadEvent = ::CreateEvent(NULL, TRUE, FALSE, NULL);
	::ResetEvent(m_hReadEvent);
	
	m_nRxLength = 0;
	m_nTxLength = 0;

//	m_nChNum = EP_BATTERY_PER_TRAY;
	m_bRunThread = FALSE;
	
//	m_nServerID = FORM_SERVER;

//	memset(&m_sServer, 0, sizeof(EP_FORMSERVER_VER));
//	m_lpOcvBuf = NULL;
//	m_lpImpBuf = NULL;
//	m_lpCapBuf = NULL;
}

CClientSocket::~CClientSocket()
{
	::CloseHandle(m_hWriteEvent);
	::CloseHandle(m_hShutDownEvent);
	::CloseHandle(m_hReadEvent);

	StopWork();
/*	if(m_pThread != NULL)
	{
		delete m_pThread;
		m_pThread = NULL;
	}
*/
/*	if(m_bConnected)
	{
		TRACE("You Must Call StopWork() before your Application is closed\n");
		ASSERT(0);
		StopWork(FALSE);
	}
*/
}

void CClientSocket::SetIpAddress(LPSTR szIP)
{
	sprintf(m_szServerIpAddress, "%s", szIP);
}

BOOL CClientSocket::StartWork(LPSTR szIP)
{
	if(m_bRunThread)	return FALSE;	//이미 접속을 시도중이면 
	if(m_bConnected)	return FALSE;	//Already Connected
	
	SetIpAddress(szIP);
	
	WaitShutdown();
	
	m_pThread = AfxBeginThread(ClientRun, this);				//Start Connection Thread
	if (m_pThread == NULL)		AfxThrowMemoryException();			//Start Error
	m_pThread->m_bAutoDelete = FALSE;
	m_bRunThread = TRUE;
	return TRUE;
}

int CClientSocket::StopWork(BOOL bWait)
{
	if(bWait)
	{
		WaitShutdown();				//Wait to Client side socket closed
	}

	TRACE("Client Disconnected \n");
	ASSERT(m_bConnected == FALSE);
	return TRUE;
}

BOOL CClientSocket::WaitShutdown()
{
	DWORD	dwExitCode;
	int		state;

	if(m_pThread == NULL)	return TRUE;

	if(m_bConnected)
	{
		::SetEvent(m_hShutDownEvent);
		TRACE("Waiting  Client Socket close\n");
		state= ::WaitForSingleObject(m_pThread->m_hThread, INFINITE);
		if(state == WAIT_OBJECT_0)
		{
			TRACE("Client Socket closed\n");
		}
		else
		{
			TRACE("Client Socket close Fail\n");
		}

		::ResetEvent(m_hShutDownEvent);				//Auto Reset Shutdown Event Handle
		::GetExitCodeThread(m_pThread->m_hThread, &dwExitCode);
		
		if(dwExitCode == STILL_ACTIVE)
		{
			WriteLog("Client Connection Thread Terminate Fail.\n"); 
			return FALSE;
		}
	}
		
	if(m_pThread)
	{
		delete m_pThread;
		m_pThread = NULL;
	}
	return TRUE;
}

UINT CClientSocket::ClientRun(LPVOID pParam)
{
	ASSERT(pParam != NULL);
	CClientSocket *pParent = (CClientSocket *)pParam;
	TRACE("Current Client Thread ID = 0x%X\n", AfxGetThread()->m_nThreadID);	//Print the current thread ID in the Debug Window
	
	// Create client side socket to communicate with client.
	char	msg[128];								//Display Message Buffer

	CWizReadWriteSocket clientSide;
	if(clientSide.Connect(pParent->m_szServerIpAddress, _DATA_SVR_TCPIP_PORT) == FALSE)	//connect to Data Server
	{
		pParent->m_bConnected = FALSE;
		sprintf(msg, "Connection to %s Fail.\n", pParent->m_szServerIpAddress);
		WriteLog(msg);
		if(pParent->m_hMsgWnd)
			::SendMessage(pParent->m_hMsgWnd, EPWM_DB_SYS_DISCONNECTED, pParent->m_nSystemID, 0);
		
		pParent->m_bRunThread = FALSE;
		return FALSE;
	}

/////////////////////////////////////////
	UINT	nPortNo;
	int		msgReadState  = _MSG_ST_CLEAR;			//Read Done Flag
	int		reqSize = 0, offset = 0, nRtn =0;		//Data Point Indicator

	size_t msgHeadersize = SizeofHeader();			//Message Header Size
	LPEP_MSG_HEADER	 lpMsgHeader;					//Message Header	
	lpMsgHeader = new EP_MSG_HEADER;
	ASSERT(lpMsgHeader);

	LPVOID lpReadBuff = NULL;						//Body read Buffer

	HANDLE 	*lpHandles;								//Handle
	lpHandles = new HANDLE[3];
	ASSERT(lpHandles);
//////////////////////////////////////

	WSAEVENT myObjectEvent = ::WSACreateEvent();	//Event
	lpHandles[0] = myObjectEvent;					//Read, Server Close Event
	lpHandles[1] = pParent->m_hShutDownEvent;		//Client close

	::WSAEventSelect(clientSide.H(), myObjectEvent, FD_READ|FD_CLOSE);		//select Read and close event
	LPWSANETWORKEVENTS lpEvents = new WSANETWORKEVENTS;
	ASSERT(lpEvents);
	
	clientSide.GetPeerName(msg, EP_IP_NAME_LENGTH, nPortNo);	//Get Server Side Ip Address and Port Number
	TRACE("Conncted to Data Server %s, Port %d\n", msg, nPortNo);
////////////////////////////////////////////////////////////////////////////////

	ZeroMemory(lpMsgHeader, msgHeadersize);
	lpMsgHeader->nID = pParent->m_nSystemID;
	lpMsgHeader->nCommand = DB_CMD_DATA_SERVER_VER;
	lpMsgHeader->nLength = msgHeadersize+sizeof(EP_SYSTEM_VER);

	EP_SYSTEM_VER dataServer;
	ZeroMemory(&dataServer, sizeof(EP_SYSTEM_VER));
	dataServer.nID =  pParent->m_nSystemID;						//Device ID
	dataServer.nSystemType = pParent->m_nSystemType;
	dataServer.nVersion = EP_SYSTEM_SERVER_PROTO_VER;									//Version

	memcpy(pParent->m_szTxBuffer, lpMsgHeader, msgHeadersize);
	memcpy((char *)pParent->m_szTxBuffer+msgHeadersize, &dataServer, sizeof(dataServer));
	nRtn = clientSide.Write(pParent->m_szTxBuffer, lpMsgHeader->nLength);
	if(nRtn != lpMsgHeader->nLength)
	{
		sprintf(msg, "Server Information Send Fail. %d/%d\n", nRtn, lpMsgHeader->nLength);
		goto _CLOSE_MODULE;
	}

	nRtn = ::WaitForMultipleObjects(2, lpHandles, FALSE, /*_EP_MSG_TIMEOUT*/INFINITE);		//Wait for Read or Write Event
	if(WAIT_OBJECT_0 != nRtn)
	{
		sprintf(msg, "Read Ack Time Out. %d", nRtn);
		goto _CLOSE_MODULE;
	}

/*	nRtn  = SizeofHeader()+sizeof(EP_REAPONSE);
	LPVOID	lpData;
	lpData = new char[nRtn];

	if((nPort = clientSide.Read(lpData, nRtn)) != nRtn)
	{
		sprintf(msg, "Response of Data Server Version Command is Fail 1 %d/%d", nPort, nRtn);
		TRACE("%s\n", msg);
		goto _CLOSE_MODULE;
	}
*/
	EP_RESPONSE_COMMAND response;

//	EP_RESPONSE	response;
//	nRtn = clientSide.Read(lpMsgHeader, msgHeadersize);
	Sleep(100);
	nRtn = clientSide.Read(&response, sizeof(response));
	if(nRtn !=  sizeof(response) || nRtn != response.msgHeader.nLength)
	{
		sprintf(msg, "Response of Data Server Version Command is Fail. %d / %d\n", nRtn, response.msgHeader.nLength);
		goto _CLOSE_MODULE;
	}

	if(response.msgHeader.nCommand != EP_CMD_RESPONSE || response.msgBody.nCode != EP_ACK)
	{
		sprintf(msg, "Response of Data Server Version Command is Fail. Cmd  = 0x%x, %d\n", response.msgHeader.nCommand, response.msgBody.nCode);
		goto _CLOSE_MODULE;
	}

	lpHandles[2] = pParent->m_hWriteEvent;			//Write to Server
	pParent->m_bConnected = TRUE;
	if(pParent->m_hMsgWnd)
		::SendMessage(pParent->m_hMsgWnd, EPWM_DB_SYS_CONNECTED, pParent->m_nSystemID, 0);

	WriteLog("Data Client Connected to Server.\n");

/////////////////////////////////////////////////////////////////////////////////////////////
	//-----------------start Socket Read/Write--------------------------------//

	while(1)
	{	//Wait for Read or Write Event
		nRtn = ::WaitForMultipleObjects(3, lpHandles, FALSE, INFINITE);
			//INFINITE);		//Wait for Read or Write Event
		switch(nRtn)
		{
		case WAIT_OBJECT_0:		// Client socket event FD_READ
			if(SOCKET_ERROR == ::WSAEnumNetworkEvents(clientSide.H(), myObjectEvent, lpEvents))		//Get Client side Socket Event
			{ 
				sprintf(msg, "Socket Error. Code %d\n", WSAGetLastError());
				goto _CLOSE_MODULE;
			}

			//---------------Data Read Event ---------------------------//
			if (lpEvents->lNetworkEvents & FD_READ)		//Read Event
			{
				//--------------------Message Head Read -----------------//
				if( msgReadState & (_MSG_ST_CLEAR | _MSG_ST_HEADER_REMAIN))				//Read Message Header
				{
					if(msgReadState & _MSG_ST_CLEAR)	reqSize = msgHeadersize;		
					offset = msgHeadersize - reqSize;
					nRtn = clientSide.Read((char *)lpMsgHeader+offset, reqSize);		//Read Message Header
					
					if (nRtn != reqSize) 
					{
						if(nRtn == SOCKET_ERROR)	//Socket Error
						{
							msgReadState = _MSG_ST_CLEAR;
							sprintf(msg, "Message Header Read Fail. Code %d\n", WSAGetLastError());
							WriteLog(msg);
							break;
						}
						else
						{
							msgReadState = _MSG_ST_HEADER_REMAIN;
							TRACE("Message Header Data %d Byte Read %d Byte Remain\n", nRtn, reqSize);
						}
						reqSize -= nRtn;
					}
					else		//Read Done
					{
//						TRACE("Module %d Message Header Receive. G%d-C%d::Cmd 0x%x(%d)\n", nModuleID, lpMsgHeader->wGroupNum, lpMsgHeader->wChannelNum, lpMsgHeader->nCommand, lpMsgHeader->nLength);
						if(lpMsgHeader->nLength <= msgHeadersize )	//Incorrect Message if reqSize <  msgHeadersize
						{
							msgReadState = _MSG_ST_CLEAR;
						}
						else
						{
							msgReadState = _MSG_ST_HEAD_READED;
						}
					}
				} 
				else	//--------------------Message Body Read -----------------////Read Body And Command Parsing	
				{
					if (msgReadState & _MSG_ST_HEAD_READED)
					{
						reqSize = lpMsgHeader->nLength - msgHeadersize;		
						if(lpReadBuff)
						{
							delete[] lpReadBuff;
							lpReadBuff = NULL;
						}
						lpReadBuff = new char[reqSize];
						ASSERT(lpReadBuff);
					}

					offset = (lpMsgHeader->nLength - msgHeadersize) - reqSize;
					nRtn = clientSide.Read((char *)lpReadBuff+offset, reqSize);
					if (nRtn != reqSize) 
					{
						if(nRtn == SOCKET_ERROR)	//Socket Error
						{
							msgReadState = _MSG_ST_CLEAR;
							sprintf(msg, "Message Body Read Fail. Code %d\n", WSAGetLastError());
							WriteLog(msg);
							break;
						}
						else
						{
							msgReadState = _MSG_ST_REMAIN;
//							TRACE("Module %d Message Body Data %d Byte Read %d Byte Remain\n", nModuleID, nRtn, reqSize);
						}
						reqSize -= nRtn;
					}
					else		//Read Done
					{
						msgReadState = _MSG_ST_CLEAR;
					}
					
				}
				
				///---------------Parsing Command---------------------///
				if(	msgReadState & _MSG_ST_CLEAR)
				{
					ParsingCommand(&clientSide, lpMsgHeader, lpReadBuff, pParent);	//Parsing Recevied Command 

/*					if(lpMsgHeader->nCommand == EP_CMD_AUTO_GP_STEP_END_DATA && nEndDataSize == TRUE)
					{
						nEndDataSize = lpMsgHeader->nLength-SizeofHeader();
						if(lpEndDataBuff != NULL)
						{
							delete[] lpEndDataBuff;
							lpEndDataBuff = NULL;
						}	
						lpEndDataBuff = new char[nEndDataSize];
						ASSERT(lpEndDataBuff);
							
						memcpy(lpEndDataBuff, lpReadBuff, nEndDataSize);
						if(m_hMessageWnd)
							::PostMessage(m_hMessageWnd, EPWM_STEP_ENDDATA_RECEIVE, 
											(WPARAM)MAKELONG(lpMsgHeader->wGroupNum-1, m_stModule[nModuleIndex].sysParam.nModuleID), 
											(LPARAM)lpEndDataBuff
								 );			//Send to Parent Wnd to Step End Data Received
						//TRACE("Module %d::Step Result Data Received\n", m_stModule[nModuleIndex].sysParam.nModuleID);
					}
*/				}
			}
			else if(lpEvents->lNetworkEvents == FD_CLOSE)	//Client Disconnected
			{
				sprintf(msg, "Data Server disconnect event detected. code %d.\n", lpEvents->lNetworkEvents);
				goto _CLOSE_MODULE;
			}
			else											//Error
			{
				if (lpEvents->lNetworkEvents != 0) 
				{
					sprintf(msg, "Data Client UnKnown event %d detected. Disconnect Data Client\n", lpEvents->lNetworkEvents);
					goto _CLOSE_MODULE;
				}
			}
			break;
		
		case WAIT_OBJECT_0 + 1:		//Client Side shutdown event
			sprintf(msg, "Data Client shutdown event event detected.\n");
			goto _CLOSE_MODULE;
		
		case WAIT_OBJECT_0 + 2:		// client side write event
			nRtn = clientSide.Write(pParent->m_szTxBuffer, pParent->m_nTxLength);
			if(nRtn != pParent->m_nTxLength)
			{
				sprintf(msg, "Data Client Socket write Error, %d Byte Write - %d Byte remain\n", nRtn,   pParent->m_nTxLength - nRtn);
			}
			pParent->m_nTxLength = 0;
			::ResetEvent(pParent->m_hWriteEvent);
			break;

		case WAIT_TIMEOUT:
			sprintf(msg, "Receive Timeout. Disconnect Data Client.\n");
			goto _CLOSE_MODULE;

		default: //WAIT_TIMEOUT or WAIT_FAILED ->>something wrong event
			sprintf(msg, "Error Code %d detected[%d]. Disconnect Data Client.\n", GetLastError(), nRtn);
			goto _CLOSE_MODULE;
		}
	}

_CLOSE_MODULE:				//Release Memory and close client socket
	WriteLog(msg);
	clientSide.Close();

	pParent->m_bConnected = FALSE;
	if(pParent->m_hMsgWnd)
	{
		::PostMessage(pParent->m_hMsgWnd, EPWM_DB_SYS_DISCONNECTED, pParent->m_nSystemID,	0);			//Send to Parent Wnd to Module State change
	}
	
	if(lpReadBuff)
	{
		delete [] lpReadBuff;		lpReadBuff = NULL;
	}
	delete lpMsgHeader;			lpMsgHeader =  NULL;
	delete [] lpHandles;		lpHandles = NULL;
	delete lpEvents;			lpEvents = NULL;

	pParent->m_bRunThread = FALSE;
	return TRUE;
}

BOOL CClientSocket::SendMsg(LPVOID pData, size_t nLength)
{
	ASSERT(nLength >= 0 && nLength < _EP_TX_BUF_LEN);
	memcpy(m_szTxBuffer, pData, nLength);
	m_nTxLength = nLength;
	::SetEvent(m_hWriteEvent);
	return TRUE;
}

void CClientSocket::SetWnd(HWND hWnd)
{
	m_hMsgWnd = hWnd;
}

//Parsing Response Command 
BOOL ParsingCommand(CWizReadWriteSocket *pSock, LPVOID lpMsgHeader, LPVOID lpReadBuff, CClientSocket *lpParent)
{
	ASSERT(pSock);
	ASSERT(lpMsgHeader);

	INT		wrSize = 0;
	LPEP_MSG_HEADER lpHeader = (LPEP_MSG_HEADER)lpMsgHeader;
	int nResult = EP_ACK;

	switch(lpHeader->nCommand)
	{
	case EP_CMD_RESPONSE:
		wrSize = lpHeader->nLength - SizeofHeader();		//Data Size Check
		if(wrSize == sizeof(EP_RESPONSE))
		{
			memcpy(&lpParent->m_CommandAck,  lpReadBuff, sizeof(EP_RESPONSE));
		}
		else
		{
			lpParent->m_CommandAck.nCode = EP_SIZE_MISMATCH;
		}
//		TRACE("Response Command %d\n", lpParent->m_CommandAck.nCode);
		::SetEvent(lpParent->m_hReadEvent);			
		return TRUE;

	default:
		break;
	}

////////////////////////////Send Reaponse	/////////////////////////////////////
	LPEP_MSG_HEADER lpResponseHeader = NULL;
	LPVOID lpBuffer = NULL;
	int nBufferSize =0;
//	EP_REAPONSE *pResponse= NULL;

	switch(lpHeader->nCommand)
	{
	case DB_CMD_HEARTBEAT:
//		TRACE("Received Heartbeat Packet\n");
	case DB_CMD_DATA_SERVER_VER:							//Send Response Command
		nBufferSize = sizeof(EP_RESPONSE)+SizeofHeader();					//Response size
		lpBuffer = new char[nBufferSize];
		memcpy(lpBuffer, lpHeader, SizeofHeader());
		lpResponseHeader = (LPEP_MSG_HEADER)lpBuffer;
		lpResponseHeader->nLength = nBufferSize;
		lpResponseHeader->nCommand = EP_CMD_RESPONSE;
		memcpy((char *)lpBuffer + SizeofHeader(), &nResult, sizeof(nResult));
		break;

	default:
		TRACE("Invalied Command Receive\n");
		return FALSE;
	}
	nResult = pSock->Write(lpBuffer, nBufferSize);		//Reply to Module

#ifdef _DEBUG
	if(nResult != lpResponseHeader->nLength)
	{
		char	msg[512];
		sprintf(msg, "Socket Write Fail %d/%d", nResult, lpResponseHeader->nLength);
		TRACE("%s\n", msg);
		AfxMessageBox(msg);
	}
#endif
/////////////////////////////////////////////////////////////////////////////

	if(lpBuffer)					//delete reply Message Body		
	{
		delete[] lpBuffer;
		lpBuffer = NULL;
	}
	return TRUE;
}

LPSTR CClientSocket::GetClientIP()
{
	if(strlen(m_szClientIPAddress) <= 0)
	{
		char szTemp[128];
		WSADATA wsaData;
		WORD wVersionRequested = MAKEWORD( 2, 2 );		//WinSock Version Greater than 2.0
		int  err = ::WSAStartup( wVersionRequested, &wsaData );
		if (err != 0) 
		{
			// Tell the user that we couldn't find a usable WinSock DLL.
			sprintf(szTemp, "Couldn't find a usable WinSock DLL. %d\n", err);
			WriteLog(szTemp);
			return "";
		}	
		//Get Server IP
		struct hostent *serverHostent;
		struct in_addr sin_addr;
		::gethostname( szTemp,100 );
		serverHostent = ::gethostbyname( szTemp );
		strcpy( szTemp, serverHostent->h_name );
		memcpy( &sin_addr, serverHostent->h_addr, serverHostent->h_length);
		strcpy( m_szClientIPAddress, inet_ntoa( sin_addr ) );
		
		::WSACleanup();

	}
	return m_szClientIPAddress;
}
