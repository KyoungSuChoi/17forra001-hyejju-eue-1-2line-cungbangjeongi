//EPDLL_API_HEADER_FILE
#ifndef _EP_FORMATION_DATA_SERVER_DLL_DEFINE_H_
#define _EP_FORMATION_DATA_SERVER_DLL_DEFINE_H_

#define	_DATA_SVR_TCPIP_PORT	2002
#define	MAX_DATA_CLIENT			8

#define INDEX_FORM_OP			0
#define INDEX_IROCV				1
#define INDEX_GRADING			2
#define _TIMER_HEARTBEAT		1000

//////////////////////////////////////////////////////////////////////////
//	Data Server Command
//////////////////////////////////////////////////////////////////////////
#define EP_SYSTEM_SERVER_PROTO_VER	0x00001000
#define EP_DB_TEST_LOG_DELETE		0x0001						//TestLog Table에서 목록 삭제

#define DBWM_DB_SYS_CONNECTED		WM_USER+2101
#define DBWM_DB_SYS_DISCONNECTED	WM_USER+2102	
#define DBWM_PROCEDURE_DATA			WM_USER+2103	//Procedure Data
#define DBWM_DB_MANAGE_DATA			WM_USER+2104	//DataBase Management Cmd			//2004/4/27
#define DBWM_STEP_ENDDATA_RECEIVE	WM_USER+2105
#define DBWM_TEST_RESULT			WM_USER+2106	//Test Result Data

//DB Command
#define DB_CMD_HEARTBEAT			EP_CMD_HEARTBEAT
#define DB_CMD_RESPONSE				EP_CMD_RESPONSE
#define DB_CMD_DATA_SERVER_VER		0x00003000
#define DB_CMD_TEST_RESULT			0x00003001
#define DB_CMD_PROCEDURE_DATA		0x00003002
#define DB_CMD_DATA_MANAGE			0x00003003				//2004/4/27	DataBase 조작 명령어를 DB Server에 전송 
#define DB_CMD_STEP_RESULT			0x00003004				//2006/9/5	Step end data를 모두 전송한다.

#define	DB_MD_SYSTEM_DATA			EP_MD_SYSTEM_DATA

typedef struct tag_DataBase_Manage {
	int nType;
	char szData[256];
} EP_DATABASE_MANAGE_DATA;

//Module에서 시행 되는 정보를 DataBase에 저장 하기 위한 Format
typedef struct tag_ModuleTestData {
	char	szModuleName[32];
	int		nModuleID;
	WORD	wModuleGroupNo;
	WORD	wJigIndex;
	char	szDateTime[EP_DATE_TIME_LENGTH+1];			//Init. -> SendConditionStep()에서
	char	szModuleIP[EP_IP_NAME_LENGTH+1];			//SaveResultFileHeader()에서
	char	szTrayNo[EP_TRAY_NAME_LENGTH+1];			//Init. -> SendConditionStep()에서
	char	szLotNo[EP_LOT_NAME_LENGTH+1];				//Init. -> SendConditionStep()에서
	char	szOperatorID[EP_MAX_LONINID_LENGTH+1];
	char	szTestSerialNo[EP_TEST_SERIAL_LENGTH+1];
	long	lTestSeqenceNo;
	long	lTestID;
	char	szTestName[EP_TEST_NAME_LENGTH+1];
	long	lModelID;
	char	szModelName[EP_TEST_NAME_LENGTH+1];
	char	szResultFileName[256];
	int		nCellNo;
	WORD	procedureType;
	WORD	wCellInTrayCount;
	long	nInputCellCount;
	long	bAutoProcess;
	long	lReserved[4];
	DB_MD_SYSTEM_DATA	mdSysData;
	_MAPPING_DATA	sensorMap1[EP_MAX_CH_PER_MD];
	_MAPPING_DATA	sensorMap2[EP_MAX_CH_PER_MD];
} EP_PROCEDURE_DATA;

typedef struct tag_TestResultData {			//Test Result Data(NetWrok ST.)
	char	szTestSerialNo[EP_TEST_SERIAL_LENGTH+1];
	BYTE	bReport;						//Grading 결과 보고 유무 Falg 
	BYTE	dataType;						//data Type
	WORD	wProgressID;					//Data ID
	WORD	nStep;
	WORD	nType;
	WORD	nTotalChannel;
	WORD	nNormal;
	WORD	nMinChannel;
	WORD	nMaxChannel;
	float	fMinValue;
	float	fMaxValue;
	float	fAverage;
	float	fStdD;
} EP_TEST_RESULT, *LPEP_TEST_RESULT;

typedef struct tag_TestResultDataBase {
	EP_TEST_RESULT	testResult;
	long lTestID;
	float fTempData;
	float fGasData;
	long procType;
	long totalChCount;
} EP_RESULT_TO_DATABASE;

typedef struct tag_ProductStep {
	WORD wTotCh;
	WORD wInputCellCount;
	long lTestID;
	BYTE bAutoProcess;
	BYTE bReserved;
	WORD wReserved;
	
	//File name을 만들기 위한
	char szTestSerialNo[EP_TEST_SERIAL_LENGTH+1];
	char szFileName[256];
	char szLot[32];
	char szTray[32];
	char szModuleName[32];
} STR_DB_REPORT_HEADER;

typedef struct tag_ChannelResult {			//Channel result Data(NetWork St.)
	float	fData;
	BYTE	gradeCode;
	BYTE	channelCode;
	WORD	state;
	WORD	dataType;
	WORD	reserved;
} EP_CH_RESULT, *LPEP_CH_RESULT;	

typedef struct tag_DataServerInfomation {
	WORD nID;						//Device ID
	WORD nSystemType;				
	int nVersion;					//Version
	int nReserved;		//Installed Module Number
} EP_SYSTEM_VER, *LPEP_SYSTEM_VER;

typedef struct tag_DataClient {						//Server Module Management Data
	BOOL			bConnected;
	char			szIpAddress[EP_IP_NAME_LENGTH+1];	//Accepted Module IP	BOOL CWizRawSocketListener::TreatData(HANDLE hShutDownEvent, HANDLE hDataTakenEvent)

	char			szTxBuffer[_EP_TX_BUF_LEN];			//Write Buffer
	HANDLE			m_hWriteEvent;						//Module별 Write Event	
	int				nTxLength;							//Tx Data Length

	char			szRxBuffer[_EP_RX_BUF_LEN];			//Read Buffer	
	HANDLE			m_hReadEvent;						//Module별 Read Event
	int				nRxLength;							//Rx Data Length

	EP_RESPONSE		CommandAck;							//Command Ack 
	UINT			nSystemID;							//Connected System ID
	
	EP_SYSTEM_VER	sysVsersion;						//Connected System Version Data

} DC_DATA_CLIENT, *LPDC_DATA_CLIENT;


#ifdef __cplusepluse
extern "C" {
#endif //__cplusepluse

#ifndef __EP_FORM_DLL__
#define __EP_FORM_DLL__

#ifdef EXPORT_EPDLL_API
#define EPDLL_API	__declspec(dllexport)
#else	//EXPORT_EPDLL_API
#define EPDLL_API	__declspec(dllimport)
#endif	//EXPORT_EPDLL_API

#endif	//__EP_FORM_DLL__
EPDLL_API int  dsSetFormDataServer(HWND hMsgWnd);
EPDLL_API BOOL dsCloseFormDataServer();
EPDLL_API void dsSetLog(BOOL bWrite);
EPDLL_API int dsSendCommand(int nCommand, int nSystemID);
EPDLL_API int dsSendData(int nCommand, LPVOID lpData, int nSize, int nSystemID);
EPDLL_API char* dsGetClientIP(int nSystemID);

//EPDLL_API	int	 dsSendData(int nCommand, LPVOID lpData, int nSize );

#ifdef __cplusepluse
}	// extern "C" {
#endif //__cplusepluse

#endif	//_EP_FORMATION_DATA_SERVER_DLL_DEFINE_H_


