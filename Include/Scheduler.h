///////SFT SCHEDULER_HEADER_FILE//////////////
//											//	
//	Scheduler.h								//	
//	Softech Formation System API			//	
//	define of Scheduler.dll					//
//	Byung Hum - Kim		2004.1.13			//
//											//
//////////////////////////////////////////////

#ifndef _SFT_SCHEDULER_DEFINE_H_
#define _SFT_SCHEDULER_DEFINE_H_

#include "Server.h"

#define SFT_APP_VERSION				0x1000		//Program Version

#define	SFT_MAX_MODULE_NUM			32			//Max Installable Module Number		
#define	SFT_MAX_BD_PER_MD			8			//Max Board Per Module
#define	SFT_MAX_CH_PER_BD			8
#define	SFT_MAX_CH_PER_MD			(SFT_MAX_BD_PER_MD*SFT_MAX_CH_PER_BD)
#define	SFT_MAX_GP_PER_MD			8

#define SFT_TRAY_NAME_LENGTH			32
#define SFT_LOT_NAME_LENGTH				32
#define SFT_IP_NAME_LENGTH				16
#define SFT_TEST_NAME_LENGTH			64
#define SFT_TEST_DESCRIPTION_LENGTH		128
#define SFT_CREATOR_NAME_LENGTH			64
#define SFT_SHORT_DATE_TIME_LENGTH		16
#define SFT_TEST_SERIAL_LENGTH			24
#define SFT_TRAY_SERIAL_LENGTH			8
#define SFT_RESULT_FILE_NAME_LENGTH		256
#define SFT_BATTERY_PER_TRAY			128
#define SFT_TEST_LOG_FILE_LENGTH		SFT_TEST_SERIAL_LENGTH
#define SFT_MODEL_ID_LENGTH				12
#define	SFT_PROCESS_ID_LENGTH			12
#define SFT_CELLNO_LENGTH				8


#define SFT_RESULT_ITEM_NO			8		//Test Result Item 수
#define SFT_MAX_PORT_NO				8		//DIO Port 수

//Tray 인식 Type
#define SFT_TRAY_REC_NVRAM			0
#define SFT_TRAY_REC_BARCODE		1	

#if SFT_RESULT_ITEM_NO <= SFT_RESERVED_BIT_POS
#pragma message( "------------KBH Message-------------" )
#pragma message( "Data Result Item Number Error" )
#pragma message( "------------------------------------" )
#error Item Define Error
#endif

#define SFT_TIME_BIT				(0x01<<SFT_TIME_BIT_POS)
#define SFT_VOLTAGE_BIT			(0x01<<SFT_VOLTAGE_BIT_POS)
#define SFT_CURRENT_BIT			(0x01<<SFT_CURRENT_BIT_POS)
#define SFT_CAPACITY_BIT			(0x01<<SFT_CAPACITY_BIT_POS)
#define SFT_WATT_BIT				(0x01<<SFT_WATT_BIT_POS)
#define SFT_WATT_HOUR_BIT		(0x01<<SFT_WATT_HOUR_BIT_POS)
#define SFT_IMPEDANCE_BIT		(0x01<<SFT_IMPEDANCE_BIT_POS)
#define SFT_RESERVED_BIT			(0x01<<SFT_RESERVED_BIT_POS)


#define SFT_DEFAULT_AUTO_REPORT_INTERVAL		2000		//defualt 2Sec

#define SFT_MAX_VOLTAGE			500000		//System Maximum Voltage		5V
#define SFT_MIN_VOLTAGE			-500000		//System Minimum Voltage		-5V
#define SFT_MAX_CURRENT			300000		//System Maximum Current		3A
#define SFT_MIN_CURRENT			300000		//System Minimum Current		-3A
#define SFT_MAX_TEMPERATURE		8000L		//System Maximum Temperature	80℃
#define SFT_MIN_TEMPERATURE		0L			//System Minimum Temperature
#define SFT_DIFFER_TEMPERATURE	10L			//System Maximun temperature difference	10℃	
#define SFT_MAX_V24_IN			2500000		//Max 24 Voltage Input			25V
#define SFT_MIN_V24_IN			2300000		//Min 24 Voltage Input			23V
#define SFT_MAX_V24_OUT			2500000		//Max 24 Voltage Output			25V
#define SFT_MIN_V24_OUT			2300000		//Max 24 Voltage Output			23V
#define SFT_MAX_V12_OUT			1300000		//Max 12 Voltage Output			13V
#define SFT_MIN_V12_OUT			1100000		//Max 12 Voltage Output			11V
#define SFT_MAX_GAS_LIMIT		10000L		//Max Gas sensor Limit			100

//define default System Parameter
#define SFT_DEFAULT_CH_PER_BD		8
#define SFT_DEFAULT_BD_PER_MD		4
#define SFT_DEFAULT_CH_PER_MD		(SFT_DEFAULT_CH_PER_BD*SFT_DEFAULT_BD_PER_MD)


#define SFT_CHANNEL_MAP_FILE_NAME	"monitoring.map"


//define Error
#define	SFT_ERR_EMPTY				0x00
#define	SFT_ERR_INVALID_ARGUMENT	0x01
#define	SFT_ERR_WINSOCK_STARTUP		0x02
#define	SFT_ERR_WINSOCK_VERSION		0x03
#define	SFT_ERR_NOT_INITIALIZED		0x04
#define	SFT_ERR_RECEIVE_TIMEOUT		0x05
#define SFT_ERR_MODULE_READ_FAIL	0x06
#define SFT_ERR_WINSOCK_INITIALIZE	0x07
#define	SFT_ERR_LINE_OFF			0x08

#define	SFT_ERR_MODULE_NOT_EXIST	0x09
#define SFT_ERR_GROUP_NOT_EXIST		0x0A
#define	SFT_ERR_BOARD_NOT_EXIST		0x0B
#define	SFT_ERR_CHANNEL_NOT_EXIST	0x0C

//Module Window Message
#define SFTWM_MODULE_STATE_CHANGE	WM_USER+2000		//모듈 상태 변경
#define SFTWM_MODULE_CONNECTED		WM_USER+2001		//모듈 통신 접속 
#define SFTWM_MODULE_CLOSED			WM_USER+2002		//모듈 통신 종료
#define SFTWM_STEP_ENDDATA_RECEIVE	WM_USER+2003		//Step결과 Data 수신 
#define SFTWM_CALI_RESULT_DATA		WM_USER+2004		//교정 완료 명령 수신 
#define SFTWM_TEST_RESULT			WM_USER+2005		//Test Result Data
#define SFTWM_MODULE_EMG			WM_USER+2006
#define SFTWM_MSEC_CH_DATA			WM_USER+2007		//msec 간격 channel data


//#define SFTWM_DB_SYS_CONNECTED		WM_USER+2005
//#define SFTWM_DB_SYS_DISCONNECTED	WM_USER+2006	
//#define SFTWM_PROCEDURE_DATA			WM_USER+2007	//Procedure Data
//#define SFTWM_NVRAM_HEADER			WM_USER+2008		//NVRAM Header
//#define SFTWM_CHECK_RESULT			WM_USER+2009		//Check Step Result
//#define SFTWM_MODULE_EMG				WM_USER+2010	//Emergency State Detected

#define	SFTWM_USER_CMD				WM_USER+2111


#define SFT_PGS_NOWORK		0x00
#define SFT_PGS_PREIMPEDANCE	0x01
#define	SFT_PGS_PRECHARGE	0x02
#define SFT_PGS_FORMATION	0x03
#define SFT_PGS_IROCV		0x04
#define SFT_PGS_FINALCHARGE	0x05
#define SFT_PGS_AGING		0x06
#define	SFT_PGS_GRADING		0x07
#define SFT_PGS_SELECTOR		0x08

#define SFT_STP_ALLSTEP			-1
#define INDEX					register int

//define Error
#define	SFT_ERR_EMPTY				0x00
#define	SFT_ERR_INVALID_ARGUMENT		0x01
#define	SFT_ERR_WINSOCK_STARTUP		0x02
#define	SFT_ERR_WINSOCK_VERSION		0x03
#define	SFT_ERR_NOT_INITIALIZED		0x04
#define	SFT_ERR_RECEIVE_TIMEOUT		0x05
#define SFT_ERR_MODULE_READ_FAIL		0x06
#define SFT_ERR_WINSOCK_INITIALIZE	0x07
#define	SFT_ERR_LINE_OFF				0x08

#define	SFT_ERR_MODULE_NOT_EXIST		0x09
#define SFT_ERR_GROUP_NOT_EXIST		0x0A
#define	SFT_ERR_BOARD_NOT_EXIST		0x0B
#define	SFT_ERR_CHANNEL_NOT_EXIST	0x0C

#define SFT_FILE_ID_LENGTH		16
#define SFT_FILE_VER_LENGTH		16
#define SFT_FILE_DESCRIPTION_LENGTH	128
#define SFT_IP_NAME_LENGTH		16

//Channel Mapping type of Monitoring Data 
#define SFT_CH_INDEX_MEMBER		0		//Use wChIndex( the member variable of SFT_CH_DATA)
#define SFT_CH_INDEX_SEQUENCE	1		//Network incomming Squence 
#define SFT_CH_INDEX_MAPPING	2		//Read Channel Mapping Table from "mapping.dat" File

//Data Unit Type
#define SFT_UNIT_NORMAL	0
#define SFT_UNIT_MILLI	1
#define SFT_UNIT_MICRO	2

#define SFT_MAX_CURRENT_RANGE	4
#define SFT_MAX_VOLTAGE_RANGE	4

typedef struct	tag_SystemParameter {			//System Parameter(Network St.)
    char		szIPAddr[SFT_IP_NAME_LENGTH];	//	char		IPAddr[20];
	LONG		lModuleType;
	WORD		wVRangeCount;
	WORD		wIRangeCount;
	LONG		lVSpec[SFT_MAX_VOLTAGE_RANGE];
	LONG		lISpec[SFT_MAX_CURRENT_RANGE];
	WORD		wInstallChCount;
	BOOL		bAutoProcess;					// 자동 공정 여부
    long		lMaxVoltage;
    long		lMinVoltage;
    long		lMaxCurrent;
    long		lMinCurrent;
    long		lMaxTemp;						//lOverTemp; 온도 상한치
    BOOL		bUseTempLimit;					//lLimitTemp; 온도 Limit 사용 여부
    long		lMaxGas;						//lDifferTemp;	가스 상한치
    long		bUseGasLimit;					//lV24Over;	 가스 Limit 사용 여부
	WORD		wModuleType;					//Module 의 형태 (원통형/각형)==> Tray의 배열과 같다.
	WORD		nAutoReportInterval;			//자동 보고 시간 간격 
} SFT_SYSTEM_PARAM, *LPSFT_SYSTEM_PARAM;


typedef struct tag_ChannelResult {			//Channel result Data(NetWork St.)
	long	lData;
	BYTE	gradeCode;
	BYTE	channelCode;
	WORD	state;
	WORD	dataType;
	WORD	reserved;
} SFT_CH_RESULT, *LPSFT_CH_RESULT;	

typedef struct tag_TestResultData {			//Test Result Data(NetWrok ST.)
	char	szTestSerialNo[SFT_TEST_SERIAL_LENGTH];
	BYTE	bReport;						//Stop의 종료값이나 Step 결과의 값이나 검사 
	BYTE	dataType;						//data Type
	WORD	wProgressID;					//Data ID
	WORD	nStep;
	WORD	nType;
	WORD	nTotalChannel;
	WORD	nNormal;
	WORD	nMinChannel;
	WORD	nMaxChannel;
	long	lMinValue;
	long	lMaxValue;
	long	lAverage;
	long	lStdD;
} SFT_TEST_RESULT, *LPSFT_TEST_RESULT;

typedef struct tag_TestResultDataBase {
	SFT_TEST_RESULT	testResult;
	long lTestID;
	long tempData;
	long gasData;
	long procType;
	long reserved1;
} SFT_RESULT_TO_DATABASE;

typedef struct tag_ChSelData {
	DWORD	nSelBitData[2];
} SFT_CH_SEL_DATA, *LPSFT_CH_SEL_DATA;

#endif //_SFT_SCHEDULER_DEFINE_H_