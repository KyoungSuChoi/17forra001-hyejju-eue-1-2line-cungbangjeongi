
//#include <afxsock.h> 
//#include <math.h> 

#define TIME_PROTOCOL	37 
#define DATE_1970		2208988800 

class GTime
{
public:   
	GTime();
    ~GTime();
    		

	//time.bora.net/time.nist.gov/ntp.tohoku.ac.jp
	/*static BOOL time_GetStandardTime(CString host,SYSTEMTIME &SystemTime)
	{//1
		CSocket* pSocket; // Create socket 
		pSocket= new CSocket(); 
		
		WSADATA wsaData; 
		WORD wVersionRequested = MAKEWORD( 1, 1 ); 
		UINT iErr = WSAStartup( wVersionRequested, &wsaData ); 
		if ( iErr != 0 ) return FALSE; 

		if(LOBYTE(wsaData.wVersion)!=1 || 
		   HIBYTE( wsaData.wVersion)!=1)
		{ 
			WSACleanup( ); 
			return FALSE; 
		}

		DWORD sBuffer=0; 	
		if(pSocket->Create()==FALSE) return FALSE;
		if(pSocket->Connect(host, TIME_PROTOCOL)==FALSE) return FALSE;
		if(pSocket->Receive((void*)&sBuffer, sizeof(sBuffer))==FALSE) return FALSE;
		pSocket->Close(); // Close socket handle 
		delete pSocket; // Delete from memory 			
		
		FILETIME FileTime, LocalFileTime; 
		sBuffer = htonl(sBuffer); // Reverse byte ordering 
		long lnTime = (long)(sBuffer-(unsigned long)DATE_1970); 

		__int64 Time_Int64 = Int32x32To64(lnTime, 10000000) + 116444736000000000; 
		FileTime.dwLowDateTime = (DWORD)Time_Int64; 
		FileTime.dwHighDateTime = Time_Int64 >> 32; 
		FileTimeToLocalFileTime(&FileTime, &LocalFileTime); 
		FileTimeToSystemTime(&LocalFileTime, &SystemTime); 
		
		return TRUE;
	}*/

	static CString time_GetTimeStr(SYSTEMTIME SystemTime)
	{//2
		CString sTime=_T("");
		sTime.Format(_T("%4d-%02d-%02d %02d:%02d:%02d"),
				  SystemTime.wYear,SystemTime.wMonth,SystemTime.wDay,
				  SystemTime.wHour,SystemTime.wMinute,SystemTime.wSecond);
		
		return sTime;
	}	


	static CString time_GetTimeStr( __int64 nTimeSec )
	{//3		
		CString strTime;
		int nHH, nMM, nSS;

		nHH = (int)( nTimeSec / 3600 );
		nMM = (int)( ( nTimeSec - ( nHH * 3600 ) ) / 60 );
		nSS = (int)( ( nTimeSec - ( nHH * 3600 ) - ( nMM * 60 ) ) );

		strTime.Format(_T("%02d:%02d:%02d"), nHH, nMM, nSS );
		
		return strTime;
	}

	static void time_GetTimeValue( __int64 nTimeSec, int &nHH, int &nMM, int &nSS )
	{//4
		nHH = (int)( nTimeSec / 3600 );
		nMM = (int)( ( nTimeSec - ( nHH * 3600 ) ) / 60 );
		nSS = (int)( ( nTimeSec - ( nHH * 3600 ) - ( nMM * 60 ) ) );	
	}

	static float time_GetSecStr(CString sTime)
	{//5
		CString shour=GStr::str_GetSplit(sTime,0,':');
		CString sminute=GStr::str_GetSplit(sTime,1,':');
		CString ssec=GStr::str_GetSplit(sTime,2,':');

		if(GStr::str_ChkNumeric(shour)==TRUE)   return 0;
		if(GStr::str_ChkNumeric(sminute)==TRUE) return 0;
		
		//if(GStr::str_ChkNumeric(ssec)==TRUE)    return 0;

		#ifdef _UNICODE			
			float fmin=(float)(_tstof(sminute)*60);
			float fhour=(float)(_tstof(shour)*60*60);
			float nSec=(float)(_tstof(ssec)+fmin+fhour);
		#else
			float fmin=(float)(atof(sminute)*60);
			float fhour=(float)(atof(shour)*60*60);
			float nSec=(float)(atof(ssec)+fmin+fhour);
		#endif

		//if(nSec>86400) nSec=0;
		return nSec;
	}

    /*static CString time_GetCTime(CTime cTime)
	{//6
		CString str;
		str.Format(_T("%04d-%02d-%02d %02d:%02d:%02d"),  cTime.GetYear(),
														 cTime.GetMonth(),
														 cTime.GetDay(),
														 cTime.GetHour(),
														 cTime.GetMinute(),
														 cTime.GetSecond());


		return str;
	}*/

	/*static int time_GetSecTime(CTime cTime)
	{//6
		return cTime.GetSecond()+cTime.GetMinute()*60+cTime.GetHour()*60*60;
	}
	

	static FILETIME time_GetFileTime()
	{//7
		SYSTEMTIME systemTime;
		GetSystemTime(&systemTime);

		FILETIME ftLastWriteTime;
		::SystemTimeToFileTime(&systemTime, &ftLastWriteTime);

		return ftLastWriteTime;
	}

	static BOOL time_SystemTime(CString sTime, FILETIME &ftLastWriteTime)
	{//8
		CString syear=GStr::str_GetSplit(sTime,0,' ');
		CString stime=GStr::str_GetSplit(sTime,1,' ');

		SYSTEMTIME now;
		GetLocalTime(&now);

		now.wYear=_ttoi(GStr::str_GetSplit(syear,0,'-'));
		now.wMonth=_ttoi(GStr::str_GetSplit(syear,1,'-'));
		now.wDay=_ttoi(GStr::str_GetSplit(syear,2,'-'));

		now.wHour=_ttoi(GStr::str_GetSplit(stime,0,':'));
		now.wMinute=_ttoi(GStr::str_GetSplit(stime,1,':'));
		now.wSecond=_ttoi(GStr::str_GetSplit(stime,2,':'));	
		
		return SystemTimeToFileTime(&now, &ftLastWriteTime);		
	}*/

	
	static void time_SetLocalTime(CString data)
	{//9	
		CString syear=GStr::str_GetSplit(data,0,' ');
		CString stime=GStr::str_GetSplit(data,1,' ');

		SYSTEMTIME now;

		now.wYear=_ttoi(GStr::str_GetSplit(syear,0,'-'));
		now.wMonth=_ttoi(GStr::str_GetSplit(syear,1,'-'));
		now.wDay=_ttoi(GStr::str_GetSplit(syear,2,'-'));

		now.wHour=_ttoi(GStr::str_GetSplit(stime,0,':'));
		now.wMinute=_ttoi(GStr::str_GetSplit(stime,1,':'));
		now.wSecond=_ttoi(GStr::str_GetSplit(stime,2,':'));
				
		SetLocalTime(&now);
	}

	static void time_SetLocalTime1(CString data)
	{//9	
		if(data.IsEmpty() || data==_T("")) return;
		SYSTEMTIME now;

		now.wYear=_ttoi(data.Mid(0,4));
		now.wMonth=_ttoi(data.Mid(4,2));
		now.wDay=_ttoi(data.Mid(6,2));

		now.wHour=_ttoi(data.Mid(8,2));
		now.wMinute=_ttoi(data.Mid(10,2));
		now.wSecond=_ttoi(data.Mid(12,2));
				
		SetLocalTime(&now);
	}
	
	static CTime time_GetTime(CString data)
	{//10		
		CString syear=GStr::str_GetSplit(data,0,' ');
		CString stime=GStr::str_GetSplit(data,1,' ');
		
		int iyear=0;
		int imonth=0;
		int iday=0;

		int ihour=0;
		int iminute=0;
		int isecond=0;
		
		iyear=_ttoi(GStr::str_GetSplit(syear,0,'-'));
		imonth=_ttoi(GStr::str_GetSplit(syear,1,'-'));
		iday=_ttoi(GStr::str_GetSplit(syear,2,'-'));

		if(!stime.IsEmpty() && stime!=_T(""))
		{
			ihour=_ttoi(GStr::str_GetSplit(stime,0,':'));
			iminute=_ttoi(GStr::str_GetSplit(stime,1,':'));
			isecond=_ttoi(GStr::str_GetSplit(stime,2,':'));		
		}

	    CTime rtime(iyear,imonth,iday,ihour,iminute,isecond);	
  	    return rtime;
	}

	
	static BOOL time_GetTimeA(CString data,CTime &rtime)
	{//10		
		if(data.IsEmpty() || data==_T("")) return FALSE;
		
		COleDateTime oTime = COleDateTime::GetCurrentTime();
		oTime.ParseDateTime(data);
		
		CString strTemp=oTime.Format(_T("%Y-%m-%d %H:%M:%S"));
		if(strTemp.Find(_T("Invalid"))>-1) return FALSE;
		if(strTemp.Find(_T("잘못된"))>-1)  return FALSE;
		
		rtime=CTime(oTime.GetYear(),oTime.GetMonth(),oTime.GetDay(),oTime.GetHour(),oTime.GetMinute(),oTime.GetSecond());	
		return TRUE;
	}

	/*
	static CTime time_GetEmpTime(CString data)
	{//11	
		int iyear=0;
		int imonth=0;
		int iday=0;

		int ihour=0;
		int iminute=0;
		int isecond=0;

		iyear=_ttoi(data.Mid(0,4));
		imonth=_ttoi(data.Mid(4,2));
		iday=_ttoi(data.Mid(6,2));

		ihour=_ttoi(data.Mid(8,2));
		iminute=_ttoi(data.Mid(10,2));
		isecond=_ttoi(data.Mid(12,2));
			
	    CTime rtime(iyear,imonth,iday,ihour,iminute,isecond);	
  	    return rtime;
	}

	static void time_SetDataPickerTime(CDateTimeCtrl *DpickerTime,CString data)
	{	
		if(data.IsEmpty() || data==_T("")) return;

		int iyear=0;
		int imonth=0;
		int iday=0;

		iyear=_ttoi(GStr::str_GetSplit(data,0,'-'));
		imonth=_ttoi(GStr::str_GetSplit(data,1,'-'));
		iday=_ttoi(GStr::str_GetSplit(data,2,'-'));
		
	    CTime rtime(iyear,imonth,iday,0,0,0);
		DpickerTime->SetTime( &rtime );	  	    
	}*/

	static void time_SetHourDataPicker(CDateTimeCtrl *DpickerTime,CString shh,CString smm)
	{//12
		CTime t = CTime::GetCurrentTime();	

		int ihh=0;
		int imm=0;

		ihh=_ttoi(shh);
		imm=_ttoi(smm);			
		
		CTime rtime(t.GetYear(),t.GetMonth(),t.GetDay(),ihh,imm,0);	
		DpickerTime->SetTime( &rtime );	
	}

	static void time_SetSecDataPicker(CDateTimeCtrl *DpickerTime,int iss) 
	{//13
		int nHH,nMM,nSS;
		time_GetTimeValue( iss, nHH, nMM, nSS );
		CTime t = CTime::GetCurrentTime();	
		
		CTime rtime(t.GetYear(),t.GetMonth(),t.GetDay(),nHH, nMM, nSS);	
		DpickerTime->SetTime( &rtime );	
	}
	
	/*static BOOL time_GetDataPicker(CDateTimeCtrl *DpickerTime,CTime &rtime) 
	{//14				
		DWORD dwResult1 = DpickerTime->GetTime( rtime );
		if( dwResult1 == GDT_VALID )
		{
			return TRUE;
		}
		return FALSE;
	}

	static int time_GetSecDataPicker(CDateTimeCtrl *DpickerTime) 
	{//15
		CTime ptime;
		if(GTime::time_GetDataPicker(DpickerTime,ptime)==TRUE)
		{
			return time_GetSecTime(ptime);
		}
		return 0;
	}


	static BOOL time_GetHourType(int nHour) 
	{//16 AM PM
		BOOL pm = FALSE;		
		if(nHour == 0)
		{
			nHour = 12;
		}
		else 
		{
			if(nHour == 12)
			{
				pm = TRUE;
			}
			else 
			{
				if(nHour > 12) 
				{
					nHour -= 12;
					pm = TRUE;
				}
			}
		}
		return pm;
	}*/

	static int time_GetElapse(CTime cstart,CTime cend) 
	{//17 시간차 구하기
		CTimeSpan elapsedTime = cend - cstart;

		return (int)elapsedTime.GetTotalSeconds();
	}

	/*
   	static CTime time_GetMaxOfMonth(CTime t) 
	{//18시간차 구하기
				
		int nYear=t.GetYear();
		int nMonth=t.GetMonth();
		if(nMonth==12)
		{//12월이면 다음해 1월1일
			nYear=nYear+1;
			nMonth=1;
		}
		else 
		{
			nMonth=nMonth+1;
		}

		CTime ltime(nYear, nMonth, 1, 0, 0, 0 );
		CTime ctMonthLast = ltime - CTimeSpan(1,0,0,0);//다음달1일에서 1일을 빼면됨
		
		return ctMonthLast;
	}

	static int time_GetCurrentSec() 
	{//19 1970년기준으로한 초 단위 시간계산
	    return static_cast<int>(time(0));
	}

	static void time_SetDateDataPicker(CDateTimeCtrl *DpickerTime,CString syear,CString smonth,CString sday)
	{//12		
		CTime rtime(_ttoi(syear),_ttoi(smonth),_ttoi(sday),0,0,0);	
		DpickerTime->SetTime( &rtime );	
	}

	static void time_SetDataPickerMonthFirst(CDateTimeCtrl *DpickerTime) 
	{//현재달의 첫번째일자		
		CTime t = CTime::GetCurrentTime();	
		
		CTime rtime(t.GetYear(),t.GetMonth(),1,0, 0, 0);	//t.GetYear() t.GetMonth()
		DpickerTime->SetTime( &rtime );	
	}

	static void time_SetDataPickerMonthLast(CDateTimeCtrl *DpickerTime) 
	{//현재달의 마지막일자		
		CTime t = CTime::GetCurrentTime();	
		CTimeSpan span(1, 0, 0, 0);

		CTime rtime(t.GetYear(),t.GetMonth()+1,1,9, 0, 0);	
		rtime-=span;

		DpickerTime->SetTime( &rtime );			
	}*/

} ;