// CaliPoint.cpp: implementation of the CCaliPoint class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CaliPoint.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCaliPoint::CCaliPoint()
{
	ResetPoint();
	LanguageinitMonConfig();
}

CCaliPoint::~CCaliPoint()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

bool CCaliPoint::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCaliPoint"), _T("TEXT_CCaliPoint_CNT"), _T("TEXT_CCaliPoint_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CCaliPoint_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCaliPoint"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}


void CCaliPoint::ResetPoint(WORD wRange, int nDataType)
{
	if(nDataType == VTG_CAL_SET_POINT)
	{
		if(wRange == CAL_RANGE_ALL)
		{
			for(int i =0; i<CAL_MAX_VOLTAGE_RANGE; i++)
			{
				m_nVCalPointNum[i] = 0;	
			}
		}
		else
			m_nVCalPointNum[wRange] = 0;	
		
		return;

	}
	if(nDataType == VTG_CAL_CHECK_POINT)
	{
		if(wRange == CAL_RANGE_ALL)
		{
			for(int i =0; i<CAL_MAX_VOLTAGE_RANGE; i++)
			{
				m_nVCheckPointNum[i] = 0;	
			}
		}
		else
			m_nVCheckPointNum[wRange] = 0;	
		return;
	}

	if(nDataType == CRT_CAL_SET_POINT)
	{
		if(wRange == CAL_RANGE_ALL)
		{
			for(int i =0; i<CAL_MAX_CURRENT_RANGE; i++)
			{
				m_nICalPointNum[i] = 0;	
			}
		}
		else
			m_nICalPointNum[wRange] = 0;	
		return;
	}
	if(nDataType == CRT_CAL_CHECK_POINT)
	{
		if(wRange == CAL_RANGE_ALL)
		{
			for(int i =0; i<CAL_MAX_CURRENT_RANGE; i++)
			{
				m_nICheckPointNum[i] = 0;	
			}
		}
		else
			m_nICheckPointNum[wRange] = 0;	
		return;
	}

	int i = 0;

	for(i =0; i<CAL_MAX_VOLTAGE_RANGE; i++)
	{
		m_nVCalPointNum[i] = 0;	
		m_nVCheckPointNum[i] = 0;
	}

	for(i =0; i<CAL_MAX_CURRENT_RANGE; i++)
	{
		m_nICalPointNum[i] = 0;
		m_nICheckPointNum[i] = 0;
	}

	m_nVRangeCnt = 1;
	m_nIRangeCnt = 1;

	ZeroMemory( m_ShuntR, sizeof(m_ShuntR) );
	ZeroMemory( m_ShuntT, sizeof(m_ShuntT) );
	ZeroMemory( m_ShuntSerial, sizeof(m_ShuntSerial) );
}


// Multi Range FTN
double CCaliPoint::GetVSetPoint(WORD wRange, WORD nPoint)
{
	if(wRange >= CAL_MAX_VOLTAGE_RANGE ||	 nPoint>= CAL_MAX_CALIB_SET_POINT)
		return 0.0;
	
	return	m_VCalPoint[wRange][nPoint];
}
double CCaliPoint::GetVCheckPoint(WORD wRange, WORD nPoint)
{
	if(wRange >= CAL_MAX_VOLTAGE_RANGE ||	 nPoint>= CAL_MAX_CALIB_CHECK_POINT)
		return 0.0;
	
	return	m_VCheckPoint[wRange][nPoint];
}

double CCaliPoint::GetISetPoint(WORD wRange, WORD nPoint)
{
	if(wRange >= CAL_MAX_CURRENT_RANGE ||	 nPoint>= CAL_MAX_CALIB_SET_POINT)
		return 0.0;
	
	return	m_ICalPoint[wRange][nPoint];
}

double CCaliPoint::GetICheckPoint(WORD wRange, WORD nPoint)
{
	if(wRange >= CAL_MAX_CURRENT_RANGE ||	 nPoint>= CAL_MAX_CALIB_CHECK_POINT)
		return 0.0;
	
	return	m_ICheckPoint[wRange][nPoint];
}

double CCaliPoint::GetShuntT( int nBoardIndexNum )
{
	if( nBoardIndexNum >= CAL_MAX_BOARD_NUM || nBoardIndexNum < 0 )
		return 0.0;

	return	m_ShuntT[nBoardIndexNum];
}

double CCaliPoint::GetShuntR( int nBoardIndexNum )
{
	if( nBoardIndexNum >= CAL_MAX_BOARD_NUM || nBoardIndexNum < 0 )
		return 0.0;

	return	m_ShuntR[nBoardIndexNum];
}

CString CCaliPoint::GetShuntSerial( int nBoardIndexNum )
{
	CString strSerial;
	strSerial.Empty();

	if( nBoardIndexNum >= CAL_MAX_BOARD_NUM || nBoardIndexNum < 0 )
		return strSerial;

	strSerial.Format("%s", m_ShuntSerial[nBoardIndexNum] );
	return strSerial;
}

//1 Range FTN
double CCaliPoint::GetVSetPoint(WORD nPoint)
{
	if(nPoint>= CAL_MAX_CALIB_SET_POINT)		return 0.0;
	return	m_VCalPoint[0][nPoint];
}
double CCaliPoint::GetVCheckPoint(WORD nPoint)
{
	if(nPoint>= CAL_MAX_CALIB_SET_POINT)		return 0.0;
	return	m_VCheckPoint[0][nPoint];
}
double CCaliPoint::GetISetPoint(WORD nPoint)
{
	if( nPoint>= CAL_MAX_CALIB_CHECK_POINT)		return 0.0;
	return	m_ICalPoint[0][nPoint];
}
double CCaliPoint::GetICheckPoint(WORD nPoint)
{
	if(nPoint>= CAL_MAX_CALIB_CHECK_POINT)		return 0.0;
	return	m_ICheckPoint[0][nPoint];
}

//BOOL CCaliPoint::AddPointData(WORD wRange, int nDataType)
//{
//
//	return TRUE;
//}

//Set Data FTN
BOOL CCaliPoint::SetVSetPointData(int nDataCount, double *pdData, WORD wRange)
{
//	SortData();
	int size = nDataCount;	
	if(size <0 )	return FALSE;
	if( size >=  CAL_MAX_CALIB_SET_POINT)	size = 	CAL_MAX_CALIB_SET_POINT;
	ASSERT(pdData);
	if(wRange < 0 || wRange >= CAL_MAX_VOLTAGE_RANGE)	return FALSE;

	m_nVCalPointNum[wRange] = (WORD)size;
	memcpy(&m_VCalPoint[wRange], pdData, sizeof(double)*size);
	return TRUE;
}
BOOL CCaliPoint::SetVCheckPointData(int nDataCount, double *pdData, WORD wRange)
{
//	SortData();
	int size = nDataCount;	
	if(size <=0)		return FALSE;
	if(size > CAL_MAX_CALIB_CHECK_POINT)		size = 	CAL_MAX_CALIB_CHECK_POINT;
	ASSERT(pdData);
	if(wRange < 0 || wRange >= CAL_MAX_VOLTAGE_RANGE)	return FALSE;

	m_nVCheckPointNum[wRange] = (WORD)size;
	memcpy(&m_VCheckPoint[wRange], pdData, sizeof(double)*size);
	return TRUE;
}
BOOL CCaliPoint::SetISetPointData(int nDataCount, double *pdData, WORD wRange)
{
//	SortData();
	int size = nDataCount;	
	if(size <=0	)			return FALSE;
	if( size > CAL_MAX_CALIB_SET_POINT)		size = CAL_MAX_CALIB_SET_POINT;
	ASSERT(pdData);
	if(wRange < 0 || wRange >= CAL_MAX_CURRENT_RANGE)	return FALSE;

	m_nICalPointNum[wRange] = (WORD)size;
	memcpy(&m_ICalPoint[wRange], pdData, sizeof(double)*size);
	return TRUE;
}
BOOL CCaliPoint::SetICheckPointData(int nDataCount, double *pdData, WORD wRange)
{
//	SortData();
	int size = nDataCount;	
	if(size <= 0)		return FALSE;
	if(size > CAL_MAX_CALIB_CHECK_POINT)	size = CAL_MAX_CALIB_CHECK_POINT;
	ASSERT(pdData);
	if(wRange < 0 || wRange >= CAL_MAX_CURRENT_RANGE)	return FALSE;

	m_nICheckPointNum[wRange] = (WORD)size;
	memcpy(&m_ICheckPoint[wRange], pdData, sizeof(double)*size);
	return TRUE;

}

BOOL CCaliPoint::SetShuntT( double *pdData )
{
	ASSERT(pdData);

	memcpy( m_ShuntT, pdData, sizeof(double) * CAL_MAX_BOARD_NUM );

	return TRUE;
}

BOOL CCaliPoint::SetShuntR( double *pdData )
{
	ASSERT(pdData);

	memcpy( m_ShuntR, pdData, sizeof(double) * CAL_MAX_BOARD_NUM );

	return TRUE;
}

BOOL CCaliPoint::SetShuntSerial( int nBoardIndexNum, CHAR *pdData )
{
	ASSERT(pdData);	

	memcpy( &m_ShuntSerial[nBoardIndexNum][0], pdData, sizeof(UCHAR) * CAL_MAX_SHUNT_SERIAL_SIZE);

	return TRUE;
}

//교정 Point Data를 파일 or Registry에서 Loading한다. 
BOOL CCaliPoint::LoadPointData(BOOL bAll)
{
	UINT nSize;
	LPVOID* pData;
	
	if(bAll)
	{
		m_nVRangeCnt = AfxGetApp()->GetProfileInt(REG_CAL_SECTION, "Volt Range Count", 1);
		m_nIRangeCnt = AfxGetApp()->WriteProfileInt(REG_CAL_SECTION, "Amp Range Count", 1);

		AfxGetApp()->GetProfileBinary(REG_CAL_SECTION, "Volt Cal Count", (LPBYTE *)&pData, &nSize);
		if(nSize > 0)	memcpy(m_nVCalPointNum, pData, nSize);
		else
		{
			memset(m_nVCalPointNum, 0, sizeof(m_nVCalPointNum));
		}
		delete [] pData;

 		AfxGetApp()->GetProfileBinary(REG_CAL_SECTION, "Volt Check Count", (LPBYTE *)&pData, &nSize);
		if(nSize > 0)	memcpy(m_nVCheckPointNum, pData, nSize);
		else
		{
			memset(m_nVCheckPointNum, 0, sizeof(m_nVCheckPointNum));
		}
		delete [] pData;

		AfxGetApp()->GetProfileBinary(REG_CAL_SECTION, "Amp Cal Count", (LPBYTE *)&pData, &nSize);
		if(nSize > 0)	memcpy(m_nICalPointNum, pData, nSize);
		else			
		{
			memset(m_nICalPointNum, 0, sizeof(m_nICalPointNum));
		}
		delete [] pData;

		AfxGetApp()->GetProfileBinary(REG_CAL_SECTION, "Amp Check Count", (LPBYTE *)&pData, &nSize);
		if(nSize > 0)	memcpy(m_nICheckPointNum, pData, nSize);
		else
		{
			memset(m_nICheckPointNum, 0, sizeof(m_nICheckPointNum));
		}
		delete [] pData;

		AfxGetApp()->GetProfileBinary(REG_CAL_SECTION, "Volt Cal", (LPBYTE *)&pData, &nSize);
		if(nSize > 0)	memcpy(m_VCalPoint, pData, nSize);
		else			
		{
			memset(m_VCalPoint, 0, sizeof(m_VCalPoint));
		}
		delete [] pData;

		AfxGetApp()->GetProfileBinary(REG_CAL_SECTION, "Volt Check", (LPBYTE *)&pData, &nSize);
		if(nSize > 0)	memcpy(m_VCheckPoint, pData, nSize);
		else			
		{
			memset(m_VCheckPoint, 0, sizeof(m_VCheckPoint));
		}
		delete [] pData;

		AfxGetApp()->GetProfileBinary(REG_CAL_SECTION, "Amp Cal", (LPBYTE *)&pData, &nSize);
		if(nSize > 0)	memcpy(m_ICalPoint, pData, nSize);
		else			
		{
			memset(m_ICalPoint, 0, sizeof(m_ICalPoint));
		}
		delete [] pData;

		AfxGetApp()->GetProfileBinary(REG_CAL_SECTION, "Amp Check", (LPBYTE *)&pData, &nSize);
		if(nSize > 0)	memcpy(m_ICheckPoint, pData, nSize);
		else			
		{
			memset(m_ICheckPoint, 0, sizeof(m_ICheckPoint));
		}
		delete [] pData;

		// Shunt value Add	
		AfxGetApp()->GetProfileBinary(REG_CAL_SECTION, "ShuntT", (LPBYTE *)&pData, &nSize);
		if(nSize > 0)	memcpy(m_ShuntT, pData, nSize);
		else			
		{
			memset(m_ShuntT, 0, sizeof(m_ShuntT));
		}	
		delete [] pData;

		AfxGetApp()->GetProfileBinary(REG_CAL_SECTION, "ShuntR", (LPBYTE *)&pData, &nSize);
		if(nSize > 0)	memcpy(m_ShuntR, pData, nSize);
		else			
		{
			memset(m_ShuntR, 0, sizeof(m_ShuntR));
		}	
		delete [] pData;

		AfxGetApp()->GetProfileBinary(REG_CAL_SECTION, "ShuntSerial", (LPBYTE *)&pData, &nSize);
		if(nSize > 0)	memcpy(m_ShuntSerial, pData, nSize);
		else			
		{
			memset(m_ShuntSerial, 0, sizeof(m_ShuntSerial));
		}	
		delete [] pData;
	}

	AfxGetApp()->GetProfileBinary(REG_CAL_SECTION, "VDA Accuracy", (LPBYTE *)&pData, &nSize);
	if(nSize > 0)	memcpy(m_dVDAAccuracy, pData, nSize);
	else			
	{
		memset(m_dVDAAccuracy, 2.0f, sizeof(m_dVDAAccuracy));
	}
	delete [] pData;

	AfxGetApp()->GetProfileBinary(REG_CAL_SECTION, "VAD Accuracy", (LPBYTE *)&pData, &nSize);
	if(nSize > 0)	memcpy(m_dVADAccuracy, pData, nSize);
	else			
	{
		memset(m_dVADAccuracy, 2.0f, sizeof(m_dVADAccuracy));
	}
	delete [] pData;

	AfxGetApp()->GetProfileBinary(REG_CAL_SECTION, "IDA Accuracy", (LPBYTE *)&pData, &nSize);
	if(nSize > 0)	memcpy(m_dIDAAccuracy, pData, nSize);
	else			
	{
		memset(m_dIDAAccuracy, 2.0f, sizeof(m_dIDAAccuracy));
	}
	delete [] pData;

	AfxGetApp()->GetProfileBinary(REG_CAL_SECTION, "IAD Accuracy", (LPBYTE *)&pData, &nSize);
	if(nSize > 0)	memcpy(m_dIADAccuracy, pData, nSize);
	else
	{
		memset(m_dIADAccuracy, 2.0f, sizeof(m_dIADAccuracy));
	}
	delete [] pData;
	return TRUE;
}


/**
@author  
@brief 교정 Point Data를 파일 or Registry에서 저장한다.
@bug    
@code   
@date   2013 - 06 -13
@endcode 
@param   
@remark  
@return  FALSE
@see  
@todo    
*/ 
BOOL CCaliPoint::SavePointData(BOOL bAll)
{
	SortData();
	
	//Size Over
	if(bAll)
	{
		AfxGetApp()->WriteProfileInt(REG_CAL_SECTION, "Volt Range Count", m_nVRangeCnt);
		AfxGetApp()->WriteProfileInt(REG_CAL_SECTION, "Amp Range Count", m_nIRangeCnt);
		AfxGetApp()->WriteProfileBinary(REG_CAL_SECTION, "Volt Check Count", (LPBYTE)m_nVCheckPointNum, sizeof(m_nVCheckPointNum));
		AfxGetApp()->WriteProfileBinary(REG_CAL_SECTION, "Volt Cal Count",(LPBYTE)m_nVCalPointNum, sizeof(m_nVCalPointNum));
		AfxGetApp()->WriteProfileBinary(REG_CAL_SECTION, "Volt Check Count", (LPBYTE)m_nVCheckPointNum, sizeof(m_nVCheckPointNum));
		AfxGetApp()->WriteProfileBinary(REG_CAL_SECTION, "Amp Cal Count", (LPBYTE)m_nICalPointNum, sizeof(m_nICalPointNum));
		AfxGetApp()->WriteProfileBinary(REG_CAL_SECTION, "Amp Check Count", (LPBYTE)m_nICheckPointNum, sizeof(m_nICheckPointNum));

		AfxGetApp()->WriteProfileBinary(REG_CAL_SECTION, "Volt Cal", (LPBYTE)m_VCalPoint, sizeof(m_VCalPoint));
		AfxGetApp()->WriteProfileBinary(REG_CAL_SECTION, "Volt Check", (LPBYTE)m_VCheckPoint, sizeof(m_VCheckPoint));
		AfxGetApp()->WriteProfileBinary(REG_CAL_SECTION, "Amp Cal", (LPBYTE)m_ICalPoint, sizeof(m_ICalPoint));
		AfxGetApp()->WriteProfileBinary(REG_CAL_SECTION, "Amp Check", (LPBYTE)m_ICheckPoint, sizeof(m_ICheckPoint));

		AfxGetApp()->WriteProfileBinary(REG_CAL_SECTION, "ShuntT", (LPBYTE)m_ShuntT, sizeof(m_ShuntT));
		AfxGetApp()->WriteProfileBinary(REG_CAL_SECTION, "ShuntR", (LPBYTE)m_ShuntR, sizeof(m_ShuntR));
		AfxGetApp()->WriteProfileBinary(REG_CAL_SECTION, "ShuntSerial", (LPBYTE)m_ShuntSerial, sizeof(m_ShuntSerial));
	}

	AfxGetApp()->WriteProfileBinary(REG_CAL_SECTION, "VDA Accuracy", (LPBYTE)m_dVDAAccuracy, sizeof(m_dVDAAccuracy));
	AfxGetApp()->WriteProfileBinary(REG_CAL_SECTION, "VAD Accuracy", (LPBYTE)m_dVADAccuracy, sizeof(m_dVADAccuracy));
	AfxGetApp()->WriteProfileBinary(REG_CAL_SECTION, "IDA Accuracy", (LPBYTE)m_dIDAAccuracy, sizeof(m_dIDAAccuracy));
	AfxGetApp()->WriteProfileBinary(REG_CAL_SECTION, "IAD Accuracy", (LPBYTE)m_dIADAccuracy, sizeof(m_dIADAccuracy));

	return FALSE;
}

void SwapValue(double &lFirst, double &lSecond)
{
	double lTmp = lSecond;
	lSecond = lFirst;
	lFirst = lTmp;
}

//입력된 Data값을 크기 순으로 정렬한다.
void CCaliPoint::SortData()
{
	int range, point1, point2;
	for( range =0; range<CAL_MAX_VOLTAGE_RANGE; range++)
	{
		//교정 전압 
		for( point1 = 0; point1 < m_nVCalPointNum[range]; point1++)
		{
			for( point2 = point1+1; point2 < m_nVCalPointNum[range]; point2++)
			{
				if(m_VCalPoint[range][point1] > m_VCalPoint[range][point2])
				{
					SwapValue(m_VCalPoint[range][point1], m_VCalPoint[range][point2]);
				}
			}
		}
		//확인 전압 
		for( point1 = 0; point1 < m_nVCheckPointNum[range]; point1++)
		{
			for( point2 = point1+1; point2 < m_nVCheckPointNum[range]; point2++)
			{
				if(m_VCheckPoint[range][point1] > m_VCheckPoint[range][point2])
				{
					SwapValue(m_VCheckPoint[range][point1], m_VCheckPoint[range][point2]);
				}
			}
		}
	}

	for( range =0; range<CAL_MAX_CURRENT_RANGE; range++)
	{	
		//교정 전류
		for( point1 = 0; point1 < m_nICalPointNum[range]; point1++)
		{
			for( point2 = point1+1; point2 < m_nICalPointNum[range]; point2++)
			{
				if(m_ICalPoint[range][point1] > m_ICalPoint[range][point2])
				{
					SwapValue(m_ICalPoint[range][point1], m_ICalPoint[range][point2]);
				}
			}
		}
		
		//확인 전류
		for( point1 = 0; point1 < m_nICheckPointNum[range]; point1++)
		{
			for( point2 = point1+1; point2 < m_nICheckPointNum[range]; point2++)
			{
				if(m_ICheckPoint[range][point1] > m_ICheckPoint[range][point2])
				{
					SwapValue(m_ICheckPoint[range][point1], m_ICheckPoint[range][point2]);
				}
			}
		}
	}
}

CCaliPoint& CCaliPoint::operator =(CCaliPoint& OrgPoint)
{
	WORD i = 0, j=0, k=0;

	for(i=0; i<CAL_MAX_VOLTAGE_RANGE; i++)
	{
		m_nVCalPointNum[i] = OrgPoint.GetVtgSetPointCount(i);
		m_nVCheckPointNum[i] = OrgPoint.GetVtgCheckPointCount(i);
	}

	for( i=0; i<CAL_MAX_CURRENT_RANGE; i++)
	{
		m_nICalPointNum[i] = OrgPoint.GetCrtSetPointCount(i);
		m_nICheckPointNum[i] = OrgPoint.GetCrtCheckPointCount(i);
	}

	for(i=0; i<CAL_MAX_VOLTAGE_RANGE; i++)
	{
		for(j =0; j<CAL_MAX_CALIB_SET_POINT; j++)
		{
			m_VCalPoint[i][j] = OrgPoint.GetVSetPoint(i, j);
		}
		for(k =0; k<CAL_MAX_CALIB_CHECK_POINT; k++)
		{
			m_VCheckPoint[i][k] =OrgPoint.GetVCheckPoint(i, k);
		}
	}
	for(i=0; i<CAL_MAX_CURRENT_RANGE; i++)
	{
		for(j =0; j<CAL_MAX_CALIB_SET_POINT; j++)
		{
			m_ICalPoint[i][j] = OrgPoint.GetISetPoint(i, j);
		}
		for(k =0; k<CAL_MAX_CALIB_CHECK_POINT; k++)
		{
			m_ICheckPoint[i][k] =OrgPoint.GetICheckPoint(i, k);
		}
	}

	return *this;
}


BOOL CCaliPoint::ReadDataFromFile(FILE *fp)
{
	if(fp == NULL)	return FALSE;
	if(fread(m_nVCalPointNum, sizeof(m_nVCalPointNum), 1, fp) < 1)
	{
		return FALSE;
	}
	if(fread(m_nVCheckPointNum, sizeof(m_nVCheckPointNum), 1, fp) < 1)
	{
		return FALSE;
	}
	if(fread(m_nICalPointNum, sizeof(m_nICalPointNum), 1, fp) < 1)
	{
		return FALSE;
	}
	if(fread(m_nICheckPointNum, sizeof(m_nICalPointNum), 1, fp) < 1)
	{
		return FALSE;
	}
	if(fread(m_VCalPoint, sizeof(m_VCalPoint), 1, fp) < 1)
	{
		return FALSE;
	}
	if(fread(m_VCheckPoint, sizeof(m_VCheckPoint), 1, fp) < 1)
	{
		return FALSE;
	}
	if(fread(m_ICalPoint, sizeof(m_ICalPoint), 1, fp) < 1)
	{
		return FALSE;
	}
	if(fread(m_ICheckPoint, sizeof(m_ICheckPoint), 1, fp) < 1)
	{
		return FALSE;
	}
	return TRUE;
}

BOOL CCaliPoint::WriteDataToFile(FILE *fp)
{
	if(fp == NULL)	return FALSE;
	if(fwrite(m_nVCalPointNum, sizeof(m_nVCalPointNum), 1, fp) < 1)
	{
		return FALSE;
	}
	if(fwrite(m_nVCheckPointNum, sizeof(m_nVCheckPointNum), 1, fp) < 1)
	{
		return FALSE;
	}
	if(fwrite(m_nICalPointNum, sizeof(m_nICalPointNum), 1, fp) < 1)
	{
		return FALSE;
	}
	if(fwrite(m_nICheckPointNum, sizeof(m_nICalPointNum), 1, fp) < 1)
	{
		return FALSE;
	}
	if(fwrite(m_VCalPoint, sizeof(m_VCalPoint), 1, fp) < 1)
	{
		return FALSE;
	}
	if(fwrite(m_VCheckPoint, sizeof(m_VCheckPoint), 1, fp) < 1)
	{
		return FALSE;
	}
	if(fwrite(m_ICalPoint, sizeof(m_ICalPoint), 1, fp) < 1)
	{
		return FALSE;
	}
	if(fwrite(m_ICheckPoint, sizeof(m_ICheckPoint), 1, fp) < 1)
	{
		return FALSE;
	}
	return TRUE;
}

//SFT_CALI_SET_POINT CCaliPoint::GetNetworkFormatPoint()
//{
//	SFT_CALI_SET_POINT	aNetCalPoint;
//	ZeroMemory(&aNetCalPoint, sizeof(SFT_CALI_SET_POINT));
//	int i, j;
//	
//	for(i=0; i<3 && i<SFT_MAX_VOLTAGE_RANGE; i++)
//	{
//		aNetCalPoint.calPoint[0][i].byValidPointNum = m_nVCalPointNum[i];
//		for(j=0; j<15 && j<m_nVCalPointNum[i]; j++)
//		{
//			aNetCalPoint.calPoint[0][i].lCaliPoint[j] = FLOAT2LONG(m_VCalPoint[i][j]);
//		}
//	}
//	
//	for(i=0; i<3 && i<SFT_MAX_CURRENT_RANGE; i++)
//	{
//		aNetCalPoint.calPoint[1][i].byValidPointNum = m_nICalPointNum[i];
//		for(j=0; j<15 && j<m_nICalPointNum[i]; j++)
//		{
//			aNetCalPoint.calPoint[1][i].lCaliPoint[j] = FLOAT2LONG(m_ICalPoint[i][j]);
//		}
//	}
//	
//	for(i=0; i<3 && i<SFT_MAX_VOLTAGE_RANGE; i++)
//	{
//		aNetCalPoint.calPoint[0][i].byCheckPointNum = m_nVCheckPointNum[i];
//		for(j=0; j<15 && j<m_nVCheckPointNum[i]; j++)
//		{
//			aNetCalPoint.calPoint[0][i].lCheckPoint[j] = FLOAT2LONG(m_VCheckPoint[i][j]);
//		}
//	}
//	
//	for(i=0; i<3 && i<SFT_MAX_CURRENT_RANGE; i++)
//	{
//		aNetCalPoint.calPoint[1][i].byCheckPointNum = m_nICheckPointNum[i];
//		for(j=0; j<15 && j<m_nICheckPointNum[i]; j++)
//		{
//			aNetCalPoint.calPoint[1][i].lCheckPoint[j] = FLOAT2LONG(m_ICheckPoint[i][j]);
//		}
//	}
//	
//	return aNetCalPoint;
//}
