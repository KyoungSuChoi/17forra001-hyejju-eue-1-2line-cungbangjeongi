// UserSetDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "UserSetDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUserSetDlg dialog


bool CUserSetDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CUserSetDlg"), _T("TEXT_CUserSetDlg_CNT"), _T("TEXT_CUserSetDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CUserSetDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CUserSetDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

CUserSetDlg::CUserSetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUserSetDlg::IDD, pParent)
{
	LanguageinitMonConfig();

	//{{AFX_DATA_INIT(CUserSetDlg)
	m_nTrayColSize = 0;
	m_bDispCapSum = FALSE;
	m_bHideNonCellData = FALSE;
	//}}AFX_DATA_INIT
}
CUserSetDlg::~CUserSetDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

void CUserSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CUserSetDlg)
	DDX_Control(pDX, IDC_FONT_NAME, m_ctrFontResult);
	DDX_Control(pDX, IDC_TIME_UNIT_COMBO, m_ctrlTimeUnit);
	DDX_Control(pDX, IDC_C_PRESI_COMBO, m_ctrlCDecimal);
	DDX_Control(pDX, IDC_I_PRESI_COMBO, m_ctrlIDecimal);
	DDX_Control(pDX, IDC_V_PRESI_COMBO, m_ctrlVDecimal);
	DDX_Control(pDX, IDC_C_UNIT_COMBO, m_ctrlCUnit);
	DDX_Control(pDX, IDC_I_UNIT_COMBO, m_ctrlIUnit);
	DDX_Control(pDX, IDC_V_UNIT_COMBO, m_ctrlVUnit);
	DDX_Text(pDX, 1078, m_nTrayColSize);
	DDX_Check(pDX, IDC_CAP_SUM_CHECK, m_bDispCapSum);
	DDX_Check(pDX, IDC_CHECK_NONCELL_DATA, m_bHideNonCellData);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CUserSetDlg, CDialog)
	//{{AFX_MSG_MAP(CUserSetDlg)
	ON_BN_CLICKED(IDC_FONT_SET_BTN, OnFontSetBtn)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUserSetDlg message handlers

BOOL CUserSetDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	//////////////////////////////////////////////////////////////////////////

	m_nTrayColSize = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "TrayColSize", EP_DEFAULT_TRAY_COL_COUNT);

	UINT nSize;
	LPVOID* pData;
	BOOL nRtn = AfxGetApp()->GetProfileBinary(FORMSET_REG_SECTION, "GridFont", (LPBYTE *)&pData, &nSize);
	if(nSize > 0 && nRtn)	
	{
		memcpy(&m_afont, pData, nSize);
	}
	else
	{
		CFont *pFont = GetFont();
		pFont->GetLogFont(&m_afont);
	}
	delete [] pData;

	char szBuff[32], szUnit[16], szDecimalPoint[16];
	sprintf(szBuff, AfxGetApp()->GetProfileString(FORMSET_REG_SECTION, "V Unit", "V 0"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_ctrlVUnit.SelectString(0, szUnit);
	m_ctrlVDecimal.SelectString(0,szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(FORMSET_REG_SECTION, "I Unit", "mA 0"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_ctrlIUnit.SelectString(0,szUnit);
	m_ctrlIDecimal.SelectString(0,szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(FORMSET_REG_SECTION, "C Unit", "mAh 0"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);

#ifdef _EDLC_TEST_SYSTEM
	m_ctrlCUnit.ResetContent();
	m_ctrlCUnit.AddString("F");
	m_ctrlCUnit.AddString("mF");
#endif

	m_ctrlCUnit.SelectString(0,szUnit);
	m_ctrlCDecimal.SelectString(0,szDecimalPoint);
	
	sprintf(szBuff, AfxGetApp()->GetProfileString(FORMSET_REG_SECTION, "Time Unit", "0"));
	m_ctrlTimeUnit.SetCurSel(atol(szBuff));
	//////////////////////////////////////////////////////////////////////////
	
	m_bDispCapSum = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "Capacity Sum", FALSE);
	m_bHideNonCellData = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "HideNonCellData", FALSE);

	GetDlgItem(IDC_EDIT_EXCEL_PATH)->SetWindowText(AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"Excel Path", ""));

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CUserSetDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
	
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "TrayColSize", m_nTrayColSize);

	AfxGetApp()->WriteProfileBinary(FORMSET_REG_SECTION, "GridFont",(LPBYTE)&m_afont, sizeof(LOGFONT));
	
	char szBuff[32], szUnit[16], szDecimalPoint[16];
	m_ctrlVUnit.GetLBText(m_ctrlVUnit.GetCurSel(), szUnit);
	m_ctrlVDecimal.GetLBText(m_ctrlVDecimal.GetCurSel(), szDecimalPoint);
	sprintf(szBuff, "%s %s", szUnit, szDecimalPoint);
	AfxGetApp()->WriteProfileString(FORMSET_REG_SECTION, "V Unit", szBuff);

	m_ctrlIUnit.GetLBText(m_ctrlIUnit.GetCurSel(), szUnit);
	m_ctrlIDecimal.GetLBText(m_ctrlIDecimal.GetCurSel(), szDecimalPoint);
	sprintf(szBuff, "%s %s", szUnit, szDecimalPoint);
	AfxGetApp()->WriteProfileString(FORMSET_REG_SECTION, "I Unit", szBuff);

	m_ctrlCUnit.GetLBText(m_ctrlCUnit.GetCurSel(), szUnit);
	m_ctrlCDecimal.GetLBText(m_ctrlCDecimal.GetCurSel(), szDecimalPoint);
	sprintf(szBuff, "%s %s", szUnit, szDecimalPoint);
	AfxGetApp()->WriteProfileString(FORMSET_REG_SECTION, "C Unit", szBuff);

	sprintf(szBuff, "%d", m_ctrlTimeUnit.GetCurSel());
	AfxGetApp()->WriteProfileString(FORMSET_REG_SECTION, "Time Unit", szBuff);
	
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "Capacity Sum", m_bDispCapSum);

	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "HideNonCellData", m_bHideNonCellData);

	CString strTemp;
	GetDlgItem(IDC_EDIT_EXCEL_PATH)->GetWindowText(strTemp);
	AfxGetApp()->WriteProfileString(FORM_PATH_REG_SECTION ,"Excel Path", strTemp);

	CDialog::OnOK();
}

void CUserSetDlg::OnFontSetBtn() 
{
	// TODO: Add your control notification handler code here
	CFontDialog aDlg(&m_afont);
	if(aDlg.DoModal()==IDOK)
	{
		aDlg.GetCurrentFont(&m_afont);
		m_ctrFontResult.SetUserFont(&m_afont);
	}	
}

void CUserSetDlg::OnButton1() 
{
	// TODO: Add your control notification handler code here
	CString str;
	GetDlgItem(IDC_EDIT_EXCEL_PATH)->GetWindowText(str);
	if(str.IsEmpty())
	{
		str = "C:\\Program Files\\Microsoft Office\\OFFICE11\\EXCEL.EXE";
	}
	
	CFileDialog pDlg(TRUE, "exe", str, OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, "exe file(*.exe)|*.exe|All Files(*.*)|*.*|");
	pDlg.m_ofn.lpstrTitle = TEXT_LANG[0];//"Excel 파일 위치"
	if(IDOK == pDlg.DoModal())
	{
		str = pDlg.GetPathName();
		GetDlgItem(IDC_EDIT_EXCEL_PATH)->SetWindowText(str);	
	}		
}
