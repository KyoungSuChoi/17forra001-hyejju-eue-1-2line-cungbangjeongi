#pragma once

// CStopperSettingDlg 대화 상자입니다.

class CStopperSettingDlg : public CDialog
{
	DECLARE_DYNAMIC(CStopperSettingDlg)

public:
	CStopperSettingDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CStopperSettingDlg();
	
	void InitLabel();
	void InitFont();
	
	CFont m_Font;

// 대화 상자 데이터입니다.
	enum { IDD = IDD_STOOPER_SETTING };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	CLabel m_Label1;
	CLabel m_Label2;
	CLabel m_Label3;
	CLabel m_Label4;
	virtual BOOL OnInitDialog();
	CString m_Stopper1;
	CString m_Stopper2;
	CString m_Stopper3;
	CString m_Stopper4;

	CString m_strLabelName1;
	CString m_strLabelName2;
	CString m_strLabelName3;
	CString m_strLabelName4;
};
