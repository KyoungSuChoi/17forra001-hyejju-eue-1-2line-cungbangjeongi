#if !defined(AFX_MYGRIDWND_H__345D1683_26CE_11D2_975A_444553540000__INCLUDED_)
#define AFX_MYGRIDWND_H__345D1683_26CE_11D2_975A_444553540000__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// MyGridWnd.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMyGridWnd window
struct COLORARRAY
{
	COLORREF BackColor;
	COLORREF TextColor;
};
//Grid User Message define
#define WM_GRID_DOUBLECLICK		WM_USER+101
#define WM_TOOLBAR_COMBO1		WM_USER+102
#define WM_TOOLBAR_COMBO2		WM_USER+103
#define WM_COMBO_SELECT			WM_USER+104
#define WM_TOP_CHANNEL_REFLASH	WM_USER+105
#define WM_GRID_KILLFOCUS		WM_USER+106
#define WM_GRID_SETFOCUS		WM_USER+107
#define WM_CALLBACK_ERROR		WM_USER+108
#define WM_SEL_CHANGED			WM_USER+109
#define WM_GRID_MOVECELL		WM_USER+110
#define WM_GRID_BEGINEDIT		WM_USER+111
#define WM_GRID_ENDEDIT			WM_USER+112
#define WM_GRID_CANCELEDIT		WM_USER+113
#define WM_GRID_MOVEROW			WM_USER+114
#define WM_DATETIME_CHANGED		WM_USER+115
#define	WM_GRID_BTNCLICK		WM_USER+116
#define WM_GRID_CLICK			WM_USER+117
#define WM_GRID_RIGHT_CLICK		WM_USER+118
#define WM_GRID_ROW_DRAG_DROP	WM_USER+119
#define WM_END_DLG_CLOSE		WM_USER+120
#define WM_GRID_MOUSE_MOVEOVER	WM_USER+122
#define WM_CHECKBOX_CLICKED		WM_USER+124

#define MAX_COLOR_ARRAY 20

class CMyGridWnd : public CGXGridWnd
{
// Construction
public:
	CMyGridWnd();
	virtual ~CMyGridWnd();

// Attributes
public:
	BOOL		m_bSelectRange;
	CGXRange	m_SelectRange;
	COLORREF	m_BackColor;
	COLORREF	m_SelColor;
	BOOL		m_bSameColSize;
	BOOL		m_bSameRowSize;
	ROWCOL		m_nCol1, m_nCol2;
	COLORREF	m_ColHeaderFgColor[16];
	COLORREF	m_ColHeaderBgColor[16];
	BOOL		m_bCustomColor;
	CGXRange	m_CustomColorRange;
	char *		m_pCustomColorFlag;
	COLORARRAY	m_ColorArray[MAX_COLOR_ARRAY];
	BOOL		m_bRowSelection;
	BOOL		m_bCanDelete;
	BOOL		m_bCustomWidth;
	int			m_nWidth[64];
	ROWCOL		m_nOutlineRow;
	ROWCOL		m_nOutlineCol;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyGridWnd)
	//}}AFX_VIRTUAL

// Implementation
public:
	BOOL OnRButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	BOOL OnLButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	void OnClickedButtonRowCol(ROWCOL nRow, ROWCOL nCol);
	void OnInitialUpdate();
	BOOL OnLButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	BOOL OnStartSelection(ROWCOL nRow, ROWCOL nCol, UINT flags, CPoint point);
	BOOL CanChangeSelection(CGXRange* pRange, BOOL bIsDragging, BOOL bKey);
	int  GetColWidth(ROWCOL nCol);
	int  GetRowHeight(ROWCOL nRow);
	BOOL GetStyleRowCol(ROWCOL nRow, ROWCOL nCol, CGXStyle& style, GXModifyType mt, int nType);
	BOOL CanSelectCurrentCell(BOOL bSelect, ROWCOL dwSelectRow, ROWCOL dwSelectCol, ROWCOL dwOldRow, ROWCOL dwOldCol);
	void OnChangedSelection(const CGXRange* pRange, BOOL bIsDragging, BOOL bKey);
	void DrawInvertCell(CDC* /*pDC*/, ROWCOL nRow, ROWCOL nCol, CRect rectItem);
	BOOL OnDeleteCell(ROWCOL nRow, ROWCOL nCol);
	BOOL OnLeftCell(ROWCOL nRow, ROWCOL nCol, ROWCOL nNewRow, ROWCOL nNewCol);
	BOOL OnStartEditing(ROWCOL nRow, ROWCOL nCol);
	void OnCanceledEditing(ROWCOL nRow, ROWCOL nCol);
	void OnMovedCurrentCell(ROWCOL nRow, ROWCOL nCol);
//	BOOL OnMouseMoveOver(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	// Generated message map functions
protected:
	//{{AFX_MSG(CMyGridWnd)
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MYGRIDWND_H__345D1683_26CE_11D2_975A_444553540000__INCLUDED_)
