//---------------------------------------------------------------------------
#include <vcl\vcl.h>
#pragma hdrstop

#include "Unit4.h"
#include <math.h>

//---------------------------------------------------------------------------
#pragma link "Pepsvcl"
#pragma resource "*.dfm"
TForm4 *Form4;
//---------------------------------------------------------------------------
__fastcall TForm4::TForm4(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm4::FormShow(TObject *Sender)
{
int s, p;

PEPGraph1->NullDataValueX = -99999;
PEPGraph1->NullDataValueY = -99999;

PEPGraph1->PrepareImages = True;
PEPGraph1->Subsets = 2;
PEPGraph1->Points = 360;
s = 0;
for (p=0; p<360; p++)
{
    PEPGraph1->XData[s][p] = p;
    PEPGraph1->YData[s][p] = (sin(p * 0.054) * 1);
}
s = 1;
for (p=0; p<360; p++)
{
    PEPGraph1->XData[s][p] = p;
    PEPGraph1->YData[s][p] = (sin(p * 0.044) * 1);
}

PEPGraph1->ComparisonSubsets = 1;
PEPGraph1->PlottingMethod = psLine;
PEPGraph1->PlottingMethodII = psPoint2;
PEPGraph1->AllowZooming = psHorzPlusVertZooming;
PEPGraph1->CursorPromptStyle = psDegreeAndMagnitudeVal;
PEPGraph1->CursorPromptTracking = True;
PEPGraph1->MarkDataPoints = True;
PEPGraph1->SubsetLineTypes[0] = PELT_THINSOLID;
PEPGraph1->SubsetLineTypes[1] = 0;

PEPGraph1->SubsetColors[0] = clRed;
PEPGraph1->SubsetColors[1] = clLime;

PEPGraph1->SubsetLabels[0] = "Signal #1";
PEPGraph1->SubsetLabels[1] = "Signal #2";

//{PEPGraph1->SmithChart = True}
PEPGraph1->FocalRect = False;

PEPGraph1->PEactions = psReinitAndReset;
}
//--------------------------------------------------------------------------- 