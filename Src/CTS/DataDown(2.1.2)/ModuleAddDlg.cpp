// ModuleAddDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DataDown.h"
#include "ModuleAddDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CModuleAddDlg dialog


CModuleAddDlg::CModuleAddDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CModuleAddDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CModuleAddDlg)
	m_nModuleID = 0;
	//}}AFX_DATA_INIT

//	m_nModulePerRack = 3;
//	m_bUseGroupSet = FALSE;
//	m_bUseRackIndex = FALSE;
//	m_strGroupName = "Group";
//	m_strModuleName = "Module";

}


void CModuleAddDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CModuleAddDlg)
	DDX_Control(pDX, IDC_MODULE_SEL_COMBO, m_ctrlModuleSel);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CModuleAddDlg, CDialog)
	//{{AFX_MSG_MAP(CModuleAddDlg)
	ON_EN_CHANGE(IDC_MODULENUM, OnChangeModulenum)
	ON_CBN_SELCHANGE(IDC_MODULE_SEL_COMBO, OnSelchangeModuleSelCombo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CModuleAddDlg message handlers

void CModuleAddDlg::OnChangeModulenum() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(m_nModuleID <1 || m_nModuleID > 0x7FFF)
	{
		AfxMessageBox("Invalid module ID.");
	}
}

void CModuleAddDlg::OnOK() 
{
	// TODO: Add extra validation here
	if(m_nModuleID <1 || m_nModuleID > 0x7FFF)
	{
		AfxMessageBox("Invalid module ID.");
		return;
	}
	CDialog::OnOK();
}

BOOL CModuleAddDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	// TODO: Add extra initialization here

	//Registry에서 PowerFormation DataBase 경로를 검색 
	long rtn;
	HKEY hKey = 0;
	BYTE buf[512];//, buf2[512];
	DWORD size = 511;
	DWORD type;
	
	CString strDBPath;

	rtn = ::RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\CTSMon\\Path", 0, KEY_READ, &hKey);
	if(ERROR_SUCCESS == rtn)
	{	
		//PowerFormation DataBase 경로를 읽어온다. 
		rtn = ::RegQueryValueEx(hKey, "DataBase", NULL, &type, buf, &size);
		::RegCloseKey(hKey);		
		
		if(ERROR_SUCCESS == rtn && (type == REG_SZ || type == REG_EXPAND_SZ))
				strDBPath.Format("%s\\%s", buf, FORM_SET_DATABASE_NAME);		
	}

/*	rtn = ::RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\CTSMon\\FormSetting", 0, KEY_READ, &hKey);
	if(ERROR_SUCCESS == rtn)
	{
		rtn = ::RegQueryValueEx(hKey, "Module Name", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && (type == REG_SZ || type == REG_EXPAND_SZ))
		{
			m_strModuleName = buf;
		}
		size = 255;
		rtn = ::RegQueryValueEx(hKey, "Group Name", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && (type == REG_SZ || type == REG_EXPAND_SZ))
		{
			m_strGroupName = buf;
		}
		size = 255;
		rtn = ::RegQueryValueEx(hKey, "Module Per Rack", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && type == REG_DWORD)
		{
			sprintf((char *)buf2, "%ld", *(long *)buf);
			m_nModulePerRack = atol((LPCTSTR)buf2);
		}
		size = 255;
		rtn = ::RegQueryValueEx(hKey, "Use Rack Index", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && type == REG_DWORD)
		{
			sprintf((char *)buf2, "%ld", *(long *)buf);
			m_bUseRackIndex = atol((LPCTSTR)buf2);
		}
		size = 255;
		rtn = ::RegQueryValueEx(hKey, "Use Group", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && type == REG_DWORD)
		{
			sprintf((char *)buf2, "%ld", *(long *)buf);
			m_bUseGroupSet = atol((LPCTSTR)buf2);
		}
		::RegCloseKey(hKey);
	}
*/
	int nCount = 0;
	CString strTemp;
	int nSelIndex = 0;
	CDaoDatabase  db;
	
	try
	{
		db.Open(strDBPath);

		CString strSQL("SELECT data3, ModuleID FROM SystemConfig ORDER BY ModuleID");
		CDaoRecordset rs(&db);
			rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
			while(!rs.IsEOF())
			{
				COleVariant data = rs.GetFieldValue(0);
				strTemp.Format("%s", data.pbVal);
				m_ctrlModuleSel.AddString(strTemp);
				
				if(data.lVal == m_nModuleID)
					nSelIndex = nCount;
				
				data = rs.GetFieldValue(1);
				m_ctrlModuleSel.SetItemData(nCount++, data.lVal);
				rs.MoveNext();
			}
		rs.Close();
		db.Close();	
	}
	catch (CDaoException *e)
	{
	//	e->GetErrorMessage();
		e->Delete();
	}

	m_ctrlModuleSel.SetCurSel(nSelIndex);
	m_nModuleID = m_ctrlModuleSel.GetItemData(nSelIndex);
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

int CModuleAddDlg::GetModuleID()
{
	return m_nModuleID;
}

/*
CString CModuleAddDlg::ModuleName(int nModuleID, int nGroupIndex)
{
	CString strName;
	
	if(m_bUseRackIndex)
	{
		div_t div_result;
		div_result = div( nModuleID-1, m_nModulePerRack );

		if(m_bUseGroupSet && nGroupIndex >= 0)
		{
			strName.Format("%s %d-%d, %s %d", m_strModuleName, div_result.quot+1, div_result.rem+1, m_strGroupName, nGroupIndex+1);
		}
		else
		{
			strName.Format("%s %d-%d", m_strModuleName, div_result.quot+1, div_result.rem+1);
		}
	}
	else
	{
		if(m_bUseGroupSet && nGroupIndex >= 0)
		{
			strName.Format("%s %d, %s %d", m_strModuleName, nModuleID,  m_strGroupName, nGroupIndex+1);
		}
		else
		{
			strName.Format("%s %d", m_strModuleName, nModuleID);
		}
	}
	return strName;
}
*/
void CModuleAddDlg::OnSelchangeModuleSelCombo() 
{
	// TODO: Add your control notification handler code here
	int index =m_ctrlModuleSel.GetCurSel();
	if(index >= 0)
		m_nModuleID = m_ctrlModuleSel.GetItemData(index);
}
