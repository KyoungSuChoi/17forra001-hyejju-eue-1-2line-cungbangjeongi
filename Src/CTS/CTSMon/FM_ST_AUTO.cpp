#include "stdafx.h"

#include "CTSMon.h"

#include "FMS.h"
#include "FM_Unit.h"

#include "FM_STATE.h"

CFM_ST_AUTO::CFM_ST_AUTO(FM_STATUS_ID _Eqstid, FM_STATUS_ID _stid, CFM_Unit* _unit)
:CFM_STATE(_Eqstid, _stid, _unit)
, m_pstATVacancy(NULL)
, m_pstATReady(NULL)
, m_pstATTrayIn(NULL)
, m_pstATContacCheck(NULL)
, m_pstATRun(NULL)
, m_pstATEnd(NULL)
, m_pstATError(NULL)
{
}


CFM_ST_AUTO::~CFM_ST_AUTO(void)
{
	if(m_pstATVacancy)
	{
		delete m_pstATVacancy;
		m_pstATVacancy = NULL;
	}
	if(m_pstATReady)
	{
		delete m_pstATReady;
		m_pstATReady = NULL;
	}
	if(m_pstATTrayIn)
	{
		delete m_pstATTrayIn;
		m_pstATTrayIn = NULL;
	}
	if(m_pstATContacCheck)
	{
		delete m_pstATContacCheck;
		m_pstATContacCheck = NULL;
	}
	if(m_pstATRun)
	{
		delete m_pstATRun;
		m_pstATRun = NULL;
	}
	if(m_pstATEnd)
	{
		delete m_pstATEnd;
		m_pstATEnd = NULL;
	}
	if(m_pstATError)
	{
		delete m_pstATError;
		m_pstATError = NULL;
	}
}

VOID CFM_ST_AUTO::fnEnter()
{
	TRACE("%s \n", g_strSBCState[m_Unit->fnGetModule()->GetState()]);

	switch(m_Unit->fnGetModule()->GetState())
	{
	case EP_STATE_IDLE:
		if(m_Unit->fnGetModuleTrayState())
		{
			CHANGE_STATE(AUTO_ST_TRAY_IN);
		}
		else
		{
			CHANGE_STATE(AUTO_ST_VACANCY);
		}
		break;
	case EP_STATE_STANDBY:
		{
			CHANGE_STATE(AUTO_ST_TRAY_IN);
			//공정 진행
			if(m_Unit->fnGetFMS()->fnGetWorkCode() == 0)
			{
				m_Unit->fnGetFMS()->fnReStartSet(0);
			}
		}
		break;
	case EP_STATE_PAUSE:
		CHANGE_STATE(AUTO_ST_ERROR);
		break;
	case EP_STATE_RUN:
		{
			if(m_Unit->fnGetModule()->GetState(TRUE) == EP_STATE_CHECK)
			{
				CHANGE_STATE(AUTO_ST_CONTACT_CHECK);
			}
			else
			{
				CHANGE_STATE(AUTO_ST_RUN);
			}
			
			//공정 진행
			if(m_Unit->fnGetFMS()->fnGetWorkCode() == 0)
			{
				m_Unit->fnGetFMS()->fnReStartSet(0);
			}
		}
		break;
	case EP_STATE_READY:
		{
			CHANGE_STATE(AUTO_ST_READY);
			//공정 진행
			if(m_Unit->fnGetFMS()->fnGetWorkCode() == 0)
			{
				m_Unit->fnGetFMS()->fnReStartSet(0);
			}
		}
		break;
	case EP_STATE_END:
		{
			CHANGE_STATE(AUTO_ST_END);
			//공정 진행
			if(m_Unit->fnGetFMS()->fnGetWorkCode() == 0)
			{
				m_Unit->fnGetFMS()->fnReStartSet(0);
			}
		}
		break;
	case EP_STATE_ERROR:
		{
			CHANGE_STATE(AUTO_ST_ERROR);
		}
		break;
	}

	//if(m_Unit->fnGetFMS()->fnGetLastState() == FMS_ST_ERROR)
	//{
	//	CHANGE_STATE(AUTO_ST_ERROR);
	//}
	//TRACE("CFM_ST_AUTO::fnEnter %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_AUTO::fnProc()
{
	TRACE("CFM_ST_AUTO::fnProc %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_AUTO::fnExit()
{
	TRACE("CFM_ST_AUTO::fnExit %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_AUTO::fnSBCPorcess(WORD _state)
{
	WORD state = m_Unit->fnGetModule()->GetState();
	if(EP_STATE_LINE_OFF == state)
	{
		CHANGE_STATE(EQUIP_ST_OFF);
	}
	else
	{
		switch(m_Unit->fnGetModule()->GetOperationMode())
		{
		case EP_OPERATION_LOCAL:
			CHANGE_STATE(EQUIP_ST_LOCAL);
			break;
		case EP_OPEARTION_MAINTENANCE:
			CHANGE_STATE(EQUIP_ST_MAINT);
			break;
		case EP_OPERATION_AUTO:
			//CHANGE_STATE(EQUIP_ST_AUTO);
			break;
		}
	}

	//TRACE("CFM_ST_AUTO::fnSBCPorcess %d \n", m_Unit->fnGetModuleIdx());
}

FMS_ERRORCODE CFM_ST_AUTO::fnFMSPorcess(FMS_COMMAND _msgId, st_FMS_PACKET* _recvData)
{
	FMS_ERRORCODE _error = ER_NO_Process;

	switch(_msgId)
	{
	default:
		{
		}
		break;
	}
	TRACE("CFM_ST_AUTO::fnFMSPorcess %d \n", m_Unit->fnGetModuleIdx());

	return _error;

}
