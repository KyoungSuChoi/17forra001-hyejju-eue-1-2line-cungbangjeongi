// SelectTrayDataDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "SelectTrayDataDlg.h"

#include "TestLogSet.h"
#include "ChResultSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelectTrayDataDlg dialog


bool CSelectTrayDataDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CSelectTrayDataDlg"), _T("TEXT_CSelectTrayDataDlg_CNT"), _T("TEXT_CSelectTrayDataDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CSelectTrayDataDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CSelectTrayDataDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

CSelectTrayDataDlg::CSelectTrayDataDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectTrayDataDlg::IDD, pParent)
{
	LanguageinitMonConfig();

	//{{AFX_DATA_INIT(CSelectTrayDataDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CSelectTrayDataDlg::~CSelectTrayDataDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


void CSelectTrayDataDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelectTrayDataDlg)
	DDX_Control(pDX, IDC_TITLE_LABEL, m_ctrlDataTitleLabel);
	DDX_Control(pDX, IDC_SEARCH_NO_LABEL, m_ctrlSearchNoLabel);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelectTrayDataDlg, CDialog)
	//{{AFX_MSG_MAP(CSelectTrayDataDlg)
	ON_BN_CLICKED(IDC_LIST_SAVE, OnListSave)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_GRID_DOUBLECLICK, OnGridDoubleClick)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelectTrayDataDlg message handlers

BOOL CSelectTrayDataDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	InitTrayListGrid();


	RequeryTrayList();
	
	m_ctrlDataTitleLabel.SetBkColor(RGB(255, 255, 255));
	m_ctrlDataTitleLabel.SetTextColor(RGB(0, 0, 255));
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CSelectTrayDataDlg::InitTrayListGrid()
{
	
	m_wndTrayListGrid.SubclassDlgItem(IDC_TRAY_LIST, this);
	m_wndTrayListGrid.m_bSameRowSize = FALSE;
	m_wndTrayListGrid.m_bSameColSize = FALSE;
//---------------------------------------------------------------------//
	m_wndTrayListGrid.m_bCustomWidth = TRUE;
	m_wndTrayListGrid.m_bCustomColor = FALSE;

	m_wndTrayListGrid.m_nWidth[1] = 0;
	m_wndTrayListGrid.m_nWidth[2] = 50;
	m_wndTrayListGrid.m_nWidth[3] = 100;
	m_wndTrayListGrid.m_nWidth[4] = 150;
	m_wndTrayListGrid.m_nWidth[5] = 180;
	m_wndTrayListGrid.m_nWidth[6] = 80;
	m_wndTrayListGrid.m_nWidth[7] = 80;
	m_wndTrayListGrid.m_nWidth[8] = 80;
	m_wndTrayListGrid.m_nWidth[9] = 80;

	m_wndTrayListGrid.Initialize();
	m_wndTrayListGrid.LockUpdate();

	m_wndTrayListGrid.SetColCount(9);        
	m_wndTrayListGrid.SetDefaultRowHeight(20);
	m_wndTrayListGrid.EnableCellTips();
	m_wndTrayListGrid.GetParam()->SetSortRowsOnDblClk(TRUE);
	
	//Row Header Setting
	m_wndTrayListGrid.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(9).SetFaceName(TEXT_LANG[0])).SetWrapText(FALSE).SetAutoSize(TRUE));//"굴림"
	m_wndTrayListGrid.SetStyleRange(CGXRange().SetCols(1),	CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(0,0,0)));
		
	m_wndTrayListGrid.SetValueRange(CGXRange( 0, 0), TEXT_LANG[1]);//"TestLogID"
	m_wndTrayListGrid.SetValueRange(CGXRange( 0, 1), TEXT_LANG[2]);//"Test Serial"
	m_wndTrayListGrid.SetValueRange(CGXRange( 0, 2), TEXT_LANG[3]);//"순서"
	m_wndTrayListGrid.SetValueRange(CGXRange( 0, 3), TEXT_LANG[4]);//"Tray이름"
	m_wndTrayListGrid.SetValueRange(CGXRange( 0, 4), TEXT_LANG[5]);//"Lot 번호"
	m_wndTrayListGrid.SetValueRange(CGXRange( 0, 5), TEXT_LANG[6]);//"적용 공정"
	m_wndTrayListGrid.SetValueRange(CGXRange( 0, 6), TEXT_LANG[7]);//"입력 수량"
	m_wndTrayListGrid.SetValueRange(CGXRange( 0, 7), TEXT_LANG[8]);//"정상수"
	m_wndTrayListGrid.SetValueRange(CGXRange( 0, 8), TEXT_LANG[9]);//"불량률"
	m_wndTrayListGrid.SetValueRange(CGXRange( 0, 9), TEXT_LANG[10]);//"완료"

	m_wndTrayListGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	
//	m_wndTrayListGrid.SetScrollBarMode(SB_BOTH, gxnDisabled,TRUE);
	m_wndTrayListGrid.LockUpdate(FALSE);
	m_wndTrayListGrid.Redraw();
	return TRUE;
}

BOOL CSelectTrayDataDlg::RequeryTrayList()
{
	CTestLogSet testLogSet;
	CChResultSet chResultSet;
	CString strTemp;
	if(m_strFilterString.IsEmpty())		return FALSE;

	if(m_nFilterType == DATA_LOT)
	{
//		testLogSet.m_strFilter.Format("[LotNo] = '%s'", m_strFilterString);	
		strTemp.Format(TEXT_LANG[11], m_strFilterString);//"[%s] Lot의 Cell Data"
	}
	else if(m_nFilterType == DATA_TRAY)
	{
//		testLogSet.m_strFilter.Format("[TrayNo] = '%s'", m_strFilterString);
		strTemp.Format(TEXT_LANG[12], m_strFilterString);//"[%s] Tray에서 시행된 Cell Data"
	}
	else
	{
//		testLogSet.m_strFilter.Format("[ModelName] LIKE '%s'", m_strFilterString);
		strTemp.Format(TEXT_LANG[13], m_strFilterString);//"[%s] Model 공정를 시행한 Cell Data"
	}

	
	testLogSet.m_strFilter = m_strFilterString;
	m_ctrlDataTitleLabel.SetText(strTemp);

	testLogSet.m_strSort.Format("[TestLogID] ASC");
	testLogSet.Open();

	chResultSet.m_strSort.Format("[Index] DESC");
	chResultSet.Open();

	if(testLogSet.IsBOF())
	{
		testLogSet.Close();
		return FALSE;
	}

	BeginWaitCursor();
	int row = m_wndTrayListGrid.GetRowCount();
	if(row > 0)
	{
		m_wndTrayListGrid.RemoveRows(1, row);
	}
	row = 1;
	int nNormalCount, nNoncellCount, nTemp;
	while(!testLogSet.IsEOF())
	{
		m_wndTrayListGrid.InsertRows(row, 1);
		m_wndTrayListGrid.SetValueRange(CGXRange(row, 0), testLogSet.m_TestLogID);
		m_wndTrayListGrid.SetValueRange(CGXRange(row, 1), testLogSet.m_TestSerialNo);
		m_wndTrayListGrid.SetValueRange(CGXRange(row, 2), ROWCOL(row));
		m_wndTrayListGrid.SetValueRange(CGXRange(row, 3), testLogSet.m_TrayNo);
		m_wndTrayListGrid.SetValueRange(CGXRange(row, 4), testLogSet.m_LotNo);
		m_wndTrayListGrid.SetValueRange(CGXRange(row, 5), testLogSet.m_ModelName);

		nNormalCount = 0;
		nNoncellCount = 0;
		if(!testLogSet.m_TestSerialNo.IsEmpty())
		{
			
			chResultSet.m_strFilter.Format("[TestSerialNo] = '%s'", testLogSet.m_TestSerialNo);
			chResultSet.Requery();
			if(!chResultSet.IsBOF())
			{
				for(int i=0; i<EP_BATTERY_PER_TRAY; i++)
				{
					if(IsNonCell((BYTE)chResultSet.m_ch[i]))	nNoncellCount++;
					if(IsNormalCell((BYTE)chResultSet.m_ch[i]))		nNormalCount++;
				}
			}
		}
		
		nTemp = EP_BATTERY_PER_TRAY-nNoncellCount;			//입력 수량 
		m_wndTrayListGrid.SetValueRange(CGXRange(row, 6), ROWCOL(nTemp));
		m_wndTrayListGrid.SetValueRange(CGXRange(row, 7), ROWCOL(nNormalCount));
		if(EP_BATTERY_PER_TRAY == nNoncellCount)
		{
			strTemp.Empty();
		}
		else
		{
			strTemp.Format("%.1f%%", float(nTemp-nNormalCount)/float(EP_BATTERY_PER_TRAY-nNoncellCount)*100.0f);
		}
		m_wndTrayListGrid.SetValueRange(CGXRange(row, 8), strTemp);
		if(testLogSet.m_TestDone)
		{
			m_wndTrayListGrid.SetValueRange(CGXRange(row, 9), TEXT_LANG[10]);//"완료"
		}
		else
		{
			m_wndTrayListGrid.SetValueRange(CGXRange(row, 9), TEXT_LANG[14]);//"진행중"
		}
		testLogSet.MoveNext();
		row++;
	}
	strTemp.Format(TEXT_LANG[15], row-1);//"Total %d개의 Data가 검색 되었습니다."
	m_ctrlSearchNoLabel.SetText(strTemp);
	chResultSet.Close();
	testLogSet.Close();

	EndWaitCursor();
	return TRUE;
}


LONG CSelectTrayDataDlg::OnGridDoubleClick(WPARAM wParam, LPARAM /*lParam*/)
{
	ROWCOL nRow, nCol;
	CString str,CellNO;

//	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;
	nCol = LOWORD(wParam);
	nRow = HIWORD(wParam);

	if(nRow <1 || nCol <1)		return 0;
	m_strTestLogID = m_wndTrayListGrid.GetValueRowCol(nRow, 0);
	m_TestSerialNo = m_wndTrayListGrid.GetValueRowCol(nRow, 1);
	OnOK();
	return 0;	
}

void CSelectTrayDataDlg::OnOK() 
{
	// TODO: Add extra validation here
	ROWCOL nRow, nCol;
	if(m_wndTrayListGrid.GetCurrentCell(nRow, nCol) == FALSE)
	{
		m_strTestLogID.Empty();
		AfxMessageBox(TEXT_LANG[16]);//"선택된 Data가 없습니다."
		return;
	}
	else
	{
		if(nRow<1 || nCol<1)
		{
			m_strTestLogID.Empty();
		}
		else
		{
			m_strTestLogID = m_wndTrayListGrid.GetValueRowCol(nRow, 0);
			m_TestSerialNo = m_wndTrayListGrid.GetValueRowCol(nRow, 1);
		}
	}
	CDialog::OnOK();
}

void CSelectTrayDataDlg::OnListSave() 
{
	// TODO: Add your control notification handler code here
	CString strFileName;
	strFileName.Format("%s.csv", "Tray List");
		
	CFileDialog pDlg(FALSE, "", strFileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "csv 파일(*.csv)|*.csv|");

	if(IDOK == pDlg.DoModal())
	{
		strFileName = pDlg.GetPathName();
		if(strFileName.IsEmpty())	return ;

		FILE *fp = fopen(strFileName, "wt");
		if(fp == NULL)	return ;

		BeginWaitCursor();

		CString strTemp;
		GetDlgItem(IDC_TITLE_LABEL)->GetWindowText(strTemp);
		fprintf(fp, TEXT_LANG[17]+":, %s\n", strTemp);	//"검색 범위"
		GetDlgItem(IDC_SEARCH_NO_LABEL)->GetWindowText(strTemp);
		fprintf(fp, "%s\n\n", strTemp);	// FileName
		
		for(int row = 0; row<m_wndTrayListGrid.GetRowCount()+1; row++)
		{
			for(int col = 2; col<m_wndTrayListGrid.GetColCount(); col++)
			{
				fprintf(fp, "%s,", m_wndTrayListGrid.GetValueRowCol(row, col));
			}
			fprintf(fp, "\n");
		}
		fclose(fp);
		EndWaitCursor();
	}			
}
