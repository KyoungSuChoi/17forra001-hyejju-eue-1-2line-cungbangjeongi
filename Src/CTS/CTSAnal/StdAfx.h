// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//  are changed infrequently
//

#if !defined(AFX_STDAFX_H__24773B98_3062_4B2E_8624_F3611821DD5E__INCLUDED_)
#define AFX_STDAFX_H__24773B98_3062_4B2E_8624_F3611821DD5E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxdb.h>

#ifndef _AFX_NO_DAO_SUPPORT
#include <afxdao.h>			// MFC DAO database classes
#include <afxinet.h>
#endif // _AFX_NO_DAO_SUPPORT

#include <toolkit/secall.h>
#include <grid/gxall.h>
#include <math.h>

#include "../../../Include/Formall.h"
#include "../../../Include/ReportDataID.h"		//��� ���� Data ID
#include "Mydefine.h"
#include "Result.h"

#include "../../UtilityClass/FormUtility.h"
#include "../../../Include/IniParser.h"

extern int g_nLanguage;//TEXT_LANG[@]			
extern CString g_strLangPath;
enum LanguageType {LANGUAGE_KOR=0, LANGUAGE_ENG, LANGUAGE_CHI };

typedef double element;

typedef struct stackNode 
{			
	element data; 
	struct stackNode *link; 
}stackNode;

extern stackNode* top;

#include "NameInc.h"

#define RGB_MIDNIGHTBLUE	RGB(25,25,112)
#define RGB_WHITE			RGB(255,255,255)
#define RGB_CORNFLOWERBLUE	RGB(100,149,237)
#define RGB_DARKSLATEBLUE	RGB(72,61,141)
#define RGB_RED        RGB(220,  0,  0)
#define RGB_GREEN      RGB(  0,220,  0)
#define RGB_BLUE       RGB(  0,  0,127)
#define RGB_BLACK			RGB(0,0,0)
#define RGB_LTGRAY			RGB(192, 192, 192)
#define RGB_LIGHTRED   RGB(255,  0,  0)
#define RGB_LIGHTGREEN RGB(  0,255,  0)
#define RGB_LIGHTBLUE  RGB(  0,  0,255)
#define RGB_WHITE      RGB(255,255,255)
#define RGB_GRAY       RGB(192,192,192)
#define RGB_YELLO	   RGB(255,255,0)

#define RGB_LABEL_FONT_STAGENAME RGB(255, 255, 255)
#define RGB_LABEL_BACKGROUND_STAGENAME	RGB(0,0,0)

#define RGB_LABEL_FONT		RGB(255,255,255)
#define RGB_LABEL_BACKGROUND	RGB(72,61,141)

#define RGB_BTN_BACKGROUND	RGB(225, 225, 225)
#define RGB_BTN_FONT		RGB(0,0,0)
#define RGB_BTN_DISBKGND	RGB(255,255,255)

#ifdef _DEBUG
#pragma comment(lib, "../../../Lib/BFCommonD.lib")
#pragma message("Automatically linking with BFCommonD.lib By K.B.H ")
#else
#pragma comment(lib, "../../../Lib/BFCommon.lib")
#pragma message("Automatically linking with BFCommon.lib By K.B.H")
#endif	//_DEBUG

#ifdef _DEBUG
#pragma comment(lib, "../../../Lib/BFCtrlD.lib")
#pragma message("Automatically linking with BFCtrlD.lib By K.B.H ")
#else
#pragma comment(lib, "../../../Lib/BFCtrl.lib")
#pragma message("Automatically linking with BFCtrl.lib By K.B.H")
#endif	//_DEBUG

#ifdef _DEBUG
#pragma comment(lib, "../../../Lib/BFServerD.lib")
#pragma message("Automatically linking with BFServerD.lib By K.B.H ")
#else
#pragma comment(lib, "../../../Lib/BFServer.lib")
#pragma message("Automatically linking with BFServer.lib By K.B.H")
#endif	//_DEBUG


#if CUSTOMER_TYPE == LG_CHEMICAL || CUSTOMER_TYPE == LGC_PB5
	#define DSP_TYPE_VERTICAL
#else
	#define DSP_TYPE_HORIZENTAL
#endif

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__24773B98_3062_4B2E_8624_F3611821DD5E__INCLUDED_)
