#pragma once

class CFMSObj : public CFMSNetObj<CFMSObj>
{
public:

	CFMSObj();
	~CFMSObj(void);

	INT		strLinker(CHAR *msg, VOID * _dest, INT* mesmap);
	INT		strCutter(CHAR *msg, VOID * _dest, INT* mesmap);

	INT64 fnSaveWorkInfo(st_FMSMSGHEAD&_hdr, BYTE *pReadBuf, DWORD dwLen);
	INT64 fnSaveWorkInfo(BYTE *pReadBuf, DWORD dwLen);
	BOOL fnSendResult(const CHAR* _Longdata);
	INT64 fnSaveDCIRWorkInfo(st_FMSMSGHEAD&_hdr, BYTE *pReadBuf, DWORD dwLen);
};