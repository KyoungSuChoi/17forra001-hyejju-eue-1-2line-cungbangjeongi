#include "afxwin.h"
#if !defined(AFX_TESTNAMEDLG_H__9402A2ED_9FB1_468C_AEAF_49923E9EEC5F__INCLUDED_)
#define AFX_TESTNAMEDLG_H__9402A2ED_9FB1_468C_AEAF_49923E9EEC5F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TestNameDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTestNameDlg dialog
class CTestNameDlg : public CDialog
{
// Construction
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();
	CTestNameDlg(int nProcess = 0, CWnd* pParent = NULL);   // standard constructor
	virtual ~CTestNameDlg();

	long m_lTestTypeID;	
	int m_nProcess;
	int m_bProcessSetting;	
	long m_lModelID;
	COleDateTime m_createdDate;	
	

// Dialog Data
	//{{AFX_DATA(CTestNameDlg)
	enum { IDD = IDD_TEST_NAME_DLG };
	CComboBox	m_ctrlProcTypeCombo;	
	CString	m_strName;
	CString	m_strDecription;
	CString	m_strCreator;
	BOOL	m_bContinueCellCode;
	//}}AFX_DATA
public:
	void InitLabel();


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestNameDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CTestNameDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnChangeTestNameEdit();
	afx_msg void OnChangeTestDescripEdit();
	afx_msg void OnChangeTestCreatorEdit();
	afx_msg void OnSelchangeModelSelCombo();
	afx_msg void OnSelchangeProcTypeCombo();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();	
	CLabel m_LabelProcess;
	afx_msg void OnStnClickedModelStatic();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTNAMEDLG_H__9402A2ED_9FB1_468C_AEAF_49923E9EEC5F__INCLUDED_)
