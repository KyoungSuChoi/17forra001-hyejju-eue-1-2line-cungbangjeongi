//											//	
//											//			
//////////////////////////////////////////////

#ifndef _CALIBRATION_DEFINE_H_
#define _CALIBRATION_DEFINE_H_

#define CAL_MAX_VOLTAGE_RANGE 1
#define CAL_MAX_CURRENT_RANGE 4

#define CAL_MAX_CALIB_SET_POINT		15
#define CAL_MAX_CALIB_CHECK_POINT	15
#define CAL_MAX_BOARD_NUM			EP_MAX_BD_PER_MD
#define CAL_MAX_SHUNT_SERIAL_SIZE	16

#if 1 < CAL_MAX_VOLTAGE_RANGE
#pragma message( "------------KBH Message-------------" )
#pragma message( "Voltage Calibration Range set Number Error" )
#pragma message( "------------------------------------" )
#error Item Define Error
#endif
	
#if 4 < CAL_MAX_CURRENT_RANGE
#pragma message( "------------KBH Message-------------" )
#pragma message( "Current Calibration Range set Number Error" )
#pragma message( "------------------------------------" )
#error Item Define Error
#endif

#if 15 < CAL_MAX_CALIB_SET_POINT
#pragma message( "------------KBH Message-------------" )
#pragma message( "Calibration point set Number Error" )
#pragma message( "------------------------------------" )
#error Item Define Error
#endif

#if 15 < CAL_MAX_CALIB_CHECK_POINT
#pragma message( "------------KBH Message-------------" )
#pragma message( "Calibration check point set Number Error" )
#pragma message( "------------------------------------" )
#error Item Define Error
#endif


#endif	//_CALIBRATION_DEFINE_H_