#include "stdafx.h"

#include "FMS.h"
#include "FM_Unit.h"

#include "FM_STATE.h"

CFM_STATE::CFM_STATE(FM_STATUS_ID _Eqstid, FM_STATUS_ID _stid, CFM_Unit* _unit)
: m_EQSTID(_Eqstid)
, m_STID(_stid)
, m_Unit(_unit)
, m_ProcID(FNID_ENTER)
, m_RetryMap(0)
, m_iWaitUserTimer(-1)
, m_iWaitTimer(-1)
, m_iWaitCount(-1)
, m_iConstWaitTimer(FMSSM_MINITE * 10)	// 10(분)
, m_iConstWaitCount(1)					// 반복횟수
, m_nSeqNum(0)
{
}

CFM_STATE::~CFM_STATE(void)
{
}
VOID CFM_STATE::fnWaitInit()
{
	m_RetryMap = 0;
	m_iWaitCount = -1;
	m_iWaitTimer = -1;
	m_iWaitUserTimer = FMSSM_MINITE * 10;
	
	// 상태별 타이머 기본값을 적용
	switch( m_STID )
	{
		case AUTO_ST_READY:
		case AUTO_ST_TRAY_IN:
			{
				// Blue Ready 확인 신호
				m_iWaitUserTimer = FMSSM_MINITE * AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "DelayBlueReady", 5);
			}
			break;			
		case AUTO_ST_END:
			{
				// 1. Blue End, Red End 확인 신호
				// 2. Blue End : 설정 시간동안 작업 완료 신호를 못받은 상태에서 트레이가 남이 있는 경우 Red End : 트레이가 없는 경우
				m_iWaitUserTimer = FMSSM_MINITE * AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "DelayBlueEnd", 5);
			}
			break;
	}	
}

VOID CFM_STATE::fnPorcessing()
{
	//INT nModuleID = m_Unit->fnGetModuleID();
	/*
	if(m_STID > AUTO_ST_END)
	{
		//m_Unit->m_log.fnWriteLog(LT_NOMAL, "#%d MANUAL State \n", nModuleID);
		return;
	}*/

	if(m_RetryMap & RETRY_IMPORT_ON || m_RetryMap & RETRY_NOMAL_ON)
	{	
		// 1. 타이머 설정
		if (m_iWaitCount == -1)		//최초 진입
		{
			m_iWaitCount = m_iConstWaitCount;		//카운트 셋
			if(m_iWaitUserTimer > 0)
				m_iWaitTimer = m_iWaitUserTimer;	//유저 타이머 셋
			else
				m_iWaitTimer = m_iConstWaitTimer;	//기본 타이머 셋
		}

		// 2. 타이머가 되면 실행되는 부분		
		if(m_iWaitTimer == 0)//타이머가 다되면 실행
		{
			(this->*CFM_STATE_mfg[m_ProcID])();
		}
		
		if(m_iWaitCount == 0 && m_iWaitTimer == 0)	// 메뉴얼 운영으로 전환
		{
			m_iWaitUserTimer = -1;					//타이머값 초기화
			m_iWaitCount = -1;
			m_iWaitTimer = -1;
		}

		if(m_RetryMap & RETRY_CHECK)
		{
			(this->*CFM_STATE_mfg[m_ProcID])();
		}
	}
	else
	{
		//TRACE("#%d Nomal Processing \n", nModuleID);
		(this->*CFM_STATE_mfg[m_ProcID])();
	}

	if(m_iWaitTimer > 0)
	{
		m_iWaitTimer--;
	}
}

VOID CFM_STATE::fnSetFMSStateCode(FMS_STATE_CODE _code)
{
	m_Unit->fnGetFMS()->fnSetFMSStateCode(fnGetEQStateID(), _code);	
}

FMS_STATE_CODE CFM_STATE::fnGetFMSStateCode()
{
	return m_Unit->fnGetFMS()->fnGetFMSStateCode();
}
