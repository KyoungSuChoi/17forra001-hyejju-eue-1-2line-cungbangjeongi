#pragma once

//////////////////////////////////////////////////////////////////////////
// 입고 가능 장비 요구	// 0101
// 입고 예약			// 0102
typedef struct _st_Cell_ID
{
	CHAR Cell_ID[16+1];
}st_Cell_ID;

static INT g_iCHARGER_INRESEVE_F_Map[] = 
{
	3
	, 3
	, 2
	, 20
	, 7
	, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16
	, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16
	, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16
	, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16	
	, 1
	, 3
	, 0
};

typedef struct _st_CHARGER_INRESEVE_F
{
	CHAR MachinID[3+1];
	CHAR Type[3+1];
	CHAR Process_No[2+1];
	CHAR Batch_No[20+1];
	CHAR Tray_ID[7+1];
	st_Cell_ID arCell_ID[MAX_CELL_COUNT];	
	CHAR Tray_Type[1+1];	
	CHAR Total_Step[3+1];
}st_CHARGER_INRESEVE_F;

typedef enum tag_CHARGER_INRESEVE_F
{
	C_MACHINID
	, C_TYPE
	, C_PROCESS_NO
	, C_BATCH_NO
	, C_TRAY_ID
	, C_ARCELL_ID	
	, C_TRAY_TYPE
	, C_TOTAL_STEP
	, C_CIF_END

}CHARGER_INRESEVE_F;
static const TCHAR g_str_CHARGER_INRESEVE_F[C_CIF_END][32] = 
{
	TEXT("MachinID")
	, TEXT("Type")
	, TEXT("Process_No")
	, TEXT("Batch_No")
	, TEXT("Tray_ID")
	, TEXT("arCell_ID")	
	, TEXT("Tray_Type")
	, TEXT("Total_Step")
};
//////////////////////////////////////////////////////////////////////////


//Step 갯수변화
static INT g_iStep_Set_Map[] = 
{
	3
	, 1
	, 6
	, 5
	, 5
	, 6
	, 6
	, 6
	, 5
	, 5
	, 0
};
typedef struct _st_Step_Set
{
	CHAR Step_ID[3+1];
	CHAR Step_Type[1+1];
	CHAR Current[6+1];
	CHAR Volt[5+1];
	CHAR Time[5+1];
	CHAR CutOff[6+1];
	CHAR CutOff_SOC[6+1];
	CHAR CutOff_A[6+1];
	CHAR GetTime_Setting[5+1];
	CHAR Step_LimitChkTime[5+1];
}st_Step_Set;

typedef enum tag_Step_Set
{
	STEP_ID
	, STEP_TYPE
	, CURRENT
	, VOLT
	, TIME
	, CUTOFF
	, CUTOFF_SOC
	, CUTOFF_A
	, GETTIME_T
	, SS_END

}Step_Set;
static const TCHAR g_str_Step_Set[SS_END][32] = 
{
	TEXT("Step_ID")
	, TEXT("Step_Type")
	, TEXT("Current")
	, TEXT("Volt")
	, TEXT("Time")
	, TEXT("CutOff")
	, TEXT("CutOff_SOC")
	, TEXT("CutOff_A")
	, TEXT("GetTime_T")
};
//////////////////////////////////////////////////////////////////////////

static INT g_iProtect_Limit_Set_Map[] = 
{
	5
	, 5
	, 6
	, 6
	, 6
	, 6

	, 5
	, 5
	, 6
	, 6
	, 6
	, 6

	, 5
	, 5
	, 4

	, 5
	, 3
	, 5
	, 6
	, 3
	, 6

	, 5
	, 5
	, 0
};
typedef struct _st_Protect_Limit_Set
{
	CHAR Charge_H_Vol[5+1];
	CHAR Charge_L_Vol[5+1];
	CHAR Charge_H_Cur[6+1];
	CHAR Charge_L_Cur[6+1];
	CHAR Charge_H_Cap[6+1];
	CHAR Charge_L_Cap[6+1];

	CHAR DisCharge_H_Vol[5+1];
	CHAR DisCharge_L_Vol[5+1];
	CHAR DisCharge_H_Cur[6+1];
	CHAR DisCharge_L_Cur[6+1];
	CHAR DisCharge_H_Cap[6+1];
	CHAR DisCharge_L_Cap[6+1];

	CHAR Charge_H_Time_Vol[5+1];	
	CHAR Charge_L_Time_Vol[5+1];	
	CHAR Charge_Vol_Get_Time[4+1];	

	CHAR Charge_Comp_CC_Vol[5+1];		
	CHAR Charge_Comp_CC_Time[3+1];		
	CHAR Charge_Comp_CC_DeltaVol[5+1];
	CHAR Charge_Comp_CV_Cur[6+1];
	CHAR Charge_Comp_CV_Time[3+1];
	CHAR Charge_Comp_Cv_DeltaCur[6+1];

	CHAR OCV_H_Vol[5+1];
	CHAR OCV_L_Vol[5+1];
}st_Protect_Limit_Set;

static INT g_iContact_Set_Map[] = 
{
	6
	, 2
	, 6
	, 6
	, 6
	, 6
	, 6
	, 6
	, 6  //12
	, 0
};
typedef struct _st_Contact_Set
{
	CHAR Charge_Cur[6+1]; 
	CHAR Charge_Time[2+1];
	CHAR Inverse_Vol[6+1];		// 초기 전압 확인		//20210223 KSJ
	CHAR Upper_Vol_Check[6+1];	// 전압 상한치 확인		//20210223 KSJ
	CHAR Lower_Vol_Check[6+1];	// 전압 하한치 확인		//20210223 KSJ 
	CHAR Upper_Cur_Check[6+1];	// 전류 상한치 확인		//20210223 KSJ
	CHAR Lower_Cur_Check[6+1];	// 전류 하한치 확인		//20210223 KSJ
	CHAR DeltaVLimit[6+1];	// 전류 하한치 확인			//20210223 KSJ
	CHAR Reserve[6+1];  //6
}st_Contact_Set;

typedef enum tag_Contact_Set
{
	CHARGE_CUR
	, CHARTE_TIME
	, INVERSE_VOL
	, CONTACT_REG
	, CS_RESERVE
	, CS_END

}Contact_Set;

static const TCHAR g_str_Contact_Set[CS_END][32] = 
{
	TEXT("Charge_Cur")
	, TEXT("Charge_Time")
	, TEXT("Inverse_Vol")
	, TEXT("Contact_Reg")
	, TEXT("CS_Reserve")
};
//////////////////////////////////////////////////////////////////////////

static INT g_iCell_Info_Map[] = 
{
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1
	, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
	, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
	, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1	
	, 0
};

typedef struct _st_CELL_INFO
{
	CHAR Cell_Info[1+1];
}st_CELL_INFO;

static INT g_iCHARGER_INRESEVE_B_Map[] = 
{
	5
	, 5
	, 6
	, 6
	, 6
	, 6

	, 5
	, 5
	, 6
	, 6
	, 6
	, 6

	, 5
	, 5
	, 4

	, 5
	, 3
	, 5
	, 6
	, 3
	, 6

	, 5
	, 5
	, 6
	, 2
	, 5
	, 6
	, 5
	, 5
	, 21
	, 3
	, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
	, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
	, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
	, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1	
	, 3  //20210223 KSJ : TypeSel
	, 0
};
typedef struct _st_CHARGER_INRESEVE_B
{
	st_Protect_Limit_Set Protect;
	st_Contact_Set Contact;
	CHAR Contact_Error_Setting[3+1];
	st_CELL_INFO arCellInfo[MAX_CELL_COUNT];
	CHAR Type_Sel[3+1]; //20210223 KSJ : TypeSel

}st_CHARGER_INRESEVE_B;

typedef struct _st_CHARGER_INRESEVE
{
	st_CHARGER_INRESEVE_F F_Value;
	st_Step_Set *Step_Info;
	st_CHARGER_INRESEVE_B B_Value;

}st_CHARGER_INRESEVE;

// 입고 완료 통지		// 0103
static INT g_iCHARGER_IN_Map[] = 
{
	3
	, 7
	, 0
};

typedef struct _st_CHARGER_IN
{
	CHAR Equipment_Num[3+1];
	CHAR Tray_ID[7+1];
}st_CHARGER_IN;

// 검사 종료 응답 // 0104
static INT g_iCHARGER_WORKEND_RESPONSE_Map[] = 
{
	3
	, 0
};

typedef struct _st_CHARGER_WORKEND_RESPONSE
{
	CHAR Equipment_Num[3+1];

}st_CHARGER_WORKEND_RESPONSE;
// 결과 FILE 요구 // 0105
static INT g_iCHARGER_RESULT_Map[] = 
{
	3
	, 7
	, 0
};

typedef struct _st_CHARGER_RESULT
{
	CHAR Equipment_Num[3+1];
	CHAR Tray_ID[7+1];
}st_CHARGER_RESULT;

// 결과 FILE 수신 확인 통지 // 0106
static INT g_iCHARGER_RESULT_RECV_Map[] = 
{
	3
	, 1
	, 7
	, 0
};

typedef struct _st_CHARGER_RESULT_RECV
{
	CHAR Equipment_Num[3+1];
	CHAR Notify_Mode[1+1];
	CHAR Tray_ID[7+1];
}st_CHARGER_RESULT_RECV;

// 출고 완료 통지 // 0107
static INT g_iCHARGER_OUT_Map[] = 
{
	3
	, 0
};

typedef struct _st_CHARGER_OUT
{
	CHAR Equipment_Num[3+1];
}st_CHARGER_OUT;

// Mode 변경 요구 // 0108
static INT g_iCHARGER_MODE_CHANGE_Map[] = 
{
	3
	, 1
	, 0
};

typedef struct _st_CHARGER_MODE_CHANGE
{
	CHAR Equipment_Num[3+1];
	CHAR Equipment_State[1+1];
}st_CHARGER_MODE_CHANGE;

// 설비 상태 통지 응답 // 0109
static INT g_iCHARGER_STATE_RESPONSE_Map[] = 
{
	3
	, 0
};

typedef struct _st_CHARGER_STATE_RESPONSE
{
	CHAR Equipment_Num[3+1];
}st_CHARGER_STATE_RESPONSE;

//////////////////////////////////////////////////////////////////////////
// 입고 가능 장비 응답 // 1101
typedef struct _st_IMPOSSBLE_STATE
{
	CHAR imState[2+1];
}st_IMPOSSBLE_STATE;

static INT g_iCHARGER_IMPOSSBLE_RESPONSE_Map[] = 
{
	2, 2, 2, 2, 2, 2, 2, 2, 2, 2
	, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
	, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
	, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
	, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
	, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
	, 2, 2, 2, 2
	,0
};

typedef struct _st_CHARGER_IMPOSSBLE_RESPONSE
{
	st_IMPOSSBLE_STATE EQ_State[MAX_SBC_COUNT];
}st_CHARGER_IMPOSSBLE_RESPONSE;
// 입고 예약 응답 // 1102
static INT g_iCHARGER_INRESEVE_RESPONSE_Map[] = 
{
	3
	, 0
};

typedef struct _st_CHARGER_INRESEVE_RESPONSE
{
	CHAR Equipment_Num[3+1];
}st_CHARGER_INRESEVE_RESPONSE;

// 입고 완료 응답 // 1103
static INT g_iCHARGER_IN_RESPONSE_Map[] = 
{
	3
	, 0
};

typedef struct _st_CHARGER_IN_RESPONSE
{
	CHAR Equipment_Num[3+1];
}st_CHARGER_IN_RESPONSE;

// 검사 종료 통지 // 1104
static INT g_iCHARGER_WORKEND_Map[] = 
{
	3
	, 7
	, 0
};

typedef struct _st_CHARGER_WORKEND
{
	CHAR Equipment_Num[3+1];
	CHAR Tray_ID[7+1];
}st_CHARGER_WORKEND;

//결과파일 전송 //1105


// 결과 FILE 수신 확인 응답 // 1106
static INT g_iCHARGER_RESULT_RECV_RESPONSE_Map[] = 
{
	3
	, 7
	, 0
};

typedef struct _st_CHARGER_RESULT_RECV_RESPONSE
{
	CHAR Equipment_Num[3+1];
	CHAR Tray_ID[7+1];
}st_CHARGER_RESULT_RECV_RESPONSE;
// 출고 완료 응답 // 1107
static INT g_iCHARGER_OUT_RESPONSE_Map[] = 
{
	3
	, 0
};

typedef struct _st_CHARGER_OUT_RESPONSE
{
	CHAR Equipment_Num[3+1];
}st_CHARGER_OUT_RESPONSE;

// Mode 변경 응답 // 1108
static INT g_iCHARGER_MODE_CHANGE_RESPONSE_Map[] = 
{
	3
	, 0
};

typedef struct _st_CHARGER_MODE_CHANGE_RESPONSE
{
	CHAR Equipment_Num[3+1];
}st_CHARGER_MODE_CHANGE_RESPONSE;

// 설비 상태 통지 // 1109
static INT g_iCHARGER_STATE_Map[] = 
{
	3
	, 2
	, 0
};

typedef struct _st_CHARGER_STATE
{
	CHAR Equipment_Num[3+1];
	CHAR Equipment_State[2+1];
}st_CHARGER_STATE;

//////////////////////////////////////////////////////////////////////////
// 결과 FILE 응답 - Stage No ~ Total Step 까지 // 1105
static INT g_iResult_File_Response_L_Map[] = 
{
	3	
	, 3
	, 2
	, 20
	, 7
	, 3
	, 0
};

typedef struct _st_RESULT_FILE_RESPONSE_L
{
	CHAR Stage_No[3+1];
	CHAR Type[3+1];
	CHAR Process_ID[2+1];
	CHAR Batch_No[20+1];
	CHAR Tray_ID[7+1];
	CHAR Total_Step[3+1];
}st_RESULT_FILE_RESPONSE_L;

typedef enum tag_RESULT_FILE_RESPONSE_L
{
	R_STAGE_NO	
	, R_TYPE
	, R_PROCESS_ID
	, R_BATCH_NO
	, R_TRAY_ID	
	, R_TOTAL_STEP
	, R_L_END

}RESULT_FILE_RESPONSE_L;

static const TCHAR g_str_RESULT_FILE_RESPONSE_L[R_L_END][32] = 
{
	TEXT("Stage_No")	
	, TEXT("Type")
	, TEXT("Process_Id")
	, TEXT("Batch_No")
	, TEXT("Tray_Id")
	, TEXT("Total_Step")
};


// Step 온도값
typedef struct _st_Age_temp
{
	CHAR Age_temp[5+1];  // ###.# C
}st_Age_temp;

// Step 전압값
static INT g_iVoltage_Worth_Map[] = 
{
	5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5	
	, 0
};

typedef struct _st_VOLTAGE_WORTH_RESPONSE
{
	CHAR Voltage_Worth[5+1];
}st_VOLTAGE_WORTH_RESPONSE;


// Step 전류값(5분후)
static INT g_iCurrent_Worth_Five_Min_Map[] = 
{
	6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6	
	, 0
};
typedef struct _st_CURRENT_WORTH_FIVE_MIN_RESPONSE
{
	CHAR Current_Worth_Five_Min[6+1];
}st_CURRENT_WORTH_FIVE_MIN_RESPONSE;

// Step 용량값
static INT g_iCapacity_Worth_Map[] = 
{
	6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6	
	, 0
};

typedef struct _st_CAPACITY_WORTH_RESPONSE
{
	CHAR Capacity_Worth[6+1];
}st_CAPACITY_WORTH_RESPONSE;

// Step 전류값(시간)
static INT g_iCurrent_Worth_Hour_Map[] = 
{
	6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6	
	, 0
};
typedef struct _st_CURRENT_WORTH_HOUR_RESPONSE
{
	CHAR Current_Worth_Hour[6+1];
}st_CURRENT_WORTH_HOUR_RESPONSE;


// Step 종지 전류값(discharge)
static INT g_iDischarge_Current_Worth_Map[] = 
{
	6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6	
	, 0
};
typedef struct _st_DISCHARGE_CURRENT_WORTH_RESPONSE
{
	CHAR Discharge_Current_Worth[6+1];
}st_DISCHARGE_CURRENT_WORTH_RESPONSE;


// Step 종료 시간
static INT g_iStep_End_Time_Map[] = 
{
	6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6	
	, 0
};
typedef struct _st_STEP_END_TIME_RESPONSE
{
	CHAR Step_End_Time[6+1];
}st_STEP_END_TIME_RESPONSE;


// Step 전압값(시간)
static INT g_iStep_Voltage_Time_Map[] = 
{
	5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5	
	, 0
};
typedef struct _st_STEP_VOLTAGE_TIME_RESPONSE
{
	CHAR Step_Voltage_Time[5+1];
}st_STEP_VOLTAGE_TIME_RESPONSE;


// Step 정전류값(시간)
static INT g_iStep_Current_Time_Map[] = 
{
	6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6	
	, 0
};
typedef struct _st_STEP_CURRENT_TIME_RESPONSE
{
	CHAR Step_Current_Time[6+1];
}st_STEP_CURRENT_TIME_RESPONSE;

// Step 정전류 용량 (CC 용량)
static INT g_iStep_Current_Capacity_CC_Map[] = 
{
	6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6	
	, 0
};

typedef struct _st_STEP_CURRENT_CAPACITY_CC
{
	CHAR Step_Current_Capacity_CC[6+1];
}st_STEP_CURRENT_CAPACITY_CC;
//////////////////////////////////////////////////////////////////////////

// Step CC 시간
static INT g_iStep_CV_Time_Map[] = 
{
	6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 0
};
typedef struct _st_STEP_CV_TIME
{
	CHAR Step_CV_Time[6+1];
}st_STEP_CV_TIME;
//////////////////////////////////////////////////////////////////////////

// Step CV 용량
static INT g_iStep_CV_Capacity_Map[] = 
{
	6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 0
};

typedef struct _st_STEP_CV_CAPACITY
{
	CHAR Step_CV_Capacity[6+1];
}st_STEP_CV_CAPACITY;
// DV (Contact Check)
static INT g_iStep_DeltaVoltage_Map[] = 
{
	6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 0
};
typedef struct _st_STEP_DeltaVoltage
{
	CHAR Step_DeltaVoltage[6+1];
}st_STEP_DeltaVoltage;
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
static INT g_iResult_R_Step_No_Map[] = 
{
	3
	, 0
};

static INT g_iResult_R_Action_Map[] = 
{
	1
	, 0
};


static INT g_iResult_R_Temp_Map[] =
{
	4
	, 0
};

// Step 온도보정 용량
static INT g_iStep_Capacity_T_Map[] = 
{
	6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 0
};

typedef struct _st_CAPACITY_T
{
	CHAR Step_Capacity[6+1];
}st_CAPACITY_T;

static INT g_iStep_CC_Capacity_T_Map[] = 
{
	6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6	
	, 0
};

typedef struct _st_STEP_CC_CAPACITY_T
{
	CHAR Step_CC_Capacity[6+1];
}st_STEP_CAPACITY_CC_T;

static INT g_iStep_CV_Capacity_T_Map[] = 
{
	6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6	
	, 0
};

typedef struct _st_STEP_CV_CAPACITY_T
{
	CHAR Step_CV_Capacity[6+1];
}st_STEP_CAPACITY_CV_T;

typedef struct _st_Result_Step_Set
{
	CHAR R_Step_No[3+1];
	CHAR R_Action[1+1];

	CHAR R_JigTemp1[4+1];
	CHAR R_JigTemp2[4+1];
	CHAR R_JigTemp3[4+1];
	CHAR R_JigTemp4[4+1];
	CHAR R_JigTemp5[4+1];
	CHAR R_JigTemp6[4+1];
	CHAR R_JigTempMax[4+1];
	CHAR R_JigTempMin[4+1];
	CHAR R_JigTempAvg[4+1];
	
	st_VOLTAGE_WORTH_RESPONSE  R_Voltage_Worth[MAX_CELL_COUNT];
	st_CAPACITY_WORTH_RESPONSE  R_Capacity_Worth[MAX_CELL_COUNT];
	
	st_DISCHARGE_CURRENT_WORTH_RESPONSE  R_Discharge_Current_Worth[MAX_CELL_COUNT];
	st_STEP_END_TIME_RESPONSE  R_Step_End_Time[MAX_CELL_COUNT];
	st_STEP_VOLTAGE_TIME_RESPONSE  R_Step_Voltage_Time[MAX_CELL_COUNT];
	st_STEP_CURRENT_TIME_RESPONSE  R_Step_Current_Time[MAX_CELL_COUNT];
	/////////////////////////////////////////////////////////////////////////
	st_STEP_CURRENT_CAPACITY_CC  R_Step_Current_Capacity_CC[MAX_CELL_COUNT];
	st_STEP_CV_TIME  R_Step_CV_Time[MAX_CELL_COUNT];
	st_STEP_CV_CAPACITY  R_Step_CV_Capacity[MAX_CELL_COUNT];
	//////////////////////////////////////////////////////////////////////////
	// st_CAPACITY_T R_Step_Capacity_T[MAX_CELL_COUNT];
	// st_STEP_CAPACITY_CC_T R_Step_CC_Capacity_T[MAX_CELL_COUNT];
	// st_STEP_CAPACITY_CV_T R_Step_CV_Capacity_T[MAX_CELL_COUNT];
	st_STEP_DeltaVoltage R_Step_DeltaVoltage[MAX_CELL_COUNT];						//20210223 KSJ
}st_Result_Step_Set;

typedef enum tag_Result_Step_Set
{
	R_STEP_NO
	,R_ACTION
	, R_VOLTAGE_WORTH
	, R_CURRENT_WORTH_FIVE_MIN
	, R_CAPACITY_WORTH
	, R_CURRENT_WORTH_HOUR
	, R_DISCHARGE_CURRENT_WORTH
	, R_STEP_END_TIME
	, R_STEP_VOLTAGE_TIME
	, R_STEP_CURRENT_TIME
	, RSS_END

}Result_Step_Set;
static const TCHAR g_str_Result_Step_Set[RSS_END][32] = 
{
	TEXT("Step_No")
	, TEXT("Action")
	, TEXT("전압값")
	, TEXT("전류값(5분후)")
	, TEXT("용량값")
	, TEXT("전류값(시간)")
	, TEXT("종지 전류값")
	, TEXT("Step 종료 시간")
	, TEXT("전압값(시간)")
	, TEXT("정전류 시간")
};

// 결과 File 응답 Cell_Info
static INT g_iResult_File_Response_Cell_Info_Map[] = 
{
	2, 2, 2, 2, 2, 2, 2, 2, 2, 2
	, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
	, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
	, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2	
	,0
};

typedef struct _st_RESULT_FILE_RESPONSE_CELL_INFO
{
	CHAR Cell_Info[2+1];
}st_RESULT_FILE_RESPONSE_CELL_INFO;

// 결과 FILE 응답 - 현재 진행 Step 번호 ~ 검사 종료 일시 까지 // 1105
static INT g_iResult_File_Response_R_Map[] = 
{
	3
	, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
	, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
	, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
	, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2	
	, 14
	, 14
	, 0
};

typedef struct _st_RESULT_FILE_RESPONSE_R
{
	CHAR Now_Step_No[3+1];
	st_RESULT_FILE_RESPONSE_CELL_INFO arCellInfo[MAX_CELL_COUNT];
	CHAR Test_Begin_Date[14+1];
	CHAR Test_Finish_Date[14+1];
}st_RESULT_FILE_RESPONSE_R;

//////////////////////////////////////////////////////////////////////////