// DllTimer.cpp : implementation file
//

#include "stdafx.h"
#include "DataSvr.h"
#include "DllTimer.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDllTimer dialog


CDllTimer::CDllTimer(CWnd* pParent /*=NULL*/)
	: CDialog(CDllTimer::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDllTimer)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CDllTimer::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDllTimer)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDllTimer, CDialog)
	//{{AFX_MSG_MAP(CDllTimer)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDllTimer message handlers

void CDllTimer::OnTimer(UINT nIDEvent) 
{
	((CDataSvrApp*)AfxGetApp())->OnTimer(nIDEvent);	
}
