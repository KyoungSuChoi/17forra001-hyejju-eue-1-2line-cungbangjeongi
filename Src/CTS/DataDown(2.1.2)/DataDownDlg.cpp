// DataDownDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DataDown.h"
#include "DataDownDlg.h"
#include "IPSetDlg.h"

#include "NameInputDlg.h"
#include <math.h>
#include "ShowDetailResultDlg.h"
#include "ProfileListInfo.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include "ModuleAddDlg.h"
//#define REG_SECTION_NAME	"Ftp Set"

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();
// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	//}}AFX_VIRTUAL
// Implementation

protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP

END_MESSAGE_MAP()
/////////////////////////////////////////////////////////////////////////////

// CDataDownDlg dialog

CDataDownDlg::CDataDownDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDataDownDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDataDownDlg)
	m_bModuleFind = FALSE;
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32

	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_nModuleID = 0;
	m_strPathName = "formation_data/monitoringData";
	m_strPassword = "dusrnth";
	m_strID = "root";
	
	m_bFindLocal = TRUE;
	m_bHideMode = FALSE;

	m_bSearchModeOffLine = TRUE;
	m_nOnLineSelModuleID = 0;
	m_bWork = FALSE;
	LanguageinitMonConfig();
}

CDataDownDlg::~CDataDownDlg()
{
	m_InternetSession.Close();
	m_TrayIcon.RemoveIcon();	// Tray Icon 삭제
	delete[] TEXT_LANG;
	TEXT_LANG = NULL;
	if( TEXT_LANG != NULL )					
	{					
		delete[] TEXT_LANG;					
		TEXT_LANG = NULL;					
	}
}


void CDataDownDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDataDownDlg)
	DDX_Control(pDX, IDC_LIST_PROFILE, m_ctrlProfileList);
	DDX_Control(pDX, IDC_STATIC_UNIT, m_ctrlSelModule);
	DDX_Control(pDX, IDC_REMOTE_LIST, m_ctrlRemoteList);
	DDX_Control(pDX, IDC_COMBO_UNIT, m_ctrlUnitCombo);
	DDX_Control(pDX, IDCANCEL, m_btnOK);
	DDX_Control(pDX, IDC_BUTTON2, m_btnShowFile);	
	DDX_Control(pDX, IDC_GET_LIST, m_btnGetFile);
	DDX_Control(pDX, IDC_DESEL_ALL_BUTTON, m_btnDeSelAll);
	DDX_Control(pDX, IDC_SELECT_ALL_BUTTON, m_btnSelAll);
	DDX_Control(pDX, IDC_OPTION_BUTTON, m_btnSaveOption);
	DDX_Control(pDX, IDC_BUTTON1, m_btnSelSave);
	DDX_Control(pDX, IDC_DOWN_PROGRESS, m_downProgress);
	DDX_Control(pDX, IDC_LOG_LIST, m_LogList);
	DDX_Control(pDX, IDC_DATA_LIST, m_listBox);
	DDX_Check(pDX, IDC_CHECK_FIND_REMOTE, m_bModuleFind);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CDataDownDlg, CDialog)

	//{{AFX_MSG_MAP(CDataDownDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_GET_LIST, OnGetList)
	ON_LBN_DBLCLK(IDC_DATA_LIST, OnDblclkDataList)
	ON_BN_CLICKED(IDC_BUTTON1, OnSaveDataButton)
	ON_WM_CONTEXTMENU()
	ON_COMMAND(ID_DOWNLOAD_DATA, OnDownloadData)
	ON_UPDATE_COMMAND_UI(ID_DELETE_DATA, OnUpdateDeleteData)
	ON_UPDATE_COMMAND_UI(ID_DOWNLOAD_DATA, OnUpdateDownloadData)
	ON_COMMAND(ID_DELETE_DATA, OnDeleteData)
	ON_BN_CLICKED(IDC_DEL_ALL_BUTTON, OnDelAllButton)
	ON_BN_CLICKED(IDC_SELECT_ALL_BUTTON, OnSelectAllButton)
	ON_WM_COPYDATA()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_DESEL_ALL_BUTTON, OnDeselAllButton)
	ON_COMMAND(ID_SHOW_DLG, OnShowDlg)
	ON_COMMAND(ID_EXIT, OnExit)
	ON_BN_CLICKED(IDC_OPTION_BUTTON, OnOptionButton)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON2, OnButton2)
	ON_LBN_SELCHANGE(IDC_DATA_LIST, OnSelchangeDataList)
	ON_BN_CLICKED(IDC_RADIO1, OnRadio1)
	ON_BN_CLICKED(IDC_RADIO2, OnRadio2)
	ON_BN_CLICKED(IDC_ON_BUTTON, OnOnButton)
	ON_LBN_SELCHANGE(IDC_REMOTE_LIST, OnSelchangeRemoteList)
	ON_CBN_SELCHANGE(IDC_COMBO_UNIT, OnSelchangeComboUnit)
	ON_BN_CLICKED(IDC_RADIO3, OnRadio3)
	ON_BN_CLICKED(IDC_RADIO4, OnRadio4)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_ICON_NOTIFY, OnTrayNotification)

END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////

// CDataDownDlg message handlers

BOOL CDataDownDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	// Add "About..." menu item to system menu.
	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

//	m_btnSaveOption.SubclassDlgItem(IDC_OPTION_BUTTON, this);

	LanguageinitMonConfig();
	//변경하면 안됨
	//다른 프로그램이 찾는 이름으로 사용 
	SetWindowText(DATA_DOWN_TITLE_NAME);

	m_strDBPath.Format(".\\DataBase\\%s", FORM_SET_DATABASE_NAME);
	
	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	if (!m_TrayIcon.Create(this, WM_ICON_NOTIFY, _T("PNE DataDown Server"),  NULL, IDR_TRAY_MENU))
          return -1;

     // 아이콘 설정
     // 아이콘 아이디에 자신의 아이콘을 넣으세요...
     m_TrayIcon.SetIcon(IDR_MAINFRAME);

	 if(m_bHideMode)
	 {
		 SetTimer(TIMER_ID_SHOWMODE, TIMER_INTERVAL_SHOWMODE, NULL);
	 }

	 m_ctrlSelModule.SetTextColor(RGB(255, 0, 0));
	 m_ctrlSelModule.SetFontBold(TRUE);
	 m_ctrlSelModule.SetFontAlign(DT_LEFT);

	 ((CButton*)GetDlgItem(IDC_RADIO3))->SetCheck(TRUE);
	 
	// TODO: Add extra initialization here

	 char szBuff[512];
	 if(GetCurrentDirectory(511, szBuff) < 1)	//Get Current Direcotry Name
	 {
		 sprintf(szBuff, "File Path Name Read Error %d\n", GetLastError()); //100056 //7019
//		 sprintf(szBuff, "File Path Name Read Error %d\n", GetLastError());
		 AfxMessageBox(szBuff);
		 return FALSE;	
	}
	 m_strCurFolder = szBuff;

	//default value load
/*	m_strPathName = AfxGetApp()->GetProfileString(REG_SECTION_NAME, "Monitoring Dir", "");
	if(m_strPathName.IsEmpty())
	{
		m_strPathName = "formation_data/monitoringData";
		AfxGetApp()->WriteProfileString(REG_SECTION_NAME, "Monitoring Dir", m_strPathName);
	}
*/
//	m_strPassword = AfxGetApp()->GetProfileString(REG_SECTION_NAME, "Login Password", (LPCSTR)m_strPassword);
//	m_strID = AfxGetApp()->GetProfileString(REG_SECTION_NAME, "Login ID", (LPCSTR)m_strID);
//	m_strDBPath = AfxGetApp()->GetProfileString(REG_SECTION_NAME, "DB Path", (LPCSTR)m_strDBPath);

	//CTSMon Program에서 Database 와 FTP 설정 정보를 가져온다.
	 //=> DataDown은 CTSMon Program 이 반드시 설치 되어 있는곳에 설치해야 함
	long rtn;
	HKEY hKey = 0;
	BYTE buf[512];
	DWORD size = 511;
	DWORD type;
	CString strTemp, strDest;
	
	rtn = ::RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\CTSMon\\Path", 0, KEY_READ, &hKey);
	//Note Found 
	if(ERROR_SUCCESS == rtn)
	{
		//Load PowerFormation DataBase  Path
		rtn = ::RegQueryValueEx(hKey, "CTSMon", NULL, &type, buf, &size);
		::RegCloseKey(hKey);
		
		//
		if(ERROR_SUCCESS == rtn && (type == REG_SZ || type == REG_EXPAND_SZ))
		{
			m_strMonDir = buf;
		}
		else
		{
			AfxMessageBox(TEXT_LANG[54]); //100057//7019 
		//	AfxMessageBox("CTSMon 설치 정보를 찾을 수 없습니다.");
			GetDlgItem(IDC_GET_LIST)->EnableWindow(FALSE);
		}
	}
	else
	{
		//AfxMessageBox("CTSMon 설치 경로 정보를 찾을 수 없습니다.");
		AfxMessageBox(TEXT_LANG[55]);
		GetDlgItem(IDC_GET_LIST)->EnableWindow(FALSE);
	}

	rtn = ::RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\CTSMon\\Path", 0, KEY_READ, &hKey);
	//Note Found 
	if(ERROR_SUCCESS == rtn)
	{
		//Load PowerFormation DataBase  Path
		size = 511;
		rtn = ::RegQueryValueEx(hKey, "DataBase", NULL, &type, buf, &size);
		::RegCloseKey(hKey);
		
		//
		if(ERROR_SUCCESS == rtn && (type == REG_SZ || type == REG_EXPAND_SZ))
		{
			m_strDBPath.Format("%s\\%s", buf, FORM_SET_DATABASE_NAME);
		}
		else
		{
		//	AfxMessageBox("CTSMon 설치 정보를 찾을 수 없습니다."); 
			AfxMessageBox(TEXT_LANG[54]); 
			GetDlgItem(IDC_GET_LIST)->EnableWindow(FALSE);
		}
	}
	else
	{
		AfxMessageBox(TEXT_LANG[56]);
	//	AfxMessageBox("CTSMon data base 경로 정보를 찾을 수 없습니다."); //100059//7019
		GetDlgItem(IDC_GET_LIST)->EnableWindow(FALSE);
	}

	rtn = ::RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\CTSMon\\Ftp Set", 0, KEY_READ, &hKey);
	if(ERROR_SUCCESS == rtn)
	{
		//Monitoring Data Path
		size = 511;
		rtn = ::RegQueryValueEx(hKey, "Monitoring Location", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && (type == REG_SZ || type == REG_EXPAND_SZ))
			m_strPathName = buf;
		
		//FTP Login ID 
		size = 511;
		rtn = ::RegQueryValueEx(hKey, "Login ID", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && (type == REG_SZ || type == REG_EXPAND_SZ))
			m_strID = buf;
		
		//FTP Login Password 
		size = 511;
		rtn = ::RegQueryValueEx(hKey, "Login Password", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && (type == REG_SZ || type == REG_EXPAND_SZ))
			m_strPassword = buf;
		
		::RegCloseKey(hKey);
	}
	else
	{
		AfxMessageBox(TEXT_LANG[57]);
	//	AfxMessageBox("Remote 접속 설정 정보를 찾을 수 없습니다."); //100060
		GetDlgItem(IDC_GET_LIST)->EnableWindow(FALSE);
	}
	//////////////////////////////////////////////////////////////////////////

/*	m_strDataTempPath = AfxGetApp()->GetProfileString(REG_SEC_NAME, "TempDir", "");
	if(m_strDataTempPath.IsEmpty())
	{
		m_strDataTempPath =  strDest + "\\DataTemp";
		m_strDataTempPath = MakeMyFolder(m_strDataTempPath);
		if(m_strDataTempPath.IsEmpty())
		{
			AfxMessageBox("Data 저장용 임시 폴더를 생성할 수 없습니다.");	
			GetDlgItem(IDC_GET_LIST)->EnableWindow(FALSE);
		}
	}
*/
	m_downProgress.SetRange(0, 100);

//	AfxMessageBox(m_strIPAddress);

	//IP 번호가 입력되어 있지 않을 경우 Module 번호에서 IP를 구한다.
/*	if(m_strIPAddress.IsEmpty())	
	{
		if(m_nModuleID <= 0)		//IP와 모듈 번호 모두 비정상 입력 되었을 경우 모듈 번호를 입력 받는다.
		{
			CModuleAddDlg *pDlg = new CModuleAddDlg(this);
			if(pDlg->DoModal() == IDOK)
			{
				m_nModuleID = pDlg->GetModuleID();
			}
			delete pDlg;
			pDlg = NULL;
		}
		
		m_strIPAddress = GetIPAddress(m_nModuleID);
//		AfxMessageBox(m_strIPAddress);
	}
	
	strTemp.LoadString(IDS_TEXT_DOWN_LOAD_DATA);
	strDest.Format("\nModule %d (%s)", m_nModuleID, m_strIPAddress);
	GetDlgItem(IDC_TITLE_STATIC)->SetWindowText(strTemp+strDest);

	if(m_strIPAddress.IsEmpty())
	{
		AfxMessageBox(IDS_TEXT_INVALID_IP);
	}


	CString strDestPath;
	strDestPath = "C:\\Documents and Settings\\kitsat\\My Documents\\Projects\\세방전지\\Manual\\bb";
	strDestPath = MakeMyFolder(strDestPath, FALSE);
*/
	BOOL bFileSeperate = AfxGetApp()->GetProfileInt(REG_SEC_NAME, "FileSeperate", TRUE);
	if(bFileSeperate)	OnRadio1() ;
	else				OnRadio2() ;

	((CButton *)GetDlgItem(IDC_RADIO1))->SetCheck(bFileSeperate);
	((CButton *)GetDlgItem(IDC_RADIO2))->SetCheck(!bFileSeperate);

	SetTimer(TIMER_ID_AUTOSAVE, TIMER_INTERVAL_AUTOSAVE, NULL);

	DWORD lSystemType = 2;
	rtn = ::RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\CTSMon\\FormSetting", 0, KEY_READ, &hKey);
	if(ERROR_SUCCESS == rtn)
	{
		//Load PowerFormation DataBase  Path
		size = 511;
		rtn = ::RegQueryValueEx(hKey, "System Type", NULL, &type, buf, &size);
		::RegCloseKey(hKey);
		
		if( type == REG_DWORD )
		{
			lSystemType = buf[0];
		}
	}

	CString unitName;
	CDaoDatabase  db;
	db.Open(m_strDBPath);

	CString strSQL;
	strSQL.Format("SELECT data3, ModuleID FROM SystemConfig where ModuleType = %d ORDER BY ModuleID", lSystemType);
	try
	{
		CDaoRecordset rs(&db);
		
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
		int a = 0;
		while(!rs.IsEOF())
		{
			COleVariant data = rs.GetFieldValue(0);
			unitName = data.pbVal;						
			m_ctrlUnitCombo.AddString(unitName);				// Module name
			data = rs.GetFieldValue(1);
			m_ctrlUnitCombo.SetItemData(a++, data.lVal);		// Module ID
			rs.MoveNext();
		}
		rs.Close();
		db.Close();
	} 
	catch (CDaoException *e)
	{
		e->Delete();
	}
	m_ctrlUnitCombo.SetCurSel(0);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CDataDownDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.
void CDataDownDlg::OnPaint() 
{
	if (IsIconic())
	{

		CPaintDC dc(this); // device context for painting
		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle

		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);

		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}


// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CDataDownDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CDataDownDlg::SetModuleIP(CString strIP)
{
	m_strIPAddress = strIP;
}

void CDataDownDlg::SetModuleID(int nModuleID)
{
	m_nModuleID = nModuleID;
}

BOOL CDataDownDlg::WriteLog(char *szLog, int nMode )
{
	if( strlen(szLog) == 0 )
		return FALSE;
	
	char tempName[512];

	sprintf(tempName, "%s\\DataDownLog\\", m_strCurFolder );	
	if( !FileExists(tempName) )
	{
		ForceDirectory(tempName);
	}

	COleDateTime oleCurdate(COleDateTime::GetCurrentTime());
	sprintf(tempName, "%s\\DataDownLog\\%s", m_strCurFolder, oleCurdate.Format("%Y%m%d.log"));
	
	if( strcmp( m_szLogFileName, tempName ) != 0)
	{
		strcpy( m_szLogFileName, tempName );
		SetLogFileName( m_szLogFileName );
	}
	
	if( strlen( m_szLogFileName )==0 || szLog==NULL )
		return FALSE;
	
	if( WriteLogFile( m_szLogFileName, szLog, FILE_APPEND ) )
	{	return TRUE;	}
	else
	{	return FALSE;	}
}

BOOL CDataDownDlg::WriteLog(CString szLog, int nMode )
{
	if( WriteLog((LPSTR)(LPCTSTR)szLog, nMode) )
	{	return TRUE;	}
	else{	return FALSE;	}
}

BOOL CDataDownDlg::WriteLogFile(char *szFileName, char *szData, int nMode)
{
	if(szData == NULL || szFileName == NULL)	
		return FALSE;
	
	if(nMode == FILE_CREATE)	{
		FILE *fp;
		fp = fopen(szFileName, "wb");
		if(fp)
		{
			COleDateTime nowtime(COleDateTime::GetCurrentTime());
			fprintf(fp, "%s :: %s\r\n", nowtime.Format(), szData);
			fclose(fp);
			return TRUE;
		}
	}
	else if(nMode == FILE_APPEND)	{
		FILE *fp;
		fp = fopen(szFileName, "ab+");				
		if(fp)
		{
			COleDateTime nowtime(COleDateTime::GetCurrentTime());
			fprintf(fp, "%s :: %s\r\n", nowtime.Format(), szData);
			fclose(fp);
			return TRUE;
		}
	}	
	return FALSE;
}

BOOL CDataDownDlg::ForceDirectory(LPCTSTR lpDirectory)
{
	CString szDirectory = lpDirectory;
	szDirectory.TrimRight("\\");
	
	if ((GetFilePath(szDirectory) == szDirectory) ||
		(FileExists(szDirectory) == -1))
		return TRUE;
	if (!ForceDirectory(GetFilePath(szDirectory)))
		return FALSE;
	if (!CreateDirectory(szDirectory, NULL))
		return FALSE;
	return TRUE;
}

int CDataDownDlg::FileExists(LPCTSTR lpszName)
{
	CFileFind fileFind;
	if (!fileFind.FindFile(lpszName))
	{
		if (DirectoryExists(lpszName))  // if root ex. "C:\"
			return -1;
		return 0;
	}
	fileFind.FindNextFile();
	return fileFind.IsDirectory() ? -1 : 1;
}

BOOL CDataDownDlg::DirectoryExists(LPCTSTR lpszDir)
{
	char curPath[_MAX_PATH];   /* Get the current working directory: */
	if (!_getcwd(curPath, _MAX_PATH))
		return FALSE;
	if (_chdir(lpszDir))	// retruns 0 if error
		return FALSE;
	_chdir(curPath);
	return TRUE;
}

CString CDataDownDlg::GetFilePath(LPCTSTR lpszFilePath)
{
	TCHAR szDir[_MAX_DIR];
	TCHAR szDrive[_MAX_DRIVE];
	_tsplitpath(lpszFilePath, szDrive, szDir, NULL, NULL);
	
	return  CString(szDrive) + CString(szDir);
}

VOID CDataDownDlg::SetLogFileName(char *szFileName)
{
	if(szFileName == NULL)	return;
	strcpy(szLogFileName, szFileName);
}

//해당 모듈 번호를 이용하여 대상 IP를 구한다.
CString CDataDownDlg::GetIPAddress(int nModuleID,int &nGroupNo)
{
	CString address;
	address.Empty();

	CDaoDatabase  db;
	CString strSQL;
	CString strTemp;

	try
	{
		db.Open(m_strDBPath);

		CDaoRecordset rs(&db);

		strSQL.Format("SELECT IP_Address, Data5 FROM SystemConfig WHERE ModuleID = %d", nModuleID);
		
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
		if(!rs.IsBOF())
		{
			COleVariant data = rs.GetFieldValue(0);
			data = rs.GetFieldValue(0);
			address = data.pbVal;
			data = rs.GetFieldValue(1);
			if(data.vt == VT_EMPTY)
			{
				nGroupNo = 1;
			}
			else
			{
				nGroupNo = data.lVal;
			}
		}
		rs.Close();
		db.Close();
	}
	catch (CDaoException *e)
	{
		//strTemp.Format("IP found error => [%s]", strSQL );
		WriteLog(TEXT_LANG[0]);
		e->Delete();
	}

	/*
	//IP를 찾지 못하였을 경우 수동 입력 받는다. 
	if(address.IsEmpty())
	{
		CIPSetDlg dlg;
		if(dlg.DoModal() == IDOK)
		{
			address = dlg.GetIPAddress();
			nGroupNo = 1;
		}	
	}
	*/

	return address;
}
/*
//현재 저장되어 있는 작업 List(Folder 단위 저장)를 검색 한다.
BOOL CDataDownDlg::GetFolderList(CString strTestSerial)
{
	// TODO: Add your control notification handler code here

	m_listBox.ResetContent();
	
	CString strLocation, strTemp;
	int nCh = 0;
	BYTE code;
	//1. 주어진 test serial이 local PC에 존재하는지 검사 
	strLocation.Format("%s\\MD%03d", m_strDataTempPath, m_nModuleID);
	strLocation = FindRealTimeDataFromLocal(strLocation, strTestSerial);
	
	//2. 존재하지 않을 경우 remote에서 검색 
	if(strLocation.IsEmpty())
	{
		WriteLog( strTestSerial+"를 Local에서 찾을 수 없습니다.");

		m_bFindLocal = FALSE;		//주어진 시험이 local PC에 존재하지 않음 

		//2.1 현재 선택한 모듈에서 주어진 serial no의 결과를 검색한다.
		CStringList chList;			//remote channel list
		//get remote file location
		m_strFileLocation = FindRealTimeDataFromRemote(m_strIPAddress, strTestSerial, chList);
		if(m_strFileLocation.IsEmpty() || chList.GetCount() < 1)
		{
			WriteLog( strTestSerial+"를 Remote에서 찾을수 없습니다.");
			return FALSE;
		}
		
		WriteLog( strTestSerial+"를 Remote에서 찾았습니다.");
		//2.2 검색한 remote channel을  list 한다.
		POSITION pos = chList.GetHeadPosition();
		while(pos)
		{
			strTemp = chList.GetNext(pos);		//format => ch001
			nCh = atol(strTemp.Right(3));

			code = m_resultData.GetLastCellCode(nCh-1);
			//Check Error
			if(code != EP_CODE_NONCELL && code != EP_CODE_CELL_NONE && code != EP_CODE_CELL_EXIST)
			{
				m_listBox.AddString(strTemp);
				m_listBox.SetItemData(m_listBox.GetCount()-1, nCh);			//Channel No
			}
		}
	}
	// 3. local에 존재하면 채널 파일들을 list한다.
	else
	{
		WriteLog( strTestSerial+"를 Local에서 찾았습니다.");

		m_bFindLocal = TRUE;		////주어진 시험이 local PC에 존재함
		m_strFileLocation = strLocation;
		
		CFileFind aFileFinder;
		strTemp.Format("%s\\ch???.csv", m_strFileLocation);
		BOOL bFind = aFileFinder.FindFile(strTemp);
		while(bFind)
		{
			bFind = aFileFinder.FindNextFile();
			if(!aFileFinder.IsDirectory())
			{
				strLocation = aFileFinder.GetFileName();
				strTemp = strLocation.Left(strLocation.ReverseFind('.'));
				nCh = atol(strTemp.Right(3));
				code = m_resultData.GetLastCellCode(nCh-1);
				//Check Error
				if(code != EP_CODE_NONCELL && code != EP_CODE_CELL_NONE && code != EP_CODE_CELL_EXIST)
				{
					m_listBox.AddString(strTemp);
					m_listBox.SetItemData(m_listBox.GetCount()-1, nCh);	//Channel No
				}
			}
		}
		aFileFinder.Close();
	}
	
	strTemp.Format("%d 채널이 검색되었습니다.", m_listBox.GetCount());
	GetDlgItem(IDC_TOT_CH_COUNT_STATIC)->SetWindowText(strTemp);
	
	return TRUE;

}
*/
//현재 저장되어 있는 작업 List(Folder 단위 저장)를 검색 한다.
//Bug Fix 진행중 1번이라도 Download하면 local에서 찾은 data를 사용하는 bug 수정 20060523
//1.remote에 존재하며 무조건 remote data로 갱신 
//2. remote에 존재하지 않으면 local에서 찾는다.
BOOL CDataDownDlg::GetFolderList(CString strTestSerial)
{
	// TODO: Add your control notification handler code here
	m_listBox.ResetContent();
	
	CString strLocation, strTemp;
	int nMdCh = 0, nTrayCh = 0, nStepNum=0;
	BYTE code;
	CStringList chFadmList;
	CStringList chListModuleBase;				//remote channel list
	CStringList sensorChList;
	CString strTempLog;

	//결과 파일의 최종 Step 정보를 확인 한다.
	m_bFindLocal = FALSE;			//완료가 아니며 무조건 Download(주어진 시험이 local PC에 존재하지 않음)
	CString strFile = _T("");
	strFile = m_resultData.GetFileName();

	strLocation = strFile.Left(strFile.ReverseFind('.'));
	STR_STEP_RESULT *pStepResult = m_resultData.GetLastStepData();	//최종 step정보를 구함 
	if(pStepResult)
	{
		CFileFind fileFinder;
		//strLocation.Format("%s\\MD%03d\\%s", m_strDataTempPath, m_nModuleID, strTestSerial);
		if(	pStepResult->stepCondition.stepHeader.type == EP_TYPE_END 
			&& fileFinder.FindFile(strLocation))
		{
			m_bFindLocal = TRUE;	//주어진 시험이 local PC에 존재
		}
	}
	//진행중에 1개 Channel Download하고 완료되면 나머지 Channel을 Remote에서는 찾지 않는 Bug//200612/19 
	if(m_bModuleFind) m_bFindLocal = FALSE;

	//최종 data 사용로 갱신하기 위해 마지막이 End가 아닌 시험은 Remote에 존재하면 무조건 Download하여 갱신한다.
	//마지막인 End인 시험은 자동 DownLoad 되어 있다. 
	BOOL bFindRemote = FALSE;
	if(m_bFindLocal == FALSE)		//End Step Check
	{
		strTempLog.Format(TEXT_LANG[1], strTestSerial);
		WriteLog( strTempLog); // 100061
		strLocation = FindRealTimeDataFromRemote(m_strIPAddress, m_resultData.GetMDSysData()->nModuleGroupNo, m_resultData.GetTestSerialNo(), chListModuleBase, sensorChList, chFadmList);
		if(strLocation.IsEmpty())
		{
			strTempLog.Format(TEXT_LANG[2], strTestSerial); //100062
			WriteLog( strTempLog); // 100061
		}
		else
		{
			if(chListModuleBase.GetCount() < 1)	//remote에 존재 하지 않음
			{
				//WriteLog( strTestSerial+"는 저장조건이 설정되지 않았습니다.");
			}
			else
			{
				//Remote에 있는 파일을 List한다.
				strTempLog.Format(TEXT_LANG[3], strTestSerial); //100062
				WriteLog(strTempLog); // 100061
				m_strFileLocation = strLocation;

				//2.2 검색한 remote channel을  list 한다.
				POSITION pos = chListModuleBase.GetHeadPosition();	//Module Base channel no
				while(pos)
				{
					strTemp = chListModuleBase.GetNext(pos);				//format => ch001
					nMdCh = atol(strTemp.Right(3));							//Get module base channel number			
					nTrayCh = m_resultData.ConvertModuleCh2TrayCh(nMdCh);	//Convert module base channel no to Tray base channel no
					code = m_resultData.GetLastTrayChCellCode(nTrayCh-1);
					//Check Error => NonCell이 아닌 채널만 표시 한다.
					if(code != EP_CODE_NONCELL && code != EP_CODE_CELL_NONE && code != EP_CODE_CELL_EXIST )
					{
						strTemp.Format("CH %03d        [%03d]", nTrayCh, nMdCh);
						m_listBox.AddString(strTemp);						//Add
						m_listBox.SetItemData(m_listBox.GetCount()-1, nTrayCh);			//Channel No
					}
				}
				if(m_listBox.GetCount() > 0)	bFindRemote = TRUE;
			}

			if( chFadmList.GetCount() < 1 )
			{
				
			}
			else
			{
				POSITION pos = chFadmList.GetHeadPosition();
				while(pos)
				{
					strTemp = chFadmList.GetNext(pos);
					nMdCh = atol(strTemp.Mid(strTemp.ReverseFind('_')-3,3));
					nStepNum = atol(strTemp.Right(2));
					strTemp.Format("CH F%03d_%02d", nMdCh, nStepNum);							
					m_listBox.AddString(strTemp);						//Add
					m_listBox.SetItemData(m_listBox.GetCount()-1, nMdCh);			//Channel No
				}
			}

			if( sensorChList.GetCount() < 1 )
			{

			}
			else
			{
				POSITION pos = sensorChList.GetHeadPosition();
				while(pos)
				{
					strTemp = sensorChList.GetNext(pos);
					nMdCh = atol(strTemp.Right(3));
					strTemp.Format("CH Sensor%03d", nMdCh );							
					m_listBox.AddString(strTemp);						//Add
					m_listBox.SetItemData(m_listBox.GetCount()-1, nMdCh);			//Channel No
				}
			}
		}
	}

	//Remote에서 못찾았을 경우는 End까지 진행안되었더라도 Local에 있으면 모두 표시한다.
	if(m_bFindLocal == TRUE || bFindRemote == FALSE)		//End된 시험은 Local에서 찾는다.
	{
//		WriteLog( "Found ["+strTestSerial+"] from local database.");
		//strLocation.Format("%s\\MD%03d", m_strDataTempPath, m_nModuleID);
		strLocation = strFile.Left(strFile.ReverseFind('.'));
//		strLocation = FindRealTimeDataFromLocal(strLocation, strTestSerial);
		if(strLocation.IsEmpty())	
		{
			//WriteLog("Could not find location. ====> ["+strLocation+"]");
			strTempLog.Format(TEXT_LANG[4], strLocation); //100062
			WriteLog(strTempLog); // 100061
			return FALSE;
		}

		m_strFileLocation = strLocation;
		
		CFileFind aFileFinder;
		strTemp.Format("%s\\CH???_??.csv", m_strFileLocation);		//File name is CH001_01.csv format
		BOOL bFind = aFileFinder.FindFile(strTemp);
		while(bFind)
		{
			bFind = aFileFinder.FindNextFile();
			if(!aFileFinder.IsDirectory())
			{
				strLocation = aFileFinder.GetFileName();
				strTemp = strLocation.Left(strLocation.ReverseFind('.'));
				nTrayCh = atol(strTemp.Mid(2, 3));
				nMdCh = m_resultData.ConvertTrayCh2ModuleCh(nTrayCh);
				code = m_resultData.GetLastTrayChCellCode(nTrayCh-1);
				//Check Error => NonCell이 아닌 채널만 표시 한다.
				if(code != EP_CODE_NONCELL && code != EP_CODE_CELL_NONE && code != EP_CODE_CELL_EXIST )
				{
					strTemp.Format("CH %03d        [%03d]", nTrayCh, nMdCh);
					m_listBox.AddString(strTemp);
					m_listBox.SetItemData(m_listBox.GetCount()-1, nTrayCh);	//Channel No
				}
			}
		}
		aFileFinder.Close();
		
		if(m_listBox.GetCount() > 0)	m_bFindLocal = TRUE;
		else							m_bFindLocal = FALSE;

		if(m_bFindLocal)
		{
			//WriteLog("Found location. ====> ["+m_strFileLocation+"]");	
			strTempLog.Format(TEXT_LANG[5], m_strFileLocation); //100062
			WriteLog(strTempLog); // 100061
		}
		else
		{
			//WriteLog("Could not find location. ====> ["+m_strFileLocation+"]");
			strTempLog.Format(TEXT_LANG[6], m_strFileLocation); //100062
			WriteLog(strTempLog); // 100061
		}
	}

	//%d개의 셀 데이터를 찾을수 있습니다.
	strTemp.Format(TEXT_LANG[65], m_listBox.GetCount()); //100066
	GetDlgItem(IDC_TOT_CH_COUNT_STATIC)->SetWindowText(strTemp);
	
	return TRUE;

}

void CDataDownDlg::OnGetList() 
{
	// TODO: Add your control notification handler code here
	//주어진 testserial로 시험한 채널을 표시한다.
	if( !UpdateData() )
		return;
	
	CFileDialog pDlg(TRUE, "fmt", m_strUserOpenFileName, OFN_HIDEREADONLY|OFN_FILEMUSTEXIST, "Formation Data(*.fmt)|*.fmt|");
	pDlg.m_ofn.lpstrTitle = "Formation File";
	if(pDlg.DoModal() == IDOK)
	{
		m_strUserOpenFileName = pDlg.GetPathName();

		GetChannelList(m_strUserOpenFileName);

		m_bSearchModeOffLine = TRUE;

		GetDlgItem(IDC_OPTION_BUTTON)->EnableWindow(TRUE);
		GetDlgItem(IDC_RADIO1)->EnableWindow(TRUE);
		GetDlgItem(IDC_RADIO2)->EnableWindow(TRUE);
		GetDlgItem(IDC_STATIC_FORMAT)->EnableWindow(TRUE);
	}
}

void CDataDownDlg::OnDblclkDataList() 
{
	// TODO: Add your control notification handler code here
	if(SaveData() == TRUE)
	{		
		WriteLog(TEXT_LANG[7]); //100067
	}
}


void CDataDownDlg::OnSaveDataButton() 
{
	// TODO: Add your control notification handler code here
	
	//최종 목록으로 갱신한다.
	if(m_bSearchModeOffLine)
	{
		CString strTestSerial = LoadFormResultData(m_strUserOpenFileName);
		if(strTestSerial.IsEmpty())
		{
			AfxMessageBox(m_strUserOpenFileName+" Loading fail!!!");
			return;
		}
	}

	if(SaveData() == TRUE)
	{
		WriteLog(TEXT_LANG[7]); //100067
	}

	//Added by KBH	20070313
/*	CWordArray	awTraySelCh;
	awTraySelCh.Add(1);
	awTraySelCh.Add(2);
	awTraySelCh.Add(3);
	if(DownLoadLocalDataAll("c:\\a" , "c:\\a\\a.csv", awTraySelCh) == FALSE)
	{
	}
	//////////////////////////////////////////////////////////////////////////
*/
}

//Manual execute
BOOL CDataDownDlg::SaveData()
{
	CWaitCursor	wait;

	// Get the indexes of all the selected items.
	int nCount = m_listBox.GetSelCount();
	CArray<int,int> aryListBoxSel;

	aryListBoxSel.SetSize(nCount);
	m_listBox.GetSelItems(nCount, aryListBoxSel.GetData()); 

	// Dump the selection array.
#ifdef _DEBUG
	afxDump << aryListBoxSel;
#endif

	//1. 선택한 채널 리스트를 구한다.
	CWordArray awSelTrayCh;
	CWordArray awSelSensorCh;
	CDWordArray awSelFadmCh;	

	CString strTemp = _T("");
	int nRtn = 0;
	int nStepNum = 0;

	for(int i =0; i<aryListBoxSel.GetSize(); i++)
	{	
		m_listBox.GetText(aryListBoxSel.GetAt(i), strTemp);
		nRtn = strTemp.Find('F');
		if( nRtn == -1 )
		{
			nRtn = strTemp.Find('S');
			if( nRtn == - 1 )
			{
				awSelTrayCh.Add((WORD)m_listBox.GetItemData(aryListBoxSel.GetAt(i)));
			}
			else
			{
				awSelSensorCh.Add((WORD)m_listBox.GetItemData(aryListBoxSel.GetAt(i)));
			}
		}
		else
		{
			// Fadm data
			nStepNum = atol(strTemp.Right(2));
			int nTemp = strTemp.ReverseFind('F');
			int nCh = atol(strTemp.Mid(nTemp+1,3));
			awSelFadmCh.Add(MAKELONG(nCh, nStepNum));		
		}		
	}
	
	if( awSelTrayCh.GetSize() < 1 && awSelFadmCh.GetSize() < 1 && awSelSensorCh.GetSize() < 1 )
	{
		MessageBox("First, select channel list.", "Error", MB_OK|MB_ICONSTOP);
		return FALSE;
	}

	//2. Download한 파일을 저장할 위치를 지정한다.
	CString strDestPath;
	static CString strSelPath;
	if(((CButton *)GetDlgItem(IDC_RADIO1))->GetCheck() == BST_CHECKED || m_bSearchModeOffLine == FALSE)
	{
		CFolderDialog *pDlg = new CFolderDialog(strSelPath, BIF_RETURNONLYFSDIRS, this);
		pDlg->SetTitle("Select Folder");
		pDlg->m_Title = "Select save folder";

		if(pDlg->DoModal() == IDOK)
		{
			strSelPath = pDlg->GetPathName();
			delete pDlg;
			pDlg = NULL;
		}
		else 
		{
			delete pDlg;
			pDlg = NULL;
			return FALSE;
		}
		//3. 새로운 폴더 생성;
		CNameInputDlg dlg;
		//저장할 폴더명을 입력하십시요.
		dlg.m_strTitle = "input the save folder name"; //100069
		//dlg.m_strTitle = "저장할 폴더명을 입력하십시요."; //100069
		if(m_resultData.IsLoaded() && m_bSearchModeOffLine)
		{
			dlg.m_strName = m_strTestName+"_"+m_resultData.GetLotNo()+"_"+m_resultData.GetTrayNo();
		}
		else
		{
			GetDlgItem(IDC_STATIC_UNIT)->GetWindowText(dlg.m_strName);
		}
		if(dlg.DoModal() != IDOK)
		{
			return TRUE;
		}
		strDestPath = strSelPath+ "\\"+ dlg.m_strName;
		strDestPath = MakeMyFolder(strDestPath, FALSE);
		//////////////////////////////////////////////////////////////////////////
	}
	else
	{
		CFileDialog pDlg(FALSE, "csv", "", OFN_HIDEREADONLY|OFN_FILEMUSTEXIST, "csv( , )(*.csv)|*.csv|"); //100070
		pDlg.m_ofn.lpstrTitle = "csv File";
		if(pDlg.DoModal() == IDOK)
		{
			strDestPath = pDlg.GetPathName();
		}
		else
		{
			return FALSE;
		}
	}

	if(strDestPath.IsEmpty())
	{
		//대상 폴더를 만들 수 없습니다.
		MessageBox(TEXT_LANG[3], "Error", MB_OK|MB_ICONSTOP); //100071
		//MessageBox("Can't create destination folder.\n"+strDestPath, "Error", MB_OK|MB_ICONSTOP); //100071
		return FALSE;
	}

	//4. Search channel file and download to destination location
	if(GetRealTimeData(m_bFindLocal, m_strFileLocation, awSelTrayCh, awSelFadmCh, awSelSensorCh, m_strIPAddress, strDestPath) == FALSE)
	{
		//채널 데이터를 찾을 수 없습니다.
		MessageBox(TEXT_LANG[4], "Error", MB_OK|MB_ICONSTOP); //100072
		//MessageBox("Can't find channel data.", "Error", MB_OK|MB_ICONSTOP);
		return TRUE;
	}

	//5. 저장 폴더를 Open한다.
	CString strArgu;
	if(((CButton *)GetDlgItem(IDC_RADIO1))->GetCheck() == BST_CHECKED)
	{
		strArgu.Format("explorer %s", strDestPath);
	}
	else
	{
		strArgu.Format("excel %s", strDestPath);
	}
	//Open Folder
	STARTUPINFO	stStartUpInfo;
	PROCESS_INFORMATION	pi;
	ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
	ZeroMemory(&pi, sizeof(PROCESS_INFORMATION));
	
	stStartUpInfo.cb = sizeof(STARTUPINFO);
	stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
	stStartUpInfo.wShowWindow = SW_SHOWNORMAL;

		
	BOOL bFlag = ::CreateProcess(	NULL, 
									(LPSTR)(LPCTSTR)strArgu, 
									NULL, 
									NULL, 
									FALSE, 
									NORMAL_PRIORITY_CLASS, 
									NULL, 
									NULL, 
									&stStartUpInfo, 
									&pi
								);
	if(bFlag == FALSE)
	{
		return FALSE;
	}
	CloseHandle( pi.hProcess );
	CloseHandle( pi.hThread );		
		
	return	TRUE; 
}


void CDataDownDlg::OnContextMenu(CWnd* pWnd, CPoint point) 
{
	// TODO: Add your message handler code here

//	TRACE("%d/%d Clicked\n", point.x, point.y);

	CRect rect;
	GetDlgItem(IDC_DATA_LIST)->GetWindowRect(&rect);

//	TRACE("%d/%d %d/%dClicked\n", rect.left, rect.top, rect.right, rect.bottom);

	if(point.x > rect.left && point.x < rect.right && point.y > rect.top && point.y < rect.bottom)
	{
		CMenu menu, *pPopup;
		VERIFY(menu.LoadMenu(IDR_CONTEXT_MENU));
		pPopup = menu.GetSubMenu(0);
		ASSERT(pPopup != NULL);
		
		CWnd* pWndPopupOwner = this;
		while (pWndPopupOwner->GetStyle() & WS_CHILD)
			pWndPopupOwner = pWndPopupOwner->GetParent();

		pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, pWndPopupOwner);
	}
}

void CDataDownDlg::OnDownloadData() 
{
	// TODO: Add your command handler code here
	SaveData();
}

void CDataDownDlg::OnUpdateDeleteData(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(m_listBox.GetSelCount());
}

void CDataDownDlg::OnUpdateDownloadData(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(m_listBox.GetSelCount());
	
}

void CDataDownDlg::OnDeleteData() 
{
	// TODO: Add your command handler code here
//	DeleteData();
}

BOOL CDataDownDlg::DeleteData()
{
	int num = m_listBox.GetCurSel();
	CString strData;
	if(num != LB_ERR)
	{
		m_listBox.GetText(num, strData);
	}
	else
		return FALSE;

	CString strCurPath;
	//%s의 데이터 삭제 하시겠습니까?
	strCurPath.Format(TEXT_LANG[5], strData); //100073
	//strCurPath.Format("Delete %s Data?", strData);
	if(MessageBox(strCurPath, "Del", MB_YESNO|MB_ICONQUESTION) != IDYES)
	{
		return FALSE;
	}

	int nIndex = strData.ReverseFind('(');
	if(nIndex < 0)	return FALSE;


	CFtpConnection   *pConnect=NULL;         //Ftp 명령 연결 컨트롤 클래스
    TCHAR sz[1024];

	//IP Address Check
	if(m_strIPAddress.IsEmpty())
	{
		return FALSE;
	}

	BeginWaitCursor();	//Set Wait Cursor

	try		//Ftp Connection
	{
		m_InternetSession.SetOption(INTERNET_OPTION_CONNECT_TIMEOUT, 5000);
		pConnect = m_InternetSession.GetFtpConnection(m_strIPAddress, m_strID, m_strPassword);
    }
	catch(CInternetException *pEx)
	{
		EndWaitCursor();
		pEx->GetErrorMessage(sz, 1024);
	    pEx->Delete();
		AfxMessageBox(sz);
		return FALSE;
	}
	
	try
	{	
		//내부 파일 모두 삭제
		strCurPath = m_strPathName + "/" + strData.Mid(nIndex+1, strData.GetLength()-(nIndex)-2);		//Remote 저장 위치
		
		//Remote 현재 위치 변경 
		if(pConnect->SetCurrentDirectory(strCurPath) == FALSE)		
		{
			AfxMessageBox(TEXT_LANG[58]); //100074
			pConnect->Close();
			EndWaitCursor();
			return FALSE;
		}
			
		//폴더내 모든 파일 삭제 
		CFtpFileFind *pFileFind = new CFtpFileFind(pConnect);
		BOOL bContinue = pFileFind->FindFile(_T("*"));	
		CString strRemoteFile, strLocalFile;
		while(bContinue)
		{
			bContinue = pFileFind->FindNextFile();
			strRemoteFile = pFileFind->GetFileName();
			if(pConnect->Remove(strRemoteFile) == TRUE)
			{
			}
		}
		pFileFind->Close();
		delete pFileFind;
		pFileFind = NULL;

		//Remote 현재 위치 변경 
		if(pConnect->SetCurrentDirectory(m_strPathName) == FALSE)		
		{
			AfxMessageBox(TEXT_LANG[58]); //100074
			pConnect->Close(); 
			EndWaitCursor();
			return FALSE;
		}

		//자기 자신 폴더 삭제 
		if(pConnect->RemoveDirectory(strData.Mid(nIndex+1, strData.GetLength()-(nIndex)-2)) == FALSE)
		{
			LPVOID lpMsgBuf;
			FormatMessage( 
				FORMAT_MESSAGE_ALLOCATE_BUFFER | 
				FORMAT_MESSAGE_FROM_SYSTEM | 
				FORMAT_MESSAGE_IGNORE_INSERTS,
				NULL,
				GetLastError(),
				MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
				(LPTSTR) &lpMsgBuf,
				0,
				NULL 
			);
			// Process any inserts in lpMsgBuf.
			// ...
			// Display the string.
			MessageBox( (LPCTSTR)lpMsgBuf, "Error", MB_OK | MB_ICONINFORMATION );
			// Free the buffer.
			LocalFree( lpMsgBuf );
		}
	}
	catch(CInternetException *pEx)
	{
		pEx->GetErrorMessage(sz, 1024);
		pEx->Delete();
		AfxMessageBox(sz);
	}
		
	pConnect->Close();
	delete pConnect;
	EndWaitCursor();

	//폴더 리스트 갱신 
//	if(GetFolderList())
//		GetDlgItem(IDC_BUTTON1)->EnableWindow(TRUE);
//	else
		GetDlgItem(IDC_BUTTON1)->EnableWindow(FALSE);

	return TRUE;
}

//모든 모듈의 IP에서 주어진 serial의 시험을 찾는다.
CString CDataDownDlg::GetRemoteLocation(CString strTestSerial, CString strIp, int nGroupNo, int nLineMode)
{
	if(strIp.IsEmpty())	
	{
		WriteLog(TEXT_LANG[8]);
		return "";
	}
	CString strTemp;
	CWaitCursor wait;
	
	CString strRemoteLocation;
	CFtpDownLoad *pDownLoad = new CFtpDownLoad;
	
	BOOL bConnect = FALSE;
	bConnect = pDownLoad->ConnectFtpSession(strIp, m_strID, m_strPassword);
	if(bConnect == FALSE)
	{
		strTemp.Format(TEXT_LANG[9], strIp, pDownLoad->GetLastErrorMsg());
		WriteLog(strTemp);
		delete pDownLoad;
		return "";
	}
	
	char szBuff[_MAX_PATH];//, szBuff1[_MAX_PATH];
	::GetTempPath(_MAX_PATH, szBuff);		//Get Windows temp. folder
	CString strRemoteFileName, strLocalFileName;

	//1. 목록 Index 파일을 Download한 후 주어진 test serialno의 폴더를 찾는다.
	strLocalFileName.Format("%s\\downtemp.csv", szBuff);
	strRemoteFileName.Format("%s/group%d/savingFileIndex_start.csv", m_strPathName, nGroupNo);

	if(pDownLoad->DownLoadFile(strLocalFileName, strRemoteFileName) < 1)
	{
//		WriteLog(strRemoteFileName);
//		WriteLog(strLocalFileName);
		WriteLog(TEXT_LANG[10]);
		delete pDownLoad;
		// return strRemoteLocation;
		return "";
	}
	delete pDownLoad;
	pDownLoad = NULL;

	CString strDataBuf, strData, strSerial;
	int nIndex = 0;
	BOOL bFind = FALSE;

	//Download list file open
	CStdioFile lineFile;
	if(lineFile.Open(strLocalFileName, CFile::modeRead) == FALSE)
	{
		lineFile.Close();

		//strTemp.Format("Remote filelist Open fail[%s].", strLocalFileName);
		strTemp.Format(TEXT_LANG[11], strLocalFileName);
		WriteLog(strTemp);
		return "";
	}

	if( nLineMode == 0 )
	{
		// 1. Local Mode
		while(lineFile.ReadString(strDataBuf))
		{
			strData = strDataBuf;
			strData.TrimLeft(" ");
			strData.TrimRight(" ");
			if(strData.Right(1) != ",")		strData +=",";

			int p1 = strData.Find("test_serial_no");
			int p2 = strData.Find(' ', p1);

			if( p2 > 0 )
			{
				int p3 = strData.Find(',', p2);
				strSerial = strData.Mid(p2+1, p3-p2-1);

				int p4 = strSerial.GetLength();

				if(strTestSerial.Left(p4) == strSerial)
				{
					bFind = TRUE;

					p1 = strData.Find("index");
					p2 = strData.Find(' ', p1);
					p3 = strData.Find(',', p2);
					nIndex = atol(strData.Mid(p2+1, p3-p2-1));
				}
			}
		}
	}
	else
	{
		while(lineFile.ReadString(strDataBuf))
		{
			// 2. Auto Mode ( Online )
			strData = strDataBuf;

			strData.TrimLeft(" ");
			strData.TrimRight(" ");
			if(strData.Right(1) != ",")		strData +=",";

			int p1 = strData.Find("tray_id");
			int p2 = strData.Find(' ', p1);
			if( p2 > 0 )
			{
				int p3 = strData.Find(',', p2);
				if(p3 < 0)
				{
					strSerial = strData.Mid(p2+1);
				}
				else
				{
					strSerial = strData.Mid(p2+1, p3-p2-1);
				}

				if(strTestSerial == strSerial)
				{
					bFind = TRUE;

					p1 = strData.Find("index");
					p2 = strData.Find(' ', p1);
					p3 = strData.Find(',', p2);
					nIndex = atol(strData.Mid(p2+1, p3-p2-1));
				}
			}					
		}
	}
	
	lineFile.Close();

	_unlink(strLocalFileName);

	//주어진 Serial의 Test가 목록에 존재 하지 않을 경우(리눅스에서 이미 삭제된 경우) 	
	if(bFind == FALSE)
	{
		//임의의 폴더명을 주어 폴더명은 존재하지만 Ch이 없는 것으로 return
		strRemoteLocation.Format("%s/group%d/unkown_not_exist_%d", m_strPathName, nGroupNo, 1000000);
	}
	else
	{
		strRemoteLocation.Format("%s/group%d/data%d", m_strPathName, nGroupNo, nIndex);
	}

	//2. 주어진 test serialno의 폴더의 channel 목록을 표시한다.
	return strRemoteLocation;
}

//Download channel real time data and make excel file
//bAuto : TRUE	=> 저정위치에 파일을 자동으로 backup한다.(최근 N건)
//		: FALSE => 사용자가 지정한 위치에 파일을 backup한다.(1차적으로 PC에서 검색하고 없으면 SBC에서 검색) 

//SBC Dir 구조
// 1. 모니터링 data 저장 위치 
// /root/formation_data/monitoringData/group1/savingFileIndex_start.csv			=> 시험 목록 저장 파일(test serial no 포함)
//											 /data1/ch001_MonitoringData01.csv	=> Ch1번의 시험 1일째 저장 data
//												   /ch001_MonitoringData02.csv	=> Ch1번의 시험 2일째 저장 data
// 2. Step End Data 저장 위치
// /root/formation_data/resultData/group1/ch001_SaveData.csv					=> 각 step의 End Data 저장 파일  
BOOL CDataDownDlg::DownLoadRemoteData( CString strIp, CString strRemote, CString strSaveDir, CDWordArray &aSelCh, CWordArray &awSensorCh, CDWordArray &awFadmCh)
{
	CWaitCursor wait;
	if(strIp.IsEmpty() || strRemote.IsEmpty())
	{
		return FALSE;
	}

	//1. 주어진 serialno가 저장된 remote 위치를 구한다.
	CFtpConnection   *pConnect=NULL;         //Ftp 명령 연결 컨트롤 클래스
    TCHAR sz[1024];

	try		//Ftp Connection
	{
		m_InternetSession.SetOption(INTERNET_OPTION_CONNECT_TIMEOUT, 5000);
		pConnect = m_InternetSession.GetFtpConnection(strIp, m_strID, m_strPassword);
    }
	catch(CInternetException *pEx)
	{
		pEx->GetErrorMessage(sz, 1024);
		WriteLog(sz);
	    pEx->Delete();
		return FALSE;
	}
	
	//Remote Current Directory
	if(pConnect->SetCurrentDirectory(strRemote) == FALSE)		
	{
		//WriteLog("Remote file location error!!"); //100074
		WriteLog(TEXT_LANG[12]); //100074
		pConnect->Close();
		return FALSE;
	}

	/*
	CInternetFile *file;
	try
	{
		file=pConnect->OpenFile(pstrName, GENERIC_READ, NULL);
	}
	catch(CInternetException *)
	{
		return FALSE;
	}

	DWORD dwSize=file->GetLength();// 파일의 크기
	CString str;
	str.Format("%d", dwSize);
	AfxMessageBox(str);
	*/
	
	//Make New Directory
	CString strCurPath, strTemp;
	//strTemp.Format("Download data form %s %s", strIp, strRemote); //100075
	strTemp.Format(TEXT_LANG[13], strIp, strRemote); //100075
	WriteLog(strTemp);

	m_downProgress.ShowWindow(SW_SHOW);
	m_downProgress.SetPos(0);

	//int nTotalCount = 0, nDownLoadedCount=0, 
	int nCount = 0;
	CString strFilter;
	CStringList strDownFileList;

	CStringList astrDownSensorFileList[EP_MAX_SENSOR_CH], strSensorFileList;
	CString strRemoteFile, strLocalFile;
	CFtpFileFind *pFileFind = new CFtpFileFind(pConnect);
	CFileFind bFinder;

	//선택된 Channel에 사용된 Sensor channel을 검색한다.
	int nChNo;
//	down load해야할 channel 목록만큼 download한다.
//	for(int index = 0; index < EP_MAX_SENSOR_CH; index++)
//	{
//		nChNo = index+1;

	for(int index = 0; index < awSensorCh.GetSize(); index++)
	{
		nChNo = awSensorCh[index];
		//download sensor file
		for(int fIdx = 0; fIdx < 99; fIdx++)	//Max 100일 data 저장 
		{
			strFilter.Format("sensor_data_ch%03d_%02d.csv", nChNo, fIdx+1);
			if(pFileFind->FindFile(strFilter) == FALSE)
			{
				pFileFind->Close();
				break;
			}
			pFileFind->FindNextFile();				
			strRemoteFile = pFileFind->GetFileName();

			strLocalFile = strSaveDir+"\\"+strRemoteFile;

			BOOL bDownLoad = FALSE;
			if(bFinder.FindFile(strLocalFile))
			{
				bFinder.FindNextFile();	
				int size1 = pFileFind->GetLength();
				int size2 = bFinder.GetLength();
				
				//2006/07/14 Size가 같은 파일이 이미 있으면 Download를 생략한다.
				if(size1 == size2)
				{
					bDownLoad = TRUE;
				}
				bFinder.Close();
			}
			
			if(bDownLoad == FALSE)
			{
				if(pConnect->GetFile(strRemoteFile , strLocalFile, FALSE, FILE_ATTRIBUTE_NORMAL, FTP_TRANSFER_TYPE_BINARY) == TRUE)
				{
					bDownLoad = TRUE;
				}
				else
				{
					//Channel list와 같은 size가 되도록 하기 위해 
					//파일 Open시 error 나도록 하는 이름을 삽입한다.(MakeExcelFile()에서 파일 이름 error시 자동 Skip한다.
					astrDownSensorFileList[nChNo-1].AddTail("%%%%%%%%Error%%%%%%%%%%");		
				}
			}

			if(bDownLoad)
			{
				astrDownSensorFileList[nChNo-1].AddTail(strLocalFile);
			}
			
			pFileFind->Close();
		}
	}
	
	//download한 파일은 삭제하지 않는다.
	//Sensor 이름과 시간 정보 전달이 필요 !!!!
	if(MakeSensorExcelFile(astrDownSensorFileList, strSaveDir, awSensorCh) == FALSE)
	{
		//WriteLog("Sensor data save fail!!!");
		WriteLog(TEXT_LANG[14]);
	}

#ifdef _DEBUG
	char szBuff[_MAX_PATH];
	DWORD nLength = _MAX_PATH;
	pConnect->GetCurrentDirectory(szBuff, &nLength);
	TRACE("FTP current directory is %s\n", szBuff);

	CFtpFileFind *pDebugFileFind = new CFtpFileFind(pConnect);
	BOOL bOK = pDebugFileFind->FindFile("*");
	while(bOK)
	{
		bOK = pDebugFileFind->FindNextFile();
		TRACE("%s\n", pDebugFileFind->GetFilePath());
	}
	pDebugFileFind->Close();
	
	delete pDebugFileFind;
	pDebugFileFind = NULL;
#endif

	//Channel File Download
	WORD wModuleCh, wTrayCh;
	//2007/04/18
	//현재 진행 중인 작업을 Download 할 경우 첫번째 Download한 record 보다 나중에 download한 record 수가 더 많다.
	//=> Excel 파일로 통합 저장시 오류가 발생(뒤부분 Data가 섞임)하므로 
	//뒷 채널부터 Download 하고 Excel 통합 저장시는 앞채널 부터 저장 한다.
	//for(index = 0; index < aSelCh.GetSize(); index++)
	//{
	//	m_downProgress.SetPos(index * 100/aSelCh.GetSize());
	for(index = aSelCh.GetSize()-1; index >=0; index--)
	{
		m_downProgress.SetPos((aSelCh.GetSize()-index) * 100/aSelCh.GetSize());

		//*************************************
		//파일을 저장 순서대로 sort하여야 한다.
		strDownFileList.RemoveAll();		
//		strSensorFileList.RemoveAll();
		DWORD  aa = aSelCh[index];
		wModuleCh = HIWORD(aSelCh[index]);
		wTrayCh = LOWORD(aSelCh[index]);

		//***기존 파일 삭제 
		CFileFind aCSVFinder;
		strTemp.Format("%s\\CH%03d_*.csv", strSaveDir, wTrayCh);
		BOOL bFind = aCSVFinder.FindFile(strTemp);
		while(bFind)
		{
			bFind = aCSVFinder.FindNextFile();
			unlink(aCSVFinder.GetFilePath());
		}

		for(int fIdx = 0; fIdx < 99; fIdx++)	//Max 100일 data 저장 
		{
			//remote file format ch001_MonitoringData01.csv
			strFilter.Format("ch%03d_MonitoringData%02d.csv", wModuleCh, fIdx+1);
			if(pFileFind->FindFile(strFilter) == FALSE)
			{
/*				DWORD dwCode = ::GetLastError();
				if(dwCode ==  ERROR_FILE_EXISTS)
				{
					WriteLog(strLocalFile+" file is exist.");
				}
				else
				{
					LPVOID lpMsgBuf;
					FormatMessage( 
						FORMAT_MESSAGE_ALLOCATE_BUFFER | 
						FORMAT_MESSAGE_FROM_SYSTEM | 
						FORMAT_MESSAGE_IGNORE_INSERTS,
						NULL,
						dwCode,
						MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
						(LPTSTR) &lpMsgBuf,
						0,
						NULL 
					);
					// Process any inserts in lpMsgBuf.
					// ...
					// Display the string.
					WriteLog((LPCTSTR)lpMsgBuf);
					//MessageBox( (LPCTSTR)lpMsgBuf, "Error", MB_OK | MB_ICONINFORMATION );
					// Free the buffer.
					LocalFree( lpMsgBuf );
				}
*/				pFileFind->Close();
				break;
			}
						
			pFileFind->FindNextFile();				
			strRemoteFile = pFileFind->GetFileName();

			//임시 폴더에 Channel data download 저장 
			//strLocalFile.Format("%s\\%s", strSaveDir, strFilter);
			//V2.0 바로 TrayIndex Channel 번호로 이름 바꿔서 저장한다.
			strLocalFile.Format("%s\\CH%03d_%02d.csv", strSaveDir, wTrayCh, fIdx+1);
			
			BOOL bDownLoad = FALSE;

			//위쪽 		//***기존 파일 삭제 부분이 있어서 의미가 없음 
			if(bFinder.FindFile(strLocalFile))
			{
				bFinder.FindNextFile();	
				int size1 = pFileFind->GetLength();
				int size2 = bFinder.GetLength();
				
				//2006/07/14 Size가 같은 파일이 이미 있으면 Download를 생략한다.
				//같은 내용이라도 Linux의 파일크기와 Windows의 파일크기가 다름
				if(size1 == size2)
				{
					bDownLoad = TRUE;
				}
				bFinder.Close();
			}

			if(bDownLoad == FALSE)
			{
				if(pConnect->GetFile(strRemoteFile , strLocalFile, FALSE, FILE_ATTRIBUTE_NORMAL, FTP_TRANSFER_TYPE_BINARY) == TRUE)
				{
					bDownLoad = TRUE;
				}
				else
				{
					DWORD dwCode = ::GetLastError();
					if(dwCode ==  ERROR_FILE_EXISTS)
					{
						WriteLog(strLocalFile+" file is exist.");
					}
					else
					{
						LPVOID lpMsgBuf;
						FormatMessage( 
							FORMAT_MESSAGE_ALLOCATE_BUFFER | 
							FORMAT_MESSAGE_FROM_SYSTEM | 
							FORMAT_MESSAGE_IGNORE_INSERTS,
							NULL,
							dwCode,
							MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
							(LPTSTR) &lpMsgBuf,
							0,
							NULL 
						);
						// Process any inserts in lpMsgBuf.
						// ...
						// Display the string.
						WriteLog((LPCTSTR)lpMsgBuf);
						//MessageBox( (LPCTSTR)lpMsgBuf, "Error", MB_OK | MB_ICONINFORMATION );
						// Free the buffer.
						LocalFree( lpMsgBuf );
					}
				}
			}

			pFileFind->Close();
			if(bDownLoad)
			{
				strDownFileList.AddTail(strLocalFile);
				TRACE("Add file list %s\n", strLocalFile);
			}


			//download sensor file
//			strFilter.Format("sensor_data_ch%03d_%02d.csv", SensorCh(aSelCh[index]), fIdx+1);		//mapping된 Sensor channel을 구한다.
/*			strFilter.Format("%s\\sensor_data_ch%03d_%02d.csv", strSaveDir, aSelCh[index], fIdx+1);
			if(bFinder.FindFile(strFilter))
			{
				bFinder.FindNextFile();				
				strLocalFile = bFinder.GetFilePath();
				strSensorFileList.AddTail(strLocalFile);
				bFinder.Close();
			}
			else
			{
				strSensorFileList.AddTail("%%%%%%%%Error%%%%%%%%%%");		
			}
*/
			//HandleMessage();
			PUMPMESSAGE();
		}

		//download한 파일을 1개의 excel파일로 만든다.
		//V 2.0 부터는 SBC에서 m단위로 저장되므로 PC에서 변환할 필요 없음
/*		if(strDownFileList.GetCount() > 0)
		{
			strLocalFile.Format("%s\\ch%03d.csv", strSaveDir, wTrayCh);
			if(MakeExcelFile(strLocalFile, strDownFileList, strSensorFileList) == FALSE)
			{
				WriteLog(strLocalFile+"를 생성할수 없습니다.");
			}
		}
*/
	}

	// 1. awFadmCh data downloding..
	for(index = awFadmCh.GetSize()-1; index >=0; index--)
	{
		m_downProgress.SetPos((awFadmCh.GetSize()-index) * 100/awFadmCh.GetSize());

		//*************************************
		//파일을 저장 순서대로 sort하여야 한다.

		DWORD  aa = awFadmCh[index];
		wModuleCh = HIWORD(awFadmCh[index]);
		wTrayCh = LOWORD(awFadmCh[index]);

		//***기존 파일 삭제 
		CFileFind aCSVFinder;
		strTemp.Format("%s\\FadmCH%03d_*.csv", strSaveDir, wTrayCh);
		BOOL bFind = aCSVFinder.FindFile(strTemp);
		while(bFind)
		{
			bFind = aCSVFinder.FindNextFile();
			unlink(aCSVFinder.GetFilePath());
		}

		for(int fIdx = 0; fIdx < 99; fIdx++)	//Max 100일 data 저장 
		{
			strFilter.Format("ch%03d_FadmData%02d.csv", wModuleCh, fIdx+1);
			
			if(pFileFind->FindFile(strFilter) == FALSE)
			{
/*				DWORD dwCode = ::GetLastError();
				if(dwCode ==  ERROR_FILE_EXISTS)
				{
					WriteLog(strLocalFile+" file is exist.");
				}
				else
				{
					LPVOID lpMsgBuf;
					FormatMessage( 
						FORMAT_MESSAGE_ALLOCATE_BUFFER | 
						FORMAT_MESSAGE_FROM_SYSTEM | 
						FORMAT_MESSAGE_IGNORE_INSERTS,
						NULL,
						dwCode,
						MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
						(LPTSTR) &lpMsgBuf,
						0,
						NULL 
					);
					// Process any inserts in lpMsgBuf.
					// ...
					// Display the string.
					WriteLog((LPCTSTR)lpMsgBuf);
					//MessageBox( (LPCTSTR)lpMsgBuf, "Error", MB_OK | MB_ICONINFORMATION );
					// Free the buffer.
					LocalFree( lpMsgBuf );
				}
*/				pFileFind->Close();
				break;
			}
						
			pFileFind->FindNextFile();				
			strRemoteFile = pFileFind->GetFileName();

			//임시 폴더에 Channel data download 저장 
			//strLocalFile.Format("%s\\%s", strSaveDir, strFilter);
			//V2.0 바로 TrayIndex Channel 번호로 이름 바꿔서 저장한다.
			strLocalFile.Format("%s\\FadmCH%03d_%02d.csv", strSaveDir, wTrayCh, fIdx+1);
			
			BOOL bDownLoad = FALSE;

			//위쪽 		//***기존 파일 삭제 부분이 있어서 의미가 없음 
			if(bFinder.FindFile(strLocalFile))
			{
				bFinder.FindNextFile();	
				int size1 = pFileFind->GetLength();
				int size2 = bFinder.GetLength();
				
				//2006/07/14 Size가 같은 파일이 이미 있으면 Download를 생략한다.
				//같은 내용이라도 Linux의 파일크기와 Windows의 파일크기가 다름
				if(size1 == size2)
				{
					bDownLoad = TRUE;
				}
				bFinder.Close();
			}

			if(bDownLoad == FALSE)
			{
				if(pConnect->GetFile(strRemoteFile , strLocalFile, FALSE, FILE_ATTRIBUTE_NORMAL, FTP_TRANSFER_TYPE_BINARY) == TRUE) // kky
				{
					bDownLoad = TRUE;
				}
				else
				{
					DWORD dwCode = ::GetLastError();
					if(dwCode ==  ERROR_FILE_EXISTS)
					{
					
					}
					else
					{
						LPVOID lpMsgBuf;
						FormatMessage( 
							FORMAT_MESSAGE_ALLOCATE_BUFFER | 
							FORMAT_MESSAGE_FROM_SYSTEM | 
							FORMAT_MESSAGE_IGNORE_INSERTS,
							NULL,
							dwCode,
							MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
							(LPTSTR) &lpMsgBuf,
							0,
							NULL 
						);
						// Process any inserts in lpMsgBuf.
						// ...
						// Display the string.
						WriteLog((LPCTSTR)lpMsgBuf);
						//MessageBox( (LPCTSTR)lpMsgBuf, "Error", MB_OK | MB_ICONINFORMATION );
						// Free the buffer.
						LocalFree( lpMsgBuf );
					}
				}
			}
			pFileFind->Close();

			if(bDownLoad)
			{
				strDownFileList.AddTail(strLocalFile);
				TRACE("Add file list %s\n", strLocalFile);
			}
			//download sensor file
//			strFilter.Format("sensor_data_ch%03d_%02d.csv", SensorCh(aSelCh[index]), fIdx+1);		//mapping된 Sensor channel을 구한다.
/*			strFilter.Format("%s\\sensor_data_ch%03d_%02d.csv", strSaveDir, aSelCh[index], fIdx+1);
			if(bFinder.FindFile(strFilter))
			{
				bFinder.FindNextFile();				
				strLocalFile = bFinder.GetFilePath();
				strSensorFileList.AddTail(strLocalFile);
				bFinder.Close();
			}
			else
			{
				strSensorFileList.AddTail("%%%%%%%%Error%%%%%%%%%%");		
			}
*/
			//HandleMessage();
			PUMPMESSAGE();
		}

		//download한 파일을 1개의 excel파일로 만든다.
		//V 2.0 부터는 SBC에서 m단위로 저장되므로 PC에서 변환할 필요 없음
/*		if(strDownFileList.GetCount() > 0)
		{
			strLocalFile.Format("%s\\ch%03d.csv", strSaveDir, wTrayCh);
			if(MakeExcelFile(strLocalFile, strDownFileList, strSensorFileList) == FALSE)
			{
				WriteLog(strLocalFile+"를 생성할수 없습니다.");
			}
		}
*/
	}

	//delete downloaded sensor file
	strFilter.Format("%s\\sensor_data_*.csv", strSaveDir);
	BOOL bFind = bFinder.FindFile(strFilter);
	while(bFind)
	{
		bFind = bFinder.FindNextFile();				
		strLocalFile = bFinder.GetFilePath();
		_unlink(strLocalFile);
	}
	bFinder.Close();

	delete pFileFind;
	pFileFind = NULL;
	
	pConnect->Close();
	delete pConnect;

	m_downProgress.SetPos(100);
	m_downProgress.ShowWindow(SW_HIDE);

	return TRUE;
}

//20090710 KBH
//이어받기 기능 추가 
//FTP session을 2개 open 함  
BOOL CDataDownDlg::DownLoadRemoteDataA( CString strIp, CString strRemote, CString strSaveDir, CDWordArray &aSelCh, CWordArray &awSensorCh, CDWordArray &awFadmCh)
{
//	CWaitCursor wait;
	if(strIp.IsEmpty() || strRemote.IsEmpty())
	{
		return FALSE;
	}

	DWORD start = GetTickCount();

	CString strCurPath, strTemp;
	strTemp.Format("Download data form %s %s", strIp, strRemote); //100075
	WriteLog(strTemp);

	m_downProgress.ShowWindow(SW_SHOW);
	m_downProgress.SetPos(0);

	CString strFilter, strRemoteFile, strLocalFile;
	CStringList strDownFileList, strServerFileList, astrDownSensorFileList[EP_MAX_SENSOR_CH];

	//선택된 Channel에 사용된 Sensor channel을 검색한다.
	int nChNo;
	for(int index = 0; index < awSensorCh.GetSize(); index++)
	{
		nChNo = awSensorCh[index];
		//download sensor file
		for(int fIdx = 0; fIdx < 1; fIdx++)	//Max 100일 data 저장 
		{
			strFilter.Format("sensor_data_ch%03d_%02d.csv", nChNo, fIdx+1);
			strServerFileList.AddTail(strFilter);
			strDownFileList.AddTail(strFilter);
			
			strLocalFile = strSaveDir+"\\"+strFilter;

//			astrDownSensorFileList[nChNo-1].AddTail("%%%%%%%%Error%%%%%%%%%%");		
			astrDownSensorFileList[nChNo-1].AddTail(strLocalFile);
		}		
	}

	//Download sensor file
	if(DownloadDataFile(strRemote, strSaveDir , strServerFileList, strDownFileList, strIp, m_strID, m_strPassword))
	{
		WriteLog(strSaveDir+TEXT_LANG[15]); //100075
	}	
	
	// download한 파일은 삭제하지 않는다.
	// Sensor 이름과 시간 정보 전달이 필요 !!!!
	if(MakeSensorExcelFile(astrDownSensorFileList, strSaveDir, awSensorCh) == FALSE)
	{
		//WriteLog("Sensor data save fail!!!");
		WriteLog(TEXT_LANG[14]);
	}
	else
	{
		//delete downloaded sensor file
		CFileFind ff;
		strFilter.Format("%s\\sensor_data_*.csv", strSaveDir);
		BOOL bFind = ff.FindFile(strFilter);
		while(bFind)
		{
			bFind = ff.FindNextFile();				
			strLocalFile = ff.GetFilePath();
			_unlink(strLocalFile);
		}
		ff.Close();
	}
	//////////////////////////////////////////////////////////////////////////
	strDownFileList.RemoveAll();
	strServerFileList.RemoveAll();

	//Channel File Download
	WORD wModuleCh, wTrayCh, wStepNum;
	for(index = aSelCh.GetSize()-1; index >=0; index--)
	{
		wModuleCh = HIWORD(aSelCh[index]);
		wTrayCh = LOWORD(aSelCh[index]);

		for(int fIdx = 0; fIdx < 1; fIdx++)	//Max 100일 data 저장 
		{
			strFilter.Format("ch%03d_MonitoringData%02d.csv", wModuleCh, fIdx+1);
			strServerFileList.AddTail(strFilter);
			strLocalFile.Format("CH%03d_%02d.csv", wTrayCh, fIdx+1);			
			strDownFileList.AddTail(strLocalFile);
		}
	}

	for(index = awFadmCh.GetSize()-1; index >=0; index--)
	{
		wStepNum = HIWORD(awFadmCh[index]);
		wModuleCh = LOWORD(awFadmCh[index]);		
		
		strFilter.Format("ch%03d_FadmData%02d.csv", wModuleCh, wStepNum);
		strServerFileList.AddTail(strFilter);
		strLocalFile.Format("FadmCH%03d_%02d.csv", wModuleCh, wStepNum);			
		strDownFileList.AddTail(strLocalFile);
	}
	
	if(DownloadDataFile(strRemote, strSaveDir , strServerFileList, strDownFileList, strIp, m_strID, m_strPassword))
	{
		start = GetTickCount()-start;
		strTemp.Format(TEXT_LANG[16], strSaveDir, start/1000.0f); //100076
		WriteLog(strTemp);
	}
	else
	{
		return FALSE;
	}

	m_downProgress.SetPos(100);
	m_downProgress.ShowWindow(SW_HIDE);

	return TRUE;
}

// 주어진 SerialNo의 실시간 저장 Data를 찾는다.
// 1. PC에서 주어진 test serial의 data를 찾는다.
// 2. PC에 존재하지 않을 경우 SBC에서 검색하여 존재하면 download 한다.
BOOL CDataDownDlg::GetRealTimeData(BOOL bLocal, CString strRemoteDir, CWordArray &awTraySelCh, CDWordArray &awFadmSelCh, CWordArray &awSensorSelCh, CString strIp, CString strSelPath)
{
	if( (awTraySelCh.GetSize() < 1 && awFadmSelCh.GetSize() < 1 && awSensorSelCh.GetSize() < 1 ) || strIp.IsEmpty())	
		return FALSE;

	if( m_bWork == TRUE )
	{
		AfxMessageBox(TEXT_LANG[59]); //100077
		return FALSE;
	}	
	m_bWork = TRUE;
	
	//////////////////////////////////////////////////////////////////////////
	//new version 1.1.0
	//down load from remote to local
	//Local에 없으면 원격에서 먼저 찾아서 download한다.
	CString strRawDataDir(strRemoteDir);					// Local일 경우는 Raw data 위치가 있음 
	if(bLocal == FALSE)
	{
		CDWordArray awDownCh;
		CDWordArray awFadmCh;
		CWordArray awSensorCh;
		
		if( m_bSearchModeOffLine )
		{
			//결과 파일명과 같은 이름의 폴더를 생성한다.
			CString strRltFile = m_resultData.GetFileName();
			strRawDataDir = strRltFile.Left(strRltFile.ReverseFind('.'));
			if(AddFolderList(strRawDataDir) == FALSE)
			{
				WriteLog(TEXT_LANG[17]+strRawDataDir);
				return  FALSE;
			}
			else
			{
				m_bWork = TRUE;
				if(m_bSearchModeOffLine)
				{
					//20061025 
					//사용자가 선택한 채널만 Download한다.
					//정상인 모든 채널 download한다.(나머지 Channel은 End Step이 전송되면 모두 DownLoad한다.)
					//User Mode일 경우 
					WORD wCh;
					for(int c =0; c<awTraySelCh.GetSize(); c++)
					{
						wCh = m_resultData.ConvertTrayCh2ModuleCh(awTraySelCh[c]);
						if(wCh > 0)
						{
							awDownCh.Add(MAKELONG(awTraySelCh[c], wCh));
						}
					}

					BOOL bSensorAllIncluded = FALSE;
					if(bSensorAllIncluded)
					{
						for(int index = 0; index < EP_MAX_SENSOR_CH; index++)
						{
							int nChNo = index+1;
							awSensorCh.Add((WORD)nChNo);
						}
					}
					else
					{
						GetUsedSensorCh(awTraySelCh, ALL_SENSOR, awSensorCh, &m_resultData);
					}
				}
				else
				{
					for(int c =0; c<awTraySelCh.GetSize(); c++)
					{
						awDownCh.Add(MAKELONG(awTraySelCh[c], c));
					}
				}

				//source dir(remote), dest dir(local), selected ch, ip
				if(DownLoadRemoteDataA(strIp, strRemoteDir, strRawDataDir, awDownCh, awSensorCh, awFadmSelCh) == FALSE)
				{					
					m_bWork = FALSE;
					return FALSE;
				}				
				
				m_bWork = FALSE;
				return true;
			}
		}
		else		//OnLine Mode 검색
		{
			for(int c =0; c<awTraySelCh.GetSize(); c++)
			{
				awDownCh.Add(MAKELONG(awTraySelCh[c], awTraySelCh[c]));
			}

			for(c =0; c<awFadmSelCh.GetSize(); c++)
			{
				awFadmCh.Add(awFadmSelCh[c]);
			}

			for(c =0; c<awSensorSelCh.GetSize(); c++)
			{
				awSensorCh.Add(awSensorSelCh[c]);
			}
			
			strRawDataDir = strSelPath;

			if(DownLoadRemoteDataA(strIp, strRemoteDir, strRawDataDir, awDownCh, awSensorCh, awFadmCh) == FALSE)
			{
				m_bWork = FALSE;
				return FALSE;
			}
			//OnLine Mode는 변환 저장하지 않는다.			
			m_bWork = FALSE;
			return TRUE;
		}		
	}

	//사용자 설정으로 변환 저장함 
	//source dir, dest dir, selected ch
	//파일이 local PC에 존재하면 해당 채널파일을 지정위치에 복사한다.

	if(((CButton *)GetDlgItem(IDC_RADIO1))->GetCheck() == BST_CHECKED)
	{
		//ch별 별도의 파일로 저장시 
		if(DownLoadLocalData(strRawDataDir, strSelPath, awTraySelCh) == FALSE)
		{
			WriteLog(strSelPath+TEXT_LANG[18]);
			m_bWork = FALSE;
			return FALSE;
		}
	}
	else
	{
		//모든 Channel Data를 1개의 파일로 묶음 
		//Added by KBH	20070313
		if(DownLoadLocalDataAll(strRawDataDir , strSelPath, awTraySelCh) == FALSE)
		{
			WriteLog(strSelPath+TEXT_LANG[18]);
			m_bWork = FALSE;
			return FALSE;
		}
	}
	//////////////////////////////////////////////////////////////////////////
	m_bWork = FALSE;
	return TRUE;
}

//local PC에서 저장 위치 검색 
CString CDataDownDlg::FindRealTimeDataFromLocal(CString strLocation, CString strTestSerial)
{
	//data temp 이하 폴더에서 주어진 serialno의 data 존재 여부 확인 	
	CFileFind aDirChecker;
	CString strTemp;
	CString strLocalDir;
	strTemp.Format("%s\\%s", strLocation, strTestSerial);
	
	if(aDirChecker.FindFile(strTemp) == TRUE)
	{
		aDirChecker.FindNextFile();
		if(aDirChecker.IsDirectory())
		{
			strLocalDir = aDirChecker.GetFilePath();
		}
	}
	aDirChecker.Close();
	return strLocalDir;
}

//remote에서 저장 data 검색 (해당 채널만 download 한다.)
CString CDataDownDlg::FindRealTimeDataFromRemote( CString strIp, int nGroupNo, CString strTestSerial, CStringList &aMdChannel, CStringList &aSensorCh, CStringList &aFdChannel )
{
	int nNo = nGroupNo;
	if(nNo < 1)	nNo = 1;

	CString strRemote;
	strRemote = GetRemoteLocation(strTestSerial, strIp, nNo);
	
	//해당 IP에 접속을 하지 못함
 	if(strRemote.IsEmpty())
 	{
		//WriteLog("Remote file location error!!");
		WriteLog(TEXT_LANG[19]);
 		return "";
 	}

	return GetRemoteFileList(strIp, strRemote, aMdChannel, aSensorCh, aFdChannel);
}

//remote에서 저장 data 검색 (해당 채널만 download 한다.)
CString CDataDownDlg::FindOnlineRealTimeDataFromRemote( CString strIp, int nGroupNo, CString strTestSerial, CStringList &aMdChannel, CStringList &aSensorCh, CStringList &aFdChannel )
{
	int nNo = nGroupNo;
	if(nNo < 1)	nNo = 1;
	
	CString strRemote;
	strRemote = GetRemoteLocation(strTestSerial, strIp, nNo, 1);
	
	//해당 IP에 접속을 하지 못함
	if(strRemote.IsEmpty())
	{
		//WriteLog("Remote file location error!!");
		WriteLog(TEXT_LANG[19]);
		return "";
	}
	
	return GetRemoteFileList(strIp, strRemote, aMdChannel, aSensorCh, aFdChannel);
}

CString CDataDownDlg::MakeMyFolder(CString strDir, BOOL bOverWrite)
{
	if(strDir.IsEmpty())	return "";
	
	//설치 폴더 밑에 data 저장 폴더을 생성한다.
	CFileFind aDirChecker;
	if(aDirChecker.FindFile(strDir) == FALSE)			//존재하지 않으면 
	{
		if(::CreateDirectory(strDir, NULL) == FALSE)	//무조건 생성 
		{
			return "";
		}
	}
	else
	{
		aDirChecker.FindNextFile();
		if(aDirChecker.IsDirectory() == FALSE)				//존재 하는데 Directory가 아닐 경우 
		{
			if(::CreateDirectory(strDir, NULL) == FALSE)	//생성한다.
			{
				return "";
			}
		}
		else
		{
			if(!bOverWrite)		//Overwrite가 아닐 경우는 사용자에게 다른 위치 선택하도록 한다.
			{
				int nRtn = AfxMessageBox(TEXT_LANG[60], MB_ICONQUESTION|MB_YESNOCANCEL); //100078
				if(nRtn == IDYES)
				{
					//폴더내 모든 Data 삭제 
					CFileFind aFF;
					BOOL bRtn = aFF.FindFile(strDir+"\\*.*");
					while(bRtn)
					{
						bRtn = aFF.FindNextFile();
						if(aFF.IsDirectory() == FALSE)
						{
							CString strName = aFF.GetFilePath();
							if(_unlink(strName) != 0)
							{
								AfxMessageBox(strName+TEXT_LANG[61]); //100079
								return "";
							}
						}
					}
				}
				else if(nRtn == IDNO)
				{
					CNameInputDlg dlg;
					CString strPath;
					strPath = strDir.Left(strDir.ReverseFind('\\'));
					dlg.m_strName = strDir.Mid(strDir.ReverseFind('\\')+1);
					//중복된 폴더가 존재합니다. 다른 이름을 입력하십시요.
					dlg.m_strTitle = TEXT_LANG[12]; //100080
					
					if(dlg.DoModal() == IDOK)
					{
						return MakeMyFolder(strPath+"\\"+dlg.m_strName , FALSE);
					}
					else 
					{
						return "";
					}
				}
				else
				{
					return "";
				}
			}
		}
	}
	aDirChecker.Close();
	return strDir;
}

//sbc에서 download한 날짜별 모니터링 파일을 
//1개의 excel 파일로 변환 저장한다.
BOOL CDataDownDlg::MakeExcelFile(CString strLocalFile, CStringList &aSourceFileList, CStringList &aSourctSensorFileList)
{
	if(aSourceFileList.GetCount() < 1)	return FALSE;

	CString strFileName, strSensorFileName;
	FILE *fp = fopen(strLocalFile, "wt");
	if(fp == NULL)		return FALSE;
	fprintf(fp, "Tot_t(sec),StepNo,Step_t(sec),V(mV),I(mA),C(mAh),W(mW),Wh(mWh),\n"); //100081
//	fprintf(fp, "StepNo,State,Tot_t(sec),Step_t(sec),V(mV),I(mA),C(mAh),Wh(mWh),\n");

	FILE *fpTemp;
	int nState, nCode, nStepNo, nTotalTime, nStepTime, nVoltage, nCurrent,  nWattHour, nCapacity;//, nGrade, nWatt, nImp;//, nTemp, nGas;
	
	POSITION pos1 = aSourceFileList.GetHeadPosition();
	while(pos1)
	{
		strFileName = aSourceFileList.GetNext(pos1);

		fpTemp = fopen(strFileName, "rt");
		if(fpTemp == NULL)	continue;

		// Save format.
		// channelstate, channelcode, gradecode, stepno, totaltime, steptime, vtg, crt, watt, watthour, capacity,z
/*		while(	fscanf(fpTemp, "%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d", 
						&nState, &nCode, &nGrade, &nStepNo, &nTotalTime, &nStepTime, &nVoltage, &nCurrent, &nWatt, &nWattHour, &nCapacity, &nImp) == 12)
		{
			fprintf(fp, "%.1f,%d,%.1f,%.2f,%.2f,%.2f,%.2f,%.2f\n", 
					MD2PCTIME(nTotalTime), nStepNo, MD2PCTIME(nStepTime), VTG_PRECISION(nVoltage), CRT_PRECISION(nCurrent), 
					ETC_PRECISION(nCapacity) , VTG_PRECISION(nVoltage)*CRT_PRECISION(nCurrent)/1000.0f, ETC_PRECISION(nWattHour));  	
		}		
*/				
		// channelstate, channelcode, stepno, totaltime, steptime, vtg, crt, watthour, capacity
		while(fscanf(fpTemp, "%d, %d, %d, %d, %d, %d, %d, %d, %d", 
						&nState, &nCode, &nStepNo, &nTotalTime, &nStepTime, &nVoltage, &nCurrent, &nWattHour, &nCapacity) == 9)
		{
			fprintf(fp, "%.1f,%d,%.1f,%.2f,%.2f,%.2f,%.2f,%.2f\n", 
					MD2PCTIME(nTotalTime), nStepNo, MD2PCTIME(nStepTime), VTG_PRECISION(nVoltage), CRT_PRECISION(nCurrent), 
					ETC_PRECISION(nCapacity) , VTG_PRECISION(nVoltage)*CRT_PRECISION(nCurrent)/1000.0f, ETC_PRECISION(nWattHour));  	
		}

		fclose(fpTemp);

		//이미 download된 파일을 Check하기 위해 Download한 파일을 삭제 하지 않는다.
//		_unlink(strFileName);	
	}
	fclose(fp);
	return TRUE;
}

//strDir 위치에서 strDataDir 위치로 awSelch에서 선택한 채널을 지정옵션으로 저장한다.
BOOL CDataDownDlg::DownLoadLocalData(CString strDir, CString strSaveDir, CWordArray &awSelTrayCh)
{
	int nChNo;
	CString strTemp;
	CString strSourceFile, strDestFile;
	
	CString strSensorSourceFile1, strSensorSourceFile2;
	strSensorSourceFile1.Format("%s\\SensorT.csv", strDir);
	strSensorSourceFile2.Format("%s\\SensorV.csv", strDir);

	//channel과 matching되는 sensor channel만을 추출하여 저장해야 한다.

	CWaitCursor wait;
	m_downProgress.ShowWindow(SW_SHOW);
	m_downProgress.SetPos(0);

	CTestCondition *pCondition = m_resultData.GetTestCondition();
	CStep *pStep, *pStep2;
	char szBuff[128];
	CString strSensorData1, strSensorData2, strFileBuff;

	FILE *fpSource, *fpDest;
	CStdioFile	sensorFile1, sensorFile2;

	int nRowCount = 0;
	int nFileCount = 0;
	CString strHeaderString;
	CWordArray awMapSensorCh1, awMapSensorCh2;

	COleDateTime starTime;
	starTime.ParseDateTime(m_resultData.GetResultHeader()->szDateTime);
	TRACE("Start Time : %s\n", starTime.Format());

	for(int i=0; i<awSelTrayCh.GetSize(); i++)
	{
		m_downProgress.SetPos(i*100/awSelTrayCh.GetSize());

		nRowCount = 0;
		nFileCount = 0;
		nChNo = awSelTrayCh.GetAt(i);	//Tray Based Ch No

		//strSourceFile.Format("%s\\ch%03d.csv", strDir, nChNo);	
		strDestFile.Format("%s\\ch%03d.csv", strSaveDir, nChNo);	
	
		m_resultData.FindMappingSensorCh(nChNo, awMapSensorCh1, 0);		//0개 or 1개 or 여러개가 Mapping될 수 있다.
		m_resultData.FindMappingSensorCh(nChNo, awMapSensorCh2, 1);		//0개 or 1개 or 여러개가 Mapping될 수 있다.
	
		BOOL bSensorFileRead1 = sensorFile1.Open(strSensorSourceFile1, CFile::modeRead);
		BOOL bSensorFileRead2 = sensorFile2.Open(strSensorSourceFile2, CFile::modeRead);
		
		fpDest = fopen(strDestFile, "wt");
		strHeaderString.Empty();
		if(fpDest == NULL)		continue;


		//온도 Sesnor Header read
		if(bSensorFileRead1)
		{	//read header
			sensorFile1.ReadString(strSensorData1);
			FindDataColumn( awMapSensorCh1, strSensorData1, 0, &m_resultData);
		}
		if(bSensorFileRead2)
		{
			//read header
			sensorFile2.ReadString(strSensorData2);
			FindDataColumn( awMapSensorCh2, strSensorData2, 1, &m_resultData);
		}

		//Make Header String
		if(m_SaveSetDlg.m_bSaveDate)
		{
			strHeaderString += "Date,";
		}

		//////////////////////////////////////////////////////////////////////////
		//Write Column Header
		if(m_SaveSetDlg.m_bTimeSum)
		{
			strTemp = m_SaveSetDlg.UnitString(EP_TOT_TIME);
			if(strTemp.IsEmpty())
			{
				strFileBuff = "TotTime,";
			}
			else
			{
				strFileBuff.Format("TotTime(%s),",	strTemp);
			}
			strHeaderString += strFileBuff;
		}
		if(m_SaveSetDlg.m_bStepNo)
		{
			strHeaderString += "StepNo,"; 
		}
		if(m_SaveSetDlg.m_bStepType)
		{
			strHeaderString += "Type,";
		}
		if(m_SaveSetDlg.m_bStepTime)
		{
			strTemp = m_SaveSetDlg.UnitString(EP_STEP_TIME);
			if(strTemp.IsEmpty())
			{
				strFileBuff = "Time,";
			}
			else
			{
				strFileBuff.Format("Time(%s),",	strTemp);
			}
			strHeaderString += strFileBuff;
		}

		if(m_SaveSetDlg.m_bVoltage)	
		{
			strFileBuff.Format("Volt(%s),",	m_SaveSetDlg.UnitString(EP_VOLTAGE));
			strHeaderString += strFileBuff;
		}
		if(m_SaveSetDlg.m_bCurrent)
		{
			strFileBuff.Format("Crt(%s),",		m_SaveSetDlg.UnitString(EP_CURRENT));
			strHeaderString += strFileBuff;
		}
		if(m_SaveSetDlg.m_bCapacity)
		{
			strFileBuff.Format("Capa(%s),",	m_SaveSetDlg.UnitString(EP_CAPACITY));
			strHeaderString += strFileBuff;
		}
		if(m_SaveSetDlg.m_bWatt)
		{
			strFileBuff.Format("Watt(%s),",	m_SaveSetDlg.UnitString(EP_WATT));
			strHeaderString += strFileBuff;
		}
		if(m_SaveSetDlg.m_bWattHour)
		{
			strFileBuff.Format("WattHour(%s),", m_SaveSetDlg.UnitString(EP_WATT_HOUR));
			strHeaderString += strFileBuff;
		}
		if(bSensorFileRead1 && m_SaveSetDlg.m_bTempData && awMapSensorCh1.GetSize() > 0)	
		{
			strFileBuff.Format( "%s,", strSensorData1);
			strHeaderString += strFileBuff;
		}
		if(bSensorFileRead2 && m_SaveSetDlg.m_bAuxVData && awMapSensorCh2.GetSize() > 0)
		{
			strFileBuff.Format( "%s,", strSensorData2);
			strHeaderString += strFileBuff;
		}

		fprintf(fpDest, "%s\n", strHeaderString);
		nRowCount++;		//Write Header name
		//////////////////////////////////////////////////////////////////////////

		//여러개의 파일이 있을 경우 모두 읽어 들임
		//V2.0 기본적으로 SBC에서 MAX_EXCEL_ROW크기로 파일을 분리하여 저장한다.
		int nCurStep = 1, nStep = 0, nLastWriteEndStep = 0, nState;
		float fStepTime =0.0f, fDay;
		float fTime, fVoltage, fCurrent, fCapacity, fWatt, fWattHour;//, fSaveLastTime = 0.0f;
		float fCurTime = 0.0f, fCurVoltage = 0.0f, fCurCurrent = 0.0f, fTimeSum = 0.0f, fStepTotTime = 0.0f, fTimeScan = 0.0f;
		CString strTime, strVoltage, strCurrent, strCapacity, strWatt, strWattHour;
		BOOL	bReadSensorData1 = FALSE;
		BOOL	bReadSensorData2 = FALSE;
		STR_STEP_RESULT *pStepData = NULL;
		STR_SAVE_CH_DATA *pStepResult = NULL;
		CFileFind aFinder;

		for(int nFileIndex = 0; nFileIndex < 99; nFileIndex++)
		{
			strSourceFile.Format("%s\\CH%03d_%02d.csv", strDir, nChNo, nFileIndex+1);
			BOOL bFind = aFinder.FindFile(strSourceFile);
			
			if(bFind == FALSE)	break;


			//////////////////////////////////////////////////////////////////////////
			//Data copy start
			fpSource = fopen(strSourceFile, "rt");
			if(fpSource == NULL)	break;


			//StepNo,t(sec),V(mV),I(mA),C(mAh),W(mW),Wh(mWh)
			//column header read //Skip Column Header
			fscanf(fpSource, "%s,%s,%s,%s,%s,%s,%s,%s,", szBuff, szBuff, szBuff, szBuff, szBuff, szBuff, szBuff, szBuff);
			

			//파일을 한줄씩 읽어 들임(1초 간격으로 저장된 파일이여야 함)
//			while(fscanf(fpSource, "%f,%d,%f,%f,%f,%f,%f,%f,", &fTimeSum, &nStep, &fTime, &fVoltage, &fCurrent, &fCapacity, &fWatt, &fWattHour) == 8)
			//V 1.3
			//	fprintf(fp, "StepNo,State,Tot_t(sec),Step_t(sec),V(mV),I(mA),C(mAh),Wh(mWh),\n");
			while(fscanf(fpSource, "%d,%d,%f,%f,%f,%f,%f,%f,", &nStep, &nState, &fTimeSum,  &fTime, &fVoltage, &fCurrent, &fCapacity, &fWattHour) == 8)
			{
				fWatt = fVoltage*fCurrent/1000.0f;
				if(bSensorFileRead1)	
				{
					//Sensor data도 한줄씩 읽어 들임
					bReadSensorData1 = sensorFile1.ReadString(strSensorData1);
				}
				if(bSensorFileRead2)	
				{
					//Sensor data도 한줄씩 읽어 들임
					bReadSensorData2 = sensorFile2.ReadString(strSensorData2);
				}
			
				pStep = pCondition->GetStep(nStep-1);
				//이상한 data or step결과가 없음 //Skip the check step
				if(pStep == NULL)	continue;

				//이전 Step의 최종값 저장 
				for(int nS = nCurStep; nS <= nStep; nS++)
				{
					pStepData = m_resultData.GetStepData(nS-1);
					pStep2 = pCondition->GetStep(nS-1);
					if(pStepData)
					{
						//최종 결과값 저장 
						pStepResult = (STR_SAVE_CH_DATA *)pStepData->aChData[nChNo-1];					
						if(fTimeScan < pStepResult->fTotalTime && pStepResult->fTotalTime <= fTimeSum)
						{
							nLastWriteEndStep = pStepResult->nStepNo;
							//if(m_SaveSetDlg.m_bSaveDate)	fprintf(fpDest, "!%s,", pStepData->stepHead.szEndDateTime);
							//V2.0 시간이 역전되어 표기되므로 Start time + Total run time으로 표시 함(약간의 시간 오차 있음) 
							fDay = fTimeSum/(3600.0f*24.0f); 
							COleDateTimeSpan stepSpanE(fDay);
							COleDateTime testDateE = starTime + stepSpanE;
							if(m_SaveSetDlg.m_bSaveDate)	fprintf(fpDest, "!%s,", testDateE.Format());
							if(m_SaveSetDlg.m_bTimeSum)		fprintf(fpDest, "%s,",	m_SaveSetDlg.ValueString(fTimeSum, EP_TOT_TIME));
							if(m_SaveSetDlg.m_bStepNo)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->nStepNo, EP_STEP_NO));
							if(m_SaveSetDlg.m_bStepType)	fprintf(fpDest, "%s,", ::StepTypeMsg(pStep2->m_type));
							if(m_SaveSetDlg.m_bStepTime)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fStepTime, EP_STEP_TIME));
							if(m_SaveSetDlg.m_bVoltage)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fVoltage, EP_VOLTAGE));
							if(m_SaveSetDlg.m_bCurrent)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fCurrent, EP_CURRENT));
							if(m_SaveSetDlg.m_bCapacity)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fCapacity, EP_CAPACITY));
							if(m_SaveSetDlg.m_bWatt)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fWatt, EP_WATT));
							if(m_SaveSetDlg.m_bWattHour)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fWattHour, EP_WATT_HOUR));
							if(bSensorFileRead1 && bReadSensorData1 && m_SaveSetDlg.m_bTempData && awMapSensorCh1.GetSize() > 0)
								fprintf(fpDest, "%s,", ConvertDataString(awMapSensorCh1, strSensorData1));
							if(bSensorFileRead2 && bReadSensorData2 && m_SaveSetDlg.m_bAuxVData && awMapSensorCh2.GetSize() > 0)
								fprintf(fpDest, "%s,", ConvertDataString(awMapSensorCh2,strSensorData2));
							
							fprintf(fpDest, "\n");
							//////////////////////////////////////////////////////////////////////////
							nRowCount++;
							if(nRowCount >= MAX_EXCEL_ROW)
							{
								if(MakeNextFile(fpDest, nFileCount, strDestFile) == FALSE)
								{
									strTemp.Format(TEXT_LANG[62], strDestFile); //100082
									AfxMessageBox(strTemp, MB_ICONSTOP|MB_OK);
									fclose(fpSource);
									return FALSE;
								}
								
								ASSERT(fpDest);
								fprintf(fpDest, "%s\n", strHeaderString);//column header 기록
								nRowCount = 1;		//Header 기록 
							}
							//////////////////////////////////////////////////////////////////////////

							fTimeScan = fTimeSum;
							fStepTime = pStepResult->fStepTime;
						}
					}
				}
									
				//같은 Step에 시간이 같은 Data가 존재할 경우 제외한다.
				//if(nCurStep == nStep && fTimeScan == fTimeSum)	continue;				
				if(fTimeScan == fTimeSum && fStepTime == fTime)	continue;
											
				//현재 총시간은 이전 step의 총합 + 현재 Step 시간 
				nCurStep  = nStep;
				//현재 Step의 최종 시간 Update
				fTimeScan = fTimeSum;
				fStepTime = fTime;

				//Step type check
				if(pStep->m_type == EP_TYPE_CHARGE && m_SaveSetDlg.m_bSaveCharge == FALSE)			continue;
				if(pStep->m_type == EP_TYPE_DISCHARGE && m_SaveSetDlg.m_bSaveDischarge == FALSE)	continue;
				if(pStep->m_type == EP_TYPE_REST && m_SaveSetDlg.m_bSaveRest == FALSE)				continue;
				if(pStep->m_type == EP_TYPE_IMPEDANCE && m_SaveSetDlg.m_bSaveImpdeacne == FALSE)	continue;
				if(pStep->m_type == EP_TYPE_END)	continue;										//완료 Step이나 
				if(pStep->m_type == EP_TYPE_OCV)	continue;										//OCV data는 실처리에서 가져오지 않고 step결과 data로 기록한다.

				fDay = fTimeSum/(3600.0f*24.0f); 
				COleDateTimeSpan stepSpan(fDay);
				COleDateTime testDate = starTime + stepSpan;
				TRACE("%s(%s)\n", testDate.Format(), stepSpan.Format("%H:%M:%S"));
				if(m_SaveSetDlg.m_bSaveDate)	fprintf(fpDest, "%s,", testDate.Format());
				if(m_SaveSetDlg.m_bTimeSum)		fprintf(fpDest, "%s,",	m_SaveSetDlg.ValueString(fTimeSum, EP_TOT_TIME));
				if(m_SaveSetDlg.m_bStepNo)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(nStep, EP_STEP_NO));
				if(m_SaveSetDlg.m_bStepType)	fprintf(fpDest, "%s,", ::StepTypeMsg(pStep->m_type));
				if(m_SaveSetDlg.m_bStepTime)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fTime, EP_STEP_TIME));
				if(m_SaveSetDlg.m_bVoltage)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fVoltage, EP_VOLTAGE));
				if(m_SaveSetDlg.m_bCurrent)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fCurrent, EP_CURRENT));
				if(m_SaveSetDlg.m_bCapacity)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fCapacity, EP_CAPACITY));
				if(m_SaveSetDlg.m_bWatt)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fWatt, EP_WATT));
				if(m_SaveSetDlg.m_bWattHour)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fWattHour, EP_WATT_HOUR));
				if(bSensorFileRead1 && bReadSensorData1 && m_SaveSetDlg.m_bTempData && awMapSensorCh1.GetSize() > 0)
					fprintf(fpDest, "%s,", ConvertDataString(awMapSensorCh1, strSensorData1));
				if(bSensorFileRead2 && bReadSensorData2 && m_SaveSetDlg.m_bAuxVData && awMapSensorCh2.GetSize() > 0)
					fprintf(fpDest, "%s,", ConvertDataString(awMapSensorCh2,strSensorData2));

				fprintf(fpDest, "\n");
				
				//////////////////////////////////////////////////////////////////////////
				nRowCount++;
				if(nRowCount >= MAX_EXCEL_ROW)
				{
					if(MakeNextFile(fpDest, nFileCount, strDestFile) == FALSE)
					{
						strTemp.Format("File create fail.(%s)", strDestFile);
						AfxMessageBox(strTemp, MB_ICONSTOP|MB_OK);
						fclose(fpSource);
						//if(bSensorFileRead1)		sensorFile1.Close();
						//if(bSensorFileRead2)		sensorFile2.Close();
						return FALSE;
					}
						
					ASSERT(fpDest);
					fprintf(fpDest, "%s\n", strHeaderString);//column header 기록
					nRowCount = 1;		//Header 기록 
				}
				//////////////////////////////////////////////////////////////////////////

				//fCurTime = fTime;
				//fCurTime = fCurTime + m_SaveSetDlg.m_nDeltaTime;
				//fSaveLastTime = fTimeSum;	
			}

			//이전 Step의 최종값 저장 
			STR_STEP_RESULT *pLastData = m_resultData.GetLastStepData();
			if(pLastData)
			{
				for(int nS = nLastWriteEndStep+1; nS < pLastData->stepCondition.stepHeader.stepIndex+1; nS++)
				{
					pStepData = m_resultData.GetStepData(nS-1);
					pStep2 = pCondition->GetStep(nS-1);
					if(pStepData)
					{
						//최종 결과값 저장 
						pStepResult = (STR_SAVE_CH_DATA *)pStepData->aChData[nChNo-1];					
						//저장한 최종 시간과 중복되는 시간이면 ??
						//fTimeScan += pStepResult->fStepTime;
						fDay = pStepResult->fTotalTime/(3600.0f*24.0f); 
						COleDateTimeSpan stepSpan1(fDay);
						COleDateTime testDate1 = starTime + stepSpan1;

						if(m_SaveSetDlg.m_bSaveDate)	fprintf(fpDest, "!%s,", testDate1.Format());
						if(m_SaveSetDlg.m_bTimeSum)		fprintf(fpDest, "%s,",	m_SaveSetDlg.ValueString(pStepResult->fTotalTime, EP_TOT_TIME));
						if(m_SaveSetDlg.m_bStepNo)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->nStepNo, EP_STEP_NO));
						if(m_SaveSetDlg.m_bStepType)	fprintf(fpDest, "%s,", ::StepTypeMsg(pStep2->m_type));
						if(m_SaveSetDlg.m_bStepTime)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fStepTime, EP_STEP_TIME));
						if(m_SaveSetDlg.m_bVoltage)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fVoltage, EP_VOLTAGE));
						if(m_SaveSetDlg.m_bCurrent)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fCurrent, EP_CURRENT));
						if(m_SaveSetDlg.m_bCapacity)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fCapacity, EP_CAPACITY));
						if(m_SaveSetDlg.m_bWatt)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fWatt, EP_WATT));
						if(m_SaveSetDlg.m_bWattHour)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fWattHour, EP_WATT_HOUR));
						if(bSensorFileRead1 && bReadSensorData1 && m_SaveSetDlg.m_bTempData && awMapSensorCh1.GetSize() > 0)
							fprintf(fpDest, "%s,", ConvertDataString(awMapSensorCh1, strSensorData1));
						if(bSensorFileRead2 && bReadSensorData2 && m_SaveSetDlg.m_bAuxVData && awMapSensorCh2.GetSize() > 0)
							fprintf(fpDest, "%s,", ConvertDataString(awMapSensorCh2,strSensorData2));

						fprintf(fpDest, "\n");

						//////////////////////////////////////////////////////////////////////////
						nRowCount++;
						if(nRowCount >= MAX_EXCEL_ROW)
						{
							if(MakeNextFile(fpDest, nFileCount, strDestFile) == FALSE)
							{
								strTemp.Format("File create fail.(%s).", strDestFile);
								AfxMessageBox(strTemp, MB_ICONSTOP|MB_OK);
								fclose(fpSource);
								return FALSE;
							}
							
							ASSERT(fpDest);
							fprintf(fpDest, "%s\n", strHeaderString);//column header 기록
							nRowCount = 1;		//Header 기록 
						}
					}
				}
			}
			if(fpSource)	fclose(fpSource);
		}
		//////////////////////////////////////////////////////////////////////////
		if(fpDest)	fclose(fpDest);
	}
	m_downProgress.SetPos(100);
	m_downProgress.ShowWindow(SW_HIDE);

	GetDlgItem(IDC_STATIC_SEL_CH)->SetWindowText("Data save complited");
	
	return TRUE;
}

/*
//strDir 위치에서 strDataDir 위치로 awSelch에서 선택한 채널을 지정옵션으로 저장한다.
BOOL CDataDownDlg::DownLoadLocalDataAll(CString strDir, CString strSaveDir, CWordArray &awSelTrayCh)
{
	CString strSourceFile, strDestFile;

	int nSelChCount = awSelTrayCh.GetSize();
	int nColumnCount = 0;
	int nRowCount = 0, nTotRow = 0;
	int nFileCount = 0;
	CString	strVtgBufArray[STR_BUFF_SIZE];
	CString	strCrtBufArray[STR_BUFF_SIZE];
	CString	strCapBufArray[STR_BUFF_SIZE];
	CString	strWattBufArray[STR_BUFF_SIZE];
	CString	strWattHBufArray[STR_BUFF_SIZE];
	CString strVtgHeaderString;
	CString strCrtHeaderString;
	CString strCapHeaderString;
	CString strWattHeaderString;
	CString strWattHourHeaderString;
	CString strHeaderString;
	CString strData, strTemp, strProgText;
	char szBuff[128];
	
	CTestCondition *pCondition = m_resultData.GetTestCondition();
	CStep *pStep;
	if(nSelChCount < 1 || strSaveDir.IsEmpty() || strDir.IsEmpty())		return FALSE;

	//source file open and make column header
	FILE **fpSrcFile = new FILE*[nSelChCount];
	for(int i=0; i<nSelChCount; i++)
	{
		//Source file open
		strSourceFile.Format("%s\\ch%03d.csv", strDir, awSelTrayCh[i]);
		fpSrcFile[i] = fopen(strSourceFile, "rt");
		if(fpSrcFile[i])
		{
			//Make column header
			if(strHeaderString.IsEmpty())
			{
				if(m_SaveSetDlg.m_bSaveDate)
				{
					strHeaderString += "Date,";
					nColumnCount++;
				}
				if(m_SaveSetDlg.m_bTimeSum)		
				{
					strData = m_SaveSetDlg.UnitString(EP_TOT_TIME);
					if(strTemp.IsEmpty())
					{
						strTemp = "TotTime,";
					}
					else
					{
						strTemp.Format("TotTime(%s),",	strData);
					}
					strHeaderString += strTemp;
					nColumnCount++;
				}
				if(m_SaveSetDlg.m_bStepNo)
				{
					strHeaderString += "StepNo,";
					nColumnCount++;
				}
				if(m_SaveSetDlg.m_bStepType)
				{
					strHeaderString += "Type,";
					nColumnCount++;
				}
				if(m_SaveSetDlg.m_bStepTime)
				{
					strData = m_SaveSetDlg.UnitString(EP_STEP_TIME);
					if(strData.IsEmpty())
					{
						strTemp = "Time,";
					}
					else
					{
						strTemp.Format("Time(%s),",	strData);
					}
					strHeaderString += strTemp;
					nColumnCount++;
				}
			}

//			fscanf(fpSrcFile[i], "%s,%s,%s,%s,%s,%s,%s,%s,", szBuff, szBuff, szBuff, szBuff, szBuff, szBuff, szBuff, szBuff);
			fscanf(fpSrcFile[i], "%s,", szBuff);
			
			sprintf(szBuff, "C%d_V(%s),", awSelTrayCh[i], m_SaveSetDlg.UnitString(EP_VOLTAGE));
			strVtgHeaderString += szBuff;
			
			sprintf(szBuff, "C%d_I(%s),", awSelTrayCh[i], m_SaveSetDlg.UnitString(EP_CURRENT));
			strCrtHeaderString += szBuff;
			
			sprintf(szBuff, "C%d_C(%s),", awSelTrayCh[i], m_SaveSetDlg.UnitString(EP_CAPACITY));
			strCapHeaderString += szBuff;
			
			sprintf(szBuff, "C%d_W(%s),", awSelTrayCh[i], m_SaveSetDlg.UnitString(EP_WATT));
			strWattHeaderString += szBuff;
			
			sprintf(szBuff, "C%d_WH(%s),", awSelTrayCh[i], m_SaveSetDlg.UnitString(EP_WATT_HOUR));
			strWattHourHeaderString += szBuff;
		}
	}

	if(m_SaveSetDlg.m_bVoltage)
	{
		strHeaderString += strVtgHeaderString;
		nColumnCount += nSelChCount;
	}
	if(m_SaveSetDlg.m_bCurrent)
	{
		strHeaderString += strCrtHeaderString;
		nColumnCount += nSelChCount;
	}
	if(m_SaveSetDlg.m_bCapacity)
	{
		strHeaderString += strCapHeaderString;
		nColumnCount += nSelChCount;
	}
	if(m_SaveSetDlg.m_bWatt)
	{
		strHeaderString += strWattHeaderString;
		nColumnCount += nSelChCount;
	}
	if(m_SaveSetDlg.m_bWattHour)
	{
		strHeaderString += strWattHourHeaderString;
		nColumnCount += nSelChCount;
	}

	//Excel column over-flow
	if(nColumnCount > 256)
	{
		for(int j=0; j<nSelChCount; j++)
		{
			if(fpSrcFile[j])
			{
				fclose(fpSrcFile[j]);	
				fpSrcFile[j] = NULL;
			}
		}
		delete [] fpSrcFile;
		strTemp.Format("선택된 channel수나 item 수가 256개를 초과하였습니다. 저장 항목을 줄이거나 선택 채널을 줄이신 후 재시도 하십시요");
		AfxMessageBox(strTemp);
		return FALSE;
	}

	CWaitCursor wait;
	m_downProgress.ShowWindow(SW_SHOW);
	m_downProgress.SetPos(0);

	strDestFile = strSaveDir;
	FILE *fpDest = fopen(strDestFile, "wt");
	if(fpDest)
	{
		//Write Test Header
		RESULT_FILE_HEADER *pHeader = m_resultData.GetResultHeader();
		fprintf(fpDest, "파일명:, %s\n", (char *)(LPCTSTR)m_resultData.GetFileName());	// FileName
		fprintf(fpDest, "모델명:,%s\n",  m_resultData.GetModelName());			// 모델명 	
		fprintf(fpDest, "공정_조건명:,%s\n",  m_resultData.GetTestName());		// 시험조건명
		fprintf(fpDest, "Tray_No:, %s\n",  pHeader->szTrayNo);		// Tray No
		fprintf(fpDest, "Lot_No:, %s\n",  pHeader->szLotNo);			// Lot No
		fprintf(fpDest, "모듈No:, 모듈 %d-%d\n",  pHeader->nModuleID, pHeader->wGroupIndex+1);					// Module name 
		fprintf(fpDest, "시작_시각:, %s\n",  pHeader->szDateTime);	// 시작시간
		fprintf(fpDest, "운영자:, %s\n\n",  pHeader->szOperatorID);					// 총시간

		fprintf(fpDest, "%s\n", strHeaderString);	
		nRowCount++;
	}

	//test stared time
	COleDateTime starTime;
	starTime.ParseDateTime(m_resultData.GetResultHeader()->szDateTime);

#ifdef _DEBUG
	DWORD start = GetTickCount();
	DWORD start1 = start;
#endif

	int nBufferRepeatCnt = 0;
	while(1 && fpDest)
	{
		//시간이 오래 걸림 
		if(strProgText.IsEmpty())			strProgText.Format("Data 저장 진행중(%d).", ++nBufferRepeatCnt);
		else								strProgText += ".";
		GetDlgItem(IDC_STATIC_SEL_CH)->SetWindowText(strProgText);
		if(strProgText.GetLength() > 50)	strProgText.Empty();

		int nPrevDataCnt = 0;
		int nBufCnt = 0;
		int nProgressCnt = 0;
		
		for(int i=0; i<nSelChCount; i++)
		{
			if(fpSrcFile[i])
			{
				int nStep;
				float fTimeSum, fTime, fVoltage, fCurrent, fCapacity, fWatt, fWattHour, fTemp;

				nBufCnt = 0;
				//파일을 한줄씩 읽어 들임(1초 간격으로 저장된 파일이여야 함)
				while(fscanf(fpSrcFile[i], "%f,%d,%f,%f,%f,%f,%f,%f,", &fTimeSum, &nStep, &fTime, &fVoltage, &fCurrent, &fCapacity, &fWatt, &fWattHour) == 8 && nBufCnt<STR_BUFF_SIZE)
//				while(fscanf(fpSrcFile[i], "%d,%f,%f,%f,%f,%f,%f,", &nStep, &fTime, &fVoltage, &fCurrent, &fCapacity, &fWatt, &fWattHour) == 7 && nBufCnt<STR_BUFF_SIZE)
				{
					m_downProgress.SetPos(int(float(nProgressCnt++*100.0f)/float(nSelChCount*STR_BUFF_SIZE)));
					
					pStep = pCondition->GetStep(nStep-1);
					//이상한 data or step결과가 없음 //Skip the check step
					if(pStep == NULL)	continue;
					if(pStep->m_type == EP_TYPE_CHARGE && m_SaveSetDlg.m_bSaveCharge == FALSE)			continue;
					if(pStep->m_type == EP_TYPE_DISCHARGE && m_SaveSetDlg.m_bSaveDischarge == FALSE)	continue;
					if(pStep->m_type == EP_TYPE_REST && m_SaveSetDlg.m_bSaveRest == FALSE)				continue;
					if(pStep->m_type == EP_TYPE_IMPEDANCE && m_SaveSetDlg.m_bSaveImpdeacne == FALSE)	continue;
					//if(pStep->m_type == EP_TYPE_END)		continue;		 
					//if(pStep->m_type == EP_TYPE_OCV)		continue;		
					
					fTemp = fTimeSum/(3600.0f*24.0f); 
					COleDateTimeSpan stepSpan(fTemp);
					COleDateTime testDate = starTime + stepSpan;
					TRACE("%s\n", starTime.Format());

					if(strVtgBufArray[nBufCnt].IsEmpty())
					{
						//Save date time
						if(m_SaveSetDlg.m_bSaveDate)
						{
							strData.Format("%s,", testDate.Format());
							strVtgBufArray[nBufCnt] += strData;
						}
						//total time
						if(m_SaveSetDlg.m_bTimeSum)		
						{
							strData.Format("%s,", m_SaveSetDlg.ValueString(fTimeSum, EP_TOT_TIME));
							strVtgBufArray[nBufCnt] += strData;
						}
						//Step No
						if(m_SaveSetDlg.m_bStepNo)
						{
							strData.Format("%s,", m_SaveSetDlg.ValueString(nStep, EP_STEP_NO));
							strVtgBufArray[nBufCnt] += strData;
						}
						//type
						if(m_SaveSetDlg.m_bStepType)
						{
							strData.Format("%s,", ::StepTypeMsg(pStep->m_type));
							strVtgBufArray[nBufCnt] += strData;
						}
						//Step Time
						if(m_SaveSetDlg.m_bStepTime)
						{
							strData.Format("%s,", m_SaveSetDlg.ValueString(fTime, EP_STEP_TIME));
							strVtgBufArray[nBufCnt] += strData;
						}
					}

					if(m_SaveSetDlg.m_bVoltage)
					{
						//Voltage
						sprintf(szBuff, "%s,", m_SaveSetDlg.ValueString(fVoltage, EP_VOLTAGE));
						strVtgBufArray[nBufCnt] = strVtgBufArray[nBufCnt]+szBuff;
						//strcat(szVtgBufArray[nBufCnt], szBuff);
					}
					if(m_SaveSetDlg.m_bCurrent)
					{
						//Current
						sprintf(szBuff, "%s,", m_SaveSetDlg.ValueString(fCurrent, EP_CURRENT));
						strCrtBufArray[nBufCnt] = strCrtBufArray[nBufCnt]+szBuff;
						//strcat(szCrtBufArray[nBufCnt], szBuff);
					}
					if(m_SaveSetDlg.m_bCapacity)
					{
						//Capacity
						sprintf(szBuff, "%s,", m_SaveSetDlg.ValueString(fCapacity, EP_CAPACITY));
						strCapBufArray[nBufCnt] = strCapBufArray[nBufCnt]+szBuff;
						//strcat(szCapBufArray[nBufCnt], szBuff);
					}
					if(m_SaveSetDlg.m_bWatt)
					{
						//Watt
						sprintf(szBuff, "%s,", m_SaveSetDlg.ValueString(fWatt, EP_WATT));
						strWattBufArray[nBufCnt] = strWattBufArray[nBufCnt]+szBuff;
					}
					if(m_SaveSetDlg.m_bWattHour)
					{
						//Watt hour
						sprintf(szBuff, "%s,", m_SaveSetDlg.ValueString(fWattHour, EP_WATT_HOUR));
						strWattHBufArray[nBufCnt] = strWattHBufArray[nBufCnt]+szBuff;
					}

					nBufCnt++;
					
					//file 별로 record 수가 다르게 저장되어 있을 가장 첫번째 record file 수만큼 만 저장한다.
					//if(i != 0 && nPrevDataCnt <= nBufCnt)
					//{
					//	break;
					//}
				}
			}
			nPrevDataCnt = nBufCnt;	//이전 channel에서 저장한 record 수 
		}
		
		//Write data to file
		for(int s=0; s<nBufCnt; s++)
		{
			//make next excel file : FILE_NAME(1).csv
			if(nRowCount >= MAX_EXCEL_ROW)
			{
				if(MakeNextFile(fpDest, nFileCount, strDestFile) == FALSE)
				{
					strData.Format("%s 파일을 생성할 수 없습니다.", strDestFile);
					AfxMessageBox(strData, MB_ICONSTOP|MB_OK);
					goto _FAIL_EXIT;
				}
					
				ASSERT(fpDest);
				fprintf(fpDest, "%s\n", strHeaderString);	//column header 기록
				nRowCount = 1;								//Header 기록 
			}
			strData.Empty();
			if(m_SaveSetDlg.m_bVoltage)
			{
				strData += strVtgBufArray[s];
				//strData += szVtgBufArray[s];
			}
			if(m_SaveSetDlg.m_bCurrent)
			{
				strData += strCrtBufArray[s];
				//strData += szCrtBufArray[s];
			}
			if(m_SaveSetDlg.m_bCapacity)
			{
				strData += strCapBufArray[s];
				//strData += szCapBufArray[s];
			}
			if(m_SaveSetDlg.m_bWatt)
			{
				strData += strWattBufArray[s];
			}
			if(m_SaveSetDlg.m_bWattHour)
			{
				strData += strWattHBufArray[s];
			}
			fprintf(fpDest, "%s\n", strData);
			nRowCount++;
			
			strVtgBufArray[s].Empty();
			strCrtBufArray[s].Empty();
			strCapBufArray[s].Empty();
// 			ZeroMemory(szVtgBufArray, sizeof(szVtgBufArray));
// 			ZeroMemory(szCrtBufArray, sizeof(szCrtBufArray));
// 			ZeroMemory(szCapBufArray, sizeof(szCapBufArray));
			strWattBufArray[s].Empty();
			strWattHBufArray[s].Empty();
		}

#ifdef _DEBUG
		DWORD end = GetTickCount();
		TRACE("=================>%dmsec(%d)\n", end -start, end - start1);
		start1 = GetTickCount();
#endif

		//file read scan 완료
		if(nBufCnt < STR_BUFF_SIZE)
		{
			break;
		}
	}

	GetDlgItem(IDC_STATIC_SEL_CH)->SetWindowText("Data 저장 완료");

_FAIL_EXIT:

	//close source channel file
	for(int j=0; j<nSelChCount; j++)
	{
		if(fpSrcFile[j])
		{
			fclose(fpSrcFile[j]);	
			fpSrcFile[j] = NULL;
		}
	}
	delete [] fpSrcFile;

	//close destination file
	if(fpDest)	
	{
		fclose(fpDest);
		fpDest = NULL;
	}

	//Hide progress bar
	m_downProgress.SetPos(100);
	m_downProgress.ShowWindow(SW_HIDE);
	
	return TRUE;
}
*/

//strDir 위치에서 strDataDir 위치로 awSelch에서 선택한 채널을 지정옵션으로 저장한다.
BOOL CDataDownDlg::DownLoadLocalDataAll(CString strDir, CString strSaveDir, CWordArray &awSelTrayCh)
{
	CString strSourceFile, strDestFile;

	int nSelChCount = awSelTrayCh.GetSize();
	int nColumnCount = 0;
	int nRowCount = 0, nTotRow = 0;
	int nFileCount = 0;
	CString	strVtgBufArray[STR_BUFF_SIZE];
	CString strVtgHeaderString;
	CString strCrtHeaderString;
	CString strCapHeaderString;
	CString strWattHeaderString;
	CString strWattHourHeaderString;
	CString strHeaderString;
	CString strData, strTemp, strProgText;
	char szBuff[128];
	
	CTestCondition *pCondition = m_resultData.GetTestCondition();
	CStep *pStep;
	if(nSelChCount < 1 || strSaveDir.IsEmpty() || strDir.IsEmpty())		return FALSE;

	//source file open and make column header
	for(int i=0; i<nSelChCount; i++)
	{
		//Make column header
		if(strHeaderString.IsEmpty())
		{
			if(m_SaveSetDlg.m_bSaveDate)
			{
				strHeaderString += "Date,";
				nColumnCount++;
			}
			if(m_SaveSetDlg.m_bTimeSum)		
			{
				strData = m_SaveSetDlg.UnitString(EP_TOT_TIME);
				if(strTemp.IsEmpty())
				{
					strTemp = "TotTime,";
				}
				else
				{
					strTemp.Format("TotTime(%s),",	strData);
				}
				strHeaderString += strTemp;
				nColumnCount++;
			}
			if(m_SaveSetDlg.m_bStepNo)
			{
				strHeaderString += "StepNo,";
				nColumnCount++;
			}
			if(m_SaveSetDlg.m_bStepType)
			{
				strHeaderString += "Type,";
				nColumnCount++;
			}
			if(m_SaveSetDlg.m_bStepTime)
			{
				strData = m_SaveSetDlg.UnitString(EP_STEP_TIME);
				if(strData.IsEmpty())
				{
					strTemp = "Time,";
				}
				else
				{
				strTemp.Format("Time(%s),",	strData);
				}
			strHeaderString += strTemp;
			nColumnCount++;
			}
		}
		
		sprintf(szBuff, "C%d_V(%s),", awSelTrayCh[i], m_SaveSetDlg.UnitString(EP_VOLTAGE));
		strVtgHeaderString += szBuff;
		
		sprintf(szBuff, "C%d_I(%s),", awSelTrayCh[i], m_SaveSetDlg.UnitString(EP_CURRENT));
		strCrtHeaderString += szBuff;
		
		sprintf(szBuff, "C%d_C(%s),", awSelTrayCh[i], m_SaveSetDlg.UnitString(EP_CAPACITY));
		strCapHeaderString += szBuff;
		
		sprintf(szBuff, "C%d_W(%s),", awSelTrayCh[i], m_SaveSetDlg.UnitString(EP_WATT));
		strWattHeaderString += szBuff;
			
		sprintf(szBuff, "C%d_WH(%s),", awSelTrayCh[i], m_SaveSetDlg.UnitString(EP_WATT_HOUR));
		strWattHourHeaderString += szBuff;
	}

	nColumnCount += nSelChCount;
	//Excel column over-flow
	if(nColumnCount > 256)
	{
		strTemp.Format("File create fail.(%s)"); //100083
		AfxMessageBox(strTemp);
		return FALSE;
	}

	CWaitCursor wait;
	m_downProgress.ShowWindow(SW_SHOW);
	m_downProgress.SetPos(0);

	strDestFile = strSaveDir;
	FILE *fpDest = fopen(strDestFile, "wt");
	if(fpDest)
	{
		//Write Test Header
		RESULT_FILE_HEADER *pHeader = m_resultData.GetResultHeader();
		fprintf(fpDest, "FileName, %s\n", (char *)(LPCTSTR)m_resultData.GetFileName());	// FileName //100084
		fprintf(fpDest, "ModleName,%s\n",  m_resultData.GetModelName());					// 모델명 	//100085
		fprintf(fpDest, "TestName,%s\n",  m_resultData.GetTestName());	//공정_조건명:,%s\n			// 시험조건명//100086
		fprintf(fpDest, "Tray_No,%s\n",  pHeader->szTrayNo);							// Tray No//100087
		fprintf(fpDest, "Lot_No,%s\n",  pHeader->szLotNo);							// Lot No//100088
		fprintf(fpDest, "Module_No,%d(%d)\n",  pHeader->nModuleID, pHeader->wGroupIndex+1); //모듈No:, 모듈 %d-%d\n			// Module name //100089
		fprintf(fpDest, "StartTime,%s\n",  pHeader->szDateTime);		//시작_시각:, %s\n				// 시작시간//100090
		fprintf(fpDest, "Worker,%s\n",  pHeader->szOperatorID);	//운영자:, %s\n					// 총시간//100091
		nRowCount = 8;
	}

	//test stared time
	COleDateTime starTime;
	starTime.ParseDateTime(m_resultData.GetResultHeader()->szDateTime);

#ifdef _DEBUG
	DWORD start = GetTickCount();
	DWORD start1 = start;
#endif

	FILE **fpSrcFile = new FILE*[nSelChCount];
	for( i=0; i<nSelChCount; i++)
	{
		//strSourceFile.Format("%s\\ch%03d.csv", strDir, awSelTrayCh[i]);
		strSourceFile.Format("%s\\CH%03d_%02d.csv", strDir, awSelTrayCh[i], 1);
		fpSrcFile[i] = fopen(strSourceFile, "rt");
		if(fpSrcFile[i])
		{
			//read data header
			fscanf(fpSrcFile[i], "%s,", szBuff);
		}
	}

	DWORD dataItem = EP_NOT_USE_DATA;
	for(int it = 0; it <5; it++)
	{
		strProgText.Empty();
		dataItem = EP_NOT_USE_DATA;

		if(m_SaveSetDlg.m_bVoltage && it == 0)		//1. 전압 저장 
		{
			dataItem = EP_VOLTAGE;
		}			
		if(m_SaveSetDlg.m_bCurrent && it == 1)		//2. 전류 저장 
		{
			dataItem = EP_CURRENT;
		}
		if(m_SaveSetDlg.m_bCapacity && it == 2)		//3. 용량 저장 
		{
			dataItem = EP_CAPACITY;
		}
		if(m_SaveSetDlg.m_bWatt && it == 3)			//4. Watt 저장 
		{
			dataItem = EP_WATT;
		}
		if(m_SaveSetDlg.m_bWattHour && it == 4)		//5. Watt Hour 저장
		{
			dataItem = EP_WATT_HOUR;
		}
		
		if(dataItem == EP_NOT_USE_DATA)	continue;

		CString strItemHeader;
		switch(dataItem)
		{
		case EP_VOLTAGE:	strItemHeader = strVtgHeaderString;			break;
		case EP_CURRENT:	strItemHeader = strCrtHeaderString;			break;
		case EP_CAPACITY:	strItemHeader = strCapHeaderString;			break;
		case EP_WATT:		strItemHeader = strWattHeaderString;		break;
		case EP_WATT_HOUR:	strItemHeader = strWattHourHeaderString;	break;
		}

		nRowCount++;
		fprintf(fpDest, "\n%s%s\n", strHeaderString, strItemHeader);	
		nRowCount++;

		for( i=0; i<nSelChCount; i++)
		{
			if(fpSrcFile[i])
			{
				fseek(fpSrcFile[i], 0,  SEEK_SET );
				fscanf(fpSrcFile[i], "%s,", szBuff);
			}
		}

		int nBufferRepeatCnt = 0;
		while(1 && fpDest)
		{
			//시간이 오래 걸림 
			if(strProgText.IsEmpty())			strProgText.Format("Item id %d Data saving...(%d).", dataItem, ++nBufferRepeatCnt);
			else								strProgText += ".";
			GetDlgItem(IDC_STATIC_SEL_CH)->SetWindowText(strProgText);
			if(strProgText.GetLength() > 40)	strProgText.Empty();

			int nPrevDataCnt = 0;
			int nBufCnt = 0;
			int nProgressCnt = 0;
			
			for(int i=0; i<nSelChCount; i++)
			{
				if(fpSrcFile[i])
				{
					int nStep, nState;
					float fTimeSum, fTime, fVoltage, fCurrent, fCapacity, fWatt, fWattHour, fTemp;

					nBufCnt = 0;
					//파일을 한 줄씩 읽어 들임(1초 간격으로 저장된 파일이여야 함)
					//while(fscanf(fpSrcFile[i], "%f,%d,%f,%f,%f,%f,%f,%f,", &fTimeSum, &nStep, &fTime, &fVoltage, &fCurrent, &fCapacity, &fWatt, &fWattHour) == 8 && nBufCnt<STR_BUFF_SIZE)
				
					//V2.0
					while(fscanf(fpSrcFile[i], "%d,%d,%f,%f,%f,%f,%f,%f,", &nStep, &nState, &fTimeSum,  &fTime, &fVoltage, &fCurrent, &fCapacity, &fWattHour) == 8 && nBufCnt<STR_BUFF_SIZE)
					{
						m_downProgress.SetPos(int(float(nProgressCnt++*100.0f)/float(nSelChCount*STR_BUFF_SIZE)));
						
						fWatt = fVoltage*fCurrent/1000.0f;
						
						pStep = pCondition->GetStep(nStep-1);
						//이상한 data or step결과가 없음 //Skip the check step
						if(pStep == NULL)	continue;
						if(pStep->m_type == EP_TYPE_CHARGE && m_SaveSetDlg.m_bSaveCharge == FALSE)			continue;
						if(pStep->m_type == EP_TYPE_DISCHARGE && m_SaveSetDlg.m_bSaveDischarge == FALSE)	continue;
						if(pStep->m_type == EP_TYPE_REST && m_SaveSetDlg.m_bSaveRest == FALSE)				continue;
						if(pStep->m_type == EP_TYPE_IMPEDANCE && m_SaveSetDlg.m_bSaveImpdeacne == FALSE)	continue;
						//if(pStep->m_type == EP_TYPE_END)		continue;		 
						//if(pStep->m_type == EP_TYPE_OCV)		continue;		
						
						fTemp = fTimeSum/(3600.0f*24.0f); 
						COleDateTimeSpan stepSpan(fTemp);
						COleDateTime testDate = starTime + stepSpan;
						TRACE("%s\n", starTime.Format());

						if(strVtgBufArray[nBufCnt].IsEmpty())
						{
							//Save date time
							if(m_SaveSetDlg.m_bSaveDate)
							{
								strData.Format("%s,", testDate.Format());
								strVtgBufArray[nBufCnt] += strData;
							}
							//total time
							if(m_SaveSetDlg.m_bTimeSum)		
							{
								strData.Format("%s,", m_SaveSetDlg.ValueString(fTimeSum, EP_TOT_TIME));
								strVtgBufArray[nBufCnt] += strData;
							}
							//Step No
							if(m_SaveSetDlg.m_bStepNo)
							{
								strData.Format("%s,", m_SaveSetDlg.ValueString(nStep, EP_STEP_NO));
								strVtgBufArray[nBufCnt] += strData;
							}
							//type
							if(m_SaveSetDlg.m_bStepType)
							{
								strData.Format("%s,", ::StepTypeMsg(pStep->m_type));
								strVtgBufArray[nBufCnt] += strData;
							}
							//Step Time
							if(m_SaveSetDlg.m_bStepTime)
							{
								strData.Format("%s,", m_SaveSetDlg.ValueString(fTime, EP_STEP_TIME));
								strVtgBufArray[nBufCnt] += strData;
							}
						}

						switch(dataItem)
						{
						case EP_VOLTAGE: 
							sprintf(szBuff, "%s,", m_SaveSetDlg.ValueString(fVoltage, EP_VOLTAGE));
							break;
						case EP_CURRENT: 
							sprintf(szBuff, "%s,", m_SaveSetDlg.ValueString(fCurrent, EP_CURRENT));
							break;
						case EP_CAPACITY: 
							sprintf(szBuff, "%s,", m_SaveSetDlg.ValueString(fCapacity, EP_CAPACITY));
							break;
						case EP_WATT: 
							sprintf(szBuff, "%s,", m_SaveSetDlg.ValueString(fWatt, EP_WATT));
							break;
						case EP_WATT_HOUR: 
							sprintf(szBuff, "%s,", m_SaveSetDlg.ValueString(fWattHour, EP_WATT));
							break;
						}

						strVtgBufArray[nBufCnt] = strVtgBufArray[nBufCnt]+szBuff;
						nBufCnt++;
						
						//file 별로 record 수가 다르게 저장되어 있을 가장 첫번째 record file 수만큼 만 저장한다.
						//if(i != 0 && nPrevDataCnt <= nBufCnt)
						//{
						//	break;
						//}
					}
				}
				nPrevDataCnt = nBufCnt;	//이전 channel에서 저장한 record 수 
			}
			
			//Write data to file
			for(int s=0; s<nBufCnt; s++)
			{
				//make next excel file : FILE_NAME(1).csv
				if(nRowCount >= MAX_EXCEL_ROW)
				{
					if(MakeNextFile(fpDest, nFileCount, strDestFile) == FALSE)
					{
						strData.Format("File create fail.(%s).", strDestFile);
						AfxMessageBox(strData, MB_ICONSTOP|MB_OK);
						goto _FAIL_EXIT;
					}
						
					ASSERT(fpDest);
					nRowCount = 0;								//Header 기록 
				}
				strData = strVtgBufArray[s];
				fprintf(fpDest, "%s\n", strData);
				nRowCount++;
				
				strVtgBufArray[s].Empty();
			}

#ifdef _DEBUG
			DWORD end = GetTickCount();
			TRACE("=================>%dmsec(%d)\n", end -start, end - start1);
			start1 = GetTickCount();
#endif

			//file read scan 완료
			if(nBufCnt < STR_BUFF_SIZE)
			{
				break;
			}
			//HandleMessage();
			PUMPMESSAGE();
		}
	}
	GetDlgItem(IDC_STATIC_SEL_CH)->SetWindowText("Data save complited");

_FAIL_EXIT:

	//close source channel file
	for(int j=0; j<nSelChCount; j++)
	{
		if(fpSrcFile[j])
		{
			fclose(fpSrcFile[j]);	
			fpSrcFile[j] = NULL;
		}
	}
	delete [] fpSrcFile;

	//close destination file
	if(fpDest)	
	{
		fclose(fpDest);
		fpDest = NULL;
	}

	//Hide progress bar
	m_downProgress.SetPos(100);
	m_downProgress.ShowWindow(SW_HIDE);
	
	return TRUE;
}


/*
//strDir 위치에서 strDataDir 위치로 awSelch에서 선택한 채널을 지정옵션으로 저장한다.
BOOL CDataDownDlg::DownLoadLocalData(CString strDir, CString strSaveDir, CWordArray &awSelTrayCh)
{
	int nChNo;
	CString strTemp;
	CString strSourceFile, strDestFile;
	
	CString strSensorSourceFile1, strSensorSourceFile2;
	strSensorSourceFile1.Format("%s\\SensorT.csv", strDir);
	strSensorSourceFile2.Format("%s\\SensorV.csv", strDir);

	//channel과 matching되는 sensor channel만을 추출하여 저장해야 한다.

	CWaitCursor wait;
	m_downProgress.ShowWindow(SW_SHOW);
	m_downProgress.SetPos(0);

	CTestCondition *pCondition = m_resultData.GetTestCondition();
	CStep *pStep, *pStep2;
	char szBuff[128];
	CString strSensorData1, strSensorData2, strFileBuff;

	FILE *fpSource, *fpDest;
	CStdioFile	sensorFile1, sensorFile2;

	int nRowCount = 0;
	int nFileCount = 0;
	CString strHeaderString;
	CWordArray awMapSensorCh1, awMapSensorCh2;

	for(int i=0; i<awSelTrayCh.GetSize(); i++)
	{
		m_downProgress.SetPos(i*100/awSelTrayCh.GetSize());

		nRowCount = 0;
		nChNo = awSelTrayCh.GetAt(i);	//Tray Based Ch No
		strSourceFile.Format("%s\\ch%03d.csv", strDir, nChNo);			
		strDestFile.Format("%s\\ch%03d.csv", strSaveDir, nChNo);	
		
		m_resultData.FindMappingSensorCh(nChNo, awMapSensorCh1, 0);		//0개 or 1개 or 여러개가 Mapping될 수 있다.
		m_resultData.FindMappingSensorCh(nChNo, awMapSensorCh2, 1);		//0개 or 1개 or 여러개가 Mapping될 수 있다.

		//////////////////////////////////////////////////////////////////////////
		//Data copy start
		BOOL bSensorFileRead1 = sensorFile1.Open(strSensorSourceFile1, CFile::modeRead);
		BOOL bSensorFileRead2 = sensorFile2.Open(strSensorSourceFile2, CFile::modeRead);
		fpSource = fopen(strSourceFile, "rt");
		if(fpSource)
		{
			fpDest = fopen(strDestFile, "wt");
			strHeaderString.Empty();

			if(fpDest)
			{		
				//StepNo,t(sec),V(mV),I(mA),C(mAh),W(mW),Wh(mWh)
				//column header read //Skip Column Header
				fscanf(fpSource, "%s,%s,%s,%s,%s,%s,%s,%s,", szBuff, szBuff, szBuff, szBuff, szBuff, szBuff, szBuff, szBuff);
				//온도 Sesnor Header read
				if(bSensorFileRead1)
				{	//read header
					sensorFile1.ReadString(strSensorData1);
					FindDataColumn( awMapSensorCh1, strSensorData1, 0, &m_resultData);
				}
				if(bSensorFileRead2)
				{
					//read header
					sensorFile2.ReadString(strSensorData2);
					FindDataColumn( awMapSensorCh2, strSensorData2, 1, &m_resultData);
				}

				//Write Column Header
				if(m_SaveSetDlg.m_bTimeSum)
				{
					strTemp = m_SaveSetDlg.UnitString(EP_TOT_TIME);
					if(strTemp.IsEmpty())
					{
						strFileBuff = "TotTime,";
					}
					else
					{
						strFileBuff.Format("TotTime(%s),",	strTemp);
					}
					strHeaderString += strFileBuff;
				}
				if(m_SaveSetDlg.m_bStepNo)
				{
					strHeaderString += "StepNo,";
				}
				if(m_SaveSetDlg.m_bStepType)
				{
					strHeaderString += "Type,";
				}
				if(m_SaveSetDlg.m_bStepTime)
				{
					strTemp = m_SaveSetDlg.UnitString(EP_STEP_TIME);
					if(strTemp.IsEmpty())
					{
						strFileBuff = "Time,";
					}
					else
					{
						strFileBuff.Format("Time(%s),",	strTemp);
					}
					strHeaderString += strFileBuff;
				}

				if(m_SaveSetDlg.m_bVoltage)	
				{
					strFileBuff.Format("Volt(%s),",	m_SaveSetDlg.UnitString(EP_VOLTAGE));
					strHeaderString += strFileBuff;
				}
				if(m_SaveSetDlg.m_bCurrent)
				{
					strFileBuff.Format("Crt(%s),",		m_SaveSetDlg.UnitString(EP_CURRENT));
					strHeaderString += strFileBuff;
				}
				if(m_SaveSetDlg.m_bCapacity)
				{
					strFileBuff.Format("Capa(%s),",	m_SaveSetDlg.UnitString(EP_CAPACITY));
					strHeaderString += strFileBuff;
				}
				if(m_SaveSetDlg.m_bWatt)
				{
					strFileBuff.Format("Watt(%s),",	m_SaveSetDlg.UnitString(EP_WATT));
					strHeaderString += strFileBuff;
				}
				if(m_SaveSetDlg.m_bWattHour)
				{
					strFileBuff.Format("WattHour(%s),", m_SaveSetDlg.UnitString(EP_WATT_HOUR));
					strHeaderString += strFileBuff;
				}
				if(bSensorFileRead1 && m_SaveSetDlg.m_bTempData && awMapSensorCh1.GetSize() > 0)	
				{
					strFileBuff.Format( "%s,", strSensorData1);
					strHeaderString += strFileBuff;
				}
				if(bSensorFileRead2 && m_SaveSetDlg.m_bAuxVData && awMapSensorCh2.GetSize() > 0)
				{
					strFileBuff.Format( "%s,", strSensorData2);
					strHeaderString += strFileBuff;
				}

				fprintf(fpDest, "%s\n", strHeaderString);
				nRowCount++;		//Write Header name
				
				int nCurStep = 1, nStep = 0;
				float fTime, fVoltage, fCurrent, fCapacity, fWatt, fWattHour, fSaveLastTime = 0.0f;
				float fCurTime = 0.0f, fCurVoltage = 0.0f, fCurCurrent = 0.0f, fTimeSum = 0.0f, fStepTotTime = 0.0f, fTimeScan = 0.0f;
				CString strTime, strVoltage, strCurrent, strCapacity, strWatt, strWattHour;
				BOOL	bReadSensorData1 = FALSE;
				BOOL	bReadSensorData2 = FALSE;
				STR_STEP_RESULT *pStepData = NULL;
				STR_SAVE_CH_DATA *pStepResult = NULL;

				//파일을 한줄씩 읽어 들임(1초 간격으로 저장된 파일이여야 함)
				while(fscanf(fpSource, "%f,%d,%f,%f,%f,%f,%f,%f,", &fTimeSum, &nStep, &fTime, &fVoltage, &fCurrent, &fCapacity, &fWatt, &fWattHour) == 8)
				{
					if(bSensorFileRead1)	
					{
						//Sensor data도 한줄씩 읽어 들임
						bReadSensorData1 = sensorFile1.ReadString(strSensorData1);
					}
					if(bSensorFileRead2)	
					{
						//Sensor data도 한줄씩 읽어 들임
						bReadSensorData2 = sensorFile2.ReadString(strSensorData2);
					}
				
					pStep = pCondition->GetStep(nStep-1);
					//이상한 data or step결과가 없음 
					//Skip the check step
					if(pStep == NULL)	continue;

					//Step이 변화된 구간
					if(nCurStep < nStep)
					{
//						fStepTotTime += fTimeScan;
						fCurTime = 0.0;

						//이전 Step의 최종값 저장 
						for(int nS = nCurStep; nS < nStep; nS++)
						{
							pStepData = m_resultData.GetStepData(nS-1);
							pStep2 = pCondition->GetStep(nS-1);
							if(pStepData)
							{
								//최종 결과값 저장 
								pStepResult = (STR_SAVE_CH_DATA *)pStepData->aChData[nChNo-1];					
								//저장을한 최종 시간과 중복되는 시간이면 ??
								if(nS == pStepResult->nStepNo && fSaveLastTime == pStepResult->fTotalTime)
								{
									TRACE("중복 data, Skip data\n");
								}
								else
								{
									if(m_SaveSetDlg.m_bTimeSum)		fprintf(fpDest, "!%s,",	m_SaveSetDlg.ValueString(fTimeSum, EP_TOT_TIME));
									if(m_SaveSetDlg.m_bStepNo)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->nStepNo, EP_STEP_NO));
									if(m_SaveSetDlg.m_bStepType)	fprintf(fpDest, "%s,", ::StepTypeMsg(pStep2->m_type));
									if(m_SaveSetDlg.m_bStepTime)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fStepTime, EP_STEP_TIME));
									if(m_SaveSetDlg.m_bVoltage)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fVoltage, EP_VOLTAGE));
									if(m_SaveSetDlg.m_bCurrent)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fCurrent, EP_CURRENT));
									if(m_SaveSetDlg.m_bCapacity)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fCapacity, EP_CAPACITY));
									if(m_SaveSetDlg.m_bWatt)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fWatt, EP_WATT));
									if(m_SaveSetDlg.m_bWattHour)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fWattHour, EP_WATT_HOUR));
									if(bSensorFileRead1 && bReadSensorData1 && m_SaveSetDlg.m_bTempData && awMapSensorCh1.GetSize() > 0)
										fprintf(fpDest, "%s,", ConvertDataString(awMapSensorCh1, strSensorData1));
									if(bSensorFileRead2 && bReadSensorData2 && m_SaveSetDlg.m_bAuxVData && awMapSensorCh2.GetSize() > 0)
										fprintf(fpDest, "%s,", ConvertDataString(awMapSensorCh2,strSensorData2));

									fprintf(fpDest, "\n");

									//////////////////////////////////////////////////////////////////////////
									nRowCount++;
									if(nRowCount >= MAX_EXCEL_ROW)
									{
										if(MakeNextFile(fpDest, nFileCount, strDestFile) == FALSE)
										{
											strTemp.Format("%s 파일을 생성할 수 없습니다.", strDestFile);
											AfxMessageBox(strTemp, MB_ICONSTOP|MB_OK);
											fclose(fpSource);
// 											if(bSensorFileRead1)
// 											{
// 												sensorFile1.Close();
// 											}
// 											if(bSensorFileRead2)
// 											{
// 												sensorFile2.Close();
// 											}
// 											
											return FALSE;
										}
										
										ASSERT(fpDest);
										fprintf(fpDest, "%s\n", strHeaderString);//column header 기록
										nRowCount = 1;		//Header 기록 
									}
									//////////////////////////////////////////////////////////////////////////
								}
							}
							else	//현재 진행중이거나 Step 결과 data가 손실된 경우 
							{
								pStepResult = NULL;
							}
						}
					}
					
					//같은 Step에 시간이 같은 Data가 존재할 경우 제외한다.
					if(nCurStep == nStep && fTimeScan == fTimeSum)	continue;
												
					//현재 총시간은 이전 step의 총합 + 현재 Step 시간 
					//fTimeSum =	fStepTotTime + fTime;
					nCurStep  = nStep;
					//현재 Step의 최종 시간 Update
					fTimeScan = fTimeSum;

					//Step type check
					if(pStep->m_type == EP_TYPE_CHARGE && m_SaveSetDlg.m_bSaveCharge == FALSE)			continue;
					if(pStep->m_type == EP_TYPE_DISCHARGE && m_SaveSetDlg.m_bSaveDischarge == FALSE)	continue;
					if(pStep->m_type == EP_TYPE_REST && m_SaveSetDlg.m_bSaveRest == FALSE)				continue;
					if(pStep->m_type == EP_TYPE_IMPEDANCE && m_SaveSetDlg.m_bSaveImpdeacne == FALSE)	continue;
					if(pStep->m_type == EP_TYPE_END)	continue;					//완료 Step이나 
					if(pStep->m_type == EP_TYPE_OCV)	continue;					//OCV data는 실처리에서 가져오지 않고 step결과 data로 기록한다.

					//1초간격으로 저장되어 있지 않거나 
					//중간 data가 손실되어 없을 경우 시간이 역전되는 경우가 발생한다.
					if(fTime > (fCurTime + 1.0f))
					{
						fCurTime = fTime - 0.5f;	//시간이 역전되면 현재 시간을 data 시간보다 0.5sec 작은 곳으로 이동  
					}
						
					//Delta time check
					//원본 data는 무조건 1초 간격으로 저장되어 있어야 한다.
					//절대 시간 delta로 변경 => 2006/4/26
					//if(m_SaveSetDlg.m_nDeltaTime > 0 && fabs(fTime - fCurTime) >= (float)m_SaveSetDlg.m_nDeltaTime)		//delta time save
					//20061209 저장 간격을 default 1초에서 시험조건에서 설정으로 변경(SBC에서 시간간격 저장)
//					if(m_SaveSetDlg.m_nDeltaTime > 0 && fTime >= fCurTime &&  fTime < (fCurTime + 1.0f))				//오차 범위 1초안 data 저장 
//					{							
							if(m_SaveSetDlg.m_bTimeSum)		fprintf(fpDest, "%s,",	m_SaveSetDlg.ValueString(fTimeSum, EP_TOT_TIME));
							if(m_SaveSetDlg.m_bStepNo)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(nStep, EP_STEP_NO));
							if(m_SaveSetDlg.m_bStepType)	fprintf(fpDest, "%s,", ::StepTypeMsg(pStep->m_type));
							if(m_SaveSetDlg.m_bStepTime)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fTime, EP_STEP_TIME));
							if(m_SaveSetDlg.m_bVoltage)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fVoltage, EP_VOLTAGE));
							if(m_SaveSetDlg.m_bCurrent)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fCurrent, EP_CURRENT));
							if(m_SaveSetDlg.m_bCapacity)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fCapacity, EP_CAPACITY));
							if(m_SaveSetDlg.m_bWatt)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fWatt, EP_WATT));
							if(m_SaveSetDlg.m_bWattHour)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fWattHour, EP_WATT_HOUR));
							if(bSensorFileRead1 && bReadSensorData1 && m_SaveSetDlg.m_bTempData && awMapSensorCh1.GetSize() > 0)
								fprintf(fpDest, "%s,", ConvertDataString(awMapSensorCh1, strSensorData1));
							if(bSensorFileRead2 && bReadSensorData2 && m_SaveSetDlg.m_bAuxVData && awMapSensorCh2.GetSize() > 0)
								fprintf(fpDest, "%s,", ConvertDataString(awMapSensorCh2,strSensorData2));

							fprintf(fpDest, "\n");
							
							//////////////////////////////////////////////////////////////////////////
							nRowCount++;
							if(nRowCount >= MAX_EXCEL_ROW)
							{
								if(MakeNextFile(fpDest, nFileCount, strDestFile) == FALSE)
								{
									strTemp.Format("%s 파일을 생성할 수 없습니다.", strDestFile);
									AfxMessageBox(strTemp, MB_ICONSTOP|MB_OK);
									fclose(fpSource);
									//if(bSensorFileRead1)		sensorFile1.Close();
									//if(bSensorFileRead2)		sensorFile2.Close();
									return FALSE;
								}
									
								ASSERT(fpDest);
								fprintf(fpDest, "%s\n", strHeaderString);//column header 기록
								nRowCount = 1;		//Header 기록 
							}
							//////////////////////////////////////////////////////////////////////////

							//fCurTime = fTime;
							fCurTime = fCurTime + m_SaveSetDlg.m_nDeltaTime;
							fSaveLastTime = fTimeSum;
//					}
						
					//Delta V Check
/*					if(m_SaveSetDlg.m_nDeltaV > 0 && fabs(fVoltage - fCurVoltage) >= (float)m_SaveSetDlg.m_nDeltaV)
					{
							if(m_SaveSetDlg.m_bTimeSum)		fprintf(fpDest, "%s,",	m_SaveSetDlg.ValueString(fTimeSum, EP_TOT_TIME));
							if(m_SaveSetDlg.m_bStepNo)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(nStep, EP_STEP_NO));
							if(m_SaveSetDlg.m_bStepType)	fprintf(fpDest, "%s,", ::StepTypeMsg(pStep->m_type));
							if(m_SaveSetDlg.m_bStepTime)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fTime, EP_STEP_TIME));
							if(m_SaveSetDlg.m_bVoltage)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fVoltage, EP_VOLTAGE));
							if(m_SaveSetDlg.m_bCurrent)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fCurrent, EP_CURRENT));
							if(m_SaveSetDlg.m_bCapacity)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fCapacity, EP_CAPACITY));
							if(m_SaveSetDlg.m_bWatt)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fWatt, EP_WATT));
							if(m_SaveSetDlg.m_bWattHour)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fWattHour, EP_WATT_HOUR));
							if(bSensorFileRead1 && bReadSensorData1 && m_SaveSetDlg.m_bTempData && awMapSensorCh1.GetSize() > 0)
								fprintf(fpDest, "%s,", ConvertDataString(awMapSensorCh1, strSensorData1));
							if(bSensorFileRead2 && bReadSensorData2 && m_SaveSetDlg.m_bAuxVData && awMapSensorCh2.GetSize() > 0)
								fprintf(fpDest, "%s,", ConvertDataString(awMapSensorCh2,strSensorData2));
							fprintf(fpDest, "\n");
							
							//////////////////////////////////////////////////////////////////////////
							nRowCount++;
							if(nRowCount >= MAX_EXCEL_ROW)
							{
								if(MakeNextFile(fpDest, nFileCount, strDestFile) == FALSE)
								{
									strTemp.Format("%s 파일을 생성할 수 없습니다.", strDestFile);
									AfxMessageBox(strTemp, MB_ICONSTOP|MB_OK);
									fclose(fpSource);
									//if(bSensorFileRead1)		sensorFile1.Close();
									//if(bSensorFileRead2)		sensorFile2.Close();
									return FALSE;
								}
									
								ASSERT(fpDest);
								fprintf(fpDest, "%s\n", strHeaderString);//column header 기록
								nRowCount = 1;		//Header 기록 
							}
							//////////////////////////////////////////////////////////////////////////

							fCurVoltage = fVoltage;
					}

					//delta I Check
					if(m_SaveSetDlg.m_nDeltaI > 0 && fabs(fCurrent - fCurCurrent) >= (float)m_SaveSetDlg.m_nDeltaI )
					{
							if(m_SaveSetDlg.m_bTimeSum)		fprintf(fpDest, "%s,",	m_SaveSetDlg.ValueString(fTimeSum, EP_TOT_TIME));
							if(m_SaveSetDlg.m_bStepNo)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(nStep, EP_STEP_NO));
							if(m_SaveSetDlg.m_bStepType)	fprintf(fpDest, "%s,", ::StepTypeMsg(pStep->m_type));
							if(m_SaveSetDlg.m_bStepTime)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fTime, EP_STEP_TIME));
							if(m_SaveSetDlg.m_bVoltage)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fVoltage, EP_VOLTAGE));
							if(m_SaveSetDlg.m_bCurrent)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fCurrent, EP_CURRENT));
							if(m_SaveSetDlg.m_bCapacity)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fCapacity, EP_CAPACITY));
							if(m_SaveSetDlg.m_bWatt)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fWatt, EP_WATT));
							if(m_SaveSetDlg.m_bWattHour)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fWattHour, EP_WATT_HOUR));
							if(bSensorFileRead1 && bReadSensorData1 && m_SaveSetDlg.m_bTempData && awMapSensorCh1.GetSize() > 0)
								fprintf(fpDest, "%s,", ConvertDataString(awMapSensorCh1, strSensorData1));
							if(bSensorFileRead2 && bReadSensorData2 && m_SaveSetDlg.m_bAuxVData && awMapSensorCh2.GetSize() > 0)	
								fprintf(fpDest, "%s,", ConvertDataString(awMapSensorCh2,strSensorData2));
							fprintf(fpDest, "\n");
							
							//////////////////////////////////////////////////////////////////////////
							nRowCount++;
							if(nRowCount >= MAX_EXCEL_ROW)
							{
								if(MakeNextFile(fpDest, nFileCount, strDestFile) == FALSE)
								{
									strTemp.Format("%s 파일을 생성할 수 없습니다.", strDestFile);
									AfxMessageBox(strTemp, MB_ICONSTOP|MB_OK);
									fclose(fpSource);
									//if(bSensorFileRead1)		sensorFile1.Close();
									//if(bSensorFileRead2)		sensorFile2.Close();
									return FALSE;
								}
									
								ASSERT(fpDest);
								fprintf(fpDest, "%s\n", strHeaderString);//column header 기록
								nRowCount = 1;		//Header 기록 
							}
							//////////////////////////////////////////////////////////////////////////
							
							fCurCurrent = fCurrent;
					}
*/					
/*				}

				//이전 Step의 최종값 저장 
				STR_STEP_RESULT *pLastData = m_resultData.GetLastStepData();

				if(pLastData)
				{
					for(int nS = nCurStep; nS < pLastData->stepCondition.stepHeader.stepIndex+1; nS++)
					{
						pStepData = m_resultData.GetStepData(nS-1);
						pStep2 = pCondition->GetStep(nS-1);
						if(pStepData)
						{
							//최종 결과값 저장 
							pStepResult = (STR_SAVE_CH_DATA *)pStepData->aChData[nChNo-1];					
							//저장을한 최종 시간과 중복되는 시간이면 ??
							if(nS == pStepResult->nStepNo && fSaveLastTime == pStepResult->fTotalTime)
							{
								TRACE("중복 data, Skip data\n");
							}
							else
							{
								if(m_SaveSetDlg.m_bTimeSum)		fprintf(fpDest, "!%s,",	m_SaveSetDlg.ValueString(fTimeSum, EP_TOT_TIME));
								if(m_SaveSetDlg.m_bStepNo)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->nStepNo, EP_STEP_NO));
								if(m_SaveSetDlg.m_bStepType)	fprintf(fpDest, "%s,", ::StepTypeMsg(pStep2->m_type));
								if(m_SaveSetDlg.m_bStepTime)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fStepTime, EP_STEP_TIME));
								if(m_SaveSetDlg.m_bVoltage)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fVoltage, EP_VOLTAGE));
								if(m_SaveSetDlg.m_bCurrent)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fCurrent, EP_CURRENT));
								if(m_SaveSetDlg.m_bCapacity)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fCapacity, EP_CAPACITY));
								if(m_SaveSetDlg.m_bWatt)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fWatt, EP_WATT));
								if(m_SaveSetDlg.m_bWattHour)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fWattHour, EP_WATT_HOUR));
								if(bSensorFileRead1 && bReadSensorData1 && m_SaveSetDlg.m_bTempData && awMapSensorCh1.GetSize() > 0)
									fprintf(fpDest, "%s,", ConvertDataString(awMapSensorCh1, strSensorData1));
								if(bSensorFileRead2 && bReadSensorData2 && m_SaveSetDlg.m_bAuxVData && awMapSensorCh2.GetSize() > 0)
									fprintf(fpDest, "%s,", ConvertDataString(awMapSensorCh2,strSensorData2));

								fprintf(fpDest, "\n");

								//////////////////////////////////////////////////////////////////////////
								nRowCount++;
								if(nRowCount >= MAX_EXCEL_ROW)
								{
									if(MakeNextFile(fpDest, nFileCount, strDestFile) == FALSE)
									{
										strTemp.Format("%s 파일을 생성할 수 없습니다.", strDestFile);
										AfxMessageBox(strTemp, MB_ICONSTOP|MB_OK);
										fclose(fpSource);
// 										if(bSensorFileRead1)
// 										{
// 											sensorFile1.Close();
// 										}
// 										if(bSensorFileRead2)
// 										{
// 											sensorFile2.Close();
// 										}
// 										
										return FALSE;
									}
									
									ASSERT(fpDest);
									fprintf(fpDest, "%s\n", strHeaderString);//column header 기록
									nRowCount = 1;		//Header 기록 
								}
								//////////////////////////////////////////////////////////////////////////
							}
						}
						else	//현재 진행중이거나 Step 결과 data가 손실된 경우 
						{
							pStepResult = NULL;
						}
					}
				}

				fclose(fpSource);
				fclose(fpDest);
//				if(bSensorFileRead1)		sensorFile1.Close();
//				if(bSensorFileRead2)		sensorFile2.Close();
			}
		}
		//////////////////////////////////////////////////////////////////////////
	}

	//Sensor data file copy
	//채널에 포함 시키므로 저장하지 않음	2006/04/26
// 	if(::CopyFile(strSensorSourceFile, strSensorDestFile, FALSE) == FALSE)	//overwrite copy
// 	{
// 		WriteLog(strDestFile + " create fail!!!");
// 	}
	m_downProgress.SetPos(100);
	m_downProgress.ShowWindow(SW_HIDE);
	
	return TRUE;
}
*/

/*	
//strDir 위치에서 strDataDir 위치로 awSelch에서 선택한 채널을 지정옵션으로 저장한다.
BOOL CDataDownDlg::DownLoadLocalData(CString strDir, CString strSaveDir, CWordArray &awSelCh)
{
	int nChNo;
	CString strTemp;
	CString strSourceFile, strDestFile;
	CString strSensorSourceFile, strSensorDestFile;
	strSensorSourceFile.Format("%s\\Sensor.csv", strDir);
	strSensorDestFile.Format("%s\\Sensor.csv", strSaveDir);

	CWaitCursor wait;
	m_downProgress.ShowWindow(SW_SHOW);
	m_downProgress.SetPos(0);

	CTestCondition *pCondition = m_resultData.GetTestCondition();
	CStep *pStep, *pStep2;
	char szBuff[128];
	CString strSenosrData;

	FILE *fpSource, *fpDest;
	CStdioFile	sensorFile;

	for(int i=0; i<awSelCh.GetSize(); i++)
	{
		m_downProgress.SetPos(i*100/awSelCh.GetSize());

		nChNo = awSelCh.GetAt(i);
		strSourceFile.Format("%s\\ch%03d.csv", strDir, nChNo);
		strDestFile.Format("%s\\ch%03d.csv", strSaveDir, nChNo);

		//////////////////////////////////////////////////////////////////////////
		//Data copy start
		BOOL bSensorFileRead = sensorFile.Open(strSensorSourceFile, CFile::modeRead);
		fpSource = fopen(strSourceFile, "rt");
		if(fpSource)
		{
			fpDest = fopen(strDestFile, "wt");
			if(fpDest)
			{		
				//StepNo,t(sec),V(mV),I(mA),C(mAh),W(mW),Wh(mWh)
				//column header read //Skip Column Header
				fscanf(fpSource, "%s,%s,%s,%s,%s,%s,%s,", szBuff, szBuff, szBuff, szBuff, szBuff, szBuff, szBuff);
				//온도 Sesnor Header read
				if(bSensorFileRead)	sensorFile.ReadString(strSenosrData);

				//Write Column Header
				if(m_SaveSetDlg.m_bTimeSum)
				{
					strTemp = m_SaveSetDlg.UnitString(EP_TOT_TIME);
					if(strTemp.IsEmpty())
					{
						fprintf(fpDest, "TotTime,");
					}
					else
					{
						fprintf(fpDest, "TotTime(%s),",	strTemp);
					}
				}
				if(m_SaveSetDlg.m_bStepNo)		fprintf(fpDest, "StepNo,");
				if(m_SaveSetDlg.m_bStepType)		fprintf(fpDest, "Type,");
				if(m_SaveSetDlg.m_bStepTime)
				{
					strTemp = m_SaveSetDlg.UnitString(EP_STEP_TIME);
					if(strTemp.IsEmpty())
					{
						fprintf(fpDest, "Time,");
					}
					else
					{
						fprintf(fpDest, "Time(%s),",	strTemp);
					}
				}
				if(m_SaveSetDlg.m_bVoltage)		fprintf(fpDest, "Volt(%s),",	m_SaveSetDlg.UnitString(EP_VOLTAGE));
				if(m_SaveSetDlg.m_bCurrent)		fprintf(fpDest, "Crt(%s),",		m_SaveSetDlg.UnitString(EP_CURRENT));
				if(m_SaveSetDlg.m_bCapacity)	fprintf(fpDest, "Capa(%s),",	m_SaveSetDlg.UnitString(EP_CAPACITY));
				if(m_SaveSetDlg.m_bWatt)		fprintf(fpDest, "Watt(%s),",	m_SaveSetDlg.UnitString(EP_WATT));
				if(m_SaveSetDlg.m_bWattHour)	fprintf(fpDest, "WattHour(%s),", m_SaveSetDlg.UnitString(EP_WATT_HOUR));
				if(bSensorFileRead && m_SaveSetDlg.m_bTempData)	fprintf(fpDest, "%s,", strSenosrData);
				fprintf(fpDest, "\n");
				
				int nCurStep = 0, nStep = 0;
				float fTime, fVoltage, fCurrent, fCapacity, fWatt, fWattHour;
				float fCurTime = 0.0f, fCurVoltage = 0.0f, fCurCurrent = 0.0f, fTimeSum = 0.0f, fStepTotTime = 0.0f, fTimeScan = 0.0f;
				CString strTime, strVoltage, strCurrent, strCapacity, strWatt, strWattHour;
				BOOL	bReadSensorData = FALSE;
				STR_STEP_RESULT *pStepData = NULL;
				STR_SAVE_CH_DATA *pStepResult = NULL;

				//현재 진행중인 Step에 대한 data 처리 				
				for(int stepIndex = 0; stepIndex <m_resultData.GetStepSize() ; stepIndex++)
				{
					pStepData = m_resultData.GetStepData(stepIndex);
					if(pStepData == NULL)	continue;
					
					pStepResult = (STR_SAVE_CH_DATA *)pStepData->stepData.chData[nChNo-1];
					pStep2 = pCondition->GetStep(pStepResult->nStepNo-1);
					if(pStep2 == NULL)	continue;
	
					//파일을 한줄씩 읽어 들임(1초 간격으로 저장된 파일이여야 함)
					//이전에 1줄 읽어들인 data를 먼저 저장하기 위해 do~while사용 
					do 
					{
						//현재 결과 Step보다 작은 step의 모든 data는 지정 옵션으로 저장한다.
						//결과 file에 손실 data가 있을 경우 step이 변할 수 있다.
						if( nStep > pStepResult->nStepNo)	break;

						if(bSensorFileRead)	
						{
							//Sensor data도 한줄씩 읽어 들임
							bReadSensorData = sensorFile.ReadString(strSenosrData);
						}

						//Step이 변화된 구간
						if(nCurStep < nStep)
						{
							fStepTotTime += fTimeScan;
							fCurTime = 0.0;
						}
						
						pStep = pCondition->GetStep(nStep-1);
						
						//Skip the check step
						//Step 0 is check step;
						if(pStep == NULL)	continue;

						//현재 총시간은 이전 step의 총합 + 현재 Step 시간 
						fTimeSum = fStepTotTime + fTime;
						nCurStep  = nStep;
						//현재 Step의 최종 시간 Update
						fTimeScan = fTime;

						//Step type check
						if(pStep->m_type == EP_TYPE_CHARGE && m_SaveSetDlg.m_bSaveCharge == FALSE)			continue;
						if(pStep->m_type == EP_TYPE_DISCHARGE && m_SaveSetDlg.m_bSaveDischarge == FALSE)	continue;
						if(pStep->m_type == EP_TYPE_REST && m_SaveSetDlg.m_bSaveRest == FALSE)				continue;
						if(pStep->m_type == EP_TYPE_IMPEDANCE && m_SaveSetDlg.m_bSaveImpdeacne == FALSE)	continue;

						//완료 Step이나 OCV data는 실처리에서 가져오지 않고 step결과 data로 기록한다.
						if(pStep->m_type == EP_TYPE_END)	continue;
						if(pStep->m_type == EP_TYPE_OCV)	continue;

						//Delta time check
						//원본 data는 무조건 1초 간격으로 저장되어 있어야 한다.
						//절대 시간 delta로 변경 => 2006/4/26
						//if(m_SaveSetDlg.m_nDeltaTime > 0 && fabs(fTime - fCurTime) >= (float)m_SaveSetDlg.m_nDeltaTime)		//delta time save
						
						//1초간격으로 저장되어 있지 않거나 
						//중간 data가 손실되어 없을 경우 시간이 역전되는 경우가 발생한다.
						if(fTime > (fCurTime + 1.0f))
						{
							fCurTime = fTime - 0.5f;	//시간이 역전되면 현재 시간을 data 시간보다 0.5sec 작은 곳으로 이동  
						}
						
						if(m_SaveSetDlg.m_nDeltaTime > 0 && fTime >= fCurTime &&  fTime < (fCurTime + 1.0f))				//오차 범위 1초안 data 저장 
						{
							//결과 data와 시간이 중복되면 저장하지 않는다.
							if(nCurStep == pStepResult->nStepNo && (int)fTime == (int)pStepResult->fStepTime)	continue;	
							
							if(m_SaveSetDlg.m_bTimeSum)		fprintf(fpDest, "%s,",	m_SaveSetDlg.ValueString(fTimeSum, EP_TOT_TIME));
							if(m_SaveSetDlg.m_bStepNo)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(nStep, EP_STEP_NO));
							if(m_SaveSetDlg.m_bStepType)	fprintf(fpDest, "%s,", ::StepTypeMsg(pStep->m_type));
							if(m_SaveSetDlg.m_bStepTime)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fTime, EP_STEP_TIME));
							if(m_SaveSetDlg.m_bVoltage)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fVoltage, EP_VOLTAGE));
							if(m_SaveSetDlg.m_bCurrent)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fCurrent, EP_CURRENT));
							if(m_SaveSetDlg.m_bCapacity)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fCapacity, EP_CAPACITY));
							if(m_SaveSetDlg.m_bWatt)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fWatt, EP_WATT));
							if(m_SaveSetDlg.m_bWattHour)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fWattHour, EP_WATT_HOUR));
							if(bSensorFileRead && bReadSensorData && m_SaveSetDlg.m_bTempData)	fprintf(fpDest, "%s,", strSenosrData);
							fprintf(fpDest, "\n");
							//fCurTime = fTime;
							fCurTime = fCurTime + m_SaveSetDlg.m_nDeltaTime;
						}
						
						//Delta V Check
						if(m_SaveSetDlg.m_nDeltaV > 0 && fabs(fVoltage - fCurVoltage) >= (float)m_SaveSetDlg.m_nDeltaV)
						{
							if(m_SaveSetDlg.m_bTimeSum)		fprintf(fpDest, "%s,",	m_SaveSetDlg.ValueString(fTimeSum, EP_TOT_TIME));
							if(m_SaveSetDlg.m_bStepNo)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(nStep, EP_STEP_NO));
							if(m_SaveSetDlg.m_bStepType)	fprintf(fpDest, "%s,", ::StepTypeMsg(pStep->m_type));
							if(m_SaveSetDlg.m_bStepTime)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fTime, EP_STEP_TIME));
							if(m_SaveSetDlg.m_bVoltage)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fVoltage, EP_VOLTAGE));
							if(m_SaveSetDlg.m_bCurrent)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fCurrent, EP_CURRENT));
							if(m_SaveSetDlg.m_bCapacity)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fCapacity, EP_CAPACITY));
							if(m_SaveSetDlg.m_bWatt)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fWatt, EP_WATT));
							if(m_SaveSetDlg.m_bWattHour)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fWattHour, EP_WATT_HOUR));
							if(bSensorFileRead && bReadSensorData && m_SaveSetDlg.m_bTempData)	fprintf(fpDest, "%s,", strSenosrData);
							fprintf(fpDest, "\n");
							fCurVoltage = fVoltage;
						}

						//delta I Check
						if(m_SaveSetDlg.m_nDeltaI > 0 && fabs(fCurrent - fCurCurrent) >= (float)m_SaveSetDlg.m_nDeltaI )
						{
							if(m_SaveSetDlg.m_bTimeSum)		fprintf(fpDest, "%s,",	m_SaveSetDlg.ValueString(fTimeSum, EP_TOT_TIME));
							if(m_SaveSetDlg.m_bStepNo)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(nStep, EP_STEP_NO));
							if(m_SaveSetDlg.m_bStepType)	fprintf(fpDest, "%s,", ::StepTypeMsg(pStep->m_type));
							if(m_SaveSetDlg.m_bStepTime)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fTime, EP_STEP_TIME));
							if(m_SaveSetDlg.m_bVoltage)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fVoltage, EP_VOLTAGE));
							if(m_SaveSetDlg.m_bCurrent)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fCurrent, EP_CURRENT));
							if(m_SaveSetDlg.m_bCapacity)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fCapacity, EP_CAPACITY));
							if(m_SaveSetDlg.m_bWatt)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fWatt, EP_WATT));
							if(m_SaveSetDlg.m_bWattHour)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fWattHour, EP_WATT_HOUR));
							if(bSensorFileRead && bReadSensorData && m_SaveSetDlg.m_bTempData)	fprintf(fpDest, "%s,", strSenosrData);
							fprintf(fpDest, "\n");
							fCurCurrent = fCurrent;
						}
					
					} while(fscanf(fpSource, "%d,%f,%f,%f,%f,%f,%f,", &nStep, &fTime, &fVoltage, &fCurrent, &fCapacity, &fWatt, &fWattHour) == 7);
					
					//이전 Step의 최종 결과 data를 포함 시킴 
					if(m_SaveSetDlg.m_bTimeSum)		fprintf(fpDest, "!%s,",	m_SaveSetDlg.ValueString(fTimeSum, EP_TOT_TIME));
					if(m_SaveSetDlg.m_bStepNo)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->nStepNo, EP_STEP_NO));
					if(m_SaveSetDlg.m_bStepType)	fprintf(fpDest, "%s,", ::StepTypeMsg(pStep2->m_type));
					if(m_SaveSetDlg.m_bStepTime)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fStepTime, EP_STEP_TIME));
					if(m_SaveSetDlg.m_bVoltage)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fVoltage, EP_VOLTAGE));
					if(m_SaveSetDlg.m_bCurrent)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fCurrent, EP_CURRENT));
					if(m_SaveSetDlg.m_bCapacity)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fCapacity, EP_CAPACITY));
					if(m_SaveSetDlg.m_bWatt)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fWatt, EP_WATT));
					if(m_SaveSetDlg.m_bWattHour)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fWattHour, EP_WATT_HOUR));
					if(bSensorFileRead && bReadSensorData && m_SaveSetDlg.m_bTempData)	fprintf(fpDest, "%s,", strSenosrData);
					fprintf(fpDest, "\n");
					
				}

				fclose(fpSource);
				fclose(fpDest);
				if(bSensorFileRead)		sensorFile.Close();
			}
		}
		//////////////////////////////////////////////////////////////////////////
	}

	//Sensor data file copy
	//채널에 포함 시키므로 저장하지 않음	2006/04/26
// 	if(::CopyFile(strSensorSourceFile, strSensorDestFile, FALSE) == FALSE)	//overwrite copy
// 	{
// 		WriteLog(strDestFile + " create fail!!!");
// 	}
	m_downProgress.SetPos(100);
	m_downProgress.ShowWindow(SW_HIDE);
	
	return TRUE;
}
*/
/*
//strDir 위치에서 strDataDir 위치로 awSelch에서 선택한 채널을 지정옵션으로 저장한다.
BOOL CDataDownDlg::DownLoadLocalData(CString strDir, CString strSaveDir, CWordArray &awSelCh)
{
	int nChNo;
	CString strTemp;
	CString strSourceFile, strDestFile;
	CString strSensorSourceFile, strSensorDestFile;
	strSensorSourceFile.Format("%s\\Sensor.csv", strDir);
	strSensorDestFile.Format("%s\\Sensor.csv", strSaveDir);

	CWaitCursor wait;
	m_downProgress.ShowWindow(SW_SHOW);
	m_downProgress.SetPos(0);

	CTestCondition *pCondition = m_resultData.GetTestCondition();
	CStep *pStep;
	char szBuff[128];
	CString strSenosrData;

	FILE *fpSource, *fpDest;
	CStdioFile	sensorFile;

	for(int i=0; i<awSelCh.GetSize(); i++)
	{
		m_downProgress.SetPos(i*100/awSelCh.GetSize());

		nChNo = awSelCh.GetAt(i);
		strSourceFile.Format("%s\\ch%03d.csv", strDir, nChNo);
		strDestFile.Format("%s\\ch%03d.csv", strSaveDir, nChNo);

		//////////////////////////////////////////////////////////////////////////
		//Data copy start
		BOOL bSensorFileRead = sensorFile.Open(strSensorSourceFile, CFile::modeRead);
		fpSource = fopen(strSourceFile, "rt");
		if(fpSource)
		{
			fpDest = fopen(strDestFile, "wt");
			if(fpDest)
			{		
				//StepNo,t(sec),V(mV),I(mA),C(mAh),W(mW),Wh(mWh)
				//column header read //Skip Column Header
				fscanf(fpSource, "%s,%s,%s,%s,%s,%s,%s,", szBuff, szBuff, szBuff, szBuff, szBuff, szBuff, szBuff);
				//온도 Sesnor Header read
				if(bSensorFileRead)	sensorFile.ReadString(strSenosrData);

				//Write Column Header
				if(m_SaveSetDlg.m_bTimeSum)
				{
					strTemp = m_SaveSetDlg.UnitString(EP_TOT_TIME);
					if(strTemp.IsEmpty())
					{
						fprintf(fpDest, "TotTime,");
					}
					else
					{
						fprintf(fpDest, "TotTime(%s),",	strTemp);
					}
				}
				if(m_SaveSetDlg.m_bStepNo)		fprintf(fpDest, "StepNo,");
				if(m_SaveSetDlg.m_bStepType)		fprintf(fpDest, "Type,");
				if(m_SaveSetDlg.m_bStepTime)
				{
					strTemp = m_SaveSetDlg.UnitString(EP_STEP_TIME);
					if(strTemp.IsEmpty())
					{
						fprintf(fpDest, "Time,");
					}
					else
					{
						fprintf(fpDest, "Time(%s),",	strTemp);
					}
				}
				if(m_SaveSetDlg.m_bVoltage)		fprintf(fpDest, "Volt(%s),",	m_SaveSetDlg.UnitString(EP_VOLTAGE));
				if(m_SaveSetDlg.m_bCurrent)		fprintf(fpDest, "Crt(%s),",		m_SaveSetDlg.UnitString(EP_CURRENT));
				if(m_SaveSetDlg.m_bCapacity)	fprintf(fpDest, "Capa(%s),",	m_SaveSetDlg.UnitString(EP_CAPACITY));
				if(m_SaveSetDlg.m_bWatt)		fprintf(fpDest, "Watt(%s),",	m_SaveSetDlg.UnitString(EP_WATT));
				if(m_SaveSetDlg.m_bWattHour)	fprintf(fpDest, "WattHour(%s),", m_SaveSetDlg.UnitString(EP_WATT_HOUR));
				if(bSensorFileRead && m_SaveSetDlg.m_bTempData)	fprintf(fpDest, "%s,", strSenosrData);
				fprintf(fpDest, "\n");
				
				int nCurStep = 0, nStep, nEndSaveStep = 0;
				float fTime, fVoltage, fCurrent, fCapacity, fWatt, fWattHour;
				float fCurTime = 0.0f, fCurVoltage = 0.0f, fCurCurrent = 0.0f, fTimeSum = 0.0f, fStepTotTime = 0.0f, fTimeScan = 0.0f;
				CString strTime, strVoltage, strCurrent, strCapacity, strWatt, strWattHour;
				BOOL	bReadSensorData = FALSE;

				
				//파일을 한줄씩 읽어 들임(1초 간격으로 저장된 파일이여야 함)
				while(fscanf(fpSource, "%d,%f,%f,%f,%f,%f,%f,", &nStep, &fTime, &fVoltage, &fCurrent, &fCapacity, &fWatt, &fWattHour) == 7)
				{
					if(bSensorFileRead)	
					{
						//Sensor data도 한줄씩 읽어 들임
						bReadSensorData = sensorFile.ReadString(strSenosrData);
					}

					//Step이 변화된 구간
					if(nCurStep < nStep)
					{
						fStepTotTime += fTimeScan;
						fCurTime = 0.0;

						//이전 Step의 최종 결과 data를 포함 시킴 
						STR_STEP_RESULT *pStepData = m_resultData.GetStepData(nCurStep-1);
						if(pStepData != NULL)
						{
							STR_SAVE_CH_DATA *pStepResult = (STR_SAVE_CH_DATA *)pStepData->stepData.chData[nChNo-1];
							if(m_SaveSetDlg.m_bTimeSum)		fprintf(fpDest, "%s,",	m_SaveSetDlg.ValueString(fTimeSum, EP_TOT_TIME));
							if(m_SaveSetDlg.m_bStepNo)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(nCurStep, EP_STEP_NO));
							if(m_SaveSetDlg.m_bStepType)	fprintf(fpDest, "%s,", ::StepTypeMsg(pStep->m_type));
							if(m_SaveSetDlg.m_bStepTime)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fStepTime, EP_STEP_TIME));
							if(m_SaveSetDlg.m_bVoltage)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fVoltage, EP_VOLTAGE));
							if(m_SaveSetDlg.m_bCurrent)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fCurrent, EP_CURRENT));
							if(m_SaveSetDlg.m_bCapacity)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fCapacity, EP_CAPACITY));
							if(m_SaveSetDlg.m_bWatt)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fWatt, EP_WATT));
							if(m_SaveSetDlg.m_bWattHour)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(pStepResult->fWattHour, EP_WATT_HOUR));
							if(bSensorFileRead && bReadSensorData && m_SaveSetDlg.m_bTempData)	fprintf(fpDest, "%s,", strSenosrData);
							fprintf(fpDest, "\n");
						}
					}
					
					pStep = pCondition->GetStep(nStep-1);
					if(pStep == NULL)	continue;

					//현재 총시간은 이전 step의 총합 + 현재 Step 시간 
					fTimeSum = fStepTotTime + fTime;
					nCurStep  = nStep;
					fTimeScan = fTime;

					//Step type check
					if(pStep->m_type == EP_TYPE_CHARGE && m_SaveSetDlg.m_bSaveCharge == FALSE)			continue;
					if(pStep->m_type == EP_TYPE_DISCHARGE && m_SaveSetDlg.m_bSaveDischarge == FALSE)	continue;
					if(pStep->m_type == EP_TYPE_REST && m_SaveSetDlg.m_bSaveRest == FALSE)				continue;
					if(pStep->m_type == EP_TYPE_IMPEDANCE && m_SaveSetDlg.m_bSaveImpdeacne == FALSE)	continue;
		
					//Delta time check
					//원본 data는 무조건 1초 간격으로 저장되어 있어야 한다.
					//절대 시간 delta로 변경 => 2006/4/26
//					if(m_SaveSetDlg.m_nDeltaTime > 0 && fabs(fTime - fCurTime) >= (float)m_SaveSetDlg.m_nDeltaTime)		//delta time save
					if(m_SaveSetDlg.m_nDeltaTime > 0 && fTime >= fCurTime &&  fTime < (fCurTime + 1.0f))				//오차 범위 1초안 data 저장 
					{
						if(m_SaveSetDlg.m_bTimeSum)		fprintf(fpDest, "%s,",	m_SaveSetDlg.ValueString(fTimeSum, EP_TOT_TIME));
						if(m_SaveSetDlg.m_bStepNo)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(nStep, EP_STEP_NO));
						if(m_SaveSetDlg.m_bStepType)	fprintf(fpDest, "%s,", ::StepTypeMsg(pStep->m_type));
						if(m_SaveSetDlg.m_bStepTime)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fTime, EP_STEP_TIME));
						if(m_SaveSetDlg.m_bVoltage)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fVoltage, EP_VOLTAGE));
						if(m_SaveSetDlg.m_bCurrent)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fCurrent, EP_CURRENT));
						if(m_SaveSetDlg.m_bCapacity)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fCapacity, EP_CAPACITY));
						if(m_SaveSetDlg.m_bWatt)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fWatt, EP_WATT));
						if(m_SaveSetDlg.m_bWattHour)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fWattHour, EP_WATT_HOUR));
						if(bSensorFileRead && bReadSensorData && m_SaveSetDlg.m_bTempData)	fprintf(fpDest, "%s,", strSenosrData);
						fprintf(fpDest, "\n");
						//fCurTime = fTime;
						fCurTime = fCurTime + m_SaveSetDlg.m_nDeltaTime;
					}
					
					//Delta V Check
					if(m_SaveSetDlg.m_nDeltaV > 0 && fabs(fVoltage - fCurVoltage) >= (float)m_SaveSetDlg.m_nDeltaV)
					{
						if(m_SaveSetDlg.m_bTimeSum)		fprintf(fpDest, "%s,",	m_SaveSetDlg.ValueString(fTimeSum, EP_TOT_TIME));
						if(m_SaveSetDlg.m_bStepNo)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(nStep, EP_STEP_NO));
						if(m_SaveSetDlg.m_bStepType)	fprintf(fpDest, "%s,", ::StepTypeMsg(pStep->m_type));
						if(m_SaveSetDlg.m_bStepTime)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fTime, EP_STEP_TIME));
						if(m_SaveSetDlg.m_bVoltage)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fVoltage, EP_VOLTAGE));
						if(m_SaveSetDlg.m_bCurrent)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fCurrent, EP_CURRENT));
						if(m_SaveSetDlg.m_bCapacity)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fCapacity, EP_CAPACITY));
						if(m_SaveSetDlg.m_bWatt)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fWatt, EP_WATT));
						if(m_SaveSetDlg.m_bWattHour)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fWattHour, EP_WATT_HOUR));
						if(bSensorFileRead && bReadSensorData && m_SaveSetDlg.m_bTempData)	fprintf(fpDest, "%s,", strSenosrData);
						fprintf(fpDest, "\n");
						fCurVoltage = fVoltage;
					}

					//delta I Check
					if(m_SaveSetDlg.m_nDeltaI > 0 && fabs(fCurrent - fCurCurrent) >= (float)m_SaveSetDlg.m_nDeltaI )
					{
						if(m_SaveSetDlg.m_bTimeSum)		fprintf(fpDest, "%s,",	m_SaveSetDlg.ValueString(fTimeSum, EP_TOT_TIME));
						if(m_SaveSetDlg.m_bStepNo)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(nStep, EP_STEP_NO));
						if(m_SaveSetDlg.m_bStepType)	fprintf(fpDest, "%s,", ::StepTypeMsg(pStep->m_type));
						if(m_SaveSetDlg.m_bStepTime)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fTime, EP_STEP_TIME));
						if(m_SaveSetDlg.m_bVoltage)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fVoltage, EP_VOLTAGE));
						if(m_SaveSetDlg.m_bCurrent)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fCurrent, EP_CURRENT));
						if(m_SaveSetDlg.m_bCapacity)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fCapacity, EP_CAPACITY));
						if(m_SaveSetDlg.m_bWatt)		fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fWatt, EP_WATT));
						if(m_SaveSetDlg.m_bWattHour)	fprintf(fpDest, "%s,", m_SaveSetDlg.ValueString(fWattHour, EP_WATT_HOUR));
						if(bSensorFileRead && bReadSensorData && m_SaveSetDlg.m_bTempData)	fprintf(fpDest, "%s,", strSenosrData);
						fprintf(fpDest, "\n");
						fCurCurrent = fCurrent;
					}
				}

				fclose(fpSource);
				fclose(fpDest);
				if(bSensorFileRead)		sensorFile.Close();
			}
		}
		//////////////////////////////////////////////////////////////////////////
	}

	//Sensor data file copy
	//채널에 포함 시키므로 저장하지 않음	2006/04/26
// 	if(::CopyFile(strSensorSourceFile, strSensorDestFile, FALSE) == FALSE)	//overwrite copy
// 	{
// 		WriteLog(strDestFile + " create fail!!!");
// 	}
	m_downProgress.SetPos(100);
	m_downProgress.ShowWindow(SW_HIDE);
	
	return TRUE;
}
*/
void CDataDownDlg::OnDelAllButton() 
{
	// TODO: Add your control notification handler code here
	m_LogList.ResetContent();
}

void CDataDownDlg::WriteLog(CString strMsg)
{
	COleDateTime tTime = COleDateTime::GetCurrentTime();

	if(m_LogList.GetCount() > 100)
	{
		m_LogList.ResetContent();
	}

	CString strTemp;
	strTemp.Format("%s :: %s", tTime.Format("%Y/%m/%d %H:%M:%S"), strMsg);
	m_LogList.AddString(strTemp);
	m_LogList.SetCurSel(m_LogList.GetCount()-1);
	WriteLog( strMsg, FILE_APPEND );
}

void CDataDownDlg::OnSelectAllButton() 
{
	// TODO: Add your control notification handler code here
	m_listBox.SelItemRange(TRUE, 0, m_listBox.GetCount());
//	GetDlgItem(IDC_STATIC_SEL_CH)->SetWindowText(strTemp);
}

void CDataDownDlg::OnDeselAllButton() 
{
	// TODO: Add your control notification handler code here
	m_listBox.SelItemRange(FALSE, 0, m_listBox.GetCount());
}


BOOL CDataDownDlg::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct) 
{
	// TODO: Add your message handler code here and/or call default
	// CTSMon에서 전달된 Message
	// End Step 전송시 결과 data를 자동 Download하여 저장한다.
	if(pCopyDataStruct->dwData == 2)
	{
		if(pCopyDataStruct->cbData > 0)	
		{
			//결과 파일명 
			char *pData = new char[pCopyDataStruct->cbData];
			memcpy(pData, pCopyDataStruct->lpData, pCopyDataStruct->cbData);
			m_strUserOpenFileName = pData;
			delete [] pData;			

			WriteLog(m_strUserOpenFileName + TEXT_LANG[20]); //100092

			SetTimer(TIMER_ID_USERINTERFACE, TIMER_INTERVAL_USERINTERFACE, NULL);
		}
		else
		{
			m_strUserOpenFileName.Empty();
		}
	}

	//지정 파일을 사용자 Interface로 표시 
	if(pCopyDataStruct->dwData == 3)
	{
		if(pCopyDataStruct->cbData > 0)	
		{
			//결과 파일명 
			CString strName;
			char *pData = new char[pCopyDataStruct->cbData];
			memcpy(pData, pCopyDataStruct->lpData, pCopyDataStruct->cbData);
			strName = pData;
			delete [] pData;

			m_cs.Lock();

			STR_GRAPH_DATA *pResultData;
			pResultData = new STR_GRAPH_DATA;
			sprintf(pResultData->szResultFile, "%s", strName);
			pResultData->bExcuteGraphAnalyzer = FALSE;
			pResultData->nRetryCnt = 0;
			m_apGraphResultList.Add( pResultData );			

			m_cs.Unlock();
		}
	}
	return CDialog::OnCopyData(pWnd, pCopyDataStruct);
}

void CDataDownDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	//Hide Timer
	switch( nIDEvent )
	{
	case TIMER_ID_SHOWMODE:
		{
			KillTimer( TIMER_ID_SHOWMODE );
			ShowWindow(SW_HIDE);
		}		
		break;

	case TIMER_ID_AUTOSAVE:
		{
			KillTimer( TIMER_ID_AUTOSAVE );
			if( !m_bWork )
			{	
				CString strName;
				CString strLog;
				
				m_bWork = true;

				if( m_apGraphResultList.GetSize() > 0 )
				{
					STR_GRAPH_DATA* pResultData;
					pResultData = (STR_GRAPH_DATA*)m_apGraphResultList.GetAt(0);					
					
					strName.Format("%s", pResultData->szResultFile);
					strLog.Format(TEXT_LANG[21], strName);
					WriteLog(strLog);
					
					if( DownLoadAutoMatic(strName) == TRUE )
					{
						m_apGraphResultList.RemoveAt(0);
						delete pResultData;
						pResultData = NULL;
					}
					else
					{
						// 1. 3회 재 시도후 실패시 데이터 삭제
						if( pResultData->nRetryCnt > 2 ) 
						{
							m_apGraphResultList.RemoveAt(0);
							delete pResultData;
							pResultData = NULL;
						}
						else
						{
							pResultData->nRetryCnt++;
							strLog.Format(TEXT_LANG[22], strName, pResultData->nRetryCnt);
							WriteLog(strLog);
						}						
					}
				}
				
				if(m_strAutoFileList.GetCount() > 0)
				{
					strName = m_strAutoFileList.GetHead();
					m_strAutoFileList.RemoveHead();
					TRACE("===>%s file download start\n", strName);
					//Progress wnd를 뛰운다.
					//////////////////////////////////////////////////////////////////////////
					if( DownLoadAutoMatic(strName) )
					{
						// ExecuteGraphAnalyzer(strName.Left(strName.ReverseFind('.')));
					}
				}				
				
				//////////////////////////////////////////////////////////////////////////
				//20090706 KBH
				//File 결과 파일명을 가져온다.
				CString strListName = GetNextTargetListFile_new();			// 가장 우선순의 파일을 선택한다.
				
				if(strListName.IsEmpty())
				{
					// strLog.Format("★★★★★★★★★★★★★★★★===>Download 대기 파일이 없습니다.");
					// WriteLog(strLog);
					m_bWork = FALSE;
					SetTimer(TIMER_ID_AUTOSAVE, TIMER_INTERVAL_AUTOSAVE, NULL);			
					return;
				}
				else
				{
					strLog.Format(TEXT_LANG[23], strListName);
					WriteLog(strLog);					

					strName = _T("");
					
					CProfileListInfo profileInfo;

					if( profileInfo.LoadData(strListName) )				// 만약 실패하면?
					{
						strName = profileInfo.GetResultFileName();

						strLog.Format("====> ResultFile [%s], Unit : [%s], SavePath : [%s], TrayId : [%s] <====", strName, profileInfo.GetUnitName(), profileInfo.GetSavePath(), profileInfo.GetTrayID() );
						
						WriteLog(strLog);

						if(strName.IsEmpty() == FALSE)					// offline mode
						{
							// 1. Local mode 의 경우
							if( DownLoadAutoMatic(strName, profileInfo.GetSavePath()) )			// 접속이 되면 리스트 파일 삭제한다. (download 실패 하여도 삭제)
							{
								if( unlink( strListName ) < 0 )			// 0:Success, -1:Fail
								{
									WriteLog(TEXT_LANG[24]+ strListName);									
								}
								else {
									WriteLog(TEXT_LANG[25]+ strListName);
								}
							}
							else	
							{
								SetPrimary(strListName);				// 접속 실패시 우선 순위를 가장 마지막으로 재설정한다.
							}					
						}
						else
						{					
							// 2. Automatic mode 의 경우
							if(DownLoadOnLineModeProfile(profileInfo.GetUnitID(), profileInfo.GetSavePath(), profileInfo.GetTrayID(), profileInfo.GetUnitName()))
							{
								if( unlink( strListName ) < 0 )		// 0:Success, -1:Fail
								{
									WriteLog(TEXT_LANG[24]+strListName);									
								}
								else {
									WriteLog(TEXT_LANG[25]+strListName);
								}								
							}
							else {
								SetPrimary(strListName);
							}
						}
					}
					else
					{
						unlink( strListName );
						WriteLog(TEXT_LANG[25] + strListName);
					}					

					m_bWork = FALSE;
				}	
			}

			SetTimer(TIMER_ID_AUTOSAVE, TIMER_INTERVAL_AUTOSAVE, NULL);			
		}
		break;

	case TIMER_ID_USERINTERFACE:
		{
			KillTimer( TIMER_ID_USERINTERFACE );

			ShowWindow( SW_SHOW );		

			if( m_strUserOpenFileName.IsEmpty() )
			{
				OnGetList();
			}
			else
			{
				if( DownLoadAutoMatic( m_strUserOpenFileName) )
				{
					GetChannelList(m_strUserOpenFileName);
				}
			}

			m_bSearchModeOffLine = TRUE;
			
			GetDlgItem(IDC_OPTION_BUTTON)->EnableWindow(TRUE);
			GetDlgItem(IDC_RADIO1)->EnableWindow(TRUE);
			GetDlgItem(IDC_RADIO2)->EnableWindow(TRUE);
			GetDlgItem(IDC_STATIC_FORMAT)->EnableWindow(TRUE);

			m_strUserOpenFileName.Empty();
		}
		break;
	}
	CDialog::OnTimer(nIDEvent);
}

CString CDataDownDlg::LoadFormResultData(CString strFileName)
{
	m_strUserOpenFileName = strFileName;
	
	GetDlgItem(IDC_RESULT_FILE_EDIT)->SetWindowText(strFileName);

	CString strTemp, strDest;
	
	//결과 파일에서 시험한 모듈의 IP와 시험명을 추출한다.
	if(m_resultData.ReadFile(strFileName) <= 0)
	{
		return "";
	}
	
	RESULT_FILE_HEADER *lpResultFileHeader = m_resultData.GetResultHeader();

	m_strIPAddress = lpResultFileHeader->szModuleIP;

	// CString strTestSerial(lpResultFileHeader->szTestSerialNo);
	CString strTestSerial = _T("");
	COleDateTime timeDate;
	timeDate.ParseDateTime(lpResultFileHeader->szDateTime);
	strTestSerial = timeDate.Format("%Y%m%d%H%M%S");

	CTestCondition *pCon = m_resultData.GetTestCondition();
	if(pCon)
	{
		m_strTestName = pCon->GetTestName();
	}
	m_nModuleID = m_resultData.GetModuleID();
	strTemp.Format(TEXT_LANG[26], m_nModuleID, m_strIPAddress, m_strTestName); // 100093
	WriteLog(strTemp);

	strTemp.LoadString(IDS_TEXT_DOWN_LOAD_DATA);
	CString strName(m_resultData.GetModuleName());
	if(strName.IsEmpty())
	{
		strDest.Format("\nModule %d : %s", m_nModuleID, m_strIPAddress);
	}
	else
	{
		strDest.Format("\n%s : %s", strName, m_strIPAddress);
	}
	GetDlgItem(IDC_TITLE_STATIC)->SetWindowText(strTemp+strDest);
	return strTestSerial;
}

// 저장 구조
// c:\설치폴더\DataTemp\M001\testserial1\ch001.csv
//					   |	|		    |ch002.csv
//					   |    |				:
//					   |	|				: Install Channel Number
//					   |	+testserial2
//					   |			: 
//					   |			: Max BF_MAX_TEST_REAL_TIME_DATA(20)
//					   +M002
//						 :
//						 : Install Module Number

//Backgroud로 실행되어 NonCell이 아니 모든 Cell Data를 Linux에서 Download한다.
BOOL CDataDownDlg::DownLoadAutoMatic(CString strFileName, CString strSavePath )
{
	//1. 결과 파일 Loading
	CString strTemp, strDest;
	CString strLog;
	CFormResultFile rltFile;

	//결과 파일에서 시험한 모듈의 IP와 시험명을 추출한다.
	if(rltFile.ReadFile(strFileName) <= 0)
	{
		return TRUE;
	}
	
	RESULT_FILE_HEADER *lpResultFileHeader = rltFile.GetResultHeader();
	CString strIPAddress(lpResultFileHeader->szModuleIP);
	CString strTestSerialNo(rltFile.GetTestSerialNo());
	CTestCondition *pCon = rltFile.GetTestCondition();
	EP_MD_SYSTEM_DATA *pSysData = rltFile.GetMDSysData();

	strTemp.Format(TEXT_LANG[27], rltFile.GetModuleID(), strIPAddress, pCon->GetTestName());
	WriteLog(strTemp);

	if(strTestSerialNo.IsEmpty())
	{
		WriteLog(TEXT_LANG[28] + strFileName);
		return TRUE;
	}
	
	strTemp.Format("Search AutoMatic test code [" + strTestSerialNo + "] from %s(%s) on DownLoadAutoMatic", rltFile.GetModuleName(), strIPAddress);
	WriteLog(strTemp);		

	//2. 현재 선택한 모듈에서 주어진 serial no의 결과를 모듈에서 검색한다. get remote file location
	CStringList chList;			//remote channel list
	CStringList sensorChList;
	CStringList chFadmList;
	CString strRemoteFileLocation = FindRealTimeDataFromRemote(strIPAddress, pSysData->nModuleGroupNo, strTestSerialNo, chList, sensorChList, chFadmList);

	//접속에 실패한 경우 
	if(strRemoteFileLocation.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[29], strTestSerialNo, strIPAddress); //100094
		WriteLog(strTemp);
		return TRUE;
	}

	//Linux 목록에서 이미 삭제된 파일 
	if(chList.GetCount() < 1 && chFadmList.GetCount() < 1)
	{
		strTemp.Format(TEXT_LANG[30], strTestSerialNo, strIPAddress); //100095
		WriteLog(strTemp);
		return TRUE;
	}

	//3. 검색한 remote channel을  list 한다.
	CDWordArray adwSelCh;
	CDWordArray awFadmSelCh;
	CWordArray aSelTrayCh;
	int nModuleChNo, nTrayCh, nStepNum;
	BYTE code;
	POSITION pos = chList.GetHeadPosition();
	while(pos)
	{
		strTemp = chList.GetNext(pos);		//format => ch001
		nModuleChNo = atol(strTemp.Right(3));
		nTrayCh = rltFile.ConvertModuleCh2TrayCh(nModuleChNo);	//Convert module base channel no to Tray base channel no
		
		//해당되는 Channel만 DownLoad한다.
		code = rltFile.GetLastTrayChCellCode(nTrayCh-1);	//해당 모듈 Channel이 있는지 확인 
		//Check Error
		if(code != EP_CODE_NONCELL && code != EP_CODE_CELL_NONE && code != EP_CODE_CELL_EXIST)
		{
			//자동 DownLoad는 표시하지 않음 
			//strTemp.Format("CH %03d  [%03d]", nTrayCh, nMdCh);
			//m_listBox.AddString(strTemp);
			//m_listBox.SetItemData(m_listBox.GetCount()-1, nChNo);			//Channel No
			adwSelCh.Add(MAKELONG(nTrayCh, nModuleChNo));
			aSelTrayCh.Add((WORD)nTrayCh);
//			TRACE("Ch %d Added\n", nModuleChNo);
		}
	}

	pos = chFadmList.GetHeadPosition();
	while(pos)
	{
		strTemp = chFadmList.GetNext(pos);		//format => ch001
		
		nModuleChNo = atol(strTemp.Mid(strTemp.ReverseFind('_')-3,3));
		nTrayCh = rltFile.ConvertModuleCh2TrayCh(nModuleChNo);	//Convert module base channel no to Tray base channel no
		
		nStepNum = atol(strTemp.Right(2));

		awFadmSelCh.Add(MAKELONG(nTrayCh, nStepNum));
	}

	
	//표시된 전체 선택 
	//m_listBox.SelItemRange(TRUE, 0, m_listBox.GetCount());

	//4. 모듈 폴더 생성 및 최근 20목록 확인 
	//strTemp.Format("%s\\MD%03d", m_strDataTempPath, rltFile.GetModuleID());
	if( strSavePath.IsEmpty() )
	{
		strTemp = strFileName.Left(strFileName.ReverseFind('.'));
	}
	else
	{
		strTemp = strSavePath;
	}
	
	if(AddFolderList(strTemp) == FALSE)
	{
		strLog.Format(TEXT_LANG[31], strTemp);
		WriteLog(strLog);
		return FALSE;
	}
	else
	{
		//5. 지정 위치에 download 실시 
		//strTemp.Format("%s\\MD%03d\\%s", m_strDataTempPath, rltFile.GetModuleID(), lpResultFileHeader->szTestSerialNo);

		//Search channel file and download to destination location		
		//source dir(remote), dest dir(local), selected ch, ip
		
		CWordArray awSensorCh;
		//사용중인 Sensor Channel만 선택한다.
		BOOL bSensorAllIncluded = FALSE;
		if(bSensorAllIncluded)
		{
			for(int index = 0; index < EP_MAX_SENSOR_CH; index++)
			{
				int nChNo = index+1;
				awSensorCh.Add((WORD)nChNo);
			}
		}
		else
		{
			GetUsedSensorCh(aSelTrayCh, ALL_SENSOR, awSensorCh, &rltFile);
		}

		if(DownLoadRemoteDataA( strIPAddress, strRemoteFileLocation, strTemp, adwSelCh, awSensorCh, awFadmSelCh) == FALSE)
		{
			strTemp.Format(TEXT_LANG[32], strRemoteFileLocation);
			WriteLog(strTemp);
			return FALSE;
		}
	}

	strTemp.Format(TEXT_LANG[33], strFileName.Mid(strFileName.ReverseFind('\\')+1), adwSelCh.GetSize());
	WriteLog(strTemp);		//100096
	return TRUE;
}

//각 모듈 폴더에 download된 시험결과 목록이 BF_MAX_TEST_REAL_TIME_DATA(20)보다 많으면 가장 
//오래전에 생성된 폴더를 삭제하고 새롭게 저장할 폭더를 생성한다.
BOOL CDataDownDlg::AddFolderList(CString strCurFolderName)
{
	int nCount = 0;
	CString strTemp;

	//모듈 폴더가 없으면 생성한다. MD0001 폴더 
	strCurFolderName = MakeMyFolder(strCurFolderName);
	return !strCurFolderName.IsEmpty();
	
	//모듈 폴더에서 저장된 결과 목록의 수를 확인한다.
/*	CFileFind aDirChecker;
	CTime minTime(CTime::GetCurrentTime()), tempTime;
	CString strMinTimeFolder;
	BOOL bFind = aDirChecker.FindFile(strCurFolderName+"\\*");
	while(bFind)
	{
		bFind = aDirChecker.FindNextFile();
		if(aDirChecker.IsDirectory() && !aDirChecker.IsDots())		//Folder count
		{
			nCount++;
			if(aDirChecker.GetCreationTime(tempTime))
			{
				if(minTime > tempTime)
				{
					minTime = tempTime;
					strMinTimeFolder = aDirChecker.GetFilePath();
				}
			}
		}
	}
	aDirChecker.Close();

	//최대 임시 저장수 설정보다 많을 경우 가장 오래 전에 생성된 폴더 삭제 
	if(nCount > AfxGetApp()->GetProfileInt(REG_SEC_NAME,"HistoryCount", BF_MAX_TEST_REAL_TIME_DATA))
	{
		//지정수만 남기고 삭제 한다.
		if(DeletePath(strMinTimeFolder) == FALSE)
		{
			WriteLog(strMinTimeFolder + "를 삭제할 수 없습니다.");
			return FALSE;
		}
	}

	//새롭게 저장할 폴더를 생성한다.
	strTemp = strCurFolderName + "\\" + strNewName;
	strTemp = MakeMyFolder(strTemp);
	return !strTemp.IsEmpty();
*/
}

BOOL CDataDownDlg::DeletePath(CString strPath)
{
	if(strPath.IsEmpty())	return TRUE;
    
	CFileFind finder;
    BOOL bContinue = TRUE;

	CString strTemp(strPath);
    if(strTemp.Right(1) != _T("\\"))
        strTemp += _T("\\");

    strTemp += _T("*.*");
    bContinue = finder.FindFile(strTemp);
    while(bContinue)
    {
        bContinue = finder.FindNextFile();
        if(finder.IsDots()) // Ignore this item.
        {
            continue;
        }
        else if(finder.IsDirectory()) // Delete all sub item.
        {
            if(DeletePath(finder.GetFilePath()) == FALSE)	return FALSE;
        }
        else // Delete file.
        {
            if(::DeleteFile((LPCTSTR)finder.GetFilePath()) == 0)
			{
				return FALSE;
			}
        }
    }
    finder.Close();

    if(::RemoveDirectory((LPCTSTR)strPath) == 0)
		return FALSE;
	
	return TRUE;

/*	char szBuff[_MAX_PATH];
	sprintf(szBuff, "%s", strTemp);
	LPSHFILEOPSTRUCT lpFileOp = new SHFILEOPSTRUCT;
	ZeroMemory(lpFileOp, sizeof(SHFILEOPSTRUCT));
	lpFileOp->wFunc = FO_DELETE;
	lpFileOp->pTo = szBuff;
	lpFileOp->fFlags = FOF_SILENT|FOF_NOERRORUI;
	
	if(SHFileOperation(lpFileOp) != 0)
	{
		return FALSE;
	}

	return TRUE;
*/
}

LRESULT CDataDownDlg::OnTrayNotification(WPARAM wParam, LPARAM lParam)
{
	return m_TrayIcon.OnTrayNotification(wParam, lParam);
}

void CDataDownDlg::OnShowDlg() 
{
	// TODO: Add your command handler code here
	ShowWindow(SW_SHOW);
}

void CDataDownDlg::OnExit() 
{
	// TODO: Add your command handler code here
	if( m_apGraphResultList.GetSize() > 0 )
	{
		//그래프 데이터를 다운받는 중입니다. 프로그램을 종료 하시겠습니까?
		if( (TEXT_LANG[17], "DataDownLoading...",  MB_ICONWARNING|MB_OKCANCEL) != IDOK ) //100097
		{
			return;
		}
	}	
	
	m_apGraphResultList.RemoveAll();
	m_apGraphResultList.FreeExtra();

	CDialog::OnOK();
}

void CDataDownDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	ShowWindow(SW_HIDE);	
//	CDialog::OnCancel();
}

BOOL CDataDownDlg::GetChannelList(CString strFileName)
{
	CString strTemp = _T("");
	CString strTestSerial = _T("");
	
	strTestSerial = LoadFormResultData(strFileName);
	if(strTestSerial.IsEmpty())
	{
		AfxMessageBox(strFileName+TEXT_LANG[64]); //100098
		return FALSE;
	}
	
	//file list check
	if(GetFolderList(strTestSerial))
	{
		GetDlgItem(IDC_BUTTON1)->EnableWindow(TRUE);
		GetDlgItem(IDC_SELECT_ALL_BUTTON)->EnableWindow(TRUE);
		GetDlgItem(IDC_DESEL_ALL_BUTTON)->EnableWindow(TRUE);
	}
	else
	{
		int size = AfxGetApp()->GetProfileInt(REG_SEC_NAME,"HistoryCount", BF_MAX_TEST_REAL_TIME_DATA);
		//Profie Data를 찾을 수 없습니다. 저장 조건이 설정되지 않았거나 모듈별로 %d개 이상이 되어 자동 삭제 되었습니다."
		strTemp.Format(TEXT_LANG[18], size); //100099
		AfxMessageBox(strTemp);
		return FALSE;
	}

	OnSelectAllButton();
	OnSelchangeDataList();

	return TRUE;
}

//Sensor data를 저장한다. 
//모든 Sensor channel이 모두 동일한 Time에 저장되어야 한다.
BOOL CDataDownDlg::MakeSensorExcelFile(CStringList *pFileList, CString strFileName, CWordArray &awSensorCh)
{
	if(pFileList == NULL || strFileName.IsEmpty())	return FALSE;
	if(awSensorCh.GetSize() < 1)		return TRUE;

	FILE *fpList[EP_MAX_SENSOR_CH] = {NULL,};
	FILE *fpSensorT = NULL;
	FILE *fpSensorV = NULL;

	CString strRemoteFile[EP_MAX_SENSOR_CH];
	POSITION pos;
	POSITION pos1[EP_MAX_SENSOR_CH] = {NULL,};
	
	CString strTemp;
	strTemp.Format("%s\\SensorT.csv", strFileName);
	fpSensorT = fopen(strTemp, "wt");
	if(fpSensorT == NULL)		return FALSE;

	strTemp.Format("%s\\SensorV.csv", strFileName);
	fpSensorV = fopen(strTemp, "wt");
	if(fpSensorV == NULL)		return FALSE;

	int nMaxSizeChIndex = 0;
	int nMaxSize = 0;
	//가장 큰 Size의 채널을 구한다.
	for(int index = 0; index < EP_MAX_SENSOR_CH; index++)
	{
		if(nMaxSize < pFileList[index].GetCount())
		{
			nMaxSizeChIndex = index;
			nMaxSize = pFileList[index].GetCount();
		}
		pos1[index] = pFileList[index].GetHeadPosition();

		if(pos1[index])
		{
			fprintf(fpSensorT, "T_CH%02d,", index+1);	
			fprintf(fpSensorV, "V_CH%02d,", index+1);	
		}
	}
	fprintf(fpSensorT, "\n");	
	fprintf(fpSensorV, "\n");	

	//각채널을 Time동기 시키면서 1개의 파일에 저장한다.
	int nData0, nData1, nData2;
	//가장 큰 Size 만큼 반복 
	pos = pFileList[nMaxSizeChIndex].GetHeadPosition();
	while(pos)	
	{
		//반복 횟수만 지정
		pFileList[nMaxSizeChIndex].GetNext(pos);

		//File Open
		BOOL bRun = FALSE;
		for(int index = 0; index < EP_MAX_SENSOR_CH; index++)
		{	
			if(pos1[index])
			{
				strRemoteFile[index] = pFileList[index].GetNext(pos1[index]);
			}
			else
			{
				strRemoteFile[index].Empty();
			}

			if(strRemoteFile[index].IsEmpty())
			{
				fpList[index] = NULL;
			}
			else
			{
				fpList[index] = fopen(strRemoteFile[index], "rt");
				if(fpList[index]) 
					bRun = TRUE;
			}
		}

		while(bRun)
		{
			for(index = 0; index < EP_MAX_SENSOR_CH; index++)
			{
				if(fpList[index])
				{
					if(fscanf(fpList[index], "%d, %d, %d", &nData0, &nData1, &nData2) < 2)
					{
						bRun = FALSE;		//file을 다 읽음 
					}
					else
					{
						fprintf(fpSensorT, "%f,", ETC_PRECISION(nData1));	//Temperature
						fprintf(fpSensorV, "%f,", VTG_PRECISION(nData2));	//Voltage
					}
				}
				// else
				// {
				//	fprintf(fpSensorT, ",");
				//	fprintf(fpSensorV, ",");
				// }

			}
			fprintf(fpSensorT, "\n");
			fprintf(fpSensorV, "\n");
		}

		for(index = 0; index < EP_MAX_SENSOR_CH; index++)
		{
			if(fpList[index])	fclose(fpList[index]);
		}
	}
	fclose(fpSensorT);
	fclose(fpSensorV);

	return TRUE;
}

void CDataDownDlg::OnOptionButton() 
{
	// TODO: Add your control notification handler code here
//	m_SaveSetDlg.m_strTempDir = m_strDataTempPath;
	if(m_SaveSetDlg.DoModal() ==IDOK)
	{
//		m_strDataTempPath = m_SaveSetDlg.m_strTempDir;
	}
}

BOOL CDataDownDlg::MakeNextFile(FILE *fp, int &nFileCount, CString strFile)
{
	ASSERT(fp);
	fclose(fp);
	fp = NULL;
	
	//파일 이름에 (1)을 붙인다.
	//1) 파일 이름 추출 

	CString strTemp;
	int n = strFile.ReverseFind('\\');
	CString strFileName = strFile.Mid(n+1);
	CString strPathName = strFile.Left(n+1);
	
	int m = strFileName.ReverseFind('.');
	if(m<0)
	{
		strTemp.Format("%s(%d)", strFileName, nFileCount+1);
	}
	else
	{
		strTemp.Format("%s(%d)%s", strFileName.Left(m), nFileCount+1, strFileName.Mid(m));
	}
	
	CString str;
	str.Format("%s%s", strPathName,strTemp);
	
	fp = fopen(str, "wt");

	if(fp == NULL)	return FALSE;

	nFileCount++;
	
	return TRUE;
}

void CDataDownDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here
	KillTimer(100);
	BOOL	bCheck = FALSE;
	if(((CButton *)GetDlgItem(IDC_RADIO1))->GetCheck() == BST_CHECKED)
	{
		bCheck = TRUE;
	}
	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "FileSeperate", bCheck);
}

//Get Sensro channel and sensor name
BOOL CDataDownDlg::GetUsedSensorCh(CWordArray &aSelTrayCh, int nOption, CWordArray &awSensorCh, CFormResultFile *pRltFile)
{
	//Sensor 1Mapping Channel 목록 구함 
	CWordArray awMappingCh;
	CWordArray awSensor1Ch;

	for(int s=0; s<aSelTrayCh.GetSize(); s++)
	{
		int nChNo = aSelTrayCh[s];
		if(pRltFile->FindMappingSensorCh(nChNo, awMappingCh))		//0개 or 1개 or 여러개가 Mapping될 수 있다.
		{
			//중복된 Channel을 제외하고 Append한다.
			for(int a=0; a<awMappingCh.GetSize(); a++)
			{
				//중복검사
				BOOL bFind = FALSE;
				for(int b = 0; b<awSensor1Ch.GetSize(); b++)
				{
					//중복채널 발견 
					if(awSensor1Ch[b] == awMappingCh[a])
					{
						bFind = TRUE;
						break;
					}
				}
				//중복없으면 추가 
				if(bFind == FALSE)
				{
					awSensor1Ch.Add(awMappingCh[a]);
				}
			}

		}
	}
	if(nOption == SENSOR1)
	{
		awSensorCh.RemoveAll();
		awSensorCh.Append(awSensor1Ch);
		return TRUE;
	}

#ifdef _DEBUG
	for(int g =0; g<awSensor1Ch.GetSize(); g++)
	{
		TRACE("Sensor1 %d\n", awSensor1Ch[g]);
	}
#endif


	//Sensor2 Mapping Channel 목록 구함 
	awMappingCh.RemoveAll();
	CWordArray awSensor2Ch;
	for(s=0; s<aSelTrayCh.GetSize(); s++)
	{
		int nChNo = aSelTrayCh[s];
		if(pRltFile->FindMappingSensorCh(nChNo, awMappingCh, 1))		//0개 or 1개 or 여러개가 Mapping될 수 있다.
		{
			//중복된 Channel을 제외하고 Append한다.
			for(int a=0; a<awMappingCh.GetSize(); a++)
			{
				WORD w = awMappingCh[a];
				//중복검사
				BOOL bFind = FALSE;
				for(int b = 0; b<awSensor2Ch.GetSize(); b++)
				{
					//중복채널 발견 
					if(awSensor2Ch[b] == awMappingCh[a])
					{
						bFind = TRUE;
						break;
					}
				}
				//중복없으면 추가 
				if(bFind == FALSE)
				{
					awSensor2Ch.Add(awMappingCh[a]);
				}
			}
		}
	}
	if(nOption == SENSOR2)
	{
		awSensorCh.RemoveAll();
		awSensorCh.Append(awSensor2Ch);
		return TRUE;
	}

#ifdef _DEBUG
	for( g =0; g<awSensor2Ch.GetSize(); g++)
	{
		TRACE("Sensor2 %d\n", awSensor2Ch[g]);
	}
#endif

	//Sensor1과 Sensor2의 채널을 통합한다.
	awMappingCh.RemoveAll();
	awMappingCh.Append(awSensor1Ch);
	for(s= 0; s<awSensor2Ch.GetSize(); s++)
	{
		BOOL bFind = FALSE;
		//이미 추가되어 있으면 생략 한다.
		for(int a =0; a<awMappingCh.GetSize(); a++)
		{
			if(awSensor2Ch[s] == awMappingCh[a])
			{
				bFind = TRUE;
			}
		}
		if(bFind == FALSE)
		{
			awMappingCh.Add(awSensor2Ch[s]);
		}
	}
	//////////////////////////////////////////////////////////////////////////
	if(nOption == ALL_SENSOR)
	{
		awSensorCh.RemoveAll();
		awSensorCh.Append(awMappingCh);
	}

#ifdef _DEBUG
	for( g =0; g<awSensorCh.GetSize(); g++)
	{
		TRACE("Sensor3 %d\n", awSensorCh[g]);
	}
#endif
	
	return TRUE;
}

void CDataDownDlg::OnButton2() 
{
	// TODO: Add your control notification handler code here

// 	if(DownloadDataFile("/root/formation_data/monitoringData/group1/data26", "ch001_MonitoringData01.csv" , "c:\\Temp", "192.168.7.131", "root", "dusrnth"))
// 	{
// 				
// 	}

// 	if(DownloadDataFile("", "formation_data/monitoringData/group1/data26/ch001_MonitoringData01.csv" , "c:\\Temp\\ch001_MonitoringData01.csv", "192.168.7.131", "root", "dusrnth"))
// 	{
// 				
// 	}
	if(!m_resultData.IsLoaded())	return;
	
	CShowDetailResultDlg	*pDlg = NULL;
	pDlg = new CShowDetailResultDlg(&m_resultData, this);
	pDlg->DoModal();

	delete pDlg;
	pDlg = NULL;
}

//strColumn과 일치에서 aChArray와 일치하는 Index를 achArray로 반납 
BOOL CDataDownDlg::FindDataColumn(CWordArray &aChArray, CString &strColumn, int nSensorType, CFormResultFile *pRltFile)
{
	CString strSrc = strColumn;
	CWordArray aSrcArray;
	aSrcArray.Copy(aChArray);

	aChArray.RemoveAll();
	strColumn.Empty();

	CString strTemp;
	CStringArray aStrData;
	ParsingString(strSrc, aStrData);	//, 단위로 분리 

	int nSensorChNo;
	for(int i=0; i<aStrData.GetSize(); i++)
	{
		strTemp = aStrData.GetAt(i);
		nSensorChNo =  atol(strTemp.Right(2));

		CString strSensorName = pRltFile->GetSensorName(nSensorChNo-1, nSensorType);

		for(int j=0; j<aSrcArray.GetSize(); j++)
		{
			if(aSrcArray[j] == nSensorChNo)
			{
				aChArray.Add(i);
				strColumn += (strSensorName+",");
				break;
			}
		}
	}

	strTemp.Empty();
	//가장 마지막 , 제거 
	if(strColumn.GetLength()>0)
	{
		strTemp = strColumn.Left(strColumn.GetLength()-1);
	}
	strColumn = strTemp;

	//Find Mapping name
/*	for(i = 0; i<aChArray.GetSize(); i++)
	{
		if(i == aChArray.GetSize()-1)
		{
			strTemp.Format("%s", aStrData.GetAt(aChArray[i]));
		}
		else
		{
			strTemp.Format("%s,", aStrData.GetAt(aChArray[i]));
		}
		strColumn = strColumn + strTemp;
	}
*/	TRACE("%s\n", strColumn);
	return TRUE;
}

//, 단위로 분리 
BOOL CDataDownDlg::ParsingString(CString strString, CStringArray &aStrArray)
{
	int s=0, p1=0;
	while(p1!=-1)
	{
		p1 = strString.Find(',', s);
		if(p1!=-1)
		{
			CString Title = strString.Mid(s, p1-s);
			s  = p1+1;
			aStrArray.Add(Title);
		}
	}
	return TRUE;
}

CString CDataDownDlg::ConvertDataString(CWordArray &awIndex, CString &strSrcData)
{
	CString strSrc = strSrcData;
	CStringArray aStrData;
	ParsingString(strSrc, aStrData);

	CString strTemp, strData;
	for(int i = 0; i<awIndex.GetSize(); i++)
	{
		if(awIndex[i] < aStrData.GetSize())
		{
			strTemp.Format("%s,", aStrData.GetAt(awIndex[i]));
			strData = strData + strTemp;
		}
	}

	strTemp.Empty();
	//가장 마지막 , 제거 
	if(strData.GetLength()>0)
	{
		strTemp = strData.Left(strData.GetLength()-1);
	}
			
	return strTemp;
}

void CDataDownDlg::OnSelchangeDataList() 
{
	// TODO: Add your control notification handler code here
	int n = m_listBox.GetSelCount();
	CString str;
	str.Format("%d Channels are selected.", n);
	GetDlgItem(IDC_STATIC_SEL_CH)->SetWindowText(str);	
}

void CDataDownDlg::OnRadio1() 
{
	// TODO: Add your control notification handler code here
	GetDlgItem(IDC_STATIC_FORMAT)->SetWindowText("time  vtg  crt  cap  watt\n1sec  xxx   xxx   xxx   xxx\n2sec  xxx   xxx   xxx   xxx\n  :        :       :       :       :");
}

void CDataDownDlg::OnRadio2() 
{
	// TODO: Add your control notification handler code here
	GetDlgItem(IDC_STATIC_FORMAT)->SetWindowText("time  CH1_V CH2_V  CH3_V\n1sec     xxx       xxx        xxx\n2sec     xxx       xxx        xxx\n  :           :           :            :");	//100101
}

BOOL CDataDownDlg::HandleMessage()
{
//	MSG msg;
// 	if(GetMessage(&msg, NULL, 0, 0))
// 	{
// 		TranslateMessage(&msg);
// 		DispatchMessage(&msg);
// 	}
	return TRUE;
}

void CDataDownDlg::OnOnButton() 
{
	// TODO: Add your control notification handler code here
	int index = m_ctrlUnitCombo.GetCurSel();
	if(index < 0)	return;
	m_nOnLineSelModuleID = m_ctrlUnitCombo.GetItemData(index);

	CString strData, strIP;
	m_ctrlUnitCombo.GetLBText(index, strData);
	
	int nGroupNo;
	//2008-12	ljb : mdb에서 groupNo (Data5 filde 가지고 온다)
	strIP = GetIPAddress(m_nOnLineSelModuleID,nGroupNo);

	//2Group 방식일 경우 
//////////////////////////////////////////////////////////////////////////
//	int nGroupPerModule = AfxGetApp()->GetProfileInt("Settings", "GroupPerModule", 2);
//	if(nGroupPerModule < 1)	nGroupPerModule = 1;
//	int nGroupNo = (m_nOnLineSelModuleID-1) % nGroupPerModule +1;
//////////////////////////////////////////////////////////////////////////

	CString strTemp;
	strTemp.Format("%s [%s - Group %d]", strData, strIP, nGroupNo);
	m_ctrlSelModule.SetText(strTemp);

	if(ListUpAllRemote(strIP, nGroupNo) == FALSE)
	{
		AfxMessageBox(TEXT_LANG[63]);//100102
	}
}

BOOL CDataDownDlg::ListUpAllRemote(CString strIp, int nGroupNo, CString strSearchTray)
{
	if(strIp.IsEmpty())	
	{
		WriteLog(TEXT_LANG[34]);
		return FALSE;
	}
	CString strTemp;
	CWaitCursor wait;

	strTemp.Format(TEXT_LANG[35], strIp);//100103
	WriteLog(strTemp);

	
	CString strRemoteLocation;
	CFtpDownLoad *pDownLoad = new CFtpDownLoad;
	
	BOOL bConnect = pDownLoad->ConnectFtpSession(strIp, m_strID, m_strPassword);
	if(bConnect == FALSE)
	{
		strTemp.Format(TEXT_LANG[36], strIp);//100104
		WriteLog(strTemp);
		delete pDownLoad;
		return FALSE;
	}

	m_ctrlRemoteList.ResetContent();
	
	char szBuff[_MAX_PATH];//, szBuff1[_MAX_PATH];
	::GetTempPath(_MAX_PATH, szBuff);		//Get Windows temp. folder
	CString strRemoteFileName, strLocalFileName;

	//1. 목록 Index 파일을 Download한 후 주어진 test serialno의 폴더를 찾는다.
	strLocalFileName.Format("%s\\downtemp.csv", szBuff);
	strRemoteFileName.Format("%s/group%d/savingFileIndex_start.csv", m_strPathName, nGroupNo);

	if(pDownLoad->DownLoadFile(strLocalFileName, strRemoteFileName) < 1)
	{
//		WriteLog(strRemoteFileName);
//		WriteLog(strLocalFileName);
		WriteLog(TEXT_LANG[37]);
		delete pDownLoad;
		return FALSE;
	}
	delete pDownLoad;
	pDownLoad = NULL;

	//Download list file open
	
	CString strDataBuf, strData, strTrayNo, strTestSerial, strDay, strTime;
	int nIndex = 0;
	int nDay = 0, nYear = 0, nMonth = 0;
	BOOL bFind = FALSE;
/*	FILE *fp;
	fp = fopen(strLocalFileName, "rb");
	if(fp == NULL)
	{
		WriteLog("Local 위치에서 작업 목록 파일을 찾을 수 없습니다.");
		return FALSE;
	}
	while(fscanf(fp,	"%s %d, %s %d, %s %d, %s %d, %s %s", 
						szBuff, &nIndex, szBuff, &nYear, szBuff, &nMonth, szBuff, &nDay, szBuff, szBuff) > 1)
	{		
		if(strTestSerial == CString(szBuff))
		{
			bFind = TRUE;
			break;
		}
	}
	fclose(fp);
*/
	
	CStdioFile lineFile;
	if(lineFile.Open(strLocalFileName, CFile::modeRead) == FALSE)
	{
		WriteLog(TEXT_LANG[38]);
		return FALSE;
	}
	//저장 Format : 
	// mointorIndex 1, open_day 2006/07/21, open_time 14:11:02, test_serial 200205060, tray_no tttt 
	int p1, p2, p3;
	while(lineFile.ReadString(strDataBuf))
	{
		strData = strDataBuf;
		strData.TrimLeft(" ");
		strData.TrimRight(" ");
		if(strData.Right(1) != ",")		strData +=",";

		p1 = strData.Find("index");
		p2 = strData.Find(' ', p1);
		p3 = strData.Find(',', p2);
		nIndex = atol(strData.Mid(p2+1,p3-p2-1));
					
		if( p1 != -1 )
		{
			p2 = strData.Find(' ', p1);
			p3 = strData.Find(',', p2);
			nYear = atol(strData.Mid(p2+1,p3-p2-1));		
		}

		p1 = strData.Find("open_month");
		if( p1 != -1 )
		{
			p2 = strData.Find(' ', p1);
			p3 = strData.Find(',', p2);
			nMonth = atol(strData.Mid(p2+1,p3-p2-1));
		}


		p1 = strData.Find("open_day");
		if( p1 != -1 )
		{
			p2 = strData.Find(' ', p1);
			p3 = strData.Find(',', p2);
			strDay = strData.Mid(p2+1,p3-p2-1);
			nDay = atol(strDay);
		}


		p1 = strData.Find("open_time");
		if( p1 != -1 )
		{
			p2 = strData.Find(' ', p1);
			p3 = strData.Find(',', p2);
			strTime = strData.Mid(p2+1,p3-p2-1);
		}		

		p1 = strData.Find("test_serial_no");
		if( p1 != -1 )
		{
			p2 = strData.Find(' ', p1);
			p3 = strData.Find(',', p2);
			strTestSerial = strData.Mid(p2+1,p3-p2-1);
		}		

		p1 = strData.Find("tray_id");
		if( p1 != -1 )
		{
			p2 = strData.Find(' ', p1);
			if( p2 != -1 )
			{
				p3 = strData.Find(',', p2);
				strTrayNo = strData.Mid(p2+1,p3-p2-1);			
			}			
		}
				
		CString str;
		if(strSearchTray.IsEmpty())
		{
			if(strTime.IsEmpty() == FALSE)		//2007/7/6 이후 Versino( V2.0)
			{
				str.Format("[%03d] %s %s :: %s", nIndex, strDay, strTime, strTrayNo);
			}
			else								//version 2.0 이하 
			{
				str.Format("[%03d] %04d/%02d/%02d :: %s", nIndex, 2000+nYear, nMonth, nDay, strTrayNo);
			}

			if(strTestSerial.IsEmpty() == FALSE)
			{
				strData.Format(" (%s)", strTestSerial);
				str = str + strData;
			}
			m_ctrlRemoteList.AddString(str);
		}
		else		//검색 조건이 있음
		{
			if(strTrayNo == strSearchTray)
			{
				if(strTime.IsEmpty() == FALSE)		//2007/7/6 이후 Versino( V2.0)
				{
					str.Format("[%03d] %s %s :: %s", nIndex, strDay, strTime, strTrayNo);
				}
				else								//version 2.0 이하 
				{
					str.Format("[%03d] %04d/%02d/%02d :: %s", nIndex, 2000+nYear, nMonth, nDay, strTrayNo);
				}

				if(strTestSerial.IsEmpty() == FALSE)
				{
					strData.Format(" (%s)", strTestSerial);
					str = str + strData;
				}
				m_ctrlRemoteList.AddString(str);			
			}
		}
	}
	lineFile.Close();

	_unlink(strLocalFileName);

//	strRemoteLocation.Format("%s/group%d/data%d", m_strPathName, nGroupNo, nIndex);

	//2. 주어진 test serialno의 폴더의 channel 목록을 표시한다.
	return TRUE;
}

void CDataDownDlg::OnSelchangeRemoteList() 
{
	// TODO: Add your control notification handler code here
	//Group No를 얻는 방법 수정 해야함 -> 2008-12  ljb mdb에서 가져옴(Data5)
//////////////////////////////////////////////////////////////////////////
	int nGroupNo; //= (m_nOnLineSelModuleID-1) % 2 +1;
//////////////////////////////////////////////////////////////////////////
	m_strIPAddress = GetIPAddress(m_nOnLineSelModuleID, nGroupNo);

	CString strList;
	CString strTemp;

	int nStepNum=0, nMdCh=0; 

	int nIndex = m_ctrlRemoteList.GetCurSel();
	if(nIndex < 0)	return;
	
	//"[%03d] %s %s :: %s", nIndex, szDay, szTime, szTrayNo 에서 Index를 추출 한다.
	m_ctrlRemoteList.GetText(nIndex, strList);
	int a = strList.Find('[');
	int b = strList.Find(']', a);
	if(b > a)
	{
		nIndex = atol(strList.Mid(a+1, b-1));
	}

	m_strFileLocation.Format("%s/group%d/data%d", m_strPathName, nGroupNo, nIndex);
	
	CStringList aFileList;
	CStringList aSensorFileList;
	CStringList aFadmFileList;

	GetRemoteFileList(m_strIPAddress, m_strFileLocation, aFileList, aSensorFileList, aFadmFileList);
	if(aFileList.GetCount() < 1)
	{
		strTemp.Format(TEXT_LANG[39], m_strIPAddress);
		WriteLog(strTemp);
		return;
	}

	// 1. Ch Fadn data
	m_listBox.ResetContent();
	POSITION pos = aFileList.GetHeadPosition();	//Module Base channel no
	while(pos)
	{
		strTemp = aFileList.GetNext(pos);				//format => ch001
		nMdCh = atol(strTemp.Right(3));
		strTemp.Format("CH %03d", nMdCh);							
		m_listBox.AddString(strTemp);						//Add
		m_listBox.SetItemData(m_listBox.GetCount()-1, nMdCh);			//Channel No
	}

	// 2. Ch Fadn data
	pos = aFadmFileList.GetHeadPosition();
	while(pos)
	{
		strTemp = aFadmFileList.GetNext(pos);
		nMdCh = atol(strTemp.Mid(strTemp.ReverseFind('_')-3,3));
		nStepNum = atol(strTemp.Right(2));
		strTemp.Format("CH F%03d_%02d", nMdCh, nStepNum);							
		m_listBox.AddString(strTemp);						//Add
		m_listBox.SetItemData(m_listBox.GetCount()-1, nMdCh);			//Channel No
	}

	pos = aSensorFileList.GetHeadPosition();
	while(pos)
	{
		strTemp = aSensorFileList.GetNext(pos);
		nMdCh = atol(strTemp.Right(3));
		strTemp.Format("CH Sensor%03d", nMdCh);							
		m_listBox.AddString(strTemp);						//Add
		m_listBox.SetItemData(m_listBox.GetCount()-1, nMdCh);			//Channel No
	}

	OnSelectAllButton();
	OnSelchangeDataList();

	a = strList.Find("::");
	strTemp.Format("[ %s ] %d cell data are found.", strList.Mid(a+3), m_listBox.GetCount());
	GetDlgItem(IDC_TOT_CH_COUNT_STATIC)->SetWindowText(strTemp);

	m_bSearchModeOffLine = FALSE;
	m_bFindLocal = FALSE;

	GetDlgItem(IDC_OPTION_BUTTON)->EnableWindow(FALSE);
	GetDlgItem(IDC_RADIO1)->EnableWindow(FALSE);
	GetDlgItem(IDC_RADIO2)->EnableWindow(FALSE);
	GetDlgItem(IDC_STATIC_FORMAT)->EnableWindow(FALSE);
}

// [12/15/2009 kky]
// DataDown Error 부분
CString CDataDownDlg::GetRemoteFileList(CString strIp, CString strDir, CStringList &aFileList, CStringList & aSensorFileList, CStringList &aFadmFileList)
{
	CString strRemote;
	strRemote = strDir;

	//Channel 목록을 구함
	CFtpConnection   *pConnect=NULL;         //Ftp 명령 연결 컨트롤 클래스
    TCHAR sz[1024];

	try		// Ftp Connection
	{
		m_InternetSession.SetOption(INTERNET_OPTION_CONNECT_TIMEOUT, 5000);
		pConnect = m_InternetSession.GetFtpConnection(strIp, m_strID, m_strPassword);
    }
	catch(CInternetException *pEx)
	{
		pEx->GetErrorMessage(sz, 1024);
		WriteLog(sz);
	    pEx->Delete();
		return "";
	}
	
	//Remote Current Directory
	if(pConnect->SetCurrentDirectory(strRemote) == FALSE)		
	{
//		WriteLog("Remote file location error!!");
		pConnect->Close();
		return strRemote;
	}
	
	CFtpFileFind *pFileFind = new CFtpFileFind(pConnect);
	BOOL bContinue = pFileFind->FindFile(_T("ch???_MonitoringData01.csv"));	
	int nCnt = 0;
	while(bContinue)
	{
		try
		{
			bContinue = pFileFind->FindNextFile();
			aFileList.AddTail( pFileFind->GetFileName().Left(5));	//파일명에서 Channel 목록을 추출 
		}
		catch(CInternetException *pEx)
		{
			pEx->GetErrorMessage(sz, 1024);
			WriteLog(sz);
			pEx->Delete();
			continue;
		}
		nCnt++;
		TRACE("%d file find\n", nCnt);
	}
	pFileFind->Close();

	bContinue = pFileFind->FindFile(_T("ch???_FadmData??.csv"));	
	nCnt = 0;
	while(bContinue)
	{
		try
		{
			bContinue = pFileFind->FindNextFile();
			aFadmFileList.AddTail( pFileFind->GetFileName().Left(pFileFind->GetFileName().ReverseFind('.')));
		}
		catch(CInternetException *pEx)
		{
			pEx->GetErrorMessage(sz, 1024);
			WriteLog(sz);
			pEx->Delete();
			continue;
		}
		nCnt++;
		TRACE("%d file find\n", nCnt); //100173
	}
	pFileFind->Close();

	//find sensor file list
	bContinue = pFileFind->FindFile(_T("sensor_data_ch???_01.csv"));	
	nCnt = 0;
	while(bContinue)
	{
		try
		{
			bContinue = pFileFind->FindNextFile();
//			CString str = pFileFind->GetFileName().Mid(12, 5);
			aSensorFileList.AddTail( pFileFind->GetFileName().Mid(12, 5));	//파일명에서 Channel 목록을 추출 
		}
		catch(CInternetException *pEx)
		{
			pEx->GetErrorMessage(sz, 1024);
			WriteLog(sz);
			pEx->Delete();
			continue;
		}
		nCnt++;
		TRACE("%d file find\n", nCnt);
	}
	pFileFind->Close();

	delete pFileFind;
	pFileFind = NULL;

	pConnect->Close();
	delete pConnect;

	// m_InternetSession.Close();
	//	m_InternetSession.Close();//??
	return strRemote;
}

void CDataDownDlg::OnSelchangeComboUnit() 
{
	// TODO: Add your control notification handler code here
	
}

// 폴더명 중복시 처리??
// 지정 폴더에서 가장 위선순위 파일을 찾는다.
CString CDataDownDlg::GetNextTargetListFile_new()
{
	CString strFileName, strTemp, str, str1, str2;
	CString strPath;	
	
	strPath.Format("%s\\ProfileList", m_strMonDir);
	
	strFileName.Format("%s\\*.pfl", strPath);
	
	CFileFind ff;
	BOOL bFind = ff.FindFile(strFileName);
	if( bFind == FALSE )
	{
		strFileName = __T("");
		return strFileName;
	}
	
	int nCnt = 0;
	while(bFind)
	{
		bFind = ff.FindNextFile();

		if( ff.IsDots())
		{
			continue;
		}
		else
		{
			strTemp = ff.GetFilePath();
			strFileName = strTemp;
			bFind = FALSE;
		}
		
	}
	return strFileName;
}

CString CDataDownDlg::GetNextTargetListFile()
{
	CString strFileName, strTemp, str, str1, str2;
	CString strPath;	
	
	strPath.Format("%s\\ProfileList", m_strMonDir);

	strFileName.Format("%s\\*.pfl", strPath);

	m_ctrlProfileList.ResetContent();

	CFileFind ff;
	BOOL bFind = ff.FindFile(strFileName);
	if( bFind == FALSE )
	{
		strFileName = __T("");
		return strFileName;
	}

	int nCnt = 0;
	while(bFind)
	{
		nCnt++;
		bFind = ff.FindNextFile();
		strTemp = ff.GetFilePath();
		if(nCnt == 1)
		{
			strFileName = strTemp;
		}
		//str2 = ParsingResultFileName(strTemp);
		CProfileListInfo profileInfo;
		if(profileInfo.LoadData(strTemp))
		{
			str2 = profileInfo.GetResultFileName();
		}
		else
		{
			str2 = strTemp + " parsing error!!!";
		}
		
		int n = strTemp.ReverseFind('\\')+1;
		str = strTemp.Mid(n, strTemp.ReverseFind('.') - n);
		str1.Format("%03d [%s] :: %s", nCnt, str, str2);
		m_ctrlProfileList.AddString(str1);
	}
	return strFileName;
}

/*
CString CDataDownDlg::ParsingResultFileName(CString strFileName, CString &strSavePath, CString &strIp, CString &strTestSerial, CString &strTray)
{
	CString strResultFile;
	CStdioFile	file;
	if(!file.Open(strFileName, CFile::modeRead|CFile::shareDenyNone))	
	{

#ifdef _DEBUG
		afxDump << "Configuration File Open Fail" << "\n";
#endif
		return strResultFile;
	}
	
	CString buff;
	BOOL	bEOF;

	TRY
	{
		while(TRUE)
		{
			bEOF = file.ReadString(buff);
			if(!bEOF)	break;

			if(!buff.IsEmpty())
			{
				CString strConfig(buff);
				int a, b;
				a = strConfig.Find('[');
				b = strConfig.ReverseFind(']');

				if(a >=0 && b > 0 && a < b)
				{
					CString strTitle, strData;
					strTitle = strConfig.Mid(a+1, b-a-1);

					a = strConfig.ReverseFind('=');

					strData = strConfig.Mid( a+1 );

					strData.TrimLeft(" ");
					strData.TrimRight(" ");

					if(strTitle ==  "UNIT_IP")
					{
						strIp =  strData;
					}	
					else if(strTitle == "TrayID")
					{
						strTray = strData;
					}
					else if(strTitle == "TestSerial")
					{
						strTestSerial = strData;
					}
					else if(strTitle == "Result")
					{
						strResultFile = strData;
					}
					else if(strTitle == "SaveDir")
					{
						strSavePath = strData;
					}
					else if(strTitle == "UNIT_NAME")
					{

					}
				}
			}
		}
		
	}
	CATCH (CFileException, e)
	{
		//AfxMessageBox(e->m_cause);
		WriteLog(e->m_cause);
		file.Close();
	}
	END_CATCH

	file.Close();	
	return strResultFile;
}
*/

//파일명을 "현재 시각.pfl" 파일로 변경
BOOL CDataDownDlg::SetPrimary(CString strListFile)
{
	if(strListFile.IsEmpty())	return FALSE;

	CString strLog;

	CTime curTime = CTime::GetCurrentTime();
	CString strNewName;
	strNewName.Format("%s\\%s.%s", 
					strListFile.Left(strListFile.ReverseFind('\\')), 
					curTime.Format("%Y%m%d%H%M%S"), 
					strListFile.Mid(strListFile.ReverseFind('.')+1));

	strLog.Format(TEXT_LANG[40], strListFile, strNewName);
	WriteLog(strLog);

	int  result;
	/* Attempt to rename file: */
	result = rename( strListFile, strNewName );
	if( result != 0 )
	{
		WriteLog(TEXT_LANG[41]);
	}
	else
	{
		TRACE( "File '%s' renamed to '%s'\n", strListFile, strNewName );
	}
	return TRUE;
}

void CDataDownDlg::OnRadio3() 
{
	// TODO: Add your control notification handler code here
	m_ctrlProfileList.ShowWindow(FALSE);
	m_LogList.ShowWindow(TRUE);
}

void CDataDownDlg::OnRadio4() 
{
	// TODO: Add your control notification handler code here
	m_ctrlProfileList.ShowWindow(TRUE);
	m_LogList.ShowWindow(FALSE);	
}

//
BOOL CDataDownDlg::DownloadDataFile(CString strRemote, CString strSaveDir, CStringList &strServerFileList, CStringList &strLocalFileList, CString strIp, CString strID, CString strPassword)
{
	ASSERT(strServerFileList.GetCount() == strLocalFileList.GetCount());
	HINTERNET	hConnect = NULL;
	HINTERNET	hOpen = NULL;
	HINTERNET	hOpenFile = NULL;

	BOOL nRtn = TRUE;

	CString strMsg;
	CString strFileNameAtLocalMachine;  	//local file name
	CString strFileNameAtServer;			//remote file name

	CStringList downfilieList;

	//initializes an applications use of Internet functions.
	hOpen = InternetOpen("Pne_Downloader", INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, 0 );
	if( !hOpen )
	{
         return FALSE;
	}

	//connect to ftp server
	if ( !(hConnect = InternetConnect (hOpen, strIp, INTERNET_DEFAULT_FTP_PORT, strID, strPassword, INTERNET_SERVICE_FTP, 0  , 0) ) )
	{
		if(hOpen)	InternetCloseHandle(hOpen);
		strMsg.Format(TEXT_LANG[42], strIp, GetLastFtpErrorResponse());
		WriteLog(strMsg);
		return FALSE;
	}

	//set current directory 
// #ifdef _DEBUG
// 	char szCurDir[MAX_PATH];
// 	ZeroMemory(szCurDir, MAX_PATH);
// 	DWORD dwSize = MAX_PATH;
// 	FtpGetCurrentDirectory(hConnect, szCurDir, &dwSize);
// 	TRACE("Current directory is %s\n", szCurDir);
// #endif

// 	HINTERNET hResponse;
// 	strMsg.Format("CWD %s", strRemoteDir);
// 	BOOL bRet = ::FtpCommand(	hConnect,					// WinInet Connection handle
// 								FALSE,						// No, I don't expect a response
// 								FTP_TRANSFER_TYPE_ASCII,	// I'm receiving ASCII
// 								strMsg,						// This is the FTP command I am passing
// 								0,							// No context needed
// 								&hResponse);					// The handle to read the response 
	
// 	if(FtpSetCurrentDirectory(hConnect, strRemoteDir))
// 	{
// 		if(hConnect)	InternetCloseHandle(hConnect);
// 		if(hOpen)		InternetCloseHandle(hOpen);
// 
// 		strMsg.Format("Remote directory not found.[%s](Description:%s)", strRemoteDir, GetLastFtpErrorResponse());
// 		AfxMessageBox(strMsg, MB_OK);
// //		return FALSE;
// 	}

// #ifdef _DEBUG
// 	ZeroMemory(szCurDir, MAX_PATH);
// 	FtpGetCurrentDirectory(hConnect, szCurDir, &dwSize);
// 	TRACE("Current directory is %s\n", szCurDir);
// #endif
	//////////////////////////////////////////////////////////////////////////
	
	int nCnt = 0;
	int nTotCh = strServerFileList.GetCount();
	POSITION pos1 = strServerFileList.GetHeadPosition();	//Module Base channel no
	POSITION pos2 = strLocalFileList.GetHeadPosition();	//Module Base channel no
	while(pos1)
	{
		m_downProgress.SetPos((((float)nCnt++)/(float)nTotCh) * 100);
		PUMPMESSAGE();

		strFileNameAtServer.Format("%s/%s", strRemote, strServerFileList.GetNext(pos1));				//format => ch001
		strFileNameAtLocalMachine.Format("%s\\%s", strSaveDir, strLocalFileList.GetNext(pos2));				//format => ch001

		//local에 있는 파일 크기를 구한다.
		double dOffsetToSeek =0;
		double dCurrentFileSize=0;
		if(IfLocalFileExist(strFileNameAtLocalMachine, &dCurrentFileSize)) 
		{
			dOffsetToSeek = dCurrentFileSize;
		}
		
		//local의 파일 크기 이후 영역 download 준비하도록 ftp server에 보냄
		if(dOffsetToSeek > 0)
		{
			TRACE("Sending REST...");
			
			//Seek to the file first
			HINTERNET hResponse;

			char pszOffset[30];
			ltoa(dOffsetToSeek, pszOffset, 10);
			strMsg.Format("REST %s", pszOffset);
			BOOL bRet = ::FtpCommand(	hConnect,					// WinInet Connection handle
										FALSE,						// No, I don't expect a response
										FTP_TRANSFER_TYPE_BINARY,	// I'm receiving ASCII
										strMsg,    // This is the FTP command I am passing
										0,							// No context needed
										&hResponse);					// The handle to read the response 
			if (!bRet)
			{
				strMsg.Format(TEXT_LANG[43], GetLastFtpErrorResponse());
				WriteLog(strMsg);
			}
		}
		
		hOpenFile = ::FtpOpenFile(hConnect, strFileNameAtServer, GENERIC_READ, FTP_TRANSFER_TYPE_BINARY, 1);
		if(hOpenFile)
		{
			//File의 Size
	// 		DWORD dwHigh;
	// 		DWORD dwLow = ::FtpGetFileSize(hOpenFile, &dwHigh);

			FILE * pFile = NULL;
			if((pFile = fopen (strFileNameAtLocalMachine, "ab" ) ) )
			{
			   if(dOffsetToSeek >= 0)
			   {
					TRACE("Seeking to local file...\n");
					int n = fseek(pFile, dOffsetToSeek, SEEK_SET);
			   }

				DWORD dwSize;
				DWORD dwBytesWrrittenToFile = dOffsetToSeek;		//이미 local에 기록되 파일 크기
				DWORD dwToRead = MAX_READ_PACKET_SIZE * 1024;
				PBYTE  pBuffer  = new BYTE[dwToRead];

				while(InternetReadFile (hOpenFile, (LPVOID)pBuffer, dwToRead,  &dwSize))
				{
					if (!dwSize)
					{
						TRACE("Percentage Completed:%d%%\n", 100);
						break;  // Condition of dwSize=0 indicate EOF. Stop.
					}
					else
					{
						TRACE("%d byte readed from server\n", dwSize);
						fwrite(pBuffer, sizeof (char), dwSize , pFile);
						dwBytesWrrittenToFile = dwBytesWrrittenToFile + dwSize;
					}
					
					//Message 처리				
	//				PUMPMESSAGE();

					//Intentional delay so that user get chance to hit stop button
	//				Sleep(100);

	// 				if(m_bStopped)
	// 				{
	// 					TRACE("Stopped..\n");				
	// 					AfxMessageBox("Download aborted\nThere is an incomplete temp file 'Temp_find-ls.txt.gz' at current directory. Don't delete it just check it out. It has useful data downloaded so far...\nOnce you are done click Start again, now download will start from exact location where it left off");
	// 
	// 					m_bStopped = FALSE;
	// 					bInComplete = TRUE;
	// 					break;
	// 				}        
				}
				
				fflush (pFile);
				fclose (pFile);
				delete [] pBuffer;

				downfilieList.AddTail(strFileNameAtLocalMachine);

				// 	if(!bInComplete)
				// 	{
				// 		TRACE("Renaming file at local machine..");
				// 		if(!MoveFile(strTmpFileName, strFileNameAtLocalMachine))
				// 		{
				// 			strMsg.Format("Error:%d", GetLastError());
				// 			AfxMessageBox(strMsg, MB_OK); 
				// 
				// 		}
				// 		TRACE("Done..");
				// 		AfxMessageBox("File has downloaded..");
				// 	}
		   }
		   else	//	if( !(pFile = fopen (strTmpFileName, "ab" ) ) )
		   {
				strMsg.Format(TEXT_LANG[44], strFileNameAtLocalMachine);
				WriteLog(strMsg);
				nRtn = FALSE;
		   }
			
		   InternetCloseHandle(hOpenFile);
		}
		else //if(hOpenFile == NULL)
		{
			strMsg.Format(TEXT_LANG[25],strFileNameAtServer, GetLastFtpErrorResponse());
			WriteLog(strMsg);
			nRtn = FALSE;
		}
	}

	if(hConnect)	InternetCloseHandle(hConnect);
	if(hOpen)		InternetCloseHandle(hOpen);

	strLocalFileList.RemoveAll();
	pos1 = downfilieList.GetHeadPosition();
	while(pos1)
	{
		strLocalFileList.AddTail(downfilieList.GetNext(pos1)) ;
	}
	 return nRtn;	
}

CString CDataDownDlg::GetLastFtpErrorResponse()
{
	CString str;
	LPVOID lpMsgBuf;
	DWORD code = GetLastError();
	FormatMessage( 
		FORMAT_MESSAGE_ALLOCATE_BUFFER | 
		FORMAT_MESSAGE_FROM_SYSTEM | 
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		code,
		0, // Default language
		(LPTSTR) &lpMsgBuf,
		0,
		NULL 
	);
	
	str.Format("%s(%d)", (LPCSTR)lpMsgBuf, code);
	// Free the buffer.
	LocalFree( lpMsgBuf );
	return str;
}

BOOL CDataDownDlg::IfLocalFileExist(const char *pszFilePath, double *pdwFileSize)
{
	struct _finddata_t c_file;
	long hFile;

	if( (hFile = _findfirst( pszFilePath, &c_file )) == -1L )
	{
	   *pdwFileSize = 0;
		return FALSE;
	}
	else
	{
	   *pdwFileSize = c_file.size;
	   _findclose(hFile);
	   return TRUE;
	}

	return FALSE;
}

VOID CDataDownDlg::ExecuteGraphAnalyzer(CString strFile)
{
	CString strProgramName;
	strProgramName.Format("%s\\CTSGraphAnalyzer.exe", m_strMonDir);
	if(ExecuteProgram(strProgramName, "", "", "CTSGraphAnalyzer", FALSE, TRUE))
	{
		//Message를 전송한다.
		HWND pWnd = ::FindWindow(NULL, "CTSGraphAnalyzer");
		if(pWnd)
		{
			CString strData;
			strData = strFile;
			if(strData.IsEmpty())
			{
				return;
			}
			int nSize = strData.GetLength()+1;
			char *pData = new char[nSize];
			ZeroMemory(pData, nSize);
			sprintf(pData, "%s", strData);
			pData[nSize-1] = '\0';

			COPYDATASTRUCT CpStructData;
			CpStructData.dwData = 5;		//CTS Program 구별 Index
			CpStructData.cbData = nSize;
			CpStructData.lpData = pData;
			
			::SendMessage(pWnd, WM_COPYDATA, 0, (LPARAM)&CpStructData);
			
			delete [] pData;
		}
	}
}

BOOL CDataDownDlg::ExecuteProgram(CString strProgram, CString strArgument, CString strClassName, CString strWindowName, BOOL bNewExe, 	BOOL bWait)
{
	if(strProgram.IsEmpty())	return FALSE;

//	TCHAR szBuff[_MAX_PATH];
//	::GetModuleFileName(AfxGetApp()->m_hInstance,szBuff,_MAX_PATH);
//	CString path = CString(szBuff).Mid(0, CString(szBuff).ReverseFind('\\'));

	//새롭게 실행 하지 않으면 주어진 이름으로 검색한다.
	if(!bNewExe)
	{
		CWnd *pWndPrev = NULL, *pWndChild = NULL;

		if(!strWindowName.IsEmpty())
		{
			pWndPrev = CWnd::FindWindow(NULL, strWindowName);
		}

		if(!strClassName.IsEmpty())
		{
			pWndPrev = CWnd::FindWindow(strClassName, NULL);
		}

		// Determine if a window with the class name exists...
		if(pWndPrev != NULL)
		{
			// If so, does it have any popups?
			pWndChild = pWndPrev->GetLastActivePopup();

			// If iconic, restore the main window.
			if (pWndPrev->IsIconic())
				pWndPrev->ShowWindow(SW_RESTORE);

			// Bring the main window or its popup to the foreground
			pWndChild->SetForegroundWindow();

			// and you are done activating the other application.
			return TRUE;
		}
		/*
		HWND FirsthWnd = NULL;
		if(!strWindowName.IsEmpty())
		{
			FirsthWnd = ::FindWindow(NULL, strWindowName);
		}
		if(!strClassName.IsEmpty())
		{
			FirsthWnd = ::FindWindow(strClassName, NULL);
		}
		if (FirsthWnd)		//실행 되어 있으면 
		{
//			::SetActiveWindow(FirsthWnd);	
			::SetForegroundWindow(FirsthWnd);
			return TRUE;
		}
		//else	//실행된게 없으면 새롭게 실행 
		*/
	}

	CString strFullName;
	if(!strArgument.IsEmpty())
	{
		strFullName.Format("%s \"%s\"", strProgram, strArgument);
	}
	else
	{
		strFullName.Format("%s", strProgram);
	}

	STARTUPINFO	stStartUpInfo;
	PROCESS_INFORMATION	pi;
	ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
	ZeroMemory(&pi, sizeof(PROCESS_INFORMATION));
	
	stStartUpInfo.cb = sizeof(STARTUPINFO);
	stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
	stStartUpInfo.wShowWindow = SW_SHOWNORMAL;

		
	BOOL bFlag = ::CreateProcess(	NULL, 
									(LPSTR)(LPCTSTR)strFullName, 
									NULL, 
									NULL, 
									FALSE, 
									NORMAL_PRIORITY_CLASS, 
									NULL, 
									NULL, 
									&stStartUpInfo, 
									&pi
								);
	if(bFlag == FALSE)
	{
		strFullName += TEXT_LANG[19]; //를 실행할 수 없습니다. //100105
		AfxMessageBox(strFullName, MB_OK|MB_ICONSTOP);
		return FALSE;
	}
    
	if(bWait)
	{
		DWORD dwRet = WaitForInputIdle(pi.hProcess, 15000);
		if (WAIT_TIMEOUT != dwRet)  
			return TRUE;
	}

	CloseHandle( pi.hProcess );
	CloseHandle( pi.hThread );
	
	return TRUE;
}

// 정상 Download시 return TRUE;
// 정보가 없는 파일의 경우 삭제를 위해 return TRUE;
// 접속 이상 등이 있어 나중에 다시 시도해야 하며 return FALSE;
BOOL CDataDownDlg::DownLoadOnLineModeProfile(long lUnitID, CString strSaveDir, CString strTrayID, CString strUnitName)
{
	int nGroupNo = 1;
	CString strIp = GetIPAddress(lUnitID, nGroupNo);
	CString strTemp, strDest, strLog;
	if(strIp.IsEmpty() || strSaveDir.IsEmpty() || strTrayID.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[46], strIp, strTrayID, strSaveDir);
		WriteLog(strTemp);
		return TRUE;
	}
	else
	{
		strTemp.Format(TEXT_LANG[47], strIp );
		WriteLog(strTemp);
	}

	//1. 결과 파일 Loading
	CString strIPAddress(strIp);
	CString strSyncCode(strTrayID);
	
	strTemp.Format(TEXT_LANG[48], strSyncCode, strUnitName, strIPAddress);
	WriteLog(strTemp);		

	//2. 현재 선택한 모듈에서 주어진 serial no의 결과를 모듈에서 검색한다. get remote file location
	CStringList chList;			//remote channel list
	CStringList sensorChList;
	CStringList chFadmList;

	CString strRemoteFileLocation = FindOnlineRealTimeDataFromRemote(strIPAddress, nGroupNo, strSyncCode, chList, sensorChList, chFadmList);

	//접속에 실패한 경우 
	if(strRemoteFileLocation.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[49], strSyncCode, strIPAddress);
		WriteLog(strTemp);
		return FALSE;
	}

	//Linux 목록에서 이미 삭제된 파일 
	if(chList.GetCount() < 1 && chFadmList.GetCount() < 1)
	{
		strTemp.Format(TEXT_LANG[50], strSyncCode, strIPAddress); //100193
		WriteLog(strTemp);
		return TRUE;
	}

	//3. 검색한 remote channel을  list 한다.
	CDWordArray adwSelCh;
	CDWordArray awFadmSelCh;
	int nModuleChNo, nTrayCh, nStepNum, nCh;
	
	POSITION pos = chList.GetHeadPosition();
	while(pos)
	{
		strTemp = chList.GetNext(pos);		//format => ch001
		nModuleChNo = atol(strTemp.Right(3));
		nTrayCh = nModuleChNo;		//Tray 번호로 Convert
		
		adwSelCh.Add(MAKELONG(nTrayCh, nModuleChNo));
	}

	pos = chFadmList.GetHeadPosition();
	while(pos)
	{
		strTemp = chFadmList.GetNext(pos);		//format => ch001	
		nStepNum = atol(strTemp.Right(2));			
		nCh = atol(strTemp.Mid(strTemp.ReverseFind('_')-3,3));
		awFadmSelCh.Add(MAKELONG(nCh, nStepNum));
	}

	//4. 모듈 폴더 생성 및 최근 20목록 확인 
	if(AddFolderList(strSaveDir) == FALSE)
	{
		strLog.Format(TEXT_LANG[51], strSaveDir);
		WriteLog(strLog);
		return TRUE;
		//return  FALSE;
	}
	else
	{
		//Search channel file and download to destination location
		//source dir(remote), dest dir(local), selected ch, ip
		
		CWordArray awSensorCh;

		pos = sensorChList.GetHeadPosition();
		while(pos)
		{
			strTemp = sensorChList.GetNext(pos);		//format => ch001
			int nSensorChNo = atol(strTemp.Right(3));
			awSensorCh.Add((WORD)nSensorChNo);
		}

// 		//사용중인 Sensor Channel만 선택한다.
// 		for(int index = 0; index < EP_MAX_SENSOR_CH; index++)
// 		{
// 			int nChNo = index+1;
// 		}
		
		if(DownLoadRemoteDataA( strIPAddress, strRemoteFileLocation, strSaveDir, adwSelCh, awSensorCh, awFadmSelCh) == FALSE)
		{
			strTemp.Format(TEXT_LANG[52], strRemoteFileLocation);
			WriteLog(strTemp);
			return TRUE;
			//return FALSE;
		}
	}
	strTemp.Format(TEXT_LANG[53], strTrayID, adwSelCh.GetSize());
	WriteLog(strTemp);	
	
	return TRUE;
}

bool CDataDownDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CDataDownDlg"), _T("TEXT_CDataDownDlg_CNT"), _T("TEXT_CDataDownDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt];

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CDataDownDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CDataDownDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Languge error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}