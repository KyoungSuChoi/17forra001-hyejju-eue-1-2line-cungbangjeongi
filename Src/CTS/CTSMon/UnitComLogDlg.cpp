// UnitComLogDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ctsmon.h"
#include "UnitComLogDlg.h"
#include "FolderDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUnitComLogDlg dialog


CUnitComLogDlg::CUnitComLogDlg(CString strIpAddress, CWnd* pParent /*=NULL*/)
	: CDialog(CUnitComLogDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CUnitComLogDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_strIpAddress = strIpAddress;
}


void CUnitComLogDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CUnitComLogDlg)
	DDX_Control(pDX, IDC_SEL_LIST, m_wndSelList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CUnitComLogDlg, CDialog)
	//{{AFX_MSG_MAP(CUnitComLogDlg)
	ON_NOTIFY(NM_DBLCLK, IDC_SEL_LIST, OnDblclkSelList)
	ON_BN_CLICKED(IDC_BUTTON_SAVE, OnButtonSave)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDCANCEL, &CUnitComLogDlg::OnBnClickedCancel)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUnitComLogDlg message handlers

BOOL CUnitComLogDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	DWORD style = 	m_wndSelList.GetExtendedStyle();
	style |= LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_SUBITEMIMAGES;
	m_wndSelList.SetExtendedStyle(style );

	m_wndSelList.InsertColumn(0, "No", LVCFMT_LEFT, 50);
	m_wndSelList.InsertColumn(1, "File name", LVCFMT_LEFT, 350);
	m_wndSelList.InsertColumn(2, "Size", LVCFMT_LEFT, 100);

	LVITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.mask = LVIF_TEXT|LVIF_IMAGE;
	char szName[32];

	//원격 접속한다.

	CString strLoginID = AfxGetApp()->GetProfileString(FORM_FTP_SECTION, "Login ID", "root");
	CString strPassword = AfxGetApp()->GetProfileString(FORM_FTP_SECTION, "Login Password", "dusrnth");
	CString strLocation = AfxGetApp()->GetProfileString(FORM_FTP_SECTION, "Log Location", "formation_data/log");

	CWaitCursor wait;
	CInternetSession ftpSession;
	CFtpConnection *pFtpConnection = NULL;

	try
	{	
		ftpSession.SetOption(INTERNET_OPTION_CONNECT_TIMEOUT, 3000);
		pFtpConnection = ftpSession.GetFtpConnection(m_strIpAddress, strLoginID, strPassword);
		
		if(!pFtpConnection->SetCurrentDirectory(strLocation))
		{
			AfxMessageBox(GetStringTable(IDS_TEXT_SAVE_DIR_ERROR));
		}
		else
		{
			int nI = 0;
			int nSize = 0;
			CString strFile;
			CFtpFileFind pFileFind(pFtpConnection);
			BOOL bWorking = pFileFind.FindFile(_T("*"));
			while (bWorking)
			{
				bWorking = pFileFind.FindNextFile();
				strFile = pFileFind.GetFileName();
				nSize	= pFileFind.GetLength();
				TRACE("%s\n", strFile);
				
				sprintf(szName, "%d", nI+1);
				lvItem.iItem = nI;
				lvItem.iSubItem = 0;
				lvItem.pszText = szName;
				m_wndSelList.InsertItem(&lvItem);
				m_wndSelList.SetItemData(lvItem.iItem, nI);		//==>LVN_ITEMCHANGED 를 발생 기킴 
				m_wndSelList.SetItemText(nI, 1, strFile);
				nSize = nSize/1000.0f;
				if(nSize < 1)	nSize = 1;
				strFile.Format("%dKB", nSize);
				m_wndSelList.SetItemText(nI, 2, strFile);
				nI++;
			}
			pFileFind.Close();
		}
	}
	catch(CInternetException *e)
	{
		TCHAR sz[1024];
		e->GetErrorMessage(sz, 1024);
		AfxMessageBox(sz);
		e->Delete();
	}
	if(pFtpConnection)	
	{
		pFtpConnection->Close();	
	}
	
	ftpSession.Close();
	delete pFtpConnection;
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CUnitComLogDlg::OnDblclkSelList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
		
	POSITION pos = m_wndSelList.GetFirstSelectedItemPosition();
	if(pos == NULL)	return;
	int nItem = m_wndSelList.GetNextSelectedItem(pos);
	CString strSelFileName = m_wndSelList.GetItemText(nItem, 1);
	
	//Download file to temp. folder
	CString strLoginID = AfxGetApp()->GetProfileString(FORM_FTP_SECTION, "Login ID", "root");
	CString strPassword = AfxGetApp()->GetProfileString(FORM_FTP_SECTION, "Login Password", "dusrnth");
	CString strLocation = AfxGetApp()->GetProfileString(FORM_FTP_SECTION, "Log Location", "formation_data/log");

	CWaitCursor wait;
	CInternetSession ftpSession;
	CFtpConnection *pFtpConnection = NULL;

	try
	{
		pFtpConnection = ftpSession.GetFtpConnection(m_strIpAddress, strLoginID, strPassword);
		if(!pFtpConnection->SetCurrentDirectory(strLocation))
		{
			AfxMessageBox(GetStringTable(IDS_TEXT_SAVE_DIR_ERROR));
		}
		else
		{
			CString toFileName;
			toFileName.Format("%s\\Temp\\Unitlog.txt", AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION, "CTSMon"));
			if(pFtpConnection->GetFile(strSelFileName, toFileName, FALSE, FILE_ATTRIBUTE_NORMAL, FTP_TRANSFER_TYPE_ASCII) == FALSE)
			{
				MessageBox(strSelFileName +" download fail!!!", "Error", MB_OK|MB_ICONSTOP);
			}
			else
			{
				//Execute notepad
				STARTUPINFO	stStartUpInfo;
				PROCESS_INFORMATION	ProcessInfo;
				ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
				ZeroMemory(&ProcessInfo, sizeof(PROCESS_INFORMATION));
				
				stStartUpInfo.cb = sizeof(STARTUPINFO);
				stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
				stStartUpInfo.wShowWindow = SW_SHOWNORMAL;
				
				char szWinDir[256];
				GetWindowsDirectory(szWinDir, 255);
				CString strTemp;
				strTemp.Format("%s\\Notepad.exe %s", szWinDir, toFileName);
					
				BOOL bFlag = CreateProcess(NULL, (LPSTR)(LPCTSTR)strTemp, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &stStartUpInfo, &ProcessInfo);
				if(bFlag == FALSE)
				{
					strTemp.Format(GetStringTable(IDS_MSG_NOT_FOUND), toFileName);
					MessageBox(strTemp, "File not found", MB_OK|MB_ICONSTOP);
				}	
			}
		}
	}
	catch(CInternetException *e)
	{
		TCHAR sz[1024];
		e->GetErrorMessage(sz, 1024);
		AfxMessageBox(sz);
		e->Delete();
	}
	if(pFtpConnection) pFtpConnection->Close();
	ftpSession.Close();
	delete pFtpConnection;

	*pResult = 0;
}

void CUnitComLogDlg::OnButtonSave() 
{
	CFolderDialog dlg;
	CString strDir;
	if( dlg.DoModal() == IDOK)
	{
		strDir = dlg.GetPathName();
	}
	else 
	{
		return;
	}

	//Download file to temp. folder
	CString strLoginID = AfxGetApp()->GetProfileString(FORM_FTP_SECTION, "Login ID", "root");
	CString strPassword = AfxGetApp()->GetProfileString(FORM_FTP_SECTION, "Login Password", "dusrnth");
	CString strLocation = AfxGetApp()->GetProfileString(FORM_FTP_SECTION, "Log Location", "formation_data/log");

	int nCnt = 0;
	CWaitCursor wait;
	CInternetSession ftpSession;
	CFtpConnection *pFtpConnection = NULL;

	try
	{
		pFtpConnection = ftpSession.GetFtpConnection(m_strIpAddress, strLoginID, strPassword);
		if(!pFtpConnection->SetCurrentDirectory(strLocation))
		{
			AfxMessageBox(GetStringTable(IDS_TEXT_SAVE_DIR_ERROR));
		}
		else
		{
			POSITION pos = m_wndSelList.GetFirstSelectedItemPosition();
			while(pos)
			{
				int nItem = m_wndSelList.GetNextSelectedItem(pos);
				CString strSelFileName = m_wndSelList.GetItemText(nItem, 1);
				CString toFileName;
				toFileName.Format("%s\\%s", strDir, strSelFileName);
				if(pFtpConnection->GetFile(strSelFileName, toFileName, FALSE, FILE_ATTRIBUTE_NORMAL, FTP_TRANSFER_TYPE_ASCII) == FALSE)
				{
					MessageBox(strSelFileName +" download fail!!!", "Error", MB_OK|MB_ICONSTOP);
				}
				nCnt++;
			}
		}
	}
	catch(CInternetException *e)
	{
		TCHAR sz[1024];
		e->GetErrorMessage(sz, 1024);
		AfxMessageBox(sz);
		e->Delete();
	}
	if(pFtpConnection) pFtpConnection->Close();
	ftpSession.Close();
	delete pFtpConnection;

	if(nCnt > 0)
	{
		CString strTemp;
		strTemp.Format("Total %d file download complite.", nCnt);
		AfxMessageBox(strTemp);
	}
}

void CUnitComLogDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}
