// Step.h: interface for the CStep class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_STEP_H__B1C39ECF_2E5D_4089_85F8_D596E36E94A0__INCLUDED_)
#define AFX_STEP_H__B1C39ECF_2E5D_4089_85F8_D596E36E94A0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CStep  
{
public:
	BOOL IsGradeInclude();
	CString EndConditonString(CString strVUnit = "V", CString strIUnit = "mA");
/*	BOOL SetLoopStep(EP_LOOP_STEP *pStep);
	BOOL SetDisChargeStep(EP_DISCHARGE_STEP *pStep);
	BOOL SetRestStep(EP_REST_STEP *pStep);
	BOOL SetOcvStep(EP_OCV_STEP *pStep);
	BOOL SetEndStep(EP_END_STEP *pStep);
	BOOL SetChargeStep(EP_CHARGE_STEP *pStep);
	BOOL SetImpStep(EP_IMPEDANCE_STEP *pStep);
*/	
	
	BOOL m_bOverCChk; //20200411 ?? ?? ???? 0?? ??
	BOOL SetStepData(STR_COMMON_STEP *pStep);
	STR_COMMON_STEP GetStepData();
	BOOL GetNetStepData(LPVOID &lpData, int &rSize, int nProcessNo); //20200411 ??  ???? ?? ?? (????)	
//	BOOL GetNetStepData(LPVOID &lpData, int &rSize);
	BOOL GetNetStepGradeData(LPVOID &lpData, int &rSize);
	
	BOOL ClearStepData();
	CStep(int nStepType /*=0*/);
	CStep();
	virtual ~CStep();
//	void operator = (CStep &step); 

	BYTE	m_type;			//Step Type
	BYTE	m_StepIndex;	//StepNo
	BYTE	m_mode;
//	BYTE	m_bGrade;
	long	m_lProcType;				
	float	m_fVref;
    float	m_fIref;
    
	/* Step End Value */
	float	m_fEndTime;
    float	m_fEndV;
    float	m_fEndI;
    float	m_fEndC;
    float	m_fEndDV;
    float	m_fEndDI;

	/*Step Fail Value */
	float	m_fHighLimitV;
    float	m_fLowLimitV;
    float	m_fHighLimitI;
    float	m_fLowLimitI;
    float	m_fHighLimitC;
    float	m_fLowLimitC;
	float 	m_fHighLimitImp;
    float	m_fLowLimitImp;

	float	m_fCompTimeV[EP_COMP_POINT];			
	float	m_fCompVLow[EP_COMP_POINT];			// 1 : 전압취득하한
 	float	m_fCompVHigh[EP_COMP_POINT];		// 1 : 전압취득상한
	float	m_fCompTimeI[EP_COMP_POINT];		// 1 : 전압취득시간
	float	m_fCompILow[EP_COMP_POINT];
	float	m_fCompIHigh[EP_COMP_POINT];
	
	float	m_fDeltaTimeV;
    float	m_fDeltaV;
    float	m_fDeltaTimeI;
    float	m_fDeltaI;

	BOOL	m_bUseActucalCap;
	BYTE	m_UseStepContinue;
	long	m_lCapaRefStepIndex;
	float	m_fSocRate;

	CGrading	m_Grading;
	CGrading	m_ex1Grading;
	CGrading	m_ex2Grading;

	BOOL	m_bSkipStep;
	long	m_StepID;

	long	m_lCycleNo;
	long	m_lGotoStep;

	//record condition
	float		m_fRecDeltaTime;
	float		m_fRecDeltaV;
	float		m_fRecDeltaI;
	float		m_fRecDeltaT;
	long		m_lRecVIGetTime;
	
	//capacity check condition
	float		m_fParam1;		//parameter 1  ex) EDLC : 용량측정 전압1(Low) , etc
	float		m_fParam2;		//parameter 1  ex) EDLC : 용량측정 전압2(High) , etc
	
	long		m_lLowVoltageChkT;
	
	unsigned char	m_fmsType;

	long		m_lDCIR_RegTemp;
	long		m_lDCIR_ResistanceRate;

	float	m_fCompChgCcVtg;
	float	m_fCompChgCcTime;
	float	m_fCompChgCcDeltaVtg;

	float	m_fCompChgCvCrt;
	float	m_fCompChgCvTime;
	float	m_fCompChgCvDeltaCrt;

	unsigned char m_nFanOffFlag;	
protected:
	CString TimeString(double dTime);
	CString ValueString(double dData, BOOL bMiliUnit = FALSE);
};

#endif // !defined(AFX_STEP_H__B1C39ECF_2E5D_4089_85F8_D596E36E94A0__INCLUDED_)
