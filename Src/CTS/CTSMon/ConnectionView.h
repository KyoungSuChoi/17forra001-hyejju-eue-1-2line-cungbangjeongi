#pragma once
#include "afxwin.h"

// CConnectionView 폼 뷰입니다.
#include "MyGridWnd.h"
#include "CTSMonDoc.h"
#include "afxcmn.h"

class CConnectionView : public CFormView
{
	DECLARE_DYNCREATE(CConnectionView)

protected:
	CConnectionView();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CConnectionView();

public:
	enum { IDD = IDD_CONNECTION_VIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

public:
	void InitLabel();
	void InitChannelCodeList();
	bool LoadChannelCode();	

	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	CCTSMonDoc* GetDocument();
	void UpdateGroupState(int nModuleID, int nGroupIndex=0);	
	void ModuleConnected(int nModuleID);
	void ModuleDisConnected(int nModuleID);
	void GroupSetChanged(int nColCount = -1);
	void TopConfigChanged(); 
	void InitSmallChGrid();

	BOOL GetChRowCol(int nItemIndex, int nChindex, ROWCOL &nRow, ROWCOL &nCol);
	void StopMonitoring();
	void StartMonitoring();
	void DrawConnectionCode();
	void DrawChannel();
	void SetTopGridFont();
	void SetCurrentModule(int nModuleID, int nGroupIndex = 0);		// Stage 선택시 반드시 실행되는 부분
	CString GetTargetModuleName();

public:
	STR_TOP_CONFIG		*m_pConfig;
	CMyGridWnd	m_wndSmallChGrid;

	int m_nTopGridRowCount;
	int m_nTopGridColCount;
	UINT m_nDisplayTimer;
	int m_nCurModuleID;	
	int m_nCurGroup;
	int m_nCurChannel;
	int m_nCurTrayIndex;

	SHORT	*m_pRowCol2GpCh;
	

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CLabel m_LabelViewName;
	virtual void OnInitialUpdate();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	CLabel m_CmdTarget;
	CListCtrl m_ctrlChErrorList;
};

#ifndef _DEBUG  // debug version in TopView.cpp
inline CCTSMonDoc* CConnectionView::GetDocument()
   { return (CCTSMonDoc*)m_pDocument; }
#endif