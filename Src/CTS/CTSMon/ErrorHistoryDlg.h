#pragma once


// CErrorHistoryDlg 대화 상자입니다.
#include "CTSMonDoc.h"
#include "MyGridWnd.h"
//  #include "afxwin.h"
#include "StdAfx.h"
#include "afxwin.h"

class CErrorHistoryDlg : public CDialog
{
	DECLARE_DYNAMIC(CErrorHistoryDlg)

public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	CErrorHistoryDlg(CCTSMonDoc *pDoc = NULL, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CErrorHistoryDlg();
	
	CCTSMonDoc *m_pDoc;
	
	CMyGridWnd m_wndFieldGrid;
	
	enum { ID_COL_DATE = 1, ID_COL_STAGENO, ID_COL_TRAYNO, ID_COL_TYPE, ID_COL_MODE, ID_COL_CODE, ID_COL_ERRMSG };
	
	void InitLabel();
	void InitFieldGrid();
	void InitColorBtn();
	bool LoadEmgFromProductsMDB();
	void ClearGrid();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ERROR_HISTORY_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();	
	afx_msg void OnBnClickedOk();	
	afx_msg void OnBnClickedBtnExcelConvert();
	CLabel m_Label1;
	CColorButton2 m_BtnClose;
	CColorButton2 m_BtnExcelConvert;	
};
