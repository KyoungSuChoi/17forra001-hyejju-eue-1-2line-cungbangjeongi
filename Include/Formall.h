#ifndef _EP_FORMATION_HEADER_INCLUDE_
#define _EP_FORMATION_HEADER_INCLUDE_

#include "epdlldef.h"			//Inculde NewMsgdef.h(Network), Dataform.h(Structure)
#include "EPProcType.h"		
#include "EPDataClt.h"			//DataBase Server에 Socket 사용	->EPDatasvr.h
#include "BFCtrlAll.h"			//control class
#include "SerialCom.h"			//serial 통신관련
#include "BFCalibration.h"		//Calibration

#include "Customer.h"			//업체별 정의 


#endif	//_EP_FORMATION_HEADER_INCLUDE_