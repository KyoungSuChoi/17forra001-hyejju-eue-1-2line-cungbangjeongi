#if !defined(AFX_SAVEOPTIONDLG_H__220B1C79_8B57_4C63_A28F_F8EA3675D63D__INCLUDED_)
#define AFX_SAVEOPTIONDLG_H__220B1C79_8B57_4C63_A28F_F8EA3675D63D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SaveOptionDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSaveOptionDlg dialog

#define BF_MAX_TEST_REAL_TIME_DATA	20

class CSaveOptionDlg : public CDialog
{
// Construction
public:
	CString m_strTempDir;
	CString UnitString(int type);
	CString ValueString(double dData, int nType);
	BOOL	LoadRegSetting();
	CSaveOptionDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSaveOptionDlg)
	enum { IDD = IDD_DOWN_OPTION_DIALOG };
	int		m_nDeltaTime;
	int		m_nDeltaV;
	int		m_nDeltaI;
	BOOL	m_bStepNo;
	BOOL	m_bState;
	BOOL	m_bStepTime;
	BOOL	m_bVoltage;
	BOOL	m_bCurrent;
	BOOL	m_bCapacity;
	BOOL	m_bImpedance;
	BOOL	m_bWatt;
	BOOL	m_bWattHour;
	BOOL	m_bGradeCode;
	BOOL	m_bCellCode;
	int		m_nVUnit;
	int		m_nTUnit;
	int		m_nIUnit;
	int		m_nCUnit;
	int		m_nZUnit;
	int		m_nWUnit;
	int		m_nWhUnit;
	int		m_nVPrecision;
	int		m_nIPrecision;
	int		m_nCPrecision;
	int		m_nZPrecision;
	int		m_nWPrecision;
	int		m_nWhPrecision;
	BOOL	m_bTimeSum;
	int		m_nTypeSelCombo;
	BOOL	m_bSaveCharge;
	BOOL	m_bSaveDischarge;
	BOOL	m_bSaveRest;
	BOOL	m_bSaveImpdeacne;
	int		m_nTotTUnit;
	BOOL	m_bTempData;
	BOOL	m_bStepType;
	UINT	m_nHistoryCount;
	BOOL	m_bAuxVData;
	BOOL	m_bSaveDate;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSaveOptionDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSaveOptionDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnButtonDir();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SAVEOPTIONDLG_H__220B1C79_8B57_4C63_A28F_F8EA3675D63D__INCLUDED_)
