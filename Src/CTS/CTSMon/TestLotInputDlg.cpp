// TestLotInputDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSMon.h"
#include "TestLotInputDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTestLotInputDlg dialog


CTestLotInputDlg::CTestLotInputDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTestLotInputDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTestLotInputDlg)
	m_strLot = _T("");
	m_strTestName = _T("Not Selected");
	m_strTrayNo = _T("");
	//}}AFX_DATA_INIT
}

void CTestLotInputDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTestLotInputDlg)
	DDX_Control(pDX, IDC_SELECTED_TEST_NAME, m_ctrlSelTestName);
	DDX_Text(pDX, IDC_LOT_NO, m_strLot);
	DDV_MaxChars(pDX, m_strLot, EP_LOT_NAME_LENGTH);
	DDX_Text(pDX, IDC_TRAY_NO, m_strTrayNo);
	DDV_MaxChars(pDX, m_strTrayNo, EP_TRAY_NAME_LENGTH);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTestLotInputDlg, CDialog)
	//{{AFX_MSG_MAP(CTestLotInputDlg)
	ON_BN_CLICKED(IDOK, OnOk)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestLotInputDlg message handlers

void CTestLotInputDlg::OnOk() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	
	if(m_strLot.GetLength() > LOT_NAME_LENGTH)
	{
		m_strLot = m_strLot.Left(LOT_NAME_LENGTH);
	}
	CDialog::OnOK();
}

BOOL CTestLotInputDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_ctrlSelTestName.SetText(m_strTestName);

	if(!m_strTitleLabelString.IsEmpty())
	{
		GetDlgItem(IDC_STATIC_TITLE)->SetWindowText(m_strTitleLabelString);
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
