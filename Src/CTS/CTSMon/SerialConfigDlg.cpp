// SerialConfigDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSMon.h"
#include "SerialConfigDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSerialConfigDlg dialog


CSerialConfigDlg::CSerialConfigDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSerialConfigDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSerialConfigDlg)
	m_bUsePort1 = FALSE;
	//}}AFX_DATA_INIT
	m_SerialConfig[0].nPortNum =1;
	m_SerialConfig[0].nPortBaudRate = 9600;
	m_SerialConfig[0].nPortDataBits = 8;
	m_SerialConfig[0].nPortStopBits = 1;
	m_SerialConfig[0].nPortBuffer = 512;
	m_SerialConfig[0].portParity = 'N';
	m_SerialConfig[0].nPortEvents = EV_CTS | EV_BREAK | EV_ERR | EV_RXCHAR | EV_RXFLAG;

	m_SerialConfig[1].nPortNum = 2;
	m_SerialConfig[1].nPortBaudRate = 9600;
	m_SerialConfig[1].nPortDataBits = 8;
	m_SerialConfig[1].nPortStopBits = 1;
	m_SerialConfig[1].nPortBuffer = 512;
	m_SerialConfig[1].portParity = 'N';
	m_SerialConfig[1].nPortEvents = EV_CTS | EV_BREAK | EV_ERR | EV_RXCHAR | EV_RXFLAG;

	m_ParityStr.Format("NOEMS");

}

void CSerialConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	//{{AFX_DATA_MAP(CSerialConfigDlg)
	DDX_CBString(pDX, IDC_SERIAL_BAUDRATECOMBO, m_strBaudRate);
	DDX_CBString(pDX, IDC_SERIAL_DATABITSCOMBO, m_strDataBits);
	DDX_CBString(pDX, IDC_SERIAL_STOPBITSCOMBO, m_strStopBits);
	DDX_CBString(pDX, IDC_SERIAL_PORTNUMCOMBO, m_strPortNum);
	DDX_CBString(pDX, IDC_SERIAL_SENDBUFFERCOMBO, m_strSendBuffer);
	DDX_CBIndex(pDX, IDC_SERIAL_PARITYCOMBO, m_nParity);
	DDX_CBString(pDX, IDC_COMBO1, m_strPortNum1);
	DDX_CBString(pDX, IDC_COMBO2, m_strBaudRate1);
	DDX_CBIndex(pDX, IDC_COMBO3, m_nParity1);
	DDX_CBString(pDX, IDC_COMBO4, m_strDataBits1);
	DDX_CBString(pDX, IDC_COMBO5, m_strStopBits1);
	DDX_CBString(pDX, IDC_COMBO6, m_strSendBuffer1);
	DDX_Check(pDX, IDC_USE_CHECK, m_bUsePort1);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSerialConfigDlg, CDialog)
	//{{AFX_MSG_MAP(CSerialConfigDlg)
	ON_BN_CLICKED(IDOK, OnOk)
	ON_BN_CLICKED(IDC_USE_CHECK, OnUseCheck)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSerialConfigDlg message handlers

BOOL CSerialConfigDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_strPortNum.Format("%d", m_SerialConfig[0].nPortNum);
	m_strBaudRate.Format("%d", m_SerialConfig[0].nPortBaudRate);
	m_nParity = m_ParityStr.Find(m_SerialConfig[0].portParity);
	m_strDataBits.Format("%d", m_SerialConfig[0].nPortDataBits);
	m_strStopBits.Format("%d", m_SerialConfig[0].nPortStopBits);
	m_strSendBuffer.Format("%d", m_SerialConfig[0].nPortBuffer);

	m_strPortNum1.Format("%d", m_SerialConfig[1].nPortNum);
	m_strBaudRate1.Format("%d", m_SerialConfig[1].nPortBaudRate);
	m_nParity1 = m_ParityStr.Find(m_SerialConfig[1].portParity);
	m_strDataBits1.Format("%d", m_SerialConfig[1].nPortDataBits);
	m_strStopBits1.Format("%d", m_SerialConfig[1].nPortStopBits);
	m_strSendBuffer1.Format("%d", m_SerialConfig[1].nPortBuffer);

	m_bUsePort1 = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "Bar Code Reader", FALSE);

	UpdateData(FALSE);

	OnUseCheck();
	
	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSerialConfigDlg::OnOk() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

	m_SerialConfig[0].nPortNum			= atol(m_strPortNum);
	m_SerialConfig[0].nPortBaudRate	= atol(m_strBaudRate);
	m_SerialConfig[0].portParity		= m_ParityStr[m_nParity];
	m_SerialConfig[0].nPortDataBits	= atol(m_strDataBits);
	m_SerialConfig[0].nPortStopBits	= atol(m_strStopBits);
	m_SerialConfig[0].nPortBuffer		= atol(m_strSendBuffer);

	m_SerialConfig[1].nPortNum			= atol(m_strPortNum1);
	m_SerialConfig[1].nPortBaudRate	= atol(m_strBaudRate1);
	m_SerialConfig[1].portParity		= m_ParityStr[m_nParity1];
	m_SerialConfig[1].nPortDataBits	= atol(m_strDataBits1);
	m_SerialConfig[1].nPortStopBits	= atol(m_strStopBits1);
	m_SerialConfig[1].nPortBuffer		= atol(m_strSendBuffer1);

	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "Bar Code Reader", m_bUsePort1);

	CDialog::OnOK();	
}

void CSerialConfigDlg::OnUseCheck() 
{
	// TODO: Add your control notification handler code here
	UpdateData();

	GetDlgItem(IDC_SERIAL_PORTNUMCOMBO)->EnableWindow(m_bUsePort1);
	GetDlgItem(IDC_SERIAL_BAUDRATECOMBO)->EnableWindow(m_bUsePort1);
	GetDlgItem(IDC_SERIAL_PARITYCOMBO)->EnableWindow(m_bUsePort1);
	GetDlgItem(IDC_SERIAL_DATABITSCOMBO)->EnableWindow(m_bUsePort1);
	GetDlgItem(IDC_SERIAL_STOPBITSCOMBO)->EnableWindow(m_bUsePort1);
}
