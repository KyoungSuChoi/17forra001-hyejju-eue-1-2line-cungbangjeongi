// SettingCheckView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "SettingCheckDlg.h"


// CSettingCheckView


CSettingCheckDlg::CSettingCheckDlg(CWnd* pParent /*=NULL*/,CCTSMonDoc* pDoc)
	: CDialog(CSettingCheckDlg::IDD, pParent)
{
	m_nCurModuleID = 0;
	m_pDoc = pDoc;
	m_nMaxStageCnt = m_pDoc->GetInstalledModuleNum();

}

CSettingCheckDlg::~CSettingCheckDlg()
{
}

void CSettingCheckDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LABEL_HW_FAULT_SETTING, m_labelHwFault);
	DDX_Control(pDX, IDC_LABEL_INTERNAL_FLAG, m_labelInternalFlag);
	DDX_Control(pDX, IDC_LABEL_TEMP, m_labelTemp);
	DDX_Control(pDX, ID_DLG_LABEL_NAME, m_LabelViewName);
	DDX_Control(pDX, IDC_STAGE_NAME, m_CmdTarget);

	

	DDX_Control(pDX, IDC_COMBO_STAGE_CHANGE, m_comboStageID);
}

BEGIN_MESSAGE_MAP(CSettingCheckDlg, CDialog)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
	ON_BN_CLICKED(IDC_BTN_REFRESH, &CSettingCheckDlg::OnBnClickedBtnRefresh)
	ON_BN_CLICKED(IDC_BTN_SAVE_NAME, &CSettingCheckDlg::OnBnClickedBtnSaveName)
	ON_WM_SIZE()
	ON_CBN_SELCHANGE(IDC_COMBO_STAGE_CHANGE, &CSettingCheckDlg::OnCbnSelchangeComboStageChange)
	ON_BN_CLICKED(IDC_BTN_EQUALS_CHECK, &CSettingCheckDlg::OnBnClickedBtnEqualsCheck)
	ON_BN_CLICKED(IDC_BTN_SAVE_EXCEL, &CSettingCheckDlg::OnBnClickedBtnSaveExcel)
END_MESSAGE_MAP()



// CSettingCheckView 메시지 처리기입니다.

BOOL CSettingCheckDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	initCtrl();
	initName();
	initValue();

	return TRUE;
}

void CSettingCheckDlg::initCtrl()
{
	initGrid();
	initLabel();
	initFont();
	initCombo();
}

void CSettingCheckDlg::initCombo()
{
	int nModuleID = 1;	

	for(int i=0; i<m_nMaxStageCnt; i++ )
	{
		nModuleID = EPGetModuleID(i);
	
		m_comboStageID.AddString(::GetModuleName(nModuleID));
		m_comboStageID.SetItemData(i, nModuleID);
	}
}
void CSettingCheckDlg::initFont()
{
	LOGFONT LogFont;

	m_comboStageID.GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight = 1000;
	LogFont.lfHeight = 35;

	m_Font.CreateFontIndirect( &LogFont );

	m_comboStageID.SetFont(&m_Font);
}


void CSettingCheckDlg::initGrid()
{
	m_gridInternalFlag.SubclassDlgItem(IDC_GRID_INTERNAL_FLAG, this);
	m_gridInternalFlag.m_bSameRowSize = FALSE;
	m_gridInternalFlag.m_bSameColSize = FALSE;
	//---------------------------------------------------------------------//
	m_gridInternalFlag.m_bCustomWidth = TRUE;
	m_gridInternalFlag.m_bCustomColor = FALSE;

	m_gridInternalFlag.Initialize();

	m_gridInternalFlag.SetColCount(3);     
	m_gridInternalFlag.SetRowCount(MAX_INTERNAL_FALG_CNT);        
	m_gridInternalFlag.SetDefaultRowHeight(20);

	m_gridInternalFlag.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	m_gridInternalFlag.SetScrollBarMode(SB_VERT, gxnEnabled | gxnEnhanced);	//Only Vertial Scroll Bar

	m_gridInternalFlag.EnableGridToolTips();
	m_gridInternalFlag.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));


	m_gridInternalFlag.GetParam()->SetSortRowsOnDblClk(TRUE);

	m_gridInternalFlag.m_nWidth[1] = 50;
	m_gridInternalFlag.m_nWidth[2] = 250;		

	m_gridInternalFlag.SetStyleRange(CGXRange().SetCols(2),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_EDIT)
		.SetMaxLength(20)
		);
	//Row Header Setting
	m_gridInternalFlag.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(9).SetFaceName("굴림")).SetWrapText(FALSE).SetAutoSize(TRUE));
	m_gridInternalFlag.SetStyleRange(CGXRange().SetCols(1),	CGXStyle().SetInterior(RGB(240, 240, 240)));

	m_gridInternalFlag.SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetInterior(RGB(220, 220, 220)));
	m_gridInternalFlag.SetStyleRange(CGXRange().SetCols(3), CGXStyle().SetInterior(RGB(200, 200, 200)));
	m_gridInternalFlag.SetValueRange(CGXRange( 0, 1), "No"); //"순서"
	m_gridInternalFlag.SetValueRange(CGXRange( 0, 2), "설정명"); //"StepNo"
	m_gridInternalFlag.SetValueRange(CGXRange( 0, 3), "수치"); //"StepType"



	//	m_wndDataList.SetScrollBarMode(SB_BOTH, gxnDisabled,TRUE);

	for(LONG i =0; i<MAX_INTERNAL_FALG_CNT; i++)
	{
		m_gridInternalFlag.SetValueRange(CGXRange(i+1,1), i+1);
	}

	m_gridInternalFlag.Redraw();


	////=====================================================================================================================

	m_gridTemp.SubclassDlgItem(IDC_GRID_TEMP, this);
	m_gridTemp.m_bSameRowSize = FALSE;
	m_gridTemp.m_bSameColSize = FALSE;
	//---------------------------------------------------------------------//
	m_gridTemp.m_bCustomWidth = TRUE;
	m_gridTemp.m_bCustomColor = FALSE;

	m_gridTemp.Initialize();

	m_gridTemp.SetColCount(3);     
	m_gridTemp.SetRowCount(MAX_INTERNAL_FALG_CNT);        
	m_gridTemp.SetDefaultRowHeight(20);

	m_gridTemp.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	m_gridTemp.SetScrollBarMode(SB_VERT, gxnEnabled | gxnEnhanced);	//Only Vertial Scroll Bar

	m_gridTemp.EnableGridToolTips();
	m_gridTemp.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));

	m_gridTemp.GetParam()->SetSortRowsOnDblClk(TRUE);

	m_gridTemp.m_nWidth[1] = 50;
	m_gridTemp.m_nWidth[2] = 250;		

	m_gridTemp.SetStyleRange(CGXRange().SetCols(2),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_EDIT)
		.SetMaxLength(20)
		);
	//Row Header Setting
	m_gridTemp.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(9).SetFaceName("굴림")).SetWrapText(FALSE).SetAutoSize(TRUE));
	m_gridTemp.SetStyleRange(CGXRange().SetCols(1),	CGXStyle().SetInterior(RGB(240, 240, 240)));
	m_gridTemp.SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetInterior(RGB(220, 220, 220)));
	m_gridTemp.SetStyleRange(CGXRange().SetCols(3), CGXStyle().SetInterior(RGB(200, 200, 200)));
	m_gridTemp.SetValueRange(CGXRange( 0, 1), "No"); //"순서"
	m_gridTemp.SetValueRange(CGXRange( 0, 2), "Name"); //"StepNo"
	m_gridTemp.SetValueRange(CGXRange( 0, 3), "Value"); //"StepType"

	//	m_wndDataList.SetScrollBarMode(SB_BOTH, gxnDisabled,TRUE);

	for(LONG i =0; i<MAX_TEMP_CNT; i++)
	{
		m_gridTemp.SetValueRange(CGXRange(i+1,1), i+1);
	}

	m_gridTemp.Redraw();



	////=====================================================================================================================

	m_gridHwFaultSet.SubclassDlgItem(IDC_GRID_HW_FAULT_SET, this);
	m_gridHwFaultSet.m_bSameRowSize = FALSE;
	m_gridHwFaultSet.m_bSameColSize = FALSE;
	//---------------------------------------------------------------------//
	m_gridHwFaultSet.m_bCustomWidth = TRUE;
	m_gridHwFaultSet.m_bCustomColor = FALSE;

	m_gridHwFaultSet.Initialize();
	m_gridHwFaultSet.SetColCount(3);     
	m_gridHwFaultSet.SetRowCount(MAX_HW_FAULT_CNT);        
	m_gridHwFaultSet.SetDefaultRowHeight(20);

	m_gridHwFaultSet.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	m_gridHwFaultSet.SetScrollBarMode(SB_VERT, gxnEnabled | gxnEnhanced);	//Only Vertial Scroll Bar


	m_gridHwFaultSet.EnableGridToolTips();
	m_gridHwFaultSet.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));


	m_gridHwFaultSet.GetParam()->SetSortRowsOnDblClk(TRUE);
	m_gridHwFaultSet.GetParam()->GetProperties()->SetDisplayHorzLines(TRUE);
	m_gridHwFaultSet.GetParam()->GetProperties()->SetDisplayVertLines(TRUE);

	m_gridHwFaultSet.m_nWidth[1] = 50;
	m_gridHwFaultSet.m_nWidth[2] = 250;		

	m_gridHwFaultSet.SetStyleRange(CGXRange().SetCols(2),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_EDIT)
		.SetMaxLength(20)
		);
	//Row Header Setting
	m_gridHwFaultSet.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(9).SetFaceName("굴림")).SetWrapText(FALSE).SetAutoSize(TRUE));
	m_gridHwFaultSet.SetStyleRange(CGXRange().SetCols(1),CGXStyle().SetInterior(RGB(240,240,240)));
	m_gridHwFaultSet.SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetInterior(RGB(220, 220, 220)));
	m_gridHwFaultSet.SetStyleRange(CGXRange().SetCols(3), CGXStyle().SetInterior(RGB(200, 200, 200)));
	m_gridHwFaultSet.SetValueRange(CGXRange( 0, 1), "No"); //"순서"
	m_gridHwFaultSet.SetValueRange(CGXRange( 0, 2), "설정명"); //"StepNo"
	m_gridHwFaultSet.SetValueRange(CGXRange( 0, 3), "수치"); //"StepType"

	//	m_wndDataList.SetScrollBarMode(SB_BOTH, gxnDisabled,TRUE);

	for(LONG i =0; i<MAX_HW_FAULT_CNT; i++)
	{
		m_gridHwFaultSet.SetValueRange(CGXRange(i+1,1), i+1);
	}
	m_gridHwFaultSet.Redraw();


}


void CSettingCheckDlg::initLabel()
{
	m_labelHwFault.SetFontSize(20)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE);

	m_labelInternalFlag.SetFontSize(20)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE);

	m_labelTemp.SetFontSize(20)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE);

	m_CmdTarget.SetFontSize(20)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND_STAGENAME)
		.SetFontBold(TRUE);

	m_LabelViewName.SetBkColor(RGB_MIDNIGHTBLUE);
	m_LabelViewName.SetTextColor(RGB_WHITE);
	m_LabelViewName.SetFontSize(24);
	m_LabelViewName.SetFontBold(TRUE);
	m_LabelViewName.SetText("SettingCheck"); //"Monitoring"


}


void CSettingCheckDlg::initName()
{
	CString strDefault;
	CString strKey;;
	CString strName;

	for(LONG i =1; i <= MAX_HW_FAULT_CNT; i++)
	{
		strDefault.Format("Value %d", i);
		strKey.Format("HwFaultName_%d",i);
		strName = AfxGetApp()->GetProfileString(FORM_SETTING_CHECK_SECTION, strKey, strDefault);
		m_gridHwFaultSet.SetValueRange(CGXRange(i,2), strName);
		if(strName.IsEmpty())
		{
			m_bHwFaultEmpty[i-1] = true;
		}
		else
		{
			m_bHwFaultEmpty[i-1] = false;
		}
	}

	for(LONG i =1; i <= MAX_INTERNAL_FALG_CNT; i++)
	{
		strDefault.Format("Flag %d", i);
		strKey.Format("InternalFlagName_%d",i);
		strName = AfxGetApp()->GetProfileString(FORM_SETTING_CHECK_SECTION, strKey, strDefault);
		m_gridInternalFlag.SetValueRange(CGXRange(i,2), strName);
		if(strName.IsEmpty())
		{
			m_bInternalFlagEmpty[i-1] = true;
		}
		else
		{
			m_bInternalFlagEmpty[i-1] = false;
		}
	}


	for(LONG i =1; i <= MAX_TEMP_CNT; i++)
	{
		strDefault.Format("Temp %d", i);
		strKey.Format("Temp_%d",i);
		strName = AfxGetApp()->GetProfileString(FORM_SETTING_CHECK_SECTION, strKey, strDefault);
		m_gridTemp.SetValueRange(CGXRange(i,2), strName);
		if(strName.IsEmpty())
		{
			m_bTempEmpty[i-1] = true;
		}
		else
		{
			m_bTempEmpty[i-1] = false;
		}
	}

	m_gridHwFaultSet.RedrawWindow();
	m_gridInternalFlag.RedrawWindow();
	m_gridTemp.RedrawWindow();
}

void CSettingCheckDlg::initValue()
{
	for(LONG i =1; i <= MAX_HW_FAULT_CNT; i++)
	{
		m_gridHwFaultSet.SetValueRange(CGXRange(i,3), "-");
		m_gridHwFaultSet.SetStyleRange(CGXRange(i,3), CGXStyle().SetInterior(RGB(200, 200, 200)));
	}
	for(LONG i =1; i <= MAX_INTERNAL_FALG_CNT; i++)
	{
		m_gridInternalFlag.SetValueRange(CGXRange(i,3), "-");
		m_gridInternalFlag.SetStyleRange(CGXRange(i,3), CGXStyle().SetInterior(RGB(200, 200, 200)));
	}
	for(LONG i =1; i <= MAX_TEMP_CNT; i++)
	{
		m_gridTemp.SetValueRange(CGXRange(i,3), "-");
		m_gridTemp.SetStyleRange(CGXRange(i,3), CGXStyle().SetInterior(RGB(200, 200, 200)));
	}

	m_gridHwFaultSet.RedrawWindow();
	m_gridInternalFlag.RedrawWindow();
	m_gridTemp.RedrawWindow();
}


void CSettingCheckDlg::ModuleConnected(int nModuleID)
{
	if(nModuleID == m_nCurModuleID)							//현재 선택된 Module이면 Grid를 Setting 시키고
	{	
		UpdateAllData();
	}	
}

void CSettingCheckDlg::ModuleDisConnected(int nModuleID)
{
	if(nModuleID == m_nCurModuleID)							//현재 선택된 Module이면 Grid를 Setting 시키고
	{	
		initValue();
	}
}

void CSettingCheckDlg::SetCurrentModule(int nModuleID)
{
	m_nCurModuleID = nModuleID;
	m_CmdTarget.SetText(GetTargetModuleName());
	m_comboStageID.SetCurSel(nModuleID-1);

	EPSendCommand( m_nCurModuleID, 0, 0, EP_CMD_SBC_PARAM_REQUEST);
}

CString CSettingCheckDlg::GetTargetModuleName()
{
	CString temp;
	CString strMDName = GetModuleName(m_nCurModuleID);	
	temp.Format("Stage No. %s", strMDName);	
	return temp;
}


void CSettingCheckDlg::RecvData(int nModuleID)
{
	if(nModuleID == m_nCurModuleID)							//현재 선택된 Module이면 Grid를 Setting 시키고
	{	
		UpdateAllData();
	}	
}
void CSettingCheckDlg::UpdateAllData()
{
	WORD state = EPGetGroupState(m_nCurModuleID);

	EP_MD_SBC_PARAM *lpSbcParam;
	lpSbcParam = EPGetModuleSbcParam(EPGetModuleIndex(m_nCurModuleID));

	if(lpSbcParam == NULL)
	{
		return;
	}
	if( state == EP_STATE_LINE_OFF )
	{
		initValue();
		return;
	}

	CString strTemp;
	for(int i=0; i<MAX_HW_FAULT_CNT; i++)
	{
		if(m_bHwFaultEmpty[i] == FALSE)
		{
			strTemp.Format("%d", lpSbcParam->lFaultSettingValue[i]);
		}
		else
		{
			strTemp = "-";
		}
		m_gridHwFaultSet.SetValueRange(CGXRange(i+1,3), strTemp);
	}
	m_gridHwFaultSet.RedrawWindow();

	for(int i=0; i<MAX_INTERNAL_FALG_CNT; i++)
	{
		if(m_bInternalFlagEmpty[i] == FALSE)
		{
			if(lpSbcParam->lSafetyFlag[i] == 0)
			{
				m_gridInternalFlag.SetValueRange(CGXRange(i+1,3), "N");
				m_gridInternalFlag.SetStyleRange(CGXRange(i+1,3), CGXStyle().SetInterior(RGB(237, 28, 36)));
			}
			if(lpSbcParam->lSafetyFlag[i] == 1)
			{
				m_gridInternalFlag.SetValueRange(CGXRange(i+1,3), "Y");
				m_gridInternalFlag.SetStyleRange(CGXRange(i+1,3), CGXStyle().SetInterior(RGB(34, 177, 76)));
			}
		}
		else
		{
			m_gridInternalFlag.SetValueRange(CGXRange(i+1,3), "-");
			m_gridInternalFlag.SetStyleRange(CGXRange(i+1,3), CGXStyle().SetInterior(RGB(200, 200, 200)));
		}
	}
	m_gridInternalFlag.RedrawWindow();

	for(int i=0; i<MAX_TEMP_CNT; i++)
	{

		if(m_bTempEmpty[i] == FALSE)
		{
			if(lpSbcParam->lTemp[i] == 0)
			{
				m_gridTemp.SetValueRange(CGXRange(i+1,3), "N");
				m_gridTemp.SetStyleRange(CGXRange(i+1,3), CGXStyle().SetInterior(RGB(237, 28, 36)));
			}
			if(lpSbcParam->lTemp[i] == 1)
			{
				m_gridTemp.SetValueRange(CGXRange(i+1,3), "Y");
				m_gridTemp.SetStyleRange(CGXRange(i+1,3), CGXStyle().SetInterior(RGB(34, 177, 76)));
			}
		}
		else
		{
			m_gridTemp.SetValueRange(CGXRange(i+1,3), "-");
			m_gridTemp.SetStyleRange(CGXRange(i+1,3), CGXStyle().SetInterior(RGB(200, 200, 200)));
		}

	}
	m_gridTemp.RedrawWindow();
}

void CSettingCheckDlg::OnBnClickedBtnRefresh()
{
	initGridNum();
	initValue();	
	int nRtn = 0;

	if((nRtn = EPSendCommand( m_nCurModuleID, 0, 0, EP_CMD_SBC_PARAM_REQUEST)) != EP_ACK)
	{
		AfxMessageBox("명령 전송 실패!");//
	}
}

void CSettingCheckDlg::OnBnClickedBtnSaveName()
{
	CString strKey;;
	CString strName;
	for(LONG i =1; i <= MAX_HW_FAULT_CNT; i++)
	{
		strKey.Format("HwFaultName_%d",i);
		strName = m_gridHwFaultSet.GetValueRowCol(i,2);
		AfxGetApp()->WriteProfileString(FORM_SETTING_CHECK_SECTION, strKey, strName);
	}

	for(LONG i =1; i <= MAX_INTERNAL_FALG_CNT; i++)
	{
		strKey.Format("InternalFlagName_%d",i);
		strName = m_gridInternalFlag.GetValueRowCol(i,2);
		AfxGetApp()->WriteProfileString(FORM_SETTING_CHECK_SECTION, strKey, strName);
	}


	for(LONG i =1; i <= MAX_TEMP_CNT; i++)
	{
		strKey.Format("Temp%d",i);
		strName = m_gridTemp.GetValueRowCol(i,2);
		AfxGetApp()->WriteProfileString(FORM_SETTING_CHECK_SECTION, strKey, strName);
	}

	initName();
	UpdateAllData();
}
void CSettingCheckDlg::ClipboardChangeJustText()
{

	TCHAR *pszBuffer =NULL;
	CString strData;

	if(::OpenClipboard(this->GetSafeHwnd()) == TRUE)
	{	
		HANDLE hd ;

//		if(::IsClipboardFormatAvailable(CF_TEXT))
		{
			// 클립보드의 내용을 가져온다
			hd = ::GetClipboardData(CF_TEXT);
			LPCTSTR data = (LPCTSTR)::GlobalLock(hd);
			if(hd != NULL)
			{			
				int nLen = strlen(data) +1;
				// 메모리 재할당
				HGLOBAL hNewData = GlobalAlloc(GMEM_MOVEABLE, nLen );
				LPTSTR lptstrNewData = (LPTSTR)GlobalLock(hNewData);
				CopyMemory(lptstrNewData, data, nLen);
				GlobalUnlock(hNewData);

				::EmptyClipboard();
				SetClipboardData(CF_TEXT, lptstrNewData);
			}
			GlobalUnlock(hd);
		}

	
		::CloseClipboard();		
	}
}

void CSettingCheckDlg::OnEditCopy() 
{
	ROWCOL		nRow, nCol;
	// TODO: Add your command handler code here
	CWnd *pWnd= GetFocus();
	if(&m_gridHwFaultSet == (CMyGridWnd *)pWnd)
	{
		m_gridHwFaultSet.Copy();
	}

	if(&m_gridInternalFlag == (CMyGridWnd *)pWnd)
	{
		m_gridInternalFlag.Copy();
	}

	if(&m_gridTemp == (CMyGridWnd *)pWnd)
	{
		m_gridTemp.Copy();
	}
}
void CSettingCheckDlg::OnEditPaste() 
{
	ROWCOL		nRow, nCol;
	// TODO: Add your command handler code here
	CWnd *pWnd= GetFocus();
	if(&m_gridHwFaultSet == (CMyGridWnd *)pWnd)
	{
		ClipboardChangeJustText();
		m_gridHwFaultSet.Paste();
	}

	if(&m_gridInternalFlag == (CMyGridWnd *)pWnd)
	{
		ClipboardChangeJustText();
		m_gridInternalFlag.Paste();
	}

	if(&m_gridTemp == (CMyGridWnd *)pWnd)
	{
		ClipboardChangeJustText();
		m_gridTemp.Paste();
	}
}




void CSettingCheckDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	if(::IsWindow(this->GetSafeHwnd()))
	{

		RECT rect;
		::GetClientRect(m_hWnd, &rect);

		int width = rect.right/100;

		if( GetDlgItem(ID_DLG_LABEL_NAME)->GetSafeHwnd() )
		{
			CRect rectGrid;
			CRect rectStageLabel;
			GetDlgItem(ID_DLG_LABEL_NAME)->GetWindowRect(rectGrid);
			ScreenToClient(&rectGrid);
			GetDlgItem(IDC_STAGE_NAME)->GetWindowRect(rectStageLabel);
			ScreenToClient(&rectStageLabel);
			GetDlgItem(ID_DLG_LABEL_NAME)->MoveWindow(rectGrid.left, rectGrid.top, rect.right-rectStageLabel.Width(), rectGrid.Height(), FALSE);
		}
	}
}

void CSettingCheckDlg::OnCbnSelchangeComboStageChange()
{

	initGridNum();
	initValue();
	int nIndex = m_comboStageID.GetCurSel();
	if(nIndex >= 0)
	{
		m_nCurModuleID = nIndex+1;
		m_CmdTarget.SetText(GetTargetModuleName());

		EPSendCommand( m_nCurModuleID, 0, 0, EP_CMD_SBC_PARAM_REQUEST);
	}
	UpdateAllData();
}

void CSettingCheckDlg::initGridNum()
{
	for(LONG i =0; i < MAX_HW_FAULT_CNT; i++)
	{
		m_gridHwFaultSet.SetStyleRange(CGXRange(i+1,1), CGXStyle().SetInterior(RGB(240, 240, 240)));
		m_gridHwFaultSet.SetStyleRange(CGXRange(i+1,1), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T("")));
	}

	for(LONG i =0; i < MAX_INTERNAL_FALG_CNT; i++)
	{
		m_gridInternalFlag.SetStyleRange(CGXRange(i+1,1), CGXStyle().SetInterior(RGB(240, 240, 240)));
		m_gridInternalFlag.SetStyleRange(CGXRange(i+1,1), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T("")));
	}


	for(LONG i =0; i < MAX_TEMP_CNT; i++)
	{
		m_gridTemp.SetStyleRange(CGXRange(i+1,1), CGXStyle().SetInterior(RGB(240, 240, 240)));
		m_gridTemp.SetStyleRange(CGXRange(i+1,1), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T("")));

	}


}
void CSettingCheckDlg::OnBnClickedBtnEqualsCheck()
{
	initGridNum();
	WORD state = EPGetGroupState(m_nCurModuleID);

	if( state == EP_STATE_LINE_OFF )
	{
		return;
	}



	LONG lcurFaultSettingValue[200][40];
	unsigned short	lcurSafetyFlag[200][40];	
	unsigned short	lcurTemp[200][40];

	//전라인 정보 Get
	for(int moduleIdx=0; moduleIdx<m_nMaxStageCnt; moduleIdx++)
	{
		state = EPGetGroupState(m_nCurModuleID);
		if( state != EP_STATE_LINE_OFF )
		{
			EP_MD_SBC_PARAM *lpSbcParam;
			lpSbcParam = EPGetModuleSbcParam(EPGetModuleIndex(m_nCurModuleID));
			memcpy(lcurFaultSettingValue[moduleIdx], lpSbcParam->lFaultSettingValue, sizeof(LONG)* MAX_HW_FAULT_CNT);
			memcpy(lcurFaultSettingValue[moduleIdx], lpSbcParam->lSafetyFlag, sizeof(short)* MAX_INTERNAL_FALG_CNT);
			memcpy(lcurFaultSettingValue[moduleIdx], lpSbcParam->lTemp, sizeof(short)* MAX_TEMP_CNT);
		}
	}

	//한개씩 비교

		
	for(LONG i =0; i < MAX_HW_FAULT_CNT; i++)
	{
		bool bCheck = false;
		CString strTemp = "";

		if(m_bHwFaultEmpty[i] == FALSE)
		{
			for(int moduleIdx=0; moduleIdx<m_nMaxStageCnt; moduleIdx++)
			{
				state = EPGetGroupState(moduleIdx+1);
				if( state != EP_STATE_LINE_OFF )
				{
					if(lcurFaultSettingValue[m_nCurModuleID-1][i] != lcurFaultSettingValue[moduleIdx][i])
					{
						if(bCheck == false)
						{
							bCheck = true;
							strTemp = ::GetModuleName(moduleIdx+1);
						}
						else
						{
							strTemp += ","+ ::GetModuleName(moduleIdx+1);
						}
					}
				}
			}
			if(bCheck == true)
			{
				m_gridHwFaultSet.SetStyleRange(CGXRange(i+1,1), CGXStyle().SetInterior(RGB(237, 28, 36)));
				m_gridHwFaultSet.SetStyleRange(CGXRange(i+1,1), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(strTemp)));
			}
		}
	}

	for(LONG i =1; i <= MAX_INTERNAL_FALG_CNT; i++)
	{
		bool bCheck = false;
		CString strTemp = "";

		if(m_bInternalFlagEmpty[i] == FALSE)
		{
			for(int moduleIdx=0; moduleIdx<m_nMaxStageCnt; moduleIdx++)
			{
				if(lcurSafetyFlag[m_nCurModuleID-1][i] != lcurSafetyFlag[moduleIdx][i])
				{
					if(bCheck == false)
					{
						bCheck = true;
						strTemp = ::GetModuleName(moduleIdx+1);
					}
					else
					{
						strTemp += ","+ ::GetModuleName(moduleIdx+1);
					}
				}
			}
			if(bCheck == true)
			{
				m_gridInternalFlag.SetStyleRange(CGXRange(i+1,1), CGXStyle().SetInterior(RGB(237, 28, 36)));
				m_gridInternalFlag.SetStyleRange(CGXRange(i+1,1), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(strTemp)));
			}
		}
	}


	for(LONG i =1; i <= MAX_TEMP_CNT; i++)
	{
		bool bCheck = false;
		CString strTemp = "";

		if(m_bTempEmpty[i] == FALSE)
		{
			for(int moduleIdx=0; moduleIdx<m_nMaxStageCnt; moduleIdx++)
			{
				if(lcurTemp[m_nCurModuleID-1][i] != lcurTemp[moduleIdx][i])
				{
					if(bCheck == false)
					{
						bCheck = true;
						strTemp = ::GetModuleName(moduleIdx+1);
					}
					else
					{
						strTemp += ","+ ::GetModuleName(moduleIdx+1);
					}
				}
			}
			if(bCheck == true)
			{
				m_gridTemp.SetStyleRange(CGXRange(i+1,1), CGXStyle().SetInterior(RGB(237, 28, 36)));
				m_gridTemp.SetStyleRange(CGXRange(i+1,1), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(strTemp)));
			}
		}
	}

}

void CSettingCheckDlg::OnBnClickedBtnSaveExcel()
{
	WORD state = EPGetGroupState(m_nCurModuleID);
	if( state == EP_STATE_LINE_OFF )
	{
		initValue();
		AfxMessageBox("Save fail !!");
		return;
	}

	EP_MD_SBC_PARAM *lpSbcParam;
	lpSbcParam = EPGetModuleSbcParam(EPGetModuleIndex(m_nCurModuleID));

	if(lpSbcParam == NULL)
	{
		AfxMessageBox("Save fail !!");
		return;
	}

	CStdioFile csvFile;			
	CFileException ex;
	CString strTemp;
	CString strName;
	CString strKey;
	CString strDefault;

	CFileDialog fileDlg(TRUE,NULL,NULL, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT,"csv File(*.csv) | *.csv; *.csv");

	if(fileDlg.DoModal() == IDOK)
	{ 
		strTemp=fileDlg.GetPathName();  
		CString strExt=fileDlg.GetFileExt();
		
		if(strExt.IsEmpty())
		{
			strTemp += ".csv";
		}
	}
	else
	{
		return;
	}



	if(csvFile.Open( (LPSTR)(LPCTSTR)strTemp, CFile::modeWrite | CFile::modeNoTruncate | CFile::modeCreate, &ex ))
	{
		COleDateTime oleTime = COleDateTime::GetCurrentTime();
		csvFile.WriteString(oleTime.Format("%Y/%m/%d %H:%M:%S"));
		csvFile.WriteString("\n");
		csvFile.WriteString("\n");
		csvFile.WriteString("Hw Fault Setting Value\n");
		for(LONG i =1; i <= MAX_HW_FAULT_CNT; i++)
		{
			strDefault.Format("Value %d", i);
			strKey.Format("HwFaultName_%d",i);
			strName = AfxGetApp()->GetProfileString(FORM_SETTING_CHECK_SECTION, strKey, strDefault);
			strTemp.Format("%s,%d\n",strName,lpSbcParam->lFaultSettingValue[i-1]);
			csvFile.WriteString(strTemp);
		}


		csvFile.WriteString("\n");
		csvFile.WriteString("\n");
		csvFile.WriteString("Internal Flag\n");
		for(LONG i =1; i <= MAX_INTERNAL_FALG_CNT; i++)
		{
			strDefault.Format("Flag %d", i);
			strKey.Format("InternalFlagName_%d",i);
			strName = AfxGetApp()->GetProfileString(FORM_SETTING_CHECK_SECTION, strKey, strDefault);
			strTemp.Format("%s,%d\n",strName,lpSbcParam->lSafetyFlag[i-1]);
			csvFile.WriteString(strTemp);
		}


		csvFile.WriteString("\n");
		csvFile.WriteString("\n");
		csvFile.WriteString("Temperature\n");
		for(LONG i =1; i <= MAX_TEMP_CNT; i++)
		{
			strDefault.Format("Temp %d", i);
			strKey.Format("Temp_%d",i);
			strName = AfxGetApp()->GetProfileString(FORM_SETTING_CHECK_SECTION, strKey, strDefault);
			strTemp.Format("%s,%d\n",strName,lpSbcParam->lTemp[i-1]);
			csvFile.WriteString(strTemp);
		}

		csvFile.Close();
		AfxMessageBox("Save succeed");
	}
	else
	{
		AfxMessageBox("Save fail !!");
		return ;
	}
}
