// TestTypeRecordSet.cpp : implementation file
//

#include "stdafx.h"
#include "CTSEditor.h"
#include "TestTypeRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTestTypeRecordSet

IMPLEMENT_DYNAMIC(CTestTypeRecordSet, CDaoRecordset)

CTestTypeRecordSet::CTestTypeRecordSet(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CTestTypeRecordSet)
	m_TestType = 0;
	m_TestTypeName = _T("");
	m_nFields = 2;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString CTestTypeRecordSet::GetDefaultDBName()
{
	return _T(GetDataBaseName());
}

CString CTestTypeRecordSet::GetDefaultSQL()
{
	return _T("[TestType]");
}

void CTestTypeRecordSet::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CTestTypeRecordSet)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Long(pFX, _T("[TestType]"), m_TestType);
	DFX_Text(pFX, _T("[TestTypeName]"), m_TestTypeName);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CTestTypeRecordSet diagnostics

#ifdef _DEBUG
void CTestTypeRecordSet::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CTestTypeRecordSet::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
