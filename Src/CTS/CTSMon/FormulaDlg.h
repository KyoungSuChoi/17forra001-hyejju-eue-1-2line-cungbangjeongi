#pragma once
#include "afxwin.h"
#include "afxcmn.h"

#include "CTSMonDoc.h"

// CFormulaDlg 대화 상자입니다.

class CFormulaDlg : public CDialog
{
	DECLARE_DYNAMIC(CFormulaDlg)

public:
	CFormulaDlg(CCTSMonDoc* pDoc, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CFormulaDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_FOMULA_DLG };

public:
	void InitFont();
	void InitLabel();
	void SaveSetting();
	bool LoadSetting();

	void fnUpdateStatus();
	void fnUpdateCapacityT();
	void fnUpdateDcirT();

	CFont	font;
	CCTSMonDoc* m_pDoc;

	bool m_bChkDCR_T;
	bool m_bChkCapacity_T;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CLabel m_Label1;
	CLabel m_Label2;
	afx_msg void OnBnClickedBtnCheckCapacityT();
	afx_msg void OnBnClickedBtnCheckDcirT();
	CString m_strCapacityFormula;
	CString m_strDCRFormula;
	afx_msg void OnBnClickedOk();
	CString m_strCapacity;
	CString m_strCapTemp;
	CString m_strDCR;
	CString m_strDCRTemp;
	CLabel m_Label3;
	CLabel m_Label4;
};
#pragma once