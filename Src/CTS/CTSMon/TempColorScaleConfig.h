#if !defined(AFX_TEMPCOLORSCALECONFIG_H__D8CF4307_8B8C_41F9_9938_A170CC582485__INCLUDED_)
#define AFX_TEMPCOLORSCALECONFIG_H__D8CF4307_8B8C_41F9_9938_A170CC582485__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TempColorScaleConfig.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTempColorScaleConfig dialog
#include "CTSMonDoc.h"

class CTempColorScaleConfig : public CDialog
{
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();
	virtual ~CTempColorScaleConfig();

// Construction
public:
	CTempColorScaleConfig(CCTSMonDoc *pDoc, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CTempColorScaleConfig)
	enum { IDD = IDD_TEMPCOLOR_SCALECONFIG_DLG };
	float	m_fJigMax;
	float	m_fJigMin;
	float	m_fUnitMax;
	float	m_fUnitMin;
	//}}AFX_DATA

	CCTSMonDoc *m_pDoc;
	STR_TEMPSCALE_CONFIG	m_TempScaleConfig;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTempColorScaleConfig)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CTempColorScaleConfig)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TEMPCOLORSCALECONFIG_H__D8CF4307_8B8C_41F9_9938_A170CC582485__INCLUDED_)
