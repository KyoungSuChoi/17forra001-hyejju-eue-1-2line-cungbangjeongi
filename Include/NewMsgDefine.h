//////////EPDLL_API_HEADER_FILE///////////////
//											//	
//	NewMsgDefine.h							//	
//	Elico Power Formation System API		//	
//	Byung Hum - Kim		2001.7.11			//
//											//			
//////////////////////////////////////////////

//Power Formation API 
#ifndef _NEW_FORM_MSG_DEFINE_H_
#define _NEW_FORM_MSG_DEFINE_H_


//Version Define
#define _EP_FORM_VERSION			0x4001		//dll Program Version
#define _EP_PROTOCOL_VERSION_V1		0x1001		//Message Protocol Version
#define _EP_PROTOCOL_VERSION		0x1002		//Message Protocol Version

//TCPIP Port
#define _EP_CONTROL_TCPIP_PORT1		4001
#define _EP_MSG_TIMEOUT				12000		// 5=>10 //Message Read Time Out ( 20110809 4s->2s )
												// 10=>12 //Message Read Time Out ( 20140729 )
#define _EP_MAX_PACKET_LENGTH		4096 * 10	// 50000

//System Max Define 
#define EP_MAX_STEP					100			//Max Step 	
#define EP_MAX_GRADE_STEP			20			//Max Grade Step
#define EP_MAX_GRADE_CODE_LENGTH	31			//Max Grade Code Length	
// #define EP_MAX_MODULE_NUM		145			//Max Installable Module Number		//Edited 64->128 2002.10.7 => 145 => 20080425
#define EP_MAX_MODULE_NUM			288			//Max Installable Module Number		//145 => 288 => 20090331

#ifdef _DCIR
	#define EP_MAX_BD_PER_MD			8			//30			//Max Board Per Module			//Edited 16->8 2005/12/26
	#define EP_MAX_CH_PER_BD			64			//16
#else
	#define EP_MAX_BD_PER_MD			30		
	#define EP_MAX_CH_PER_BD			16			
#endif

#define MAX_USE_TEMP				8 

#define EP_MAX_CH_PER_MD			256											//Edited 16->8 2005/12/26
#define EP_MAX_TRAY_TYPE			0
#define EP_MAX_TRAY_PER_MD			16
#define EP_MAX_PINLOG				30

#define EP_MAX_SENSOR_CH			64
#define EP_USE_SENSOR_CH			16			//Current ch num

#define EP_AUTO_REPORT_INTERVAL		2000		//defualt 2Sec
#define EP_HEART_BEAT_INTERVAL		2000
#define EP_MAX_WAIT_COUNT			2
#define EP_HEART_BEAT_TIMEOUT		(EP_HEART_BEAT_INTERVAL+2000)

#define EP_TRAY_NAME_LENGTH			31
#define EP_LOT_NAME_LENGTH			31
#define EP_TYPE_SEL_NAME_LENGTH		3
#define EP_IP_NAME_LENGTH			15
#define EP_TEST_NAME_LENGTH			63
#define EP_TEST_DESCRIPTION_LENGTH	127
#define EP_CREATOR_NAME_LENGTH		63
#define EP_DATE_TIME_LENGTH			31
#define EP_SHORT_DATE_TIME_LENGTH	15
#define EP_TEST_SERIAL_LENGTH		23
#define EP_TRAY_SERIAL_LENGTH		7
#define EP_RESULT_FILE_NAME_LENGTH	255
#define EP_BATTERY_PER_TRAY			128
//#define EP_TEST_LOG_FILE_LENGTH		EP_TEST_SERIAL_LENGTH
#define EP_MODEL_ID_LENGTH			11
#define	EP_PROCESS_ID_LENGTH		11
#define EP_CELLNO_LENGTH			7

#define EP_MAX_CH_CODE_LENGTH		63
#define EP_MAX_LONINID_LENGTH		15
#define EP_MAX_PASSWORD_LENGTH		15

#define EP_RESULT_ITEM_NO			8		//ljb 2009-01		8에서 7로 수정
#define EP_DELTA_POINT				3
#define EP_COMP_POINT				3
#define EP_MAX_PORT_NO				8

#define EP_TRAY_REC_NVRAM			0
#define EP_TRAY_REC_BARCODE			1	

#define TRAY_TYPE_8_BY_32			2
#define TRAY_TYPE_16_BY_16			1
#define EP_DEFAULT_TRAY_ROW_COUNT	16
#define EP_DEFAULT_TRAY_COL_COUNT	16

#define EP_MAX_TEMP					16

#define EP_CHANNEL_MAP_FILE_NAME	"mapping.dat"
#define LOG_DATABASE_NAME			"Products.mdb"
#define DSN_DATABASE_NAME			"DSN=CTSProduct;"

#define EP_ON					1
#define EP_OFF					0

#define EP_RESULTFILE_TYPE_NORMAL	0		// 정상
#define EP_RESULTFILE_TYPE_EMG		1		// EMG 발생
#define EP_RESULTFILE_TYPE_RES		2		// 정전발생 데이터
#define EP_RESULTFILE_TYPE_STP		4		// 사용자 STOP
#define EP_RESULTFILE_TYPE_CONTACT	5		// ContactError 발생

//response of Command
#define EP_NACK					0x00000000
#define EP_ACK					0x00000001
#define EP_TIMEOUT				0x00000002

#define EP_SIZE_MISMATCH		0x00000003
#define EP_RX_BUFF_OVER_FLOW	0x00000004
#define EP_TX_BUFF_OVER_FLOW	0x00000005
#define EP_CONNECTED			0x00000006	
#define EP_FAIL					0xFFFFFFFF

//category of Command 
#define EP_SYS_CMD					0x00000010		//Special Command : 0x00000000 ~ 0x0000000F 
#define EP_RESPONSE_CMD				0x00010000		//Response Command  
#define EP_DATA_SERVER_CMD			0x00004000		//Formatin System Command
#define EP_MODULE_CMD				0x00003000		//Module Command : 0x00002000 ~ 0x00002FFF
#define EP_GROUP_CMD				0x00002000		//Board	Command	 : 0x00001000 ~ 0x00001FFF	
#define EP_CHANNEL_CMD				0x00001000		//Channel Command: 0x00000000 ~ 0x00000FFF

//System Command
#define EP_CMD_RESPONSE				0x00000001		//Command response 
#define EP_CMD_SHUTDOWN				0x00000002		//Module shutdown Command
#define EP_CMD_QUIT					0x00000003
#define EP_CMD_AUTO_GP_DATA			0x00000004		//(PC <- SBC)	Auto Group Data(Does not need Send Ack)
#define EP_CMD_AUTO_GP_STEP_END_DATA	0x00000005	//(PC <- SBC)	Auto Group Step End Data(Does not need Send Ack)
#define EP_CMD_AUTO_GP_STATE_DATA	0x00000006		//(PC <- SBC)	Auto Group State Data(Does not need Send Ack)
#define EP_CMD_AUTO_SENSOR_DATA		0x00000007		//(PC <- SBC)	Auto Temp. Gas Data(Does not need Send Ack)
#define EP_CMD_AUTO_EMG_DATA		0x00000008		//(PC <- SBC)	Auto Emergency Data (Does not need Send Ack)
#define EP_CMD_SYSTEM_INIT			0x00000009		//(PC <- SBC)   Module Init
//2003/1/14
#define EP_CMD_HEARTBEAT			0x0000000A		//(PC <- SBC)	Network State Check Send Ack  2003/1/14

//Channel Command	
#define EP_CMD_CHECK				0x00000010		//(PC -> SBC)	//Receive Ack
#define EP_CMD_RUN					0x00000011		//(PC -> SBC)	//Receive Ack
#define EP_CMD_PAUSE				0x00000012		//(PC -> SBC)	//Receive Ack
#define EP_CMD_CONTINUE				0x00000013		//(PC -> SBC)	//Receive Ack
#define EP_CMD_STOP					0x00000014		//(PC -> SBC)	//Receive Ack
#define EP_CMD_CLEAR				0x00000015		//(PC -> SBC)	//Receive Ack		// Reset
#define EP_CMD_NEXTSTEP				0x00000016		//(PC -> SBC)	//Receive Ack
#define EP_CMD_RESTART				0x00000017		//(PC -> SBC)	//Receive Ack		// 1. 작업을 처음부터 재시작
#define EP_CMD_FAULT_DATA			0x00000018


//2008/11/10		ljb -> 모듈 형 변환 추가
#define EP_CMD_CHANGE_MODULE0		0x00000020		//(PC -> SBC)	//Receive Ack
#define EP_CMD_CHANGE_MODULE1		0x00000021		//(PC -> SBC)	//Receive Ack

//2009/05/07	kky for Online모드일시 진행중인 StepList Data를 표시
// #define EP_CMD_ONLINE_STEPLIST		0x00000022		//(PC <- SBC)	//StepList on OnlineMode
#define EP_CMD_BUZZER_OFF			0x00000022		//(PC <- SBC)	//Buzzer off
#define EP_CMD_EMERGENCY_STOP		0x00000023		//(PC <- SBC)	//Emergency stop

#define EP_CMD_MODULE_CONNECT		0x00000024
#define EP_CMD_MODULE_DISCONNECT	0x00000025

//Group Command
#define EP_CMD_CAL_EXEC				0x00001000		//(PC -> SBC)	//5V Reference AD Value Update
//#define EP_CMD_CAL_REQUEST			0x00001001		//(PC -> SBC)
//#define EP_CMD_CAL_DATA				0x00011001		//(PC <- SBC)
//#define EP_CMD_TESTINFO_REQUEST		0x00001005		//(PC -> SBC)	
//#define EP_CMD_TESTINFO_DATA		0x00011005		//(PC <- SBC)
#define EP_CMD_TAG_INFO				0x00001006		//(PC <-SBC)	Does not need Send Ack
//#define EP_CMD_NVRAM_CHSTATE_REQUEST 0x00001007		//(PC -> SBC)
//#define EP_CMD_NVRAM_CHSTATE_DATA	0x00011007		//(PC <- SBC)
#define EP_CMD_AUTO_CHECK_RESULT	0x00001008		//(PC <- SBC)	Check Result Data
#define EP_CMD_SET_TRAY_READY		0x00001009		//(PC -> SBC)	Receive Ack
//#define EP_CMD_NVRAM_HEADER			0x0000100A		//(PC -> SBC)	Receive Ack
//#define EP_CMD_SET_TRAY_SERIAL		0x0000100B		//(pc -> sbc)	Receive Ack
//#define EP_CMD_INIT_NVRAM			0x0000100C		//(PC -> SBC)	Receive Ack
#define EP_CMD_SENSOR_LIMIT_SET		0x0000100D		//(PC -> SBC)	
#define EP_CMD_SENSOR_LIMIT_RES		0x0001100D		//(SBC -> PC)	response Sensor setting	//20200831 KSJ

#define EP_CMD_TRAY_DATA			0x0000100E		//(PC -> SBC)	Receive Ack
//#define EP_CMD_TRAY_SERIAL_REQUEST	0x0000100F		//(PC -> SBC)	response EP_CMD_TRAY_SERIAL_DATA
//#define EP_CMD_TRAY_SERIAL_DATA		0x0001100F		//				response of EP_CMD_TRAY_SERIAL_REQUEST
//#define EP_CMD_TEST_END				0x00001010		//(PC->SBC)		Send to Module Test End
#define EP_CMD_USER_CMD				0x00001011		//(PC <-> SBC)	Maintenance Command or Module Button Command		Receive Ack 

//2003/1/14 version 0x3001
#define EP_CMD_SET_AUTO_REPORT		0x00001012		//(PC -> SBC)	AutoReport Setting //2003/1/14 Receive Ack
#define EP_CMD_DIGITAL_IN_REQUEST	0x00001013		//(PC -> SBC)
#define EP_CMD_DIGITAL_IN_DATA		0x00011013		//(PC <- SBC)	Response of EP_CMD_DIGITAL_IN_REQUEST
#define EP_CMD_SET_DIGITAL_OUT		0x00001014		//(PC -> SBC)

//2003/7/5  version 0x3002
#define EP_CMD_GET_REF_IC_DATA		0x00001015		//(PC -> SBC)   Ref IC AD Data Request
#define EP_CMD_REF_IC_DATA			0x00011015		//(PC <- SBC)	response of EP_CMD_GET_REF_IC_DATA

//2004/3/20 version 0x3003
#define EP_CMD_GET_HOST_TIME		0x00001016		//(HOST <- SBC)	Sync Module time to host Time
#define EP_CMD_TIME_DATA			0x00011016		//(HOST -> SBC) Send Time Data to Module

//2004/7/1 version 0x3004
#define EP_CMD_CONFIG_REQUEST		0x00001017		//(Host -> SBC) Configuration Data Request
#define EP_CMD_CONFIG_DATA			0x00011017		//(Host <- SBC) Response of EP_CMD_CONFIG_REQUEST
#define EP_CMD_SET_CONFIG			0x00001018		//(Host -> SBC) Setting Configuration Data

//2006/11/09 
#define EP_CMD_REAL_TIME_DATA		0x00001019		//(SBC -> PC) Send real time data (Send ACK)
//2010/04/22
#define EP_CMD_JIGTEMP_TARGET_SET	0x00001020		//(PC -> SBC)	JigTargetTemp Setting AutoReport

//Module Command
//#define EP_CMD_BOARDSET			0x00002001		
//#define EP_CMD_BOARDDATA			0x00002002
#define EP_CMD_STEPDATA				0x00002003		//(PC -> SBC)	send Test Step Data
#define EP_CMD_GRADEDATA			0x00002004		//(PC -> SBC)	send Grade Data
#define EP_CMD_TEST_HEADERDATA		0x00002005		//(PC -> SBC)	send Test Header Data
#define EP_CMD_CHECK_PARAM			0x00002006		//(PC -> SBC)	Send Check Parameter
//#define EP_CMD_FAILCH				0x00002007
//#define EP_CMD_PROCDATA				0x00002008
//#define EP_CMD_SENDHEADER			0x00002009
//#define EP_CMD_SENDCONTENT			0x0000200A
//#define EP_CMD_CLEARALL				0x0000200B
//#define EP_CMD_CHANGEPROC			0x0000200C
//#define EP_CMD_CHANGEHEADER			0x0000200D
//#define EP_CMD_CLEARCONTENT			0x0000200E
//#define EP_CMD_SENDCURRENT			0x0000200F
#define EP_CMD_VER_REQUEST			0x00002010		//(PC -> SBC) Version Data request	//Ack EP_AMD_VER_DATA
#define EP_CMD_VER_DATA				0x00012010		//(PC <- SBC) Version Data Response
//#define EP_CMD_SYS_PARAM_REQUEST	0x00002012		//(PC -> SBC)
//#define EP_CMD_SYS_PARAM_DATA		0x00012012		//(PC <- SBC) System Parameter Request or Send
#define EP_CMD_MD_SET_DATA			0x00002013		//(PC -> SBC) Module Setting Data		//Receive Ack Form SBC
//#define EP_CMD_TEST_STEP_DATA		0x00002014		//(PC ->SBC)	1 Step Data 
//2008/11/10	ljb
#define EP_CMD_MD_CHANGE_DATA		0x00002018		//(PC -> SBC) Module I/O Change		//Receive Ack Form SBC

//2003/1/14
#define EP_CMD_SET_LINE_MODE		0x00002015		//(PC -> SBC)
#define EP_CMD_LINE_MODE_REQUEST	0x00002016		//(PC -> SBC)
#define EP_CMD_LINE_MODE_DATA		0x00012016		//(PC <- SBC)	Response of EP_CMD_LINE_MODE_REQUEST

//20090908
#define EP_CMD_MAPPING_INFO			0x00002017		//(PC->SBC)		hardware mapping information request
#define EP_CMD_MAPPING_DATA			0x00012017		//(SBC->PC)		response of EP_CMD_MAPPING_INFO


//2009/08/22 [Calibration Command]==============================================================================
#define EP_CMD_SET_CAL_POINT		0x00003001		//(PC -> SBC) Calibration Point set	// Receive Ack

//2012/06/05 [Gripper Clamping 누적 횟수 Command]==============================================================================
#define EP_CMD_SET_GRIPPER_CLAMPING_INIT	0x00005001		//(PC -> SBC) Gripper Clamping initialize _ Receive Ack
#define EP_CMD_GRIPPER_CLAMPING_REQUEST		0x00005002		//(PC -> SBC) Request Gripper Clamping value
#define EP_CMD_GRIPPER_CLAMPING_RES			0x00015002		//(PC -> SBC) Request Gripper Clamping value
//[Gripper Clamping End]==============================================================================================

//[sync - Receive Data]=========================================================================================
#define EP_CMD_GET_MAIN_CAL_POINT	0x00003002		//(PC -> SBC) Previous Cal Point Request	//Main DA
#define EP_CMD_GET_CH_CAL_POINT		0x0000300D		//(PC -> SBC) Previous Cal Point Request	//CH cal
#define EP_CMD_CAL_POINT_DATA		0x00013002		//(SBC -> PC) Reponse of EP_CMD_GET_CAL_POINT
//==============================================================================================================

#define EP_CMD_CAL_STOP             0X00003003		//(PC -> SBC) Calibration Stop Command	// Receive Ack
#define EP_CMD_CAL_PAUSE            0x00003004      //(PC -> SBC) Calibration Pause Command	// Receive Ack
#define EP_CMD_CAL_RESUME           0x00003005		//(PC -> SBC) Calibration Resume Command	//Receive Ack

#define EP_CMD_CAL_START			0x00003006		//(PC -> SBC) Calibration Start	//Receive Ack 
#define EP_CMD_CHK_START			0x00003007		//(PC -> SBC) Check Start	//Receive Ack 

#define EP_CMD_CAL_RES				0X00003008		//(SBC -> PC) Calibration Result Data //non sync
#define EP_CMD_CHK_RES				0x00003009		//(SBC -> PC) Check Result Data	//non sync

#define EP_CMD_CAL_END				0x0000300A		//(SBC -> PC) Calibration END Data //non sync
#define EP_CMD_CHK_END				0x0000300B		//(SBC -> PC) Check END Data	//non sync
#define EP_CMD_CAL_UPDATE			0x0000300C		//(PC -> SBC) Calibration Info SBC Update	// Receive Ack

//Measure
#define EP_CMD_RM_START				0x00003101		//(PC -> SBC) User Real Measure Request // Receive Ack
#define EP_CMD_RM_RES				0x00003102		//(SBC -> PC) Real Measure Result //non sync
#define EP_CMD_RM_STOP				0x00003103		//(PC -> SBC) User Real Measure STOP // Receive Ack				//ljb 2011519 
#define EP_CMD_RM_STOP_RESPONS		0x00013103		//(SBC -> PC) work Stop or End Message //non sync				//ljb 2011519 
#define EP_CMD_GET_RM_POINT_DATA	0x00003104		//(PC -> SBC) Real Measure Shunt Point //non sync
#define EP_CMD_RM_POINT_DATA		0x00013104		//(SBC -> PC) Reponse of EP_CMD_GET_RM_POINT_DATA
//[Cal Define End]==============================================================================================

//2012/04/06 [IOTest Command]==============================================================================
#define EP_CMD_IO_SET_FUNC			0x00004001		//(PC -> SBC) IOTest SetValue _ Receive Ack
//[IOTest Define End]==============================================================================================


//2020/10/29 KSJ [SBC Setting Command]==============================================================================
#define EP_CMD_SBC_PARAM_REQUEST	0x00005004		//(PC -> SBC) SBC Parameter Request			
#define EP_CMD_SBC_PARAM_RESPONS	0x00015004		//(SBC -> PC) SBC Parameter Respons		
//[SBC Setting Define End]==============================================================================================


//2020/12/24 KSJ [SBC Version Command]==============================================================================
#define EP_CMD_SAFETY_VERSION_REQUEST	0x00005006		//(PC -> SBC) SBC Parameter Request			
#define EP_CMD_SAFETY_VERSION_RESPONS	0x00015006		//(SBC -> PC) SBC Parameter Respons		
//[SBC Setting Define End]==============================================================================================
//2021/01/05 KSJ [SBC ERCD VALUE]==============================================================================
#define EP_CMD_WRITE_ERCD_VALUE_REQUEST	0x00005008		//(PC -> SBC) SBC Parameter Request			
#define EP_CMD_WRITE_ERCD_VALUE_RESPONS	0x00015008		//(SBC -> PC) SBC Parameter Respons		
//[SBC Setting Define End]==============================================================================================

//2020/12/31 KSCHOI [SBC Socket Reconnect Command]======================================================================
#define EP_CMD_SBC_SOCKET_RECONNECT_REQUEST	0x00005007		//(PC -> SBC) SBC Cali Data Request			
#define EP_CMD_SBC_SOCKET_RECONNECT_RESPONS	0x00015007		//(SBC -> PC) SBC Cali Data Respons		
//[SBC Setting Define End]==============================================================================================



//define State
#define EP_STATE_LINE_OFF		0x0100
#define EP_STATE_LINE_ON		0x0101
#define EP_STATE_EMERGENCY		0x0200
#define EP_STATE_ERROR			0xFFFF

#define EP_STATE_IDLE			0x0000		//
#define EP_STATE_SELF_TEST		0x0001
#define EP_STATE_STANDBY		0x0002
#define EP_STATE_RUN			0x0003
#define EP_STATE_PAUSE			0x0004
#define EP_STATE_FAIL			0X0005
#define EP_STATE_MAINTENANCE	0x0006
#define EP_STATE_OCV			0x0007
#define EP_STATE_CHARGE			0x0008
#define EP_STATE_DISCHARGE		0x0009
#define EP_STATE_REST			0x000A
#define EP_STATE_IMPEDANCE		0x000B
#define EP_STATE_CHECK			0x000C
#define EP_STATE_STOP			0x000D
#define EP_STATE_END			0x000E
#define EP_STATE_FAULT			0x000F
#define EP_STATE_COMMON			0x0010
#define EP_STATE_NONCELL		0x0011
#define EP_STATE_READY			0x0012
#define EP_STATE_INDEX_END		0x0013


static CHAR g_strSBCState[EP_STATE_INDEX_END][32] = 
{
	"EP_STATE_IDLE"
	, "EP_STATE_SELF_TEST"
	, "EP_STATE_STANDBY"
	, "EP_STATE_RUN"
	, "EP_STATE_PAUSE"
	, "EP_STATE_FAIL"
	, "EP_STATE_MAINTENANCE"
	, "EP_STATE_OCV"
	, "EP_STATE_CHARGE"
	, "EP_STATE_DISCHARGE"
	, "EP_STATE_REST"
	, "EP_STATE_IMPEDANCE"
	, "EP_STATE_CHECK"
	, "EP_STATE_STOP"
	, "EP_STATE_END"
	, "EP_STATE_FAULT"
	, "EP_STATE_COMMON"
	, "EP_STATE_NONCELL"
	, "EP_STATE_READY"
};

/*
//Channel Code define
#define EP_CODE_ERROR		0x00
#define EP_CODE_NORMAL		0x01
#define EP_CODE_REVERSED	0x02
#define EP_CODE_BAD_CONTECT	0x03
#define EP_CODE_DISCONNTECT	0x04
#define EP_CODE_SHORT		0x05
#define EP_CODE_VTG_EXCEED	0x06
#define EP_CODE_VTG_LOW		0x07
#define EP_CODE_CRT_EXCEED	0x08
#define EP_CODE_CRT_LOW		0x09
#define EP_CODE_CAP_EXCEED	0x0A
#define EP_CODE_CAP_LOW		0x0B
#define EP_CODE_DELTA_VTG	0x0C
#define EP_CODE_DELTA_CRT	0x0D
#define EP_CODE_TIME_EXCEED	0x0E
#define EP_CODE_TIME_UNDER	0x0F
#define EP_CODE_IMP_EXCEED	0x10
#define EP_CODE_IMP_LOW		0x11	
*/

//Jig State
#define EP_JIG_NONE				0x00
#define EP_JIG_UP				0x01
#define EP_JIG_MOVE_DOWN		0x02
#define EP_JIG_DOWN				0x03
#define EP_JIG_MOVE_UP			0x04
#define EP_JIG_ERROR			0x05
	
//door State
#define EP_DOOR_NONE			0x00
#define EP_DOOR_OPEN			0x01
#define EP_DOOR_MOVE			0x02
#define EP_DOOR_CLOSE			0x03
#define EP_DOOR_ERROR			0x04

//define Tray state 
#define EP_TRAY_NONE			0x00
#define EP_TRAY_UNLOAD			0x01
#define EP_TRAY_MOVE			0x02
#define EP_TRAY_LOAD			0x03
#define EP_TRAY_ERROR			0x04

//Step Type
#define EP_TYPE_NONE			0x00		//Added  By KBH 2002/4/26
#define EP_TYPE_CHARGE			0x01
#define EP_TYPE_DISCHARGE		0x02
#define EP_TYPE_REST			0x03
#define EP_TYPE_OCV				0x04
#define EP_TYPE_IMPEDANCE		0x05
#define EP_TYPE_END				0x06

#define EP_TYPE_AGING			0x07		//Added  By KBH 2002/4/26
#define EP_TYPE_GRADING			0x08		//Added  By KBH 2002/4/26
#define EP_TYPE_SELECTING		0x09		//Added  By KBH 2002/4/26
#define EP_TYPE_CHECK			0x0A		//Added  kky

#define EP_TYPE_LOOP			0x0C

#define EP_TYPE_INDEX_END		0x0D

static CHAR g_strStepType[EP_TYPE_INDEX_END][16] = 
{
	"IDLE"
	, "CHARGE"
	, "DISCHARGE"
	, "REST"
	, "OCV"
	, "DCIR"
	, "END"

	, "AGING"
	, "GRADING"
	, "SELECTING"

	, ""
	, ""

	, "LOOP"
};

//Grade Item
#define EP_GRADE_VOLTAGE		0x01
#define EP_GRADE_CAPACITY		0x02
#define EP_GRADE_IMPEDANCE		0x03
#define EP_GRADE_OCV_CHECK1		0x04
#define EP_GRADE_OCV_CHECK2		0x05
#define EP_GRADE_CURRENT		0x06
#define EP_GRADE_TIME			0x07

//Item =>>TestLog.mdb의 [DataType] Table
#define EP_STATE				0x00
#define EP_VOLTAGE				0x01
#define EP_CURRENT				0x02
#define EP_CAPACITY				0x03
#define EP_CH_CODE				0x04
#define EP_IMPEDANCE			0x05
#define EP_TOT_TIME				0x06
#define EP_STEP_TIME			0x07	
#define EP_GRADE_CODE			0x08
#define EP_STEP_NO				0x09
#define	EP_WATT					0x0A
#define EP_WATT_HOUR			0x0B
//#define EP_OCV				0x0C
#define EP_DELTA_OCV			0x0D
#define EP_TEMPER				0x0C
#define EP_ALL_DATA				0x0D
#define EP_CELL_SERIAL			0x0E
#define EP_DELTA_CAPACITY		0x0F
#define EP_NOT_USE_DATA			0xFFFFFFFF

//TempView ViewItem
#define EP_POWAVG				0x00
#define EP_POWMAX				0x01
#define EP_POWMIN				0x02
#define EP_POWPOINT				0x03
#define EP_JIGAVG				0x04
#define EP_JIGMAX				0x05
#define EP_JIGMIN				0x06
#define EP_JIGPOINT				0x07	


//Bit Flag 
#define EP_TIME_BIT_POS			0
#define EP_VOLTAGE_BIT_POS		1
#define EP_CURRENT_BIT_POS		2
#define EP_CAPACITY_BIT_POS		3
#define EP_WATT_BIT_POS			4
#define EP_WATT_HOUR_BIT_POS	5
#define EP_IMPEDANCE_BIT_POS	6
#define EP_RESERVED_BIT_POS		7

#define EP_LINE_MODE			1
#define EP_OPERATION_MODE		2
#define EP_WORKTYPE_MODE		3

#define EP_ONLINE_MODE	0
#define EP_OFFLINE_MODE	1

#define EP_OPERATION_LOCAL			0
#define EP_OPEARTION_MAINTENANCE	1
#define EP_OPERATION_AUTO			2

//ljb  2009-11-10	-> Tray 변경 인수 전달
#define EP_CHANGE_TRAY_MODE0	0
#define EP_CHANGE_TRAY_MODE1	1

#if EP_RESULT_ITEM_NO <= EP_RESERVED_BIT_POS
#pragma message( "------------KBH Message-------------" )
#pragma message( "Data Result Item Number Error" )
#pragma message( "------------------------------------" )
#error Item Define Error
#endif

#define EP_TIME_BIT				(0x01<<EP_TIME_BIT_POS)
#define EP_VOLTAGE_BIT			(0x01<<EP_VOLTAGE_BIT_POS)
#define EP_CURRENT_BIT			(0x01<<EP_CURRENT_BIT_POS)
#define EP_CAPACITY_BIT			(0x01<<EP_CAPACITY_BIT_POS)
#define EP_WATT_BIT				(0x01<<EP_WATT_BIT_POS)
#define EP_WATT_HOUR_BIT		(0x01<<EP_WATT_HOUR_BIT_POS)
#define EP_IMPEDANCE_BIT		(0x01<<EP_IMPEDANCE_BIT_POS)
#define EP_RESERVED_BIT			(0x01<<EP_RESERVED_BIT_POS)

//Charge, Discharge Mode
#define EP_MODE_CCCV		0x01
#define EP_MODE_CC			0x02
#define EP_MODE_CV			0x03
//Impedance Mode
#define EP_MODE_DC_IMP		0x04
#define EP_MODE_AC_IMP		0x05
#define EP_MODE_CP			0x06
#define EP_MODE_PULSE		0x07
#define EP_MODE_CR			0x08

#define EP_CHARGE_MODE		" CC-CV\n CC\n"
#define EP_IMPEDANCE_MODE	" DC Imp" // \n AC Imp\n"

//progress ==> Scheudle.mdb의 TestType에 사용됨 
#define EP_PGS_NOWORK		0x00
#define EP_PGS_PREIMPEDANCE	0x01		//PI
#define	EP_PGS_PRECHARGE	0x02		//PC
#define EP_PGS_FORMATION	0x03		//FO
#define EP_PGS_IROCV		0x04		//IR
#define EP_PGS_FINALCHARGE	0x05		//FC
#define EP_PGS_AGING		0x06		//AG
#define	EP_PGS_GRADING		0x07		//GD
#define EP_PGS_SELECTOR		0x08		//SL
#define EP_PGS_OCV			0x09		//OV

#define EP_PGS_PREIMPEDANCE_CODE	"PI"
#define	EP_PGS_PRECHARGE_CODE		"PC"
#define EP_PGS_FORMATION_CODE		"FO"
#define EP_PGS_IROCV_CODE			"IR"
#define EP_PGS_FINALCHARGE_CODE		"FC"
#define EP_PGS_AGING_CODE			"AG"
#define	EP_PGS_GRADING_CODE			"GD"
#define EP_PGS_SELECTOR_CODE		"SL"
#define EP_PGS_OCV_CODE				"OV"


//Emergency State(LG 4호 이전 Project) 
#define EP_EMG_NORMAL		0x00
#define EP_EMG_UPS_POWER	0x01
#define EP_EMG_UPS_BATTERY_FAIL	0x02
#define EP_EMG_MAIN_EMG_SWITCH	0x03
#define EP_EMG_SUB_EMG_SWITCH	0x04
#define EP_EMG_TEMP_LIMIT		0x05
#define EP_EMG_TEMP_DIFF_LIMIT	0x06
#define EP_EMG_SMOKE_LEAK		0x07
#define EP_EMG_NETWORK_ERROR	0x08
#define EP_EMG_POWER_SWITCH		0x09
#define EP_EMG_DVM_COMM_ERROR	0x0A
#define EP_EMG_CALIBRATOR_COMM_ERROR	0x0B
#define EP_EMG_CPU_WATCHDOG		0x0C
#define EP_EMG_MODULE_WATCHDOG	0x0D

#define EP_EMG_MAINCYL_UP_TIMEOUT		0x20
#define EP_EMG_GRIPCYL_OPEN_TIMEOUT		0x21
#define EP_EMG_GRIPCYL_CLOSE_TIMEOUT	0x22
#define EP_EMG_MAINCYL_DOWN_TIMEOUT		0x23
#define EP_EMG_GRIPCYL_RO_SENS			0x24
#define EP_EMG_GRIPCYL_RC_SENS			0x25
#define EP_EMG_GRIPCYL_LO_SENS			0x26
#define EP_EMG_GRIPCYL_LC_SENS			0x27
#define EP_EMG_MAINCYL_U_SENS			0x28
#define EP_EMG_MAINCYL_L_SENS			0x29
#define EP_EMG_AIR_PRESS				0x2A
#define EP_EMG_NVRAM_COMM_ERROR			0x2B
#define EP_EMG_DOOR_OPEN				0x2C

#define EP_EMG_MAX_FAULT				0x40	//Group Fail

#define EP_EMG_UPPER_TEMP				0x60	//Main Board Fail
#define EP_EMG_LOWER_TEMP				0x61
#define EP_EMG_ADC_READ_FAIL			0x62
#define EP_EMG_HW_FAULT					0x63

//New Module Fail Code Define 2003/5/26 KBH
#define EP_FAIL_NONE					0x00
#define EP_FAIL_TERMINAL_QUIT			0x01
#define EP_FAIL_TERMINAL_HALT			0x02
#define EP_FAIL_NET_CMD_EXIT			0x03
#define EP_FAIL_POWER_SWITCH			0x04
#define EP_FAIL_AC_POWER1				0x05
#define EP_FAIL_AC_POWER2				0x06
#define EP_FAIL_UPS_BATTERY				0x07
#define EP_FAIL_MAIN_EMG				0x08
#define EP_FAIL_SUB_EMG					0x09
#define EP_FAIL_CPU_WATCHDOG			0x0A
#define EP_FAIL_MAIN_BD_ADC				0x0B
#define EP_FAIL_MAIN_BD_HW				0x0C
#define EP_FAIL_MAIN_BD_TEMP_UPPER		0x0D
#define EP_FAIL_NETWORK_ERROR			0x0E
#define EP_FAIL_DVM_COMM_ERROR			0x0F
#define EP_FAIL_CAL_COMM_ERROR			0x10
#define EP_FAIL_OVP						0x11
#define EP_FAIL_OCP						0x12
#define EP_FAIL_UPPER_VOLTAGE			0x13
#define EP_FAIL_UPPER_CURRENT			0x14
#define EP_FAIL_RUN_TIME_OVER			0x15

#define EP_FAIL_MAX_FAULT				0x20

#define EP_FAIL_MAINCYL_UP_TIME_OVER	0x40
#define EP_FAIL_MAINCYL_DWON_TIME_OVER	0x41
#define EP_FAIL_MAINCYL_UP_SENS			0x42
#define EP_FAIL_MAINCYL_DOWN_SENS		0x43
#define EP_FAIL_MAINCYL_RU_SENS			0x44
#define EP_FAIL_MAINCYL_LU_SENS			0x45
#define EP_FAIL_MAINCYL_RD_SENS			0x46
#define EP_FAIL_MAINCYL_LD_SENS			0x47
#define EP_FAIL_LATCHCYL_OPEN_TIME_OVER	0x48
#define EP_FAIL_LATCHCYL_CLOSE_TIME_OVER	0x49
#define EP_FAIL_LATCHCYL_RC_SENS		0x4A
#define EP_FAIL_LATCHCYL_RO_SENS		0x4B
#define EP_FAIL_LATCHCYL_LC_SENS		0x4C
#define EP_FAIL_LATCHCYL_LO_SENS		0x4D
#define EP_FAIL_GRIPCYL_OPEN_TIME_OVER	0x4E
#define EP_FAIL_GRIPCYL_CLOSE_TIME_OVER	0x4F
#define EP_FAIL_GRIPCYL_RO_SENS			0x50
#define EP_FAIL_GRIPCYL_RC_SENS			0x51
#define EP_FAIL_GRIPCYL_LC_SENS			0x52
#define EP_FAIL_GRIPCYL_LO_SENS			0x53
#define EP_FAIL_TRAY_SENS				0x54
#define EP_FAIL_TRAY_DIR_SENS			0x55
#define EP_FAIL_DOOR_OPEN				0x56
#define EP_FAIL_SMOKE_LEAK				0x57
#define EP_FAIL_AIR_PRESS				0x58
#define EP_FAIL_TEMP_UPPER				0x59
#define EP_FAIL_DIFF_TEMP_UPPER			0x5A
#define EP_FAIL_NVRAM_COMM_ERROR		0x5B
#define EP_FAIL_CRAIN_ERROR				0x5C

#define SBC_MAX_BUF_LEN	4086

union	PACKETID{
	WORD nSysID[2];
	UINT nID;
};				//unsigned char	: Packet Serial Number

//define Message Header(Nt. Data)
typedef struct tag_MessageHeader
{
	UINT	nID;						//
	WORD	wGroupNum;					//unsigned short: Group Number -> 0: Module, 1~MAX_BOARD: Board
	WORD	wChannelNum;				//unsigned short: Channel Number -> 1 ~ Channel per Board
	UINT	nCommand;					//unsigned int	: Command
	UINT	nLength;					//unsigned int  : Length of Packet (From startByte to Body)
} EP_MSG_HEADER, *LPEP_MSG_HEADER;		//Total size : 16Byte

enum SBCPacketState
{
	SPS_NONE
	, SPS_SEND
	, SPS_SENDED
	, SPS_DEL
	, SPS_RESEND
	, SPS_RECV
	, SPS_ERROR

	, SPS_MAXERROR
};

static TCHAR g_szSBCPacketState[SPS_MAXERROR][64] = 
{
	TEXT("SPS_NONE")
	, TEXT("SPS_SEND")
	, TEXT("SPS_RECV_WAIT")
	, TEXT("SPS_RESEND")
	, TEXT("SPS_RECV")
	, TEXT("SPS_ERROR")
};

typedef struct tag_SBC_Message
{
	EP_MSG_HEADER stHeader;
	BYTE	byBody[SBC_MAX_BUF_LEN];
	UINT	recvCommand;
	SBCPacketState	isState;
}NEW_SBC_MESSAGE;

typedef struct _SBCQueue_DATA
{
	WPARAM	id;
	DWORD	command;
	BYTE	Data[_EP_MAX_PACKET_LENGTH];
	DWORD	DataLength;
}SBCQueue_DATA;

//20201029 KSJ
typedef struct tag_EP_CMD_SBC_PARAM {	
	LONG	lFaultSettingValue[40];				//SettingCheckView - FaultSetting
	unsigned short	lSafetyFlag[40];					//SettingCheckView - InternalFlag
	unsigned short	lTemp[40];
} EP_MD_SBC_PARAM, *LPEP_MD_SBC_PARAM;




//System Version Data (Nt. Data)
typedef struct tag_EP_CMD_VER_DATA {
	int		nModuleID;
	int		nVersion;							//2004/4/8 UINT	nVersion에서 변경 unsigned int :System Version 

	CHAR	cVersionMain[EP_MAX_BD_PER_MD][6+1]; //19-01-10 엄륭 SBC Version 추가 관련
	int		nVersionDiffFlag;					//0: Same, 1:Diff //19-01-10 엄륭 SBC Version 추가 관련
	
	int		nControllerType;					//SBC Type	0: HS-6637, 1: WEB-6580, 2:PCM-5864
	WORD	wInstalledBoard;					//unsigned short : Total Board Number in one Modlule
	WORD	wChannelPerBoard;					//unsinged short : Total Channel Per Board
	int		nModuleGroupNo;						//Group Number
	int		nTotalChNo;							
	WORD	wTrayType;							//Tray Type => 2008.10.31 LJB -> 0:사용자형, 1:가로방식,2:세로방식,3:각형 가로방식,4:각형 세로방식
	WORD	wTotalTrayNo;						//Total insertable tray Number
	WORD	awChInTray[14];						//Channel in tray
	WORD	wCaliMaxBd;
	WORD	wCaliMaxCh;
} EP_MD_SYSTEM_DATA, *LPEP_MD_SYSTEM_DATA;		//Total size : 48Byte => 60Byte


//System Version Data (Nt. Data)
typedef struct tag_EP_CMD_FW_VER_DATA {	//20201224 KSJ
	int		nLine;								// New Version 20201223 KSJ
	char	cMainPro[4];						// New Version 20201223 KSJ
	char	cSubPro[3];							// New Version 20201223 KSJ
	char	cMainRev[4];						// New Version 20201223 KSJ
	char	cSubRev[3];							// New Version 20201223 KSJ
} EP_MD_FW_VER_DATA, *LPEP_MD_FW_VER_DATA;		//


//System Version Data (Nt. Data)
typedef struct tag_EP_CMD_VER_DATA_V0 {
	int		nModuleID;
	WORD	nVersion;							//2004/4/8 UINT	nVersion에서 변경 unsigned int :System Version 
	WORD	nControllerType;					//SBC Type	0: HS-6637, 1: WEB-6580, 2:PCM-5864
	WORD	wInstalledBoard;					//unsigned short : Total Board Number in one Modlule
	WORD	wChannelPerBoard;					//unsinged short : Total Channel Per Board
	int		nModuleGroupNo;						//Group Number
	WORD	awChInGroup[8];						//
	WORD	wTrayType;							//Tray Type => LG 각형 256 : 2, LG 원통형 256: 1, SKC 128 : 3
	WORD	wTotalTrayNo;						//Total insertable tray Number
	WORD	awChInTray[8];						//Channel in tray
} EP_MD_SYSTEM_DATA_V0, *LPEP_MD_SYSTEM_DATA_V0;		//Total size : 48Byte	

//Module Setting Data(NT.Data)
typedef struct tag_EP_CMD_MD_SET_DATA {
	BYTE	bConnectionReTry;		// Module Connection retry
	BYTE	bAutoProcess;			// NVRAM Use
	BYTE	bUseTemp;				//Temperature Limit Value Use
	BYTE	bUSeJigTemp;				//Gas Limit Value Use
	unsigned short sWanningTemp;	//Temperature Limit Value
	unsigned short sCutTemp;		//Temperature Limit Value
	unsigned short sJigWanningTemp;	//Temperature Limit Value	kky
	unsigned short sJigCutTemp;		//Temperature Limit Value	kky
	UINT	nAutoReportInterval;	//Auto Report Data Time Interval
	BYTE	bTrayReadType;			//Tray Recognition Type		0: NVRAM	1: BarCode Reader
	BYTE	reserved1;
	WORD	nReserved2;
	LONG	vHighLimit;				//Limit of voltage high
	LONG	iHighLimit;				//Limit of current high
	LONG	reserved3[8];			//reserved
} EP_MD_SET_DATA1;					//Total size : 8Byte


unsigned short sWanningTemp;				// 경고값 상한	
unsigned short sReserved1;					
unsigned short sCutTemp;					// 위험값 상한
BOOL		bUseTempLimit;					//경고온도 사용 여부

unsigned short sJigWanningTemp;				// 경고값 상한	
unsigned short sReserved2;					// 경고값 하한
unsigned short sJigCutTemp;					// 위험값 상한
BOOL		bUseJigTempLimit;				//경고온도 사용 여부


/*
//Temperature and Gas Sensor Setting
typedef struct tag_TempGasSensor {
	BYTE	bUseTemp;				//Temperature Limit Value Use			
	BYTE	bUseGas;				//Gas Limit Value Use
	WORD	wReserved;
	long	nMaxTemp;				//Temperature Limit Value Use		
	long	nMaxGas;				//Gas Limit Value Use
	WORD	wWarningNo1;
	WORD	wWarningNo2;
} EP_SENSOR_SET_DATA;
*/

//New define 2006/02/06
/*
typedef struct tag_SensorData {
	unsigned char bUseSensorFlag[EP_MAX_SENSOR_CH];		//sensor 사용여부 
	unsigned char bUseMaxCheck[EP_MAX_SENSOR_CH];		// lMaxLimit 
	long lMaxLimit[EP_MAX_SENSOR_CH];					//최대 수치 
	long lReserved;
} _SENSOR_SETTING;
*/

typedef struct tag_SensorData {
	unsigned char bUseSensorFlag[EP_MAX_SENSOR_CH];		//sensor 사용여부 
	unsigned short int sWanningLimit;	
	unsigned short int sCutLimit;						//최대 수치 
	unsigned char	bUseLimit;							//Sensro limit 사용여부 
	unsigned char	reserved1;
	unsigned short	sReserved1;	
} _SENSOR_SETTING;

typedef struct tag_EP_CMD_SENSOR_LIMIT_SET {
	unsigned short int wWarningNo2;
	unsigned short int wWarningNo1;
	_SENSOR_SETTING	sensorSet1;		//temp sensor
	_SENSOR_SETTING	sensorSet2;		//gas sensor		//2006/02/06 not use
	unsigned char bUseError;		//TrayError 사용여부
	unsigned char cReserved;
	unsigned short int lReserved;
} EP_SENSOR_SET_DATA;

typedef struct tag_EP_CMD_JIGTEMP_TARGET_DATA {
	unsigned short int wCurJigAvgTemp;
	unsigned short int wIntervalTime;					//Jig부 온도 확인 주기( Min. )
	unsigned short int wTargetTemp;						//Jig부 목표 온도
	unsigned char	useJigTempSetDate;
	unsigned char	Reserved[1];
} EP_CMD_JIGTEMP_TARGET_DATA;

//Temperature and Gas Sensor Monitoring Data
typedef struct tag_TempGasData {
	long	lData;
	long	lReserved1;		//1Base Matching channel Number
} _SENSOR_SENSING_DATA;

typedef struct tag_EP_CMD_AUTO_SENSOR_DATA {
	_SENSOR_SENSING_DATA	sensorData1[EP_MAX_SENSOR_CH];		//temperature
	_SENSOR_SENSING_DATA	sensorData2[EP_MAX_SENSOR_CH];		//gas			//2006/02/06 not use
} EP_SENSOR_DATA;

//body of EP_CMD_RUN
typedef struct tag_EP_CMD_RUN {
	char	szTestSerialNo[32];			//test serial no
	char	szTrayID[32];
	BYTE	nTabDeepth;
	BYTE	nTrayHeight;
	BYTE	nTrayType;
	BYTE	nCurrentCellCnt;
	BYTE	nContactErrlimit;		// 지정된 횟수 이상의 Contact Error 가 발생하면 Error 통지
	BYTE	nCappErrlimit;			// 지정된 횟수 이상의 Contact Error 가 발생하면 Error 통지
	BYTE	nChErrlimit;			// 채널에러EMG 횟수 확인
	BYTE	nTempGetPosition;		// 온도 측정 위치 변경 Flag
	DWORD	dwUseFlag;
	int		nInputCell[16];				//inputed cell count
	BYTE	code[256];
} EP_RUN_CMD_INFO;
//////////////////////////////////////////////////////////////////////////

//Channel Monitoring Data	(NT. Data)

typedef struct tag_ChannelData
{
    WORD			wChIndex;		//0 Base
	WORD			state;
    BYTE			grade;
    BYTE			reserved[3];    
    WORD			channelCode;
	WORD			nStepNo;		//1 base
    DWORD			ulStepTime;
    DWORD			ulTotalTime;
    long			lVoltage;
    long			lCurrent;
    long			lWatt;
    long			lWattHour;
    long			lCapacity;
    long 			lImpedance;
    
    // 20130824 kky
    long			lCcCapacity;
    long			lCvCapacity;
    DWORD			ulCcRunTime;
    DWORD			ulCvRunTime;    
    
    long			lDCIR_V1;
    long			lDCIR_V2;
    long			lDCIR_AvgCurrent;
    long			lDCIR_AvgTemp;
    
    // long			lFiveMinCurrent;
    long			lTimeGetChargeVoltage;
    
	long			lComDCIR;// lTimeGetVoltage;
	long			lComCapacity;// lTimeGetCurrent;
	long			lDeltaOCV;   //KSJ 20210223
	long			lStartVoltage;  //KSJ 20210223
	long			lDeltaCapacity; //KSJ 20210223 - Reserved
	
	unsigned long   currentFaultLevel3Cnt;
	unsigned long   currentFaultLevel2Cnt;
	unsigned long   currentFaultLevel1Cnt;
	
    unsigned long   voltageFaultLevelV3Cnt;
    unsigned long   voltageFaultLevelV2Cnt;
    unsigned long   voltageFaultLevelV1Cnt;  
    
    unsigned long	ulTotalSamplingVoltageCnt;      			
    unsigned long	ulTotalSamplingCurrentCnt;      

	unsigned long	ulJigTemp[6];
	unsigned long   ulTempMax;
	unsigned long   ulTempMin;
	unsigned long   ulTempAvg;
} EP_CH_DATA, *LPEP_CH_DATA;		//Total 40 Byte

//Group Monitoring Data (Nt. Data)
typedef struct tag_EP_CMD_AUTO_GP_DATA {
	WORD	state;
	WORD	failCode; 
	WORD	sensorState[8];			// =>각 Jig내 Senosr bit 정보 ( 0 : Jig Up/Down, 1: Tray 유무, 2 : Stopper, 3 : Door Sensor, 4 : Operation mode, 5 : Operation mode enable 상태 )
									// 6->8 원래 8 2를 GUI 임의사용 cwm
									// 0 : Jig Up/Down, 1: Tray 유무, 2 : Stopper, 3 : Door Sensor, 4 : Line mode 상태, 5 : Line mode 변경 가능 유무(1:가능, 0:불가능) )
	// WORD	codeState[2];			// 0: I Hunting 수량
	unsigned char IOSensorState[16];	// Maintenance 의 수동조작에 필요한 데이터  	
	unsigned short ManualFunc;			
	unsigned short reserved;
} EP_GP_STATE_DATA, *LPEP_GP_STATE_DATA;	

//Send Test condtition Information (Nt Data)
typedef struct tag_EP_CMD_TEST_HEADERDATA {
	char	szData11[24];		//not use
	char	szData22[256];		//not use
	BYTE	totalStep;
	BYTE	totalGrade;			
	WORD	reserved;			//not use
} EP_TEST_HEADER, *LPEP_TEST_HEADER;

//Step Header (Nt Data)
typedef struct tag_StepHeader {			//Step Data Header Structure
	BYTE		stepIndex;
	BYTE		type;
	BYTE		mode;
	BYTE		gradeSize;				//Grade step size
	int			nProcType;				//Added By KBH	2006/2/6
} _STEP_HEADER, *LP_STEP_HEADER;	//Total 4 Byte

////////////////////////////////////////////////////////////////
//Charge and Discharge Step(Nt Data)//Grade Step Data (nt Data)
typedef struct tag_StepCharge {			//Charge/Discharge Step Data Structure
	_STEP_HEADER	stepHeader;
	long		lVref;
    long		lIref;
    
	ULONG		ulEndTime;
    long		lEndV;
    long		lEndI;
    long		lEndC;
    long		lEndDV;
    long		lEndDI;

	unsigned char bUseActucalCap;		//flag of soc rate cut off reference capactiy
	unsigned  char bReserved;			//step no of capacity reference step no
	unsigned  short int wSocRate;		//wSoc rate (%)*100

	long		lOverV;
    long		lLimitV;
    long		lOverI;
    long		lLimitI;
    long		lOverC;
    long		lLimitC;

	ULONG		lCompTimeV[EP_COMP_POINT];			
	LONG		lCompVLow[EP_COMP_POINT];
	LONG		lCompVHigh[EP_COMP_POINT];

	ULONG		lCompTimeI[EP_COMP_POINT];			
	LONG		lCompILow[EP_COMP_POINT];
	LONG		lCompIHigh[EP_COMP_POINT];

    ULONG		lDeltaTime;
    long		lDeltaV;
    ULONG		lDeltaTime1;
    long		lDeltaI;
	
	//2006/12/9 추가 
	long		lRecDeltaTime;
	long		lRecDeltaV;
	long		lRecDeltaI;
	long		lRecDeltaT;
	
	long		lParam1;
	long		lParam2;

	long		lCompChgCcVtg;
	ULONG		ulCompChgCcTime;
	long		lCompChgCcDeltaVtg;

	long		lCompChgCvCrt;
	ULONG		ulCompChgCvtTime;
	long		lCompChgCvDeltaCrt;
	
	BYTE		aDataID[6];			// not use 20060118
	BYTE		nFanOffFlag;
	BYTE		UseStepContinue;	// ljb	2009-01
	
	long		lRecVIGetTime;	// TimeGetValue 를 입력하도록 변경
	long		lProcessType;	//20201214 ksj
} EP_CHARGE_STEP;

typedef struct tag_StepDisCharge {			//Discharge Step Data Structure
	_STEP_HEADER	stepHeader;
	long		lVref;
	long		lIref;

	ULONG		ulEndTime;
	long		lEndV;
	long		lEndI;
	long		lEndC;
	long		lEndDV;
	long		lEndDI;

	unsigned char bUseActucalCap;		//flag of soc rate cut off reference capactiy
	unsigned  char bReserved;			//step no of capacity reference step no
	unsigned  short int wSocRate;		//wSoc rate (%)*100

	long		lOverV;
	long		lLimitV;
	long		lOverI;
	long		lLimitI;
	long		lOverC;
	long		lLimitC;

	ULONG		lCompTimeV[EP_COMP_POINT];			
	LONG		lCompVLow[EP_COMP_POINT];
	LONG		lCompVHigh[EP_COMP_POINT];

	ULONG		lCompTimeI[EP_COMP_POINT];			
	LONG		lCompILow[EP_COMP_POINT];
	LONG		lCompIHigh[EP_COMP_POINT];

	ULONG		lDeltaTime;
	long		lDeltaV;
	ULONG		lDeltaTime1;
	long		lDeltaI;

	//2006/12/9 추가 
	long		lRecDeltaTime;
	long		lRecDeltaV;
	long		lRecDeltaI;
	long		lRecDeltaT;

	long		lParam1;		//parameter 1  ex) EDLC : 용량측정 전압1(Low) , etc
	long		lParam2;		//parameter 1  ex) EDLC : 용량측정 전압2(High) , etc

	BYTE		aDataID[6];			// not use 20060118
	BYTE		nFanOffFlag;
	BYTE		UseStepContinue;	// ljb	2009-01

	long		lRecVIGetTime;	// TimeGetValue 를 입력하도록 변경
long		lProcessType;	//20201214 ksj
} EP_DISCHARGE_STEP;

//Ocv Step Data Structure (nt. Data)
typedef struct tag_StepOcv {			//OCV Step Data Structure
	_STEP_HEADER	stepHeader;
	long		lOverV;
    long		lLimitV;
	BYTE		aDataID[EP_RESULT_ITEM_NO];		//not use 20060118
} EP_OCV_STEP;

//rest Step Data (nt. data)
typedef struct tag_StepRest {			//Rest Step Data Structure
	_STEP_HEADER	stepHeader;
	unsigned long	ulEndTime;
	long			lOverV;
    long			lLimitV;
	LONG			lJigUp;

	//2006/12/9 추가 
	long		lRecDeltaTime;
	long		lRecDeltaV;
	long		lRecDeltaI;
	long		lRecDeltaT;

	BYTE		aDataID[EP_RESULT_ITEM_NO];		//not use 20060118
	long		lRecVIGetTime;	// TimeGetValue 를 입력하도록 변경
} EP_REST_STEP;

//Impedance step Data (nt Data)
typedef struct tag_StepImpedance {		//Impedance Step Data Structure
	_STEP_HEADER	stepHeader;
    long		lVref;
	long		lIref;
	ULONG		ulEndTime;
    
	long		lStartVref;
	long		lStartIref;
	ULONG		ulStartTime;

	long		lOverV;
    long		lLimitV;
    long		lOverI;
    long		lLimitI;

	long 		lOverImpedance;
    long		lLimitImpedance;

	//2006/12/9 추가 
	long		lRecDeltaTime;
	long		lRecDeltaV;
	long		lRecDeltaI;
	long		lRecDeltaT;	
	
	long		lDCIR_RegTemp;
	long		lDCIR_ResistanceRate;
	
	BYTE		aDataID[8];	
	// BYTE		aDataID[EP_RESULT_ITEM_NO];		//not use 20060118
	long		lRecVIGetTime;	// TimeGetValue 를 입력하도록 변경
} EP_IMPEDANCE_STEP; 

//Loop Step Data (nt .data)
typedef struct tag_StepLoop
{		
	_STEP_HEADER	stepHeader;
	UINT nLoopInfoGoto;
	UINT nLoopInfoCycle;
} EP_LOOP_STEP; 

//End Step Data (nt .data)
typedef struct tag_StepEnd {		
	_STEP_HEADER	stepHeader;
} EP_END_STEP; 

//Grade Information Data (nt Data)
typedef struct tag_GradeHeader {
	BYTE	stepIndex;		//Applied Step Data
	BYTE	totalStep;		//Total Grade Step
	BYTE	totalAnd;		//2008-12 ljb -> 추가 중복 조건 개수
	BYTE	reserved1;		
} EP_GRADE_HEADER, *LPEP_GRADE_HEADER;

//Grade Step Data (nt Data)
typedef struct tag_Grade{			//Grading Set Data (NetWork St.)
	BYTE	gradeCode;
	BYTE	gradeItem;
	SHORT	reserved2;	
    long	lValue1;		//low value
	long	lValue2;		//high value
} EP_GRADE;
//////////////////////////////////////////////////////////////////////////

//NVRAM Content (nt Data)
/*
typedef struct tag_NVRamData {
	long lModelKey;		//Battery Model DB Primary Key
	long lTestKey;		//Test	Paramary Key
	char szTraySerialNo[EP_TRAY_SERIAL_LENGTH+1];		//Tray의 고유 번호
	char szTrayNo[EP_TRAY_NAME_LENGTH+1];				//사용자 입력 Tray No Max 31Byte
	char szLotNo[EP_LOT_NAME_LENGTH+1];					//사용자 입력 Lot No  Max 31Byte
	char szTestSerialNo[EP_TEST_SERIAL_LENGTH+1];		//현재 공정에 대한 고유 번호 23Byte(날짜+Random()수를 이용한 고유 번호)
	char szProcessID[EP_PROCESS_ID_LENGTH+1];			//현재 공정이 시행 하고 있는 시험(Battery Model의 Primary Key Index를 저장)	11Byte
	char szLastTestedDate[EP_SHORT_DATE_TIME_LENGTH+1];	//최종 시험 일시	16Byte
	char szCellNo[EP_CELLNO_LENGTH+1];
	int	 nNormalCount;
	int	 nFailCount;
//	char nBatteryState[EP_BATTERY_PER_TRAY][2];			//128*2Byte
} EP_NVRAM_DATA;	
*/

typedef struct tag_EP_CMD_AUTO_NVRAM_HEADER	{
	char szTagID[64];
	char szReserved[64];
} EP_TAG_INFOMATION;

/*
typedef struct tag_NVRamSet {
	int nUseNVRam;
} EP_NVRAM_SET;
*/

/* 
typedef struct tag_TraySerialNo {
	char szTraySerialNo[EP_TRAY_SERIAL_LENGTH+1];	//Tray의 고유 번호
	char szTrayNo[EP_TRAY_NAME_LENGTH+1];			//사용자 입력 Tray No Max 31Byte
} EP_TRAY_SERIAL;
*/
/*
typedef struct tag_Tray_Data {
	long		lTraySerial;
	CString		strJigID;
	long		lTeskID;
	COleDateTime	registedTime;
	CString		strTrayNo;
	CString		strTrayName;
	CString		strUserName;
	CString		strDescription;
	long		lModelKey;					//Battery Model DB Primary Key
	long		lTestKey;					//Test	Paramary Key
	CString		strLotNo;					//사용자 입력 Lot No  Max 31Byte
	CString		strTestSerialNo;			//현재 공정에 대한 고유 번호 23Byte(날짜+Random()수를 이용한 고유 번호)
	COleDateTime	testDateTime;
	long		lCellNo;
	int			nNormalCount;
	int			nFailCount;
	CString		cellCode;					//128
	CString 	cellGradeCode;
} EP_TRAY;
*/

typedef struct tag_Response {				//Command Response (NetWork St.)
	int nCmd;
	int nCode;
} EP_RESPONSE;

typedef struct tag_CommandResponse {
	EP_MSG_HEADER	msgHeader;
	EP_RESPONSE		msgBody;
} EP_RESPONSE_COMMAND;

typedef struct tag_StepEndHeader {			//Test Result Data(NetWrok ST.)
	WORD	wStep;							//1 Base
	WORD	wReserved;
	long	lData1[64];
	long	lData2[64];	
} EP_STEP_END_HEADER;					

typedef struct	tag_EP_CMD_CHECK_PARAM {		//Check Parameter(NetWork St.)
    long		lOCVUpperValue;		//-->OCV High Limit
    long		lOCVLowerValue;		//-->OCV Low Limit
    long		lVRef;
    long		lIRef;
    long		lTime;
    long		lDeltaVoltage;
    long		lMaxFaultBattery;
	unsigned char	compFlag;
    unsigned char	autoProcessingYN;
	WORD		reserved;
	long		lDeltaVoltageLimit;
} EP_PRETEST_PARAM;

typedef struct tag_EP_CMD_AUTO_CHECK_RESULT {
	WORD	wTotalCell;							//총검사수
	WORD	wReserved;							//not use 
	BYTE	byNocellCode[256];					//각 Cell의 검사 결과 NonCell Flag	주의) 0: 정상, 1 : 불량
} EP_CHECK_RESULT;

typedef struct tag_Code {
	int nCode;
	int nData;
} EP_CODE;

typedef struct tag_EP_CMD_USER_CMD {
	int nCmd;
	int nData;
	BYTE reserved1[8];
} EP_USER_CMD_INFO;

//2003/1/14
typedef struct tag_EP_CMD_SET_AUTO_REPORT {
	UINT nInterval;			//자동 보고 간격 
	UINT nSaveInterval;		
} EP_AUTO_REPORT_SET;

typedef struct tag_EP_CMD_DIGITAL_IN_DATA{
	unsigned char	data[EP_MAX_PORT_NO];
} EP_PORT_DATA;

//2003/1/14
typedef struct tag_EP_CMD_LINE_MODE_REQUEST {
	int nOnLineMode;	//2: Çü º¯È¯ ¸ðµå, 1: OffLine,  0: OnLine 
	int nOperationMode;	//0:Local Mode 1:Maintenance Mode 2:Auto Mode 
	unsigned short nWorkType;
	unsigned char reserved[2];
} EP_LINE_MODE_SET;

//2004/3/20
typedef struct tag_EP_CMD_TIME_DATA {
	unsigned short	year;
	unsigned short	month;
	unsigned short	day;
	unsigned short	hour;
	unsigned short	minute;
	unsigned short	second;
} EP_TIME_DATA;

//Tray의 최종 상태를 SBC에 전송하는 Type
typedef	struct tag_EP_CMD_TRAY_DATA {
	BYTE	cellCode[EP_MAX_CH_PER_MD];						//256	Byte
	BYTE    bUpdateFlag[EP_MAX_CH_PER_MD];
} EP_TRAY_STATE;

typedef struct refDate {
	long	lCalData;
	char	szCalTime[EP_DATE_TIME_LENGTH+1];		//32 Byte,  ex) 2003/04/05/17:45:22
	long	lCurData;
	char	szCurTime[EP_DATE_TIME_LENGTH+1];		//32 Byte,  ex) 2003/04/05/17:45:22
	long	lMinData;
	char	szMinTime[EP_DATE_TIME_LENGTH+1];		//32 Byte,  ex) 2003/04/05/17:45:22
	long	lMaxData;
	char	szMaxTime[EP_DATE_TIME_LENGTH+1];		//32 Byte,  ex) 2003/04/05/17:45:22
} _REF_DATA;

//Body of EP_CMD_REF_IC_DATA command
typedef struct ref_ic_data {
	_REF_DATA	vPluse;
	_REF_DATA	vZero;
	_REF_DATA	vMinus;
	_REF_DATA	iPluse;
	_REF_DATA	iZero;
	_REF_DATA	iMinus;
} _REF_AD_DATA, *LP_REF_AD_DATA;

typedef struct tag_EP_CMD_GET_REF_IC_DATA{
	_REF_AD_DATA	refIC_Data[8];
	long reserved[4];
} EP_REF_IC_DATA, *LPEP_REF_IC_DATA;

//body of EP_CMD_CONFIG_DATA, 	EP_CMD_SET_CONFIG
typedef struct tag_EP_CMD_CONFIG_DATA {
	unsigned char	vAutoCal;				//Voltage auto calibration flag
	unsigned char	iAutoCal;				//Current auto calibration flag
	unsigned int	nHuttingCout;			//Count of Hutting Check
	long	vHuttingLevel;			//Voltage Hutting Level(mV)
	long	iHuttingLevel;			//Current Hutting Level(mA)
	long	nStableTime;			//Wait Time to Stabilization(sec)
	long	nDeltaVStableTime;		//Stabilization Time for Delta V(sec)
	long	vRefNGLevel;			//V Ref IC not good level(digit)
	long	iRefNGLevel;			//I Ref ID not good level(digit)
	long	aData[32];
	char	szData[512];
}	EP_MD_CONFIG_DATA, *LPEP_MD_CONFIG_DATA;

typedef struct tag_Real_Time_Data {
	unsigned long	ulTime;
	long		lVoltage;
	long		lCurrent;
} _REAL_TIME_DATA;

typedef struct tag_EP_CMD_REAL_TIME_DATA {
	int nChIndex;
	_REAL_TIME_DATA	dataPoint[10];
} EP_REAL_TIME_DATA;


//20090822
//////////////////////////////////////////////////////////////////////////
//[Cali and Check Point Info]=======
typedef struct tag_EP_CALI_POINT
{
	BYTE byValidPointNum;
	BYTE byCheckPointNum;
	BYTE byReserved[2];
	long lCaliPoint[15];
	long lCheckPoint[15];
} _CALI_POINT; 
//===================================

//[Cali and Check Point Set Info]====
typedef struct tag_EP_CMD_SET_CAL_POINT
{
	int	 nMain;
	BYTE byValidVRangeNum;
	BYTE byValidIRangeNum;
	BYTE byReserved[2];
	long lCaliShuntT[EP_MAX_BD_PER_MD];
	long lCaliShuntR[EP_MAX_BD_PER_MD];
	CHAR byCaliShuntSerail[EP_MAX_BD_PER_MD][16];
	_CALI_POINT vCalPoinSet[4]; // Range 1-4
	_CALI_POINT iCalPoinSet[4]; // Range 1-4
} EP_CAL_POINT_SET;
//====================================

//[Cali/check Start Info]========================
// EP_CMD_CAL_START
// EP_CMD_CHK_START 
// EP_CMD_CAL_RES 
// EP_CMD_CHK_RES 
// EP_CMD_CAL_UPDATE
typedef struct tag_EP_CMD_CALCHK_START_INFO		
{
	BYTE			nMain;				// Main DA=0 / CH=1
	BYTE			bMutiMode;			// MultiMode Option
	BYTE			bTrayType;			// 교정 tray = 0, 실측 tray =1		//ljb 2011519 
	BYTE			bReserved;
	int				nType;				// V=0 or I=1
	int				nBoardNo;			// 1base, Board No
	int				nRange;				// 1base 0:Allbase
	BYTE			byCnSel[EP_MAX_CH_PER_BD];		// 0:not , 1:use
} EP_CAL_SELECT_INFO;
//====================================

//[Calibration Result]=======================
typedef struct tag_EP_CMD_CALICHK_RESULT_DATA
{
	int		nMain;				// Main DA / CH
	int		nType;				// V or I
	int		nBoardNo;			// Board No
	int		nMonCh;				// Monitoring No, 1base
	int		nHwCh;				// Hardware channel, 1base( 보드별 하드웨어 채널 번호 )
	int		nRange;				// Range No
	int		nPtIndex;			// Set Point Index, 0base, Unit:마이크로
	long	lAD;				// AD Value, Unit:마이크로
	long	lDVM;				// DMM Value, Unit:마이크로
	BYTE	byReserved[4];
} EP_CALIHCK_RESULT_DATA;
//====================================

//[Real Measure Start Info]==========================
//
typedef struct tag_EP_CMD_REAL_MEASURE_START_INFO
{
	int		nType;							// V or I	
	BYTE	bMonChNum[EP_MAX_CH_PER_MD];	// 0 = 실측 안함, 1 = 실측	
	long	lVoltageSetVal[10];	// Set Voltage Point Value, 0=>실측안함
	long	lCurrentSetVal[10];	// Set Current Point Value, 0=>실측안함
	long	lCaliShuntT[EP_MAX_BD_PER_MD];
	long	lCaliShuntR[EP_MAX_BD_PER_MD];
	CHAR	byCaliShuntSerail[EP_MAX_BD_PER_MD][16];
	WORD	wRecordTime;
	BYTE	measure_range;
	BYTE	byReserved;
} EP_REAL_MEASURE_START_INFO;
//====================================================

//[Real Measure Result Data]==========================
typedef struct tag_EP_CMD_REAL_MEASURE_RESULT_DATA
{
	int		nType;				// V or I
	int		nBoardNo;			// Board No
	int		nMonCh;				// Monitoring Channel
	int		nHwCh;				// Hardware Channel
	int		nRange;
	long	lSetVal;			// Set Point Value
	long	lAD;				// AD Value
	long	lDMM;				// DMM Value
	int		nMeasureCnt;		// 측정 횟수
} EP_REAL_MEASURE_RESULT_DATA;
//=====================================================

//[Real Measure Point Data]==========================
typedef struct tag_EP_CMD_REAL_MEASURE_POINT_DATA
{
	long lCaliShuntT[EP_MAX_BD_PER_MD];
	long lCaliShuntR[EP_MAX_BD_PER_MD];
	CHAR byCaliShuntSerail[EP_MAX_BD_PER_MD][16];
} EP_REAL_MEASURE_POINT_DATA;
//====================================================


//20090908
typedef struct tag_Mapping_Info
{
	WORD	wMonCh;			// 1Base 0 => N/A
	WORD	wBoardNo;
	WORD	wHwCh;
	WORD	wReserved;
} _MAPPING_INFO;

typedef struct tag_EP_CMD_MAPPING_DATA
{
	_MAPPING_INFO mapping[256];
} EP_HW_MAPPIING_DATA;

typedef struct tag_EP_CMD_IO_INFO
{
	unsigned short manualFunc;
	unsigned short reserved;
} EP_CMD_IO_INFO;

typedef struct tag_EP_CMD_GRIPPER_CLAMPING_INIT
{
	unsigned char set_ch[256];
	LONG lReserved;
} EP_GRIPPER_CLAMPING_INIT;

typedef struct tag_EP_CMD_GRIPPER_CLAMPING_RES
{
	unsigned short clampingCnt[256];
} EP_GRIPPER_CLAMPING_RES;
// 20210129 KSCHOI Add ERCD Write Value Function START
typedef struct tag_EP_CMD_ERCD_WRITE_VALUE
{
	long lCC0DSettingValue;		// mV
	long lCC0DFrequencyTime;	// Sec
	long lCC0ESettingValue;		// microVolt
	long lCC0EFrequencyTime;	// Sec
	long lCC0FSettingValue;		// mA
} EP_ERCD_WRITE_VALUE;
// 20210129 KSCHOI Add ERCD Write Value Function END

/*
//[Real Measure End Info]==========================
typedef struct tag_EP_CMD_REAL_MEASURE_END_INFO
{
	int		nMain;				// Main DA / CH
	int		nType;				// V or I
	int		nBoardNo;			// Board No
	BYTE	byReserved[4];
} EP_REAL_MEASURE_END_INFO;
//====================================================
*/
//**************************************//
//										//	
//			PC Part define				//	
//										//	
//**************************************//

//Server PC ID
#define EP_ID_ALL_SYSTEM		0x00
#define EP_ID_FORM_MODULE		0x01
#define EP_ID_FORM_OP_SYSTEM	0x02
#define EP_ID_IROCV_SYSTEM		0x03
#define EP_ID_GRADING_SYSTEM	0x04
#define EP_ID_DATA_SERVER		0x05
#define EP_ID_AGIGN_SYSTEM		0x06	

#define _EP_TX_BUF_LEN				26800
#define _EP_RX_BUF_LEN				26800

//receive Socket Read State
#define _MSG_ST_CLEAR				0x01
#define _MSG_ST_HEAD_READED			0x02
#define _MSG_ST_REMAIN				0x04
#define _MSG_ST_HEADER_REMAIN		0x08
#define _MSG_ST_ERROR				0x10

#define SizeofCHData()	sizeof(EP_CH_DATA)
#define SizeofGPData()	sizeof(EP_GP_DATA)
#define SizeofHeader()	sizeof(EP_MSG_HEADER)

//define float point Num
#define EP_VTG_FLOAT	1
#define EP_CRT_FLOAT	2
#define EP_ETC_FLOAT	2
#define EP_VTG_EXP		10.0f
#define EP_VTG_M_EXP	10.0f
#define EP_CRT_EXP		100.0f
#define EP_ETC_EXP		100.0f
#define EP_TIME_EXP		10.0f
#define VTG_PRECISION(x)	(float)((float)(x)/EP_VTG_EXP)
#define CRT_PRECISION(x)	(float)((float)(x)/EP_CRT_EXP)
#define ETC_PRECISION(x)	(float)((float)(x)/EP_ETC_EXP)
#define MD2PCTIME(x)			(float)((float)x/10.0f)

#define MAKE2LONG(x)		(x == 0.0 ? 0 : long( x < 0 ? (double)x-0.5 : (double)x+0.5))

#define PC2MDTIME(x)			MAKE2LONG(x*10.0f)			//100msec 단위로 전송 
#define SECTIME(x)				(float)(x/100.0f)		

#define EP_TESTSERIAL_EXP	10000000

//Module Window Message
#define EPWM_MODULE_STATE_CHANGE	WM_USER+2000
#define EPWM_MODULE_CONNECTED		WM_USER+2001
#define EPWM_MODULE_CLOSED			WM_USER+2002
#define EPWM_NVRAM_HEADER			WM_USER+2003	//NVRAM Header
#define EPWM_CHECK_RESULT			WM_USER+2004	//Check Step Result
#define EPWM_MODULE_EMG				WM_USER+2005	//Emergency State Detected
#define	EPWM_USER_CMD				WM_USER+2006
#define EPWM_BCR_SCANED				WM_USER+2007
#define EPWM_REALTIME_DATA_RECEIVED	WM_USER+2008
#define EPWM_STEP_ENDDATA_RECEIVE	WM_USER+2009
#define EPWM_DB_SYS_CONNECTED		WM_USER+2010
#define EPWM_DB_SYS_DISCONNECTED	WM_USER+2011
// kky for OnLine_StepList
#define EPWM_MODULE_ONLINE_STEPLIST		WM_USER+2012
#define EPWM_MODULE_ONLINE_STEPENDDATA	WM_USER+2013


//20090825 CALIBRATION
#define EPWM_CAL_DATA_RECEIVED		WM_USER+2014
#define EPWM_CAL_END_RECEIVED		WM_USER+2015
#define EPWM_RM_END_RECEIVED		WM_USER+2016

//20090825 CALIBRATION
#define EPWM_RM_WORK_END_RECEIVED	WM_USER+2017

#define WM_BCR_READED				WM_USER+2500
#define WM_AUTO_FILE_TRANS			WM_USER+2501

#define EPWM_MODULE_CHANGE_INFO		WM_USER+2999

//20131017 SKI FMS
#define EPWM_FMS_CONNECTED			WM_USER+2600
#define EPWM_FMS_CLOSED				WM_USER+2601
#define EPWM_FMS_PINGRECVED			WM_USER+2602
//test
#define UWM_CHANGE_STATE			WM_USER+7777

//define default System Parameter
#define EP_DEFAULT_CH_PER_BD		5
#define EP_DEFAULT_BD_PER_MD		8
#define EP_DEFAULT_CH_PER_MD		(EP_DEFAULT_CH_PER_BD*EP_DEFAULT_BD_PER_MD)

#define EP_MAX_VOLTAGE			5.0f		//System Maximum Voltage
#define EP_MIN_VOLTAGE			-1.0f		//System Minimum Voltage
#define EP_MAX_CURRENT			3000.0f		//System Maximum Current
#define EP_MIN_CURRENT			-3000.0f	//System Minimum Current	
#define EP_MAX_TEMPERATURE		80L			//System Maximum Temperature
#define EP_MIN_TEMPERATURE		0L			//System Minimum Temperature
#define EP_DIFFER_TEMPERATURE	10L			//System Maximun temperature difference
#define EP_MAX_V24_IN			25.0f		//Max 24 Voltage Input
#define EP_MIN_V24_IN			23.0f		//Min 24 Voltage Input
#define EP_MAX_V24_OUT			25.0f		//Max 24 Voltage Output
#define EP_MIN_V24_OUT			23.0f		//Max 24 Voltage Output
#define EP_MAX_V12_OUT			13.0f		//Max 12 Voltage Output
#define EP_MIN_V12_OUT			11.0f		//Max 12 Voltage Output
#define EP_MAX_GAS_LIMIT		100L		//Max Gas sensor Limit

#define EP_STP_ALLSTEP			-1
#define INDEX					register int

//define Error
#define	EP_ERR_EMPTY				0x00
#define	EP_ERR_INVALID_ARGUMENT		0x01
#define	EP_ERR_WINSOCK_STARTUP		0x02
#define	EP_ERR_WINSOCK_VERSION		0x03
#define	EP_ERR_NOT_INITIALIZED		0x04
#define	EP_ERR_RECEIVE_TIMEOUT		0x05
#define EP_ERR_MODULE_READ_FAIL		0x06
#define EP_ERR_WINSOCK_INITIALIZE	0x07
#define	EP_ERR_LINE_OFF				0x08

#define	EP_ERR_MODULE_NOT_EXIST		0x09
#define EP_ERR_GROUP_NOT_EXIST		0x0A
#define	EP_ERR_BOARD_NOT_EXIST		0x0B
#define	EP_ERR_CHANNEL_NOT_EXIST	0x0C

#define EP_FILE_ID_LENGTH	15
#define EP_FILE_VER_LENGTH	15
#define EP_FILE_DESCRIPTION_LENGTH	127

//Channel Mapping type of Monitoring Data 
#define EP_CH_INDEX_MEMBER		0		//Use wChIndex( the member variable of EP_CH_DATA)
#define EP_CH_INDEX_SEQUENCE	1		//Network incomming Squence 
#define EP_CH_INDEX_CUSTOM		2		//Special Type for LG OffLine
#define EP_CH_INDEX_MAPPING		3		//Read Channel Mapping Table from "mapping.dat" File

//Data Unit Type
#define EP_UNIT_NORMAL	0
#define EP_UNIT_MILLI	1
#define EP_UNIT_MICRO	2

//File Log Type
#define EP_FILELOG_NORMAL	0
#define EP_FILELOG_SBC		1

typedef struct	tag_SystemParameter {			//System Parameter(Network St.)
    int			nModuleID;						//Module ID
	char		szIPAddr[EP_IP_NAME_LENGTH+1];	//	char		IPAddr[16];
	BOOL		bAutoProcess;					// 자동 공정 여부
    long		lMaxVoltage;
    long		lMinVoltage;
    long		lMaxCurrent;
    long		lMinCurrent;
    // long		lMaxTemp;						//lOverTemp; 온도 상한치, 
	BOOL		bUseTrayError;			//위험온도 사용 여부
	BOOL		bReserved;						//위험온도 사용 여부

	unsigned short sWanningTemp;				// 경고값 상한	
	unsigned short sReserved1;					
	unsigned short sCutTemp;					// 위험값 상한
    BOOL		bUseTempLimit;					//경고온도 사용 여부

	unsigned short sJigWanningTemp;				// 경고값 상한	
	unsigned short sReserved2;					// 경고값 하한
	unsigned short sJigCutTemp;					// 위험값 상한
	BOOL		bUseJigTempLimit;				//경고온도 사용 여부
	   
	WORD		wModuleType;					//표시되는 Module TYPE ex) Formaition 2, IR/OCV 3 
	WORD		nAutoReportInterval;			//자동 보고 시간 간격 
    long		lV12Over;
    long		lV12Limit;
    long		lV24Over;
    long		lV24Limit;
	WORD		wWarningNo1;
	WORD		wWarningNo2;
	BOOL		bUseJigTargetTemp;
	int			nJigTargetTemp;
	int			nJigTempInterval;

} EP_SYSTEM_PARAM;


//Min Max Data
typedef struct tag_MinMaxData {
	long lMax;
	long lMin;
	long lAvg;		
	WORD wMaxIndex;
	WORD wMinIndex;
	char szMaxDateTime[20];
	char szMinDateTime[20];
} _MINMAX_DATA;

typedef struct tag_SensorMinMax {
	_MINMAX_DATA	sensorData1[EP_MAX_SENSOR_CH];
	_MINMAX_DATA	sensorData2[EP_MAX_SENSOR_CH];
}	EP_SENSOR_MINMAX;

//New GroupData Structure	2006/2/6
typedef struct tag_GroupData {
	EP_GP_STATE_DATA	gpState;		
	EP_SENSOR_DATA		sensorData;
	EP_SENSOR_MINMAX	sensorMinMax;	
	WORD				wFailCnt[510];		//20090713 KBH 불량 집계를 위해 변수 추가 
	LONG				lClampCnt;				

} EP_GP_DATA;

//Group Management Structure
typedef struct tag_GroupInformation {
	EP_GP_DATA		gpData;
	CPtrArray		chData;
	CPtrArray		chRealData;
} EP_GROUP_INFO, *LPEP_GROUP_INFO;

typedef struct tag_AllModuleData {						//Server Module Management Data
    WORD			state;
//	char			szIpAddress[EP_IP_NAME_LENGTH+1];	//Accepted Module IP		
	EP_RESPONSE		CommandAck;							//Response of Command

	char			szTxBuffer[_EP_TX_BUF_LEN];			//Socket Write Buffer
	HANDLE			m_hWriteEvent;						//Module별 Write Event	
	int				nTxLength;							//Tx Data Length

	char			szRxBuffer[_EP_RX_BUF_LEN];			//Socket Read Buffer	
	HANDLE			m_hReadEvent;						//Module별 Read Event
	int				nRxLength;		
	
	unsigned char		szTempTxBuffer[_EP_TX_BUF_LEN];			//CRC 계산을 위한 버퍼
	unsigned char		szTempRxBuffer[_EP_RX_BUF_LEN];			//CRC 계산을 위한 버퍼					//Rx Data Length
	
	EP_MD_SYSTEM_DATA	sysData;						//Module Version Information
	EP_MD_FW_VER_DATA	fwVerData;						//Module Version Information
	EP_SYSTEM_PARAM		sysParam;						//System Parameter
	EP_MD_SBC_PARAM		sbcParam;						//SBC Parameter //20201019 KSJ
	CPtrArray			gpData;							//Group Data( Array of EP_GROUP_INFO Point)
	int				nOperationMode;						// 0:Local, 1:Maintenance, 2:Auto
	int				nLineMode;
	unsigned short 	nWorkType;							//0:None, 1:'A', 2:'B', 3:'C'
	
	LPVOID			lpDataBuff;
} EP_STR_MODULE_DATA;


#ifdef __cplusepluse
extern "C" {
#endif ///__cplusepluse

#ifndef __EP_FORM_DLL__
#define __EP_FORM_DLL__

#ifdef EXPORT_EPDLL_API
#define EPDLL_API	__declspec(dllexport)
#else	//EXPORT_EPDLL_API
#define EPDLL_API	__declspec(dllimport)
#endif	//EXPORT_EPDLL_API

#endif	//__EP_FORM_DLL__

EPDLL_API	BOOL	EPOpenFormServer(int nInstalledModuleNo, HWND hMsgWnd);
EPDLL_API	BOOL	EPCloseFormServer();
EPDLL_API	int		EPGetInstalledModuleNum();
EPDLL_API	WORD	EPGetChannelState(int nModuleID, int nGroupIndex, int nChannelIndex);
EPDLL_API	WORD	EPGetGroupState(int nModuleID, int nGroupIndex = 0);
EPDLL_API	WORD	EPGetModuleState(int nModuleIndex);
EPDLL_API	int		EPGetModuleID(int nModuleIndex);
// EPDLL_API	int		EPSaveCommand(int nModuleID, int nGrouNo, int nChannelNo, UINT nCmd, LPVOID lpData = NULL, UINT nSize = 0);
// EPDLL_API	int		EPSendCommand(int nModuleID);//new
EPDLL_API	int		EPSendCommand(int nModuleID, int nGrouNo, int nChannelNo, UINT nCmd, LPVOID lpData = NULL, UINT nSize = 0);
EPDLL_API	int		EPJustSendCommand(int nModuleID, int nGrouNo, int nChannelNo, UINT nCmd, LPVOID lpData = NULL, UINT nSize = 0);	//20210223 응답 확인하지 않는 메시지
EPDLL_API	LPSTR	EPGetModuleIP(int nModuleID);
EPDLL_API	BOOL	EPSetSysParam(int nModuleIndex, EP_SYSTEM_PARAM *pParam);
EPDLL_API	EP_SYSTEM_PARAM * EPGetSysParam(int nModuleID);
EPDLL_API	long	EPGetChannelValue(int nModuleID, int nGroupIndex, int nChannelIndex, int nItem);
EPDLL_API	BYTE	EPGetChCode(int nModuleID, int nGroupIndex, int nChannelIndex);
EPDLL_API	int		EPGetModuleIndex(int nModuleNumber);
EPDLL_API	int		EPGetLastError();
EPDLL_API	BOOL	EPInitSysParam(int nModuleIndex,  EP_SYSTEM_PARAM *pParam);
EPDLL_API	EP_GROUP_INFO* EPGetGroupInfo(int nModuleID);
EPDLL_API	int		EPGetGroupCount(int nModuleID);
EPDLL_API	int		EPGetChInGroup(int nModuleID, int nGroupIndex = 0);
EPDLL_API	EP_MD_SYSTEM_DATA * EPGetModuleSysData(int nModuleIndex);
EPDLL_API	EP_MD_FW_VER_DATA * EPGetModuleVersionData(int nModuleIndex);	//20201224ksj
EPDLL_API	EP_MD_SBC_PARAM * EPGetModuleSbcParam(int nModuleIndex);		//20201224ksj
EPDLL_API	EP_GP_DATA EPGetGroupData(int nModuleID, int nGroupIndex);
EPDLL_API	WORD	EPTrayState(int nModuleID, int nJigIndex = 0);
EPDLL_API	WORD	EPJigState(int nModuleID, int nJigIndex = 0);
EPDLL_API	WORD	EPDoorState(int nModuleID, int nJigIndex = 0);
EPDLL_API	LPVOID	EPSendDataCmd(int nModuleID, int nGroupNo, int nChannelNo, int nCmd, int &nSize);
EPDLL_API	void		EPSetLogFileName(char *szFileName);
EPDLL_API	EP_CH_DATA	EPGetChannelData(int nModuleID, int nGroupIndex, int nChannelIndex);
EPDLL_API	int		EPGetInstalledBoard(int nModuleID);
EPDLL_API	int		EPGetChPerBoard(int nModuleID);
EPDLL_API	UINT	EPInstledlTotalGroup();
EPDLL_API	BOOL	EPSetAutoProcess(int nModuleID, BOOL bEnable);
EPDLL_API	BOOL	EPGetAutoProcess(int nModuleID);
EPDLL_API	WORD	EPGetFailCode(int nModuleID, int nGroupIndex = 0);
EPDLL_API	BOOL	EPSetGroupState(int nModuleID, int nGroupIndex, WORD newState);
EPDLL_API	BOOL	EPSetGroupState(int nModuleID, EP_GP_STATE_DATA& _state);
EPDLL_API	void	EPSetChIndexType(int nType);
EPDLL_API	CString EPCmdFailMsg(int nCode);
EPDLL_API	UINT	EPGetHWChIndex(UINT nIndex);
EPDLL_API	BOOL	EPServerStart();

//2003/1/14
EPDLL_API int  EPDllGetVersion();
EPDLL_API BOOL EPSetAutoReport(int nModuleID, int nGroupNo, UINT nInterval, UINT nSaveInterval=2000);
EPDLL_API BOOL EPSetLineMode(int nModuleID, int nGroupNo, int nModeType, int nModeData);
EPDLL_API BOOL EPSetOperationMode( int nModuleID, int nGroupNo, int nModeData );
EPDLL_API BOOL EPGetLineMode(int nModuleID, int nGroupNo, int &nOnLineMode, int &nControlMode, int &nCellTypeMode);
EPDLL_API UINT EPGetProtocolVer(int nModuleID);
EPDLL_API WORD EPStopperState(int nModuleID, int nJigIndex);

//2004/3/21
EPDLL_API int EPSendResponse(int nModuleID, int nCmd, int nCode);

//2007/6/7
EPDLL_API BOOL EPUpdateModuleLineMode(int nModuleID);

EPDLL_API BOOL EPQueueDataPop(SBCQueue_DATA&);
EPDLL_API BOOL EPQueueDataIsEmpty();

EPDLL_API BOOL EPDlgQueueDataPop(SBCQueue_DATA&);
EPDLL_API BOOL EPDlgQueueDataIsEmpty();

EPDLL_API BOOL EPNewServerStart(HWND);
EPDLL_API BOOL EPNewServerStop();

// EPDLL_API BOOL EPNewSendDataCmd(HWND, int nModuleID, int nGroupNo, int nChannelNo, int nCmd);
//TEST
EPDLL_API BOOL EPTestSetLineMode(int nModuleID, int /*nGroupNo*/, int nModeType, int nModeData);
EPDLL_API BOOL EPTestSetState(int nModuleID, WORD _sate);
#ifdef __cplusepluse
}	// extern "C" {
#endif //__cplusepluse

#endif	//_NEW_FORM_MSG_DEFINE_H_