#pragma once

#define LOG_STR_LEN 9192

class CFMSLog
{
public:
	CString *TEXT_LANG;
	
	inline CFMSLog()
	{
		LanguageinitMonConfig();
	}

	inline ~CFMSLog()
	{
		if( TEXT_LANG != NULL )
		{
			delete[] TEXT_LANG;
			TEXT_LANG = NULL;
		}
	}

	bool LanguageinitMonConfig() 
	{
		int i=0;
		int nTextCnt = 0;
		TEXT_LANG = NULL;

		CString strErr = _T("");
		CString strTemp = _T("");
		strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CFMSLog"), _T("TEXT_CFMSLog_CNT"), _T("TEXT_CFMSLog_CNT"));

		nTextCnt = atoi(strTemp);

		if( nTextCnt > 0 )
		{
			TEXT_LANG = new CString[nTextCnt]; //동적할당

			for( i=0; i<nTextCnt; i++ )
			{
				strTemp.Format("TEXT_CFMSLog_%d", i);
				TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CFMSLog"), strTemp, strTemp, g_nLanguage);

				if( TEXT_LANG[i] == strTemp )
				{
					if( strErr.IsEmpty() )
					{
						strErr = "Language error " + strTemp; 
					}
					else
					{
						strErr += "," + strTemp;
					}
				}
			}

			if( !strErr.IsEmpty() )
			{
				AfxMessageBox(strErr);
				return false;
			}
		}

		return true;
	}

	static VOID DirCheck(LPSTR dir)
	{
		INT iResult = _chdir(dir);
		if(iResult == -1)
		{
			iResult = _mkdir(dir);
		}
	}

	static BOOL OneDayLog(LPTSTR Log)
	{
		SYSTEMTIME	SystemTime;
		TCHAR		CurrentDate[36]					= {0,};
		TCHAR		CurrentFileName[MAX_PATH]		= {0,};
		FILE*		FilePtr							= NULL;
		TCHAR		DebugLog[LOG_STR_LEN]		= {0,};

		GetLocalTime(&SystemTime);
		_sntprintf_s(CurrentDate, 36, _T("%04d/%02d/%02d %02d:%02d:%02d:%03d"),
			SystemTime.wYear, 
			SystemTime.wMonth, 
			SystemTime.wDay, 
			SystemTime.wHour,
			SystemTime.wMinute,
			SystemTime.wSecond,
			SystemTime.wMilliseconds);
		CHAR dirdate[256] = {0,};
		sprintf_s(dirdate, "C:\\SERVERLOG\\ONE_DAY_LOG_%04d-%02d-%02d", SystemTime.wYear, SystemTime.wMonth, SystemTime.wDay);
		DirCheck(dirdate);

		_sntprintf_s(CurrentFileName, MAX_PATH, _T("C:\\SERVERLOG\\ONE_DAY_LOG_%04d-%02d-%02d\\LOG_%04d-%02d-%02d.log"),
			SystemTime.wYear, 
			SystemTime.wMonth, 
			SystemTime.wDay,
			SystemTime.wYear, 
			SystemTime.wMonth, 
			SystemTime.wDay);

		_tfopen_s(&FilePtr, CurrentFileName, _T("a"));
		if (!FilePtr)
			return FALSE;

		_ftprintf(FilePtr, _T("[%s] %s\n"), CurrentDate, Log);
		_sntprintf_s(DebugLog, LOG_STR_LEN, _T("[%s] %s\n"), CurrentDate, Log);

		fflush(FilePtr);

		fclose(FilePtr);

		return TRUE;
	}

	static BOOL RecvFMSLog(LPTSTR Log, int nFMSRecvCmdId)
	{
		SYSTEMTIME	SystemTime;
		TCHAR		CurrentDate[36]					= {0,};
		TCHAR		CurrentFileName[MAX_PATH]		= {0,};
		FILE*		FilePtr							= NULL;

		GetLocalTime(&SystemTime);
		_sntprintf_s(CurrentDate, 36, _T("%04d/%02d/%02d %02d:%02d:%02d:%03d"),
			SystemTime.wYear, 
			SystemTime.wMonth, 
			SystemTime.wDay, 
			SystemTime.wHour,
			SystemTime.wMinute,
			SystemTime.wSecond,
			SystemTime.wMilliseconds);
		CHAR dirdate[256] = {0,};
		// sprintf_s(dirdate, "C:\\SERVERLOG\\FMS_%04d-%02d-%02d", SystemTime.wYear, SystemTime.wMonth, SystemTime.wDay);
		sprintf_s(dirdate, "C:\\SERVERLOG\\FMS_%04d-%02d", SystemTime.wYear, SystemTime.wMonth);
		DirCheck(dirdate);

		_sntprintf_s(CurrentFileName, MAX_PATH, _T("C:\\SERVERLOG\\FMS_%04d-%02d\\LOG_%04d-%02d-%02d.log"),
			SystemTime.wYear, 
			SystemTime.wMonth, 
			// SystemTime.wDay,
			SystemTime.wYear, 
			SystemTime.wMonth, 
			SystemTime.wDay);

		_tfopen_s(&FilePtr, CurrentFileName, _T("a"));
		if (!FilePtr)
			return FALSE;
			
		switch( nFMSRecvCmdId )
		{

		case E_CHARGER_OUT:
			{
				_ftprintf(FilePtr, _T("[%s] CHARGER_OUT [RECV] %s\n"), CurrentDate, Log);
			}
			break;

		case E_CHARGER_IMPOSSBLE:
			{
				_ftprintf(FilePtr, _T("[%s] CHARGER_IMPOSSBLE [RECV] %s\n"), CurrentDate, Log);
			}
			break;

		case E_CHARGER_INRESEVE:
			{
				_ftprintf(FilePtr, _T("[%s] CHARGER_INRESEVE [RECV] %s\n"), CurrentDate, Log);
			}
			break;

		case E_CHARGER_IN:
			{
				_ftprintf(FilePtr, _T("[%s] CHARGER_IN [RECV] %s\n"), CurrentDate, Log);
			}
			break;

		case E_CHARGER_WORKEND_RESPONSE:
			{
				_ftprintf(FilePtr, _T("[%s] CHARGER_WORKEND_RESPONSE [RECV] %s\n"), CurrentDate, Log);
			}
			break;

		case E_CHARGER_RESULT:
			{
				_ftprintf(FilePtr, _T("[%s] CHARGER_RESULT [RECV] %s\n"), CurrentDate, Log);
			}
			break;

		case E_CHARGER_RESULT_RECV:
			{
				_ftprintf(FilePtr, _T("[%s] CHARGER_RESULT_RECV [RECV] %s\n"), CurrentDate, Log);
			}
			break;

		case E_CHARGER_MODE_CHANGE:
			{
				_ftprintf(FilePtr, _T("[%s] CHARGER_MODE_CHANGE [RECV] %s\n"), CurrentDate, Log);
			}
			break;

		case E_CHARGER_STATE_RESPONSE:
			{
				_ftprintf(FilePtr, _T("[%s] CHARGER_STATE_RESPONSE [RECV] %s\n"), CurrentDate, Log);
			}
			break;			

		case E_HEAET_BEAT_F:
			{
				_ftprintf(FilePtr, _T("[%s] HEAET_BEAT_F [RECV] %s\n"), CurrentDate, Log);
			}
			break;	

		case E_TIME_SET:
			{
				_ftprintf(FilePtr, _T("[%s] E_TIME_SET [RECV] %s\n"), CurrentDate, Log);
			}
			break;

		case E_ERROR_NOTICE:
			{
				_ftprintf(FilePtr, _T("[%s] ERROR_NOTICE [RECV] %s\n"), CurrentDate, Log);
			}
			break;

		case E_EQ_ERROR_RESPONSE:
			{
				_ftprintf(FilePtr, _T("[%s] EQ_ERROR_RESPONSE [RECV] %s\n"), CurrentDate, Log);
			}
			break;

		default:
			{
				_ftprintf(FilePtr, _T("[%s] UnKnown(%d) [RECV] %s\n"), CurrentDate, nFMSRecvCmdId, Log);

			}				
		}
		
		OutputDebugString("\n");
		OutputDebugString(CurrentDate);
		OutputDebugString("::");
		OutputDebugString(Log);
		OutputDebugString("\n");
		fflush(FilePtr);

		fclose(FilePtr);

		return TRUE;
	}

	static BOOL SendFMSLog(LPTSTR Log, int nFMSSendComId )
	{
		SYSTEMTIME	SystemTime;
		TCHAR		CurrentDate[36]					= {0,};
		TCHAR		CurrentFileName[MAX_PATH]		= {0,};
		FILE*		FilePtr							= NULL;

		GetLocalTime(&SystemTime);
		_sntprintf_s(CurrentDate, 36, _T("%04d/%02d/%02d %02d:%02d:%02d:%03d"),
			SystemTime.wYear, 
			SystemTime.wMonth, 
			SystemTime.wDay, 
			SystemTime.wHour,
			SystemTime.wMinute,
			SystemTime.wSecond,
			SystemTime.wMilliseconds);
		CHAR dirdate[256] = {0,};
		// sprintf_s(dirdate, "C:\\SERVERLOG\\FMS_%04d-%02d-%02d", SystemTime.wYear, SystemTime.wMonth, SystemTime.wDay);		
		sprintf_s(dirdate, "C:\\SERVERLOG\\FMS_%04d-%02d", SystemTime.wYear, SystemTime.wMonth);		
		DirCheck(dirdate);

		_sntprintf_s(CurrentFileName, MAX_PATH, _T("C:\\SERVERLOG\\FMS_%04d-%02d\\LOG_%04d-%02d-%02d.log"),
			SystemTime.wYear, 
			SystemTime.wMonth, 
			// SystemTime.wDay,
			SystemTime.wYear, 
			SystemTime.wMonth, 
			SystemTime.wDay);

		_tfopen_s(&FilePtr, CurrentFileName, _T("a"));
		if (!FilePtr)
			return FALSE;		
			
		switch( nFMSSendComId )
		{
			case E_CHARGER_IMPOSSBLE_RESPONSE:
				{
					_ftprintf(FilePtr, _T("[%s] CHARGER_IMPOSSBLE_RESPONSE [SEND] %s\n"), CurrentDate, Log);
				}
				break;

			case E_CHARGER_INRESEVE_RESPONSE:
				{
					_ftprintf(FilePtr, _T("[%s] CHARGER_INRESEVE_RESPONSE [SEND] %s\n"), CurrentDate, Log);
				}
				break;
				
			case E_CHARGER_IN_RESPONSE:
				{
					_ftprintf(FilePtr, _T("[%s] CHARGER_IN_RESPONSE [SEND] %s\n"), CurrentDate, Log);
				}
				break;
				
			case E_CHARGER_WORKEND:
				{
					_ftprintf(FilePtr, _T("[%s] CHARGER_WORKEND [SEND] %s\n"), CurrentDate, Log);
				}
				break;
				
			case E_CHARGER_RESULT_RESPONSE:
				{
					_ftprintf(FilePtr, _T("[%s] CHARGER_RESULT_RESPONSE [SEND] %s\n"), CurrentDate, Log);
				}
				break;
				
			case E_CHARGER_RESULT_RECV_RESPONSE:
				{
					_ftprintf(FilePtr, _T("[%s] CHARGER_RESULT_RECV_RESPONSE [SEND] %s\n"), CurrentDate, Log);
				}
				break;
				
			case E_CHARGER_OUT_RESPONSE:
				{
					_ftprintf(FilePtr, _T("[%s] CHARGER_OUT_RESPONSE [SEND] %s\n"), CurrentDate, Log);
				}
				break;
				
			case E_CHARGER_MODE_CHANGE_RESPONSE:
				{
					_ftprintf(FilePtr, _T("[%s] CHARGER_MODE_CHANGE_RESPONSE [SEND] %s\n"), CurrentDate, Log);
				}
				break;
				
			case E_CHARGER_STATE:
				{
					_ftprintf(FilePtr, _T("[%s] CHARGER_STATE [SEND] %s\n"), CurrentDate, Log);
				}
				break;			
						
			case E_HEAET_BEAT_P:
				{
					_ftprintf(FilePtr, _T("[%s] HEAET_BEAT_P [SEND] %s\n"), CurrentDate, Log);
				}
				break;	
				
			case E_TIME_SET_RESPONSE:
				{
					_ftprintf(FilePtr, _T("[%s] TIME_SET_RESPONSE [SEND] %s\n"), CurrentDate, Log);
				}
				break;

			case E_ERROR_NOTICE_RESPONSE:
				{
					_ftprintf(FilePtr, _T("[%s] ERROR_NOTICE_RESPONSE [SEND] %s\n"), CurrentDate, Log);
				}
				break;

			case E_EQ_ERROR:
				{
					_ftprintf(FilePtr, _T("[%s] EQ_ERROR [SEND] %s\n"), CurrentDate, Log);
				}
				break;
				
			default:
				{
					_ftprintf(FilePtr, _T("[%s] UnKnown(%d) [SEND] %s\n"), CurrentDate, nFMSSendComId, Log);
				}				
		}

		OutputDebugString("\n");
		OutputDebugString(CurrentDate);
		OutputDebugString("::");
		OutputDebugString(Log);
		OutputDebugString("\n");
		fflush(FilePtr);

		fclose(FilePtr);

		return TRUE;
	}
	static BOOL	WriteLog(LPTSTR data, ...)
	{
		SYSTEMTIME	SystemTime;
		TCHAR		CurrentDate[36]					= {0,};
		TCHAR		CurrentFileName[MAX_PATH]		= {0,};
		FILE*		FilePtr							= NULL;
		TCHAR		DebugLog[LOG_STR_LEN]		= {0,};

		va_list		ap;
		TCHAR		Log[LOG_STR_LEN]	= {0,};

		va_start(ap, data);
		_vstprintf_s(Log, data, ap);
		va_end(ap);

		GetLocalTime(&SystemTime);
		_sntprintf_s(CurrentDate, 36, _T("%04d/%02d/%02d %02d:%02d:%02d:%03d"),
			SystemTime.wYear, 
			SystemTime.wMonth, 
			SystemTime.wDay, 
			SystemTime.wHour,
			SystemTime.wMinute,
			SystemTime.wSecond,
			SystemTime.wMilliseconds);
		CHAR dirdate[256] = {0,};
		// sprintf_s(dirdate, "C:\\SERVERLOG\\LOG_%04d-%02d-%02d", SystemTime.wYear, SystemTime.wMonth, SystemTime.wDay);
		sprintf_s(dirdate, "C:\\SERVERLOG\\LOG_%04d-%02d", SystemTime.wYear, SystemTime.wMonth);
		DirCheck(dirdate);

		_sntprintf_s(CurrentFileName, MAX_PATH, _T("C:\\SERVERLOG\\LOG_%04d-%02d\\LOG_%04d-%02d-%02d.log"),
			SystemTime.wYear, 
			SystemTime.wMonth, 
//			SystemTime.wDay,
			SystemTime.wYear, 
			SystemTime.wMonth, 
			SystemTime.wDay);

		_tfopen_s(&FilePtr, CurrentFileName, _T("a"));
		if (!FilePtr)
			return FALSE;

		_ftprintf(FilePtr, _T("[%s] %s\n"), CurrentDate, Log);
		_sntprintf_s(DebugLog, LOG_STR_LEN, _T("[%s] %s\n"), CurrentDate, Log);

		fflush(FilePtr);

		fclose(FilePtr);

		// OneDayLog(Log);
		OutputDebugString(DebugLog);
		_tprintf(_T("%s"), DebugLog);

		return TRUE;
	}
	static BOOL	WriteErrorLog(LPTSTR data, ...)
	{
		SYSTEMTIME	SystemTime;
		TCHAR		CurrentDate[36]					= {0,};
		TCHAR		CurrentFileName[MAX_PATH]		= {0,};
		FILE*		FilePtr							= NULL;
		TCHAR		DebugLog[LOG_STR_LEN]		= {0,};

		va_list		ap;
		TCHAR		Log[LOG_STR_LEN]	= {0,};

		va_start(ap, data);
		_vstprintf_s(Log, data, ap);
		va_end(ap);

		GetLocalTime(&SystemTime);
		_sntprintf_s(CurrentDate, 36, _T("%04d/%02d/%02d %02d:%02d:%02d:%03d"),
			SystemTime.wYear, 
			SystemTime.wMonth, 
			SystemTime.wDay, 
			SystemTime.wHour,
			SystemTime.wMinute,
			SystemTime.wSecond,
			SystemTime.wMilliseconds);

		CHAR dirdate[256] = {0,};
		// sprintf_s(dirdate, "C:\\SERVERLOG\\FMS_ErrorLOG_%04d-%02d-%02d", SystemTime.wYear, SystemTime.wMonth, SystemTime.wDay);
		sprintf_s(dirdate, "C:\\SERVERLOG\\FMS_ErrorLOG_%04d-%02d", SystemTime.wYear, SystemTime.wMonth);
		DirCheck(dirdate);

		_sntprintf_s(CurrentFileName, MAX_PATH, _T("C:\\SERVERLOG\\FMS_ErrorLOG_%04d-%02d\\FMS_ErrorLOG_%04d-%02d-%02d.log"), 
			SystemTime.wYear, 
			SystemTime.wMonth, 
			// SystemTime.wDay,
			SystemTime.wYear, 
			SystemTime.wMonth, 
			SystemTime.wDay);

		_tfopen_s(&FilePtr, CurrentFileName, _T("a"));
		if (!FilePtr)
			return FALSE;

		_ftprintf(FilePtr, _T("[%s] %s\n"), CurrentDate, Log);
		_sntprintf_s(DebugLog, LOG_STR_LEN, _T("[%s] %s\n"), CurrentDate, Log);

		fflush(FilePtr);

		fclose(FilePtr);
		OutputDebugString(DebugLog);
		//_tprintf(_T("%s"), DebugLog);

		return TRUE;
	}
	static BOOL WriteLongErrorLog(LPTSTR Log)
	{
		SYSTEMTIME	SystemTime;
		TCHAR		CurrentDate[36]					= {0,};
		TCHAR		CurrentFileName[MAX_PATH]		= {0,};
		FILE*		FilePtr							= NULL;

		GetLocalTime(&SystemTime);
		_sntprintf_s(CurrentDate, 36, _T("%04d/%02d/%02d %02d:%02d:%02d:%03d"),
			SystemTime.wYear, 
			SystemTime.wMonth, 
			SystemTime.wDay, 
			SystemTime.wHour,
			SystemTime.wMinute,
			SystemTime.wSecond,
			SystemTime.wMilliseconds);
		CHAR dirdate[256] = {0,};
		sprintf_s(dirdate, "C:\\SERVERLOG\\LOG\\FMS_ErrorLOG_%04d-%02d-%02d", SystemTime.wYear, SystemTime.wMonth, SystemTime.wDay);
		DirCheck(dirdate);

		_sntprintf_s(CurrentFileName, MAX_PATH, _T("C:\\SERVERLOG\\LOG\\FMS_ErrorLOG_%04d-%02d-%02d\\FMS_ErrorLOG_%04d-%02d-%02d.log"), 
			SystemTime.wYear, 
			SystemTime.wMonth, 
			SystemTime.wDay,
			SystemTime.wYear, 
			SystemTime.wMonth, 
			SystemTime.wDay);

		_tfopen_s(&FilePtr, CurrentFileName, _T("a"));
		if (!FilePtr)
			return FALSE;

		_ftprintf(FilePtr, _T("[%s][RECV] %s\n"), CurrentDate, Log);

		OutputDebugString(CurrentDate);
		OutputDebugString(Log);

		fflush(FilePtr);

		fclose(FilePtr);

		return TRUE;
	}
	static BOOL	WriteSysLog(LPTSTR data, ...)
	{
		SYSTEMTIME	SystemTime;
		TCHAR		CurrentDate[36]					= {0,};
		TCHAR		CurrentFileName[MAX_PATH]		= {0,};
		FILE*		FilePtr							= NULL;
		TCHAR		DebugLog[LOG_STR_LEN]		= {0,};

		va_list		ap;
		TCHAR		Log[LOG_STR_LEN]	= {0,};

		va_start(ap, data);
		_vstprintf_s(Log, data, ap);
		va_end(ap);

		GetLocalTime(&SystemTime);
		_sntprintf_s(CurrentDate, 36, _T("%04d/%02d/%02d %02d:%02d:%02d:%03d"),
			SystemTime.wYear, 
			SystemTime.wMonth, 
			SystemTime.wDay, 
			SystemTime.wHour,
			SystemTime.wMinute,
			SystemTime.wSecond,
			SystemTime.wMilliseconds);

		CHAR dirdate[256] = {0,};
		sprintf_s(dirdate, "C:\\SERVERLOG\\LOG\\SYSLOG_%04d-%02d-%02d", SystemTime.wYear, SystemTime.wMonth, SystemTime.wDay);
		DirCheck(dirdate);

		_sntprintf_s(CurrentFileName, MAX_PATH, _T("C:\\SERVERLOG\\LOG\\SYSLOG_%04d-%02d-%02d\\SYSLOG_%04d-%02d-%02d %02d.log"), 
			SystemTime.wYear, 
			SystemTime.wMonth, 
			SystemTime.wDay,
			SystemTime.wYear, 
			SystemTime.wMonth, 
			SystemTime.wDay,
			SystemTime.wHour);

		_tfopen_s(&FilePtr, CurrentFileName, _T("a"));
		if (!FilePtr)
			return FALSE;

		_ftprintf(FilePtr, _T("[%s] %s\n"), CurrentDate, Log);
		_sntprintf_s(DebugLog, LOG_STR_LEN, _T("[%s] %s\n"), CurrentDate, Log);

		fflush(FilePtr);

		fclose(FilePtr);

		OutputDebugString(DebugLog);
		//_tprintf(_T("%s"), DebugLog);

		return TRUE;
	}
	static BOOL	WriteCOMLog(LPTSTR data, ...)
	{
		SYSTEMTIME	SystemTime;
		TCHAR		CurrentDate[36]					= {0,};
		TCHAR		CurrentFileName[MAX_PATH]		= {0,};
		FILE*		FilePtr							= NULL;
		TCHAR		DebugLog[LOG_STR_LEN]		= {0,};

		va_list		ap;
		TCHAR		Log[LOG_STR_LEN]	= {0,};

		va_start(ap, data);
		_vstprintf_s(Log, data, ap);
		va_end(ap);

		GetLocalTime(&SystemTime);
		_sntprintf_s(CurrentDate, 36, _T("%04d/%02d/%02d %02d:%02d:%02d:%03d"),
			SystemTime.wYear, 
			SystemTime.wMonth, 
			SystemTime.wDay, 
			SystemTime.wHour,
			SystemTime.wMinute,
			SystemTime.wSecond,
			SystemTime.wMilliseconds);

		CHAR dirdate[256] = {0,};
		sprintf_s(dirdate, "C:\\SERVERLOG\\COMLOG\\COMLOG_%04d-%02d-%02d", SystemTime.wYear, SystemTime.wMonth, SystemTime.wDay);

		DirCheck(dirdate);

		_sntprintf_s(CurrentFileName, MAX_PATH, _T("C:\\SERVERLOG\\COMLOG\\COMLOG_%04d-%02d-%02d\\COMLOG_%04d-%02d-%02d %02d.log"), 
			SystemTime.wYear, 
			SystemTime.wMonth, 
			SystemTime.wDay,
			SystemTime.wYear, 
			SystemTime.wMonth, 
			SystemTime.wDay,
			SystemTime.wHour);

		_tfopen_s(&FilePtr, CurrentFileName, _T("a"));
		if (!FilePtr)
			return FALSE;

		_ftprintf(FilePtr, _T("[%s] %s\n"), CurrentDate, Log);
		_sntprintf_s(DebugLog, LOG_STR_LEN, _T("[%s] %s\n"), CurrentDate, Log);

		fflush(FilePtr);

		fclose(FilePtr);

		//OutputDebugString(DebugLog);
		//_tprintf(_T("%s"), DebugLog);

		return TRUE;
	}
	static BOOL	WriteDBLog(LPTSTR data, ...)
	{
		SYSTEMTIME	SystemTime;
		TCHAR		CurrentDate[36]					= {0,};
		TCHAR		CurrentFileName[MAX_PATH]		= {0,};
		FILE*		FilePtr							= NULL;
		TCHAR		DebugLog[LOG_STR_LEN]		= {0,};

		va_list		ap;
		TCHAR		Log[LOG_STR_LEN]	= {0,};

		va_start(ap, data);
		_vstprintf_s(Log, data, ap);
		va_end(ap);

		GetLocalTime(&SystemTime);
		_sntprintf_s(CurrentDate, 36, _T("%04d/%02d/%02d %02d:%02d:%02d:%03d"),
			SystemTime.wYear, 
			SystemTime.wMonth, 
			SystemTime.wDay, 
			SystemTime.wHour,
			SystemTime.wMinute,
			SystemTime.wSecond,
			SystemTime.wMilliseconds);


		CHAR dirdate[256] = {0,};
		sprintf_s(dirdate, "C:\\SERVERLOG\\DBLOG\\DBLOG_%04d-%02d-%02d", SystemTime.wYear, SystemTime.wMonth, SystemTime.wDay);

		DirCheck(dirdate);

		_sntprintf_s(CurrentFileName, MAX_PATH, _T("C:\\SERVERLOG\\DBLOG\\DBLOG_%04d-%02d-%02d\\DBLOG_%04d-%02d-%02d %02d.log"), 
			SystemTime.wYear, 
			SystemTime.wMonth, 
			SystemTime.wDay,
			SystemTime.wYear, 
			SystemTime.wMonth, 
			SystemTime.wDay,
			SystemTime.wHour);

		_tfopen_s(&FilePtr, CurrentFileName, _T("a"));
		if (!FilePtr)
			return FALSE;

		_ftprintf(FilePtr, _T("[%s] %s\n"), CurrentDate, Log);
		_sntprintf_s(DebugLog, LOG_STR_LEN, _T("[%s] %s\n"), CurrentDate, Log);

		fflush(FilePtr);

		fclose(FilePtr);

		OutputDebugString(DebugLog);
		//_tprintf(_T("%s"), DebugLog);

		return TRUE;
	}
	static BOOL	WriteLogNoDate(LPTSTR data, ...)
	{
		SYSTEMTIME	SystemTime;
		TCHAR		CurrentDate[36]					= {0,};
		TCHAR		CurrentFileName[MAX_PATH]		= {0,};
		FILE*		FilePtr							= NULL;
		TCHAR		DebugLog[LOG_STR_LEN]		= {0,};

		va_list		ap;
		TCHAR		Log[LOG_STR_LEN]	= {0,};

		va_start(ap, data);
		_vstprintf_s(Log, data, ap);
		va_end(ap);

		GetLocalTime(&SystemTime);
		_sntprintf_s(CurrentDate, 36, _T("%04d/%02d/%02d %02d:%02d:%02d:%03d"),
			SystemTime.wYear, 
			SystemTime.wMonth, 
			SystemTime.wDay, 
			SystemTime.wHour,
			SystemTime.wMinute,
			SystemTime.wSecond,
			SystemTime.wMilliseconds);

		CHAR dirdate[256] = {0,};
		sprintf_s(dirdate, "C:\\SERVERLOG\\LOG\\LOG_%04d-%02d-%02d", SystemTime.wYear, SystemTime.wMonth, SystemTime.wDay);

		DirCheck(dirdate);

		_sntprintf_s(CurrentFileName, MAX_PATH, _T("C:\\SERVERLOG\\LOG\\LOG_%04d-%02d-%02d\\LOG_%04d-%02d-%02d %02d.log"), 
			SystemTime.wYear, 
			SystemTime.wMonth, 
			SystemTime.wDay,
			SystemTime.wYear, 
			SystemTime.wMonth, 
			SystemTime.wDay,
			SystemTime.wHour);

		_tfopen_s(&FilePtr, CurrentFileName, _T("a"));
		if (!FilePtr)
			return FALSE;

		_ftprintf(FilePtr, _T("%s"), Log);
		_sntprintf_s(DebugLog, LOG_STR_LEN, _T("%s"), Log);

		fflush(FilePtr);

		fclose(FilePtr);
		// OneDayLog(Log);
		OutputDebugString(DebugLog);
		_tprintf(_T("%s"), DebugLog);

		return TRUE;
	}
	static BOOL	WritePayLog(LPTSTR data, ...)
	{
		SYSTEMTIME	SystemTime;
		TCHAR		CurrentDate[36]					= {0,};
		TCHAR		CurrentFileName[MAX_PATH]		= {0,};
		FILE*		FilePtr							= NULL;
		TCHAR		DebugLog[LOG_STR_LEN]		= {0,};

		va_list		ap;
		TCHAR		Log[LOG_STR_LEN]	= {0,};

		va_start(ap, data);
		_vstprintf_s(Log, data, ap);
		va_end(ap);

		GetLocalTime(&SystemTime);
		_sntprintf_s(CurrentDate, 36, _T("%04d/%02d/%02d %02d:%02d:%02d:%03d"),
			SystemTime.wYear, 
			SystemTime.wMonth, 
			SystemTime.wDay, 
			SystemTime.wHour,
			SystemTime.wMinute,
			SystemTime.wSecond,
			SystemTime.wMilliseconds);

		CHAR dirdate[256] = {0,};
		sprintf_s(dirdate, "C:\\SERVERLOG\\PAYLOG\\PAYLOG_%04d-%02d-%02d", SystemTime.wYear, SystemTime.wMonth, SystemTime.wDay);

		DirCheck(dirdate);

		_sntprintf_s(CurrentFileName, MAX_PATH, _T("C:\\SERVERLOG\\PAYLOG\\PAYLOG_%04d-%02d-%02d\\PAYLOG_%04d-%02d-%02d.log"), 
			SystemTime.wYear, 
			SystemTime.wMonth, 
			SystemTime.wDay,
			SystemTime.wYear, 
			SystemTime.wMonth, 
			SystemTime.wDay,
			SystemTime.wHour);
		_tfopen_s(&FilePtr, CurrentFileName, _T("a"));
		if (!FilePtr)
			return FALSE;

		_ftprintf(FilePtr, _T("[%s] %s\n"), CurrentDate, Log);
		_sntprintf_s(DebugLog, LOG_STR_LEN, _T("[%s] %s\n"), CurrentDate, Log);

		fflush(FilePtr);

		fclose(FilePtr);

		//OutputDebugString(DebugLog);
		//_tprintf(_T("%s"), DebugLog);

		return TRUE;
	}
	static BOOL LastErrorLog()
	{
		DWORD _error =  GetLastError();
		// 에러 메세지를 저장하기 위한 버퍼
		HLOCAL hlocal = NULL;   

		// 윈도우 메시지 문자열을 얻기 위해 기본 시스템 지역 설정을 사용한다.
		// 주의 : 아래의 MAKELANGID는 0 값을 반환한다.
		DWORD systemLocale = MAKELANGID(LANG_NEUTRAL, SUBLANG_NEUTRAL);

		// 에러코드의 메시지를 가져온다.
		BOOL fOk = FormatMessage(
			FORMAT_MESSAGE_FROM_SYSTEM | 
			FORMAT_MESSAGE_IGNORE_INSERTS |
			FORMAT_MESSAGE_ALLOCATE_BUFFER, 
			NULL, 
			_error, 
			systemLocale, 
			(PTSTR) &hlocal, 
			0, 
			NULL);

		if (!fOk) 
		{
			// 네트워크와 관련된 에러인지 판단!
			HMODULE hDll = LoadLibraryEx(	TEXT("netmsg.dll"), 
				NULL, 
				DONT_RESOLVE_DLL_REFERENCES
				);

			if (hDll != NULL) 
		 {
			 fOk = FormatMessage( FORMAT_MESSAGE_FROM_HMODULE | 
				 FORMAT_MESSAGE_IGNORE_INSERTS |
				 FORMAT_MESSAGE_ALLOCATE_BUFFER,
				 hDll, 
				 _error, 
				 systemLocale,
				 (PTSTR) &hlocal, 
				 0,
				 NULL );
			 FreeLibrary(hDll);
			}
		}

		if (fOk && (hlocal != NULL)) 
		{
			//CString temp = ;
			//------------------------------------------------------------------------------
			SYSTEMTIME	SystemTime;
			TCHAR		CurrentDate[36]					= {0,};
			TCHAR		CurrentFileName[MAX_PATH]		= {0,};
			FILE*		FilePtr							= NULL;
			TCHAR		DebugLog[LOG_STR_LEN]		= {0,};
			TCHAR		Log[LOG_STR_LEN]	= {0,};

			memcpy (Log, (PCTSTR) LocalLock(hlocal), LOG_STR_LEN - 1);

			GetLocalTime(&SystemTime);
			_sntprintf_s(CurrentDate, 36, _T("%04d/%02d/%02d %02d:%02d:%02d:%03d"),
				SystemTime.wYear, 
				SystemTime.wMonth, 
				SystemTime.wDay, 
				SystemTime.wHour,
				SystemTime.wMinute,
				SystemTime.wSecond,
				SystemTime.wMilliseconds);

			CHAR dirdate[256] = {0,};
			sprintf_s(dirdate, "C:\\SERVERLOG\\LASTERRORLOG\\LASTERRORLOG_%04d-%02d-%02d", SystemTime.wYear, SystemTime.wMonth, SystemTime.wDay);

			DirCheck(dirdate);

			_sntprintf_s(CurrentFileName, MAX_PATH, _T("C:\\SERVERLOG\\LASTERRORLOG\\LASTERRORLOG_%04d-%02d-%02d\\LASTERRORLOG_%04d-%02d-%02d.log"), 
				SystemTime.wYear, 
				SystemTime.wMonth, 
				SystemTime.wDay,
				SystemTime.wYear, 
				SystemTime.wMonth, 
				SystemTime.wDay,
				SystemTime.wHour);
			_tfopen_s(&FilePtr, CurrentFileName, _T("a"));
			if (!FilePtr)
				return FALSE;

			_ftprintf(FilePtr, _T("[%s] %s\n"), CurrentDate, Log);
			_sntprintf_s(DebugLog, LOG_STR_LEN, _T("[%s] %s\n"), CurrentDate, Log);

			fflush(FilePtr);

			fclose(FilePtr);

			//OutputDebugString(DebugLog);
			//_tprintf(_T("%s"), DebugLog);


			//------------------------------------------------------------------------------
			LocalFree(hlocal);
		} 
		else 
		{
			//MessageBox( hwnd, TEXT("No text found for this error number."), TEXT("Error"), MB_OK );
		}

		return TRUE;
	}	
};
