// ChResultViewDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "ChResultViewDlg.h"
#include "PrntScreen.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChResultViewDlg dialog

bool CChResultViewDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CChResultViewDlg"), _T("TEXT_CChResultViewDlg_CNT"), _T("TEXT_CChResultViewDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CChResultViewDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CChResultViewDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}


CChResultViewDlg::CChResultViewDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CChResultViewDlg::IDD, pParent)
{
	LanguageinitMonConfig();
	//{{AFX_DATA_INIT(CChResultViewDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_pResultFile = NULL;
}
CChResultViewDlg::~CChResultViewDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


void CChResultViewDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChResultViewDlg)
	DDX_Control(pDX, IDC_CH_NO, m_ctrlChDisp);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CChResultViewDlg, CDialog)
	//{{AFX_MSG_MAP(CChResultViewDlg)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_FILE_SAVE, OnFileSave)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChResultViewDlg message handlers

BOOL CChResultViewDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	ASSERT(m_pResultFile);
	
	// TODO: Add extra initialization here
	CString strTemp;
	strTemp.Format(TEXT_LANG[0], m_strModule, m_nChannelIndex+1);	//"%s Channel %d 결과 Data"
	m_ctrlChDisp.SetText(strTemp);
	m_ctrlChDisp.SetBkColor(RGB(255, 255, 255));
	InitChGrid();
//	if(m_pBoard->stBoardFileHeader.wChPerBd <= m_wChannelIndex || m_wChannelIndex <0)	return TRUE;
	DisplayChannelData();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CChResultViewDlg::InitChGrid()
{
	m_wndChGrid.SubclassDlgItem(IDC_CH_STEP_VIEW_GRID, this);
	//m_wndBoardListGrid.m_bRowSelection = TRUE;
	m_wndChGrid.m_bSameColSize  = FALSE;
	m_wndChGrid.m_bSameRowSize  = FALSE;
	m_wndChGrid.m_bCustomWidth  = TRUE;

	CRect rect;
	m_wndChGrid.GetWindowRect(rect);	//Step Grid Window Position
	ScreenToClient(&rect);

	int width = rect.Width()/100;
	m_wndChGrid.m_nWidth[1]	= width* 7;
	m_wndChGrid.m_nWidth[2]	= width* 10;
	m_wndChGrid.m_nWidth[3]= width* 10;
	m_wndChGrid.m_nWidth[4]= width* 8;
	m_wndChGrid.m_nWidth[5]	= width* 15;
	m_wndChGrid.m_nWidth[6]	= width* 10;
	m_wndChGrid.m_nWidth[7]	= width* 10;
	m_wndChGrid.m_nWidth[8]	= width* 10;
	m_wndChGrid.m_nWidth[9]	= width* 10;
	m_wndChGrid.m_nWidth[10]	= width* 10;
	m_wndChGrid.m_nWidth[11]	= width* 10;

	m_wndChGrid.Initialize();
//	m_wndChGrid.SetRowCount();
	m_wndChGrid.SetColCount(11);
	m_wndChGrid.SetDefaultRowHeight(18);
	m_wndChGrid.GetParam()->SetSortRowsOnDblClk(TRUE);
	m_wndChGrid.m_BackColor	= RGB(255,255,255);
//	m_wndChGrid.m_SelColor	= RGB(225,255,225);
//	m_wndChGrid.HideCols(5, 5);

	m_wndChGrid.m_bCustomColor 	= FALSE;
//	m_wndChGrid.m_CustomColorRange	= CGXRange(1, 2, TOT_CH_PER_MODULE, 2);
//	memset(m_DetalChColorFlag, 15, sizeof(m_DetalChColorFlag));		//Default color Setting Array[15]
//	m_wndChGrid.m_pCustomColorFlag = (char *)m_DetalChColorFlag;

	m_wndChGrid.SetValueRange(CGXRange(0, 1),  TEXT_LANG[1]);//"Step"
	m_wndChGrid.SetValueRange(CGXRange(0, 2),  TEXT_LANG[2]);	//"State"
	m_wndChGrid.SetValueRange(CGXRange(0, 3),  TEXT_LANG[3]);//"Code"
	m_wndChGrid.SetValueRange(CGXRange(0, 4),  TEXT_LANG[4]);//"Grade"
	m_wndChGrid.SetValueRange(CGXRange(0, 5),  TEXT_LANG[5]);//"Step Time(s)"
	
	CString strUnit;
	strUnit.Format(TEXT_LANG[6]+"(%s)", m_pDoc->m_strVUnit);//전압
	m_wndChGrid.SetValueRange(CGXRange(0, 6),  strUnit);
	strUnit.Format(TEXT_LANG[7]+"(%s)", m_pDoc->m_strIUnit);//전류
	m_wndChGrid.SetValueRange(CGXRange(0, 7),  strUnit);
	strUnit.Format(TEXT_LANG[8]+"(%s)", m_pDoc->m_strCUnit);//용량
	m_wndChGrid.SetValueRange(CGXRange(0, 8), strUnit);
	m_wndChGrid.SetValueRange(CGXRange(0, 9), "Imp(mOhm)");
	if(m_pDoc->m_strCUnit == "Ah")
	{
		m_wndChGrid.SetValueRange(CGXRange(0, 10),  "Watt(W)");
		m_wndChGrid.SetValueRange(CGXRange(0, 11),  "Watt Hour(Wh)");
	}
	else
	{
		m_wndChGrid.SetValueRange(CGXRange(0, 10),  "Watt(mW)");
		m_wndChGrid.SetValueRange(CGXRange(0, 11),  "Watt Hour(mWh)");
	}

	m_wndChGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
}

BOOL CChResultViewDlg::DisplayChannelData()
{
	char szTemp[128];
	BYTE colorFlag;
	
	STR_STEP_RESULT *pStepResult;
	LPSTR_SAVE_CH_DATA lpChData;
	int nCount;
	int nStepSize = m_pResultFile->GetStepSize();
	m_wndChGrid.InsertRows(1, nStepSize);
//	EP_GROUP_INFO *pStep;

	for(int i = 0; i<nStepSize; i++)
	{
		nCount = i+1;
		m_wndChGrid.SetValueRange(CGXRange(nCount, 1), (ROWCOL)nCount );	//Step Number
		pStepResult = m_pResultFile->GetStepData(i);
		if(pStepResult == NULL)	break;

		int size = pStepResult->aChData.GetSize();

		if(m_nChannelIndex <0 || m_nChannelIndex >= size)	break;
		lpChData = (STR_SAVE_CH_DATA *)pStepResult->aChData[m_nChannelIndex]; 

		m_wndChGrid.SetValueRange(CGXRange(nCount, 2),  m_pDoc->GetStateMsg(lpChData->state, colorFlag));
			
/*		if(colorFlag < MODULE_COLOR)
			m_wndChGrid.SetStyleRange(CGXRange(nCount, 2), 
							CGXStyle().SetTextColor(m_pConfig->m_TModuleColor[colorFlag])
									  .SetInterior(m_pConfig->m_BModuleColor[colorFlag]));
		else
			m_wndChGrid.SetStyleRange(CGXRange(nCount, 2), 
							CGXStyle().SetTextColor(m_pConfig->m_TChannelColor[colorFlag-MODULE_COLOR])
									  .SetInterior(m_pConfig->m_BChannelColor[colorFlag-MODULE_COLOR]));
*/
		m_wndChGrid.SetStyleRange(CGXRange(nCount, 2), 
						CGXStyle().SetTextColor(m_pConfig->m_TStateColor[colorFlag])
								  .SetInterior(m_pConfig->m_BStateColor[colorFlag]));


		m_wndChGrid.SetValueRange(CGXRange(nCount, 5),  m_pDoc->ValueString(lpChData->fStepTime, EP_STEP_TIME));
		sprintf(szTemp, "%s", m_pDoc->ValueString(lpChData->fVoltage, EP_VOLTAGE));
		m_wndChGrid.SetValueRange(CGXRange(nCount, 6),  szTemp);
		sprintf(szTemp, "%s", m_pDoc->ValueString(lpChData->fCurrent, EP_CURRENT));
		m_wndChGrid.SetValueRange(CGXRange(nCount, 7),  szTemp);
		sprintf(szTemp, "%s", m_pDoc->ValueString(lpChData->fCapacity, EP_CAPACITY));
		m_wndChGrid.SetValueRange(CGXRange(nCount, 8),  szTemp);
		sprintf(szTemp, "%s", m_pDoc->ValueString(lpChData->fWatt, EP_WATT));
		m_wndChGrid.SetValueRange(CGXRange(nCount, 10),  szTemp);
		sprintf(szTemp, "%s", m_pDoc->ValueString(lpChData->fWattHour, EP_WATT_HOUR));
		m_wndChGrid.SetValueRange(CGXRange(nCount, 11),  szTemp);
		sprintf(szTemp,  "%s", m_pDoc->ValueString(lpChData->fImpedance, EP_IMPEDANCE));
		m_wndChGrid.SetValueRange(CGXRange(nCount, 9), szTemp);
		m_wndChGrid.SetValueRange(CGXRange(nCount, 4), m_pDoc->ValueString(lpChData->grade, EP_GRADE_CODE));
		m_wndChGrid.SetValueRange(CGXRange(nCount, 3), m_pDoc->ValueString(lpChData->channelCode, EP_CH_CODE));
	}
	return TRUE;
}

void CChResultViewDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	if(::IsWindow(this->GetSafeHwnd()))
	{
		RECT rect;
		::GetClientRect(m_hWnd, &rect);

		CRect rectGrid;
		float width;
		if(m_wndChGrid.GetSafeHwnd())
		{
			m_wndChGrid.GetWindowRect(rectGrid);	//Step Grid Window Position
			ScreenToClient(&rectGrid);
			m_wndChGrid.MoveWindow(5, rectGrid.top, rect.right-10, rect.bottom-rectGrid.top-5, FALSE);
			
			m_wndChGrid.GetClientRect(rectGrid);
			width = (float)rectGrid.Width()/100.0f;
				
			m_wndChGrid.m_nWidth[1]	= int(width* 7);
			m_wndChGrid.m_nWidth[2]	= int(width* 10);
			m_wndChGrid.m_nWidth[3]= int(width* 10);
			m_wndChGrid.m_nWidth[4]= int(width* 8);
			m_wndChGrid.m_nWidth[5]	= int(width* 15);
			m_wndChGrid.m_nWidth[6]	= int(width* 10);
			m_wndChGrid.m_nWidth[7]	= int(width* 10);
			m_wndChGrid.m_nWidth[8]	= int(width* 10);
			m_wndChGrid.m_nWidth[9]	= int(width* 10);
			m_wndChGrid.m_nWidth[10]	= int(width* 10);
			m_wndChGrid.m_nWidth[11]	= int(width* 10);
//			m_wndChGrid.Redraw();
			Invalidate();
		}
	}
}

void CChResultViewDlg::OnFileSave() 
{
	// TODO: Add your control notification handler code here
	CString strFileName;
	strFileName.Format("%s_C%d.csv", m_strModule, m_nChannelIndex+1);

	CFileDialog pDlg(FALSE, "", strFileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, TEXT_LANG[9]);//"csv 파일(*.csv)|*.csv|"
	if(IDOK == pDlg.DoModal())
	{
		strFileName = pDlg.GetPathName();
		if(strFileName.IsEmpty())	return ;

		FILE *fp = fopen(strFileName, "wt");
		if(fp == NULL)	return ;

		BeginWaitCursor();
			
		for(int i=0; i< m_wndChGrid.GetRowCount(); i++)
		{
			for(int j=1; j< m_wndChGrid.GetColCount()+1; j++)
			{
				if( j > 1)
					fprintf(fp, ",");

				fprintf(fp, "%s", m_wndChGrid.GetValueRowCol(i, j));
			}
			fprintf(fp, "\n");
		}

		EndWaitCursor();
		
		fclose(fp);
	}
}

void CChResultViewDlg::OnButton1() 
{
	// TODO: Add your control notification handler code here
	 CPrntScreen * ScrCap;
     ScrCap = new CPrntScreen("Impossible to print!","Error!");
     ScrCap->DoPrntScreen(1,2,TRUE);
     delete ScrCap;
     ScrCap = NULL;	
}
