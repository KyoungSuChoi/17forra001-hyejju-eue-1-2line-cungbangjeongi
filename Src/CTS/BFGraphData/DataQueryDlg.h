/*************************************************************
	Copyrighted by LG Production engineering Research Center

	File Name: DataQueryDlg.h

	Written out by 
	Evaluation Thechnology Group
	Kim, Sung-Hoon

	Comment: CDataQueryDialog class is defined

	Revision History:
	Version   Name            Date        Reason
	    1.0   Kim, Sung-Hoon  2001.06.01  Create
*************************************************************/

#if !defined(AFX_DATAQUERYDLG_H__90DAB181_E17D_11D4_88DF_006008CEDA07__INCLUDED_)
#define AFX_DATAQUERYDLG_H__90DAB181_E17D_11D4_88DF_006008CEDA07__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DataQueryDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDataQueryDialog dialog

// #include

#include "resource.h"
class CDataQueryCondition;
class CDaoDatabase;

#define QUERY_REG_SECTION	"Query"
// #define

// Class definition

//---------------------------------------------------------------------------
//	Class Name: CDataQueryDialog
//
//	Comment:
//
//	Revision History:
//	Version   Name            Date        Reason
//	    1.0   Kim, Sung-Hoon  2001.06.01  Create
//---------------------------------------------------------------------------
class CDataQueryDialog : public CDialog
{
// Construction
public:
	CDataQueryDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDataQueryDialog();
	CString *TEXT_LANG;
	BOOL LanguageinitMonConfig();

////////////////////////////////////
// 2003-02-25 추가 요구사항
	CString m_strTestName;
////////////////////////////////////

// Dialog Data
	//{{AFX_DATA(CDataQueryDialog)
	enum { IDD = IDD_DATA_QUERY_DIALOG };
//	CBitmapButton	m_TestFromFolderBtn;
//	CBitmapButton	m_TestFromListBtn;
	CListCtrl	m_TestNameListCtrl;
	CCheckListBox	m_YAxisList;
	CComboBox	m_YUnitCombo;
	CComboBox	m_XUnitCombo;
	CComboBox	m_XAxisCombo;
	CListBox	m_SelCycleList;
	CListCtrl	m_ChannelListCtrl;
	long	m_lFromCycle;
	long	m_lIntervalCycle;
	BOOL	m_bCycleStringCheck;
	CString	m_strCycle;
	long	m_lToCycle;
	int		m_iMode;
//	int		m_iOpenTime;
	int		m_iStartType;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDataQueryDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDataQueryDialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnAddSelCycleBtn();
	afx_msg void OnDelSelCycleBtn();
	afx_msg void OnCycleStringCheck();
	afx_msg void OnDestroy();
	afx_msg void OnSelchangeXAxisCombo();
	afx_msg void OnSelchangeYAxisList();
	afx_msg void OnSelchangeYUnitCombo();
	afx_msg void OnTestFromFolderBtn();
	afx_msg void OnTestFromListBtn();	
	afx_msg void OnItemchangedChannelList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBtnExcelExport();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

////////////////
// Attributes //
////////////////
private:

	// 속해 있는 CDataQueryCondition object의 Pointer
	CDataQueryCondition*   m_pQueryCondition;

	// Dialog Box가 열려 있는 동안 사용할 변수들
	CDaoDatabase*          m_pDatabase;
	BYTE*                  m_pbyXAxisIndex;			//X 축으로 사용 가능한 리스트 저장 

	// Dialog Box를 Reload할 때 ListCtrl이나 ListBox의 내용을
	// 채우기 위해 필요한 내용을 저장하는 String List
public:
	CStringList            m_strlistTestPath;		//선택 시험명 리스트 
	BYTE                   m_byVoltagePoint;
	BYTE                   m_byTemperaturePoint;
private:
	CStringList            m_strlistSelCycle;		//Cycle 구간 선택 리스트 

	//
	CList<CPoint, CPoint&> m_YAxisPropertyIndex;
	
////////////////
// Operations //
////////////////
public:
	void WriteRegUserSet();
	void AddTestPath(LPCTSTR path, int nIndex);
	void SetQueryCondition(CDataQueryCondition* pCondition) { m_pQueryCondition = pCondition;};
	int	GetFindCharCount(CString msg, char find_char);
private:
	void SortChannelListCtrl();
	static int CALLBACK ChannelCompareProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATAQUERYDLG_H__90DAB181_E17D_11D4_88DF_006008CEDA07__INCLUDED_)
