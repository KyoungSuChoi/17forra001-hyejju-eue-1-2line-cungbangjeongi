// FailMsgDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSMon.h"
#include "FailMsgDlg.h"
#include "UnitComLogDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFailMsgDlg dialog


CFailMsgDlg::CFailMsgDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFailMsgDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CFailMsgDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_errCode = 0;
	m_pJpeg = NULL;
//	m_strDataBase = ".\\DataBase\\" + FORM_SET_DATABASE_NAME;
}


void CFailMsgDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFailMsgDlg)
	DDX_Control(pDX, IDC_LIST3, m_ctrlEmgList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFailMsgDlg, CDialog)
	//{{AFX_MSG_MAP(CFailMsgDlg)
	ON_BN_CLICKED(IDC_BUTTON_ALL, OnButtonAll)
	ON_BN_CLICKED(IDC_BUTTON_LOG, OnButtonLog)
	ON_BN_CLICKED(IDC_BUTTON2, OnButton2)
	ON_WM_PAINT()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFailMsgDlg message handlers

void CFailMsgDlg::SetModuleName(CString strModuleName, CString ipAddress)
{
	m_strModuleName = strModuleName;
	m_strIpAddress = ipAddress;
}

BOOL CFailMsgDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CString msg, descript;
	msg = m_strModuleName + ::GetStringTable(IDS_LABEL_STATUS);
	GetDlgItem(IDC_MD_NAME_STATIC)->SetWindowText(msg);

	if(GetFailMsg(msg, descript) == FALSE)
	{
		GetDlgItem(IDC_STATE_STATIC)->SetWindowText(::GetStringTable(IDS_TEXT_INO_NOT_FOUND));
		GetDlgItem(IDC_DESCRIPT_STATIC)->SetWindowText(::GetStringTable(IDS_TEXT_INO_NOT_FOUND));
	}
	else
	{
		GetDlgItem(IDC_STATE_STATIC)->SetWindowText(msg);
		GetDlgItem(IDC_DESCRIPT_STATIC)->SetWindowText(descript);
	}

	LoadEmgList();

	CString strTemp;

	strTemp.Format("%s\\Img\\T%03d.jpg", AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"CTSMon"), m_errCode);
	CFileFind aFind;
	if(aFind.FindFile(strTemp))
	{
		m_pJpeg = new stingray::foundation::SECJpeg;
		if (m_pJpeg->LoadImage(strTemp) == FALSE)
		{
			delete m_pJpeg;
			m_pJpeg = NULL;
		}
		else
		{
			m_sizeImg.cx = (int)m_pJpeg->m_dwWidth; 
			m_sizeImg.cy = (int)m_pJpeg->m_dwHeight;
		}
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CFailMsgDlg::SetErrorCode(BYTE code, CString strDataBase)
{
	m_errCode = code;
	m_strDataBase = strDataBase;
}


BOOL CFailMsgDlg::GetFailMsg(CString &strMsg, CString &strDescript)
{
	CDaoDatabase  db;

	if(m_strDataBase.IsEmpty())		return FALSE;
	db.Open(m_strDataBase);
	

	CString strSQL;
	strSQL.Format("SELECT Message, Description FROM EmgCode WHERE Code = %d", m_errCode);
	
	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	
	if(!rs.IsBOF() && !rs.IsEOF())
	{
		strMsg.Empty();
		strDescript.Empty();

		data = rs.GetFieldValue(0);
		if(VT_NULL != data.vt)		strMsg = data.pbVal;
		data = rs.GetFieldValue(1);
		if(VT_NULL != data.vt)		strDescript = data.pbVal;
	}

	rs.Close();
	db.Close();
	return TRUE;
}

BOOL CFailMsgDlg::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if( pMsg->message == WM_KEYDOWN )
	{
		switch( pMsg->wParam )
		{
		case VK_RETURN:
		case VK_ESCAPE:
			return TRUE;
		}
	}
	
	return CDialog::PreTranslateMessage(pMsg);
}

void CFailMsgDlg::LoadEmgList()
{
	COleDateTime oleCurDate(COleDateTime::GetCurrentTime());
	CString strTemp;
//	TCHAR szCurDir[MAX_PATH];

	m_ctrlEmgList.ResetContent();
	strTemp.Format("%s\\Log\\%s", AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"CTSMon"), oleCurDate.Format("\\%Y%m_EMG.csv"));

	CStdioFile logFile;
	if(logFile.Open(strTemp, CFile::modeRead | CFile::shareDenyNone) == FALSE)
	{
		return;
	}

	while(logFile.ReadString(strTemp))
	{
		
		int p1  = strTemp.Find(',', 0);
		if(p1 >= 0)
		{
			int p2  = strTemp.Find(',', p1+1);
			if(p2 >= 0)
			{
				CString strName = strTemp.Mid(p1+1, p2-p1-1);
				if(m_strModuleName == strName)
				{
					strTemp.Delete(p1, p2-p1+1);
					m_ctrlEmgList.AddString(strTemp);
				}
			}
		}
	}

	if(m_ctrlEmgList.GetCount() < 1)
	{
		m_ctrlEmgList.AddString(::GetStringTable(IDS_TEXT_NO_EMG));
	}

	logFile.Close();
}

#include "MainFrm.h"
void CFailMsgDlg::OnButtonAll() 
{
	// TODO: Add your control notification handler code here
	TCHAR szCurDir[MAX_PATH];
	CString strTemp;
	COleDateTime oleCurDate(COleDateTime::GetCurrentTime());

	::GetModuleFileName(AfxGetApp()->m_hInstance, szCurDir, MAX_PATH);
	strTemp.Format("%s\\Log\\%s", CString(szCurDir).Left(CString(szCurDir).ReverseFind('\\')), oleCurDate.Format("\\%Y%m_EMG.csv"));
	
	CString strMsg = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"Excel Path", "");
	((CCTSMonDoc *)(((CMainFrame *)AfxGetMainWnd())->GetActiveDocument()))->ExecuteProgram(strMsg, strTemp);

}

void CFailMsgDlg::OnButtonLog() 
{
	// TODO: Add your control notification handler code here
	STARTUPINFO	stStartUpInfo;
	PROCESS_INFORMATION	ProcessInfo;
	ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
	ZeroMemory(&ProcessInfo, sizeof(PROCESS_INFORMATION));
	
	stStartUpInfo.cb = sizeof(STARTUPINFO);
	stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
	stStartUpInfo.wShowWindow = SW_SHOWNORMAL;

	char szWinDir[128];
	if(GetWindowsDirectory(szWinDir, 127) <=0 )		return;

	COleDateTime oleCurDate(COleDateTime::GetCurrentTime());
	CString str;
	str.Format("%s\\Log\\%s", AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"CTSMon"), oleCurDate.Format("\\%Y%m%d.log"));
	CString strTemp;
	strTemp.Format("%s\\Notepad.exe %s", szWinDir,str);

	BOOL bFlag = CreateProcess(NULL, (LPSTR)(LPCTSTR)strTemp, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &stStartUpInfo, &ProcessInfo);
	if(bFlag == FALSE)
	{
		MessageBox(GetStringTable(IDS_LANG_MSG_ALERT_LOG_FILE_NOTE_FOUND), "Excute Error", MB_OK|MB_ICONSTOP);
	}	
}

void CFailMsgDlg::OnButton2() 
{
	// TODO: Add your control notification handler code here
	CUnitComLogDlg dlg(m_strIpAddress, this);
	dlg.DoModal();
}

void CFailMsgDlg::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	// TODO: Add your message handler code here
	if(m_pJpeg)
	{
		stingray::foundation::SECImage * pImage = m_pJpeg;
		CDC dcBitmap;
//		CBitmap * pOldBitmap;
		//Now draw, etc..

		CRect rect;
		GetDlgItem(IDC_STATIC_PICTURE)->GetWindowRect(rect);
		ScreenToClient(rect);

		CPalette *pOldPalette = NULL;
		if (pImage)
		{
			if (pImage->m_pPalette)
			{
				pOldPalette = dc.SelectPalette(pImage->m_pPalette, TRUE);
			}
 
			// int nLines = ::StretchDIBits(pDC->GetSafeHdc(),
			pImage->StretchDIBits(&dc,
								  rect.left, rect.top, rect.Width(), rect.Height(), 
								  0, 0, (int)pImage->m_dwWidth, (int)pImage->m_dwHeight,
								  pImage->m_lpSrcBits,
								  pImage->m_lpBMI, DIB_RGB_COLORS,
								  SRCCOPY );     
  
			if (pImage->m_pPalette) 
			{
				dc.SelectPalette(pOldPalette, TRUE);
			} 
		} 
	}
	// Do not call CDialog::OnPaint() for painting messages
}

void CFailMsgDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here
	if(m_pJpeg)
	{
		delete m_pJpeg;
		m_pJpeg = NULL;
	}
}
