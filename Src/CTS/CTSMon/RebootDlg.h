#if !defined(AFX_REBOOTDLG_H__A92E7F6B_8D4F_4E9E_850C_052103D27536__INCLUDED_)
#define AFX_REBOOTDLG_H__A92E7F6B_8D4F_4E9E_850C_052103D27536__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RebootDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CRebootDlg dialog
#include "ClientSocket.h"

#define LOG_FILE_NAME	"rxdata.log"


class CRebootDlg : public CDialog
{
// Construction
public:
	CString m_strFileName;
	CString m_strCurDir;
	int m_nPhase;
	BOOL m_timeOutFlag;
	CString m_strPassword;
	CString m_strDir;
	CString m_strLoginID;
	void CloseSocket();
	void SetModuleName(CString strName, CString strIPAddress);
	CRebootDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CRebootDlg)
	enum { IDD = IDD_REBOOT_DLG };
	CProgressCtrl	m_wndProgress;
	CLabel	m_ctrlWaringLabel;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRebootDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CString m_strIPAddress;
	int SendData(CString strData);
	BYTE ParsingData(CString msg);
	CString m_strLineData;
	void ParsingMessage(LPCSTR pText);
	unsigned char m_bBuf[ioBuffSize];
	void MessageReceived(LPCSTR pText);
	void DispatchMessage(CString strText);
	CString m_strResp;
	void ArrangeReply(CString strOption);
	void RespondToOptions();
	CStringList m_ListOptions;
	CString m_strNormalText;
	void ProcessOptions();
	CString m_strLine;
	BOOL GetLine( unsigned char * bytes, int nBytes);
	CClientSocket *m_pClientSock;
	CString m_strModule;
	// Generated message map functions
	//{{AFX_MSG(CRebootDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnReboot();
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	afx_msg LONG OnReceive(UINT,LONG);
	afx_msg LONG OnServerConnected(UINT,LONG);
	afx_msg LONG OnServerDisConnected(UINT,LONG);

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_REBOOTDLG_H__A92E7F6B_8D4F_4E9E_850C_052103D27536__INCLUDED_)
