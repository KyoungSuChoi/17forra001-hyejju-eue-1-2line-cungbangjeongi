#if !defined(AFX_MODELNAMEDLG_H__72C82C4E_BD52_463E_A5CF_B7A62BC9E788__INCLUDED_)
#define AFX_MODELNAMEDLG_H__72C82C4E_BD52_463E_A5CF_B7A62BC9E788__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ModelNameDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CModelNameDlg dialog

class CModelNameDlg : public CDialog
{
// Construction
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();
	CModelNameDlg(BOOL bInsertMode = FALSE, CWnd* pParent = NULL);   // standard constructor
	virtual ~CModelNameDlg();

	BOOL m_bInsertMode;
	CString GetName();
	CString GetDescript();
	void SetName(CString strName, CString strDescript = "");
	

// Dialog Data
	//{{AFX_DATA(CModelNameDlg)
	enum { IDD = IDD_MODEL_NAME_DLG };
	CComboBox	m_ctrlCombo;
	CEdit	m_ctrlName;
	CString	m_strName;
	CString	m_strDescript;
	CString	m_strUserID;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CModelNameDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CModelNameDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnChangeModelNameEdit();
	afx_msg void OnChangeDescripEdit();
	afx_msg void OnSelchangeModelCombo();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MODELNAMEDLG_H__72C82C4E_BD52_463E_A5CF_B7A62BC9E788__INCLUDED_)
