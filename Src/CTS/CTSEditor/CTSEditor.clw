; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CCTSEditorApp
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "ctseditor.h"
LastPage=0

ClassCount=27
Class1=CChangeUserInfoDlg
Class2=CCommonInputDlg
Class3=CCTSEditorApp
Class4=CAboutDlg
Class5=CCTSEditorDoc
Class6=CCTSEditorView
Class7=CMyDateTimeCtrl
Class8=CEditorSetDlg
Class9=CEndConditionDlg
Class10=CDCRScopeWnd
Class11=CGridComboBox
Class12=CLoginDlg
Class13=CMainFrame
Class14=CModelInfoDlg
Class15=CModelNameDlg
Class16=CModelSelDlg
Class17=CMyGridWnd
Class18=CNewModelInfoDlg
Class19=CProcDataRecordSet
Class20=CProgressDlg
Class21=CRealEditControl
Class22=CSelectSimulDataDlg
Class23=CTestDlg
Class24=CTestNameDlg
Class25=CTestTypeRecordSet
Class26=CUserAdminDlg
Class27=CXPButton

ResourceCount=19
Resource1=IDD_MODEL_INFO_DIALOG
Resource2=IDD_SAVE_CONFIG_DIALOG
Resource3=IDD_NEW_MODEL_DIALOG
Resource4=IDD_USER_ADMIN_DLG
Resource5=IDD_END_COND_FORM_DLG
Resource6=IDR_MAINFRAME (English (U.S.))
Resource7=IDR_CONTEXT
Resource8=IDD_ABOUTBOX
Resource9=IDD_DELTA_GRADE_DIALOG
Resource10=IDD_PROG_DLG
Resource11=IDD_TEST_NAME_DLG
Resource12=IDD_MODEL_DIALOG
Resource13=IDD_EDITOR_SET_DLG
Resource14=IDD_CTSEditor_FORM
Resource15=IDD_LOGON_DLG
Resource16=IDD_USER_SETTING
Resource17=IDD_SIMUL_DATA_DLG
Resource18=IDD_MODEL_NAME_DLG
Resource19=IDR_MAINFRAME

[CLS:CChangeUserInfoDlg]
Type=0
BaseClass=CDialog
HeaderFile=ChangeUserInfoDlg.h
ImplementationFile=ChangeUserInfoDlg.cpp

[CLS:CCommonInputDlg]
Type=0
BaseClass=CDialog
HeaderFile=CommonInputDlg.h
ImplementationFile=CommonInputDlg.cpp

[CLS:CCTSEditorApp]
Type=0
BaseClass=CWinApp
HeaderFile=CTSEditor.h
ImplementationFile=CTSEditor.cpp
LastObject=CCTSEditorApp

[CLS:CAboutDlg]
Type=0
BaseClass=CDialog
HeaderFile=CTSEditor.cpp
ImplementationFile=CTSEditor.cpp
LastObject=CAboutDlg

[CLS:CCTSEditorDoc]
Type=0
BaseClass=CDocument
HeaderFile=CTSEditorDoc.h
ImplementationFile=CTSEditorDoc.cpp
LastObject=CCTSEditorDoc

[CLS:CCTSEditorView]
Type=0
BaseClass=CFormView
HeaderFile=CTSEditorView.h
ImplementationFile=CTSEditorView.cpp
LastObject=CCTSEditorView
Filter=D
VirtualFilter=VWC

[CLS:CMyDateTimeCtrl]
Type=0
BaseClass=SECDateTimeCtrl
HeaderFile=DateTimeCtrl.h
ImplementationFile=DateTimeCtrl.cpp

[CLS:CEditorSetDlg]
Type=0
BaseClass=CDialog
HeaderFile=EditorSetDlg.h
ImplementationFile=EditorSetDlg.cpp

[CLS:CEndConditionDlg]
Type=0
BaseClass=CDialog
HeaderFile=EndConditionDlg.h
ImplementationFile=EndConditionDlg.cpp

[CLS:CDCRScopeWnd]
Type=0
BaseClass=CStatic
HeaderFile=GraphWnd.h
ImplementationFile=GraphWnd.cpp

[CLS:CGridComboBox]
Type=0
BaseClass=CGXComboBox
HeaderFile=GridComboBox.h
ImplementationFile=GridComboBox.cpp

[CLS:CLoginDlg]
Type=0
BaseClass=CDialog
HeaderFile=LoginDlg.h
ImplementationFile=LoginDlg.cpp

[CLS:CMainFrame]
Type=0
BaseClass=CFrameWnd
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
LastObject=CMainFrame

[CLS:CModelInfoDlg]
Type=0
BaseClass=CDialog
HeaderFile=ModelInfoDlg.h
ImplementationFile=ModelInfoDlg.cpp

[CLS:CModelNameDlg]
Type=0
BaseClass=CDialog
HeaderFile=ModelNameDlg.h
ImplementationFile=ModelNameDlg.cpp

[CLS:CModelSelDlg]
Type=0
BaseClass=CDialog
HeaderFile=ModelSelDlg.h
ImplementationFile=ModelSelDlg.cpp

[CLS:CMyGridWnd]
Type=0
BaseClass=CGXGridWnd
HeaderFile=MyGridWnd.h
ImplementationFile=MyGridWnd.cpp

[CLS:CNewModelInfoDlg]
Type=0
BaseClass=CDialog
HeaderFile=NewModelInfoDlg.h
ImplementationFile=NewModelInfoDlg.cpp

[CLS:CProcDataRecordSet]
Type=0
HeaderFile=ProcDataRecordSet.h
ImplementationFile=ProcDataRecordSet.cpp

[CLS:CProgressDlg]
Type=0
BaseClass=CDialog
HeaderFile=ProgressDlg.h
ImplementationFile=ProgressDlg.cpp

[CLS:CRealEditControl]
Type=0
BaseClass=CGXEditControl
HeaderFile=RealEditCtrl.h
ImplementationFile=RealEditCtrl.Cpp

[CLS:CSelectSimulDataDlg]
Type=0
BaseClass=CDialog
HeaderFile=SelectSimulDataDlg.h
ImplementationFile=SelectSimulDataDlg.cpp

[CLS:CTestDlg]
Type=0
BaseClass=CDialog
HeaderFile=TestDlg.h
ImplementationFile=TestDlg.cpp

[CLS:CTestNameDlg]
Type=0
BaseClass=CDialog
HeaderFile=TestNameDlg.h
ImplementationFile=TestNameDlg.cpp

[CLS:CTestTypeRecordSet]
Type=0
HeaderFile=TestTypeRecordSet.h
ImplementationFile=TestTypeRecordSet.cpp

[CLS:CUserAdminDlg]
Type=0
BaseClass=CDialog
HeaderFile=UserAdminDlg.h
ImplementationFile=UserAdminDlg.cpp

[CLS:CXPButton]
Type=0
BaseClass=CButton
HeaderFile=XPButton.h
ImplementationFile=XPButton.cpp

[DB:CProcDataRecordSet]
DB=1

[DB:CTestTypeRecordSet]
DB=1

[DLG:IDD_USER_SETTING]
Type=1
Class=CChangeUserInfoDlg
ControlCount=29
Control1=IDC_USER_ID,edit,1350631552
Control2=IDC_USER_PWD,edit,1350631584
Control3=IDC_USER_PWD_CONFIRM,edit,1350631584
Control4=IDC_USER_NAME,edit,1350631552
Control5=IDC_USER_DESCRIPTION,edit,1350631552
Control6=IDC_AUTO_LOGOUT_FLAG,button,1476460547
Control7=IDC_LOGOUT_TIME,edit,1484849282
Control8=IDC_SPIN1,msctls_updown32,1476460594
Control9=IDC_LIST1,SysListView32,1350631424
Control10=IDC_ADD_PERMISSION,button,1476460544
Control11=IDC_REMOVE_PERMISSION,button,1476460544
Control12=IDC_LIST2,SysListView32,1350631424
Control13=IDOK,button,1342242817
Control14=IDCANCEL,button,1342242816
Control15=IDC_STATIC,static,1342308352
Control16=IDC_STATIC,static,1342308352
Control17=IDC_STATIC,static,1342308352
Control18=IDC_STATIC,static,1342308866
Control19=IDC_STATIC,static,1342308352
Control20=IDC_STATIC,button,1342177287
Control21=IDC_STATIC,static,1342308352
Control22=IDC_STATIC,static,1342308352
Control23=IDC_STATIC,static,1342177296
Control24=IDC_STATIC,static,1342308352
Control25=IDC_STATIC,static,1342179331
Control26=IDC_USER_ADMIN,button,1342242816
Control27=IDC_USER_WORK_ADMIN,button,1342242816
Control28=IDC_USER_WORKER,button,1342242816
Control29=IDC_USER_GUEST,button,1342242816

[DLG:IDD_SAVE_CONFIG_DIALOG]
Type=1
Class=CCommonInputDlg
ControlCount=7
Control1=IDCANCEL,button,1342242816
Control2=IDC_TITLE_STATIC,static,1342308352
Control3=IDC_ALL_STEP_BUTTON,button,1342242816
Control4=IDC_CHARGE_BUTTON,button,1342242816
Control5=IDC_BUTTON3,button,1342242816
Control6=IDC_STATIC,static,1342308352
Control7=IDC_VALUE_EDIT,edit,1350631554

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=6
Control1=IDC_STATIC,static,1342177283
Control2=IDOK,button,1342373889
Control3=IDC_STATIC,static,1342308480
Control4=IDC_STATIC,static,1342308352
Control5=IDC_TYPE_STATIC,static,1342308352
Control6=IDC_STATIC,static,1342177294

[DLG:IDD_EDITOR_SET_DLG]
Type=1
Class=CEditorSetDlg
ControlCount=37
Control1=IDC_USE_LOGIN_CHECK,button,1342242819
Control2=IDC_CC_V_LOW_EDIT,edit,1350631554
Control3=IDC_CC_V_HIGH_EDIT,edit,1350631554
Control4=IDC_V_H_CHECK,button,1342242819
Control5=IDC_V_HIGH_EDIT,edit,1350631554
Control6=IDC_V_L_CHECK,button,1342242819
Control7=IDC_V_LOW_EDIT,edit,1350631554
Control8=IDC_I_H_CHECK,button,1342242819
Control9=IDC_I_HIGH_EDIT,edit,1350631554
Control10=IDC_I_L_CHECK,button,1342242819
Control11=IDC_I_LOW_EDIT,edit,1350631554
Control12=IDC_C_H_CHECK,button,1342242819
Control13=IDC_C_HIGH_EDIT,edit,1350631554
Control14=IDC_C_L_CHECK,button,1342242819
Control15=IDC_C_LOW_EDIT,edit,1350631554
Control16=IDOK,button,1342242817
Control17=IDCANCEL,button,1342242816
Control18=IDC_C_LOW_LIMIT_UNIT_STATIC,static,1342308352
Control19=IDC_C_HIGH_LIMIT_UNIT_STATIC,static,1342308352
Control20=IDC_I_LOW_LIMIT_UNIT_STATIC,static,1342308352
Control21=IDC_V_LOW_LIMIT_UNIT_STATIC,static,1342308352
Control22=IDC_STATIC,static,1342308352
Control23=IDC_STATIC,button,1342177287
Control24=IDC_I_HIGH_LIMIT_UNIT_STATIC,static,1342308352
Control25=IDC_V_HIGH_LIMIT_UNIT_STATIC,static,1342308352
Control26=IDC_STATIC,static,1342308352
Control27=IDC_STATIC,button,1342177287
Control28=IDC_V_LOW_REF_UNIT_STATIC,static,1342308352
Control29=IDC_V_HIGH_REF_UNIT_STATIC,static,1342308352
Control30=IDC_STATIC,button,1073741831
Control31=IDC_STATIC,static,1073872896
Control32=IDC_STATIC,static,1073872896
Control33=IDC_STATIC,static,1073872896
Control34=IDC_V_UNIT_COMBO,combobox,1075904515
Control35=IDC_I_UNIT_COMBO,combobox,1075904515
Control36=IDC_C_UNIT_COMBO,combobox,1075904515
Control37=IDC_STATIC,static,1342308352

[DLG:IDD_END_COND_FORM_DLG]
Type=1
Class=CEndConditionDlg
ControlCount=42
Control1=IDC_STATIC,static,1342308866
Control2=IDC_STATIC,static,1342308866
Control3=IDC_STATIC,static,1342308866
Control4=IDC_DELTA_VP_STATIC,static,1342308866
Control5=IDC_STATIC,static,1073873410
Control6=IDC_STEP_NO,static,1342308864
Control7=IDC_END_VTG,edit,1484849154
Control8=IDC_END_CAP,edit,1484849154
Control9=IDC_END_DELTAV,edit,1484849154
Control10=IDC_END_DELTAI,edit,1216413698
Control11=IDC_STATIC,static,1073741840
Control12=IDC_END_VTG_UNIT_STATIC,static,1342308352
Control13=IDC_END_CAPA_UNIT_STATIC,static,1342308352
Control14=IDC_END_DELTA_V_UNIT_STATIC,static,1342308352
Control15=IDC_END_CRT_UNIT_STATIC2,static,1073872896
Control16=IDC_CHECK1,button,1073807363
Control17=IDC_SOC_RATE_EDIT,edit,1216413826
Control18=IDC_STATIC,static,1073872896
Control19=IDC_CAPA_STEP_COMBO,combobox,1075904770
Control20=IDC_SOC_STATIC,static,1073872896
Control21=IDC_DATETIMEPICKER1,SysDateTimePick32,1476460585
Control22=IDC_END_DAY_EDIT,edit,1484849282
Control23=IDC_STATIC,static,1342308352
Control24=IDC_END_MSEC_EDIT,edit,1350633602
Control25=IDC_END_TIME_MSEC_SPIN,msctls_updown32,1342177312
Control26=IDC_STATIC_MSEC,static,1342308352
Control27=IDC_STATIC,static,1073873410
Control28=IDC_EDIT_GOTO,edit,1216413826
Control29=IDC_STATIC,static,1073873410
Control30=IDC_EDIT_CYCLE,edit,1216413826
Control31=IDC_STATIC,static,1073872896
Control32=IDC_STATIC,static,1073872896
Control33=IDC_GOTO_COMBO,combobox,1210122243
Control34=IDC_END_CRT_STATIC,static,1342308866
Control35=IDC_END_CRT,edit,1484849154
Control36=IDC_END_CRT_UNIT_STATIC,static,1342308352
Control37=IDC_END_WATTHOUR,edit,1216413698
Control38=IDC_STATIC,static,1073873410
Control39=IDC_END_WATTHOUR_UNIT_STATIC,static,1073872896
Control40=IDC_STATIC,static,1342308354
Control41=IDC_EDIT_END_TEMP,edit,1484849282
Control42=IDC_STATIC,static,1342308352

[DLG:IDD_LOGON_DLG]
Type=1
Class=CLoginDlg
ControlCount=10
Control1=IDC_LOGINID,edit,1350631552
Control2=IDC_PASSWORD,edit,1350631584
Control3=IDC_REMEMBER,button,1342242819
Control4=IDOK,button,1342242817
Control5=IDCANCEL,button,1342242816
Control6=IDC_STATIC,static,1342308865
Control7=IDC_STATIC,static,1342308865
Control8=IDC_STATIC,button,1342177287
Control9=IDC_STATIC,static,1342177296
Control10=IDC_STATIC,static,1342177294

[DLG:IDD_MODEL_INFO_DIALOG]
Type=1
Class=CModelInfoDlg
ControlCount=7
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_MODEL_LIST_GRID,GXWND,1353777152
Control4=IDC_BUTTON_NEW,button,1342242816
Control5=IDC_BUTTON_DELETE,button,1342242816
Control6=IDC_BUTTON_EDIT,button,1342242816
Control7=IDC_NUM_STATIC,static,1342312961

[DLG:IDD_MODEL_NAME_DLG]
Type=1
Class=CModelNameDlg
ControlCount=11
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308352
Control4=IDC_MODEL_NAME_EDIT,edit,1350631552
Control5=IDC_STATIC,static,1342308866
Control6=IDC_DESC_STATIC,static,1342308866
Control7=IDC_DESCRIP_EDIT,edit,1350631552
Control8=IDC_INSERT_STATIC,static,1342308352
Control9=IDC_STATIC,static,1342308866
Control10=IDC_EDIT_ID,edit,1350631552
Control11=IDC_MODEL_COMBO,combobox,1075904515

[DLG:IDD_MODEL_DIALOG]
Type=1
Class=CModelSelDlg
ControlCount=13
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_MODEL_GRID_CUSTOM,GXWND,1353777152
Control4=IDC_BUTTON_NEW,button,1342242827
Control5=IDC_BUTTON_DELETE,button,1342242827
Control6=IDC_BUTTON_EDIT,button,1342242827
Control7=IDC_BUTTON_COPY,button,1342242827
Control8=IDC_BUTTON_SAVE,button,1208025099
Control9=IDC_STATIC,static,1342308352
Control10=IDC_STATIC,static,1342177296
Control11=IDC_SEL_STATIC,static,1342312960
Control12=IDC_NUM_STATIC,static,1342312961
Control13=IDC_STATIC,static,1342177294

[DLG:IDD_NEW_MODEL_DIALOG]
Type=1
Class=CNewModelInfoDlg
ControlCount=24
Control1=IDC_STATIC,static,1342308352
Control2=IDC_STATIC,static,1342308866
Control3=IDC_NAME_EDIT,edit,1350631552
Control4=IDC_STATIC,static,1342308866
Control5=IDC_DESC_EDIT,edit,1350631552
Control6=IDC_STATIC,static,1342308866
Control7=IDC_USERID_EDIT,edit,1350631552
Control8=IDC_STATIC,static,1342177296
Control9=IDC_STATIC,static,1342308866
Control10=IDC_CAP_EDIT,edit,1350631554
Control11=IDC_CAP_UNIT_STATIC,static,1342308352
Control12=IDC_STATIC,static,1342308866
Control13=IDC_W_EDIT,edit,1350631554
Control14=IDC_STATIC,static,1342308352
Control15=IDC_STATIC,static,1342308866
Control16=IDC_H_EDIT,edit,1350631554
Control17=IDC_STATIC,static,1342308352
Control18=IDC_STATIC,static,1342308866
Control19=IDC_D_EDIT,edit,1350631554
Control20=IDC_STATIC,static,1342308352
Control21=IDOK,button,1342242817
Control22=IDCANCEL,button,1342242816
Control23=IDC_STATIC,static,1342308352
Control24=IDC_BARCODE_EDIT,edit,1350631552

[DLG:IDD_SIMUL_DATA_DLG]
Type=1
Class=CSelectSimulDataDlg
ControlCount=12
Control1=IDOK,button,1476460545
Control2=IDCANCEL,button,1342242816
Control3=IDC_EDIT_DATA_TITLE,edit,1350631552
Control4=IDC_BUTTON_DIR,button,1342242816
Control5=IDC_STATIC,button,1342177287
Control6=IDC_SIMUL_GRAPH,static,1342308865
Control7=IDC_STATIC,static,1342308352
Control8=IDC_BUTTON_OPEN_EXCEL,button,1342242816
Control9=IDC_STATIC,static,1342308352
Control10=IDC_STATIC,static,1342308352
Control11=IDC_STATIC_END_TIME_CYCLE,static,1342308352
Control12=IDC_CHECK1,button,1342242819

[DLG:IDD_TEST_NAME_DLG]
Type=1
Class=CTestNameDlg
ControlCount=15
Control1=IDC_MODEL_SEL_COMBO,combobox,1478557699
Control2=IDC_TEST_NAME_EDIT,edit,1350631552
Control3=IDC_PROC_TYPE_COMBO,combobox,1344339971
Control4=IDC_TEST_DESCRIP_EDIT,edit,1350631552
Control5=IDC_TEST_CREATOR_EDIT,edit,1350633600
Control6=IDC_DATE_TIME_STATIC,static,1342308352
Control7=IDOK,button,1342242817
Control8=IDCANCEL,button,1342242816
Control9=IDC_STATIC,static,1342177280
Control10=IDC_STATIC,static,1342308866
Control11=IDC_STATIC,static,1342308866
Control12=IDC_STATIC,static,1342308866
Control13=IDC_STATIC,static,1342308866
Control14=IDC_MODEL_STATIC,static,1342308866
Control15=IDC_RESET_PROC_CHECK,button,1476460547

[DLG:IDD_USER_ADMIN_DLG]
Type=1
Class=CUserAdminDlg
ControlCount=6
Control1=IDOK,button,1342242817
Control2=IDC_USER_LIST_GIRD,GXWND,1353777152
Control3=IDC_COUNT_LABEL,static,1342308866
Control4=IDC_ADD_USER,button,1342242816
Control5=IDC_DELETE_USER,button,1342242816
Control6=IDC_USE_LOGIN_CHECK,button,1342242819

[TB:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_FILE_OPEN
Command2=ID_FILE_SAVE
Command3=ID_EXCEL_SAVE
Command4=ID_FILE_PRINT
Command5=ID_OPTION
Command6=ID_EDIT_CUT
Command7=ID_EDIT_COPY
Command8=ID_EDIT_PASTE
Command9=ID_APP_ABOUT
CommandCount=9

[MNU:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_FILE_OPEN
Command2=ID_FILE_SAVE
Command3=ID_EXCEL_SAVE
Command4=ID_FILE_PRINT
Command5=ID_APP_EXIT
Command6=ID_OPTION
Command7=ID_USER_SETTING
Command8=ID_ADMINISTRATION
Command9=ID_CHANGE_USER
Command10=ID_MODEL_REG
Command11=ID_INSERT_STEP
Command12=ID_DELETE_STEP
Command13=ID_EDIT_UNDO
Command14=ID_EDIT_CUT
Command15=ID_EDIT_COPY
Command16=ID_EDIT_PASTE
Command17=ID_EDIT_INSERT
Command18=ID_VIEW_TOOLBAR
Command19=ID_VIEW_STATUS_BAR
Command20=ID_APP_ABOUT
CommandCount=20

[MNU:IDR_CONTEXT]
Type=1
Class=?
Command1=ID_EDIT_UNDO
Command2=ID_EDIT_CUT
Command3=ID_EDIT_COPY
Command4=ID_EDIT_PASTE
CommandCount=4

[MNU:IDR_MAINFRAME (English (U.S.))]
Type=1
Class=?
Command1=ID_FILE_OPEN
Command2=ID_FILE_SAVE
Command3=ID_EXCEL_SAVE
Command4=ID_FILE_PRINT
Command5=ID_FILE_PRINT_PREVIEW
Command6=ID_FILE_PRINT_SETUP
Command7=ID_APP_EXIT
Command8=ID_OPTION
Command9=ID_EDIT_UNDO
Command10=ID_EDIT_CUT
Command11=ID_EDIT_COPY
Command12=ID_EDIT_PASTE
Command13=ID_VIEW_TOOLBAR
Command14=ID_VIEW_STATUS_BAR
Command15=ID_APP_ABOUT
CommandCount=15

[ACL:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_EDIT_COPY
Command2=ID_EDIT_INSERT
Command3=ID_FILE_NEW
Command4=ID_FILE_OPEN
Command5=ID_FILE_PRINT
Command6=ID_FILE_SAVE
Command7=ID_EDIT_PASTE
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_NEXT_PANE
Command11=ID_PREV_PANE
Command12=ID_EDIT_COPY
Command13=ID_EDIT_PASTE
Command14=ID_EDIT_CUT
Command15=ID_EDIT_REDO
Command16=ID_EDIT_UNDO
CommandCount=16

[DLG:IDD_PROG_DLG]
Type=1
Class=?
ControlCount=3
Control1=IDOK,button,1208025089
Control2=IDCANCEL,button,1208025088
Control3=IDC_PROGRESS1,msctls_progress32,1350565888

[DLG:IDD_DELTA_GRADE_DIALOG]
Type=1
Class=?
ControlCount=4
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_CUSTOM1,,1342242816
Control4=IDC_CUSTOM2,,1342242816

[DLG:IDD_CTSEditor_FORM]
Type=1
Class=CCTSEditorView
ControlCount=143
Control1=IDC_MODEL_NEW,button,1208025099
Control2=IDC_MODEL_DELETE,button,1208025099
Control3=IDC_MODEL_EDIT,button,1208025099
Control4=IDC_MODEL_COPY_BUTTON,button,1208025099
Control5=IDC_MODEL_SAVE,button,1208025099
Control6=IDC_BATTERY_MODEL,GXWND,1082195968
Control7=IDC_TEST_NEW,button,1342242827
Control8=IDC_TEST_DELETE,button,1342242827
Control9=IDC_TEST_EDIT,button,1342242827
Control10=IDC_TEST_SAVE,button,1208025099
Control11=IDC_TESTLIST_GRID,GXWND,1350631424
Control12=IDC_STEP_INSERT,button,1476460555
Control13=IDC_STEP_DELETE,button,1476460555
Control14=IDC_STEP_SAVE,button,1476460555
Control15=IDC_STEP_GRID,GXWND,1350631424
Control16=IDC_PARAM_TAB,SysTabControl32,1342177280
Control17=IDC_VTG_LOW,edit,1484849282
Control18=IDC_VTG_HIGH,edit,1484849282
Control19=IDC_CRT_LOW,edit,1484849282
Control20=IDC_CRT_HIGH,edit,1484849282
Control21=IDC_CAP_LOW,edit,1484849282
Control22=IDC_CAP_HIGH,edit,1484849282
Control23=IDC_IMP_LOW,edit,1484849282
Control24=IDC_IMP_HIGH,edit,1484849282
Control25=IDC_CAP_VTG_LOW,edit,1484849282
Control26=IDC_CAP_VTG_HIGH,edit,1484849282
Control27=IDC_EXT_OPTION_CHECK,button,1476497411
Control28=IDC_COMP_V1,edit,1216413826
Control29=IDC_COMP_V4,edit,1216413826
Control30=IDC_COMP_TIME1,edit,1216413826
Control31=IDC_COMP_V2,edit,1216413826
Control32=IDC_COMP_V5,edit,1216413826
Control33=IDC_COMP_TIME2,edit,1216413826
Control34=IDC_COMP_V3,edit,1216413826
Control35=IDC_COMP_V6,edit,1216413826
Control36=IDC_COMP_TIME3,edit,1216413826
Control37=IDC_COMP_I1,edit,1216413826
Control38=IDC_COMP_I4,edit,1216413826
Control39=IDC_COMP_I_TIME1,edit,1216413826
Control40=IDC_COMP_I2,edit,1216413826
Control41=IDC_COMP_I5,edit,1216413826
Control42=IDC_COMP_I_TIME2,edit,1216413826
Control43=IDC_COMP_I3,edit,1216413826
Control44=IDC_COMP_I6,edit,1216413826
Control45=IDC_COMP_I_TIME3,edit,1216413826
Control46=IDC_GRADE_CHECK,button,1208025091
Control47=IDC_GRADE_GRID,GXWND,1082195968
Control48=IDC_REPORT_VOLTAGE,edit,1216413826
Control49=IDC_REPORT_CURRENT,edit,1216413826
Control50=IDC_PRETEST_OCV,edit,1216413826
Control51=IDC_PRETEST_TRCKLE_I,edit,1484849282
Control52=IDC_PRETEST_DELTA_V,edit,1216413826
Control53=IDC_STATIC,static,1208090624
Control54=IDC_STATIC,static,1208090624
Control55=IDC_STATIC,static,1208090624
Control56=IDC_STATIC,static,1208090624
Control57=IDC_PRETEST_MAXV,edit,1216413826
Control58=IDC_PRETEST_MINV,edit,1216413826
Control59=IDC_PRETEST_FAULT_NO,edit,1216413826
Control60=IDC_PRETEST_MAXI,edit,1216413826
Control61=IDC_CHECK_MAXV_UNIT_STATIC,static,1208090624
Control62=IDC_CHECK_MINV_UNIT_STATIC,static,1208090624
Control63=IDC_STEP_NUM,static,1342308864
Control64=IDC_DELTA_V,edit,1216413826
Control65=IDC_STEP_RANGE,button,1342177287
Control66=IDC_V_STATIC,static,1342308866
Control67=IDC_I_STATIC,static,1342308866
Control68=IDC_CAP_STATIC,static,1342308866
Control69=IDC_IMP_STATIC,static,1342308866
Control70=IDC_COMP1_STATIC,static,1073873410
Control71=IDC_STATIC,static,1208090624
Control72=IDC_STATIC,static,1342308352
Control73=IDC_STATIC,static,1342308352
Control74=IDC_HIGH_STATIC,static,1342308352
Control75=IDC_LOW_STATIC,static,1342308352
Control76=IDC_DELTA_I,edit,1216413826
Control77=IDC_PRETEST_CHECK,button,1476460547
Control78=IDC_MODEL_TOTNUM,static,1208095233
Control79=IDC_TOT_TESTNUM,static,1342312961
Control80=IDC_TOT_STEP_COUNT,static,1342312961
Control81=IDC_STATIC,static,1208090624
Control82=IDC_CHECK_MAX_C_UNIT_STATIC,static,1208090624
Control83=IDC_CHECK_MIN_C_UNIT_STATIC,static,1208090624
Control84=IDC_CHECK_MAX_CRT_UNIT_STATIC,static,1208090624
Control85=IDC_COMP2_STATIC,static,1073872898
Control86=IDC_COMP3_STATIC,static,1073872898
Control87=IDC_STATIC,button,1342177287
Control88=IDC_LOW_VAL_STATIC,static,1073872896
Control89=IDC_COMP_TIME_STATIC,static,1073872896
Control90=IDC_HIGH_VAL_STATIC,static,1073872896
Control91=IDC_INTER_STATIC3,static,1073872896
Control92=IDC_INTER_STATIC4,static,1073872896
Control93=IDC_INTER_STATIC5,static,1073872896
Control94=IDC_BAR_STATIC2,static,1073741840
Control95=IDC_COMP1_STATIC2,static,1073873410
Control96=IDC_COMP2_STATIC2,static,1073872898
Control97=IDC_COMP3_STATIC2,static,1073872898
Control98=IDC_INTER_STATIC6,static,1073872896
Control99=IDC_INTER_STATIC7,static,1073872896
Control100=IDC_INTER_STATIC8,static,1073872896
Control101=IDC_BAR_STATIC3,static,1073741841
Control102=IDC_BAR_STATIC1,static,1073741840
Control103=IDC_RPT_TIME_STATIC,static,1073872896
Control104=IDC_RPT_V_STATIC,static,1073873410
Control105=IDC_RPT_I_STATIC,static,1073873410
Control106=IDC_BAR_STATIC4,static,1342177296
Control107=IDC_CAP_VTG_STATIC,static,1342308866
Control108=IDC_RPT_TEMP_STATIC,static,1073873410
Control109=IDC_REPORT_TEMPERATURE,edit,1216413826
Control110=IDC_DELTA_TIME,SysDateTimePick32,1476460576
Control111=IDC_REPORT_TIME,SysDateTimePick32,1208025129
Control112=IDC_TEST_COPY_BUTTON,button,1342242827
Control113=IDC_LOGO_STATIC,static,1342177550
Control114=IDC_LOAD_TEST,button,1476460555
Control115=IDC_TEST_NAME,static,1342312960
Control116=IDC_SELECT_TEST_LABEL,static,1342308864
Control117=IDC_TEST_LIST_LABEL,static,1342308864
Control118=IDC_STATIC,static,1342308864
Control119=IDC_CHECK_CRT_UNIT_STATIC,static,1342308352
Control120=IDC_REPORT_TIME_MILI,edit,1216424066
Control121=IDC_RPT_MSEC_SPIN,msctls_updown32,1207959584
Control122=IDC_RPT_TUNIT_STATIC,static,1073872896
Control123=IDC_CELL_CHECK_TIME_EDIT,edit,1484849282
Control124=IDC_CHECK_TIME_UNIT_STATIC,static,1342308352
Control125=IDC_STATIC,static,1342177294
Control126=IDC_STATIC,static,1342177294
Control127=IDC_STATIC,static,1342177294
Control128=IDC_WARRING_STATIC,static,1073872896
Control129=IDC_OR_STATIC,static,1073872896
Control130=IDC_ALL_STEP_BUTTON,button,1476460544
Control131=IDC_SAME_ALL_STEP_BUTTON,button,1476460544
Control132=IDC_TEMP_STATIC,static,1342308866
Control133=IDC_TEMP_LOW,edit,1484849282
Control134=IDC_TEMP_HIGH,edit,1484849282
Control135=IDC_BUTTON_MODEL_SEL,button,1342242827
Control136=IDC_LOADED_TEST,static,1342312960
Control137=IDC_STATIC,static,1342177296
Control138=IDC_STATIC,static,1342177294
Control139=IDC_STATIC,static,1342177294
Control140=IDC_STATIC,button,1342177287
Control141=IDC_STATIC,static,1342308352
Control142=IDC_STATIC,static,1342177294
Control143=IDC_BTN_VIEWCHANGE,button,1342242816

