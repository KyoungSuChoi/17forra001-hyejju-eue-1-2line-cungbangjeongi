// CellStateRecordSet.cpp : implementation file
//

#include "stdafx.h"
#include "CellStateRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCellStateRecordSet

IMPLEMENT_DYNAMIC(CCellStateRecordSet, CDaoRecordset)

CCellStateRecordSet::CCellStateRecordSet(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CCellStateRecordSet)
	m_ID = 0;
	m_TraySerial = 0;
	m_CellCode = 0;
	m_CellGrade = _T("");
	m_ChNo = 0;
	m_nFields = 5;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString CCellStateRecordSet::GetDefaultDBName()
{
	return _T(GetDataBaseName());
}

CString CCellStateRecordSet::GetDefaultSQL()
{
	return _T("[CellState]");
}

void CCellStateRecordSet::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CCellStateRecordSet)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Long(pFX, _T("[ID]"), m_ID);
	DFX_Long(pFX, _T("[TraySerial]"), m_TraySerial);
	DFX_Long(pFX, _T("[CellCode]"), m_CellCode);
	DFX_Text(pFX, _T("[CellGrade]"), m_CellGrade);
	DFX_Long(pFX, _T("[ChNo]"), m_ChNo);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CCellStateRecordSet diagnostics

#ifdef _DEBUG
void CCellStateRecordSet::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CCellStateRecordSet::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
