// FormResultFile.h: interface for the CFormResultFile class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FORMRESULTFILE_H__996DBB7F_0DA0_4BDD_A40A_30EAB6598DA6__INCLUDED_)
#define AFX_FORMRESULTFILE_H__996DBB7F_0DA0_4BDD_A40A_30EAB6598DA6__INCLUDED_

#include "..\..\..\INCLUDE\DataForm.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CFormResultFile  
{
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	CFormResultFile();	//constructor
	virtual ~CFormResultFile();

	PNE_RESULT_CELL_SERIAL * GetResultCellSerial();
	CString GetCellSerial(int nChIndex);
	PNE_RESULT_CELL_SERIAL m_ResultCellSerial;
	BOOL m_bLocalGrading; 
	BOOL Regrading();
	BOOL UpdateTestInfo(CString strNewTray, CString strNewLot);
	static BOOL GetLotTrayNo(CString strFile, CString &strLot, CString &strTray);
	
	int GetLastStepNo();
	int GetLastIndexStepNo();

	_MAPPING_DATA * GetSensorMap(int nNo, int nChIndex = 0);
	CString GetModuleName();
	CString GetSensorName(int nSensorChIndex, int nSensorType);
	int ConvertTrayCh2ModuleCh(int nChNo);
	int ConvertModuleCh2TrayCh(int nChNo);
	BOOL ClearResult();
	BOOL ClearConResult();
	BOOL UpdateReadFile();
	BOOL FindMappingSensorCh(int nChTrayNo/*1Base*/, CWordArray &awMappingCh, int nSensorType = 0, int nStepNo = 0 /*1 Base*/);
//	MAPPING_DATA * GetSensorMap(int nSensorIndex=0);
	WORD GetLastTrayChCellCode(int nChIndex);
	WORD GetLastModuleChCellCode(int nChIndex);

	void SetCapacitySum(BOOL bSum);
	STR_STEP_RESULT * GetFirstStepData();
	STR_STEP_RESULT * GetLastStepData();
	BOOL IsLoaded();
	CString GetModelName();
	CString GetTestName();
	int GetTotalCh();
	CString GetTrayNo();
	CString GetLotNo();
	CString GetTestSerialNo();
	CString GetTypeSel();	//20201211 ksj

	int GetGroupIndex();
	int GetModuleID();
	int GetFileVersion();
	STR_STEP_RESULT * GetStepData(UINT nStepIndex);	// 해당 Step 의 데이터
	STR_STEP_RESULT * GetConStepData();	// 해당 Step 의 데이터
	STR_STEP_RESULT * GetStepIndexData(UINT nStepIndex);	// 메모리 공간에 저장된 Step 의 데이터

	STR_CONDITION_HEADER * GetModelData();
	EP_MD_SYSTEM_DATA * GetMDSysData();
	RESULT_FILE_HEADER * GetResultHeader();
	EP_FILE_HEADER * GetFileHeader();
	STR_FILE_EXT_SPACE * GetExtraData();
	CString GetFileName();
	void SetFileName(CString strFileName);
	int ReadFile(CString strFileName = "",  BOOL bSumCapa = FALSE);	// 결과데이터
	int ReadConFile(CString strFileName = "");						// 셀체크 결과데이터
	
	
	int ReadTestConditionFile(FILE *fp, STR_CONDITION *pCondition, char *fileID, char *fileVer);
//	STR_COMMON_STEP ConvertToCommStep(STR_CONDITION *pConditionMain, int nStepIndex);
	int GetStepSize();
	BOOL	m_bSumCapacity;

	CTestCondition * GetTestCondition()	{	return &m_TestCondition;	}
//	STR_CONDITION * GetConditionMain();

	enum {sensorType1 = 0, sensorType2 = 1};

protected:

	BOOL CalculateCapa();
	BOOL RemoveCondition(STR_CONDITION *pCondition);
	

	CString m_strFileName;
	EP_FILE_HEADER		m_fileHaeder;
	RESULT_FILE_HEADER	m_resultFileHeader;
	EP_MD_SYSTEM_DATA	m_sysData;
	STR_FILE_EXT_SPACE	m_extData;
	CPtrArray			m_stepData;		//Array of STR_STEP_RESULT point
	CPtrArray			m_stepConData;

	//2006/4/6 STR_CONDITION => CTestCondition 객체로 변경하여 수정 
//	STR_CONDITION		*m_pConditionMain;
	CTestCondition	m_TestCondition;

//	CArray<Display_Data_List, Display_Data_List> m_dataList;
	_MAPPING_DATA	m_SensorMap1[EP_MAX_SENSOR_CH];
	_MAPPING_DATA	m_SensorMap2[EP_MAX_SENSOR_CH];

};

#endif // !defined(AFX_FORMRESULTFILE_H__996DBB7F_0DA0_4BDD_A40A_30EAB6598DA6__INCLUDED_)
