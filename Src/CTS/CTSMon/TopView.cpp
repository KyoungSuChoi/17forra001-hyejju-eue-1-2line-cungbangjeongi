// TopView.cpp : implementation file
//

#include "stdafx.h"
#include "CTSMon.h"
#include "CTSMonDoc.h"
#include "TopView.h"
#include "RebootDlg.h"
#include "MainFrm.h"
#include "ShowDetailResultDlg.h"
#include "GradeColorDlg.h"

#ifdef _DEBUG

#include "Mmsystem.h"

#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTopView

IMPLEMENT_DYNCREATE(CTopView, CFormView)

CTopView::CTopView()
	: CFormView(CTopView::IDD)
{
	//{{AFX_DATA_INIT(CTopView)
	//}}AFX_DATA_INIT
	m_DispItemNum = EP_STATE;
	m_nCurModuleID = 0;	
	m_nCurGroup = 0;
	m_nCurChannel = 0;
	m_nCurTrayIndex = 0;
	m_nCmdTarget = EP_MODULE_CMD;
	m_pModuleColorFlag = NULL;
	m_pTopColorFlag = NULL;
	m_pRowCol2GpCh = NULL;
	m_nDisplayTimer = 0;

	m_pConfig = NULL;
	m_pImagelist = NULL;
	m_pChStsSmallImage = NULL;
	m_nCurSelStepIndex = 0;
	m_nPrevStep = 0;	
	m_nTopGridColCount = 1;
	LanguageinitMonConfig();
}

CTopView::~CTopView()
{
	if(m_pTopColorFlag)
	{
		delete[] m_pTopColorFlag;
		m_pTopColorFlag = NULL;
	}

	if(m_pModuleColorFlag)
	{
		delete[] m_pModuleColorFlag;
		m_pModuleColorFlag = NULL;
	}

	if(m_pRowCol2GpCh)
	{
		delete[] m_pRowCol2GpCh;
		m_pRowCol2GpCh = NULL;
	}

	if(m_pImagelist)
	{
		delete m_pImagelist;
	}

	if(m_pChStsSmallImage)
	{
		delete m_pChStsSmallImage;
	}
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CTopView::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CTopView"), _T("TEXT_CTopView_CNT"), _T("TEXT_CTopView_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CTopView_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CTopView"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}


void CTopView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTopView)
	//	DDX_Control(pDX, IDC_STATIC_TRAY3, m_ctrlTray3);
	DDX_Control(pDX, IDC_STEP_LIST, m_ctrlStepList);
	DDX_Control(pDX, IDC_DATA_TYPE_COMBO, m_ctrlDiplayData);
	DDX_Control(pDX, IDC_STAGE_NAME, m_CmdTarget);	
	//}}AFX_DATA_MAP
	DDX_Control(pDX, ID_DLG_LABEL_NAME, m_LabelViewName);
	DDX_Control(pDX, IDC_VIEW_SELECT1_BTN, m_Btn_ViewSelect1);
	DDX_Control(pDX, IDC_VIEW_SELECT2_BTN, m_Btn_ViewSelect2);
	DDX_Control(pDX, IDC_RESULTDATA_DISP_BTN, m_Btn_ResultDataDisplay);
	DDX_Control(pDX, IDC_CONTACTDATA_DISP_BTN, m_Btn_ContactResultDataDisplay);
	DDX_Control(pDX, IDC_SEND_PROCESS_BTN, m_Btn_SendProcess);
}

BEGIN_MESSAGE_MAP(CTopView, CFormView)
	ON_WM_CONTEXTMENU()
	//{{AFX_MSG_MAP(CTopView)
	ON_BN_CLICKED(IDC_RADIO2, OnVoltageRadio)
	//ON_BN_CLICKED(IDC_RADIO3, OnCurrentRadio)
	//ON_BN_CLICKED(IDC_RADIO4, OnCapacityRadio)	
	ON_WM_TIMER()
	ON_WM_SIZE()
	ON_UPDATE_COMMAND_UI(ID_FILE_OPEN, OnUpdateFileOpen)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE_AS, OnUpdateFileSaveAs)
	ON_WM_DESTROY()
	ON_UPDATE_COMMAND_UI(ID_CH_CHECK, OnUpdateChCheck)
	ON_UPDATE_COMMAND_UI(ID_CH_RUN, OnUpdateChRun)
	ON_UPDATE_COMMAND_UI(ID_CH_PAUSE, OnUpdateChPause)
	ON_UPDATE_COMMAND_UI(ID_CH_CONTINUE, OnUpdateChContinue)
	ON_UPDATE_COMMAND_UI(ID_CH_STOP, OnUpdateChStop)
	ON_UPDATE_COMMAND_UI(ID_CH_STEPOVER, OnUpdateChStepover)
	ON_COMMAND(ID_TRAYNO_USER_INPUT, OnTraynoUserInput)
	ON_UPDATE_COMMAND_UI(ID_TRAYNO_USER_INPUT, OnUpdateTraynoUserInput)
	ON_COMMAND(ID_ONLINE_MODE, OnOnlineMode)
	ON_UPDATE_COMMAND_UI(ID_ONLINE_MODE, OnUpdateOnlineMode)
	ON_COMMAND(ID_OFFLINE_MODE, OnOfflineMode)
	ON_UPDATE_COMMAND_UI(ID_OFFLINE_MODE, OnUpdateOfflineMode)
	ON_COMMAND(ID_MAINTENANCE_MODE, OnMaintenanceMode)
	ON_UPDATE_COMMAND_UI(ID_MAINTENANCE_MODE, OnUpdateMaintenanceMode)
	ON_COMMAND(ID_CONTROL_MODE, OnControlMode)
	ON_UPDATE_COMMAND_UI(ID_CONTROL_MODE, OnUpdateControlMode)
	ON_COMMAND(ID_REBOOT, OnReboot)	
	ON_UPDATE_COMMAND_UI(ID_GET_PROFILE_DATA, OnUpdateGetProfileData)	
	ON_COMMAND(ID_REF_AD_VALUE_UPDATE, OnRefAdValueUpdate)
	ON_UPDATE_COMMAND_UI(ID_REF_AD_VALUE_UPDATE, OnUpdateRefAdValueUpdate)
	ON_CBN_SELCHANGE(IDC_DATA_TYPE_COMBO, OnSelchangeDataTypeCombo)
	ON_NOTIFY(LVN_GETDISPINFO, IDC_STEP_LIST, OnGetdispinfoStepList)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_STEP_LIST, OnItemchangedStepList)
	ON_NOTIFY(NM_DBLCLK, IDC_STEP_LIST, OnDblclkStepList)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, OnUpdateEditCopy)
	ON_COMMAND(ID_GRADE_COLOR_SET, OnGradeColorSet)
	ON_COMMAND(ID_VIEW_RESULT, OnViewResult)
	ON_UPDATE_COMMAND_UI(ID_VIEW_RESULT, OnUpdateViewResult)
	//}}AFX_MSG_MAP
	ON_COMMAND(ID_FILE_PRINT, 			CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, 	CView::OnFilePrintPreview)
//	ON_WM_ERASEBKGND()
//ON_BN_CLICKED(IDC_VIEW_SELECT3_BTN, &CTopView::OnBnClickedViewSelect3Btn)
ON_BN_CLICKED(IDC_VIEW_SELECT1_BTN, &CTopView::OnBnClickedViewSelect1Btn)
ON_BN_CLICKED(IDC_VIEW_SELECT2_BTN, &CTopView::OnBnClickedViewSelect2Btn)
ON_BN_CLICKED(IDC_RESULTDATA_DISP_BTN, &CTopView::OnBnClickedResultdataDispBtn)
ON_BN_CLICKED(IDC_CONTACTDATA_DISP_BTN, &CTopView::OnBnClickedContactdataDispBtn)
ON_BN_CLICKED(IDC_SEND_PROCESS_BTN, &CTopView::OnBnClickedSendProcessBtn)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTopView diagnostics

#ifdef _DEBUG
void CTopView::AssertValid() const
{
	CFormView::AssertValid();
}

void CTopView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CCTSMonDoc* CTopView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCTSMonDoc)));
	return (CCTSMonDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTopView message handlers
void CTopView::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();

	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetDocument();
	m_pConfig = pDoc->GetTopConfig();						//Top Config

	// 레지스트리 Setting 부분
	m_nTopGridColCount = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "TopGridColCout", 1);
	m_nTopGridRowCount = EP_MAX_CH_PER_MD/m_nTopGridColCount;
	if(EP_MAX_CH_PER_MD % m_nTopGridColCount) 
		m_nTopGridRowCount++;	

	InitLabel();
	InitColorBtn();

	m_pImagelist = new CImageList;	
	ASSERT(m_pImagelist);
	m_pImagelist->Create(IDB_DATA_ITEM, 19, 10, RGB(255,255,255));
	m_ctrlDiplayData.SetImageList(m_pImagelist);

	InitProgressGrid();
	InitSmallChGrid();

	//Table setting
	SetTopGridFont();
	DataUnitChanged();
	GroupSetChanged();		//default Group Grid Display
	DrawChannel();			//Update Channel State Grid				
}

void CTopView::InitLabel()
{
	m_LabelViewName.SetBkColor(RGB_MIDNIGHTBLUE);
	m_LabelViewName.SetTextColor(RGB_WHITE);
	m_LabelViewName.SetFontSize(24);
	m_LabelViewName.SetFontBold(TRUE);
	m_LabelViewName.SetText("Monitoring");

	m_CmdTarget.SetFontSize(20)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND_STAGENAME)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[0]);//"HY헤드라인M"
}

void CTopView::InitColorBtn()
{
	m_Btn_ViewSelect1.SetFontStyle(BTN_FONT_SIZE,1);
	m_Btn_ViewSelect1.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	
	m_Btn_ViewSelect2.SetFontStyle(BTN_FONT_SIZE-2,1);
	m_Btn_ViewSelect2.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	

	m_Btn_SendProcess.SetFontStyle(BTN_FONT_SIZE,1);
	m_Btn_SendProcess.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	

	m_Btn_ResultDataDisplay.SetFontStyle(BTN_FONT_SIZE,1);
	m_Btn_ResultDataDisplay.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	

	m_Btn_ContactResultDataDisplay.SetFontStyle(BTN_FONT_SIZE,1);
	m_Btn_ContactResultDataDisplay.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
}

void CTopView::DrawChannel()
{
	BYTE colorFlag = 0;
	CString  strMsg, strOld, strToolTip, strUnit;

	CCTSMonDoc *pDoc = GetDocument();
	float	fTemp = 0;

	CTestCondition *pProc;
	CStep *pStep = NULL;

	ROWCOL nRow = 0, nCol = 0;
	int nChannelNo = 0;
	int nTotalGroup = EPGetGroupCount(m_nCurModuleID);

	CFormModule *pModule;
	pModule =  pDoc->GetModuleInfo(m_nCurModuleID);
	if(pModule == NULL)		return;

	int nCurStep = pModule->GetCurStepNo();
	pProc = pModule->GetCondition();
	if(pProc != NULL)
	{
//		TRACE("STEP No : %d\r\n", nCurStep);
		pStep = pProc->GetStep(nCurStep-1);		//Step 번호가 1 Base
	}
	
	// step 이 변했으면 자동으로 현재 step을 이동한다.
	if(m_nPrevStep != nCurStep)
	{
		m_nPrevStep = nCurStep;
		m_nCurSelStepIndex = nCurStep-1;
		if(m_nCurSelStepIndex < 0)
		{
			m_nCurSelStepIndex = 0;
		}

		//현재 진행 Step에 맞게 Display 한다.
		for(int item = 0; item < m_ctrlStepList.GetItemCount(); item++)
		{
			if(pModule->IsComplitedStep(item, m_nCurTrayIndex))
			{
				//완료된 data는 파일에서 loading한다.
				m_ctrlStepList.SetItemText(item, 3, ::GetStringTable(IDS_TEXT_END));
			}
			else
			{
				// 작업을 완료한후 결과 파일이 삭제 되었거나 존재 하지 않을 경우
				// 진행중과 대기중으로 표기된다.
				if(nCurStep == item+1)
				{
					//진행중인 data는 현재값을 표시한다.
					m_ctrlStepList.SetItemText(item, 3, ::GetStringTable(IDS_TEXT_RUNNING));

					if( m_ctrlStepList.SetItemState(m_nCurSelStepIndex, LVIS_SELECTED, LVIS_SELECTED) == true )
					{
						TRACE("Set current step item %d\n", m_nCurSelStepIndex);
						CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();
						pMainFrm->m_pOperationView->DrawStepList();
					}
					break;
				}
				else
				{
					//아직 진행하지 않은 step은 현재값을 표시한다.
					m_ctrlStepList.SetItemText(item, 3, ::GetStringTable(IDS_TEXT_STANDBY));					
				}
			}
		}

		m_ctrlStepList.Invalidate();
	}
	
	int nChannelIndex;	
	STR_SAVE_CH_DATA chSaveData;	

	//Small channel grid display
	//////////////////////////////////////////////////////////////////////////
//	CTray *pTray;
	int nCurItem = EP_STATE;
	int item = 0;

	bool bLock = m_wndSmallChGrid.LockUpdate();
	for (INDEX nGroupIndex= 0; nGroupIndex < nTotalGroup; nGroupIndex++)	//Group 수 만큼 
	{
		nChannelNo = pModule->GetChInJig(nGroupIndex);

		//Cell 순서 대로 표시 한다.
		for (INDEX nChannelIndex = 0; nChannelIndex < nChannelNo; nChannelIndex++)				//Display Channel Ch Per Group
		{		
			pModule->GetChannelStepData(chSaveData, nChannelIndex, nGroupIndex, m_nCurSelStepIndex);

			for(item = 0; item < 5; item++)
			{
				if(GetChRowCol(item, nChannelIndex, nRow, nCol) == FALSE)		
					break;

				strMsg = pDoc->GetStateMsg(chSaveData.state, colorFlag);										//Get State Msg
				if( pDoc->ChCodeMsg(chSaveData.channelCode) == _T("00") )
				{
					strOld.Format(" ");
				}
				else
				{
					strOld.Format("%d", chSaveData.channelCode);
				}
				// strOld = pDoc->ChCodeMsg(chSaveData.channelCode);
	
				nCurItem = atoi(m_wndSmallChGrid.GetValueRowCol(1, m_nTopGridColCount+1+(m_nTopGridColCount*item)));

				strToolTip.Format("CH : %d", nChannelIndex+1 );

				m_wndSmallChGrid.SetStyleRange(CGXRange(nRow, nCol), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(strToolTip)));

				switch(nCurItem)
				{
				case SELECT_ITEM_STATE:
					{
						if( chSaveData.channelCode == EP_CODE_CELL_NONE || 
							chSaveData.channelCode == EP_CODE_NONCELL )						
						{
							strMsg.Format("No Cell");							
						}
						else if( chSaveData.channelCode == EP_CODE_CELL_FAIL )
						{
							strMsg.Format(TEXT_LANG[1]);							//"이전공정에러"
						}		
					}		
					break;

				case SELECT_ITEM_VOLTAGE:		//voltage
					strMsg = pDoc->ValueString(chSaveData.fVoltage, EP_VOLTAGE);
					strUnit = pDoc->m_strVUnit;
					break;

				case SELECT_ITEM_CURRENT:		//current
					strMsg = pDoc->ValueString(chSaveData.fCurrent, EP_CURRENT);
					strUnit = pDoc->m_strIUnit;
					break;

				case SELECT_ITEM_CAPACITY:		//capacity
					strMsg = pDoc->ValueString(chSaveData.fCapacity, EP_CAPACITY);
					strUnit = pDoc->m_strCUnit;
					break;

				case SELECT_ITEM_CODE:	//failureCode
					strMsg = strOld;
					break;

				case SELECT_ITEM_IMP:	//failureCode
					strMsg = pDoc->ValueString(chSaveData.fImpedance, EP_IMPEDANCE);
					strUnit = "mOhm";
					break;

				case SELECT_ITEM_STEPTIME:
					strMsg = pDoc->ValueString(chSaveData.fStepTime, EP_STEP_TIME);
					break;

				case SELECT_ITEM_WATT:	//failureCode
					strMsg = pDoc->ValueString(chSaveData.fWatt, EP_WATT);
					strUnit = pDoc->m_strWUnit;
					break;

				case SELECT_ITEM_WATTHOUR:	//failureCode
					strMsg = pDoc->ValueString(chSaveData.fWattHour, EP_WATT_HOUR);
					strUnit = pDoc->m_strWhUnit;
					break;

				case EP_TEMPER:	//failureCode
					strMsg = pDoc->ValueString(chSaveData.fAuxData[0], EP_TEMPER);
					strUnit = "℃";
					break;

				case SELECT_ITEM_CELLSERIAL:
					{
						CTray *pTray = pModule->GetTrayInfo(nGroupIndex);
						strMsg = pTray->GetCellSerial(nChannelIndex);
					}
					break;

				case EP_ALL_DATA:
					{
						strMsg.Format("V:%s I:%s C:%s S:%s",						
							pDoc->ValueString(chSaveData.fVoltage, EP_VOLTAGE), 
							pDoc->ValueString(chSaveData.fCurrent, EP_CURRENT), 
							pDoc->ValueString(chSaveData.fCapacity, EP_CAPACITY),
						pDoc->ChCodeMsg(chSaveData.channelCode) );				
					}
					break;
				}
				
				//Get Flash Setting
				if (!m_pConfig->m_bShowText) 
					strMsg.Empty();

				colorFlag = GetColorIndex(chSaveData, colorFlag, pStep);
				
				if( pDoc->m_bHideNonCellData )
				{
					if( chSaveData.channelCode == EP_CODE_CELL_NONE || 
						chSaveData.channelCode == EP_CODE_NONCELL 
						//||chSaveData.channelCode == EP_CODE_NONCELL_CONTACT		//20210306KSJ
						)
					{
						strMsg.Empty();
						pDoc->GetStateMsg(EP_STATE_IDLE, colorFlag);
					}
				}				
				
				m_wndSmallChGrid.SetStyleRange(CGXRange(nRow, nCol), 
					CGXStyle().SetValue(strMsg).SetTextColor(m_pConfig->m_TStateColor[colorFlag])
					.SetInterior(m_pConfig->m_BStateColor[colorFlag]));
			}
		}

		//1열 배열 표기시 Column Heaer에 Tray 번호 표시 
		strMsg = pModule->GetTrayNo(nGroupIndex);
		if(strMsg.IsEmpty())
		{
			strMsg.Format("TRAY %d", nGroupIndex+1);
		}

		m_wndSmallChGrid.SetValueRange(CGXRange(0, 2), strMsg);	
		
	}
	m_wndSmallChGrid.LockUpdate(bLock);
	m_wndSmallChGrid.Redraw();	
}

void CTopView::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default

	if(nIDEvent == 102 )
	{
		g_bChannelBlink = !g_bChannelBlink;
		DrawChannel();
	}
	else
	{
		ResetCmdFlag(nIDEvent);
	}

	CFormView::OnTimer(nIDEvent);
}

void CTopView::TopConfigChanged() 
{
	// TODO: Add your command handler code here
	if(m_nDisplayTimer)
	{
		StopMonitoring();
		StartMonitoring();
	}
}

void CTopView::OnPrint(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	CString strTmp, strLine;
	strLine = _T("   ");
	for(int i =0; i<90; i++)
		strLine += _T("━");
	strLine += _T("   ");

	CGXData& Header	= m_wndSmallChGrid.GetParam()->GetProperties()->GetDataHeader();
	CGXData& Footer	= m_wndSmallChGrid.GetParam()->GetProperties()->GetDataFooter();

	Header.StoreStyleRowCol(1, 1, CGXStyle().SetValue(""), gxOverride);
	Header.StoreStyleRowCol(1, 2, CGXStyle().SetValue(""), gxOverride);
	Header.StoreStyleRowCol(2, 1, CGXStyle().SetValue(""), gxOverride);
	Header.StoreStyleRowCol(2, 2, 
			CGXStyle().SetValue("Top Monitoring").SetFont(CGXFont().SetSize(14).SetBold(TRUE)), gxOverride);
	Header.StoreStyleRowCol(3, 1, CGXStyle().SetValue(strLine), gxOverride);
	Header.StoreStyleRowCol(3, 2, CGXStyle().SetValue(""), gxOverride);
	Header.StoreStyleRowCol(4, 2, CGXStyle().SetValue(""), gxOverride);

// 	strTmp.Format("%s %s", "Module No :", ::GetModuleName(m_nCurModuleID, m_nCurGroup));// Module %d, Group %d", "Module No :", m_nCurModuleID, m_nCurGroup+1); 	
// 	Header.StoreStyleRowCol(5, 2, CGXStyle().SetValue(strTmp).SetFont(CGXFont().SetSize(11).SetBold(TRUE)), gxOverride);
// 
// 	strTmp.Format("%s State :%s", GetDocument()->m_strGroupName, m_wndModuleGrid.GetValueRowCol(m_nCurGroup+1, 2)); 
// 	Header.StoreStyleRowCol(6, 2, CGXStyle().SetValue(strTmp).SetFont(CGXFont().SetSize(11).SetBold(TRUE)),gxOverride);
// 
// 	strTmp.Format("%s %s", "Battery Model :", m_wndModuleGrid.GetValueRowCol(m_nCurGroup+1, 6)); 
// 	Header.StoreStyleRowCol(7, 2, CGXStyle().SetValue(strTmp).SetFont(CGXFont().SetSize(11).SetBold(TRUE)),gxOverride);
// 
// 	strTmp.Format("%s %s", "Test Name :", m_wndModuleGrid.GetValueRowCol(m_nCurGroup+1, 7)); 
// 	Header.StoreStyleRowCol(8, 2, CGXStyle().SetValue(strTmp).SetFont(CGXFont().SetSize(11).SetBold(TRUE)),gxOverride);

	Footer.StoreStyleRowCol(1, 1, CGXStyle().SetValue(""),gxOverride);
	Footer.StoreStyleRowCol(1, 2, CGXStyle().SetValue("$P/$N"),gxOverride);
	Footer.StoreStyleRowCol(1, 3, CGXStyle().SetValue("$d{%Y/%m/%d %H:%M:%S}"),gxOverride);

	m_wndSmallChGrid.OnGridPrint(pDC, pInfo);

//	CFormView::OnPrint(pDC, pInfo);
}

void CTopView::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	m_wndSmallChGrid.OnGridEndPrinting(pDC, pInfo);
//	CFormView::OnEndPrinting(pDC, pInfo);
}

void CTopView::OnPrepareDC(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	m_wndSmallChGrid.OnGridPrepareDC(pDC, pInfo);
	//CFormView::OnPrepareDC(pDC, pInfo);
}

BOOL CTopView::OnPreparePrinting(CPrintInfo* pInfo) 
{
	// TODO: call DoPreparePrinting to invoke the Print dialog box
	pInfo->SetMaxPage(0xffff);

	pInfo->m_pPD->m_pd.Flags &= ~PD_NOSELECTION;
	pInfo->m_pPD->m_pd.hInstance = AfxGetInstanceHandle();

	// default preparation
	return DoPreparePrinting(pInfo);

//	return CFormView::OnPreparePrinting(pInfo);
}

void CTopView::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	m_wndSmallChGrid.GetParam()->GetProperties()->SetBlackWhite(FALSE);
	m_wndSmallChGrid.GetParam()->GetProperties()->SetMargins(220,10,80,10);
	m_wndSmallChGrid.SetColWidth(1, 1, 30);
	m_wndSmallChGrid.SetColWidth(2, m_nTopGridColCount+1, 40);
	m_wndSmallChGrid.OnGridBeginPrinting(pDC, pInfo);
//	CFormView::OnBeginPrinting(pDC, pInfo);
}

void CTopView::OnSize(UINT nType, int cx, int cy) 
{
	CFormView::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	//	CFormView::ShowScrollBar(SB_VERT,FALSE);

	if(::IsWindow(this->GetSafeHwnd()))
	{
		CFormView::ShowScrollBar(SB_HORZ, FALSE);
		CRect ctrlRect;
		RECT rect;
		::GetClientRect(m_hWnd, &rect);

		int width = rect.right/100;

		if(m_wndSmallChGrid.GetSafeHwnd())
		{
			m_wndSmallChGrid.GetWindowRect(&ctrlRect);
			ScreenToClient(&ctrlRect);
			m_wndSmallChGrid.MoveWindow(ctrlRect.left, ctrlRect.top, rect.right-ctrlRect.left-5, rect.bottom-ctrlRect.top-5, FALSE);
		}

		if(::IsWindow(GetDlgItem(IDC_STATIC_INFO)->GetSafeHwnd()))
		{
			GetDlgItem(IDC_STATIC_INFO)->GetWindowRect(&ctrlRect);
			ScreenToClient(&ctrlRect);
			GetDlgItem(IDC_STATIC_INFO)->MoveWindow(ctrlRect.left, ctrlRect.top, ctrlRect.Width(), rect.bottom - ctrlRect.top-5 , FALSE);
		}

// 		if(::IsWindow(GetDlgItem(IDC_STEP_LIST)->GetSafeHwnd()))
// 		{
// 			GetDlgItem(IDC_STEP_LIST)->GetWindowRect(&ctrlRect);
// 			ScreenToClient(&ctrlRect);
// 			GetDlgItem(IDC_STEP_LIST)->MoveWindow(ctrlRect.left, ctrlRect.top, ctrlRect.Width(), rect.bottom - ctrlRect.top-5 , FALSE);
// 		}

		if(::IsWindow(GetDlgItem(IDC_RANGE_STATIC)->GetSafeHwnd())) //test
		{
			GetDlgItem(IDC_RANGE_STATIC)->GetWindowRect(&ctrlRect);
			ScreenToClient(&ctrlRect);
			GetDlgItem(IDC_RANGE_STATIC)->MoveWindow(ctrlRect.left, ctrlRect.top, ctrlRect.Width(), ctrlRect.Height(), FALSE);
// 			if(rect.right-ctrlRect.left-5 > 1024)
// 			{
// 				GetDlgItem(IDC_RANGE_STATIC)->MoveWindow(ctrlRect.left, ctrlRect.top, rect.right-ctrlRect.left-5, ctrlRect.Height() , FALSE);
// 			}
		}

		if( GetDlgItem(ID_DLG_LABEL_NAME)->GetSafeHwnd() )
		{
			CRect rectGrid;
			CRect rectStageLabel;
			GetDlgItem(ID_DLG_LABEL_NAME)->GetWindowRect(rectGrid);
			ScreenToClient(&rectGrid);
			GetDlgItem(IDC_STAGE_NAME)->GetWindowRect(rectStageLabel);
			ScreenToClient(&rectStageLabel);
			GetDlgItem(ID_DLG_LABEL_NAME)->MoveWindow(rectGrid.left, rectGrid.top, rect.right-rectStageLabel.Width(), rectGrid.Height(), FALSE);
		}
	}
}

void CTopView::OnUpdateFileOpen(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);
}

void CTopView::OnUpdateFileSaveAs(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
//	pCmdUI->Enable(FALSE);

}

//If Module State is Chaneged
void CTopView::UpdateGroupState(int nModuleID, int /*nGroupIndex*/)
{
	if( nModuleID != m_nCurModuleID )
	{	
		// 1. 현재 실행중인 모듈이 아니기에 상태값을 변경하지 않는다.
		return;
	}
	
	DrawStepList();
}

void CTopView::GroupSetChanged(int nColCount)
{
	//Calculate Grid Info	
	if(nColCount >0 && nColCount<= 64)
	{
		m_nTopGridColCount = nColCount;						//Update Top Grid Column Count
	}
	
	WORD nCount = 0, wChinGroup;

	CFormModule *pModule = GetDocument()->GetModuleInfo(m_nCurModuleID);
	if(pModule == NULL)	return;

	//Tray 단위 보기 
	wChinGroup = pModule->GetChInJig(m_nCurTrayIndex);
	
	//Module 단위 보기 
//	wChinGroup = pModule->GetTotalInstallCh();
	nCount = (wChinGroup/m_nTopGridColCount);
	if(wChinGroup%m_nTopGridColCount >0)		nCount++;
	m_nTopGridRowCount = (int)nCount;					//Update Top Gird Row Count
	
	//Make Group And Channel Index Table
	if(m_pRowCol2GpCh)
	{
		delete[] m_pRowCol2GpCh;
		m_pRowCol2GpCh = NULL;
	}
	nCount= m_nTopGridRowCount*m_nTopGridColCount;
//	ASSERT(nCount);
	m_pRowCol2GpCh = new SHORT[nCount];
	memset(m_pRowCol2GpCh, -1, nCount);
	
	nCount = 0;
	WORD nRowIndex = 0, nCellCout = 0;

	//Grid의 특정 위치에 Group 번호와 Channel 번호를 쉽게 찾기 위해 Mapping Table을 생성한다. 

	WORD nTrayType = GetDocument()->m_nViewTrayType;
	WORD j;
	switch (nTrayType)
	{
	case TRAY_TYPE_CUSTOMER:
	case TRAY_TYPE_LG_ROW_COL:	//세로 번호 체계
	case TRAY_TYPE_PB5_ROW_COL:	//세로 번호 체계(각형)
		for( j=0; j<wChinGroup; j++)
		{
			nCellCout = nRowIndex*m_nTopGridColCount+nCount;
			m_pRowCol2GpCh[nCellCout] = j;
			nRowIndex++;
			
			if(nRowIndex >= m_nTopGridRowCount)
			{
				nCount++;
				nRowIndex = 0;
			}
		}
		break;
	case TRAY_TYPE_PB5_COL_ROW:	//가로 번호 체계(각형)
		for( j=0; j<wChinGroup; j++)
		{
			nCellCout = nRowIndex*m_nTopGridColCount+nCount;
			m_pRowCol2GpCh[nCellCout] = j;
			nCount++;
			
			if(nCount >= m_nTopGridColCount)
			{
				nCount=0;
				nRowIndex++;
			}
		}
		break;
	}	

	//모둘에 할당된 지그(Tray) 숫자 가져오기
	int nCnt = pModule->GetTotalJig();
	
	//////////////////////////////////////////////////////////////////////////
	//Draw small channel display grid
	BOOL bLock = m_wndSmallChGrid.LockUpdate();
	m_wndSmallChGrid.SetDefaultRowHeight(26);
	m_wndSmallChGrid.SetRowCount(1);
	m_wndSmallChGrid.SetColCount(1);		//Delete Grid Attribute
	
	UINT nRow = 0, nCol = 0;
	int n = 0;
	for(n = 0; n<nCnt; n++)
	{
		if(nRow < pModule->GetChInJig(m_nCurTrayIndex))
		{
			nRow = pModule->GetChInJig(m_nCurTrayIndex);
		}
	}

	nCol = 6 * m_nTopGridColCount;

	CString strTemp;

	if(m_wndSmallChGrid.GetRowCount() != nRow+1 || m_wndSmallChGrid.GetColCount() != nCol)
	{	
		m_wndSmallChGrid.SetRowCount(m_nTopGridRowCount+1);		//ReDraw 
		m_wndSmallChGrid.SetColCount(nCol);
		m_wndSmallChGrid.SetCoveredCellsRowCol(0, 1, 1, m_nTopGridColCount);

		strTemp.Format("TRAY %d", n);
		m_wndSmallChGrid.SetCoveredCellsRowCol(0, m_nTopGridColCount+1, 0, nCol);
		m_wndSmallChGrid.SetStyleRange(CGXRange(0, m_nTopGridColCount+1), 
											CGXStyle().SetValue(strTemp).SetFont(CGXFont().SetBold(TRUE))
										);

		m_wndSmallChGrid.SetCoveredCellsRowCol(1, m_nTopGridColCount+1, 1, m_nTopGridColCount+1+(m_nTopGridColCount*1-1));
		m_wndSmallChGrid.SetStyleRange(CGXRange(1, m_nTopGridColCount+1),
											CGXStyle().SetControl(GX_IDS_CTRL_ZEROBASED_EX)
													.SetHorizontalAlignment(DT_CENTER)
													.SetDraw3dFrame(gxFrameRaised)
													.SetInterior(GetSysColor(COLOR_3DFACE))
													.SetValue((long)SELECT_ITEM_STATE)
										);

		
		m_wndSmallChGrid.SetCoveredCellsRowCol(1, m_nTopGridColCount+1+(m_nTopGridColCount*1), 1,m_nTopGridColCount+1+(m_nTopGridColCount*2-1));
		m_wndSmallChGrid.SetStyleRange(CGXRange(1, m_nTopGridColCount+1+(m_nTopGridColCount*1)), 
											CGXStyle().SetControl(GX_IDS_CTRL_ZEROBASED_EX)
													.SetHorizontalAlignment(DT_CENTER)
													.SetDraw3dFrame(gxFrameRaised)
													.SetInterior(GetSysColor(COLOR_3DFACE))
													.SetValue((long)SELECT_ITEM_VOLTAGE)
										);

		m_wndSmallChGrid.SetCoveredCellsRowCol(1,m_nTopGridColCount+1+(m_nTopGridColCount*2), 1, m_nTopGridColCount+1+(m_nTopGridColCount*3-1));
		m_wndSmallChGrid.SetStyleRange(CGXRange(1, m_nTopGridColCount+1+(m_nTopGridColCount*2)),		
											CGXStyle().SetControl(GX_IDS_CTRL_ZEROBASED_EX)
													.SetHorizontalAlignment(DT_CENTER)
													.SetDraw3dFrame(gxFrameRaised)
													.SetInterior(GetSysColor(COLOR_3DFACE))
													.SetValue((long)SELECT_ITEM_CURRENT)
										);

		m_wndSmallChGrid.SetCoveredCellsRowCol(1, m_nTopGridColCount+1+(m_nTopGridColCount*3), 1, m_nTopGridColCount+1+(m_nTopGridColCount*4-1));
		m_wndSmallChGrid.SetStyleRange(CGXRange(1, m_nTopGridColCount+1+(m_nTopGridColCount*3)),		
											CGXStyle().SetControl(GX_IDS_CTRL_ZEROBASED_EX)
													.SetHorizontalAlignment(DT_CENTER)
													.SetDraw3dFrame(gxFrameRaised)
													.SetInterior(GetSysColor(COLOR_3DFACE))
													.SetValue((long)SELECT_ITEM_CAPACITY)
										);

		m_wndSmallChGrid.SetCoveredCellsRowCol(1, m_nTopGridColCount+1+(m_nTopGridColCount*4), 1, m_nTopGridColCount+1+(m_nTopGridColCount*5-1));
		m_wndSmallChGrid.SetStyleRange(CGXRange(1, m_nTopGridColCount+1+(m_nTopGridColCount*4)),
													CGXStyle().SetControl(GX_IDS_CTRL_ZEROBASED_EX)
													.SetHorizontalAlignment(DT_CENTER)
													.SetDraw3dFrame(gxFrameRaised)
													.SetInterior(GetSysColor(COLOR_3DFACE))
													.SetValue((long)SELECT_ITEM_CODE)
										);

		COLORREF boldColor = RGB(255, 192, 0);

		m_wndSmallChGrid.SetStyleRange(CGXRange(2, m_nTopGridColCount+1, 2, m_nTopGridColCount+1+(m_nTopGridColCount*5-1)), CGXStyle().SetBorders(gxBorderTop, CGXPen().SetWidth(2).SetColor(boldColor)));
		m_wndSmallChGrid.SetStyleRange(CGXRange(m_nTopGridRowCount+1, m_nTopGridColCount+1, m_nTopGridRowCount+1, m_nTopGridColCount+1+(m_nTopGridColCount*5-1)), CGXStyle().SetBorders(gxBorderBottom, CGXPen().SetWidth(2).SetColor(boldColor)));

		m_wndSmallChGrid.SetStyleRange(CGXRange(2, m_nTopGridColCount+1, m_nTopGridRowCount+1, m_nTopGridColCount+1), CGXStyle().SetBorders(gxBorderLeft, CGXPen().SetWidth(2).SetColor(boldColor)));
		m_wndSmallChGrid.SetStyleRange(CGXRange(2, m_nTopGridColCount+1+(m_nTopGridColCount*5-1), m_nTopGridRowCount+1, m_nTopGridColCount+1+(m_nTopGridColCount*5-1)), CGXStyle().SetBorders(gxBorderRight, CGXPen().SetWidth(2).SetColor(boldColor)));

		m_wndSmallChGrid.SetStyleRange(CGXRange(2, m_nTopGridColCount+1+(m_nTopGridColCount*1), m_nTopGridRowCount+1, m_nTopGridColCount+1+(m_nTopGridColCount*1)), CGXStyle().SetBorders(gxBorderLeft, CGXPen().SetWidth(2).SetColor(boldColor)));

		m_wndSmallChGrid.SetStyleRange(CGXRange(2, m_nTopGridColCount+1+(m_nTopGridColCount*2), m_nTopGridRowCount+1, m_nTopGridColCount+1+(m_nTopGridColCount*2)), CGXStyle().SetBorders(gxBorderLeft, CGXPen().SetWidth(2).SetColor(boldColor)));
		m_wndSmallChGrid.SetStyleRange(CGXRange(2, m_nTopGridColCount+1+(m_nTopGridColCount*3), m_nTopGridRowCount+1, m_nTopGridColCount+1+(m_nTopGridColCount*3)), CGXStyle().SetBorders(gxBorderLeft, CGXPen().SetWidth(2).SetColor(boldColor)));
		m_wndSmallChGrid.SetStyleRange(CGXRange(2, m_nTopGridColCount+1+(m_nTopGridColCount*4), m_nTopGridRowCount+1, m_nTopGridColCount+1+(m_nTopGridColCount*4)), CGXStyle().SetBorders(gxBorderLeft, CGXPen().SetWidth(2).SetColor(boldColor)));
		m_wndSmallChGrid.SetStyleRange(CGXRange(0, 1), CGXStyle().SetValue("CH(/)").SetFont(CGXFont().SetBold(TRUE)));

		int a = 1;
		int b = 1;
		int nChNum = 0;

		for( a=0; a<m_nTopGridColCount; a++ )
		{
			for ( b=1; b<=m_nTopGridRowCount; b++)
			{
				nChNum = b + m_nTopGridRowCount*a;
				m_wndSmallChGrid.SetStyleRange(CGXRange(b+1, a+1), CGXStyle().SetValue(ROWCOL(nChNum)).SetDraw3dFrame(gxFrameRaised).SetEnabled(FALSE));	
			}
		}
	}
	m_wndSmallChGrid.LockUpdate(bLock);
	m_wndSmallChGrid.Redraw();
	//////////////////////////////////////////////////////////////////////////
	DrawStepList();
}

void CTopView::StartMonitoring()
{
	//현재 모듈이 OffLine 상태이면 사용 안함	
	if(((CMainFrame *)AfxGetMainWnd())->CompareCurrentSelectTab(TAB_INDEX_MONITORING) == FALSE )	
	{
		return;
	}
	
	GroupSetChanged();

	if(m_nDisplayTimer)	
		return;

	// 1. default 2s
	// 2. 설정된 값이 있으면 그 값으로 적용
	int nTimer = 2000;
	if(m_pConfig)
	{
		nTimer = m_pConfig->m_ChannelInterval*1000;
	}
	
	m_nDisplayTimer = SetTimer(102, nTimer, NULL);
}

void CTopView::StopMonitoring()
{
	if(m_nDisplayTimer == 0)	
	{
		return;
	}

	KillTimer(m_nDisplayTimer);
	m_nDisplayTimer = 0;
}

void CTopView::OnDestroy() 
{
	CFormView::OnDestroy();
	
	// TODO: Add your message handler code here
	StopMonitoring();
}

//Module 이 접속 되었을때
void CTopView::ModuleConnected(int nModuleID)
{
	if(nModuleID == m_nCurModuleID)							//현재 선택된 Module이면 Grid를 Setting 시키고
	{	
		StartMonitoring();	
	}
}

void CTopView::ModuleDisConnected(int nModuleID)
{
	if(nModuleID == m_nCurModuleID)							//현재 선택된 Module이면 Grid를 Setting 시키고
	{	
		StopMonitoring();	
	}
}

void CTopView::OnUpdateChCheck(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);
}

void CTopView::OnUpdateChRun(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);
	
}

void CTopView::OnUpdateChPause(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);
	
}

void CTopView::OnUpdateChContinue(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);
}

void CTopView::OnUpdateChStop(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);
	
}

void CTopView::OnUpdateChStepover(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);

}

void CTopView::InitProgressGrid()
{
	m_pChStsSmallImage = new CImageList;
	//상태별 표시할 이미지 로딩
	m_pChStsSmallImage->Create(IDB_CELL_STATE_ICON,19,24,RGB(255,255,255));
	m_ctrlStepList.SetImageList(m_pChStsSmallImage, LVSIL_SMALL);

	//
	DWORD style = 	m_ctrlStepList.GetExtendedStyle();
	style |= LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_HEADERDRAGDROP|LVS_EX_SUBITEMIMAGES;
	m_ctrlStepList.SetExtendedStyle(style );//|LVS_EX_TRACKSELECT);

	// Column 삽입
	m_ctrlStepList.InsertColumn(0, "Step", LVCFMT_CENTER, 0);	//center 정렬을 0 column을 사용하지 않음 
	m_ctrlStepList.InsertColumn(1, "Step", LVCFMT_CENTER, 40);	
	m_ctrlStepList.InsertColumn(2, "Type", LVCFMT_LEFT, 100);
	m_ctrlStepList.InsertColumn(3, "Status", LVCFMT_LEFT, 80);
	m_ctrlStepList.InsertColumn(4, "Referance", LVCFMT_LEFT, 100);
}


void CTopView::SetTopGridFont()
{
	//Font를 Registry에서 Laod
	UINT nSize;
	LPVOID* pData;
	LOGFONT lfGridFont;
	BOOL nRtn = AfxGetApp()->GetProfileBinary(FORMSET_REG_SECTION, "GridFont", (LPBYTE *)&pData, &nSize);
	if(nRtn && nSize > 0)
	{
		memcpy(&lfGridFont, pData, nSize);
	}
	else
	{
		CFont *pFont = GetFont();
		pFont->GetLogFont(&lfGridFont);
		AfxGetApp()->WriteProfileBinary(FORMSET_REG_SECTION, "GridFont",(LPBYTE)&lfGridFont, sizeof(LOGFONT));
	}
	delete [] pData;
	
	m_wndSmallChGrid.SetStyleRange(CGXRange().SetTable(), CGXStyle().SetFont(CGXFont(lfGridFont)));
}

void CTopView::SetCurrentModule(int nModuleID, int nGroupIndex)
{	
	m_nCurModuleID = nModuleID;
	m_nPrevStep = 0;
	
	////2003/5/12 Added By KBH////
	/*모듈 No를 Setting 시키면 기본 명령 Target을 모듈로 하고 Group 0를 선택*/
	m_nCmdTarget = EP_MODULE_CMD;
	m_nCurGroup = 0;
	m_CmdTarget.SetText(GetTargetModuleName());
		
	StartMonitoring();		//현재 모듈 모니터링 시작 
}

CString CTopView::GetTargetModuleName()
{
	CString temp;
	CString strMDName = GetModuleName(m_nCurModuleID);	
	temp.Format("Stage No. %s", strMDName);	
	return temp;
}

BYTE CTopView::GetColorIndex(STR_SAVE_CH_DATA  &chData, BYTE colorIndex, CStep *step)
{
	//Get Flash Setting
	BOOL bFlash = m_pConfig->m_bStateFlash[colorIndex];
	BYTE colorFlag;
	double data;
	
	colorFlag = ((m_pConfig->m_bShowColor && !bFlash) || (m_pConfig->m_bShowColor && bFlash && g_bChannelBlink)) ? colorIndex : 0x0f;	//15 is Channel Default color
	
	//정상인 Channel에 대해서만 적용 한다.
	if( chData.state != EP_STATE_CHECK &&
		chData.state != EP_STATE_OCV &&
		chData.state != EP_STATE_CHARGE &&
		chData.state != EP_STATE_DISCHARGE &&
		chData.state != EP_STATE_REST)	
	{
		return colorFlag;
	}
		
	if(m_pConfig->m_bOverColor && step != NULL)
	{
		if(m_DispItemNum == EP_VOLTAGE)				//전압 표기 
		{
			if(step->m_type == EP_TYPE_CHARGE)		//CC/CV Mode		//방전은 cc Mode 이므로 지원하자 않음
			{
				data = fabs(chData.fVoltage) - fabs(step->m_fVref);
				if( fabs(data) > m_pConfig->m_fRange[0])
				{
					colorFlag = STATE_COLOR+1;
				}
			}
		}
		else if(m_DispItemNum == EP_CURRENT)		//전류 표기 
		{
			if(step->m_type == EP_TYPE_CHARGE || step->m_type == EP_TYPE_DISCHARGE)		
			{
				float aa = (float)fabs(fabs(chData.fCurrent) - fabs(step->m_fIref));
				if(  aa > m_pConfig->m_fRange[1])
				{
					colorFlag = STATE_COLOR+2;
				}
			}
		}
	}		
	return colorFlag;
}

BOOL CTopView::SetCmdFlag(int cmd)
{
	CCTSMonDoc *pDoc = GetDocument();

	if(pDoc->m_nCmdInterval <= 0)	return TRUE;

	switch(cmd)		//Timer Event 가 발생 하지 않은 경우도 있는것 같아 다른 Command 상태를 다시 reset 시킨다.
	{
	case EP_CMD_RUN:
		if(pDoc->m_bRunCmd)		return TRUE;
		
		pDoc->m_bContinueCmd = FALSE;
		pDoc->m_bStopCmd = FALSE;
		pDoc->m_bInitCmd = FALSE;
		pDoc->m_bRunCmd = TRUE;	
		pDoc->m_bNextStepCmd = FALSE;
		break;

	case EP_CMD_CONTINUE:
		if(pDoc->m_bContinueCmd == TRUE)	return TRUE;
		pDoc->m_bContinueCmd = TRUE;
		pDoc->m_bStopCmd = FALSE;
		pDoc->m_bInitCmd = FALSE;
		pDoc->m_bRunCmd = FALSE;	
		pDoc->m_bNextStepCmd = FALSE;
		break;

	case EP_CMD_STOP:
		if(pDoc->m_bStopCmd)	return TRUE;
		pDoc->m_bContinueCmd = FALSE;
		pDoc->m_bStopCmd = TRUE;
		pDoc->m_bInitCmd = FALSE;
		pDoc->m_bRunCmd = FALSE;	
		pDoc->m_bNextStepCmd = FALSE;
		break;

	case EP_CMD_CLEAR:	
		if(pDoc->m_bInitCmd)	return TRUE;
		pDoc->m_bContinueCmd = FALSE;
		pDoc->m_bStopCmd = FALSE;
		pDoc->m_bInitCmd = TRUE;
		pDoc->m_bRunCmd = FALSE;	
		pDoc->m_bNextStepCmd = FALSE;
		break;

	case EP_CMD_NEXTSTEP:
		if(pDoc->m_bNextStepCmd)	return TRUE;
		pDoc->m_bContinueCmd = FALSE;
		pDoc->m_bStopCmd = FALSE;
		pDoc->m_bInitCmd = FALSE;
		pDoc->m_bRunCmd = FALSE;
		pDoc->m_bNextStepCmd = TRUE;
		break;

	default:
		pDoc->m_bContinueCmd = FALSE;
		pDoc->m_bStopCmd = FALSE;
		pDoc->m_bInitCmd = FALSE;
		pDoc->m_bRunCmd = FALSE;
		pDoc->m_bNextStepCmd = FALSE;
		return TRUE;
	}
		
	SetTimer(cmd, pDoc->m_nCmdInterval, NULL);
	return TRUE;
}

BOOL CTopView::ResetCmdFlag(int cmd)
{
	KillTimer(cmd);
	CCTSMonDoc *pDoc = GetDocument();

	switch(cmd)
	{
	case EP_CMD_RUN:		pDoc->m_bRunCmd = FALSE;		break;
	case EP_CMD_CONTINUE:	pDoc->m_bContinueCmd = FALSE;	break;
	case EP_CMD_STOP:		pDoc->m_bStopCmd	= FALSE;	break;
	case EP_CMD_CLEAR:		pDoc->m_bInitCmd = FALSE;		break;
	case EP_CMD_NEXTSTEP:	pDoc->m_bNextStepCmd = FALSE;	break;
	default:
		return TRUE;
	}
	return TRUE;
}

void CTopView::OnTraynoUserInput() 
{
	// TODO: Add your command handler code here
	((CMainFrame *)AfxGetMainWnd())->UserInputTrayNo(m_nCurModuleID);
}

void CTopView::OnUpdateTraynoUserInput(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	WORD gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);

	if((gpState == EP_STATE_STANDBY || gpState == EP_STATE_END || gpState == EP_STATE_READY || gpState == EP_STATE_IDLE) )
//		&& EPGetAutoProcess(m_nCurModuleID)
//		&& GetDocument()->GetLineMode(m_nCurModuleID, m_nCurGroup) == EP_OFFLINE_MODE)			
	{
		pCmdUI->Enable(TRUE);
	}
	else
	{	pCmdUI->Enable(FALSE);

	}	
}

void CTopView::OnOnlineMode() 
{
	// TODO: Add your command handler code here
/*	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_PERMISSION_ERROR));
		return ;
	}*/

	if( LoginPremissionCheck(PMS_MODULE_MODE_CHANGE) == FALSE)
	{
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_PERMISSION_ERROR) +" [Change to online]");
		return;
	}


	CCTSMonDoc *pDoc = GetDocument();
	CString strTemp;

	WORD gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
	
	//먼저 Idle 상태로 만든 후에 Line Mode 변경 하도록 사용자에게 경고 Message 출력 
	if(gpState != EP_STATE_READY && gpState != EP_STATE_STANDBY && gpState != EP_STATE_IDLE) 
	{
		strTemp.Format(::GetStringTable(IDS_MSG_NOT_IDLE_STATE), ::GetModuleName(m_nCurModuleID, m_nCurGroup));
		if(MessageBox(strTemp, "State Error", MB_ICONQUESTION|MB_YESNO) == IDYES)
		{
			if(pDoc->SendInitCommand(m_nCurModuleID, m_nCurGroup) == FALSE)
				return;
		} else
			return;
	}

	strTemp.Format(::GetStringTable(IDS_MSG_CHANGE_ONLINE_CONFIRM), ::GetModuleName(m_nCurModuleID, m_nCurGroup));
	if(MessageBox(strTemp, GetStringTable(IDS_TEXT_LINE_MODE_CHANGE), MB_ICONQUESTION|MB_YESNO) != IDYES)	return;

	pDoc->SetLineMode(m_nCurModuleID, m_nCurGroup, EP_ONLINE_MODE);
}

void CTopView::OnUpdateOnlineMode(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	if(((CCTSMonApp*)AfxGetApp())->GetSystemType() != EP_ID_FORM_OP_SYSTEM)
	{
		pCmdUI->Enable(FALSE);
		return;		
	}

	WORD nTrayType = GetDocument()->m_nViewTrayType;
	WORD gpState;

	switch (nTrayType)
	{
	case TRAY_TYPE_CUSTOMER:
	case TRAY_TYPE_PB5_ROW_COL:	//각형 세로 번호 체계
	case TRAY_TYPE_LG_ROW_COL:	//세로 번호 체계
		gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
		
		if((gpState == EP_STATE_IDLE || gpState == EP_STATE_READY) && GetDocument()->GetLineMode(m_nCurModuleID, m_nCurGroup) != EP_ONLINE_MODE)
			pCmdUI->Enable(TRUE);
		else
			pCmdUI->Enable(FALSE);
		break;
	case TRAY_TYPE_LG_COL_ROW:	//가로 번호 체계 
		pCmdUI->Enable(FALSE);
		break;
	}	
}

void CTopView::OnOfflineMode() 
{
	// TODO: Add your command handler code here
/*	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_PERMISSION_ERROR));
		return ;
	}
*/	
	if( LoginPremissionCheck(PMS_MODULE_MODE_CHANGE) == FALSE)
	{
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_PERMISSION_ERROR) +" [Change to offline]");
		return;
	}

	CCTSMonDoc *pDoc = GetDocument();
	CString strTemp;
	strTemp.Format(::GetStringTable(IDS_MSG_CHANGE_OFFLINE_CONFIRM), ::GetModuleName(m_nCurModuleID, m_nCurGroup));
	if(MessageBox(strTemp, GetStringTable(IDS_TEXT_LINE_MODE_CHANGE), MB_ICONQUESTION|MB_YESNO) != IDYES)	return;

	pDoc->SetLineMode(m_nCurModuleID, m_nCurGroup, EP_OFFLINE_MODE);
	
}

void CTopView::OnUpdateOfflineMode(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	WORD nTrayType = GetDocument()->m_nViewTrayType;
	WORD gpState;
	
	switch (nTrayType)
	{
	case TRAY_TYPE_CUSTOMER:
	case TRAY_TYPE_PB5_ROW_COL:	//각형 세로 번호 체계
	case TRAY_TYPE_LG_ROW_COL:	//세로 번호 체계
		gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
		
		if((gpState == EP_STATE_IDLE || gpState == EP_STATE_READY) 
			&& GetDocument()->GetLineMode(m_nCurModuleID, m_nCurGroup) != EP_OFFLINE_MODE)
			pCmdUI->Enable(TRUE);
		else
			pCmdUI->Enable(FALSE);
		break;
	case TRAY_TYPE_LG_COL_ROW:	//가로 번호 체계 
		pCmdUI->Enable(FALSE);
		break;
	}	
}

void CTopView::OnMaintenanceMode() 
{
	// TODO: Add your command handler code here
	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_PERMISSION_ERROR) +" [Change to maintenance]");
		return ;
	}

	CCTSMonDoc *pDoc = GetDocument();
	CString strTemp;

	strTemp.Format(::GetStringTable(IDS_MSG_CHANGE_MAINTEN_CONFIRM), ::GetModuleName(m_nCurModuleID, m_nCurGroup));
	if(MessageBox(strTemp, GetStringTable(IDS_TEXT_WORK_MODE_CHANGE), MB_ICONQUESTION|MB_YESNO) != IDYES)	return;

	pDoc->SetOperationMode(m_nCurModuleID, m_nCurGroup, EP_OPEARTION_MAINTENANCE);		
}

void CTopView::OnUpdateMaintenanceMode(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	//Maintenance/Control Mode는 충방전 Program이 아니라 다른 외부 프로그램을 이용하도록 변경

	WORD gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
	int nLineMode = GetDocument()->GetLineMode(m_nCurModuleID, m_nCurGroup);
	if(((gpState == EP_STATE_IDLE || gpState == EP_STATE_READY) && nLineMode == EP_OFFLINE_MODE) ||
		(gpState != EP_STATE_RUN && nLineMode == EP_ONLINE_MODE))		//Sensor State Check Mode
		pCmdUI->Enable(TRUE);
	else
		pCmdUI->Enable(FALSE);
}

void CTopView::OnControlMode() 
{
	// TODO: Add your command handler code here
	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_PERMISSION_ERROR) +" [Change to control]");
		return ;
	}

	CCTSMonDoc *pDoc = GetDocument();
	CString strTemp;

	strTemp.Format(GetStringTable(IDS_MSG_CHANGE_CONTROL_CONFIRM), ::GetModuleName(m_nCurModuleID, m_nCurGroup));
	if(MessageBox(strTemp, GetStringTable(IDS_TEXT_WORK_MODE_CHANGE), MB_ICONQUESTION|MB_YESNO) != IDYES)	return;

	pDoc->SetOperationMode(m_nCurModuleID, m_nCurGroup, EP_OPERATION_LOCAL);
}

void CTopView::OnUpdateControlMode(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	//Maintenance/Control Mode는 충방전 Program이 아니라 다른 외부 프로그램을 이용하도록 변경
//	pCmdUI->Enable(FALSE);

	WORD gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
	if( gpState == EP_STATE_MAINTENANCE  
		&& GetDocument()->GetLineMode(m_nCurModuleID, m_nCurGroup) == EP_OFFLINE_MODE)
		pCmdUI->Enable(TRUE);
	else
		pCmdUI->Enable(FALSE);
}

void CTopView::OnReboot() 
{
	// TODO: Add your command handler code here
	CRebootDlg	*pDlg;
	pDlg = new CRebootDlg(this);
	ASSERT(pDlg);
	
	if(EPGetGroupState(m_nCurModuleID, 0) == EP_STATE_RUN)
	{
		CString strTemp;
		strTemp.Format(GetStringTable(IDS_MSG_SYS_INI_CONFIRM), 
			::GetModuleName(m_nCurModuleID));
		if(MessageBox(strTemp, GetStringTable(IDS_TEXT_RUNNING), MB_ICONQUESTION|MB_YESNO) != IDYES)
			return;
	}

	pDlg->SetModuleName(::GetModuleName(m_nCurModuleID), GetDocument()->GetModuleIPAddress(m_nCurModuleID));
	pDlg->DoModal();

	delete pDlg;
	pDlg = NULL;
}

int CTopView::GetCurModuleID()
{
	return m_nCurModuleID;
}

int CompareDateTime(CTime *time1, CTime *time2)
{
	if(time1 == NULL || time2 == NULL)	return 0;

	if(time1->GetMonth() > time2->GetMonth())	return 1;
	else if(time1->GetMonth() < time2->GetMonth())	return -1;
	else
	{
		if(time1->GetDay() > time2->GetDay())	return 1;
		else if(time1->GetDay() < time2->GetDay())	return -1;
		else
		{
			if(time1->GetHour() > time2->GetHour())	return 1;
			else if(time1->GetHour() < time2->GetHour())	return -1;
			else
			{
				if(time1->GetMinute() > time2->GetMinute())	return 1;
				else if(time1->GetMinute() < time2->GetMinute())	return -1;
				else
				{
					if(time1->GetSecond() > time2->GetSecond())	return 1;
					else if(time1->GetSecond() < time2->GetSecond())	return -1;
					else	return 0;
				}
			}
		}

	}
	return 0;
}

void CTopView::OnUpdateGetProfileData(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	WORD gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
	if(gpState != EP_STATE_LINE_OFF && ((CCTSMonApp *)AfxGetApp())->GetSystemType() != EP_ID_IROCV_SYSTEM)
	{
		pCmdUI->Enable(TRUE);
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}
}

void CTopView::OnRefAdValueUpdate() 
{
	// TODO: Add your command handler code here
	CCTSMonDoc *pDoc = GetDocument();
	CString strTemp;

	strTemp.Format(GetStringTable(IDS_REFIC_DATA_UPDATE_CONFIRM), pDoc->m_strModuleName);
	if(MessageBox(strTemp, "Ref IC Update", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;
	
	//Dialog Permission Check
	if(LoginPremissionCheck(PMS_MODULE_CAL_UPDATE) == FALSE)
	{
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_PERMISSION_ERROR) +" [Reference IC Data Update]");
		return ;
	}


	WORD state;
//	LPVOID lpData = NULL;
/*	EP_MD_SYSTEM_DATA *lpSysData;

	lpSysData = EPGetModuleSysData(EPGetModuleIndex(m_nCurModuleID));
	if(lpSysData == NULL)	return;

	//Version 검사
	if(lpSysData->nVersion < 0x3003)
	{
		strTemp.Format(GetStringTable(IDS_MSG_NOT_SUPPORT_VERSION), ::GetModuleName(m_nCurModuleID, m_nCurGroup), lpSysData->nVersion);
		AfxMessageBox(strTemp, MB_ICONSTOP|MB_OK);
		return;
	}
*/			
	state = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
	//상태 검사 
	if(state == EP_STATE_STANDBY || state == EP_STATE_IDLE || state == EP_STATE_READY || state == EP_STATE_END)
	{
		int nRtn = EPSendCommand(m_nCurModuleID, 0, 0, EP_CMD_CAL_EXEC);
		if(nRtn != EP_ACK)
		{
			AfxMessageBox("Update Error\n");
		}
		// EPSendCommand(m_nCurModuleID);
/*		lpData = EPSendDataCmd(m_nCurModuleID, m_nCurGroup+1, 0, EP_CMD_CAL_EXEC, sizeof(EP_REF_IC_DATA));
		if(lpData != NULL)
		{
			//응답으로 현재 Update 시킨 값이 오는데 활용 안함 
			LPEP_REF_IC_DATA lpRefData = new EP_REF_IC_DATA;
			ASSERT(lpRefData);
			ZeroMemory(lpRefData, sizeof(EP_REF_IC_DATA));
			memcpy(lpRefData, lpData, sizeof(EP_REF_IC_DATA));
			
			  delete lpRefData;
			lpRefData = NULL;

		}
*/	}
}

void CTopView::OnUpdateRefAdValueUpdate(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
/*	EP_MD_SYSTEM_DATA *lpSysData;
	lpSysData = EPGetModuleSysData(EPGetModuleIndex(m_nCurModuleID));
	if(lpSysData == NULL)	return;
*/
	WORD state = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
	
//	if(PermissionCheck(PMS_MODULE_CAL_UPDATE) && lpSysData != NULL && lpSysData->nVersion >= 0x3003
//		&& (state == EP_STATE_STANDBY || state == EP_STATE_IDLE || state == EP_STATE_READY || state == EP_STATE_END))
	if(PermissionCheck(PMS_MODULE_CAL_UPDATE) && (state == EP_STATE_STANDBY || state == EP_STATE_IDLE || state == EP_STATE_READY || state == EP_STATE_END))
	{
		pCmdUI->Enable(TRUE);
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}	
}

BOOL CTopView::GetChRowCol(int nItemIndex, int nChindex, ROWCOL &nRow, ROWCOL &nCol)
{
	nRow = nChindex % m_nTopGridRowCount + 2;
	nCol = nChindex / m_nTopGridRowCount + m_nTopGridColCount+1;
	nCol = nCol + (nItemIndex * m_nTopGridColCount);	

	if(nCol < 0 || nCol > 6 * m_nTopGridColCount+ m_nTopGridColCount+1)	return FALSE;
	if(nRow < 0 || nRow > m_nTopGridRowCount+2)	return FALSE;

	return TRUE;
}

CString CTopView::DataUnitChanged()
{
	CString temp;
	CString strType;

	m_ctrlDiplayData.ResetContent();

	int nSystemType = ((CCTSMonApp *)AfxGetApp())->GetSystemType();

	int nCurIndex = 0;
	int nIndex = 0;
	temp = ::GetStringTable(IDS_LABEL_STATUS);
	strType = temp;
	m_ctrlDiplayData.AddString(temp);
	m_ctrlDiplayData.SetItemData(nIndex, EP_STATE);
	m_ctrlDiplayData.SetItemIcon(nIndex, 0);
	if(m_DispItemNum == EP_STATE)	nCurIndex = nIndex;
	nIndex++;
	
	temp.Format("%s(%s)", ::GetStringTable(IDS_LABEL_VOLTAGE), GetDocument()->m_strVUnit);
	strType = strType + "\n"+ temp;
	m_ctrlDiplayData.AddString(temp);
	m_ctrlDiplayData.SetItemData(nIndex, EP_VOLTAGE);
	m_ctrlDiplayData.SetItemIcon(nIndex, 1);
	if(m_DispItemNum == EP_VOLTAGE)	nCurIndex = nIndex;
	nIndex++;

	temp.Format("%s(%s)", ::GetStringTable(IDS_LABEL_CURRENT), GetDocument()->m_strIUnit);
	strType = strType + "\n"+ temp;
	m_ctrlDiplayData.AddString(temp);
	m_ctrlDiplayData.SetItemData(nIndex, EP_CURRENT);
	m_ctrlDiplayData.SetItemIcon(nIndex, 2);
	if(m_DispItemNum == EP_CURRENT)	nCurIndex = nIndex;
	nIndex++;

	temp.Format("%s(%s)", ::GetStringTable(IDS_LABEL_CAPACITY), GetDocument()->m_strCUnit);
	strType = strType + "\n"+ temp;
	m_ctrlDiplayData.AddString(temp);
	m_ctrlDiplayData.SetItemData(nIndex, EP_CAPACITY);
	m_ctrlDiplayData.SetItemIcon(nIndex, 3);
	if(m_DispItemNum == EP_CAPACITY)	nCurIndex = nIndex;
	nIndex++;

	temp = "Code";
	strType = strType + "\n"+ temp;
	m_ctrlDiplayData.AddString(temp);
	m_ctrlDiplayData.SetItemData(nIndex, EP_CH_CODE);
	m_ctrlDiplayData.SetItemIcon(nIndex, 4);
	if(m_DispItemNum == EP_CH_CODE)	nCurIndex = nIndex;
	nIndex++;

	temp.Format("%s(mOhm)", ::GetStringTable(IDS_LABEL_IMPEDANCE));
	strType = strType + "\n"+ temp;
	m_ctrlDiplayData.AddString(temp);
	m_ctrlDiplayData.SetItemData(nIndex, EP_IMPEDANCE);
	m_ctrlDiplayData.SetItemIcon(nIndex, 5);
	if(m_DispItemNum == EP_IMPEDANCE)	nCurIndex = nIndex;
	nIndex++;

	temp = "Time(s)";
	strType = strType + "\n"+ temp;
	m_ctrlDiplayData.AddString(temp);
	m_ctrlDiplayData.SetItemData(nIndex, EP_STEP_TIME);
	m_ctrlDiplayData.SetItemIcon(nIndex, 6);
	if(m_DispItemNum == EP_STEP_TIME)	nCurIndex = nIndex;
	nIndex++;

	temp.Format("Watt(%s)", GetDocument()->m_strWUnit);
	strType = strType + "\n"+ temp;
	m_ctrlDiplayData.AddString(temp);
	m_ctrlDiplayData.SetItemData(nIndex, EP_WATT);
	m_ctrlDiplayData.SetItemIcon(nIndex, 8);
	if(m_DispItemNum == EP_WATT)	nCurIndex = nIndex;
	nIndex++;

	temp.Format("WattHour(%s)", GetDocument()->m_strWhUnit);
	strType = strType + "\n"+ temp;
	m_ctrlDiplayData.AddString(temp);
	m_ctrlDiplayData.SetItemData(nIndex, EP_WATT_HOUR);
	m_ctrlDiplayData.SetItemIcon(nIndex, 9);
	if(m_DispItemNum == EP_WATT_HOUR)	nCurIndex = nIndex;
	nIndex++;

	temp = "Cell ID";
	strType = strType + "\n"+ temp;
	m_ctrlDiplayData.AddString(temp);
	m_ctrlDiplayData.SetItemData(nIndex, EP_CELL_SERIAL);
	m_ctrlDiplayData.SetItemIcon(nIndex, 11);
	if(m_DispItemNum == EP_CELL_SERIAL)	nCurIndex = nIndex;
	nIndex++;
/*
	temp = "Temper(℃)";
	strType = strType + "\n"+ temp;
	m_ctrlDiplayData.AddString(temp);
	m_ctrlDiplayData.SetItemData(nIndex, EP_TEMPER);
	m_ctrlDiplayData.SetItemIcon(nIndex, 10);
	if(m_DispItemNum == EP_TEMPER)	nCurIndex = nIndex;
	nIndex++;
*/
	m_wndSmallChGrid.SetStyleRange(CGXRange().SetRows(1), CGXStyle().SetChoiceList(strType));

	m_ctrlDiplayData.SetCurSel(nCurIndex);

	return strType;
}

void CTopView::OnSelchangeDataTypeCombo() 
{
	// TODO: Add your control notification handler code here

	long nItemNo = m_ctrlDiplayData.GetCurSel();
	if(nItemNo < 0)		
		return;

	m_DispItemNum = m_ctrlDiplayData.GetItemData(nItemNo);

	CFormModule *pModule = GetDocument()->GetModuleInfo(m_nCurModuleID);
	if(pModule)
	{
		for(int n=0; n<pModule->GetTotalJig(); n++)
		{
			m_wndSmallChGrid.SetStyleRange(CGXRange(1, (n+1)*2), CGXStyle().SetValue(nItemNo));			
			m_wndSmallChGrid.SetStyleRange(CGXRange(1, (n+1)*2+1), CGXStyle().SetValue(nItemNo+1L));	
			m_wndSmallChGrid.SetStyleRange(CGXRange(1, (n+1)*2+2), CGXStyle().SetValue(nItemNo+2L));	
			m_wndSmallChGrid.SetStyleRange(CGXRange(1, (n+1)*2+3), CGXStyle().SetValue(nItemNo+3L));	
			m_wndSmallChGrid.SetStyleRange(CGXRange(1, (n+1)*2+4), CGXStyle().SetValue(nItemNo+4L));	
		}
	}
	DrawChannel();
}

//  [7/21/2009 kky ]
// for 온라인 모드에서 스케줄을 표시하는 함수.
void CTopView::DrawOnlineStepList( int nModule )
{
	CCTSMonDoc *pDoc =  GetDocument();
	CFormModule *pModule = pDoc->GetModuleInfo(nModule);
	BOOL bStepEnd = TRUE;
	
	if(pModule == NULL)		
		return;
	
// 	if(pModule->GetLineMode() != EP_ONLINE_MODE)
// 	{
// 		return;
// 	}

	if( pDoc->m_OnlineStepData[nModule][ONLINE_STEPEND] != 1 )
	{
		return;
	}

	m_ctrlStepList.DeleteAllItems();

	LVITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.mask = LVIF_TEXT|LVIF_IMAGE;
	char szName[32];
	int stepIndex = 2;
	while( bStepEnd )
	{
		if( pDoc->m_OnlineStepData[nModule][stepIndex] == ONLINE_STEPTYPE_END || stepIndex == MAX_ONLINE_STEPDATA )
		{
			bStepEnd = FALSE;
		}
		sprintf(szName, "%d", stepIndex-1);
		lvItem.iItem = stepIndex-2;
		lvItem.iSubItem = 0;
		lvItem.pszText = szName;
		lvItem.iImage = I_IMAGECALLBACK;
		m_ctrlStepList.InsertItem(&lvItem);
		m_ctrlStepList.SetItemData(lvItem.iItem, pDoc->m_OnlineStepData[nModule][stepIndex]);		//==>LVN_ITEMCHANGED 를 발생 기킴 
		m_ctrlStepList.SetItemText(stepIndex-2, 1, szName);
		sprintf(szName, "%s", ::StepTypeMsg(pDoc->m_OnlineStepData[nModule][stepIndex]));
		m_ctrlStepList.SetItemText(stepIndex-2, 2, szName);
		stepIndex++;		
	}
	m_ctrlStepList.Invalidate();
}

//현재 선택모듈의 시험 조건을 표시한다.
void CTopView::DrawStepList()
{
	m_ctrlStepList.DeleteAllItems();

	CCTSMonDoc *pDoc=  GetDocument();
	CFormModule *pModule = pDoc->GetModuleInfo(m_nCurModuleID);
	
	if(pModule == NULL)		return;
	/*	
	//Idle이면 표시하지 않음 
	//접속하자 마자 최종 시험 정보 Loading(CheckAndLoadPrevTestInfo()) 한것을 reset한다.
	//접속시 module의 현재 상태를 받아서 처리한면 필요업음.
	if(pModule->GetState() == EP_STATE_IDLE)		
	{
		pModule->ResetGroupData();
		}
	*/
	LVITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.mask = LVIF_TEXT|LVIF_IMAGE;

	CTestCondition *pCon = pModule->GetCondition();
	CStep *pStep = NULL;
	char szName[32];
	int nI = 0;
	for(nI = 0; nI <pCon->GetTotalStepNo(); nI++ )
	{
		pStep = pCon->GetStep(nI);
		if(pStep == NULL)	break;
		
		sprintf(szName, "%d", pStep->m_StepIndex+1);
		lvItem.iItem = nI;
		lvItem.iSubItem = 0;
		lvItem.pszText = szName;
		lvItem.iImage = I_IMAGECALLBACK;
		m_ctrlStepList.InsertItem(&lvItem);
		m_ctrlStepList.SetItemData(lvItem.iItem, pStep->m_type);		//==>LVN_ITEMCHANGED 를 발생 기킴 

		m_ctrlStepList.SetItemText(nI, 1, szName);

		sprintf(szName, "%s", ::StepTypeMsg(pStep->m_type));
		m_ctrlStepList.SetItemText(nI, 2, szName);
		if(pStep->m_type == EP_TYPE_CHARGE || pStep->m_type == EP_TYPE_DISCHARGE || pStep->m_type == EP_TYPE_IMPEDANCE)
		{
			sprintf(szName, "%s/%s", pDoc->ValueString(pStep->m_fVref, EP_VOLTAGE, TRUE), pDoc->ValueString(pStep->m_fIref, EP_CURRENT, TRUE));
			m_ctrlStepList.SetItemText(nI, 4, szName);
		}
		else if(pStep->m_type == EP_TYPE_REST)
		{
			sprintf(szName, "%s", pDoc->ValueString(pStep->m_fEndTime, EP_STEP_TIME, TRUE));
			m_ctrlStepList.SetItemText(nI, 4, szName);
		}
	}	

	//Idle Monitoring을 위한 의미 없는 Step Item 추가
	//2008/7/18 KBH
	int nCurStep = pModule->GetCurStepNo();
	//현재 진행 Step에 맞게 Display 한다.
	for(nI = 0; nI < m_ctrlStepList.GetItemCount(); nI++)
	{
		if(pModule->IsComplitedStep(nI, m_nCurTrayIndex))
		{
			//완료된 data는 파일에서 loading한다.
			m_ctrlStepList.SetItemText(nI, 3, ::GetStringTable(IDS_TEXT_END));
		}
		else
		{
			if(nCurStep == nI+1)
			{
				//진행중인 data는 현재값을 표시한다.
				m_ctrlStepList.SetItemText(nI, 3, ::GetStringTable(IDS_TEXT_RUNNING));
			}
			else
			{
				//아직 진행하지 않은 step은 현재값을 표시한다.
				m_ctrlStepList.SetItemText(nI, 3, ::GetStringTable(IDS_TEXT_STANDBY));
			}
		}
	}
	m_ctrlStepList.Invalidate();
}

void CTopView::OnGetdispinfoStepList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;
	// TODO: Add your control notification handler code here
	
	UINT nType, nCurStep = 0;
	DWORD dwImgIndex = 0;
//	EP_CH_DATA netChData;

	if(pDispInfo->item.mask & LVIF_IMAGE)
    {
		CFormModule *pMD = GetDocument()->GetModuleInfo(m_nCurModuleID);
		
		if(pDispInfo->item.iSubItem == 2)
		{
			nType = m_ctrlStepList.GetItemData(pDispInfo->item.iItem);	//step type
			
			switch(nType)
			{
			case EP_TYPE_CHARGE	:		dwImgIndex = 2;		break;
			case EP_TYPE_DISCHARGE:		dwImgIndex = 1;		break;
			case EP_TYPE_REST	:		dwImgIndex = 11;	break;
			case EP_TYPE_OCV	:		dwImgIndex = 7;		break;
			case EP_TYPE_IMPEDANCE:		dwImgIndex = 10;	break;
			case EP_TYPE_END	:		dwImgIndex = 4;		break;
			default				:		dwImgIndex = 0;		break;
			}

			pDispInfo->item.iImage = dwImgIndex + 12;

			if(pMD)		
			{
				if(pMD->IsComplitedStep(pDispInfo->item.iItem, m_nCurTrayIndex))
				{
					pDispInfo->item.iImage = dwImgIndex;
				}
			}
		}
		else if(pDispInfo->item.iSubItem == 1)
		{
			if(pMD)		
			{
				
				nCurStep = pMD->GetCurStepNo();
				if((UINT)pDispInfo->item.iItem + 1 == nCurStep)
				{
					pDispInfo->item.iImage = 8;				
				}
				else
				{
					pDispInfo->item.iImage = -1;
				}
			}
		}
	}

				//현재 진행하지 않은 step인 경우는 Disable 그림 표시 
	/*			if(pDispInfo->item.iItem+1 > nCurStep)
				{
					pDispInfo->item.iImage = dwImgIndex + 12;
				}
				else
				{
					pDispInfo->item.iImage = dwImgIndex;
				}
				*/
/*	else if(pDispInfo->item.mask & LVIF_STATE)
	{
		if(pDispInfo->item.iSubItem == 1)
		{
			if(pDispInfo->item.state & LVIS_SELECTED)
			{
				pDispInfo->item.iImage = 8;				
			}
			else
			{
				pDispInfo->item.iImage = -1;
			}
		}
	}
*/
	*pResult = 0;
}

void CTopView::OnItemchangedStepList(NMHDR* pNMHDR, LRESULT* pResult) 
{
//	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	pNMHDR = NULL;	
	POSITION pos = m_ctrlStepList.GetFirstSelectedItemPosition();
	if(pos == NULL)		return;

	//현재 선택 step을 갱신하고 
	m_nCurSelStepIndex = m_ctrlStepList.GetNextSelectedItem(pos);		//0 Base

	//data를 다시 표시
	DrawChannel();	
	
	*pResult = 0;
}

void CTopView::OnDblclkStepList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	pNMHDR = NULL;
	CCTSMonDoc	*pDoc = (CCTSMonDoc *)GetDocument();
	pDoc->ShowTestCondition(m_nCurModuleID);
	
	*pResult = 0;
}

void CTopView::OnVoltageRadio() 
{
	// TODO: Add your control notification handler code here	
}

/*void CTopView::OnCurrentRadio() 
{
	// TODO: Add your control notification handler code here
}

void CTopView::OnCapacityRadio() 
{
	// TODO: Add your control notification handler code here
}*/

void CTopView::InitSmallChGrid()
{
	m_wndSmallChGrid.SubclassDlgItem(IDC_LARGE_GRID, this);
	m_wndSmallChGrid.m_bSameRowSize = TRUE;
	m_wndSmallChGrid.m_bSameColSize = TRUE;
	m_wndSmallChGrid.m_bCustomWidth = FALSE;
	m_wndSmallChGrid.m_bCustomColor = FALSE;

	m_wndSmallChGrid.Initialize();
	m_wndSmallChGrid.SetRowCount(1);
	m_wndSmallChGrid.SetColCount(1);
	m_wndSmallChGrid.SetDefaultRowHeight(26);
	m_wndSmallChGrid.SetFrozenRows(1);

	//Enable Multi Channel Select
	m_wndSmallChGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	//Enable Tooltips
	m_wndSmallChGrid.EnableGridToolTips();
    m_wndSmallChGrid.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));
	//Use MemDc
	m_wndSmallChGrid.SetDrawingTechnique(gxDrawUsingMemDC);

	//Row Header Setting
	m_wndSmallChGrid.SetStyleRange(CGXRange().SetCols(1),
			CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(255,255,255)).SetInterior(RGB(192,192,192)));
}

void CTopView::OnEditCopy() 
{
	// TODO: Add your command handler code here
	CWnd *pWnd= GetFocus();
	if(&m_wndSmallChGrid == (CMyGridWnd *)pWnd)
	{
		m_wndSmallChGrid.Copy();
	}
}

void CTopView::OnUpdateEditCopy(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	CWnd *pWnd= GetFocus();
	if(&m_wndSmallChGrid == (CMyGridWnd *)pWnd)
	{
		pCmdUI->Enable(TRUE);
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}	
}

void CTopView::OnGradeColorSet() 
{
	STR_GRADE_COLOR_CONFIG gradeConfig = GetDocument()->m_gradeColorConfig;
	CGradeColorDlg dlg;
	dlg.m_GradeColor = gradeConfig;
	if(dlg.DoModal() == IDOK)
	{
		gradeConfig = dlg.m_GradeColor;
		GetDocument()->m_gradeColorConfig = gradeConfig;
		::WriteGradeColorConfig(gradeConfig);
	}
}

void CTopView::OnViewResult() 
{
	// TODO: Add your command handler code here
	 GetDocument()->ViewResult(m_nCurModuleID);
}

void CTopView::OnUpdateViewResult(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	// TODO: Add your command update UI handler code here
	WORD gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
	if(gpState != EP_STATE_LINE_OFF && ((CCTSMonApp *)AfxGetApp())->GetSystemType() != EP_ID_IROCV_SYSTEM)
	{
		pCmdUI->Enable(TRUE);
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}		
}

void CTopView::Fun_UpdateChannelInfo( ViewType nViewType )
{
	int n =0;
	CFormModule *pModule = GetDocument()->GetModuleInfo(m_nCurModuleID);
	if(pModule)
	{
		if( nViewType == TopViewType1 )
		{
			for(n=0; n<pModule->GetTotalJig(); n++)
			{
				m_wndSmallChGrid.SetStyleRange(CGXRange(1, m_nTopGridColCount+1+(m_nTopGridColCount*0)), CGXStyle().SetValue((long)SELECT_ITEM_STATE));			
				m_wndSmallChGrid.SetStyleRange(CGXRange(1, m_nTopGridColCount+1+(m_nTopGridColCount*1)), CGXStyle().SetValue((long)SELECT_ITEM_VOLTAGE));	
				m_wndSmallChGrid.SetStyleRange(CGXRange(1, m_nTopGridColCount+1+(m_nTopGridColCount*2)), CGXStyle().SetValue((long)SELECT_ITEM_CURRENT));	
				m_wndSmallChGrid.SetStyleRange(CGXRange(1, m_nTopGridColCount+1+(m_nTopGridColCount*3)), CGXStyle().SetValue((long)SELECT_ITEM_CAPACITY));	
				m_wndSmallChGrid.SetStyleRange(CGXRange(1, m_nTopGridColCount+1+(m_nTopGridColCount*4)), CGXStyle().SetValue((long)SELECT_ITEM_CODE));				
			}
		}
		else if( nViewType == TopViewType2 )
		{		
			for(n=0; n<pModule->GetTotalJig(); n++)
			{
				m_wndSmallChGrid.SetStyleRange(CGXRange(1, m_nTopGridColCount+1+(m_nTopGridColCount*0)), CGXStyle().SetValue((long)SELECT_ITEM_IMP));			
				m_wndSmallChGrid.SetStyleRange(CGXRange(1, m_nTopGridColCount+1+(m_nTopGridColCount*1)), CGXStyle().SetValue((long)SELECT_ITEM_STEPTIME));	
				m_wndSmallChGrid.SetStyleRange(CGXRange(1, m_nTopGridColCount+1+(m_nTopGridColCount*2)), CGXStyle().SetValue((long)SELECT_ITEM_WATT));	
				m_wndSmallChGrid.SetStyleRange(CGXRange(1, m_nTopGridColCount+1+(m_nTopGridColCount*3)), CGXStyle().SetValue((long)SELECT_ITEM_WATTHOUR));	
				m_wndSmallChGrid.SetStyleRange(CGXRange(1, m_nTopGridColCount+1+(m_nTopGridColCount*4)), CGXStyle().SetValue((long)SELECT_ITEM_CELLSERIAL));
			}
		}
	}

	DrawChannel();
}
void CTopView::OnBnClickedViewSelect1Btn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Fun_UpdateChannelInfo( TopViewType1 );	
}

void CTopView::OnBnClickedViewSelect2Btn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Fun_UpdateChannelInfo( TopViewType2 );	
}

void CTopView::OnBnClickedResultdataDispBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	GetDocument()->ViewResult(m_nCurModuleID, 0);
}

void CTopView::OnBnClickedContactdataDispBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	GetDocument()->ViewResult(m_nCurModuleID, 0, 1);
}

void CTopView::OnBnClickedSendProcessBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	GetDocument()->ShowTestCondition(m_nCurModuleID);
}