// ProfileListInfo.h: interface for the CProfileListInfo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PROFILELISTINFO_H__D78592C7_A4C9_41BB_8872_A6A0772830C0__INCLUDED_)
#define AFX_PROFILELISTINFO_H__D78592C7_A4C9_41BB_8872_A6A0772830C0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CProfileListInfo  
{
public:
	BOOL LoadData(CString strFile);
	CProfileListInfo();
	virtual ~CProfileListInfo();

	CString GetResultFileName()		{	return m_strFmtFileName; }
	CString GetIpAddress()			{	return m_strIpAddress;	}
	CString GetTrayID()				{	return m_strTrayID;		}
	CString GetUnitName()			{	return m_strUnitName;	}
	long	GetUnitID()				{	return m_lUnitID;	}
	CString GetSavePath()			{	return m_strSaveDir;	}
	int		GetCnt()				{	return m_nCnt;			}

protected:
	int		m_nCnt;
	CString m_strProfileInfoFileName;
	CString m_strFmtFileName;
	CString m_strSaveDir;
	CString m_strIpAddress;
	long m_lUnitID;
	CString m_strTrayID;
	CString m_strUnitName;
};

#endif // !defined(AFX_PROFILELISTINFO_H__D78592C7_A4C9_41BB_8872_A6A0772830C0__INCLUDED_)
