#pragma once


#include <iostream>

#include <winsock2.h>
#include <ws2tcpip.h>
#include <mswsock.h>
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>

#include <vector>
#include <list>


#include<direct.h>


//#define MAX_BUF_LEN	26800
#define MAX_BUF_LEN_EXTRA SBC_MAX_BUF_LEN * 4



#pragma pack(1)


typedef struct tagSBCPacket
{
	SHORT CRC;
	BYTE EOT;
}SBCPACKET, *LPSBCPACKET;
#pragma pack()

typedef struct _Request_CMD
{
	HWND hWnd;
	UINT Cmd;
}Request_CMD;

enum SBCNetState
{
	SNS_NONE
	, SNS_SEND				//보내기
	, SNS_SENDING			//여러개 보내기
	, SNS_RECV_WAIT			//보낸후 기다리기
	, SNS_RESEND			//다시보내기
	, SNS_RECV				//받기
	, SNS_ERROR

	, SNS_MAXERROR
};

static TCHAR g_szSBCNetState[SNS_MAXERROR][64] = 
{
	TEXT("SNS_NONE")
	, TEXT("SNS_SEND")
	, TEXT("SNS_SENDING")
	, TEXT("SNS_RECV_WAIT")
	, TEXT("SNS_RESEND")
	, TEXT("SNS_RECV")
	, TEXT("SNS_ERROR")
};

enum SBCPacketError
{
	SPE_NONE
	, SPE_UNKNOWN_COMMAND
	, SPE_SIZE
	, SPE_BODY_EMPTY
	, SPE_MODULE_ID
	, SPE_IS_CONNECT

	, SPE_EP_CMD_AUTO_GP_STEP_SEC_DATA_Channel_Index_Error
	, SPE_EP_CMD_AUTO_GP_DATA_Channel_Index_Error
	, SPE_EP_CMD_AUTO_GP_STEP_END_DATA_Channel_Index_Error
	, SPE_EP_CMD_REAL_TIME_DATA_Channel_Index_Error

	, SPE_MAXERROR
};

static TCHAR g_szSBCPacketError[SPE_MAXERROR][64] = 
{
	TEXT("SPE_NONE")
	, TEXT("SPE_UNKNOWN_COMMAND")
	, TEXT("SPE_SIZE")
	, TEXT("SPE_BODY_EMPTY")
	, TEXT("SPE_MODULE_ID")
	, TEXT("SPE_IS_CONNECT")

	, TEXT("SPE_EP_CMD_AUTO_GP_STEP_SEC_DATA_Channel_Index_Error")
	, TEXT("SPE_EP_CMD_AUTO_GP_DATA_Channel_Index_Error")
	, TEXT("SPE_EP_CMD_AUTO_GP_STEP_END_DATA_Channel_Index_Error")
	, TEXT("SPE_EP_CMD_REAL_TIME_DATA_Channel_Index_Error")
};
//typedef struct _SBCQueue_DATA
//{
//	DWORD	command;
//	BYTE	Data[MAX_BUF_LEN];
//	DWORD	DataLength;
//}SBCQueue_DATA;