// DataClt.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "DataClt.h"

#pragma comment(lib, "ws2_32.lib ")
#pragma message("Automatically linking with ws2_32.lib by K.B.H")

#include "ClientSocket.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//
//	Note!
//
//		If this DLL is dynamically linked against the MFC
//		DLLs, any functions exported from this DLL which
//		call into MFC must have the AFX_MANAGE_STATE macro
//		added at the very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//

/////////////////////////////////////////////////////////////////////////////
// CDataCltApp

BEGIN_MESSAGE_MAP(CDataCltApp, CWinApp)
	//{{AFX_MSG_MAP(CDataCltApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDataCltApp construction

CDataCltApp::CDataCltApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CDataCltApp object

CDataCltApp theApp;
CClientSocket clientSide;

static BOOL bWriteLog = FALSE; 
char szLogFileName[512]; 
static char szMsg[512];

int ReadAck();
BOOL WriteLog(char *szLog);

BOOL WriteLog(char *szLog)
{
	TRACE("%s\n", szLog);

	if(bWriteLog)
	{
		if(strlen(szLogFileName) == 0 || strlen(szLog) == 0)		return FALSE;

		FILE *fp = fopen(szLogFileName, "ab+");
		if(fp == NULL)	return FALSE;

		time_t ltime;
		time( &ltime );
		fprintf(fp, "%s :: %s", ctime(&ltime), szLog );	/* Print local time as a string */
		fclose(fp);
	}
	return TRUE;;
}

EPDLL_API int dcConnectToServer(LPSTR serverIP, HWND hMsgWnd, UINT nSystemID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if(strlen(szLogFileName) <= 0)	//create Log File
	{
		SYSTEMTIME time;
		::GetSystemTime(&time);
		sprintf(szLogFileName, "%d%02d%02d.log", time.wYear, time.wMonth, time.wDay);
	}

	if(clientSide.m_bConnected)
	{
		WriteLog("Aleady Connected to Server\n");
		return -1;
	}

	if(serverIP == NULL)
	{
		WriteLog("Server Ip is Empty.\n");
		return -2;
	}

	WSADATA wsaData;
	WORD wVersionRequested = MAKEWORD( 2, 2 );				//WinSock Version Greater than 2.0
	int  err = WSAStartup( wVersionRequested, &wsaData );
	if (err != 0) 
	{
		// Tell the user that we couldn't find a usable WinSock DLL.
		WriteLog("Initialize Socket Error.\n");
		return -3;
	}
	clientSide.SetWnd(hMsgWnd);
	clientSide.m_nSystemID =  LOWORD(nSystemID);
	clientSide.m_nSystemType = HIWORD(nSystemID);
	if(!clientSide.StartWork(serverIP))
	{
		WriteLog("Try Connecting to server Fail\n");
		return -4;
	}
	return 0;
}

EPDLL_API int dcDisConnect()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

//	if(clientSide.m_bConnected)
//	{
		clientSide.StopWork();
//	}
	WSACleanup();
	return TRUE;
}


EPDLL_API EP_SYSTEM_VER* dcGetServerInfo()
{
/*	if(clientSide.m_bConnected)
	{
		return (LPEP_FORMSERVER_VER)&clientSide.m_sServer;
	}
*/
	return NULL;
}

EPDLL_API void dcSetLog(BOOL bWrite)
{
	bWriteLog = bWrite;
}


EPDLL_API	int dcSendData(int nCommand, LPVOID lpData, int nSize )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if(clientSide.m_bConnected ==  FALSE)
	{
		sprintf(szMsg, "Command 0x%d Send Fail. Not Connected\n", nCommand);
		WriteLog(szMsg);
		return EP_FAIL;
	}

	if(lpData == NULL)
	{
		sprintf(szMsg, "Command 0x%d Sending Data is NULL\n", nCommand);
		WriteLog(szMsg);
		return EP_FAIL;
	}

	UINT nBodySize = nSize;
	int nPacketSize = SizeofHeader() + nBodySize;		//Total Frame Size
	
	if(nPacketSize >= _EP_TX_BUF_LEN)
	{
		sprintf(szMsg, "Command 0x%d Tx Buffer OverFlow\n", nCommand);
		WriteLog(szMsg);
		clientSide.m_CommandAck.nCode = EP_TX_BUFF_OVER_FLOW;
		return clientSide.m_CommandAck.nCode;
	}
	
//	m_worker.m_TxBuffCritic.Lock();						//Tx Buffer Sync.
	LPEP_MSG_HEADER pMsgHeader;
	pMsgHeader = (LPEP_MSG_HEADER)clientSide.m_szTxBuffer;
	ZeroMemory(pMsgHeader, SizeofHeader());
	pMsgHeader->nID = clientSide.m_nSystemID;				//EP_ID_MODULE,		 EP_ID_FORMATION_SYSTEM
															//EP_ID_DATA_SERVER, EP_ID_IROCV_SYSTEM		
															//EP_ID_GRADING_SYSTEM	
	pMsgHeader->nCommand = nCommand;						//EP_CMD_CAP_DATA, EP_CMD_IR_DATA, EP_CMD_OCV_DATA
	pMsgHeader->nLength = nPacketSize;

	if(nBodySize>0)		
		memcpy((char *)clientSide.m_szTxBuffer + SizeofHeader(), lpData, nBodySize);

	clientSide.m_nTxLength = nPacketSize;
	//m_worker.m_TxBuffCritic.Unlock();

	::SetEvent(clientSide.m_hWriteEvent);
	sprintf(szMsg, "Send Command 0x%d to Data Server.\n", nCommand);
	WriteLog(szMsg);
	Sleep(0);
	return ReadAck();
}

//read From Command Response (ACK/NACK/FAIL)
int ReadAck()
{
	ASSERT(clientSide.m_bConnected);
	
	int nAck = EP_FAIL;
	if (::WaitForSingleObject(clientSide.m_hReadEvent, _EP_MSG_TIMEOUT) == WAIT_OBJECT_0 )
	{
//		::ResetEvent(m_stModule[nModuleIndex].m_hReadEvent);
		nAck = clientSide.m_CommandAck.nCode;
	}
	else
	{
		nAck =  EP_TIMEOUT;
	}
	
	::ResetEvent(clientSide.m_hReadEvent);
	clientSide.m_CommandAck.nCode = EP_FAIL;
	//TRACE("Read Response of Command %d\n", nAck);
	Sleep(0);
	return nAck;
}

EPDLL_API LPSTR dcGetClientIP()
{
	return clientSide.GetClientIP();
}