/***********************************************************************
	Copyrighted by LG Production engineering Research Center

	File Name: Data.cpp

	Written out by 
	Evaluation Thechnology Group
	Kim, Sung-Hoon

	Comment: Member functions of CData class are defined
***********************************************************************/

// Data.cpp: implementation of the CData class.
//
//////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

// #include

#include "stdafx.h"
#include "Data.h"
//#include "ChData.h"
#include "Plane.h"
#include "Line.h"
#include "SortTypeDlg.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

/*
	Name:    CData
	Comment: Constructor of CData class
	Input:   
	Output:
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
CData::CData()
{
	m_BackColor = RGB(255,255,255);
	m_bShowTitle = TRUE;
}

/*
	Name:    ~CData
	Comment: Destructor of CData class
	Input:   
	Output:
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
CData::~CData()
{
	// Plane list를 비우면서 각 plane에 할당된 memory를 해제한다.
	while(!m_PlaneList.IsEmpty())
	{
		CPlane* pPlane = m_PlaneList.RemoveTail();
		delete pPlane;
	}

	// Channel Data list를 비우면서 각 data에 할당된 memory를 해제한다.
	while(!m_ChDataList.IsEmpty())
	{
		CChData* pChData = m_ChDataList.RemoveTail();
		delete pChData;
	}
}

/*
	Name:    QueryData
	Comment: User로부터 Data의 Query조건을 받아서 조건에 맞게 Data를 가져와
	         Plane을 구성하고 각 Plane의 Line을 만들어 준다.
	Input:   Data가 위치한 folder들의 Path
	         -> 여러 folder인 경우, '\n'구분
			 -> 없는 경우, NULL
	Output:  사용자로부터 Query조건을 받았는지 여부
	         -> User가 "데이터조건 대화상자"를 "OK"버튼으로 닫으면 TRUE
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoo n  2001.06.01  The first settlement
*/
BOOL CData::QueryData(LPCTSTR strPaths, BOOL bReload)
{
	//////////////////////////////////////////////////////
	// Step 1. Data가 위치한 folder들을 Drag & Drop으로 //
	//         Main Frame으로 가져왔을 때,              //
	//////////////////////////////////////////////////////

	if(strPaths!=NULL)
	{
		CString str(strPaths);
		int p1=0,p2=0;
		int s1;
		while(TRUE)
		{
			// '\n'로 분리된 각 folder의 Path를 얻어서
			p2 = str.Find('\n',p1);
			if(p2<0) break;
			CString strPath = str.Mid(p1,p2-p1);
			p1=p2+1;
			// "데이터조건 대화상자"의 Test Path목록에 넣어준다.
//			m_QueryCondition.m_dlgQuery.m_strlistTestPath.AddTail(strPath);
//			strPath = "C:\\Projects\\Cycler\\BC\\Debug_\\Data\\Ni-Cd 전지2\\M04Ch01";
			
			m_QueryCondition.AddChPath(strPath);
			CString strTestName;
			BOOL bFind = FALSE;
			
			s1 = strPath.ReverseFind('\\');
			strTestName = strPath.Left(s1);
			POSITION pos = m_QueryCondition.m_dlgQuery.m_strlistTestPath.GetHeadPosition();

			while(pos)
			{	
				CString strTemp;
				strTemp = m_QueryCondition.m_dlgQuery.m_strlistTestPath.GetNext(pos);
				if(strTemp == strTestName)	//이미 추가된 시험명 
				{
					bFind = TRUE;
					break;
				}
			}
			if(bFind == FALSE)
			{
				m_QueryCondition.m_dlgQuery.m_strlistTestPath.AddTail(strTestName);
			}		
		}
	}

	int nNewQuery = m_QueryCondition.m_dlgQuery.m_strlistTestPath.GetCount();

	//////////////////////////////////////////////////////
	// Step 2. 사용자로부터 데이터 Query 조건을 받는다. //
	//////////////////////////////////////////////////////
	//Reload가 아니면 사용자 설정을 받아 들인다.
	if(!bReload)
	{
		if(!m_QueryCondition.GetUserSelection())			return FALSE;	//사용자 취소 
		m_strTestName = m_QueryCondition.m_strTestName;						//가장 마지막 선택 작업명 
	}

	////////////////////////////////////
	// Step 3. Plane List를 작성한다. //
	////////////////////////////////////
	// 기존의 Plane이 있는 경우,
	CPlane* pPlane;
	POSITION pos;
	if(!m_PlaneList.IsEmpty())
	{
		// 첫 Plane의 X축 Title과 User가 선택한 X축 Title이 다르면
		CString strTitle = m_PlaneList.GetHead()->GetXAxis()->GetTitle();
		if(m_QueryCondition.GetXAxis()->GetTitle().CompareNoCase(strTitle)!=0)
		{
			// X축이 변경된 경우는 모두 삭제 하고 새로 그린다.
			//기존의 Plane을 모두 지운다.
			while(!m_PlaneList.IsEmpty())
			{
				pPlane = m_PlaneList.RemoveTail();
				delete pPlane;
				pPlane = NULL;
			}
		}

		// User가 선택한 X축이 이전 Plane의 것과 같다면
		else
		{
			// Y축이 다른 Plane을 일단 속아내어 지운다.
			pos = m_PlaneList.GetHeadPosition();
			while(pos)
			{
				POSITION PrePos=pos;
				// Plane을 한개씩 꺼내어
				pPlane = m_PlaneList.GetNext(pos);
				// 그 Y Axis가 User의 선택에 있으면,
				CAxis* pAxis = m_QueryCondition.IsThereThisYAxis(pPlane->GetYAxis()->GetPropertyTitle());
				if(pAxis != NULL)
				{
					// X Axis와 Y Axis의 Unit를 새로 조정한다.(Unit이 변경 되었을 수도 있을므로)
					*pPlane->GetXAxis() = *m_QueryCondition.GetXAxis();
					*pPlane->GetYAxis() = *pAxis;
				}
				// Y Axis가 User의 선택에 없으면,
				else
				{
					// 해당 Plane을 리스트에서 지운다.
					pPlane = m_PlaneList.GetAt(PrePos);
					m_PlaneList.RemoveAt(PrePos);
					delete pPlane;
				}
			}
		}
	}

	// 기존에 없는 Plane이면 사용자의 선택에 따라 새 Plane을 만든다.
	pos = m_QueryCondition.GetYAxisHead();
	while(pos)
	{
		// 사용자가 선택한 Y축 중에서 기존의 Plane에 없는 것을 찾아
		CAxis* pYAxis = m_QueryCondition.GetNextYAxis(pos);
		if(!IsTherePlaneWithThisYAxis(pYAxis->GetPropertyTitle()))
		{
			// 새 Plane을 만들고 list에 넣는다.
			pPlane = new CPlane;
			*pPlane->GetXAxis() = *m_QueryCondition.GetXAxis();
			*pPlane->GetYAxis() = *pYAxis;
			if(!pPlane->SetColorOption(m_PlaneList.GetCount()%2==0)) delete pPlane;		//database에서 색상 정보를 Loading한다.
			else m_PlaneList.AddTail(pPlane);
		}
	}

	// 각 Plane의 Serial No를 다시 부여한다.
	for(int i=0; i<m_PlaneList.GetCount(); i++)
	{
		pPlane = m_PlaneList.GetAt(m_PlaneList.FindIndex(i));
		pPlane->SetSerialNo(i);
		
		//새로 그리기일 경우는 첫번째 Plane의 Y축 Grid를 표시하고 나머지는 표시하지 않는다.
		if(nNewQuery <= 0 || bReload == FALSE)
		{
			if(i == 0)
				pPlane->GetYAxis()->SetShowGrid(TRUE);
			else
				pPlane->GetYAxis()->SetShowGrid(FALSE);
		}
		
		//새로 그리기, Reload, 사용자 Option 변경시 Zoom되어 있는 상태이면 Zoom Off시켜야 한다.
		pPlane->ZoomOff();
	}


	////////////////////////////////////////////
	// Step 4. 각 Plane에 Line을 만들어 준다. //
	////////////////////////////////////////////

	pos = m_PlaneList.GetHeadPosition();
	while(pos)
	{
		pPlane = m_PlaneList.GetNext(pos);

		// Modify 해야 되는 상황이 아니면, 모든 Line들을 삭제한다.
		if(bReload)
		{
			ReloadDataAtPlane(pPlane);
		}
		else	//새로 그리기, 사용자 Option일 경우 
		{
			if(!m_QueryCondition.IsModifyingMode()) 
			{
				pPlane->DeleteAllLines();
			}
			// Modify 해야 되는 상황이면 일단 현 User선택에 없는 Line들을 삭제한다.
			else
			{
				POSITION LinePos = pPlane->GetLineHead();
				while(LinePos)
				{
					POSITION PrePos = LinePos;
					CLine* pLine = pPlane->GetNextLine(LinePos);
					if(!m_QueryCondition.IsThereThisLine(pLine->GetChPath(),
														 pLine->GetCycle(),
														 pLine->GetMode(),
														 pLine->GetAxisName()))
					{
						pPlane->RemoveLineAt(PrePos);
					}
				}
			}

			// 이제 새 Line들을 Plane에 추가한다.
			AddLinesToPlane(pPlane);

		}
		
		//X, Y축을 Scale에 맞게 재조정 
		pPlane->AutoScaleYAxis(m_PlaneList.GetCount());
	}

//	TRACE("DrawTime elapsed %d msec\n", GetTickCount()- nStartTime);

	return TRUE;	
}

/*
	Name:    LongCompare
	Comment: long형의 두 값의 크기를 비교
	Input:   한 수의 포인터, 다른 한 수의 포인터
	Output:  크기를 비교한 결과
	         +1: 앞수>뒷수
			  0: 앞수=뒷수
			 -1: 압수<뒷수
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
int CData::LongCompare(const void *arg1, const void *arg2)
{
	if(*((long*)arg1)> *((long*)arg2)) return 1;
	if(*((long*)arg1)==*((long*)arg2)) return 0;
    return -1;
}

/*
	Name:    FloatCompare
	Comment: float형의 두 값의 크기를 비교
	Input:   한 수의 포인터, 다른 한 수의 포인터
	Output:  크기를 비교한 결과
	         +1: 앞수>뒷수
			  0: 앞수=뒷수
			 -1: 압수<뒷수
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
int CData::FloatCompare(const void *arg1, const void *arg2)
{
	if(*((float*)arg1)> *((float*)arg2)) return 1;
	if(*((float*)arg1)==*((float*)arg2)) return 0;
    return -1;
}

/*
	Name:    IsTherePlaneWithThisYAxis
	Comment: long형의 두 값의 크기를 비교
	Input:   LPCTSTR strTitle - YAxis의 Propety 이름
	Output:  Plane List에 입력으로 들어온 Property 이름을 가진
	         Plane이 있는지 여부
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
BOOL CData::IsTherePlaneWithThisYAxis(LPCTSTR strTitle)
{
	POSITION pos = m_PlaneList.GetHeadPosition();
	while(pos)
	{
		if(m_PlaneList.GetNext(pos)->GetYAxis()->GetPropertyTitle().CompareNoCase(strTitle)==0) return TRUE;
	}
	return FALSE;
}

/*
	Name:    AddLinesToPlane
	Comment: 데이터 Query 조건에 따라 데이터를 불러와 Line을 만들어 Plane에 넣어줌
	Input:   Plane의 pointer
	Output:  
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
void CData::AddLinesToPlane(CPlane* pPlane)
{
	////////////////////////////////////////////////////////////////////
	//	Function 구성                                                 //
	//                                                                //
	//	POSITION YAxisPos = m_QueryCondition.GetYAxisHead();          //
	//	while(YAxisPos)                                               //
	//	{                                                             //
	//		/*♣♣♣ Part I   ♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣*/    //
	//		:                                                         //
	//		/*♣♣♣ Part II  ♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣*/    //
	//		/////////////////                                         //
	//		// CASE 1. ... //                                         //
	//		/////////////////                                         //
	//		:                                                         //
	//		/////////////////                                         //
	//		// CASE 2. ... //                                         //
	//		/////////////////                                         //
	//		:                                                         //
	//		/////////////////                                         //
	//		// CASE 3. ... //                                         //
	//		/////////////////                                         //
	//		:                                                         //
	//		/*♣♣♣ Part III ♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣*/    //
	//		:                                                         //
	//	}                                                             //
	////////////////////////////////////////////////////////////////////

	// Data Query조건에서 User가 선택한 Y축을 하나씩 꺼내어

	POSITION YAxisPos = m_QueryCondition.GetYAxisHead();
	CAxis* pYAxis;
	while(YAxisPos)
	{
		/*♣♣♣ Part I   ♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣*/ 

		// 그 Property 이름이 pPlane이 가르키는 plane object의
		// Y축의 property 이름과 다르면, Loop 마지막으로 continue!
		pYAxis = m_QueryCondition.GetNextYAxis(YAxisPos);
		if(pYAxis->GetPropertyTitle().CompareNoCase(pPlane->GetYAxis()->GetPropertyTitle())!=0) continue;

		// 같으면, Main Frame의 상태바에 Progress바를 생성하여 데이터 loading상황을 보여줄 준비를 한다.
		/*♠*/ CStatusBar* pStatusBar = (CStatusBar*)(((CMDIFrameWnd*)AfxGetMainWnd())->GetDlgItem(AFX_IDW_STATUS_BAR));
		/*♠*/ CProgressCtrl ProgressCtrl;
		/*♠*/ CRect rect;
		/*♠*/ pStatusBar->GetItemRect(0,rect);
		/*♠*/ rect.left = rect.Width()/2;
		/*♠*/ ProgressCtrl.Create(WS_CHILD|WS_VISIBLE,rect,pStatusBar,777);
		/*♠*/ ProgressCtrl.SetRange(0,m_QueryCondition.GetChPathCount());

		// 다음의 3가지 경우에 따라 데이터를 loading하여 line을 만들어
		// 새 라인의 List를 작성한다.
		CTypedPtrList<CPtrList, CLine*> NewLineList;

		/*♣♣♣ Part II  ♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣*/ 

		////////////////////////////////
		// CASE 1. X축이 cycle인 경우 //
		////////////////////////////////

		CString strChPath;
		CString strMsg;
		POSITION ChPos, ModePos, LinePos, CyclePos;

		if(m_QueryCondition.GetXAxis()->GetTitle().CompareNoCase("StepNo")==0)
		{
			DWORD dwMode;

			ChPos = m_QueryCondition.GetChPathHead();
			while(ChPos)
			{
				CList<DWORD, DWORD&> TempList;
				strChPath = m_QueryCondition.GetNextChPath(ChPos);
				//
				CList<DWORD, DWORD&> ModeList;
				m_QueryCondition.GetModeList(&ModeList, strChPath, pYAxis->GetTitle());
				ModePos = ModeList.GetHeadPosition();
				
				while(ModePos)
				{
					dwMode = ModeList.GetNext(ModePos);
					if(!pPlane->IsThereThisLine(strChPath, 0, dwMode, pYAxis->GetTitle())) 
					{
						TempList.AddTail(dwMode);
					}
				}

				/*♠*/ strMsg.Format("Loading data %s-%s %s",
				/*♠*/ 	          pPlane->GetXAxis()->GetTitle(),
				/*♠*/ 			  pYAxis->GetTitle(),
				/*♠*/ 			  strChPath.Mid(strChPath.ReverseFind('\\')+1));
				/*♠*/ ((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText(strMsg);
				/*♠*/ ProgressCtrl.SetRange(0,(short)(TempList.GetCount()*m_QueryCondition.GetCycleCount()));
				/*♠*/ int nPos = 0;

				LinePos = TempList.GetHeadPosition();
				LONG lCycle;
				fltPoint pt;
				while(LinePos)
				{
					dwMode = TempList.GetNext(LinePos);
					//Reload이면 이전 속성을 내려 받야야 함 
					CLine* pLine = new CLine(strChPath, 0, dwMode, pPlane->GetNextLineColor(), pYAxis->GetTitle());
					//
					CyclePos = m_QueryCondition.GetCycleHead();
					while(CyclePos)
					{
						lCycle = m_QueryCondition.GetNextCycle(CyclePos);
						//해당 Cycle에서 dwMode 조건에 맞는 모든 data Point 추가
						if(GetLastDataOfTable(pt, strChPath, lCycle, dwMode, pYAxis->GetTitle(), m_QueryCondition.GetVTMeasurePoint()))
						{
							pLine->AddData(pt);
						}	

						/*♠*/ nPos++;
						/*♠*/ ProgressCtrl.SetPos(nPos);
					}

					//
					if(pLine->GetDataCount()>0) NewLineList.AddTail(pLine);
					else                        delete pLine;
				}
			}
		}
		else // <- if(m_QueryCondition.GetXAxis()->GetTitle().CompareNoCase("cycle")==0)
		{

			///////////////////////////////////////////////////////////
			// CASE 2. X축이 cycle이 아니고 시작type이 '연속'인 경우 //
			///////////////////////////////////////////////////////////
			if(m_QueryCondition.GetStartPointOption() ==0)
			{
				//
				CStringList TempList;
				LONG lCycle;
				
				//현재 채널이 이미 표기되어 있는지 확인 
				ChPos = m_QueryCondition.GetChPathHead();
				while(ChPos)
				{
					strChPath = m_QueryCondition.GetNextChPath(ChPos);
					if(!pPlane->IsThereThisLine(strChPath, 0, 0, pYAxis->GetTitle())) 
						TempList.AddTail(strChPath);
				}
				
				//새로 그려야할 채널이름 리스트 
				LinePos = TempList.GetHeadPosition();
				while(LinePos)
				{
					strChPath = TempList.GetNext(LinePos);
					//
					CLine* pLine = new CLine(strChPath, 0, 0, pPlane->GetNextLineColor(), pYAxis->GetTitle());
					float fltSum = 0.0f;
					//
					CList<DWORD, DWORD&> ModeList;
					m_QueryCondition.GetModeList(&ModeList, strChPath, pYAxis->GetTitle());

					/*♠*/ strMsg.Format("Loading data %s-%s %s",
					/*♠*/ 	          pPlane->GetXAxis()->GetTitle(),
					/*♠*/ 			  pYAxis->GetTitle(),
					/*♠*/ 			  strChPath.Mid(strChPath.ReverseFind('\\')+1));
					/*♠*/ ((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText(strMsg);
					/*♠*/ ProgressCtrl.SetRange(0,(short)(ModeList.GetCount()*m_QueryCondition.GetCycleCount()));
					/*♠*/ int nPos = 0;

//					int a = ModeList.GetCount()*m_QueryCondition.GetCycleCount();
	
		
					CyclePos = m_QueryCondition.GetCycleHead();
//					fltPoint pt;
					while(CyclePos)
					{
						lCycle = m_QueryCondition.GetNextCycle(CyclePos);

						ModePos = ModeList.GetHeadPosition();
						while(ModePos)
						{
							DWORD dwMode = ModeList.GetNext(ModePos);
							LONG      lDataNum = 0L;
							fltPoint* pfltPt = GetDataOfTable(lDataNum,
								                              strChPath, lCycle, dwMode, 
															  pPlane->GetXAxis()->GetTitle(),
															  pYAxis->GetTitle());

//							TRACE("2: %s Draw data size is %d\n", strChPath, lDataNum);
							if(lDataNum>0 && pfltPt!=NULL)
							{
								for(int i=0; i<lDataNum; i++)
								{
									fltPoint pt = pfltPt[i];
									pt.x += fltSum;
									pLine->AddData(pt);
									//TRACE("Add %d :: %f\n", i+1, pfltPt[i].x);
								}

								if(pPlane->GetXAxis()->GetTitle().CompareNoCase("Time")==0)
									fltSum += pfltPt[lDataNum-1].x;

								delete [] pfltPt;
							}

							/*♠*/ nPos++;
							/*♠*/ ProgressCtrl.SetPos(nPos);
						}
					}

					//
					if(pLine->GetDataCount()>0) NewLineList.AddTail(pLine);
					else                      delete pLine;
				}
			}

			///////////////////////////////////////////////////////////////
			// CASE 3. X축이 cycle이 아니고 시작type이 '영점시작'인 경우 //
			///////////////////////////////////////////////////////////////

			else if(m_QueryCondition.GetStartPointOption() ==1)
			{
				ChPos = m_QueryCondition.GetChPathHead();			
				while(ChPos)
				{
					strChPath = m_QueryCondition.GetNextChPath(ChPos);
					//
					CList<LONG, LONG&> TempList;
					CyclePos = m_QueryCondition.GetCycleHead();
					while(CyclePos)
					{
						LONG lCycle = m_QueryCondition.GetNextCycle(CyclePos);
						if(!pPlane->IsThereThisLine(strChPath, lCycle, 0, pYAxis->GetTitle())) TempList.AddTail(lCycle);
					}
					//
					CList<DWORD, DWORD&> ModeList;
					m_QueryCondition.GetModeList(&ModeList, strChPath, pYAxis->GetTitle());

					/*♠*/ strMsg.Format("Loading data %s-%s %s",
					/*♠*/ 	          pPlane->GetXAxis()->GetTitle(),
					/*♠*/ 			  pYAxis->GetTitle(),
					/*♠*/ 			  strChPath.Mid(strChPath.ReverseFind('\\')+1));
					/*♠*/ ((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText(strMsg);
					/*♠*/ ProgressCtrl.SetRange(0,(short)(ModeList.GetCount()*TempList.GetCount()));
					/*♠*/ int nPos = 0;

					LinePos = TempList.GetHeadPosition();
					while(LinePos)
					{
						LONG lCycle = TempList.GetNext(LinePos);
						//
						CLine* pLine = new CLine(strChPath, lCycle, 0, pPlane->GetNextLineColor(), pYAxis->GetTitle());
						float fltSum = 0.0f;
						//
						ModePos = ModeList.GetHeadPosition();
						DWORD dwMode;
						while(ModePos)
						{
							dwMode = ModeList.GetNext(ModePos);
							LONG      lDataNum = 0L;
							fltPoint* pfltPt =GetDataOfTable(lDataNum,
								                             strChPath, lCycle, dwMode, 
															 pPlane->GetXAxis()->GetTitle(),
															 pYAxis->GetTitle());
//							TRACE("3: %s Draw data size is %d\n", strChPath, lDataNum);
							if(lDataNum>0 && pfltPt!=NULL)
							{
								for(int i=0; i<lDataNum; i++)
								{
									fltPoint pt = pfltPt[i];
									pt.x += fltSum;
									pLine->AddData(pt);
								}

								if(pPlane->GetXAxis()->GetTitle().CompareNoCase("Time")==0)
									fltSum += pfltPt[lDataNum-1].x;

								delete [] pfltPt;
							}

							/*♠*/ nPos++;
							/*♠*/ ProgressCtrl.SetPos(nPos);
						}

						//
						if(pLine->GetDataCount()>0) NewLineList.AddTail(pLine);
						else                        delete pLine;
					}
				}
			}
		}

		/*♣♣♣ Part III ♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣*/ 

		CLine *pLine;
		while(!NewLineList.IsEmpty())
		{
			pLine = NewLineList.RemoveHead();			
			pPlane->AddLine(pLine);
		}

		/*♠*/ ProgressCtrl.DestroyWindow();
		//Added by KBH 2005/10/31  
		/*♠*/ strMsg.Empty();
		/*♠*/ ((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText(strMsg);
	}
}

//파일에서 data를 새로 읽은 data로 Line을 새로 갱신한다.
void CData::ReloadDataAtPlane(CPlane* pPlane)
{
	////////////////////////////////////////////////////////////////////
	//	Function 구성                                                 //
	//                                                                //
	//	POSITION YAxisPos = m_QueryCondition.GetYAxisHead();          //
	//	while(YAxisPos)                                               //
	//	{                                                             //
	//		/*♣♣♣ Part I   ♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣*/    //
	//		:                                                         //
	//		/*♣♣♣ Part II  ♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣*/    //
	//		/////////////////                                         //
	//		// CASE 1. ... //                                         //
	//		/////////////////                                         //
	//		:                                                         //
	//		/////////////////                                         //
	//		// CASE 2. ... //                                         //
	//		/////////////////                                         //
	//		:                                                         //
	//		/////////////////                                         //
	//		// CASE 3. ... //                                         //
	//		/////////////////                                         //
	//		:                                                         //
	//		/*♣♣♣ Part III ♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣*/    //
	//		:                                                         //
	//	}                                                             //
	////////////////////////////////////////////////////////////////////

	// Data Query조건에서 User가 선택한 Y축을 하나씩 꺼내어
	POSITION YAxisPos = m_QueryCondition.GetYAxisHead();
	CAxis* pYAxis;
	while(YAxisPos)
	{
		/*♣♣♣ Part I   ♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣*/ 

		// 그 Property 이름이 pPlane이 가르키는 plane object의
		// Y축의 property 이름과 다르면, Loop 마지막으로 continue!
		pYAxis = m_QueryCondition.GetNextYAxis(YAxisPos);
		if(pYAxis->GetPropertyTitle().CompareNoCase(pPlane->GetYAxis()->GetPropertyTitle())!=0) continue;

		// 같으면, Main Frame의 상태바에 Progress바를 생성하여 데이터 loading상황을 보여줄 준비를 한다.
		/*♠*/ CStatusBar* pStatusBar = (CStatusBar*)(((CMDIFrameWnd*)AfxGetMainWnd())->GetDlgItem(AFX_IDW_STATUS_BAR));
		/*♠*/ CProgressCtrl ProgressCtrl;
		/*♠*/ CRect rect;
		/*♠*/ pStatusBar->GetItemRect(0,rect);
		/*♠*/ rect.left = rect.Width()/2;
		/*♠*/ ProgressCtrl.Create(WS_CHILD|WS_VISIBLE,rect,pStatusBar,777);
		/*♠*/ ProgressCtrl.SetRange(0,m_QueryCondition.GetChPathCount());


		/*♣♣♣ Part II  ♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣*/ 

		////////////////////////////////
		// CASE 1. X축이 cycle인 경우 //
		////////////////////////////////

		CString strChPath;
		CString strMsg;
		POSITION ChPos, ModePos, LinePos, CyclePos;

		if(m_QueryCondition.GetXAxis()->GetTitle().CompareNoCase("StepNo")==0)
		{
			DWORD dwMode;

			ChPos = m_QueryCondition.GetChPathHead();
			while(ChPos)
			{
				CList<DWORD, DWORD&> TempList;
				strChPath = m_QueryCondition.GetNextChPath(ChPos);
				//
				CList<DWORD, DWORD&> ModeList;
				m_QueryCondition.GetModeList(&ModeList, strChPath, pYAxis->GetTitle());
				ModePos = ModeList.GetHeadPosition();
				
				while(ModePos)
				{
					dwMode = ModeList.GetNext(ModePos);
					if(pPlane->IsThereThisLine(strChPath, 0, dwMode, pYAxis->GetTitle())) 
					{
						TempList.AddTail(dwMode);
					}
				}

				/*♠*/ strMsg.Format("Loading data %s-%s %s",
				/*♠*/ 	          pPlane->GetXAxis()->GetTitle(),
				/*♠*/ 			  pYAxis->GetTitle(),
				/*♠*/ 			  strChPath.Mid(strChPath.ReverseFind('\\')+1));
				/*♠*/ ((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText(strMsg);
				/*♠*/ ProgressCtrl.SetRange(0,(short)(TempList.GetCount()*m_QueryCondition.GetCycleCount()));
				/*♠*/ int nPos = 0;

				LinePos = TempList.GetHeadPosition();
				LONG lCycle;
				fltPoint pt;
				while(LinePos)
				{
					dwMode = TempList.GetNext(LinePos);
					//Reload이면 이전 속성을 내려 받야야 함 
					CLine* pLine = pPlane->FindLineOf(strChPath, 0, dwMode);
					if(pLine)
					{
						pLine->RemoveAllData();
						//
						CyclePos = m_QueryCondition.GetCycleHead();
						while(CyclePos)
						{
							lCycle = m_QueryCondition.GetNextCycle(CyclePos);
							if(GetLastDataOfTable(pt, strChPath, lCycle, dwMode, pYAxis->GetTitle(), m_QueryCondition.GetVTMeasurePoint()))
								pLine->AddData(pt);

							/*♠*/ nPos++;
							/*♠*/ ProgressCtrl.SetPos(nPos);
						}

					}
				}
			}
		}
		else // <- if(m_QueryCondition.GetXAxis()->GetTitle().CompareNoCase("cycle")==0)
		{

			///////////////////////////////////////////////////////////
			// CASE 2. X축이 cycle이 아니고 시작type이 '연속'인 경우 //
			///////////////////////////////////////////////////////////
			if(m_QueryCondition.GetStartPointOption() ==0)
			{
				//
				CStringList TempList;
				LONG lCycle;
				
				//현재 채널이 이미 표기되어 있는지 확인 
				ChPos = m_QueryCondition.GetChPathHead();
				while(ChPos)
				{
					strChPath = m_QueryCondition.GetNextChPath(ChPos);
					if(pPlane->IsThereThisLine(strChPath, 0, 0, pYAxis->GetTitle())) 
						TempList.AddTail(strChPath);
				}
				
				//새로 그려야할 채널이름 리스트 
				LinePos = TempList.GetHeadPosition();
				while(LinePos)
				{
					strChPath = TempList.GetNext(LinePos);
					//
					CLine* pLine = pPlane->FindLineOf(strChPath, 0, 0);
					if(pLine == NULL)	continue;

					pLine->RemoveAllData();

					float fltSum = 0.0f;
					//
					CList<DWORD, DWORD&> ModeList;
					m_QueryCondition.GetModeList(&ModeList, strChPath, pYAxis->GetTitle());

					/*♠*/ strMsg.Format("Loading data %s-%s %s",
					/*♠*/ 	          pPlane->GetXAxis()->GetTitle(),
					/*♠*/ 			  pYAxis->GetTitle(),
					/*♠*/ 			  strChPath.Mid(strChPath.ReverseFind('\\')+1));
					/*♠*/ ((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText(strMsg);
					/*♠*/ ProgressCtrl.SetRange(0,(short)(ModeList.GetCount()*m_QueryCondition.GetCycleCount()));
					/*♠*/ int nPos = 0;

					CyclePos = m_QueryCondition.GetCycleHead();
					while(CyclePos)
					{
						lCycle = m_QueryCondition.GetNextCycle(CyclePos);

						ModePos = ModeList.GetHeadPosition();
						while(ModePos)
						{
							DWORD dwMode = ModeList.GetNext(ModePos);
							LONG      lDataNum = 0L;
							fltPoint* pfltPt = GetDataOfTable(lDataNum,
								                              strChPath, lCycle, dwMode, 
															  pPlane->GetXAxis()->GetTitle(),
															  pYAxis->GetTitle());

//							TRACE("2: %s Draw data size is %d\n", strChPath, lDataNum);
							if(lDataNum>0 && pfltPt!=NULL)
							{
								for(int i=0; i<lDataNum; i++)
								{
									fltPoint pt = pfltPt[i];
									pt.x += fltSum;
									pLine->AddData(pt);
								}

								if(pPlane->GetXAxis()->GetTitle().CompareNoCase("Time")==0)
									fltSum += pfltPt[lDataNum-1].x;

								delete [] pfltPt;
							}

							/*♠*/ nPos++;
							/*♠*/ ProgressCtrl.SetPos(nPos);
						}
					}

					//
				}
			}

			///////////////////////////////////////////////////////////////
			// CASE 3. X축이 cycle이 아니고 시작type이 '영점시작'인 경우 //
			///////////////////////////////////////////////////////////////

			else if(m_QueryCondition.GetStartPointOption() ==1)
			{
				ChPos = m_QueryCondition.GetChPathHead();			
				while(ChPos)
				{
					strChPath = m_QueryCondition.GetNextChPath(ChPos);
					//
					CList<LONG, LONG&> TempList;
					CyclePos = m_QueryCondition.GetCycleHead();
					while(CyclePos)
					{
						LONG lCycle = m_QueryCondition.GetNextCycle(CyclePos);
						if(pPlane->IsThereThisLine(strChPath, lCycle, 0, pYAxis->GetTitle())) TempList.AddTail(lCycle);
					}
					//
					CList<DWORD, DWORD&> ModeList;
					m_QueryCondition.GetModeList(&ModeList, strChPath, pYAxis->GetTitle());

					/*♠*/ strMsg.Format("Loading data %s-%s %s",
					/*♠*/ 	          pPlane->GetXAxis()->GetTitle(),
					/*♠*/ 			  pYAxis->GetTitle(),
					/*♠*/ 			  strChPath.Mid(strChPath.ReverseFind('\\')+1));
					/*♠*/ ((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText(strMsg);
					/*♠*/ ProgressCtrl.SetRange(0,(short)(ModeList.GetCount()*TempList.GetCount()));
					/*♠*/ int nPos = 0;

					LinePos = TempList.GetHeadPosition();
					while(LinePos)
					{
						LONG lCycle = TempList.GetNext(LinePos);
						//
						CLine* pLine = pPlane->FindLineOf(strChPath, 0, 0);
						if(pLine == NULL)	continue;

						pLine->RemoveAllData();

						float fltSum = 0.0f;
						//
						ModePos = ModeList.GetHeadPosition();
						DWORD dwMode;
						while(ModePos)
						{
							dwMode = ModeList.GetNext(ModePos);
							LONG      lDataNum = 0L;
							fltPoint* pfltPt =GetDataOfTable(lDataNum,
								                             strChPath, lCycle, dwMode, 
															 pPlane->GetXAxis()->GetTitle(),
															 pYAxis->GetTitle());
//							TRACE("3: %s Draw data size is %d\n", strChPath, lDataNum);
							if(lDataNum>0 && pfltPt!=NULL)
							{
								for(int i=0; i<lDataNum; i++)
								{
									fltPoint pt = pfltPt[i];
									pt.x += fltSum;
									pLine->AddData(pt);
								}

								if(pPlane->GetXAxis()->GetTitle().CompareNoCase("Time")==0)
									fltSum += pfltPt[lDataNum-1].x;

								delete [] pfltPt;
							}

							/*♠*/ nPos++;
							/*♠*/ ProgressCtrl.SetPos(nPos);
						}

						//
					}
				}
			}
		}

		/*♣♣♣ Part III ♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣*/ 

		/*♠*/ ProgressCtrl.DestroyWindow();
		//Added by KBH 2005/10/31  
		/*♠*/ strMsg.Empty();
		/*♠*/ ((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText(strMsg);
	}
}


/*
	Name:    GetLastDataOfTable
	Comment: Table의 마지막 data를 가져온다.
	Input:   fltPoint& pt          - Table 마지막 data를 가져갈 변수
	         LPCTSTR   strChPath   - Channel data가 위치한 folder path
			 LONG      lCycle      - Cycle번호      (Table index를 구하기 위한 ...)
			 BYTE      byMode      - 충/방/충휴/방휴(Table index를 구하기 위한 ...)
			 LPCTSTR   strTitle    - data를 가져올 transducer의 이름
			 WORD      wPoint      - Pack용에서 'V_MaxDiff'와 'T_MaxDiff'를 계산하기 위해
			                         사용할 point의 정보를 담고 있는 변수
	Output:  Data를 얻는데 성공했는지 여부
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
BOOL CData::GetLastDataOfTable(fltPoint& pt, LPCTSTR strChPath, LONG lCycle,  DWORD dwMode, LPCTSTR strTitle, WORD wPoint)
{
	// 입력된 Channel data가 위치한 folder path의 CChData object의 pointer를 구한다.
	CChData* pChData = GetChData(strChPath);
	if(pChData==NULL) return FALSE;

	// Efficiency를 구하는 경우,
	pt.x = (float) lCycle;
	
	if(CString(strTitle).CompareNoCase("Efficiency")==0)
	{
		// x축은 cycle이므로 pt.x는 cycle번호

		//충방전 순서를 관계없이 계산한다.
		//충방전 순서는 사용자가 맞게 사용해야 한다.
		//동일 step이 여러개 존재할 경우 가장 마지막 Step이 Data를 사용한다.
		//현재 Cycle에서 충전용량을 구한다.
		float fChargeCap = pChData->GetLastDataOfCycle(lCycle, 0x01<<PS_STEP_CHARGE, "Capacity");
		//현재 Cycle에서 방전 용량을 구한다.
		float fDischrageCap = pChData->GetLastDataOfCycle(lCycle, 0x01<<PS_STEP_DISCHARGE, "Capacity");

		if(fChargeCap < 0.00001)		pt.y = 0.0f;
		else							pt.y = fDischrageCap/fChargeCap*100.0f;

		TRACE("Cycle %d : %.1f= %.3f/%.3f\n", lCycle, pt.y, fDischrageCap, fChargeCap);
	}
	if(CString(strTitle).CompareNoCase("IR") == 0)
	{
		pt.y = pChData->GetLastDataOfCycle(lCycle, 0x01<<PS_STEP_IMPEDANCE, "IR");
	}
	else 
	{	
		//pt.y = pChData->GetLastDataOfTable(GetTableIndex(lCycle, dwMode), strTitle, wPoint);
		pt.y = pChData->GetLastDataOfCycle(lCycle, dwMode, strTitle);

		if(pt.y==FLT_MAX)
		{
			pt.y=0.0f;
			return FALSE;
		}
	}

	return TRUE;
}


//formation type
//20081029
fltPoint* CData::GetDataOfTable(LONG& lDataNum, LPCTSTR strChPath, LONG lCycle, DWORD dwMode, LPCTSTR strXAxisTitle, LPCTSTR strYAxisTitle)
{
	CChData* pChData = GetChData(strChPath);
	if(pChData==NULL)
	{
		lDataNum = 0L;
		return NULL;

	}

	return pChData->GetDataOfCycle(lCycle, lDataNum, dwMode, strXAxisTitle, strYAxisTitle, m_QueryCondition.GetVTMeasurePoint());
}

/*
	Name:    GetTableIndex
	Comment: Table Index를 구한다.
	Input:	 LONG      lCycle        - Cycle번호
			 BYTE      byMode        - 충/방/충휴/방휴
	Output:  Table index
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
LONG CData::GetTableIndex(LONG /*lCycle*/, DWORD /*dwMode*/)
{
//	if(dwMode==CDataQueryCondition::MODE_NONE) return 0L;
//	else return (lCycle-1L)*4L+(LONG)dwMode;

	return 0;
}

/*
	Name:    GetTableIndex
	Comment: Table Index를 구한다.
	Input:	 LPCTSTR strChPath - Channel data가 위치한 folder path
	Output:  해당 CChData object의 pointer
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
CChData* CData::GetChData(LPCTSTR strChPath)
{
	CChData* pChData = NULL;

	//
	POSITION pos = m_ChDataList.GetHeadPosition();
	while(pos)
	{
		pChData = m_ChDataList.GetNext(pos);
		if(pChData->GetPath().CompareNoCase(strChPath)==0) break;
		else pChData = NULL;
	}

	//
	if(pChData==NULL)
	{
		pChData = new CChData(strChPath);
		if(pChData->Create()) m_ChDataList.AddTail(pChData);
		else
		{
			delete pChData;
			pChData = NULL;
		}
	}

	//
	return pChData;
}

/*
	Name:    DrawPlaneOnView
	Comment: 각 Plane을 View에 그려줌
	Input:   CDC *pDC        - View의 CDC object pointer
	         RECT ClientRect - View의 Client영역 Rect
	Output:  
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
void CData::DrawPlaneOnView(CDC* pDC, CRect ClientRect, BOOL bStairType)
{
	if(m_PlaneList.IsEmpty()) return;
	//
	/*
	      ============================
		  ||         TopSpace
		  ||         ---------------      
		  ||LeftSpace|
		  ||         |    Scope
		  ||         |
	*/


	//
	//Fill BackGround color
	//////////////////////////////////////////////////////////////////////////
	CBrush aBrush, *oldBrush;
	aBrush.CreateSolidBrush(m_BackColor);
	oldBrush = pDC->SelectObject(&aBrush);
	pDC->Rectangle(&ClientRect); // Client영역의 전체바탕화면을 m_BackColor로칠한다.
	pDC->SelectObject(oldBrush);
	aBrush.DeleteObject();
	//////////////////////////////////////////////////////////////////////////

	CPlane *pPlane;
	POSITION pos = m_PlaneList.GetHeadPosition();
	
	CRect scopeRect = GetScopeRect(pDC, ClientRect);
	int LabelSpace = ClientRect.left;
	//
	/*								
	      ================================================================
		  || LeftSpace										Title
		  ||<--------------------->|
		  || LabelSpace for Plane[1]
		  ||<----------->|
		  || LabelSpace for Plane[0]
		  ||<->| 
		  ||    Volt(mV)  Curr(mA)  ---------------      
		  ||                       |
		  ||                       |    Scope
		  ||    1.0       2.2      |
	*/
	
	//////////////////////////////////////////////////////////////////////////
	//X, Y축을 그리고 축의 눈금을 표시한다.
	pos = m_PlaneList.GetHeadPosition();
	while(pos)
	{
		// 각 Plane에
		pPlane = m_PlaneList.GetNext(pos);
		// 좌측에 Label을 줄 위치와 Scope까지의 좌측여백, 상단여백,
		// 그리고 현재 Client영역의 Rect정보를 가지고
		// Scope의 테두리, Grid Line을 그린다.
		LabelSpace = pPlane->DrawScopeInView(pDC, LabelSpace, scopeRect, ClientRect);
		
	}

	//////////////////////////////////////////////////////////////////////////
	//Title Window 표시 
	if(m_bShowTitle)
	{
		DrawTitle(pDC, scopeRect);
	}
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	// 그리고, 각 Plan의 Data Line을 그린다.
	pos = m_PlaneList.GetHeadPosition();
	while(pos)
	{
		// 각 Plane에
		pPlane = m_PlaneList.GetNext(pos);
		// 좌측여백, 상단여백, 그리고 현재 Client영역의 Rect정보를 가지고 Line을 그린다.
		pPlane->DrawLineInView(pDC, scopeRect, bStairType);
	}
	//////////////////////////////////////////////////////////////////////////

}

void CData::DrawTitle(CDC *pDC, CRect scopeRect)
{
	CFont afont, *oldfont;
	afont.CreateFont(
					   25,                        // nHeight
					   0,                         // nWidth
					   0,                         // nEscapement
					   0,                         // nOrientation
					   FW_NORMAL,                 // nWeight
					   FALSE,                     // bItalic
					   FALSE,                     // bUnderline
					   0,                         // cStrikeOut
					   ANSI_CHARSET,              // nCharSet
					   OUT_DEFAULT_PRECIS,        // nOutPrecision
					   CLIP_DEFAULT_PRECIS,       // nClipPrecision
					   DEFAULT_QUALITY,           // nQuality
					   DEFAULT_PITCH | FF_SWISS,  // nPitchAndFamily
					   "Arial"						// lpszFacename
					  );                 

	oldfont=pDC->SelectObject(&afont);
	pDC->SetTextAlign(TA_BASELINE|TA_CENTER);
	pDC->TextOut(scopeRect.left + scopeRect.Width()/2, scopeRect.top - scopeRect.top/2+8,  m_strTestName);
	pDC->SelectObject(oldfont);
	afont.DeleteObject();
}


/*
	Name:    GetScopeRect
	Comment: 그래프가 그려질 Scope영역의 rect를 구함
	Input:   CDC *pDC        - View의 CDC object pointer
	         RECT ClientRect - View의 Client영역 Rect
	Output:  
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
//그래프 영역을 구한다.
RECT CData::GetScopeRect(CDC *pDC, RECT ClientRect)
{
	//
	int LeftSpace=0;
	int MaxTopSpace=0;
	TEXTMETRIC Metrics;
	POSITION pos = m_PlaneList.GetHeadPosition();
	CFont afont,*oldfont;
	CPlane *pPlane;
	
	while(pos)
	{
		pPlane = m_PlaneList.GetNext(pos);
		afont.CreateFontIndirect(pPlane->GetYAxis()->GetFont());
		oldfont=pDC->SelectObject(&afont);
		pDC->GetTextMetrics(&Metrics);

		//각 Y축 별로 라벨 공간을 13문자 할당한다.
		LeftSpace+=Metrics.tmAveCharWidth*_YAXIS_LABEL_WIDTH;

		//Y축 중 가장 큰 폰트의 1글자 공간을 위에서 뛰다.
		if((Metrics.tmHeight+Metrics.tmExternalLeading*2) > MaxTopSpace)
			MaxTopSpace = Metrics.tmHeight+Metrics.tmExternalLeading*2;
		
		pDC->SelectObject(oldfont);
		afont.DeleteObject();
	}

	//Title Space로 전체 높이의 5% 할당
	if(m_bShowTitle)
	{
		MaxTopSpace += int((ClientRect.bottom-ClientRect.top) *0.05f);
	}

	//
	pPlane = m_PlaneList.GetHead();
	// View에서 Scope의 크기를 정한다.
	TEXTMETRIC XAxisTextMetric;
	{
		CFont afont, *oldfont;
		afont.CreateFontIndirect(pPlane->GetXAxis()->GetFont());
		oldfont=pDC->SelectObject(&afont);
		pDC->GetTextMetrics(&XAxisTextMetric);
		pDC->SelectObject(oldfont);
		afont.DeleteObject();
	}

	//
	RECT ScopeRect = { ClientRect.left + LeftSpace,
		               ClientRect.top  + MaxTopSpace,
		               //ClientRect.right  - 260/*XAxisTextMetric.tmAveCharWidth * 6*/,		//List control 표시 영역
		               ClientRect.right  - XAxisTextMetric.tmAveCharWidth * 6,		//List control 표시 영역
		               ClientRect.bottom - (XAxisTextMetric.tmHeight+XAxisTextMetric.tmExternalLeading)*3 };
	return ScopeRect;
}

/*
	Name:    ShowPatternOfChData
	Comment: 적용된 패턴의 내용을 보여줌
	Input:   Channel Data가 위치한 folder의 path
	Output:  
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
void CData::ShowPatternOfChData(LPCTSTR strChPath)
{
	CChData* pChData = GetChData(strChPath);
	if(pChData!=NULL)
	{
		CScheduleData* pPattern = pChData->GetPattern();
		if(pPattern!=NULL) 
			pPattern->ShowContent();
//			pPattern->ShowContent(0L,0L,CMode::MODE_NO_OP);
	}
}

int CData::CheckPointInWhichChannel(CRect ClientRect, POINT destPoint)
{
	POSITION pos = m_PlaneList.GetHeadPosition();

	int nLineIndex = -1;
	CPlane *pPlane;
	while(pos)
	{
		//각 Plane의 모든 라인을 검사하여 해당 지점에 속하는 Data가 존재 하는지 검사
		pPlane = m_PlaneList.GetNext(pos);
		nLineIndex = pPlane->IsPointInLine(ClientRect, destPoint);

		if(nLineIndex >= 0)
			return nLineIndex;
	}
	return -1;
}

BOOL CData::ReLoadData()
{
	CWaitCursor wait;
	CChData* pChData = NULL;
	POSITION pos = m_ChDataList.GetHeadPosition();
	while(pos)
	{
		pChData = m_ChDataList.GetNext(pos);
		if(pChData)
		{
			pChData->ReLoadData();
		}
	}
	
/*	while(!m_PlaneList.IsEmpty())
	{
		CPlane* pPlane = m_PlaneList.RemoveTail();
		delete pPlane;
		pPlane = NULL;
	}
*/
	QueryData(NULL, TRUE);
	return TRUE;
}

//주어진 viewPoint(destPoint)에 가장 가까운 datapoint를 구하고 그 data가 어느 항목 data인지 검사 
BOOL CData::CheckPointWhichPlane(CRect rect, POINT destPoint, int &nPlanIndex, int &nLineIndex, fltPoint &fltDataPoint)
{
	POSITION pos = m_PlaneList.GetHeadPosition();

	int nLIndex = -1;
	int nPIndex = 0;
	fltPoint dataPoint;

	CPlane *pPlane;
	while(pos)
	{
		//각 Plane의 모든 라인을 검사하여 해당 지점에 속하는 Data가 존재 하는지 검사
		pPlane = m_PlaneList.GetNext(pos);
		nLIndex = pPlane->IsPointInPoint(rect, destPoint, dataPoint);
		
		if(nLIndex >= 0)
		{
			nPlanIndex = nPIndex;
			nLineIndex = nLIndex;
			fltDataPoint.x = dataPoint.x;
			fltDataPoint.y = dataPoint.y;
			return TRUE;
		}
		nPIndex++;
	}
	return FALSE;
}

