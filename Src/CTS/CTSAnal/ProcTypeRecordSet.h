#if !defined(AFX_PROCTYPERECORDSET_H__0F605C7F_6DC7_4CDE_A608_956920B9985E__INCLUDED_)
#define AFX_PROCTYPERECORDSET_H__0F605C7F_6DC7_4CDE_A608_956920B9985E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ProcTypeRecordSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CProcTypeRecordSet recordset

class CProcTypeRecordSet : public CRecordset
{
public:
	CProcTypeRecordSet(CDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CProcTypeRecordSet)

// Field/Param Data
	//{{AFX_FIELD(CProcTypeRecordSet, CRecordset)
	long	m_ProcType;
	long	m_ProcID;
	CString	m_Description;
	//}}AFX_FIELD


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CProcTypeRecordSet)
	public:
	virtual CString GetDefaultConnect();    // Default connection string
	virtual CString GetDefaultSQL();    // Default SQL for Recordset
	virtual void DoFieldExchange(CFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROCTYPERECORDSET_H__0F605C7F_6DC7_4CDE_A608_956920B9985E__INCLUDED_)
