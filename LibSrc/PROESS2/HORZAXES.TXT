To implement the horizontally stacked y axes,
we've added a new property array called..

#define PEP_naOVERLAPMULTIAXES  3059

This property array defines the number of
multiple axes (defined by PEP_naMULTIAXESSUBSETS)
which should be overlapped.

Since certain Multi-axes will be overlapped, the 
property array PEP_faMULTIAXESPROPORTIONS now
should contain the same number of elements as
the PEP_naOVERLAPMULTIAXES property array, and
not the same as PEP_naMULTIAXESSUBSETS as is the
case when no PEP_naOVERLAPMULTIAXES are defined.

For example, you can make the following adjustments
to the demo Visual C++ project that comes with
ProEssentials.  The Scientific Graph Multi Y Axes
example currently has 4 elements defined for
PEP_naMULTIAXESSUBSETS

int mas[4] = {6,2,2,2};
PEvset(m_hPE, PEP_naMULTIAXESSUBSETS, mas, 4);

It also has 4 elements defined for 
PEP_faMULTIAXESPROPORTIONS.

float map[4] = {.5F, .15F, .15F, .2F};
PEvset(m_hPE, PEP_faMULTIAXESPROPORTIONS, map, 4);

To convert this example to overlap multi axes,
you could adjust as follows.

// Add this to existing Scientific Graph Multi-Y-Axes example //
int oma[3] = {2,1,1};
PEvset(m_hPE, PEP_naOVERLAPMULTIAXES, oma, 3);

// Adjust PEP_faMULTIAXESPROPORTIONS as follows  //
float map[3] = {.75F, .125F, .125F};
PEvset(m_hPE, PEP_faMULTIAXESPROPORTIONS, map, 3);

The above code tells the graph to overlap the
first two multi-axes.  The first Multi-axis has 6 subsets
with 4 on left y axis and 2 on right y axis.  The
second Multi-Axis has 2 subsets plotted on left y axis.
So overlapping these two Multi-Axes causes the top
Overlapping-Multi-Axis to contain 8 subsets, which
results in 2 left y axes and one right y axis.  The
other Multi-Axes remain the same since the 2nd and 3rd
PEP_naOVERLAPMULTIAXES elements are defined as 1 which
means there is none overlapping.  Since we've added 
Overlapping Multi Axes, we needed to adjust the axes 
proportions so that it reflects the number of 
PEP_naOVERLAPMULTIAXES elements.  In this case
it was set to 3 elements.

Other possible adjustments could be..

// Add this to existing Scientific Graph Multi-Y-Axes example //
int oma[2] = {3,1};
PEvset(m_hPE, PEP_naOVERLAPMULTIAXES, oma, 2);
// Adjust PEP_faMULTIAXESPROPORTIONS as follows  //
float map[3] = {.75F, .25F};
PEvset(m_hPE, PEP_faMULTIAXESPROPORTIONS, map, 2);

.. Or try this combination ..

// Add this to existing Scientific Graph Multi-Y-Axes example //
int oma[1] = {4};
PEvset(m_hPE, PEP_naOVERLAPMULTIAXES, oma, 1);
// Do not set any PEP_faMULTIAXESPROPORTIONS since there is 
// only one resulting overlapping Multi-Axes. So.
// comment out any reference to PEP_faMULTIAXESPROPORTIONS.

Studying these three variations should give you an
understanding how charts with horizontally stacked
y axes are formed.

Since stacking y axes horizontally can result in using 
alot of real-estate, you will want to set the PEP_nFONTSIZE 
property to SMALL, and also might want to adjust the 
PEP_nFONTSIZEGLOBALCONTROL to 0.75.

NOTE:
As a general implementation hint.  First ignore
OverlapMultiAxes and MultiAxesProportions and
initially implement only MultiAxesSubsets.
After you have all the individual axes looking as
desired, then add the OverlapMultiAxis property elements
to overlap the desired axes.  Finally if needed
add MultiAxesProportions to contain the same
number of elements as OverlapMultiAxes.  If you
follow this approach it will greatly enhance
your understanding on how OverlapMultiAxes is
implemented. Note that when implementing
MultiAxesSubsets, the subsets have to be in the
order that you wanted them allocated to axes.
The same goes for OverlapMultiAxes and the axes have
to be in the order that you want them allocated to
overlap. Following the above recommendation on
implementing OverlapMultiAxes will help you visualize
how your subsets and axes need to be ordered
to produce the desired overlapped multi axis
graph.

************************
*** VB / ACCESS NOTES **
************************

To experiment how this new property works.  In the 
VB Example Project, Form3 Load event.  Make the 
following adjustment to MultiAxesProportions and add 
the OverlapMultiAxes items.

'** Adjust MultiAxesProportions as follows **'
Pesgo1.MultiAxesProportions(0) = .5
Pesgo1.MultiAxesProportions(1) = .25
Pesgo1.MultiAxesProportions(2) = .25

'** Set the OverlapMultiAxes property array **'
Pesgo1.OverlapMultiAxes(0) = 2
Pesgo1.OverlapMultiAxes(1) = 1
Pesgo1.OverlapMultiAxes(2) = 1

*******************
*** DELPHI NOTES **
*******************

To experiment how this new property works.  In the 
Delphi Example Project, Form3 OnLoad event.  Make the 
following adjustment to MultiAxesProportions and add 
the OverlapMultiAxes items.

{'** Adjust MultiAxesProportions as follows **'}
PESGraph1.MultiAxesProportions[0] := .5;
PESGraph1.MultiAxesProportions[1] := .25;
PESGraph1.MultiAxesProportions[2] := .25;

{'** Set the OverlapMultiAxes property array **'}
PESGraph1.OverlapMultiAxes[0] := 2;
PESGraph1.OverlapMultiAxes[1] := 1;
PESGraph1.OverlapMultiAxes[2] := 1;













