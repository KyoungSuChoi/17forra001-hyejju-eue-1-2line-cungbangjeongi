#if !defined(AFX_TOPMONITORINGCONFIGDLG_H__5558E097_40D7_11D2_80A6_0020741517CE__INCLUDED_)
#define AFX_TOPMONITORINGCONFIGDLG_H__5558E097_40D7_11D2_80A6_0020741517CE__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// TopMonitoringConfigDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTopMonitoringConfigDlg dialog
#include "Label.h"
#include "ColorBox.h"
//#include "ColorButton.h"
#include "ColorEdit.h"

#include "resource.h"
#include "../../../BCLib/Include/PSCommon.h"

class CTopMonitoringConfigDlg : public CDialog
{
// Construction
public:
	BOOL m_bInitColor;
	void SetColorCfgData(PS_COLOR_CONFIG cfgData);
	PS_COLOR_CONFIG GetColorConfigData();
	void UpdataColor();
	PS_COLOR_CONFIG GetDefaultColorConfig();
	void DrawColor();
	CTopMonitoringConfigDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CTopMonitoringConfigDlg)
	enum { IDD = IDD_TOP_CONFIG_DLG };
	BOOL			m_DisplayText;
	BOOL			m_DisplayColor;
	BOOL	m_bVOverBlink;
	BOOL	m_bIOverBlink;
	//}}AFX_DATA
	CColorBox 		m_TWellColor[PS_MAX_STATE_COLOR];
	CColorBox 		m_BWellColor[PS_MAX_STATE_COLOR];
	BOOL			m_Flash[PS_MAX_STATE_COLOR];
	CLabel			m_Result[PS_MAX_STATE_COLOR];
	CColorEdit		m_colorEdit[PS_MAX_STATE_COLOR];
	
	BOOL			m_bOverColor;
	float			m_fVRange;
	float			m_fIRange;
	CColorBox 	m_IBOverColor;
	CColorBox 	m_VBOverColor;
	CColorBox 	m_ITOverColor;
	CColorBox 	m_VTOverColor;
	CLabel			m_IOverResult;
	CLabel			m_VOverResult;

	PS_COLOR_CONFIG 		m_TopCfg;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTopMonitoringConfigDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	BOOL m_bBlinkFlag;

	// Generated message map functions
	//{{AFX_MSG(CTopMonitoringConfigDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnTopcfgSetdefault();
	afx_msg void OnTopcfgDisplayText();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnTopcfgInverse1();
	afx_msg void OnTopcfgInverse2();
	afx_msg void OnTopcfgInverse3();
	afx_msg void OnTopcfgInverse4();
	afx_msg void OnTopcfgInverse5();
	afx_msg void OnTopcfgInverse6();
	afx_msg void OnTopcfgInverse7();
	afx_msg void OnTopcfgInverse8();
	afx_msg void OnTopcfgInverse9();
	afx_msg void OnTopcfgInverse10();
	afx_msg void OnTopcfgInverse11();
	afx_msg void OnTopcfgInverse12();
	afx_msg void OnTopcfgInverse13();
	afx_msg void OnTopcfgInverse14();
	afx_msg void OnTopcfgInverse15();
	afx_msg void OnVOverBlink();
	afx_msg void OnIOverBlink();
	afx_msg void OnTopcfgDisplayColor();
	//}}AFX_MSG
	afx_msg LRESULT OnColorBtnClick(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TOPMONITORINGCONFIGDLG_H__5558E097_40D7_11D2_80A6_0020741517CE__INCLUDED_)
