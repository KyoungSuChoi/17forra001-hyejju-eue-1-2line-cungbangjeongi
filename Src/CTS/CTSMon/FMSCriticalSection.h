#pragma once

class CFMSCriticalSection
{
public:
	CFMSCriticalSection(VOID) {InitializeCriticalSection(&m_csObject);}
	~CFMSCriticalSection(VOID)	{DeleteCriticalSection(&m_csObject);}

	inline VOID Enter(VOID) {EnterCriticalSection(&m_csObject);
	////-------------------------------------------------------------------------
	//WCHAR tempStr[32] = {0,};
	//_snwprintf_s(tempStr, 1024, L"EnterCriticalSection\n");
	//OutputDebugString(tempStr);
	////-------------------------------------------------------------------------
	}
	inline VOID Leave(VOID)	{LeaveCriticalSection(&m_csObject);
	////-------------------------------------------------------------------------
	//WCHAR tempStr[32] = {0,};
	//_snwprintf_s(tempStr, 1024, L"LeaveCriticalSection\n");
	//OutputDebugString(tempStr);
	////-------------------------------------------------------------------------
	}

private:
	CRITICAL_SECTION m_csObject;
};