// ChResultViewDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSMon.h"
#include "ChResultViewDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChResultViewDlg dialog


CChResultViewDlg::CChResultViewDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CChResultViewDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CChResultViewDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CChResultViewDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChResultViewDlg)
	DDX_Control(pDX, IDC_CH_NO, m_ctrlChDisp);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CChResultViewDlg, CDialog)
	//{{AFX_MSG_MAP(CChResultViewDlg)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_FILE_SAVE, OnFileSave)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChResultViewDlg message handlers

BOOL CChResultViewDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CString strTemp;
	strTemp.Format("%s Channel %d Data", m_strModule, m_nChannelIndex+1);
	m_ctrlChDisp.SetText(strTemp);
	m_ctrlChDisp.SetBkColor(RGB(255, 255, 255));
	InitChGrid();
//	if(m_pBoard->stBoardFileHeader.wChPerBd <= m_wChannelIndex || m_wChannelIndex <0)	return TRUE;
	DisplayChannelData();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CChResultViewDlg::InitChGrid()
{
	m_wndChGrid.SubclassDlgItem(IDC_CH_STEP_VIEW_GRID, this);
	//m_wndBoardListGrid.m_bRowSelection = TRUE;
	m_wndChGrid.m_bSameColSize  = FALSE;
	m_wndChGrid.m_bSameRowSize  = FALSE;
	m_wndChGrid.m_bCustomWidth  = TRUE;

	CRect rect;
	m_wndChGrid.GetWindowRect(rect);	//Step Grid Window Position
	ScreenToClient(&rect);

	int width = rect.Width()/100;
	m_wndChGrid.m_nWidth[1]	= width* 7;
	m_wndChGrid.m_nWidth[2]	= width* 10;
	m_wndChGrid.m_nWidth[3]= width* 10;
	m_wndChGrid.m_nWidth[4]= width* 8;
	m_wndChGrid.m_nWidth[5]	= width* 15;
	m_wndChGrid.m_nWidth[6]	= width* 10;
	m_wndChGrid.m_nWidth[7]	= width* 10;
	m_wndChGrid.m_nWidth[8]	= width* 10;
	m_wndChGrid.m_nWidth[9]	= width* 10;
	m_wndChGrid.m_nWidth[10]	= width* 10;
	m_wndChGrid.m_nWidth[11]	= width* 10;

	m_wndChGrid.Initialize();
//	m_wndChGrid.SetRowCount();
	m_wndChGrid.SetColCount(11);
	m_wndChGrid.SetDefaultRowHeight(18);
	m_wndChGrid.GetParam()->SetSortRowsOnDblClk(TRUE);
	m_wndChGrid.m_BackColor	= RGB(255,255,255);
//	m_wndChGrid.m_SelColor	= RGB(225,255,225);
//	m_wndChGrid.HideCols(5, 5);

	m_wndChGrid.m_bCustomColor 	= FALSE;
//	m_wndChGrid.m_CustomColorRange	= CGXRange(1, 2, TOT_CH_PER_MODULE, 2);
//	memset(m_DetalChColorFlag, 15, sizeof(m_DetalChColorFlag));		//Default color Setting Array[15]
//	m_wndChGrid.m_pCustomColorFlag = (char *)m_DetalChColorFlag;

	m_wndChGrid.SetValueRange(CGXRange(0, 1),  ::GetStringTable(IDS_LABEL_STEP));
	m_wndChGrid.SetValueRange(CGXRange(0, 2),  ::GetStringTable(IDS_LABEL_STATUS));
	m_wndChGrid.SetValueRange(CGXRange(0, 3),  ::GetStringTable(IDS_LABEL_BAD));
	m_wndChGrid.SetValueRange(CGXRange(0, 4),  ::GetStringTable(IDS_LABEL_GRADE));
	m_wndChGrid.SetValueRange(CGXRange(0, 5),  ::GetStringTable(IDS_LABEL_STEP_TIME));
	m_wndChGrid.SetValueRange(CGXRange(0, 6),  ::GetStringTable(IDS_LABEL_VOLTAGE));
	m_wndChGrid.SetValueRange(CGXRange(0, 7),  ::GetStringTable(IDS_LABEL_CURRENT));
	m_wndChGrid.SetValueRange(CGXRange(0, 8),  ::GetStringTable(IDS_LABEL_CAPACITY));
	m_wndChGrid.SetValueRange(CGXRange(0, 9),  ::GetStringTable(IDS_LABEL_IMPEDANCE));
	m_wndChGrid.SetValueRange(CGXRange(0, 10), ::GetStringTable(IDS_LABEL_POWER));
	m_wndChGrid.SetValueRange(CGXRange(0, 11), ::GetStringTable(IDS_LABEL_ENERGY));

	m_wndChGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
}

BOOL CChResultViewDlg::DisplayChannelData()
{
	char szTemp[128];
	BYTE colorFlag;
	
	LPSTR_SAVE_CH_DATA lpChData;
	int nCount;
	int nStepSize = m_pResultFile->GetStepSize();//m_stepResult->GetSize();
	m_wndChGrid.InsertRows(1, nStepSize);
//	EP_GROUP_INFO *pStep = NULL;
	STR_STEP_RESULT *pStep = NULL;

	for(int i = 0; i<nStepSize; i++)
	{
		nCount = i+1;
		m_wndChGrid.SetValueRange(CGXRange(nCount, 1), (ROWCOL)nCount );	//Step Number
//		pStep = (EP_GROUP_INFO *)m_stepResult->GetAt(i);
		pStep = m_pResultFile->GetStepData(i);
		ASSERT(pStep);
		

		if(m_nChannelIndex <0 || m_nChannelIndex >= pStep->aChData.GetSize())	break;
		lpChData = (STR_SAVE_CH_DATA *)pStep->aChData[m_nChannelIndex]; 

		m_wndChGrid.SetValueRange(CGXRange(nCount, 2),  m_pDoc->GetStateMsg(lpChData->state, colorFlag));
			
/*		if(colorFlag < MODULE_COLOR)
			m_wndChGrid.SetStyleRange(CGXRange(nCount, 2), 
							CGXStyle().SetTextColor(m_pConfig->m_TModuleColor[colorFlag])
									  .SetInterior(m_pConfig->m_BModuleColor[colorFlag]));
		else
			m_wndChGrid.SetStyleRange(CGXRange(nCount, 2), 
							CGXStyle().SetTextColor(m_pConfig->m_TChannelColor[colorFlag-MODULE_COLOR])
									  .SetInterior(m_pConfig->m_BChannelColor[colorFlag-MODULE_COLOR]));
*/
		m_wndChGrid.SetStyleRange(CGXRange(nCount, 2), 
						CGXStyle().SetTextColor(m_pConfig->m_TStateColor[colorFlag])
								  .SetInterior(m_pConfig->m_BStateColor[colorFlag]));


		ConvertTime(szTemp, lpChData->fStepTime);
		m_wndChGrid.SetValueRange(CGXRange(nCount, 5),  szTemp);
		sprintf(szTemp, "%.3f", lpChData->fVoltage/1000.0f);
		m_wndChGrid.SetValueRange(CGXRange(nCount, 6),  szTemp);
		sprintf(szTemp, "%.1f", lpChData->fCurrent);
		m_wndChGrid.SetValueRange(CGXRange(nCount, 7),  szTemp);
		sprintf(szTemp, "%.1f", lpChData->fCapacity);
		m_wndChGrid.SetValueRange(CGXRange(nCount, 8),  szTemp);
		sprintf(szTemp, "%.1f", lpChData->fWatt);
		m_wndChGrid.SetValueRange(CGXRange(nCount, 10),  szTemp);
		sprintf(szTemp, "%.1f", lpChData->fWattHour);
		m_wndChGrid.SetValueRange(CGXRange(nCount, 11),  szTemp);
		sprintf(szTemp, "%.1f", lpChData->fImpedance);
		m_wndChGrid.SetValueRange(CGXRange(nCount, 9), szTemp);
		m_wndChGrid.SetValueRange(CGXRange(nCount, 4), m_pDoc->ValueString(lpChData->grade, EP_GRADE_CODE));
		m_wndChGrid.SetValueRange(CGXRange(nCount, 3), m_pDoc->ChCodeMsg(lpChData->channelCode));
	}
	return TRUE;
}

void CChResultViewDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	if(::IsWindow(this->GetSafeHwnd()))
	{
		RECT rect;
		::GetClientRect(m_hWnd, &rect);

		CRect rectGrid;
		float width;
		if(m_wndChGrid.GetSafeHwnd())
		{
			m_wndChGrid.GetWindowRect(rectGrid);	//Step Grid Window Position
			ScreenToClient(&rectGrid);
			m_wndChGrid.MoveWindow(5, rectGrid.top, rect.right-10, rect.bottom-rectGrid.top-5, FALSE);
			
			m_wndChGrid.GetClientRect(rectGrid);
			width = (float)rectGrid.Width()/100.0f;
				
			m_wndChGrid.m_nWidth[1]	= int(width* 7);
			m_wndChGrid.m_nWidth[2]	= int(width* 10);
			m_wndChGrid.m_nWidth[3]= int(width* 10);
			m_wndChGrid.m_nWidth[4]= int(width* 8);
			m_wndChGrid.m_nWidth[5]	= int(width* 15);
			m_wndChGrid.m_nWidth[6]	= int(width* 10);
			m_wndChGrid.m_nWidth[7]	= int(width* 10);
			m_wndChGrid.m_nWidth[8]	= int(width* 10);
			m_wndChGrid.m_nWidth[9]	= int(width* 10);
			m_wndChGrid.m_nWidth[10]	= int(width* 10);
			m_wndChGrid.m_nWidth[11]	= int(width* 10);
//			m_wndChGrid.Redraw();
			Invalidate();
		}
	}
}

void CChResultViewDlg::OnFileSave() 
{
	// TODO: Add your control notification handler code here
	CString strFileName;
	strFileName.Format("%s_C%d.csv", m_strModule, m_nChannelIndex+1);

	CFileDialog pDlg(FALSE, "", strFileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "csv ����(*.csv)|*.csv|");
	if(IDOK == pDlg.DoModal())
	{
		strFileName = pDlg.GetPathName();
		if(strFileName.IsEmpty())	return ;

		FILE *fp = fopen(strFileName, "wt");
		if(fp == NULL)	return ;

		BeginWaitCursor();
			
		for(UINT i=0; i< m_wndChGrid.GetRowCount(); i++)
		{
			for(UINT j=1; j< m_wndChGrid.GetColCount()+1; j++)
			{
				if( j > 1)
					fprintf(fp, ",");

				fprintf(fp, "%s", m_wndChGrid.GetValueRowCol(i, j));
			}
			fprintf(fp, "\n");
		}

		EndWaitCursor();
		
		fclose(fp);
	}	
}
