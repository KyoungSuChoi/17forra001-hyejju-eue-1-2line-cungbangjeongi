#include "stdafx.h"

#include "CTSMon.h"

#include "FMS.h"
#include "FM_Unit.h"

#include "FM_STATE.h"

CFM_ST_AT_TRAY_IN::CFM_ST_AT_TRAY_IN(FM_STATUS_ID _Eqstid, FM_STATUS_ID _stid, CFM_Unit* _unit)
:CFM_ST_AUTO(_Eqstid, _stid, _unit)
{
}


CFM_ST_AT_TRAY_IN::~CFM_ST_AT_TRAY_IN(void)
{
}

VOID CFM_ST_AT_TRAY_IN::fnEnter()
{
	fnSetFMSStateCode(FMS_ST_TRAY_IN);
	//트레이 검사

	TRACE("CFM_ST_AT_TRAY_IN::fnEnter %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_AT_TRAY_IN::fnProc()
{
	if(1 == m_iWaitCount)
	{
		fnSetFMSStateCode(FMS_ST_RED_TRAY_IN);
		m_iWaitCount = -1;
	}

	m_RetryMap |= RETRY_NOMAL_ON;

	TRACE("CFM_ST_AT_TRAY_IN::fnProc %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_AT_TRAY_IN::fnExit()
{
	//공정 완료

	TRACE("CFM_ST_AT_TRAY_IN::fnExit %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_AT_TRAY_IN::fnSBCPorcess(WORD _state)
{
	CFM_ST_AUTO::fnSBCPorcess(_state);

	TRACE("%s \n", g_strSBCState[m_Unit->fnGetModule()->GetState()]);
	
	if(m_Unit->fnGetFMS()->fnGetError())
	{
		CHANGE_STATE(AUTO_ST_ERROR);
	}
        
	switch(m_Unit->fnGetModule()->GetState())
	{
	case EP_STATE_IDLE:
		if(m_Unit->fnGetModuleTrayState())
		{
			
		}
		else
		{
			CHANGE_STATE(AUTO_ST_VACANCY);
		}
		break;
	case EP_STATE_STANDBY:
		{
			if(m_Unit->fnGetModuleTrayState())
			{

			}
			else
			{
				CHANGE_STATE(AUTO_ST_VACANCY);
			}
		}
		break;
	case EP_STATE_PAUSE:
		{
			CHANGE_STATE(AUTO_ST_ERROR);
		}
		break;
	case EP_STATE_RUN:
		{
			if(m_Unit->fnGetModuleCheckState())
			{
				CHANGE_STATE(AUTO_ST_CONTACT_CHECK);
			}
			else
			{
				CHANGE_STATE(AUTO_ST_RUN);
			}
		}
		break;
	case EP_STATE_READY:
		{
			CHANGE_STATE(AUTO_ST_READY);
		}
		break;
	case EP_STATE_END:
		{
			CHANGE_STATE(AUTO_ST_END);
		}
		break;
	case EP_STATE_ERROR:
		{
			CHANGE_STATE(AUTO_ST_ERROR);
		}
		break;
	}
	TRACE("CFM_ST_AT_TRAY_IN::fnSBCPorcess %d \n", m_Unit->fnGetModuleIdx());
}

FMS_ERRORCODE CFM_ST_AT_TRAY_IN::fnFMSPorcess(FMS_COMMAND _msgId, st_FMS_PACKET* _recvData)
{
	FMS_ERRORCODE _error = ER_NO_Process;

	//입고완료
	switch(_msgId)
	{
	case E_CHARGER_IN:
		{
			_error = ER_Data_Error;

			if(sizeof(st_CHARGER_IN) == _recvData->dataLen)
			{
				st_CHARGER_IN data;
				memset(&data, 0x20, sizeof(st_CHARGER_IN));
				m_Unit->fnGetFMS()->strCutter(_recvData->data, &data
					, g_iCHARGER_IN_Map);

				_error = ER_TrayID;

				if(strncmp(data.Tray_ID, m_Unit->fnGetFMS()->fnGetTrayID(), 7) == 0)
				{
					_error = ER_Run_Fail;
					
					if(m_Unit->fnGetFMS()->fnRun())
					{
						_error = FMS_ER_NONE;
					}
				}
			}
		}
		break;

	default:
		{
		}
		break;
	}

	TRACE("CFM_ST_AT_TRAY_IN::fnFMSPorcess %d \n", m_Unit->fnGetModuleIdx());

	return _error;

}