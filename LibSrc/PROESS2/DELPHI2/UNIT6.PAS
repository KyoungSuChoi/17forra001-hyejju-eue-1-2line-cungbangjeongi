unit Unit6;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Pegvcl, Pegrpapi;

type
  TForm6 = class(TForm)
    PEGraph1: PEGraph;
    Timer1: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form6: TForm6;

implementation

{$R *.DFM}

procedure TForm6.FormCreate(Sender: TObject);
begin

PEGraph1.Subsets := 2;
PEGraph1.Points := 200;
PEGraph1.PointsToGraph := 20;
PEGraph1.PointsToGraphInit := gLast;
PEGraph1.MainTitle := 'Graph Real Time Example';
PEGraph1.SubTitle := '';
PEGraph1.PrepareImages := True;

PEGraph1.ManualScaleControlY := gManualMinAndMax;
PEGraph1.ManualMaxY := 100;
PEGraph1.ManualMinY := 1;
PEGraph1.ManualMaxDataString := '000.000';
PEGraph1.ManualMaxPointLabel := '00:00:00 ';
PEGraph1.NoStackedData := True;
PEGraph1.NoRandomPointsToGraph := True;
PEGraph1.AllowHistogram := False;
PEGraph1.FocalRect := False;
PEGraph1.GridLineControl := gNoGrid;
PEGraph1.DataPrecision := gOneDecimal;

{**Needed to allocate point labels so append logic works**}
PEGraph1.PointLabels[199] := ' ';

{**Clear out first four data points**}
PEGraph1.YData[0, 0] := 0;
PEGraph1.YData[0, 1] := 0;
PEGraph1.YData[0, 2] := 0;
PEGraph1.YData[0, 3] := 0;

PEGraph1.PEactions := gReinitAndReset;

end;

procedure TForm6.Timer1Timer(Sender: TObject);
var
   CurrentTime: String;
   NewData: array[0 .. 2] of Single;
   tmpszstr: array[0 .. 48] of Char;
begin
   {'********************************************'
    '** IMPORTANT FOR REALTIME IMPLEMENTATIONS **'
    '********************************************'
    ' Be sure to set ManualScaleControl,
    '   ManualMaxY, and ManualMinY
    ' Be sure to set ManualMaxDataString
    ' Be sure to set ManualMaxPointLabel
    ' Be sure to set NoStackedData to TRUE
    ' Be sure to set NoRandomPointsToGraph to TRUE
    ' Be sure to set AllowHistogram to FALSE
    ' Be sure to use PEP_szaAPPENDPOINTLABELDATA
    '   before PEP_faAPPENDYDATA
    '********************************************'
    '********************************************' }

    {'** new point label **'}
    CurrentTime := FormatDateTime('hh:nn:ss', Time);
    StrPCopy(tmpszstr, CurrentTime);

    {'** new YData **'}
    NewData[0] := Random(20) + 2;
    NewData[1] := Random(40) + 60;

    {'** transfer new point label **'}
    PEvset(PEGraph1.hObject, PEP_szaAPPENDPOINTLABELDATA, @tmpszstr, 1);

    {'** transfer new YData **'
    '** this will also update and view new image **'}
    PEvset(PEGraph1.hObject, PEP_faAPPENDYDATA, @NewData, 1);

end;

procedure TForm6.FormShow(Sender: TObject);
begin
Timer1.Enabled := True;
end;

procedure TForm6.FormClose(Sender: TObject; var Action: TCloseAction);
begin
Timer1.Enabled := False;
end;

end.
