#if !defined(AFX_MODULESETDLG_H__58E5BC6D_ADD7_460D_A938_1A104F48697F__INCLUDED_)
#define AFX_MODULESETDLG_H__58E5BC6D_ADD7_460D_A938_1A104F48697F__INCLUDED_

#include "CTSMonDoc.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ModuleSetDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CModuleSetDlg dialog

class CModuleSetDlg : public CDialog
{
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();
	virtual ~CModuleSetDlg();

// Construction
public:
	BOOL UpdateSaveInterval(int nModuleID);
	void SetCurrentItem(HTREEITEM hItem);
	int m_nNewModuleID;
	BOOL m_bChanged;
	int m_nSelModuleID;
	CCTSMonDoc *m_pDoc;
	CModuleSetDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CModuleSetDlg)
	enum { IDD = IDD_MODULE_SET_DLG };
	CLabel	m_ctrlModuleIPLabel;
	CLabel	m_ctrlModuleLabel;
	CLabel	m_ctrlTotalCh;
	CComboBox	m_ctrlGroupList;
	CLabel	m_ctrlChInGroup;
	CLabel	m_ctrlModuleIP;
	CLabel	m_ctrlModuleID;
//	CString	m_fVSpec;
	//}}AFX_DATA
	SECTreeCtrl m_TreeX;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CModuleSetDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	float m_fVSpec;
	float m_fISpec;
	UINT m_nTrayType;
	UINT m_nTrayColCnt;
	BOOL m_bTrayChange;

	// Generated message map functions
	//{{AFX_MSG(CModuleSetDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnGetdispinfoModuleTree(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSelchangedModuleTree(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnAddModule();
	afx_msg void OnDeleteModule();
	afx_msg void OnCalFileUpdate();
	afx_msg void OnShutDown();
	afx_msg void OnSelchangeGroupListCombo();
	afx_msg void OnOk();
	afx_msg void OnClickModuleTree(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBtnSave();
	afx_msg void OnBtnEdit();
	afx_msg void OnModifyModule();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MODULESETDLG_H__58E5BC6D_ADD7_460D_A938_1A104F48697F__INCLUDED_)
