// SelUnitDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ctsmon.h"
#include "SelUnitDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelUnitDlg dialog


CSelUnitDlg::CSelUnitDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelUnitDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSelUnitDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	LanguageinitMonConfig();
}

CSelUnitDlg::~CSelUnitDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CSelUnitDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CSelUnitDlg"), _T("TEXT_CSelUnitDlg_CNT"), _T("TEXT_CSelUnitDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CSelUnitDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CSelUnitDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}


void CSelUnitDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelUnitDlg)
	DDX_Control(pDX, IDC_COMBO_UNIT, m_ctrlUnit);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelUnitDlg, CDialog)
	//{{AFX_MSG_MAP(CSelUnitDlg)
	ON_CBN_SELCHANGE(IDC_COMBO_UNIT, OnSelchangeComboUnit)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, &CSelUnitDlg::OnBnClickedOk)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelUnitDlg message handlers

BOOL CSelUnitDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	int nDefaultIndex = 0;
	m_ctrlUnit.SetCurSel(nDefaultIndex);

	long rtn;
	HKEY hKey = 0;
	BYTE buf[512];
	DWORD size = 511;
	DWORD type;
	CString strTemp, strDest, strDBPath;
	rtn = ::RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\CTSMon\\Path", 0, KEY_READ, &hKey);
	//Note Found 
	if(ERROR_SUCCESS == rtn)
	{
		//Load PowerFormation DataBase  Path
		size = 511;
		rtn = ::RegQueryValueEx(hKey, "DataBase", NULL, &type, buf, &size);
		::RegCloseKey(hKey);
		
		//
		if(ERROR_SUCCESS == rtn && (type == REG_SZ || type == REG_EXPAND_SZ))
		{
			strDBPath.Format("%s\\%s", buf, FORM_SET_DATABASE_NAME);
			
			CString address;
			CDaoDatabase  db;
			db.Open(strDBPath);

			CString strSQL;
			strSQL = "SELECT data3, ModuleID FROM SystemConfig ORDER BY ModuleID";
			try
			{
				CDaoRecordset rs(&db);
				
				rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
				int a = 0;
				while(!rs.IsEOF())
				{
					COleVariant data = rs.GetFieldValue(0);
					address = data.pbVal;
					data = rs.GetFieldValue(1);
					m_ctrlUnit.AddString(address);

					if(m_nModuleID == data.lVal)
					{
						nDefaultIndex =  a;
					}
					m_ctrlUnit.SetItemData( a++, data.lVal);
					rs.MoveNext();
				}
				rs.Close();
				db.Close();
			} 
			catch (CDaoException *e)
			{
				e->Delete();
			}	
		}
		else
		{
			AfxMessageBox(TEXT_LANG[0]);//"CTS 설치 정보를 찾을 수 없습니다."
		}
	}
	else
	{
		AfxMessageBox(TEXT_LANG[0]);//"CTS 설치 정보를 찾을 수 없습니다."
	}

	m_ctrlUnit.SetCurSel(nDefaultIndex);

	GetDlgItem(IDC_STATIC_TITLE)->SetWindowText(m_strTitle);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

int CSelUnitDlg::GetModuleID()
{
	return m_nModuleID;
}

void CSelUnitDlg::SetModuleID(int nModuleID)
{
	m_nModuleID = nModuleID;
}

void CSelUnitDlg::OnSelchangeComboUnit() 
{
	// TODO: Add your control notification handler code here
	int index = m_ctrlUnit.GetCurSel();
	if(index >= 0 )
	{
		m_nModuleID = m_ctrlUnit.GetItemData(index);
	}
}

void CSelUnitDlg::SetTitle(CString str)
{
	m_strTitle = str;
}

void CSelUnitDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnOK();
}
