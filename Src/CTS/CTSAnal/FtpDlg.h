#if !defined(AFX_FTPDLG1_H__F913E8D6_9A4E_4CB4_B78F_F730075C8826__INCLUDED_)
#define AFX_FTPDLG1_H__F913E8D6_9A4E_4CB4_B78F_F730075C8826__INCLUDED_

#include "CTSAnalDoc.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FtpDlg1.h : header file
//
#define MODULENUM    32
#define CHANNELNUM   128
/////////////////////////////////////////////////////////////////////////////
// CFtpDlg dialog

class CFtpDlg : public CDialog
{
// Construction
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	CFtpDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CFtpDlg();

	CImageList       *m_imagelist;
	CImageList       *m_imageTree;
	CImageList       *m_imageCombo;
	HTREEITEM        hCurItem;
	CString          m_IpAddress;
	CString          m_CurDir;         //현재디렉토리
	CString          m_beforeDir;
	CString          m_CurDrive;       //현재드라이브
	CInternetSession session;          //인터넷 세션 클래스
	CFtpConnection   *connect;         //Ftp 명령 연결 컨트롤 클래스
	int              m_nModuleNo;
	int              m_nChNo;
	int              m_nModuleNum;
	void             InitImgList();
	void             InitDlg();
	void             InitList();
	void             FtpFileFind();
	void             ViewListCtrl(CString& Dir);
	void             FileViewListCtrl(CString& Dir);
	void             DriveInitialize();
	BOOL             AddDriveNode(CString& strDrive);
	CString GetIPAddress(int nModuleID);

public:
	int m_nInstallModule;
	int GetModuleTotModule();
	BOOL m_bUseGroupSet;
	BOOL m_bUseRackIndex;
	int m_nModulePerRack;
	CString m_strGroupName;
	CString m_strModuleName;
	CString ModuleName(int nModuleID, int nGroupIndex = -1);
	BOOL DownFile(int nData);
	BOOL CloseSession();
	

// Dialog Data
	//{{AFX_DATA(CFtpDlg)
	enum { IDD = IDD_FTP_DLG };
	CComboBox	m_pDriveCombo;
	CListCtrl	m_List;
	CTreeCtrl	m_treeCtrl;
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFtpDlg)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON   m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CFtpDlg)
	afx_msg void OnSelchangedTree1(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDown();
	afx_msg void OnTotDown();
	afx_msg void OnItemchangedList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSelchangeCombo();
	afx_msg void OnDblclkList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnIpaddressSet();
	afx_msg void OnClose();
	afx_msg void OnStepDataDown();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FTPDLG1_H__F913E8D6_9A4E_4CB4_B78F_F730075C8826__INCLUDED_)
