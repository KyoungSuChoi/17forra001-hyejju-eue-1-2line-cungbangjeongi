#pragma once
#include "ColorButton2.h"
#include "afxwin.h"


// CInfomationDlg 대화 상자입니다.
#define EDIT_FONT_SIZE 24

class CInformationDlg : public CDialog
{
	DECLARE_DYNAMIC(CInformationDlg)

public:
	CInformationDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CInformationDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_SHOW_WANNING_DLG };
	
public:	
	void InitFont();
	void SetResultMsg( CString strMsg, int nMsgType = INFO_TYPE_NORMAL );
	int GetUnitNum();
	VOID SetUnitNum( int nUnitNum );

private:
	CFont font;
	int m_nModuleID;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	virtual BOOL OnInitDialog();
	CString m_strResultMsg;
	CLabel m_LabelStageName;
	CColorButton2 m_BtnOK;
};
