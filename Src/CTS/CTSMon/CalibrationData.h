// CalibrationData.h: interface for the CCalibrationData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CALIBRATIONDATA_H__A89BD6CE_1CAC_415B_9EBD_7110F98EC6A6__INCLUDED_)
#define AFX_CALIBRATIONDATA_H__A89BD6CE_1CAC_415B_9EBD_7110F98EC6A6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "CaliPoint.h"
#include "ChCaliData.h"


#define CAL_FILE_ID		20090909	

typedef struct tag_CAL_FILE_ID_HEADER {
	UINT	nFileID;
	UINT	nFileVersion;
	char	szCreateDateTime[64];
	char	szDescrition[128];
	char	szUserName[64];
	char	szReserved[64];
}	CAL_FILE_ID_HEADER, *LPCAL_FILE_ID_HEADER;


class CCalibrationData  
{
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	void SetUserName(CString strUser);
	CString GetUserName();
	int GetHWChCount(int nBoardNo);
	CCaliPoint * GetCalPointData()	{return &m_PointData;	}
	CChCaliData * GetChCalData(int nChIndex);
	BOOL	IsLoadedData();
	void	SetState(int state)	{ m_nState = state;	}
	void	 SetEdited();
	int		GetState();
	CString GetUpdateTime();
	BOOL	SaveDataToFile(CString strFileName, CWordArray *pawSelMonCh = NULL);
	BOOL	LoadDataFromFile(CString strFileName);
	CCalibrationData();
	virtual ~CCalibrationData();

	CCaliPoint	m_PointData;		//현재 모듈에 설정된 Calibration 조건 
	CChCaliData	m_ChCaliData[EP_MAX_CH_PER_MD];		//채널의 교정값 

private:
	int m_nState;						//현재 Data의 상태 (Empty/Saved/Modify/...)
	CString m_strCalTime;
protected:
	CString m_strUserName;
};

#endif // !defined(AFX_CALIBRATIONDATA_H__A89BD6CE_1CAC_415B_9EBD_7110F98EC6A6__INCLUDED_)
