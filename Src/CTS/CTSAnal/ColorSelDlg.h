#if !defined(AFX_COLORSELDLG_H__C86D0847_B70E_4ADF_8EE8_6E48EF683CCE__INCLUDED_)
#define AFX_COLORSELDLG_H__C86D0847_B70E_4ADF_8EE8_6E48EF683CCE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ColorSelDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CColorSelDlg dialog

class CColorSelDlg : public CDialog
{
// Construction
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	COLORREF m_Color;
	CColorSelDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CColorSelDlg();

// Dialog Data
	//{{AFX_DATA(CColorSelDlg)
	enum { IDD = IDD_COLOR_SEL_DIALOG };
	stingray::foundation::SECWellButton	m_btnColor;
	CString	m_strCode;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CColorSelDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CColorSelDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnButton1();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COLORSELDLG_H__C86D0847_B70E_4ADF_8EE8_6E48EF683CCE__INCLUDED_)
