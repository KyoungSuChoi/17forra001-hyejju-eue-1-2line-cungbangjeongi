#if !defined(AFX_CALISETDLG_H__6B299BD6_50B7_44EC_B0AF_BBAA0837BD06__INCLUDED_)
#define AFX_CALISETDLG_H__6B299BD6_50B7_44EC_B0AF_BBAA0837BD06__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CaliSetDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCaliSetDlg dialog
#include "ListEditCtrl.h"
#include "CaliPoint.h"
#include "CTSMonDoc.h"

class CCaliSetDlg : public CDialog
{
// Construction
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();
	virtual ~CCaliSetDlg();

	void UpdatePointData();
	CCaliSetDlg(CCaliPoint* pCalPoint, int nMode = 0, CWnd* pParent = NULL);
	void LoadCalConfigFile();
	BOOL SaveAll();
	void InitLabel();
	void fnUpdatePreset();
	BOOL fnLoadPreset( int nSelNum );
	BOOL fnSavePreset( int nSelNum );
//	void Sorting(CAL_POINT& aCalPoint);

	int m_nSelPresetNum;
	int m_nMode;
	CCaliPoint* m_pCalPoint;
	CCTSMonDoc* m_pdoc;
// Dialog Data
	//{{AFX_DATA(CCaliSetDlg)
	enum { IDD = IDD_CALI_SET_DLG };
	CLabel	m_Label1;
	CListEditCtrl	m_wndShuntT;
	CListEditCtrl	m_wndShuntR;
	CListEditCtrl	m_wndShuntSerial;
	CComboBox	m_ctrlICheck;
	CComboBox	m_ctrlVCheck;
	CComboBox	m_ctrlISet;
	CComboBox	m_ctrlVSet;
	CListEditCtrl	m_wndListPoint1;
	CListEditCtrl	m_wndListPoint2;
	CListEditCtrl	m_wndListCheck1;
	CListEditCtrl	m_wndListCheck2;
	BOOL m_bCaliPoint;
	BOOL m_bCheckPoint;
	CButton	m_ctrlSetCali;
	CButton	m_ctrlCheckCali;
	//}}AFX_DATA
	CLabel	m_cutionLabel;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCaliSetDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CToolTipCtrl m_toolTip;
	int	m_nVCalRange;	
	int	m_nVCheckRange;	
	int	m_nICalRange;	
	int	m_nICheckRange;	

	// Generated message map functions
	//{{AFX_MSG(CCaliSetDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnSelchangeVSetCombo();
	afx_msg void OnSelchangeVCheckCombo();
	afx_msg void OnSelchangeISetCombo();
	afx_msg void OnSelchangeICheckCombo();
	afx_msg void OnCalipoint();
	afx_msg void OnCheckpoint();
	afx_msg void OnButtonLoad();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtnPreset1();
	afx_msg void OnBnClickedBtnPreset2();
	afx_msg void OnBnClickedBtnPreset3();
	afx_msg void OnBnClickedBtnPreset4();
	afx_msg void OnBnClickedBtnPresetSave();
	afx_msg void OnBnClickedBtnPresetLoad();
	afx_msg void OnBnClickedOk();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CALISETDLG_H__6B299BD6_50B7_44EC_B0AF_BBAA0837BD06__INCLUDED_)
