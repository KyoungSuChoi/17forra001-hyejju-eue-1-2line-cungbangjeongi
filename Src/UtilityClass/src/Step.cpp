// Step.cpp: implementation of the CStep class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Grading.h"
#include "Step.h"
//#include "EPProcType.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CStep::CStep(int nStepType /*=0*/)
{
	ClearStepData();
	m_type = (BYTE)nStepType;
}

CStep::CStep()
{
	ClearStepData();
}

CStep::~CStep()
{

}

BOOL CStep::ClearStepData()
{
	m_bOverCChk = FALSE; //20200411 엄륭 용량 상한값 관련
	m_type = 0;
	m_StepIndex = 0;
	m_mode = 0;
//	m_bGrade = FALSE;
	m_lProcType = EP_PROC_TYPE_NONE;				
	m_fVref = 0;
    m_fIref = 0;
    
	m_fEndTime = 0;
    m_fEndV = 0;
    m_fEndI = 0;
    m_fEndC = 0;
    m_fEndDV = 0;
    m_fEndDI = 0;

	m_fHighLimitV = 0;
    m_fLowLimitV = 0;
    m_fHighLimitI = 0;
    m_fLowLimitI = 0;
    m_fHighLimitC = 0;
    m_fLowLimitC = 0;
	m_fHighLimitImp = 0;
    m_fLowLimitImp = 0;
	for(INDEX i=0; i<EP_COMP_POINT; i++)
	{
		m_fCompTimeV[i] = 0;			
		m_fCompVLow[i] = 0;
		m_fCompVHigh[i] = 0;
		m_fCompTimeI[i] = 0;			
		m_fCompILow[i] = 0;
		m_fCompIHigh[i] = 0;
	}
    m_fDeltaTimeV = 0;
    m_fDeltaV = 0;
    m_fDeltaTimeI = 0;
    m_fDeltaI = 0;

	m_bUseActucalCap = FALSE;
	m_lCapaRefStepIndex = 0;
	m_fSocRate = 0.0f;

	m_lCycleNo = 0;
	m_lGotoStep = 0;

	m_Grading.ClearStep();
	m_bSkipStep = FALSE;
	m_StepID = 0;

	m_lRecVIGetTime = 0.0f;
	m_fRecDeltaTime = 0.0f;
	m_fRecDeltaV = 0.0f;
	m_fRecDeltaI = 0.0f;
	m_fRecDeltaT = 0.0f;

	m_fParam1 = 0.0f;		//parameter 1  ex) EDLC : 용량측정 전압1(Low) , etc
	m_fParam2 = 0.0f;		//parameter 1  ex) EDLC : 용량측정 전압2(High) , etc
	
	m_lDCIR_RegTemp = 0;
	m_lDCIR_ResistanceRate = 0;
	m_nFanOffFlag = 0;

	m_fCompChgCcVtg = 0.0f;
	m_fCompChgCcTime = 0;
	m_fCompChgCcDeltaVtg = 0.0f;

	m_fCompChgCvCrt = 0.0f;
	m_fCompChgCvTime = 0;
	m_fCompChgCvDeltaCrt = 0.0f;

	return TRUE;
}

/*
void CStep::operator=(CStep &step) 
{
	m_type = step.m_type;
	m_StepIndex = step.m_StepIndex;
	m_mode = step.m_mode;
//	m_bGrade = step.m_bGrade;
	m_lProcType = step.m_lProcType;				
	m_fVref = step.m_fVref;
    m_fIref = step.m_fIref;
    
	m_fEndTime = step.m_fEndTime;
    m_fEndV = step.m_fEndV;
    m_fEndI = step.m_fEndI;
    m_fEndC = step.m_fEndC;
    m_fEndDV = step.m_fEndDV;
    m_fEndDI = step.m_fEndDI;

	m_fHighLimitV = step.m_fHighLimitV;
    m_fLowLimitV = step.m_fLowLimitV;
    m_fHighLimitI = step.m_fHighLimitI;
    m_fLowLimitI = step.m_fLowLimitI;
    m_fHighLimitC = step.m_fHighLimitC;
    m_fLowLimitC = step.m_fLowLimitC;
	m_fHighLimitImp = step.m_fHighLimitImp;
    m_fLowLimitImp = step.m_fLowLimitImp;
	for(INDEX i=0; i<EP_COMP_POINT; i++)
	{
		m_fCompTimeV[i] = step.m_fCompTimeV[i];			
		m_fCompVLow[i] = step.m_fCompVLow[i];
		m_fCompVHigh[i] = step.m_fCompVHigh[i];
		
		m_fCompTimeI[i] = step.m_fCompTimeI[i];			
		m_fCompILow[i] = step.m_fCompILow[i];
		m_fCompIHigh[i] = step.m_fCompIHigh[i];
	}
    m_fDeltaTimeV = step.m_fDeltaTimeV;
    m_fDeltaV = step.m_fDeltaV;
    m_fDeltaTimeI = step.m_fDeltaTimeI;
    m_fDeltaI = step.m_fDeltaI;

	m_bUseActucalCap = step.m_bUseActucalCap;
	m_lCapaRefStepIndex = step.m_lCapaRefStepIndex;
	m_fSocRate = step.m_fSocRate;

	m_lCycleNo = step.m_lCycleNo;
	m_lGotoStep = step.m_lGotoStep;


	m_Grading.SetGradingData(step.m_Grading);

	m_bSkepStep = step.m_bSkepStep;
	m_StepID = step.m_StepID;

/*	GRADE_STEP data;
	m_Grading.ClearStep();
	for(i=0; i<step.m_Grading.GetGradeStepSize(); i++)
	{
		data = step.m_Grading.GetStepData(i);
		m_Grading.AddGradeStep(data.lMin, data.lMax, data.strCode);
	}
*/
//}


STR_COMMON_STEP CStep::GetStepData()
{
	STR_COMMON_STEP	stepData;
	ZeroMemory(&stepData, sizeof(STR_COMMON_STEP));

	int i = 0;
	
	stepData.stepHeader.gradeSize = (BYTE)m_Grading.GetGradeStepSize();
	stepData.stepHeader.mode = (BYTE)m_mode;
	stepData.stepHeader.stepIndex = m_StepIndex;//BYTE(m_No-1);
	stepData.stepHeader.type = (BYTE)m_type;
	stepData.stepHeader.nProcType = m_lProcType;

	stepData.fVref = m_fVref;
    stepData.fIref = m_fIref;
    
	stepData.fEndTime = m_fEndTime;
    stepData.fEndV = m_fEndV;
    stepData.fEndI = m_fEndI;
    stepData.fEndC = m_fEndC;
    stepData.fEndDV = m_fEndDV;
    stepData.fEndDI = m_fEndDI;

	stepData.fOverV = m_fHighLimitV;
    stepData.fLimitV = m_fLowLimitV;
    stepData.fOverI = m_fHighLimitI;
    stepData.fLimitI = m_fLowLimitI;
    stepData.fOverC = m_fHighLimitC;
    stepData.fLimitC = m_fLowLimitC;
	stepData.fOverImp = m_fHighLimitImp;
	stepData.fLimitImp = m_fLowLimitImp;

	for(i =0; i<EP_COMP_POINT; i++)
	{
		stepData.fCompTimeV[i] = m_fCompTimeV[i];			
		stepData.fCompVLow[i] = m_fCompVLow[i];
		stepData.fCompVHigh[i] = m_fCompVHigh[i];
		stepData.fCompTimeI[i] = m_fCompTimeI[i];			
		stepData.fCompILow[i] = m_fCompILow[i];
		stepData.fCompIHigh[i] = m_fCompIHigh[i];
	}
	
    stepData.fDeltaV = m_fDeltaV;
    stepData.fDeltaTimeI = m_fDeltaTimeI;
    stepData.fDeltaI = m_fDeltaI;
    stepData.fDeltaTimeV = m_fDeltaTimeV;

	stepData.bUseActucalCap = m_bUseActucalCap;
	stepData.bReserved = m_lCapaRefStepIndex;
	stepData.fSocRate = m_fSocRate;

	stepData.nLoopInfoCycle = m_lCycleNo;
	stepData.nLoopInfoGoto =  m_lGotoStep;

	GRADE_STEP data;
	for(i =0; i<m_Grading.GetGradeStepSize(); i++)
	{
		data = m_Grading.GetStepData(i);
		stepData.grade[i].index = (BYTE)i;
		if(!data.strCode.IsEmpty())
		{
			stepData.grade[i].gradeCode = atoi(data.strCode);
		}
		stepData.grade[i].fMin = data.fMin;
		stepData.grade[i].fMax = data.fMax;
		stepData.grade[i].gradeItem = data.lGradeItem;
	}

	stepData.fRecDeltaTime = m_fRecDeltaTime;    	
	stepData.fRecDeltaV = m_fRecDeltaV;
	stepData.fRecDeltaI = m_fRecDeltaI;
	stepData.fRecDeltaT = m_fRecDeltaT;
	stepData.fRecVIGetTime = m_lRecVIGetTime;

	stepData.fParam1 = m_fParam1;		//parameter 1  ex) EDLC : 용량측정 전압1(Low) , etc
	stepData.fParam2 = m_fParam2;		//parameter 1  ex) EDLC : 용량측정 전압2(High) , etc
	
	stepData.fmsType = m_fmsType;

	stepData.lDCIR_RegTemp = m_lDCIR_RegTemp;
	stepData.lDCIR_ResistanceRate = m_lDCIR_ResistanceRate;
	stepData.m_nFanOffFlag = m_nFanOffFlag;

	stepData.fCompChgCcVtg = m_fCompChgCcVtg;
	stepData.fCompChgCcTime = m_fCompChgCcTime;
	stepData.fCompChgCcDeltaVtg = m_fCompChgCcDeltaVtg;
	stepData.fCompChgCvCrt = m_fCompChgCvCrt;
	stepData.fCompChgCvTime = m_fCompChgCvTime;
	stepData.fCompChgCvDeltaCrt = m_fCompChgCvDeltaCrt;

	return stepData;
}

BOOL CStep::SetStepData(STR_COMMON_STEP *pStep)
{
	ClearStepData();

	//	m_lProcType = 

//	 m_bGrade = pStep->stepHeader.bGrade;
	 m_mode = pStep->stepHeader.mode;
	 m_StepIndex = pStep->stepHeader.stepIndex;
	 m_type = pStep->stepHeader.type;
	 m_lProcType = pStep->stepHeader.nProcType;
	 m_fVref = pStep->fVref;
     m_fIref = pStep->fIref;
    
	 m_fEndTime = pStep->fEndTime;
     m_fEndV = pStep->fEndV;
     m_fEndI = pStep->fEndI;
     m_fEndC = pStep->fEndC;
     m_fEndDV = pStep->fEndDV;
     m_fEndDI = pStep->fEndDI;

	 m_fHighLimitV = pStep->fOverV;
     m_fLowLimitV= pStep->fLimitV;
     m_fHighLimitI= pStep->fOverI;
     m_fLowLimitI= pStep->fLimitI;
     m_fHighLimitC = pStep->fOverC;
     m_fLowLimitC = pStep->fLimitC;
	 m_fHighLimitImp = pStep->fOverImp;
	 m_fLowLimitImp = pStep->fLimitImp;

	 memcpy( m_fCompTimeV, pStep->fCompTimeV, sizeof(pStep->fCompTimeV));			
	 memcpy( m_fCompVLow, pStep->fCompVLow, sizeof(pStep->fCompVLow));
	 memcpy( m_fCompVHigh, pStep->fCompVHigh, sizeof(pStep->fCompVHigh));
	 memcpy( m_fCompTimeI, pStep->fCompTimeI, sizeof(pStep->fCompTimeI));			
	 memcpy( m_fCompILow, pStep->fCompILow, sizeof(pStep->fCompILow));
	 memcpy( m_fCompIHigh, pStep->fCompIHigh, sizeof(pStep->fCompIHigh));

     m_fDeltaTimeV= pStep->fDeltaTimeV;
     m_fDeltaV = pStep->fDeltaV;
     m_fDeltaTimeI = pStep->fDeltaTimeI;
     m_fDeltaI = pStep->fDeltaI;

	m_bUseActucalCap = pStep->bUseActucalCap ;
	m_lCapaRefStepIndex = pStep->bReserved ;
	m_UseStepContinue = pStep->UseStepContinue;		//ljb 2009-01 
	m_fSocRate = pStep->fSocRate;

	m_lCycleNo	 = pStep->nLoopInfoCycle ;
	m_lGotoStep = pStep->nLoopInfoGoto  ;
	 
	 //= step.lTmv;
	 //= step.ulReceTime;
	 //= step.aDataID[EP_RESULT_ITEM_NO];
	//		fReserved[16];

	m_fRecDeltaTime = pStep->fRecDeltaTime;
	m_fRecDeltaV = pStep->fRecDeltaV;
	m_fRecDeltaI = pStep->fRecDeltaI;
	m_fRecDeltaT = pStep->fRecDeltaT;
	m_lRecVIGetTime = pStep->fRecVIGetTime;

	m_fParam1 = pStep->fParam1;		//parameter 1  ex) EDLC : 용량측정 전압1(Low) , etc
	m_fParam2 = pStep->fParam2;		//parameter 1  ex) EDLC : 용량측정 전압2(High) , etc
	
	m_fmsType = pStep->fmsType;

	m_lDCIR_RegTemp = pStep->lDCIR_RegTemp;
	m_lDCIR_ResistanceRate = pStep->lDCIR_ResistanceRate;
	m_nFanOffFlag = pStep->m_nFanOffFlag;

	int i = 0;
	CString strCode = _T("");

	for(i =0; i<pStep->stepHeader.gradeSize; i++)
	{
		strCode.Format("%d",pStep->grade[i].gradeCode);
		m_Grading.AddGradeStep(pStep->grade[i].gradeItem, pStep->grade[i].fMin, pStep->grade[i].fMax, strCode);
	}

	m_fCompChgCcVtg = pStep->fCompChgCcVtg;
	m_fCompChgCcTime = pStep->fCompChgCcTime;
	m_fCompChgCcDeltaVtg = pStep->fCompChgCcDeltaVtg;
	m_fCompChgCvCrt = pStep->fCompChgCvCrt;
	m_fCompChgCvTime = pStep->fCompChgCvTime;
	m_fCompChgCvDeltaCrt = pStep->fCompChgCvDeltaCrt;

	return TRUE;
}

BOOL CStep::GetNetStepData(LPVOID &lpData, int &rSize, int nProcessNo) //20200411 엄륭 프로세스 넘버 
{	
	switch(m_type)
	{
	case EP_TYPE_CHARGE		:
		{
			rSize = sizeof(EP_CHARGE_STEP);	
			EP_CHARGE_STEP *pStep;
			pStep = new EP_CHARGE_STEP;
			ZeroMemory(pStep, sizeof(EP_CHARGE_STEP));
			
			pStep->stepHeader.type = m_type;
			pStep->stepHeader.stepIndex = m_StepIndex;
			pStep->stepHeader.nProcType = m_lProcType;
			pStep->stepHeader.mode = m_mode;
			pStep->stepHeader.gradeSize = IsGradeInclude();		//SBC에는 Grading 유무만 전달 

			pStep->lVref  = ExpFloattoLong(m_fVref, EP_VTG_FLOAT);
			pStep->lIref  = ExpFloattoLong(m_fIref, EP_CRT_FLOAT);
    
			pStep->ulEndTime =  PC2MDTIME(m_fEndTime);
			pStep->lEndV  = ExpFloattoLong(m_fEndV, EP_VTG_FLOAT);
			pStep->lEndI  = ExpFloattoLong(m_fEndI, EP_CRT_FLOAT);
			pStep->lEndC  = ExpFloattoLong(m_fEndC, EP_ETC_FLOAT);
			pStep->lEndDV  = ExpFloattoLong(m_fEndDV, EP_VTG_FLOAT);
			pStep->lEndDI  = ExpFloattoLong(m_fEndDI, EP_CRT_FLOAT);

			pStep->bUseActucalCap = m_bUseActucalCap;		//flag of soc rate cut off reference capaciy
			pStep->bReserved =  m_lCapaRefStepIndex;		//step no of capacity reference step no
			pStep->wSocRate = ExpFloattoLong(m_fSocRate, EP_ETC_FLOAT);		//wSoc rate (%)*100

			pStep->lOverV =		ExpFloattoLong(m_fHighLimitV, EP_VTG_FLOAT);
			pStep->lLimitV =	ExpFloattoLong(m_fLowLimitV, EP_VTG_FLOAT);
			pStep->lOverI =		ExpFloattoLong(m_fHighLimitI, EP_CRT_FLOAT);
			pStep->lLimitI =	ExpFloattoLong(m_fLowLimitI, EP_CRT_FLOAT);
			pStep->lOverC =		ExpFloattoLong(m_fHighLimitC, EP_ETC_FLOAT);
			if(pStep->lOverC == 0) //20200411 엄륭 용량 값 0 확인
			{
				m_bOverCChk = TRUE;
				return FALSE;
			}

			pStep->lLimitC =	ExpFloattoLong(m_fLowLimitC, EP_ETC_FLOAT);

			for(int i =0; i<EP_COMP_POINT; i++)
			{
				pStep->lCompTimeV[i] = PC2MDTIME(m_fCompTimeV[i]);
				pStep->lCompVLow[i]  = ExpFloattoLong(m_fCompVLow[i], EP_VTG_FLOAT);
				pStep->lCompVHigh[i] = ExpFloattoLong(m_fCompVHigh[i], EP_VTG_FLOAT);

				pStep->lCompTimeI[i] = PC2MDTIME(m_fCompTimeI[i]);
				pStep->lCompILow[i]  = ExpFloattoLong(m_fCompILow[i], EP_CRT_FLOAT);
				pStep->lCompIHigh[i] = ExpFloattoLong(m_fCompIHigh[i], EP_CRT_FLOAT);
			}
			
			pStep->lDeltaTime = PC2MDTIME(m_fDeltaTimeV);
			pStep->lDeltaV = ExpFloattoLong(m_fDeltaV, EP_VTG_FLOAT);
			pStep->lDeltaTime1 = PC2MDTIME(m_fDeltaTimeI);
			pStep->lDeltaI = ExpFloattoLong(m_fDeltaI, EP_VTG_FLOAT);
			
			//pStep->lTmv;
			//pStep->ulReceTime;
			//pStep->aDataID[EP_RESULT_ITEM_NO];

			pStep->lRecDeltaTime = PC2MDTIME(m_fRecDeltaTime);			
			pStep->lRecDeltaV = ExpFloattoLong(m_fRecDeltaV, EP_VTG_FLOAT);
			pStep->lRecDeltaI = ExpFloattoLong(m_fRecDeltaI, EP_CRT_FLOAT);
			pStep->lRecDeltaT = ExpFloattoLong(m_fRecDeltaT, EP_ETC_FLOAT);

			pStep->lParam1 = ExpFloattoLong(m_fParam1, EP_VTG_FLOAT);		//parameter 1  ex) EDLC : 용량측정 전압1(Low) , etc
			pStep->lParam2 = ExpFloattoLong(m_fParam2, EP_VTG_FLOAT);		//parameter 1  ex) EDLC : 용량측정 전압2(High) , etc

			//ljb 2009-01 
			pStep->UseStepContinue = m_UseStepContinue;
			//2007/2/16
			pStep->aDataID[0] = 1;											//Oven 가동 여부 

			// 20150422
			pStep->nFanOffFlag = m_nFanOffFlag;							// 20150312 FanOff
			
			// 2013/8/24 kky
			pStep->lRecVIGetTime = PC2MDTIME(m_lRecVIGetTime);

			pStep->lCompChgCcVtg = ExpFloattoLong(m_fCompChgCcVtg, EP_VTG_FLOAT);
			pStep->ulCompChgCcTime = PC2MDTIME(m_fCompChgCcTime);
			pStep->lCompChgCcDeltaVtg = ExpFloattoLong(m_fCompChgCcDeltaVtg, EP_VTG_FLOAT);
			pStep->lCompChgCvCrt = ExpFloattoLong(m_fCompChgCvCrt, EP_CRT_FLOAT);
			pStep->ulCompChgCvtTime = PC2MDTIME(m_fCompChgCvTime);
			pStep->lCompChgCvDeltaCrt = ExpFloattoLong(m_fCompChgCvDeltaCrt, EP_CRT_FLOAT);
			
			pStep->lProcessType  = nProcessNo; //20201214 ksj
			lpData = pStep;
			break;
		}
	case EP_TYPE_DISCHARGE	:
		{
			rSize = sizeof(EP_DISCHARGE_STEP);	
			EP_DISCHARGE_STEP *pStep;
			pStep = new EP_DISCHARGE_STEP;
			ZeroMemory(pStep, sizeof(EP_DISCHARGE_STEP));

			pStep->stepHeader.type = m_type;
			pStep->stepHeader.stepIndex = m_StepIndex;
			pStep->stepHeader.nProcType = m_lProcType;
			pStep->stepHeader.mode = m_mode;
			pStep->stepHeader.gradeSize = IsGradeInclude();		//SBC에는 Grading 유무만 전달 

			pStep->lVref  = ExpFloattoLong(m_fVref, EP_VTG_FLOAT);
			pStep->lIref  = ExpFloattoLong(m_fIref, EP_CRT_FLOAT);
    
			pStep->ulEndTime =  PC2MDTIME(m_fEndTime);
			pStep->lEndV  = ExpFloattoLong(m_fEndV, EP_VTG_FLOAT);
			pStep->lEndI  = ExpFloattoLong(m_fEndI, EP_CRT_FLOAT);
			pStep->lEndC  = ExpFloattoLong(m_fEndC, EP_ETC_FLOAT);
			pStep->lEndDV  = ExpFloattoLong(m_fEndDV, EP_VTG_FLOAT);
			pStep->lEndDI  = ExpFloattoLong(m_fEndDI, EP_CRT_FLOAT);

			pStep->bUseActucalCap = m_bUseActucalCap;		//flag of soc rate cut off reference capaciy
			pStep->bReserved =  m_lCapaRefStepIndex;		//step no of capacity reference step no
			pStep->wSocRate = ExpFloattoLong(m_fSocRate, EP_ETC_FLOAT);		//wSoc rate (%)*100

			pStep->lOverV =		ExpFloattoLong(m_fHighLimitV, EP_VTG_FLOAT);
			pStep->lLimitV =	ExpFloattoLong(m_fLowLimitV, EP_VTG_FLOAT);
			pStep->lOverI =		ExpFloattoLong(m_fHighLimitI, EP_CRT_FLOAT);
			pStep->lLimitI =	ExpFloattoLong(m_fLowLimitI, EP_CRT_FLOAT);
			pStep->lOverC =		ExpFloattoLong(m_fHighLimitC, EP_ETC_FLOAT);
			pStep->lLimitC =	ExpFloattoLong(m_fLowLimitC, EP_ETC_FLOAT);

			for(int i =0; i<EP_COMP_POINT; i++)
			{
				pStep->lCompTimeV[i] = PC2MDTIME(m_fCompTimeV[i]);			
				pStep->lCompVLow[i]  = ExpFloattoLong(m_fCompVLow[i], EP_VTG_FLOAT);
				pStep->lCompVHigh[i] = ExpFloattoLong(m_fCompVHigh[i], EP_VTG_FLOAT) ;

				pStep->lCompTimeI[i] = PC2MDTIME(m_fCompTimeI[i]);			
				pStep->lCompILow[i]  = ExpFloattoLong(m_fCompILow[i], EP_CRT_FLOAT);
				pStep->lCompIHigh[i] = ExpFloattoLong(m_fCompIHigh[i], EP_CRT_FLOAT);
			}

			pStep->lDeltaTime = PC2MDTIME(m_fDeltaTimeV);
			pStep->lDeltaV = ExpFloattoLong(m_fDeltaV, EP_VTG_FLOAT);
			pStep->lDeltaTime1 = PC2MDTIME(m_fDeltaTimeI);
			pStep->lDeltaI = ExpFloattoLong(m_fDeltaI, EP_VTG_FLOAT);

			pStep->lRecDeltaTime = PC2MDTIME(m_fRecDeltaTime);
			pStep->lRecDeltaV = ExpFloattoLong(m_fRecDeltaV, EP_VTG_FLOAT);
			pStep->lRecDeltaI = ExpFloattoLong(m_fRecDeltaI, EP_CRT_FLOAT);
			pStep->lRecDeltaT = ExpFloattoLong(m_fRecDeltaT, EP_ETC_FLOAT);

			pStep->lParam1 = ExpFloattoLong(m_fParam1, EP_VTG_FLOAT);		//parameter 1  ex) EDLC : 용량측정 전압1(Low) , etc
			pStep->lParam2 = ExpFloattoLong(m_fParam2, EP_VTG_FLOAT);		//parameter 1  ex) EDLC : 용량측정 전압2(High) , etc

			//ljb 2009-01 
			pStep->UseStepContinue = m_UseStepContinue;

			//2007/2/16
			pStep->aDataID[0] = 1;											//Oven 가동 여부 
			pStep->lRecVIGetTime = PC2MDTIME(m_lRecVIGetTime);
			
			pStep->lProcessType = nProcessNo;	//20210223 ksj

			lpData = pStep;
			break;
		}
	case EP_TYPE_IMPEDANCE	:		
		{
			rSize = sizeof(EP_IMPEDANCE_STEP);	
			EP_IMPEDANCE_STEP *pStep;
			pStep = new EP_IMPEDANCE_STEP;
			ZeroMemory(pStep, sizeof(EP_IMPEDANCE_STEP));

			pStep->stepHeader.type = m_type;
			pStep->stepHeader.stepIndex = m_StepIndex;
			pStep->stepHeader.nProcType = m_lProcType;
			pStep->stepHeader.mode = m_mode;
			pStep->stepHeader.gradeSize = IsGradeInclude();		//SBC에는 Grading 유무만 전달 

			pStep->lVref  = ExpFloattoLong(m_fVref, EP_VTG_FLOAT);
			pStep->lIref  = ExpFloattoLong(m_fIref, EP_CRT_FLOAT);
    
			pStep->ulEndTime =  PC2MDTIME(m_fEndTime);
    
			//pStep->lStartVref;
			//pStep->lStartIref;
			//pStep->ulStartTime;

			pStep->lOverV =		ExpFloattoLong(m_fHighLimitV, EP_VTG_FLOAT);
			pStep->lLimitV =	ExpFloattoLong(m_fLowLimitV, EP_VTG_FLOAT);
			pStep->lOverI =		ExpFloattoLong(m_fHighLimitI, EP_CRT_FLOAT);
			pStep->lLimitI =	ExpFloattoLong(m_fLowLimitI, EP_CRT_FLOAT);
			pStep->lOverImpedance =	ExpFloattoLong(m_fHighLimitImp, EP_CRT_FLOAT);
			pStep->lLimitImpedance =	ExpFloattoLong(m_fLowLimitImp, EP_CRT_FLOAT);
			//pStep->aDataID[EP_RESULT_ITEM_NO];

			pStep->lRecDeltaTime = PC2MDTIME(m_fRecDeltaTime);
			pStep->lRecDeltaV = ExpFloattoLong(m_fRecDeltaV, EP_VTG_FLOAT);
			pStep->lRecDeltaI = ExpFloattoLong(m_fRecDeltaI, EP_CRT_FLOAT);
			pStep->lRecDeltaT = ExpFloattoLong(m_fRecDeltaT, EP_ETC_FLOAT);

			//2007/2/16
			pStep->aDataID[0] = 1;											//Oven 가동 여부 
			pStep->lDCIR_RegTemp = m_lDCIR_RegTemp;
			pStep->lDCIR_ResistanceRate = m_lDCIR_ResistanceRate;
			pStep->lRecVIGetTime = PC2MDTIME(m_lRecVIGetTime);	
			lpData = pStep;
			break;
		}
	case EP_TYPE_OCV		:		
		{
			rSize = sizeof(EP_OCV_STEP);
			EP_OCV_STEP *pStep;
			pStep = new EP_OCV_STEP;
			ZeroMemory(pStep, sizeof(EP_OCV_STEP));

			pStep->stepHeader.type = m_type;
			pStep->stepHeader.stepIndex = m_StepIndex;
			pStep->stepHeader.nProcType = m_lProcType;
			pStep->stepHeader.mode = m_mode;
			pStep->stepHeader.gradeSize = IsGradeInclude();		//SBC에는 Grading 유무만 전달 

			pStep->lOverV =		ExpFloattoLong(m_fHighLimitV, EP_VTG_FLOAT);
			pStep->lLimitV =	ExpFloattoLong(m_fLowLimitV, EP_VTG_FLOAT);

			//2007/2/16
			pStep->aDataID[0] = 1;											//Oven 가동 여부 

			lpData = pStep;
			break;
		}
	case EP_TYPE_REST		:		
		{
			rSize = sizeof(EP_REST_STEP);
			EP_REST_STEP *pStep;
			pStep = new EP_REST_STEP;
			ZeroMemory(pStep, sizeof(EP_REST_STEP));

			pStep->stepHeader.type = m_type;
			pStep->stepHeader.stepIndex = m_StepIndex;
			pStep->stepHeader.nProcType = m_lProcType;
			pStep->stepHeader.mode = m_mode;
			pStep->stepHeader.gradeSize = IsGradeInclude();		//SBC에는 Grading 유무만 전달 

			pStep->ulEndTime =  PC2MDTIME(m_fEndTime);
			pStep->lOverV =		ExpFloattoLong(m_fHighLimitV, EP_VTG_FLOAT);
			pStep->lLimitV =	ExpFloattoLong(m_fLowLimitV, EP_VTG_FLOAT);
			//pStep->lJigUp;
			//pStep->aDataID[EP_RESULT_ITEM_NO];

			pStep->lRecDeltaTime = PC2MDTIME(m_fRecDeltaTime);
			pStep->lRecDeltaV = ExpFloattoLong(m_fRecDeltaV, EP_VTG_FLOAT);
			pStep->lRecDeltaI = ExpFloattoLong(m_fRecDeltaI, EP_CRT_FLOAT);
			pStep->lRecDeltaT = ExpFloattoLong(m_fRecDeltaT, EP_ETC_FLOAT);

			//2007/2/16
			pStep->aDataID[0] = 1;											//Oven 가동 여부 
			
            pStep->lRecVIGetTime = PC2MDTIME(m_lRecVIGetTime);

			lpData = pStep;
			break;
		}
		
	case EP_TYPE_END:		
		{
			rSize = sizeof(EP_END_STEP);
			EP_END_STEP *pStep;
			pStep = new EP_END_STEP;
			ZeroMemory(pStep, sizeof(EP_END_STEP));

			pStep->stepHeader.type = m_type;
			pStep->stepHeader.stepIndex = m_StepIndex;
			pStep->stepHeader.nProcType = m_lProcType;
			pStep->stepHeader.mode = m_mode;
			pStep->stepHeader.gradeSize = IsGradeInclude();		//SBC에는 Grading 유무만 전달 

			//2007/2/16

			lpData = pStep;
			break;
		}
	case EP_TYPE_LOOP:				
		{
			rSize = sizeof(EP_LOOP_STEP);
			EP_LOOP_STEP *pStep;
			pStep = new EP_LOOP_STEP;
			ZeroMemory(pStep, sizeof(EP_LOOP_STEP));
			
			pStep->stepHeader.type = m_type;
			pStep->stepHeader.stepIndex = m_StepIndex;
			pStep->stepHeader.nProcType = m_lProcType;
			pStep->stepHeader.mode = m_mode;
			pStep->stepHeader.gradeSize = IsGradeInclude();		//SBC에는 Grading 유무만 전달 

			pStep->nLoopInfoGoto = m_lGotoStep;
			pStep->nLoopInfoCycle = m_lCycleNo;

			//2007/2/16
			lpData = pStep;
			break;
		}
	default:
		{
			rSize = 0;							
			return FALSE;
		}
	}	

	return TRUE;
}

BOOL CStep::GetNetStepGradeData(LPVOID &lpData, int &rSize)
{
	int nSize = m_Grading.GetGradeStepSize();
	if(nSize < 1)	return FALSE;
	
	rSize = sizeof(EP_GRADE_HEADER)+sizeof(EP_GRADE)* nSize;

	lpData = new char[rSize];
	ZeroMemory(lpData, rSize);

	EP_GRADE_HEADER *lpGradeHeader = (EP_GRADE_HEADER *)lpData;

//	lpGradeHeader->gradeItem = m_Grading.GetGradeItem();
	lpGradeHeader->stepIndex = m_StepIndex;
	lpGradeHeader->totalStep = nSize;
	lpGradeHeader->totalAnd = 1;

	GRADE_STEP gData;
	EP_GRADE netgData;
	int iGradeCnt=0;
	for(int j =0; j<nSize; j++)		// &,+,0 있으면 Grade 조건 값이 2개
	{
		gData = m_Grading.GetStepData(j);
		if(gData.strCode == "&" || gData.strCode == "+" || gData.strCode == "0")
		{
			lpGradeHeader->totalStep = nSize / 2;
			lpGradeHeader->totalAnd = 2;
		}
	}

	//Grade 데이터	2008-12-01 ljb
	for(int i =0; i<nSize; i++)
	{
		gData = m_Grading.GetStepData(i);
		if(gData.strCode.IsEmpty())
		{
			netgData.gradeCode = 0;
		}
		else
		{
			netgData.gradeCode = atoi(gData.strCode);
		}
		if(gData.lGradeItem == EP_GRADE_VOLTAGE)
		{
			netgData.lValue1 = ExpFloattoLong (gData.fMin, EP_VTG_FLOAT);
			netgData.lValue2 = ExpFloattoLong (gData.fMax, EP_VTG_FLOAT);
		}
		else if(gData.lGradeItem == EP_GRADE_TIME)
		{
			netgData.lValue1 = PC2MDTIME(gData.fMin);
			netgData.lValue2 = PC2MDTIME(gData.fMax);
		}
		else
		{
			netgData.lValue1 = ExpFloattoLong (gData.fMin, EP_ETC_FLOAT);
			netgData.lValue2 = ExpFloattoLong (gData.fMax, EP_ETC_FLOAT);
		}
		netgData.gradeItem = gData.lGradeItem ;
		netgData.reserved2 = 0 ;
		
		memcpy((char *)lpData+sizeof(EP_GRADE_HEADER)+i*sizeof(EP_GRADE), &netgData, sizeof(EP_GRADE));
	}
	
	return TRUE;
}


/*
BOOL CStep::SetChargeStep(EP_CHARGE_STEP *pStep)
{
	ClearStepData();

	m_bGrade = pStep->stepHeader.bGrade;
	m_mode = pStep->stepHeader.mode;
	m_StepIndex = pStep->stepHeader.stepIndex;
	m_type = pStep->stepHeader.type;
	m_fProcType = pStep->stepHeader.nProcType;
	m_fVref = pStep->fVref;
    m_fIref = pStep->fIref;
    
	 m_ulEndTime = pStep->ulEndTime;
     m_fEndV = pStep->fEndV;
     m_fEndI = pStep->fEndI;
     m_fEndC = pStep->fEndC;
     m_fEndDV = pStep->fEndDV;
     m_fEndDI = pStep->fEndDI;

	 m_fHighLimitV = pStep->fOverV;
     m_fLowLimitV= pStep->fLimitV;
     m_fHighLimitI= pStep->fOverI;
     m_fLowLimitI= pStep->fLimitI;
     m_fHighLimitC = pStep->fOverC;
     m_fLowLimitC = pStep->fLimitC;

	 memcpy( m_fCompTimeV, pStep->fCompTimeV, sizeof(pStep->fCompTimeV));			
	 memcpy( m_fCompVLow, pStep->fCompVLow, sizeof(pStep->fCompVLow));
	 memcpy( m_fCompVHigh, pStep->fCompVHigh, sizeof(pStep->fCompVHigh));
	 memcpy( m_fCompTimeI, pStep->fCompTimeI, sizeof(pStep->fCompTimeI));			
	 memcpy( m_fCompILow, pStep->fCompILow, sizeof(pStep->fCompILow));
	 memcpy( m_fCompIHigh, pStep->fCompIHigh, sizeof(pStep->fCompIHigh));

     m_fDeltaTimeV= pStep->fDeltaTime;
     m_fDeltaV = pStep->fDeltaV;
     m_fDeltaTimeI = pStep->fDeltaTime1;
     m_fDeltaI = pStep->fDeltaI;
	
	 //= step.lTmv;
	 //= step.ulReceTime;
	 //= step.aDataID[EP_RESULT_ITEM_NO];
	 return TRUE;
}

BOOL CStep::SetDisChargeStep(EP_DISCHARGE_STEP *pStep)
{
	ClearStepData();


	m_bGrade = pStep->stepHeader.bGrade;
	 m_mode = pStep->stepHeader.mode;
	 m_StepIndex = pStep->stepHeader.stepIndex;
	 m_type = pStep->stepHeader.type;
	m_fProcType = pStep->stepHeader.nProcType;

	 m_fVref = pStep->fVref;
     m_fIref = pStep->fIref;
    
	 m_ulEndTime = pStep->ulEndTime;
     m_fEndV = pStep->fEndV;
     m_fEndI = pStep->fEndI;
     m_fEndC = pStep->fEndC;
     m_fEndDV = pStep->fEndDV;
     m_fEndDI = pStep->fEndDI;

	 m_fHighLimitV = pStep->fOverV;
     m_fLowLimitV= pStep->fLimitV;
     m_fHighLimitI= pStep->fOverI;
     m_fLowLimitI= pStep->fLimitI;
     m_fHighLimitC = pStep->fOverC;
     m_fLowLimitC = pStep->fLimitC;

	 memcpy( m_fCompTimeV, pStep->fCompTimeV, sizeof(pStep->fCompTimeV));			
	 memcpy( m_fCompVLow, pStep->fCompVLow, sizeof(pStep->fCompVLow));
	 memcpy( m_fCompVHigh, pStep->fCompVHigh, sizeof(pStep->fCompVHigh));
	 memcpy( m_fCompTimeI, pStep->fCompTimeI, sizeof(pStep->fCompTimeI));			
	 memcpy( m_fCompILow, pStep->fCompILow, sizeof(pStep->fCompILow));
	 memcpy( m_fCompIHigh, pStep->fCompIHigh, sizeof(pStep->fCompIHigh));

     m_fDeltaTimeV= pStep->fDeltaTime;
     m_fDeltaV = pStep->fDeltaV;
     m_fDeltaTimeI = pStep->fDeltaTime1;
     m_fDeltaI = pStep->fDeltaI;
	
	 //= step.lTmv;
	 //= step.ulReceTime;
	 //= step.aDataID[EP_RESULT_ITEM_NO];
	 return TRUE;
}

 BOOL CStep::SetRestStep(EP_REST_STEP *pStep)
{
	ClearStepData();


	 m_bGrade = pStep->stepHeader.bGrade;
	 m_mode = pStep->stepHeader.mode;
	 m_StepIndex = pStep->stepHeader.stepIndex;
	 m_type = pStep->stepHeader.type;
 	m_fProcType = pStep->stepHeader.nProcType;
   
	 m_ulEndTime = pStep->ulEndTime;

	 m_fHighLimitV = pStep->fOverV;
     m_fLowLimitV= pStep->fLimitV;

	 return TRUE;
}

BOOL CStep::SetOcvStep(EP_OCV_STEP *pStep)
{
	ClearStepData();


	 m_bGrade = pStep->stepHeader.bGrade;
	 m_mode = pStep->stepHeader.mode;
	 m_StepIndex = pStep->stepHeader.stepIndex;
	 m_type = pStep->stepHeader.type;
	m_fProcType = pStep->stepHeader.nProcType;

	 m_fHighLimitV = pStep->fOverV;
     m_fLowLimitV= pStep->fLimitV;

	 return TRUE;
}

BOOL CStep::SetImpStep(EP_IMPEDANCE_STEP *pStep)
{
	ClearStepData();


	m_bGrade = pStep->stepHeader.bGrade;
	 m_mode = pStep->stepHeader.mode;
	 m_StepIndex = pStep->stepHeader.stepIndex;
	 m_type = pStep->stepHeader.type;
	m_fProcType = pStep->stepHeader.nProcType;

	 m_fVref = pStep->fVref;
     m_fIref = pStep->fIref;
    
	 m_ulEndTime = pStep->ulEndTime;

	 m_fHighLimitV = pStep->fOverV;
     m_fLowLimitV= pStep->fLimitV;
     m_fHighLimitI= pStep->fOverI;
     m_fLowLimitI= pStep->fLimitI;
	 m_fHighLimitImp = pStep->fOverImpedance;
	 m_fLowLimitImp = pStep->fLimitImpedance;
	 
	 return TRUE;
}

BOOL CStep::SetEndStep(EP_END_STEP *pStep)
{
	ClearStepData();


	 m_bGrade = pStep->stepHeader.bGrade;
	 m_mode = pStep->stepHeader.mode;
	 m_StepIndex = pStep->stepHeader.stepIndex;
	 m_type = pStep->stepHeader.type;
	m_fProcType = pStep->stepHeader.nProcType;
	
	 return TRUE;
}

BOOL CStep::SetLoopStep(EP_LOOP_STEP *pStep)
{
	ClearStepData();

	m_fGotoStep = pStep->nLoopInfoGoto;
	m_fCycleNo = pStep->nLoopInfoCycle;
	m_fProcType = pStep->stepHeader.nProcType;

	 m_bGrade = pStep->stepHeader.bGrade;
	 m_mode = pStep->stepHeader.mode;
	 m_StepIndex = pStep->stepHeader.stepIndex;
	 m_type = pStep->stepHeader.type;

 
	 return TRUE;
}
*/
CString CStep::EndConditonString(CString strVUnit, CString strIUnit)
{
	CString strTemp, strTemp1;
	float data;

	switch(m_type)
	{
	case EP_TYPE_CHARGE:			//Charge
		{
			if(m_fEndV > 0L)	
			{
				data = m_fEndV;
				strTemp.Format("V >%s", ValueString(data, strVUnit == "V" ? FALSE:TRUE));
			}
	
			if(m_fEndI > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				data = m_fEndI;
				strTemp1.Format("I < %s",  ValueString(data, strIUnit == "A" ? FALSE:TRUE));
				strTemp += strTemp1;
			}
			
			if(m_fEndTime >0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				data = m_fEndTime;
				strTemp1.Format("t > %s", TimeString(data));
				strTemp += strTemp1;
			}
			if(m_fEndC > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				data = m_fEndC;
				strTemp1.Format("C > %s", ValueString(data, strIUnit == "A" ? FALSE:TRUE));
				strTemp += strTemp1;
			}
			if(m_fEndDV > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				data = m_fEndDV;
				strTemp1.Format("dV > %s", ValueString(data, strVUnit == "V" ? FALSE:TRUE));
				strTemp += strTemp1;
			}
			if(m_fEndDI > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				data = m_fEndDI;
				strTemp1.Format("dI > %s", ValueString(data, strIUnit == "A" ? FALSE:TRUE));
				strTemp += strTemp1;
			}
			break;
		}

	case EP_TYPE_DISCHARGE:		//Discharge
		{
			if(m_fEndV > 0L)	
			{
				data = m_fEndV;
				strTemp.Format("V < %s", ValueString(data, strVUnit == "V" ? FALSE:TRUE));
			}
			if(m_fEndI < 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				data = m_fEndI;
				strTemp1.Format("I > %s", ValueString(data, strIUnit == "A" ? FALSE:TRUE) );
				strTemp += strTemp1;
			}
			if(m_fEndTime > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				
				data = m_fEndTime;
				strTemp1.Format("t > %s", TimeString(data));
				strTemp += strTemp1;
			}
			if(m_fEndC > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				data = m_fEndC;
				strTemp1.Format("C > %s", ValueString(data, strIUnit == "A" ? FALSE:TRUE) );
				strTemp += strTemp1;
			}
			if(m_fEndDV < 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				data = m_fEndDV;
				strTemp1.Format("dV < %s", ValueString(data, strVUnit == "V" ? FALSE:TRUE) );
				strTemp += strTemp1;
			}
			if(m_fEndDI > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				data = m_fEndDI;
				strTemp1.Format("dI > %s", ValueString(data, strIUnit == "A" ? FALSE:TRUE) );
				strTemp += strTemp1;
			}
			break;
		}

	case EP_TYPE_REST:				//Rest
		{
			data = m_fEndTime;
			strTemp.Format("t > %s", TimeString(data)); 
			break;
		}
	case EP_TYPE_LOOP:
		{
			strTemp.Format("Goto(%d) Cycle(%d)", m_lGotoStep+1, m_lCycleNo); 
			break;
		}
	case EP_TYPE_IMPEDANCE:
		{
			if(m_mode==EP_MODE_DC_IMP)
			{
				data = m_fEndTime;
				strTemp.Format("t > %s", TimeString(data));
			}
			else
				strTemp.Empty();
			break;
		}
	case EP_TYPE_OCV:				//Ocv
	case EP_TYPE_END:				//End
	case -1:					//Not Seleted
	default:
		strTemp.Empty();
		break;
	}
	return strTemp;
}

CString CStep::ValueString(double dData, BOOL bMiliUnit)
{
	CString strString;
	if(bMiliUnit)
	{
		strString.Format("%.1f", dData);
	}
	else
	{
		strString.Format("%.3f", dData/1000);
	}
	return strString;
}

CString CStep::TimeString(double dTime)
{
	CString strMsg;
	CTimeSpan timeSpan((ULONG)dTime);
	if(timeSpan.GetDays() > 0)
	{
		strMsg =  timeSpan.Format("%Dd %H:%M:%S");
	}
	else
	{
		strMsg = timeSpan.Format("%H:%M:%S");
	}
	return strMsg;
}

BOOL CStep::IsGradeInclude()
{
	if(m_Grading.GetGradeStepSize() > 0)	return TRUE;
	else return FALSE;
}
