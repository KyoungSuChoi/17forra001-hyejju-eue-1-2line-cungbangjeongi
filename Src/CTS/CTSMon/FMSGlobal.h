#pragma once

#include <winsock2.h>
#include <ws2tcpip.h>
#include <mswsock.h>
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>

#include <vector>
#include <list>

#include<direct.h>


#define PACKET_HEADER_SIZE 10
#define PACKET_SIZE 23

#define FMS_MAX_BUF_LEN	6144
#define FMS_MAX_BUF_LEN_EXTRA FMS_MAX_BUF_LEN * 4

#define FMSPORT 7000

#define FMS_HEARTBEATCHECK 60	// �ʴ���
