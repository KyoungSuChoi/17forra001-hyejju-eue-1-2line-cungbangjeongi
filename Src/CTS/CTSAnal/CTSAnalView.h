// CTSAnalView.h : interface of the CCTSAnalView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CTSAnalVIEW_H__243127B5_7F2C_4AD2_871B_78C20907DA6D__INCLUDED_)
#define AFX_CTSAnalVIEW_H__243127B5_7F2C_4AD2_871B_78C20907DA6D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "TestLogSet.h"
#include "ModuleRecordSet.h"
#include "TestRecordSet.h"
#include "ChValueRecordSet.h"
#include "ChResultSet.h"
//#include "TestLogBkSet.h"
//#include "ModuleBkSet.h"
//#include "TestBkSet.h"
//#include "ChValueBkSet.h"
//#include "ChResultBkSet.h"
#include "MyGridWnd.h"
#include "toolkit/secmedit.h"
#include "DataDlg.h"
//#include "FieldDlg.h"
#include "CTSAnalDoc.h"
#include "FtpDlg.h"
#include "CpkViewDlg.h"

//#define  GRADECODE     1
#define  FAILCODE      1

#define LIST_COLUMN_TEST_LOG_ID			0
#define LIST_COLUMN_NO					1
#define LIST_COLUMN_CELL_NO				2
#define LIST_COLUMN_MODEL_NAME			3
#define LIST_COLUMN_TEST_SERIAL_NO		4
#define LIST_COLUMN_TRAY_SERIAL			5
#define LIST_COLUMN_LOT_NO				6
#define LIST_COLUMN_TRAY_NO				7
#define LIST_COLUMN_RUN_MODULE			8
#define LIST_COLUMN_TEST_DONE			9
#define LIST_COLUMN_DATETIME			10


class CCTSAnalView : public CFormView
{
protected: // create from serialization only
	CCTSAnalView();
	DECLARE_DYNCREATE(CCTSAnalView)

public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	//{{AFX_DATA(CCTSAnalView)
	enum { IDD = IDD_CTSAnal_FORM };
	CLabel	m_wndSearchCount;
//	CLabel	m_ctrlSelDataLabel;
	CComboBox	m_ctrlFilterCombo;
	CComboBox	m_ComboMonth2;
	CComboBox	m_ComboMonth1;
	CComboBox	m_ComboDay2;
	CComboBox	m_ComboDay1;
	CComboBox	m_Combo;
	CString	m_strLotFrom;
	CString	m_strLotTo;
	CString	m_strTrayFrom;
	CString	m_strTrayTo;
	CTime	m_dateFrom;
	CTime	m_dateTo;
	BOOL	m_bDateTimeSerarch;
	CString	m_strModelName;
	//}}AFX_DATA
// Attributes

	stingray::foundation::SECBitmapButton			m_btnSearch;
	stingray::foundation::SECBitmapButton			m_btnModuleTimeLog;
	CString					m_strSearchTitle;
	CPtrArray		    	 m_apDataList;
	CDataDlg*                m_pDlg ;
	CFtpDlg                 *m_pFtpDlg;
	CCpkViewDlg*             m_pCpkViewDlg;
	CTestLogSet              m_TestLog;
	CModuleRecordSet         m_Module;
	CTestRecordSet           m_Test;
	CChValueRecordSet        m_ChValue;
	CChResultSet             m_ChResult;
//	CTestLogBkSet            m_TestLogBk;
//	CModuleBkSet             m_ModuleBk;
//	CTestBkSet               m_TestBk;
//	CChValueBkSet            m_ChValueBk;
//	CChResultBkSet           m_ChResultBk;
	CMyGridWnd               m_DataGrid;
	CMyGridWnd               m_TestGrid;
	CMyGridWnd               m_ModuleGrid;
	int        m_SelNO;
	int        m_Col;
	int        m_TotCol;             //총컬럼 수
	int        m_casenum;
	int        m_DataType;             //
	int        m_currentRow;         //현재 레코드의 인텍스
	int        m_CellNo;
	unsigned long       m_beforecasenum;
	long       m_imsi;
	CString    m_Year1;
	CString    m_Year2;
	CString    m_Month1;
	CString    m_Month2;
	CString    m_Day1;
	CString    m_Day2;
	FIELD     m_field[FIELDNO];
//	BOOL       m_C1;                  //C1필드의 값이 수행되었는지 아닌지를 위한 변수
//	BOOL       m_D1;                  //D1필드의 값이 수행되었는지 아닌지를 위한 변수
	CString    PathName;
	BOOL       m_BackUp;              //BackUp할때 사용하기 위한 변수
	BOOL       m_Save;
	BOOL       m_Clear;               //Clear 함수를 위한 변수
	BOOL       m_Delete;              //Delete함수를 위한 변수
//--------------------------- 2001.1.24 ------------------------//
	BOOL       m_Check;               //AND 검색인지를 확인하기 위한 변수
	CString    m_Year3;
	CString    m_Year4;
	CString    m_Month3;
	CString    m_Month4;
	CString    m_Day3;
	CString    m_Day4;
	int        m_SelNO2;
	CString    m_strLogInID;
	CString    m_strPassWord;
	BOOL       m_Remember;
//--------------------------------------------------------------//
	BOOL       m_TestGridDisplay;

	CDatabase m_db;


public:
	CCTSAnalDoc* GetDocument();
	BOOL  LogInCheck();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCTSAnalView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo = NULL);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	void ShowCellList(CString strTestID);
	int m_nKeyColumn;
	BOOL DataCpCpk();
	CString m_strQuerySQL;
	BOOL DisplayListData(CString strSQL);
	CString m_strFilterString;
	int m_nFilterType;
	int m_nCpItem;
	double m_dDivision;
	double m_dLowLimit;
	double m_dHighLimit;
	BOOL RequeryChResult(int nCol, CString testSerial, int ID);
	BOOL RequeryChVal(int nCol, long testIndex, int nDataType);
	BOOL DisplayChData(CString strTestserial);
	BOOL DisplayModuleData(CString strTestSerial, int nType, CString strData);
	virtual ~CCTSAnalView();
//	void     OnSeekTrayNo();
	void     OnSeekDateTime();
	void     OnSeekLotNo();
	void     OnSeekCellNo();
//	void     OnSeekTestLogID();
//	void     OnSeekTraySerialNo();
	void     OnSeekTestSerialNo();
	void     InitDataGrid();
	void     InitModuleGrid();
	void     InitTestGrid();
	void     DrawTestGrid();
	void     UpdateResource();    //  View Module Table!
	void     UpdateResource1();   //  View Test   Table!
	void     UpdateTestGrid();
	void     BackUp();
	void     Clear();
	void     DeleteRecord();
//-------------------- 2002. 1. 24 -------------------------//
	void     GetCurrentTime();
//----------------------------------------------------------//
	CString  Time(float time);              // 초를 시/분/초로 변환
	void     FieldSelect();                // Display될 필드가 몇개인가를 판단.
//	void     StateCheck();                 // 채널의 상태를 체크한다.
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	void UpdateDataList();
	BOOL RequeryDataSerarch(int dataType);
	void UpdateTestList();
	BOOL RequeryCellCount(long lTestID, CString strTestSerialNo, int &nInputCount, int &nNormalCount, long nProcdureID = 0);
	CArray<DATA_LIST ,DATA_LIST> m_ptArray;
	CArray<PROC_LIST, PROC_LIST> m_ptTestArray;
	BOOL m_bConnected;
// Generated message map functions
protected:
	//{{AFX_MSG(CCTSAnalView)
	afx_msg void OnSelchangeCombo1();
	afx_msg void OnButtonSearch();
	afx_msg void OnReportView1();
	afx_msg void OnSelchangeComboMonth1();
	afx_msg void OnSelchangeComboMonth2();
	afx_msg void OnSelchangeComboDay1();
	afx_msg void OnSelchangeComboDay2();
	afx_msg void OnSelchangeComboYear1();
	afx_msg void OnSelchangeComboYear2();
	afx_msg void OnCherrorView();
	afx_msg void OnFileSaveAs();
	afx_msg void OnFileNew();
	afx_msg void OnUpdateFileNew(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFileSaveAs(CCmdUI* pCmdUI);
	afx_msg void OnDelete();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnCheck1();
	afx_msg void OnSelchangeCombo2();
	afx_msg void OnFieldSet();
	afx_msg void OnCpcpkView();
	afx_msg void OnAnalProc();
	afx_msg void OnSelchangeDataFilter();
	afx_msg void OnDateTimeSearch();
	afx_msg void OnDeleteRecord();
	afx_msg void OnFileSave();
	afx_msg void OnCellListSave();
	afx_msg void OnDataRestore();
	afx_msg void OnUseCountModule();
	afx_msg void OnDeleteTest();
	afx_msg void OnCellNoEdit();
	afx_msg void OnModelNameEdit();
	afx_msg void OnLotNoEdit();
	afx_msg void OnTrayNoEdit();
	afx_msg void OnUpdateCellNoEdit(CCmdUI* pCmdUI);
	afx_msg void OnUpdateModelNameEdit(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLotNoEdit(CCmdUI* pCmdUI);
	afx_msg void OnUpdateTrayNoEdit(CCmdUI* pCmdUI);
	afx_msg void OnDetailViewTest();
	afx_msg void OnDataSort();
	//}}AFX_MSG
	afx_msg LRESULT OnMovedCurrentCell(WPARAM wParam, LPARAM lParam);
    afx_msg LRESULT OnGridDoubleClick(WPARAM wParam, LPARAM lParam);
	afx_msg	LRESULT OnRButtonClickedRowCol(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in CTSAnalView.cpp
inline CCTSAnalDoc* CCTSAnalView::GetDocument()
   { return (CCTSAnalDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CTSAnalVIEW_H__243127B5_7F2C_4AD2_871B_78C20907DA6D__INCLUDED_)
