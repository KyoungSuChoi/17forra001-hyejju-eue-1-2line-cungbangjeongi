// ScheduleData.h: interface for the CScheduleData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SCHEDULEDATA_H__707CB6FF_2156_4365_87AD_1D2295496A0E__INCLUDED_)
#define AFX_SCHEDULEDATA_H__707CB6FF_2156_4365_87AD_1D2295496A0E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Step.h"

#define SCHEDULE_FILE_ID		740721	//스케쥴 파일 ID
#define SCHEDULE_FILE_VER	0x00010001	//상위 2Byte는 Majer Version No, 하위 2Byte는 Minu Version No

class AFX_EXT_CLASS CScheduleData  
{
public:
	BOOL AddStep(FILE_STEP_PARAM *pStepData);
	int GetCurrentStepCycle(int nStepIndex);
	BOOL IsThereGradingStep();
	BOOL IsThereImpStep();
	BOOL SaveToExcelFile(CString strFileName, BOOL bOpenExcel = TRUE, BOOL bCheckMode = FALSE);
	void ShowContent(int nStepIndex = 0);
	CStep* GetStepData(UINT nStepIndex);
	void ResetData();
	UINT GetStepSize();
	BOOL SaveToFile(CString strFileName);
	BOOL SetSchedule(CString strDBName, long lModelPK, long lTestPK);
	BOOL SetSchedule(CString strFileName);
	CScheduleData();
	CScheduleData(CString strFileName);
	virtual ~CScheduleData();

	static BOOL	   ExecuteEditor(long lModelPK, long lTestPK);
	static CString GetTypeString(int nType);
	static CString GetModeString(int nType, int nMode);

	//Model Information
	LPCTSTR GetModelName()		{	return m_ModelData.szName;			}
	LPCTSTR	GetModelEditTime()	{	return m_ModelData.szModifiedTime;	}
	long	GetModelPK()		{	return m_ModelData.lID;				}
	LPCTSTR GetModelDescript()	{	return m_ModelData.szDescription;	}
//	LPCTSTR	GetModelCreator()	{	return m_ModelData.szCreator;		}

	//Schedule Information
	LPCTSTR GetScheduleName()	{	return m_ProcedureData.szName;		}
	LPCTSTR	GetScheduleEditTime()	{	return m_ProcedureData.szModifiedTime;		}
	long	GetSchedulePK()		{	return m_ProcedureData.lID;						}
	LPCTSTR GetScheduleDescript()	{	return	m_ProcedureData.szDescription;		}
	LPCTSTR GetScheduleCreator()	{	return m_ProcedureData.szCreator;			}
	long	GetScheduleType()	{	return m_ProcedureData.lType;			}

	BOOL ExecuteExcel(CString strFileName);
	CELL_CHECK_DATA * GetCellCheckParam()	{	return &m_CellCheck;	}

protected:
	CString GetExcelPath();
	BOOL LoadGradeInfo(CDaoDatabase &db, CStep *pStepData);
	BOOL LoadCellCheckInfo(CDaoDatabase &db, long lProcPK);
	BOOL LoadStepInfo(CDaoDatabase &db, long lProcPK);
	BOOL LoadProcedureInfo(CDaoDatabase &db, long lModelPK, long lProcPK);
	BOOL LoadModelInfo(CDaoDatabase &db, long lPK);
	SCHDULE_INFORMATION_DATA	m_ModelData;
	SCHDULE_INFORMATION_DATA	m_ProcedureData;
	CELL_CHECK_DATA				m_CellCheck;
	CPtrArray	m_apStepArray;
	BOOL	m_bContinueCellCode;
};

#endif // !defined(AFX_SCHEDULEDATA_H__707CB6FF_2156_4365_87AD_1D2295496A0E__INCLUDED_)
