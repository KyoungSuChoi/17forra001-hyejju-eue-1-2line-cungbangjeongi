#if !defined(AFX_REGULATORDLG_H__8ED0A412_FABE_477C_BAB8_23DDD3CC597D__INCLUDED_)
#define AFX_REGULATORDLG_H__8ED0A412_FABE_477C_BAB8_23DDD3CC597D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RegulatorDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CRegulatorDlg dialog

class CRegulatorDlg : public CDialog
{
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();
	virtual ~CRegulatorDlg();

	// Construction
public:
	void DisplayData(int nModuleID, LPEP_REF_IC_DATA lpData = NULL);
	void UpdateMDData(int nModuleID);
	CRegulatorDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CRegulatorDlg)
	enum { IDD = IDD_REGULAOR_DLG };
	CString	m_strFileName;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRegulatorDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CRegulatorDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDataRequest();
	afx_msg void OnFileNameEdit();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_REGULATORDLG_H__8ED0A412_FABE_477C_BAB8_23DDD3CC597D__INCLUDED_)
