#pragma once

enum IO_TYPE
{
	IO_NONE,
	IO_NEW_ACCEPTOBJ,
	IO_DISCONNECT,
	IO_ACCEPT,
	IO_CONNECT,
	IO_READ,
	IO_WRITE
};

class CFMSObj;

class CFMSPacketBox;

class CFMSNetIoStatus : public CFMSMemoryPool<CFMSNetIoStatus>
{
public:
	CFMSNetIoStatus()
	{
		m_eIO = IO_NONE;
		m_poObject = NULL;
		m_dwNumOfByteTransfered = 0;
	}

	IO_TYPE m_eIO;
	CFMSObj *m_poObject;
	DWORD m_dwNumOfByteTransfered;
};

class CFMSOverlapped : public CFMSMemoryPool<CFMSOverlapped>
{
public:
	CFMSOverlapped()
	{
		ZeroMemory(&m_oOverlapped, sizeof(m_oOverlapped));
		m_eIoType = IO_NONE;
		m_poObject = NULL;
	}

	OVERLAPPED m_oOverlapped;
	IO_TYPE m_eIoType;
	CFMSObj *m_poObject;
};

template <class TNetObj>
class CFMSNetObj : public CFMSSyncParent<TNetObj>, public CFMSMemoryPool<TNetObj>
{
public:
	CFMSNetObj()
	{
		m_poAccept = new CFMSOverlapped();
		m_poRead = new CFMSOverlapped();
		m_poPacketBox = new CFMSPacketBox();

		ZeroMemory(m_aucReadBuf, sizeof(m_aucReadBuf));

		m_poAccept->m_eIoType = IO_ACCEPT;
		m_poRead->m_eIoType = IO_READ;

		//m_poAccept->m_poObject = m_poOwnerNetObj;
		//m_poRead->m_poObject = m_poOwnerNetObj;

		m_hSocket = NULL;
		m_bActive = FALSE;
		m_IsWorking = FALSE;
		
		m_HeartBeatCheck = 0;
		m_dwRemainLen = 0;

		m_recvTotlen = 0;
		m_procTotlen = 0;

		ZeroMemory(m_aucReadBuf, FMS_MAX_BUF_LEN);
		ZeroMemory(m_aucPacketBuf, FMS_MAX_BUF_LEN_EXTRA);
	}
	virtual ~CFMSNetObj(VOID)
	{
		delete m_poAccept;
		delete m_poRead;
		delete m_poPacketBox;
	}
	VOID SetOwnerNetObj(TNetObj *_Owner)
	{
		m_poOwnerNetObj = _Owner;
		m_poAccept->m_poObject = m_poOwnerNetObj;
		m_poRead->m_poObject = m_poOwnerNetObj;
	}
	
	BOOL Listen(USHORT usPort, INT iBackLog)
	{
		CFMSSyncObj FMSSync;

		if (iBackLog <= 0 || m_bActive || m_hSocket)
			return FALSE;

		m_hSocket = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED);
		if (m_hSocket == INVALID_SOCKET)
			return FALSE;

		SOCKADDR_IN si;
		si.sin_family = AF_INET;
		si.sin_port = htons(usPort);
		si.sin_addr.S_un.S_addr = htonl(INADDR_ANY);		
		// si.sin_addr.S_un.S_addr = inet_addr("192.168.102.112");

		if (bind(m_hSocket, (struct sockaddr*) &si, sizeof(si)) == SOCKET_ERROR)
		{
			return FALSE;
		}

		if (listen(m_hSocket, iBackLog) == SOCKET_ERROR)
			return FALSE;

		//LINGER lg;
		//lg.l_onoff = 0;
		//lg.l_linger = 0;
		//if (setsockopt(m_hSocket, SOL_SOCKET, SO_LINGER, (char*) &lg, sizeof(lg)) == SOCKET_ERROR)
		//	return FALSE;

		m_bActive = TRUE;

		return TRUE;
	}
	BOOL Accept(SOCKET hListen)
	{
		CFMSSyncObj FMSSync;

		if (!hListen || m_bActive || m_hSocket)
			return FALSE;

		m_hSocket = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED);
		if (m_hSocket == INVALID_SOCKET)
			return FALSE;

		if (!AcceptEx(hListen,
			m_hSocket,
			m_aucReadBuf,
			0,
			sizeof(sockaddr_in) + 16,
			sizeof(sockaddr_in) + 16,
			NULL,
			&m_poAccept->m_oOverlapped))
		{
			if (WSAGetLastError() != ERROR_IO_PENDING && WSAGetLastError() != WSAEWOULDBLOCK)
				return FALSE;
		}

		//LINGER lg;
		//lg.l_onoff = 0;
		//lg.l_linger = 0;
		//if (setsockopt(m_hSocket, SOL_SOCKET, SO_LINGER, (char*) &lg, sizeof(lg)) == SOCKET_ERROR)
		//	return FALSE;

		m_bActive = TRUE;

		return TRUE;
	}
	BOOL Connect(LPTSTR lpwAddress, USHORT usPort)
	{
		CFMSSyncObj FMSSync;

		if (!lpwAddress || m_bActive || m_hSocket)
			return FALSE;

		m_hSocket = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED);
		if (m_hSocket == INVALID_SOCKET)
			return FALSE;

		int nValue = 1;
		setsockopt(m_hSocket, IPPROTO_TCP, TCP_NODELAY, (char*) &nValue, sizeof(int));

		//CHAR szTemp[16] = {0,};

		SOCKADDR_IN si;
		si.sin_family = AF_INET;
		si.sin_port = htons(usPort);
		//si.sin_addr.S_un.S_addr = inet_addr(W2M(lpwAddress, szTemp, sizeof(szTemp)));
		si.sin_addr.S_un.S_addr = inet_addr(lpwAddress);

		if (WSAConnect(m_hSocket, (LPSOCKADDR) &si, sizeof(SOCKADDR_IN), NULL, NULL, NULL, NULL) == SOCKET_ERROR)
		{
			if (WSAGetLastError() != WSAEWOULDBLOCK)
				return FALSE;
		}

		//LINGER lg;
		//lg.l_onoff = 0;
		//lg.l_linger = 0;
		//if (setsockopt(m_hSocket, SOL_SOCKET, SO_LINGER, (char*) &lg, sizeof(lg)) == SOCKET_ERROR)
		//	return FALSE;

		m_bActive = TRUE;

		return TRUE;
	}

	BOOL Read(VOID)
	{
		CFMSSyncObj FMSSync;

		if (!m_bActive || !m_hSocket)
			return FALSE;

		WSABUF WsaBuf;
		DWORD dwReadBytes = 0;
		DWORD dwReadFlag = 0;

		WsaBuf.buf = (CHAR*) m_aucReadBuf;// + m_dwRemainLen;
		WsaBuf.len = FMS_MAX_BUF_LEN;// - m_dwRemainLen;

		INT iRet = WSARecv(m_hSocket,
			&WsaBuf,
			1,
			&dwReadBytes,
			&dwReadFlag,
			&m_poRead->m_oOverlapped,
			NULL);

		if( iRet == 0 )
		{

		}
		else if( iRet == SOCKET_ERROR && WSAGetLastError() == WSA_IO_PENDING )
		{

		}
		else
		{
			return FALSE;
		}

		/*
		if (iRet == SOCKET_ERROR && WSAGetLastError() != WSA_IO_PENDING && WSAGetLastError() != WSAEWOULDBLOCK)
			return FALSE;
		*/
		return TRUE;
	}

	BOOL ReadPacket(st_FMSMSGHEAD& _hdr, BYTE *pucData, DWORD &rdwRemainLen, DWORD &dwDataLen)
	{
		// CFMSSyncObj FMSSync;

		//{
		//	//-------------------------------------------------------------------------
		//	TCHAR tempStr[409600] = {0,};
		//	TCHAR _hex[5] ={0,};
		//	for (DWORD i = 0; i < m_dwRemainLen; i++)
		//	{
		//		_snprintf_s(_hex, 5, "%02x ", m_aucPacketBuf[i]);
		//		_tcscat_s(tempStr, _hex);
		//	}
		//	_tcscat_s(tempStr, "\n");
		//	//_snwprintf_s(tempStr, 1024, L"IO_READ %d \n", ponios->m_eIO);
		//	OutputDebugString(tempStr);
		//	//-------------------------------------------------------------------------
		//}

		//TRACE("\n\n  ReadPacket m_dwRemainLen %d / rdwRemainLen %d \n\n"
		//	, m_dwRemainLen
		//	, rdwRemainLen);

		if (!pucData || m_dwRemainLen > FMS_MAX_BUF_LEN_EXTRA || !m_bActive || m_dwRemainLen == 0 || !m_hSocket)
			return FALSE;

		////-------------------------------------------------------------------------
		//TCHAR tempStr[409600] = {0,};
		//TCHAR _hex[5] ={0,};
		//for (DWORD i = 0; i < m_dwRemainLen; i++)
		//{
		//	_snprintf_s(_hex, 5, "%02x ", m_aucPacketBuf[i]);
		//	_tcscat_s(tempStr, _hex);
		//}
		//_tcscat_s(tempStr, "\n");
		////_snwprintf_s(tempStr, 1024, L"IO_READ %d \n", ponios->m_eIO);
		//OutputDebugString(tempStr);
		////-------------------------------------------------------------------------

		//m_dwRemainLen = rdwRemainLen;	
		dwDataLen = m_dwRemainLen;

		PACKET_ERROR pe = PACKET_NOERROR;
		if ((pe = m_poPacketBox->WrapOffPacket(_hdr
			, m_aucPacketBuf, pucData, dwDataLen)) != PACKET_NOTENOUGH_LENGTH)
		{
			//memcpy(&_hdr, m_aucPacketBuf, sizeof(EP_MSG_HEADER));
			//if(_hdr.nID != EP_ID_FORM_MODULE)
			//{
			//	pe = PACKET_HEAD_CHECK_FAIL;				
			//}
			//
			//memcpy(&pucData, m_aucPacketBuf + sizeof(EP_MSG_HEADER), _hdr.nLength - sizeof(EP_MSG_HEADER));
			//rdwPacketLen = _hdr.nLength - sizeof(EP_MSG_HEADER);

			//rdwRemainLen -= rdwPacketLen;
			//MoveMemory(m_aucPacketBuf, m_aucPacketBuf + rdwPacketLen, MAX_BUF_LEN_EXTRA - rdwPacketLen);
			//m_dwRemainLen -= rdwPacketLen;

			//rdwRemainLen -= dwDataLen + sizeof(EP_MSG_HEADER);
						
			CFMSLog::RecvFMSLog((CHAR*)m_aucPacketBuf, atoi(_hdr.Command));

			if(PACKET_HEAD_CHECK_FAIL == pe)//';' 찾아서 길이 빼기
			{
				MoveMemory(m_aucPacketBuf
					, m_aucPacketBuf + dwDataLen
					, FMS_MAX_BUF_LEN_EXTRA - dwDataLen);

				m_dwRemainLen -= dwDataLen;

				m_procTotlen += dwDataLen; 
			}
			else
			{
				MoveMemory(m_aucPacketBuf
					, m_aucPacketBuf + (dwDataLen + FMS_HEAD_LEN)
					, FMS_MAX_BUF_LEN_EXTRA - (dwDataLen + FMS_HEAD_LEN));

				m_dwRemainLen -= dwDataLen + FMS_HEAD_LEN;

				m_procTotlen += dwDataLen + FMS_HEAD_LEN; 
			}
			
			//TRACE("\n\n  After WrapOffPacket 0x%x / %d / m_dwRemainLen %d / rdwRemainLen %d \n\n"
			//	, _hdr.nCommand
			//	, _hdr.nLength
			//	, m_dwRemainLen
			//	, rdwRemainLen);

			switch(pe)
			{
			case PACKET_NOERROR:
				return TRUE;
			case PACKET_HEAD_CHECK_FAIL:
			case PACKET_PACKETNO_FAIL:
			case PACKET_CHECKSUM_FAIL:
			case PACKET_SYSTEM_FAIL:
				CFMSLog::WriteErrorLog("%s", gszPACKET_ERROR[pe]);
				dwDataLen = 0;
				return TRUE;
			}
		}

		//TRACE("\n\n PACKET_NOTENOUGH_LENGTH 0x%x / %d / m_dwRemainLen %d / rdwRemainLen %d \n\n"
		//	, _hdr.nCommand
		//	, _hdr.nLength
		//	, m_dwRemainLen
		//	, rdwRemainLen);

		return FALSE;
	}
	BOOL Write(st_FMSMSGHEAD& _hdr, BYTE *pucData, DWORD dwLen)
	{
		CFMSSyncObj FMSSync;

		if (dwLen >= FMS_MAX_BUF_LEN || !m_bActive || !m_hSocket)
			return FALSE;

		WSABUF WsaBuf;
		DWORD dwWriteBytes = 0;
		DWORD dwWriteFlag = 0;
		ULONG ulLen = 0;
		
		CFMSManagedBufSP Buf;

		if (m_poPacketBox->WrapPacket(_hdr, pucData, Buf->m_aucBuf, dwLen))
		{
			WsaBuf.buf = (CHAR*) Buf->m_aucBuf;
			WsaBuf.len = FMS_HEAD_LEN + dwLen;

			CFMSOverlapped *poOverlapped = new CFMSOverlapped();
			poOverlapped->m_eIoType = IO_WRITE;
			poOverlapped->m_poObject = m_poOwnerNetObj;

			INT iRet = WSASend(m_hSocket,
				&WsaBuf,
				1,
				&dwWriteBytes,
				dwWriteFlag,
				&poOverlapped->m_oOverlapped,
				NULL);

			if (iRet == SOCKET_ERROR && WSAGetLastError() != WSA_IO_PENDING && WSAGetLastError() != WSAEWOULDBLOCK)
			{
				INT _error = WSAGetLastError();
				delete poOverlapped;
				m_bActive = FALSE;
				return FALSE;
			}
			
			if( iRet == 0 )
			{
				CFMSLog::SendFMSLog((CHAR*)Buf->m_aucBuf, atoi(_hdr.Command));			
			}
		}

		return TRUE;
	}

	BOOL Write(BYTE *pucData, DWORD dwLen)
	{
		CFMSSyncObj FMSSync;

		if (dwLen > MAX_RESULT_BUFFER_LENGTH || !m_bActive || !m_hSocket)
			return FALSE;
			
		// if ( !m_bActive || !m_hSocket )
		//	return FALSE;

		WSABUF WsaBuf;
		DWORD dwWriteBytes = 0;
		DWORD dwWriteFlag = 0;
		ULONG ulLen = 0;

		CFMSManagedBufSP Buf;
		
		memcpy(Buf->m_aucResultBuf, pucData, dwLen);
		WsaBuf.buf = (CHAR*) Buf->m_aucResultBuf;
		WsaBuf.len = dwLen;

		CFMSOverlapped *poOverlapped = new CFMSOverlapped();
		poOverlapped->m_eIoType = IO_WRITE;
		poOverlapped->m_poObject = m_poOwnerNetObj;

		INT iRet = WSASend(m_hSocket,
			&WsaBuf,
			1,
			&dwWriteBytes,
			dwWriteFlag,
			&poOverlapped->m_oOverlapped,
			NULL);

		if (iRet == SOCKET_ERROR && WSAGetLastError() != WSA_IO_PENDING && WSAGetLastError() != WSAEWOULDBLOCK)
		{
			INT _error = WSAGetLastError();
			delete poOverlapped;
			m_bActive = FALSE;								
			return FALSE;
		}
		
		return TRUE;
	}

	BOOL ForceClose(VOID)
	{
		CFMSSyncObj FMSSync;

		if (m_hSocket)
		{	
			m_HeartBeatCheck = 0;
			m_bActive = FALSE;
			m_IsWorking = FALSE;
			shutdown(m_hSocket, SD_BOTH);
			closesocket(m_hSocket);
			m_hSocket = NULL;
			//-------------------------------------------------------------------------
			TCHAR tempStr[1024] = {0,};
			_snprintf_s(tempStr, 1024, "ForceClose m_hSocket %d\n", m_hSocket);
			OutputDebugString(tempStr);
			//-------------------------------------------------------------------------

			return TRUE;
		}
		return FALSE;
	}

	VOID InitPacketInfo(VOID)
	{
		CFMSSyncObj FMSSync;
		m_poPacketBox->Init();
	}

	BOOL DoubleBuffering(DWORD dwLen)
	{
		CFMSSyncObj FMSSync;

		//{
		//	//-------------------------------------------------------------------------
		//	TCHAR tempStr[409600] = {0,};
		//	TCHAR _hex[5] ={0,};
		//	for (DWORD i = 0; i < dwLen; i++)
		//	{
		//		_snprintf_s(_hex, 5, "%02x ", m_aucReadBuf[i]);
		//		_tcscat_s(tempStr, _hex);
		//	}
		//	_tcscat_s(tempStr, "\n");
		//	//_snwprintf_s(tempStr, 1024, L"IO_READ %d \n", ponios->m_eIO);
		//	OutputDebugString(tempStr);
		//	//-------------------------------------------------------------------------
		//}

		if (!m_bActive || !m_hSocket)
			return FALSE;

		if (dwLen + m_dwRemainLen > FMS_MAX_BUF_LEN_EXTRA)
			return FALSE;

		CopyMemory(m_aucPacketBuf + m_dwRemainLen, m_aucReadBuf, dwLen);
		m_dwRemainLen += dwLen;

		m_recvTotlen +=dwLen;

		//TRACE("DoubleBuffering [m_dwRemainLen] %d, [dwLen] %d \n"
		//	, m_dwRemainLen, dwLen);

		return TRUE;
	}

	SOCKET GetSocket(VOID)
	{
		CFMSSyncObj FMSSync; 
		return m_hSocket; 
	}
	BOOL GetActive(VOID)
	{ 
		//CSyncObj Sync; 
		return m_bActive;
	}
	
	BOOL GetIsWork(VOID)
	{ 
		CFMSSyncObj FMSSync;
		return m_IsWorking;
	}
	
	VOID SetIsWork( BOOL bWorking )
	{
		CFMSSyncObj FMSSync;
		m_IsWorking = bWorking;
	}
	
	VOID SetHeartbeatClear()
	{
		CFMSSyncObj FMSSync;
		m_HeartBeatCheck = 0;
	}
		
	BOOL ChkHeartbeat()
	{
		CFMSSyncObj FMSSync;

		if( !m_bActive )
		{
			return FALSE;
		}
		
		m_HeartBeatCheck++;
		
		if( m_HeartBeatCheck > 100 * FMS_HEARTBEATCHECK )		
		{
			m_HeartBeatCheck = 0;
			return TRUE;
		}
		
		return FALSE;
	}

	BOOL GetRemoteAddressAfterAccept(LPTSTR remoteAddress, USHORT &remotePort)
	{
		CFMSSyncObj FMSSync;

		if (!remoteAddress)
			return FALSE;

		sockaddr_in		*Local			= NULL;
		INT				LocalLength		= 0;

		sockaddr_in		*Remote			= NULL;
		INT				RemoteLength	= 0;

		GetAcceptExSockaddrs(m_aucReadBuf, 
			0, 
			sizeof(sockaddr_in) + 16, 
			sizeof(sockaddr_in) + 16,
			(sockaddr **) &Local,
			&LocalLength,
			(sockaddr **) &Remote,
			&RemoteLength);

		_tcscpy_s(remoteAddress, 16, (LPTSTR)(inet_ntoa(Remote->sin_addr)));
		//CHAR	TempRemoteAddress[32] = {0,};
		//strcpy(TempRemoteAddress, inet_ntoa(Remote->sin_addr));

		//MultiByteToWideChar(CP_ACP,
		//	0,
		//	TempRemoteAddress,
		//	-1,
		//	remoteAddress,
		//	32);

		remotePort = ntohs(Remote->sin_port);

		return TRUE;
	}

	INT getRecvTotlen(){return m_recvTotlen;}
	INT getProcTotlen(){return m_procTotlen;}
	VOID ResetTotlen(){m_recvTotlen = m_procTotlen = 0;}

private:
	CFMSOverlapped *m_poAccept;
	CFMSOverlapped *m_poRead;
	CFMSPacketBox *m_poPacketBox;

	BYTE m_aucReadBuf[FMS_MAX_BUF_LEN];
	BYTE m_aucPacketBuf[FMS_MAX_BUF_LEN_EXTRA];

	SOCKET m_hSocket;
	BOOL m_bActive;
	BOOL m_IsWorking;
	DWORD m_dwRemainLen;
	DWORD m_HeartBeatCheck;

	CFMSObj *m_poOwnerNetObj;

	//////////////////////////////////////////////////////////////////////////
	INT m_recvTotlen;
	INT m_procTotlen;
};