// CellStateRecordSet.cpp : implementation file
//

#include "stdafx.h"
#include "CellStateRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCellStateRecordSet

IMPLEMENT_DYNAMIC(CCellStateRecordSet, CRecordset)

CCellStateRecordSet::CCellStateRecordSet(CDatabase* pdb)
	: CRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CCellStateRecordSet)
	m_ID = 0;
	m_TraySerial = 0;
	m_CellCode = 0;
	m_CellGrade = _T("");
	m_ChNo = 0;
	m_nFields = 5;
	//}}AFX_FIELD_INIT
	m_nDefaultType = snapshot;
}


CString CCellStateRecordSet::GetDefaultConnect()
{
	return _T("ODBC;DSN=TestCondition");
}

CString CCellStateRecordSet::GetDefaultSQL()
{
	return _T("[CellState]");
}

void CCellStateRecordSet::DoFieldExchange(CFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CCellStateRecordSet)
	pFX->SetFieldType(CFieldExchange::outputColumn);
	RFX_Long(pFX, _T("[ID]"), m_ID);
	RFX_Long(pFX, _T("[TraySerial]"), m_TraySerial);
	RFX_Long(pFX, _T("[CellCode]"), m_CellCode);
	RFX_Text(pFX, _T("[CellGrade]"), m_CellGrade);
	RFX_Long(pFX, _T("[ChNo]"), m_ChNo);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CCellStateRecordSet diagnostics

#ifdef _DEBUG
void CCellStateRecordSet::AssertValid() const
{
	CRecordset::AssertValid();
}

void CCellStateRecordSet::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG
