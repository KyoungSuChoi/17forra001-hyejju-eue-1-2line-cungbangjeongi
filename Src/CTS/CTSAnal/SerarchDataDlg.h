#if !defined(AFX_SERARCHDATADLG_H__D7302CB7_9376_4270_A5C6_53131E698B1D__INCLUDED_)
#define AFX_SERARCHDATADLG_H__D7302CB7_9376_4270_A5C6_53131E698B1D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SerarchDataDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSerarchDataDlg dialog
#include "MyGridWnd.h"
#include "CTSAnalDoc.h"

class CSerarchDataDlg : public CDialog
{
// Construction
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	void DisplayData(CString strFile, int nStartRow);
	void SetTitle(CString strTitle);
	CString m_strTitle;
	void SetFileName(CStringArray *aFileName);
	void InitGrid();
	CSerarchDataDlg(CCTSAnalDoc *pDoc, CWnd* pParent = NULL);   // standard constructor
	virtual ~CSerarchDataDlg();

// Dialog Data
	//{{AFX_DATA(CSerarchDataDlg)
	enum { IDD = IDD_ALL_DATA_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSerarchDataDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
	CCTSAnalDoc *m_pDoc;
protected:
	void SearchDataA();
	void SearchData();
	CStringArray * m_aFile;
	CMyGridWnd m_wndDataListGrid;

	// Generated message map functions
	//{{AFX_MSG(CSerarchDataDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnButtonExcel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERARCHDATADLG_H__D7302CB7_9376_4270_A5C6_53131E698B1D__INCLUDED_)
