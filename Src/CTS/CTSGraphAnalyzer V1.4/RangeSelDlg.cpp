// RangeSelDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSGraphAnal.h"
#include "RangeSelDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRangeSelDlg dialog


CRangeSelDlg::CRangeSelDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CRangeSelDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CRangeSelDlg)
	m_lFrom = 1;
	//}}AFX_DATA_INIT
	m_lTo = MAX_RAW_DATA_COUNT;
	m_lMax = 0;
	LanguageinitMonConfig();
}
CRangeSelDlg::~CRangeSelDlg()
{
	if( TEXT_LANG != NULL )					
	{					
		delete[] TEXT_LANG;					
		TEXT_LANG = NULL;					
	}
}

void CRangeSelDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRangeSelDlg)
	DDX_Text(pDX, IDC_FROM_EDIT, m_lFrom);
	DDX_Text(pDX, IDC_TO_EDIT, m_lTo);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CRangeSelDlg, CDialog)
	//{{AFX_MSG_MAP(CRangeSelDlg)
	ON_EN_CHANGE(IDC_FROM_EDIT, OnChangeFromEdit)
	ON_EN_CHANGE(IDC_TO_EDIT, OnChangeToEdit)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRangeSelDlg message handlers

BOOL CRangeSelDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// TODO: Add extra initialization here
	CString str;
	str.Format(TEXT_LANG[0], m_lMax);// 000683
	GetDlgItem(IDC_MAX_STATIC)->SetWindowText(str);

	str.Format(TEXT_LANG[1], MAX_RAW_DATA_COUNT);
	GetDlgItem(IDC_WARING_STATIC)->SetWindowText(str); //000684

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CRangeSelDlg::OnChangeFromEdit() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	CString str;
	str.Format("���� : %d", m_lTo-m_lFrom+1);
	GetDlgItem(IDC_COUNT_STATIC)->SetWindowText(str);
}

void CRangeSelDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
	if(m_lFrom > m_lTo)
	{
		AfxMessageBox(TEXT_LANG[2], MB_OK|MB_ICONSTOP); //000685
		return;
	}

	if(m_lFrom > m_lMax)
	{
		AfxMessageBox(TEXT_LANG[3], MB_OK|MB_ICONSTOP); //000686
		return;
	}

	if(m_lTo-m_lFrom+1 > MAX_RAW_DATA_COUNT)
	{
		if(AfxMessageBox(TEXT_LANG[4],
						MB_ICONQUESTION|MB_YESNO) == IDNO)
						return; //000687
	}

	CDialog::OnOK();
}

void CRangeSelDlg::OnChangeToEdit() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	CString str;
	str.Format("���� : %d", m_lTo-m_lFrom+1);
	GetDlgItem(IDC_COUNT_STATIC)->SetWindowText(str);	
}



BOOL CRangeSelDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CRangeSelDlg"), _T("TEXT_CRangeSelDlg_CNT"), _T("TEXT_CRangeSelDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt];

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CRangeSelDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CRangeSelDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Languge error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}