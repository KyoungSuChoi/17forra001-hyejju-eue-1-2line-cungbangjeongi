// CpkSetDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "CpkSetDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCpkSetDlg dialog
bool CCpkSetDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCpkSetDlg"), _T("TEXT_CCpkSetDlg_CNT"), _T("TEXT_CCpkSetDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CCpkSetDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCpkSetDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

CCpkSetDlg::CCpkSetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCpkSetDlg::IDD, pParent)
{
	LanguageinitMonConfig();

	//{{AFX_DATA_INIT(CCpkSetDlg)
	m_strHighRange = _T("");
	m_strLowRange = _T("");
	m_strGraphGap = _T("");
	m_strTitle = TEXT_LANG[0];//"분포도 보기"
	//}}AFX_DATA_INIT
}

CCpkSetDlg::~CCpkSetDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


void CCpkSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCpkSetDlg)
	DDX_Control(pDX, IDC_EDIT_GRAPHGAP, m_pGraphGap);
	DDX_Control(pDX, IDC_EDIT_HIGHRANGE, m_pHighRange);
	DDX_Text(pDX, IDC_EDIT_HIGHRANGE, m_strHighRange);
	DDX_Text(pDX, IDC_EDIT_LOWRANGE, m_strLowRange);
	DDX_Text(pDX, IDC_EDIT_GRAPHGAP, m_strGraphGap);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCpkSetDlg, CDialog)
	//{{AFX_MSG_MAP(CCpkSetDlg)
	ON_CBN_SELCHANGE(IDC_COMBO1, OnSelchangeCombo1)
	ON_EN_CHANGE(IDC_EDIT_HIGHRANGE, OnChangeEditHighrange)
	ON_EN_CHANGE(IDC_EDIT_LOWRANGE, OnChangeEditLowrange)
	ON_BN_CLICKED(IDC_NORANGE, OnNorange)
	ON_BN_CLICKED(IDC_RANGE, OnRange)
	ON_EN_CHANGE(IDC_EDIT_GRAPHGAP, OnChangeEditGraphgap)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCpkSetDlg message handlers

void CCpkSetDlg::OnSelchangeCombo1() 
{
	// TODO: Add your control notification handler code here
/*	int n;
	CComboBox* pCombo = (CComboBox*)GetDlgItem(IDC_COMBO1);
	n = pCombo->GetCurSel();

	switch(n)
	{
	case 0:
		m_nItem = RPT_PRE_CAP;
		break;
	case 1:
		m_nItem = RPT_FORM_CHARGE_CAP1;
		break;
	case 2:
		m_nItem = RPT_FORM_DISCHARGE_CAP;
		break;
	case 3:
		m_nItem = RPT_OCV;
		break;
	case 4:
		m_nItem = RPT_OUT_IMP;
		break;
	}
*/
}

BOOL CCpkSetDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CComboBox* pCombo = (CComboBox*)GetDlgItem(IDC_COMBO1);
	for(int i =0; i<m_nCount; i++)
	{
		if(m_field[i].ID == EP_REPORT_GRADING || m_field[i].ID == EP_REPORT_CH_CODE)	continue;
		pCombo->AddString(m_field[i].FieldName);
	}
	pCombo->SetCurSel(0);

	GetDlgItem(IDC_STATIC_TITLE)->SetWindowText(m_strTitle);
	
/*	if(m_nItem  == RPT_FORM_CHARGE_CAP1)
	{
		pCombo->SetCurSel(1);	
	}
	else if(m_nItem == RPT_FORM_DISCHARGE_CAP)
	{
		pCombo->SetCurSel(2);	
	}
	else if(m_nItem == RPT_OCV)
	{
		pCombo->SetCurSel(3);	
	}
	else if(m_nItem == RPT_OUT_IMP)
	{
		pCombo->SetCurSel(4);	
	}
	else //if(m_nItem == RPT_PRE_CAP)
	{
		pCombo->SetCurSel(0);	
	}
*/
	pCombo->SetCurSel(m_nItem);	
	((CButton*)GetDlgItem(IDC_NORANGE))->SetCheck(TRUE);

	m_bHighRange = FALSE;
	m_bLowRange = FALSE;
//	m_nItem     = RPT_PRE_CAP;
	m_bRange    = FALSE;           // 전체 범위
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCpkSetDlg::OnChangeEditHighrange() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	GetDlgItem(IDC_EDIT_HIGHRANGE)->GetWindowText(m_strHighRange);
}

void CCpkSetDlg::OnChangeEditLowrange() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	GetDlgItem(IDC_EDIT_LOWRANGE)->GetWindowText(m_strLowRange);
}

void CCpkSetDlg::OnChangeEditGraphgap() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	GetDlgItem(IDC_EDIT_GRAPHGAP)->GetWindowText(m_strGraphGap);
}

void CCpkSetDlg::OnOK() 
{
	UpdateData(TRUE);
	CComboBox* pCombo = (CComboBox*)GetDlgItem(IDC_COMBO1);

	m_nItem = pCombo->GetCurSel();	
	if(m_nItem < 0)	
	{
		AfxMessageBox(TEXT_LANG[1]);//"Data 종류를 선택 하십시요"
		pCombo->SetFocus();
		return;

	}
	if(m_strHighRange.IsEmpty() && m_strLowRange.IsEmpty())
	{
		AfxMessageBox(TEXT_LANG[2]);//"상,하한치를 입력 하시오!"
		m_pHighRange.SetFocus();
		return;
	}

	float ff = atof(m_strGraphGap);
	if(m_strGraphGap.IsEmpty() || ff < 0.001f)
	{
		AfxMessageBox(TEXT_LANG[3]);//"Graph 간격 설정값이 너무 작거나 입력되지 않았습니다."
		GetDlgItem(IDC_EDIT_GRAPHGAP)->SetFocus();
		return;
	}

	if(!m_strHighRange.IsEmpty() && !m_strLowRange.IsEmpty())
	{
		m_bHighRange = TRUE;
		m_bLowRange  = TRUE;
		if(atof(m_strHighRange) < atof(m_strLowRange))
		{
			AfxMessageBox(TEXT_LANG[4]);//"상한 설정값이 하한 설정값보다 작습니다."
			GetDlgItem(IDC_EDIT_HIGHRANGE)->SetFocus();
			return;
		}
	}
	else if(m_strHighRange.IsEmpty() && !m_strLowRange.IsEmpty())
	{
		m_bHighRange = FALSE;
		m_bLowRange  = TRUE;
	}
	else if(!m_strHighRange.IsEmpty() && m_strLowRange.IsEmpty())
	{
		m_bHighRange = TRUE;
		m_bLowRange  = FALSE;
	}
	if(m_strGraphGap.IsEmpty())
	{
		AfxMessageBox(TEXT_LANG[5]);//"그래프간격을 입력하시오!"
		m_pGraphGap.SetFocus();
		return;
	}
	CDialog::OnOK();
}

void CCpkSetDlg::OnNorange() 
{
	// TODO: Add your control notification handler code here
	m_bRange = FALSE;
	((CButton *)GetDlgItem(IDC_NORANGE))->SetCheck(TRUE);
	((CButton *)GetDlgItem(IDC_RANGE))->SetCheck(FALSE);
}

void CCpkSetDlg::OnRange() 
{
	// TODO: Add your control notification handler code here
	m_bRange = TRUE;
	((CButton *)GetDlgItem(IDC_NORANGE))->SetCheck(FALSE);
	((CButton *)GetDlgItem(IDC_RANGE))->SetCheck(TRUE);
}

