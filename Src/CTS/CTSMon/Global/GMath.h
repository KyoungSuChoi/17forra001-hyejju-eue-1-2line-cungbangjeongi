#define round(a) ( int ) ( a + .5 )

class GMath
{
public:   
	GMath();
    ~GMath();
    
	static void push(element item) 
	{	
		stackNode* temp=(stackNode *)malloc(sizeof(stackNode));
		temp->data = item; 
		temp->link = top;	
		top = temp; 
	}

	static element pop() 
	{ 
		element item;
		stackNode* temp=top;

		if(top == NULL)
		{ 	 
			AfxMessageBox("Stack is empty!");	 
			return 0; 
		} 
		else
		{ 
			item = temp->data;	
			top = temp->link;	 
			free(temp);
			return item;
		} 
	} 

	static element peek() 
	{	
		if(top == NULL) 
		{		 
			AfxMessageBox("Stack is empty!");	 
			return 0;	 
		} 
		else
		{ 
			return top->data; 
		} 
	} 

	static void del() 
	{  
		stackNode* temp; 

		if(top == NULL) 
		{		
			AfxMessageBox("Stack is empty!");			
		} 
		else 
		{ 
			temp = top;	
			top = top->link;	 
			free(temp); 
		} 
	} 

	// 후위 표기 연산
	static element evalPostfix(char *exp)
	{
		double opr1, opr2;
		double value;
		int i=0, count=0;
		int length = strlen(exp);
		char symbol;

		for(i=0; i<length; i++)
		{
			symbol = exp[i];

			if(symbol >= '0' && symbol <= '9' || symbol == '.' )
			{
				while(exp[i + count] != ' ') // 숫자 구분을 위해 검사
				{
					count++;
				}
				value = atof(&exp[i]);
				i+=count;
				count=0;
				push(value);
			}
			else
			{
				if(symbol != ' ') // 숫자 구분을 위한 ' ' 이 아닌 경우에만 
				{
					opr2 = pop();
					opr1 = pop();

					switch(symbol)
					{
					case '^' : push(pow(opr1, opr2)); break;
					case '+' : push(opr1 + opr2); break;
					case '-' : push(opr1 - opr2); break;
					case '*' : push(opr1 * opr2); break;
					case '/' : push(opr1 / opr2); break;			
					}
				}
			}
		}
		return pop();
	}

	static void postfix_case_bloack(char* postfix, int *p)
	{
		char temp;

		while(1)
		{
			temp = (char)pop(); // 스택에서 하나를 꺼냄

			if((temp != '(') && (temp != '{') && (temp != '[')) // 열림 괄호가 아니라면
			{
				postfix[(*p)++] = temp; // 문자 배열에 저장
				postfix[(*p)++] = ' ';
			}
			else
			{
				break; 
			}
		}
	}

	static void postfix_case_operator_1(char* postfix, char symbol, int *p)
	{
		char temp;

		while(1)
		{
			if(top == NULL)
			{
				break;
			}
			temp = (char)pop(); // 스택에서 하나를 꺼냄

			if(temp == '+' || temp == '-' || temp == '*' || temp == '/' || temp == '^') // 연산 기호라면
			{
				postfix[(*p)++] = temp; // 문자 배열에 저장	
				postfix[(*p)++] = ' ';
			}
			else
			{
				push(temp); // 연산 기호가 아니면 다시 스택에 저장
				break;
			}
		}
		push(symbol); // 현재 연산 기호 저장
	}

	static void postfix_case_operator_2(char* postfix, char symbol, int *p)
	{
		char temp;

		while(1)
		{
			if(top == NULL)
			{
				break;
			}
			temp = (char)pop(); // 스택에서 하나를 꺼냄

			if(temp == '*' || temp == '/' || temp == '^') // 우선순위가 같은 연산 기호라면
			{
				postfix[(*p)++] = temp; // 문자 배열에 저장		
				postfix[(*p)++] = ' ';
			}
			else
			{
				push(temp); // 연산 기호가 아니면 다시 스택에 저장 
				break;
			}
		}
		push(symbol); // 현재 연산 기호 저장
	}

	static void postfix_case_default(char* postfix, char symbol, char* exp, int *p, int i)
	{
		postfix[(*p)++] = symbol; // 숫자를 문자 배열에 저장

		// 숫자 구분을 위해
		if(exp[i+1] == '+' || exp[i+1] == '-' || exp[i+1] == '*' || exp[i+1] == '/' ||
			exp[i+1] == ')' || exp[i+1] == '}' || exp[i+1] == ']' || exp[i+1] == '\0' || exp[i+1] == '^')
		{
			postfix[(*p)++] = ' '; // 공란 삽입
		}
	}

	// 중위 표기를 후위 표기로 변환
	static char* infix_to_postfix(char* exp)
	{
		int i=0, p=0;
		int length = strlen(exp);
		char symbol;

		char* postfix;
		postfix = new char[length*2];
		// char* postfix = (char*)malloc(length * 2);
		memset( postfix, 0, _msize(postfix));

		for(i=0; i<length; i++)
		{
			symbol = exp[i];

			switch(symbol)
			{		
			case '(' :
			case '{' :
			case '[' :
				push(symbol); // 열림 괄호를 스택에 저장
				break;

			case ')':
			case '}':
			case ']':
				postfix_case_bloack(postfix, &p);
				break;

			case '+' :			
			case '-' :	
				postfix_case_operator_1(postfix, symbol, &p);
				break;

			case '*' :			
			case '/' :				
			case '^' :
				postfix_case_operator_2(postfix, symbol, &p);
				break;

			default:
				postfix_case_default(postfix, symbol, exp, &p, i);
				break;
			}
		}

		while(top) // 스택에 남아 있는 값을
		{
			postfix[p++] = (char)pop(); // 문자 배열에 저장
			postfix[p++] = ' ';
		}
		postfix[p] = '\0'; // 문자 배열 마지막에 널 값 저장
		return postfix;
	}

	static int syntaxsearch(char *exp)
	{
		top = NULL;

		int length = strlen(exp);
		int i=0;
		int ncount=0;
		bool bOpr = true;
		char symbol, opr;
		CString strLog;

		for(i=0; i<length; i++)
		{
			symbol = exp[i];

			switch(symbol)
			{
			case '+' : 
			case '-' : 
			case '*' : 
			case '/' :
			case '^' :
				bOpr = true;

				opr = exp[i+1];
				if(opr == '+' || opr == '-' || opr == '*' || opr == '/' || opr == '^' )
				{
					AfxMessageBox("Operator error");
					return -1;
				}
				break;

			case '(' :
			case '{' :
			case '[' :
				if( bOpr == false )
				{
					AfxMessageBox("Operator missing error");
					return -1;
				}
				push(symbol);
				break;

			case ')':
				if(top == NULL)
				{
					AfxMessageBox("' ) ' Syntax error!");
					return -1;
				}
				else
				{
					if(pop() != '(')
					{
						AfxMessageBox("' ( ' Syntax error!");
						return -1;
					}
				}
				break;

			case '}':
				if(top == NULL)
				{
					AfxMessageBox("' } ' Syntax error!");
					return -1;
				}
				else
				{
					if(pop() != '{')
					{
						AfxMessageBox("' { ' Syntax error!");
						return -1;
					}
				}
				break;

			case ']':
				if(top == NULL)
				{
					AfxMessageBox("' ] ' Syntax error!");
					return -1;
				}
				else
				{
					if(pop() != '[')
					{
						AfxMessageBox("' [ ' Syntax error!");
						return -1;
					}
				}
				break;

			default:
				if(symbol < '0' || symbol > '9' )
				{
					if( symbol != '.' )
					{
						AfxMessageBox("All not number!");
						return -1;
					}
				}
				else
				{
					bOpr = false;
					ncount++;
				}
				break;
			}
		}

		if(!ncount || ncount == 1)
		{
			AfxMessageBox("Number is empty or unavailable!");
			return -1;
		}

		while(top)
		{
			opr = (char)pop();
			if(opr == '(' || opr == '{' || opr == '[' ||
				opr == '+' || opr == '-' || opr == '*' || opr == '/')
			{
				strLog.Format("' %c ' syntax error!", opr);
				AfxMessageBox(strLog);
				return -1;
			}		
		}	
		return 0;
	}

	/*static int math_RoundToInt(double x, int nIndex) 
	{
		double dValue = x;
		int i = 0, j;
		__int64 nGob = 1, nPosValue;
		while( i < nIndex )
		{
			nGob = nGob * 10;
			i++;
		}

		i = 0;
		while( nIndex > 0 ) 
		{
			j = 0;
			int nValue1 = 1;
			while( j < i )
			{
				nValue1 = nValue1 * 10;
				j++;
			}

			nPosValue = (__int64)( dValue * nGob / nValue1 );

			if( nPosValue % 10 >= 5 )
			{
				j = 0;
				double dValue1 = 1;
				while( j < nIndex - 1 )
				{
					dValue1 = dValue1 / 10;
					j++;
				}
				dValue = dValue + dValue1; 
			}
			nIndex--;
			i++;
		}

		return (int)dValue;
	}

	static CRect math_GetResolutionRect(CRect rc, int nResWidth, int nResHeight, int nWidth, int nHeight )
	{
		CRect rc1;
		rc1.left   = math_RoundToInt( rc.left   * ( (double)nResWidth  / (double)nWidth ), 4 );
		rc1.right  = math_RoundToInt( rc.right  * ( (double)nResWidth  / (double)nWidth ), 4 );
		rc1.top    = math_RoundToInt( rc.top    * ( (double)nResHeight / (double)nHeight ), 4 );
		rc1.bottom = math_RoundToInt( rc.bottom * ( (double)nResHeight / (double)nHeight ), 4 );

		//long TwipsPerPixelX  = 1440 / GetDeviceCaps(hdc, LOGPIXELSX);
        //long TwipsPerPixelY   = 1440 / GetDeviceCaps(hdc,LOGPIXELSY);	
				
		return rc1;
	}

	static BOOL math_IsInArea( CRect rc, CPoint point, int nGap=0)
	{
		CRect rect=CRect(rc.left-nGap,rc.top-nGap,rc.right+nGap,rc.bottom+nGap);
		return rect.PtInRect(point);		
	}
	
	static CString math_FormatSize(DWORD dwSizeLow, DWORD dwSizeHigh=0)
	{
		TCHAR szBuff[100];		

		unsigned __int64 nFileSize = ((unsigned __int64)(((DWORD)(dwSizeLow)) | 
									 ((unsigned __int64)((DWORD)(dwSizeHigh))) << 32));
		unsigned __int64 kb = 1;

		if (nFileSize > 1024)
		{
			kb = nFileSize / 1024;
			if (nFileSize % 1024)
				kb++;
		}

		// make it a string
		_ui64tot(kb, szBuff, 10);

		// add thousand seperators
		int nLength = lstrlen(szBuff);
		if (nLength > 3)
		{
			LPCTSTR ptr = szBuff;
			ptr += (nLength-1);

			TCHAR szTemp[100];			
			LPTSTR ptrTemp = szTemp;
			for(int i=0; i<nLength; i++)
			{
				if (i && ((i % 3) == 0)) 
				{
					if (*ptrTemp != ',')
					{
						*ptrTemp = ',';
						ptrTemp = _tcsinc(ptrTemp);
					}
				}
				*ptrTemp = *ptr;
				ptrTemp = _tcsinc(ptrTemp);
				ptr = _tcsdec(szBuff, ptr);
			}
			// terminate string
			*ptrTemp = '\0';
			// reverse string
			_tcsrev(szTemp);
			lstrcpy(szBuff, szTemp);
		}		

		CString str=szBuff;
		return str+_T(" KB");
	}*/

} ;