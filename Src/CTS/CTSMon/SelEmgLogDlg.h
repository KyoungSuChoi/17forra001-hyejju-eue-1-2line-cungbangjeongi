#if !defined(AFX_SELEMGLOGDLG_H__4BAC872F_24AB_41C4_AB47_6A022A4A5F65__INCLUDED_)
#define AFX_SELEMGLOGDLG_H__4BAC872F_24AB_41C4_AB47_6A022A4A5F65__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelEmgLogDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSelEmgLogDlg dialog

class CSelEmgLogDlg : public CDialog
{
// Construction
public:
	CString m_strSelFileName;
	CString GetSelFileName();
	CSelEmgLogDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSelEmgLogDlg)
	enum { IDD = IDD_SEL_EMGLOG_DIALOG };
	CListCtrl	m_wndSelList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelEmgLogDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSelEmgLogDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDblclkSelList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnOk();
	afx_msg void OnItemchangedSelList(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELEMGLOGDLG_H__4BAC872F_24AB_41C4_AB47_6A022A4A5F65__INCLUDED_)
