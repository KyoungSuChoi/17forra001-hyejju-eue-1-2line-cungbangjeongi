#include "StdAfx.h"
#include "PneApp.h"


PneApp::PneApp(void)
{
	m_bSendFlag = FALSE;
	mesData.RemoveAll();
}

PneApp::~PneApp(void)
{
}


void PneApp::ReConnectTime()
{
	CTime CurTime=CTime::GetCurrentTime();
	int nElpSec=GTime::time_GetElapse(m_StartTime,CurTime);
    CString strDbtime=m_MySql.GetDBTimeFmt();

	if(nElpSec>=7200 || 
	   m_MySql.m_bCon==FALSE ||
	   m_MySql.m_iErr==2003 ||
       m_MySql.m_iErr==2013 ||
	   strDbtime.IsEmpty() || strDbtime==_T(""))
	{//1hour(3600)
		m_StartTime=CurTime;

		CString str;
		str.Format(_T("elp:%d dbcon:%d ierr:%d time:%s"),nElpSec,m_MySql.m_bCon,m_MySql.m_iErr,strDbtime);
		GLog::log_Save("mysql","db1","reconn1:"+str,NULL,TRUE);

		if( m_MySql.ReConn() == TRUE )
		{
			CreateTbCellTrayMap();
			CreateTbTrayWorkinfo();
		}

		GLog::log_Save("mysql","db1","reconn2",NULL,TRUE);
	}
}

void PneApp::ReConnectError()
{
	if(mainPneApp.m_MySql.m_iErr>0)
	{								
		if(mainPneApp.m_MySql.m_iErr==2013)
		{//lost connect				
			GLog::log_Save("mysql","db2","reconn_ConnectError1",NULL,TRUE);
			/*
			if( m_MySql.ReConn() == TRUE )
			{
				CreateTbMonitor();
				CreateTbWorkinfo();
				CreateTbWorkevent();
				CreateTbWorkstate();
				
				CString table;
				table.Format("STEP_%s",g_AppInfo.strBlockid);
				CreateTbWorkstep(table);
			}

			GLog::log_Save("mysql","db2","reconn2",NULL,TRUE);
			*/
		}
	}
}

BOOL PneApp::CreateTbTrayWorkinfo()
{
	CString table=_T("TrayWorkInfo");
	if(m_MySql.ChkTable(table)==TRUE) return TRUE;

	CString sql;
	sql.Format(_T("CREATE TABLE `%s` (       \
				  `NOWORK` int(11) NOT NULL AUTO_INCREMENT,\
				  `TrayNo` varchar(30) DEFAULT NULL,      \
				  `LotID` varchar(30) DEFAULT NULL,       \
				  `ModelID` int(5) DEFAULT NULL,       \
				  `ProcessID` int(5) DEFAULT NULL,    \
				  `DTREG` datetime DEFAULT NULL,          \
				  `WorkingSeq` int(1) DEFAULT NULL,\
				  PRIMARY KEY (`NOWORK`)                  \
				  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"), table);

	return m_MySql.SetQuery(sql);
}

BOOL PneApp::SetClearTrayMainppingInfo(CString strTrayNo)
{
	if(m_MySql.m_bCon==FALSE) return FALSE;

	BOOL bSucc = FALSE;
	 
	CString sql = _T("");
	sql.Format("DELETE FROM TrayWorkInfo WHERE TrayNo = '%s'", strTrayNo );
	GLog::log_Save("mysql","sql",sql,NULL,TRUE);
	bSucc = m_MySql.SetQuery(sql);
	if( bSucc == FALSE )
	{
		return bSucc;
	}

	sql.Format("DELETE FROM CellTrayMap WHERE TrayNo = '%s'", strTrayNo );
	GLog::log_Save("mysql","sql",sql,NULL,TRUE);
	bSucc = m_MySql.SetQuery(sql);
	if( bSucc == FALSE )
	{
		return bSucc;
	}	

	return bSucc;
}


BOOL PneApp::InsertTrayWorkinfo(CString trayno, CString lotid,  int nTypeId, int nProcessId)
{		
	if(m_MySql.m_bCon==FALSE) return FALSE;

	if(trayno.IsEmpty() || lotid == _T(""))       return TRUE;
	if(nTypeId==0 || nProcessId ==0 )     return TRUE;

	////////////////////////////////////////////////		
	CString sql=_T("");
	BOOL bSucc = FALSE;

	sql.Format(_T("DELETE FROM TrayWorkInfo WHERE TrayNo = '%s'"), trayno );
	GLog::log_Save("mysql","sql",sql,NULL,TRUE);
	bSucc = m_MySql.SetQuery(sql);
	if( bSucc == FALSE )
	{
		return bSucc;
	}

	sql.Format(_T("DELETE FROM CellTrayMap WHERE TrayNo = '%s'"), trayno );
	GLog::log_Save("mysql","sql",sql,NULL,TRUE);
	bSucc = m_MySql.SetQuery(sql);
	if( bSucc == FALSE )
	{
		return bSucc;
	}	

	sql.Format(_T("INSERT INTO TrayWorkInfo \
				  (TrayNo,LotID,ModelID,ProcessID,DTREG,WorkingSeq) \
				  VALUES ('%s','%s', %d, %d, NOW(), 0)")\
				  ,trayno, lotid, nTypeId, nProcessId );

	GLog::log_Save("mysql","sql",sql,NULL,TRUE);

	return m_MySql.SetQuery(sql);
}

BOOL PneApp::UpdateTrayWorkinfo(CString strTrayNo, int nProcessID, int nWorkingSeq)
{
	// if(m_MySql.m_bCon == FALSE) return FALSE;

	CString sql=_T("");
	
	sql.Format(_T("UPDATE trayworkinfo SET ProcessID = %d, WorkingSeq = %d WHERE TrayNo = '%s'"), nProcessID, nWorkingSeq, strTrayNo );

	mesData.AddTail(sql);

	return TRUE;
	// return m_MySql.SetQuery(sql);
}

BOOL PneApp::CreateTbCellTrayMap()
{
	CString table=_T("CellTrayMap");
	if(m_MySql.ChkTable(table)==TRUE) return TRUE;

	CString sql;	
	sql.Format(_T("CREATE TABLE `%s` (        \
				  `NOWORK` int(11) NOT NULL AUTO_INCREMENT,\
				  `TrayNo` varchar(30) DEFAULT NULL,      \
				  `LotID` varchar(30) DEFAULT NULL,      \
				  `ChannelNum` int(3) DEFAULT NULL,   \
				  `CellID` varchar(30) DEFAULT NULL,      \
				  PRIMARY KEY (`NOWORK`)                  \
				  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"), table);

	return m_MySql.SetQuery(sql);
}

BOOL PneApp::InsertCellTrayMap( CString trayno, CString lotid, int nChannelnum, CString cellid )
{		
	if(m_MySql.m_bCon==FALSE) return FALSE;
	
	if(trayno.IsEmpty() || nChannelnum == 0)       return TRUE;
	if(cellid.IsEmpty() )     return TRUE;

	////////////////////////////////////////////////		
	CString sql=_T("");
	sql.Format(_T("INSERT INTO CellTrayMap \
				  (TrayNo,LotID,ChannelNum,CellID) \
				  VALUES ('%s','%s', %d,'%s')")\
				  ,trayno, lotid, nChannelnum, cellid );

	GLog::log_Save("mysql","sql",sql,NULL,TRUE);

	return m_MySql.SetQuery(sql);
}


BOOL PneApp::IsMySqlLive()
{
	if(m_MySql.m_bCon==FALSE) return FALSE;

	int nRtn = 0;
	nRtn = mysql_ping(&m_MySql.m_Mysql);

	if( nRtn != 0 )
	{
		return FALSE;
	}

	return TRUE;
}


MYSQLINFO* PneApp::GetTrayMappingInfo(CString strTrayNo)
{	
	if(m_MySql.m_bCon==FALSE) return NULL;

	CString sql;
	sql.Format(_T("SELECT ChannelNum, CellID FROM CellTrayMap WHERE TrayNo = '%s'"), strTrayNo );	
	
	sql+=_T(" ORDER BY channelnum ASC");

	MYSQLINFO *MysqlInfo = m_MySql.obj_Result(sql);
	if(!MysqlInfo)
	{
		delete MysqlInfo; 
		return NULL;
	}

	return MysqlInfo;
}

MYSQLINFO* PneApp::GetTrayProcessInfo(CString strTrayNo)
{	
	if(m_MySql.m_bCon==FALSE) return NULL;

	CString sql;
	sql.Format(_T("SELECT * FROM TrayWorkInfo WHERE TrayNo = '%s' ORDER BY DTREG LIMIT 1"), strTrayNo );

	MYSQLINFO *MysqlInfo=m_MySql.obj_Result(sql);
	if(!MysqlInfo)
	{
		delete MysqlInfo; 
		return NULL;
	}

	return MysqlInfo;
}

BOOL PneApp::GetCellTrayMappingInfo(CString strCellNo, CString lotid, CString &strTrayNo)
{
	if( m_MySql.m_bCon==FALSE) return FALSE;

	CString sql;
	sql.Format(_T("SELECT TrayNo FROM CellTrayMap WHERE CellID = '%s' AND LotID = '%s'"), strCellNo, lotid );

	CStringArray *list=m_MySql.list_Query(sql);	
	if(m_MySql.list_ChkList(list)==FALSE) return FALSE;

	CString value=m_MySql.list_FDA(list,1,_T("TrayNo"));
	
	strTrayNo = value;

	m_MySql.list_FreeList(list);
	return TRUE;
}

BOOL PneApp::GetWorkinfoTbIndex( CString &strTbName, CString &strIdx )
{
	if( m_MySql.m_bCon==FALSE) return FALSE;

	CString sql;
	sql.Format(_T("SELECT * FROM tb_workinfo ORDER BY STARTDATE DESC, IDX DESC LIMIT 1"));
	CStringArray *list=m_MySql.list_Query(sql);	
	if(m_MySql.list_ChkList(list)==FALSE) return FALSE;

	CString value=m_MySql.list_FDA(list,1,_T("IDX"));
	strIdx = value;

	value += m_MySql.list_FDA(list,1,_T("EQUIPNM"));
	value += m_MySql.list_FDA(list,1,_T("BLOCKNM"));
	m_MySql.list_FreeList(list);

	strTbName = value;
	return TRUE;
}

/*
{		GetTrayMappingInfo
	

	CString strValue, strValue1, strTemp;
	BOOL bEdit = FALSE;
	int nIndex = 1;
	long lModelID = 0;
	long lTestID =0;
	int nCellCnt = 0;
	int nRow = 1;
	int nCol = 1;

	m_CellGrid.SetStyleRange(CGXRange().SetRows(2,3),
		CGXStyle().SetReadOnly(FALSE));

	m_CellGrid.SetStyleRange(CGXRange().SetRows(5,6),
		CGXStyle().SetReadOnly(FALSE));

	try
	{
		LoadTrayData(m_strTrayNo);

		recordSet.Open();
		recordSet.m_strFilter.Format("[Tray_No] = '%s'", strTrayNo);
		recordSet.m_strSort.Format("[CellIndexInTray]");
		recordSet.Requery();

		while(!recordSet.IsEOF())
		{
			if(nCol >= m_CellGrid.GetColCount()+1)
			{
				nRow +=3;
				nCol = 1;
			}	

			nCellCnt++;
			if( nCellCnt > 32 )	// 설정된 CH 수보다 많으면 멈춤
			{
				break;
			}
			strValue = recordSet.m_Cell_No;
			strValue.TrimRight();		

			if(!strValue.IsEmpty())
			{					
				m_CellGrid.SetValueRange(CGXRange(nRow, nCol), strValue);

				//20101110 KBH
				//strTemp.Format("최종공정 [%s]", GetDocument()->SearchTestName(recordSet.m_Test_ID, strValue));
				strTemp.Format("최종공정 [%s]", GetDocument()->SearchTestName(recordSet.m_Test_ID));//, strValue));
				m_CellGrid.SetValueRange(CGXRange(nRow+1, nCol), strTemp);

				lModelID = recordSet.m_Model_ID;
				lTestID = recordSet.m_Test_ID;

				strValue = recordSet.m_Lot_No;
				strValue.TrimRight();
				strValue1 = recordSet.m_Cell_Type;
				strValue1.TrimRight();
				m_strLotNo = strValue;
				strTemp.Format("LotNo [%s] : Type [%s]", strValue, strValue1);

				m_CellGrid.SetValueRange(CGXRange(nRow+2, nCol), strTemp);	

				m_CellGrid.SetStyleRange(CGXRange(nRow, nCol),
					CGXStyle().SetInterior(RGB(243,248,84)));
				m_CellGrid.SetStyleRange(CGXRange(nRow+1, nCol, nRow+2, nCol),
					CGXStyle().SetInterior(RGB(211,208,154)));			
			}
			else
			{
				m_CellGrid.SetValueRange(CGXRange(nRow, nCol), "");
			}
			nCol++;			
			recordSet.MoveNext();
		}
		if (nCol == 1 && nRow == 1)
		{
			strTrayNo = "";
			AfxMessageBox("조회할 Tray가 없습니다.");
		}
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return strTrayNo;
	}
	recordSet.Close();	

	if(lModelID > 0)
	{
		m_lSelectedModelID = lModelID;
		m_lSelectedTestID = lTestID;

		m_bChangedModelID = FALSE;
	}
	else
	{
		m_lSelectedModelID = 0;
		m_lSelectedTestID = 0;

		long m_lSelectedTypeID;
		long m_lSelectedProcessID;

	}
	//m_strViewModel = GetDocument()->SearchModelName(lModelID);
	m_strSelectedModelName = GetDocument()->SearchModelName(m_lSelectedModelID);
	m_strSelectedTestName = GetDocument()->SearchTestName(m_lSelectedTestID);

	UpdateData(FALSE);

	SetEditMode(FALSE);
	m_CellGrid.SetStyleRange(CGXRange().SetRows(2,3),
		CGXStyle().SetReadOnly(TRUE));

	m_CellGrid.SetStyleRange(CGXRange().SetRows(5,6),
		CGXStyle().SetReadOnly(TRUE));

	return strTrayNo;
}
*/
/*
BOOL PneApp::CreateTbMonitor()
{//Src Table Create		
	CString table=_T("monitorinfo");//MyISAM
	if(m_MySql.ChkTable(table)==FALSE)
	{
		CString sql;	   
		sql.Format(_T("CREATE TABLE `%s` (          \
					  `NOMONITOR` int(5) NOT NULL AUTO_INCREMENT, \
					  `NMWORK` varchar(100) DEFAULT NULL,         \
					  `TESTSERIAL` varchar(20) DEFAULT NULL,      \
					  `MODULEID` varchar(30) DEFAULT NULL,        \
					  `NMMODULE` varchar(20) DEFAULT NULL,        \
					  `DEVICEID` varchar(10) DEFAULT NULL,        \
					  `BLOCKID` varchar(10) DEFAULT NULL,         \
					  `NMSCHED` varchar(20) DEFAULT NULL,         \
					  `CHANNEL` varchar(10) DEFAULT NULL,         \
					  `CHNO` varchar(10) DEFAULT NULL,            \
					  `CHSTEPNO` varchar(3) DEFAULT NULL,         \
					  `CHSTATE` varchar(10) DEFAULT NULL,         \
					  `EQSTATE` varchar(3) DEFAULT NULL,          \
					  `EQSTR` varchar(30) DEFAULT NULL,           \
					  `CHSTEPTYPE` varchar(3) DEFAULT NULL,       \
					  `CHCODE` varchar(3) DEFAULT NULL,           \
					  `CHGRADECODE` varchar(3) DEFAULT NULL,      \
					  `NCURRENTCYCLENUM` varchar(20) DEFAULT NULL,\
					  `NTOTALCYCLENUM` varchar(20) DEFAULT NULL,  \
					  `FVOLTAGE` varchar(20) DEFAULT NULL,        \
					  `FCURRENT` varchar(20) DEFAULT NULL,        \
					  `FCAPACITY` varchar(20) DEFAULT NULL,       \
					  `FDISCAPACITY` varchar(20) DEFAULT NULL,    \
					  `FCAPACITYSUM` varchar(20) DEFAULT NULL,"),table);

		sql+=_T("`FWATT` varchar(20) DEFAULT NULL,  \
				`FWATTHOUR` varchar(20) DEFAULT NULL,       \
				`FDISWATTHOUR` varchar(20) DEFAULT NULL,    \
				`FWATTHOURSUM` varchar(20) DEFAULT NULL,    \
				`FAVGVOLTAGE` varchar(20) DEFAULT NULL,     \
				`FAVGCURRENT` varchar(20) DEFAULT NULL,     \
				`FSTEPTIME` varchar(20) DEFAULT NULL,       \
				`FTOTALTIME` varchar(20) DEFAULT NULL,      \
				`FDAYTIME` varchar(20) DEFAULT NULL,        \
				`FDAYTOTTIME` varchar(20) DEFAULT NULL,     \
				`FIMPEDANCE` varchar(20) DEFAULT NULL,      \
				`FTEMPARATURE` varchar(20) DEFAULT NULL,    \
				`FTEMPARATURE2` varchar(20) DEFAULT NULL,   \
				`FTEMPARATURE3` varchar(20) DEFAULT NULL,   \
				`FOVENTEMPARATURE` varchar(20) DEFAULT NULL,\
				`FPRESSURE` varchar(20) DEFAULT NULL,       \
				`FMETER` varchar(20) DEFAULT NULL,          \
				`FSHARINGINFO` varchar(20) DEFAULT NULL,    \
				`FCHAMBERUSING` varchar(20) DEFAULT NULL,   \
				`FREALDATE` varchar(20) DEFAULT NULL,       \
				`FREALCLOCK` varchar(20) DEFAULT NULL,      \
				`FDEBUG1` varchar(20) DEFAULT NULL,         \
				`FDEBUG2` varchar(20) DEFAULT NULL,         \
				`STARTTIME` varchar(20) DEFAULT NULL,       \
				`ENDTIME` varchar(20) DEFAULT NULL,         \
				`GOTOCYCLECOUNT` varchar(3) DEFAULT NULL,   \
				`MESMAPPINGLOT` varchar(20) DEFAULT NULL,   \
				`MESCELLNO` varchar(20) DEFAULT NULL,       \
				`MESSAMPLENO` varchar(20) DEFAULT NULL,     \
				`MESLASTCYCLENO` varchar(10) DEFAULT NULL,  \
				`MESLASTSTEPSEQ` varchar(10) DEFAULT NULL,  \
				`DTREG` datetime DEFAULT NULL,              \
				PRIMARY KEY (`NOMONITOR`)                   \
				) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

		return m_MySql.SetQuery(sql);
	}
	return TRUE;
}


BOOL PneApp::CreateTbWorkinfo()
{//worinfo Table Create		
	CString table=_T("WORKINFO");
	if(m_MySql.ChkTable(table)==TRUE) return TRUE;

	CString sql;
	sql.Format(_T("CREATE TABLE `%s` (       \
				  `NOWORK` int(11) NOT NULL AUTO_INCREMENT,\
				  `DEVICEID` varchar(10) DEFAULT NULL,     \
				  `BLOCKID` varchar(5) DEFAULT NULL,       \
				  `LOTID` varchar(30) DEFAULT NULL,        \
				  `CELLID` varchar(30) DEFAULT NULL,       \
				  `CHANNEL` varchar(2) DEFAULT NULL,       \
				  `MODULEID` varchar(2) DEFAULT NULL,      \
				  `CHANNELIDX` varchar(2) DEFAULT NULL,    \
				  `NMMODULE` varchar(20) DEFAULT NULL,     \
				  `DTSTART` datetime DEFAULT NULL,         \
				  `NMWORK` varchar(100) DEFAULT NULL,      \
				  `WORKER` varchar(50) DEFAULT NULL,       \
				  `IP` varchar(30) DEFAULT NULL,           \
				  `SCHEDNM` varchar(100) DEFAULT NULL,     \
				  `SCHEDFILE` longblob,                    \
				  `SCHEDSIZE` varchar(10) DEFAULT NULL,    \
				  `STAT` varchar(1) DEFAULT NULL,          \
				  `DTREG` datetime DEFAULT NULL,           \
				  PRIMARY KEY (`NOWORK`)                   \
				  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"),table);

	return m_MySql.SetQuery(sql);
}

BOOL PneApp::CreateTbWorkevent()
{//workevent Table Create	 7002,7005	
	CString table=_T("WORKEVENT");
	if(m_MySql.ChkTable(table)==TRUE) return TRUE;
	
CString sql;
sql.Format(_T("CREATE TABLE `%s` (        \
`NOEVENT` int(11) NOT NULL AUTO_INCREMENT,\
`DEVICEID` varchar(10) DEFAULT NULL,      \
`BLOCKID` varchar(5) DEFAULT NULL,        \
`LOTID` varchar(30) DEFAULT NULL,         \
`CELLID` varchar(30) DEFAULT NULL,        \
`CHANNEL` varchar(2) DEFAULT NULL,        \
`MODULEID` varchar(2) DEFAULT NULL,       \
`CHANNELIDX` varchar(2) DEFAULT NULL,     \
`SAMPLENO` varchar(30) DEFAULT NULL,      \
`NMWORK` varchar(100) DEFAULT NULL,       \
`TPSTATE` varchar(4) DEFAULT NULL,        \
`STAT` varchar(1) DEFAULT NULL,           \
`DTREG` datetime DEFAULT NULL,            \
PRIMARY KEY (`NOEVENT`)                   \
) ENGINE=InnoDB DEFAULT CHARSET=utf8;"),table);

	return m_MySql.SetQuery(sql);
	
}

BOOL PneApp::CreateTbWorkstate()
{//workstat Table Create 7004	
	CString table=_T("WORKSTATE");
	if(m_MySql.ChkTable(table)==TRUE) return TRUE;
		
CString sql;
sql.Format(_T("CREATE TABLE `%s` (        \
`NOSTATE` int(11) NOT NULL AUTO_INCREMENT,\
`DEVICEID` varchar(10) DEFAULT NULL,      \
`BLOCKID` varchar(5) DEFAULT NULL,        \
`LOTID` varchar(30) DEFAULT NULL,         \
`CELLID` varchar(30) DEFAULT NULL,        \
`CHANNEL` varchar(2) DEFAULT NULL,        \
`MODULEID` varchar(2) DEFAULT NULL,       \
`CHANNELIDX` varchar(2) DEFAULT NULL,     \
`TPSTATE` varchar(1) DEFAULT NULL,        \
`STAT` varchar(1) DEFAULT NULL,           \
`DTREG` datetime DEFAULT NULL,            \
PRIMARY KEY (`NOSTATE`)                   \
) ENGINE=InnoDB DEFAULT CHARSET=utf8;"),table);
	   
	return m_MySql.SetQuery(sql);
}


BOOL PneApp::CreateTbWorkstep(CString table)
{//workstep Table Create		
	if(m_MySql.ChkTable(table)==TRUE) return TRUE;

CString sql1;
sql1.Format(_T("CREATE TABLE `%s` (         \
`NOSTEP` int(11) NOT NULL AUTO_INCREMENT,   \
`DEVICEID` varchar(10) DEFAULT NULL,        \
`BLOCKID` varchar(5) DEFAULT NULL,          \
`LOTID` varchar(30) DEFAULT NULL,           \
`CELLID` varchar(30) DEFAULT NULL,          \
`CHANNEL` varchar(2) DEFAULT NULL,          \
`MODULEID` varchar(2) DEFAULT NULL,         \
`CHANNELIDX` varchar(2) DEFAULT NULL,       \
`SAMPLENO` varchar(30) DEFAULT NULL,        \
`NMWORK` varchar(100) DEFAULT NULL,         \
`CYCLENO` varchar(10) DEFAULT NULL,         \
`STEPNO` varchar(10) DEFAULT NULL,"),table);

CString sql2;
sql2.Format(_T("`CHNO` int(5) DEFAULT NULL, \
`CHSTEPNO` int(5) DEFAULT NULL,             \
`CHSTATE` int(5) DEFAULT NULL,              \
`CHSTEPTYPE` int(5) DEFAULT NULL,           \
`CHSTEPTYPESTR` varchar(10) DEFAULT NULL,   \
`LSTEP_SEQ` int(11) DEFAULT NULL,           \
`CHDATASELECT` int(11) DEFAULT NULL,        \
`CHCODE` int(11) DEFAULT NULL,              \
`CHGRADECODE` int(11) DEFAULT NULL,         \
`CHRESERVED` int(11) DEFAULT NULL,          \
`START_TIME` datetime DEFAULT NULL,         \
`END_TIME` datetime DEFAULT NULL,           \
`NINDEXFROM` int(11) DEFAULT NULL,          \
`NINDEXTO` int(11) DEFAULT NULL,            \
`NCURRENTCYCLENUM` int(11) DEFAULT NULL,    \
`NTOTALCYCLENUM` int(11) DEFAULT NULL,      \
`LSAVESEQUENCE` int(11) DEFAULT NULL,       \
`LRESERVED` int(11) DEFAULT NULL,           \
`NGOTOCYCLENUM` int(11) DEFAULT NULL,       \
`FVOLTAGE` float DEFAULT NULL,              \
`FCURRENT` float DEFAULT NULL,              \
`FCHARGE_AMPAREHOUR` float DEFAULT NULL,    \
`FDISCHARGE_AMPAREHOUR` float DEFAULT NULL, \
`FWATT` float DEFAULT NULL,                 \
`FCHARGE_WATTHOUR` float DEFAULT NULL,      \
`FDISCHARGE_WATTHOUR` float DEFAULT NULL,   \
`FSTEPTIME_DAY` float DEFAULT NULL,"));

CString sql3;
sql3.Format(_T("`FSTEPTIME` float DEFAULT NULL, \
`FTOTALTIME_DAY` float DEFAULT NULL,        \
`FTOTALTIME` float DEFAULT NULL,            \
`FIMPEDANCE` float DEFAULT NULL,            \
`FTEMPARATURE` float DEFAULT NULL,          \
`FTEMPARATURE2` float DEFAULT NULL,         \
`FTEMPARATURE3` float DEFAULT NULL,         \
`FOVENTEMPARATURE` float DEFAULT NULL,      \
`CHUSINGCHAMBER` int(11) DEFAULT NULL,      \
`CRECORDTIMENO` int(11) DEFAULT NULL,       \
`RESERVED1` int(11) DEFAULT NULL,           \
`RESERVED2` int(11) DEFAULT NULL,           \
`FPRESSURE` float DEFAULT NULL,             \
`FAVGVOLTAGE` float DEFAULT NULL,           \
`FAVGCURRENT` float DEFAULT NULL,           \
`FREALDATE` float DEFAULT NULL,             \
`FREALCLOCK` float DEFAULT NULL,            \
`FDEBUG1` float DEFAULT NULL,               \
`FDEBUG2` float DEFAULT NULL,               \
`FTEMPARATURE_MIN` float DEFAULT NULL,      \
`FTEMPARATURE_MAX` float DEFAULT NULL,      \
`FTEMPARATURE_AVG` float DEFAULT NULL,      \
`FTEMPARATURE2_MIN` float DEFAULT NULL,     \
`FTEMPARATURE2_MAX` float DEFAULT NULL,     \
`FTEMPARATURE2_AVG` float DEFAULT NULL,     \
`FTEMPARATURE3_MIN` float DEFAULT NULL,     \
`FTEMPARATURE3_MAX` float DEFAULT NULL,     \
`FTEMPARATURE3_AVG` float DEFAULT NULL,     \
`FSTEP_CC_TIME_DAY` float DEFAULT NULL,     \
`FSTEP_CC_TIME` float DEFAULT NULL,         \
`FSTEP_CV_TIME_DAY` float DEFAULT NULL,     \
`FSTEP_CV_TIME` float DEFAULT NULL,         \
`FCHARGE_CC_AMPAREHOUR` float DEFAULT NULL, \
`FCHARGE_CV_AMPAREHOUR` float DEFAULT NULL, \
`FDISCHARGE_CC_AMPAREHOUR` float DEFAULT NULL,\
`FDISCHARGE_CV_AMPAREHOUR` float DEFAULT NULL,\
`FSTARTOCV` float DEFAULT NULL,               \
`STAT` varchar(1) DEFAULT NULL,               \
`DTREG` datetime DEFAULT NULL,                \
PRIMARY KEY (`NOSTEP`),                       \
KEY `IDX_1` (`DEVICEID`,`BLOCKID`,`LOTID`,`CELLID`,`CHANNEL`,`NMWORK`) \
) ENGINE=InnoDB DEFAULT CHARSET=utf8;"));

	 return m_MySql.SetQuery(sql1+sql2+sql3);
}
*/

/*
BOOL PneApp::InsertWorkinfo(CString deviceid,CString blockid,CString lotid,CString cellid,
							CString channel, CString moduleid,CString channelidx,
							CString nmmodule,CString nowdt,CString nmwork,
							CString worker,CString ip,CString schednm,CString schedfile)
{
	if(m_MySql.m_bCon==FALSE) return FALSE;

	if(deviceid.IsEmpty() || deviceid==_T("")) return TRUE;
    if(blockid.IsEmpty() || blockid==_T(""))   return TRUE;
	//lot mapping 상관없이 업데이트함

	CString nowork=m_MySql.GetData6("WORKINFO","DEVICEID",deviceid,"BLOCKID",blockid,
								    "LOTID",lotid,"CELLID",cellid,
								    "CHANNEL",channel,"NMWORK",nmwork,
                                    "NOWORK");
	if(nowork!="") return TRUE;

	//binary data insert////////////////////////////		
	CString schData=_T("");
	CString schSize=_T("0");
	if(GFile::file_Finder(schedfile)==TRUE)
	{		
		schSize.Format(_T("%d"),GFile::file_SizeA(schedfile));

		#ifdef _UNICODE	
			FILE* fp=_wfopen(schedfile,_T("rb"));
		#else
			FILE *fp = fopen(schedfile,_T("rb"));
		#endif

		int file_size=0;
		char *buf=NULL;
		if(fp!=NULL)
		{
			file_size=_ttoi(schSize);
			buf=new char[file_size];

			fread(buf, sizeof(char), file_size, fp); 
			fclose(fp); 

			if(file_size>0)
			{
				for(int i=0;i<file_size;i++)
				{
					CString temp;
					temp.Format(_T("%02X"),(BYTE)buf[i]);
					schData=schData+temp;
				}
			}
			delete buf;
		}
	}
////////////////////////////////////////////////
CString sql=_T("");	
sql.Format(_T("INSERT INTO WORKINFO \
(DEVICEID,BLOCKID,LOTID,CELLID,CHANNEL,MODULEID,CHANNELIDX,NMMODULE,DTSTART,NMWORK,WORKER,IP, \
SCHEDNM,SCHEDSIZE,SCHEDFILE,STAT,DTREG) \
VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s', \
'%s','%s','%s','1',NOW())"),
deviceid,blockid,lotid,cellid,channel,moduleid,channelidx,nmmodule,nowdt,nmwork,worker,ip,
schednm,schSize,schData);

	GLog::log_Save("mysql","sql",sql,NULL,TRUE);
	return m_MySql.SetQuery(sql);
}
*/

/*
BOOL PneApp::InsertWorkevent(CString deviceid,CString blockid,CString lotid,CString cellid,
							 CString channel, CString moduleid,CString channelidx,
							 CString sampleno,CString nmwork,CString tpstate)
{		
	if(m_MySql.m_bCon==FALSE) return FALSE;

	if(deviceid.IsEmpty() || deviceid==_T("")) return TRUE;
    if(blockid.IsEmpty() || blockid==_T(""))   return TRUE;
	if(lotid.IsEmpty() || lotid==_T(""))       return TRUE;
	if(cellid.IsEmpty() || cellid==_T(""))     return TRUE;

////////////////////////////////////////////////		
CString sql=_T("");
sql.Format(_T("INSERT INTO WORKEVENT \
(DEVICEID,BLOCKID,LOTID,CELLID,CHANNEL,MODULEID,CHANNELIDX, \
SAMPLENO,NMWORK,TPSTATE,STAT,DTREG) \
VALUES ('%s','%s','%s','%s','%s','%s','%s', \
'%s','%s','%s','1',NOW())"),
deviceid,blockid,lotid,cellid,channel,moduleid,channelidx,
sampleno,nmwork,tpstate);

	GLog::log_Save("mysql","sql",sql,NULL,TRUE);
	return m_MySql.SetQuery(sql);
}
*/

CString PneApp::Time_Conversion(CString str_Time)
{
	CString Mon[7], str_Mon_Time;	

	str_Time.Replace(" ","-");
	str_Time.Replace(":","-");

	for(int i = 0; i < 7; i++)
	{
		AfxExtractSubString(Mon[i], str_Time, i, '-');
		Mon[i].Trim();
	}

	str_Time = "";
	str_Time += Mon[0];
	str_Time += Mon[1];
	str_Time += Mon[2];
	str_Time += Mon[3];
	str_Time += Mon[4];
	str_Time += Mon[5];
	str_Time += Mon[6];

	return str_Time;
}

/*
BOOL PneApp::CreateTbMonitor()
{//Src Table Create		
	CString table=_T("monitorinfo");
	if(m_MySql.ChkTable(table)==FALSE)
	{	
	   CString sql;
	   sql.Format(_T("CREATE TABLE `%s` (                         \
					  `NOMONITOR` int(5) NOT NULL AUTO_INCREMENT, \
					  `NMWORK` varchar(100) DEFAULT NULL,         \
					  `TESTSERIAL` varchar(20) DEFAULT NULL,      \
					  `MODULEID` varchar(30) DEFAULT NULL,        \
					  `NMMODULE` varchar(20) DEFAULT NULL,        \
					  `DEVICEID` varchar(10) DEFAULT NULL,        \
					  `BLOCKID` varchar(10) DEFAULT NULL,         \
					  `NMSCHED` varchar(20) DEFAULT NULL,         \
					  `CHANNEL` varchar(10) DEFAULT NULL,         \
					  `CHNO` varchar(10) DEFAULT NULL,            \
					  `CHSTEPNO` varchar(3) DEFAULT NULL,         \
					  `CHSTATE` varchar(10) DEFAULT NULL,         \
					  `EQSTATE` varchar(3) DEFAULT NULL,          \
					  `EQSTR` varchar(30) DEFAULT NULL,           \
					  `CHSTEPTYPE` varchar(3) DEFAULT NULL,       \
					  `CHCODE` varchar(3) DEFAULT NULL,           \
					  `CHGRADECODE` varchar(3) DEFAULT NULL,      \
					  `NCURRENTCYCLENUM` varchar(20) DEFAULT NULL,\
					  `NTOTALCYCLENUM` varchar(20) DEFAULT NULL,  \
					  `FVOLTAGE` varchar(20) DEFAULT NULL,        \
					  `FCURRENT` varchar(20) DEFAULT NULL,        \
					  `FCAPACITY` varchar(20) DEFAULT NULL,       \
					  `FDISCAPACITY` varchar(20) DEFAULT NULL,    \
					  `FCAPACITYSUM` varchar(20) DEFAULT NULL,    \
					  `FWATT` varchar(20) DEFAULT NULL,"),table);

		sql+=_T("     `FWATTHOUR` varchar(20) DEFAULT NULL,       \
					  `FDISWATTHOUR` varchar(20) DEFAULT NULL,    \
					  `FWATTHOURSUM` varchar(20) DEFAULT NULL,    \
					  `FAVGVOLTAGE` varchar(20) DEFAULT NULL,     \
					  `FAVGCURRENT` varchar(20) DEFAULT NULL,     \
					  `FSTEPTIME` varchar(20) DEFAULT NULL,       \
					  `FTOTALTIME` varchar(20) DEFAULT NULL,      \
					  `FDAYTIME` varchar(20) DEFAULT NULL,        \
					  `FDAYTOTTIME` varchar(20) DEFAULT NULL,     \
					  `FIMPEDANCE` varchar(20) DEFAULT NULL,      \
					  `FTEMPARATURE` varchar(20) DEFAULT NULL,    \
					  `FTEMPARATURE2` varchar(20) DEFAULT NULL,   \
					  `FTEMPARATURE3` varchar(20) DEFAULT NULL,   \
					  `FOVENTEMPARATURE` varchar(20) DEFAULT NULL,\
					  `FPRESSURE` varchar(20) DEFAULT NULL,       \
					  `FMETER` varchar(20) DEFAULT NULL,          \
					  `FSHARINGINFO` varchar(20) DEFAULT NULL,    \
					  `FCHAMBERUSING` varchar(20) DEFAULT NULL,   \
					  `FREALDATE` varchar(20) DEFAULT NULL,       \
					  `FREALCLOCK` varchar(20) DEFAULT NULL,      \
					  `FDEBUG1` varchar(20) DEFAULT NULL,         \
					  `FDEBUG2` varchar(20) DEFAULT NULL,         \
					  `STARTTIME` varchar(20) DEFAULT NULL,       \
					  `ENDTIME` varchar(20) DEFAULT NULL,         \
					  `GOTOCYCLECOUNT` varchar(3) DEFAULT NULL,   \
					  `DTREG` datetime DEFAULT NULL,              \
					  PRIMARY KEY (`NOMONITOR`)                   \
					) ENGINE=MyISAM DEFAULT CHARSET=euckr;");
	   
	   return m_MySql.SetQuery(sql);
	}
	return TRUE;
}

BOOL PneApp::CreateTbRaw(CString table)
{//Raw Table Create		
	if(m_MySql.ChkTable(table)==FALSE)
	{	
	   CString sql;
	   sql.Format(_T("CREATE TABLE `%s` (                      \
				  `NOIDX` int(11) NOT NULL,                    \
				  `CHNO` varchar(3) DEFAULT NULL,              \
				  `CHSTEPNO` varchar(3) DEFAULT NULL,          \
				  `CHSTATE` varchar(3) DEFAULT NULL,           \
				  `CHSTEPTYPE` varchar(3) DEFAULT NULL,        \
				  `CHDATASELECT` varchar(3) DEFAULT NULL,      \
				  `CHCODE` varchar(3) DEFAULT NULL,            \
				  `CHGRADECODE` varchar(3) DEFAULT NULL,       \
				  `CHRESERVED` varchar(3) DEFAULT NULL,        \
				  `NINDEXFROM` varchar(20) DEFAULT NULL,       \
				  `NINDEXTO` varchar(20) DEFAULT NULL,         \
				  `NCURRENTCYCLENUM` varchar(20) DEFAULT NULL, \
				  `NTOTALCYCLENUM` int(11) DEFAULT NULL,       \
				  `ISAVESEQUENCE` varchar(20) DEFAULT NULL,    \
				  `IRESERVED` varchar(20) DEFAULT NULL,        \
				  `FVOLTAGE` varchar(20) DEFAULT NULL,         \
				  `FCURRENT` varchar(20) DEFAULT NULL,         \
				  `FCAPACITY` varchar(20) DEFAULT NULL,        \
				  `FDISCAPACITY` varchar(20) DEFAULT NULL,     \
				  `FWATT` varchar(20) DEFAULT NULL,            \
				  `FWATTHOUR` varchar(20) DEFAULT NULL,        \
				  `FDISWATTHOUR` varchar(20) DEFAULT NULL,     \
				  `FSTEPTIME` varchar(20) DEFAULT NULL,        \
				  `FTOTALTIME` varchar(20) DEFAULT NULL,       \
				  `FIMPEDANCE` varchar(20) DEFAULT NULL,       \
				  `FTEMPARATURE` varchar(20) DEFAULT NULL,     \
				  `FTEMPARATURE2` varchar(20) DEFAULT NULL,    \
				  `FTEMPARATURE3` varchar(20) DEFAULT NULL,"),table);

	   sql+=_T("  `FOVENTEMPARATURE` varchar(20) DEFAULT NULL, \
				  `CHUSINGCHAMPER` varchar(3) DEFAULT NULL,    \
				  `CRECORDTIMENO` varchar(3) DEFAULT NULL,     \
				  `RESERVED1` varchar(3) DEFAULT NULL,         \
				  `RESERVED2` varchar(3) DEFAULT NULL,         \
				  `FPRESSURE` varchar(20) DEFAULT NULL,        \
				  `FAVGVOLTAGE` varchar(20) DEFAULT NULL,      \
				  `FAVGCURRENT` varchar(20) DEFAULT NULL,      \
				  `NGOTOCYCLENUM` varchar(20) DEFAULT NULL,    \
				  `FRESERVED1` varchar(50) DEFAULT NULL,       \
				  `FRESERVED2` varchar(50) DEFAULT NULL,       \
				  `FRESERVED3` varchar(50) DEFAULT NULL,       \
				  `DTREG` datetime DEFAULT NULL,               \
				  PRIMARY KEY (`NOIDX`)                        \
				) ENGINE=MyISAM DEFAULT CHARSET=euckr");

	   return m_MySql.SetQuery(sql);
	}
	return TRUE;
}

BOOL PneApp::CreateTbSrc(CString table)
{//Src Table Create		
	if(m_MySql.ChkTable(table)==FALSE)
	{	
	   CString sql;
	   sql.Format(_T("CREATE TABLE `%s` (                      \
				  `NOSRC` int(11) NOT NULL,                    \
				  `SZTIME` varchar(20) DEFAULT NULL,           \
				  `TOTALCYCLE` varchar(10) DEFAULT NULL,       \
				  `NSTEPNO` varchar(20) DEFAULT NULL,          \
				  `NTYPE` varchar(20) DEFAULT NULL,            \
				  `SZTYPE` varchar(20) DEFAULT NULL,           \
				  `FVOLTAGE` varchar(20) DEFAULT NULL,         \
				  `FCURRENT` varchar(20) DEFAULT NULL,         \
				  `FCAPACITY` varchar(20) DEFAULT NULL,        \
				  `FDISCAPACITY` varchar(20) DEFAULT NULL,     \
				  `FWATT` varchar(20) DEFAULT NULL,            \
				  `FWATTHOUR` varchar(20) DEFAULT NULL,        \
				  `FDISWATTHOUR` varchar(20) DEFAULT NULL,     \
				  `FSTEPTIME` varchar(20) DEFAULT NULL,        \
				  `FTOTALTIME` varchar(20) DEFAULT NULL,       \
				  `FIMPEDANCE` varchar(20) DEFAULT NULL,       \
				  `FTEMPARATURE` varchar(20) DEFAULT NULL,     \
				  `FTEMPARATURE2` varchar(20) DEFAULT NULL,    \
				  `FTEMPARATURE3` varchar(20) DEFAULT NULL,    \
				  `FOVENTEMPARATURE` varchar(20) DEFAULT NULL, \
				  `CHUSINGCHAMPER` varchar(3) DEFAULT NULL,    \
				  `CRECORDTIMENO` varchar(3) DEFAULT NULL,     \
				  `RESERVED1` varchar(3) DEFAULT NULL,         \
				  `RESERVED2` varchar(3) DEFAULT NULL,         \
				  `FPRESSURE` varchar(20) DEFAULT NULL,        \
				  `FAVGVOLTAGE` varchar(20) DEFAULT NULL,      \
				  `FAVGCURRENT` varchar(20) DEFAULT NULL,      \
				  `NGOTOCYCLENUM` varchar(20) DEFAULT NULL,    \
				  `FRESERVED1` varchar(50) DEFAULT NULL,       \
				  `FRESERVED2` varchar(50) DEFAULT NULL,       \
				  `FRESERVED3` varchar(50) DEFAULT NULL,       \
				  `METER1` varchar(20) DEFAULT NULL,           \
				  `DTREG` datetime DEFAULT NULL,               \
				  PRIMARY KEY (`NOSRC`)                        \
				) ENGINE=MyISAM DEFAULT CHARSET=euckr"),table);

	   return m_MySql.SetQuery(sql);
	}
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
BOOL PneApp::InsertWorkinfo(PS_TEST_RESULT_FILE_HEADER *pHeader,CString ip,
							CString nmdir,CString fileTitle,CString nmwork,CString nmnode,
							CString deviceid,CString blockid,
							CString channelid,CString &tbraw,CString &tbsrc)
{
	if(!pHeader) return FALSE;

	CString nowork;//5자리
	nowork.Format(_T("%05d"),mainPneApp.m_MySql.GetMax(_T("WORKINFO"),_T("NOWORK"))+1);
	
	//binary data insert////////////////////////////

	CString schPath;
	schPath.Format(_T("%s\\%s.sch"),nmdir,fileTitle);	
	schPath.Replace(_T("\\"),_T("\\\\"));
	
	CString schData=_T("");
	CString schSize=_T("0");
	if(GFile::file_Finder(schPath)==TRUE)
	{
		//schData.Format(_T("load_file('%s')"),schPath);
		schSize.Format(_T("%d"),GFile::file_SizeA(schPath));

		#ifdef _UNICODE	
			FILE* fp=_wfopen(schPath,_T("rb"));
		#else
			FILE *fp = fopen(schPath,_T("rb"));
		#endif

		int file_size=0;
		char *buf=NULL;
		if(fp!=NULL)
		{
			file_size=_ttoi(schSize);
			buf=new char[file_size];

			fread(buf, sizeof(char), file_size, fp); 
			fclose(fp); 

			if(file_size>0)
			{
				for(int i=0;i<file_size;i++)
				{
					CString temp;
					temp.Format(_T("%02X"),(BYTE)buf[i]);
					schData=schData+temp;
				}
			}
			delete buf;
		}
	}
	////////////////////////////////////////////////
	
	CString nfileid=GStr::str_IntToStr(pHeader->fileHeader.nFileID);
	CString nfileversion=GStr::str_IntToStr(pHeader->fileHeader.nFileVersion);

	CString szReserved=(CString)pHeader->fileHeader.szReserved;//deviceid,blockid
	CString szcreatedatetime=(CString)pHeader->fileHeader.szCreateDateTime;
	CString szdescription=(CString)pHeader->fileHeader.szDescrition;
	CString szstarttime=(CString)pHeader->testHeader.szStartTime;
	CString szendtime=(CString)pHeader->testHeader.szEndTime;
	CString szserial=(CString)pHeader->testHeader.szSerial;
	CString szuserid=(CString)pHeader->testHeader.szUserID;
	CString sztrayno=(CString)pHeader->testHeader.szTrayNo;
	CString szbuff=(CString)pHeader->testHeader.szBuff;	

	szcreatedatetime=GetTime(szcreatedatetime);
	szstarttime=GetTime(szstarttime);
	szendtime=GetTime(szendtime);
	
	CString nrecordsize;
	nrecordsize.Format(_T("%d"),pHeader->testHeader.wRecordItem);
			
	SYSTEMTIME now;
	GetLocalTime(&now);

	tbraw=m_MySql.GetData3(_T("WORKINFO"),
		                _T("NMWORK"),nmwork,
		                _T("NMNODE"),nmnode,
						_T("BLOCKID"),blockid,
						_T("TBRAW"));
	tbsrc=m_MySql.GetData3(_T("WORKINFO"),
		                _T("NMWORK"),nmwork,
		                _T("NMNODE"),nmnode,
						_T("BLOCKID"),blockid,
						_T("TBSRC"));
	if(!tbraw.IsEmpty() && tbraw!=_T("")) return TRUE;//동일한테이블명
	if(!tbsrc.IsEmpty() && tbsrc!=_T("")) return TRUE;//동일한테이블명

	//설비코드(10)_블록ID_번호_년월일_시분초
	tbraw.Format(_T("%s_%s_%s_%04d%02d%02d_%02d%02d%02d_RAW"),deviceid,blockid,nowork,
		           now.wYear,now.wMonth,now.wDay,now.wHour,now.wMinute,now.wSecond);

	tbsrc.Format(_T("%s_%s_%s_%04d%02d%02d_%02d%02d%02d_SRC"),deviceid,blockid,nowork,
		           now.wYear,now.wMonth,now.wDay,now.wHour,now.wMinute,now.wSecond);
	
	if(m_MySql.GetCount1(_T("WORKINFO"),_T("TBRAW"),tbraw)<0) return TRUE;//존재하는 테이블
	if(m_MySql.GetCount1(_T("WORKINFO"),_T("TBSRC"),tbsrc)<0) return TRUE;//존재하는 테이블
	
	nmdir.Replace(_T("\\"),_T("/"));
	
	CString sql=_T("");
	sql.Format(_T("INSERT INTO WORKINFO \
				(NOWORK,NMDIR,NMWORK,NMNODE,DEVICEID,BLOCKID,CHANNEL,NFILEID,NFILEVERSION,\
				    SZCREATEDATETIME,SZDESCRIPTION,SZSTARTTIME,SZENDTIME,SZSERIAL,SZUSERID,\
					SZTRAYNO,SZBUFF,NRECORDSIZE,IP,TBRAW,TBSRC,SCHEDSIZE,SCHEDFILE,DTREG) \
			    VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s', \
				    '%s','%s','%s','%s','%s','%s', \
				    '%s','%s','%s','%s','%s','%s','%s','%s',NOW())"),
					nowork,nmdir,nmwork,nmnode,deviceid,blockid,channelid,nfileid,nfileversion,
					szcreatedatetime,szdescription,szstarttime,szendtime,szserial,szuserid,
					sztrayno,szbuff,nrecordsize,ip,tbraw,tbsrc,schSize,schData);

	BOOL bflag=m_MySql.SetQuery(sql);
	if(bflag==FALSE) return FALSE;
	
	if(bflag==TRUE) bflag=CreateTbRaw(tbraw);
	if(bflag==TRUE) bflag=CreateTbSrc(tbsrc);

	return bflag;
}


BOOL PneApp::InsertRaw(PS_STEP_END_RECORD *pRecord,LONGLONG noidx,CString tbraw)
{
	if(!pRecord) return FALSE;
	
	CString snoIdx;
	CString schNo;	
	CString schStepNo;
	CString schState;		
	CString schStepType;	
	CString schDataSelect;	
	CString schCode;
	CString schGradeCode;
	CString schReserved;

	CString snIndexFrom;
	CString snIndexTo;
	CString snCurrentCycleNum;
	CString snTotalCycleNum;
	CString slSaveSequence;	
	CString slReserved;

	CString sfVoltage;
	CString sfCurrent;
	CString sfCapacity;
	CString sfWatt;
	CString sfWattHour;
	CString sfStepTime;
	CString sfTotalTime;
	CString sfImpedance;		
	CString sfTemparature;
	CString sfTemparature2;		
	CString sfTemparature3;		
	CString sfOvenTemparature;	
	CString schUsingChamber;	
	CString scRecordTimeNo;	
	CString sReserved1;
	CString sReserved2;
	CString sfPressure;
	CString sfAvgVoltage;
	CString sfAvgCurrent;
	CString snGotoCycleNum;	
	CString sfReserved1;
	CString sfReserved2;
	CString sfReserved3;

	snoIdx.Format(_T("%d"),noidx);	
	schNo.Format(_T("%d"),pRecord->chNo);	
	schStepNo.Format(_T("%d"),pRecord->chStepNo);
	schState.Format(_T("%d"),pRecord->chState);		
	schStepType.Format(_T("%d"),pRecord->chStepType);	
	schDataSelect.Format(_T("%d"),pRecord->chDataSelect);	
	schCode.Format(_T("%d"),pRecord->chCode);
	schGradeCode.Format(_T("%d"),pRecord->chGradeCode);
	schReserved.Format(_T("%d"),pRecord->chReserved);

	snIndexFrom.Format(_T("%d"),pRecord->nIndexFrom);
	snIndexTo.Format(_T("%d"),pRecord->nIndexTo);
	snCurrentCycleNum.Format(_T("%d"),pRecord->nCurrentCycleNum);
	snTotalCycleNum.Format(_T("%d"),pRecord->nTotalCycleNum);
	slSaveSequence.Format(_T("%d"),pRecord->lSaveSequence);
	slReserved.Format(_T("%d"),pRecord->lReserved);	

	sfVoltage.Format(_T("%f"),pRecord->fVoltage);				
	sfCurrent.Format(_T("%f"),pRecord->fCurrent);	
	sfCapacity.Format(_T("%f"),pRecord->fCharge_AmpareHour);	
	sfWatt.Format(_T("%f"),pRecord->fWatt);	
	sfWattHour.Format(_T("%f"),pRecord->fCharge_WattHour);	
	sfStepTime.Format(_T("%f"),pRecord->fStepTime);	
	sfTotalTime.Format(_T("%f"),pRecord->fTotalTime);	
	sfImpedance.Format(_T("%f"),pRecord->fImpedance);	
	sfTemparature.Format(_T("%f"),pRecord->fTemparature);	
	sfTemparature2.Format(_T("%f"),pRecord->fTemparature2);	
	sfTemparature3.Format(_T("%f"),pRecord->fTemparature3);	
	sfOvenTemparature.Format(_T("%f"),pRecord->fOvenTemparature);	

	schUsingChamber.Format(_T("%d"),pRecord->chUsingChamber);
	scRecordTimeNo.Format(_T("%d"),pRecord->cRecordTimeNo);		
	sReserved1.Format(_T("%d"),pRecord->Reserved[0]);		
	sReserved2.Format(_T("%d"),pRecord->Reserved[1]);		
	
	sfPressure.Format(_T("%f"),pRecord->fPressure);	
	sfAvgVoltage.Format(_T("%f"),pRecord->fAvgVoltage);	
	sfAvgCurrent.Format(_T("%f"),pRecord->fAvgCurrent);	

	snGotoCycleNum.Format(_T("%d"),pRecord->nGotoCycleNum);	
//	sfReserved1.Format(_T("%f"),pRecord->fReserved[0]);	
//	sfReserved2.Format(_T("%f"),pRecord->fReserved[1]);	
//	sfReserved3.Format(_T("%f"),pRecord->fReserved[2]);	

	//lSaveSequence :비교하여 데이타 존재여부 비교
	if(m_MySql.GetCount1(tbraw,_T("ISAVESEQUENCE"),slSaveSequence)>0) return TRUE;
		
	CString sql;
	sql.Format(_T("INSERT INTO %s \
					(NOIDX,CHNO,CHSTEPNO,CHSTATE,CHSTEPTYPE,CHDATASELECT,CHCODE,CHGRADECODE,CHRESERVED,  \
					 NINDEXFROM,NINDEXTO,NCURRENTCYCLENUM,NTOTALCYCLENUM,ISAVESEQUENCE,IRESERVED,        \
					 FVOLTAGE,FCURRENT,FCAPACITY,FWATT,FWATTHOUR,FSTEPTIME,FTOTALTIME,FIMPEDANCE,        \
					 FTEMPARATURE,FTEMPARATURE2,FTEMPARATURE3,FOVENTEMPARATURE,CHUSINGCHAMPER,           \
					 CRECORDTIMENO,RESERVED1,RESERVED2,FPRESSURE,FAVGVOLTAGE,FAVGCURRENT,NGOTOCYCLENUM,  \
					 FRESERVED1,FRESERVED2,FRESERVED3,DTREG) \
					VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s',\
					'%s','%s','%s','%s','%s','%s',\
					'%s','%s','%s','%s','%s','%s','%s','%s',\
					'%s','%s','%s','%s','%s',\
					'%s','%s','%s','%s','%s','%s','%s',\
					'%s','%s','%s',NOW())"),tbraw,
					snoIdx,schNo,schStepNo,schState,schStepType,schDataSelect,schCode,schGradeCode,schReserved,
					snIndexFrom,snIndexTo,snCurrentCycleNum,snTotalCycleNum,slSaveSequence,slReserved,
					sfVoltage,sfCurrent,sfCapacity,sfWatt,sfWattHour,sfStepTime,sfTotalTime,sfImpedance,
					sfTemparature,sfTemparature2,sfTemparature3,sfOvenTemparature,schUsingChamber,
					scRecordTimeNo,sReserved1,sReserved2,sfPressure,sfAvgVoltage,sfAvgCurrent,snGotoCycleNum,	
					sfReserved1,sfReserved2,sfReserved3);
	
	return m_MySql.SetQuery(sql);
}


BOOL PneApp::InsertRaw2(PS_STEP_END_RECORD2 *pRecord,LONGLONG noidx,CString tbraw)
{
	if(!pRecord) return FALSE;
	
	CString snoIdx;
	CString schNo;	
	CString schStepNo;
	CString schState;		
	CString schStepType;	
	CString schDataSelect;	
	CString schCode;
	CString schGradeCode;
	CString schReserved;

	CString snIndexFrom;
	CString snIndexTo;
	CString snCurrentCycleNum;
	CString snTotalCycleNum;
	CString slSaveSequence;	
	CString slReserved;

	CString sfVoltage;
	CString sfCurrent;
	CString sfCapacity;
	CString sfDisCapacity;
	CString sfWatt;
	CString sfWattHour;
	CString sfDisWattHour;
	CString sfStepTime;
	CString sfTotalTime;
	CString sfImpedance;		
	CString sfTemparature;
	CString sfTemparature2;		
	CString sfTemparature3;		
	CString sfOvenTemparature;	
	CString schUsingChamber;	
	CString scRecordTimeNo;	
	CString sReserved1;
	CString sReserved2;
	CString sfPressure;
	CString sfAvgVoltage;
	CString sfAvgCurrent;
	CString snGotoCycleNum;	
	CString sfReserved1;
	CString sfReserved2;
	CString sfReserved3;

	snoIdx.Format(_T("%d"),noidx);	
	schNo.Format(_T("%d"),pRecord->chNo);	
	schStepNo.Format(_T("%d"),pRecord->chStepNo);
	schState.Format(_T("%d"),pRecord->chState);		
	schStepType.Format(_T("%d"),pRecord->chStepType);	
	schDataSelect.Format(_T("%d"),pRecord->chDataSelect);	
	schCode.Format(_T("%d"),pRecord->chCode);
	schGradeCode.Format(_T("%d"),pRecord->chGradeCode);
	schReserved.Format(_T("%d"),pRecord->chReserved);

	snIndexFrom.Format(_T("%d"),pRecord->nIndexFrom);
	snIndexTo.Format(_T("%d"),pRecord->nIndexTo);
	snCurrentCycleNum.Format(_T("%d"),pRecord->nCurrentCycleNum);
	snTotalCycleNum.Format(_T("%d"),pRecord->nTotalCycleNum);
	slSaveSequence.Format(_T("%d"),pRecord->lSaveSequence);
	slReserved.Format(_T("%d"),pRecord->lReserved);	

	sfVoltage.Format(_T("%f"),pRecord->fVoltage);				
	sfCurrent.Format(_T("%f"),pRecord->fCurrent);	
	sfCapacity.Format(_T("%f"),pRecord->fCharge_AmpareHour);
	sfDisCapacity.Format(_T("%f"),pRecord->fDisCharge_AmpareHour);
	sfWatt.Format(_T("%f"),pRecord->fWatt);	
	sfWattHour.Format(_T("%f"),pRecord->fCharge_WattHour);	
	sfDisWattHour.Format(_T("%f"),pRecord->fDisCharge_WattHour);	
	sfStepTime.Format(_T("%f"),pRecord->fStepTime);	
	sfTotalTime.Format(_T("%f"),pRecord->fTotalTime);	
	sfImpedance.Format(_T("%f"),pRecord->fImpedance);	
	sfTemparature.Format(_T("%f"),pRecord->fTemparature);	
	sfTemparature2.Format(_T("%f"),pRecord->fTemparature2);	
	sfTemparature3.Format(_T("%f"),pRecord->fTemparature3);	
	sfOvenTemparature.Format(_T("%f"),pRecord->fOvenTemparature);	

	schUsingChamber.Format(_T("%d"),pRecord->chUsingChamber);
	scRecordTimeNo.Format(_T("%d"),pRecord->cRecordTimeNo);		
	sReserved1.Format(_T("%d"),pRecord->Reserved[0]);		
	sReserved2.Format(_T("%d"),pRecord->Reserved[1]);		
	
	sfPressure.Format(_T("%f"),pRecord->fPressure);	
	sfAvgVoltage.Format(_T("%f"),pRecord->fAvgVoltage);	
	sfAvgCurrent.Format(_T("%f"),pRecord->fAvgCurrent);	

	snGotoCycleNum.Format(_T("%d"),pRecord->nGotoCycleNum);	
	sfReserved1.Format(_T("%f"),pRecord->fReserved[0]);	
	sfReserved2.Format(_T("%f"),pRecord->fReserved[1]);	
	sfReserved3.Format(_T("%f"),pRecord->fReserved[2]);	

	//lSaveSequence :비교하여 데이타 존재여부 비교
	if(m_MySql.GetCount1(tbraw,_T("ISAVESEQUENCE"),slSaveSequence)>0) return TRUE;
		
	CString sql;
	sql.Format(_T("INSERT INTO %s \
					(NOIDX,CHNO,CHSTEPNO,CHSTATE,CHSTEPTYPE,CHDATASELECT,CHCODE,CHGRADECODE,CHRESERVED,  \
					 NINDEXFROM,NINDEXTO,NCURRENTCYCLENUM,NTOTALCYCLENUM,ISAVESEQUENCE,IRESERVED,        \
					 FVOLTAGE,FCURRENT,FCAPACITY,FDISCAPACITY,                                           \
					 FWATT,FWATTHOUR,FDISWATTHOUR,FSTEPTIME,FTOTALTIME,FIMPEDANCE,                       \
					 FTEMPARATURE,FTEMPARATURE2,FTEMPARATURE3,FOVENTEMPARATURE,CHUSINGCHAMPER,           \
					 CRECORDTIMENO,RESERVED1,RESERVED2,FPRESSURE,FAVGVOLTAGE,FAVGCURRENT,NGOTOCYCLENUM,  \
					 FRESERVED1,FRESERVED2,FRESERVED3,DTREG) \
					VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s',\
					'%s','%s','%s','%s','%s','%s',\
					'%s','%s','%s','%s',\
					'%s','%s','%s','%s','%s','%s',\
					'%s','%s','%s','%s','%s',\
					'%s','%s','%s','%s','%s','%s','%s',\
					'%s','%s','%s',NOW())"),tbraw,
					snoIdx,schNo,schStepNo,schState,schStepType,schDataSelect,schCode,schGradeCode,schReserved,
					snIndexFrom,snIndexTo,snCurrentCycleNum,snTotalCycleNum,slSaveSequence,slReserved,
					sfVoltage,sfCurrent,sfCapacity,sfDisCapacity,
					sfWatt,sfWattHour,sfDisWattHour,sfStepTime,sfTotalTime,sfImpedance,
					sfTemparature,sfTemparature2,sfTemparature3,sfOvenTemparature,schUsingChamber,
					scRecordTimeNo,sReserved1,sReserved2,sfPressure,sfAvgVoltage,sfAvgCurrent,snGotoCycleNum,	
					sfReserved1,sfReserved2,sfReserved3);
	
	return m_MySql.SetQuery(sql);
}

BOOL PneApp::InsertSrc(PS_STEP_SRC_RECORD *pRecord,CString tbsrc)
{
	if(!pRecord) return FALSE;
			
    CString sISaveSequence;
	CString sTotCycle;
	CString snStepNo;
	CString snType;

	CString szTime;
	CString szType;

	CString sfVoltage;				
	CString sfCurrent;
	CString sfCapacity;
	CString sfWatt;	
	CString sfWattHour;
	CString sfStepTime;			
	CString sfTotalTime;
	CString sfImpedance;		
	CString sfTemparature;
	CString sfTemparature2;		
	CString sfTemparature3;		
	CString sfOvenTemparature;	

	CString	sfAvgVoltage;
	CString	sfAvgCurrent;
	CString	sfMeter1;
		
	sISaveSequence.Format(_T("%d"),pRecord->ISaveSequence);			
	sTotCycle.Format(_T("%d"),pRecord->TotCycle);	
	snStepNo.Format(_T("%d"),pRecord->nStepNo);	
	snType.Format(_T("%d"),pRecord->nType);	
		
	szTime.Format(_T("%s"),pRecord->szTime);	
	szType.Format(_T("%s"),pRecord->szType);	
			
	sfVoltage=pRecord->sfVoltage;
	sfCurrent=pRecord->sfCurrent;
	sfCapacity=pRecord->sfCapacity;
	sfWatt=pRecord->sfWatt;
	sfWattHour=pRecord->sfWattHour;
	sfStepTime=pRecord->sfStepTime;
	sfTotalTime=pRecord->sfTotalTime;
	sfImpedance=pRecord->sfImpedance;
	sfAvgVoltage=pRecord->sfAvgVoltage;
	sfAvgCurrent=pRecord->sfAvgCurrent;
	sfMeter1=pRecord->sfMeter1;
	sfTemparature=pRecord->sfTemparature;
	sfTemparature2=pRecord->sfTemparature2;
	sfTemparature3=pRecord->sfTemparature3;
	sfOvenTemparature=pRecord->sfOvenTemparature;

	//lSaveSequence :비교하여 데이타 존재여부 비교
	if(m_MySql.GetCount1(tbsrc,_T("NOSRC"),sISaveSequence)>0) return TRUE;
		
	CString sql;
	sql.Format(_T("INSERT INTO %s \
					(NOSRC,SZTIME,TOTALCYCLE,NSTEPNO,NTYPE,SZTYPE,              \
					 FVOLTAGE,FCURRENT,FCAPACITY,FWATT,FWATTHOUR,FSTEPTIME,     \
					 FTOTALTIME,FIMPEDANCE,FAVGVOLTAGE,FAVGCURRENT,METER1,      \
					 FTEMPARATURE,FTEMPARATURE2,FTEMPARATURE3,FOVENTEMPARATURE, \
					 DTREG) \
					VALUES ('%s','%s','%s','%s','%s','%s',\
					'%s','%s','%s','%s','%s','%s',\
					'%s','%s','%s','%s','%s',\
					'%s','%s','%s','%s',\
					NOW())"),tbsrc,
					sISaveSequence,szTime,sTotCycle,snStepNo,snType,szType,
					sfVoltage,sfCurrent,sfCapacity,sfWatt,sfWattHour,sfStepTime,
					sfTotalTime,sfImpedance,sfAvgVoltage,sfAvgCurrent,sfMeter1,
					sfTemparature,sfTemparature2,sfTemparature3,sfOvenTemparature);
	
	return m_MySql.SetQuery(sql);	
}


BOOL PneApp::InsertSrc2(PS_STEP_SRC_RECORD2 *pRecord,CString tbsrc)
{
	if(!pRecord) return FALSE;
			
    CString sISaveSequence;
	CString sTotCycle;
	CString snStepNo;
	CString snType;

	CString szTime;
	CString szType;

	CString sfVoltage;				
	CString sfCurrent;
	CString sfCapacity;
	CString sfDisCapacity;
	CString sfWatt;	
	CString sfWattHour;
	CString sfDisWattHour;
	CString sfStepTime;			
	CString sfTotalTime;
	CString sfImpedance;		
	CString sfTemparature;
	CString sfTemparature2;		
	CString sfTemparature3;		
	CString sfOvenTemparature;	

	CString	sfAvgVoltage;
	CString	sfAvgCurrent;
	CString	sfMeter1;
		
	sISaveSequence.Format(_T("%d"),pRecord->ISaveSequence);			
	sTotCycle.Format(_T("%d"),pRecord->TotCycle);	
	snStepNo.Format(_T("%d"),pRecord->nStepNo);	
	snType.Format(_T("%d"),pRecord->nType);	
		
	szTime.Format(_T("%s"),pRecord->szTime);	
	szType.Format(_T("%s"),pRecord->szType);	
			
	sfVoltage=pRecord->sfVoltage;
	sfCurrent=pRecord->sfCurrent;
	sfCapacity=pRecord->sfCapacity;
	sfDisCapacity=pRecord->sfDisCapacity;
	sfWatt=pRecord->sfWatt;
	sfWattHour=pRecord->sfWattHour;
	sfDisWattHour=pRecord->sfDisWattHour;
	sfStepTime=pRecord->sfStepTime;
	sfTotalTime=pRecord->sfTotalTime;
	sfImpedance=pRecord->sfImpedance;
	sfAvgVoltage=pRecord->sfAvgVoltage;
	sfAvgCurrent=pRecord->sfAvgCurrent;
	sfMeter1=pRecord->sfMeter1;
	sfTemparature=pRecord->sfTemparature;
	sfTemparature2=pRecord->sfTemparature2;
	sfTemparature3=pRecord->sfTemparature3;
	sfOvenTemparature=pRecord->sfOvenTemparature;

	//lSaveSequence :비교하여 데이타 존재여부 비교
	if(m_MySql.GetCount1(tbsrc,_T("NOSRC"),sISaveSequence)>0) return TRUE;
		
	CString sql;
	sql.Format(_T("INSERT INTO %s \
					(NOSRC,SZTIME,TOTALCYCLE,NSTEPNO,NTYPE,SZTYPE,              \
					 FVOLTAGE,FCURRENT,FCAPACITY,FDISCAPACITY,                  \
					 FWATT,FWATTHOUR,FDISWATTHOUR,FSTEPTIME,                    \
					 FTOTALTIME,FIMPEDANCE,FAVGVOLTAGE,FAVGCURRENT,METER1,      \
					 FTEMPARATURE,FTEMPARATURE2,FTEMPARATURE3,FOVENTEMPARATURE, \
					 DTREG) \
					VALUES ('%s','%s','%s','%s','%s','%s',\
					'%s','%s','%s','%s',\
					'%s','%s','%s','%s',\
					'%s','%s','%s','%s','%s',\
					'%s','%s','%s','%s',\
					NOW())"),tbsrc,
					sISaveSequence,szTime,sTotCycle,snStepNo,snType,szType,
					sfVoltage,sfCurrent,sfCapacity,sfDisCapacity,
					sfWatt,sfWattHour,sfDisWattHour,sfStepTime,
					sfTotalTime,sfImpedance,sfAvgVoltage,sfAvgCurrent,sfMeter1,
					sfTemparature,sfTemparature2,sfTemparature3,sfOvenTemparature);
	
	return m_MySql.SetQuery(sql);	
}

void PneApp::ListWorkInfo(CListCtrl *list,CString deviceid,CString blockid,CString nmwork,CString sstart,CString send,CString estart,CString eend)
{	
	if(!list) return;
	if(m_MySql.m_bCon==FALSE) return;

	list->DeleteAllItems();

	CString sql;
	sql.Format(_T("SELECT NMWORK FROM WORKINFO WHERE"));

	if(!deviceid.IsEmpty() && deviceid!=_T("")) sql+=_T(" AND DEVICEID LIKE '%")+deviceid+_T("%'");
	if(!blockid.IsEmpty() && blockid!=_T(""))   sql+=_T(" AND BLOCKID LIKE '%")+blockid+_T("%'");
	if(!nmwork.IsEmpty() && nmwork!=_T(""))     sql+=_T(" AND NMWORK LIKE '%")+nmwork+_T("%'");

	if(!sstart.IsEmpty() && sstart!=_T("") && !send.IsEmpty() && send!=_T(""))
	{
		sql+=_T(" AND (SZSTARTTIME>='")+sstart+_T(" 00:00:00'")+_T(" AND SZSTARTTIME<'")+send+_T(" 23:59:59') ");
	}
	if(!estart.IsEmpty() && estart!=_T("") && !eend.IsEmpty() && eend!=_T(""))
	{
		sql+=_T(" AND  (SZENDTIME>='")+estart+_T(" 00:00:00'")+_T(" AND SZENDTIME<'")+eend+_T(" 23:59:59' )");	
	}
	if(sql.Find(_T("WHERE AND"))<0 && sql.Find(_T("WHERE"))>-1)
	{
		sql.Replace(_T("WHERE"),_T(""));
	}
	sql+=_T(" GROUP BY NMWORK ");
	sql.Replace(_T("WHERE AND"),_T("WHERE"));
	
	MYSQLINFO *MysqlInfo=m_MySql.obj_Result(sql);
	if(!MysqlInfo)
	{
		delete MysqlInfo; 
		return;
	}		
	if(MysqlInfo && MysqlInfo->res)
	{						
		int icnt=1;
		
		//while(MysqlInfo->row=mysql_fetch_row(MysqlInfo->res))
		while(TRUE)
		{		
			MysqlInfo->row=mysql_fetch_row(MysqlInfo->res);
			if(!MysqlInfo->row) break;

			CString *listdata=new CString[2];	
			listdata[0]=GStr::str_IntToStr(icnt++);
			listdata[1]=m_MySql.obj_FD(MysqlInfo,_T("NMWORK"));	
			
			GList::list_Insert(0,list,2,listdata,FALSE);
		}		
	}
	m_MySql.obj_InfoRefresh(MysqlInfo);//res,mysql,delete clear	
}

CStringArray* PneApp::GetListTreeRoot(CStringList *strData)
{	
	if(m_MySql.m_bCon==FALSE)   return NULL;
	if(strData->GetCount() < 1) return NULL;

	CString sql;
	sql.Format(_T("SELECT DISTINCT(NMWORK) AS NMWORK FROM WORKINFO WHERE"));
	
	POSITION pos = strData->GetHeadPosition();
	while (pos)
	{					
		CString nowork = strData->GetNext(pos);
		sql+=_T(" OR NOWORK='")+nowork+_T("' ");
	}	
	sql+=_T(" ORDER BY NMWORK DESC");
	sql.Replace(_T("WHERE OR"),_T("WHERE"));
	
	CStringArray *list=m_MySql.list_Query(sql);	
	if(m_MySql.list_ChkList(list)==FALSE) return NULL;
	
	return list;	
}

CStringArray* PneApp::GetListTreeNode(CString nmwork)
{	
	if(m_MySql.m_bCon==FALSE)  return NULL;
	
	CString sql;
	sql.Format(_T("SELECT * FROM WORKINFO WHERE"));
	sql+=_T(" NMWORK='")+nmwork+_T("' ");

	sql+=_T(" ORDER BY NOWORK DESC");
		
	CStringArray *list=m_MySql.list_Query(sql);	
	if(m_MySql.list_ChkList(list)==FALSE) return NULL;
	
	return list;	
}

CStringArray* PneApp::GetChData(CString nmwork,CString nmnode)
{		
	if(m_MySql.m_bCon==FALSE)  return NULL;
	
	CString sql;
	sql.Format(_T("SELECT * FROM WORKINFO WHERE"));
	sql+=_T(" AND NMWORK='")+nmwork+_T("' ");
	sql+=_T(" AND NMNODE='")+nmnode+_T("' ");
		
	sql.Replace(_T("WHERE AND"),_T("WHERE"));

	CStringArray *list=m_MySql.list_Query(sql);	
	if(m_MySql.list_ChkList(list)==FALSE) return NULL;
		
	return list;
}

CString PneApp::GetSchdData(CString nowork)
{	
	if(m_MySql.m_bCon==FALSE) return _T("");
	
	CString sql;
	sql.Format(_T("SELECT * FROM WORKINFO WHERE"));	
	sql+=_T(" NOWORK='")+nowork+_T("' ");

	MYSQLINFO *MysqlInfo=m_MySql.obj_Result(sql);
	if(!MysqlInfo)
	{
		delete MysqlInfo; 
		return _T("");
	}	
	
	CString strPath=_T("");
	if(MysqlInfo && MysqlInfo->res)
	{						
		MysqlInfo->row=mysql_fetch_row(MysqlInfo->res);
		
		CString nmwork=(CString)MysqlInfo->row[1];
		CString schedsize=(CString)MysqlInfo->row[21];
		int ilen=_ttoi(schedsize);

		//char *buffer=new char[ilen];
		//memset(buffer,0,ilen);
		//memcpy(buffer,(char*)(MysqlInfo->row[19]),ilen);	//ljb 20121219 20 -> 19
		 
		strPath.Format(_T("%s\\%05s.sch"),g_AppTempPath,nowork);

		#ifdef _UNICODE	
			FILE* file=_wfopen(strPath,_T("wb"));
		#else
			FILE* file=fopen(strPath,_T("wb"));
		#endif
		
		CString str=(CString)MysqlInfo->row[20];
		char *buf=new char[ilen];
        for(int i=0;i<ilen;i++)
		{
			CString temp=str.Mid(i*2,2);
			buf[i]=(char)GStr::str_Hex2IntB(temp);
		}
		fwrite(buf, ilen, 1, file);
		fclose(file);
		delete buf;
	}
	m_MySql.obj_InfoRefresh(MysqlInfo);//res,mysql,delete clear	
	return strPath;
}

CStringArray* PneApp::GetRawData(CString tbraw)
{		
	if(m_MySql.m_bCon==FALSE)  return NULL;
	
	CString sql;
	sql.Format(_T("SELECT * FROM %s "),tbraw);
	
	CStringArray *list=m_MySql.list_Query(sql);	
	if(m_MySql.list_ChkList(list)==FALSE) return NULL;
		
	return list;
}

MYSQLINFO* PneApp::GetSrcData(CString tbsrc,CString snIndexFrom,CString snIndexTo)
{	
	if(m_MySql.m_bCon==FALSE) return NULL;
	
	CString sql;
	sql.Format(_T("SELECT * FROM %s WHERE"),tbsrc);
	
	sql+=_T(" AND NOSRC>='")+snIndexFrom+_T("' ")+_T(" AND NOSRC<='")+snIndexTo+_T("' ");
	
	sql+=_T(" ORDER BY NOSRC ASC");	
	sql.Replace(_T("WHERE AND"),_T("WHERE"));
		
	MYSQLINFO *MysqlInfo=m_MySql.obj_Result(sql);
	if(!MysqlInfo)
	{
		delete MysqlInfo; 
		return NULL;
	}	
	return MysqlInfo;	
}

MYSQLINFO* PneApp::GetSrcCycleData(CString tbsrc,CString sCycle)
{	
	if(m_MySql.m_bCon==FALSE) return NULL;
	
	CString sql;
	sql.Format(_T("SELECT * FROM %s WHERE"),tbsrc);	
	sql+=_T(" TOTALCYCLE='")+sCycle+_T("' ");
	
	sql+=_T(" ORDER BY NOSRC ASC");	
	sql.Replace(_T("WHERE AND"),_T("WHERE"));
		
	MYSQLINFO *MysqlInfo=m_MySql.obj_Result(sql);
	if(!MysqlInfo)
	{
		delete MysqlInfo; 
		return NULL;
	}	
	return MysqlInfo;	
}

void PneApp::GetChList(CString sName,CListCtrl *list)
{	
	if(m_MySql.m_bCon==FALSE) return;
	
	CString sql;
	sql.Format(_T("SELECT * FROM WORKINFO WHERE"));	
	sql+=_T(" AND NMWORK ='")+sName+_T("' ");	
	sql+=_T(" ORDER BY NMWORK DESC");	
	sql.Replace(_T("WHERE AND"),_T("WHERE"));
		
	MYSQLINFO *MysqlInfo=m_MySql.obj_Result(sql);
	if(!MysqlInfo)
	{
		delete MysqlInfo; 
		return;
	}		
	if(MysqlInfo && MysqlInfo->res)
	{						
		int icnt=1;
		//while(MysqlInfo->row=mysql_fetch_row(MysqlInfo->res))
		while(TRUE)
		{	
			MysqlInfo->row=mysql_fetch_row(MysqlInfo->res);
			if(!MysqlInfo->row) break;

			CString tbraw=m_MySql.obj_FD(MysqlInfo,_T("TBRAW"));

			CString *listdata=new CString[4];	
			listdata[0]=GStr::str_IntToStr(icnt);
			listdata[1]=m_MySql.obj_FD(MysqlInfo,_T("NMWORK"));	
			listdata[2]=m_MySql.obj_FD(MysqlInfo,_T("NMNODE"));	
			listdata[3].Format(_T("%d"),m_MySql.GetMax(tbraw,_T("NTOTALCYCLENUM")));						
			GList::list_Insert(0,list,4,listdata,FALSE);
			icnt=icnt+1;
		}		
	}
	m_MySql.obj_InfoRefresh(MysqlInfo);//res,mysql,delete clear
}

void PneApp::GetSchedList(CString sName,CStringArray &DataList)
{	
	if(m_MySql.m_bCon==FALSE) return;
	
	CString sql;
	sql.Format(_T("SELECT NOWORK,SCHEDSIZE,SCHEDFILE FROM WORKINFO WHERE"));	
	sql+=_T(" AND NMWORK ='")+sName+_T("' ");	
	sql+=_T(" ORDER BY NMNODE DESC");	
	sql.Replace(_T("WHERE AND"),_T("WHERE"));
			
	MYSQLINFO *MysqlInfo=m_MySql.obj_Result(sql);
	if(!MysqlInfo)
	{
		delete MysqlInfo; 
		return;
	}		
	if(MysqlInfo && MysqlInfo->res)
	{								
		//while(MysqlInfo->row=mysql_fetch_row(MysqlInfo->res))
		while(TRUE)
		{	
			MysqlInfo->row=mysql_fetch_row(MysqlInfo->res);
			if(!MysqlInfo->row) break;

			CString nowork=m_MySql.obj_FD(MysqlInfo,_T("NOWORK"));			
			CString schedsize=m_MySql.obj_FD(MysqlInfo,_T("SCHEDSIZE"));
			if(schedsize.IsEmpty() || schedsize==_T("")) continue;
			int ilen=_ttoi(schedsize);

			CString strPath;
			strPath.Format(_T("%s\\%05s.sch"),g_AppTempPath,nowork);
			if(GFile::file_Finder(strPath)==TRUE)
			{
					DataList.Add(strPath);
					continue;
			}
		
			char *buffer=new char[ilen];
			memset(buffer,0,ilen);
			memcpy(buffer,(char*)(MysqlInfo->row[2]),ilen); 
					
			#ifdef _UNICODE	
				FILE* file=_wfopen(strPath,_T("wb"));
			#else
				FILE* file=fopen(strPath,_T("wb"));
			#endif
			fwrite(buffer, ilen, 1, file);
			fclose(file);

			delete buffer;			
			DataList.Add(strPath);
		}		
	}
	m_MySql.obj_InfoRefresh(MysqlInfo);//res,mysql,delete clear
}

CString PneApp::GetWorkChTable(CString strChPath,CString sfield)
{	
	if(m_MySql.m_bCon==FALSE) return _T("");
	
	CString strName=GStr::str_GetSplit(strChPath,0,'\\');
	CString strCh=GStr::str_GetSplit(strChPath,1,'\\');
	
	CString sql;
	sql.Format(_T("SELECT * FROM WORKINFO WHERE"));	
	sql+=_T(" AND LOWER(NMWORK) ='")+strName+_T("' ");	
	sql+=_T(" AND LOWER(NMNODE) ='")+strCh+_T("' ");	
	sql.Replace(_T("WHERE AND"),_T("WHERE"));
			
	MYSQLINFO *MysqlInfo=m_MySql.obj_Result(sql);
	if(!MysqlInfo)
	{
		delete MysqlInfo; 
		return _T("");
	}		

	CString strTb=_T("");
	if(MysqlInfo && MysqlInfo->res)
	{						
		//while(MysqlInfo->row=mysql_fetch_row(MysqlInfo->res))
		while(TRUE)
		{	
			MysqlInfo->row=mysql_fetch_row(MysqlInfo->res);
			if(!MysqlInfo->row) break;
		
			strTb=m_MySql.obj_FD(MysqlInfo,sfield);			
			break;
		}		
	}
	m_MySql.obj_InfoRefresh(MysqlInfo);//res,mysql,delete clear
	return strTb;
}

long PneApp::GetFirstTableIndexOfCycle(CString tbraw,LONG lCycleID)
{	
	if(m_MySql.m_bCon==FALSE) return 0;
		
	if(tbraw.IsEmpty() || tbraw==_T("")) return 0;

	CString strCyc;
	strCyc.Format(_T("%d"),lCycleID);
	
	CString noidx=m_MySql.GetData1(tbraw,_T("NTOTALCYCLENUM"),strCyc,_T("NOIDX"));	
    return _ttol(noidx);	
}

MYSQLINFO* PneApp::GetTableCycleList(CString tbraw,LONG lCycleID)
{	
	if(m_MySql.m_bCon==FALSE) return NULL;
	if(tbraw.IsEmpty() || tbraw==_T("")) return NULL;
	
	CString sql;
	sql.Format(_T("SELECT * FROM %s WHERE \
	               NTOTALCYCLENUM ='%d' "),tbraw,lCycleID);	
	
	MYSQLINFO *MysqlInfo=m_MySql.obj_Result(sql);
	if(!MysqlInfo)
	{
		delete MysqlInfo; 
		return NULL;
	}
	return MysqlInfo;
}

MYSQLINFO* PneApp::GetWorkChListTable(CString strTestName,CString strChName)
{	
	if(m_MySql.m_bCon==FALSE) return NULL;
		
	CString sql;
	sql.Format(_T("SELECT * FROM WORKINFO WHERE"));	

	if(!strTestName.IsEmpty() && strTestName!=_T(""))
	{
		sql+=_T(" AND LOWER(NMWORK) ='")+strTestName+_T("' ");	
	}
	if(!strChName.IsEmpty() && strChName!=_T(""))
	{
		sql+=_T(" AND LOWER(NMNODE) ='")+strChName+_T("' ");	
	}
	sql+=_T(" ORDER BY NMNODE ASC");
	sql.Replace(_T("WHERE AND"),_T("WHERE"));
					
	MYSQLINFO *MysqlInfo=m_MySql.obj_Result(sql);
	if(!MysqlInfo)
	{
		delete MysqlInfo; 
		return NULL;
	}		
	return MysqlInfo;
}

CString PneApp::GetRawIndexData(CStringArray *rawData,int nIdx,CString sfield)
{
	if(m_MySql.list_ChkList(rawData)==FALSE) return _T("");
	
	CString sdata=m_MySql.list_FD(rawData,nIdx,sfield);
	return sdata;
}

fltPoint* PneApp::GetSrcDataList(CStringArray *rawlist,CString tbsrc,int nItem,LONG &nTimeDataCnt,CString strXAxisTitle, CString strYAxisTitle)
{	
	CString snIndexFrom=mainPneApp.GetRawIndexData(rawlist,nItem, _T("NINDEXFROM"));	
	CString snIndexTo=mainPneApp.GetRawIndexData(rawlist,nItem, _T("NINDEXTO"));		
	
	nTimeDataCnt=0;
	MYSQLINFO *MysqlInfo=mainPneApp.GetSrcData( tbsrc, snIndexFrom, snIndexTo);		
	if(MysqlInfo && MysqlInfo->res)
	{
		nTimeDataCnt = (LONG)mysql_num_rows(MysqlInfo->res);
		fltPoint *fData = new fltPoint[nTimeDataCnt];

		int i=0;
		//while(MysqlInfo->row=mysql_fetch_row(MysqlInfo->res))
		while(TRUE)
		{	
			MysqlInfo->row=mysql_fetch_row(MysqlInfo->res);
			if(!MysqlInfo->row) break;
		
			CString strTime=mainPneApp.m_MySql.obj_FD(MysqlInfo,_T("SZTIME"));			
			CString sFVoltage=mainPneApp.m_MySql.obj_FD(MysqlInfo,_T("FVOLTAGE"));
			CString sFCurrent=mainPneApp.m_MySql.obj_FD(MysqlInfo,_T("FCURRENT"));
			CString sFCapacity=mainPneApp.m_MySql.obj_FD(MysqlInfo,_T("FCAPACITY"));
			CString sFWatthour=mainPneApp.m_MySql.obj_FD(MysqlInfo,_T("FWATTHOUR"));			

			CString sFTempature=mainPneApp.m_MySql.obj_FD(MysqlInfo,_T("FTEMPARATURE"));			
			CString sFTempature2=mainPneApp.m_MySql.obj_FD(MysqlInfo,_T("FTEMPARATURE2"));			
			CString sFTempature3=mainPneApp.m_MySql.obj_FD(MysqlInfo,_T("FTEMPARATURE3"));			
			CString sFOvenTempature=mainPneApp.m_MySql.obj_FD(MysqlInfo,_T("FOVENTEMPARATURE"));	
			CString sFAvgVoltage=mainPneApp.m_MySql.obj_FD(MysqlInfo,_T("FAVGVOLTAGE"));
			CString sFAvgCurrent=mainPneApp.m_MySql.obj_FD(MysqlInfo,_T("FAVGCURRENT"));
			CString sFMeter1=mainPneApp.m_MySql.obj_FD(MysqlInfo,_T("METER1"));

			#ifdef _UNICODE			
				if(strXAxisTitle==_T("Time"))                   fData[i].x=(float)GTime::time_GetSecStr(strTime);
				else if(strXAxisTitle==RS_COL_VOLTAGE)          fData[i].x=(float)_ttof(sFVoltage);
				else if(strXAxisTitle==RS_COL_CURRENT)          fData[i].x=(float)_ttof(sFCurrent);
				else if(strXAxisTitle==RS_COL_CAPACITY)         fData[i].x=(float)_ttof(sFCapacity);
				else if(strXAxisTitle==RS_COL_WATT_HOUR)        fData[i].x=(float)_ttof(sFWatthour);
				else if(strXAxisTitle==RS_COL_TEMPERATURE)      fData[i].x=(float)_ttof(sFTempature);
				else if(strXAxisTitle==RS_COL_TEMPERATURE2)     fData[i].x=(float)_ttof(sFTempature2);
				else if(strXAxisTitle==RS_COL_TEMPERATURE3)     fData[i].x=(float)_ttof(sFTempature3);
				else if(strXAxisTitle==RS_COL_OVEN_TEMPERATURE) fData[i].x=(float)_ttof(sFOvenTempature);
				else if(strXAxisTitle==RS_COL_AVG_VTG)          fData[i].x=(float)_ttof(sFAvgVoltage);
				else if(strXAxisTitle==RS_COL_AVG_CRT)          fData[i].x=(float)_ttof(sFAvgCurrent);
				else if(strXAxisTitle==_T("Meter1"))            fData[i].x=(float)_ttof(sFMeter1);
				
				if(strYAxisTitle==_T("Time"))                   fData[i].y=(float)GTime::time_GetSecStr(strTime);
				else if(strYAxisTitle==RS_COL_VOLTAGE)          fData[i].y=(float)_ttof(sFVoltage);
				else if(strYAxisTitle==RS_COL_CURRENT)          fData[i].y=(float)_ttof(sFCurrent);
				else if(strYAxisTitle==RS_COL_CAPACITY)         fData[i].y=(float)_ttof(sFCapacity);
				else if(strYAxisTitle==RS_COL_WATT_HOUR)        fData[i].y=(float)_ttof(sFWatthour);
				else if(strYAxisTitle==RS_COL_TEMPERATURE)      fData[i].y=(float)_ttof(sFTempature);
				else if(strYAxisTitle==RS_COL_TEMPERATURE2)     fData[i].y=(float)_ttof(sFTempature2);
				else if(strYAxisTitle==RS_COL_TEMPERATURE3)     fData[i].y=(float)_ttof(sFTempature3);
				else if(strYAxisTitle==RS_COL_OVEN_TEMPERATURE) fData[i].y=(float)_ttof(sFOvenTempature);
				else if(strYAxisTitle==RS_COL_AVG_VTG)          fData[i].y=(float)_ttof(sFAvgVoltage);
				else if(strYAxisTitle==RS_COL_AVG_CRT)          fData[i].y=(float)_ttof(sFAvgCurrent);
				else if(strYAxisTitle==_T("Meter1"))            fData[i].y=(float)_ttof(sFMeter1);		
			#else
				if(strXAxisTitle==_T("Time"))                   fData[i].x=(float)GTime::time_GetSecStr(strTime);
				else if(strXAxisTitle==RS_COL_VOLTAGE)          fData[i].x=(float)atof(sFVoltage);
				else if(strXAxisTitle==RS_COL_CURRENT)          fData[i].x=(float)atof(sFCurrent);
				else if(strXAxisTitle==RS_COL_CAPACITY)         fData[i].x=(float)atof(sFCapacity);
				else if(strXAxisTitle==RS_COL_WATT_HOUR)        fData[i].x=(float)atof(sFWatthour);
				else if(strXAxisTitle==RS_COL_TEMPERATURE)      fData[i].x=(float)atof(sFTempature);
				else if(strXAxisTitle==RS_COL_TEMPERATURE2)     fData[i].x=(float)atof(sFTempature2);
				else if(strXAxisTitle==RS_COL_TEMPERATURE3)     fData[i].x=(float)atof(sFTempature3);
				else if(strXAxisTitle==RS_COL_OVEN_TEMPERATURE) fData[i].x=(float)atof(sFOvenTempature);
				else if(strXAxisTitle==RS_COL_AVG_VTG)          fData[i].x=(float)atof(sFAvgVoltage);
				else if(strXAxisTitle==RS_COL_AVG_CRT)          fData[i].x=(float)atof(sFAvgCurrent);
				else if(strXAxisTitle==_T("Meter1"))            fData[i].x=(float)atof(sFMeter1);
				
				if(strYAxisTitle==_T("Time"))                   fData[i].y=(float)GTime::time_GetSecStr(strTime);
				else if(strYAxisTitle==RS_COL_VOLTAGE)          fData[i].y=(float)atof(sFVoltage);
				else if(strYAxisTitle==RS_COL_CURRENT)          fData[i].y=(float)atof(sFCurrent);
				else if(strYAxisTitle==RS_COL_CAPACITY)         fData[i].y=(float)atof(sFCapacity);
				else if(strYAxisTitle==RS_COL_WATT_HOUR)        fData[i].y=(float)atof(sFWatthour);
				else if(strYAxisTitle==RS_COL_TEMPERATURE)      fData[i].y=(float)atof(sFTempature);
				else if(strYAxisTitle==RS_COL_TEMPERATURE2)     fData[i].y=(float)atof(sFTempature2);
				else if(strYAxisTitle==RS_COL_TEMPERATURE3)     fData[i].y=(float)atof(sFTempature3);
				else if(strYAxisTitle==RS_COL_OVEN_TEMPERATURE) fData[i].y=(float)atof(sFOvenTempature);
				else if(strYAxisTitle==RS_COL_AVG_VTG)          fData[i].y=(float)atof(sFAvgVoltage);
				else if(strYAxisTitle==RS_COL_AVG_CRT)          fData[i].y=(float)atof(sFAvgCurrent);
				else if(strYAxisTitle==_T("Meter1"))            fData[i].y=(float)atof(sFMeter1);
			#endif


			i++;
		}	
		return fData;
	}	
	return NULL;
}

fltPoint* PneApp::GetSrcCycleDataList(CStringArray *rawlist,CString tbsrc,LONG nCycle,LONG &nTimeDataCnt,CString strXAxisTitle, CString strYAxisTitle)
{
	CString str;
	str.Format(_T("%d"),nCycle);

	nTimeDataCnt=0;
	MYSQLINFO *MysqlInfo=GetSrcCycleData( tbsrc, str);		
	if(MysqlInfo && MysqlInfo->res)
	{
		nTimeDataCnt = (LONG)mysql_num_rows(MysqlInfo->res);		
		fltPoint *fData = new fltPoint[nTimeDataCnt];

		int i=0;
		//while(MysqlInfo->row=mysql_fetch_row(MysqlInfo->res))
		while(TRUE)
		{	
			MysqlInfo->row=mysql_fetch_row(MysqlInfo->res);
			if(!MysqlInfo->row) break;
		
			CString strTime=mainPneApp.m_MySql.obj_FD(MysqlInfo,_T("SZTIME"));			
			CString sFVoltage=mainPneApp.m_MySql.obj_FD(MysqlInfo,_T("FVOLTAGE"));
			CString sFCurrent=mainPneApp.m_MySql.obj_FD(MysqlInfo,_T("FCURRENT"));
			CString sFCapacity=mainPneApp.m_MySql.obj_FD(MysqlInfo,_T("FCAPACITY"));
			CString sFWatthour=mainPneApp.m_MySql.obj_FD(MysqlInfo,_T("FWATTHOUR"));			

			CString sFTempature=mainPneApp.m_MySql.obj_FD(MysqlInfo,_T("FTEMPARATURE"));			
			CString sFTempature2=mainPneApp.m_MySql.obj_FD(MysqlInfo,_T("FTEMPARATURE2"));			
			CString sFTempature3=mainPneApp.m_MySql.obj_FD(MysqlInfo,_T("FTEMPARATURE3"));			
			CString sFOvenTempature=mainPneApp.m_MySql.obj_FD(MysqlInfo,_T("FOVENTEMPARATURE"));	
			CString sFAvgVoltage=mainPneApp.m_MySql.obj_FD(MysqlInfo,_T("FAVGVOLTAGE"));
			CString sFAvgCurrent=mainPneApp.m_MySql.obj_FD(MysqlInfo,_T("FAVGCURRENT"));
			CString sFMeter1=mainPneApp.m_MySql.obj_FD(MysqlInfo,_T("METER1"));

			#ifdef _UNICODE			
			    if(strXAxisTitle==_T("Time"))                   fData[i].x=(float)GTime::time_GetSecStr(strTime);					
				else if(strXAxisTitle==RS_COL_VOLTAGE)          fData[i].x=(float)_ttof(sFVoltage);
				else if(strXAxisTitle==RS_COL_CURRENT)          fData[i].x=(float)_ttof(sFCurrent);
				else if(strXAxisTitle==RS_COL_CAPACITY)         fData[i].x=(float)_ttof(sFCapacity);
				else if(strXAxisTitle==RS_COL_WATT_HOUR)        fData[i].x=(float)_ttof(sFWatthour);
				else if(strXAxisTitle==RS_COL_TEMPERATURE)      fData[i].x=(float)_ttof(sFTempature);
				else if(strXAxisTitle==RS_COL_TEMPERATURE2)     fData[i].x=(float)_ttof(sFTempature2);
				else if(strXAxisTitle==RS_COL_TEMPERATURE3)     fData[i].x=(float)_ttof(sFTempature3);
				else if(strXAxisTitle==RS_COL_OVEN_TEMPERATURE) fData[i].x=(float)_ttof(sFOvenTempature);
				else if(strXAxisTitle==RS_COL_AVG_VTG)          fData[i].x=(float)_ttof(sFAvgVoltage);
				else if(strXAxisTitle==RS_COL_AVG_CRT)          fData[i].x=(float)_ttof(sFAvgCurrent);
				else if(strXAxisTitle==_T("Meter1"))            fData[i].x=(float)_ttof(sFMeter1);
				
				if(strYAxisTitle==_T("Time"))                   fData[i].y=(float)GTime::time_GetSecStr(strTime);
				else if(strYAxisTitle==RS_COL_VOLTAGE)          fData[i].y=(float)_ttof(sFVoltage);
				else if(strYAxisTitle==RS_COL_CURRENT)          fData[i].y=(float)_ttof(sFCurrent);
				else if(strYAxisTitle==RS_COL_CAPACITY)         fData[i].y=(float)_ttof(sFCapacity);
				else if(strYAxisTitle==RS_COL_WATT_HOUR)        fData[i].y=(float)_ttof(sFWatthour);
				else if(strYAxisTitle==RS_COL_TEMPERATURE)      fData[i].y=(float)_ttof(sFTempature);
				else if(strYAxisTitle==RS_COL_TEMPERATURE2)     fData[i].y=(float)_ttof(sFTempature2);
				else if(strYAxisTitle==RS_COL_TEMPERATURE3)     fData[i].y=(float)_ttof(sFTempature3);
				else if(strYAxisTitle==RS_COL_OVEN_TEMPERATURE) fData[i].y=(float)_ttof(sFOvenTempature);
				else if(strYAxisTitle==RS_COL_AVG_VTG)          fData[i].y=(float)_ttof(sFAvgVoltage);
				else if(strYAxisTitle==RS_COL_AVG_CRT)          fData[i].y=(float)_ttof(sFAvgCurrent);
				else if(strYAxisTitle==_T("Meter1"))            fData[i].y=(float)_ttof(sFMeter1);
			#else
				if(strXAxisTitle==_T("Time"))                   fData[i].x=(float)GTime::time_GetSecStr(strTime);
				else if(strXAxisTitle==RS_COL_VOLTAGE)          fData[i].x=(float)atof(sFVoltage);
				else if(strXAxisTitle==RS_COL_CURRENT)          fData[i].x=(float)atof(sFCurrent);
				else if(strXAxisTitle==RS_COL_CAPACITY)         fData[i].x=(float)atof(sFCapacity);
				else if(strXAxisTitle==RS_COL_WATT_HOUR)        fData[i].x=(float)atof(sFWatthour);
				else if(strXAxisTitle==RS_COL_TEMPERATURE)      fData[i].x=(float)atof(sFTempature);
				else if(strXAxisTitle==RS_COL_TEMPERATURE2)     fData[i].x=(float)atof(sFTempature2);
				else if(strXAxisTitle==RS_COL_TEMPERATURE3)     fData[i].x=(float)atof(sFTempature3);
				else if(strXAxisTitle==RS_COL_OVEN_TEMPERATURE) fData[i].x=(float)atof(sFOvenTempature);
				else if(strXAxisTitle==RS_COL_AVG_VTG)          fData[i].x=(float)atof(sFAvgVoltage);
				else if(strXAxisTitle==RS_COL_AVG_CRT)          fData[i].x=(float)atof(sFAvgCurrent);
				else if(strXAxisTitle==_T("Meter1"))            fData[i].x=(float)atof(sFMeter1);
				
				if(strYAxisTitle==_T("Time"))                   fData[i].y=(float)GTime::time_GetSecStr(strTime);
				else if(strYAxisTitle==RS_COL_VOLTAGE)          fData[i].y=(float)atof(sFVoltage);
				else if(strYAxisTitle==RS_COL_CURRENT)          fData[i].y=(float)atof(sFCurrent);
				else if(strYAxisTitle==RS_COL_CAPACITY)         fData[i].y=(float)atof(sFCapacity);
				else if(strYAxisTitle==RS_COL_WATT_HOUR)        fData[i].y=(float)atof(sFWatthour);
				else if(strYAxisTitle==RS_COL_TEMPERATURE)      fData[i].y=(float)atof(sFTempature);
				else if(strYAxisTitle==RS_COL_TEMPERATURE2)     fData[i].y=(float)atof(sFTempature2);
				else if(strYAxisTitle==RS_COL_TEMPERATURE3)     fData[i].y=(float)atof(sFTempature3);
				else if(strYAxisTitle==RS_COL_OVEN_TEMPERATURE) fData[i].y=(float)atof(sFOvenTempature);
				else if(strYAxisTitle==RS_COL_AVG_VTG)          fData[i].y=(float)atof(sFAvgVoltage);
				else if(strYAxisTitle==RS_COL_AVG_CRT)          fData[i].y=(float)atof(sFAvgCurrent);
				else if(strYAxisTitle==_T("Meter1"))            fData[i].y=(float)atof(sFMeter1);
			#endif
 
			i++;
		}	
		return fData;
	}	
	return NULL;
}


BOOL PneApp::SetStateInsert(CTypedPtrList<CPtrList, CHINFO*> &ChList)
{
	int ncount=ChList.GetCount();
	if(ncount<1) return TRUE;

	CHINFO *chinfo=NULL;
	POSITION pos =  ChList.GetHeadPosition();

	for(int i=0;i<ncount;i++)
	{
		chinfo=(CHINFO*)ChList.GetNext(pos);
		if(!chinfo) continue;
		
		if(SetMonitoring(chinfo)==FALSE) return FALSE;
		Sleep(100);
	}

	return TRUE;
}

BOOL PneApp::SetMonitoring(CHINFO *chinfo)
{
	if(!chinfo) return FALSE;

	CString szDeviceid;
	CString szBlockid;
	CString szModuleid;
	CString szChannelIndex;
    
	CString szNmwork;
	CString szNmmodule;
	CString szTestSerial;
	CString szEqstate;
	CString szEqstr;
	CString szNmsched;

	CString szChNo;
	CString szStepNo;
	CString szState;	//chstate
	CString szStepType;
	CString szChCode;
	CString szChGradeCode;

	CString szCurrentCycleNum;	
    CString szTotalCycleNum;     
	
	CString szfVoltage;			
    CString szfCurrent;
	CString szfCapacity;
	CString szfDisCapacity;
	CString szfWatt;	
	CString szfWattHour;
	CString szfDisWattHour;
	CString szfStepTime;			
	CString szfTotalTime;
	CString szfImpedance;		
	CString szfTemparature;
	CString szfTemparature2;		
	CString szfTemparature3;		
	CString szfOvenTemparature;	
	CString szfPressure;
	CString szfMeter;
	CString szStartTime;
	CString szEndTime;
	CString szfSharingInfo;
	CString szfChamberUsing;
    CString szGotoCycleCount;

    CString szfDayTime;
	CString szfDayTotTime;
	CString szfRealDate;
	CString szfRealClock;
	CString szfDebug1;
	CString szfDebug2;
	CString szfAvgVoltage;
	CString szfAvgCurrent;

	CString szfCapacitySum;
	CString szfWatthourSum;
		
	szDeviceid.Format(_T("%010s"),g_AppInfo.strDeviceid);
	szBlockid.Format(_T("%010s"),g_AppInfo.strBlockid);

	szNmwork=chinfo->nmwork;
	szModuleid.Format(_T("%d"),chinfo->nModuleID);
	szChannelIndex.Format(_T("%d"),chinfo->nChannelIndex);
	szNmmodule=chinfo->nmmodule;
	szTestSerial=chinfo->testserial;//nmcondition
    szEqstate=chinfo->Eqstate;
	szEqstr=chinfo->Eqstr;
	szNmsched=chinfo->nmsched;

	szChNo.Format(_T("%d"),chinfo->pRecord.chNo);
	szStepNo.Format(_T("%d"),chinfo->pRecord.chStepNo);
	szState.Format(_T("%d"),chinfo->pRecord.chState);
	szStepType.Format(_T("%d"),chinfo->pRecord.chStepType);
	szChCode.Format(_T("%d"),chinfo->pRecord.chCode);
    szChGradeCode.Format(_T("%d"),chinfo->pRecord.chGradeCode);
	
	szCurrentCycleNum.Format(_T("%d"),chinfo->pRecord.nCurrentCycleNum);
	szTotalCycleNum.Format(_T("%d"),chinfo->pRecord.nTotalCycleNum);
    
	szfCurrent.Format(_T("%f"),chinfo->pRecord.fCurrent);
	szfVoltage.Format(_T("%f"),chinfo->pRecord.fVoltage);
	szfCapacity.Format(_T("%f"),chinfo->pRecord.fCharge_AmpareHour);
	szfDisCapacity.Format(_T("%f"),chinfo->pRecord.fDisCharge_AmpareHour);
	szfWatt.Format(_T("%f"),chinfo->pRecord.fWatt);	
	szfWattHour.Format(_T("%f"),chinfo->pRecord.fCharge_WattHour);
	szfDisWattHour.Format(_T("%f"),chinfo->pRecord.fDisCharge_WattHour);
	szfStepTime.Format(_T("%f"),chinfo->pRecord.fStepTime);
	szfTotalTime.Format(_T("%f"),chinfo->pRecord.fTotalTime);
	szfImpedance.Format(_T("%f"),chinfo->pRecord.fImpedance);
	szfTemparature.Format(_T("%f"),chinfo->pRecord.fTemparature);
	szfTemparature2.Format(_T("%f"),chinfo->pRecord.fTemparature2);
	szfTemparature3.Format(_T("%f"),chinfo->pRecord.fTemparature3);
	szfOvenTemparature.Format(_T("%f"),chinfo->pRecord.fOvenTemparature);
	szfPressure.Format(_T("%f"),chinfo->pRecord.fPressure);
	szfMeter.Format(_T("%f"),chinfo->fMeter);
    szStartTime=chinfo->starttime;
	szEndTime=chinfo->endtime;
	szfSharingInfo.Format(_T("%f"),chinfo->fSharingInfo);
    szfChamberUsing.Format(_T("%f"),chinfo->fChamberUsing);
    szGotoCycleCount.Format(_T("%f"),chinfo->GotoCycleCount);

	szfDayTime.Format(_T("%f"),chinfo->pRecord.fStepTime_day);
	szfDayTotTime.Format(_T("%f"),chinfo->pRecord.fTotalTime_day);
	szfRealDate.Format(_T("%f"),chinfo->pRecord.fRealDate);
	
	szfRealClock.Format(_T("%f"),chinfo->pRecord.fRealClock);	
	szfDebug1.Format(_T("%f"),chinfo->pRecord.fDebug1);
	szfDebug2.Format(_T("%f"),chinfo->pRecord.fDebug2);
	szfAvgVoltage.Format(_T("%f"),chinfo->pRecord.fAvgVoltage);
	szfAvgCurrent.Format(_T("%f"),chinfo->pRecord.fAvgCurrent);
    
	szfCapacitySum.Format(_T("%f"),chinfo->fCapacitySum);
	szfWatthourSum.Format(_T("%f"),chinfo->fWatthourSum);

	CString nomonitor=chinfo->nomonitor;
	if(nomonitor.IsEmpty() || nomonitor==_T(""))
	{
		nomonitor=m_MySql.GetData4(_T("MONITORINFO"),_T("DEVICEID"),szDeviceid,
													 _T("BLOCKID"),szBlockid,
													 _T("MODULEID"),szModuleid,
													 _T("CHNO"),szChNo,
													 _T("NOMONITOR"));
		chinfo->nomonitor=nomonitor;		
	}

    CString sql;
	if(nomonitor.IsEmpty() || nomonitor==_T(""))
	{		
		sql.Format(_T("INSERT INTO MONITORINFO \
						(NMWORK,DEVICEID,BLOCKID,MODULEID,NMMODULE,CHANNEL,             \
						 TESTSERIAL,EQSTATE,EQSTR,NMSCHED,                              \
                         CHNO,CHSTEPNO,CHSTATE,CHSTEPTYPE,CHCODE,CHGRADECODE,           \
						 NCURRENTCYCLENUM,NTOTALCYCLENUM,                               \
                         FVOLTAGE,FCURRENT,FCAPACITY,FDISCAPACITY,FCAPACITYSUM,         \
						 FWATT,FWATTHOUR,FDISWATTHOUR,FWATTHOURSUM,                     \
                         FAVGVOLTAGE,FAVGCURRENT,STARTTIME,ENDTIME,                     \
						 FSTEPTIME,FTOTALTIME,FIMPEDANCE,FPRESSURE,FMETER,              \
                         FTEMPARATURE,FTEMPARATURE2,FTEMPARATURE3,FOVENTEMPARATURE,     \
						 FDAYTIME,FDAYTOTTIME,FREALDATE,FREALCLOCK,FDEBUG1,FDEBUG2,     \
						 FSHARINGINFO,FCHAMBERUSING,GOTOCYCLECOUNT,                     \
						 DTREG)                                                         \
						VALUES ('%s','%s','%s','%s','%s','%s',                          \
                        '%s','%s','%s','%s',                                            \
                        '%s','%s','%s','%s','%s','%s',                                  \
						'%s','%s',                                                      \
						'%s','%s','%s','%s','%s',                                       \
						'%s','%s','%s','%s',                                            \
						'%s','%s', '%s','%s',                                           \
						'%s','%s','%s','%s','%s',                                       \
						'%s','%s','%s','%s',                                            \
						'%s','%s','%s','%s','%s','%s',                                  \
						'%s','%s','%s',                                                 \
						NOW())"),
						szNmwork,szDeviceid,szBlockid,szModuleid,szNmmodule,szChannelIndex,
						szTestSerial,szEqstate,szEqstr,szNmsched,
						szChNo,szStepNo,szState,szStepType,szChCode,szChGradeCode,
						szCurrentCycleNum,szTotalCycleNum,
						szfVoltage,szfCurrent,szfCapacity,szfDisCapacity,szfCapacitySum,
						szfWatt,szfWattHour,szfDisWattHour,szfWatthourSum,
						szfAvgVoltage,szfAvgCurrent,szStartTime,szEndTime,
						szfStepTime,szfTotalTime,szfImpedance,szfPressure,szfMeter,
						szfTemparature,szfTemparature2,szfTemparature3,szfOvenTemparature,
						szfDayTime,szfDayTotTime,szfRealDate,szfRealClock,szfDebug1,szfDebug2,
						szfSharingInfo,szfChamberUsing,szGotoCycleCount);	
	}
	else
	{
		sql.Format(_T("UPDATE MONITORINFO SET  \
					   NMWORK='%s',            \
					   TESTSERIAL='%s',        \
					   EQSTATE='%s',           \
					   EQSTR='%s',             \
					   NMSCHED='%s',           \
                       CHANNEL='%s',           \
					   CHSTEPNO='%s',          \
					   CHSTATE='%s',           \
					   CHSTEPTYPE='%s',        \
					   CHCODE='%s',            \
					   CHGRADECODE='%s',       \
					   NCURRENTCYCLENUM='%s',  \
					   NTOTALCYCLENUM='%s',    \
					   FVOLTAGE='%s',          \
					   FCURRENT='%s',          \
					   FCAPACITY='%s',         \
					   FDISCAPACITY='%s',      \
					   FCAPACITYSUM='%s',      \
					   FWATT='%s',             \
					   FWATTHOUR='%s',         \
					   FDISWATTHOUR='%s',      \
					   FWATTHOURSUM='%s',      \
					   FAVGVOLTAGE='%s',       \
					   FAVGCURRENT='%s',       \
					   FSTEPTIME='%s',         \
					   FTOTALTIME='%s',        \
					   FIMPEDANCE='%s',        \
					   FTEMPARATURE='%s',      \
					   FTEMPARATURE2='%s',     \
					   FTEMPARATURE3='%s',     \
					   FOVENTEMPARATURE='%s',  \
					   FPRESSURE='%s',         \
					   FMETER='%s',            \
					   STARTTIME='%s',         \
					   ENDTIME='%s',           \
					   FSHARINGINFO='%s',      \
					   FCHAMBERUSING='%s',     \
					   GOTOCYCLECOUNT='%s',    \
					   FDAYTIME='%s',          \
					   FDAYTOTTIME='%s',       \
					   FREALDATE='%s',         \
					   FREALCLOCK='%s',        \
					   FDEBUG1='%s',           \
					   FDEBUG2='%s',           \
					   DTREG=NOW()             \
					   WHERE NOMONITOR='%s' "),
					   szNmwork,szTestSerial,szEqstate,szEqstr,szNmsched,
					   szChannelIndex,szStepNo,szState,szStepType,szChCode,szChGradeCode,
					   szCurrentCycleNum,szTotalCycleNum,
					   szfVoltage,szfCurrent,szfCapacity,szfDisCapacity,szfCapacitySum,
					   szfWatt,szfWattHour,szfDisWattHour,szfWatthourSum,
					   szfAvgVoltage,szfAvgCurrent,
					   szfStepTime,szfTotalTime,szfImpedance,			   
					   szfTemparature,szfTemparature2,szfTemparature3,szfOvenTemparature,
					   szfPressure,szfMeter,szStartTime,szEndTime,szfSharingInfo,szfChamberUsing,
					   szGotoCycleCount,
					   szfDayTime,szfDayTotTime,szfRealDate,szfRealClock,szfDebug1,szfDebug2,
					   nomonitor);
	}
	BOOL bRet=m_MySql.SetQuery(sql);
	return bRet;
}*/