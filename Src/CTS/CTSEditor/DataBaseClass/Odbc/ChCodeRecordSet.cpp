// ChCodeRecordSet.cpp : implementation file
//

#include "stdafx.h"
#include "ChCodeRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChCodeRecordSet

IMPLEMENT_DYNAMIC(CChCodeRecordSet, CRecordset)

CChCodeRecordSet::CChCodeRecordSet(CDatabase* pdb)
	: CRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CChCodeRecordSet)
	m_Code = 0;
	m_Messge = _T("");
	m_nFields = 2;
	//}}AFX_FIELD_INIT
	m_nDefaultType = snapshot;
}


CString CChCodeRecordSet::GetDefaultConnect()
{
	return _T("ODBC;DSN=TestCondition");
}

CString CChCodeRecordSet::GetDefaultSQL()
{
	return _T("[ChannelCode]");
}

void CChCodeRecordSet::DoFieldExchange(CFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CChCodeRecordSet)
	pFX->SetFieldType(CFieldExchange::outputColumn);
	RFX_Long(pFX, _T("[Code]"), m_Code);
	RFX_Text(pFX, _T("[Messge]"), m_Messge);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CChCodeRecordSet diagnostics

#ifdef _DEBUG
void CChCodeRecordSet::AssertValid() const
{
	CRecordset::AssertValid();
}

void CChCodeRecordSet::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG
