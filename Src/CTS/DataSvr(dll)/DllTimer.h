#if !defined(AFX_DLLTIMER_H__47B422D2_652C_4F0B_8F12_EB6DD50B716E__INCLUDED_)
#define AFX_DLLTIMER_H__47B422D2_652C_4F0B_8F12_EB6DD50B716E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DllTimer.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDllTimer dialog

class CDllTimer : public CDialog
{
// Construction
public:
	CDllTimer(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDllTimer)
	enum { IDD = IDD_TIMER_DLG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDllTimer)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDllTimer)
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLLTIMER_H__47B422D2_652C_4F0B_8F12_EB6DD50B716E__INCLUDED_)
