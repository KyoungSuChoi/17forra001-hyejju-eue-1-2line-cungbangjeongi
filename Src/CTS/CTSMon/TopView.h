#if !defined(AFX_TOPVIEW_H__A8B5D776_2D75_11D4_850D_0060083FBBB6__INCLUDED_)
#define AFX_TOPVIEW_H__A8B5D776_2D75_11D4_850D_0060083FBBB6__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// TopView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTopView form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

#include "ColorButton2.h"
#include "MyGridWnd.h"
#include "CTSMonDoc.h"
#include "IconCombo.h"
#include "MatrixStatic.h"
#include "afxwin.h"

#define BTN_FONT_SIZE 20

enum { SELECT_ITEM_STATE = 0, SELECT_ITEM_VOLTAGE, SELECT_ITEM_CURRENT, SELECT_ITEM_CAPACITY, SELECT_ITEM_CODE, 
SELECT_ITEM_IMP, SELECT_ITEM_STEPTIME, SELECT_ITEM_WATT, SELECT_ITEM_WATTHOUR, SELECT_ITEM_CELLSERIAL };

static bool g_bChannelBlink = false;

class CTopView : public CFormView
{
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

protected:
	CTopView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CTopView)

// Form Data
public:
	//{{AFX_DATA(CTopView)
	enum { IDD = IDD_TOP_VIEW };
//	CMatrixStatic	m_ctrlTray3;
	CListCtrl	m_ctrlStepList;
	CIconCombo	m_ctrlDiplayData;
	CLabel	m_ctrlErrorRate;
	CLabel	m_ctrlNormalRate;
	CLabel	m_ctrlErrorRate1;
	CLabel	m_ctrlNormalRate1;	
	//}}AFX_DATA

// Attributes
public:
	int m_nTopGridRowCount;
	int m_nTopGridColCount;	

// Operations
public:
	CCTSMonDoc* GetDocument();
	void UpdateGroupState(int nModuleID, int nGroupIndex=0);	
	void ModuleConnected(int nModuleID);
	void ModuleDisConnected( int nModuleID);
	void GroupSetChanged(int nColCount = -1);
	void TopConfigChanged(); 
	void InitSmallChGrid();
	void DrawStepData(int nStepIndex, int nItem);
	CString DataUnitChanged();
	BOOL GetChRowCol(int nItemIndex, int nChindex, ROWCOL &nRow, ROWCOL &nCol);
	void StopMonitoring();
	void StartMonitoring();
	int GetCurModuleID();
	UINT GetMonIndex(UINT index) {	return ((index%m_nTopGridColCount)*m_nTopGridRowCount)+(index/m_nTopGridColCount);	};
	BOOL ResetCmdFlag(int cmd);
	BOOL SetCmdFlag(int cmd);
	BYTE GetColorIndex(STR_SAVE_CH_DATA  &chData, BYTE colorIndex = STATE_COLOR, CStep *step = NULL);
	CString GetTargetModuleName();
	void SetCurrentModule(int nModuleID, int nGroupIndex = 0);
	void SetTopGridFont();
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTopView)
	public:
	virtual void OnInitialUpdate();
	virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo = NULL);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
protected:
	int m_nCurTrayIndex;
	int m_nCurSelStepIndex;
	void DrawStepList();
	void DrawOnlineStepList( int nModule );
	void InitProgressGrid();
	CImageList *m_pImagelist;
	UINT m_nDisplayTimer;
	SHORT m_awSelChArray[MAX_CH_PER_MD];
	int m_nCmdTarget;

	int	 m_DispItemNum;
	int  m_nPrevStep;
	void DrawChannel();
	void InitLabel();
	void InitColorBtn();
	
	CImageList *m_pChStsSmallImage;
	STR_TOP_CONFIG		*m_pConfig;
	char	*m_pModuleColorFlag;
	char	*m_pTopColorFlag;
	SHORT	*m_pRowCol2GpCh;

	int m_nCurModuleID;	
	int m_nCurGroup;
	int m_nCurChannel;
	
	CMyGridWnd	m_wndSmallChGrid;
	
	enum ViewType { TopViewType1=0, TopViewType2 };

	void	Fun_UpdateChannelInfo( ViewType nViewType );

	virtual ~CTopView();
	
	

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CTopView)
	afx_msg void OnVoltageRadio();
	//afx_msg void OnCurrentRadio();
	//afx_msg void OnCapacityRadio();	
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnUpdateFileOpen(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFileSaveAs(CCmdUI* pCmdUI);
	afx_msg void OnDestroy();
	afx_msg void OnUpdateChCheck(CCmdUI* pCmdUI);
	afx_msg void OnUpdateChRun(CCmdUI* pCmdUI);
	afx_msg void OnUpdateChPause(CCmdUI* pCmdUI);
	afx_msg void OnUpdateChContinue(CCmdUI* pCmdUI);
	afx_msg void OnUpdateChStop(CCmdUI* pCmdUI);
	afx_msg void OnUpdateChStepover(CCmdUI* pCmdUI);
	afx_msg void OnTraynoUserInput();
	afx_msg void OnUpdateTraynoUserInput(CCmdUI* pCmdUI);
	afx_msg void OnOnlineMode();
	afx_msg void OnUpdateOnlineMode(CCmdUI* pCmdUI);
	afx_msg void OnOfflineMode();
	afx_msg void OnUpdateOfflineMode(CCmdUI* pCmdUI);
	afx_msg void OnMaintenanceMode();
	afx_msg void OnUpdateMaintenanceMode(CCmdUI* pCmdUI);
	afx_msg void OnControlMode();
	afx_msg void OnUpdateControlMode(CCmdUI* pCmdUI);
	afx_msg void OnReboot();
	afx_msg void OnGetProfileData();
	afx_msg void OnUpdateGetProfileData(CCmdUI* pCmdUI);	
	afx_msg void OnUpdateAutoProcOn(CCmdUI* pCmdUI);
	afx_msg void OnUpdateAutoProcOff(CCmdUI* pCmdUI);
	afx_msg void OnRefAdValueUpdate();
	afx_msg void OnUpdateRefAdValueUpdate(CCmdUI* pCmdUI);
	afx_msg void OnSelchangeDataTypeCombo();
	afx_msg void OnGetdispinfoStepList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnItemchangedStepList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDblclkStepList(NMHDR* pNMHDR, LRESULT* pResult);	
	afx_msg void OnEditCopy();
	afx_msg void OnUpdateEditCopy(CCmdUI* pCmdUI);
	afx_msg void OnGradeColorSet();
	afx_msg void OnViewResult();
	afx_msg void OnUpdateViewResult(CCmdUI* pCmdUI);
	//}}AFX_MSG	
	DECLARE_MESSAGE_MAP()
private:	
public:
	CLabel m_LabelViewName;
	CLabel m_CmdTarget;
//	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnBnClickedViewSelect1Btn();
	afx_msg void OnBnClickedViewSelect2Btn();
	CColorButton2 m_Btn_ViewSelect1;
	CColorButton2 m_Btn_ViewSelect2;
	CColorButton2 m_Btn_ResultDataDisplay;
	CColorButton2 m_Btn_ContactResultDataDisplay;
	CColorButton2 m_Btn_SendProcess;
	afx_msg void OnBnClickedResultdataDispBtn();
	afx_msg void OnBnClickedContactdataDispBtn();
	afx_msg void OnBnClickedSendProcessBtn();
};

#ifndef _DEBUG  // debug version in TopView.cpp
inline CCTSMonDoc* CTopView::GetDocument()
   { return (CCTSMonDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TOPVIEW_H__A8B5D776_2D75_11D4_850D_0060083FBBB6__INCLUDED_)
