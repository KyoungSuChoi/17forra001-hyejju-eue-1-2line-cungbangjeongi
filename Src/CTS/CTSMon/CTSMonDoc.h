// CTSMonDoc.h : interface of the CCTSMonDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CTSMonDOC_H__22C12B1F_B537_4E37_A549_C83AAFFD8BF9__INCLUDED_)
#define AFX_CTSMonDOC_H__22C12B1F_B537_4E37_A549_C83AAFFD8BF9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "FMS.h"
#include "FM_Unit.h"
#include "FM_STATE.h"
#include "FMS_SM.h"
// #include "CMySql_DB.h"

class CCTSMonDoc : public CDocument
{
protected: // create from serialization only
	CCTSMonDoc();
	DECLARE_DYNCREATE(CCTSMonDoc)

// Attributes
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	CFMS_SM m_fmst;

	VOID	TotalMonThread(VOID);
	VOID	onDbInit();
	BOOL	onWorkDeal();
	VOID	ReConnectError();
	HANDLE	mTotalMonThreadHandle;
	HANDLE	mTotalMonThreadDestroyEvent;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCTSMonDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual void OnCloseDocument();
	void ReLoadSensorSetting();	//20200831ksj
	//}}AFX_VIRTUAL

// Implementation
public:
	void fnWriteTemperature( int nModuleID = 0, BOOL bEndFlag = FALSE );

	void fnWriteTempFileSave();

	int m_nDeviceID;					// Line 별 DeviceID
	bool m_bUsePneMonitoringSystem;		// 통합모니터링 사용 유무
	// CMySql_DB *m_MySqlServer;
	BOOL Fun_UpdateCellLastTime(CDatabase &db, CString strCell);
	BOOL Fun_UpdateCellData(CDatabase &db,CString strCell,CString strTestName,CString strTrayName,long lProType,long lVolt,long lCurrent,long lCapa, unsigned long lTime);

	void UpdateTrayCellStartNo( int nModuleID );
	int GetModuleStartChNo(int nModuleID, int nTrayIndex = 0);
	//  [7/21/2009 kky ]
	// for 온라인 모드시 스텝데이터 저장

	UINT m_OnlineStepData[MAX_ONLINE_UNIT][MAX_ONLINE_STEPDATA];
	BOOL ConvertToExcel(CString strFileName);
	
	COLORREF GetGradeColor(CString strCode);
	STR_GRADE_COLOR_CONFIG m_gradeColorConfig;
	BOOL m_bHideNonCellData;
	BOOL	CheckProcAndSystemType(long lProcTypeID);
	CString CreateResultFilePath(int nModuleID, CString strDataFolder, CString strLot, CString strTray);
	int		SendAutoProcessRun(int nModuleID);
	BOOL	SendTrayReady(int nModule, int nJigNo, BOOL bReady);
	CString GetJigState(int nModuleID);
	CString GetDoorState(int nModuleID);
	CString GetTrayLoadState(int nModuleID);
	BOOL	CheckStopperSensor(int nModuleID, int nJigNo = 0);
	int		FindJigNoTrayNo(int nModuleID, CString strID);
	DWORD	CheckEnableRun(int nModuleID);
	BOOL	TrayMatchingCheck(int nModuleID);
	BOOL	CheckFileNameValidate(CString strFileName, CString &strNewFileName, int nModuleID, int nGroupIndex = 0 );
	BOOL	SendStopCmd(int nModuleID, int nGroupIndex = 0);
	BOOL	SendChangeTrayTypeCmd(int nModuleID, int nGroupIndex = 0,int nTrayType = 0);
	BOOL	DownLoadProfileData(int nModuleID, CString strFileName, BOOL bAuto = FALSE);
//	BOOL DownLoadProfileData(int nModuleID = 0, BOOL bAuto = FALSE);
//  BOOL DownLoadProfileData(CString strFileName, CWordArray &awSelCh);

	int m_nTimeUnit;
	CString m_strVUnit;
	int m_nVDecimal ;
	CString m_strIUnit;
	int m_nIDecimal;
	CString m_strCUnit;
	int m_nCDecimal;
	CString m_strWUnit;
	CString m_strWhUnit;

	CStringArray m_IoInfoModuleArray;

	CString GetLastErrorString()	{return m_strLastErrorString;	}
	void	UpdateUnitSetting();
	int		m_nTimeFolderInterval;
	BOOL	m_bFolderTime;
	BOOL	m_bFolderModuleName;
	BOOL	m_bFolderLot;
	BOOL	m_bFolderTray;
	// [2/19/2010 kimky]
	// ChVoltageSafe
	long	m_lWarnningChVoltage;				// 경고값
	long	m_lDangerChVoltage;					// 위험값
	int		m_nAfterSafeVoltageError;			// 위험값을 넘어갈 경우 조치상황 0:경고창, 1:경고창+Pause, 2:경고창+Stop, 3:경고창+Shutdown
	int		m_nContactErrlimit;					// ContactSetting Error 채널 수
	int		m_nCapaErrlimit;	
	int		m_nChErrlimit;

	CString	m_strTypeSel[4];					//20201211 ksj
	int		m_ProcessNo;
	int		nFlag;				

	CString m_strCapacityFormula;
	CString m_strDCRFormula; 

	BOOL	CheckAndLoadPrevTestInfo(int nModuleID, int nGroupIndex = 0);
	BOOL	CheckPrevTestLog(int nModuleID, int nTrayIndex = 0);
	CFormModule * GetModuleInfo(int nModuleID);
	BOOL	ResetTrayProcSchedule(CString strTrayNo);
	BOOL	ExecuteEditor(CString strModel, CString strTest, BOOL bNewWnd = FALSE);
	BOOL	ExecuteProgram (	CString strProgram, 
							CString strArgument = "", 
							CString strClassName = "",
							CString strWindowName = "",
							BOOL bNewExe = FALSE,
							BOOL bWait = FALSE,
							BOOL bForground = TRUE);
	BOOL	ExecuteProgram1 (	CString strProgram, 
		CString strArgument = "", 
		CString strClassName = "",
		CString strWindowName = "",
		BOOL bNewExe = FALSE,
		BOOL bWait = FALSE,
		BOOL bForground = TRUE);

	void ViewResult(int nModule, int nTrayIndex=-1, int nDataType = 0);		// 0:result data view, 1:Contact check result view
	WORD m_nViewTrayType;
	bool GetModuleTroubeMsg(WORD code, CString &strCode, CString &strMsg);
	void ShellExecute(CString strName, CString strOption);
	void WriteEMGLog(CString strLog);
	CString GetModuleIPAddress(int nModuleID);
	BOOL WriteModuleIPToDataBase(int nModuleID, LPSTR szIPAddress);
	BOOL ExecuteIOTest(int nModuleID, BOOL bWaitExit = FALSE);
	BOOL CheckLastProcess(CTray *pTrayData);
	BOOL SearchLastTrayState(CString strTrayNo, int nModuleID = 0, int nGroupIndex = 0);
	void ShowGroupFailCode(int nModuleID, int nGroupIndex = 0);
	void ShowTestCondition(int nModuleID, int nGroupIndex = 0);
	CString StepEndString(STR_COMMON_STEP *pStep) ;
	CString StepEndString(CStep *pStep) ;
	CString m_strLastErrorString;
	VOID SetAutoReport(int nNewModuleID = 0, int nOldModuleID = 0);
	void WriteLog(CString strMsg);
	BOOL SetOperationMode(int nModuleID, int nGroupIndex, int nControlMode);
	
	BOOL SetLineMode(int nModuleID, int nGroupIndex, int nLineMode);
	int GetLineMode(int nModuleID, int nGroupIndex = 0);
	int GetOperationMode(int nModuleID, int nGroupIndex = 0);
	UINT m_nTrayColSize;
	CPtrArray m_apProcType;
	CString GetProcTypeName(int nType, int nStepType);
	long m_nCmdInterval;

	CString ValueString(double dData, int item, BOOL bUnit = FALSE);
	BOOL m_bUseRackIndex;
	BOOL m_bUseOnLine;
	int m_nModulePerRack;
	BOOL SendProcedureLogToDataBase(int nModuleID, int nTrayIndex = 0);
	CTestCondition * GetCondition(int nModuleID, int nGroupIndex = 0);
	BOOL RequeryBatteryModel(long lModelID, STR_CONDITION_HEADER *pModel);
	CTray * GetTrayData(int nModuleID, int nTrayIndex = 0);
	BOOL SendLastTrayStateData(int nModuleID, int nGroupIndex = 0);
	BOOL UpdateTrayTestData(int nModuleID, int nTrayIndex = 0);
	BOOL FileNameCheck(char *szfileName);
	STR_CONDITION_HEADER SelectTest(BOOL bSelectFirstTest = FALSE);
	STR_TOP_CONFIG		m_TopConfig;
	STR_TEMP_CONFIG		m_TempConfig;
	STR_TEMPSCALE_CONFIG	m_TempScaleConfig;
	STR_CONDITION_HEADER GetTestData(long lTestID);
	STR_EMG_LOG	m_EmgList[200];
	int	m_nEmgLogIndex;
	int m_nEmgMaxCnt;
	BOOL FindTrayFromDB(CString strTrayNo, CTray &trayData);
	BOOL SaveAutoProcSetToDB(int nModuleID, BOOL bAutoSet);
	CString CmdFailMsg(int nCode);	
	STR_CONDITION_HEADER GetNextTest(long lTestID);
	STR_CONDITION_HEADER GetPrevTest(long lTestID);
	
	BOOL m_bUseGroupSet;
	BOOL WriteProcedureLog(int nModuleID, int nGroupIndex = 0);
	int SendRunCommand(int nModuleID, int nGroupIndex = 0 );	
	int ReadEPFileHeader(int nFile, FILE *fp);
	BOOL WriteEPFileHeader(int nFile, FILE *fp);
	CString GetTrayNo(int nModuleID, int nTrayIndex = 0);
	CString GetLotNo(int nModuleID, int nTrayIndex = 0);
	BOOL SaveRealTimeData(int nModuleID);
	int ShowRunInformation(int nModuleID, int nGroupIndex = 0, BOOL bNewFileName = TRUE, long lProcPK = 0);
	int GetStepCount(int nModuleID, int nCurGroup = 0);
	CString ChCodeMsg(unsigned short code,  BOOL bDetail = FALSE);
	UINT GetStateImgIndex(WORD state);
	CString GetTestName(int nModuleID, int nGroupIndex = 0);
	CString GetResultFileName(int nModuleID, int nTrayIndex = 0);

	void DrawTree(SECTreeCtrl *pTreeX, int nSelModuleID = -1, BOOL bShowGroup = TRUE);
	BOOL SendConditionToModule(int nModuleID, int nGroupIndex, CTestCondition *lpProc);
	BOOL SendInitCommand(int nModuleID, int nGroupIndex = 0);
	BOOL SendContinueCommand(int nModuleID, int nGroupIndex = 0);
	BOOL SendReStartCommand(int nModuleID, int nGroupIndex = 0);
	BOOL SaveResultData(int nModuleID, int nTrayIndex, int nStepIndex, LPVOID	lpData, STR_STEP_RESULT &rStepResult /*, EP_STEP_SUMMERY *pStepSum = NULL*/);
	BOOL SaveCellChkResultData(int nModuleID, int nTrayIndex, int nStepIndex, LPVOID	lpData, STR_STEP_RESULT &rStepResult /*, EP_STEP_SUMMERY *pStepSum = NULL*/);
	bool Fun_UpdatePrecisionData( int nModuleID );
	
	BOOL SaveToProductsMDB(int nModuleID, int nTrayIndex, int nStepIndex, EP_CH_DATA *lpData, CTray *pTrayInfo /*, EP_STEP_SUMMERY *pStepSum = NULL*/);
	bool SaveEmgToProductsMDB(int nModuleID, int nOperationMode, CString nFaultCode, CString strMessage, CString strTrayNo, int nType );	// Stage, (local/auto), code, decription
	int  LoadEmgFromProductsMDB();
	int	 DeleteEmgFromProductsMDB( int nLogIndex );
	STR_TOP_CONFIG * GetTopConfig();
	STR_TEMP_CONFIG * GetTempConfig();
	STR_TEMPSCALE_CONFIG * GetTempScaleConfig();
	BOOL m_bProgressInit;
	CString m_strTempFolder;
	CString m_strDataFolder;
	CString m_strOnlineDataPath;
	CString m_strDataBaseName;
	CString m_strLogDataBaseName;
	CString m_strDBFolder;
	CString m_strCurFolder;
	char m_szLogFileName[MAX_PATH];
	BOOL	m_bRunCmd;			//TimerID = 1000	//사용자가 같은 Cmd를 연속 해서 전송하는 것을 방지하기 위한 Flag
	BOOL	m_bPauseCmd;		//TimerID = 1001
	BOOL	m_bContinueCmd;		//TimerID = 1002
	BOOL	m_bStopCmd;			//TimerID = 1003
	BOOL	m_bInitCmd;			//TimerID = 1004
	BOOL	m_bNextStepCmd;
	
	CString m_strGroupName;
	CString m_strModuleName;
	
	long	m_lSystemID;
	BOOL	LoadSetting();	
	int		GetInstalledModuleNum();
	CString GetStateMsg(WORD State, BYTE &colorFlag);
	CString GetAutoStateMsg(WORD State, BYTE &colorFlag);

//	BOOL	RemoveCondition(STR_CONDITION *pCondition);
	void	InitProgressWnd();
	void	SetProgressWnd(UINT nMin, UINT nMax, CString m_strTitle);
	void	SetProgressPos(int nPos);
	void	HideProgressWnd();

	void WriteLog(char *szLog);

	void InitTree(SECTreeCtrl *pTreeX, UINT nResourceID, CWnd *pWnd);
	void UpdateGetdispinfoTree(TV_DISPINFO* pTVDispInfo, SECTreeCtrl *pTreeCtrlX);
	int LoadInstalledModuleNum();
	BOOL m_Initialized;
	virtual ~CCTSMonDoc();

	void SendAutoProcessOn(CWordArray& awSelModuleIDs, BOOL bOn = TRUE);
//	void SendAutoProcessOff(CRowColArray& awSelModuleIDs);
	BOOL SendSetSaveIntervalCommand(int nSelModuleID);

	void OnReceiveModuleShutdownCmd(int nModuleID, int nReceiveData);
	BOOL IsExeReservedTestTray(int nModuleID);
	int CheckSameProcAndFindNextProc(int nModuleID, int nJigNo = 0, CString strTrayNo = "");

	//  [7/9/2009 kky ]
	// for File처리함수
	UINT file_SummaryDataWrite( char* szFileName, char* szData, int nMode );	

	VOID file_addWrite(CString filename,CString data);
	BOOL file_Finder(CString str_path);
	BOOL ForceDirectory(LPCTSTR lpDirectory);
	CString GetFilePath(LPCTSTR lpszFilePath);
	int FileExists(LPCTSTR lpszName);
	BOOL DirectoryExists(LPCTSTR lpszDir);
	
	BOOL m_bUseFaultAlarm;
	bool SaveBfSensorMap(); 

	//CPtrArray GetModuleArray(){return m_apModuleInfo;}

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	CPtrArray m_apModuleInfo;	//Array of CFormModule
protected:

//	CFormModule	*m_pModule;
	int LoadSystemParam();
	BOOL LoadCodeTable();
	BOOL RequeryProcType();
	bool LoadBfSensorMap();	
	CPtrArray m_apChCodeMsg;
	CProgressWnd *m_pProgressWnd;
	CImageList	*m_imagelist;
	void InitImgList();
	int m_nInstalledModuleNum;
//	char *m_pszFileBuff;

// Generated message map functions
protected:
	//{{AFX_MSG(CCTSMonDoc)
//	afx_msg void OnCalibrationSet();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnTypeSettingRun();
	afx_msg void OnInspectedResultRun();		
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CTSMonDOC_H__22C12B1F_B537_4E37_A549_C83AAFFD8BF9__INCLUDED_)
