#if !defined(AFX_DEVIATIONGRAPHWND_H__84472FB5_D85C_4C38_9C1E_EFF823A62015__INCLUDED_)
#define AFX_DEVIATIONGRAPHWND_H__84472FB5_D85C_4C38_9C1E_EFF823A62015__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DeviationGraphWnd.h : header file
//
#include "PEGRPAPI.h"
#include "pemessag.h"
#include "Mydefine.h"

/////////////////////////////////////////////////////////////////////////////
// CDeviationGraphWnd window

class CDeviationGraphWnd : public CStatic
{
// Construction
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	CDeviationGraphWnd();
	virtual ~CDeviationGraphWnd();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDeviationGraphWnd)
	protected:
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

// Implementation
public:
	void SetXAxisLabel(CString strName);
	void SaveFile();
	void PrintData();
	BOOL SetLimitVal(double dHigh, double dLow);
	BOOL AddXAnnotation(double dPoint, CString strString, COLORREF color = RGB(255, 0, 0), int nLineType = PELT_THINSOLID);
	BOOL SetTitle(CString main, CString subTitle = "");
	BOOL SetDivision(double dDivision);
	BOOL SetDataRange(double dMin, double dMax);
	BOOL m_bUseAutoYScale;
	BOOL ProcessData();
	double *m_pData;
	BOOL SetData(int nSize, double *data= NULL);
	BOOL InitGraph();
	
	
	double       GetMean() { return m_dAvg; }
	double       GetDeviation() { return m_dDev; }
	double       GetDataDivision(){ return m_dDivision;}
	double       GetMinData(){ return m_dMin;}
	double       GetMaxData(){ return m_dMax;}
	
	// Generated message map functions
protected:
	int		m_nAnnoCount;
	double	m_dVLA[MAX_ADD_ANNO_NO];							//Point
	int		m_nVLAT[MAX_ADD_ANNO_NO];							//Line Style
	long	m_lVLAC[MAX_ADD_ANNO_NO];							//Line Color
	CString m_strVLAT;	//String 
	char	m_szVLAT[MAX_ADD_ANNO_NO][48];

	double	m_dDivision;
	ULONG	m_ulDataCount;
	HWND	m_hWndPE;
	double	m_dDev;
	double	m_dAvg;
	double	m_dMin;
	double	m_dMax;
	ULONG	m_ulMaxIndex;
	ULONG	m_ulMinIndex;

	double m_dHighLimit;
	double m_dLowLimit;

	double m_dRangeMin;
	double m_dRangeMax;
	BOOL	m_bAutoRagne;
	
	double	m_dYAxisMinMax[2][2];			//사용자가 설정한 값
	double  m_dYDataMinMax[2][2];
	//{{AFX_MSG(CDeviationGraphWnd)
	afx_msg void OnPaint();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEVIATIONGRAPHWND_H__84472FB5_D85C_4C38_9C1E_EFF823A62015__INCLUDED_)
