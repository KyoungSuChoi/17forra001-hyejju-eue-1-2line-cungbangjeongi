#pragma once


// CClampCountChkDlg 대화 상자입니다.
#include "stdafx.h"
#include "MyGridWnd.h"
#include "CTSMonDoc.h"
#include "afxcmn.h"
#include "afxwin.h"

#define GRID_DATA_FONT_SIZE	16

class CClampCountChkDlg : public CDialog
{
	DECLARE_DYNAMIC(CClampCountChkDlg)

public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	CClampCountChkDlg(CCTSMonDoc *pDoc, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CClampCountChkDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_CLAMP_CNTCHK_DLG };

	CMyGridWnd	m_wndChGrid;
	CCTSMonDoc	*m_pDoc;
	CFont		m_Font;
	
	CLabel	m_LabelPrecisionName;	
	CString m_strModuleName;
	int m_nMaxStageCnt;	
	int m_nModuleID;	
	
	void InitGridWnd();	
	void ReDrawGrid();
	void InitFont();
	void InitLabel();
	void InitColorBtn();	
	void fnUpdateCnt();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtnUpdate();
	afx_msg void OnBnClickedOk();	
	virtual BOOL OnInitDialog();
	CLabel m_LabelName;
	CColorButton2 m_BtnUpdate;
	CColorButton2 m_BtnClose;
	CColorButton2 m_BtnCSVConvert;
	afx_msg void OnBnClickedCsvOutput();
	afx_msg void OnBnClickedSelectChannelInitBtn();
	CComboBox m_ctrlMDSelCombo;
	afx_msg void OnCbnSelchangeUnitSelCombo();
	CColorButton2 m_BtnSelectAllOk;
	CColorButton2 m_BtnSelectAllOff;
	CColorButton2 m_BtnSelectChannelInit;
	afx_msg void OnBnClickedSelectAllOkBtn();
	afx_msg void OnBnClickedSelectAllOffBtn();
};
