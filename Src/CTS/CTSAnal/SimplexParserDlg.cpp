// SimplexParserDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "SimplexParserDlg.h"
#include "FormelParser.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSimplexParserDlg dialog


CSimplexParserDlg::CSimplexParserDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSimplexParserDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSimplexParserDlg)
	m_strFormulainput = _T("2+3");
	m_strFormulaoutput = _T("");
	m_dKonstB = 1.0;
	m_dKonstF = 1.0;
	m_dKonstG = 1.0;
	m_dKonstH = 1.0;
	m_dKonstJ = 1.0;
	m_dKonstK = 1.0;
	m_dKonstM = 1.0;
	m_dKonstN = 3.14159265358979;
	m_dKonstU = 1.0;
	m_dKonstX = 0.0123;
	//}}AFX_DATA_INIT
	m_strFormulainput_vorher = m_strFormulainput;
	m_strFileName = "Test.fkt";
}

void CSimplexParserDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSimplexParserDlg)
	DDX_Control(pDX, IDC_COMBO_X, m_Field10);
	DDX_Control(pDX, IDC_COMBO_U, m_Field9);
	DDX_Control(pDX, IDC_COMBO_N, m_Field8);
	DDX_Control(pDX, IDC_COMBO_M, m_Field7);
	DDX_Control(pDX, IDC_COMBO_K, m_Field6);
	DDX_Control(pDX, IDC_COMBO_J, m_Field5);
	DDX_Control(pDX, IDC_COMBO_H, m_Field4);
	DDX_Control(pDX, IDC_COMBO_G, m_Field3);
	DDX_Control(pDX, IDC_COMBO_F, m_Field2);
	DDX_Control(pDX, IDC_COMBO_B, m_Field1);
	DDX_Text(pDX, IDC_FORMELEINGABE, m_strFormulainput);
	DDX_Text(pDX, IDC_FORMELAUSGABE, m_strFormulaoutput);
	DDX_Text(pDX, IDC_EDIT_B, m_dKonstB);
	DDX_Text(pDX, IDC_EDIT_F, m_dKonstF);
	DDX_Text(pDX, IDC_EDIT_G, m_dKonstG);
	DDX_Text(pDX, IDC_EDIT_H, m_dKonstH);
	DDX_Text(pDX, IDC_EDIT_J, m_dKonstJ);
	DDX_Text(pDX, IDC_EDIT_K, m_dKonstK);
	DDX_Text(pDX, IDC_EDIT_M, m_dKonstM);
	DDX_Text(pDX, IDC_EDIT_N, m_dKonstN);
	DDX_Text(pDX, IDC_EDIT_U, m_dKonstU);
	DDX_Text(pDX, IDC_EDIT_X, m_dKonstX);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSimplexParserDlg, CDialog)
	//{{AFX_MSG_MAP(CSimplexParserDlg)
	ON_BN_CLICKED(IDC_ENTER, OnEnter)
	ON_BN_CLICKED(IDC_SIN, OnSin)
	ON_BN_CLICKED(IDC_PLUS, OnPlus)
	ON_BN_CLICKED(IDC_MINUS, OnMinus)
	ON_BN_CLICKED(IDC_MAL, OnMal)
	ON_BN_CLICKED(IDC_GETEILT, OnGeteilt)
	ON_BN_CLICKED(IDC_HOCH, OnHoch)
	ON_BN_CLICKED(IDC_PKT, OnPkt)
	ON_BN_CLICKED(IDC_KLAMMER_AUF, OnKlammerAuf)
	ON_BN_CLICKED(IDC_KLAMMER_ZU, OnKlammerZu)
	ON_BN_CLICKED(IDC_ARCSIN, OnArcsin)
	ON_BN_CLICKED(IDC_SINH, OnSinh)
	ON_BN_CLICKED(IDC_ARSINH, OnArsinh)
	ON_BN_CLICKED(IDC_COS, OnCos)
	ON_BN_CLICKED(IDC_ARCCOS, OnArccos)
	ON_BN_CLICKED(IDC_COSH, OnCosh)
	ON_BN_CLICKED(IDC_ARCOSH, OnArcosh)
	ON_BN_CLICKED(IDC_TAN, OnTan)
	ON_BN_CLICKED(IDC_ARCTAN, OnArctan)
	ON_BN_CLICKED(IDC_TANH, OnTanh)
	ON_BN_CLICKED(IDC_ARTANH, OnArtanh)
	ON_BN_CLICKED(IDC_EXP, OnExp)
	ON_BN_CLICKED(IDC_LN, OnLn)
	ON_BN_CLICKED(IDC_INT, OnInt)
	ON_BN_CLICKED(IDC_ABS, OnAbs)
	ON_BN_CLICKED(IDC_10X, On10x)
	ON_BN_CLICKED(IDC_LOG, OnLog)
	ON_BN_CLICKED(IDC_RAD, OnRad)
	ON_BN_CLICKED(IDC_DEG, OnDeg)
	ON_BN_CLICKED(IDC_X2, OnX2)
	ON_BN_CLICKED(IDC_SQR, OnSqr)
	ON_BN_CLICKED(IDC_X, OnX)
	ON_BN_CLICKED(IDC_DELETE, OnDelete)
	ON_BN_CLICKED(IDC_UNDO, OnUndo)
	ON_BN_CLICKED(IDC_B, OnB)
	ON_BN_CLICKED(IDC_F, OnF)
	ON_BN_CLICKED(IDC_G, OnG)
	ON_BN_CLICKED(IDC_H, OnH)
	ON_BN_CLICKED(IDC_J, OnJ)
	ON_BN_CLICKED(IDC_K, OnK)
	ON_BN_CLICKED(IDC_M, OnM)
	ON_BN_CLICKED(IDC_N, OnN)
	ON_BN_CLICKED(IDC_U, OnU)
	ON_BN_CLICKED(IDC_XB, OnXb)
	ON_BN_CLICKED(IDC_SAVE, OnSaveParameter)
	ON_BN_CLICKED(IDC_LOAD, OnLoadParameter)
	//}}AFX_MSG_MAP
	ON_COMMAND_RANGE(IDC_0, IDC_9, OnClickedNumber)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSimplexParserDlg message handlers

//Klicks auf Buttons auswerten
void CSimplexParserDlg::OnSin()			{	SetEingabe("sin("); }
void CSimplexParserDlg::OnPlus()		{	SetEingabe("+"); }
void CSimplexParserDlg::OnMinus()  	{	SetEingabe("-"); }
void CSimplexParserDlg::OnMal() 		{	SetEingabe("*"); }
void CSimplexParserDlg::OnGeteilt() {	SetEingabe("/"); }
void CSimplexParserDlg::OnHoch() 		{	SetEingabe("^"); }
void CSimplexParserDlg::OnPkt() 		{	SetEingabe("."); }
void CSimplexParserDlg::OnKlammerAuf() 	{	SetEingabe("("); }
void CSimplexParserDlg::OnKlammerZu() 	{	SetEingabe(")"); }
void CSimplexParserDlg::OnArcsin() 	{	SetEingabe("arcsin("); }
void CSimplexParserDlg::OnSinh() 		{	SetEingabe("sinh("); }
void CSimplexParserDlg::OnArsinh() 	{	SetEingabe("arsinh("); }
void CSimplexParserDlg::OnCos() 		{	SetEingabe("cos("); }
void CSimplexParserDlg::OnArccos() 	{	SetEingabe("arccos("); }
void CSimplexParserDlg::OnCosh() 		{	SetEingabe("cosh("); }
void CSimplexParserDlg::OnArcosh() 	{	SetEingabe("arcosh("); }
void CSimplexParserDlg::OnTan() 		{	SetEingabe("tan("); }
void CSimplexParserDlg::OnArctan() 	{	SetEingabe("arctan("); }
void CSimplexParserDlg::OnTanh()		{	SetEingabe("tanh("); }
void CSimplexParserDlg::OnArtanh() 	{	SetEingabe("artanh("); }
void CSimplexParserDlg::OnExp() 		{	SetEingabe("exp("); }
void CSimplexParserDlg::OnLn() 			{	SetEingabe("ln("); }
void CSimplexParserDlg::OnInt() 		{	SetEingabe("int("); }
void CSimplexParserDlg::OnAbs()			{	SetEingabe("abs("); }
void CSimplexParserDlg::On10x() 		{	SetEingabe("10^"); }
void CSimplexParserDlg::OnLog() 		{	SetEingabe("log("); }
void CSimplexParserDlg::OnRad() 		{	SetEingabe("rad("); }
void CSimplexParserDlg::OnDeg() 		{	SetEingabe("deg("); }
void CSimplexParserDlg::OnX2() 			{	SetEingabe("^2"); }
void CSimplexParserDlg::OnSqr() 		{	SetEingabe("sqr("); }
void CSimplexParserDlg::OnX()  			{	SetEingabe("x"); }
void CSimplexParserDlg::OnB()  			{	SetEingabe("B"); } 
void CSimplexParserDlg::OnF()  			{	SetEingabe("F"); } 
void CSimplexParserDlg::OnG()  			{	SetEingabe("G"); } 
void CSimplexParserDlg::OnH()  			{	SetEingabe("H"); } 
void CSimplexParserDlg::OnJ()  			{	SetEingabe("J"); } 
void CSimplexParserDlg::OnK()  			{	SetEingabe("K"); } 
void CSimplexParserDlg::OnM()  			{	SetEingabe("M"); } 
void CSimplexParserDlg::OnN()  			{	SetEingabe("N"); } 
void CSimplexParserDlg::OnU()  			{	SetEingabe("U"); } 
void CSimplexParserDlg::OnXb()  		{	SetEingabe("X"); } 

void CSimplexParserDlg::OnClickedNumber(UINT nID) 
{
	ASSERT(nID >= IDC_0 && nID <= IDC_9);
	char buffer[20];   
	_itoa(nID - IDC_0, buffer, 10);
	SetEingabe(buffer);
}

void CSimplexParserDlg::SetEingabe(CString str)
{
	UpdateData(true);
	m_strFormulainput_vorher = m_strFormulainput;
	m_strFormulainput += str;
	UpdateData(false);

	CWnd* pWnd = GetDlgItem(IDC_FORMELEINGABE);
	pWnd->SetFocus();
	pWnd->SendMessage(EM_SETSEL,32767,32767); //Cursor ans Ende
}

void CSimplexParserDlg::OnEnter() 
{
	WORD ErrorPosition;
	char buffer[64];
	CString Errortext;
	Errortext.Empty();

	UpdateData(true);

	//Object definieren
	CFormulaParser FormulaParser;

/*	FormulaParser.SetFunctConst(1, m_dKonstB);
	FormulaParser.SetFunctConst(2, m_dKonstF);
	FormulaParser.SetFunctConst(3, m_dKonstG);
	FormulaParser.SetFunctConst(4, m_dKonstH);
	FormulaParser.SetFunctConst(5, m_dKonstJ);
	FormulaParser.SetFunctConst(6, m_dKonstK);
	FormulaParser.SetFunctConst(7, m_dKonstM);
	FormulaParser.SetFunctConst(8, m_dKonstN);
	FormulaParser.SetFunctConst(9, m_dKonstU);
*/
	//필드 항목을 숫자로 대치하여 계산식이 맞는지 검사  
	FormulaParser.SetFunctConst(1, m_dKonstB);
	FormulaParser.SetFunctConst(2, m_dKonstF);
	FormulaParser.SetFunctConst(3, m_dKonstG);
	FormulaParser.SetFunctConst(4, m_dKonstH);
	FormulaParser.SetFunctConst(5, m_dKonstJ);
	FormulaParser.SetFunctConst(6, m_dKonstK);
	FormulaParser.SetFunctConst(7, m_dKonstM);
	FormulaParser.SetFunctConst(8, m_dKonstN);
	FormulaParser.SetFunctConst(9, m_dKonstU);


	//Methode aufrufen
	double retValue = FormulaParser.Calculation(m_strFormulainput, m_dKonstX, ErrorPosition, Errortext);
	
	CWnd* pWnd = GetDlgItem(IDC_FORMELEINGABE);
	pWnd->SetFocus();
  if (ErrorPosition == 0)
	{
    if (Errortext == "")
		{
			sprintf(buffer, "%f", retValue);
			m_strFormulaoutput = 	buffer;
		}
		else
			m_strFormulaoutput = 	"BerechnungsError: " + Errortext;
		pWnd->SendMessage(EM_SETSEL,32767,32767); //Cursor ans Ende
	}
  else
	{
		m_strFormulaoutput = 	Errortext;
		pWnd->SendMessage(EM_SETSEL,ErrorPosition - 1,ErrorPosition); //Cursor an Errorposition
	}

	UpdateData(false);
}

void CSimplexParserDlg::OnDelete() 
{
	UpdateData(true);
	m_strFormulainput_vorher = m_strFormulainput;
	m_strFormulainput.Empty();
	UpdateData(false);
}

void CSimplexParserDlg::OnUndo() 
{
	UpdateData(true);
	CString dummy;
	dummy = m_strFormulainput;
	m_strFormulainput = m_strFormulainput_vorher;
	m_strFormulainput_vorher = dummy;
	UpdateData(false);
}

void CSimplexParserDlg::OnSaveParameter() 
{
	UpdateData(true);
	SaveParameter();
}

void CSimplexParserDlg::OnLoadParameter() 
{
	LoadParameter();	
	UpdateData(false);
}

void CSimplexParserDlg::SaveParameter()
{
	BOOL bOpenFileDialog = false;  				//TRUE: <File Open>, FALSE: <File Save As> 
  const char* szDefExt = "FKT";  				//default extension
  const char* szFileName = (const char*) m_strFileName;  //default filename            
  //DWORD style = OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY;   //for load
  DWORD style = OFN_PATHMUSTEXIST | OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT; 	//for save    
  const char* lpszFilter = "Formulaparser (*.FKT)|*.FKT|All Files (*.*)|*.*||";  
	CWnd* pParent = NULL;                                       
    
  CFileDialog dlgFile(bOpenFileDialog,szDefExt,szFileName,style,lpszFilter,pParent);
  
  if (dlgFile.DoModal() != IDOK) return;
  	
	m_strFileName = dlgFile.GetPathName();
	  		
	FILE* stream;
  if ((stream = fopen(m_strFileName, "w" )) == NULL)    
  {
    AfxMessageBox("Can not save the file " + m_strFileName);    
    fclose(stream); 
    return;
  }
    
	CString m_strComment("no comment");
  /////////////////////////////////////////////////////////////////////////
  fputs("SimplexNumerica Formulaparser\n",stream);
  fputs("\n",stream);
  
  fputs("\n[COMMENT]\n",stream);
  fputs((LPCSTR) (m_strComment + "\n"),stream);

  fputs("\n[FORMEL]\n",stream);
  fputs((LPCSTR) (m_strFormulainput + "\n"),stream);

  fputs("\n[CONSTANTS]\n",stream);
	UINT anz = ANZFUNKTKONST;
	fprintf(stream, "%u\n", anz);
/*
	for (UINT i = 0; i < anz; i++)
	{
		fputs((LPCSTR) (m_pPropSheet->m_pPage1->m_strArray[i] + "\n"),stream);
	}
*/
  fprintf(stream, "%f\n", m_dKonstB);
  fprintf(stream, "%f\n", m_dKonstF);
  fprintf(stream, "%f\n", m_dKonstG);
  fprintf(stream, "%f\n", m_dKonstH);
  fprintf(stream, "%f\n", m_dKonstJ);
  fprintf(stream, "%f\n", m_dKonstK);
  fprintf(stream, "%f\n", m_dKonstM);
  fprintf(stream, "%f\n", m_dKonstN);
  fprintf(stream, "%f\n", m_dKonstU);
  fprintf(stream, "%f\n", m_dKonstX);



  fputs("\n[COMMAND]\n",stream);	

  fputs("\n",stream); 
  fputs("[END]\n",stream);                       	

	fclose(stream); 	 

  /////////////////////////////////////////////////////////////////////////

	//Update mainframe window caption.
	CString str;
	str.Format(_T("Formulaparser - %s"),m_strFileName);
	SetWindowText(str);
}

void CSimplexParserDlg::LoadParameter() 
{
	BOOL bOpenFileDialog = true;  				//TRUE: <File Open>, FALSE: <File Save As> 
  const char* szDefExt = "FKT";  				//default extension
  const char* szFileName = (const char*) m_strFileName;  //default filename            
  DWORD style = OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY;   //for load
  //DWORD style = OFN_PATHMUSTEXIST | OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT; 	//for save    
  const char* lpszFilter = "Formulaparser (*.FKT)|*.FKT|All Files (*.*)|*.*||";  
  CWnd* pParent = NULL;                                       
    
  CFileDialog dlgFile(bOpenFileDialog,szDefExt,szFileName,style,lpszFilter,pParent);
  
  if (dlgFile.DoModal() != IDOK) return;
  	
	m_strFileName = dlgFile.GetPathName();
	CString FileName = m_strFileName.GetBuffer(128);
		
	FILE* stream;
  if ((stream = fopen(FileName, "r" )) == NULL)    
  {
    AfxMessageBox("Can not open the file " + FileName);    
    fclose(stream); 
    return;   
  }

	char buffer[256];       
	//////////////////////////////////////////////////////
  //fputs("SimplexNumerica Formulaparser\n",stream);
  if (fgets( buffer, 256, stream ) == NULL)
	{
		TRACE("fgets error\n");
    fclose(stream); 
    return;   
	}
	else
		//TRACE("%s\n", buffer);
                 
 
	//fputs("\n[FORMEL]\n",stream);
  m_strFormulainput.Empty(); 
  while (fgets(buffer, 256, stream) != NULL)
  {
		TRACE("%s\n", buffer);		
		if (strcmp(buffer,"[FORMEL]\n") == 0) break; //then equal
  }
	if (fgets(buffer, 128, stream) != NULL)
	{      
		m_strFormulainput = buffer;  
		m_strFormulainput = m_strFormulainput.Left(m_strFormulainput.GetLength() - 1); //Remove CR
	}  
	TRACE("Formula: %s\n",m_strFormulainput);

//	UINT anz;
//	fscanf(stream, "%u\n", &anz);
}



BOOL CSimplexParserDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CDialog::OnInitDialog();
	
	ASSERT(m_apProcType);
	// TODO: Add extra initialization here
	STR_MSG_DATA *pObject;
//	int nIndex = 0;			//Display 할 Index
	int nComboIndex = 0;			//Combo Add Index

	for(int i =0; i<m_apProcType->GetSize(); i++)
	{
		pObject = (STR_MSG_DATA *)m_apProcType->GetAt(i);
		if( pObject->nCode >= EP_PROC_TYPE_CHARGE_LOW)
		{
			m_Field1.AddString(pObject->szMessage);
			m_Field1.SetItemData(nComboIndex, i);				//Index를 Data로 지정 

			m_Field2.AddString(pObject->szMessage);
			m_Field2.SetItemData(nComboIndex, i);				//Index를 Data로 지정 

			m_Field3.AddString(pObject->szMessage);
			m_Field3.SetItemData(nComboIndex, i);				//Index를 Data로 지정 
			
			m_Field4.AddString(pObject->szMessage);
			m_Field4.SetItemData(nComboIndex, i);				//Index를 Data로 지정 
			
			m_Field5.AddString(pObject->szMessage);
			m_Field5.SetItemData(nComboIndex, i);				//Index를 Data로 지정 
			
			m_Field6.AddString(pObject->szMessage);
			m_Field6.SetItemData(nComboIndex, i);				//Index를 Data로 지정 
			
			m_Field7.AddString(pObject->szMessage);
			m_Field7.SetItemData(nComboIndex, i);				//Index를 Data로 지정 
			
			m_Field8.AddString(pObject->szMessage);
			m_Field8.SetItemData(nComboIndex, i);				//Index를 Data로 지정 
			
			m_Field9.AddString(pObject->szMessage);
			m_Field9.SetItemData(nComboIndex, i);				//Index를 Data로 지정 
			
			m_Field10.AddString(pObject->szMessage);
			m_Field10.SetItemData(nComboIndex, i);				//Index를 Data로 지정 
			//Diaply할 항목 검사 
//			if(strcmp(m_ProcType.szMessage, pObject->szMessage) == 0)	nIndex = nComboIndex;

			nComboIndex++;
		}
	}
		
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
