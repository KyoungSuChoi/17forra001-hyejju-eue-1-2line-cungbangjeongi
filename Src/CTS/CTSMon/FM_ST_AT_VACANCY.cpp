#include "stdafx.h"

#include "CTSMon.h"

#include "FMS.h"
#include "FM_Unit.h"

#include "FM_STATE.h"

CFM_ST_AT_VACANCY::CFM_ST_AT_VACANCY(FM_STATUS_ID _Eqstid, FM_STATUS_ID _stid, CFM_Unit* _unit)
:CFM_ST_AUTO(_Eqstid, _stid, _unit)
{
	
}


CFM_ST_AT_VACANCY::~CFM_ST_AT_VACANCY(void)
{
}

VOID CFM_ST_AT_VACANCY::fnEnter()
{
	fnSetFMSStateCode(FMS_ST_VACANCY);
	
	// 1. Automatic 모드에서 10분동안 Vacancy 상태가 지속될 경우 미전송 파일을 확인한다.
	// CHANGE_FNID(FNID_PROC);
	
	//if(m_Unit->fnGetFMS()->fnGetSendMisState()) 
	//{
	//	//load
	//	if(m_Unit->fnGetFMS()->fnSendMisProc())
	//		CHANGE_STATE(AUTO_ST_END);
	//}
	//else
	//{
	//	
	//}
	//TRACE("CFM_ST_AT_VACANCY::fnEnter %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_AT_VACANCY::fnProc()
{
	/*
	if( 1 == m_iWaitCount )
	{
		AfxMessageBox("ProcTest");	
		CHANGE_FNID(FNID_EXIT);	
	}
	
	
	m_RetryMap |= RETRY_NOMAL_ON;
	*/

	TRACE("CFM_ST_AT_VACANCY::fnProc %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_AT_VACANCY::fnExit()
{
	/*
	if( 1 == m_iWaitCount )
	{
		AfxMessageBox("EndTest");	
		// CHANGE_FNID(FNID_EXIT);	
	}
	*/
	
	TRACE("CFM_ST_AT_VACANCY::fnExit %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_AT_VACANCY::fnSBCPorcess(WORD _state)
{
	CFM_ST_AUTO::fnSBCPorcess(_state);

	if(m_Unit->fnGetFMS()->fnGetError())
	{
		CHANGE_STATE(AUTO_ST_ERROR);
	}
        
	//TRACE("%s \n", g_strSBCState[m_Unit->fnGetModule()->GetState()]);
	switch(m_Unit->fnGetModule()->GetState())
	{
	case EP_STATE_IDLE:
		if(m_Unit->fnGetModuleTrayState())
		{
			CHANGE_STATE(AUTO_ST_TRAY_IN);
		}
		break;
	case EP_STATE_STANDBY:
		{
			CHANGE_STATE(AUTO_ST_TRAY_IN);
		}
		break;
	case EP_STATE_PAUSE:
		{
			CHANGE_STATE(AUTO_ST_ERROR);
		}
		break;
	case EP_STATE_RUN:
		{
			if(m_Unit->fnGetModuleCheckState())
			{
				CHANGE_STATE(AUTO_ST_CONTACT_CHECK);
			}
			else
			{
				CHANGE_STATE(AUTO_ST_RUN);
			}
		}
		break;
	case EP_STATE_READY:
		{
			CHANGE_STATE(AUTO_ST_READY);
		}
		break;
	case EP_STATE_END:
		{
			CHANGE_STATE(AUTO_ST_END);
		}
		break;
	case EP_STATE_ERROR:
		{
			CHANGE_STATE(AUTO_ST_ERROR);
		}
		break;
	}
	//TRACE("CFM_ST_AT_VACANCY::fnSBCPorcess %d \n", m_Unit->fnGetModuleIdx());
}

FMS_ERRORCODE CFM_ST_AT_VACANCY::fnFMSPorcess(FMS_COMMAND _msgId, st_FMS_PACKET* _recvData)
{
	FMS_ERRORCODE _error = ER_NO_Process;

	//예약설정
	switch(_msgId)
	{	
	case E_CHARGER_INRESEVE:
		{
			if(fnGetProcID() == FNID_ENTER)
			{
				//20210225 ksj
				ULONGLONG ulStartTime =  GetTickCount64();
				_error = m_Unit->fnGetFMS()->fnReserveProc(_recvData->inreservCode);
				int iElapsedTime = GetTickCount64() - ulStartTime;

				if(iElapsedTime > 5000)
				{ 
					CFMSLog::WriteErrorLog("[ModuleIdx : %s] fnReserveProc Elapsed Time Is Over. Elapsed Time %d ms",  m_Unit->fnGetModuleIdx(), iElapsedTime);
					_error = ER_SBC_NetworkError;

					EPSendCommand(m_Unit->fnGetModuleID(), 0, 0, EP_CMD_CLEAR);

					m_Unit->fnGetModule()->GetTrayInfo(0)->m_nResultFileType = EP_RESULTFILE_TYPE_NORMAL;	
					m_Unit->fnGetModule()->UpdateTestLogHeaderTempFile();			
					m_Unit->fnGetModule()->ResetGroupData();
					m_Unit->fnGetModule()->ClearTestLogTempFile();

					EPSendCommand(m_Unit->fnGetModuleID(), 0, 0, EP_CMD_SBC_SOCKET_RECONNECT_REQUEST );
				}

				if(FMS_ER_NONE == _error)
				{
					CHANGE_FNID(FNID_PROC);
				}
			}
		}		
		break;
		
	// 1. 미전송 관련 메세지 추가
	default:
		{
		}
		break;
	}

	TRACE("CFM_ST_AT_VACANCY::fnFMSPorcess %d ErrorCode %d \n", m_Unit->fnGetModuleIdx(), _error );

	return _error;
}