#pragma once

template <class T>
class CFMSSyncParent
{
	friend class CFMSSyncObj;
public:
	class CFMSSyncObj
	{
	private:
		const CFMSSyncParent &m_rOwner;

	public:
		CFMSSyncObj(const CFMSSyncParent &rOwner) : m_rOwner(rOwner) 
		{
			m_rOwner.m_csSyncObj.Enter();
		}
		~CFMSSyncObj(VOID) 
		{
			m_rOwner.m_csSyncObj.Leave();
		}
	};

private:
	mutable CFMSCriticalSection m_csSyncObj;
};

#define FMSSync FMSSync(*this)