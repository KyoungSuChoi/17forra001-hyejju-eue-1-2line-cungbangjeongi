// BatteryModelRecordSet.cpp : implementation file
//

#include "stdafx.h"
#include "BatteryModelRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBatteryModelRecordSet

IMPLEMENT_DYNAMIC(CBatteryModelRecordSet, CRecordset)

CBatteryModelRecordSet::CBatteryModelRecordSet(CDatabase* pdb)
	: CRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CBatteryModelRecordSet)
	m_ModelID = 0;
	m_No = 0;
	m_ModelName = _T("");
	m_Description = _T("");
	m_CreatedTime = CTime::GetCurrentTime();
	m_nFields = 5;
	//}}AFX_FIELD_INIT
	m_nDefaultType = snapshot;
}


CString CBatteryModelRecordSet::GetDefaultConnect()
{
	return _T("ODBC;DSN=TestCondition");
}

CString CBatteryModelRecordSet::GetDefaultSQL()
{
	return _T("[BatteryModel]");
}

void CBatteryModelRecordSet::DoFieldExchange(CFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CBatteryModelRecordSet)
	pFX->SetFieldType(CFieldExchange::outputColumn);
	RFX_Long(pFX, _T("[ModelID]"), m_ModelID);
	RFX_Long(pFX, _T("[No]"), m_No);
	RFX_Text(pFX, _T("[ModelName]"), m_ModelName);
	RFX_Text(pFX, _T("[Description]"), m_Description);
	RFX_Date(pFX, _T("[CreatedTime]"), m_CreatedTime);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CBatteryModelRecordSet diagnostics

#ifdef _DEBUG
void CBatteryModelRecordSet::AssertValid() const
{
	CRecordset::AssertValid();
}

void CBatteryModelRecordSet::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG
