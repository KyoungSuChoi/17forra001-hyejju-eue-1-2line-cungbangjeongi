///////SOCKET_SERVER_DLL_API_HEADER_FILE///////
//											//	
//	Server.h								//	
//	Softech Charge/Discharge System API		//	
//  Define of Network Data
//	Byung Hum - Kim		2005.5.24			//
//											//
//////////////////////////////////////////////

#ifndef _SFT_SCHEDULER_MSG_DEFINE_H_
#define _SFT_SCHEDULER_MSG_DEFINE_H_


//Version Define
#define _SFT_PROTOCOL_VERSIONV1			0x1000		//Message Protocol Version
#define _SFT_PROTOCOL_VERSIONV2			0x1001		//SFT_CH_DATA에서 	//unsigned short int	nTotalCycleNum;	
																		//unsigned short int	nCurrentCycleNum;	
													//을 unsinged long으로 변경함 
#define _SFT_PROTOCOL_VERSIONV3			0x1002		//SFT_CMD_SAFETY_DATA command 추가	2006/3/24
#define _SFT_PROTOCOL_VERSIONV4			0x1003		//2006/6/8
#define _SFT_PROTOCOL_VERSIONV5			0x1004		//2007/11/06
#define _SFT_PROTOCOL_VERSION			0x1005		//2008/01/28 EndV, EndI, EndC, EndT

//Formation TCPIP Port
#define _SFT_FROM_SCHEDULER_TCPIP_PORT1	2001
#define _SFT_FROM_SCHEDULER_TCPIP_PORT2	2002
#define _SFT_FROM_SCHEDULER_TCPIP_PORT3	2003
#define _SFT_FROM_SCHEDULER_TCPIP_PORT4	2004
#define _SFT_FROM_SCHEDULER_TCPIP_PORT5	2005

//IROCV TCPIP Port
#define _SFT_IROCV_SCHEDULER_TCPIP_PORT1	2051
#define _SFT_IROCV_SCHEDULER_TCPIP_PORT2	2052
#define _SFT_IROCV_SCHEDULER_TCPIP_PORT3	2053
#define _SFT_IROCV_SCHEDULER_TCPIP_PORT4	2054
#define _SFT_IROCV_SCHEDULER_TCPIP_PORT5	2055

//System Max Define 
#define _SFT_MAX_PACKET_LENGTH		256000		//최대 Packet 길이 
#define _SFT_MAX_PACKET_GRADE_STEP	10
#define _SFT_MAX_PACKET_COMP_POINT	3
#define _SFT_MAX_PACKET_SUB_STEP	10
#define _SFT_MAX_PACKET_GRADE_ITEM_IN_STEP	4

#if _SFT_MAX_PACKET_GRADE_STEP < PS_MAX_GRADE_STEP
#pragma message( "------------KBH Message-------------" )
#pragma message( "Max Grade Step define Error" )
#pragma message( "------------------------------------" )
#error Grade step Define Error
#endif

#if _SFT_MAX_PACKET_COMP_POINT < PS_MAX_COMP_POINT
#pragma message( "------------KBH Message-------------" )
#pragma message( "Max comperation point define Error" )
#pragma message( "------------------------------------" )
#error Comp. point Define Error
#endif

#if _SFT_MAX_PACKET_SUB_STEP < PS_MAX_SUB_STEP
#pragma message( "------------KBH Message-------------" )
#pragma message( "Maximum sub step define Error" )
#pragma message( "------------------------------------" )
#error Max sub step Define Error
#endif

//NetWork RX/TX Buffer Size
#define _SFT_TX_BUF_LEN				_SFT_MAX_PACKET_LENGTH
#define _SFT_RX_BUF_LEN				_SFT_MAX_PACKET_LENGTH


//Time define
#define SFT_MSG_TIMEOUT				5000		//Message Read Time Out
#define SFT_HEART_BEAT_INTERVAL		2000		//Heart beat check interval
#define SFT_HEART_BEAT_TIMEOUT		(SFT_HEART_BEAT_INTERVAL+5000)		//heart beat time out
#define _SFT_MAX_WAIT_COUNT			2


//Data Size Define
#define _SFT_DATE_TIME_LENGTH			32

// SFT_CMD_CH_DATA 의 data 종류 
#define	SFT_SAVE_REPORT			0x00		//Monitoring용 Data
#define	SFT_SAVE_STEP_END		0x01		//Step End 저장용 data
#define SFT_SAVE_DELTA_TIME		0x02
#define SFT_SAVE_DELTA_V		0x03
#define SFT_SAVE_DELTA_I		0x04
#define SFT_SAVE_DELTA_T		0x05
#define SFT_SAVE_DELTA_P		0x06
#define SFT_SAVE_ETC			0x07

#define MAX_PATTERN_ROW_COUNT 20000

//////////////////////////////////////////////////////////////////////////
//Message structure Define
//////////////////////////////////////////////////////////////////////////

// SFT Server Message Header
typedef struct s_CMD_HEADER
{	
	unsigned long	nCommand;			// Packet Command 종류	(예 : 이 패킷은 컨트롤 패킷이다)
	unsigned long	lCmdSerial;			// Command Serial Number..(Command ID)
	unsigned short	wCmdType;			// 명령별도 별도 정의
	unsigned short 	wNum;				// 이 Command에 해당 사항이 있는 채널의 수
	unsigned long	lChSelFlag[2];		// 이 시험조건이 전송될 Channel (Bit 당 Channel)
	unsigned long	nLength;			// Size of Body
} SFT_MSG_HEADER, *LPSFT_MSG_HEADER;


//Response Body
typedef struct s_SFT_CMD_RESPONSE{				//Command Response (NetWork St.)
	unsigned long nCmd;
	unsigned long nCode;
} SFT_RESPONSE, *LPSFT_RESPONSE;
//Response Packet

//Network Structure
typedef struct s_CommandResponse {
	SFT_MSG_HEADER		msgHeader;
	SFT_RESPONSE		msgBody;
} SFT_PACKET_RESPONSE, *LPSFT_PACKET_RESPONSE;

// SFTScheduler Group Information Data
// Network Structure
typedef struct s_SFT_CMD_INFO_DATA {
	unsigned int		nModuleID;							//Module에서는 GroupID, Host에서는 ModuleID
	unsigned int		nSystemType;						//Formation/IROCV/Aging/Grader/Selector
	unsigned int		nProtocolVersion;					//unsigned int :Protocol Version 
	char				szModelName[128];					//Module Model Name
	unsigned int		nOSVersion;							//unsigned int :System Version 
	unsigned short int	wVoltageRange;						//전압 Range 수
	unsigned short int	wCurrentRange;						//전류 Range 수
	unsigned int		nVoltageSpec[5];					//전압 Range 별 Spec
	unsigned int		nCurrentSpec[5];					//전류 Range 별 Spec
	unsigned char		byTypeData[8];						//Module Location, Line No, Module Type, Etc...	
	unsigned short int	wInstalledBoard;					//unsigned short : Total Board Number in one Module
	unsigned short int	wChannelPerBoard;					//unsinged short : Total Channel Per Board
	unsigned int		nInstalledChCount;					// Installed channel in module
	unsigned int		nTotalJigNo;						//
	unsigned int		awBdInJig[16];						//Board in Each Jig
	unsigned int		reserved[4];
} SFT_MD_SYSTEM_DATA, *LPSFT_MD_SYSTEM_DATA;			

// SFTScheduler Group Information Data
// Network Structure
typedef struct s_SFT_CMD_MD_SET_DATA {
	unsigned char	bConnectionReTry;		//Module Connection retry
	unsigned char	bLineMode;				// N/A
	unsigned char	bControlMode;			// N/A
	unsigned char	bWorkingMode;			// N/A
	unsigned int	nAutoReportInterval;	//Auto Report Data Time Interval	ms
	unsigned int	nDataSaveInterval;		//Data Save Interval	ms
	unsigned int	nReserved[16];			//
} SFT_MD_SET_DATA;				

// SFTScheduler Group Monitoring Data 
typedef struct s_SFT_CMD_STATE_DATA {
	WORD	state;
	BYTE	jigState;				//    
	BYTE	trayState;				//  
	BYTE	doorState;				//  
	BYTE	failCode; 
	WORD	reserved1;				// 총 시간   
 	ULONG	sensorState;			//Bit Data -> Smoke, Fan, door, tray, 기타 
} SFT_MD_STATE_DATA, *LPSFT_MD_STATE_DATA;			

//SFTScheduler Step Header
typedef struct s_StepHeader {			//Step Data Header Structure
	INT			nStepTypeID;			//공정의 ID
	BYTE		stepNo;					//진행 Step 번호(1 Base)
	BYTE		mode;
	BYTE		bTestEnd;
	BYTE		nSubStep;
	BYTE		bUseSocFlag;			//Flag of soc rate end condition
	BYTE		reserved[3];
} SFT_STEP_HEADER, *LPSFT_STEP_HEADER;	

//SFTScheduler Step Referance
//Protocol Ver 0x1005 이후 
typedef struct s_RefData {
	long		lVref;					//Voltage Reference
    long		lIref;					//Current Reference
	long		lTRef;					//Temperature reference	20071106    
	ULONG		ulEndTime;				//End time
    long		lEndV;					//End Voltage
    long		lEndI;					//End Current
    long		lEndC;					//End Capacity
	long		lCycleGoto;				//Cycle goto step
	long		lEndTGoto;
	long		lEndVGoto;				//EndV goto step	
	long		lEndIGoto;			//Reserved for EndI
	long		lEndCGoto;	
	long		lCycleCount;				//Cycle count
    long		lDeltaV;				//DeltaV (Vpeak - Delta V)
	unsigned short int wSocRate;				//Capa SOC Rate
	unsigned short int wSocCapStepNo;			//Soc Actual capa step no		=> 사용안함(언제나 마지막 Step을 자동 사용)
	long		lEndWatt;				//End Watt
	long		lEndWattHour;				//End WattHour
	long		lStartTemp;				//End Temperature	20071106
	long		lEndTemp;				//Start Temperature	20071106
//	long		lReserved;				//Reserved
	unsigned short int wGotoCycleCount;	//Goto Cycle 20081201 KHS
	unsigned short int wReserved;		//Reserved
} SFT_STEP_REFERANCE, *LPSFT_STEP_REFERANCE;

//SFTScheduler Step Referance
//Protocol Ver 0x1004 이후 
typedef struct s_RefData_V5 {
	long		lVref;					//Voltage Reference
    long		lIref;					//Current Reference
	long		lTRef;					//Temperature reference	20071106
    
	ULONG		ulEndTime;				//End time
    long		lEndV;					//End Voltage
    long		lEndI;					//End Current
    long		lEndC;					//End Capacity
	long		lGoto;					//Cycle goto step
	long		lCycleCount;			//Cycle count
    long		lDeltaV;				//DeltaV (Vpeak - Delta V)
	unsigned short int wSocRate;		//Capa SOC Rate
	unsigned short int wSocCapStepNo;	//Soc Actual capa step no		=> 사용안함(언제나 마지막 Step을 자동 사용)
	long		lEndWatt;				//End Watt
	long		lEndWattHour;			//End WattHour
	long		lStartTemp;				//End Temperature	20071106
	long		lEndTemp;				//Start Temperature	20071106
//	long		lReserved;				//Reserved
	unsigned short int wGotoCycleCount;	//Goto Cycle
	unsigned short int wReserved;		//Reserved
} SFT_STEP_REFERANCE_V5, *LPSFT_STEP_REFERANCE_V5;

//SFTScheduler Step Referance
//Protocal Ver 0x1000~0x1001
typedef struct s_RefData_V1 {
	long		lVref;					//Voltage Referance
    long		lIref;					//Current Referance
	long		lRange;					//Range of current
    
	ULONG		ulEndTime;				//End time
    long		lEndV;					//End Voltage
    long		lEndI;					//End Current
    long		lEndC;					//End Capacity
	long		lGoto;					//Cycle goto step
	long		lCycleCount;			//Cycle count
    long		lDeltaV;				//DeltaV (Vpeak - Delta V)
	unsigned short int wSocRate;		//Capa SOC Rate
	unsigned short int wSocCapStepNo;	//Soc Actual capa step no
	long		lEndData;				//End 
} SFT_STEP_REFERANCE_V1, *LPSFT_STEP_REFERANCE_V1;

//SFTScheduler Step Ramp Comparison Point 
typedef struct s_StepRampData {			//전압 상승 및 하강 기울기 검사 
	long			lLowData;
	long			lHighData;
	unsigned long	ulTime;
} SFT_COMP_CONDITION;

//SFTScheduler Step Delta condition
//Network Structure
typedef struct s_StepDeltaData {			//전압 전류 변화 검사  
	long			lMinData;
	long			lMaxData;
	unsigned long	ulTime;					//c
} SFT_DELTA_CONDITION;

//Grade Step Data 
typedef struct tag_Grade{			
	char	gradeCode;
	char	item;
	SHORT	reserved2;	
    long	lMinValue;
	long	lMaxValue;
} SFT_GRADE_STEP;

//Grade Condition 
//Network Structure
typedef struct s_Grade_Item {
	BYTE	item;		
	BYTE	stepCount;
	SHORT	gradeItemType;
	SFT_GRADE_STEP step[_SFT_MAX_PACKET_GRADE_STEP];
} SFT_GRADE_CONDITION;

typedef struct s_Record_Contition {
	unsigned long	lTime;
	long			lDeltaV;
	long			lDeltaI;
	long			lDeltaT;
	long			lDeltaP;
	long			lReserved;
}	SFT_RECORD_CONDITION;

typedef struct s_EDLC_Condition {
	long	lCapV1;
	long	lCapV2;
	ULONG	ulDCRStart;
	ULONG	ulDCREndT;
	ULONG	ulLCStartT;
	ULONG	ulLCEndT;

}	SFT_EDLC_CONDITION;

//Protocal Ver 0x1005 이후 
typedef struct s_SFT_CMD_STEP_DATA {			//Charge/Discharge Step Data Structure
	SFT_STEP_HEADER	stepHeader;
	SFT_STEP_REFERANCE stepReferance[_SFT_MAX_PACKET_SUB_STEP];

	SFT_COMP_CONDITION	vRamp[_SFT_MAX_PACKET_COMP_POINT];		//전압 상승 비교값 
	SFT_COMP_CONDITION	iRamp[_SFT_MAX_PACKET_COMP_POINT];		//전류 상승 비교값
	
	SFT_DELTA_CONDITION	vDelta;									//전압 변화 비교값
	SFT_DELTA_CONDITION	iDelta;									//전류 변화 비교값 

	SFT_RECORD_CONDITION	conREC;							//Record condition	
	SFT_EDLC_CONDITION		conEDLC;
	
	SFT_GRADE_CONDITION	gradeCondition[_SFT_MAX_PACKET_GRADE_ITEM_IN_STEP];	//Grading 조건 

	long	lHighV;									
    long	lLowV;
    long	lHighI;
    long	lLowI;
	long	lHighC;
	long	lLowC;
	long	lHighZ;
	long	lLowZ;
	long	lHighTemp;
	long	lLowTemp;
	long	lReserved[2];
} SFT_STEP_CONDITION, *LPSFT_STEP_CONDITION;

//Protocal Ver 0x1002 이후 
typedef struct s_SFT_CMD_STEP_DATA_V5 {			//Charge/Discharge Step Data Structure
	SFT_STEP_HEADER	stepHeader;
	SFT_STEP_REFERANCE_V5 stepReferance[_SFT_MAX_PACKET_SUB_STEP];

	SFT_COMP_CONDITION	vRamp[_SFT_MAX_PACKET_COMP_POINT];		//전압 상승 비교값 
	SFT_COMP_CONDITION	iRamp[_SFT_MAX_PACKET_COMP_POINT];		//전류 상승 비교값
	
	SFT_DELTA_CONDITION	vDelta;									//전압 변화 비교값
	SFT_DELTA_CONDITION	iDelta;									//전류 변화 비교값 

	SFT_RECORD_CONDITION	conREC;							//Record condition	
	SFT_EDLC_CONDITION		conEDLC;
	
	SFT_GRADE_CONDITION	gradeCondition[_SFT_MAX_PACKET_GRADE_ITEM_IN_STEP];	//Grading 조건 

	long	lHighV;									
    long	lLowV;
    long	lHighI;
    long	lLowI;
	long	lHighC;
	long	lLowC;
	long	lHighZ;
	long	lLowZ;
	long	lHighTemp;
	long	lLowTemp;
	long	lReserved[2];
} SFT_STEP_CONDITION_V5, *LPSFT_STEP_CONDITION_V5;

//Ptorocol ver 0x1000~0x1001
typedef struct s_SFT_CMD_STEP_DATA_V1 {			//Charge/Discharge Step Data Structure
	SFT_STEP_HEADER	stepHeader;
	SFT_STEP_REFERANCE_V1 stepReferance[_SFT_MAX_PACKET_SUB_STEP];

	SFT_COMP_CONDITION	vRamp[_SFT_MAX_PACKET_COMP_POINT];		//전압 상승 비교값 
	SFT_COMP_CONDITION	iRamp[_SFT_MAX_PACKET_COMP_POINT];		//전류 상승 비교값
	
	SFT_DELTA_CONDITION	vDelta;									//전압 변화 비교값
	SFT_DELTA_CONDITION	iDelta;									//전류 변화 비교값 

	SFT_RECORD_CONDITION	conREC;							//Record condition	
	SFT_EDLC_CONDITION		conEDLC;
	
	SFT_GRADE_CONDITION	gradeCondition[_SFT_MAX_PACKET_GRADE_ITEM_IN_STEP];	//Grading 조건 

	long	lHighV;									
    long	lLowV;
    long	lHighI;
    long	lLowI;
	long	lHighC;
	long	lLowC;
	long	lHighZ;
	long	lLowZ;
	long	lReserved[4];
} SFT_STEP_CONDITION_V1, *LPSFT_STEP_CONDITION_V1;

//Module에서 PC로 전송되는 채널의 Data 전송 Format
typedef struct s_SFT_CMD_CH_DATA		
{
	unsigned char	chNo;					// Channel Number			//One Base
	unsigned char	chState;				// Run, Stop(Manual, Error), End
	unsigned char	chStepType;				// Charge, Discharge, OCV, Impedance(AC or DC), Rest
	unsigned char	chMode;
	unsigned char	chDataSelect;			// For Display Data, For Saving Data
	unsigned char	chCode;
	unsigned char	chStepNo;
	unsigned char	chGradeCode;			
	long			lVoltage;				// Result Data...
	long			lCurrent;
	long			lCapacity;
	long			lWatt;
	long			lWattHour;
	unsigned long	ulStepTime;				// 이번 Step 진행 시간
	unsigned long	ulTotalTime;			// 시험 Total 진행 시간
	long			lImpedance;				// Impedance (AC or DC)
	long			lTemparature;
//	long			lPressure;
// =>	
	unsigned char	chReservedCmd;			//0:None, 1:Stop, 2:Pause
	unsigned char	chSharingInfo;			//0: Normal(사용안함), 1(1순위), 2(2순위), n(n순위) 2008/10/21 kjh&kky
//	unsigned char	reserved[2];			//2008/10/21 [3] ==> [2]로 변경
	unsigned short int wGotoCycleNum;		//2008/12/01 Loop 횟수

	unsigned long	nTotalCycleNum;			//2005/12/29 unsigned short int => unsigned long으로 변경
	unsigned long	nCurrentCycleNum;		//2005/12/29 unsigned short int => unsigned long으로 변경
	long			lAvgVoltage;			//Average Voltage of current step
	long			lAvgCurrent;			//Average current of current step
	long			lSaveSequence;			// Expanded Field 2005/12/29 모듈에서 저장하는 Data의 순서 번호 
} SFT_CH_DATA, *LPSFT_CH_DATA;

#define SizeofNetChData()		(sizeof(SFT_CH_DATA))

//교정 Point 설정 
typedef struct s_CaliPointSet
{
	BYTE byValidPointNum;
	BYTE byCheckPointNum;
	BYTE byReserved[2];
	long lCaliPoint[15];
	long lCheckPoint[15];
} SFT_CALI_POINT; 

//typedef struct s_SFT_CMD_CALI_POINT
//{
//	SFT_CALI_POINT	calPoint[2][3];		//전압 3:Range0/1/2 
////	SFT_CALI_POINT	calIPoint[3];		//전류 3:Range0/1/2 
//} SFT_CALI_SET_POINT;

typedef struct s_SFT_CMD_CALI_START
{
	int				nType;				//voltage or current
	int				nRange;				//range 0,1,2...
	int				nMode;
	SFT_CALI_POINT	pointData;
} SFT_CALI_START_INFO, *LPSFT_CALI_START_INFO;

typedef struct s_SFT_CMD_CALI_CHECK_RESULT
{
	int		nType;				//voltage or current
	BYTE	byRange;
	BYTE	reserved;
	BYTE	byValidCheckPointNum;
	BYTE	byChannelID;
	long	lCheckAD[15];
	long	lCheckDVM[15];
} SFT_CALI_CHECK_DATA;

//교정 완료후 수신되는 Data
typedef struct s_SFT_CMD_CALI_RESULT
{
	int		nType;				//voltage or current
	BYTE	byRange;	
	BYTE	byValidCalPointNum;
	BYTE	byValidCheckPointNum;
	BYTE	byChannelID;		//One Base
	long	lCaliAD[15];
	long	lCaliDVM[15];
	long	lCheckAD[15];
	long	lCheckDVM[15];
} SFT_CALI_END_DATA, *LPSFT_CALI_END_DATA;

typedef struct s_SFT_CMD_CALI_READY_DATA
{
	long	nCode;
}	SFT_CALI_READY_RESPONSE;

typedef struct s_SFT_CMD_CALI_READY
{
	long	nCode;
}	SFT_CALI_SET_READY_DATA;

typedef struct s_SFT_CMD_SCHEDULE_START
{
	unsigned char	byTotalStep;
	unsigned char	reserved[3];
	long			reserved2[2];
}	SFT_STEP_START_INFO;

typedef struct s_SFT_CMD_EMERGENCY
{
	long	lCode;
	long	lValue;
}	SFT_EMG_DATA;

typedef struct tag_AutoReportSet {
	UINT nInterval;			//자동 보고 간격 
	BYTE reserved[4];		
} SFT_AUTO_REPORT_SET;


typedef struct tag_LineModeSet {
	BOOL bOnLineMode;	//0: OffLine,  1: OnLine 
	BOOL bControlMode;	//0: Maintenance Mode, 1: Control Mode 
	BYTE reserved[4];		
} SFT_LINE_MODE_SET;

//2006/3/24 test safety data
typedef struct tag_SFT_CMD_SAFETY_DATA {
	long	lVtgLow;
	long	lVtgHigh;
	long	lCrtLow;
	long	lCrtHigh;
	long	lCapLow;
	long	lCapHigh;
	long	lTempLow;
	long	lTempHigh;
	long	lReserved[4];
} SFT_TEST_SAFETY_SET;

//msec 저장용 channel data 구조 
typedef struct tag_SFT_CMD_MSEC_CH_DATA_HEADER {
	long	lTotCycNo;
	long	lStepNo;
	long	lDataCount;
} SFT_MSEC_CH_DATA_INFO;

//body = > { SFT_MSEC_CH_DATA * data 수 }
//Protocol Ver 0x1002
typedef struct tag_SFT_CMD_MSEC_CH_DATA_BODY {
	long	lSec;		//time
	long	lData1;		//Voltage
	long	lData2;		//current
	long	lData3;		//Capacity
	long	lData4;		//WattHour
} SFT_MSEC_CH_DATA, *LPSFT_MSEC_CH_DATA;

//Protocol Ver 0x1003
typedef struct tag_SFT_CMD_CONTROL {
	long	lStepNo;		// 0 or 현재 Step보다 작은값이면 즉시 Stop
	long	lCycleNo;		// 0 or 현재 Cycle보다 작은값이면 즉시 Stop
	long	lData1;
	long	lData2;
	long	lReserved[4];
} SFT_CONTROL_INFO;

//body of SFT_CMD_STEP_DATA_REQUEST command
typedef struct tag_SFT_CMD_STEP_DATA_REQUEST {
	long	lStepNo;
	long	lReserved[3];
} SFT_STEP_NO_INFO;

//body of SFT_CMD_PARALLEL_DATA command
typedef struct s_SFT_CMD_PARALLEL_DATA{

	unsigned char chNoMaster;	//Master Channel		(1 Base)
	unsigned char chNoSlave[3];	//3개까지 Slave 가능	(1 Base)
	unsigned char bParallel;	//0:단독, 1:병렬
	unsigned char reserved[3];

}SFT_MD_PARALLEL_DATA;

//body of SFT_CMD_AUX_SET_DATA command
typedef struct s_STF_CMD_AUX_SET_DATA{

	unsigned char chNo;			//Channel Number (1 Base)
	unsigned char reserved[3];
	short	auxChNo;	//AUX ID	(1 Base)
	short	auxType;	//AUX Type (0 : 온도센서, 1:Voltage)	
	char	szName[32];	//Aux Name
	long	lMaxData;	//센서의 상한값
	long	lMinData;	//센서의 하한값
	long	lEndMaxData;
	long	lEndMinData;
	long	reserved1[2];
} STF_MD_AUX_SET_DATA;

//body of STF_CMD_USERPATTERN_DATA
typedef struct s_SFT_CMD_USERPATTERN_DATA{
	long	lStepNo;			//User Pattern 이 사용된 스텝 번호 
	long	lLength;			//동적으로 길이가 변한다.
	long	lType;			//Data종류  
	char reserved[4];
} SFT_MD_USERPATTERN_DATA;

typedef struct s_PATTERN_DATA{
	long	lTime;
	long	lValue;
}SFT_PATTERN_DATA;

//body of SFT_CMD_SET_MEASURE_DATA
typedef struct s_SFT_CMD_SET_MEASURE_DATA 
{
	unsigned short int	wID;
	unsigned short int	wType;
	long				lData;
	long				lReserved;
} SFT_MESAURE_DATA_PACKET;

//////////////////////////////////////////////////////////////////////////
// Command Define start
//////////////////////////////////////////////////////////////////////////

//common command(0x0000~0xFFF)
//System Command
#define SFT_CMD_NONE				0x00000000
#define SFT_CMD_RESPONSE			0x00000001		//Command response 
#define SFT_CMD_SHUTDOWN			0x00000002		//Module shutdown Command
#define SFT_CMD_SYSTEM_INIT			0x00000003		//(PC <- SBC)   Module Init
#define SFT_CMD_HEARTBEAT			0x00000004		//(PC <- SBC)	Network State Check  Send Ack 
#define SFT_CMD_EMERGENCY			0x00000005

#define SFT_CMD_INFO_REQUEST		0x00000006		//(PC -> SBC) Version Data request	//Ack SFT_AMD_VER_DATA
#define SFT_CMD_INFO_DATA			0x00010006		//(PC <- SBC) Version Data Response


//special command(0x1000~)
//Control command	
#define SFT_CMD_RUN					0x00001000		//(PC -> SBC)	//Receive Ack
#define SFT_CMD_STOP				0x00001001		//(PC -> SBC)	//Receive Ack
#define SFT_CMD_PAUSE				0x00001002		//(PC -> SBC)	//Receive Ack
#define SFT_CMD_CONTINUE			0x00001003		//(PC -> SBC)	//Receive Ack
#define SFT_CMD_CLEAR				0x00001004		//(PC -> SBC)	//Receive Ack
#define SFT_CMD_NEXTSTEP			0x00001005		//(PC -> SBC)	//Receive Ack

//Data Sending command
#define SFT_CMD_CH_DATA				0x00001010		//(PC <- SBC)	Auto Group Data	(Send ACK) 
#define SFT_CMD_STATE_DATA			0x00001011		//(PC <- SBC)	Auto Group State Data	(Send ACK) 
#define SFT_CMD_MD_SET_DATA			0x00001012		//(PC -> SBC)	//SFT_MD_SET_DATA

//Schedule data sending command
#define SFT_CMD_SCHEDULE_START		0x00001020		//(PC -> SBC)	send Test Header Data
#define SFT_CMD_STEP_DATA			0x00001021		//(PC -> SBC)	send Test Step Data
#define SFT_CMD_SCHEDULE_END		0x00001022		//(PC -> SBC)	send Test Header Data

//2006/3/24 ver 0x1002 cmd
#define SFT_CMD_SAFETY_DATA			0x00001023		//(PC -> SBC)	send test safty condition
#define SFT_CMD_MSEC_CH_DATA		0x00001013		//(PC <- SBC)	Auto msec data	(Send ACK) 

//2006/6/9 Ver 0x1003 CMD
#define SFT_CMD_STEP_DATA_REQUEST		0x00001024	//(PC ->SBC)	
#define SFT_CMD_STEP_INFO				0x00011024	//(PC <- SBC)	response of SFT_CMD_STEP_DATA_REQUEST
#define SFT_CMD_STEP_DATA_UPDATE		0x00001025	//(PC ->SBC )	//ACK

#define SFT_CMD_SAFETY_DATA_REQUEST		0x00001026	//(PC ->SBC)	
#define SFT_CMD_SAFETY_INFO				0x00011026	//(PC ->SBC)	response of SFT_CMD_SAFETY_DATA_REQUEST
#define SFT_CMD_SAFETY_DATA_UPDATE		0x00001027	//(PC ->SBC)	//ACK

#define SFT_CMD_RESET_RESERVED_CMD		0x00001028	//(PC ->SBC)	//ACK

//2007/11/06 Ver 0x1004 CMD
#define SFT_CMD_USERPATTERN_DATA		0x00001029	//(PC -> SBC)
#define SFT_CMD_SET_MEASURE_DATA		0x0000102A	//(PC<->SBC)	//Ack

//Calibration command
#define SFT_CMD_CALI_READY			0x00001030		//20051123 삭제됨 KBH
#define SFT_CMD_CALI_READY_DATA		0x00011030		//20051123 삭제됨 KBH
#define SFT_CMD_CALI_POINT			0x00001031		//20051123 삭제됨 KBH
#define SFT_CMD_CALI_START			0x00001032		//(PC -> SBC)	//Receive Ack
#define SFT_CMD_CALI_RESULT			0x00001033		//(PC <- SBC)	//Receive Ack
#define SFT_CMD_CALI_CHECK_RESULT	0x00001034
#define SFT_CMD_CALI_UPDATE			0x00001035		//(PC -> SBC)	//Receive Ack

#define SFT_CMD_SET_AUTO_REPORT		0x00002000		//(PC -> SBC)	AutoReport Setting //2003/1/14 Receive Ack
#define SFT_CMD_SET_LINE_MODE		0x00002001		//(PC -> SBC)
#define SFT_CMD_LINE_MODE_REQUEST	0x00002002		//(PC -> SBC)
#define SFT_CMD_LINE_MODE_DATA		0x00012002		//(PC <- SBC)	Response of SFT_CMD_LINE_MODE_REQUEST

//////////////////////////////////////////////////////////////////////////
// Command Define end
//////////////////////////////////////////////////////////////////////////


//Define of module kind
#define SFT_ID_FORM_MODULE		0x01
#define SFT_ID_FORM_OP_SYSTEM	0x02
#define SFT_ID_IROCV_SYSTEM		0x03
#define SFT_ID_GRADING_SYSTEM	0x04
#define SFT_ID_DATA_SERVER		0x05
#define SFT_ID_CYCLER			0x06

//response of command code
#define SFT_NACK				0x00000000
#define SFT_ACK					0x00000001
#define SFT_TIMEOUT				0x00000002
#define SFT_SIZE_MISMATCH		0x00000003
#define SFT_RX_BUFF_OVER_FLOW	0x00000004
#define SFT_TX_BUFF_OVER_FLOW	0x00000005
#define SFT_CONNECTED			0x00000006	
#define SFT_FAIL				0xFFFFFFFF


//Size 함수 
#define SizeofCHData()	sizeof(SFT_CH_DATA)
#define SizeofHeader()	sizeof(SFT_MSG_HEADER)










/*


#define SFT_CMD_AUTO_SENSOR_DATA		0x00000007		//(PC <- SBC)	Auto Temp. Gas Data(Does not need Send Ack)
#define SFT_CMD_AUTO_EMG_DATA			0x00000008		//(PC <- SBC)	Auto Emergency Data (Does not need Send Ack)


#define SFT_CMD_FAULTQUERY			0x00000017		//(PC -> SBC)	//Receive Ack
#define SFT_CMD_FAULT_DATA			0x00000018


#define SFT_CMD_AUTO_GP_DATA			0x00000004		//(PC <- SBC)	Auto Group Data(Does not need Send Ack)
#define SFT_CMD_AUTO_GP_STEP_END_DATA	0x00000005		//(PC <- SBC)	Auto Group Step End Data(Does not need Send Ack)
#define SFT_CMD_AUTO_GP_STATE_DATA		0x00000006		//(PC <- SBC)	Auto Group State Data(Does not need Send Ack)
#define SFT_CMD_AUTO_SENSOR_DATA		0x00000007		//(PC <- SBC)	Auto Temp. Gas Data(Does not need Send Ack)
#define SFT_CMD_AUTO_EMG_DATA			0x00000008		//(PC <- SBC)	Auto Emergency Data (Does not need Send Ack)

//category of Command 
#define SFT_SYS_CMD					0x00000010		//Special Command : 0x00000000 ~ 0x0000000F 
#define SFT_RESPONSE_CMD			0x00010000		//Response Command  
#define SFT_DATA_SERVER_CMD			0x00004000		//Formatin System Command
#define SFT_MODULE_CMD				0x00003000		//Module Command : 0x00002000 ~ 0x00002FFF
#define SFT_GROUP_CMD				0x00002000		//Board	Command	 : 0x00001000 ~ 0x00001FFF	
#define SFT_CHANNEL_CMD				0x00001000		//Channel Command: 0x00000000 ~ 0x00000FFF




//Group Command
#define SFT_CMD_CAL_EXEC			0x00001000		//(PC -> SBC)	
#define SFT_CMD_CAL_REQUEST			0x00001001		//(PC -> SBC)
#define SFT_CMD_CAL_DATA			0x00011001		//(PC <- SBC)
#define SFT_CMD_TESTINFO_REQUEST	0x00001005		//(PC -> SBC)	
#define SFT_CMD_TESTINFO_DATA		0x00011005		//(PC <- SBC)
#define SFT_CMD_AUTO_NVRAM_HEADER	0x00001006		//(PC <-SBC)	Does not need Send Ack
#define SFT_CMD_NVRAM_CHSTATE_REQUEST 0x00001007		//(PC -> SBC)
#define SFT_CMD_NVRAM_CHSTATE_DATA	0x00011007		//(PC <- SBC)
#define SFT_CMD_AUTO_CHECK_RESULT	0x00001008		//(PC <- SBC)	Check Result Data
#define SFT_CMD_NVRAM_SET			0x00001009		//(PC -> SBC)	Receive Ack
#define SFT_CMD_NVRAM_HEADER		0x0000100A		//(PC -> SBC)	Receive Ack
#define	SFT_CMD_SET_TRAY_SERIAL		0x0000100B		//(pc -> sbc)	Receive Ack
#define SFT_CMD_INIT_NVRAM			0x0000100C		//(PC -> SBC)	Receive Ack
#define SFT_CMD_SENSOR_LIMIT_SET	0x0000100D		//(PC -> SBC)	Receive Ack
#define SFT_CMD_TRAY_DATA			0x0000100E		//(PC -> SBC)	Receive Ack
#define SFT_CMD_TRAY_SERIAL_REQUEST	0x0000100F		//(PC -> SBC)	response SFT_CMD_TRAY_SERIAL_DATA
#define SFT_CMD_TRAY_SERIAL_DATA	0x0001100F		//				response of SFT_CMD_TRAY_SERIAL_REQUEST
#define SFT_CMD_TEST_END			0x00001010		//(PC->SBC)		Send to Module Test End
#define SFT_CMD_USER_CMD			0x00001011		//(PC <-> SBC)	Maintenance Command or Module Button Command		Receive Ack 

//2003/1/14 version 0x3001
#define SFT_CMD_DIGITAL_IN_REQUEST	0x00001013		//(PC -> SBC)
#define SFT_CMD_DIGITAL_IN_DATA		0x00011013		//(PC <- SBC)	Response of SFT_CMD_DIGITAL_IN_REQUEST
#define SFT_CMD_SET_DIGITAL_OUT		0x00001014		//(PC -> SBC)

//2003/7/5  version 0x3002
#define SFT_CMD_GET_REF_IC_DATA		0x00001015		//(PC -> SBC)   Ref IC AD Data Request
#define SFT_CMD_REF_IC_DATA			0x00011015		//(PC <- SBC)	response of SFT_CMD_GET_REF_IC_DATA

//Module Command

#define SFT_CMD_BOARDSET			0x00002001		
#define SFT_CMD_BOARDDATA			0x00002002
#define SFT_CMD_STEPDATA			0x00002003		//(PC -> SBC)	send Test Step Data
#define SFT_CMD_GRADEDATA			0x00002004		//(PC -> SBC)	send Grade Data
#define SFT_CMD_CHECK_PARAM			0x00002006		//(PC -> SBC)	Send Check Parameter
#define SFT_CMD_FAILCH				0x00002007
#define SFT_CMD_PROCDATA			0x00002008
#define SFT_CMD_SENDHEADER			0x00002009
#define SFT_CMD_SENDCONTENT			0x0000200A
#define SFT_CMD_CLEARALL			0x0000200B
#define SFT_CMD_CHANGEPROC			0x0000200C
#define SFT_CMD_CHANGEHEADER		0x0000200D
#define SFT_CMD_CLEARCONTENT		0x0000200E
#define SFT_CMD_SENDCURRENT			0x0000200F
#define SFT_CMD_SYS_PARAM_REQUEST	0x00002012		//(PC -> SBC)
#define SFT_CMD_SYS_PARAM_DATA		0x00012012		//(PC <- SBC) System Parameter Request or Send
#define SFT_CMD_TEST_STEP_DATA		0x00002014		//(PC ->SBC)	1 Step Data 

//2003/1/14

//Data Server Command
#define SFT_CMD_DATA_SERVER_VER		0x00003000
#define SFT_CMD_TEST_RESULT			0x00003001
#define SFT_CMD_PROCEDURE_DATA		0x00003002


//Jig State
#define SFT_JIG_NONE			0x00
#define SFT_JIG_UP				0x01
#define SFT_JIG_MOVE_DOWN		0x02
#define SFT_JIG_DOWN			0x03
#define SFT_JIG_MOVE_UP			0x04
#define SFT_JIG_ERROR			0x05
	
//door State
#define SFT_DOOR_NONE			0x00
#define SFT_DOOR_OPEN			0x01
#define SFT_DOOR_MOVE			0x02
#define SFT_DOOR_CLOSE			0x03
#define SFT_DOOR_ERROR			0x04

//define Tray state 
#define SFT_TRAY_NONE			0x00
#define SFT_TRAY_UNLOAD			0x01
#define SFT_TRAY_MOVE			0x02
#define SFT_TRAY_LOAD			0x03
#define SFT_TRAY_ERROR			0x04

#define SFT_OFFLINE_MODE	0
#define SFT_ONLINE_MODE	1

#define SFT_MAINTENANCE_MODE	0
#define SFT_CONTROL_MODE		1

//Emergency State(LG 4호 이전 Project) 
#define SFT_EMG_NORMAL		0x00
#define SFT_EMG_UPS_POWER	0x01
#define SFT_EMG_UPS_BATTERY_FAIL	0x02
#define SFT_EMG_MAIN_EMG_SWITCH	0x03
#define SFT_EMG_SUB_EMG_SWITCH	0x04
#define SFT_EMG_TEMP_LIMIT		0x05
#define SFT_EMG_TEMP_DIFF_LIMIT	0x06
#define SFT_EMG_SMOKE_LEAK		0x07
#define SFT_EMG_NETWORK_ERROR	0x08
#define SFT_EMG_POWER_SWITCH		0x09
#define SFT_EMG_DVM_COMM_ERROR	0x0A
#define SFT_EMG_CALIBRATOR_COMM_ERROR	0x0B
#define SFT_EMG_CPU_WATCHDOG		0x0C
#define SFT_EMG_MODULE_WATCHDOG	0x0D

#define SFT_EMG_MAINCYL_UP_TIMEOUT		0x20
#define SFT_EMG_GRIPCYL_OPEN_TIMEOUT		0x21
#define SFT_EMG_GRIPCYL_CLOSE_TIMEOUT	0x22
#define SFT_EMG_MAINCYL_DOWN_TIMEOUT		0x23
#define SFT_EMG_GRIPCYL_RO_SENS			0x24
#define SFT_EMG_GRIPCYL_RC_SENS			0x25
#define SFT_EMG_GRIPCYL_LO_SENS			0x26
#define SFT_EMG_GRIPCYL_LC_SENS			0x27
#define SFT_EMG_MAINCYL_U_SENS			0x28
#define SFT_EMG_MAINCYL_L_SENS			0x29
#define SFT_EMG_AIR_PRESS				0x2A
#define SFT_EMG_NVRAM_COMM_ERROR			0x2B
#define SFT_EMG_DOOR_OPEN				0x2C

#define SFT_EMG_MAX_FAULT				0x40	//Group Fail

#define SFT_EMG_UPPER_TEMP				0x60	//Main Board Fail
#define SFT_EMG_LOWER_TEMP				0x61
#define SFT_EMG_ADC_READ_FAIL			0x62
#define SFT_EMG_HW_FAULT					0x63

//New Module Fail Code Define 2003/5/26 KBH
#define SFT_FAIL_NONE					0x00
#define SFT_FAIL_TERMINAL_QUIT			0x01
#define SFT_FAIL_TERMINAL_HALT			0x02
#define SFT_FAIL_NET_CMD_EXIT			0x03
#define SFT_FAIL_POWER_SWITCH			0x04
#define SFT_FAIL_AC_POWER1				0x05
#define SFT_FAIL_AC_POWER2				0x06
#define SFT_FAIL_UPS_BATTERY				0x07
#define SFT_FAIL_MAIN_EMG				0x08
#define SFT_FAIL_SUB_EMG					0x09
#define SFT_FAIL_CPU_WATCHDOG			0x0A
#define SFT_FAIL_MAIN_BD_ADC				0x0B
#define SFT_FAIL_MAIN_BD_HW				0x0C
#define SFT_FAIL_MAIN_BD_TEMP_UPPER		0x0D
#define SFT_FAIL_NETWORK_ERROR			0x0E
#define SFT_FAIL_DVM_COMM_ERROR			0x0F
#define SFT_FAIL_CAL_COMM_ERROR			0x10
#define SFT_FAIL_OVP						0x11
#define SFT_FAIL_OCP						0x12
#define SFT_FAIL_UPPER_VOLTAGE			0x13
#define SFT_FAIL_UPPER_CURRENT			0x14
#define SFT_FAIL_RUN_TIME_OVER			0x15

#define SFT_FAIL_MAX_FAULT				0x20

#define SFT_FAIL_MAINCYL_UP_TIME_OVER	0x40
#define SFT_FAIL_MAINCYL_DWON_TIME_OVER	0x41
#define SFT_FAIL_MAINCYL_UP_SENS			0x42
#define SFT_FAIL_MAINCYL_DOWN_SENS		0x43
#define SFT_FAIL_MAINCYL_RU_SENS			0x44
#define SFT_FAIL_MAINCYL_LU_SENS			0x45
#define SFT_FAIL_MAINCYL_RD_SENS			0x46
#define SFT_FAIL_MAINCYL_LD_SENS			0x47
#define SFT_FAIL_LATCHCYL_OPEN_TIME_OVER	0x48
#define SFT_FAIL_LATCHCYL_CLOSE_TIME_OVER	0x49
#define SFT_FAIL_LATCHCYL_RC_SENS		0x4A
#define SFT_FAIL_LATCHCYL_RO_SENS		0x4B
#define SFT_FAIL_LATCHCYL_LC_SENS		0x4C
#define SFT_FAIL_LATCHCYL_LO_SENS		0x4D
#define SFT_FAIL_GRIPCYL_OPEN_TIME_OVER	0x4E
#define SFT_FAIL_GRIPCYL_CLOSE_TIME_OVER	0x4F
#define SFT_FAIL_GRIPCYL_RO_SENS			0x50
#define SFT_FAIL_GRIPCYL_RC_SENS			0x51
#define SFT_FAIL_GRIPCYL_LC_SENS			0x52
#define SFT_FAIL_GRIPCYL_LO_SENS			0x53
#define SFT_FAIL_TRAY_SENS				0x54
#define SFT_FAIL_TRAY_DIR_SENS			0x55
#define SFT_FAIL_DOOR_OPEN				0x56
#define SFT_FAIL_SMOKE_LEAK				0x57
#define SFT_FAIL_AIR_PRESS				0x58
#define SFT_FAIL_TEMP_UPPER				0x59
#define SFT_FAIL_DIFF_TEMP_UPPER			0x5A
#define SFT_FAIL_NVRAM_COMM_ERROR		0x5B
#define SFT_FAIL_CRAIN_ERROR				0x5C


//New Command Define, Receive 할 Packet 의 종류..
#define	SFT_PACKET_AUTO_RESULT_DATA		102		// Monitoring 데이터
#define	SFT_PACKET_ACK_MODULE_ID		104		// Module ID 전송
#define	SFT_PACKET_ACK_RESULT			108		
#define	SFT_PACKET_CALI_RESULT			109		// Calibration Result
#define	SFT_PACKET_COMM_CONNECTED		110		// Multimeter Connected

////////////////////////////////////////////////////////
// Control PC 에서 혹은 Monitoring에서 SBC에 내릴
// Command 아이디
#define		CMD_START					0x01
#define		CMD_STOP					0x02
#define		CMD_TESTCOND_START			0x03
#define		CMD_TESTCOND_STEP			0x04
#define		CMD_TESTCOND_END			0x05
#define		CMD_CONTINUE				0x06
#define		CMD_PAUSE					0x07
#define		CMD_COMM_CONNECT			0x10
#define		CMD_CALI_V					0x11
#define		CMD_CALI_I					0x12
#define		CMD_CALI_UPDATE				0x13
#define		CMD_COMM_DISCONNECT			0x14
#define		CMD_CALI_C_H				0x15
#define		CMD_CALI_C_L				0x16
#define		CMD_CALI_C_M				0x17
#define		CMD_CALI_POINT_SET			0x18
#define		CMD_CLEAR_CH_STATE			0x19
#define		CMD_HARDWARE_ERROR			0x20








//Group의 정보 중 주기적이 Update 가 필요한 Data
//Temperature and Gas Sensor Monitoring Data
// Network Structure
typedef struct tag_TempGasData {
	long	nTempData;
	long	nGasData;
	long	nReserved[8];		
} SFT_SENSOR_DATA;


// SFTScheduler Channel Monitoring Data
// Network Structure



//Ocv Step Data Structure 
//Network Structure
typedef struct tag_StepOcv {			//OCV Step Data Structure
	SFT_STEP_HEADER	stepHeader;
	long		lOverV;
    long		lLimitV;
	long		lReserved[4];
} SFT_OCV_STEP;

//rest Step Data 
//Network Structure
typedef struct tag_StepRest {			//Rest Step Data Structure
	SFT_STEP_HEADER	stepHeader;
	unsigned long	ulEndTime;
	long			lOverV;
    long			lLimitV;
	long			lReserved[4];
} SFT_REST_STEP;

//Impedance step Data 
//Network Structure
typedef struct tag_StepImpedance {		//Impedance Step Data Structure
	SFT_STEP_HEADER	stepHeader;
	SFT_STEP_REFERANCE	stepReferance[2];
	unsigned long		ulRestTime;
	long 		lOverImp;
    long		lLimitImp;
	long		lReserved[4];
} SFT_IMPEDANCE_STEP; 

//End Step Data
//Network Structure
typedef struct tag_StepEnd {		
	SFT_STEP_HEADER	stepHeader;
} SFT_END_STEP; 

//Cell Check parameter
//Network Structure
typedef struct	tag_CellCheckParameter {		
    long		lOCVUpperValue;		//OCV High Limit
    long		lOCVLowerValue;		//OCV Low Limit
    long		lVRef;				//V Referance Val
    long		lIRef;				//I Referance Val
    long		lTime;				//Check Time
    long		lDeltaVoltage;		//Delta Voltage
    long		lMaxFaultCount;		//Max Fault Count
	unsigned char	compFlag;		//Use Flag
    unsigned char	reserved;	
	WORD		wReserved;
} SFT_CELL_CHECK_PARAM;

//Cell Check Result
//Network Structure
typedef struct tag_CellCheckResult {
	WORD	wErrorChNo;
	WORD	wNormalChNo;
} SFT_CHECK_RESULT;


////////////////////////////////////////////////////////////////
//Charge and Discharge Step(Nt Data)//Grade Step Data (nt Data)

/*
typedef struct tag_Common_StepData {			//Charge/Discharge Step Data Structure
	SFT_STSFT_HEADER	stepHeader;					//Step Header
	SFT_STEP_REFERANCE stepReferance[SFT_MAX_SUB_STEP];	//Step Referance and End Condition

	SFT_COMP_CONDITION	vRamp[SFT_MAX_COMP_POINT];		//전압 상승 비교값 
	SFT_COMP_CONDITION	iRamp[SFT_MAX_COMP_POINT];		//전류 상승 비교값
	
	SFT_DELTA_CONDITION	vDelta;							//전압 변화 비교값
	SFT_DELTA_CONDITION	iDelta;							//전류 변화 비교값 
	

	long	lOverV;									
    long	lLimitV;
    long	lOverI;
    long	lLimitI;
	long	lOverImp;
	long	lLimitImp;
	unsigned long	ulEndTime;

	SFT_GRADE_CON	gradeCondition[SFT_MAX_STEP_GRADE];	//Grading 조건 
	
} SFT_COMMON_STEP;
*/

/*
////////////////////////////////////////////////////////////////
//Send Test condtition Information (Nt Data)
typedef struct tag_TestInformation {
	char	szTestLogFileName[SFT_TEST_LOG_FILE_LENGTH];
	char	szResultFileName[SFT_RESULT_FILE_NAME_LENGTH];
	BYTE	totalStep;
	BYTE	totalGrade;
	WORD	reserved;
} SFT_TEST_HEADER, *LPSFT_TEST_HEADER;

//NVRAM Content (nt Data)
typedef struct tag_NVRamData {
	long lModelKey;		//Battery Model DB Primary Key
	long lTestKey;		//Test	Paramary Key
	char szTraySerialNo[SFT_TRAY_SERIAL_LENGTH];		//Tray의 고유 번호
	char szTrayNo[SFT_TRAY_NAME_LENGTH];				//사용자 입력 Tray No Max 31Byte
	char szLotNo[SFT_LOT_NAME_LENGTH];					//사용자 입력 Lot No  Max 31Byte
	char szTestSerialNo[SFT_TEST_SERIAL_LENGTH];		//현재 공정에 대한 고유 번호 23Byte(날짜+Random()수를 이용한 고유 번호)
	char szProcessID[SFT_PROCESS_ID_LENGTH];			//현재 공정이 시행 하고 있는 시험(Battery Model의 Primary Key Index를 저장)	11Byte
	char szLastTestedDate[SFT_SHORT_DATE_TIME_LENGTH];	//최종 시험 일시	16Byte
	char szCellNo[SFT_CELLNO_LENGTH];
	int	 nNormalCount;
	int	 nFailCount;
//	char nBatteryState[SFT_BATTERY_PER_TRAY][2];			//128*2Byte
} SFT_NVRAM_DATA;	

typedef struct tag_NVRamSet {
	int nUseNVRam;
} SFT_NVRAM_SET;

typedef struct tag_TraySerialNo {
	char szTraySerialNo[SFT_TRAY_SERIAL_LENGTH];	//Tray의 고유 번호
	char szTrayNo[SFT_TRAY_NAME_LENGTH];			//사용자 입력 Tray No Max 31Byte
} SFT_TRAY_SERIAL;

*/
/*
//New GroupData Structure	2002/2/20
typedef struct tag_GroupData {
	SFT_GP_STATE_DATA	gpState;			//
	SFT_SENSOR_DATA		gpSensorData;		//12Byte
	SFT_SENSOR_MINMAX	sensorMinMax;		
} SFT_GP_DATA;
*/

/*
typedef struct	tag_BoardSetParameter {		//Check Parameter(NetWork St.)
    long		lMaxVoltage;	//-->OCV
    long		lMinVoltage;

    long		lOcvLimitValue;

    long		lMaxCurrent;
    long		lTrickleCurrent;
    long		lTrickleDuration;
    long		lDeltaVoltage;
    long		lMaxFaultBattery;
    long		lAutoTime;
    
	unsigned char	compFlag;
    unsigned char	autoProcessingYN;
} SFT_PRETEST_PARAM	;
*/
/*
//User Key In command
//Network Structure
typedef struct tag_User_Command {
	int nCmd;
	int nData;
	BYTE reserved[32];
} SFT_USER_CMD;

//Port Scan Data, Port Set Data
//Network Structure
typedef struct tag_Port_Data {
	BYTE	data[8];
} SFT_PORT_DATA;


typedef struct tag_RefDate {
	long	lCalData;
	char	szCalTime[_SFT_DATE_TIME_LENGTH];		//32 Byte,  ex) 2003/04/05/17:45:22
	long	lCurData;
	char	szCurTime[_SFT_DATE_TIME_LENGTH];		//32 Byte,  ex) 2003/04/05/17:45:22
	long	lMinData;
	char	szMinTime[_SFT_DATE_TIME_LENGTH];		//32 Byte,  ex) 2003/04/05/17:45:22
	long	lMaxData;
	char	szMaxTime[_SFT_DATE_TIME_LENGTH];		//32 Byte,  ex) 2003/04/05/17:45:22
} SFT_REF_DATA;

//Body of SFT_CMD_REF_IC_DATA command
typedef struct tag_Ref_ic_data {
	SFT_REF_DATA	vPluse;
	SFT_REF_DATA	vZero;
	SFT_REF_DATA	vMinus;
	SFT_REF_DATA	iPluse;
	SFT_REF_DATA	iZero;
	SFT_REF_DATA	iMinus;
} SFT_REF_AD_DATA, *LPSFT_REF_AD_DATA;

typedef struct tag_Ref_IC{
	SFT_REF_AD_DATA	refIC_Data[16];
	long reserved[4];
} SFT_REF_IC_DATA, *LPSFT_REF_IC_DATA;


//Tray의 최종 상태를 SBC에 전송하는 Type
//typedef	struct tag_CellStateInTray {
//	BYTE	bFirstProc;										//Check Flag for First Procedure 
//	BYTE	cellType;										//Tray Type
//	WORD	nCellCount;										//Total Number of Cell In Tray
//	BYTE	cellCode[SFT_MAX_CH_PER_MD];						//256	Byte
//	BYTE	cellGradeCode[SFT_MAX_CH_PER_MD];				//256	Byte
//	char	szTraySerialNo[SFT_TRAY_SERIAL_LENGTH];			//Tray의 고유 번호
//	char	szTrayNo[SFT_TRAY_NAME_LENGTH];					//사용자 입력 Tray No Max 31Byte
//	char	szLotNo[SFT_LOT_NAME_LENGTH];					//사용자 입력 Lot No  Max 31Byte
//	char	szTestSerialNo[SFT_TEST_SERIAL_LENGTH];			//현재 공정에 대한 고유 번호 23Byte(날짜+Random()수를 이용한 고유 번호)
//	char	szLastTestedDate[16];	//최종 시험 일시	16Byte
//	long	lProcessID;				//현재 공정이 시행 하고 있는 시험(Battery Model의 Primary Key Index를 저장)	11Byte
//	int		nCellNo;
//} SFT_TRAY_STATE;

*/




#endif //_SFT_SCHEDULER_MSG_DEFINE_H_



