// IPSetDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "IPSetDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIPSetDlg dialog


CIPSetDlg::CIPSetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CIPSetDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CIPSetDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_strIPAddress = "192.168.1.131";
}


void CIPSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CIPSetDlg)
	DDX_Control(pDX, IDC_IPADDRESS1, m_ctrlIPAddress);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CIPSetDlg, CDialog)
	//{{AFX_MSG_MAP(CIPSetDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIPSetDlg message handlers

void CIPSetDlg::SetIPAddress(CString strIP)
{
	m_strIPAddress =  strIP;
}

CString CIPSetDlg::GetIPAddress()
{
	return m_strIPAddress;
}

BOOL CIPSetDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_ctrlIPAddress.SetWindowText(m_strIPAddress);
	m_ctrlIPAddress.SetFieldFocus(3);

	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CIPSetDlg::OnOK() 
{
	// TODO: Add extra validation here
	char address[64];
	ZeroMemory(address, 64);
	m_ctrlIPAddress.GetWindowText(address, 63);
	m_strIPAddress = address;
	CDialog::OnOK();
}
