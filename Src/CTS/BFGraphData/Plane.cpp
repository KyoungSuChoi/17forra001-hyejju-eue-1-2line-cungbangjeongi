// Plane.cpp: implementation of the CPlane class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Plane.h"
#include "Line.h"
#include <math.h>
#include "Data.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPlane::CPlane()
{
	m_BackColor          = RGB(255,255,255);
//Edited by KBH 2005/4/25
//	m_GridColor          = RGB(0,0,0);
	m_GridColor          = RGB(192,192,192);

//	m_bNewScale          = TRUE;
	m_iNumOfLineColors   = 0;
	m_pLineColor         = NULL;
	m_iCurLineColorIndex = 0;
	m_iSerialNo          = 0;
}

CPlane::~CPlane()
{
	while(!m_LineList.IsEmpty())
	{
		CLine* pLine = m_LineList.RemoveTail();
		delete pLine;
	}

//Added by KBH 2005/4/20
	if(m_pLineColor != NULL)
	{
		delete [] m_pLineColor;
		m_pLineColor = NULL;
	}
}

void CPlane::DeleteAllLines()
{
	while(!m_LineList.IsEmpty())
	{
		CLine* pLine = m_LineList.RemoveTail();
		delete pLine;
	}
//	m_bNewScale = TRUE;
}

CLine* CPlane::FindLineOf(LPCTSTR ChPath, LONG lCycle, BYTE byMode)
{
	POSITION pos = m_LineList.GetHeadPosition();
	while(pos)
	{
		CLine* pLine = m_LineList.GetNext(pos);
		if(   (pLine->GetChPath().CompareNoCase(ChPath)==0)
			&&(pLine->GetCycle()==lCycle)
			&&(pLine->GetMode()==byMode)
		  ) return pLine;
	}
	return NULL;
}

BOOL CPlane::IsThereThisLine(LPCTSTR strChPath, LONG lCycle, DWORD dwMode, LPCTSTR strAxisName)
{
	POSITION pos = m_LineList.GetHeadPosition();
	while(pos)
	{
		CLine* pLine = m_LineList.GetNext(pos);
		if(  (pLine->GetChPath().CompareNoCase(strChPath)==0)
		   &&(pLine->GetCycle()==lCycle)
		   &&(pLine->GetMode() == dwMode)
		   &&(pLine->GetAxisName().CompareNoCase(strAxisName)==0)
		  ) return TRUE;
	}
	return FALSE;
}

void CPlane::RemoveLineAt(POSITION pos)
{
	CLine* pLine = m_LineList.GetAt(pos);
	m_LineList.RemoveAt(pos);
	delete pLine;
}

//int CPlane::DrawScopeInView(CDC* pDC, int LeftSpace, int TopSpace, int LabelSpace, CRect ClientRect, BOOL bScope)
int  CPlane::DrawScopeInView(CDC* pDC, int LabelSpace, CRect ScopeRect, CRect ClientRect, BOOL bScope)
{
	/*
	      ============================================
		  || LeftSpace                     /|
		  ||<--------------------->|        |
		  || LabelSpace for Plane[1]     TopSpace
		  ||<----------->|                  |
		  || LabelSpace for Plane[0]        |
		  ||<->|                            |/
		  ||    Volt(mV)  Curr(mA)  -------------------      
		  ||                       |
		  ||                       |    Scope
		  ||    1.0       2.2      |
	*/

	/*
      ======================================================
	  ||                          TopSpace
	  ||            ----------------------------
	  || LabelSpace |                                           ||
	  ||            |             Scope             | x축폰트로 ||
      ||                                            | 6자여유   ||
                            -------------------------           ||
                                    x축폰트로 2줄여유           ||
             ===================================================||
	*/

	// 첫번째 Plane인 경우,
	if(m_iSerialNo == 0) 
	{
		//그리기 영역의 Background color와 그리기 영역의 사각형을 그린다.
		DrawScopeRect(pDC, ScopeRect);

		//X축 눈금과 Label을 표시한다.
		DrawXAxis(pDC, ScopeRect, bScope);
	}

	// y축 *******************************************
	int nYLabelSpace;
	nYLabelSpace = DrawYAxis(pDC, LabelSpace, ScopeRect, bScope);

	return nYLabelSpace;
}

void CPlane::DrawLineInView(CDC* pDC, CRect ScopeRect, BOOL bStairType)//, CRect ClientRect)
{
	// View에서 Scope의 크기를 정한다.
	RECT viewrect;
	viewrect = ScopeRect;

	BOOL PreCheck, CurCheck;
	CLine* pLine;
	fltPoint pre_point, cur_point, tmp_point;
	POSITION DataPos, LinePos;
	POINT temppoint, viewPoint, prePoint;
	ZeroMemory( &temppoint, sizeof(POINT));
	ZeroMemory( &viewPoint, sizeof(POINT));
	ZeroMemory( &prePoint, sizeof(POINT));

	LinePos = m_LineList.GetHeadPosition();

	int nStyle;
	int nWidth;
	POINT ptPrev;

	while(LinePos)
	{
		pLine = m_LineList.GetNext(LinePos);

		if(pLine->GetShow() == FALSE)	continue;
		//
		CPen apen, *oldpen;
		nStyle = pLine->GetType();
		nWidth = pLine->GetWidth();
		if(nStyle == PS_NULL)
		{
			nStyle = PS_SOLID;
		}
		if(pLine->GetSelect())
		{
			nWidth +=2;
		}

		apen.CreatePen(nStyle, nWidth, pLine->GetColor());
		oldpen = pDC->SelectObject(&apen);

		DataPos = pLine->GetDataHead();

		if(DataPos == NULL) continue;
		//
		pre_point = pLine->GetNextData(DataPos);
		PreCheck = IsInMinMax(pre_point);
		
		if(PreCheck)
		{
			temppoint = GetViewPosition(pre_point, viewrect);
			pDC->MoveTo(temppoint);
			if(pLine->IsDataPointMark() || pLine->GetType() == PS_NULL)
			{
//				temppoint = GetViewPosition(pre_point,viewrect);
				pDC->Arc(temppoint.x-3,temppoint.y-3,
					temppoint.x+3,temppoint.y+3,
					temppoint.x,temppoint.y-3,
					temppoint.x,temppoint.y-3);
			}
		}
		
		//Cycle인데 data가 없는 경우
		if(m_XAxis.GetTitle().CompareNoCase("StepNo")==0 && PreCheck && DataPos==NULL)
		{
//			temppoint = GetViewPosition(pre_point,viewrect);
			pDC->Arc(temppoint.x-3,temppoint.y-3,
				     temppoint.x+3,temppoint.y+3,
					 temppoint.x,temppoint.y-3,
					 temppoint.x,temppoint.y-3);
		}

		while(DataPos)
		{
			tmp_point = pLine->GetNextData(DataPos);

			//계단식 표시일 경우 X축 선을 먼저 긋는다.
			if(bStairType)
			{
				cur_point.x = tmp_point.x;
				cur_point.y = pre_point.y;


				//X축 방향 Line
				CurCheck = IsInMinMax(cur_point);
//				if(cur_point.x != pre_point.x)	continue;	//1개의 X 좌표에 2개 이상의 data가 있을 경우 제일 처음 data만 사용

				// " 내부 -> 내부" 인 경우
				if(PreCheck && CurCheck)
				{
					viewPoint = GetViewPosition(cur_point,viewrect);
					
					//2008/10/31 그리기 속도 향상을 위해 X축값이 변하거나 같은 X축에서 1pixcel이상 변화시 라인을 그림 
					if( labs(prePoint.x - viewPoint.x) > 1)	// || labs(prePoint.y - viewPoint.y) > 1)
					{
						if(pLine->GetType() != PS_NULL)
						{
							pDC->LineTo(viewPoint);
						}
						prePoint= viewPoint;
					}
				}
				// " 내부 -> 외부" 인 경우
				else if(PreCheck&&!CurCheck)
				{
					if(pLine->GetType() != PS_NULL)
					{
						pDC->LineTo(GetViewPosition(GetCrossPosition(pre_point,cur_point,TRUE),viewrect));
					}
				}
				// " 외부 -> 내부" 인 경우
				else if(!PreCheck&&CurCheck)
				{
					if(pLine->GetType() != PS_NULL)
					{
						pDC->MoveTo(GetViewPosition(GetCrossPosition(pre_point,cur_point,FALSE),viewrect));
						pDC->LineTo(GetViewPosition(cur_point,viewrect));
					}
						
					if(pLine->IsDataPointMark() || pLine->GetType() == PS_NULL)
					{
						temppoint = GetViewPosition(cur_point,viewrect);
						pDC->Arc(temppoint.x-3,temppoint.y-3,
							temppoint.x+3,temppoint.y+3,
							temppoint.x,temppoint.y-3,
							temppoint.x,temppoint.y-3);
					}

				}
				// " 외부 -> 외부" 인 경우
				else if(!PreCheck && !CurCheck)
				{
					//영역으로 corss하는 선이 있으면 표시한다.
					fltPoint p1, p2;
					if(GetCrossTwoPosition(pre_point,cur_point, p1, p2) && pLine->GetType() != PS_NULL)
					{
						TRACE("Draw point2 (%.3f,%.3f)=>(%.3f,%.3f)\n", pre_point.x, pre_point.y, cur_point.x, cur_point.y);
						pDC->MoveTo(GetViewPosition(p1,viewrect));
						pDC->LineTo(GetViewPosition(p2,viewrect));
					}
				}

				// 한 스텝진행
				PreCheck=CurCheck;
				pre_point.x=cur_point.x;
				pre_point.y=cur_point.y;
			}

			//Y축 방향 Line
			cur_point = tmp_point;
			CurCheck = IsInMinMax(cur_point);
//			if(cur_point.x == pre_point.x)	continue;	//1개의 X 좌표에 2개 이상의 data가 있을 경우 제일 처음 data만 사용

			// " 내부 -> 내부" 인 경우
			if(PreCheck && CurCheck)
			{
				viewPoint = GetViewPosition(cur_point,viewrect);
				
				//2008/10/31 그리기 속도 향상을 위해 X축값이 변하거나 같은 X축에서 3pixcel이상 변화시 라인을 그림 
				if( (!bStairType && (labs(prePoint.x - viewPoint.x) > 3 || labs(prePoint.y - viewPoint.y) > 3)) || 
					( bStairType && (labs(prePoint.x - viewPoint.x) > 1 || labs(prePoint.y - viewPoint.y) > 1)))
				{
					if(pLine->GetType() != PS_NULL)
					{
						pDC->LineTo(viewPoint);
					}
					prePoint= viewPoint;
				}
					
				//Point Data 지점에 O로 표시 
				if(pLine->IsDataPointMark() || pLine->GetType() == PS_NULL)
				{
					pDC->Arc(viewPoint.x-3,viewPoint.y-3,
						viewPoint.x+3,viewPoint.y+3,
						viewPoint.x,viewPoint.y-3,
						viewPoint.x,viewPoint.y-3);
				}
			}
			// " 내부 -> 외부" 인 경우
			else if(PreCheck&&!CurCheck)
			{
				if(pLine->GetType() != PS_NULL)
				{
					pDC->LineTo(GetViewPosition(GetCrossPosition(pre_point,cur_point,TRUE),viewrect));
				}
			}
			// " 외부 -> 내부" 인 경우
			else if(!PreCheck&&CurCheck)
			{
				if(pLine->GetType() != PS_NULL)
				{
					pDC->MoveTo(GetViewPosition(GetCrossPosition(pre_point,cur_point,FALSE),viewrect));
					pDC->LineTo(GetViewPosition(cur_point,viewrect));
				}
					
				if(pLine->IsDataPointMark() || pLine->GetType() == PS_NULL)
				{
					temppoint = GetViewPosition(cur_point,viewrect);
					pDC->Arc(temppoint.x-3,temppoint.y-3,
						temppoint.x+3,temppoint.y+3,
						temppoint.x,temppoint.y-3,
						temppoint.x,temppoint.y-3);
				}

			}
			// " 외부 -> 외부" 인 경우
			else if(!PreCheck && !CurCheck)
			{
				//영역으로 corss하는 선이 있으면 표시한다.
				fltPoint p1, p2;
				if(GetCrossTwoPosition(pre_point,cur_point, p1, p2) && pLine->GetType() != PS_NULL)
				{
//					TRACE("Draw point2 (%.3f,%.3f)=>(%.3f,%.3f)\n", pre_point.x, pre_point.y, cur_point.x, cur_point.y);
					pDC->MoveTo(GetViewPosition(p1,viewrect));
					pDC->LineTo(GetViewPosition(p2,viewrect));
				}
			}

			// 한 스텝진행
			PreCheck=CurCheck;
			pre_point.x=cur_point.x;
			pre_point.y=cur_point.y;
		}

		//
		pDC->SelectObject(oldpen);
		apen.DeleteObject();
	}
}
  
POINT CPlane::GetViewPosition(fltPoint point, RECT viewrect)
{
	POINT viewpoint;
	viewpoint.x = int((float)(viewrect.right-viewrect.left)/(m_XAxis.GetMax()-m_XAxis.GetMin())*(point.x-m_XAxis.GetMin()))+viewrect.left;
	viewpoint.y = viewrect.bottom-int((float)(viewrect.bottom-viewrect.top)/(m_YAxis.GetMax()-m_YAxis.GetMin())*(point.y-m_YAxis.GetMin()));
	return viewpoint;
}

fltPoint CPlane::GetRealPosition(POINT point, RECT viewrect)
{
	fltPoint realpoint;
	realpoint.x = ((m_XAxis.GetMax()-m_XAxis.GetMin())/(float)(viewrect.right-viewrect.left)*(float)(point.x-viewrect.left)+m_XAxis.GetMin());
	realpoint.y = ((m_YAxis.GetMax()-m_YAxis.GetMin())/(float)(viewrect.bottom-viewrect.top)*(float)(viewrect.bottom-point.y)+m_YAxis.GetMin());
	return realpoint;
}

//check value is in scope
BOOL CPlane::IsInMinMax(fltPoint point)
{
	return m_XAxis.GetMin()<=point.x&&point.x<=m_XAxis.GetMax()&&m_YAxis.GetMin()<=point.y&&point.y<=m_YAxis.GetMax();
}
/*
fltPoint CPlane::GetCrossPosition(fltPoint prepoint, fltPoint curpoint, BOOL IsFromInToOut)
{
	fltPoint CrossPoint={0.0f,0.0f};
	//수직선인 경우
	if(fabs(prepoint.x-curpoint.x)<=0.001f){
		CrossPoint.x=curpoint.x;
		//내부->외부
		if(IsFromInToOut){
			if(curpoint.y<=m_YAxis.GetMin()) CrossPoint.y=m_YAxis.GetMin();
			if(curpoint.y>=m_YAxis.GetMax()) CrossPoint.y=m_YAxis.GetMax();
		}
		// 외부->내부
		else{
			if(prepoint.y<=m_YAxis.GetMin()) CrossPoint.y=m_YAxis.GetMin();
			if(prepoint.y>=m_YAxis.GetMax()) CrossPoint.y=m_YAxis.GetMax();
		}
	}
	//수평선인 경우
	else if(fabs(prepoint.y-curpoint.y)<=0.001f){
		CrossPoint.y=curpoint.y;
		//내부->외부
		if(IsFromInToOut){
			if(curpoint.x<m_XAxis.GetMin()) CrossPoint.x=m_XAxis.GetMin();
			if(curpoint.x>m_XAxis.GetMax()) CrossPoint.x=m_XAxis.GetMax();
		}
		// 외부->내부
		else{
			if(prepoint.x<m_XAxis.GetMin()) CrossPoint.x=m_XAxis.GetMin();
			if(prepoint.x>m_XAxis.GetMax()) CrossPoint.x=m_XAxis.GetMax();
		}
	}
	// Slope가 있는 경우
	else{		//최소 내붕
		float Slope = 0.0f;
		// 상단 수평선과 만나는지 체크
		Slope=(m_YAxis.GetMax()-prepoint.y)/(curpoint.y-prepoint.y);
		if(0.0f<=Slope&&Slope<=1.0f)
		{
			CrossPoint.y=m_YAxis.GetMax();
			CrossPoint.x=(1.0f-Slope)*prepoint.x+curpoint.x*Slope;
		}
		else{
			// 우측 수직선과 만나는지 체크
			Slope=(m_XAxis.GetMax()-prepoint.x)/(curpoint.x-prepoint.x);
			if(0.0f<=Slope&&Slope<=1.0f)
			{
				CrossPoint.x=m_XAxis.GetMax();
				CrossPoint.y=(1.0f-Slope)*prepoint.y+curpoint.y*Slope;
			}
			else{
				// 하단 수평선과 만나는지 체크
				Slope=(m_YAxis.GetMin()-prepoint.y)/(curpoint.y-prepoint.y);
				if(0.0f<=Slope&&Slope<=1.0f)
				{
					CrossPoint.y=m_YAxis.GetMin();
					CrossPoint.x=(1.0f-Slope)*prepoint.x+curpoint.x*Slope;
				}
				else{
					// 좌측 수직선과 만나는지 체크
					Slope=(m_XAxis.GetMin()-prepoint.x)/(curpoint.x-prepoint.x);
					if(0.0f<=Slope&&Slope<=1.0f)
					{
						CrossPoint.x=m_XAxis.GetMin();
						CrossPoint.y=(1.0f-Slope)*prepoint.y+curpoint.y*Slope;
					}
				}
			}
		}
	}
	//
	return CrossPoint;
}
*/

//Bug Fix 2006/5/25 KBH
fltPoint CPlane::GetCrossPosition(fltPoint prepoint, fltPoint curpoint, BOOL /*IsFromInToOut*/)
{
//	TRACE("Draw point1 (%.3f,%.3f)=>(%.3f,%.3f)\n", prepoint.x, prepoint.y, curpoint.x, curpoint.y);
	fltPoint CrossPoint={0.0f,0.0f};
	//수직선인 경우
	if(fabs(prepoint.x-curpoint.x)<=0.0001f){
		CrossPoint.x=curpoint.x;
		if(min(prepoint.y, curpoint.y)<= m_YAxis.GetMax() && m_YAxis.GetMax()<= max(prepoint.y, curpoint.y)) 
		{
			CrossPoint.y = m_YAxis.GetMax();
		}
		else //if(min(prepoint.y, curpoint.y)<= m_YAxis.GetMin() && m_YAxis.GetMin()<= max(prepoint.y, curpoint.y)) 
		{
			CrossPoint.y = m_YAxis.GetMin();
		}
		//TRACE("수직선\n");
	}
	//수평선인 경우
	else if(fabs(prepoint.y-curpoint.y)<=0.0001f){
		CrossPoint.y=curpoint.y;
		if(min(prepoint.x, curpoint.x)<= m_XAxis.GetMax() && m_XAxis.GetMax()<= max(prepoint.x, curpoint.x)) 
		{
			CrossPoint.x = m_XAxis.GetMax();
		}
		else if(min(prepoint.x, curpoint.x)<= m_XAxis.GetMin() && m_XAxis.GetMin()<= max(prepoint.x, curpoint.x)) 
		{
			CrossPoint.x = m_XAxis.GetMin();
		}
		//TRACE("수평선\n");
	}
	// Slope가 있는 경우
	else
	{		
		float slop = (curpoint.y-prepoint.y)/(curpoint.x-prepoint.x);
		float offset = curpoint.y - slop * curpoint.x;
		float x, y;

		//최소한 내부에 1개 포인트가 존재 함
		//1. 상단 Y축 Max와 교차 여부 확인
		y = m_YAxis.GetMax();	x = (y-offset)/slop;
		if(	m_XAxis.GetMin() <= x && x <= m_XAxis.GetMax() 
			&& min(prepoint.y, curpoint.y)<= y && y<= max(prepoint.y, curpoint.y))	
		{
			CrossPoint.x = x;	CrossPoint.y = y;
//			TRACE("Y축 상단 교차(%.3f, %.3f)\n", x, y);
		}
		
		//2. 하단 Y축 Min과 교차 여부 확인
		y = m_YAxis.GetMin();	x = (y-offset)/slop;
		if(	m_XAxis.GetMin() <= x && x <= m_XAxis.GetMax()
			&& min(prepoint.y, curpoint.y)<= y && y<= max(prepoint.y, curpoint.y))	
		{
			CrossPoint.x = x;	CrossPoint.y = y;
//			TRACE("Y축 하단 교차(%.3f, %.3f)\n", x, y);
		}

		//3. 좌측 X축 Min과 교차 여부 확인
		x= m_XAxis.GetMin();	y = slop * x + offset;
		if(	m_YAxis.GetMin() <= y && y <= m_YAxis.GetMax()
			&& min(prepoint.x, curpoint.x)<= x && x<= max(prepoint.x, curpoint.x))	
	
		{
			CrossPoint.x = x;	CrossPoint.y = y;
//			TRACE("X축 좌측 교차(%.3f, %.3f)\n", x, y);
		}

		//4. 우측 X축 Max과 교차 여부 확인
		x= m_XAxis.GetMax();	y = slop * x + offset;
		if(	m_YAxis.GetMin() <= y && y <= m_YAxis.GetMax()	
			&& min(prepoint.x, curpoint.x)<= x && x<= max(prepoint.x, curpoint.x))	
		{
			CrossPoint.x = x;	CrossPoint.y = y;
//			TRACE("X축 우측 교차(%.3f, %.3f)\n", x, y);
		}
	}
	return CrossPoint;
}

//두지점이 그래프 영역의 경계점과 만나는 두지점을 찾는다.
BOOL CPlane::GetCrossTwoPosition(fltPoint prepoint, fltPoint curpoint, fltPoint &crsPoint1, fltPoint &crsPoint2)
{
	//수직선인 경우

	if(fabs(prepoint.x-curpoint.x) <= 0.0001f)
	{
		//변화가 없으면 선이 생성될수 없다. or 동일 좌표에 다른값이 2개 존재 할 수 없다.
		if(prepoint.x == curpoint.x)			return FALSE;
		
		if( m_XAxis.GetMin() < curpoint.x && curpoint.x< m_XAxis.GetMax())
		{
			crsPoint1.x = curpoint.x;	crsPoint2.x = curpoint.x;
			crsPoint1.y = m_YAxis.GetMin();	crsPoint2.y = m_YAxis.GetMax();	
//			TRACE("수직선\n");
			return TRUE;
		}
		return FALSE;
	}
	//수평선인 경우
	else if(fabs(prepoint.y-curpoint.y) <= 0.0001f)
	{	
		if( m_YAxis.GetMin() < curpoint.y && curpoint.y < m_YAxis.GetMax())
		{
			crsPoint1.y =curpoint.y;	crsPoint2.y =curpoint.y; 
			crsPoint1.y = m_XAxis.GetMin();	crsPoint2.y = m_XAxis.GetMax(); 
//			TRACE("수평선\n");
		}
		return FALSE;
	}
	// Slope가 있는 경우
	else
	{		
		float slop = (curpoint.y-prepoint.y)/(curpoint.x-prepoint.x);
		float offset = curpoint.y - slop * curpoint.x;
		float x, y;
		int nCount = 0;
		fltPoint fltCross[4];

		//최소한 내부에 1개 포인트가 존재 함
		//1. 상단 Y축 Max와 교차 여부 확인
		y = m_YAxis.GetMax();	x = (y-offset)/slop;
		if(	m_XAxis.GetMin() <= x && x <= m_XAxis.GetMax()
			&& min(prepoint.y, curpoint.y)<= y && y<= max(prepoint.y, curpoint.y))	
		{
			fltCross[nCount].x = x;	fltCross[nCount].y = y;
			nCount++;
//			TRACE("Y축 상단 교차(%.3f, %.3f)\n", x, y);
		}
		
		//2. 하단 Y축 Min과 교차 여부 확인
		y = m_YAxis.GetMin();	x = (y-offset)/slop;
		if(	m_XAxis.GetMin() <= x && x <= m_XAxis.GetMax()
			&& min(prepoint.y, curpoint.y)<= y && y<= max(prepoint.y, curpoint.y))	
		{
			fltCross[nCount].x = x;	fltCross[nCount].y = y;
			nCount++;
//			TRACE("Y축 하단 교차(%.3f, %.3f)\n", x, y);
		}

		//3. 좌측 X축 Min과 교차 여부 확인
		x= m_XAxis.GetMin();	y = slop * x + offset;
		if(	m_YAxis.GetMin() <= y && y <= m_YAxis.GetMax()
			&& min(prepoint.x, curpoint.x)<= x && x<= max(prepoint.x, curpoint.x))	
		{
			fltCross[nCount].x = x;	fltCross[nCount].y = y;
			nCount++;
//			TRACE("X축 좌측 교차(%.3f, %.3f)\n", x, y);
		}

		//4. 우측 X축 Max과 교차 여부 확인
		x= m_XAxis.GetMax();	y = slop * x + offset;
		if(	m_YAxis.GetMin() <= y && y <= m_YAxis.GetMax()
			&& min(prepoint.x, curpoint.x)<= x && x<= max(prepoint.x, curpoint.x))	
		{
			fltCross[nCount].x = x;	fltCross[nCount].y = y;
			nCount++;
//			TRACE("X축 우측 교차(%.3f, %.3f)\n", x, y);
		}
		//최소 2개 지점이 교차한다.
		if(nCount < 2)	return FALSE;

		crsPoint1.x = fltCross[0].x;
		crsPoint1.y = fltCross[0].y;
		crsPoint2.x = fltCross[1].x;
		crsPoint2.y = fltCross[1].y;

	}
//	TRACE("Draw point (%.3f,%.3f)=>(%.3f,%.3f)\n", prepoint.x, prepoint.y, curpoint.x, curpoint.y);
	TRACE("Two cross point (%.3f, %.3f), (%.3f, %.3f)\n", crsPoint1.x, crsPoint1.y, crsPoint2.x, crsPoint2.y);
	return TRUE;
}


void CPlane::AutoScaleYAxis(int iNumOfPlanes)
{
//	if(!m_bNewScale) return;
	//
	fltPoint MaxValue={0.0f,0.0f}, MinValue={0.0f,0.0f};
	if(GetMinMaxValue(MinValue, MaxValue))
	{
		float fPoint;
		//
		if(MinValue.x==MaxValue.x)
		{
			MinValue.x-=1;
			MaxValue.x+=1;
		}
		if(MinValue.y==MaxValue.y)
		{
			MinValue.y-=1;
			MaxValue.y+=1;
		}
		//
		// X Axis
		//
//		m_XAxis.SetMinMax(MinValue.x, MaxValue.x, (MaxValue.x-MinValue.x)/10.0f);
		if(0.0f < MinValue.x && MinValue.x < 0.3f)	MinValue.x = 0.0f;		//0 ~ 0.3f사이일 경우 0부터 그리도록 수정 

		fPoint = GetAutoGridGap(MinValue.x, MaxValue.x);

		if(m_XAxis.GetTitle() == "StepNo")
		{
			if(MaxValue.x-MinValue.x < 10)	fPoint = 1.0f;
			else					fPoint = int(fPoint);
		}
		
		
		m_XAxis.SetMinMax(MinValue.x, MaxValue.x, fPoint);

		//
		// Y Axis
		//
		BOOL bSubset = FALSE;
		if(bSubset)	//축을 분리하여 표시 
		{
			m_YAxis.SetMinMax(MinValue.y-(MaxValue.y-MinValue.y)*(float)(iNumOfPlanes-1-m_iSerialNo),
				              MaxValue.y+(MaxValue.y-MinValue.y)*(float)m_iSerialNo,
							  (MaxValue.y-MinValue.y)/5.0f);
		}
		else		//한개의 축으로 표시 
		{
			if(MaxValue.y - MinValue.y < 0.001)
			{
				MaxValue.y += (fabs(MaxValue.y/100.0f));
				MinValue.y -= (fabs(MinValue.y/100.0f));
			}
			float fPrevMinVal = MinValue.y;
			
			//2008/10/30 KBH//////////////////////////////////////////////////////////////////////////
			//무조건 8% 마진이 아니라(소숫점 발생)
			//MinValue.y 보다 작은 수에서 가장 주눈금 간격 숫자로 Y 축의 시작 좌표를 구함 
			
//			float fYMargin = (MaxValue.y - MinValue.y)*0.08f;	//Min Max폭의 8% 공간을 상/하에 준다.
//			MinValue.y -= fYMargin;
//			MaxValue.y += fYMargin;

			fPoint = GetAutoGridGap(MinValue.y, MaxValue.y);
			
			// kky0703
			// MinValue.y  = int(MinValue.y/fPoint*fPoint);
			MinValue.y  = int(MinValue.y/fPoint)*int(fPoint);
			// MinValue.y  = int(MinValue.y/fPoint*fPoint);
			MaxValue.y  = int(MaxValue.y/fPoint)*int(fPoint);
			MinValue.y -= (fPoint);		//하부에 주표시 눈금 간격의 0~1배 여유 공간 배치
			MaxValue.y += (fPoint*2);	//상부에 주표시 눈금 간격의 1~2배 여유 공간 배치 
			//////////////////////////////////////////////////////////////////////////

			m_YAxis.SetMinMax(MinValue.y, MaxValue.y, fPoint);
//			m_YAxis.SetMinMax(MinValue.y, MaxValue.y, (MaxValue.y - MinValue.y)/10.0f);
			
			TRACE("Set Y Axis Min Max :%f, %f, %f\n", MinValue.y, MaxValue.y, fPoint);
		}
		
//
//		TRACE("SetMinMax %d: (%f, %f), (%f, %f, %f)\n", m_iSerialNo, MinValue.y, MaxValue.y, 
//				MinValue.y-(MaxValue.y-MinValue.y)*(float)(iNumOfPlanes-1-m_iSerialNo),
//				MaxValue.y+(MaxValue.y-MinValue.y)*(float)m_iSerialNo, (MaxValue.y-MinValue.y)/5.0f);
	}

//	m_bNewScale = FALSE;
}

BOOL CPlane::GetMinMaxValue(fltPoint& MinValue, fltPoint& MaxValue)
{
	//
	POSITION pos = m_LineList.GetHeadPosition();
	if(pos==NULL) return FALSE;

	//
	BOOL bInit = TRUE;
	while(pos)
	{
		CLine* pLine = m_LineList.GetNext(pos);
		if(pLine->GetMinMaxValue(MinValue, MaxValue, bInit)) bInit = FALSE;
	}
	
	//
	return TRUE;
}

BOOL CPlane::SetColorOption(BOOL IsEven)
{
	CString path = _T("");
	path = AfxGetApp()->GetProfileString("Config" ,"Path", "");
	if( path.IsEmpty() )
	{
		TCHAR szBuff[_MAX_PATH];
		::GetModuleFileName(AfxGetApp()->m_hInstance,szBuff,_MAX_PATH);
		path = szBuff;
	}

	path = path.Left(path.ReverseFind('\\'))+"\\DataBase\\"+FORM_SET_DATABASE_NAME;
		
	CDaoDatabase *pDB=NULL;
	CDaoRecordset *pRecSet=NULL;

	TRY{
		pDB=new CDaoDatabase;
		pDB->Open(path);
			//
			pRecSet=new CDaoRecordset(pDB);
			//
			pRecSet->Open(dbOpenTable,"LineColor");
				pRecSet->MoveLast();
				m_iNumOfLineColors = pRecSet->GetRecordCount();
				m_pLineColor = new COLORREF[m_iNumOfLineColors];

				if(!IsEven) pRecSet->MoveFirst();
				else       pRecSet->MoveLast();
				
				for(int i=0;i<m_iNumOfLineColors;i++){
					//
					COleVariant red,green,blue;
					pRecSet->GetFieldValue("Red"  ,red  );
					pRecSet->GetFieldValue("Green",green);
					pRecSet->GetFieldValue("Blue" ,blue );
					m_pLineColor[i]=RGB(red.bVal,green.bVal,blue.bVal);
					//
					if(!IsEven) 
						pRecSet->Move(1);
					else       
						pRecSet->Move(-1);
				}
			pRecSet->Close();

			//Y 축 Color
			//재일 첫번째 Data(채널)은 Y축 색깔과 같은 색으로 그린다.
			if(m_YAxis.GetTitle() == "Voltage")
			{
				m_YAxis.SetLabelColor(RGB(0, 0, 255));
				m_pLineColor[0] = RGB(0, 0, 255);
			}	
			else if(m_YAxis.GetTitle() == "Current")
			{
				m_YAxis.SetLabelColor(RGB(255, 0, 0));
				m_pLineColor[0] = RGB(255, 0, 0);
			}	
			else if(m_YAxis.GetTitle() == "Capacity")
			{
				m_YAxis.SetLabelColor(RGB(0, 255, 0));
				m_pLineColor[0] = RGB(0, 255, 0);
			}	
			else if(m_YAxis.GetTitle() == "Power")
			{
				m_YAxis.SetLabelColor(RGB(0, 128, 128));
				m_pLineColor[0] = RGB(0, 128, 128);
			}
			else if(m_YAxis.GetTitle() == "WattHour")
			{
				m_YAxis.SetLabelColor(RGB(128, 128, 0));
				m_pLineColor[0] = RGB(128, 128, 0);
			}	
			else // default color
			{
				m_YAxis.SetLabelColor(RGB(0, 0, 0));
				m_pLineColor[0] = RGB(0, 0, 0);
			}
			
			//X 축 color
			m_XAxis.SetLabelColor(RGB(128, 128, 128));

			//
/*			//Commented By KBH 2005/09/08
			//배경은 흰색을 사용
			pRecSet->Open(dbOpenTable,"BackColor");
				//
			COleVariant red,green,blue;
			pRecSet->GetFieldValue("Red"  ,red  );
			pRecSet->GetFieldValue("Green",green);
			pRecSet->GetFieldValue("Blue" ,blue );
			m_GridColor=RGB(red.bVal,green.bVal,blue.bVal);

//Commanted by KBH 2005/4/20
//Grid 색을 자동 보색이 아니라 설정 가능 하도록 수정 
//				m_GridColor=RGB(255-red.bVal,255-green.bVal,255-blue.bVal);

				pRecSet->Close();
*/			//
			delete pRecSet;
		pDB->Close();
		delete pDB;
	}
	CATCH(CDaoException,e){
		e->ReportError();
		return FALSE;
	}
	AND_CATCH(CMemoryException,e){
		e->ReportError();
		return FALSE;
	}
	AND_CATCH(CException,e){
		e->ReportError();
		return FALSE;
	}
	END_CATCH
	//
	return TRUE;
}

COLORREF CPlane::GetNextLineColor()
{	
	COLORREF color = m_pLineColor[m_iCurLineColorIndex];
	m_iCurLineColorIndex++;
	if(m_iCurLineColorIndex>=m_iNumOfLineColors) m_iCurLineColorIndex = 0;
	return color;
}

void CPlane::DrawAColumnData(CDC* pDC, CList<float,float&>* pList, CRect ClientRect)
{
	//
	CLine* pLine = new CLine("",0,0,RGB(255,255,0),"");
	POSITION pos = pList->GetHeadPosition();
	float fltX = 0.0f;
	while(pos)
	{
		float fltY = pList->GetNext(pos);
		//
		fltPoint pt;
		pt.x = fltX;
		pt.y = fltY;
		pLine->AddData(pt);
		fltX += 0.020f;
	}
	//
	AddLine(pLine);
	//
	m_BackColor = RGB(0, 0, 64);
	m_GridColor = RGB(192, 192, 255);
	m_iSerialNo = 0;
	//
	TEXTMETRIC XAxisTextMetric;
	{
		CFont afont, *oldfont;
		afont.CreateFontIndirect(m_XAxis.GetFont());
		oldfont=pDC->SelectObject(&afont);
			pDC->GetTextMetrics(&XAxisTextMetric);
		pDC->SelectObject(oldfont);
		afont.DeleteObject();
	}
	//
/*	DrawScopeInView(pDC,
		            XAxisTextMetric.tmAveCharWidth*10,
				    XAxisTextMetric.tmHeight+XAxisTextMetric.tmExternalLeading*2,
					ClientRect.left,
				    ClientRect,
					TRUE);

	DrawLineInView (pDC,
		            XAxisTextMetric.tmAveCharWidth*10,
				    XAxisTextMetric.tmHeight+XAxisTextMetric.tmExternalLeading*2,
				    ClientRect);
*/}

void CPlane::ZoomIn(fltPoint min, fltPoint max)
{
	if(max.x - min.x < 0.0001f)		return;
	if(max.y - min.y < 0.0001f)	
	{
		TRACE("Can't zoom1. (%f, %f)\n", max.y, min.y);
		return;
	}

	//Scope 영역을 확인 한다.
	if(max.x < m_XAxis.GetMin() || min.x > m_XAxis.GetMax())	return;
	if(max.y < m_YAxis.GetMin() || min.y > m_YAxis.GetMax())	return;
	if(min.x < m_XAxis.GetMin())	min.x = m_XAxis.GetMin();
	if(max.x > m_XAxis.GetMax())	max.x = m_XAxis.GetMax();
	if(max.y < m_YAxis.GetMin())	max.y = m_YAxis.GetMin();
	if(min.y > m_YAxis.GetMax())	min.y = m_YAxis.GetMax();


	float yGridGap  = GetAutoGridGap(min.y, max.y);
//	float xGridGap = (max.x - min.x)/10.0f;
	float xGridGap = GetAutoGridGap( min.x, max.x);

	//float변수의 분해능으로 인해 Min Max차의 단위가 Minmax와 5자리 이상 차이나면 안된다.
	// => ex) y1 : 2.12345*10^6 y2: 2.12345*10^6 => y2-y1 = 0.5 일 경우  y1에 0.5를 무한대로 더해도 y2에 도달하지 못함
	//두 수중 자리수가 작은 수보다 6자리 이상 작을 경우 Zoom을 하지 못하도록 한다.
	//즉 여섯자리 10^6 이상의 Zoom은 안됨 
	if(min(fabs(max.y), fabs(min.y)) - fabs(yGridGap)*1000000 > 0)
	{
		TRACE("Can't zoom2. (%f, %f)\n", min(fabs(max.y), fabs(min.y)), fabs(yGridGap));
		return;
	}

	if(min(fabs(max.x), fabs(min.x)) - fabs(xGridGap) * 10000000 > 0)
	{
		TRACE("Can't zoom3. (%f, %f)\n", min(fabs(max.x), fabs(min.x)), xGridGap);
		return;
	}

	m_XAxis.AddMinMaxBuffer(min.x, max.x);
	m_YAxis.AddMinMaxBuffer(min.y, max.y);

	m_XAxis.SetMinMax(min.x, max.x, GetAutoGridGap(min.x, max.x));
//	m_XAxis.SetMinMax(min.x, max.x, (max.x - min.x)/10.0f);
	
	m_YAxis.SetMinMax(min.y, max.y, GetAutoGridGap(min.y, max.y));
//	m_YAxis.SetMinMax(min.y, max.y, (max.y - min.y)/10.0f);

	TRACE("Zoom in X(%.3f~%.3f), Y(%.3f~%.3f)\n", min.x, max.x, min.y, max.y );
}

void CPlane::ZoomOut()
{	
	//Zoom이 되어 있는 상태에서 Y축 Item을 추가 하게 되면 ZoomOff 되어야 한다.
	fltPoint pt;
	if(!m_XAxis.IsMinMaxBufferEmpty())
	{
		pt = m_XAxis.GetRecentMinMax();
		m_XAxis.SetMinMax(pt.x, pt.y,  GetAutoGridGap(pt.x , pt.y));
	//	m_XAxis.SetMinMax(pt.x, pt.y,  (pt.y - pt.x)/10.0f);
	}

	if(!m_YAxis.IsMinMaxBufferEmpty())
	{
		pt = m_YAxis.GetRecentMinMax();
		m_YAxis.SetMinMax(pt.x, pt.y, GetAutoGridGap(pt.x, pt.y));
	//	m_YAxis.SetMinMax(pt.x, pt.y, (pt.y - pt.x)/10);
	}
}

BOOL CPlane::IsZoomInMode()
{
	return !m_XAxis.IsMinMaxBufferEmpty();
}

void CPlane::ZoomOff()
{
	while(!m_XAxis.IsMinMaxBufferEmpty())
	{
		fltPoint pt = m_XAxis.GetRecentMinMax();
		m_XAxis.SetMinMax(pt.x, pt.y, GetAutoGridGap(pt.x , pt.y));
//		m_XAxis.SetMinMax(pt.x, pt.y, (pt.y - pt.x)/10);
	}

	while(!m_YAxis.IsMinMaxBufferEmpty())
	{
		fltPoint pt = m_YAxis.GetRecentMinMax();
		m_YAxis.SetMinMax(pt.x, pt.y, GetAutoGridGap(pt.x, pt.y));
		//m_YAxis.SetMinMax(pt.x, pt.y, (pt.y - pt.x)/10);
	}
}

BOOL CPlane::GetModeListOfLines(CList<BYTE, BYTE&>* pbylistMode, LPCTSTR strYAxisName)
{
	if(m_LineList.GetCount()==0) return FALSE;
	//
	POSITION pos = m_LineList.GetHeadPosition();
	while(pos)
	{
		CLine* pLine = m_LineList.GetNext(pos);
		if(pLine->GetAxisName().CompareNoCase(strYAxisName)==0)
		{
			BYTE mode = pLine->GetMode();
			if(pbylistMode->Find(mode)==NULL) pbylistMode->AddTail(mode);
		}
	}
	return TRUE;
}


//dest Point 부근으로 바운더리(nBoundary) 안에 있는지 확인
//		 ---------------
//		|				|
//		|	   * //dest	|
//		|				|
//		 ---------------
int IsLinearInPoint(POINT sourcePoint, POINT destPoint, int nBoundary)
{
	if( destPoint.x+nBoundary < sourcePoint.x || destPoint.x-nBoundary > sourcePoint.x )
		return 0;
	else if( destPoint.y+nBoundary < sourcePoint.y || destPoint.y-nBoundary > sourcePoint.y )
		return 0;
	else
		return 1;
}

//prevPoint => nextPoint로 가는 사이에 destPoint가 존재하는지 확인 검사 
//지정 범위 안에 있느지 확인 
int IsLinearInPoint(RECT rect, POINT prevPoint, POINT nextPoint, POINT destPoint)
{
	if( destPoint.x < prevPoint.x || destPoint.x > nextPoint.x )	//dest는 prev와 next 사이에 있어야 한다.
		return 0;

//	TRACE("P2(%d, %d)<<>>X(%d, %d)\n", nextPoint.x, nextPoint.y, destPoint.x, destPoint.y);

	long lHeight=rect.bottom-rect.top;
	float X1=(float)prevPoint.x;	
	float X2=(float)nextPoint.x;
	float Y1=(float)lHeight-(float)prevPoint.y;
	float Y2=(float)lHeight-(float)nextPoint.y;
	float fX=(float)destPoint.x;
	float fY=(float)lHeight-(float)destPoint.y;

	long DETECT_BOUNDARY = 5;

	//X축 직선
	if( Y2-Y1 ==0 ){
		if(fY <= Y1+DETECT_BOUNDARY && fY >= Y1-DETECT_BOUNDARY)
			return 1;
		return 0;
	}

	//Y축 직선
	if( X2-X1 > -5 && X2-X1 < 5 ) 
	{
		if(Y1<Y2)
		{
			if(fY <= Y2+DETECT_BOUNDARY && fY >= Y1-DETECT_BOUNDARY)
				return 1;
		}
		else
		{
			if(fY <= Y1+DETECT_BOUNDARY && fY >= Y2-DETECT_BOUNDARY)
				return 1;
		}
		return 0;
	}

	float A = (Y2-Y1)/(X2-X1);
	float B = Y1-(A*X1);

	float fltSource = fX * A + B;

//	TRACE("P1(%.f, %.f) P2(%.f, %.f) %.2f = A(%.4f)*%.f + B(%.3f)\t",
//		X1, Y1, X2, Y2, fltSource, A, fX, B);

//	TRACE("Y:%.f - In (%.f ~ %.f)\n", fltSource, fY-DETECT_BOUNDARY, fY+DETECT_BOUNDARY);
	if( fltSource <= fY+DETECT_BOUNDARY && 
		fltSource >= fY-DETECT_BOUNDARY )
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

//int CPlane::IsPointInLine(CDC* pDC, 
//						  int LeftSpace, 
//						  int TopSpace, 
//						  CRect ClientRect, 
//						  POINT sourcepoint)


//Source point에 해당하는 data가 존재하는 라인번호를 구함
//bPointMode : TRUE  =>	Data 포인트가 인근 검색
//bPointMode : FALSE => Data 포인트와 포인트와 포인트간도 검색 
int CPlane::IsPointInLine(CRect ClientRect, POINT sourcepoint)
{
	RECT viewrect;
	viewrect = ClientRect;

	int		nLineIndex=0;
	//
	CLine* pLine;
	POSITION DataPos;
	POSITION LinePos = m_LineList.GetHeadPosition();
	
	fltPoint pre_point, cur_point;
	POINT	prevPoint, curPoint;
	BOOL	PreCheck, CurCheck;

	while(LinePos)
	{
		pLine = m_LineList.GetNext(LinePos);
		//
		DataPos = pLine->GetDataHead();
		if(DataPos == NULL) 		continue;

		//
		pre_point = pLine->GetNextData(DataPos);				//첫번째 data
		prevPoint = GetViewPosition(pre_point, viewrect);		//첫번째 data의 View에서의 위치 

		PreCheck = IsInMinMax(pre_point);						//첫번째가 영역안에 있는지 확인		
/*		if(PreCheck) 
		{
			//TRACE("P1(%d,%d)->P2(%d,%d)\n", prevPoint.x, prevPoint.y,sourcepoint.x,sourcepoint.y);
			if( IsLinearInPoint(viewrect, prevPoint, prevPoint, sourcepoint) > 0)
			//if( IsLinearInPoint(prevPoint, sourcepoint, 5) > 0)
				return nLineIndex;
		}
*/		
		if(m_XAxis.GetTitle().CompareNoCase("StepNo")==0 && PreCheck && DataPos == NULL)
		{
			//source point가 prevPoint에서 상하좌우 10 Pixel 범위에 있는지 확인
			if( IsLinearInPoint(prevPoint, sourcepoint, 5) > 0)
			{
				return nLineIndex;
			}
		}

		//
		while(DataPos)
		{
			cur_point = pLine->GetNextData(DataPos);
			CurCheck = IsInMinMax(cur_point);
			curPoint = GetViewPosition(cur_point, viewrect);
			
				// " 내부 -> 내부" 인 경우
			if(PreCheck&&CurCheck)
			{
				if(m_XAxis.GetTitle().CompareNoCase("StepNo")==0)	//Data Point와 Matching되는지 검사 
				{
					if( IsLinearInPoint(curPoint, sourcepoint, 5) > 0)	//두 포인트가 10 Pixel 범위안에 있는지 검사 
					{
						return nLineIndex;
					}
				}
				else 
				{
					if(IsLinearInPoint(viewrect, prevPoint, curPoint, sourcepoint) > 0)
						return nLineIndex;
				}
			}
			// " 내부 -> 외부" 인 경우
			else if(PreCheck&&!CurCheck)
			{
				if(IsLinearInPoint(viewrect, prevPoint, 
								   GetViewPosition(GetCrossPosition(pre_point,cur_point,TRUE),viewrect), 
								   sourcepoint) > 0)
					return nLineIndex;
			}
			// " 외부 -> 내부" 인 경우
			else if(!PreCheck&&CurCheck)
			{
				if(IsLinearInPoint(viewrect,
						GetViewPosition(GetCrossPosition(pre_point,cur_point,FALSE),viewrect),
						GetViewPosition(cur_point,viewrect),
						sourcepoint)>0)
					return nLineIndex;
				
				if(m_XAxis.GetTitle().CompareNoCase("StepNo")==0)	//Data Point와 Matching되는지 검사 
				{
					if( IsLinearInPoint(prevPoint, sourcepoint, 5) > 0)	//두 포인트가 10 Pixel 범위안에 있는지 검사 
					{
						return nLineIndex;
					}
				}
			}

			// 한 스텝진행
			PreCheck=CurCheck;
			pre_point.x=cur_point.x;
			pre_point.y=cur_point.y;

			prevPoint = GetViewPosition(pre_point, viewrect);
		}
		nLineIndex++;
	}
	return -1;
}

//Source point에 가장 가까운 라인 번호와 data Point를 구함
int CPlane::IsPointInPoint(CRect ClientRect, POINT sourcepoint, fltPoint &fltDataPoint)
{
	RECT viewrect = ClientRect;
	int		nLineIndex=0;
	//	
	fltPoint  cur_point;
	POINT	 curPoint;

	int nSelectedLineIndex = -1;		//선택된 Line 중에서 가장 처음 Match되는 라인 번호
	int nFirstLineIndex = -1;			//가장 먼저 match되는 번호 
	fltPoint fltSelectData = {0.0f, 0.0f};
	fltPoint fltFirstData = {0.0f, 0.0f};

	CLine* pLine;
	POSITION DataPos;
	POSITION LinePos = m_LineList.GetHeadPosition();
	while(LinePos)
	{
		pLine = m_LineList.GetNext(LinePos);
		DataPos = pLine->GetDataHead();
		while(DataPos)
		{
			cur_point = pLine->GetNextData(DataPos);
			curPoint = GetViewPosition(cur_point, viewrect);
			if(IsInMinMax(cur_point))
			{
				if( IsLinearInPoint(curPoint, sourcepoint, 5) > 0)	//두 포인트가 5 Pixel 범위안에 있는지 검사 
				{
					//우선순위를 
					//1. Data Point Mark 된 라인
					if(pLine->IsDataPointMark())
					{
						fltDataPoint.x = cur_point.x;
						fltDataPoint.y = cur_point.y;
						return nLineIndex;
					}

					//2. Select 된 라인
					if(pLine->GetSelect() && nSelectedLineIndex < 0)
					{
						fltSelectData.x = cur_point.x;
						fltSelectData.y = cur_point.y;
						nSelectedLineIndex = nLineIndex;
					}	
					
					//3. 가장 첫번째 라인 순으로 한다.
					if(nFirstLineIndex < 0)
					{
						fltFirstData.x = cur_point.x;
						fltFirstData.y = cur_point.y;
						nFirstLineIndex = nLineIndex;
					}
				}
			}
		}
		nLineIndex++;
	}
	
	//Match되는 Mark된 Data가 없을 경우 
	//2. Select 된 라인
	if(nSelectedLineIndex >= 0)
	{
		fltDataPoint.x = fltSelectData.x;
		fltDataPoint.y = fltSelectData.y;
		return nSelectedLineIndex;
	}

	//3. 가장 첫번째 라인 순으로 한다.
	if(nFirstLineIndex >= 0)
	{
		fltDataPoint.x = fltFirstData.x;
		fltDataPoint.y = fltFirstData.y;
		return nFirstLineIndex;
	}

	return -1;
}


//자리수가 딱 떨어지는 Grid 간격을 구한다.
//2005/5/23 KBH
float CPlane::GetAutoGridGap(float fMin, float fMax)
{
	int interval = int((fMax - fMin)/8.0f);		//8개 내외로 표시
	int n = 0;
	while(1 && interval >= 10)		//10의 승수를 구한다.
	{
		interval = interval /10;
		n++;
		if(interval < 10)
		{
			break;
		}
	}
	
	//가장 높은 자리로 자른다.
	//1=>1, 2=>2, 3~7=>5, 8~9=>10
	if(interval >= 3 && interval <= 7)	interval =  5;
	else if(interval > 7)	interval = 10;
			
	int c =1;
	for(int d = 0; d < n; d++)
	{
		c = c * 10;
	}

	interval = interval * c;
	if(interval < 1)	
		return (fMax - fMin)/8.0f;

	
	return float(interval);
}

//X축을 그림
void CPlane::DrawXAxis(CDC *pDC, CRect ScopeRect, BOOL bScope)
{
	// x축의 Label 및 Grid Line을 그린다.
	CPen   apen, *oldpen;
	CFont afont, *oldfont;
	POINT pt;
	fltPoint fltpoint_s, fltpoint_e;
	float fTemp;
	CString strXLable;          
	
	TEXTMETRIC XAxisTextMetric;
	afont.CreateFontIndirect(m_XAxis.GetFont());
	oldfont=pDC->SelectObject(&afont);
	pDC->GetTextMetrics(&XAxisTextMetric);

	COLORREF oldTextColor = pDC->SetTextColor(m_XAxis.GetLabelColor());

	apen.CreatePen(PS_SOLID,2, m_GridColor);

	oldfont=pDC->SelectObject(&afont);
	oldpen =pDC->SelectObject(&apen);                    
			
	strXLable.Format("%s(%s)", m_XAxis.GetTitle(),  m_XAxis.GetUnitNotation());

	pDC->SetTextAlign(TA_TOP|TA_RIGHT);
//	pDC->TextOut(	ScopeRect.right,				// 우측 붙이고 바닥에서 1줄 위치에 표시한다.
//					ClientRect.bottom-XAxisTextMetric.tmHeight-XAxisTextMetric.tmExternalLeading,
//					strXLable);

	pDC->TextOut(	ScopeRect.right,				// 우측 붙이고 바닥에서 1줄 위치에 표시한다.
					ScopeRect.bottom+(XAxisTextMetric.tmHeight+XAxisTextMetric.tmExternalLeading)*2,
					strXLable);

	float fltStart = m_XAxis.GetMin();
	float fltMiniGrid = m_XAxis.GetGrid()/5.0f;
	if( m_XAxis.GetMin() < 0 && 0 < m_XAxis.GetMax())	//0을 포함하고 있으면 0부터 시작해서 라인을 그린다.
 	{
		//0 ~ Min() 구간 누금을 그린다.
		for(float iter = 0.0f; iter>=m_XAxis.GetMin(); iter-=m_XAxis.GetGrid())
		{
			fltpoint_s.x = iter;	fltpoint_s.y = m_YAxis.GetMin();
			//Grid와 Grid사이에 5칸의 작은 표시 설정
			pt = GetViewPosition(fltpoint_s,ScopeRect);
			pDC->MoveTo(pt);
			pDC->LineTo(pt.x, pt.y-10);
			
			CPen miniPen, *pMiniOld;
			miniPen.CreatePen(PS_SOLID, 1, m_GridColor);
			pMiniOld =pDC->SelectObject(&miniPen);                    
			//Grid와 Grid사이에 5칸의 작은 표시 설정
			for(int i=0; i<5; i++)
			{
				fTemp = iter+i*fltMiniGrid;
				if(fTemp > m_XAxis.GetMax())	break;
				fltpoint_s.x = fTemp;
				fltpoint_s.y = m_YAxis.GetMin();
				pt = GetViewPosition(fltpoint_s, ScopeRect);
				pDC->MoveTo(pt);
				pDC->LineTo(pt.x, pt.y-7);	
			}
			pDC->SelectObject(pMiniOld);
			miniPen.DeleteObject();
		}
		fltStart = 0.0f;
	}

	//Min() or 0 ~ Max() 구간 그림
	for(float iter = fltStart; iter<=m_XAxis.GetMax(); iter+=m_XAxis.GetGrid())
	{
		fltpoint_s.x = iter;	fltpoint_s.y = m_YAxis.GetMin();
		//Grid와 Grid사이에 5칸의 작은 표시 설정
		pt = GetViewPosition(fltpoint_s, ScopeRect);
		pDC->MoveTo(pt);
		pDC->LineTo(pt.x, pt.y-10);
		
		CPen miniPen, *pMiniOld;
		miniPen.CreatePen(PS_SOLID, 1, m_GridColor);
		pMiniOld =pDC->SelectObject(&miniPen);                    
		//Grid와 Grid사이에 5칸의 작은 표시 설정
		for(int i=0; i<5; i++)
		{
			fTemp = iter+i*fltMiniGrid;
			if(fTemp > m_XAxis.GetMax())	break;
			fltpoint_s.x = fTemp;
			fltpoint_s.y = m_YAxis.GetMin();
			pt = GetViewPosition(fltpoint_s, ScopeRect);
			pDC->MoveTo(pt);
			pDC->LineTo(pt.x, pt.y-7);	
		}
		pDC->SelectObject(pMiniOld);
		miniPen.DeleteObject();
	}
	pDC->SelectObject(oldpen);
	apen.DeleteObject();


	//X Axis grid draw
	apen.CreatePen(PS_DOT,1,m_XAxis.GetLabelColor());
	oldpen =pDC->SelectObject(&apen);                    
		
	fltStart = m_XAxis.GetMin();
	if( m_XAxis.GetMin() < 0 && 0 < m_XAxis.GetMax())	//0을 포함하고 있으면 0부터 시작해서 라인을 그린다.
	{
		//Draw Min() ~ 0
		for( iter = 0.0f; iter>=m_XAxis.GetMin(); iter-=m_XAxis.GetGrid())
		{
			// Grid Line
			fltpoint_s.x = iter;	fltpoint_s.y = m_YAxis.GetMin();
			fltpoint_e.x = iter;	fltpoint_e.y = m_YAxis.GetMax();
			if(m_XAxis.IsShowGrid())
			{
				pDC->MoveTo(GetViewPosition(fltpoint_s, ScopeRect));
				pDC->LineTo(GetViewPosition(fltpoint_e, ScopeRect));
			}
				
			// Grid 좌표값
			if(!bScope)
			{
				if(m_XAxis.GetTitle().CompareNoCase("StepNo") == 0)
				{
					strXLable.Format("%d",(int)m_XAxis.ConvertUnit(iter));
				}
				else if(m_XAxis.GetTitle().CompareNoCase("Time") == 0)
				{
					if(m_XAxis.ConvertUnit(m_XAxis.GetGrid()) < 0.5f)
					{
						strXLable.Format("%.2f", m_XAxis.ConvertUnit(iter));
					}
					else
					{
						strXLable.Format("%.1f", m_XAxis.ConvertUnit(iter));
					}
				}
				else
				{
					strXLable = m_XAxis.GetUnitNotation();
					if(!strXLable.IsEmpty())
					{
						if(strXLable.Left(1) == "m")
						{
							if(m_XAxis.ConvertUnit(m_XAxis.GetGrid()) < 0.5f)
							{
								strXLable.Format("%.2f", m_XAxis.ConvertUnit(iter));
							}
							else
							{
								strXLable.Format("%.1f", m_XAxis.ConvertUnit(iter));
							}

						}
						else
						{
							if(m_XAxis.ConvertUnit(m_XAxis.GetGrid()) < 0.005f)
							{
								strXLable.Format("%.4f", m_XAxis.ConvertUnit(iter));
							}
							else
							{
								strXLable.Format("%.3f", m_XAxis.ConvertUnit(iter));
							}
						}
					}
					else
					{
						strXLable.Format("%.1f", m_XAxis.ConvertUnit(iter));
					}
				}	
				pDC->SetTextAlign(TA_TOP|TA_CENTER);
				pDC->TextOut(GetViewPosition(fltpoint_e,ScopeRect).x,
							 ScopeRect.bottom+XAxisTextMetric.tmExternalLeading+XAxisTextMetric.tmHeight/2,
							 strXLable);
			}
		}
		fltStart = 0.0f;
	}

	//Draw Min() or 0 ~ Max()
	for( iter = fltStart; iter<=m_XAxis.GetMax(); iter+=m_XAxis.GetGrid())
	{
		// Grid Line
		fltpoint_s.x = iter;	fltpoint_s.y = m_YAxis.GetMin();
		fltpoint_e.x = iter;	fltpoint_e.y = m_YAxis.GetMax();
		if(m_XAxis.IsShowGrid())
		{
			pDC->MoveTo(GetViewPosition(fltpoint_s, ScopeRect));
			pDC->LineTo(GetViewPosition(fltpoint_e, ScopeRect));
		}
		
		// Grid 좌표값
		if(!bScope)
		{
			if(m_XAxis.GetTitle().CompareNoCase("StepNo") == 0)
			{
				strXLable.Format("%d",(int)m_XAxis.ConvertUnit(iter));
			}
			else if(m_XAxis.GetTitle().CompareNoCase("Time") == 0)
			{
				if(m_XAxis.ConvertUnit(m_XAxis.GetGrid()) < 0.5f)
				{
					strXLable.Format("%.2f", m_XAxis.ConvertUnit(iter));
				}
				else
				{
					strXLable.Format("%.1f", m_XAxis.ConvertUnit(iter));
				}
			}
			else
			{
				strXLable = m_XAxis.GetUnitNotation();
				if(!strXLable.IsEmpty())
				{
					if(strXLable.Left(1) == "m")
					{
						if(m_XAxis.ConvertUnit(m_XAxis.GetGrid()) < 0.5f)
						{
							strXLable.Format("%.2f", m_XAxis.ConvertUnit(iter));
						}
						else
						{
							strXLable.Format("%.1f", m_XAxis.ConvertUnit(iter));
						}

					}
					else
					{
						if(m_XAxis.ConvertUnit(m_XAxis.GetGrid()) < 0.005f)
						{
							strXLable.Format("%.4f", m_XAxis.ConvertUnit(iter));
						}
						else
						{
							strXLable.Format("%.3f", m_XAxis.ConvertUnit(iter));
						}
					}
				}
				else
				{
					strXLable.Format("%.1f", m_XAxis.ConvertUnit(iter));
				}
			}	
			pDC->SetTextAlign(TA_TOP|TA_CENTER);
			pDC->TextOut(GetViewPosition(fltpoint_e,ScopeRect).x,
				         ScopeRect.bottom+XAxisTextMetric.tmExternalLeading+XAxisTextMetric.tmHeight/2,
				         strXLable);
		}
	}
		//??
	if(bScope)
	{
		fltpoint_e.x = iter-m_XAxis.GetGrid()*6.0f;		fltpoint_e.y = m_YAxis.GetMax();
		strXLable.Format("%.1f",m_XAxis.ConvertUnit(m_XAxis.GetGrid()));
		pDC->TextOut(GetViewPosition(fltpoint_e,ScopeRect).x,
//			         ClientRect.bottom-XAxisTextMetric.tmHeight-XAxisTextMetric.tmExternalLeading,
					 ScopeRect.bottom+(XAxisTextMetric.tmHeight+XAxisTextMetric.tmExternalLeading)*2,
			         strXLable);
	}

	//
	pDC->SelectObject(oldfont);
	pDC->SelectObject(oldpen);
	afont.DeleteObject();
	apen.DeleteObject();
	pDC->SetTextColor(oldTextColor);
}

void CPlane::DrawScopeRect(CDC *pDC, CRect ScopeRect)
{
	//배경을 원하는 색으로 칠한다.
	CBrush aBrush, *oldBrush;
	aBrush.CreateSolidBrush(m_BackColor);
	oldBrush = pDC->SelectObject(&aBrush);
	pDC->Rectangle(&ScopeRect); // Client영역의 전체바탕화면을 m_BackColor로
	                             // 칠한다.
	pDC->SelectObject(oldBrush);
	aBrush.DeleteObject();

	//그래프 영역 사각형을 칠한다.
	CPen   aPen, *oldpen;
	pDC->SetBkMode(TRANSPARENT);
//	pDC->SetTextColor(m_XAxis.GetLabelColor());

	aPen.CreatePen(PS_SOLID, 2, m_GridColor);
	oldpen   = pDC->SelectObject(&aPen);

	pDC->MoveTo(ScopeRect.left, ScopeRect.top);		//Scope 태두리를 m_Gridcolor로 그린다.
	pDC->LineTo(ScopeRect.left, ScopeRect.bottom);
	pDC->LineTo(ScopeRect.right, ScopeRect.bottom);
	pDC->LineTo(ScopeRect.right, ScopeRect.top);
	pDC->LineTo(ScopeRect.left, ScopeRect.top);
		
	pDC->SelectObject(oldpen);
	aPen.DeleteObject();
	pDC->RestoreDC(-1);
}

int CPlane::DrawYAxis(CDC *pDC, int LabelSpace, CRect ScopeRect, BOOL bScope)
{
	//
	// y축 *******************************************
	CPen  aPen, *oldPen;
	TEXTMETRIC YAxisTextMetric;
	POINT pt;
	float fTemp;
	CString	str;

	CFont afont, *oldfont;
	afont.CreateFontIndirect(m_YAxis.GetFont());
	oldfont=pDC->SelectObject(&afont);
	pDC->GetTextMetrics(&YAxisTextMetric);
	
	int  iYLabelStartPos = LabelSpace + YAxisTextMetric.tmAveCharWidth;
	int  iYLabelEndPos   = LabelSpace + YAxisTextMetric.tmAveCharWidth*_YAXIS_LABEL_WIDTH;
	int	 iYAxisPos = iYLabelEndPos - 10;

	COLORREF oldTextColor = pDC->SetTextColor(m_YAxis.GetLabelColor());
	
	// 좌측 상단에 축의 물리량과 단위를 표시하고
	CString strYLabel;
	{
		strYLabel.Format("%s(%s)", m_YAxis.GetPropertyTitle().Left(4),
		  	                       m_YAxis.GetUnitNotation());
		pDC->SetTextAlign(TA_BOTTOM|TA_RIGHT);
		pDC->TextOut(iYLabelEndPos/*LabelSpace*/, ScopeRect.top-YAxisTextMetric.tmHeight*0.5-YAxisTextMetric.tmExternalLeading, strYLabel);
	}
	
	// 세로선을 긋는다.
	aPen.CreatePen(PS_SOLID,1, m_YAxis.GetLabelColor());
	oldPen=pDC->SelectObject(&aPen);
	pDC->MoveTo(iYAxisPos,ScopeRect.top);
	pDC->LineTo(iYAxisPos,ScopeRect.bottom);
	pDC->SelectObject(oldPen);
	aPen.DeleteObject();
	
	//
	CString strGridValue;
	aPen.CreatePen(PS_SOLID|PS_ENDCAP_SQUARE, 2, m_YAxis.GetLabelColor());
	oldPen=pDC->SelectObject(&aPen);

	//눈금 표시
	float fltMiniGrid, fltStart;
	//////////////////////////////////////////////////////////////////////////
	fltMiniGrid = m_YAxis.GetGrid()/5;
	fltStart = m_YAxis.GetMin();
	if( m_YAxis.GetMin() < 0 && 0 < m_YAxis.GetMax())	//0을 포함하고 있으면 0부터 시작해서 라인을 그린다.
	{
			//0보다 작은 라인을 긋는다.
			for(float iter = 0.0f; iter >= m_YAxis.GetMin(); iter-=m_YAxis.GetGrid())
			{
				fltPoint fltpoint_e = {m_XAxis.GetMax(), iter};
				pt = GetViewPosition(fltpoint_e, ScopeRect);
				pDC->MoveTo(iYAxisPos-8, pt.y);
				pDC->LineTo(iYAxisPos, pt.y);
						
				CPen miniPen, *pMiniOld;
				miniPen.CreatePen(PS_SOLID, 1, m_YAxis.GetLabelColor());
				pMiniOld =pDC->SelectObject(&miniPen);                    
				//Grid와 Grid사이에 5칸의 작은 표시 설정
				for(int i=0; i<5; i++)
				{
					fTemp = iter+i*fltMiniGrid;
					if(fTemp > m_YAxis.GetMax())	break;
					fltpoint_e.x = m_XAxis.GetMin();
					fltpoint_e.y = fTemp;
					pt = GetViewPosition(fltpoint_e,ScopeRect);
					pDC->MoveTo(iYAxisPos-3, pt.y);
					pDC->LineTo(iYAxisPos, pt.y);	
				}
				pDC->SelectObject(pMiniOld);
				miniPen.DeleteObject();
			}
			fltStart = 0.0f;		//0 라인을 긋는다.
	}

	//한쪽 방향으로 있는 경우(Min/Max가 음수 or Min/Max가 양수 or 0보다 큰 구간 그림)
	for(float iter = fltStart; iter<=m_YAxis.GetMax(); iter+=m_YAxis.GetGrid())
	{
			fltPoint fltpoint_e = {m_XAxis.GetMax(), iter};
			pt = GetViewPosition(fltpoint_e, ScopeRect);
			pDC->MoveTo(iYAxisPos-8, pt.y);
			pDC->LineTo(iYAxisPos, pt.y);
					
			CPen miniPen, *pMiniOld;
			miniPen.CreatePen(PS_SOLID, 1, m_YAxis.GetLabelColor());
			pMiniOld =pDC->SelectObject(&miniPen);                    
			//Grid와 Grid사이에 5칸의 작은 표시 설정
			for(int i=0; i<5; i++)
			{
				fTemp = iter+i*fltMiniGrid;
				if(fTemp > m_YAxis.GetMax())	break;
				fltpoint_e.x = m_XAxis.GetMin();
				fltpoint_e.y = fTemp;
				pt = GetViewPosition(fltpoint_e,ScopeRect);
				pDC->MoveTo(iYAxisPos-3, pt.y);
				pDC->LineTo(iYAxisPos, pt.y);	
			}
			pDC->SelectObject(pMiniOld);
			miniPen.DeleteObject();
	}
	pDC->SelectObject(oldPen);
	aPen.DeleteObject();

	//
	aPen.CreatePen(PS_DOT,1,m_YAxis.GetLabelColor());
	oldPen=pDC->SelectObject(&aPen);

	fltStart = m_YAxis.GetMin();
	if( m_YAxis.GetMin() < 0 && 0 < m_YAxis.GetMax())	//0을 포함하고 있으면 0부터 시작해서 라인을 그린다.
	{
			//0보다 작은 라인을 긋는다.
			for(iter = 0.0f; iter>=m_YAxis.GetMin(); iter-=m_YAxis.GetGrid())
			{
				// Grid Line
				fltPoint fltpoint_s = {m_XAxis.GetMin(), iter};
				fltPoint fltpoint_e = {m_XAxis.GetMax(), iter};

				if(m_YAxis.IsShowGrid())
				{
					pDC->MoveTo(GetViewPosition(fltpoint_s, ScopeRect));
					if(iter != fltStart)
						pDC->LineTo(GetViewPosition(fltpoint_e, ScopeRect));
				}

				if(!bScope)
				{
					fTemp = m_YAxis.ConvertUnit(iter);
					// Grid 좌표값
	//				TRACE("%f\n", m_YAxis.ConvertUnit(m_YAxis.GetGrid()));
					if(m_YAxis.ConvertUnit(fabs(m_YAxis.GetMax()-m_YAxis.GetMin())) < 0.005f)
					{
						strGridValue.Format("%.4f", fTemp);
					}
					else
					{
						strGridValue.Format("%.3f", fTemp);
					}
					str = m_YAxis.GetUnitNotation();
					if(!str.IsEmpty())
					{
						if(str.Left(1) == "m" || str.Left(1) == "%%")
						{
							if(m_YAxis.ConvertUnit(fabs(m_YAxis.GetMax()-m_YAxis.GetMin())) < 5.0f)
							{
								strGridValue.Format("%.2f", fTemp);
							}
							else
							{
								strGridValue.Format("%.1f", fTemp);
							}
						}
					}
					if(fTemp == 0.0f)	strGridValue = "0";

	//Edited By KBH 2005/9/13 
	//y축 눈금 숫자를 오른쪽 정렬 하기 위해 서 TextOut()->DrawText() 함수로 변경 
					pDC->SetTextAlign(TA_TOP|TA_RIGHT);
	//				pDC->TextOut(iYLabelStartPos,GetViewPosition(fltpoint_e,viewrect).y,strGridValue);
					pDC->TextOut(iYAxisPos-YAxisTextMetric.tmAveCharWidth*2, GetViewPosition(fltpoint_e, ScopeRect).y- (YAxisTextMetric.tmHeight)/2, strGridValue);
	//////////////////////////////////////////////////////////////////////////

				}
			}
			fltStart = 0.0f;		//0 라인을 긋는다.
	}

	for(iter = fltStart; iter<=m_YAxis.GetMax(); iter+=m_YAxis.GetGrid())
	{
			// Grid Line
			fltPoint fltpoint_s = {m_XAxis.GetMin(), iter};
			fltPoint fltpoint_e = {m_XAxis.GetMax(), iter};

			if(m_YAxis.IsShowGrid())
			{
				pDC->MoveTo(GetViewPosition(fltpoint_s, ScopeRect));
				if(iter != fltStart)
					pDC->LineTo(GetViewPosition(fltpoint_e, ScopeRect));
			}

			if(!bScope)
			{
				// Grid 좌표값
				fTemp = m_YAxis.ConvertUnit(iter);
				//5mV 보다 작을 경우 자릿수 1개 더 표시 
				if(m_YAxis.ConvertUnit(fabs(m_YAxis.GetMax()-m_YAxis.GetMin())) < 0.005f)
				{
					strGridValue.Format("%.4f", fTemp);
				}
				else
				{
					strGridValue.Format("%.3f", fTemp);
				}
				str = m_YAxis.GetUnitNotation();
				if(!str.IsEmpty())
				{
					if(str.Left(1) == "m" || str.Left(1) == "%%")
					{
						if(m_YAxis.ConvertUnit(fabs(m_YAxis.GetMax()-m_YAxis.GetMin())) < 5.0f)
						{
							strGridValue.Format("%.2f", fTemp);
						}
						else
						{
							strGridValue.Format("%.1f", fTemp);
						}
					}
				}

//Edited By KBH 2005/9/13 
//y축 눈금 숫자를 오른쪽 정렬 하기 위해 서 TextOut()->DrawText() 함수로 변경 
				pDC->SetTextAlign(TA_TOP|TA_RIGHT);
//				pDC->TextOut(iYLabelStartPos,GetViewPosition(fltpoint_e,viewrect).y,strGridValue);
				pDC->TextOut(iYAxisPos-YAxisTextMetric.tmAveCharWidth*2, GetViewPosition(fltpoint_e,ScopeRect).y- (YAxisTextMetric.tmHeight)/2, strGridValue);
//////////////////////////////////////////////////////////////////////////

			}
	}
		
	//??
	if(bScope)
	{
		fltPoint fltpoint_e = {m_XAxis.GetMax(),iter-m_YAxis.GetGrid()*5.0f};
		// Grid 좌표값
		strGridValue.Format("%6.1f",  m_YAxis.ConvertUnit(m_YAxis.GetGrid()));//,m_YAxis.GetUnitNotation());
		pDC->TextOut(iYLabelStartPos,GetViewPosition(fltpoint_e,ScopeRect).y,strGridValue);
	}
	//
	pDC->SelectObject(oldPen);
	aPen.DeleteObject();
		
	pDC->SelectObject(oldfont);
	afont.DeleteObject();

	pDC->SetTextColor(oldTextColor);

	return  iYLabelEndPos;
}
