#include "stdafx.h"

#include "CTSMon.h"

#include "FMS.h"
#include "FM_Unit.h"

#include "FM_STATE.h"

CFM_ST_MA_ERROR::CFM_ST_MA_ERROR(FM_STATUS_ID _Eqstid, FM_STATUS_ID _stid, CFM_Unit* _unit)
:CFM_ST_MAINT(_Eqstid, _stid, _unit)
{
}


CFM_ST_MA_ERROR::~CFM_ST_MA_ERROR(void)
{
}

VOID CFM_ST_MA_ERROR::fnEnter()
{
	m_Unit->fnGetFMS()->fnSend_E_EQ_ERROR(FMS_ER_NONE);
	CHANGE_FNID(FNID_PROC);
}

VOID CFM_ST_MA_ERROR::fnProc()
{
	fnSetFMSStateCode(FMS_ST_ERROR);
}

VOID CFM_ST_MA_ERROR::fnExit()
{
}

VOID CFM_ST_MA_ERROR::fnSBCPorcess(WORD _state)
{
	CFM_ST_MAINT::fnSBCPorcess(_state);
}

FMS_ERRORCODE CFM_ST_MA_ERROR::fnFMSPorcess(FMS_COMMAND _msgId, st_FMS_PACKET* _recvData)
{
	FMS_ERRORCODE _error = ER_NO_Process;

	switch(_msgId)
	{
	default:
		{
		}
		break;
	}

	return _error;

}