// TestCondition.h: interface for the CTestCondition class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TESTCONDITION_H__1FEB9409_D1A8_4660_8585_F651D9F9C463__INCLUDED_)
#define AFX_TESTCONDITION_H__1FEB9409_D1A8_4660_8585_F651D9F9C463__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CStep;

#include  <afxdao.h>

class CTestCondition  
{
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	CTestCondition();
	virtual ~CTestCondition();

	void AddStep(CStep *pStep);
	float GetStepTimeSum();
	EP_PRETEST_PARAM GetNetCheckParam();
	CString GetTestName();
	BOOL	ExcelSave();
	void	ResetCondition();
	int		GetCountIsGradeStep();
	void	ShowCondition();
	int		GetTotalStepNo();
	CStep * GetStep(int nStepIndex);
	BOOL	SetProcedure(STR_CONDITION *pCondition);
	int		FindNextTestName(long lModelID, long lTestID, STR_CONDITION_HEADER *pTestHeader, long *pnProcType = NULL, long *plContinueCellCode = NULL);
	
	void	SetTestConditon(CTestCondition *pCondition); 

	//load test condition from database
	BOOL	LoadTestCondition(CString strFileName, STR_CONDITION *pCondition = NULL);
	//load test condition from file
	BOOL	LoadTestCondition(long lTestID, long lModelID=0, STR_CONDITION *pCondition = NULL);
	//make
	//kky	BOOL	LoadTestCondition(st_CHARGER_INRESEVE&, STR_CONDITION *pCondition = NULL);

	long	GetModelID();	
	int		GetStepProcedureType(int nStepIndex);

	//Step 단위 Scheduling
/*	BOOL	LoadFirstStepA(long lTestID, CStep *pStep);					//Test 의 처음 Step을 검색
	BOOL	LoadNextStepA(long lTestID, int lStepNo, CStep *pStep);	//Test 의 다음 step을 검색
	BOOL	LoadStepA(long lTestID, int lStepNo, CStep *pStep);		//Test의 지정 Step을 검색 
	int		LoadGradeStep(CGradeRecordSet *pRecordSet, long lStepID, CStep *step);
*/
	STR_CHECK_PARAM* GetCheckParam()		{	return &m_boardParam;		}
	STR_CONDITION_HEADER *GetModelInfo()	{	return &m_modelHeader;		}
	STR_CONDITION_HEADER *GetTestInfo()		{	return &m_conditionHeader;	}

	long	GetTestProcType()				{	return m_lProcType;			}
	
	int		RequeryTestName(long lTestID, long lModelID, STR_CONDITION_HEADER *pTestHeader, long *pnProcType= NULL, long *plResetAutoProc = NULL);
	BOOL	RequeryBatteryModel(long lModelID, STR_CONDITION_HEADER *pModel);
	void	RemoveStep();

protected:

	STR_CONDITION_HEADER	m_conditionHeader;	//공정 정보 
	STR_CONDITION_HEADER	m_modelHeader;		//Model 정보
	long					m_lProcType;		//공정 Type
//	EP_TEST_HEADER			m_testHeader;
	STR_CHECK_PARAM			m_boardParam;
	CPtrArray				m_apStepList;
	BOOL					m_bContinueCellCode;
	
	BOOL	ReadTestConditionFile(FILE *fp, STR_CONDITION *pCondition, char *fileID = NULL, char *fileVer= NULL);
	int		ReadEPFileHeader(int nFile, FILE *fp);	
	int		RequeryStepGrade(CDaoDatabase &rDb, long lStepID, STR_COMMON_STEP *pStep);
	int		RequeryStepCondition(long lTestID, long lModelID, STR_CONDITION *pCondition);
	BOOL	RequeryCheckParam(long lTestID, long lModelID, STR_CHECK_PARAM *pParam);
};

#endif // !defined(AFX_TESTCONDITION_H__1FEB9409_D1A8_4660_8585_F651D9F9C463__INCLUDED_)
