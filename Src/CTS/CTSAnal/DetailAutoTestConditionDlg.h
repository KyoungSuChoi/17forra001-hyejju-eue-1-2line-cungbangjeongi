#pragma once

// CDetailAutoTestConditionDlg 대화 상자입니다.
#include "MyGridWnd.h"
#include "CTSAnalDoc.h"
#include "afxdtctl.h"

class CDetailAutoTestConditionDlg : public CDialog
{
	DECLARE_DYNAMIC(CDetailAutoTestConditionDlg)

public:
	CDetailAutoTestConditionDlg(CTestCondition *pTestCondition, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDetailAutoTestConditionDlg();

	CString *TEXT_LANG;
	bool LanguageinitMonConfig();
	
	// 대화 상자 데이터입니다.
	enum { IDD = IDD_DETAIL_AUTOTESTCONDITION_DLG };
	
	CCTSAnalDoc *m_pDoc;
	
private:
	CLabel m_Label1;
	CLabel m_Label2;
	CLabel m_Label3;
	
	CFont m_Font;

	CMyGridWnd m_ProtectSettingGrid;
	CMyGridWnd m_StepGrid;
	CMyGridWnd m_ContactSettingGrid;
	
	CTestCondition *m_pTestCondition;
	
public:
	VOID InitLabel();
	VOID InitFont();
	
	VOID InitProtectSettingGrid();
	VOID InitStepGrid();
	VOID InitContactSettingGrid();
	
	VOID DisplayStepGrid( int nRow = -1 );
	VOID DisplayProtectSettingGrid();
	VOID DisplayContactSettingGrid();
	
	void DisplayCondition();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	afx_msg LONG OnMovedCurrentCell(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	virtual BOOL OnInitDialog();	
	CLabel m_LabelType;
	CLabel m_LabelProcess;
	float m_fDCIR_RegTemp;
	float m_fDCIR_ResistanceRate;
	UINT m_nStepLimitCheckTime;
	UINT m_nChargeVoltageChkTime;
	CString m_strChargeFanOffChk;
	CString m_strChgCCVtg;
	CString m_strChgCCTime;
	CString m_strChgCCDeltaV;
	CString m_strChgCVCrt;
	CString m_strChgCVTime;
	CString m_strChgCVDeltaA;
	afx_msg void OnStnClickedFanoffStatic();
	CString m_strVIGetTime;
	CDateTimeCtrl m_ReportTime;
	CString m_strOCVHighVtg;
	CString m_strOCVLowVtg;
};
