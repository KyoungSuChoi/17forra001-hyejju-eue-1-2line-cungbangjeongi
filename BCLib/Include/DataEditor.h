#ifndef _DATAFORM_
#define _DATAFORM_

#include "PowerSys.h"

#define SCH_MAX_COMP_POINT	3
#define SCH_MAX_GRADE_STEP	10
#define SCH_STEP_ALL		-1
#define SCH_MAX_STEP		100
#define SCH_MAX_GRADE_ITEM_IN_STEP 4

//#define SCH_UNIT_MILI		0

#define SCH_MAX_ID_SIZE		16
#define SCH_MAX_PWD_SIZE	16
#define SCH_DATE_TIME_SIZE		32
#define SCH_USER_NAME_SIZE		32
#define SCH_DESCRIPTION_SIZE	128	

#define SCH_MAX_CODE_SIZE	64
#define SCH_MAX_MSEC_DATA_POINT	600

typedef struct s_LoginInformation 
{
	char	szLoginID[SCH_MAX_ID_SIZE];
	char	szPassword[SCH_MAX_PWD_SIZE];
	long	nPermission;
	char	szRegistedDate[SCH_DATE_TIME_SIZE];
	char	szUserName[SCH_USER_NAME_SIZE];
	char	szDescription[SCH_DESCRIPTION_SIZE];
	BOOL	bUseAutoLogOut;	
	ULONG	lAutoLogOutTime;
} SCH_LOGIN_INFO, STR_LOGIN;

typedef struct s_ChannelCodeMessage {
	int nCode;
	int nData;
	char szMessage[SCH_MAX_CODE_SIZE];
} SCH_CODE_MSG;

typedef struct s_TestInformation {
	LONG	lID;
	char	szName[64];
	char	szDescription[128];
	char	szCreator[64];
	char	szModifiedTime[32];
} SCH_TEST_INFORMATION;


#if SCH_MAX_COMP_POINT < 3
#pragma message( "------------KBH Message-------------" )
#pragma message( "comparison Voltage Data Point Define Error" )
#pragma message( "------------------------------------" )
#error Least 3 Point
#endif


typedef struct tag_GRADE_CON {
// 	long	lGradeItem;
// 	BYTE	chTotalGrade;
// 	float	faValue1[SCH_MAX_GRADE_STEP];
// 	float	faValue2[SCH_MAX_GRADE_STEP];
// 	char	aszGradeCode[SCH_MAX_GRADE_STEP];

	//20081208 KHS
	long	lGradeItem[SCH_MAX_GRADE_STEP*2];
	BYTE	chTotalGrade;
	float	faValue1[SCH_MAX_GRADE_STEP*2];
	float	faValue2[SCH_MAX_GRADE_STEP*2];
	char	aszGradeCode[SCH_MAX_GRADE_STEP*2];
	UINT	iGradeCount;
}	GRADE;

typedef struct tag_STEP_CON {
	//Step Header
	char	chStepNo;
	char	chType;
	int		nProcType;
	char	chMode;
	float	fVref;
	float	fIref;
	float	fTref;
	
	//End 조건
	ULONG	ulEndTime;					//10msec 단위 입력 
	float	fEndV;
	float	fEndI;
	float	fEndC;
	float	fEndDV;						//Step Charge/Discharge Flag
	float	fEndDI;						//Step 충방전시 2번째 I ref 
	float	fEndW;
	float	fEndWh;
	float	fEndTemp;
	
	//SOC 종료 조건//20051208
	int		bUseActualCapa;		//현재 Step의 용량값을 기준 용량으로 사용 할지 여부 
	int		nUseDataStepNo;		//비교할 기준 용량이 속한 Step번호
	float	fSocRate;			//비교 SOC Rate

	//Cycle 
	UINT nLoopInfoGoto;
	UINT nLoopInfoEndVGoto;
	UINT nLoopInfoEndTGoto;
	UINT nLoopInfoEndCGoto;
	UINT nLoopInfoCycle;
	UINT nGotoStepID;				//EndV,EndT,EndC를 위한 임시 Step ID -> 이동 스텝은 이 ID를 참조하게 된다. 20080201 kjh
	UINT nLoopInfoGotoCnt;

	//안전 조건 
	float	fVLimitHigh;
	float	fVLimitLow;
	float	fILimitHigh;
	float	fILimitLow;
	float	fCLimitHigh;
	float	fCLimitLow;
	float	fImpLimitHigh;
	float	fImpLimitLow;
	LONG	lDeltaTime;
	LONG	lDeltaTime1;				 
	float	fDeltaV;					
	float	fDeltaI;
	float	fTempHigh;
	float	fTempLow;
	
	//Grading
	BOOL	bGrade;
//	GRADE	sGrading_Val[SCH_MAX_GRADE_ITEM_IN_STEP];	//20081007 kjh Ah + ESR or F + ESR 조합 Grade 설정
	GRADE	sGrading_Val;		//20081208 KHS

	//전압 상승 비교 설정
	float	fCompVHigh[SCH_MAX_COMP_POINT];
	float	fCompVLow[SCH_MAX_COMP_POINT];
	ULONG	ulCompTimeV[SCH_MAX_COMP_POINT];

	//전류 상승 비교 설정
	float	fCompIHigh[SCH_MAX_COMP_POINT];
	float	fCompILow[SCH_MAX_COMP_POINT];
	ULONG	ulCompTimeI[SCH_MAX_COMP_POINT];

	//종지 전류 전압비교 설정
	//deleted 20071106
	float	fIEndHigh;			//not use	
	float	fIEndLow;			//not use	
	float	fVEndHigh;			//not use	
	float	fVEndLow;			//not use
	
	//Report 설정
	float	fReportV;
	float	fReportI;
	ULONG	ulReportTime;
	float	fReportTemp;
	ULONG	ulReportVIGetTime;
	
	//EDLC 용//////2003/11/28 KBH	for EDLC	///////////////////////////////////
	float	fCapaVoltage1;			//EDLC 용량 검사시 전압 Point1
	float	fCapaVoltage2;			//EDLC 용량 검사시 전압 Point2
	float	fDCRStartTime;			//EDLC DCR 검사시 Start Time
	float	fDCREndTime;			//EDLC DCR 검사시 End Time
	ULONG	fLCStartTime;			//EDLC LC 측정 시작 시간
	ULONG	fLCEndTime;				//EDLC LC 측정 종료 시간
	LONG	lRange;					//Range 선택
	///////////////////////////////////////////////////////////////////////
	
	//copy paste 기능시 사용
	INT		nEditState;

	//Simulation file name
	char	szSimulFile[256];

	float	fValueLimitLow;
	float	fValueLimitHigh;
	float	fStartT;
	// long	nChargeLowVCheckTime;		// 충전 하한 전압 체크 시간
	float	fDCIR_RegTemp;				// DCIR 기준온도
	float	fDCIR_ResistanceRate;		// DCIR 저항 변화율

	float	fCompChgCcVtg;
	ULONG	ulCompChgCcTime;
	float	fCompChgCcDeltaVtg;

	float	fCompChgCvCrt;
	ULONG	ulCompChgCvtTime;
	float	fCompChgCvDeltaCrt;
	
}	STEP;	

typedef struct tag_GROUP_PARAMETER {
	float	fMaxVoltage;
	float	fMinVoltage;
	float	fMaxCurrent;
	float	fOCVLimitVal;			//Vref
	float	fTrickleCurrent;
	float	fDeltaVoltage;			// PreResistance
	long	lTrickleTime;
	float	fMaxFaultNo;
	BOOL	bPreTest;	
	float	fDeltaVoltageLimit;
}	TEST_PARAM;

typedef struct tag_ConditionFile_Header {
	char	szFileIdentify[16];
	char	szDateTime[32];
}	CONDITION_FILE_HEADER;

//#define V_UNIT_FACTOR	1000.0f
//#define I_UNIT_FACTOR	1000.0f
#define T_UNIT_FACTOR	100.0F
#define Z_UNIT_FACTOR	1
//#define C_UNIT_FACTOR	1000.0f

typedef struct tag_UnitInfo {
	float fTransfer;
	char szUnit[8];
	int  nFloatPoint;
} UNIT_INFO;

#define COPY_DATA	0
#define INSERT_DATA 1
#define DELETE_DATA 2

/*
//단위 계산용 
class CUnit 
{
public:
	CUnit() {};
	
	CUnit(WORD wUnit)
	{
		SetUnit(wUnit);
	};

	virtual ~CUnit() {};

	void SetUnit(WORD wUnit)
	{
		m_Unit = wUnit;
		if(m_Unit == SCH_UNIT_MILI)
		{
			m_Decimal = 1;
			m_Fractional = 3;
		}
		else
		{
			m_Decimal = 4;
			m_Fractional = 0;			
		}
	};

	CString GetValueString(float data)
	{
		char szBuff[32];
		sprintf(szBuff, "%%d.%d", m_Decimal, m_Fractional);
		CString strData;
		strData.Format(szBuff, data);
		return strData;
	};

	WORD GetUnit() { return m_Unit; };
	BYTE GetDecimalDigit() {	return m_Decimal; };
	BYTE GetFractionalDigit() { return m_Fractional; };

protected:
	WORD	m_Unit;
	BYTE	m_Decimal;
	BYTE	m_Fractional;
} ;

*/
#endif	//_DATAFORM_