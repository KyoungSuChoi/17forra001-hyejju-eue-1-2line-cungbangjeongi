#if !defined(AFX_CELLLISTSAVEDLG_H__8C3C2C66_7612_4248_BF00_E720EC697E67__INCLUDED_)
#define AFX_CELLLISTSAVEDLG_H__8C3C2C66_7612_4248_BF00_E720EC697E67__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CellListSaveDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCellListSaveDlg dialog
class CCellListSaveDlg : public CDialog
{
// Construction
public:
	int m_nOpType;
	int m_nSelItem;
	void SetField(FIELD *pField, UINT nCount);
	CCellListSaveDlg(CWnd* pParent = NULL);   // standard constructor
// Dialog Data
	//{{AFX_DATA(CCellListSaveDlg)
	enum { IDD = IDD_CELL_LIST_SAVE_DLG };
	CComboBox	m_ctrlOPCombo;
	CComboBox	m_ctrlFieldListCombo;
	CString	m_strFileName;
	float	m_fVal1;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCellListSaveDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	FIELD     *m_pField;
	int		m_nFieldCount;

	// Generated message map functions
	//{{AFX_MSG(CCellListSaveDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnButton1();
	afx_msg void OnOk();
	afx_msg void OnSelchangeCombo1();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnStnClickedUnitLabel();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CELLLISTSAVEDLG_H__8C3C2C66_7612_4248_BF00_E720EC697E67__INCLUDED_)
