// TextInputDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ctsmon.h"
#include "TextInputDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTextInputDlg dialog


CTextInputDlg::CTextInputDlg(CWnd* pParent /*=NULL*/)
: CDialog(CTextInputDlg::IDD, pParent)
, m_strEdit1(_T(""))
{
	//{{AFX_DATA_INIT(CTextInputDlg)
	// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_nValue = 1;
}


void CTextInputDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTextInputDlg)
	// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
	DDX_Text(pDX, IDC_EDIT1, m_strEdit1);
}


BEGIN_MESSAGE_MAP(CTextInputDlg, CDialog)
	//{{AFX_MSG_MAP(CTextInputDlg)
	// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, &CTextInputDlg::OnBnClickedOk)
	//	ON_WM_CREATE()
	ON_BN_CLICKED(IDCANCEL, &CTextInputDlg::OnBnClickedCancel)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTextInputDlg message handlers

void CTextInputDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);

	if( !m_strEdit1.IsEmpty() )
	{
		m_nValue = atoi(m_strEdit1);
	}

	if( m_nValue<0 || m_nValue>4 )
	{
		m_nValue = 0;
	}

	OnOK();
}

void CTextInputDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}
