#if !defined(AFX_TEMPVIEW_H__AFD2E233_5059_4D0B_9140_F8EBD02A7EF5__INCLUDED_)
#define AFX_TEMPVIEW_H__AFD2E233_5059_4D0B_9140_F8EBD02A7EF5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TempView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTempView form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

#include "CTSMonDoc.h"	
#include "MyGridWnd.h"
#include "IconCombo.h"

#define TEMP_SYSTEM_GRID_COL	20
#define TEMP_SYSTEM_GRID_ROW	13

class CTempView : public CFormView
{
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

protected:
	CTempView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CTempView)
	enum { FONTSIZE = 11 };	
// Form Data
public:
	//{{AFX_DATA(CTempView)
	enum { IDD = IDD_TEMP_VIEW };
	CStatic	m_UnitScale;
	CStatic	m_JigScale;
	CLabel	m_UnitLevel8;
	CLabel	m_UnitLevel7;
	CLabel	m_UnitLevel6;
	CLabel	m_UnitLevel5;
	CLabel	m_UnitLevel4;
	CLabel	m_UnitLevel3;
	CLabel	m_UnitLevel2;
	CLabel	m_UnitLevel1;
	CLabel	m_JigLevel8;
	CLabel	m_JigLevel7;
	CLabel	m_JigLevel6;
	CLabel	m_JigLevel5;
	CLabel	m_JigLevel3;
	CLabel	m_JigLevel2;
	CLabel	m_JigLevel4;
	CLabel	m_JigLevel1;
	CComboBox	m_ctrlChNameCombo;
	CIconCombo	m_ctrlDisplayCombo;
	CLabel	m_MinTempIndex;
	CLabel	m_MaxTempIndex;
	CLabel	m_CmdTarget;
	//}}AFX_DATA

// Attributes 속성
public:
	CCTSMonDoc*		GetDocument();	
	CCTSMonDoc*		m_pDoc;	
	CMyGridWnd		m_wndTempGrid;	
	CImageList		*m_pImagelist;
	CStringArray	m_UnitChData;		// 콤보박스 표시를 위한 Unit1 정보
	CStringArray	m_JigChData;		// 콤보박스 표시를 위한 Jig1 정보		

	float	m_fMinTemp;
	float	m_fMaxTemp;
	
	UINT	m_nDisplayTimer;
	int		m_nCurModuleID;
	int		m_nTempGridRowCount;
	int		m_nTempGridColCount;	

	int		m_nSelectUnitDisplay;	// TempTab에서 화면에 표시되는 형태
	int		m_nSelectChDisplay;
	int		m_nPrevUnitDisplay;
	int		m_nModuleID;			// 화면에 표시되는 각 모듈의 ID
	BOOL	m_bInitCombo;			// 콤포박스 초기화 여부
	BOOL    m_bRackIndexUp;			// Grid 화면에 표시되는 기준방향( 위, 아래 )
	BOOL	m_bWarnningWindow;
// Operations 작동
public:	
	VOID	InitTempColor();		// InitDrawTempColor
	VOID	DrawTempColor();		// DrawTempColor
	BOOL	InitTempGridWnd();	
	VOID	StartMonitoring();
	VOID	StopMonitoring();
	VOID	SelectUnitTemp();		// DrawTempData, 그리드 색상표시	
	BOOL	GetChRowCol( int nIndex, ROWCOL &nRow, ROWCOL &nCol );
	INT		GetModuleIDRowCol( ROWCOL nRow, ROWCOL nCol );	
	VOID	SetTempAvgVal();		// AVG, 최소값, 최대값
	VOID	EnableJigChNameCombo();
	VOID	EnableUnitChNameCombo();
	
	CString GetUnitColor( CString strValue );		// 해당 하는 값의 R,G,B 값을 String 형식으로 리턴','구별
	CString GetJigColor( CString strValue );
	CString	ProcUnitTempData( int nModuleID );		// 해당 Module의 결과 값을 리턴

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTempView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CTempView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CTempView)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnSelchangeTempCombo();
	afx_msg void OnSelchangeChnameCombo();
	afx_msg void OnBtnTempsetting();
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	afx_msg LONG OnGridDoubleClick(WPARAM wParam, LPARAM lParam);
	afx_msg LONG OnMovedCurrentCell(WPARAM wParam, LPARAM lParam);
	afx_msg LONG OnGridRightClick(WPARAM wParam, LPARAM lParam);
public:
//	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
};

#ifndef _DEBUG  // debug version in TopView.cpp
inline CCTSMonDoc* CTempView::GetDocument()
{ return (CCTSMonDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TEMPVIEW_H__AFD2E233_5059_4D0B_9140_F8EBD02A7EF5__INCLUDED_)
