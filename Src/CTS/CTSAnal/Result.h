/********************************************************************
	created:	2002/04/03
	created:	3:4:2002   13:53
	filename: 	D:\SKC\ResultViewer\Result.h
	file path:	D:\SKC\ResultViewer
	file base:	Result
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/
#ifndef _DB_VIEWER_HEADEER_ 
#define _DB_VIEWER_HEADEER_

#define DATA_MODEL	0
#define DATA_LOT	1
#define DATA_TRAY	2

#define ODBC_SCH_DB_NAME	"CTSSchedule"
#define ODBC_PROD_DB_NAME	"CTSProduct"

typedef struct tagListData {
	CString m_Name;
	int		count;
	long	dataID;
} DATA_LIST;

typedef struct tagListProc {
	CString m_Name;
	int count;
	long dataID;
	int dataType;
	int nInputNo;
	int nNormalNo;
	CString strSerial;
} PROC_LIST;

typedef struct tagField1{
	int      No;
	int      ID;
	CString  FieldName;
	int      DataType;
	BOOL     bDisplay;
}FIELD;

#define      FIELDNO     100
#define DB_PATH_REG_SECTION		"Path"


#endif _DB_VIEWER_HEADEER_

