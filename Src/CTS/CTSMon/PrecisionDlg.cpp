// PrecisionDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "PrecisionDlg.h"


#define	EDIT_FONT_SIZE	18
#define	BTN_FONT_SIZE	20

// CPrecisionDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CPrecisionDlg, CDialog)

CPrecisionDlg::CPrecisionDlg(CCTSMonDoc *pDoc, CWnd* pParent /*=NULL*/)
: CDialog(CPrecisionDlg::IDD, pParent)
, m_nResultType(0)
{
	m_pDoc = pDoc;
	
	m_nSelectMode = 0;
	m_nSelectLevel = 0;
	m_bSelectData = false;
	LanguageinitMonConfig();
}

CPrecisionDlg::~CPrecisionDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

bool CPrecisionDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CPrecisionDlg"), _T("TEXT_CPrecisionDlg_CNT"), _T("TEXT_CPrecisionDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CPrecisionDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CPrecisionDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}


void CPrecisionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, ID_DLG_LABEL_PRECISON_NAME, m_LabelPrecisionName);
	DDX_Control(pDX, IDC_CURRENT_CC, m_BtnCurrentCC);
	DDX_Control(pDX, IDC_VOLTAGE_CV, m_BtnVoltageCV);
	DDX_Control(pDX, IDC_LEVEL1, m_BtnLevel1);
	DDX_Control(pDX, IDC_LEVEL2, m_BtnLevel2);
	DDX_Control(pDX, IDC_LEVEL3, m_BtnLevel3);
	DDX_Control(pDX, IDC_PERIOD_TODAY, m_BtnPeriodToday);
	DDX_Control(pDX, IDC_PERIOD_WEEK, m_BtnPeriodWeek);
	DDX_Control(pDX, IDC_PERIOD_MONTH, m_BtnPeriodMonth);
	DDX_Control(pDX, IDC_PERIOD_All, m_BtnPeriodAll);
	DDX_Control(pDX, IDC_SELECT_STAGE, m_ctrlStage);
	DDX_Control(pDX, IDC_CSV_OUTPUT, m_BtnCSVConvert);
	DDX_Control(pDX, IDC_ALL_CH_CLEAR, m_BtnAllChClear);
	DDX_Radio(pDX, IDC_RADIO, m_nResultType);
	DDX_Control(pDX, IDC_DATETIME_TO, m_DateTimeEnd);
	DDX_Control(pDX, IDC_DATETIME_FROM, m_DateTimeStart);
	DDX_Control(pDX, IDC_BTN_SEARCH, m_Btn_Search);
}

BEGIN_MESSAGE_MAP(CPrecisionDlg, CDialog)
//	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDOK, &CPrecisionDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_LEVEL1, &CPrecisionDlg::OnBnClickedLevel1)
	ON_BN_CLICKED(IDC_CURRENT_CC, &CPrecisionDlg::OnBnClickedCurrentCc)
	ON_BN_CLICKED(IDC_LEVEL3, &CPrecisionDlg::OnBnClickedLevel3)
	ON_BN_CLICKED(IDC_VOLTAGE_CV, &CPrecisionDlg::OnBnClickedVoltageCv)
	ON_BN_CLICKED(IDC_RADIO, &CPrecisionDlg::OnBnClickedRadio)
	ON_BN_CLICKED(IDC_RADIO5, &CPrecisionDlg::OnBnClickedRadio5)
	ON_BN_CLICKED(IDC_LEVEL2, &CPrecisionDlg::OnBnClickedLevel2)
	ON_BN_CLICKED(IDC_PERIOD_WEEK, &CPrecisionDlg::OnBnClickedPeriodWeek)
	ON_BN_CLICKED(IDC_PERIOD_MONTH, &CPrecisionDlg::OnBnClickedPeriodMonth)
	ON_BN_CLICKED(IDC_BTN_SEARCH, &CPrecisionDlg::OnClickedBtnSearch)
	ON_BN_CLICKED(IDC_PERIOD_All, &CPrecisionDlg::OnBnClickedPeriodAll)
	ON_BN_CLICKED(IDC_PERIOD_TODAY, &CPrecisionDlg::OnBnClickedPeriodToday)
	ON_BN_CLICKED(IDC_CSV_OUTPUT, &CPrecisionDlg::OnBnClickedCsvOutput)
	ON_BN_CLICKED(IDC_ALL_CH_CLEAR, &CPrecisionDlg::OnBnClickedAllChClear)
END_MESSAGE_MAP()


// CPrecisionDlg 메시지 처리기입니다.
void CPrecisionDlg::IntGridWnd()
{
	m_ListChInfoGrid.SubclassDlgItem(IDC_LIST_CH_INFO_GIRD, this);
	m_ListChInfoGrid.Initialize();
	m_ListChInfoGrid.m_bSameColSize = true;
	m_ListChInfoGrid.m_bSameRowSize = false;

	CRect rect;
	m_ListChInfoGrid.GetParam()->EnableUndo(FALSE);
	
	m_ListChInfoGrid.SetRowCount(0);
	m_ListChInfoGrid.SetColCount(16);
	m_ListChInfoGrid.SetDefaultRowHeight(30);
	m_ListChInfoGrid.EnableScrollBar(SB_VERT);
	
	m_ListChInfoGrid.SetColWidth(0,0,150);
	
	int i = 0;
	CString strTemp = _T("");
	
	for( i=1; i<=m_ListChInfoGrid.GetColCount(); i++ )
	{
		strTemp.Format("CH %d", i);	
		m_ListChInfoGrid.SetStyleRange(CGXRange(0, i),	CGXStyle().SetValue(strTemp));	
	}
	
	m_ListChInfoGrid.SetStyleRange(CGXRange().SetTable(), CGXStyle().SetFont(CGXFont().SetSize(11).SetBold(TRUE)).SetWrapText(FALSE));
	
	m_ListChInfoGrid.GetParam()->EnableUndo(TRUE);
}

BOOL CPrecisionDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	InitFont();
	InitLabel();
	IntGridWnd();
	InitColorBtn();
	
	CenterWindow();
	
	Fun_LoadStageList();
	
	Fun_UpdateBtnState();
	
	return TRUE;  // return TRUE unless you set the focus to a control
}

bool CPrecisionDlg::Fun_LoadStageList()
{
	int nDefaultIndex = 0;
	
	CDaoDatabase  db;
	db.Open(m_pDoc->m_strDataBaseName);

	COleVariant data;
	CString strData;
	CString strSQL;
	strSQL = "SELECT data3, ModuleID FROM SystemConfig ORDER BY ModuleID";
	try
	{
		CDaoRecordset rs(&db);

		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
		int index = 0;
		while(!rs.IsEOF())
		{
			data = rs.GetFieldValue(0);
			strData = data.pbVal;
			m_ctrlStage.AddString(strData);
			
			data = rs.GetFieldValue(1);
			m_ctrlStage.SetItemData( index++, data.lVal );
			rs.MoveNext();
		}
		rs.Close();
		db.Close();
	} 
	catch (CDaoException *e)
	{
		e->Delete();
	}
	
	m_ctrlStage.SetCurSel(nDefaultIndex);	
	return true;
}

void CPrecisionDlg::InitLabel()
{
	m_LabelPrecisionName.SetBkColor(RGB_MIDNIGHTBLUE);
	m_LabelPrecisionName.SetTextColor(RGB_WHITE);
	m_LabelPrecisionName.SetFontSize(24);
	m_LabelPrecisionName.SetFontBold(TRUE);
	m_LabelPrecisionName.SetText("Precision");
}

void CPrecisionDlg::InitFont()
{
	LOGFONT LogFont;

	GetDlgItem(IDC_STATIC1)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight = 1000;
	LogFont.lfHeight = EDIT_FONT_SIZE;

	m_Font.CreateFontIndirect( &LogFont );

	GetDlgItem(IDC_STATIC1)->SetFont(&m_Font);
	GetDlgItem(IDC_STATIC2)->SetFont(&m_Font);
	GetDlgItem(IDC_STATIC3)->SetFont(&m_Font);
	
	GetDlgItem(IDC_SELECT_STAGE_NAME)->SetFont(&m_Font);
	GetDlgItem(IDC_SELECT_STAGE)->SetFont(&m_Font);
	GetDlgItem(IDC_RADIO)->SetFont(&m_Font);
	GetDlgItem(IDC_RADIO5)->SetFont(&m_Font);
	
	
}

void CPrecisionDlg::InitColorBtn()
{
	m_BtnCurrentCC.SetFontStyle(BTN_FONT_SIZE, 1);
	m_BtnCurrentCC.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	
	m_BtnVoltageCV.SetFontStyle(BTN_FONT_SIZE, 1);
	m_BtnVoltageCV.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	
	m_BtnLevel1.SetFontStyle(BTN_FONT_SIZE, 1);
	m_BtnLevel1.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	

	m_BtnLevel2.SetFontStyle(BTN_FONT_SIZE, 1);
	m_BtnLevel2.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	
	m_BtnLevel3.SetFontStyle(BTN_FONT_SIZE, 1);
	m_BtnLevel3.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	
	m_BtnPeriodToday.SetFontStyle(BTN_FONT_SIZE, 1);
	m_BtnPeriodToday.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	
	m_BtnPeriodWeek.SetFontStyle(BTN_FONT_SIZE, 1);
	m_BtnPeriodWeek.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	
	m_BtnPeriodMonth.SetFontStyle(BTN_FONT_SIZE, 1);
	m_BtnPeriodMonth.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	
	m_BtnPeriodAll.SetFontStyle(BTN_FONT_SIZE, 1);
	m_BtnPeriodAll.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
	
	m_BtnCSVConvert.SetFontStyle(BTN_FONT_SIZE, 1);
	m_BtnCSVConvert.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
	
	m_BtnAllChClear.SetFontStyle(BTN_FONT_SIZE, 1);
	m_BtnAllChClear.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
	
	m_Btn_Search.SetFontStyle(BTN_FONT_SIZE, 1);
	m_Btn_Search.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
}

void CPrecisionDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnOK();
}

void CPrecisionDlg::OnBnClickedLevel1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nSelectLevel = 0;
	Fun_UpdateBtnState();
}

void CPrecisionDlg::OnBnClickedLevel2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nSelectLevel = 1;
	Fun_UpdateBtnState();
}

void CPrecisionDlg::OnBnClickedLevel3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.	
	m_nSelectLevel = 2;
	Fun_UpdateBtnState();
}

void CPrecisionDlg::OnBnClickedCurrentCc()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nSelectMode = 0;
	Fun_UpdateBtnState();
}

void CPrecisionDlg::OnBnClickedVoltageCv()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nSelectMode = 1;
	Fun_UpdateBtnState();
}

void CPrecisionDlg::OnBnClickedRadio()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(true);
}

void CPrecisionDlg::OnBnClickedRadio5()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(true);
	
	
}

void CPrecisionDlg::Fun_UpdateBtnState()
{
	switch ( m_nSelectMode )
	{
		case 0:
			{
				m_BtnCurrentCC.SetColor(RGB_BTN_FONT, RGB_YELLOW);
				m_BtnVoltageCV.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND);
			}
		break;
		
		case 1:
			{
				m_BtnCurrentCC.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND);
				m_BtnVoltageCV.SetColor(RGB_BTN_FONT, RGB_YELLOW);			
			}
		break;
	}
	
	switch( m_nSelectLevel )
	{
	case 0:
		{
			m_BtnLevel1.SetColor(RGB_BTN_FONT, RGB_YELLOW);
			m_BtnLevel2.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND);
			m_BtnLevel3.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND);
		}
		break;

	case 1:
		{
			m_BtnLevel1.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND);
			m_BtnLevel2.SetColor(RGB_BTN_FONT, RGB_YELLOW);
			m_BtnLevel3.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND);
		}
		break;
		
	case 2:
		{
			m_BtnLevel1.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND);
			m_BtnLevel2.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND);
			m_BtnLevel3.SetColor(RGB_BTN_FONT, RGB_YELLOW);		
		}
		break;
	}

}



void CPrecisionDlg::OnBnClickedPeriodWeek()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strTemp;
	COleDateTime	OleStartTime, OleEndTime;//(COleDateTime::GetCurrentTime());

	m_DateTimeEnd.GetTime(OleEndTime);
	COleDateTimeSpan elaplsed(7,0,0,0);			// 7일 전

	OleStartTime = OleEndTime - elaplsed;

	m_DateTimeStart.SetTime(OleStartTime);	
}

void CPrecisionDlg::OnBnClickedPeriodMonth()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strTemp;
	int iDate;
	COleDateTime OleStartTime, OleEndTime;//(COleDateTime::GetCurrentTime());

	m_DateTimeEnd.GetTime(OleEndTime);

	if(OleEndTime.GetMonth() == 1)
	{
		iDate = 12;
	}
	else
	{
		iDate = OleEndTime.GetMonth() - 1;
	}

	OleStartTime.SetDateTime(OleEndTime.GetYear(),iDate,OleEndTime.GetDay(),OleEndTime.GetHour(),OleEndTime.GetMinute(),OleEndTime.GetSecond());
	m_DateTimeStart.SetTime(OleStartTime);	
}

void CPrecisionDlg::Fun_ClearGrid()
{
	int nRow=0, nCol=0;
	for( nRow=0; nRow<m_ListChInfoGrid.GetRowCount(); nRow++ )
	{
		for( nCol=0; nCol<=m_ListChInfoGrid.GetColCount(); nCol++ )
		{
			m_ListChInfoGrid.SetValueRange(CGXRange(nRow+1,nCol), "");	
		}			
		
		m_ListChInfoGrid.SetRowCount(0);
	}
}

void CPrecisionDlg::OnClickedBtnSearch()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(true);
	
	Fun_ClearGrid();
	
	CString strData = _T("");
	int nModuleID = 0;
	
	nModuleID = m_ctrlStage.GetItemData(m_ctrlStage.GetCurSel());
		
	CString strSql = _T(""), strTemp = _T("");
	
	strSql = "Select UpdateTime, ChNo, ";	
		
	switch( m_nSelectLevel )
	{
		case LEVEL1:
			{
				if( m_nSelectMode == SELECT_CC )
				{
					strSql += "CCFaultCnt, CCLevel1Fault ";	
				}
				else
				{
					strSql += "CVFaultCnt, CVLevel1Fault ";		
				}
			}
			break;
		case LEVEL2:
			{
				if( m_nSelectMode == SELECT_CC )
				{
					strSql += "CCFaultCnt, CCLevel2Fault ";	
				}
				else
				{
					strSql += "CVFaultCnt, CVLevel2Fault ";		
				}
			}
			break;
		case LEVEL3:
			{
				if( m_nSelectMode == SELECT_CC )
				{
					strSql += "CCFaultCnt, CCLevel3Fault ";	
				}
				else
				{
					strSql += "CVFaultCnt, CVLevel3Fault ";		
				}
			}
			break;
	}
	
	strSql += "From ResultData";
	
	// 1. 모든 데이터 검색
	if( m_bSelectData == true )
	{
	
	}
	else
	{
		COleDateTime	OleStartTime, OleEndTime;
				
		m_DateTimeStart.GetTime(OleStartTime);
		m_DateTimeEnd.GetTime(OleEndTime);
		
		COleDateTimeSpan sp = OleEndTime - OleStartTime;
		
		if( sp.GetTotalMinutes() < 0 )
		{
			AfxMessageBox(TEXT_LANG[0]);//"날짜 검색 조건값이 잘못되었습니다. [시작기간 > 완료기간]"
			return;
		}
		
		INT64 iStartTime = OleStartTime.GetYear()	* 10000000000;
		iStartTime += OleStartTime.GetMonth()		* 100000000;
		iStartTime += OleStartTime.GetDay()			* 1000000;
		
		INT64 iEndTime = OleEndTime.GetYear()	* 10000000000;
		iEndTime += OleEndTime.GetMonth()		* 100000000;
		iEndTime += OleEndTime.GetDay()			* 1000000;
		iEndTime += 23		* 10000;
		iEndTime += 59		* 100;
		iEndTime += 59;
					
		strTemp.Format(" Where UpdateTime >= %I64d AND UpdateTime <= %I64d ", iStartTime, iEndTime );
	
		strSql += strTemp;
	}
	
	Search( nModuleID, strSql );	
}

// ret -1 : db open 경로 실패
// ret -2 : db open 실패
// ret -3 : DB 파일 실패
int CPrecisionDlg::Search(int nModuleID, CString strQuery)
{
	int nRtn = 0;
	INT sqlerr = 0;
	
	if( m_pDoc->m_strDBFolder.IsEmpty() )
	{
		return -1;
	}
	
	st_PRECISION_RESULT_INFO *p_StPrecision_result = new st_PRECISION_RESULT_INFO[32];
	ZeroMemory( p_StPrecision_result, sizeof(st_PRECISION_RESULT_INFO) * 32);
		
	CString dbname;
	dbname.Format("%s\\StageInfo\\Stage_%02d.db", m_pDoc->m_strDBFolder, nModuleID);
	
	sqlite3* mSqlite;

	sqlerr = sqlite3_open(dbname, &mSqlite);

	sqlite3_stmt * stmt = 0;
	
	sqlerr = sqlite3_prepare( mSqlite,  strQuery, -1, &stmt, 0 );
	if( sqlerr != SQLITE_OK )
	{
		LPSTR szError = (LPSTR)sqlite3_errmsg(mSqlite);
		return -2;
	}
	
	int c = 1;
	int nChannel = 0;
	int nGridRow = 0;
	int nCol = 0;
	CString strTemp = _T("");
	
	INT64 nPrevDate = 0;
	INT64 nUpdateDate = 0;
	
	INT64 nFaultCnt = 0;
	INT64 nLevelFaultCnt = 0;
	
	bool bFlag = false;		// 마지막 데이터 확인
	
	while( (sqlerr = sqlite3_step( stmt )) == SQLITE_ROW )
	{	
		c = 0;	
		
		nUpdateDate = sqlite3_column_int64( stmt, c++);
		if( nUpdateDate != nPrevDate)
		{	
			if( nPrevDate != 0 )
			{
				nGridRow++;
				m_ListChInfoGrid.SetRowCount(nGridRow);
				
				for( nCol=0; nCol <= m_ListChInfoGrid.GetColCount(); nCol++ )
				{
					if( nCol == 0 )
					{
						UINT64 idx = nPrevDate;
						INT year = idx / 10000000000;
						idx -= year * 10000000000;
						INT month = idx / 100000000;
						idx -= month * 100000000;
						INT day = idx / 1000000;
						idx -= day * 1000000;
						strTemp.Format("%d/%d", month, day);
					}
					else
					{
						if( m_nResultType == TYPE_COUNT )
						{
							strTemp.Format("%ld", p_StPrecision_result[nCol-1].nFaultCnt);					
						}
						else
						{
							float fRatio = 0.0f;
							if( p_StPrecision_result[nCol-1].nFaultCnt != 0 )
							{
								fRatio = (float)p_StPrecision_result[nCol-1].nFaultLevelCnt / (float)p_StPrecision_result[nCol-1].nFaultCnt * 100.0f;													
							}
							
							strTemp.Format("%.1f", fRatio);				
						}
					}
					m_ListChInfoGrid.SetValueRange(CGXRange(nGridRow, nCol), strTemp);					
				}
				ZeroMemory( p_StPrecision_result, sizeof(st_PRECISION_RESULT_INFO) * 32);
				bFlag = false;
			}
			
			nPrevDate = nUpdateDate;
		}
		bFlag = true;
		nChannel = sqlite3_column_int(stmt, c++);
		p_StPrecision_result[nChannel-1].nFaultCnt = p_StPrecision_result[nChannel-1].nFaultCnt + sqlite3_column_int64(stmt, c++);
		p_StPrecision_result[nChannel-1].nFaultLevelCnt = p_StPrecision_result[nChannel-1].nFaultLevelCnt + sqlite3_column_int64(stmt, c++);
	}
	
	if( bFlag == true )
	{
		// 1. 마지막 데이터를 그리드에 표시한다.
		nGridRow++;
		m_ListChInfoGrid.SetRowCount(nGridRow);
		
		for( nCol=0; nCol <= m_ListChInfoGrid.GetColCount(); nCol++ )
		{
			if( nCol == 0 )
			{
				UINT64 idx = nPrevDate;
				INT year = idx / 10000000000;
				idx -= year * 10000000000;
				INT month = idx / 100000000;
				idx -= month * 100000000;
				INT day = idx / 1000000;
				idx -= day * 1000000;
				strTemp.Format("%d/%d", month, day);
			}
			else
			{
				if( m_nResultType == TYPE_COUNT )
				{
					strTemp.Format("%ld", p_StPrecision_result[nCol-1].nFaultCnt);					
				}
				else
				{
					float fRatio = 0.0f;
					if( p_StPrecision_result[nCol-1].nFaultCnt != 0 )
					{
						fRatio = (float)p_StPrecision_result[nCol-1].nFaultLevelCnt / (float)p_StPrecision_result[nCol-1].nFaultCnt * 100.0f;													
					}

					strTemp.Format("%.1f", fRatio);				
				}
			}
			
			
			m_ListChInfoGrid.SetValueRange(CGXRange(nGridRow, nCol), strTemp);					
		}
	
	}
	
	if(SQLITE_DONE != sqlerr)
	{
		LPSTR szError = (LPSTR)sqlite3_errmsg(mSqlite);
		m_pDoc->WriteLog(szError);
		
		sqlerr = sqlite3_reset(stmt);

		if (sqlerr != SQLITE_OK)
		{
			szError = (LPSTR)sqlite3_errmsg(mSqlite);
			CFMSLog::WriteLog(szError);
		}
	}

	sqlite3_finalize(stmt);
	sqlite3_close(mSqlite);
	
	delete p_StPrecision_result;
	p_StPrecision_result = NULL;
	
	return nRtn;
}

void CPrecisionDlg::OnBnClickedPeriodAll()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(false);
	
	if( m_bSelectData == false )
	{
		m_bSelectData = true;		
		m_BtnPeriodAll.SetColor(RGB_BTN_FONT, RGB_YELLOW);
		
	}
	else
	{
		m_bSelectData = false;
		m_BtnPeriodAll.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND);
	}
	
	UpdateData(true);
}

void CPrecisionDlg::OnBnClickedPeriodToday()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int iDate;
	COleDateTime OleTime = COleDateTime::GetCurrentTime();
	
	m_DateTimeEnd.SetTime(OleTime);
			
	m_DateTimeStart.SetTime(OleTime);		
}

void CPrecisionDlg::OnBnClickedCsvOutput()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strTime,strData;
	CTime tm = CTime::GetCurrentTime();
	strTime.Format("%s", tm.Format("%y%m%d_%H%M%S"));

	CStdioFile	file;
	CString		strFileName;		

	int i=0;
	int j=0;

	CFileDialog dlg(false, "", strTime, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, TEXT_LANG[1]);//"csv 파일(*.csv)|*.csv|All Files (*.*)|*.*|"

	if(dlg.DoModal() == IDOK)
	{		
		strFileName = dlg.GetPathName();		
		file.Open(strFileName, CFile::modeCreate|CFile::modeReadWrite|CFile::modeNoTruncate);

		int nRowCnt = m_ListChInfoGrid.GetRowCount();
		int nColCnt = m_ListChInfoGrid.GetColCount();

		for( i=0; i<=nRowCnt; i++ )
		{
			for( j=0; j<=nColCnt; j++ )
			{
				if( j==0 )
				{
					strData = m_ListChInfoGrid.GetValueRowCol(i, j);
				}
				else
				{
					strData = strData + "," + m_ListChInfoGrid.GetValueRowCol(i, j);
				}
			}
			file.WriteString(strData+"\n");
		}		
		file.Close();
	}
}

void CPrecisionDlg::OnBnClickedAllChClear()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(true);
	
	CString strData = _T(""), strTemp = _T("");
	int nModuleID = 0;
	
	nModuleID = m_ctrlStage.GetItemData(m_ctrlStage.GetCurSel());
	
	m_ctrlStage.GetLBText(m_ctrlStage.GetCurSel(), strData);
	
	strTemp.Format(TEXT_LANG[2], strData );									//"[ %s ] 의 채널 에러 정보를 삭제하시겠습니까?"
	if(MessageBox(strTemp, "Clear", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;
	
	int nRtn = 0;
	INT sqlerr = 0;

	if( m_pDoc->m_strDBFolder.IsEmpty() )
	{
		return;
	}

	CString dbname;
	dbname.Format("%s\\StageInfo\\Stage_%02d.db", m_pDoc->m_strDBFolder, nModuleID);

	sqlite3* mSqlite;

	sqlerr = sqlite3_open(dbname, &mSqlite);

	sqlite3_stmt * stmt = 0;
	
	sqlerr = sqlite3_prepare( mSqlite, "Delete from ResultData;", -1, &stmt, 0 );
	
	sqlerr = sqlite3_step(stmt);

	if(SQLITE_DONE != sqlerr)
	{
		LPSTR szError = (LPSTR)sqlite3_errmsg(mSqlite);
		CFMSLog::WriteLog(szError);

		int nRowsChanged = sqlite3_changes(mSqlite);

		sqlerr = sqlite3_reset(stmt);

		if (sqlerr != SQLITE_OK)
		{
			szError = (LPSTR)sqlite3_errmsg(mSqlite);
			CFMSLog::WriteLog(szError);
		}
	}
	
	sqlite3_finalize(stmt);
	sqlite3_close(mSqlite);
	
	Fun_ClearGrid();
}
