// ScheduleData.cpp: implementation of the CScheduleData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ScheduleData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#include <math.h>

#define CT_CONFIG_REG_SEC "Config"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CScheduleData::CScheduleData()
{
	ResetData();
	LanguageinitMonConfig();

}

CScheduleData::CScheduleData(CString strFileName)
{
	SetSchedule(strFileName);
}


CScheduleData::~CScheduleData()
{
	CStep *pStep;
	for(int i = m_apStepArray.GetSize()-1; i>=0; i--)
	{
		pStep = (CStep *)m_apStepArray[i];
		delete pStep;
		pStep = NULL;
		m_apStepArray.RemoveAt(i);
	}
	m_apStepArray.RemoveAll();

	if( TEXT_LANG != NULL )					
	{					
		delete[] TEXT_LANG;					
		TEXT_LANG = NULL;					
	}

}
bool CScheduleData::LanguageinitMonConfig() 
{
	g_nLanguage = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Language", 0);
	g_strLangPath.Format("%s\\Lang\\GraphAnalyzer_Lang.ini", AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC ,"Path", "C:\Program Files\PNE CTS"));

	switch(g_nLanguage)
	{
	case 0:
		{
			SetThreadUILanguage( MAKELCID( MAKELANGID(LANG_KOREAN , SUBLANG_KOREAN) , SORT_DEFAULT));
			break;
		}

	case 1:
		{
			SetThreadUILanguage( MAKELCID( MAKELANGID(LANG_ENGLISH , SUBLANG_ENGLISH_US) , SORT_DEFAULT));
			break;
		}

	case 2:
		{
			SetThreadUILanguage( MAKELCID( MAKELANGID(LANG_CHINESE_SIMPLIFIED , SUBLANG_CHINESE_SIMPLIFIED) , SORT_DEFAULT));
			break;
		}
	}

	int i=0;				
	int nTextCnt = 0;				
	TEXT_LANG = NULL;				

	CString strErr = _T("");				
	CString strTemp = _T("");				
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("[IDD_ScheduleData]"), _T("TEXT_ScheduleData_CNT"), _T("TEXT_ScheduleData_CNT"));				

	nTextCnt = atoi(strTemp);				

	if( nTextCnt > 0 )				
	{				
		TEXT_LANG = new CString[nTextCnt];			

		for( i=0; i<nTextCnt; i++ )			
		{			
			strTemp.Format("TEXT_ScheduleData_%d", i);		
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("[IDD_ScheduleData]"), strTemp, strTemp, g_nLanguage);		

			if( TEXT_LANG[i] == strTemp )		
			{		
				if( strErr.IsEmpty() )	
				{	
					strErr = "Languge error " + strTemp; 
				}	
				else	
				{	
					strErr += "," + strTemp;
				}	
			}		
		}			

		if( !strErr.IsEmpty() )			
		{			
			AfxMessageBox(strErr);		
			return false;		
		}			
	}				

	return true;				
}
//Schedule File로 부터 공정 조건을 Load한다.
BOOL CScheduleData::SetSchedule(CString strFileName)
{
	if(strFileName.IsEmpty())	return FALSE;
	
	CString strTemp;

	ResetData();

	//File Open
	FILE *fp = fopen(strFileName, "rt");
	if(fp == NULL){
		return FALSE;
	}

	//파일 Header 기록 
	PS_FILE_ID_HEADER fileHeader;
	if(fread(&fileHeader, sizeof(PS_FILE_ID_HEADER), 1, fp) < 1)				//기록 실시 
	{
		fclose(fp);
		return FALSE;
	}
	//Header 정보 검사
	if(fileHeader.nFileID != SCHEDULE_FILE_ID)
	{
		fclose(fp);
		return FALSE;
	}
	//Header 정보 검사
	if(fileHeader.nFileVersion  > SCHEDULE_FILE_VER)
	{
		fclose(fp);
		return FALSE;
	}

	//모델 정보 기록 
	FILE_TEST_INFORMATION modelData;
	if(fread(&modelData, sizeof(modelData), 1, fp) < 1)			//기록 실시 
	{
		fclose(fp);
		return FALSE;
	}
	m_ModelData = modelData;

	//공정 정보 기록 
	FILE_TEST_INFORMATION testData;
	if(fread(&testData, sizeof(testData), 1, fp)  < 1)			//기록 실시 
	{
		fclose(fp);
		return FALSE;
	}
	m_ProcedureData = testData;

	//cell check 조건 기록 
	FILE_CELL_CHECK_PARAM cellCheck;
	if(fread(&cellCheck, sizeof(cellCheck), 1, fp)  < 1)				//기록 실시 
	{
		fclose(fp);
		return FALSE;
	}
	m_CellCheck = cellCheck;

	//step 정보 기록 
	CStep *pStep;
	FILE_STEP_PARAM_10000	oldFileStep;
	FILE_STEP_PARAM	stepData;
	
	while(1)
	{
		if(fileHeader.nFileVersion < SCHEDULE_FILE_VER)
		{
			if(fread(&oldFileStep, sizeof(oldFileStep), 1, fp) < 1)			//기록 실시 
				break;				

			memcpy(&stepData, &oldFileStep, sizeof(oldFileStep));
		}
		else
		{
			if(fread(&stepData, sizeof(stepData), 1, fp) < 1)			//기록 실시 
				break;				
		}

		
		pStep = new CStep;
		pStep->SetStepData(&stepData);
		m_apStepArray.Add(pStep);
	}
	fclose(fp);

	return TRUE;
}


//공정 조건을 File로 저장한다.
BOOL CScheduleData::SaveToFile(CString strFileName)
{
	CString strTemp;
	if(strFileName.IsEmpty())	return FALSE;

	FILE *fp = fopen(strFileName, "wt");
	if(fp == NULL){
		return FALSE;
	}

	//파일 Header 기록 
	PS_FILE_ID_HEADER	FileHeader;
	ZeroMemory(&FileHeader, sizeof(PS_FILE_ID_HEADER));

	COleDateTime dateTime = COleDateTime::GetCurrentTime();
	sprintf(FileHeader.szCreateDateTime, "%s", dateTime.Format());			//기록 시간
	FileHeader.nFileID = SCHEDULE_FILE_ID;									//파일 ID
	FileHeader.nFileVersion = SCHEDULE_FILE_VER;							//파일 Version
	sprintf(FileHeader.szDescrition, "ADP power supply schedule file.");	//파일 서명 
	fwrite(&FileHeader, sizeof(PS_FILE_ID_HEADER), 1, fp);					//기록 실시 
	
	//모델 정보 기록 
	fwrite(&m_ModelData, sizeof(m_ModelData), 1, fp);				//기록 실시 

	//공정 정보 기록 
	fwrite(&m_ProcedureData, sizeof(m_ProcedureData), 1, fp);		//기록 실시 

	//cell check 조건 기록 
	fwrite(&m_CellCheck, sizeof(m_CellCheck), 1, fp);				//기록 실시 
	
	//step 정보 기록 
	FILE_STEP_PARAM fileStep;
	
	CStep *pStep;
	for(int i = 0; i< m_apStepArray.GetSize(); i++)
	{
		pStep = (CStep *)m_apStepArray.GetAt(i);
		fileStep = pStep->GetFileStep();
		fwrite(&fileStep, sizeof(fileStep), 1, fp);					//기록 실시 

	}
	fclose(fp);
	return TRUE;
}

UINT CScheduleData::GetStepSize()
{
	return m_apStepArray.GetSize();
}

void CScheduleData::ResetData()
{
	memset(&m_ModelData, 0, sizeof(m_ModelData));
	memset(&m_ProcedureData, 0, sizeof(m_ProcedureData));
	memset(&m_CellCheck, 0, sizeof(m_CellCheck));

	CStep *pStep;
	for(int i = m_apStepArray.GetSize()-1; i>=0; i--)
	{
		pStep = (CStep *)m_apStepArray[i];
		delete pStep;
		pStep = NULL;
		m_apStepArray.RemoveAt(i);
	}
	m_apStepArray.RemoveAll();

	m_bContinueCellCode = TRUE;
}

//Load Schedule Data from database
BOOL CScheduleData::SetSchedule(CString strDBName, long lModelPK, long lTestPK)
{
	CDaoDatabase  db;
	if(strDBName.IsEmpty())		return 0;

	try
	{
		db.Open(strDBName);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}
	
	ResetData();
	
	if(LoadModelInfo(db, lModelPK) == FALSE)
	{
		db.Close();
		return FALSE;
	}

	if(LoadProcedureInfo(db, lModelPK, lTestPK)== FALSE)
	{
		db.Close();
		return FALSE;
	}

	if(LoadCellCheckInfo(db, lTestPK) == FALSE)
	{
		db.Close();
		return FALSE;
	}

	if(LoadStepInfo(db, lTestPK) == FALSE)
	{
		db.Close();
		return FALSE;
	}

	db.Close();
	return TRUE;
}

BOOL CScheduleData::LoadModelInfo(CDaoDatabase &db, long lPK)
{
	ASSERT(db.IsOpen());

	CString strSQL;
	strSQL.Format("SELECT No, ModelName, Description, CreatedTime FROM BatteryModel WHERE ModelID = %d ORDER BY No", lPK);
	
	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return 0;
	}	
	
	if(rs.IsBOF() || rs.IsEOF())
	{
		rs.Close();
		return FALSE;
	}
	
	ZeroMemory(&m_ModelData, sizeof(m_ModelData));

	//No
	data = rs.GetFieldValue(0);
	m_ModelData.lID = data.lVal;
	//ModelName
	data = rs.GetFieldValue(1);
	sprintf(m_ModelData.szName, "%s", data.pcVal);
	//Description
	data = rs.GetFieldValue(2);
	if(VT_NULL != data.vt)
	{
		sprintf(m_ModelData.szDescription, "%s", data.pcVal);
	}
	//DateTime
	data = rs.GetFieldValue(3);
	COleDateTime  date = data;
	sprintf(m_ModelData.szModifiedTime, "%s", date.Format());

	rs.Close();

	return TRUE;
}

BOOL CScheduleData::LoadProcedureInfo(CDaoDatabase &db, long lModelPK, long lProcPK)
{
	ASSERT(db.IsOpen());

	CString strSQL;
	strSQL.Format("SELECT TestNo, ProcTypeID, TestName, Description, Creator, ModifiedTime FROM TestName WHERE ModelID =  %d AND TestID = %d ORDER BY TestNo", 
					lModelPK, lProcPK);
	
	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return 0;
	}	
	
	if(rs.IsBOF() || rs.IsEOF())
	{
		rs.Close();
		return FALSE;
	}
	
	ZeroMemory(&m_ProcedureData, sizeof(m_ProcedureData));
	
	//TestNo
	data = rs.GetFieldValue(0);
	m_ProcedureData.lID = data.lVal;
	//ProcTypeID
	data = rs.GetFieldValue(1);
	m_ProcedureData.lType = data.lVal;
	//TestName
	data = rs.GetFieldValue(2);
	sprintf(m_ProcedureData.szName, "%s", data.pcVal);
	//Description
	data = rs.GetFieldValue(3);
	if(VT_NULL != data.vt)
	{
		sprintf(m_ProcedureData.szDescription, "%s", data.pcVal);
	}
	//Creator
	data = rs.GetFieldValue(4);
	if(VT_NULL != data.vt)
	{
		sprintf(m_ProcedureData.szCreator, "%s", data.pcVal);		
	}
	//DateTime
	data = rs.GetFieldValue(5);
	COleDateTime  date = data;
	sprintf(m_ProcedureData.szModifiedTime, "%s", date.Format());

	rs.Close();

	return TRUE;
}

BOOL CScheduleData::LoadCellCheckInfo(CDaoDatabase &db, long lProcPK)
{
	ASSERT(db.IsOpen());
	
	CString strSQL, strQuery;
	CString strFrom;
	strSQL = "SELECT";
	strSQL += " MaxV, MinV, CurrentRange, OCVLimit, TrickleCurrent, TrickleTime, DeltaVoltage, MaxFaultBattery, AutoTime, AutoProYN, PreTestCheck";
	strFrom.Format(" FROM Check WHERE TestID = %d ORDER BY CheckID", lProcPK);
	
	strQuery = strSQL+strFrom;

	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strQuery, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	
	if(!rs.IsEOF() && !rs.IsBOF())
	{
		data = rs.GetFieldValue(0);		//long	MaxV;	
		m_CellCheck.fMaxVoltage = data.fltVal;
		data = rs.GetFieldValue(1);		//long	MinV;
		m_CellCheck.fMinVoltage = data.fltVal;
		data = rs.GetFieldValue(2);		//long	CurrentRange;
		m_CellCheck.fMaxCurrent = data.fltVal;
		data = rs.GetFieldValue(3);		//long	OCVLimit;
		m_CellCheck.fVrefVal = data.fltVal;
		data = rs.GetFieldValue(4);		//long	TrickleCurrent;
		m_CellCheck.fIrefVal = data.fltVal;
		data = rs.GetFieldValue(5);		//float	TrickleTime;
		m_CellCheck.lTrickleTime = data.lVal;
		data = rs.GetFieldValue(6);		//float	DeltaVoltage;
		m_CellCheck.fDeltaVoltage = data.fltVal;
		data = rs.GetFieldValue(7);		//long	MaxFaultBattery;
		m_CellCheck.nMaxFaultNo = data.lVal;
		data = rs.GetFieldValue(8);		//float	AutoTime;
		data = rs.GetFieldValue(9);		//float	AutoProYN;
		data = rs.GetFieldValue(10);	//float	PreTestCheck;
		//?? DB값이 참이면 255가 들어 있음
		//?? 거짓이면 0가 들어 있음
		m_CellCheck.bPreTest = (BYTE)data.bVal == 0 ? FALSE : TRUE;
	}
	else
	{
		rs.Close();
		return FALSE;
	}
	rs.Close();
	
	return TRUE;
}

BOOL CScheduleData::LoadStepInfo(CDaoDatabase &db, long lProcPK)
{
	ASSERT(db.IsOpen());
	
	CString strSQL, strQuery;
	CString strFrom;
	strSQL = "SELECT ";
	strSQL += "StepID, StepNo, StepProcType, StepType, StepMode, Vref, Iref, EndTime, EndV, EndI, EndCapacity, End_dV, End_dI, CycleCount ";	//14
	strSQL += ", OverV, LimitV, OverI, LimitI, OverCapacity, LimitCapacity, OverImpedance, LimitImpedance, DeltaTime, DeltaTime1, DeltaV, DeltaI ";	//12
	strSQL += ", Grade, CompTimeV1, CompTimeV2, CompTimeV3,	CompVLow1,	CompVLow2, CompVLow3, CompVHigh1, CompVHigh2, CompVHigh3 ";	//10
	strSQL += ", CompTimeI1, CompTimeI2, CompTimeI3, CompILow1, CompILow2, CompILow3, CompIHigh1, CompIHigh2, CompIHigh3, RecordTime, RecordDeltaV, RecordDeltaI ";	//12
	strSQL += ", CapVLow, CapVHigh ";	//2
//	strSQL += ", EndCheckVLow, EndCheckVHigh, EndCheckILow, EndCheckIHigh ";		//4
	strSQL += ", Value0, Value1, Value2, Value3, Value4, Value5, Value6, Value7, Value8, Value9 ";	//10

	strFrom.Format(" FROM Step WHERE TestID = %d ORDER BY StepNo", lProcPK);
	
	strQuery = strSQL+strFrom;

	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strQuery, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return 0;
	}	
	
	CStep *pStepData;
	int nTemp1, nTemp2;
	float fTemp1, fTemp2, fTemp3, fTemp4, fTemp5;
	char szTemp[64];


	while(!rs.IsEOF())
	{
		pStepData = new CStep;
		ASSERT(pStepData);

		data = rs.GetFieldValue(0);		//long	m_StepID;	
		pStepData->m_StepID = data.lVal;
		data = rs.GetFieldValue(1);		//long	m_StepNo;
		pStepData->m_StepIndex = BYTE(data.lVal-1);
		data = rs.GetFieldValue(2);		//long	m_StepProcType;
		pStepData->m_lProcType = data.lVal;
		data = rs.GetFieldValue(3);		//long	m_StepType;
		pStepData->m_type = (BYTE)data.lVal;
		data = rs.GetFieldValue(4);		//long	m_StepMode;
		if(data.lVal < 0)	pStepData->m_mode = 0;
		else				pStepData->m_mode = (BYTE)data.lVal;
		data = rs.GetFieldValue(5);		//float	m_Vref;
		pStepData->m_fVref = data.fltVal;
		data = rs.GetFieldValue(6);		//float	m_Iref;
		pStepData->m_fIref = data.fltVal;
		data = rs.GetFieldValue(7);		//long	m_EndTime;
		pStepData->m_fEndTime = PCTIME(data.lVal);
		data = rs.GetFieldValue(8);		//float	m_EndV;
		pStepData->m_fEndV = data.fltVal;
		data = rs.GetFieldValue(9);		//float	m_EndI;
		pStepData->m_fEndI = data.fltVal;
		data = rs.GetFieldValue(10);	//float	m_EndCapacity;
		pStepData->m_fEndC = data.fltVal;
		data = rs.GetFieldValue(11);	//float	m_End_dV;
		pStepData->m_fEndDV = data.fltVal;
		data = rs.GetFieldValue(12);	//float	m_End_dI;
		pStepData->m_fEndDI = data.fltVal;
		data = rs.GetFieldValue(13);	//long	m_CycleCount;
		pStepData->m_nLoopInfoCycle = data.lVal;
		
		data = rs.GetFieldValue(14);	//float	m_OverV;
		pStepData->m_fHighLimitV = data.fltVal;
		data = rs.GetFieldValue(15);	//float	m_LimitV;
		pStepData->m_fLowLimitV = data.fltVal;
		data = rs.GetFieldValue(16);	//float	m_OverI;
		pStepData->m_fHighLimitI = data.fltVal;
		data = rs.GetFieldValue(17);	//float	m_LimitI;
		pStepData->m_fLowLimitI = data.fltVal;
		data = rs.GetFieldValue(18);	//float	m_OverCapacity;
		pStepData->m_fHighLimitC = data.fltVal;
		data = rs.GetFieldValue(19);	//float	m_LimitCapacity;
		pStepData->m_fLowLimitC = data.fltVal;
		data = rs.GetFieldValue(20);	//float	m_OverImpedance;
		pStepData->m_fHighLimitImp = data.fltVal;
		data = rs.GetFieldValue(21);	//float	m_LimitImpedance;
		pStepData->m_fLowLimitImp = data.fltVal;

		data = rs.GetFieldValue(22);	//long	m_DeltaTime;
		pStepData->m_fDeltaTimeV = PCTIME(data.lVal);
		data = rs.GetFieldValue(23);	//long	m_DeltaTime1;
		pStepData->m_fDeltaTimeI = PCTIME(data.lVal);
		data = rs.GetFieldValue(24);	//float	m_DeltaV;
		pStepData->m_fDeltaV = data.fltVal;
		data = rs.GetFieldValue(25);	//float	m_DeltaI;
		pStepData->m_fDeltaI = data.fltVal;
		data = rs.GetFieldValue(26);	//BOOL	m_Grade;

		//?? DB값이 참이면 255가 들어 있음
		//?? 거짓이면 0가 들어 있음
		if((BYTE)data.bVal == 0)
			pStepData->m_bGrade = FALSE;
		else
			pStepData->m_bGrade = TRUE;

		data = rs.GetFieldValue(27);	//long	m_CompTimeV1;
		pStepData->m_fCompTimeV[0] = PCTIME(data.lVal);
		data = rs.GetFieldValue(28);	//long	m_CompTimeV2;
		pStepData->m_fCompTimeV[1] = PCTIME(data.lVal);
		data = rs.GetFieldValue(29);	//long	m_CompTimeV3;
		pStepData->m_fCompTimeV[2] = PCTIME(data.lVal);
		data = rs.GetFieldValue(30);	//float	m_CompVLow1;
		pStepData->m_fCompLowV[0] = data.fltVal;
		data = rs.GetFieldValue(31);	//float	m_CompVLow2;
		pStepData->m_fCompLowV[1] = data.fltVal;
		data = rs.GetFieldValue(32);	//float	m_CompVLow3;
		pStepData->m_fCompLowV[2] = data.fltVal;
		data = rs.GetFieldValue(33);	//float	m_CompVHigh1;
		pStepData->m_fCompHighV[0] = data.fltVal;
		data = rs.GetFieldValue(34);	//float	m_CompVHigh2;
		pStepData->m_fCompHighV[1] = data.fltVal;
		data = rs.GetFieldValue(35);	//float	m_CompVHigh3;
		pStepData->m_fCompHighV[2] = data.fltVal;
		
		data = rs.GetFieldValue(36);	//long	m_CompTimeI1;
		pStepData->m_fCompTimeI[0] = PCTIME(data.lVal);
		data = rs.GetFieldValue(37);	//long	m_CompTimeI2;
		pStepData->m_fCompTimeI[1] = PCTIME(data.lVal);
		data = rs.GetFieldValue(38);	//long	m_CompTimeI3;
		pStepData->m_fCompTimeI[2] = PCTIME(data.lVal);
		data = rs.GetFieldValue(39);	//float	m_CompILow1;
		pStepData->m_fCompLowI[0] = data.fltVal;
		data = rs.GetFieldValue(40);	//float	m_CompILow2;
		pStepData->m_fCompLowI[1] = data.fltVal;
		data = rs.GetFieldValue(41);	//float	m_CompILow3;
		pStepData->m_fCompLowI[2] = data.fltVal;
		data = rs.GetFieldValue(42);	//float	m_CompIHigh1;
		pStepData->m_fCompHighI[0] = data.fltVal;
		data = rs.GetFieldValue(43);	//float	m_CompIHigh2;
		pStepData->m_fCompHighI[1] = data.fltVal;
		data = rs.GetFieldValue(44);	//float	m_CompIHigh3;
		pStepData->m_fCompHighI[2] = data.fltVal;

		data = rs.GetFieldValue(45);	//long	m_RecordTime;
		pStepData->m_fReportTime = PCTIME(data.lVal);
		data = rs.GetFieldValue(46);	//float	m_RecordDeltaV;
		pStepData->m_fReportV = data.fltVal;
		data = rs.GetFieldValue(47);	//float	m_RecordDeltaI;
		pStepData->m_fReportI = data.fltVal;

		data = rs.GetFieldValue(48);	//float	m_CapVLow;
		pStepData->m_fCapaVoltage1 = data.fltVal;
		data = rs.GetFieldValue(49);	//float	m_CapVHigh;
		pStepData->m_fCapaVoltage2 = data.fltVal;

		//Goto 정보 
		data = rs.GetFieldValue(50);	//CString	m_Value0;
		if(VT_NULL == data.vt)	pStepData->m_nLoopInfoGoto = 0;
		else					pStepData->m_nLoopInfoGoto = atol(data.pcVal);

		//Report Temp조건 들어옴 
		data = rs.GetFieldValue(51);	//CString	m_Value1;
		if(VT_NULL == data.vt)	pStepData->m_fReportTemp = 0.0f;
		else pStepData->m_fReportTemp = (float)atof(data.pcVal);

		//SOC Condition 
		data = rs.GetFieldValue(52);	//CString	m_Value2;
		if(VT_NULL == data.vt)
		{
			pStepData->m_bUseActualCapa = FALSE;
			pStepData->m_UseAutucalCapaStepNo = 0;
			pStepData->m_fSocRate = 0.0f;
		}
		else
		{
			sprintf(szTemp, "%s", data.pcVal);
			if(sscanf(szTemp, "%d %d %f", &nTemp1, &nTemp2, &fTemp1) == 3)
			{
				pStepData->m_bUseActualCapa = (BYTE)nTemp1;
				pStepData->m_UseAutucalCapaStepNo = (BYTE)nTemp2;
				pStepData->m_fSocRate = fTemp1;
			}

			TRACE("STEP %d : %d, %d, %f\n", pStepData->m_StepIndex+1, pStepData->m_bUseActualCapa, pStepData->m_UseAutucalCapaStepNo, pStepData->m_fSocRate);
		}

		//End Watt, Watthour Condition 
		data = rs.GetFieldValue(53);	//CString	m_Value2;
		if(VT_NULL == data.vt)
		{
			pStepData->m_fEndW = 0.0f;
			pStepData->m_fEndWh = 0.0f;
		}
		else
		{
			sprintf(szTemp, "%s", data.pcVal);
			if(sscanf(szTemp, "%f %f", &fTemp1, &fTemp2) == 2)
			{
				pStepData->m_fEndW = fTemp1;
				pStepData->m_fEndWh = fTemp2;
			}
		}

		//Safety Temp
		data = rs.GetFieldValue(54);
		if(VT_NULL == data.vt)
		{
			pStepData->m_fLowLimitTemp = 0.0f;
			pStepData->m_fHighLimitTemp= 0.0f;
		}
		else
		{
			sprintf(szTemp, "%s", data.pcVal);
			//안전온도하한, 안전온도상한, 종료 온도, 온도 설정값, 시작 온도값 
			if(sscanf(szTemp, "%f %f %f %f %f", &fTemp1, &fTemp2, &fTemp3, &fTemp4, &fTemp5) == 5)
			{
				pStepData->m_fLowLimitTemp = fTemp1;
				pStepData->m_fHighLimitTemp= fTemp2;
				pStepData->m_fEndT = fTemp3;
				pStepData->m_fTref = fTemp4;
				pStepData->m_fStartT = fTemp5;
			}
		}

		//Pattern file name
		data = rs.GetFieldValue(55);
		if(VT_NULL == data.vt)
		{
			pStepData->m_strPatternFileName = "";
		}
		else
			pStepData->m_strPatternFileName.Format("%s", data.pcVal);

		//Pattern data max min value
		data = rs.GetFieldValue(56);	//float m_ValueLimitLow;
		if(VT_NULL == data.vt)
		{
			pStepData->m_fValueMax = 0.0f;
			pStepData->m_fValueMin = 0.0f;
		}
		else
		{
			sprintf(szTemp, "%s", data.pcVal);
			if(sscanf(szTemp, "%f %f", &fTemp1, &fTemp2) == 2)
			{
				pStepData->m_fValueMax = fTemp1;
				pStepData->m_fValueMin = fTemp2;
			}
		}

		//EndV Goto 정보
		data = rs.GetFieldValue(57);	//CString	m_Value3;
		if(VT_NULL == data.vt)	pStepData->m_nLoopInfoEndVGoto = 0;
		else					pStepData->m_nLoopInfoEndVGoto = atol(data.pcVal);

		//EndT Goto 정보
		data = rs.GetFieldValue(58);	//CString	m_Value4;
		if(VT_NULL == data.vt)	pStepData->m_nLoopInfoEndTGoto = 0;
		else					pStepData->m_nLoopInfoEndTGoto = atol(data.pcVal);

		//EndC Goto 정보
		data = rs.GetFieldValue(59);	//CString	m_Value5;
		if(VT_NULL == data.vt)	pStepData->m_nLoopInfoEndCGoto = 0;
		else					pStepData->m_nLoopInfoEndCGoto = atol(data.pcVal);
		
		//Not Use
//		data = rs.GetFieldValue(60);	//CString	m_Value6;	
//		data = rs.GetFieldValue(61);	//CString	m_Value7;	
//		data = rs.GetFieldValue(62);	//CString	m_Value8;	
//		data = rs.GetFieldValue(63);	//CString	m_Value9;	

		if(pStepData->m_bGrade == TRUE)
		{
			LoadGradeInfo(db, pStepData);
		}
		rs.MoveNext();

		
		m_apStepArray.Add(pStepData);
	}

	rs.Close();
	
	return m_apStepArray.GetSize();
}

BOOL CScheduleData::LoadGradeInfo(CDaoDatabase &db, CStep *pStepData)
{
	ASSERT(db.IsOpen());
	ASSERT(pStepData);

	CString strSQL, strQuery;
	CString strFrom;
	
	strSQL.Format("SELECT Value, Value1, GradeItem, GradeCode FROM GRADE WHERE StepID = %d ORDER BY GradeID", pStepData->m_StepID);

	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return 0;
	}	
	
	
	float fdata1, fdata2; 
	CString strcode;
	while(!rs.IsEOF())
	{
		//value0
		data = rs.GetFieldValue(0);	
		fdata1 = data.fltVal;
		//value1
		data = rs.GetFieldValue(1);	
		fdata2 = data.fltVal;
		//Item
		data = rs.GetFieldValue(2);	
		pStepData->m_Grading.m_lGradingItem = data.lVal;
		//code
		data = rs.GetFieldValue(3);	
		strcode = data.pcVal;
		pStepData->m_Grading.AddGradeStep(fdata1, fdata2, strcode);
		rs.MoveNext();
	}

	return pStepData->m_Grading.GetGradeStepSize();
}

CStep* CScheduleData::GetStepData(UINT nStepIndex)
{
	if(GetStepSize() <= nStepIndex)
	{
		return NULL;
	}
	return (CStep *)m_apStepArray[nStepIndex];	
}

BOOL CScheduleData::ExecuteEditor(long /*lModelPK*/, long /*lTestPK*/)
{
/*	CString strTemp, strArgu;
	TCHAR szCurDir[MAX_PATH];
	::GetModuleFileName(AfxGetApp()->m_hInstance, szCurDir, MAX_PATH);
	strTemp = CString(szCurDir).Mid(0, CString(szCurDir).ReverseFind('\\')) + "\\CTSEditor.exe";
	strTemp.Format("%s\\CTSEditor.exe", CString(szCurDir).Mid(0, CString(szCurDir).ReverseFind('\\')));
	strArgu.Format("%s %s", strModel,  strTest);

	if(ExecuteProgram(strTemp, strArgu, "", "CTSEditor", bNewWnd, TRUE)== FALSE)	return FALSE;

	//Argument가 있으면 
	if(!strModel.IsEmpty())
	{
		HWND FirsthWnd;
		FirsthWnd = ::FindWindow(NULL, "CTSEditor");
		if (FirsthWnd)
		{
			int nSize = strModel.GetLength()+strTest.GetLength()+2;
			char *pData = new char[nSize];
			sprintf(pData, "%s %s", strModel, strTest);
			pData[nSize-1] = '\0';

			COPYDATASTRUCT CpStructData;
			CpStructData.dwData = 3;		//CTS2005 Program 구별 Index
			CpStructData.cbData = nSize;
			CpStructData.lpData = pData;
			
			::SendMessage(FirsthWnd, WM_COPYDATA, 0, (LPARAM)&CpStructData);
			
			delete [] pData;
		}

	}
*/	return TRUE;
}

void CScheduleData::ShowContent(int nStepIndex)
{	
}

CString CScheduleData::GetTypeString(int nType)
{
	return PSGetTypeMsg(nType);

}

CString CScheduleData::GetModeString(int nType, int nMode)
{
	CString msg;

	if(nType == PS_STEP_CHARGE || nType == PS_STEP_DISCHARGE)
	{
		switch(nMode)
		{
		case PS_MODE_CCCV:		msg = "CC/CV";		break;	
		case PS_MODE_CC	:		msg = "CC";			break;	
		case PS_MODE_CV	:		msg = "CV";			break;	
		case PS_MODE_CP	:		msg = "CP";			break;
		}
	}
	else if(nType == PS_STEP_IMPEDANCE)
	{
		if(nMode == PS_MODE_DCIMP)
		{
			msg = "DC";
		}
		else if(nMode == PS_MODE_ACIMP)
		{
			msg = "AC";
		}
	}
	return msg;
}

BOOL CScheduleData::ExecuteExcel(CString strFileName)
{
	STARTUPINFO	stStartUpInfo;
	PROCESS_INFORMATION	ProcessInfo;
	ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
	ZeroMemory(&ProcessInfo, sizeof(PROCESS_INFORMATION));
		
	stStartUpInfo.cb = sizeof(STARTUPINFO);
	stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
	stStartUpInfo.wShowWindow = SW_NORMAL;

	CString strTemp;
	strTemp = GetExcelPath();
	if(strTemp.IsEmpty())	return FALSE;

	char buff1[_MAX_PATH];
	char buff2[_MAX_PATH];

	int aa =0;
	do {
		//존재 여부 확인
		aa = GetShortPathName((LPSTR)(LPCTSTR)strTemp, buff2, _MAX_PATH);
		if(aa <= 0)
		{
			if(AfxMessageBox(TEXT_LANG[0], MB_ICONSTOP|MB_OKCANCEL) == IDCANCEL)
			{
				return FALSE;

			}
			strTemp = GetExcelPath();
			if(strTemp.IsEmpty())	return FALSE;
		}
		
	} while( aa <= 0 );	

	GetShortPathName((LPSTR)(LPCTSTR)strTemp, buff2, _MAX_PATH);
	GetShortPathName((LPSTR)(LPCTSTR)strFileName, buff1, _MAX_PATH);
	
	strTemp.Format("%s %s", buff2, buff1);
	
	BOOL bFlag = ::CreateProcess(NULL, (LPSTR)(LPCTSTR)strTemp, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &stStartUpInfo, &ProcessInfo);
	if(bFlag == FALSE)
	{
		strTemp.Format(TEXT_LANG[1], strFileName);
		AfxMessageBox(strTemp, MB_OK|MB_ICONSTOP);
	}
	return TRUE;
}

CString CScheduleData::GetExcelPath()
{
	CString strExcelPath = AfxGetApp()->GetProfileString("Config","Excel Path");
	CFileFind finder;
	if(finder.FindFile(strExcelPath) == FALSE)
	{
		CFileDialog pDlg(TRUE, "exe", strExcelPath, OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, "exe file(*.exe)|*.exe|All Files(*.*)|*.*|");
		pDlg.m_ofn.lpstrTitle = "Excel File Loacation";
		if(IDOK != pDlg.DoModal())
		{
			return	"";
		}
		strExcelPath = pDlg.GetPathName();
		AfxGetApp()->WriteProfileString("Config","Excel Path", strExcelPath);
	}
	return strExcelPath;
}

//Schedule에 Impdedance step이 포함되어 있는지 검사
BOOL CScheduleData::IsThereImpStep()
{
	CStep *pStep;
	for(int step = 0; step<GetStepSize(); step++)
	{
		pStep = GetStepData(step);
		if(pStep)
		{
			if(pStep->m_type == PS_STEP_IMPEDANCE)
			{
				return TRUE;
			}
		}
	}
	return FALSE;
}

//Schedule에 Grading을 실시하는 Step이 존재하는지 검사 
BOOL CScheduleData::IsThereGradingStep()
{
	CStep *pStep;
	for(int step = 0; step<GetStepSize(); step++)
	{
		pStep = GetStepData(step);
		if(pStep)
		{
			if(pStep->m_Grading.GetGradeStepSize() > 0)
			{
				return TRUE;
			}
		}
	}
	return FALSE;
}

//현재 Step이 속한 Cycle의 반복 횟수를 구한다.
int CScheduleData::GetCurrentStepCycle(int nStepIndex)
{
	if(nStepIndex < 0)		return 0;

	for(int step = nStepIndex; step < GetStepSize(); step++)
	{
		CStep *pStep = GetStepData(step);
		if(pStep && pStep->m_type == PS_STEP_LOOP)
		{
			return pStep->m_nLoopInfoCycle;
		}
	}
	return 0;
}

BOOL CScheduleData::AddStep(FILE_STEP_PARAM *pStepData)
{
	CStep *pStep;
	pStep = new CStep;
	pStep->SetStepData(pStepData);
	m_apStepArray.Add(pStep);

	return TRUE;
}

