#if !defined(AFX_LOGINDLG_H__5F1415B8_D237_42A9_B001_9F507F3E236A__INCLUDED_)
#define AFX_LOGINDLG_H__5F1415B8_D237_42A9_B001_9F507F3E236A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LoginDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLoginDlg dialog

class CLoginDlg : public CDialog
{
// Construction
public:
	CLoginDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CLoginDlg();

	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	BOOL IDPasswordCheck();
	
//	CFile m_hLogDataFile;
	BOOL SearchUser(CString strUserID, BOOL bPassWordCheck = TRUE, CString strPassword = "" , SCH_LOGIN_INFO *pUserData = NULL);

	SCH_LOGIN_INFO	m_LoginInfo;
// Dialog Data
	//{{AFX_DATA(CLoginDlg)
	enum { IDD = IDD_LOGON_DLG };
	BOOL	m_bRemember;
	CString	m_strPassword;
	CString	m_strLoginID;
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLoginDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CLoginDlg)
	afx_msg void OnOk();
	virtual BOOL OnInitDialog();
	afx_msg void OnChangePassword();
	afx_msg void OnChangeLoginid();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LOGINDLG_H__5F1415B8_D237_42A9_B001_9F507F3E236A__INCLUDED_)
