// TestCondition.cpp: implementation of the CTestCondition class.
//
//////////////////////////////////////////////////////////////////////

#include "Stdafx.h"

//#include "EPProcType.h"
#include "TestCondition.h"
#include "Grading.h"
#include "Step.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


CTestCondition::CTestCondition()
{
	RemoveStep();

	LanguageinitMonConfig();
}

CTestCondition::~CTestCondition()
{
	RemoveStep();

	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
	
}

bool CTestCondition::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CTestCondition"), _T("TEXT_CTestCondition_CNT"), _T("TEXT_CTestCondition_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CTestCondition_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CTestCondition"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

/*
BOOL CTestCondition::ResetData()
{
	m_lProcType = 0;
	memset(&m_modelHeader, 0, sizeof(STR_CONDITION_HEADER));
	memset(&m_conditionHeader, 0, sizeof(STR_CONDITION_HEADER));
	memset(&m_testHeader, 0, sizeof(EP_TEST_HEADER));
	memset(&m_boardParam, 0, sizeof(STR_CHECK_PARAM));
	m_pkProcedure =0;
	m_pkStep = 0;
	return TRUE;
}
*/
void CTestCondition::SetTestConditon(CTestCondition *pCondition)
{
	if(this == pCondition)	return;

	RemoveStep();

	m_lProcType = pCondition->GetTestProcType();
	memcpy(&m_modelHeader, pCondition->GetModelInfo(), sizeof(STR_CONDITION_HEADER));
	memcpy(&m_conditionHeader, pCondition->GetTestInfo(), sizeof(STR_CONDITION_HEADER));
	memcpy(&m_boardParam, pCondition->GetCheckParam(), sizeof(STR_CHECK_PARAM));

	CStep *pStep1, *pStep2;
	STR_COMMON_STEP comStep;
	for(int i=0; i<pCondition->GetTotalStepNo(); i++)
	{
		pStep1 = pCondition->GetStep(i);
		if(pStep1)
		{
			pStep2 = new CStep;
			comStep= pStep1->GetStepData();
			pStep2->SetStepData(&comStep);
			m_apStepList.Add(pStep2);
		}
	}
}

/*
BOOL CTestCondition::SetStepProcedureType(int nStepIndex, int nType)
{
	ASSERT(nStepIndex < EP_MAX_STEP && nStepIndex>= 0);

	if(nStepIndex < 0 )	return FALSE;

	m_aStepProcType[nStepIndex] = nType;

	return TRUE;
}
*/
int CTestCondition::GetStepProcedureType(int nStepIndex)
{
/*	ASSERT(nStepIndex < EP_MAX_STEP);

	if(nStepIndex < 0 || nStepIndex >= m_testHeader.totalStep)	return EP_PROC_TYPE_NONE;
	
	return m_aStepProcType[nStepIndex];
*/
	CStep *pStep = GetStep(nStepIndex);
	if(pStep)		return pStep->m_lProcType;
	return 0;

}

long CTestCondition::GetModelID()
{
	return m_modelHeader.lID;
}

int CTestCondition::FindNextTestName(long lModelID, long lTestID, STR_CONDITION_HEADER *pTestHeader, long *pnProcType, long *plModelID)
{
	ASSERT(pTestHeader != NULL);

/*	CTestListRecordSet	recordSet;
	recordSet.m_strFilter.Format("[ModelID] = %ld AND [TestID] = %ld", lModelID, lTestID);
	recordSet.m_strSort.Format("[TestNo]");		//2002/6/19  공정 순서를 번호로 관리 

	try
	{
		recordSet.Open();
	}
	catch (CDBException* e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();
		return FALSE;
	}
	
	if(recordSet.IsEOF() || recordSet.IsBOF())		//현재 조건을 찾을 수 없음  
	{
		recordSet.Close();
		return FALSE;
	}
	int nTestNo = recordSet.m_TestNo+1;	//현재 조건의 Test No를 구함 
	recordSet.m_strFilter.Format("[ModelID] = %ld AND [TestNo] = %ld", lModelID, nTestNo);
	recordSet.Requery();
	
	if(recordSet.IsEOF() || recordSet.IsBOF())		//다음 조건을 찾을 수 없음  
	{
		recordSet.Close();
		return FALSE;
	}

	pTestHeader->lID = recordSet.m_TestID;
	sprintf(pTestHeader->szName, "%s", recordSet.m_TestName);
	sprintf(pTestHeader->szDescription, "%s", recordSet.m_Description);
	sprintf(pTestHeader->szCreator, "%s", recordSet.m_Creator);
	
	sprintf(pTestHeader->szModifiedTime, "%s",  recordSet.m_ModifiedTime.Format());

	if(pnProcType)	*pnProcType = recordSet.m_ProcTypeID;
	if(plModelID)	*plModelID = recordSet.m_ModelID;

	recordSet.Close();
*/
/*	TCHAR szCurDir[MAX_PATH];
	::GetModuleFileName(AfxGetApp()->m_hInstance, szCurDir, MAX_PATH);
	CString strTemp;
	strTemp = CString(szCurDir).Mid(0, CString(szCurDir).ReverseFind('\\'));
	strTemp = strTemp + "\\DataBase\\" +FORM_SET_DATABASE_NAME;
*/
	CString strTemp;
	strTemp = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");	//Get Current Folder(CTSMon Folde)
	if(strTemp.IsEmpty())
	{
		return 0;
	}

	strTemp = strTemp + "\\" +FORM_SET_DATABASE_NAME;
	
	CDaoDatabase  db;
	try
	{
		db.Open(strTemp);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	
	CString strSQL;
	strSQL.Format("SELECT TestNo FORM TestName WHERE ModelID = %ld AND TestID = %d ORDER BY TestNo", lModelID, lTestID);
	
	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}
	
	if(rs.IsBOF() || rs.IsEOF())
	{
		rs.Close();
		db.Close();
		return FALSE;
	}

	data = rs.GetFieldValue(0);		//TestNo
	int nTestNo = data.lVal+1;		//현재 조건의 Test No를 구함 

	rs.Close();

	strSQL.Format("SELECT TestID, TestName, Description, ModifiedTime, ProcTypeID FORM TestName WHERE ModelID = %ld AND TestNo = %ld", lModelID, nTestNo);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}
	
	if(rs.IsBOF() || rs.IsEOF())
	{
		rs.Close();
		db.Close();
		return FALSE;
	}

	//Init m_ConditionMain
	data = rs.GetFieldValue(0);		//TestID
	pTestHeader->lID = data.lVal;
	data = rs.GetFieldValue(1);		//TestName
	sprintf(pTestHeader->szName, "%s", data.pcVal);
	data = rs.GetFieldValue(2);		//Description
	sprintf(pTestHeader->szDescription, "%s", data.pcVal);
	data = rs.GetFieldValue(3);		//ModifiedTime
	COleDateTime  date = data;
	sprintf(pTestHeader->szModifiedTime, "%s", date.Format()); 

	data = rs.GetFieldValue(3);		//ProcTypeID
	pTestHeader->lType = data.lVal;
	if(pnProcType)	*pnProcType = data.lVal;
	data = rs.GetFieldValue(4);		//ModelID
	if(plModelID)	*plModelID = lModelID;

	pTestHeader->lNo = nTestNo;

	rs.Close();
	db.Close();

	return TRUE;	
}

int CTestCondition::RequeryTestName(long lTestID, long lModelID, STR_CONDITION_HEADER *pTestHeader, long *pnProcType, long *plContinueCellCode)
{
	ASSERT(pTestHeader != NULL);

/*	CTestListRecordSet	recordSet;
	recordSet.m_strFilter.Format("[TestID] = %ld", lTestID);
	recordSet.m_strSort.Format("[TestID]");
	try
	{
		recordSet.Open();
	}
	catch (CDBException* e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();
		return FALSE;
	}
	
	if(recordSet.IsEOF())
	{
		recordSet.Close();
		return FALSE;
	}

	//Init m_ConditionMain
	pTestHeader->lID = lTestID;
	sprintf(pTestHeader->szName, "%s", recordSet.m_TestName);
	sprintf(pTestHeader->szDescription, "%s", recordSet.m_Description);
	sprintf(pTestHeader->szCreator, "%s", recordSet.m_Creator);
	
	//1/25/1996 8:30:00
	sprintf(pTestHeader->szModifiedTime, "%d/%d/%d %d:%d:%d", 
		recordSet.m_ModifiedTime.GetMonth(), 
		recordSet.m_ModifiedTime.GetDay(),
		recordSet.m_ModifiedTime.GetYear(),
		recordSet.m_ModifiedTime.GetHour(),
		recordSet.m_ModifiedTime.GetMinute(),
		recordSet.m_ModifiedTime.GetSecond()
	);

	if(pnProcType)	*pnProcType = recordSet.m_ProcTypeID;
	if(plModelID)	*plModelID = recordSet.m_ModelID;

	recordSet.Close();
*/
/*	TCHAR szCurDir[MAX_PATH];
	::GetModuleFileName(AfxGetApp()->m_hInstance, szCurDir, MAX_PATH);
	CString strTemp;
	strTemp = CString(szCurDir).Mid(0, CString(szCurDir).ReverseFind('\\'));
	strTemp = strTemp + "\\DataBase\\" +FORM_SET_DATABASE_NAME;
*/

	CString strTemp;
	strTemp = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");	//Get Current Folder(CTSMon Folde)
	if(strTemp.IsEmpty())
	{
		return 0;
	}

	strTemp = strTemp + "\\" +FORM_SET_DATABASE_NAME;
	
	CDaoDatabase  db;
	try
	{
		db.Open(strTemp);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	

	CString strSQL;
	strSQL.Format("SELECT TestName, Description, ModifiedTime, ProcTypeID, TestNo, Creator, PreTestCheck FROM TestName WHERE TestID = %d AND ModelID = %ld", lTestID, lModelID);
	
	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}
	
	if(rs.IsBOF() || rs.IsEOF())
	{
		rs.Close();
		db.Close();
		return FALSE;
	}

	//Init m_ConditionMain
	pTestHeader->lID = lTestID;		//TestID
	data = rs.GetFieldValue(0);		//TestName
	sprintf(pTestHeader->szName, "%s", data.pcVal);
	data = rs.GetFieldValue(1);		//Description
	sprintf(pTestHeader->szDescription, "%s", data.pcVal);
	data = rs.GetFieldValue(2);		//ModifiedTime
	COleDateTime  date = data;
	sprintf(pTestHeader->szModifiedTime, "%s", date.Format()); 

	data = rs.GetFieldValue(3);		//ProcTypeID
	pTestHeader->lType = data.lVal;
	if(pnProcType)	*pnProcType = data.lVal;
	
	
	data = rs.GetFieldValue(4);		//TestNo
	pTestHeader->lNo = data.lVal;

	data = rs.GetFieldValue(5);		//Creator
	sprintf(pTestHeader->szCreator, "%s", data.pcVal);

	data = rs.GetFieldValue(6);		//PreTestCheck
	if(plContinueCellCode)
	{
		if(data.lVal == 0)
			*plContinueCellCode =  FALSE;
		else
			*plContinueCellCode =  TRUE;
	}
	
	rs.Close();
	db.Close();

	return TRUE;
}

//DataBase에서 pCondition에 step조건 Laoding
BOOL CTestCondition::LoadTestCondition(long lTestID, long lModelID, STR_CONDITION *pCondition)
{
	long nTotNum = 0;
	long lProcType = 0;

	long bContinueCellCode = TRUE;
	
	RemoveStep();

	if(RequeryTestName(lTestID, lModelID, &m_conditionHeader, &lProcType, &bContinueCellCode) == FALSE)	return FALSE;
	
	if(pCondition)
	{
		pCondition->conditionHeader = m_conditionHeader;
		pCondition->bContinueCellCode = bContinueCellCode;
	}
	
	m_bContinueCellCode = bContinueCellCode;

	//선택 공정이 속한 Battery Model 정보 Load
	if(RequeryBatteryModel(lModelID, &m_modelHeader) == FALSE)
	{
		AfxMessageBox("Model data loading fail.");
		return FALSE;
	}
	
	if(pCondition)
	{
		pCondition->modelHeader = m_modelHeader;
	}

	if(RequeryCheckParam(m_conditionHeader.lID, lModelID, &m_boardParam) == FALSE)
	{
		AfxMessageBox("Cell check parameter loading fail.");
		return FALSE;
	}	
	if(pCondition)
	{
		pCondition->checkParam = m_boardParam;
	}

	nTotNum = RequeryStepCondition(m_conditionHeader.lID, lModelID, pCondition);
	if(nTotNum <= 0 || nTotNum > EP_MAX_STEP)
	{
		AfxMessageBox("Step data loading fail.");
		return FALSE;
	}
	if(pCondition)
	{
		STR_COMMON_STEP commStep, *pCommStep;
		CStep *pStep;
		for(int i =0; i<GetTotalStepNo(); i++)
		{
			pStep = GetStep(i);
			if(pStep)
			{
				commStep = pStep->GetStepData();
				pCommStep = new STR_COMMON_STEP;
				memcpy(pCommStep, &commStep, sizeof(STR_COMMON_STEP));
				pCondition->apStepList.Add(pCommStep);
			}
		}
	}
	return TRUE;
}

/* kky
BOOL CTestCondition::LoadTestCondition(st_CHARGER_INRESEVE& WorkInfo, STR_CONDITION *pCondition)
{
	long bContinueCellCode = TRUE;

	CHAR Type[3+1];
	CHAR Process_No[2+1];
	
	m_conditionHeader.lID = atoi(WorkInfo.F_Value.Process_No);
	m_conditionHeader.lNo = atoi(WorkInfo.F_Value.Process_No);
	m_conditionHeader.lType;
	m_conditionHeader.szName;
	m_conditionHeader.szDescription;
	m_conditionHeader.szCreator;
	m_conditionHeader.szModifiedTime;

	if(pCondition)
	{
		pCondition->conditionHeader = m_conditionHeader;
		pCondition->bContinueCellCode = bContinueCellCode;
	}

	m_modelHeader.lID = atoi(WorkInfo.F_Value.Type);
	m_modelHeader.lNo = atoi(WorkInfo.F_Value.Type);
	m_modelHeader.lType;
	m_modelHeader.szName;
	m_modelHeader.szDescription;
	m_modelHeader.szCreator;
	m_modelHeader.szModifiedTime;

	if(pCondition)
	{
		pCondition->modelHeader = m_modelHeader;
	}

	m_boardParam.fOCVUpperValue;
	m_boardParam.fOCVLowerValue;
	m_boardParam.fVRef;
	m_boardParam.fIRef;
	m_boardParam.fTime;
	m_boardParam.fDeltaVoltage;
	m_boardParam.fMaxFaultBattery;
	m_boardParam.compFlag;
	m_boardParam.autoProcessingYN;
	m_boardParam.reserved;

	//////////////////////////////////////////////////////////////////////////
	st_Step_Set *pFMSStep = WorkInfo.Step_Info;

	INT totStep = atoi(WorkInfo.F_Value.Total_Step);
		
	INT i = 0;
	for(; i < totStep; i++)
	{
		STR_COMMON_STEP _step;
		ZeroMemory(&_step, sizeof(STR_COMMON_STEP));

		_step.stepHeader.stepIndex = atoi(pFMSStep[i].Step_ID);

		FMS_Step_Type fmsStepType = (FMS_Step_Type)atoi(pFMSStep[i].Step_Type);
		//EP_TYPE_CHARGE
		//EP_MODE_CCCV
		switch(fmsStepType)
		{
		case FMS_CHG:
			{
				_step.stepHeader.type
					= EP_TYPE_CHARGE;
				_step.stepHeader.mode
					= EP_MODE_CCCV;

				_step.fEndI = atof(pFMSStep[i].CutOff);

				_step.fOverV = atof(WorkInfo.B_Value.Protect.Charge_H_Vol);
				_step.fLimitV = atof(WorkInfo.B_Value.Protect.Charge_L_Vol);
				_step.fOverI = atof(WorkInfo.B_Value.Protect.Charge_H_Cur);
				_step.fLimitI = atof(WorkInfo.B_Value.Protect.Charge_L_Cur);
				_step.fOverC = atof(WorkInfo.B_Value.Protect.Charge_H_Cap);
				_step.fLimitC = atof(WorkInfo.B_Value.Protect.Charge_L_Cap);
			}
			break;
		case FMS_DCHG:
			{
				_step.stepHeader.type
					= EP_TYPE_DISCHARGE;
				_step.stepHeader.mode
					= EP_MODE_CC;

				_step.fEndV = atof(pFMSStep[i].CutOff);

				_step.fOverV = atof(WorkInfo.B_Value.Protect.DisCharge_H_Vol);
				_step.fLimitV = atof(WorkInfo.B_Value.Protect.DisCharge_L_Vol);
				_step.fOverI = atof(WorkInfo.B_Value.Protect.DisCharge_H_Cur);
				_step.fLimitI = atof(WorkInfo.B_Value.Protect.DisCharge_L_Cur);
				_step.fOverC = atof(WorkInfo.B_Value.Protect.DisCharge_H_Cap);
				_step.fLimitC = atof(WorkInfo.B_Value.Protect.DisCharge_L_Cap);
			}
			break;
		case FMS_REST:
			{
				_step.stepHeader.type
					= EP_TYPE_REST;
				_step.stepHeader.mode
					= 0;
			}
			break;
		case FMS_OCV:
			{
				_step.stepHeader.type
					= EP_TYPE_OCV;
				_step.stepHeader.mode
					= 0;
			}
			break;
		//case FMS_NONE:
		//	{
		//		//_step.stepHeader.type
		//		//	= ;
		//		//_step.stepHeader.mode
		//		//	= ;
		//	}
		//	break;
		case FMS_CC_CHARGE:
			{
				_step.stepHeader.type
					= EP_TYPE_CHARGE;
				_step.stepHeader.mode
					= EP_MODE_CC;

				_step.fEndV = atof(pFMSStep[i].CutOff);

				_step.fOverV = atof(WorkInfo.B_Value.Protect.Charge_H_Vol);
				_step.fLimitV = atof(WorkInfo.B_Value.Protect.Charge_L_Vol);
				_step.fOverI = atof(WorkInfo.B_Value.Protect.Charge_H_Cur);
				_step.fLimitI = atof(WorkInfo.B_Value.Protect.Charge_L_Cur);
				_step.fOverC = atof(WorkInfo.B_Value.Protect.Charge_H_Cap);
				_step.fLimitC = atof(WorkInfo.B_Value.Protect.Charge_L_Cap);
			}
			break;
		case FMS_OG_CHARGE:
			{
				_step.stepHeader.type
					= EP_TYPE_CHARGE;
				_step.stepHeader.mode
					= EP_MODE_CCCV;

				_step.fEndI = atof(pFMSStep[i].CutOff);

				_step.fOverV = atof(WorkInfo.B_Value.Protect.Charge_H_Vol);
				_step.fLimitV = atof(WorkInfo.B_Value.Protect.Charge_L_Vol);
				_step.fOverI = atof(WorkInfo.B_Value.Protect.Charge_H_Cur);
				_step.fLimitI = atof(WorkInfo.B_Value.Protect.Charge_L_Cur);
				_step.fOverC = atof(WorkInfo.B_Value.Protect.Charge_H_Cap);
				_step.fLimitC = atof(WorkInfo.B_Value.Protect.Charge_L_Cap);
			}
			break;
		}
		_step.stepHeader.gradeSize;
		_step.stepHeader.nProcType;

		_step.fVref = atof(pFMSStep[i].Volt);
		_step.fIref = atof(pFMSStep[i].Current);

		_step.fEndTime = atoi(pFMSStep[i].Time) * 60 * 100;
		//_step.fEndV = atof(pFMSStep->CutOff);
		//_step.fEndI = atof(pFMSStep->CutOff);
		_step.fEndC = atof(pFMSStep[i].CutOff_A);
		_step.fEndDV;
		_step.fEndDI;

		_step.bUseActucalCap;
		_step.bReserved;

		_step.fSocRate;

		_step.fOverV;
		_step.fLimitV;
		_step.fOverI;
		_step.fLimitI;
		_step.fOverC;
		_step.fLimitC;
		_step.fOverImp;
		_step.fLimitImp;

		_step.fCompTimeV;
		_step.fCompVLow;
		_step.fCompVHigh;

		_step.fCompTimeI;
		_step.fCompILow;
		_step.fCompIHigh;

		_step.fDeltaTimeV;
		_step.fDeltaV;
		_step.fDeltaTimeI;
		_step.fDeltaI;

		_step.grade;

		_step.fRecDeltaTime = 100.0f;
		_step.fRecDeltaV;
		_step.fRecDeltaI;
		_step.fRecDeltaT;

		_step.fParam1;
		_step.fParam2;

		_step.UseStepContinue;
		//_step.bReserved1;
		_step.Reserved2;

		_step.fReserved;

		STR_COMMON_STEP* pStep = new STR_COMMON_STEP;
		CopyMemory(pStep, &_step, sizeof(STR_COMMON_STEP));
		pCondition->apStepList.Add(pStep);

	}

	STR_COMMON_STEP* pstepEnd = new STR_COMMON_STEP;
	ZeroMemory(pstepEnd, sizeof(STR_COMMON_STEP));
	pstepEnd->stepHeader.type = EP_TYPE_END;//nType;
	pstepEnd->stepHeader.stepIndex = i + 1;//stepIndex; 0x00
	pstepEnd->stepHeader.mode = 0xFF; //StepMode 1 : CC-CV, 2: CC, 3: CV, 4: OCV

	pCondition->apStepList.Add(pstepEnd);

	pCondition->testHeader.totalStep = totStep;

	return TRUE;
}
*/

BOOL CTestCondition::RequeryBatteryModel(long lModelID, STR_CONDITION_HEADER *pModel)
{

/*
	CBatteryModelRecordSet recordSet;
	recordSet.m_strFilter.Format("[ModelID] = %ld", lModelID);
	
	try
	{
		recordSet.Open();
	}
	catch (CDBException* e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();
		return FALSE;
	}

	if(recordSet.IsBOF() || recordSet.IsEOF())
	{
		recordSet.Close();
		return FALSE;
	}

	pModel->lID = recordSet.m_ModelID;
	sprintf(pModel->szName, "%s", recordSet.m_ModelName);
	sprintf(pModel->szDescription, "%s", recordSet.m_Description);
//	sprintf(pModel->szModifiedTime, "%s", recordSet.m_CreatedTime.Format());
	sprintf(pModel->szModifiedTime, "%d/%d/%d %d:%d:%d", 
		recordSet.m_CreatedTime.GetMonth(), 
		recordSet.m_CreatedTime.GetDay(),
		recordSet.m_CreatedTime.GetYear(),
		recordSet.m_CreatedTime.GetHour(),
		recordSet.m_CreatedTime.GetMinute(),
		recordSet.m_CreatedTime.GetSecond()
	);

	recordSet.Close();
*/
/*	TCHAR szCurDir[MAX_PATH];
	::GetModuleFileName(AfxGetApp()->m_hInstance, szCurDir, MAX_PATH);
	CString strTemp;
	strTemp = CString(szCurDir).Mid(0, CString(szCurDir).ReverseFind('\\'));
	strTemp = strTemp + "\\DataBase\\" +FORM_SET_DATABASE_NAME;
*/
	CString strTemp;
	strTemp = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");	//Get Current Folder(CTSMon Folde)
	if(strTemp.IsEmpty())
	{
		return FALSE;
	}

	strTemp = strTemp + "\\" +FORM_SET_DATABASE_NAME;
	

	CDaoDatabase  db;
	try
	{
		db.Open(strTemp);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	

	CString strSQL;
	strSQL.Format("SELECT No, ModelName, Description, CreatedTime FROM BatteryModel WHERE ModelID = %d ORDER BY No", lModelID);
	
	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}
	
	if(rs.IsBOF() || rs.IsEOF())
	{
		rs.Close();
		db.Close();
		return FALSE;
	}
	
	ZeroMemory(pModel, sizeof(STR_CONDITION_HEADER));

	pModel->lID = lModelID;			//PK

	data = rs.GetFieldValue(0);		//No
	pModel->lNo = data.lVal;

	data = rs.GetFieldValue(1);		//Name
	sprintf(pModel->szName, "%s", data.pcVal);

	data = rs.GetFieldValue(2);		//Description
	sprintf(pModel->szDescription, "%s", data.pcVal);
	
	data = rs.GetFieldValue(3);		//DateTime
	COleDateTime  date = data;
	sprintf(pModel->szModifiedTime, "%s", date.Format()); 

	rs.Close();
	db.Close();

	return TRUE;
}

//PreTest Parameter Load
BOOL CTestCondition::RequeryCheckParam(long lTestID, long lModelID, STR_CHECK_PARAM *pParam)
{
/*	CPreTestCheckRecordSet	recordSet;
		
	recordSet.m_strFilter.Format("TestID = %ld", m_conditionHeader.lID);
	recordSet.m_strSort.Format("CheckID");
	
	try
	{
		recordSet.Open();
	}
	catch (CDBException* e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();
		return FALSE;
	}

	if(recordSet.IsBOF())
	{
		recordSet.Close();
		return TRUE;
	}

	pCondition->boardParam.compFlag = (BYTE)recordSet.m_PreTestCheck;
	pCondition->boardParam.lOCVUpperValue = ExpFloattoLong(recordSet.m_MaxV, EP_VTG_FLOAT);
	pCondition->boardParam.lOCVLowerValue =  ExpFloattoLong(recordSet.m_MinV, EP_VTG_FLOAT);
//	pCondition->boardParam.lIRef =  ExpFloattoLong(recordSet.m_CurrentRange, EP_CRT_FLOAT);
	pCondition->boardParam.lVRef =  ExpFloattoLong(recordSet.m_OCVLimit, EP_VTG_FLOAT);
	pCondition->boardParam.lIRef =  ExpFloattoLong(recordSet.m_TrickleCurrent, EP_CRT_FLOAT);
	pCondition->boardParam.lTime =  recordSet.m_TrickleTime.GetHour()*36000
			+ recordSet.m_TrickleTime.GetMinute()*600 + recordSet.m_TrickleTime.GetSecond()*10;
	pCondition->boardParam.lDeltaVoltage =  ExpFloattoLong(recordSet.m_DeltaVoltage, EP_VTG_FLOAT);
	pCondition->boardParam.lMaxFaultBattery =  recordSet.m_MaxFaultBattery;

	pCondition->boardParam.autoProcessingYN =  (BYTE)recordSet.m_PreTestCheck;
	
	recordSet.Close();
*/

/*	TCHAR szCurDir[MAX_PATH];
	::GetModuleFileName(AfxGetApp()->m_hInstance, szCurDir, MAX_PATH);
	CString strTemp;
	strTemp = CString(szCurDir).Mid(0, CString(szCurDir).ReverseFind('\\'));
	strTemp = strTemp + "\\DataBase\\" +FORM_SET_DATABASE_NAME;
*/
	CString strTemp;
	strTemp = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");	//Get Current Folder(CTSMon Folder)
	if(strTemp.IsEmpty())
	{
		return FALSE;
	}

	strTemp = strTemp + "\\" +FORM_SET_DATABASE_NAME;
	
	CDaoDatabase  db;
	try
	{
		db.Open(strTemp);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	

	CString strSQL;
	//20210223 KSJ
	strSQL.Format("SELECT PreTestCheck, MaxV, MinV, CurrentRange, OCVLimit, TrickleCurrent, TrickleTime, DeltaVoltage, MaxFaultBattery, DeltaVoltageLimit FROM Check WHERE TestID = %d AND ModelID = %d ORDER BY CheckID", lTestID, lModelID);
	
	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}
	
	if(rs.IsBOF() || rs.IsEOF())
	{
		rs.Close();
		db.Close();
		return FALSE;
	}

	data = rs.GetFieldValue(0);		//PreTestCheck
	pParam->compFlag = (BYTE)(data.lVal == 0 ? FALSE: TRUE);

	data = rs.GetFieldValue(1);		//MaxV
	pParam->fOCVUpperValue =	 data.fltVal;

	data = rs.GetFieldValue(2);		//MinV
	pParam->fOCVLowerValue =  data.fltVal;
	
	data = rs.GetFieldValue(3);		//CurrentRange

	data = rs.GetFieldValue(4);		//OCVLimit
	pParam->fVRef =  data.fltVal;

	data = rs.GetFieldValue(5);		//TrickleCurrent
	pParam->fIRef =  data.fltVal;

	data = rs.GetFieldValue(6);		//TrickleTime
	pParam->fTime = (float)data.lVal/100.0f;

	data = rs.GetFieldValue(7);		//DeltaVoltage
	pParam->fDeltaVoltage = data.fltVal;

	data = rs.GetFieldValue(8);		//MaxFaultBattery
	pParam->fMaxFaultBattery =  data.fltVal;

//20210223 KSJ
	data = rs.GetFieldValue(9);		//DeltaVoltageLimit
	pParam->fDeltaVoltageLimit =  data.fltVal;

	pParam->autoProcessingYN =  pParam->compFlag;


	rs.Close();
	db.Close();

	return TRUE;	
}

//Step Data Load
int CTestCondition::RequeryStepCondition(long lTestID, long lModelID, STR_CONDITION *pCondition)
{
/*	TCHAR szCurDir[MAX_PATH];
	::GetModuleFileName(AfxGetApp()->m_hInstance, szCurDir, MAX_PATH);
	CString strTemp;
	strTemp = CString(szCurDir).Mid(0, CString(szCurDir).ReverseFind('\\'));
	strTemp = strTemp + "\\DataBase\\" +FORM_SET_DATABASE_NAME;
*/
	CString strTemp;
	strTemp = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");	//Get Current Folder(CTSMon Folde)
	if(strTemp.IsEmpty())
	{
		return 0;
	}

	strTemp = strTemp + "\\" +FORM_SET_DATABASE_NAME;

	CDaoDatabase  db;
	try
	{
		db.Open(strTemp);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return 0;
	}	
	
	CString strSQL, strQuery;
	CString strFrom;
	strSQL = "SELECT ";
	strSQL += "StepID, StepNo, StepProcType, StepType, StepMode, Vref, Iref, EndTime, EndV, EndI, EndCapacity, End_dV, End_dI, CycleCount ";	//14
	strSQL += ", OverV, LimitV, OverI, LimitI, OverCapacity, LimitCapacity, OverImpedance, LimitImpedance, DeltaTime, DeltaTime1, DeltaV, DeltaI ";	//12
	strSQL += ", Grade, CompTimeV1, CompTimeV2, CompTimeV3,	CompVLow1,	CompVLow2, CompVLow3, CompVHigh1, CompVHigh2, CompVHigh3 ";	//10
	strSQL += ", CompTimeI1, CompTimeI2, CompTimeI3, CompILow1, CompILow2, CompILow3, CompIHigh1, CompIHigh2, CompIHigh3, RecordTime, RecordDeltaV, RecordDeltaI,RecordVIGetTime ";	//12
	strSQL += ", CapVLow, CapVHigh ";	//2
//	strSQL += ", EndCheckVLow, EndCheckVHigh, EndCheckILow, EndCheckIHigh ";		//4
	strSQL += ", Value0, Value1, Value2, Value8, DCIR_RegTemp, DCIR_ResistanceRate";//, Value2, Value3, Value4, Value5, Value6, Value7, Value8, Value9 ";	//10
	strSQL += ", CompChgCcVtg, CompChgCcTime, CompChgCcDeltaVtg, CompChgCvCrt, CompChgCvTime, CompChgCvDeltaCrt";

	strFrom.Format(" FROM Step WHERE TestID = %d AND ModelID = %d ORDER BY StepNo", lTestID, lModelID);
	
	strQuery = strSQL+strFrom;
	
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strQuery, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return 0;
	}
	
	if(rs.IsBOF() || rs.IsEOF())
	{
		rs.Close();
		db.Close();
		return 0;
	}

	CStep *pStep = NULL;
	STR_COMMON_STEP *pObject = NULL;
	pObject = new STR_COMMON_STEP;
	
	COleVariant data;
	int nTotNum =0 ;
	long lStepID;
	int nTemp1, nTemp2;
	float fTemp1, fTemp2, fTemp3;
	char szTemp[64];
	
	BYTE nType, stepIndex;
	while(!rs.IsEOF())
	{
		nTotNum++;

		data = rs.GetFieldValue("StepID");
		lStepID = data.lVal;
		data = rs.GetFieldValue("StepType");
		nType = (BYTE)data.lVal;
		data = rs.GetFieldValue("StepNo");
		stepIndex = BYTE(data.lVal-1);

		ZeroMemory(pObject, sizeof(STR_COMMON_STEP));

  		pObject->stepHeader.type = nType;
		pObject->stepHeader.stepIndex = stepIndex;
 		data = rs.GetFieldValue("StepMode");
   		pObject->stepHeader.mode = (BYTE)data.lVal;
 		data = rs.GetFieldValue("Grade");	//BOOL	m_Grade;

		//Not use 2006/2/15
		//stepHeader.gradeSize로 변경하여 Grading step 수를 관리 하도록 수정 
		//	pObject->stepHeader.bGrade = (BYTE)(data.bVal == 0 ? FALSE: TRUE);//?? 거짓이면 0가 들어 있음//?? DB값이 참이면 255가 들어 있음	
		
		data = rs.GetFieldValue("StepProcType");
		pObject->stepHeader.nProcType = data.lVal;
		
		switch(nType)
		{
		case EP_TYPE_CHARGE:
		{	
			data = rs.GetFieldValue("Vref");
			pObject->fVref = data.fltVal;
			data = rs.GetFieldValue("Iref");
			pObject->fIref = data.fltVal;
			
			data = rs.GetFieldValue("EndTime");
			pObject->fEndTime = data.ulVal/100.0f;		//dataBase에 10msec단위로 저장됨 
			data = rs.GetFieldValue("EndV");
    		pObject->fEndV = data.fltVal;
 			data = rs.GetFieldValue("EndI");
	   		pObject->fEndI = data.fltVal ;
 			data = rs.GetFieldValue("EndCapacity");
    		pObject->fEndC = data.fltVal;
 			data = rs.GetFieldValue("End_dV");
    		pObject->fEndDV = data.fltVal;
 			data = rs.GetFieldValue("End_dI");
    		pObject->fEndDI = data.fltVal ;
    		
 			data = rs.GetFieldValue("OverV");
			pObject->fOverV = data.fltVal;
 			data = rs.GetFieldValue("LimitV");
    		pObject->fLimitV = data.fltVal;
 			data = rs.GetFieldValue("OverI");
    		pObject->fOverI = data.fltVal;
 			data = rs.GetFieldValue("LimitI");
    		pObject->fLimitI = data.fltVal;
 			data = rs.GetFieldValue("OverCapacity");
    		pObject->fOverC = data.fltVal;
 			data = rs.GetFieldValue("LimitCapacity");
    		pObject->fLimitC =data.fltVal;
    		
 			data = rs.GetFieldValue("DeltaTime");
			pObject->fDeltaTimeV = data.lVal/100.0f ;
 			data = rs.GetFieldValue("DeltaV");
    		pObject->fDeltaV =data.fltVal;
 			data = rs.GetFieldValue("DeltaTime1");
			pObject->fDeltaTimeI = data.lVal/100.0f;
 			data = rs.GetFieldValue("DeltaI");
    		pObject->fDeltaI =data.fltVal;
			
 			data = rs.GetFieldValue("CompTimeV1");
			pObject->fCompTimeV[0] = data.lVal/100.0f ;
 			data = rs.GetFieldValue("CompVLow1");
			pObject->fCompVLow[0] = data.fltVal;
  			data = rs.GetFieldValue("CompVHigh1");
			pObject->fCompVHigh[0] = data.fltVal;
			data = rs.GetFieldValue("CompTimeV2");
			pObject->fCompTimeV[1] = data.lVal/100.0f ;
 			data = rs.GetFieldValue("CompVLow2");
			pObject->fCompVLow[1] = data.fltVal;
 			data = rs.GetFieldValue("CompVHigh2");
			pObject->fCompVHigh[1] = data.fltVal;
 			data = rs.GetFieldValue("CompTimeV3");
			pObject->fCompTimeV[2] = data.lVal/100.0f ;
 			data = rs.GetFieldValue("CompVLow3");
			pObject->fCompVLow[2] = data.fltVal;
 			data = rs.GetFieldValue("CompVHigh3");
			pObject->fCompVHigh[2] = data.fltVal;
	
 			data = rs.GetFieldValue("CompTimeI1");
			pObject->fCompTimeI[0] = data.lVal/100.0f ;
 			data = rs.GetFieldValue("CompILow1");
			pObject->fCompILow[0] = data.fltVal;
  			data = rs.GetFieldValue("CompIHigh1");
			pObject->fCompIHigh[0] = data.fltVal;
			data = rs.GetFieldValue("CompTimeI2");
			pObject->fCompTimeI[1] = data.lVal/100.0f ;
 			data = rs.GetFieldValue("CompILow2");
			pObject->fCompILow[1] = data.fltVal;
 			data = rs.GetFieldValue("CompIHigh2");
			pObject->fCompIHigh[1] = data.fltVal;
 			data = rs.GetFieldValue("CompTimeI3");
			pObject->fCompTimeI[2] = data.lVal/100.0f ;
 			data = rs.GetFieldValue("CompILow3");
			pObject->fCompILow[2] = data.fltVal;
 			data = rs.GetFieldValue("CompIHigh3");
			pObject->fCompIHigh[2] = data.fltVal;
 			data = rs.GetFieldValue("RecordTime");
			pObject->fRecDeltaTime = data.lVal/100.0f;
 			data = rs.GetFieldValue("RecordDeltaV");
			pObject->fRecDeltaV = data.fltVal;
 			data = rs.GetFieldValue("RecordDeltaI");
 			pObject->fRecDeltaI = data.fltVal; 			
			data = rs.GetFieldValue("RecordVIGetTime");
			pObject->fRecVIGetTime = data.lVal/100.0f;
			
 			data = rs.GetFieldValue("Value1");
			if(VT_NULL != data.vt)
			{
				pObject->fRecDeltaT = data.fltVal;
			}	
			data = rs.GetFieldValue("Value2");
			if(VT_NULL == data.vt)
			{
				pObject->fSocRate = 0.0f;
			}
			else
			{
				sprintf(szTemp, "%s", data.pcVal);
				if(sscanf(szTemp, "%d %d %f", &nTemp1, &nTemp2, &fTemp1) == 3)
				{	
					pObject->fSocRate = fTemp1;
				}
			}	
			data = rs.GetFieldValue("Value8");
			if(VT_NULL != data.vt)
			{
				pObject->UseStepContinue = atoi(data.pcVal);
			}	
 			data = rs.GetFieldValue("CapVLow");
			pObject->fParam1 = data.fltVal;
 			data = rs.GetFieldValue("CapVHigh");
			pObject->fParam2 = data.fltVal;

			data = rs.GetFieldValue("CompChgCcVtg");
			pObject->fCompChgCcVtg = data.fltVal;
			data = rs.GetFieldValue("CompChgCcTime");
			pObject->fCompChgCcTime = data.lVal/100.0f;
			data = rs.GetFieldValue("CompChgCcDeltaVtg");
			pObject->fCompChgCcDeltaVtg = data.fltVal;

			data = rs.GetFieldValue("CompChgCvCrt");
			pObject->fCompChgCvCrt = data.fltVal;
			data = rs.GetFieldValue("CompChgCvTime");
			pObject->fCompChgCvTime = data.lVal/100.0f;
			data = rs.GetFieldValue("CompChgCvDeltaCrt");
			pObject->fCompChgCvDeltaCrt = data.fltVal;
			break;
		}
		case EP_TYPE_DISCHARGE:
		{	
			data = rs.GetFieldValue("Vref");
			pObject->fVref = data.fltVal;
			data = rs.GetFieldValue("Iref");
			pObject->fIref = data.fltVal;
			
			data = rs.GetFieldValue("EndTime");
			pObject->fEndTime = (data.ulVal)/100.0f;
			data = rs.GetFieldValue("EndV");
    		pObject->fEndV = data.fltVal;
 			data = rs.GetFieldValue("EndI");
	   		pObject->fEndI = data.fltVal;
 			data = rs.GetFieldValue("EndCapacity");
    		pObject->fEndC = data.fltVal;
 			data = rs.GetFieldValue("End_dV");
    		pObject->fEndDV = data.fltVal;
 			data = rs.GetFieldValue("End_dI");
    		pObject->fEndDI = data.fltVal;
    		
 			data = rs.GetFieldValue("OverV");
			pObject->fOverV = data.fltVal;
 			data = rs.GetFieldValue("LimitV");
    		pObject->fLimitV = data.fltVal;
 			data = rs.GetFieldValue("OverI");
    		pObject->fOverI = data.fltVal;
 			data = rs.GetFieldValue("LimitI");
    		pObject->fLimitI = data.fltVal;
 			data = rs.GetFieldValue("OverCapacity");
    		pObject->fOverC = data.fltVal;
 			data = rs.GetFieldValue("LimitCapacity");
    		pObject->fLimitC =data.fltVal;
 
 			data = rs.GetFieldValue("DeltaTime");
			pObject->fDeltaTimeV = (data.lVal)/100.0f ;
 			data = rs.GetFieldValue("DeltaV");
    		pObject->fDeltaV =data.fltVal;
 			data = rs.GetFieldValue("DeltaTime1");
			pObject->fDeltaTimeI = (data.lVal)/100.0f;
 			data = rs.GetFieldValue("DeltaI");
    		pObject->fDeltaI =data.fltVal;
			
 			data = rs.GetFieldValue("CompTimeV1");
			pObject->fCompTimeV[0] = (data.lVal)/100.0f ;
 			data = rs.GetFieldValue("CompVLow1");
			pObject->fCompVLow[0] = data.fltVal;
  			data = rs.GetFieldValue("CompVHigh1");
			pObject->fCompVHigh[0] = data.fltVal;
			data = rs.GetFieldValue("CompTimeV2");
			pObject->fCompTimeV[1] = (data.lVal)/100.0f ;
 			data = rs.GetFieldValue("CompVLow2");
			pObject->fCompVLow[1] = data.fltVal;
 			data = rs.GetFieldValue("CompVHigh2");
			pObject->fCompVHigh[1] = data.fltVal;
 			data = rs.GetFieldValue("CompTimeV3");
			pObject->fCompTimeV[2] = (data.lVal)/100.0f ;
 			data = rs.GetFieldValue("CompVLow3");
			pObject->fCompVLow[2] = data.fltVal;
 			data = rs.GetFieldValue("CompVHigh3");
			pObject->fCompVHigh[2] = data.fltVal;
	
 			data = rs.GetFieldValue("CompTimeI1");
			pObject->fCompTimeI[0] = (data.lVal)/100.0f ;
 			data = rs.GetFieldValue("CompILow1");
			pObject->fCompILow[0] = data.fltVal;
  			data = rs.GetFieldValue("CompIHigh1");
			pObject->fCompIHigh[0] = data.fltVal;
			data = rs.GetFieldValue("CompTimeI2");
			pObject->fCompTimeI[1] = (data.lVal)/100.0f ;
 			data = rs.GetFieldValue("CompILow2");
			pObject->fCompILow[1] = data.fltVal;
 			data = rs.GetFieldValue("CompIHigh2");
			pObject->fCompIHigh[1] = data.fltVal;
 			data = rs.GetFieldValue("CompTimeV3");
			pObject->fCompTimeV[2] = data.lVal/100.0f ;
 			data = rs.GetFieldValue("CompVLow3");
			pObject->fCompVLow[2] = data.fltVal;
 			data = rs.GetFieldValue("CompVHigh3");
			pObject->fCompVHigh[2] = data.fltVal;

			data = rs.GetFieldValue("RecordTime");
			pObject->fRecDeltaTime = data.lVal/100.0f;
 			data = rs.GetFieldValue("RecordDeltaV");
			pObject->fRecDeltaV = data.fltVal;
 			data = rs.GetFieldValue("RecordDeltaI");
			pObject->fRecDeltaI = data.fltVal;
			data = rs.GetFieldValue("RecordVIGetTime");
			pObject->fRecVIGetTime = data.lVal/100.0f;
 			data = rs.GetFieldValue("Value1");
			if(VT_NULL != data.vt)
			{
				pObject->fRecDeltaT = data.fltVal;
			}	
			data = rs.GetFieldValue("Value2");
			if(VT_NULL == data.vt)
			{
				pObject->fSocRate = 0.0f;
			}
			else
			{
				sprintf(szTemp, "%s", data.pcVal);
				if(sscanf(szTemp, "%d %d %f", &nTemp1, &nTemp2, &fTemp1) == 3)
				{	
					pObject->fSocRate = fTemp1;
				}
			}	
			data = rs.GetFieldValue("Value8");
			if(VT_NULL != data.vt)
			{
				pObject->UseStepContinue = atoi(data.pcVal);
			}	
			data = rs.GetFieldValue("CapVLow");
			pObject->fParam1 = data.fltVal;
 			data = rs.GetFieldValue("CapVHigh");		
			pObject->fParam2 = data.fltVal;
			break;
		}
		case EP_TYPE_REST:
		{			
			data = rs.GetFieldValue("EndTime");
			pObject->fEndTime = data.ulVal/100.0f;
 			data = rs.GetFieldValue("OverV");
			pObject->fOverV = data.fltVal;
 			data = rs.GetFieldValue("LimitV");
    		pObject->fLimitV = data.fltVal;

			data = rs.GetFieldValue("RecordTime");
			pObject->fRecDeltaTime = data.lVal/100.0f;
 			data = rs.GetFieldValue("RecordDeltaV");
			pObject->fRecDeltaV = data.fltVal;
 			data = rs.GetFieldValue("RecordDeltaI");
			pObject->fRecDeltaI = data.fltVal;
			data = rs.GetFieldValue("RecordVIGetTime");
			pObject->fRecVIGetTime = data.lVal/100.0f;
 			data = rs.GetFieldValue("Value1");
			break;
		}
		case EP_TYPE_OCV:
		{	
 			data = rs.GetFieldValue("OverV");
			pObject->fOverV = data.fltVal;
 			data = rs.GetFieldValue("LimitV");
    		pObject->fLimitV = data.fltVal;
			break;
		}

		case EP_TYPE_END:
		{
			break;
		}
		
		case EP_TYPE_IMPEDANCE:			//Impedance 은  Formation 장비에서 지원하지 않는다.
		{
			data = rs.GetFieldValue("Vref");
			pObject->fVref = data.fltVal;
			data = rs.GetFieldValue("Iref");
			pObject->fIref = data.fltVal;

			data = rs.GetFieldValue("EndTime");
			pObject->fEndTime = data.ulVal/100.0f;		//dataBase에 10msec단위로 저장됨 
			data = rs.GetFieldValue("EndV");
    		pObject->fEndV = data.fltVal;
 			data = rs.GetFieldValue("EndI");
	   		pObject->fEndI = data.fltVal ;
 			data = rs.GetFieldValue("EndCapacity");
    		pObject->fEndC = data.fltVal;
 			data = rs.GetFieldValue("End_dV");
    		pObject->fEndDV = data.fltVal;
 			data = rs.GetFieldValue("End_dI");
    		pObject->fEndDI = data.fltVal ;
    		
 			data = rs.GetFieldValue("OverV");
			pObject->fOverV = data.fltVal;
 			data = rs.GetFieldValue("LimitV");
    		pObject->fLimitV = data.fltVal;
 			data = rs.GetFieldValue("OverI");
    		pObject->fOverI = data.fltVal;
 			data = rs.GetFieldValue("LimitI");
    		pObject->fLimitI = data.fltVal;
 			data = rs.GetFieldValue("OverCapacity");
    		pObject->fOverC = data.fltVal;
 			data = rs.GetFieldValue("LimitCapacity");
    		pObject->fLimitC =data.fltVal;
 

			data = rs.GetFieldValue("OverImpedance");
			pObject->fOverImp = data.fltVal;
			data = rs.GetFieldValue("LimitImpedance");
			pObject->fLimitImp =data.fltVal;

			data = rs.GetFieldValue("RecordTime");
			pObject->fRecDeltaTime = data.lVal/100.0f;
 			data = rs.GetFieldValue("RecordDeltaV");
			pObject->fRecDeltaV = data.fltVal;
 			data = rs.GetFieldValue("RecordDeltaI");
			pObject->fRecDeltaI = data.fltVal;
			data = rs.GetFieldValue("RecordVIGetTime");
			pObject->fRecVIGetTime = data.lVal/100.0f;
 			data = rs.GetFieldValue("Value1");
			data = rs.GetFieldValue("DCIR_RegTemp");
			pObject->lDCIR_RegTemp = data.lVal;
			data = rs.GetFieldValue("DCIR_ResistanceRate");
			pObject->lDCIR_ResistanceRate = data.lVal;

			break;
		}	
		case EP_TYPE_LOOP:
		{	
			data = rs.GetFieldValue("strGotoValue");
			if(VT_NULL != data.vt)
			{
				pObject->nLoopInfoGoto = atol(data.pcVal);
			}
			data = rs.GetFieldValue("CycleCount");
			pObject->nLoopInfoCycle= data.lVal;
			break;
		}
		
		default:			
			nTotNum = 0;
			break;
		}

		pObject->stepHeader.gradeSize = RequeryStepGrade(db, lStepID, pObject);
		
		pStep = new CStep;
		pStep->SetStepData(pObject);
		m_apStepList.Add(pStep);

		if(pCondition)
		{
			STR_COMMON_STEP *pConStep;
			pConStep = new STR_COMMON_STEP;
			memcpy(pConStep, pObject, sizeof(STR_COMMON_STEP));
			pCondition->apStepList.Add(pConStep);
		}


		rs.MoveNext();
	}
	
	if(pObject)
	{
		delete pObject;
		pObject = NULL;
	}

	rs.Close();
	db.Close();
	
	if(pCondition)
	{
		pCondition->testHeader.totalStep = (BYTE)pCondition->apStepList.GetSize();
		pCondition->testHeader.totalGrade =	GetCountIsGradeStep();//	(BYTE)pCondition->apGradeList.GetSize();
	}

	return nTotNum;
}

//Grade Data Load
//int CTestCondition::RequeryStepGrade(CDaoDatabase &rDb, long lStepID, long lStepIndex, STR_CONDITION *pCondition)
int CTestCondition::RequeryStepGrade(CDaoDatabase &rDb, long lStepID, STR_COMMON_STEP *pStep)
{
	CString strSQL;
	strSQL.Format("SELECT Value, Value1, GradeItem, GradeCode FROM GRADE WHERE StepID = %d ORDER BY GradeID", lStepID);

	COleVariant data;
	CDaoRecordset rs(&rDb);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return 0;
	}	

//	CGrading gradeRS;

	int nTotGradeStep = 0;	
	CString strcode;
	while(!rs.IsEOF())
	{

		//value0
		data = rs.GetFieldValue(0);	
		//fdata1 = data.fltVal;
		pStep->grade[nTotGradeStep].fMin  = data.fltVal;
		//value1
		data = rs.GetFieldValue(1);	
		//fdata2 = data.fltVal;
		pStep->grade[nTotGradeStep].fMax  = data.fltVal;
		//Item
		data = rs.GetFieldValue(2);	
		//nItem = data.lVal;
		pStep->grade[nTotGradeStep].gradeItem = (SHORT)data.lVal;

		//code
		data = rs.GetFieldValue(3);	
		strcode = data.pcVal;
		if(!strcode.IsEmpty())
		{
			pStep->grade[nTotGradeStep].gradeCode = strcode[0];
			
		}
		else
		{
			pStep->grade[nTotGradeStep].gradeCode = 0;
		}
		nTotGradeStep++;
		rs.MoveNext();
	}
	pStep->stepHeader.gradeSize = (BYTE)nTotGradeStep;

/*	int nTotGradeStep = gradeRS.GetGradeStepSize();
	if(nTotGradeStep < 1)	return 0;

	int nDataSize = sizeof(EP_GRADE_HEADER)+sizeof(EP_GRADE)*nTotGradeStep;
	char *pObject = new char[nDataSize];
	ASSERT(pObject);
	ZeroMemory(pObject, nDataSize);

	EP_GRADE_HEADER gradeHeader;
	EP_GRADE	gradeData;
	ZeroMemory(&gradeHeader, sizeof(EP_GRADE_HEADER));
	ZeroMemory(&gradeData, sizeof(EP_GRADE));

	gradeHeader.stepIndex = (BYTE)lStepIndex;
	gradeHeader.totalStep = (BYTE)nTotGradeStep;
	gradeHeader.gradeItem = (WORD)nItem;
	memcpy(pObject, &gradeHeader, sizeof(EP_GRADE_HEADER));

	for(int i=0; i<nTotGradeStep; i++)
	{
		GRADE_STEP gStepData = gradeRS.GetStepData(i);
		switch(nItem)
		{
		case EP_GRADE_VOLTAGE:
			gradeData.lValue1 = (long)ExpFloattoLong(gStepData.lMin, EP_VTG_FLOAT);
			gradeData.lValue2 = (long)ExpFloattoLong(gStepData.lMax, EP_VTG_FLOAT);
			break;
		case EP_GRADE_CAPACITY:
		case EP_GRADE_IMPEDANCE:
			gradeData.lValue1 = (long)ExpFloattoLong(gStepData.lMin, EP_ETC_FLOAT);
			gradeData.lValue2 = (long)ExpFloattoLong(gStepData.lMax, EP_ETC_FLOAT);
			break;
		default:
			gradeData.lValue1 = 0L;
			gradeData.lValue2 = 0L;
			break;
		}
		
		if(gStepData.strCode.IsEmpty())
		{
			gradeData.gradeCode = 0;
		}
		else
		{
			gradeData.gradeCode = (BYTE)gStepData.strCode[0];
		}
		memcpy((char *)pObject+sizeof(EP_GRADE_HEADER)+i*sizeof(EP_GRADE), &gradeData, sizeof(EP_GRADE));
	}
	pCondition->apGradeList.Add(pObject);
*/	rs.Close();

	return nTotGradeStep;

/*	ASSERT(pRecordSet);
	pRecordSet->m_strFilter.Format("StepID = %ld", lStepID);
	pRecordSet->m_strSort.Format("GradeID");
	pRecordSet->Requery();
	
	int nTotGradeStep = 0;
	
	while(!pRecordSet->IsEOF())
	{
		nTotGradeStep++;
		pRecordSet->MoveNext();
	}
	if(nTotGradeStep < 0)	return 0;
	pRecordSet->MoveFirst(); 

	int nDataSize = sizeof(EP_GRADE_HEADER)+sizeof(EP_GRADE)*nTotGradeStep;
	char *pObject = new char[nDataSize];
	ASSERT(pObject);
	ZeroMemory(pObject, nDataSize);

	EP_GRADE_HEADER gradeHeader;
	EP_GRADE	gradeData;
	ZeroMemory(&gradeHeader, sizeof(EP_GRADE_HEADER));
	ZeroMemory(&gradeData, sizeof(EP_GRADE));
	
	gradeHeader.stepIndex = (BYTE)lStepIndex;
	gradeHeader.totalStep = (BYTE)nTotGradeStep;
	gradeHeader.gradeItem = (WORD)pRecordSet->m_GradeItem;
	memcpy(pObject, &gradeHeader, sizeof(EP_GRADE_HEADER));

	nTotGradeStep = 0;
	while(!pRecordSet->IsEOF())
	{
		switch(pRecordSet->m_GradeItem)
		{
		case EP_GRADE_VOLTAGE:
			gradeData.lValue = (long)ExpFloattoLong(pRecordSet->m_Value, EP_VTG_FLOAT);
			break;
		case EP_GRADE_CAPACITY:
		case EP_GRADE_IMPEDANCE:
			gradeData.lValue = (long)ExpFloattoLong(pRecordSet->m_Value, EP_ETC_FLOAT);
			break;
		default:
			gradeData.lValue = 0L;
			break;
		}
		
		if(pRecordSet->m_GradeCode.IsEmpty())
		{
			gradeData.gradeCode = 0;
		}
		else
		{
			gradeData.gradeCode = (BYTE)pRecordSet->m_GradeCode[0];
		}

		memcpy((char *)pObject+sizeof(EP_GRADE_HEADER)+nTotGradeStep*sizeof(EP_GRADE), &gradeData, sizeof(EP_GRADE));
		nTotGradeStep++;
		try		
		{
			pRecordSet->MoveNext();
		}
		catch (CDBException* e)
		{
			AfxMessageBox(e->m_strError);
			e->Delete();
			return nTotGradeStep;
		}
	}
	pCondition->apGradeList.Add(pObject);
*/
}
/*
//Test 의 처음 Step을 검색
BOOL CTestCondition::LoadFirstStepA(long lTestID, CStep *pStep)
{
	ASSERT(pStep);
	return LoadStepA(lTestID, 1, pStep);
}

//Test 의 다음 step을 검색			
BOOL CTestCondition::LoadNextStepA(long lTestID, int lStepNo, CStep *pStep)
{
	ASSERT(pStep);	
	return LoadStepA(lTestID, lStepNo+1, pStep);
}

//Test의 지정 Step을 검색 
BOOL CTestCondition::LoadStepA(long lTestID, int lStepNo, CStep *pStep)
{
	CStepRecordSet	StepRecordSet;
	CGradeRecordSet GradeRecordSet;

	int nTotNum =0 ;

	StepRecordSet.m_strFilter.Format("[TestID] = %ld AND [StepNo] = %ld", lTestID, lStepNo);
//	StepRecordSet.m_strSort.Format("[StepNo]");
	
	try		//Open Step RecordSet
	{
		StepRecordSet.Open();
	}
	catch (CDBException* e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();
		return FALSE;
	}
	
	if(StepRecordSet.IsBOF())				//다음 조건이 없음 
	{
		StepRecordSet.Close();
		return FALSE;
	}

	try		//Open Grade RecordSet
	{
		GradeRecordSet.Open();
	}
	catch (CDBException* e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();
		StepRecordSet.Close();
		return -1;
	}

	pStep->ClearStepData();

	pStep->m_bSkepStep = FALSE;
	pStep->m_StepID = lStepID;		//Step No
	pStep->m_StepIndex = BYTE(StepRecordSet.m_StepNo-1);
   	pStep->m_type = (BYTE)StepRecordSet.m_StepType;
   	pStep->m_mode = (BYTE)StepRecordSet.m_StepMode;
   	pStep->m_bGrade = (BYTE)StepRecordSet.m_Grade;
   	pStep->m_lProcType = StepRecordSet.m_StepProcType;

	switch(StepRecordSet.m_StepType)
	{
	case EP_TYPE_CHARGE:
		pStep->m_lVref = (long)ExpFloattoLong(StepRecordSet.m_Vref, EP_VTG_FLOAT);
		pStep->m_lIref = (long)ExpFloattoLong(StepRecordSet.m_Iref, EP_CRT_FLOAT);
		
		pStep->m_ulEndTime = (unsigned long) PC2MDTIME(StepRecordSet.m_EndTime);
    	pStep->m_lEndV = (long)ExpFloattoLong(StepRecordSet.m_EndV, EP_VTG_FLOAT);
    	pStep->m_lEndI = (long)ExpFloattoLong(StepRecordSet.m_EndI, EP_CRT_FLOAT);
    	pStep->m_lEndC = (long)ExpFloattoLong(StepRecordSet.m_EndCapacity, EP_ETC_FLOAT);
    	pStep->m_lEndDV = (long)ExpFloattoLong(StepRecordSet.m_End_dV, EP_VTG_FLOAT);
    	pStep->m_lEndDI = (long)ExpFloattoLong(StepRecordSet.m_End_dI, EP_CRT_FLOAT);
    	
		pStep->m_lHighLimitV = (long)ExpFloattoLong(StepRecordSet.m_OverV, EP_VTG_FLOAT);
    	pStep->m_lLowLimitV = (long)ExpFloattoLong(StepRecordSet.m_LimitV, EP_VTG_FLOAT);
    	pStep->m_lHighLimitI = (long)ExpFloattoLong(StepRecordSet.m_OverI, EP_CRT_FLOAT);
    	pStep->m_lLowLimitI = (long)ExpFloattoLong(StepRecordSet.m_LimitI, EP_CRT_FLOAT);
    	pStep->m_lHighLimitC = (long)ExpFloattoLong(StepRecordSet.m_OverCapacity, EP_ETC_FLOAT);
    	pStep->m_lLowLimitC =(long)ExpFloattoLong(StepRecordSet.m_LimitCapacity, EP_ETC_FLOAT);
    	
		pStep->m_lDeltaTimeV = PC2MDTIME(StepRecordSet.m_DeltaTime) ;
    	pStep->m_lDeltaV =(long)ExpFloattoLong(StepRecordSet.m_DeltaV, EP_VTG_FLOAT);
		pStep->m_lDeltaTimeI = PC2MDTIME(StepRecordSet.m_DeltaTime1);
    	pStep->m_lDeltaI =(long)ExpFloattoLong(StepRecordSet.m_DeltaI, EP_CRT_FLOAT);
		
		pStep->m_lCompTime[0] = PC2MDTIME(StepRecordSet.m_CompTimeV1) ;
		pStep->m_lCompV[0] = (long)ExpFloattoLong(StepRecordSet.m_CompVLow1, EP_VTG_FLOAT);
		pStep->m_lCompTime[1] = PC2MDTIME(StepRecordSet.m_CompTimeV2) ;
		pStep->m_lCompV[1] = (long)ExpFloattoLong(StepRecordSet.m_CompVLow2, EP_VTG_FLOAT);
		pStep->m_lCompTime[2] = PC2MDTIME(StepRecordSet.m_CompTimeV3) ;
		pStep->m_lCompV[2] = (long)ExpFloattoLong(StepRecordSet.m_CompVLow3, EP_VTG_FLOAT);
		break;
		
	case EP_TYPE_DISCHARGE:
		pStep->m_lVref = (long)ExpFloattoLong(StepRecordSet.m_Vref, EP_VTG_FLOAT);
		pStep->m_lIref = (long)ExpFloattoLong(StepRecordSet.m_Iref, EP_CRT_FLOAT);
		
		pStep->m_ulEndTime = (unsigned long) PC2MDTIME(StepRecordSet.m_EndTime);
    	pStep->m_lEndV = (long)ExpFloattoLong(StepRecordSet.m_EndV, EP_VTG_FLOAT);
    	pStep->m_lEndI = (long)ExpFloattoLong(StepRecordSet.m_EndI, EP_CRT_FLOAT);
    	pStep->m_lEndC = (long)ExpFloattoLong(StepRecordSet.m_EndCapacity, EP_ETC_FLOAT);
    	pStep->m_lEndDV = (long)ExpFloattoLong(StepRecordSet.m_End_dV, EP_VTG_FLOAT);
    	pStep->m_lEndDI = (long)ExpFloattoLong(StepRecordSet.m_End_dI, EP_CRT_FLOAT);
    	
		pStep->m_lHighLimitV = (long)ExpFloattoLong(StepRecordSet.m_OverV, EP_VTG_FLOAT);
    	pStep->m_lLowLimitV = (long)ExpFloattoLong(StepRecordSet.m_LimitV, EP_VTG_FLOAT);
    	pStep->m_lHighLimitI = (long)ExpFloattoLong(StepRecordSet.m_OverI, EP_CRT_FLOAT);
    	pStep->m_lLowLimitI = (long)ExpFloattoLong(StepRecordSet.m_LimitI, EP_CRT_FLOAT);
    	pStep->m_lHighLimitC = (long)ExpFloattoLong(StepRecordSet.m_OverCapacity, EP_ETC_FLOAT);
    	pStep->m_lLowLimitC =(long)ExpFloattoLong(StepRecordSet.m_LimitCapacity, EP_ETC_FLOAT);
 
		pStep->m_lDeltaTimeV = PC2MDTIME(StepRecordSet.m_DeltaTime) ;
    	pStep->m_lDeltaV =(long)ExpFloattoLong(StepRecordSet.m_DeltaV, EP_VTG_FLOAT);
		pStep->m_lDeltaTimeI = PC2MDTIME(StepRecordSet.m_DeltaTime1);
    	pStep->m_lDeltaI =(long)ExpFloattoLong(StepRecordSet.m_DeltaI, EP_CRT_FLOAT);
		
		pStep->m_lCompTime[0] = PC2MDTIME(StepRecordSet.m_CompTimeV1) ;
		pStep->m_lCompV[0] = (long)ExpFloattoLong(StepRecordSet.m_CompVLow1, EP_VTG_FLOAT);
		pStep->m_lCompTime[1] = PC2MDTIME(StepRecordSet.m_CompTimeV2) ;
		pStep->m_lCompV[1] = (long)ExpFloattoLong(StepRecordSet.m_CompVLow2, EP_VTG_FLOAT);
		pStep->m_lCompTime[2] = PC2MDTIME(StepRecordSet.m_CompTimeV3) ;
		pStep->m_lCompV[2] = (long)ExpFloattoLong(StepRecordSet.m_CompVLow3, EP_VTG_FLOAT);
		
		break;
	
	case EP_TYPE_REST:
		pStep->m_ulEndTime = (unsigned long)PC2MDTIME(StepRecordSet.m_EndTime);
		break;
	
	case EP_TYPE_OCV:
   		pStep->m_lHighLimitV = (long)ExpFloattoLong(StepRecordSet.m_OverV, EP_VTG_FLOAT);
   		pStep->m_lLowLimitV = (long)ExpFloattoLong(StepRecordSet.m_LimitV, EP_VTG_FLOAT);
		break;
	
	case EP_TYPE_END:
		break;
	
		//임피던스 사용 여부에 따라 결정 
	case EP_TYPE_IMPEDANCE:			//Impedance 은  Formation 장비에서 지원하지 않는다.
	{
    	pStep->m_lHighLimitImp = (long)ExpFloattoLong(StepRecordSet.m_OverImpedance, EP_ETC_FLOAT);
		pStep->m_lLowLimitImp =(long)ExpFloattoLong(StepRecordSet.m_LimitImpedance, EP_ETC_FLOAT);
		break;
	}	
		
	default:	
		nTotNum = 0;
		goto _EXIT;
		break;
	}
	
	if(StepRecordSet.m_Grade == TRUE)
	{
		LoadGradeStep(&GradeRecordSet, StepRecordSet.m_StepID, pStep);
	}
	
_EXIT:
	if(StepRecordSet.IsOpen())	StepRecordSet.Close();
	if(GradeRecordSet.IsOpen())	GradeRecordSet.Close();

	return TRUE;	
}

int CTestCondition::LoadGradeStep(CGradeRecordSet *pRecordSet, long lStepID, CStep *step)
{
	ASSERT(pRecordSet && step);
	pRecordSet->m_strFilter.Format("StepID = %ld", lStepID);
	pRecordSet->m_strSort.Format("GradeID");
	pRecordSet->Requery();
	
	int nTotGradeStep = 0;
	
	step->m_Grading.ClearStep();
	while(!pRecordSet->IsEOF())
	{
		switch(pRecordSet->m_GradeItem)
		{
		case EP_GRADE_VOLTAGE:
			step->m_Grading.AddGradeStep(	(long)ExpFloattoLong(pRecordSet->m_Value, EP_VTG_FLOAT),
											(long)ExpFloattoLong(pRecordSet->m_Value1, EP_VTG_FLOAT), pRecordSet->m_GradeCode);
			break;
		case EP_GRADE_CAPACITY:
		case EP_GRADE_IMPEDANCE:
			step->m_Grading.AddGradeStep(	(long)ExpFloattoLong(pRecordSet->m_Value, EP_ETC_FLOAT),
											(long)ExpFloattoLong(pRecordSet->m_Value1, EP_ETC_FLOAT), pRecordSet->m_GradeCode);
			break;
		default:
			break;
		}
		nTotGradeStep++;
		
		try		
		{
			pRecordSet->MoveNext();
		}
		catch (CDBException* e)
		{
			AfxMessageBox(e->m_strError);
			e->Delete();
			return nTotGradeStep;
		}
	}
	return nTotGradeStep;
}
*/

BOOL CTestCondition::SetProcedure(STR_CONDITION *pCondition)
{
	if(pCondition == NULL)		return FALSE;
	
	RemoveStep();

	m_boardParam = pCondition->checkParam;
	m_conditionHeader = pCondition->conditionHeader;
	
	m_modelHeader = pCondition->modelHeader;
	m_lProcType = m_conditionHeader.lType; 

	ASSERT(pCondition->apStepList.GetSize() == pCondition->testHeader.totalStep);
	STR_COMMON_STEP * lpStepHeader;

	CStep	*pStep = NULL;
	
	for(int i=0; i< pCondition->testHeader.totalStep; i++)
	{
		lpStepHeader = (STR_COMMON_STEP *)pCondition->apStepList[i];

		pStep = new CStep;
		pStep->SetStepData(lpStepHeader);
		m_apStepList.Add(pStep);

// 		for(int j = 0; j<lpStepHeader->stepHeader.gradeSize; j++)
// 		{
// 			pStep->m_Grading.AddGradeStep(	lpStepHeader->grade[j].fMin, 
// 											lpStepHeader->grade[j].fMax, 
// 											(char )lpStepHeader->grade[j].gradeCode
// 											);
// 
// 		}
	}
	return TRUE;
}

void CTestCondition::RemoveStep()
{
	m_lProcType = 0;
//	m_pkProcedure =0;
//	m_pkStep = 0;
	
	memset(&m_modelHeader, 0, sizeof(STR_CONDITION_HEADER));
	ZeroMemory(&m_boardParam, sizeof(STR_CHECK_PARAM));
	ZeroMemory(&m_conditionHeader, sizeof(STR_CONDITION_HEADER));
//	ZeroMemory(&m_testHeader, sizeof(EP_TEST_HEADER));
	
	int nSize = m_apStepList.GetSize();
	CStep *pObject; 

	if(nSize > 0)
	{
		for (int i = nSize-1 ; i>=0 ; i--)
		{
			if( (pObject = (CStep *)m_apStepList[i]) != NULL )
			{
				delete[] pObject; // Delete the original element at 0.
				pObject = NULL;
				m_apStepList.RemoveAt(i);  
			}
		}
		m_apStepList.RemoveAll();
	}
}

CStep * CTestCondition::GetStep(int nStepIndex)
{
	CStep *pStep = NULL;
	if(nStepIndex < 0 || nStepIndex >= m_apStepList.GetSize())
	{
		return pStep;
	}

	pStep = (CStep *)m_apStepList[nStepIndex];
	return pStep;
}

int CTestCondition::GetTotalStepNo()
{
	return m_apStepList.GetSize();
}

int CTestCondition::ReadEPFileHeader(int nFile, FILE *fp)
{
	int nRtn = 0;
	LPEP_FILE_HEADER	lpEpFileHeader, lpTempHeader;
	lpEpFileHeader = new EP_FILE_HEADER;
	lpTempHeader = new EP_FILE_HEADER;

	ASSERT(lpEpFileHeader);
	ASSERT(lpTempHeader);
	ASSERT(fp);
	
	fread(lpEpFileHeader, sizeof(EP_FILE_HEADER), 1, fp);

	switch(nFile)
	{
	case RESULT_FILE:
		sprintf(lpTempHeader->szFileID, RESULT_FILE_ID);
		sprintf(lpTempHeader->szFileVersion, RESULT_FILE_VER1);
		break;
	case TEST_LOG_FILE:
		sprintf(lpTempHeader->szFileID, TEST_LOG_FILE_ID);
		sprintf(lpTempHeader->szFileVersion, TEST_LOG_FILE_VER);
		break;
	case CONDITION_FILE:
		sprintf(lpTempHeader->szFileID, CONDITION_FILE_ID);
		sprintf(lpTempHeader->szFileVersion, CONDITION_FILE_VER);
		break;
	case GRADE_CODE_FILE:
		sprintf(lpTempHeader->szFileID, GRADE_CODE_FILE_ID);
		sprintf(lpTempHeader->szFileVersion, GRADE_CODE_FILE_VER);
		break;
	case IROCV_FILE:
		sprintf(lpEpFileHeader->szFileID, IROCV_FILE_ID);
		sprintf(lpEpFileHeader->szFileVersion, IROCV_FILE_VER1);
		break;

	default:
		nRtn = -1;
	}

	if(strcmp(lpEpFileHeader->szFileID, lpTempHeader->szFileID) != 0)
	{
		nRtn = -2;
	}

	delete lpTempHeader;
	lpTempHeader = NULL;
	delete lpEpFileHeader;
	lpEpFileHeader = NULL;
	return nRtn;
}

//Read condition from file
//Test Log파일에서만 읽을 수 있다.
BOOL CTestCondition::LoadTestCondition(CString strFileName, STR_CONDITION *pCondition)
{
	if(strFileName.IsEmpty())	return FALSE;

	FILE *fp = fopen(strFileName, "rb");
	if(fp == NULL)
	{
		return FALSE;
	}

	//File ID Check	
	if(ReadEPFileHeader(TEST_LOG_FILE, fp) < 0)
	{
		fclose(fp);
		fp = NULL;
		return FALSE;
	}
	
	RESULT_FILE_HEADER	resultFileHeader;
	if(fread(&resultFileHeader, sizeof(resultFileHeader), 1, fp)<1)		//File Header Read
	{
		fclose(fp);
		fp = NULL;
		return FALSE;		
	}

	fseek(fp, sizeof(EP_MD_SYSTEM_DATA)+sizeof(STR_FILE_EXT_SPACE)/*+sizeof(SENSOR_MAPPING_TABLE)*/, SEEK_CUR);

	BOOL bLocalCreate = FALSE;
	if(pCondition == NULL)
	{
		pCondition = new STR_CONDITION;
		bLocalCreate = TRUE;
	}

	BOOL nRtn = TRUE;
	if(ReadTestConditionFile(fp, pCondition))					//Read Test Condition
	{
		nRtn = SetProcedure(pCondition);
	}
	
	fclose(fp);
	fp = NULL;

	if(bLocalCreate && pCondition)
	{
		int nSize = pCondition->apStepList.GetSize();
		char *pObject; 

		if(nSize > 0)
		{
			for (int i = nSize-1 ; i>=0 ; i--)
			{
				if( (pObject = (char *)pCondition->apStepList[i]) != NULL )
				{
					delete[] pObject; // Delete the original element at 0.
					pObject = NULL;
					pCondition->apStepList.RemoveAt(i);  
				}
			}
			pCondition->apStepList.RemoveAll();
		}

		delete pCondition;
		pCondition = NULL;
	}

	return nRtn;
}

BOOL CTestCondition::ReadTestConditionFile(FILE *fp, STR_CONDITION *pCondition, char *fileID, char *fileVer)
{
	if(fp == NULL || pCondition == NULL)	return FALSE;

	if(fread(&pCondition->modelHeader, sizeof(STR_CONDITION_HEADER), 1, fp) != 1)			return FALSE;
	if(fread(&pCondition->conditionHeader, sizeof(STR_CONDITION_HEADER), 1, fp) != 1) return FALSE;
	if(fread(&pCondition->testHeader, sizeof(EP_TEST_HEADER), 1, fp) != 1)			return FALSE;
	if(fread(&pCondition->checkParam, sizeof(STR_CHECK_PARAM), 1, fp) != 1)	return FALSE;

	STR_COMMON_STEP *lpBuffer = NULL;
//	size_t nStepDataSize = 0;

	if(fileID != NULL && fileVer != NULL && strcmp(fileID, RESULT_FILE_ID) == 0)	//공정 결과 파일인 경우 
	{
		//Result File의 File Ver이 RESULT_FILE_VER1인 것만 STEP_V1 structure로 되어 있다.
		for(int i =0; i<pCondition->testHeader.totalStep; i++)
		{
			if(strcmp(fileVer, RESULT_FILE_VER1) == 0)
			{
				lpBuffer = new STR_COMMON_STEP;
				if(fread((char *)lpBuffer, sizeof(STR_COMMON_STEP), 1, fp) != 1)		return FALSE;
				pCondition->apStepList.Add(lpBuffer);
			}
		}	
	}
	else	//공정 결과 파일이 아닌 경우 
	{
		for(int i =0; i<pCondition->testHeader.totalStep; i++)
		{
			lpBuffer = new STR_COMMON_STEP;
			if(fread((char *)lpBuffer, sizeof(STR_COMMON_STEP), 1, fp) != 1)		return FALSE;
			pCondition->apStepList.Add(lpBuffer);

		}
	}

/*	EP_GRADE_HEADER	gradeHeader;
	for(int i =0; i<pCondition->testHeader.totalGrade; i++)
	{
		if(fread(&gradeHeader, sizeof(EP_GRADE_HEADER), 1, fp) != 1)	return FALSE;
//		fseek(fp, -sizeof(EP_GRADE_HEADER), SEEK_CUR);
		if(gradeHeader.totalStep > 0)
		{
			nStepDataSize = gradeHeader.totalStep*sizeof(EP_GRADE);
			ASSERT(nStepDataSize > 0);
			lpBuffer = new char[nStepDataSize+sizeof(EP_GRADE_HEADER)];
			memcpy(lpBuffer, &gradeHeader, sizeof(EP_GRADE_HEADER));
			if(fread((char *)lpBuffer+sizeof(EP_GRADE_HEADER), nStepDataSize, 1, fp) != 1)	return FALSE;
			pCondition->apGradeList.Add(lpBuffer);
		}
	}
*/	return TRUE;
}

void CTestCondition::ShowCondition()
{
/*	CDetailTestConditionDlg *pDlg;
	pDlg = new CDetailTestConditionDlg;
//	pDlg->m_pDoc = (CCTSMonDoc *)this;
	pDlg->m_Condition = pCondition;
	pDlg->DoModal();

	delete pDlg;
	pDlg = NULL;
*/
}

//Grading을 하는 Step의 수를 구한다.
int CTestCondition::GetCountIsGradeStep()
{
	CStep *pStep;
	int nCount = 0;
	for(int i =0; i<m_apStepList.GetSize(); i++)
	{
		pStep  = (CStep *)m_apStepList.GetAt(i);
		if(pStep->IsGradeInclude() > 0)
		{
			nCount++;
		}
	}
	return nCount;
}

void CTestCondition::ResetCondition()
{
	RemoveStep();
}

BOOL CTestCondition::ExcelSave()
{
	CString FileName;
	CFileDialog pDlg(FALSE, "", m_conditionHeader.szName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, TEXT_LANG[10]);//"csv 파일(*.csv)|*.csv|"
	if(IDOK == pDlg.DoModal())
	{
		FileName = pDlg.GetPathName();
	}
	if(FileName.IsEmpty())
	{
		return	FALSE;
	}

	FILE *fp = fopen(FileName, "wt");
	if(fp == NULL)	return	FALSE;

	int a = 0;
	CString strData;

	fprintf(fp, "[Procedure Name]\n");
	fprintf(fp, "%s,%s\n", TEXT_LANG[0], m_conditionHeader.szName);//"공정명"
	fprintf(fp, "%s,%s\n", TEXT_LANG[1], m_conditionHeader.szCreator);//"작성자"
	fprintf(fp, "%s,%s\n", TEXT_LANG[2], m_conditionHeader.szModifiedTime);//"작성일"
	fprintf(fp, "%s,%s\n\n", TEXT_LANG[3], m_conditionHeader.szDescription);//"설명"

	fprintf(fp, "[Cell Check Param]\n");
	fprintf(fp, "%s,%.1f\n", TEXT_LANG[4], m_boardParam.fOCVUpperValue);//"OCV 상한값(mV)"
	fprintf(fp, "%s,%.1f\n", TEXT_LANG[5], m_boardParam.fOCVLowerValue);//"OCV 하한값(mV)"
	fprintf(fp, "%s,%.1f\n", TEXT_LANG[6], m_boardParam.fVRef);//"설정전압(mV)"
	fprintf(fp, "%s,%.1f\n", TEXT_LANG[7], m_boardParam.fIRef);//"설정전류(mA)"
	fprintf(fp, "%s,%.1f\n", TEXT_LANG[8], m_boardParam.fTime);//"시간(sec)"
	// fprintf(fp, "%s,%.1f\n", "Delta V(mV)", m_boardParam.fDeltaVoltage);
	fprintf(fp, "%s,%.1f\n", TEXT_LANG[9], m_boardParam.fDeltaVoltage);//"접촉저항(mOhm)"
	// fprintf(fp, "%s,%f\n\n", "Max Fault Count", m_boardParam.fMaxFaultBattery);
	fprintf(fp, "%s,%.1f\n", "DeltaVoltageLimit(mV)", m_boardParam.fDeltaVoltageLimit); //
	fprintf(fp, "[Step]\n");
//	fprintf(fp, "StepNo,Type,Mode,VRef,IRef,EndV,EndI,EndTime,EndC,");
	fprintf(fp, "StepNo,Type,Mode,VRef(mV),IRef(mA),End,");
	fprintf(fp, "V Limit,I Limit,C Limit, Imp Limit,");
	for(a = 0; a<EP_COMP_POINT; a++)
	{
		fprintf(fp, "V_low%d<t%d(sec)<V_high%d,", a+1, a+1, a+1);
	}
	fprintf(fp, "\n");
	
	CString strTemp;
	CStep *pStep;

	int i = 0;
	for(i = 0; i<m_apStepList.GetSize(); i++)
	{
		pStep = (CStep *)m_apStepList[i];
		if(pStep == NULL) break;	
		
		fprintf(fp, "%d,", pStep->m_StepIndex+1);			
		fprintf(fp, "%s,", StepTypeMsg(pStep->m_type));
		fprintf(fp, "%s,", ModeTypeMsg(pStep->m_type, pStep->m_mode));

		switch(pStep->m_type)
		{
		case EP_TYPE_CHARGE:
		case EP_TYPE_DISCHARGE:
			{
				fprintf(fp, "%.1f,", VTG_PRECISION(pStep->m_fVref));
				fprintf(fp, "%.1f,", CRT_PRECISION(pStep->m_fIref));
				fprintf(fp, "%s,", pStep->EndConditonString());

				if(pStep->m_fLowLimitV != 0 && pStep->m_fHighLimitV != 0)
				{
					strTemp.Format("%.1f≤V≤%.1f", VTG_PRECISION(pStep->m_fLowLimitV), VTG_PRECISION(pStep->m_fHighLimitV));
				}
				else if(pStep->m_fLowLimitV != 0)
				{
					strTemp.Format("%.1f≤V", VTG_PRECISION(pStep->m_fLowLimitV));
				}
				else if(pStep->m_fHighLimitV != 0)
				{
					strTemp.Format("V≤%.1f", VTG_PRECISION(pStep->m_fHighLimitV));
				}
				else 
					strTemp.Empty();
				fprintf(fp, "%s,", strTemp);

				if(pStep->m_fLowLimitI != 0 && pStep->m_fHighLimitI != 0)
				{
					strTemp.Format("%.1f≤I≤%.1f", CRT_PRECISION(pStep->m_fLowLimitI), CRT_PRECISION(pStep->m_fHighLimitI));
				}
				else if(pStep->m_fLowLimitI != 0)
				{
					strTemp.Format("%.1f≤I", CRT_PRECISION(pStep->m_fLowLimitI));
				}
				else if(pStep->m_fHighLimitI != 0)
				{
					strTemp.Format("I≤%.1f", CRT_PRECISION(pStep->m_fHighLimitI));
				}
				else 
					strTemp.Empty();
				fprintf(fp, "%s,", strTemp);

				if(pStep->m_fLowLimitC != 0 && pStep->m_fHighLimitC != 0)
				{
					strTemp.Format("%.1f≤C≤%.1f", ETC_PRECISION(pStep->m_fLowLimitC), ETC_PRECISION(pStep->m_fHighLimitC));
				}
				else if(pStep->m_fLowLimitC != 0)
				{
					strTemp.Format("%.1f≤C", ETC_PRECISION(pStep->m_fLowLimitC));
				}
				else if(pStep->m_fHighLimitC != 0)
				{
					strTemp.Format("C≤%.1f", ETC_PRECISION(pStep->m_fHighLimitC));
				}
				else 
					strTemp.Empty();
				fprintf(fp, "%s,", strTemp);
				fprintf(fp, ",");
				for(a = 0; a<EP_COMP_POINT; a++)
				{
					fprintf(fp, "%.1f<%.1f<%.1f,", VTG_PRECISION(pStep->m_fCompVLow[a]), SECTIME(pStep->m_fCompTimeV[a]), VTG_PRECISION(pStep->m_fCompVHigh[a])) ;
				}
				fprintf(fp, "\n");
				break;
			}

		case EP_TYPE_REST:
			{
				fprintf(fp, ",,%s\n", pStep->EndConditonString());
				break;
			}
		case EP_TYPE_OCV:
			{
				fprintf(fp, ",,,");
				if(pStep->m_fLowLimitV != 0 && pStep->m_fHighLimitV != 0)
				{
					strTemp.Format("%.1f≤V≤%.1f", VTG_PRECISION(pStep->m_fLowLimitV), VTG_PRECISION(pStep->m_fHighLimitV));
				}
				else if(pStep->m_fLowLimitV != 0)
				{
					strTemp.Format("%.1f≤V", VTG_PRECISION(pStep->m_fLowLimitV));
				}
				else if(pStep->m_fHighLimitV != 0)
				{
					strTemp.Format("V≤%.1f", VTG_PRECISION(pStep->m_fHighLimitV));
				}
				else 
					strTemp.Empty();
				fprintf(fp, "%s,", strTemp);
				fprintf(fp, "\n");
				break;
			}
		case EP_TYPE_IMPEDANCE:
			{
				fprintf(fp, ",,%s,,,,", pStep->EndConditonString());
				if(pStep->m_fLowLimitImp != 0 && pStep->m_fHighLimitImp != 0)
				{
					strTemp.Format("%.1f≤R≤%.1f", ETC_PRECISION(pStep->m_fLowLimitImp), ETC_PRECISION(pStep->m_fHighLimitImp));
				}
				else if(pStep->m_fLowLimitImp != 0)
				{
					strTemp.Format("%.1f≤R", ETC_PRECISION(pStep->m_fLowLimitImp));
				}
				else if(pStep->m_fHighLimitImp != 0)
				{
					strTemp.Format("R≤%.1f", ETC_PRECISION(pStep->m_fHighLimitImp));
				}
				else 
					strTemp.Empty();
				fprintf(fp, "\n");
				break;
			}
		case EP_TYPE_END:
		default:
			{
				break;
			}
		}
	}
	fclose(fp);
	return TRUE;
}

CString CTestCondition::GetTestName()
{
	return m_conditionHeader.szName;
}

EP_PRETEST_PARAM CTestCondition::GetNetCheckParam()
{
	EP_PRETEST_PARAM	checkParam;
	ZeroMemory(&checkParam, sizeof(EP_PRETEST_PARAM));
	
    checkParam.lOCVUpperValue	=  ExpFloattoLong(m_boardParam.fOCVUpperValue, EP_VTG_FLOAT);		//-->OCV High Limit
    checkParam.lOCVLowerValue	=  ExpFloattoLong(m_boardParam.fOCVLowerValue, EP_VTG_FLOAT);		//-->OCV Low Limit
    checkParam.lVRef			=  ExpFloattoLong(m_boardParam.fVRef, EP_VTG_FLOAT);
    checkParam.lIRef			=  ExpFloattoLong(m_boardParam.fIRef, EP_CRT_FLOAT);
    checkParam.lTime			=  PC2MDTIME(m_boardParam.fTime);    
    // checkParam.lDeltaVoltage	=  m_boardParam.fDeltaVoltage;
	checkParam.lDeltaVoltage	= ExpFloattoLong(m_boardParam.fDeltaVoltage, EP_CRT_FLOAT);
    checkParam.lMaxFaultBattery = ExpFloattoLong(m_boardParam.fMaxFaultBattery, EP_CRT_FLOAT);
	checkParam.lDeltaVoltageLimit = ExpFloattoLong(m_boardParam.fDeltaVoltageLimit, EP_VTG_FLOAT);
	checkParam.compFlag			= m_boardParam.compFlag;
    checkParam.autoProcessingYN	= m_boardParam.autoProcessingYN	;
	checkParam.reserved			= m_boardParam.reserved;

	return checkParam;
}

float CTestCondition::GetStepTimeSum()
{
	float fTime = 0.0f;
	CStep *pStep;
	for(int s =0; s< GetTotalStepNo(); s++)
	{
		pStep = GetStep(s);
		if(pStep)
		{
			fTime += pStep->m_fEndTime;
		}
	}
	return fTime;
}


void CTestCondition::AddStep(CStep *pStep)
{
	if(pStep == NULL)	return;
	m_apStepList.Add(pStep);
}
