// ProfileListInfo.cpp: implementation of the CProfileListInfo class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DataDown.h"
#include "ProfileListInfo.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CProfileListInfo::CProfileListInfo()
{
	m_strProfileInfoFileName = _T("");
	m_strFmtFileName = _T("");
	m_strSaveDir = _T("");
	m_strIpAddress = _T("");
	m_lUnitID = 0;
	m_strTrayID = _T("");
	m_strUnitName = _T("");
	m_nCnt = 0;
}

CProfileListInfo::~CProfileListInfo()
{
}

BOOL CProfileListInfo::LoadData(CString strFile)
{
	// 1. 파일을 읽어오기전에 정보를 초기화 한다.
	m_strProfileInfoFileName = strFile;

	CString strResultFile;
	CStdioFile	file;
	if(!file.Open(strFile, CFile::modeRead|CFile::shareDenyNone))	
	{
		return FALSE;
	}
	
	CString buff;
	CString strConfig;
	CString strTitle, strData;
	BOOL	bEOF;
	int a=0, b=0;
	m_nCnt = 0;

	while( file.ReadString(buff) )
	{
		m_nCnt++;

		if(!buff.IsEmpty())
		{
			strConfig = buff;
			
			a = strConfig.Find('[');
			b = strConfig.ReverseFind(']');
			
			if(a >=0 && b > 0 && a < b)
			{
				strTitle = strConfig.Mid(a+1, b-a-1);
				
				a = strConfig.ReverseFind('=');
				
				strData = strConfig.Mid( a+1 );
				
				strData.TrimLeft(" ");
				strData.TrimRight(" ");
				
				if(strTitle ==  "UNIT_IP")
				{
					m_strIpAddress =  strData;
				}	
				else if(strTitle == "TrayID")
				{
					m_strTrayID = strData;
				}
				else if(strTitle == "UNIT_ID")
				{
					m_lUnitID = atol(strData);
				}
				else if(strTitle == "Result")
				{
					m_strFmtFileName = strData;
				}
				else if(strTitle == "SaveDir")
				{
					m_strSaveDir = strData;
				}
				else if(strTitle == "UNIT_NAME")
				{
					m_strUnitName= strData;
				}
			}
		}
		else
		{
			break;
		}

		if( m_nCnt > 10 )
		{
			// 1. 무한루프 방지?
			break;
		}
	}

	/*
	TRY
	{
		while(TRUE)
		{
			bEOF = file.ReadString(buff);
			if(!bEOF)	break;
		}		
	}
	CATCH (CFileException, e)
	{
		//AfxMessageBox(e->m_cause);
		file.Close();
		return FALSE;
	}
	END_CATCH
	*/

	file.Close();

	return TRUE;	
}
