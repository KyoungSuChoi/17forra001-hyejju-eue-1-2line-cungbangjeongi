//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ExcelTrans.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_ExcelTrans_DIALOG           102
#define IDS_TEXT_VOLT                   102
#define IDS_TEXT_CURR                   103
#define IDS_TEXT_CAP                    104
#define IDS_TEXT_EMPTY_FILENAME         105
#define IDS_TEXT_CONVERTING             106
#define IDS_TEXT_FAIL_CONVERT           107
#define IDS_TEXT_FILE_OPEN              108
#define IDS_TEXT_NOT_READ               109
#define IDS_TEXT_NOT_EXECUTE            110
#define IDS_TEXT_OVER_PARAMETER         111
#define IDS_TEXT_OVER_COLUMN            112
#define IDR_MAINFRAME                   128
#define IDD_SAVE_SET_DIALOG             129
#define IDD_SAVE_SET_DIALOG1            130
#define IDI_EXCEL_ICON                  131
#define IDC_FILE_SEL_BUTTON             1000
#define IDC_SEL_FILE_EDIT               1001
#define IDC_CHANNEL_LIST_RADIO          1003
#define IDC_STEP_LIST_RADIO             1004
#define IDC_CHANNEL_PROCESS_RADIO       1005
#define IDC_SEL_SAVE_FILE               1006
#define IDC_SAVE_FILE_EDIT              1007
#define IDC_CONVERT_RUN                 1008
#define IDC_PREVIEW_STATIC              1009
#define IDC_TRAY_FORM_RADIO             1010
#define IDC_CYCLE_RADIO                 1011
#define IDC_FILE_ADD_CHECK              1013
#define IDC_SAVE_ITEM_BUTTON            1014
#define IDC_EDIT1                       1015
#define IDC_SAVE_FOLDER_BUTTON          1016
#define IDC_SEL_FILE_COUNT_STATIC       1017
#define IDC_SAVE_UNIT_BUTTON            1018
#define IDC_MESSAGE_STATIC              1019
#define IDC_CHECK1                      1020
#define IDC_FILE_ADD_CHECK2             1020
#define IDC_EXCEL_FLAG                  1020
#define IDC_CHECK2                      1021
#define IDC_CHECK3                      1022
#define IDC_CHECK4                      1023
#define IDC_CHECK5                      1024
#define IDC_CHECK6                      1025
#define IDC_CHECK7                      1026
#define IDC_CHECK8                      1027
#define IDC_CHECK9                      1028
#define IDC_CHECK10                     1029
#define IDC_CHECK11                     1030
#define IDC_CHECK12                     1031
#define IDC_CHECK13                     1032
#define IDC_CHECK15                     1033
#define IDC_CHECK14                     1034
#define IDC_CHECK16                     1035
#define IDC_CHECK17                     1036
#define IDC_CHECK18                     1037
#define IDC_CHECK19                     1038
#define IDC_CHECK20                     1039
#define IDC_CHECK21                     1040
#define IDC_CHECK22                     1041
#define IDC_STATIC1                     1042
#define IDC_STATIC2                     1043

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1044
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
