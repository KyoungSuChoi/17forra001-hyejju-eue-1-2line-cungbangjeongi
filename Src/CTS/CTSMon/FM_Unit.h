#pragma once

#include "FMSCriticalSection.h"
#include "FMSSyncParent.h"

class CFM_Unit: public CFMSSyncParent<CFM_Unit>
{
public:
	CFM_Unit(void);
	virtual ~CFM_Unit(void);

public:
	VOID fnInit(INT, CFormModule *);
	VOID fnUnInit();

	//모듈의 현재 상태
	WORD fnGetModuleState(){return m_pModule->GetState();};
	//트레이의 현재 상태
	BOOL fnGetModuleTrayState();

	class CFM_STATE* fnProcessing();
	class CFM_STATE* fnGetState(){return m_pState;}
	class CFM_STATE* fnGetPreState(){return m_pPreState;}
	class CFM_STATE* fnGet_EQUIP_ST_OFF(){return m_pstOff;}

	class CFM_STATE* fnGet_EQUIP_ST_AUTO(){return m_pstAuto;}
	class CFM_STATE* fnGet_AUTO_ST_VACANCY();
	class CFM_STATE* fnGet_AUTO_ST_READY();
	class CFM_STATE* fnGet_AUTO_ST_TRAY_IN();
	class CFM_STATE* fnGet_AUTO_ST_CONTACT_CHECK();
	class CFM_STATE* fnGet_AUTO_ST_RUN();
	class CFM_STATE* fnGet_AUTO_ST_END();
	class CFM_STATE* fnGet_AUTO_ST_ERROR();
	VOID fnSetState(class CFM_STATE*);
	VOID fnSetState(FM_STATUS_ID);
	VOID fnSetPreState();
	//////////////////////////////////////////////////////////////////////////
	class CFM_STATE* fnGet_EQUIP_ST_LOCAL(){return m_pstLocal;}
	class CFM_STATE* fnGet_LOCAL_ST_LOCAL();
	class CFM_STATE* fnGet_LOCAL_ST_ERROR();
	//////////////////////////////////////////////////////////////////////////
	class CFM_STATE* fnGet_EQUIP_ST_MAINT(){return m_pstMaint;}
	class CFM_STATE* fnGet_MAINT_ST_MAINT();
	class CFM_STATE* fnGet_MAINT_ST_ERROR();
	//////////////////////////////////////////////////////////////////////////
private:
	//생성된 상태의 포인터
	class CFM_STATE* m_pstOff;
	class CFM_STATE* m_pstAuto;
	class CFM_STATE* m_pstLocal;
	class CFM_STATE* m_pstMaint;

	class CFM_TRAY_STATE* m_pTraystReady;
	class CFM_TRAY_STATE* m_pTraystIn;
	class CFM_TRAY_STATE* m_pTraystIned;
	class CFM_TRAY_STATE* m_pTraystOut;
	class CFM_TRAY_STATE* m_pTraystOuted;
private:
	class CFM_STATE* m_pState;
	class CFM_STATE* m_pPreState;

	class CFM_TRAY_STATE* m_pTrayState;
	class CFM_TRAY_STATE* m_pTrayPreState;
public:
	INT fnGetMachineId() {return m_nMachineId; }
	INT fnGetModuleIdx(){return m_ModuleIdx;}
	INT fnGetModuleID(){return m_ModuleID;}	
	CFormModule* fnGetModule(){return m_pModule;}
public:
	CFMS* fnGetFMS(){return &m_FMS;}

private:
	INT m_nMachineId;
	INT m_ModuleIdx;
	INT m_ModuleID;
	CFormModule *m_pModule;
private:
	INT m_NowLineMode;
	FM_STATUS_ID m_SaveStateID;
	PROC_FN_ID m_SaveFnID;

private:
	std::vector<st_RESPONS_RESERVE*> m_vResponsReserve;
public:
	VOID fnRsponse(st_RESPONS_RESERVE* _pResponse);
	VOID fnRsponseTimer();
	VOID fnRsponseProc();
	VOID fnRsponseSend(st_RESPONS_RESERVE *pRR);
	VOID fnRsponseSet(EP_RESPONSE& _response);

private:
	CFMS m_FMS;
public:
	st_FMS_PACKET* fnGetPack(){return m_FMS.fnGetPack();}
	//VOID fnSetPack(st_FMS_PACKET& _pack, INT _lineno)
	//{m_FMS.fnSetPack(_pack, _lineno);}

	BOOL fnGetModuleCheckState();

	FMS_STATE_CODE fnGetFMSState(){return m_FMS.fnGetFMSStateCode();}

private:
	EP_CODE m_EmgData;

public:
	VOID fnSendEmg(HWND, INT);
};
