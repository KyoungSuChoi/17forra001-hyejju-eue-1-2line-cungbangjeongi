#if !defined(AFX_TEXTINPUTDLG_H__667919DA_EB94_4126_80DF_F3DEC9385651__INCLUDED_)
#define AFX_TEXTINPUTDLG_H__667919DA_EB94_4126_80DF_F3DEC9385651__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TextInputDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTextInputDlg dialog

class CTextInputDlg : public CDialog
{
	// Construction
public:
	CTextInputDlg(CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
	//{{AFX_DATA(CTextInputDlg)
	enum { IDD = IDD_TEXT_INPUT_DLG };
	// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	void InitFont();
	CFont	m_font;

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTextInputDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CTextInputDlg)
	// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	int		m_nValue;
	CString m_strEdit1;
	afx_msg void OnBnClickedOk();
	//	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnBnClickedCancel();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TEXTINPUTDLG_H__667919DA_EB94_4126_80DF_F3DEC9385651__INCLUDED_)
