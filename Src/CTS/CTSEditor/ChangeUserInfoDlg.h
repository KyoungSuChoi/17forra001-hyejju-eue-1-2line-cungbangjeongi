#if !defined(AFX_CHANGEUSERINFODLG_H__8D452147_1869_4C95_AFFA_31ACE87CB907__INCLUDED_)
#define AFX_CHANGEUSERINFODLG_H__8D452147_1869_4C95_AFFA_31ACE87CB907__INCLUDED_

//#include "DataForm.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ChangeUserInfoDlg.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// CChangeUserInfoDlg dialog
//#include "XPButton.h"
class CChangeUserInfoDlg : public CDialog
{
// Construction
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	CChangeUserInfoDlg(	STR_LOGIN *pCurLoginInfo, STR_LOGIN *pEnditLoginInfo = NULL, CWnd* pParent = NULL);   // standard constructor
	virtual ~CChangeUserInfoDlg();

	STR_LOGIN * GetUserInfo();
	void DrawList();
	int m_bNewUser;
	BOOL CheckInfo();
	long	m_lPermission;

// Dialog Data
	//{{AFX_DATA(CChangeUserInfoDlg)
	enum { IDD = IDD_USER_SETTING };
//	CXPButton	m_btnWorker;
//	CXPButton	m_btnMang;
//	CXPButton	m_btnRemovePer;
//	CXPButton	m_btnAddPer;
//	CXPButton	m_btnGuest;
//	CXPButton	m_btnAdmin;
	CButton	m_btnWorker;
	CButton	m_btnMang;
	CButton	m_btnRemovePer;
	CButton	m_btnAddPer;
	CButton	m_btnGuest;
	CButton	m_btnAdmin;

	CSpinButtonCtrl	m_ctrlLogOutTimeSpin;
	CListCtrl	m_ctrlPermissionList2;
	CListCtrl	m_ctrlPermissionList1;
	CString	m_ID;
	CString	m_PWD1;
	CString	m_PWD2;
	CString	m_strName;
	CString	m_strDescription;
	BOOL	m_bAutoLogOut;
	UINT	m_nLogOutTime;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChangeUserInfoDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	STR_LOGIN *m_pCurLoginInfo;
	STR_LOGIN *m_pEnditLoginInfo;

	// Generated message map functions
	//{{AFX_MSG(CChangeUserInfoDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnChangeUserId();
	afx_msg void OnChangeUserPwd();
	afx_msg void OnChangeUserPwdConfirm();
	afx_msg void OnChangeUserName();
	afx_msg void OnChangeUserDescription();
	afx_msg void OnAddPermission();
	afx_msg void OnRemovePermission();
	afx_msg void OnAutoLogoutFlag();
	afx_msg void OnUserGuest();
	afx_msg void OnUserWorker();
	afx_msg void OnUserWorkAdmin();
	afx_msg void OnUserAdmin();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHANGEUSERINFODLG_H__8D452147_1869_4C95_AFFA_31ACE87CB907__INCLUDED_)
