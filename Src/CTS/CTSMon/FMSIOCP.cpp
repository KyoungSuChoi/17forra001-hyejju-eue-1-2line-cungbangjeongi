#include "StdAfx.h"
#include "FMSGlobal.h"
#include "FMSCriticalSection.h"
#include "FMSSyncParent.h"
#include "FMSStaticSyncParent.h"
#include "FMSMemoryPool.h"
#include "FMSCircularQueue.h"
#include "FMSNetObj.h"
#include "FMSIocp.h"
#include "FMSLog.h"

#define THREAD_STOP_TIMEOUT 1000

DWORD WINAPI WorkerThreadCallback(LPVOID lpParam)
{
	CFMSIocp *pOwner = (CFMSIocp*) lpParam;
	pOwner->WorkerThreadCallback();
	return 0;
}

CFMSIocp::CFMSIocp(VOID)
{
	m_hIocp = NULL;
	m_dwWorkerThreadCnt = 0;
}

CFMSIocp::~CFMSIocp(VOID)
{
}

BOOL CFMSIocp::RegToIocp(HANDLE hHandle, ULONG_PTR ulpCompletionKey)
{
	if (!hHandle || !ulpCompletionKey)
		return FALSE;

	m_hIocp = CreateIoCompletionPort((HANDLE) hHandle, m_hIocp, ulpCompletionKey, 0);
	if (!m_hIocp)
	{
		printf("GetLastError : %d\n", GetLastError());
		return FALSE;
	}

	return TRUE;
}

BOOL CFMSIocp::Begin(DWORD dwWorkerThreadCnt)
{
	m_hIocp = NULL;

	if (dwWorkerThreadCnt == 0)
	{
		SYSTEM_INFO si;
		GetSystemInfo(&si);

		m_dwWorkerThreadCnt = si.dwNumberOfProcessors * 2;
	}
	else
	{
		m_dwWorkerThreadCnt = dwWorkerThreadCnt;
	}

	m_hIocp = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, 0);

	if (!m_hIocp)
		return FALSE;

	for (DWORD i=0;i<m_dwWorkerThreadCnt;i++)
	{
		HANDLE hWorkerThread = CreateThread(NULL, 0, ::WorkerThreadCallback, this, 0, NULL);
		m_vWorkerThreadHandle.push_back(hWorkerThread);
	}

	return TRUE;
}

VOID CFMSIocp::End(VOID)
{
	for (DWORD i=0;i<m_vWorkerThreadHandle.size();i++)
	{
		PostQueuedCompletionStatus(m_hIocp, 0, 0, NULL);
		WaitForSingleObject(m_vWorkerThreadHandle[i], THREAD_STOP_TIMEOUT);
		TerminateThread(m_vWorkerThreadHandle[i], 0);
		CloseHandle(m_vWorkerThreadHandle[i]);
	}

	if (m_hIocp)
		CloseHandle(m_hIocp);

	m_vWorkerThreadHandle.clear();
}

VOID CFMSIocp::WorkerThreadCallback(VOID)
{	
	BOOL bSucc = FALSE;
	DWORD dwNumOfByteTransfered = 0;
	ULONG_PTR pCompletionKey = NULL;
	OVERLAPPED *pol = NULL;

	while (TRUE)
	{	
		bSucc = GetQueuedCompletionStatus(
			m_hIocp,
			&dwNumOfByteTransfered,
			&pCompletionKey,
			&pol,
			INFINITE);

		if (!pCompletionKey)
		{
			CFMSLog::WriteLog("pCompletionKey == NULL");
			return;
		}
		
		if( !pol)
		{
			CFMSLog::WriteLog("pol == NULL");
		}

		if( bSucc )
		{
			OnIo( bSucc, dwNumOfByteTransfered, pCompletionKey, pol);			
		}
	}
}