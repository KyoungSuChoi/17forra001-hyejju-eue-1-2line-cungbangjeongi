/********************************************************************
	created:	2002/02/16
	created:	16:10:2001   15:59
	filename: 	D:\MYDll\Include\DataForm.h
	file path:	D:\MYDll\Include
	file base:	DataForm
	file ext:	h
	author:		Kim Byung Hum
	
	purpose: Formation system
*********************************************************************/
#ifndef	_DATA_FORM_
#define _DATA_FORM_

#include "NewMsgdefine.h"
#include "FMSDefine.h"

#include "FMS_Charger_Define.h"
#include "FMS_DC_IR_Define.h"

#include <math.h>
//#include "FormUtility.h"

#define STATE_COLOR				15	
#define STATE_AUTO_COLOR		10
#define MAX_GRADE_COLOR			20
 
#define MAX_BD_PER_MD			EP_MAX_BD_PER_MD
#define MAX_CH_PER_BD			EP_MAX_CH_PER_BD
#define MAX_CH_PER_MD			EP_MAX_CH_PER_MD

#define TRAY_NAME_LENGTH		EP_TRAY_NAME_LENGTH
#define LOT_NAME_LENGTH			EP_LOT_NAME_LENGTH
#define IP_NAME_LENGTH			EP_IP_NAME_LENGTH
#define TEST_NAME_LENGTH		EP_TEST_NAME_LENGTH
#define TEST_DESCRIPTION_LENGTH	EP_TEST_DESCRIPTION_LENGTH
#define CREATOR_NAME_LENGTH		EP_CREATOR_NAME_LENGTH
#define DATE_TIME_LENGTH		EP_DATE_TIME_LENGTH
#define TEST_SERIAL_LENGTH		EP_TEST_SERIAL_LENGTH

#define	FILE_ID_LENGTH			EP_FILE_ID_LENGTH

#define RESULT_FILE				1
#define RESULT_FILE_ID			"051124-10000000"		//File ID Max Length is FILE_ID_LENGTH
#define RESULT_FILE_VER1		"000000000000000"
#define RESULT_FILE_VER2		"000000000000001"		
#define RESULT_FILE_VER3		"000000000000002"		//Cell Serail 기록 20090920	
//#define RESULT_FILE_VER4		"000000000000003"		 
//#define RESULT_FILE_VER5		"000000000000004"		// 
//#define RESULT_FILE_VER6		"000000000000005"		// 

#define CONDITION_FILE			2
#define CONDITION_FILE_ID		"051124-20000000"		//File ID Max Length is FILE_ID_LENGTH
#define CONDITION_FILE_VER		"000000000000000"

#define TEST_LOG_FILE			3
#define TEST_LOG_FILE_ID		"051124-40000000"
#define TEST_LOG_FILE_VER		"000000000000000"

#define GRADE_CODE_FILE			4
#define GRADE_CODE_FILE_ID		"051124-50000000"
#define GRADE_CODE_FILE_VER		"000000000000000"

#define IROCV_FILE				5
#define IROCV_FILE_ID			"051124-60000000"

#define PRODUCT_RESULT_FILE		6
#define PRODUCT_RESULT_FILE_ID	"060905-50000000"
#define PRODUCT_RESULT_FILE_VER	"000000000000000"


#define IROCV_FILE_VER1			"000000000000000"			//시험 조건에 Report 기능 추가 
#define TRAY_RECT_FAIL_CODE		"@@@trayerr@@@"					//Tray 인식 불가 Code
#define LOG_DATABASE_NAME		"Products.mdb"
#define DSN_DATABASE_NAME		"DSN=CTSProduct;"
#define FORM_SET_DATABASE_NAME	"Schedule.mdb"
/*
#define FORM_SET_DATABASE_NAME_ENG	"Schedule_eng.mdb"
#define FORM_SET_DATABASE_NAME_CHI	"Schedule_chi.mdb"
*/

#define BF_SENSOR_MAPPING_FILE_NAME "BFSensor.map"
#define BF_RESULT_FILE_EXT		"fmt"
#define BF_SUMMARY_FILE_EXT		"csv"

#define PMS_SUPERVISOR				0xFFFFFFFF		//모든 권한
#define PMS_MODULE_SHUT_DOWN		0x00000001		//모듈 종료 (Power Off)
#define PMS_MODULE_CAL_UPDATE		0x00000002		//보정 파일 Update
#define PMS_GROUP_CONTROL_CMD		0x00000004		//Control Command
#define PMS_TEST_CONDITION_SEND		0x00000008		//시험 조건 전송
#define PMS_FORM_START				0x00000010		//PowerForm 시작
#define PMS_FORM_CLOSE				0x00000020		//Powerform 종료
#define PMS_USER_SETTING_CHANGE		0x00000040		//사용자 설정 변경
#define PMS_MODULE_ADD_DELETE		0x00000080		//모듈 추가 삭제 
#define PMS_EDITOR_START			0x00000100		//시험 조건 편집기 시작
#define PMS_EDITOR_EDIT				0x00000200		//시험 조건 편집 
#define PMS_TRAY_REG				0x00000400		//Tray 등록
#define PMS_MODULE_MODE_CHANGE		0x00000800

#define PS_USER_SUPER		PMS_SUPERVISOR
#define PS_USER_ADMIN		PMS_SUPERVISOR
#define PS_USER_OPERATOR	(PMS_GROUP_CONTROL_CMD|PMS_TEST_CONDITION_SEND|PMS_FORM_START|PMS_FORM_CLOSE|PMS_EDITOR_START)
#define PS_USER_GUEST		(PMS_FORM_START|PMS_EDITOR_START|PMS_FORM_CLOSE)

#define PASSWORD_OFFSET		7
#define TOT_STATE_IMG_NO	11

#define FORM_SETTING_SAVE_FILE		"CommConfig.cfg"

//registration section name 
#define FORMSET_REG_SECTION			"FormSetting"
#define FROM_SYSTEM_SECTION			"SystemSetting"
#define FORM_SETTING_CHECK_SECTION	"FormSettingCheck"
#define	VERSION_SECTION				"Version"
#define SERIAL_REG_SECTION			"Serial1"
#define FORM_PATH_REG_SECTION		"Path"
#define FORM_DB_SERVER_SECTION		"DBServer"
#define FORM_FTP_SECTION			"Ftp Set"
#define TYPESEL_REG_SECTION			"TypeSel"
#define REAGING_REG_SECTION			"ReAgingSetting"
#define DATA_DOWN_TITLE_NAME	"PNE BF series data down utility."
#define EDITOR_APP_NAME		"CTSEditor"
#define ANAL_CLASS_NAME		"PNE_DataAnal_Formation"
#define CTSMON_CLASS_NAME	"PNE_CTSMon_Formation"
#define CTSDATA_SERVER		"CTSData Server"
#define FMS_REG				"FMS"

#define TAG_TYPE_NONE	0
#define TAG_TYPE_JIG	1
#define TAG_TYPE_TRAY	2
#define TAG_TYPE_LOT	3
// ljb 2009925
#define TAG_TYPE_CELL	4

//For ls cable 45, 60mm
#define _STOPPER_HEIGHT_0	45
#define _STOPPER_HEIGHT_1	60

CString			GetDataBaseName();
CString			GetLogDataBaseName();
BOOL			LoginCheck();
BOOL			PasswordCheck();
BOOL			LoginPremissionCheck(int nAction);
BOOL			PermissionCheck(int nAction);
CString			GetStringTable(UINT nStringResourceID);	
CString			GetModuleName(int nModuleID, int nGroupIndex=0);

class CCalAvg  
{

public:
	double GetAvg()			{	return m_dAvg;		}
	long GetDataCount()		{	return m_lCount;	}
	double GetSum()			{	return m_dSum;		}
	double GetMaxValue()	{	return 	m_dMaxVal;	}
	long GetMaxIndex()		{	return m_lMaxIndex;	}		
	double GetMinValue()	{	return m_dMinVal;	}
	long GetMinIndex()		{	return m_lMinIndex;	}
	
	virtual ~CCalAvg()
	{

	};

	CCalAvg()
	{
		ResetData();
	}

	double GetSTDD()		
	{	
		if(m_dSTDD < 0.00001)	return 0.0;		
		return m_dSTDD;		
	}

	void ResetData()
	{
		m_dSum = 0.0;
		m_dAvg = 0.0;
		m_lMaxIndex = -1;
		m_dMaxVal = 0.0;
		m_lMinIndex  = -1;
		m_dMinVal = 0.0;
		m_lCount = 0;
		m_dSTDD = 0.0;
		m_dSquareSum = 0.0;
	}
	void AddData(int nIndex, double dValue)
	{
		ASSERT(nIndex >=0); 
		m_dSum = m_dSum + dValue;
		m_dSquareSum = m_dSquareSum + (dValue*dValue);
		m_lCount++;
		m_dAvg = m_dSum/(double)m_lCount;
		m_dSTDD = sqrt(fabs(m_dSquareSum/m_lCount - m_dAvg*m_dAvg));

		if(m_lCount == 1)
		{
			m_lMaxIndex = m_lMinIndex = nIndex;
			m_dMaxVal = m_dMinVal = dValue;
		}
		else
		{
			if(m_dMaxVal < dValue)
			{
				m_lMaxIndex = nIndex;
				m_dMaxVal = dValue;
			}
			if(m_dMinVal > dValue)
			{
				m_lMinIndex = nIndex;
				m_dMinVal = dValue;
			}
		}
	}
protected:
	double m_dSum;
	double m_dAvg;
	long  m_lMaxIndex;
	double m_dMaxVal;
	long m_lMinIndex;
	double m_dMinVal;
	long m_lCount;
	double m_dSTDD;
	double m_dSquareSum;
};

// 전체 Size가 Total 512 Byte 이어야 한다.
// Member 변수가 추가 되면 szReserved 크기를 줄여야 한다.
typedef struct tagResultFile_ExtraSpace {
	char					szResultFileName[256];
	int						nCellNo;					//4Byte			
	UINT					nInputCellNo;				//4Byte	초기 새 Tray에 적재된 Cell의 수 
	UINT					nReserved;
	WORD					nSystemID;
	WORD					nSystemType;
	char					szModuleName[32];				//Module Name
	char					szReserved[208];	//
} STR_FILE_EXT_SPACE;

typedef struct tagResultFile_TrayNoList {
	char szTrayNo[32][32];
} STR_JIG_TRAY_LIST;

typedef struct tagTOP_CFG
{
	int			m_ModuleInterval;
	int			m_ChannelInterval;
	BOOL		m_bShowText;
	BOOL		m_bShowColor;
	COLORREF	m_TStateColor[STATE_COLOR];
	COLORREF	m_BStateColor[STATE_COLOR];
	BOOL		m_bStateFlash[STATE_COLOR];
	
	COLORREF	m_TAutoStateColor[STATE_AUTO_COLOR];
	COLORREF	m_BAutoStateColor[STATE_AUTO_COLOR];
	BOOL		m_bAutoStateFlash[STATE_AUTO_COLOR];

//	2002/10/29 전압 전류 벗어난 Channel 색상 
	BOOL		m_bOverColor;
	float		m_fRange[2];			//0 : Voltage //1: Current
	COLORREF	m_TOverColor[2];
	COLORREF	m_BOverColor[2];		
//	kky 2009/02/11
	BOOL		m_bLevelColor;
	COLORREF	m_VDefaultColor;
	COLORREF	m_VLevelColor[3];
	
	float		m_fMaxLevel1;
	float		m_fMaxLevel2;
	float		m_fMaxLevel3;
	float		m_fMinLevel1;
	float		m_fMinLevel2;
	float		m_fMinLevel3;	
} STR_TOP_CONFIG;

typedef struct tagTemp_CFG
{	
	COLORREF	m_UnitColorlevel[8];
	COLORREF	m_JigColorlevel[8];
	// [0] : 최소 [1] : 최대	
	float		m_fUnitlevel1[2];
	float		m_fUnitlevel2[2];
	float		m_fUnitlevel3[2];
	float		m_fUnitlevel4[2];
	float		m_fUnitlevel5[2];
	float		m_fUnitlevel6[2];
	float		m_fUnitlevel7[2];
	float		m_fUnitlevel8[2];

	float		m_fJiglevel1[2];
	float		m_fJiglevel2[2];
	float		m_fJiglevel3[2];
	float		m_fJiglevel4[2];
	float		m_fJiglevel5[2];
	float		m_fJiglevel6[2];
	float		m_fJiglevel7[2];
	float		m_fJiglevel8[2];
} STR_TEMP_CONFIG;

typedef struct tagTempScale_CFG
{	
	float		m_fUnitlevel1;
	float		m_fUnitlevel2;
	float		m_fUnitlevel3;
	float		m_fUnitlevel4;
	float		m_fUnitlevel5;
	float		m_fUnitlevel6;
	float		m_fUnitlevel7;
	float		m_fUnitlevel8;
	
	float		m_fJiglevel1;
	float		m_fJiglevel2;
	float		m_fJiglevel3;
	float		m_fJiglevel4;
	float		m_fJiglevel5;
	float		m_fJiglevel6;
	float		m_fJiglevel7;
	float		m_fJiglevel8;
} STR_TEMPSCALE_CONFIG;


typedef struct tag_GradeColor{
	BYTE	bUse;
	BYTE	bReserved;
	WORD	wReserved;
	COLORREF	dwColor;
	char	szCode[32];
} STR_GRADE_COLOR;

typedef struct tagGradeColorConfig {
	STR_GRADE_COLOR colorData[MAX_GRADE_COLOR];
} STR_GRADE_COLOR_CONFIG;

typedef struct tagLoginInformation 
{
	char	szLoginID[EP_MAX_LONINID_LENGTH+1];
	char	szPassword[EP_MAX_PASSWORD_LENGTH+1];
	long	nPermission;
	char	szRegistedDate[DATE_TIME_LENGTH+1];
	char	szUserName[CREATOR_NAME_LENGTH+1];
	char	szDescription[TEST_DESCRIPTION_LENGTH+1];
	BOOL	bUseAutoLogOut;	
	ULONG	lAutoLogOutTime;
} STR_LOGIN;

typedef struct tag_ElicoPowerFileHeader {
	char	szFileID[EP_FILE_ID_LENGTH+1];
	char	szFileVersion[EP_FILE_VER_LENGTH+1];
	char	szCreateDateTime[EP_DATE_TIME_LENGTH+1];
	char	szDescrition[EP_FILE_DESCRIPTION_LENGTH+1];
	int		nResultFileType;//emg
	int		nContactErrlimit;
	int		nCapaErrlimit;
	int		nTabDeepth;
	int		nTrayHeight;
	int		nTrayType;
	int		nInputCellCount;
	int		nChErrlimit;
	char	szReserved[96];
	// char	szReserved[128];
}	EP_FILE_HEADER, *LPEP_FILE_HEADER;

//결과 파일의 Header
typedef struct tag_ResultFileHeader {					//Test Result File Header
	int		nModuleID;
	WORD	wGroupIndex;
	WORD	wJigIndex;
	char	szDateTime[EP_DATE_TIME_LENGTH+1];			//Init. -> SendConditionStep()에서
	char	szModuleIP[EP_IP_NAME_LENGTH+1];			//SaveResultFileHeader()에서
	char	szTrayNo[EP_TRAY_NAME_LENGTH+1];			//Init. -> SendConditionStep()에서
	char	szLotNo[EP_LOT_NAME_LENGTH+1];				//Init. -> SendConditionStep()에서
	char	szTypeSel[EP_TYPE_SEL_NAME_LENGTH+1];			//20201211 ksj
	char	szOperatorID[EP_MAX_LONINID_LENGTH+1];
	char	szTestSerialNo[EP_TEST_SERIAL_LENGTH+1];
	char	szTraySerialNo[EP_TRAY_SERIAL_LENGTH+1];
} RESULT_FILE_HEADER;

//Step 파일의 Header
typedef struct tag_ResultStepHeader {					//Test Result File Header
	int		nModuleID;
	WORD	wGroupIndex;
	WORD	wJigIndex;
	char	szStartDateTime[EP_DATE_TIME_LENGTH+1];		//Init. -> SendConditionStep()에서
	char	szModuleIP[EP_IP_NAME_LENGTH+1];			//SaveResultFileHeader()에서
	char	szTrayNo[EP_TRAY_NAME_LENGTH+1];			//Init. -> SendConditionStep()에서
	char	szEndDateTime[EP_DATE_TIME_LENGTH+1];		//Init. -> SendConditionStep()에서
	char	szOperatorID[EP_MAX_LONINID_LENGTH+1];
	char	szTestSerialNo[EP_TEST_SERIAL_LENGTH+1];
	char	szTraySerialNo[EP_TRAY_SERIAL_LENGTH+1];
} RESULT_STEP_HEADER;

typedef struct tag_AuxData {
	float	fData;
	long	lReserved;
	_MINMAX_DATA minmaxData;
} _SENSOR_DATA;

typedef struct tag_SaveState {
	WORD	state;
	WORD	failCode; 
	WORD	sensorState1;	//0 : Jig Up/Down,
	WORD	sensorState2;	//1 : Tray 유무		
	WORD	sensorState3;	//2 : Stopper		
	WORD	sensorState4;	//3 : Door Sensor		
	WORD	sensorState5;			
	WORD	sensorState6;			
	WORD	sensorState7;			
	WORD	sensorState8;			
	long	lReserved[4];
} _SAVE_STATE;

//#define _STEP_END_HEADER EP_STEP_END_HEADER
typedef struct tag_GroupStateSave {
	_SAVE_STATE	gpState;
	_SENSOR_DATA sensorChData1[EP_MAX_SENSOR_CH];
	_SENSOR_DATA sensorChData2[EP_MAX_SENSOR_CH];
} GROUP_STATE_SAVE;

typedef struct tag_FileSaveChannelData
{
    WORD			wChIndex;		//Tray Channel No
	WORD			wModuleChIndex;	//Module Channel No
	WORD			wReserved;
	WORD			state;
    BYTE			grade;
    WORD			channelCode;
	
	WORD			nStepNo;
    float			fStepTime;
    float			fTotalTime;
    float			fVoltage;
    float			fCurrent;
    float			fWatt;
    float			fWattHour;
    float			fCapacity;
    float 			fImpedance;
	float			fAuxData[4];	// 1:CC(온도보정), 2:CV(온도보정)

// Add kky SKI 데이터 추가 관련
	float			fCcCapacity;
	float			fCvCapacity;
	float			fCcRunTime;    
	float			fCvRunTime;

// DCIR 데이터 부분
	float			fDCIR_V1;
	float			fDCIR_V2;
	float			fDCIR_AvgCurrent;
	float			fDCIR_AvgTemp;
		
	// float		fFiveMinCurrent;
	float			fTimeGetChargeVoltage;
	float			fDeltaOCV;
	float			fStartVoltage;   //KSJ 20210423
	float			fDeltaCapacity; //KSJ 20210223 - Reserved
	
	float			fComDCIR;// fTimeGetVoltage;
	float			fComCapacity;// fTimeGetCurrent;

// 장비 채널 상태 체크 부분
	unsigned long   currentFaultLevel3Cnt;
	unsigned long   currentFaultLevel2Cnt;
	unsigned long   currentFaultLevel1Cnt;

	unsigned long   voltageFaultLevelV3Cnt;
	unsigned long   voltageFaultLevelV2Cnt;
	unsigned long   voltageFaultLevelV1Cnt;

	unsigned long	ulTotalSamplingVoltageCnt;     
	unsigned long	ulTotalSamplingCurrentCnt;

	float	fJigTemp[6];
	float   fTempMax;	float   fTempMin;	float   fTempAvg;
} STR_SAVE_CH_DATA, *LPSTR_SAVE_CH_DATA;

typedef struct tag_MapData {
	short int nChannelNo;		//Module Channel No (1 Base)	< 0 => 사용안함,  0 == 모듈 사용 
	WORD wReseted;			//
	char szName[16];
} _MAPPING_DATA;

typedef struct tag_SernsorMap {
	int nModuleID;
	_MAPPING_DATA mapData1[EP_MAX_SENSOR_CH];
	_MAPPING_DATA mapData2[EP_MAX_SENSOR_CH];
} SENSOR_MAPPING_SAVE_TABLE;


typedef struct tag_SerialPortConfigData {
	UINT	nPortNum; 
	UINT	nPortBaudRate;
	char	portParity;
	UINT	nPortDataBits;
	UINT	nPortStopBits;
	int		nPortEvents;
	UINT	nPortBuffer;
} SERIAL_CONFIG;

typedef struct tag_ChannelCodeMessage {
	int nCode;
	int nData;
	char szMessage[EP_MAX_CH_CODE_LENGTH+1];
	char szDescript[126];
} STR_MSG_DATA;

//test condition define
typedef struct tagConditionHeader {
	LONG	lID;
	LONG	lNo;
	LONG	lType;
	char	szName[EP_TEST_NAME_LENGTH+1];
	char	szDescription[EP_TEST_DESCRIPTION_LENGTH+1];
	char	szCreator[EP_CREATOR_NAME_LENGTH+1];
	char	szProcesstype[4+1];			//20210303ksj
	char	szModifiedTime[EP_DATE_TIME_LENGTH+1];
} STR_CONDITION_HEADER;

typedef struct tagTestInformaiton {
	STR_CONDITION_HEADER	modelData;
	STR_CONDITION_HEADER	testData;
} STR_TEST_INFO;
//fms test condition
typedef struct	tag_CellCheckParameter {		//Check Parameter(NetWork St.)
    float		fOCVUpperValue;		//-->OCV High Limit
    float		fOCVLowerValue;		//-->OCV Low Limit
    float		fVRef;
    float		fIRef;				//FMS 전류
    float		fTime;				//FMS 시간
    float		fDeltaVoltage;		//FMS 접촉저항 => mOhm
    float		fMaxFaultBattery;	//FMS 역전압
	unsigned char	compFlag;
    unsigned char	autoProcessingYN;
	WORD		reserved;
	float		fDeltaVoltageLimit;			//20210303ksj
} STR_CHECK_PARAM;

typedef struct tag_CommonGrade{			//Grading Set Data (NetWork St.)
	BYTE	index;
	BYTE	gradeCode;
	SHORT	gradeItem;	
    float	fMin;
	float	fMax;
} STR_COMMON_GRADE;

typedef struct tag_Common_StepData {			//Charge/Discharge Step Data Structure
	_STEP_HEADER	stepHeader;
	float		fVref;
    float		fIref;
    
	float		fEndTime;
    float		fEndV;
    float		fEndI;
    float		fEndC;
    float		fEndDV;
    float		fEndDI;

	unsigned char bUseActucalCap;
	unsigned char bReserved;			//사용  m_lCapaRefStepIndex
	
	float		fSocRate;

	float		fOverV;
    float		fLimitV;
    float		fOverI;
    float		fLimitI;
    float		fOverC;
    float		fLimitC;
	float		fOverImp;
	float		fLimitImp;

	float		fCompTimeV[EP_COMP_POINT];			
	float		fCompVLow[EP_COMP_POINT];
	float		fCompVHigh[EP_COMP_POINT];

	float		fCompTimeI[EP_COMP_POINT];			
	float		fCompILow[EP_COMP_POINT];
	float		fCompIHigh[EP_COMP_POINT];

    float		fDeltaTimeV;
    float		fDeltaV;
    float		fDeltaTimeI;
    float		fDeltaI;

	long		nLoopInfoGoto;
	long		nLoopInfoCycle;

	STR_COMMON_GRADE	grade[EP_MAX_GRADE_STEP];

	float		fRecDeltaTime;
	float		fRecDeltaV;
	float		fRecDeltaI;
	float		fRecDeltaT;
	float		fRecVIGetTime;
	
	float		fParam1;		//parameter 1  ex) EDLC : 용량측정 전압1(Low) , etc
	float		fParam2;		//parameter 1  ex) EDLC : 용량측정 전압2(High) , etc

	unsigned char	UseStepContinue;	//ljb add 2008-12 Step 연속공정  
	unsigned char	fmsType;			//ljb add 
	unsigned short	Reserved2;			//ljb add 
	
	long		Reserved;
	long		lDCIR_RegTemp;				// 20130901 kky DCIR 기준 온도
	long		lDCIR_ResistanceRate;		// 20130901 kky DCIR 저항변화율
	unsigned char m_nFanOffFlag;
	unsigned char byReserved[3];	

	float	fCompChgCcVtg;
	float	fCompChgCcTime;
	float	fCompChgCcDeltaVtg;

	float	fCompChgCvCrt;
	float	fCompChgCvTime;
	float	fCompChgCvDeltaCrt;

	float		fReserved[1];	
} STR_COMMON_STEP;

typedef struct tagConditionData {			//Test Condition 
	char szTestSerialNo[EP_TEST_SERIAL_LENGTH+1];
	STR_CONDITION_HEADER	modelHeader;		//model info
	STR_CONDITION_HEADER	conditionHeader;	//test info
	EP_TEST_HEADER		testHeader;
	STR_CHECK_PARAM		checkParam;
	CPtrArray			apStepList;
	BOOL				bContinueCellCode;
} STR_CONDITION;

typedef struct tag_StepResult {
	RESULT_STEP_HEADER	stepHead;
	STR_COMMON_STEP		stepCondition;
	CCalAvg				averageData[EP_RESULT_ITEM_NO];
	int					nInputCount;
	int					nNormalCount;
	GROUP_STATE_SAVE	gpStateSave;
	CPtrArray			aChData;
} STR_STEP_RESULT;

//20090920 KBH
typedef struct tag_Result_Cell_Serial{
	char szCellNo[EP_MAX_CH_PER_MD][32];
}PNE_RESULT_CELL_SERIAL;

//20130804 KKY
typedef struct tag_Emglog {
	bool bUse;
	int nNo;
	char szStage[TEST_DESCRIPTION_LENGTH+1];
	int nOperationMode;
	char szCode[TEST_DESCRIPTION_LENGTH+1];
	char szMessage[TEST_DESCRIPTION_LENGTH+1];	
	char curTime[EP_DATE_TIME_LENGTH+1];
} STR_EMG_LOG;


//////////////////////////////////////////////////////////////////////////

#endif	//_DATA_FORM_

