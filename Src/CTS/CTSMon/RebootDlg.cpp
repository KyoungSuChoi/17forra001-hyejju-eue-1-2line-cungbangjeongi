// RebootDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSMon.h"

#include "RebootDlg.h"
#include "CalFileDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRebootDlg dialog


CRebootDlg::CRebootDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CRebootDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CRebootDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_strModule = "Module";
	m_pClientSock = NULL;
	m_strLoginID = "sbc";
	m_timeOutFlag = FALSE;
}


void CRebootDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRebootDlg)
	DDX_Control(pDX, IDC_REBOOT_PROGRESS, m_wndProgress);
	DDX_Control(pDX, IDC_WARING, m_ctrlWaringLabel);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CRebootDlg, CDialog)
	//{{AFX_MSG_MAP(CRebootDlg)
	ON_BN_CLICKED(IDC_REBOOT, OnReboot)
	ON_WM_DESTROY()
	ON_WM_TIMER()
	//}}AFX_MSG_MAP

	ON_MESSAGE(WM_RECEIVE_DATA, OnReceive)
	ON_MESSAGE(WM_SOCKET_CONNECTED, OnServerConnected)
	ON_MESSAGE(WM_SOCKET_DISCONNECTED, OnServerDisConnected)

END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRebootDlg message handlers

BOOL CRebootDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_ctrlWaringLabel.SetBkColor(RGB(255, 0, 0));
	m_ctrlWaringLabel.FlashBackground(TRUE);

	CString strMsg, strTemp;
/*	strMsg += " 선택한 %s를 재실행하려 합니다. Power Lamp가 깜빡이고 접속이 되지 않는다던가, 접속이 되어 있는데 ";
	strMsg += "작업 명령을 전송할 수 없거나, 비상 정지시나 오류에 의해 종료되어 재시작하여도 해제가 되지 않을 경우만 ";
	strMsg += "사용하시기 바랍니다.	재실행하게 되면 %s에서 진행하던 모든 작업은 초기 상태가 됩니다. (장비가 동작 ";
	strMsg += "중이거나, 잠시 멈춤 상태에 있을 경우 진행중인 작업은 잃게 됩니다.)  %s에서 현재 진행중인 작업을 초기화";
	strMsg += "시켜도 되는지 확인 하시고 재실행을 하시기 바랍니다.(약 3분 소요) 현재의 작업 상태를 유지 하여야 하거나 장비의 재시작이";
	strMsg += "되지 않을 경우는 (주)PNE로 연락 주시기 바랍니다.";
*/
	strTemp.Format(::GetStringTable(IDS_LANG_MSG_WARNING_REBOOT), m_strModule, m_strModule, m_strModule);

	m_wndProgress.SetRange(0, 100);

	CWinApp* pApp = AfxGetApp();
	m_strLoginID = pApp->GetProfileString(FORM_FTP_SECTION , "User ID", "sbc");
	m_strPassword = pApp->GetProfileString(FORM_FTP_SECTION , "User Password", "dusrnth");
	m_strDir = pApp->GetProfileString(FORM_FTP_SECTION , "Shared Dir", "");
	m_strFileName = pApp->GetProfileString(FORM_FTP_SECTION , "Shared File", "tmpData");

	if(m_strDir.GetLength() >= 1)
	{
		if(m_strDir.Right(1) == "/")		//가장 마지막에 '/'가 있으면 
		{
			m_strCurDir = m_strDir.Left(m_strDir.GetLength()-1);
		}
		else
			m_strCurDir = m_strDir;
		
		int nIdnex = m_strCurDir.ReverseFind('/');
		if(nIdnex > 0)
		{
			m_strCurDir = m_strCurDir.Mid(nIdnex+1);
		}
	}


	GetDlgItem(IDC_WARING_MSG_STATIC)->SetWindowText(strTemp);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CRebootDlg::SetModuleName(CString strName, CString strIPAddress)
{
	m_strModule = strName;
	m_strIPAddress = strIPAddress;
}

void CRebootDlg::OnReboot() 
{
	// TODO: Add your control notification handler code here
	
	if( LoginPremissionCheck(PMS_MODULE_SHUT_DOWN) == FALSE)
	{
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_PERMISSION_ERROR) +" [System Reboot]");
		return;
	}

	CString strTemp;

//	strTemp.Format("%s를 재시작 하시겠습니까?", m_strModule);
//	if(MessageBox(strTemp, "확인", MB_ICONQUESTION|MB_OK) == IDNO)	return;

/*	CCalFileDlg  *pDlg;
	pDlg = new CCalFileDlg();
	strIPAddress = pDlg->GetIPAddress(m_nModuleID);
	delete pDlg;
*/	
	if(m_strIPAddress.IsEmpty())
	{
		MessageBox(GetStringTable(IDS_LANG_MSG_ERROR_FOUND_IP), "IP Address Error", MB_ICONSTOP|MB_OK);
		return;
	}

	CloseSocket();

	m_pClientSock = new CClientSocket(this->m_hWnd);
	if(m_pClientSock == NULL)
	{
		MessageBox("Could not create new socket", "Socket Error", MB_ICONSTOP|MB_OK);
		return;
	}

	BOOL bOK = m_pClientSock->Create();
	if(bOK == TRUE)
	{
		m_pClientSock->AsyncSelect(FD_READ | FD_WRITE | FD_CLOSE | FD_CONNECT | FD_OOB);
		m_pClientSock->Connect(m_strIPAddress, 23);
	}
	else
	{
		ASSERT(FALSE);  //Did you remember to call AfxSocketInit()?
		delete m_pClientSock;
		m_pClientSock = NULL;
	}
}


LONG CRebootDlg::OnServerConnected(UINT,LONG)
{
	TRACE("%s Telnet Connected\n", m_strModule);

	m_nPhase = 0;
	SetTimer(100, 5000, NULL);		//Time Out Timer
	GetDlgItem(IDC_REBOOT)->EnableWindow(FALSE);
	return TRUE;
}

LONG CRebootDlg::OnServerDisConnected(UINT,LONG)
{
	TRACE("%s Telnet Disconnected\n", m_strModule);
	CloseSocket();

	GetDlgItem(IDC_REBOOT)->EnableWindow(TRUE);
	return TRUE;
}

LONG CRebootDlg::OnReceive(UINT wParam, LONG lParam)
{
	if(!IsWindow(m_hWnd)) return FALSE;
	
	int nBytes = m_pClientSock->Receive(m_bBuf ,ioBuffSize );
	if(nBytes != SOCKET_ERROR)
	{
		while(GetLine(m_bBuf, nBytes) != TRUE);
		ProcessOptions();
		MessageReceived(m_strNormalText);
	}
	m_strLine.Empty();
	m_strResp.Empty();

	return TRUE;
}

BOOL CRebootDlg::GetLine(unsigned char *bytes, int nBytes)
{
	BOOL bLine = FALSE;
	int ndx = 0;

	while ( bLine == FALSE && ndx < nBytes )
	{
		unsigned char ch = bytes[ndx];
			
//		TRACE("0x%02x [%c]\n", ch, ch);
		
		switch( ch )
		{
		case '\r': // ignore
			m_strLine += "\r\n"; //"CR";
			break;
		case '\n': // end-of-line
			m_strLine += '\n'; //"LF";
//			bLine = TRUE;
			break;
		default:   // other....
			m_strLine += ch;
			break;
		} 

		ndx ++;

		if (ndx == nBytes)
		{
			bLine = TRUE;
		}
	}
	return bLine;
}

void CRebootDlg::ProcessOptions()
{
	CString strTemp;
	CString strOption;
	unsigned char ch;
	int ndx;
	int ldx;
	BOOL bScanDone = FALSE;

	strTemp = m_strLine;

//	TRACE("RX :: %s\n", strTemp);
	
	while(!strTemp.IsEmpty() && bScanDone != TRUE)
	{
		ndx = strTemp.Find(IAC);
		if(ndx != -1)
		{
			m_strNormalText += strTemp.Left(ndx);		//IAC 이전 
			ch = strTemp.GetAt(ndx + 1);				//IAC 다음 문자
			switch(ch)
			{
			case DO:
			case DONT:
			case WILL:
			case WONT:
				strOption		= strTemp.Mid(ndx, 3);	//IAC부터 3개의 문자 
				strTemp		= strTemp.Mid(ndx + 3);		//3개 이후의 문자 (새로운 문장)
				m_strNormalText	= strTemp.Left(ndx);	//IAC 이전 문자들 
				m_ListOptions.AddTail(strOption);
				break;
			case IAC:
				m_strNormalText	= strTemp.Left(ndx);
				strTemp		= strTemp.Mid(ndx + 1);
				break;
			case SB:
				m_strNormalText = strTemp.Left(ndx);
				ldx = strTemp.Find(SE);
				strOption	= strTemp.Mid(ndx, ldx);
				m_ListOptions.AddTail(strOption);
				strTemp		= strTemp.Mid(ldx);
				
				AfxMessageBox(strOption, MB_OK);
				break;
			}
		}
		else
		{
			m_strNormalText = strTemp;
			bScanDone = TRUE;
		}
	} 
	
	RespondToOptions();
}

void CRebootDlg::RespondToOptions()
{
	CString strOption;
	
	while(!m_ListOptions.IsEmpty())
	{
		strOption = m_ListOptions.RemoveHead();

		ArrangeReply(strOption);
	}

	DispatchMessage(m_strResp);
	m_strResp.Empty();
}

void CRebootDlg::ArrangeReply(CString strOption)
{

	unsigned char Verb;
	unsigned char Option;
	unsigned char Modifier;
	unsigned char ch;
	BOOL bDefined = FALSE;

	if(strOption.GetLength() < 3) return;

	Verb = strOption.GetAt(1);
	Option = strOption.GetAt(2);

	switch(Option)
	{
	case 1:	// Echo
	case 3: // Suppress Go-Ahead
		bDefined = TRUE;
		break;
	}

	m_strResp += IAC;

	if(bDefined == TRUE)
	{
		switch(Verb)
		{
		case DO:
			ch = WILL;
			m_strResp += ch;
			m_strResp += Option;
			break;
		case DONT:
			ch = WONT;
			m_strResp += ch;
			m_strResp += Option;
			break;
		case WILL:
			ch = DO;
			m_strResp += ch;
			m_strResp += Option;
			break;
		case WONT:
			ch = DONT;
			m_strResp += ch;
			m_strResp += Option;
			break;
		case SB:
			Modifier = strOption.GetAt(3);
			if(Modifier == SEND)
			{
				ch = SB;
				m_strResp += ch;
				m_strResp += Option;
				m_strResp += IS;
				m_strResp += IAC;
				m_strResp += SE;
			}
			break;
		}
	}

	else
	{
		switch(Verb)
		{
		case DO:
			ch = WONT;
			m_strResp += ch;
			m_strResp += Option;
			break;
		case DONT:
			ch = WONT;
			m_strResp += ch;
			m_strResp += Option;
			break;
		case WILL:
			ch = DONT;
			m_strResp += ch;
			m_strResp += Option;
			break;
		case WONT:
			ch = DONT;
			m_strResp += ch;
			m_strResp += Option;
			break;
		}
	}
}

void CRebootDlg::DispatchMessage(CString strText)
{
	ASSERT(m_pClientSock);
	m_pClientSock->Send(strText, strText.GetLength());

//	TRACE("TX :: %s\n", strText);

}

void CRebootDlg::MessageReceived(LPCSTR pText)
{
//	TRACE("TX =>> %s\n", m_strSendedData);
//	TRACE("RX =>> %s\n", pText);
//	if( m_nPhase == PHASE_LOGIN_DONE)
//	{
//		BYTE data = ParsingData(pText);
		ParsingMessage(pText);
//	}
//	else
//	{

//	}
}

//Parsing Data to Line String 
void CRebootDlg::ParsingMessage(LPCSTR pText)		
{
	CString buff(pText);
	if(buff.IsEmpty())	return;
	int nIndex;

	while((nIndex = buff.Find("\r\n")) >= 0)
	{
		m_strLineData += buff.Left(nIndex);
		buff = buff.Mid(nIndex+3);

		if(!m_strLineData.IsEmpty())
		{
			ParsingData(m_strLineData);	//한라인 단위 Parsing
			m_strLineData.Empty();
		}	
	}

	if(buff.GetLength() > 0)
	{
		m_strLineData += buff;
		ParsingData(m_strLineData);
	}
	
}

void CRebootDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here
	CloseSocket();	
}

BYTE CRebootDlg::ParsingData(CString msg)
{
	m_timeOutFlag = FALSE;

	TRACE("%s\n", msg);
	FILE *fp = fopen(LOG_FILE_NAME, "at+");
	if(fp != NULL)
	{
		fprintf(fp, "%s\n", msg);
		fclose(fp);
	}
	
	CString subMsg, dir;

	switch(m_nPhase)
	{
	case 0:		//ID 입력
		subMsg = msg.Right(7);
		if(subMsg == "login: ")
		{
			SendData(m_strLoginID);
			m_nPhase++;
		}
		break;
	case 1:		//Password 입력
		subMsg = msg.Right(10);
		if(subMsg == "Password: ")
		{
			SendData(m_strPassword);
			m_nPhase++;
		}
		break;
	case 2:		//root 계정 변경 
		subMsg = msg.Right(m_strLoginID.GetLength()+3);
		if(subMsg == m_strLoginID+"]$ ")
		{
			SendData("su - root");
			m_nPhase++;
		}
		break;

	case 3:		//root 암호 입력  
		subMsg = msg.Right(10);
		if(subMsg == "Password: ")
		{
			SendData(m_strPassword);
			m_nPhase++;
		}
		break;

	case 4:		//Directory 이동   
		subMsg = msg.Right(7);
		if(subMsg == "root]# ")
		{
			SendData("cd "+m_strDir);
			m_nPhase++;
		}
		break;

	case 5:		//파일 삭제    
		subMsg = msg.Right(m_strCurDir.GetLength()+3);
		if(subMsg == m_strCurDir+"]# ")
		{
			SendData("rm -rf " + m_strFileName);
			m_nPhase++;
		}
		break;
	case 6:		//파일 생성     
		subMsg = msg.Right(dir.GetLength()+3);
		if(subMsg == dir+"]# ")
		{
			SendData("touch " + m_strFileName);
			m_nPhase++;
		}
		break;
	case 7:		//리부팅    
		subMsg = msg.Right(dir.GetLength()+3);
		if(subMsg == dir+"]# ")
		{
			SendData("reboot");
			m_nPhase++;
		}
		break;

	default:
		KillTimer(100);
		m_wndProgress.SetPos(100);
		Sleep(100);
		OnOK();
	}
	m_wndProgress.SetPos(100/7 * m_nPhase);

	return TRUE;
}

int CRebootDlg::SendData(CString strData)
{
	int nSendSize = 0;
	if(m_pClientSock == NULL)	return 0;
	
	int nSize = strData.GetLength();
	if(nSize > 0)
	{
		strData += "\n";
		nSize = strData.GetLength();
		if( nSize != (nSendSize = m_pClientSock->Send(strData, nSize)))
		{
			CloseSocket();
			TRACE("MessageSend Fail\n");
		}
//		TRACE(strData);
	}
	return nSendSize;
}

void CRebootDlg::CloseSocket()
{
	if(m_pClientSock != NULL)
	{
		m_pClientSock->Close();
		delete m_pClientSock ;
		m_pClientSock = NULL;
	}
	GetDlgItem(IDC_REBOOT)->EnableWindow();
	m_wndProgress.SetPos(0);
}

void CRebootDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	if(m_timeOutFlag == TRUE)
	{
		KillTimer(100);

		CString strMsg;
		strMsg.Format(::GetStringTable(IDS_MSG_CONNECTION_FAIL), m_strModule);
		MessageBox(strMsg, "Connection Error", MB_ICONSTOP|MB_OK);

		CloseSocket();
	}
	m_timeOutFlag = TRUE;
	CDialog::OnTimer(nIDEvent);
}


