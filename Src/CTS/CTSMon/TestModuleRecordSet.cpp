// TestModuleRecordSet.cpp : implementation file
//

#include "stdafx.h"
#include "CTSMon.h"
#include "TestModuleRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTestModuleRecordSet

IMPLEMENT_DYNAMIC(CTestModuleRecordSet, CDaoRecordset)

CTestModuleRecordSet::CTestModuleRecordSet(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CTestModuleRecordSet)
	m_ProcedureID = 0;
	m_ModuleID = 0;
	m_BoardIndex = 0;
	m_TrayNo =  _T("");
	m_UserID = _T("");
	m_GroupIndex = 0;
	m_DateTime = (DATE)0;
	m_TraySerial = _T("");
	m_TestSerialNo = _T("");
	m_State = _T("");
	m_LotNo = _T("");
	m_nFields = 11;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString CTestModuleRecordSet::GetDefaultDBName()
{
	return _T(GetLogDataBaseName());
}

CString CTestModuleRecordSet::GetDefaultSQL()
{
	return _T("[Module]");
}

void CTestModuleRecordSet::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CTestModuleRecordSet)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Long(pFX, _T("[ProcedureID]"), m_ProcedureID);
	DFX_Long(pFX, _T("[ModuleID]"), m_ModuleID);
	DFX_Long(pFX, _T("[BoardIndex]"), m_BoardIndex);
	DFX_Text(pFX, _T("[TrayNo]"), m_TrayNo);
	DFX_Text(pFX, _T("[UserID]"), m_UserID);
	DFX_Long(pFX, _T("[GroupIndex]"), m_GroupIndex);
	DFX_DateTime(pFX, _T("[DateTime]"), m_DateTime);
	DFX_Text(pFX, _T("[TraySerial]"), m_TraySerial);
	DFX_Text(pFX, _T("[TestSerialNo]"), m_TestSerialNo);
	DFX_Text(pFX, _T("[State]"), m_State);
	DFX_Text(pFX, _T("[LotNo]"), m_LotNo);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CTestModuleRecordSet diagnostics

#ifdef _DEBUG
void CTestModuleRecordSet::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CTestModuleRecordSet::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
