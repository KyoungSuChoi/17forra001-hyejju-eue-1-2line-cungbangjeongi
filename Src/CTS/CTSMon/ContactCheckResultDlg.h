#pragma once


// ContactCheckResultDlg 대화 상자입니다.
#include "CTSMonDoc.h"

#define EDIT_FONT_SIZE 18

class ContactCheckResultDlg : public CDialog
{
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	DECLARE_DYNAMIC(ContactCheckResultDlg)

public:
	ContactCheckResultDlg(CCTSMonDoc* pdoc = NULL, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~ContactCheckResultDlg();
public:
	int m_nRnt;	
	int m_nGroupIndex;
	
	void SetModuleID(int nModuleID);
	int	 GetModuleID();
	void SetResultMsg( CString strMsg );


// 대화 상자 데이터입니다.
	enum { IDD = IDD_CONTACT_CHECK_RESULT_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	CCTSMonDoc *m_pDoc;
	CFont font;
	int m_nModuleID;
	void InitFont();
	void InitLabel();	
	
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedRestartBtn();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedContinueBtn();
	afx_msg void OnBnClickedStopBtn();
	afx_msg void OnBnClickedCancel();
	CString m_strResultMsg;
	virtual BOOL OnInitDialog();	
	CLabel m_Label1;
};
