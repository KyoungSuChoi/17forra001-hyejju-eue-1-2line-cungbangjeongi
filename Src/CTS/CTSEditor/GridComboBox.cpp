// GridComboBox.cpp : implementation file
//

#include "stdafx.h"
#include "GridComboBox.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGridComboBox

CGridComboBox::CGridComboBox(CWnd *pFormView, CGXGridCore* pGrid, UINT nEditID, UINT nListBoxID, UINT nFlags)
 : CGXComboBox(pGrid, nEditID, nListBoxID, nFlags)
{
	m_pFormView = pFormView;
	m_OldSel = -1;
}

CGridComboBox::~CGridComboBox()
{
}


BEGIN_MESSAGE_MAP(CGridComboBox, CGXComboBox)
	//{{AFX_MSG_MAP(CGridComboBox)
	ON_WM_DRAWITEM()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CGridComboBox message handlers

void CGridComboBox::OnStoreDroppedList(CListBox* pLbox)
{
	CString tmp;
	GetValue(tmp);
	m_OldSel = atoi(tmp);

	CGXComboBox::OnStoreDroppedList(pLbox);
	int sel = -1;
	if(pLbox)
	{
		sel = pLbox->GetCurSel();
		if(m_pFormView)
		{
			m_pFormView->PostMessage(WM_COMBO_SELECT, (WPARAM) sel, (LPARAM) this);
		}
	}
}

void CGridComboBox::OnFillDroppedList(CListBox* pLbox)
{
	CGXComboBox::OnFillDroppedList(pLbox);
	m_pFormView->PostMessage(WM_COMBO_DROP_DOWN, (WPARAM) 0, (LPARAM) this);
}

int CGridComboBox::SetItemData(int nIndex, DWORD dwItemData)
{
/*	const CGXStyle& style = Grid()->LookupStyleRowCol(m_nRow, m_nCol);

	// fill with Choices
	if (m_bFillWithChoiceList && style.GetIncludeChoiceList())
	{
		CString s = style.GetChoiceListRef();
		int n = 0;
		CString sItem;
		while (!s.IsEmpty() && n != -1)
		{
			n = GXGetNextLine(s, sItem);
		}
	}
	return m_pDropDownWnd->GetLBox().SetItemData(nIndex, dwItemData);
*/
	m_aItemData.SetAtGrow(nIndex, dwItemData);

/*	TRACE("Item Size is %d\n", m_aItemData.GetSize());

	for(int i=0; i<m_aItemData.GetSize(); i++)
	{
		TRACE("Item %d: %d\n", i, m_aItemData.GetAt(i));
	}
*/

	return TRUE;
}

DWORD CGridComboBox::GetItemData(int nIndex) const
{
/*	TRACE("Item Size is %d\n", m_aItemData.GetSize());

	for(int i=0; i<m_aItemData.GetSize(); i++)
	{
		TRACE("Item %d: %d\n", i, m_aItemData.GetAt(i));
	}
*/	
	if(nIndex < 0 || m_aItemData.GetSize() <= nIndex)	return -1;
	
	return m_aItemData.GetAt(nIndex);
//	return m_pDropDownWnd->GetLBox().GetItemData(nIndex);
}

int CGridComboBox::GetItemDataSize()
{
	return m_aItemData.GetSize();
}

void CGridComboBox::ResetItemData()
{
	m_aItemData.RemoveAll();
}

void CGridComboBox::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{	
	CGXComboBox::OnDrawItem(nIDCtl, lpDrawItemStruct);
}
