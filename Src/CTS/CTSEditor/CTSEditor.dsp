# Microsoft Developer Studio Project File - Name="CTSEditor" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=CTSEditor - Win32 DebugForm
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "CTSEditor.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "CTSEditor.mak" CFG="CTSEditor - Win32 DebugForm"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "CTSEditor - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "CTSEditor - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE "CTSEditor - Win32 DebugForm" (based on "Win32 (x86) Application")
!MESSAGE "CTSEditor - Win32 ReleaseForm" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "CTSEditor - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "../../../obj/CTSEditor_Release/"
# PROP Intermediate_Dir "../../../obj/CTSEditor_Release/"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GR /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXEXT" /D "_MBCS" /D "_FORMATION_" /D "_DLG_MODEL_" /D "_USE_MODEL_USER_" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x412 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x412 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 /nologo /subsystem:windows /machine:I386 /out:"../../../bin/Release/CTSEditor.exe"

!ELSEIF  "$(CFG)" == "CTSEditor - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "../../../obj/CTSEditor_Debug/"
# PROP Intermediate_Dir "../../../obj/CTSEditor_Debug/"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GR /GX /ZI /Od /D "_AFXEXT" /D "_FORMATION_" /D "_DLG_MODEL_" /D "_USE_MODEL_USER_" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x412 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x412 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /out:"../../../bin/Debug/CTSEditor.exe" /pdbtype:sept

!ELSEIF  "$(CFG)" == "CTSEditor - Win32 DebugForm"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "CTSEditor___Win32_DebugForm"
# PROP BASE Intermediate_Dir "CTSEditor___Win32_DebugForm"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "CTSEditor___Win32_DebugForm"
# PROP Intermediate_Dir "CTSEditor___Win32_DebugForm"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GR /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_AFXDLL" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GR /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_AFXDLL" /D "_FORMATION_" /D "_DLG_MODEL_" /D "_USE_MODEL_USER_" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x412 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x412 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 PSCommonD.lib /nologo /subsystem:windows /debug /machine:I386 /out:"../Debug_/CTSEditor.exe" /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /out:"../../../bin/Debug/CTSEditor.exe" /pdbtype:sept

!ELSEIF  "$(CFG)" == "CTSEditor - Win32 ReleaseForm"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "CTSEditor___Win32_ReleaseForm"
# PROP BASE Intermediate_Dir "CTSEditor___Win32_ReleaseForm"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "CTSEditor___Win32_ReleaseForm"
# PROP Intermediate_Dir "CTSEditor___Win32_ReleaseForm"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GR /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GR /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "_FORMATION_" /D "_DLG_MODEL_" /D "_USE_MODEL_USER_" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x412 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x412 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 PSCommon.lib /nologo /subsystem:windows /machine:I386 /out:"../Release_/CTSEditor.exe"
# ADD LINK32 /nologo /subsystem:windows /machine:I386 /out:"../../../bin/Release/CTSEditor.exe"

!ENDIF 

# Begin Target

# Name "CTSEditor - Win32 Release"
# Name "CTSEditor - Win32 Debug"
# Name "CTSEditor - Win32 DebugForm"
# Name "CTSEditor - Win32 ReleaseForm"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\DataBaseClass\Dao\BatteryModelRecordSet.cpp
# End Source File
# Begin Source File

SOURCE=.\ChangeUserInfoDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CommonInputDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CTSEditor.cpp
# End Source File
# Begin Source File

SOURCE=.\CTSEditor.rc
# End Source File
# Begin Source File

SOURCE=.\CTSEditorDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\CTSEditorView.cpp
# End Source File
# Begin Source File

SOURCE=.\DateTimeCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\EditorSetDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\EndConditionDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DataBaseClass\Dao\GradeRecordSet.cpp
# End Source File
# Begin Source File

SOURCE=.\GraphWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\GridComboBox.cpp
# End Source File
# Begin Source File

SOURCE=.\LoginDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\ModelInfoDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ModelNameDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ModelSelDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MyGridWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\NewModelInfoDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DataBaseClass\Dao\PreTestCheckRecordSet.cpp
# End Source File
# Begin Source File

SOURCE=.\PrntScreen.cpp
# End Source File
# Begin Source File

SOURCE=.\DataBaseClass\Dao\ProcTypeRecordSet.cpp
# End Source File
# Begin Source File

SOURCE=.\ProgressDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\RealEditCtrl.Cpp
# End Source File
# Begin Source File

SOURCE=.\SelectSimulDataDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\DataBaseClass\Dao\StepRecordSet.cpp
# End Source File
# Begin Source File

SOURCE=.\TestDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DataBaseClass\Dao\TestListRecordSet.cpp
# End Source File
# Begin Source File

SOURCE=.\TestNameDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\TextParsing.cpp
# End Source File
# Begin Source File

SOURCE=.\UserAdminDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DataBaseClass\Dao\UserRecordSet.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\DataBaseClass\Dao\BatteryModelRecordSet.h
# End Source File
# Begin Source File

SOURCE=.\ChangeUserInfoDlg.h
# End Source File
# Begin Source File

SOURCE=.\CommonInputDlg.h
# End Source File
# Begin Source File

SOURCE=.\CTSEditor.h
# End Source File
# Begin Source File

SOURCE=.\CTSEditorDoc.h
# End Source File
# Begin Source File

SOURCE=.\CTSEditorView.h
# End Source File
# Begin Source File

SOURCE=.\DateTimeCtrl.h
# End Source File
# Begin Source File

SOURCE=.\EditorSetDlg.h
# End Source File
# Begin Source File

SOURCE=.\EndConditionDlg.h
# End Source File
# Begin Source File

SOURCE=.\DataBaseClass\Dao\GradeRecordSet.h
# End Source File
# Begin Source File

SOURCE=.\GraphWnd.h
# End Source File
# Begin Source File

SOURCE=.\GridComboBox.h
# End Source File
# Begin Source File

SOURCE=.\LoginDlg.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\ModelInfoDlg.h
# End Source File
# Begin Source File

SOURCE=.\ModelNameDlg.h
# End Source File
# Begin Source File

SOURCE=.\ModelSelDlg.h
# End Source File
# Begin Source File

SOURCE=.\MyGridWnd.h
# End Source File
# Begin Source File

SOURCE=.\NewModelInfoDlg.h
# End Source File
# Begin Source File

SOURCE=..\..\MyLib\Include\PowerSys.h
# End Source File
# Begin Source File

SOURCE=.\DataBaseClass\Dao\PreTestCheckRecordSet.h
# End Source File
# Begin Source File

SOURCE=.\PrntScreen.h
# End Source File
# Begin Source File

SOURCE=.\DataBaseClass\Dao\ProcTypeRecordSet.h
# End Source File
# Begin Source File

SOURCE=.\ProgressDlg.h
# End Source File
# Begin Source File

SOURCE=.\RealEditCtrl.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\SelectSimulDataDlg.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\DataBaseClass\Dao\StepRecordSet.h
# End Source File
# Begin Source File

SOURCE=.\TestDlg.h
# End Source File
# Begin Source File

SOURCE=.\DataBaseClass\Dao\TestListRecordSet.h
# End Source File
# Begin Source File

SOURCE=.\TestNameDlg.h
# End Source File
# Begin Source File

SOURCE=.\TextParsing.h
# End Source File
# Begin Source File

SOURCE=.\UserAdminDlg.h
# End Source File
# Begin Source File

SOURCE=.\DataBaseClass\Dao\UserRecordSet.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=".\res\ADP logo small.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\ADP logo.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\ADP logo2.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\ADP로고.bmp"
# End Source File
# Begin Source File

SOURCE=.\res\bitmap1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00001.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00002.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00003.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00004.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00005.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00006.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00007.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00008.bmp
# End Source File
# Begin Source File

SOURCE=.\res\cross.bmp
# End Source File
# Begin Source File

SOURCE=.\res\CTSEditor.ico
# End Source File
# Begin Source File

SOURCE=.\res\CTSEditor.rc2
# End Source File
# Begin Source File

SOURCE=.\res\CTSEditorDoc.ico
# End Source File
# Begin Source File

SOURCE=.\res\cursor1.cur
# End Source File
# Begin Source File

SOURCE=.\res\DATA2.ICO
# End Source File
# Begin Source File

SOURCE=.\res\ElicoPower.bmp
# End Source File
# Begin Source File

SOURCE=.\res\insert1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\load.bmp
# End Source File
# Begin Source File

SOURCE=.\res\next.bmp
# End Source File
# Begin Source File

SOURCE=".\res\PNELogo Small.bmp"
# End Source File
# Begin Source File

SOURCE=.\res\PNELogo.bmp
# End Source File
# Begin Source File

SOURCE=.\res\red_btn_.bmp
# End Source File
# Begin Source File

SOURCE=.\res\RedButton.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Save.bmp
# End Source File
# Begin Source File

SOURCE=.\res\SmallCross.bmp
# End Source File
# Begin Source File

SOURCE=.\res\tick.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\YellowBar.bmp
# End Source File
# Begin Source File

SOURCE=".\res\크기변환_NessLogo.bmp"
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
