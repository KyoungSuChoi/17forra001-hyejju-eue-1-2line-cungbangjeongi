// ModuleAddDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "ModuleAddDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CModuleAddDlg dialog

bool CModuleAddDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CModuleAddDlg"), _T("TEXT_CModuleAddDlg_CNT"), _T("TEXT_CModuleAddDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CModuleAddDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CModuleAddDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}


CModuleAddDlg::CModuleAddDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CModuleAddDlg::IDD, pParent)
{
	LanguageinitMonConfig();

	//{{AFX_DATA_INIT(CModuleAddDlg)
	m_nModuleID = 1;
	//}}AFX_DATA_INIT
	m_bAddDlg = TRUE;
}

CModuleAddDlg::~CModuleAddDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


void CModuleAddDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CModuleAddDlg)
	DDX_Control(pDX, IDC_MODULE_SEL_COMBO, m_ctrlModuleSel);
	DDX_Text(pDX, IDC_MODULENUM, m_nModuleID);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CModuleAddDlg, CDialog)
	//{{AFX_MSG_MAP(CModuleAddDlg)
	ON_EN_CHANGE(IDC_MODULENUM, OnChangeModulenum)
	ON_CBN_SELCHANGE(IDC_MODULE_SEL_COMBO, OnSelchangeModuleSelCombo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CModuleAddDlg message handlers

void CModuleAddDlg::OnChangeModulenum() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(m_nModuleID <1 || m_nModuleID > 0x7FFF)
	{
		AfxMessageBox(TEXT_LANG[0]);//"입력된 ID는 사용가능한 ID가 아닙니다."
	}
}

void CModuleAddDlg::OnOK() 
{
	// TODO: Add extra validation here
	if(m_nModuleID <1 || m_nModuleID > 0x7FFF)
	{
		AfxMessageBox(TEXT_LANG[0]);//"입력된 ID는 사용가능한 ID가 아닙니다."
		return;
	}
	CDialog::OnOK();
}

BOOL CModuleAddDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if(m_bAddDlg)
	{
		GetDlgItem(IDC_MODULE_SEL_COMBO)->ShowWindow(SW_HIDE);
	}
	else
	{
		GetDlgItem(IDC_MODULENUM)->ShowWindow(SW_HIDE);
	}
		
	GetDlgItem(IDC_MODULENUM)->SetFocus();
	//Registry에서 PowerFormation DataBase 경로를 검색 
	long rtn;
	HKEY hKey = 0;
	BYTE buf[512], buf2[512];
	DWORD size = 511;
	DWORD type;
	
	CString strDBPath;

	rtn = ::RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\ElicoPower Formation System\\PowerFormation\\Path", 0, KEY_READ, &hKey);
	if(ERROR_SUCCESS == rtn)
	{	
		//PowerFormation DataBase 경로를 읽어온다. 
		rtn = ::RegQueryValueEx(hKey, "DataBase", NULL, &type, buf, &size);
		::RegCloseKey(hKey);
		
		//
		if(ERROR_SUCCESS == rtn && (type == REG_SZ || type == REG_EXPAND_SZ))
		{
			strDBPath.Format("%s\\%s", buf, FORM_SET_DATABASE_NAME);			
		}
	}

	rtn = ::RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\ElicoPower Formation System\\PowerFormation\\FormSetting", 0, KEY_READ, &hKey);
	if(ERROR_SUCCESS == rtn)
	{
		rtn = ::RegQueryValueEx(hKey, "Module Name", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && (type == REG_SZ || type == REG_EXPAND_SZ))
		{
			m_strModuleName = buf;
		}
		size = 255;
		rtn = ::RegQueryValueEx(hKey, "Group Name", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && (type == REG_SZ || type == REG_EXPAND_SZ))
		{
			m_strGroupName = buf;
		}
		size = 255;
		rtn = ::RegQueryValueEx(hKey, "Module Per Rack", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && type == REG_DWORD)
		{
			sprintf((char *)buf2, "%ld", *(long *)buf);
			m_nModulePerRack = atol((LPCTSTR)buf2);
		}
		size = 255;
		rtn = ::RegQueryValueEx(hKey, "Use Rack Index", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && type == REG_DWORD)
		{
			sprintf((char *)buf2, "%ld", *(long *)buf);
			m_bUseRackIndex = atol((LPCTSTR)buf2);
		}
		size = 255;
		rtn = ::RegQueryValueEx(hKey, "Use Group", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && type == REG_DWORD)
		{
			sprintf((char *)buf2, "%ld", *(long *)buf);
			m_bUseGroupSet = atol((LPCTSTR)buf2);
		}
		::RegCloseKey(hKey);
	}

	int nCount = 0;
	CString strTemp;
	int nSelIndex = 0;
	CDaoDatabase  db;
	
	try
	{
		db.Open(strDBPath);

		CString strSQL("SELECT ModuleID FROM SystemConfig ORDER BY ModuleID");
		CDaoRecordset rs(&db);
			rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
			while(!rs.IsEOF())
			{
				COleVariant data = rs.GetFieldValue(0);
				strTemp.Format("%s", ModuleName(data.lVal));
				m_ctrlModuleSel.AddString(strTemp);
				
				if(data.lVal == m_nModuleID)
					nSelIndex = nCount;
				
				m_ctrlModuleSel.SetItemData(nCount++, data.lVal);
				rs.MoveNext();
			}
		rs.Close();
		db.Close();	
	}
	catch (CDaoException *e)
	{
	//	e->GetErrorMessage();
		e->Delete();
	}

	m_ctrlModuleSel.SetCurSel(nSelIndex);
	
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

int CModuleAddDlg::GetModuleID()
{
	return m_nModuleID;
}


CString CModuleAddDlg::ModuleName(int nModuleID, int nGroupIndex)
{
	CString strName, strMDName;
	strMDName = m_strModuleName;

/*	EP_SYSTEM_PARAM *pParam = EPGetSysParam(nModuleID);
	if(pParam)
	{
		if(pParam->wModuleType == EP_ID_IROCV_SYSTEM)
		{
			strMDName = "IROCV";
		}
	}
*/	
	if(m_bUseRackIndex)
	{
		div_t div_result;
		div_result = div( nModuleID-1, m_nModulePerRack );

		if(m_bUseGroupSet && nGroupIndex >= 0)
		{
			strName.Format("%s %d-%d, %s %d", m_strModuleName, div_result.quot+1, div_result.rem+1, m_strGroupName, nGroupIndex+1);
		}
		else
		{
			strName.Format("%s %d-%d", m_strModuleName, div_result.quot+1, div_result.rem+1);
		}
	}
	else
	{
		if(m_bUseGroupSet && nGroupIndex >= 0)
		{
			strName.Format("%s %d, %s %d", m_strModuleName, nModuleID,  m_strGroupName, nGroupIndex+1);
		}
		else
		{
			strName.Format("%s %d", m_strModuleName, nModuleID);
		}
	}
	return strName;
}

void CModuleAddDlg::OnSelchangeModuleSelCombo() 
{
	// TODO: Add your control notification handler code here
	int index =m_ctrlModuleSel.GetCurSel();
	if(index >= 0)
		m_nModuleID = m_ctrlModuleSel.GetItemData(index);	
}