// NewModelInfoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ctseditor.h"
#include "NewModelInfoDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CNewModelInfoDlg dialog


CNewModelInfoDlg::CNewModelInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CNewModelInfoDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CNewModelInfoDlg)
	m_strName = _T("");
	m_strDescript = _T("");
	m_strID = _T("");
	m_fCapacity = 0.0f;
	m_fWidth = 0.0f;
	m_fHeight = 0.0f;
	m_fDepth = 0.0f;
	m_strBarCode = _T("");
	//}}AFX_DATA_INIT
}


void CNewModelInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNewModelInfoDlg)
	DDX_Text(pDX, IDC_NAME_EDIT, m_strName);
	DDX_Text(pDX, IDC_DESC_EDIT, m_strDescript);
	DDX_Text(pDX, IDC_USERID_EDIT, m_strID);
	DDX_Text(pDX, IDC_CAP_EDIT, m_fCapacity);
	DDX_Text(pDX, IDC_W_EDIT, m_fWidth);
	DDX_Text(pDX, IDC_H_EDIT, m_fHeight);
	DDX_Text(pDX, IDC_D_EDIT, m_fDepth);
	DDX_Text(pDX, IDC_BARCODE_EDIT, m_strBarCode);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CNewModelInfoDlg, CDialog)
	//{{AFX_MSG_MAP(CNewModelInfoDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNewModelInfoDlg message handlers


BOOL CNewModelInfoDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CString strTemp;
	char szBuff[64], szUnit[32], szDecimalPoint[32];
	//Current Unit
	sprintf(szBuff, AfxGetApp()->GetProfileString(EDITOR_REG_SECTION, "C Unit", "mAh 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	
	GetDlgItem(IDC_CAP_UNIT_STATIC)->SetWindowText(szUnit);

	if(m_strID.IsEmpty() == FALSE)
	{
		GetDlgItem(IDC_USERID_EDIT)->EnableWindow(FALSE);
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CNewModelInfoDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData();
	if(m_strName.IsEmpty())
	{
		AfxMessageBox("모델명이 입력되지 않았습니다.");
		return;
	}
	CDialog::OnOK();
}
