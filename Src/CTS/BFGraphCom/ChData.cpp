/***********************************************************************
	Copyrighted by LG Production engineering Research Center

	File Name: ChData.cpp

	Written out by 
	Evaluation Thechnology Group
	Kim, Sung-Hoon

	Comment: Member functions of CChData class are defined
***********************************************************************/

// ChData.cpp: implementation of the CChData class.
//
//////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

// #include

#include "stdafx.h"
#include "ChData.h"
#include "Table.h"
#include "ScheduleData.h"
#include <float.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

/*
	Name:    CChData
	Comment: Constructor of CChData class
	Input:   LPCTSTR strPath - Path where channel data is located
	Output:
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
CChData::CChData(LPCTSTR strPath) : m_strPath(strPath)
{
	// Initial point of pattern object is NULL
	m_pSchedule = NULL;
	m_nTotalRecordCount = 0;
//	m_fCapacitySum = 0;
}

/*
	Name:    ~CChData
	Comment: Destructor of CChData class
	Input:   
	Output:
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
CChData::~CChData()
{
	// Pattern object에 할당된 memory가 있으면 지운다.
	if(m_pSchedule!=NULL) 
	{
		delete m_pSchedule;
		m_pSchedule = NULL;
	}

	// Table의 list를 비우면서 각 Table에 할당된 memeory를 지운다.
	while(!m_TableList.IsEmpty())
	{
		CTable* pTable = m_TableList.RemoveTail();
		delete pTable;
	}
}

/*
	Name:    Create
	Comment: 생성자에서 지정된 path에서
	         1. "pattern file"을 열어 pattern object를 생성하고
			 2. "Table list file"을 열어 table object의 list를 만든다. 
	Input:   
	Output:
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/

//Formation Type
//20081028
BOOL CChData::Create(BOOL bIncludeWorkingStep)
{
	CFileFind afinder;
	ResetData();

	ASSERT(m_pSchedule == NULL);

	if(afinder.FindFile(m_strPath))
	{
		return CreateA(bIncludeWorkingStep);
	}

/*	if(afinder.FindFile(m_strPath+"\\*.rp$") && bIncludeWorkingStep)
	{
		afinder.FindNextFile();
		CString fileName = afinder.GetFilePath();
		// Table List File을 열어서
		CStdioFile afile;
		if(afile.Open(fileName, CFile::modeRead|CFile::typeText|CFile::shareDenyNone))
		{
			// 그 내용을 읽어온다.
			CStringList strlist;
			CString strContent;
			while(afile.ReadString(strContent))
			{
				if(!strContent.IsEmpty()) strlist.AddTail(strContent);
			}
			afile.Close();

			// 내용이 2줄 이상이면 Table이 생성되어 있는 것임
			if(strlist.GetCount()>2)
			{
				// 첫번째는 StartT= ,EndT=, Serial=,	기록 에서 End Time 만 추출 

				POSITION pos = strlist.GetHeadPosition();

				// 첫번째는 StartT= ,EndT=, Serial=,	기록
				strContent = strlist.GetNext(pos);
				int p1, p2, s;
				
				CString strStartTime, strSerial, strUser, strDes, strTray;
					//Start Time and End Time Parsing
					// Start Time 정보 추출 
					s  = strContent.Find("StartT");
					p1 = strContent.Find('=', s);
					p2 = strContent.Find(',', s);
					strStartTime= strContent.Mid(p1+1,p2-p1-1);
					
					// End Time 정보 추출 
					s  = strContent.Find("EndT");
					p1 = strContent.Find('=', s);
					p2 = strContent.Find(',', s);
					m_strEndT= strContent.Mid(p1+1,p2-p1-1);
					
					// Test Serial 정보 추출 
					s  = strContent.Find("Serial");
					p1 = strContent.Find('=', s);
					p2 = strContent.Find(',', s);
					strSerial = strContent.Mid(p1+1,p2-p1-1);
					
					// UserID 정보 추출 
					s  = strContent.Find("User");
					p1 = strContent.Find('=', s);
					p2 = strContent.Find(',', s);
					strUser = strContent.Mid(p1+1,p2-p1-1);
					
					// Comment 정보 추출 
					s  = strContent.Find("Descript");
					p1 = strContent.Find('=', s);
					p2 = strContent.Find(',', s);
					strDes = strContent.Mid(p1+1,p2-p1-1);

					s  = strContent.Find("TrayNo");
					p1 = strContent.Find('=', s);
					p2 = strContent.Find(',', s);
					strTray = strContent.Mid(p1+1,p2-p1-1);

				if(m_TableList.GetCount() < 1)
				{
					m_strStartT = strStartTime;
					m_strSerial = strSerial;
					m_strUserID = strUser;
					m_strDescript = strDes;
					m_strTrayNo = strTray;
				}
				
				//2번째 Row는 Title 기록 
				CString strTitle =  strlist.GetNext(pos);		//Title
				
				// Table을 만든다.
				strContent = strlist.GetTail();
				
				CTable* pTable = new CTable(strTitle,strContent);
				//결과 data가 하나도 없거나(최초 step) step이나
				if(	m_TableList.GetCount() <= 0 )
				{
					m_TableList.AddTail(pTable);
					m_nTotalRecordCount += pTable->GetRecordCount();
				}
				else 
				{	//index가 정상인 경우  
					if(pTable->GetRecordCount() > 0)
					{
						CTable *pLastT = m_TableList.GetTail();						
						if(pLastT->GetLastRecordIndex() < pTable->GetRecordIndex())
						{
							m_TableList.AddTail(pTable);
							m_nTotalRecordCount += pTable->GetRecordCount();
						}
						else
						{
							delete pTable;
						}
					}
					else
					{
						delete pTable;
					}
				}
			}
		}
	}
*/
	return FALSE;
}


//Formation Type
//20081028
BOOL CChData::CreateA(BOOL bIncludeWorkingStep)
{
	if(m_strPath.IsEmpty())		return FALSE;

	TRACE("■■■File Read Start!!\n");

	//Formation Field 강제 Mapping 
	CWordArray aItem;
	aItem.Add(PS_DATA_SEQ);
	aItem.Add(PS_TOT_TIME);
	aItem.Add(PS_STEP_TIME);
	aItem.Add(PS_VOLTAGE);
	aItem.Add(PS_CURRENT);
	aItem.Add(PS_CAPACITY);
	aItem.Add(PS_WATT_HOUR);
//	aItem.Add(PS_TEMPERATURE);
		
/*	m_strStartT = pHeader->testHeader.szStartTime;
	m_strEndT	= pHeader->testHeader.szEndTime;
	m_strSerial	= pHeader->testHeader.szSerial;
	m_strUserID = pHeader->testHeader.szUserID;
	m_strDescript = pHeader->testHeader.szDescript;
	m_strTrayNo = pHeader->testHeader.szTrayNo;
*/

	CFileException ex;	
	CStdioFile sourceFile;

	CString saveFilePath;
	CString strReadData;
	CString strData;
	CString strIndex;
	CString strLog;
	int nIndex = 0;
	int nSize = -1;
	
	int rsCount = 0;
	int nStepNo;
	int nPrevStep = -1;
	int nState, nPrevState = -1;	
	float *fData = new float[aItem.GetSize()];
	CTable* pTable = NULL;
	float fTime = 0.0f;
	float fPreTotTime = 0.0f;

	CArray<float, float> aryData1[7]; 
	int nMyStpNo = 0;

	int nTemp1, nTemp2;

	if( !sourceFile.Open( m_strPath, CFile::modeRead, &ex ))
	{
		CString strMsg;
		ex.GetErrorMessage((LPSTR)(LPCSTR)strMsg,1024);
		AfxMessageBox(strMsg);	
		return false;
	}

	int nRtn = 0;
	bool bInitValue = false;
/*
	FILE *fp = NULL;
	if( fopen_s(&fp, m_strPath, "rt") != 0 )
	{
		return FALSE;
	}
	*/

	sourceFile.ReadString(strReadData);
	strLog.Format("File 주석 : %s\n", strReadData );
	TRACE(strLog);

	while(TRUE)	
	{			
		sourceFile.ReadString(strReadData);    // 한 문자열씩 읽는다.	
		nSize = strReadData.Find(',');
		if( nSize < 0 )
		{
			break;
		}
	
		AfxExtractSubString(strData, strReadData, 0,',');
		nStepNo = atoi(strData);
		AfxExtractSubString(strData, strReadData, 1,',');
		nState = atoi(strData);
		AfxExtractSubString(strData, strReadData, 2,',');
		fData[1] = atof(strData);
		AfxExtractSubString(strData, strReadData, 3,',');
		fData[2] = atof(strData);
		AfxExtractSubString(strData, strReadData, 4,',');
		fData[3] = atof(strData);
		AfxExtractSubString(strData, strReadData, 5,',');
		fData[4] = atof(strData);
		AfxExtractSubString(strData, strReadData, 6,',');
		fData[5] = atof(strData);
		AfxExtractSubString(strData, strReadData, 7,',');
		fData[6] = atof(strData);
		// AfxExtractSubString(strData, strReadData, 8,',');
		// AfxExtractSubString(strData, strReadData, 9,',');			
	
/*
	char szBuff[256];
	ZeroMemory( szBuff, sizeof(szBuff));

	fscanf_s(fp, "%s", szBuff, 256);

	int nRtn = 0;
	bool bInitValue = false;

	while( TRUE )
	{
		nRtn = fscanf(fp, "%d,%d,%f,%f,%f,%f,%f,%f,%d,%21s,", &nStepNo, &nState, &fData[1],&fData[2],&fData[3],&fData[4],&fData[5],&fData[6],&nTemp1,szBuffTemp,sizeof(szBuffTemp));
		if( nRtn != 10 )
		{
			fclose(fp);
			break;
		}
		*/

		if(nPrevState != nState || nPrevStep != nStepNo )
		{
			if(pTable != NULL)
			{
				pTable->SetTableData(aryData1);
				m_nTotalRecordCount += pTable->GetRecordCount();
			}
			
			pTable = new CTable(&aItem, nMyStpNo, nState, rsCount);
			TRACE("Add step data : STEP %d, MYSTEP %d\n", nStepNo, nMyStpNo);			
			m_TableList.AddTail(pTable);

			nMyStpNo++;

			nPrevStep = nStepNo;
			nPrevState = nState;

			fTime = 0;
		}
		
		if( bIncludeWorkingStep == FALSE )
		{
			//시간이 중복된 Data는 표시하지 않음(Formation은 먼저 끝난 CH은 종료 Data가 중복되서 연속  저장된다. 
			// if(fPrefTime == fData[2] || fPrefTotTime == fData[1] )
			if( fPreTotTime > fData[1] )
			{
				continue;
			}
		}
		else
		{	
			// if( fPrefTime == fData[2] || fPrefTotTime == fData[1] )
			if( fPreTotTime > fData[1] )
			{
				continue;
			}
			else
			{
				if( bInitValue == false )
				{
					bInitValue = true;
					fPreTotTime = fData[1];					
				}

				fTime = fTime + (fData[1] - fPreTotTime);
				fPreTotTime = fData[1];

				fData[2] = fTime;			
				
				/*
				if( fPrefTime >= fData[2] && nPrevState == nState && nPrevStep == nStepNo )	//같은 Step종류에서 Step 시간이 멈춘 경우 => 먼저 끝난 Ch
				{	
					if(fData[1] - fPrefTotTime > 0)	// total time 변경분만큼 Step time을 이전 time에 더해줘서 시간을 생성 함
					{
						fData[2] = fPrefTime + (fData[1]-fPrefTotTime);
						// strLog.Format("StepTime : %f\n", fData[2]);							
						// TRACE(strLog);
					}
					else
					{
						fData[2] = fPrefTime + (fData[1]-fPrefTotTime);
						// strLog.Format("Error StepTime : %f\n", fData[2]);							
						// TRACE(strLog);
					}
				}
				*/
			}
		}

		/*
		//시간이 중복된 Data는 표시하지 않음(Formation은 먼저 끝난 CH은 종료 Data가 중복되서 연속  저장된다. 
		if(fPrefTime == fData[2])	
			continue;		

		fPrefTime = fData[2];
		*/
		
		fData[0] = rsCount;			//RECORD COUNT

		for(int i=0; i<7; i++)
		{
			aryData1[i].Add(fData[i]);
		}

		rsCount++;
	}

	delete [] fData;
	sourceFile.Close();

	//가장 마지막 Step Table 저장 
	if(pTable)
	{
		pTable->SetTableData(aryData1);
		m_nTotalRecordCount += pTable->GetRecordCount();
//		TRACE("%d :: Step %d record size %d(Total %d)\n", m_TableList.GetCount(), nPrevStep, pTable->GetRecordCount(), m_nTotalRecordCount);
	}
	
	//21227
	ASSERT(m_nTotalRecordCount == rsCount);

	if(m_TableList.GetCount() < 1)	return FALSE;

	TRACE("■■■File Read OK, Table Size is %d\n", m_TableList.GetCount());
	return TRUE;
}

CScheduleData* CChData::GetPattern()
{
	// Pattern file이 없으면 return NULL.
	if(m_pSchedule == NULL)
	{
		CFileFind afinder;
		if(afinder.FindFile(m_strPath+"\\*.sch"))
		{
			afinder.FindNextFile();
			m_pSchedule = new CScheduleData;		
			if(!m_pSchedule->SetSchedule(afinder.GetFilePath()))
			{
				delete m_pSchedule;
				m_pSchedule = NULL;
			}
		}
	}
	return m_pSchedule; 
}

CString CChData::GetLogFileName()
{
	CString strFileName;
	CFileFind afinder;
	if(afinder.FindFile(m_strPath+"\\*.log"))
	{
		afinder.FindNextFile();
		strFileName =  afinder.GetFilePath();
	}
	return strFileName;
}


/*
	Name:    GetLastDataOfTable
	Comment: 지정된 Table에서 지정된 Transducer의 마지막 값을 가져온다.
	Input:   LONG    lIndex   - Table의 번호
	         LPCTSTR strTitle - data를 가져올 transducer의 이름
			 WORD    wPoint   - Pack용에서 'V_MaxDiff'와 'T_MaxDiff'를 계산하기 위해
			                    사용할 point의 정보를 담고 있는 변수
	Output:
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
float CChData::GetLastDataOfTable(LONG lIndex, LPCTSTR strTitle, WORD wPoint)
{
	CTable* pTable = NULL;

	// Table list에서 lIndex의 index를 가지는 table을 찾는다.

	//Table에서 No가 아닌 순서 Index로 찾도록 수정 //20050818 KBH
/*	POSITION pos = m_TableList.GetHeadPosition();
	while(pos)
	{
		pTable = m_TableList.GetNext(pos);
		if(pTable->GetNo()==lIndex) break;
		else pTable = NULL;
	}
*/
	POSITION pos = m_TableList.FindIndex(lIndex);
	if(pos == NULL)		return 0.0f;
	pTable = m_TableList.GetAt(pos);

//////////////////////////////////////////////////////////////////////////
//	Data를 요청하면 만약 Index가 없을 경우 전체 Size가 증가 하므로 적절치 않음
//  Commented By KBH 2005/7/22
//////////////////////////////////////////////////////////////////////////
	// 혹 "TableList" file이 없거나 이 file에 index가 누락되어
	// table이 생성되어 있지 않아서 해당 object가 없는 경우,
//	if(pTable==NULL)
//	{
//		// lIndex를 index로 가지는 table object를 하나 생성하여 list에 넣는다.
//		pTable = new CTable(lIndex);
//		m_TableList.AddTail(pTable);
//		// 그리고 Table의 data를 loading한다.
//		pTable->LoadData(m_strPath);
//	}
//////////////////////////////////////////////////////////////////////////

	if(pTable == NULL)
		return 0.0f;

	// Table에서 strTitle을 이름으로 하는 transducer의 마지막 데이터를 얻는다.
	//OCV는 첫번재 Data에 기록되어 있으므로 실시간 data를 load후 호출 
	if(CString(strTitle).CompareNoCase(RS_COL_OCV) == 0)
	{
//		pTable->LoadDataA(m_strPath);
	}

	float fltVal = pTable->GetLastData(strTitle,wPoint);

	// 만약 마지막 데이터를 얻지 못한 상태이면 0.0 이 return 되도록 한다.
	if(fltVal==FLT_MAX) fltVal=0.0f;

	//
	return fltVal; 
}

//지정 Cycle에서 type의 strYAxisTitle 값을 return
float CChData::GetLastDataOfCycle(LONG lCycleID, DWORD dwMode, LPCTSTR strYAxisTitle)
{
	CTable* pTable = NULL;

	float fltRtn =0.0f;
	LONG lIndex = GetFirstTableIndexOfCycle(lCycleID);

	POSITION pos = m_TableList.FindIndex(lIndex);
//	while(pos)
//	{
		pTable = m_TableList.GetNext(pos);
		if(pTable)
		{
			if(pTable->GetStepNo() == lCycleID)
			{
				//여러개 Step조건을 선택하면 가장 처음 Maching된는 조건을 사용(Charge => Discharge => Reset)
				if(dwMode & (0x01<<pTable->GetType()))
				{
					//요청 항목이 OCV이면 실시간 data를 Loading 후 return
					if(CString(strYAxisTitle).CompareNoCase(RS_COL_OCV) == 0)
					{
//						pTable->LoadDataA(m_strPath);
					}
					fltRtn = pTable->GetLastData(strYAxisTitle);
				}
			}
			//1 Cycle에 같은 종류의 Step이 여러개 존재할 경우 가장 마지막 Step의 Data값 사용
//			else if(pTable->GetCycleNo() > lCycleID)				
//			{
//				break;
//			}
		}
//	}

	return fltRtn;
}


/*
	Name:    GetDataOfTable
	Comment: 지정된 Table에서 지정된 Transducer의 데이터를 가져온다.
	Input:   LONG    lIndex        - Table의 번호
	         LONG&   lDataNum      - Table에서 가져가는 데이터의 개수를 돌려받을 reference 변수
	         LPCTSTR strXAxisTitle - X축 data의 transducer 이름
	         LPCTSTR strYAxisTitle - Y축 data의 transducer 이름
			 WORD    wPoint        - Pack용에서 'V_MaxDiff'와 'T_MaxDiff'를 계산하기 위해
			                         사용할 point의 정보를 담고 있는 변수
	Output:
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
fltPoint* CChData::GetDataOfTable(LONG lIndex, LONG& lDataNum, LPCTSTR strXAxisTitle, LPCTSTR strYAxisTitle, WORD wPoint)
{

	CTable* pTable = NULL;

	//Table에서 No가 아닌 순서 Index로 찾도록 수정 //20050818 KBH
	// Table list에서 lIndex의 index를 가지는 table을 찾는다.
/*	POSITION pos = m_TableList.GetHeadPosition();
	while(pos)
	{
		pTable = m_TableList.GetNext(pos);
		if(pTable->GetNo()==lIndex) break;
		else pTable = NULL;
	}
*/
	POSITION pos = m_TableList.FindIndex(lIndex);
	if(pos == NULL)		return NULL;
	pTable = m_TableList.GetAt(pos);


//////////////////////////////////////////////////////////////////////////
//	Data를 요청 하면 서 Size가 변하므로 적절치 않음
//  Commented By KBH 2005/7/22
//////////////////////////////////////////////////////////////////////////
	// 혹 "TableList" file이 없거나 이 file에 index가 누락되어
	// table이 생성되어 있지 않아서 해당 object가 없는 경우,
//	if(pTable==NULL)
//	{
//		// lIndex를 index로 가지는 table object를 하나 생성하여 list에 넣는다.
//		pTable = new CTable(lIndex);
//		m_TableList.AddTail(pTable);
//	}
//////////////////////////////////////////////////////////////////////////

	if(pTable == NULL)
		return NULL;
//////////////////////////////////////////////////////////////////////////

	// Table의 data를 loading한다.
//	pTable->LoadData(m_strPath);
//	pTable->LoadDataA(m_strPath);

	//
	return pTable->GetData(lDataNum, strXAxisTitle, strYAxisTitle, wPoint);
}


//Formation type
//해당 cycle 번호를 갖는 Step Data
fltPoint* CChData::GetDataOfCycle(LONG lCycleID, LONG& lDataNum, DWORD dwMode, LPCTSTR strXAxisTitle, LPCTSTR strYAxisTitle, WORD wPoint)
{
	CTable* pTable = NULL;
	floatData *fTempData = NULL;
	LONG lTotSize = 0, lSize;

	LONG lIndex = GetFirstTableIndexOfCycle(lCycleID);
	POSITION pos = m_TableList.FindIndex(lIndex);
	
	//1. 전체 Data의 크기를 구한다.
	pTable = m_TableList.GetNext(pos);
	if(pTable)
	{
		lSize = 0;
		fTempData = NULL;
		//Mode에 따라 해당 Type의 Step 포함 여부 Check
		if(pTable->GetStepNo() == lCycleID)
		{
			TRACE("Type 0x%X\n", pTable->GetType());
			if(dwMode & (0x01<<pTable->GetType()))
			{
				//lTotSize +=pTable->GetRecordCount();
				fTempData = pTable->GetData(lSize, strXAxisTitle, strYAxisTitle, wPoint);
 				if(fTempData != NULL)
 				{
 					lTotSize += lSize;
 					delete fTempData;
 				}
			}
		}
	}
	if(lTotSize <= 0)	return NULL;


	//2. Data를 합한다.
	CString strXAxisTemp(strXAxisTitle);
	LONG lOffset = 0;
	float fltSum =0.0f;
	floatData *fData = new floatData[lTotSize]; 
	pos = m_TableList.FindIndex(lIndex);

	//전체 Data의 크기를 구한다.
	pTable = m_TableList.GetNext(pos);
	if(pTable)
	{
		lSize = 0;
		fTempData = NULL;
		//해당 Cycle 번호를 갖는 모든 data를 합하여 return한다.
		//Mode에 따라 해당 Type의 Step 포함 여부 Check
		if(pTable->GetStepNo() == lCycleID)
		{
			if(dwMode & (0x01<<pTable->GetType()))
			{
				TRACE("Cycle data added. Type %d, Time Sum :%f\n", pTable->GetType(), fltSum);
				// Table의 data를 loading한다.
				fTempData = pTable->GetData(lSize, strXAxisTitle, strYAxisTitle, wPoint);
//				lSize = pTable->GetRecordCount();

				if(fTempData != NULL)
				{
					//1 Cycle
/*					if(strXAxisTemp.CompareNoCase("Time") == 0 )
					{
						//시간합을 표시 
						for(int i=0; i<lSize; i++)
						{
							fltPoint pt = fTempData[i];
							pt.x += fltSum;
							fTempData[i] = pt;
						}
						fltSum = fTempData[lSize-1].x;
					}
*/					
					memcpy((char *)fData+sizeof(floatData)*lOffset, fTempData, sizeof(floatData)*lSize);
					lOffset += lSize;
					delete fTempData;
				}
			}
		}
	}
	lDataNum = lTotSize;

	return fData;
}


void CChData::ResetData()
{
	// Pattern object에 할당된 memory가 있으면 지운다.
	if(m_pSchedule!= NULL) 
	{
		delete m_pSchedule;
		m_pSchedule = NULL;
	}

	// Table의 list를 비우면서 각 Table에 할당된 memeory를 지운다.
	while(!m_TableList.IsEmpty())
	{
		CTable* pTable = m_TableList.RemoveTail();
		delete pTable;
	}

	m_nTotalRecordCount = 0;
//	m_fCapacitySum = 0;
}

BOOL CChData::IsOKCell()
{
	//상태 정보 
	float fData = GetLastDataOfTable(m_TableList.GetCount(), "Code");
	BYTE code = (BYTE)fData;
	
	return PSIsCellOk(code);
}

LONG	CChData::GetTableNo(int nIndex)
{
/*	POSITION pos = m_TableList.GetHeadPosition();
	int nCount = 0;
	while(pos)
	{
		pTable = m_TableList.GetNext(pos);
		if(nIndex == nCount++)
		{
			CString strName;
			strName.Format("%d", nIndex);
			AfxMessageBox(strName);

		}
	}
*/

	CTable* pTable = NULL;
	pTable = (CTable*)m_TableList.GetAt(m_TableList.FindIndex(nIndex));
	if(pTable == NULL)	return -1;

	return pTable->GetStepNo();
}

LONG CChData::GetTableIndex(int nNo)
{
	POSITION pos = m_TableList.GetHeadPosition();
	int nIndex = 0;
	CTable* pTable = NULL;
	while(pos)
	{
		pTable = m_TableList.GetNext(pos);
//		int aa = pTable->GetStepNo();
		if(pTable->GetStepNo() == nNo) return nIndex;
		
		nIndex++;
	}
	return -1;
}

//Formation type
//20081029
long CChData::GetFirstTableIndexOfCycle(LONG lCycleID)
{
	POSITION pos = m_TableList.GetHeadPosition();
	int nIndex = 0;
	CTable* pTable = NULL;
	while(pos)
	{
		pTable = m_TableList.GetNext(pos);
		if(pTable->GetStepNo() == lCycleID) return nIndex;
		
		nIndex++;
	}
	return 0;
}

BOOL CChData::ReLoadData()
{
	Create();	
	return TRUE;
}

//nStepIndex까지의 용량 합산을 return 한다.
//nStepIndex = -1 =>모든 step
float CChData::GetCapacitySum(int nStepIndex)
{
	CTable *pTable = NULL;
	POSITION pos = m_TableList.GetHeadPosition();

	float fCapaSum = 0.0f;
	int nIndex = 0;
	int nType = 0;
	
	//All step data
	if(nStepIndex < 1)	nStepIndex = 0x7FFFFFFF;

	while(pos)
	{
		pTable = m_TableList.GetNext(pos);
		if(pTable && nIndex<nStepIndex)
		{
			nType = (int)pTable->GetLastData("Type");
			if(nType == PS_STEP_CHARGE)
			{
				fCapaSum += pTable->GetLastData("Capacity");
			}
			else if(nType == PS_STEP_DISCHARGE || nType == PS_STEP_IMPEDANCE)
			{
				fCapaSum -= pTable->GetLastData("Capacity");
			}
		}
		nIndex++;
	}
	return fCapaSum;
}

CWordArray * CChData::GetRecordItemList(LONG lIndex)
{
	CTable *pTable = NULL;
	POSITION pos = m_TableList.GetHeadPosition();

	int nCnt = 0;
	while(pos)
	{
		pTable = m_TableList.GetNext(pos);
		if(pTable && nCnt++ == lIndex)
		{
			return pTable->GetRecordItemList();
		}
	}
	return NULL;
}

//많은양의 table이 있을시 GetLastDataOfTable()과 같은 함수에서 
//CList::Find() 함수를 여러번 호출하여 속도가 느려지는 현상을 없에기 위해 
//Table을 직접 호출하는 함수 추가 
//2006/8/7 KBH
POSITION CChData::GetFirstTablePosition()
{
	return m_TableList.GetHeadPosition();
}

CTable * CChData::GetNextTable(POSITION &pos)
{
	return (CTable *)m_TableList.GetNext(pos);
}

CTable * CChData::GetTableAt(LONG lIndex)
{
	POSITION pos = m_TableList.FindIndex(lIndex);
	if(pos)
	{
		return (CTable*)m_TableList.GetAt(pos);
	}
	return NULL;
}	

//속도 향상을 위해 현재 결과의 최종 data table을 Loading하는 함수를 별도로 만든다.
//Static 함수이고 반환된 CTable *은 호출자가 Delete해야 한다.
CTable * CChData::GetLastDataRecord(CString strFileName)
{
	CTable* pTable = NULL;
	CFileFind afinder;
	CString strTitle, strLineData;
	char buff[128];
	FILE *fWrite = NULL;	

	//1. 현재 진행중인 step 첫번째 라인(Header) 추출 
	if(afinder.FindFile(strFileName))
	{
  		fWrite = fopen(strFileName,"r+");	
		if(fWrite)
		{
			ZeroMemory(buff, sizeof(buff));
			fread((char*)buff, sizeof(buff), 1, fWrite);
			fclose(fWrite);

			CString strBuff(buff);
			
			int nCnt = strBuff.Find('\n');
			if(nCnt >= 0)
			{
				strTitle = strBuff.Left(nCnt);				
			}
		}
	}

	//2. 현재 진행중인 step 마지막 라인 검색 
	if(afinder.FindFile(strFileName))
	{
  		fWrite=fopen(strFileName,"rt");	
		
		if(fWrite)
		{
			ZeroMemory(buff, sizeof(buff));
			fseek(fWrite, 0, SEEK_END);
			int n = ftell(fWrite);
			if(n > sizeof(buff))
			{
				fseek(fWrite, n-sizeof(buff), SEEK_SET);
			}
			else
			{
				fseek(fWrite, 0, SEEK_SET);
			}
			fread((char*)buff, sizeof(buff), 1, fWrite);
			fclose(fWrite);

			CString strBuff(buff);
			
			int nCnt = strBuff.ReverseFind('\n');
			if(nCnt >= 0)
			{
				//"xxx,xxx\nxxx,xxx\n" 인 경우 (제일 마지막이 '\n'일 경우 )
				strLineData = strBuff.Mid(nCnt+1);
				if(strLineData.IsEmpty())
				{
					strBuff.TrimRight("\n");
					//끝에서 두번째 \n을 찾음 
					nCnt = strBuff.ReverseFind('\n');
					if(nCnt >= 0)
					{
						strLineData = strBuff.Mid(nCnt+1);		
						if(strLineData.IsEmpty())
						{
							TRACE("Error\n");
						}
					}
				}
				else //xxx,xxx\nxxx,xxx\nxxx, 인 경우
				{
					CString strBuff1;
					strBuff1 = strBuff.Left(nCnt+1);		//마지막 \n 이후 Data 무시(마지막은 반듯이 \n으로 끝나야 함)
					
					nCnt = strBuff1.ReverseFind('\n');
					if(nCnt >= 0)
					{
						strLineData = strBuff1.Mid(nCnt+1);
						if(strLineData.IsEmpty())
						{
							strBuff1.TrimRight("\n");

							nCnt = strBuff1.ReverseFind('\n');
							if(nCnt >= 0)
							{
								strLineData = strBuff1.Mid(nCnt+1);		
								if(strLineData.IsEmpty())
								{
									TRACE("Error\n");
								}
							}
							else
							{
								strLineData = strBuff1;
							}
						}
					}

				}
			}
		}
	}

	if(strTitle == strLineData)
	{
		TRACE("%s에 Data 가 없습니다.\n", strFileName);
	}
//	TRACE("%s :: %s\n", strTitle,  strLineData);
	
	//인위적 Mapping 
	strTitle.Empty();
	strTitle += RS_COL_STEP_NO;
	strTitle += ",";
	strTitle += RS_COL_STATE;
	strTitle += ",";
	strTitle += RS_COL_TOT_TIME;
	strTitle += ",";
	strTitle += RS_COL_TIME;
	strTitle += ",";
	strTitle += RS_COL_VOLTAGE;
	strTitle += ",";
	strTitle += RS_COL_CURRENT;
	strTitle += ",";
	strTitle += RS_COL_CAPACITY;
	strTitle += ",";
	strTitle += RS_COL_WATT_HOUR;
	strTitle += ",";


	//가장 마지막 Line 추출 
	if(!strLineData.IsEmpty())
	{
		pTable = new CTable(strTitle,strLineData);
		return pTable;
	}

	return pTable;
}

//c:\\Test_Name\\M01C01[001] 형태
CString CChData::GetTestName()
{
	if(m_strPath.IsEmpty())	return "";

	int a = m_strPath.ReverseFind('\\');
	if(a < 1)	return "";

	CString strName(m_strPath.Left(a));

	a = strName.ReverseFind('\\');
	if(a < 1)	return "";

	return strName.Mid(a+1);
}


int CChData::GetModuleID()
{
	if(m_strPath.IsEmpty())	return -1;

	int a = m_strPath.ReverseFind('\\');
	if(a < 1)	return -1;

	CString strName(m_strPath.Mid(a+1));
	strName.MakeUpper();

	int indexS = strName.Find('M');
	int indexE = strName.Find('C');
	
	if(indexS >= indexE)	return -1;

	return atol(strName.Mid(indexS+1, (indexE-indexS)-1));
}

int CChData::GetChannelNo()
{
	if(m_strPath.IsEmpty())	return -1;

	int a = m_strPath.ReverseFind('\\');
	if(a < 1)	return -1;

	CString strName(m_strPath.Mid(a+1));
	strName.MakeUpper();

	int indexS = strName.Find('H');
	int indexE = strName.Find('[');

	//[]를 발견하지 못하였을 경우 
	if(indexE < 0)
	{
		return atol(strName.Mid(indexS+1));
	}
		
	if(indexS >= indexE)	return -1;

	return atol(strName.Mid(indexS+1, (indexE-indexS)-1));
}

//Tray 번호와 Cell 번호를 Update한다.
BOOL CChData::UpdateTestInfo(CString strTray, CString strCellSerial)
{
	if(m_strPath.IsEmpty())		return FALSE;

	CFileFind afinder;
	if(afinder.FindFile(m_strPath+"\\*."+PS_RESULT_FILE_NAME_EXT))
	{
		afinder.FindNextFile();
		CString fileName = afinder.GetFilePath();
		
		FILE *fp = fopen(fileName, "rb+");
		if(fp == NULL)		return FALSE; 
		
		PS_TEST_RESULT_FILE_HEADER *pHeader = new PS_TEST_RESULT_FILE_HEADER;
		fread(pHeader, sizeof(PS_TEST_RESULT_FILE_HEADER), 1, fp);
		if(pHeader->fileHeader.nFileID != PS_ADP_STEP_RESULT_FILE_ID)
		{
			fclose(fp);
			return FALSE;
		}

		sprintf(pHeader->testHeader.szTrayNo, "%s", strTray);
		sprintf(pHeader->testHeader.szSerial, "%s", strCellSerial);

		m_strTrayNo = strTray;
		m_strSerial = strCellSerial;

		fseek(fp, 0, SEEK_SET);
		fwrite(pHeader, sizeof(PS_TEST_RESULT_FILE_HEADER), 1, fp);
		fclose(fp);
		delete pHeader;
	}
	else
	{
		return FALSE;
	}
	afinder.Close();

	return TRUE;
}

//DCIR값을 Update한다.
BOOL CChData::UpdateDCIRData(LONG lStepNo, float fData)
{
	if(m_strPath.IsEmpty())		return FALSE;

	CFileFind afinder;
	if(afinder.FindFile(m_strPath+"\\*."+PS_RESULT_FILE_NAME_EXT))
	{
		afinder.FindNextFile();
		CString fileName = afinder.GetFilePath();
		
		FILE *fp = fopen(fileName, "rb+");
		if(fp == NULL)		return FALSE; 
		
		long lOffset = 0;
		PS_TEST_RESULT_FILE_HEADER *pHeader = new PS_TEST_RESULT_FILE_HEADER;
		lOffset = fread(pHeader, 1, sizeof(PS_TEST_RESULT_FILE_HEADER), fp);
		if(pHeader->fileHeader.nFileID != PS_ADP_STEP_RESULT_FILE_ID)
		{
			fclose(fp);
			return FALSE;
		}
		delete 	pHeader;


		// 한줄씩
		int nReadSize = 0;
		PS_STEP_END_RECORD *pRecord = new PS_STEP_END_RECORD;
		while((nReadSize = fread(pRecord, 1, sizeof(PS_STEP_END_RECORD), fp)) > 0)
		{
			//지정 Step의 Impedance 값을 Update한다.
			if(pRecord->chStepNo == lStepNo && pRecord->chStepType == PS_STEP_IMPEDANCE)
			{
				pRecord->fImpedance = fData;
				fseek(fp, lOffset, SEEK_SET);
				fwrite(pRecord, 1, sizeof(PS_STEP_END_RECORD), fp);
				break;
			}
			lOffset += nReadSize;
		}
		delete	pRecord;
		
		fclose(fp);
		return TRUE;
	}
	return FALSE;
}
