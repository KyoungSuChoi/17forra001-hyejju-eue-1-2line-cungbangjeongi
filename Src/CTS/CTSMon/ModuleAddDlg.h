#if !defined(AFX_MODULEADDDLG_H__AE89DB28_5B96_4E20_9416_E5EFA362F58C__INCLUDED_)
#define AFX_MODULEADDDLG_H__AE89DB28_5B96_4E20_9416_E5EFA362F58C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ModuleAddDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CModuleAddDlg dialog

class CModuleAddDlg : public CDialog
{
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();
	virtual ~CModuleAddDlg();

	// Construction
public:
	int		m_nType;
	CString	m_strName;
	BOOL	m_bAddDlg;
	int		m_nModulePerRack;
	UINT	m_nModuleTrayType;
	BOOL	m_bUseGroupSet;
	BOOL	m_bUseRackIndex;
	CString m_strGroupName;
	CString m_strModuleName;

//	CString ModuleName(int nModuleID, int nGroupIndex = 0);
	
	int GetModuleID();
	CModuleAddDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CModuleAddDlg)
	enum { IDD = IDD_MODULE_ADD_DLG };
	CComboBox	m_ctrlTrayType;
	CComboBox	m_ctrlModuleType;
	CComboBox	m_ctrlModuleSel;
	UINT	m_nModuleID;
	float	m_fVSpec;
	float	m_fISpec;
	UINT	m_nTrayCol;
	BOOL	m_bUseModeChange;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CModuleAddDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CModuleAddDlg)
	afx_msg void OnChangeModulenum();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeModuleSelCombo();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MODULEADDDLG_H__AE89DB28_5B96_4E20_9416_E5EFA362F58C__INCLUDED_)
