// GraphSetDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSGraphAnal.h"
#include "GraphSetDlg.h"

#include "CTSGraphAnalDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGraphSetDlg dialog


CGraphSetDlg::CGraphSetDlg(CCTSGraphAnalDoc *pDoc, CWnd* pParent /*=NULL*/)
	: CDialog(CGraphSetDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CGraphSetDlg)
	m_fltXAxisGrid = 0.0f;
	m_fltXAxisMax  = 0.0f;
	m_fltXAxisMin  = 0.0f;
	m_fltYAxisGrid = 0.0f;
	m_fltYAxisMax  = 0.0f;
	m_fltYAxisMin  = 0.0f;
	m_strTestName = _T("");
	m_bXGrid = FALSE;
	m_bYGrid = FALSE;
	m_bStairLineType = FALSE;
	//}}AFX_DATA_INIT
	m_iCurSelPlaneIndex = CB_ERR;

	m_pDoc = pDoc;
	ASSERT(m_pDoc);
	
	LanguageinitMonConfig();
}

CGraphSetDlg::~CGraphSetDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

void CGraphSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGraphSetDlg)
	DDX_Control(pDX, IDC_Y_ITEM_LIST, m_YItemList);
	DDX_Control(pDX, IDC_LINE_LIST, m_LineListBox);
	DDX_Control(pDX, IDC_PLANE_COMBO, m_PlaneCombo);
	DDX_Text(pDX, IDC_XAXIS_GRID_EDIT, m_fltXAxisGrid);
	DDX_Text(pDX, IDC_XAXIS_MAX_EDIT,  m_fltXAxisMax);
	DDX_Text(pDX, IDC_XAXIS_MIN_EDIT,  m_fltXAxisMin);
	DDX_Text(pDX, IDC_YAXIS_GRID_EDIT, m_fltYAxisGrid);
	DDX_Text(pDX, IDC_YAXIS_MAX_EDIT,  m_fltYAxisMax);
	DDX_Text(pDX, IDC_YAXIS_MIN_EDIT,  m_fltYAxisMin);
	DDX_Text(pDX, IDC_EDIT_TESTNAME, m_strTestName);
	DDX_Check(pDX, IDC_GRID_CHECK2, m_bXGrid);
	DDX_Check(pDX, IDC_GRID_CHECK, m_bYGrid);
	DDX_Check(pDX, IDC_CHECK_STAIR_TYPE, m_bStairLineType);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CGraphSetDlg, CDialog)
	//{{AFX_MSG_MAP(CGraphSetDlg)
	ON_CBN_SELCHANGE(IDC_PLANE_COMBO, OnSelchangePlaneCombo)
	ON_WM_DRAWITEM()
	ON_BN_CLICKED(IDC_XAXIS_FONT_BTN, OnXaxisFontBtn)
	ON_BN_CLICKED(IDC_YAXIS_FONT_BTN, OnYaxisFontBtn)
	ON_BN_CLICKED(IDC_APPLY, OnApply)
	ON_LBN_SELCHANGE(IDC_Y_ITEM_LIST, OnSelchangeYItemList)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGraphSetDlg message handlers

void CGraphSetDlg::AddPlaneIntoComboBox()
{
	CCTSGraphAnalDoc* pDoc = m_pDoc;//(CCTSGraphAnalDoc*)(GetParentFrame()->GetActiveDocument());
	//
	GetDlgItem(IDC_XAXIS_EDIT)->SetWindowText("");
	m_PlaneCombo.ResetContent();
	//
	POSITION pos = pDoc->m_Data.GetHeadPlanePos();
	BOOL bCheck = pos!=NULL;
	while(pos)
	{
		CPlane* pPlane = pDoc->m_Data.GetNextPlane(pos);
		CString str;
		str.Format("%s(%s)", pPlane->GetXAxis()->GetPropertyTitle(),
			                 pPlane->GetXAxis()->GetUnitNotation());
		GetDlgItem(IDC_XAXIS_EDIT)->SetWindowText(str);
		str.Format("%s(%s)", pPlane->GetYAxis()->GetPropertyTitle(),
							 pPlane->GetYAxis()->GetUnitNotation());
//		m_PlaneCombo.AddString(str);

		m_YItemList.AddString(str);
	}
	//
	if(bCheck)
	{
		m_PlaneCombo.SetCurSel(m_iCurSelPlaneIndex);
		m_YItemList.SetCurSel(m_iCurSelPlaneIndex);

		//OnSelchangePlaneCombo();
		OnSelchangeYItemList();
	}

	SetTestName(pDoc->m_Data.GetTestName());
}

void CGraphSetDlg::OnSelchangePlaneCombo() 
{
	// TODO: Add your control notification handler code here
/*	m_iCurSelPlaneIndex = m_PlaneCombo.GetCurSel();
	if(m_iCurSelPlaneIndex!=CB_ERR)
	{
		CCTSGraphAnalDoc* pDoc = m_pDoc;//(CCTSGraphAnalDoc*)(GetParentFrame()->GetActiveDocument());
		CPlane* pPlane     = pDoc->m_Data.GetPlaneAt(m_iCurSelPlaneIndex);
		//
		StandLinesInALine(pPlane);
		//
		m_fltXAxisMax  = pPlane->GetXAxis()->ConvertUnit(pPlane->GetXAxis()->GetMax());
		m_fltXAxisMin  = pPlane->GetXAxis()->ConvertUnit(pPlane->GetXAxis()->GetMin());
		m_fltXAxisGrid = pPlane->GetXAxis()->ConvertUnit(pPlane->GetXAxis()->GetGrid());
		m_fltYAxisMax  = pPlane->GetYAxis()->ConvertUnit(pPlane->GetYAxis()->GetMax());
		m_fltYAxisMin  = pPlane->GetYAxis()->ConvertUnit(pPlane->GetYAxis()->GetMin());
		m_fltYAxisGrid = pPlane->GetYAxis()->ConvertUnit(pPlane->GetYAxis()->GetGrid());
		m_bXGrid = pPlane->GetXAxis()->IsShowGrid();
		m_bYGrid = pPlane->GetYAxis()->IsShowGrid();
		UpdateData(FALSE);
		//
		Invalidate(FALSE);
	}	
*/
}

void CGraphSetDlg::StandLinesInALine(CPlane *pPlane)
{
	if(m_iCurSelPlaneIndex!=CB_ERR)
	{
		m_LineListBox.ResetContent();
		POSITION pos = pPlane->GetLineHead();
		while(pos)
		{
			CLine* pLine = pPlane->GetNextLine(pos);
			CString str;
			str.Format("%s,%d,%d,%d,%d,%d",
				pLine->GetName(),
				GetRValue(pLine->GetColor()),
				GetGValue(pLine->GetColor()),
				GetBValue(pLine->GetColor()),
				pLine->GetWidth(),
				pLine->GetType());
			m_LineListBox.AddString(str);
		}
	}
}

void CGraphSetDlg::SetTestName(CString strTestName)
{
	strTestName.MakeUpper();
	m_strTestName = strTestName;
	UpdateData(FALSE);
}

void CGraphSetDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	// TODO: Add your message handler code here and/or call default
	if(m_iCurSelPlaneIndex!=CB_ERR)
	{
		CCTSGraphAnalDoc* pDoc = m_pDoc;//(CCTSGraphAnalDoc*)(GetParentFrame()->GetActiveDocument());
		CPlane* pPlane     = pDoc->m_Data.GetPlaneAt(m_iCurSelPlaneIndex);
		if(lpDrawItemStruct->CtlID==IDC_XAXIS_FONT_BTN)
		{
			CDC* pDC=CDC::FromHandle(lpDrawItemStruct->hDC);
			CFont aFont, *oldFont;
			aFont.CreateFontIndirect(pPlane->GetXAxis()->GetFont());
			oldFont=pDC->SelectObject(&aFont);
			pDC->Rectangle(&(lpDrawItemStruct->rcItem));
			COLORREF oldTextColor = pDC->SetTextColor(pPlane->GetXAxis()->GetLabelColor());
			pDC->DrawText("����ABab",&(lpDrawItemStruct->rcItem),DT_SINGLELINE|DT_CENTER|DT_VCENTER);
			pDC->SelectObject(oldFont);
			aFont.DeleteObject();
			pDC->SetTextColor(oldTextColor);
		}
		if(lpDrawItemStruct->CtlID==IDC_YAXIS_FONT_BTN)
		{
			CDC* pDC=CDC::FromHandle(lpDrawItemStruct->hDC);
			CFont aFont, *oldFont;
			aFont.CreateFontIndirect(pPlane->GetYAxis()->GetFont());
			oldFont=pDC->SelectObject(&aFont);
			pDC->Rectangle(&(lpDrawItemStruct->rcItem));
			COLORREF oldTextColor = pDC->SetTextColor(pPlane->GetYAxis()->GetLabelColor());
			pDC->DrawText("����ABab",&(lpDrawItemStruct->rcItem),DT_SINGLELINE|DT_CENTER|DT_VCENTER);
			pDC->SelectObject(oldFont);
			aFont.DeleteObject();
			pDC->SetTextColor(oldTextColor);
		}
	}	
	CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CGraphSetDlg::OnXaxisFontBtn() 
{
	// TODO: Add your control notification handler code here
	if(m_iCurSelPlaneIndex!=CB_ERR)
	{
		CCTSGraphAnalDoc* pDoc = m_pDoc;//(CCTSGraphAnalDoc*)(GetParentFrame()->GetActiveDocument());
		CPlane* pPlane     = pDoc->m_Data.GetPlaneAt(m_iCurSelPlaneIndex);
		LOGFONT afont = *(pPlane->GetXAxis()->GetFont());
		CFontDialog aDlg(&afont);
		aDlg.m_cf.rgbColors = pPlane->GetXAxis()->GetLabelColor();
		if(aDlg.DoModal()==IDOK)
		{
			aDlg.GetCurrentFont(&afont);
			POSITION pos = pDoc->m_Data.GetHeadPlanePos();
			while(pos)
			{
				CPlane* pPlane = pDoc->m_Data.GetNextPlane(pos);
				*(pPlane->GetXAxis()->GetFont()) = afont;
				pPlane->GetXAxis()->SetLabelColor(aDlg.GetColor());
			}
			//
			pDoc->UpdateAllViews(NULL);
			Invalidate(TRUE);
		}
	}		
}

void CGraphSetDlg::OnYaxisFontBtn() 
{
	// TODO: Add your control notification handler code here
	if(m_iCurSelPlaneIndex!=CB_ERR)
	{
		CCTSGraphAnalDoc* pDoc = m_pDoc;//(CCTSGraphAnalDoc*)(GetParentFrame()->GetActiveDocument());
		CPlane* pPlane     = pDoc->m_Data.GetPlaneAt(m_iCurSelPlaneIndex);
		LOGFONT afont = *(pPlane->GetYAxis()->GetFont());
		CFontDialog aDlg(&afont);
		aDlg.m_cf.rgbColors = pPlane->GetYAxis()->GetLabelColor();
		if(aDlg.DoModal()==IDOK)
		{
			aDlg.GetCurrentFont(&afont);
			*(pPlane->GetYAxis()->GetFont()) = afont;
			pPlane->GetYAxis()->SetLabelColor(aDlg.GetColor());

			//
			pDoc->UpdateAllViews(NULL);
			Invalidate(TRUE);
		}
	}		
}

BOOL CGraphSetDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	// TODO: Add extra initialization here
	m_iCurSelPlaneIndex = m_pDoc->m_iCurSelPlaneIndex;

	AddPlaneIntoComboBox();

	m_bStairLineType = AfxGetApp()->GetProfileInt(REG_CONFIG_SECTION, "StairLine", FALSE);
	
	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CGraphSetDlg::OnOK() 
{
	// TODO: Add extra validation here
	if(UpdateParam() == FALSE)
		return;

	CDialog::OnOK();
}

BOOL CGraphSetDlg::UpdateParam()
{
	UpdateData();

	CCTSGraphAnalDoc* pDoc = m_pDoc;//(CCTSGraphAnalDoc*)(GetParentFrame()->GetActiveDocument());

	m_pDoc->m_bStairLineType = m_bStairLineType;
	AfxGetApp()->WriteProfileInt(REG_CONFIG_SECTION, "StairLine", m_bStairLineType);
	
	m_pDoc->m_Data.SetTestName(m_strTestName);
	m_pDoc->m_iCurSelPlaneIndex = m_iCurSelPlaneIndex;

	if(m_fltXAxisGrid <= 0.0f)
	{
		AfxMessageBox(TEXT_LANG[0]); //000678
		GetDlgItem(IDC_XAXIS_GRID_EDIT)->SetFocus();
		((CEdit *)GetDlgItem(IDC_XAXIS_GRID_EDIT))->SetSel(0, -1);
		return FALSE;
	}
	if(m_fltYAxisGrid <= 0.0f)
	{
		AfxMessageBox(TEXT_LANG[0]); //000678
		GetDlgItem(IDC_YAXIS_GRID_EDIT)->SetFocus();
		((CEdit *)GetDlgItem(IDC_YAXIS_GRID_EDIT))->SetSel(0, -1);
		return FALSE;
	}

	if(m_fltXAxisMin >= m_fltXAxisMax)
	{
		AfxMessageBox(TEXT_LANG[1]); //000679
		GetDlgItem(IDC_XAXIS_MAX_EDIT)->SetFocus();
		((CEdit *)GetDlgItem(IDC_XAXIS_MAX_EDIT))->SetSel(0, -1);
		return FALSE;
	}

	if(m_fltYAxisMin >= m_fltYAxisMax)
	{
		AfxMessageBox(TEXT_LANG[2]); //000680
		GetDlgItem(IDC_YAXIS_MAX_EDIT)->SetFocus();
		((CEdit *)GetDlgItem(IDC_YAXIS_MAX_EDIT))->SetSel(0, -1);
		return FALSE;
	}

	
	if(m_iCurSelPlaneIndex!=CB_ERR)
	{
		POSITION pos = pDoc->m_Data.GetHeadPlanePos();
		while(pos)
		{
			CPlane* pPlane     = pDoc->m_Data.GetNextPlane(pos);
			pPlane->GetXAxis()->SetMinMax( pPlane->GetXAxis()->ReverseUnit(m_fltXAxisMin),
							               pPlane->GetXAxis()->ReverseUnit(m_fltXAxisMax),
										   pPlane->GetXAxis()->ReverseUnit(m_fltXAxisGrid));
			pPlane->GetXAxis()->SetShowGrid(m_bXGrid);
		}
	}
		
	if(m_iCurSelPlaneIndex!=CB_ERR)
	{
		CPlane* pPlane     = pDoc->m_Data.GetPlaneAt(m_iCurSelPlaneIndex);
		pPlane->GetYAxis()->SetMinMax( pPlane->GetYAxis()->ReverseUnit(m_fltYAxisMin),
			                           pPlane->GetYAxis()->ReverseUnit(m_fltYAxisMax),
									   pPlane->GetYAxis()->ReverseUnit(m_fltYAxisGrid));
		
		pPlane->GetYAxis()->SetShowGrid(m_bYGrid);
	}
	
	pDoc->UpdateAllViews(NULL);	

	return TRUE;
}

void CGraphSetDlg::OnApply() 
{
	// TODO: Add your control notification handler code here
	UpdateParam();	
}

void CGraphSetDlg::OnSelchangeYItemList() 
{
	// TODO: Add your control notification handler code here
	m_iCurSelPlaneIndex = m_YItemList.GetCurSel();
	if(m_iCurSelPlaneIndex!=CB_ERR)
	{
		CCTSGraphAnalDoc* pDoc = m_pDoc;//(CCTSGraphAnalDoc*)(GetParentFrame()->GetActiveDocument());
		CPlane* pPlane     = pDoc->m_Data.GetPlaneAt(m_iCurSelPlaneIndex);
		//
		StandLinesInALine(pPlane);
		//
		m_fltXAxisMax  = pPlane->GetXAxis()->ConvertUnit(pPlane->GetXAxis()->GetMax());
		m_fltXAxisMin  = pPlane->GetXAxis()->ConvertUnit(pPlane->GetXAxis()->GetMin());
		m_fltXAxisGrid = pPlane->GetXAxis()->ConvertUnit(pPlane->GetXAxis()->GetGrid());
		m_fltYAxisMax  = pPlane->GetYAxis()->ConvertUnit(pPlane->GetYAxis()->GetMax());
		m_fltYAxisMin  = pPlane->GetYAxis()->ConvertUnit(pPlane->GetYAxis()->GetMin());
		m_fltYAxisGrid = pPlane->GetYAxis()->ConvertUnit(pPlane->GetYAxis()->GetGrid());
		m_bXGrid = pPlane->GetXAxis()->IsShowGrid();
		m_bYGrid = pPlane->GetYAxis()->IsShowGrid();
		UpdateData(FALSE);
		//
		Invalidate(FALSE);
	}		
}

bool CGraphSetDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CGraphSetDlg"), _T("TEXT_CGraphSetDlg_CNT"), _T("TEXT_CGraphSetDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt];

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CGraphSetDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CGraphSetDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Languge error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}