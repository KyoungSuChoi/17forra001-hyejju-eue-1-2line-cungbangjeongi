/////////////////////////////////////////////////////////////////////
// Class Creator Version 2.0.000 Copyrigth (C) Poul A. Costinsky 1994
///////////////////////////////////////////////////////////////////
// Implementation File RawSocketServerWorker.cpp
// class CWizRawSocketServerWorker
//
// 16/07/1996 17:53                             Author: Poul
///////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "RawSocketServerWorker.h"
#include "SBCCriticalSection.h"
#include "SBCSyncParent.h"
#include "SBCCircularQueue.h"

#ifdef _DEBUG

//#include "Mmsystem.h"

#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#define new DEBUG_NEW
#endif

/*
//Message Direction
#define _HOST_TO_MODULE		0x00
#define _MODULE_TO_HOST		0x01

//#define CheckMsgDirection(x)	((x) == _MODULE_TO_HOST ? TRUE : FALSE)	
*/

extern CSBCCircularQueue <SBCQueue_DATA> g_Que;
extern EP_STR_MODULE_DATA	*g_pstModule;
extern BOOL	InitGroupData (int nModuleIndex);	//Module Data
extern BOOL WriteLog( char *szLog );
extern int	g_nChannelIndexType;	

//Channel Mapping FTN
extern int g_channelMap[EP_MAX_CH_PER_MD];

void ModuleClosed (int nModuleIndex , HWND hMsgWnd);
void ModuleConnected(int nModuleIndex, HWND hMsgWnd);
BOOL ParsingCommand(CWizReadWriteSocket *pSock, int nGroupIndex, LPVOID lpMsgHeader, LPVOID lpReadBuff, HWND hMsgWnd);
void MakeMsgHeader (LPEP_MSG_HEADER pHeader, int nBoardIndex, int nChannelIndex, int nCommand);
int ConnectionSequence(CWizReadWriteSocket *pSocket, HANDLE *hShutDownEvent);
int CheckModuleIDValidate(LPEP_MD_SYSTEM_DATA pSysData);
int GetMappingCh(int nSeqIndex, WORD wSystemType, int nMemberIndex);
USHORT fnCRC16_ccitt(const unsigned char *buf, int len);

static const USHORT crc16tab[256]= {
	0x0000,0x1021,0x2042,0x3063,0x4084,0x50a5,0x60c6,0x70e7,
	0x8108,0x9129,0xa14a,0xb16b,0xc18c,0xd1ad,0xe1ce,0xf1ef,
	0x1231,0x0210,0x3273,0x2252,0x52b5,0x4294,0x72f7,0x62d6,
	0x9339,0x8318,0xb37b,0xa35a,0xd3bd,0xc39c,0xf3ff,0xe3de,
	0x2462,0x3443,0x0420,0x1401,0x64e6,0x74c7,0x44a4,0x5485,
	0xa56a,0xb54b,0x8528,0x9509,0xe5ee,0xf5cf,0xc5ac,0xd58d,
	0x3653,0x2672,0x1611,0x0630,0x76d7,0x66f6,0x5695,0x46b4,
	0xb75b,0xa77a,0x9719,0x8738,0xf7df,0xe7fe,0xd79d,0xc7bc,
	0x48c4,0x58e5,0x6886,0x78a7,0x0840,0x1861,0x2802,0x3823,
	0xc9cc,0xd9ed,0xe98e,0xf9af,0x8948,0x9969,0xa90a,0xb92b,
	0x5af5,0x4ad4,0x7ab7,0x6a96,0x1a71,0x0a50,0x3a33,0x2a12,
	0xdbfd,0xcbdc,0xfbbf,0xeb9e,0x9b79,0x8b58,0xbb3b,0xab1a,
	0x6ca6,0x7c87,0x4ce4,0x5cc5,0x2c22,0x3c03,0x0c60,0x1c41,
	0xedae,0xfd8f,0xcdec,0xddcd,0xad2a,0xbd0b,0x8d68,0x9d49,
	0x7e97,0x6eb6,0x5ed5,0x4ef4,0x3e13,0x2e32,0x1e51,0x0e70,
	0xff9f,0xefbe,0xdfdd,0xcffc,0xbf1b,0xaf3a,0x9f59,0x8f78,
	0x9188,0x81a9,0xb1ca,0xa1eb,0xd10c,0xc12d,0xf14e,0xe16f,
	0x1080,0x00a1,0x30c2,0x20e3,0x5004,0x4025,0x7046,0x6067,
	0x83b9,0x9398,0xa3fb,0xb3da,0xc33d,0xd31c,0xe37f,0xf35e,
	0x02b1,0x1290,0x22f3,0x32d2,0x4235,0x5214,0x6277,0x7256,
	0xb5ea,0xa5cb,0x95a8,0x8589,0xf56e,0xe54f,0xd52c,0xc50d,
	0x34e2,0x24c3,0x14a0,0x0481,0x7466,0x6447,0x5424,0x4405,
	0xa7db,0xb7fa,0x8799,0x97b8,0xe75f,0xf77e,0xc71d,0xd73c,
	0x26d3,0x36f2,0x0691,0x16b0,0x6657,0x7676,0x4615,0x5634,
	0xd94c,0xc96d,0xf90e,0xe92f,0x99c8,0x89e9,0xb98a,0xa9ab,
	0x5844,0x4865,0x7806,0x6827,0x18c0,0x08e1,0x3882,0x28a3,
	0xcb7d,0xdb5c,0xeb3f,0xfb1e,0x8bf9,0x9bd8,0xabbb,0xbb9a,
	0x4a75,0x5a54,0x6a37,0x7a16,0x0af1,0x1ad0,0x2ab3,0x3a92,
	0xfd2e,0xed0f,0xdd6c,0xcd4d,0xbdaa,0xad8b,0x9de8,0x8dc9,
	0x7c26,0x6c07,0x5c64,0x4c45,0x3ca2,0x2c83,0x1ce0,0x0cc1,
	0xef1f,0xff3e,0xcf5d,0xdf7c,0xaf9b,0xbfba,0x8fd9,0x9ff8,
	0x6e17,0x7e36,0x4e55,0x5e74,0x2e93,0x3eb2,0x0ed1,0x1ef0
};

//BYTE GetNextPacketID();

///////////////////////////////////////////////////////////////////
//*****************************************************************
inline void ThrowIfNull(void* p)
{
	if (p == NULL)	AfxThrowMemoryException();
}

///////////////////////////////////////////////////////////////////
// class CWizRawSocketListener
///////////////////////////////////////////////////////////////////
//**********************************************************//
// Default Constructor										//
CWizRawSocketListener::CWizRawSocketListener(int nPort)     //
	: m_pListenSocket (NULL),								//
	  m_nPort         (nPort)								//
{															//
//	m_pDoc = NULL;	
	m_hMessageWnd = NULL;									//
}															//
//**********************************************************//
// Destructor												//
CWizRawSocketListener::~CWizRawSocketListener()				//
{															//
	if (m_pListenSocket != NULL)							//
	{														//
		TRACE("You Must call EPCloseFormServer() before your application is closed\n");
		ASSERT(0);											//
		delete m_pListenSocket;								//
	}														//
}															//
//**********************************************************//

void CWizRawSocketListener::SetMsgWnd(HWND hMessageWnd)
{
	m_hMessageWnd = hMessageWnd;
}

// Method called from dispath thread.
void CWizRawSocketListener::Prepare ()
{
	// Create listening socket
	if (m_pListenSocket != NULL)
	{
		ASSERT(0);
		delete m_pListenSocket;
	}
	m_pListenSocket = new CWizSyncSocket(m_nPort);
	ThrowIfNull (m_pListenSocket);

#ifdef _DEBUG
	TCHAR buff[100];
	unsigned int nP;
	VERIFY(m_pListenSocket->GetHostName (buff,100,nP));
	TRACE(_T("Control Server Listening at %s:%d\n"), buff, nP);
#endif
}

//*****************************************************************
// Method called from dispath thread.
void CWizRawSocketListener::CleanUp()
{
	// close and destroy listening socket
	delete m_pListenSocket;
	m_pListenSocket = NULL;
}

//*****************************************************************
// Method called from dispath thread.
BOOL CWizRawSocketListener::WaitForData (HANDLE hShutDownEvent)
{
	HANDLE *lpHandles;
	lpHandles = new HANDLE[2];

	WSAEVENT myObjectEvent = WSACreateEvent();
	lpHandles[0]= myObjectEvent;
	lpHandles[1]= hShutDownEvent;

	WSAEventSelect( m_pListenSocket->H(), myObjectEvent, FD_ACCEPT);
	
	DWORD rtn;
	SOCKET h;

	while (1)
	{
		rtn = ::WaitForMultipleObjects(2, lpHandles, FALSE,   INFINITE);
		if( (rtn - WAIT_OBJECT_0) == 0 )
			 h = m_pListenSocket->Accept();
		else 
		{
			WSACloseEvent(myObjectEvent);
			delete [] lpHandles;
			return FALSE;
		}

		// Get accepted socket.	 If it's connected client, go to serve it.
		if (h != INVALID_SOCKET)
		{
//			WriteLog("A socket accepted");
			m_hAcceptedSocket = h;
			WSACloseEvent(myObjectEvent);
			delete [] lpHandles;
			return TRUE;
		}
	} // while 1
	return TRUE;
}

//*****************************************************************
// Method called from dispath thread.
// return TRUE : Wait clinet connection
// return FALSE:  Server Close
//****************************************************************
BOOL CWizRawSocketListener::TreatData   (HANDLE hShutDownEvent, HANDLE hDataTakenEvent)
{		
	char	msg[512];								//Display Message Buffer
//	UINT	nPortNo;								//Receive Port No
	int		msgReadState  = _MSG_ST_CLEAR;			//Read Done Flag
	int		reqSize = 0, offset = 0, nRtn =0;		//Data Point Indicator
	
	size_t msgHeadersize = SizeofHeader();			//Message Header Size
	LPEP_MSG_HEADER	 lpMsgHeader;					//Message Header	
	lpMsgHeader = new EP_MSG_HEADER;
	ASSERT(lpMsgHeader);

	LPVOID lpReadBuff = NULL;						//Body read Buffer
	
	HANDLE 	*lpHandles;								//Handle
	lpHandles = new HANDLE[3];
	ASSERT(lpHandles);

////////////////////////////////////////////////////////////////
	INT		nModuleID = 0, nModuleIndex =0;			//Return Value, Module ID

//	LPVOID lpEndDataBuff = NULL;
	int nEndDataSize;
//	EP_SYSTEM_PARAM *pParam = NULL;
//	long waitTime;
	long nWaitCount = 0;
	bool bReading = false;							//Read process 처리중
	CString strLog = _T("");
	CString strTemp = _T("");
	unsigned char		szTempRxBuffer[26800];
	int i = 0;
	
	// Create client side socket to communicate with client.
	CWizReadWriteSocket clientSide (m_hAcceptedSocket);
	// Signal dispather to continue waiting.
	::SetEvent(hDataTakenEvent);

	WSAEVENT myObjectEvent = WSACreateEvent();		//Event
	lpHandles[0] = myObjectEvent;					//Read 
	lpHandles[1] = hShutDownEvent;					//Server Side Shut down
	
	WSAEventSelect(clientSide.H(), myObjectEvent, FD_READ|FD_CLOSE);		//select Read and close event
	LPWSANETWORKEVENTS lpEvents = new WSANETWORKEVENTS;
	ASSERT(lpEvents);
	
	//Start Connection Sequence
	if((nModuleIndex = ConnectionSequence(&clientSide, lpHandles)) < 0)
	{
		ZeroMemory( msg, sizeof(msg));
		sprintf(msg, "Connection error. (Code %d)", nModuleIndex);
		goto _CLOSE_MODULE;
	}

	lpHandles[2] = g_pstModule[nModuleIndex].m_hWriteEvent;
	nModuleID = g_pstModule[nModuleIndex].sysData.nModuleID;
	//----------------Module Connection Complited-----------------------------//

	ZeroMemory( msg, sizeof(msg));
	sprintf(msg, "Module %d connected from %s", nModuleID, g_pstModule[nModuleIndex].sysParam.szIPAddr);
	WriteLog(msg);
	
	InitGroupData (nModuleIndex);					//Make Group Data Structure

	ModuleConnected(nModuleIndex, m_hMessageWnd);
////////////////////////////////////////////////////////////////////////////////
/*	pParam = EPGetSysParam(nModuleID);
	ASSERT(pParam);
	waitTime = pParam->nAutoReportInterval;
	if(waitTime <= 0)
	{
		waitTime = INFINITE;
	}
	else
	{
		waitTime += _EP_MON_DATA_TIMEOUT;
	}
*/	//-----------------start Socket Read/Write--------------------------------//
	int idx;
	while(1)
	{
		Sleep(1);
		nRtn = ::WaitForMultipleObjects(3, lpHandles, FALSE, EP_HEART_BEAT_TIMEOUT);		//Wait for Read or Write Event
		if (nRtn == WAIT_FAILED)
		{
			ZeroMemory( msg, sizeof(msg));
			sprintf(msg, "Module %d Message Read Failed %d, Disconnect Module %d", nModuleID, GetLastError(), nModuleID);
			goto _CLOSE_MODULE;
		}
		else if (nRtn == WAIT_TIMEOUT)
		{
			nWaitCount++;
			ZeroMemory( msg, sizeof(msg));
			sprintf(msg, "Module %d Data Read Time Out, Count %d", nModuleID, nWaitCount);
			
			if(nWaitCount > EP_MAX_WAIT_COUNT)
			{
				ZeroMemory( msg, sizeof(msg));
				sprintf(msg, "Module %d Data Read Time Out. Disconnect Module %d", nModuleID, nModuleID);
				nWaitCount = 0;
				goto _CLOSE_MODULE;
			}
		}
		else if (nRtn >= WAIT_ABANDONED_0 && nRtn < WAIT_ABANDONED_0 + 3)
		{
			idx = nRtn - WAIT_ABANDONED_0;
			ResetEvent(lpHandles[idx]);
			continue;
			// 	// something wrong event
			// 	sprintf(msg, "Module %d something error event detected %d. Disconnect Module %d", nModuleID, nRtn, nModuleID);
			// 	goto _CLOSE_MODULE;
		}
		else
		{
			idx = nRtn - WAIT_OBJECT_0;
			//작업 처리

			switch(nRtn)
			{
			case WAIT_OBJECT_0:		// Client socket event FD_READ
				if(SOCKET_ERROR == ::WSAEnumNetworkEvents(clientSide.H(), myObjectEvent, lpEvents))		//Get Client side Socket Event
				{ 
					ZeroMemory( msg, sizeof(msg));
					sprintf(msg, "Module %d Socket Error. Code::%d", nModuleID, WSAGetLastError());
					goto _CLOSE_MODULE;
				}
			
				//---------------Data Read Event ---------------------------//
				if (lpEvents->lNetworkEvents & FD_READ)		//Read Event
				{
					nWaitCount = 0;		// Reset Wait Count;						
					
					if( bReading == false )
					{
						msgReadState = _MSG_ST_CLEAR;
						bReading = true;
						
						while( bReading )
						{
							//--------------------Message Head Read -----------------//
							if( msgReadState & (_MSG_ST_CLEAR | _MSG_ST_HEADER_REMAIN))				//Read Message Header
							{
								if(msgReadState & _MSG_ST_CLEAR)
								{
									reqSize = msgHeadersize;		
								}

								offset = msgHeadersize - reqSize;

								nRtn = clientSide.Read((char *)lpMsgHeader+offset, reqSize);		//Read Message Header

								if( nRtn != reqSize )
								{	
									if( nRtn < 1 )
									{
										msgReadState = _MSG_ST_CLEAR;
										bReading = false;							

										// ZeroMemory( msg, sizeof(msg));
										// sprintf(msg, "Module %d Message Header Read Fail. Code::%d\n", nModuleID, WSAGetLastError());
										// WriteLog(msg);
									}									
									else
									{	
										reqSize -= nRtn;
										msgReadState = _MSG_ST_HEADER_REMAIN;
									}						
								}
								else
								{
									if(lpMsgHeader->nLength <= msgHeadersize )	//Incorrect Message if reqSize <  msgHeadersize
									{
										msgReadState = _MSG_ST_ERROR;
										bReading = false;							
									}
									else
									{
										msgReadState = _MSG_ST_HEAD_READED;
									}						
								}
							} 
							
							//--------------------Message Body Read -----------------////Read Body And Command Parsing	
							if (msgReadState & ( _MSG_ST_HEAD_READED | _MSG_ST_REMAIN ))
							{
								if( msgReadState & _MSG_ST_HEAD_READED )
								{
									reqSize = lpMsgHeader->nLength - msgHeadersize;		
									if(lpReadBuff)
									{
										delete[] lpReadBuff;
										lpReadBuff = NULL;
									}

									//too long packet 
									if(reqSize > _EP_MAX_PACKET_LENGTH || reqSize < 0 )
									{
										msgReadState = _MSG_ST_ERROR;
										ZeroMemory( msg, sizeof(msg));
										sprintf(msg, "Module %d Message Packet Length Error. Length::%d\n", nModuleID, reqSize);
										WriteLog(msg);
										bReading = false;
										goto _CLOSE_MODULE;
									}
									else
									{
										lpReadBuff = new char[reqSize];
										ASSERT(lpReadBuff);
									}								
								}
								
								offset = (lpMsgHeader->nLength - msgHeadersize) - reqSize;						
								nRtn = clientSide.Read((char *)lpReadBuff+offset, reqSize);
								if (nRtn != reqSize) 
								{
									if(nRtn == SOCKET_ERROR)	//Socket Error
									{
										msgReadState = _MSG_ST_ERROR;
										bReading = false;
										ZeroMemory( msg, sizeof(msg));
										sprintf(msg, "Module %d message body read fail. Code %d\n", nModuleID, WSAGetLastError());
										WriteLog(msg);
										break;
									}
									else
									{
										msgReadState = _MSG_ST_REMAIN;
									}

									reqSize -= nRtn;
								}
								else
								{
									msgReadState = _MSG_ST_CLEAR;
								}

								/* 로그 정보
								ZeroMemory(szTempRxBuffer, sizeof(szTempRxBuffer));
								memcpy(szTempRxBuffer, (byte*)lpReadBuff, lpMsgHeader->nLength - msgHeadersize);

								strLog.Format("[RECV %d-%d] ", nModuleIndex, lpMsgHeader->nLength);								

								strLog += strTemp;

								for(i=0; i<lpMsgHeader->nLength - SizeofHeader(); i++)
								{
									strTemp.Format("%02x ", szTempRxBuffer[i]);
									strLog += strTemp;
								}

								WriteLog((LPSTR)(LPCTSTR)strLog);
								*/
							}							

							///----------------------Parsing Command---------------------///
							if(	msgReadState & _MSG_ST_CLEAR)
							{
								USHORT crcFull = 0xFFFF;
								byte crcHigh = 0xFF, crcLow = 0xFF;

								int nHeadSize = SizeofHeader();

								ZeroMemory(g_pstModule[nModuleIndex].szTempRxBuffer, sizeof(g_pstModule[nModuleIndex].szTempRxBuffer));
								memcpy(g_pstModule[nModuleIndex].szTempRxBuffer, lpMsgHeader, SizeofHeader());
								memcpy(&g_pstModule[nModuleIndex].szTempRxBuffer[nHeadSize], (byte*)lpReadBuff, lpMsgHeader->nLength - msgHeadersize);

								// crcFull = fnCRC16_ccitt( crcCheckRecvBuf, lpMsgHeader->nLength + SizeofHeader() -2 );
								crcFull = fnCRC16_ccitt( g_pstModule[nModuleIndex].szTempRxBuffer, lpMsgHeader->nLength - 2 );

								crcHigh = (byte)((crcFull>>8)&0xFF);	// CRC[1]
								crcLow = (byte)(crcFull&0xFF);			// CRC[0]

								if( crcLow != (byte)g_pstModule[nModuleIndex].szTempRxBuffer[lpMsgHeader->nLength - 2] 
									|| crcHigh != (byte)g_pstModule[nModuleIndex].szTempRxBuffer[lpMsgHeader->nLength - 1] )
								{
									msgReadState = _MSG_ST_ERROR;
									bReading = false;
									ZeroMemory( msg, sizeof(msg));
									sprintf(msg, "Module %d message body CRC16 Error\n", nModuleID );
									WriteLog(msg);
								}
								else
								{
									if( lpMsgHeader->nCommand == (UINT)EP_CMD_RESPONSE )
									{		
										if((lpMsgHeader->nLength - SizeofHeader() - 2) == sizeof(EP_RESPONSE)
											&& lpReadBuff != NULL)
										{
											memcpy(&g_pstModule[nModuleIndex].CommandAck, lpReadBuff, sizeof(EP_RESPONSE));
											ZeroMemory( msg, sizeof(msg));	
											sprintf(msg, "[RECV] Response Cmd 0x%08x From Module = %d, Return Code %d (%d Bytes)", 
												g_pstModule[nModuleIndex].CommandAck.nCmd, g_pstModule[nModuleIndex].sysParam.nModuleID,  
												g_pstModule[nModuleIndex].CommandAck.nCode, lpMsgHeader->nLength );
										}
										else
										{
											g_pstModule[nModuleIndex].CommandAck.nCode = EP_SIZE_MISMATCH;
											ZeroMemory( msg, sizeof(msg));
											sprintf(msg, "[RECV] Module = %d Group = %d, Response Size Error %d (%d Bytes)", 
												g_pstModule[nModuleIndex].sysParam.nModuleID, lpMsgHeader->wGroupNum, lpMsgHeader->nLength, lpMsgHeader->nLength);
										}	

										WriteLog(msg);
										::SetEvent(g_pstModule[nModuleIndex].m_hReadEvent);
									}
									else
									{
										ParsingCommand(&clientSide, nModuleIndex, lpMsgHeader, lpReadBuff, m_hMessageWnd);	//Parsing Recevied Command
									}

									reqSize = msgHeadersize;									

									nRtn = clientSide.Read((char *)lpMsgHeader, reqSize);		//Read Message Header

									offset = reqSize - nRtn;

									if( nRtn != reqSize )
									{	
										if( nRtn < 1 )
										{
											msgReadState = _MSG_ST_CLEAR;
											bReading = false;																				
											
											// ZeroMemory( msg, sizeof(msg));
											// sprintf(msg, "Module %d Message Header Read Fail. Code::%d\n", nModuleID, WSAGetLastError());
											// WriteLog(msg);
										}
										else
										{	
											reqSize -= nRtn;

											msgReadState = _MSG_ST_HEADER_REMAIN;
										}
									}
									else
									{
										if(lpMsgHeader->nLength <= msgHeadersize )	//Incorrect Message if reqSize <  msgHeadersize
										{
											bReading = false;
										}
										else
										{
											// 1. Read 이벤트 처리중 소켓으로 데이터들이 들어옴
											// ZeroMemory( msg, sizeof(msg));
											// sprintf(msg, "[Stage %d] ====> Linked complete header data was exit...", nModuleID );
											// WriteLog(msg);
											
											msgReadState = _MSG_ST_HEAD_READED;
										}						
									}
								}	
							}

							Sleep(1);
						}					
					}
					else
					{
						ZeroMemory( msg, sizeof(msg));
						sprintf(msg, "[Stage %d] ====> recv read event in reading process", nModuleID );
						WriteLog(msg);
					}
				}
				else if(lpEvents->lNetworkEvents == FD_CLOSE)		//Client Disconnected
				{
					sprintf(msg, "Disconnect event detected from Module %d.(Code %d)", nModuleID, lpEvents->lNetworkEvents);
					goto _CLOSE_MODULE;
				}
				else //else of if (lpEvents->lNetworkEvents & FD_READ) is Not Read Event
				{
					if (lpEvents->lNetworkEvents != 0) 
					{
						sprintf(msg, "Module %d UnKnown event %d detected. Disconnect Module %d", nModuleID, lpEvents->lNetworkEvents, nModuleID);
						goto _CLOSE_MODULE;
					}
				}
			break;
			
			case WAIT_OBJECT_0 + 1: // Server side shutdown event
				{
					sprintf(msg, "Server shutdown event event detected. Disconnect Module %d", nModuleID);
					WriteLog(msg);
										
					clientSide.Close();
					if(lpReadBuff)
					{
						delete [] lpReadBuff;			lpReadBuff = NULL;
					}
					
					delete lpMsgHeader;
					delete [] lpHandles;
					delete lpEvents;
					return TRUE;			
				}
			
			case WAIT_OBJECT_0 + 2:	// client side write event
				{
	//				TRACE("Here\n");
	//				m_TxBuffCritic.Lock();	
					ASSERT(g_pstModule[nModuleIndex].nTxLength < _EP_TX_BUF_LEN);
					nRtn = 0;
					
					// 1. CRC16 계산 부분을 추가
					USHORT crcFull = 0xFFFF;
					byte crcHigh = 0xFF, crcLow = 0xFF;
					
					int nDataSize = 0;
					
					ZeroMemory(g_pstModule[nModuleIndex].szTempTxBuffer, sizeof(g_pstModule[nModuleIndex].szTempTxBuffer));
					memcpy(g_pstModule[nModuleIndex].szTempTxBuffer, g_pstModule[nModuleIndex].szTxBuffer, g_pstModule[nModuleIndex].nTxLength);
					nDataSize = g_pstModule[nModuleIndex].nTxLength;
					
					crcFull = fnCRC16_ccitt( g_pstModule[nModuleIndex].szTempTxBuffer, nDataSize);

					crcHigh = (byte)((crcFull>>8)&0xFF);	// CRC[1]
					crcLow = (byte)(crcFull&0xFF);			// CRC[0]

					// TRACE("SendMsg CRC ====> High: 0x%02x, Low: 0x%02x\n", crcHigh, crcLow);					
					
					/* CRC16
					*/
					g_pstModule[nModuleIndex].szTempTxBuffer[nDataSize++] = crcLow;
					g_pstModule[nModuleIndex].szTempTxBuffer[nDataSize++] = crcHigh;
					
					LPEP_MSG_HEADER lpHeader = (LPEP_MSG_HEADER)g_pstModule[nModuleIndex].szTxBuffer;					
					
					nRtn = clientSide.Write(g_pstModule[nModuleIndex].szTempTxBuffer, nDataSize);
					
					if( nRtn != nDataSize )
					{
						sprintf(msg, "Module %d Socket write Error, %d Byte Write - %d Byte remain", nModuleID, nRtn, g_pstModule[nModuleIndex].nTxLength - nRtn);
						WriteLog(msg);
					}
					else
					{	
						sprintf(msg, ">>[SEND] Cmd = 0x%08x From Module = %d : %d Byte", 
							lpHeader->nCommand,
							nModuleID, 
							nDataSize );
						WriteLog(msg);
					}
					
					g_pstModule[nModuleIndex].nTxLength = 0;
					::ResetEvent( g_pstModule[nModuleIndex].m_hWriteEvent );
					Sleep(1);
					break;
				}
			}	//switch
		} // end if
	}	//while(1)

//Module Conntection Colse Label
_CLOSE_MODULE:				//Release Memory and close client socket
	WriteLog(msg);
	clientSide.Close();

///////////////
	ModuleClosed(nModuleIndex, m_hMessageWnd);
	//if(lpEndDataBuff)
	//{
	//	delete [] lpEndDataBuff;	lpEndDataBuff = NULL;
	//}
///////////////
	
	if(lpReadBuff)
	{
		delete [] lpReadBuff;		lpReadBuff = NULL;
	}
	delete lpMsgHeader;			lpMsgHeader = NULL;
	delete [] lpHandles;		lpHandles = NULL;
	delete lpEvents;			lpEvents = NULL;
	return TRUE;
}

void ModuleConnected(int nModuleIndex, HWND hMsgWnd)
{
	int nModuleID =  g_pstModule[nModuleIndex].sysData.nModuleID;			//Get Module ID
	g_pstModule[nModuleIndex].state = EP_STATE_LINE_ON;						//Module Line On State

	LPEP_GROUP_INFO lpGPData;
	lpGPData = (EP_GROUP_INFO *)g_pstModule[nModuleIndex].gpData[0];
	ASSERT(lpGPData);
	lpGPData->gpData.gpState.state = EP_STATE_LINE_ON;

	SBCQueue_DATA QData;// = new SBCQueue_DATA;
	QData.id = (WPARAM)MAKELONG(0, nModuleID);
	QData.command = EP_CMD_MODULE_CONNECT;
	g_Que.Push(QData);

	if(hMsgWnd)
		::PostMessage(hMsgWnd, EPWM_MODULE_CHANGE_INFO, MAKELONG(0, nModuleID), 0);


// 	if(hMsgWnd)
// 	{
// 		::PostMessage(hMsgWnd, EPWM_MODULE_CONNECTED, MAKELONG(0, nModuleID), 0);		//Send to Parent Wnd to Module Conntected 
// 	}	
// 
// 	if(hMsgWnd)
// 	{
// 		::PostMessage(hMsgWnd, EPWM_MODULE_STATE_CHANGE, MAKELONG(0, nModuleID), 0);		//Send to Parent Wnd To Module State Change
// 	}
//	TRACE("Module %d Connected\n", nModuleID);
}

void ModuleClosed (int nModuleIndex , HWND hMsgWnd)
{
	if(g_pstModule == NULL || nModuleIndex < 0)	return ;					//Form Server is closed
	int nModuleID = g_pstModule[nModuleIndex].sysData.nModuleID;				//Get Module ID
	
	g_pstModule[nModuleIndex].state = EP_STATE_LINE_OFF;						//Module Line Off State
	
	LPEP_GROUP_INFO lpGPData;
	lpGPData = (EP_GROUP_INFO *)g_pstModule[nModuleIndex].gpData[0];
	ASSERT(lpGPData);
	
	lpGPData->gpData.gpState.state = EP_STATE_LINE_OFF;

	SBCQueue_DATA QData;// = new SBCQueue_DATA;
	QData.id = (WPARAM)MAKELONG(0, nModuleID);
	QData.command = EP_CMD_MODULE_DISCONNECT;
	g_Que.Push(QData);

// 	if(hMsgWnd)
// 		::PostMessage(hMsgWnd, EPWM_MODULE_CHANGE_INFO, MAKELONG(0, nModuleID), 0);
// 
// 	
// 	if(hMsgWnd)
// 		::PostMessage(hMsgWnd, EPWM_MODULE_CLOSED, MAKELONG(0, nModuleID), 0);	//Send to Parent Wnd to Module Close Message
// 
// 	if(hMsgWnd)
		::PostMessage(hMsgWnd, EPWM_MODULE_STATE_CHANGE, MAKELONG(0, nModuleID), 0);			//Send to Parent Wnd to Module State change
//	TRACE("Module %d Disconnected\n", nModuleID);
}
/*
BYTE GetNextPacketID()
{
	static BYTE bPacketSerial = 0;
	return bPacketSerial++;
}
*/
void MakeMsgHeader (LPEP_MSG_HEADER pHeader, int nGroupIndex, int nChannelIndex, int nCommand)
{
//	pHeader->bDirection = _HOST_TO_MODULE;
//	pHeader->bMessageID = GetNextPacketID();
	pHeader->nID = EP_ID_FORM_OP_SYSTEM;
	pHeader->wGroupNum = (WORD)nGroupIndex;
	pHeader->wChannelNum = (WORD)nChannelIndex;
	pHeader->nCommand = nCommand;
	pHeader->nLength = SizeofHeader();
}

inline int CheckMsgHeader(int nModuleIndex, LPEP_MSG_HEADER lpHeader)
{
	ASSERT(lpHeader);
	if(lpHeader->wChannelNum < 0 || lpHeader->wChannelNum > g_pstModule[nModuleIndex].sysData.nTotalChNo)	
		return -3;
//	if(GetCmdDataSize(lpHeader->nCommand) != (lpHeader->nLength - SizeofHeader()))	return -4;
	return TRUE;
}

// -----------------------------------------------------------------------------
//  [5/3/2009 kky] - Formation system
// DESC : Data parsing from Sbc to CTSMon. 
// -----------------------------------------------------------------------------
//Parsing Response Command 
//Ack 가 필요 하지 않은 Data는 반드시 return 하여야 한다.
BOOL ParsingCommand(CWizReadWriteSocket *pSock, int nModuleIndex, LPVOID lpMsgHeader, LPVOID lpReadBuff, HWND hMsgWnd)
{
	ASSERT(pSock);
	ASSERT(lpMsgHeader);
//	ASSERT(lpReadBuff);		//최초 Command가 Body 없는 명령일 경우 lpReadBuff가 NULL이다.

	char	msg[512];
	INT		wrSize = 0;

	LPEP_MSG_HEADER lpHeader = (LPEP_MSG_HEADER)lpMsgHeader;

	UINT nCmd = 0;
	nCmd = lpHeader->nCommand;

	if( nCmd == EP_CMD_AUTO_GP_DATA ||
		nCmd == EP_CMD_AUTO_GP_STATE_DATA ||
		nCmd == EP_CMD_AUTO_SENSOR_DATA ||
		nCmd == EP_CMD_HEARTBEAT )
	{

	}
	else
	{
		sprintf(msg, "[RECV] Cmd = 0x%08x From Module = %d : %d Byte", 
			lpHeader->nCommand,
			g_pstModule[nModuleIndex].sysParam.nModuleID, 
			lpHeader->nLength );
		WriteLog(msg);
	}
		
	if(( wrSize = CheckMsgHeader(nModuleIndex, lpHeader)) < 0)		//Header Check
	{
		sprintf(msg, "Module %d Message Header Error. Cmd = %d, Code= %d, Gp = %d", 
			g_pstModule[nModuleIndex].sysParam.nModuleID, 
			lpHeader->nCommand, 
			wrSize, 
			lpHeader->wGroupNum);
		WriteLog(msg);
		return FALSE;
	}

	//Response packet
	EP_RESPONSE	response;
	ZeroMemory(&response, sizeof(EP_RESPONSE));
	response.nCmd =	lpHeader->nCommand;
	response.nCode = EP_ACK;			//default Response Code is Ack

	switch(lpHeader->nCommand)
	{
	case EP_CMD_AUTO_GP_STATE_DATA:
		{				
			wrSize = lpHeader->nLength - SizeofHeader() - 2;					//CRC16
			if(wrSize == sizeof(EP_GP_STATE_DATA) && lpReadBuff != NULL)		//Data Size Check
			{
				EP_GP_STATE_DATA *lpGpState = (EP_GP_STATE_DATA *)lpReadBuff;
				EP_GP_STATE_DATA GpState;				
				LPEP_GROUP_INFO lpGPData = (EP_GROUP_INFO *)g_pstModule[nModuleIndex].gpData[0];
				ASSERT(lpGPData);				

				//Host PC에서 새로운 Group 상태 추가(Ready, End) 
				////////////////////////////////////////////////////////////////////////
				//	1. Group이 Standby 상태이지만 tray가 Load 안되거나 Door가 Open 된 경우 Ready 상태 유지 (Module은  Standby 상태)
				//  2. Group 이 End 이고 지그가 Down->Up을 갈 경우 계속 End 유지 (Module은 Standby 상태)
				//	2002.7.11 Kim Byung Hum
				/////////////////////////////////////////////////////////////////////////
				//EP_GP_STATE_DATA *lpGpState = (EP_GP_STATE_DATA *)data;
				::CopyMemory(&GpState, lpGpState, wrSize);

				SBCQueue_DATA QData;// = new SBCQueue_DATA;
				QData.id = (WPARAM)MAKELONG(0, g_pstModule[nModuleIndex].sysParam.nModuleID);
				QData.command = lpHeader->nCommand;
				::CopyMemory(&QData.Data, (LPVOID)&GpState, sizeof(EP_GP_STATE_DATA));
				QData.DataLength = wrSize;

				g_Que.Push(QData);

				::PostMessage(hMsgWnd, 
					EPWM_MODULE_CHANGE_INFO, 
					(WPARAM)MAKELONG(0, g_pstModule[nModuleIndex].sysParam.nModuleID), 
					(LPARAM)wrSize);
					
				return true;
			}
			else
			{
				sprintf(msg, "Module %d Group %d, Size of Group State Data is Error.", 
					g_pstModule[nModuleIndex].sysParam.nModuleID,
					lpHeader->wGroupNum);
				WriteLog(msg);
				return false;
			}
		}		
	
	// Module(SBC)에서 주기적으로 자동 보고 되는 Data 
	case EP_CMD_AUTO_GP_DATA:
		{
			LPEP_GROUP_INFO lpGPData;
			LPEP_CH_DATA	lpChannel;
			lpGPData = (EP_GROUP_INFO *)g_pstModule[nModuleIndex].gpData[0];

			ASSERT(lpGPData);

			wrSize = lpHeader->nLength - SizeofHeader() - 2;		//Data Size Check
			if( wrSize > SizeofCHData() * g_pstModule[nModuleIndex].sysData.nTotalChNo ||
				(wrSize % SizeofCHData() != 0) || 
				 lpReadBuff == NULL)
			{
				sprintf(msg, "Module %d Group %d, number of channel is error in EP_CMD_AUTO_GP_DATA", g_pstModule[nModuleIndex].sysParam.nModuleID, lpHeader->wGroupNum);
				WriteLog(msg);
				response.nCode = EP_SIZE_MISMATCH; 
			}
			else
			{	
				//  [7/13/2009 kky ]
				// for 에러 코드 세분화 => 나중에 수정할 것
				int index;
				ZeroMemory(lpGPData->gpData.wFailCnt, sizeof(lpGPData->gpData.wFailCnt));
				
				for(INDEX ch = 0; ch<g_pstModule[nModuleIndex].sysData.nTotalChNo; ch++)
				{				
					wrSize = SizeofCHData() * ch;
					lpChannel = (EP_CH_DATA *)((char *)lpReadBuff+wrSize);
					
					//Host Mapping
					index = GetMappingCh(ch, g_pstModule[nModuleIndex].sysData.wTrayType, lpChannel->wChIndex);
					
					if(index < 0 || index >=g_pstModule[nModuleIndex].sysData.nTotalChNo)
					{
						sprintf(msg, "Module %d Group %d, Channel index error %d", g_pstModule[nModuleIndex].sysParam.nModuleID, lpHeader->wGroupNum, index);
						WriteLog(msg);
					}
					else
					{
						/*
						if(lpChannel->channelCode >=0 && lpChannel->channelCode < 512)
						{						
						//	lpGPData->gpData.wFailCnt[lpChannel->channelCode]++;				
							switch( lpChannel->channelCode )
							{
							case EP_CODE_NORMAL:
								lpGPData->gpData.wFailCnt[EP_CODE_NORMAL]++;
								break;
							case EP_CODE_CELL_NONE:
								lpGPData->gpData.wFailCnt[EP_CODE_CELL_NONE]++;
								lpGPData->gpData.wFailCnt[EP_CODE_FAIL_ALL]++;
								break;
							case EP_CODE_FAIL_VTG_HIGH:
								lpGPData->gpData.wFailCnt[EP_CODE_FAIL_VTG_HIGH]++;
								lpGPData->gpData.wFailCnt[EP_CODE_FAIL_ALL]++;
								break;
							case EP_CODE_FAIL_VTG_LOW:
								lpGPData->gpData.wFailCnt[EP_CODE_FAIL_VTG_LOW]++;
								lpGPData->gpData.wFailCnt[EP_CODE_FAIL_ALL]++;
								break;
							case EP_CODE_FAIL_CRT_HIGH:
								lpGPData->gpData.wFailCnt[EP_CODE_FAIL_CRT_HIGH]++;
								lpGPData->gpData.wFailCnt[EP_CODE_FAIL_ALL]++;
								break;
							case EP_CODE_FAIL_CRT_LOW:
								lpGPData->gpData.wFailCnt[EP_CODE_FAIL_CRT_LOW]++;
								lpGPData->gpData.wFailCnt[EP_CODE_FAIL_ALL]++;
								break;
							case EP_CODE_FAIL_V_HUNTING:
								lpGPData->gpData.wFailCnt[EP_CODE_FAIL_V_HUNTING]++;
								lpGPData->gpData.wFailCnt[EP_CODE_FAIL_ALL]++;
								break;
							case EP_CODE_FAIL_I_HUNTING:
								lpGPData->gpData.wFailCnt[EP_CODE_FAIL_I_HUNTING]++;
								lpGPData->gpData.wFailCnt[EP_CODE_FAIL_ALL]++;
								break;
						//	default:
						//		lpGPData->gpData.wFailCnt[EP_CODE_FAIL_ALL]++;
							}	
						}
						*/
						// -----------------------------------------------------------------------------
						lpChannel =	(EP_CH_DATA *)lpGPData->chData[index];
						ASSERT(lpChannel);
						memcpy(lpChannel, (char *)lpReadBuff+wrSize, SizeofCHData());
						lpChannel->wChIndex = (WORD)index;
					}
				}
			}
		}
		return TRUE;

	case EP_CMD_AUTO_GP_STEP_END_DATA:
		{		
			LPEP_CH_DATA	lpChDest, lpChSource;
			int nItemResultSize = sizeof(EP_STEP_END_HEADER);
			wrSize = lpHeader->nLength - SizeofHeader() - 2;		//Data Size Check
			
			if( wrSize != (SizeofCHData()*g_pstModule[nModuleIndex].sysData.nTotalChNo+nItemResultSize ) ||
				lpReadBuff == NULL)
			{
				sprintf(msg, "Module %d Group %d, Number of Channel is Error in Step End Data. %d/%d", g_pstModule[nModuleIndex].sysParam.nModuleID, lpHeader->wGroupNum,
					wrSize, SizeofCHData()*g_pstModule[nModuleIndex].sysData.nTotalChNo+nItemResultSize);
				WriteLog(msg);
				return FALSE;
			}
			else
			{
				LPVOID lpBuff = new char[wrSize];
				memcpy(lpBuff, lpReadBuff, wrSize);
				EP_STEP_END_HEADER *pStepEndHeader = (EP_STEP_END_HEADER *)lpBuff;
				
				ASSERT(pStepEndHeader);

				INT wrSize = 0;
				INT index = 0;

				LPEP_GROUP_INFO lpGPData = (EP_GROUP_INFO *)g_pstModule[nModuleIndex].gpData[0];
				LPEP_CH_DATA	lpChDest, lpChSource;

				SBCQueue_DATA QData;// = new SBCQueue_DATA;
				
				for(INDEX ch = 0; ch<g_pstModule[nModuleIndex].sysData.nTotalChNo; ch++)
				{				
					wrSize = SizeofCHData()*ch;
					lpChSource = (EP_CH_DATA *)((char *)lpBuff+nItemResultSize+wrSize);

					//Host Mapping
					index = GetMappingCh(ch, g_pstModule[nModuleIndex].sysData.wTrayType, lpChSource->wChIndex);

					if(index < 0 || index >= g_pstModule[nModuleIndex].sysData.nTotalChNo)
					{
						response.nCode = EP_NACK;
						sprintf(msg, "[Stage %d] Group %d, Channel Index Error %d", g_pstModule[nModuleIndex].sysParam.nModuleID, lpHeader->wGroupNum, index);
						WriteLog(msg);
					}
					else
					{
						lpChSource = (EP_CH_DATA *)((char *)lpBuff+nItemResultSize+wrSize);
						lpChDest =	(EP_CH_DATA *)lpGPData->chData[index];
						ASSERT(lpChDest);
						memcpy(lpChDest, (char *)lpChSource, SizeofCHData());
						lpChDest->wChIndex = (WORD)index;
						lpChSource->wChIndex = (WORD)index;					
						memcpy((char *)QData.Data+nItemResultSize+index*SizeofCHData(), lpChSource, SizeofCHData());	//Result 결과를 재정렬 한다.
					}
				}

				g_pstModule[nModuleIndex].lpDataBuff = new CHAR[wrSize];
				memcpy(g_pstModule[nModuleIndex].lpDataBuff, QData.Data, wrSize);
				QData.id = (WPARAM)MAKELONG(lpHeader->wGroupNum, g_pstModule[nModuleIndex].sysParam.nModuleID);
				QData.command = lpHeader->nCommand;
				QData.DataLength = wrSize;

				g_Que.Push(QData);
				
				if( g_pstModule[nModuleIndex].lpDataBuff )
				{
					delete [] g_pstModule[nModuleIndex].lpDataBuff;
					g_pstModule[nModuleIndex].lpDataBuff = NULL;
				}
				
				delete lpBuff;
				lpBuff = NULL;

				::PostMessage(hMsgWnd, 
					EPWM_MODULE_CHANGE_INFO, 
					(WPARAM)MAKELONG(lpHeader->wGroupNum, g_pstModule[nModuleIndex].sysParam.nModuleID),
					(LPARAM)wrSize);
			}
			return true;
		}
		
// 	case EP_CMD_RESPONSE:					// Response of Command
// 		{
// 			if((lpHeader->nLength - SizeofHeader() - 2) == sizeof(EP_RESPONSE)
// 				&& lpReadBuff != NULL)
// 			{
// 				memcpy(&g_pstModule[nModuleIndex].CommandAck, lpReadBuff, sizeof(EP_RESPONSE));
// 
// 				/*
// 				SBCQueue_DATA QData;// = new SBCQueue_DATA;
// 				memcpy(QData.Data, lpReadBuff, sizeof(EP_RESPONSE));
// 				QData.id = (WPARAM)MAKELONG(lpHeader->wGroupNum, g_pstModule[nModuleIndex].sysParam.nModuleID);
// 				QData.command = lpHeader->nCommand;
// 				QData.DataLength = wrSize;
// 
// 				g_Que.Push(QData);
// 
// 				::PostMessage(hMsgWnd, 
// 					EPWM_MODULE_CHANGE_INFO, 
// 					(WPARAM)MAKELONG(lpHeader->wGroupNum, g_pstModule[nModuleIndex].sysParam.nModuleID),
// 					(LPARAM)wrSize);
// 				*/
// 			}
// 			else
// 			{
// 				g_pstModule[nModuleIndex].CommandAck.nCode = EP_SIZE_MISMATCH;
// 				sprintf(msg, "Module %d Group %d, Command Response Size Error. %d", 
// 							g_pstModule[nModuleIndex].sysParam.nModuleID, lpHeader->wGroupNum, lpHeader->nLength);
// 				WriteLog(msg);
// 			}
// 			
// 			sprintf(msg, "<<== Response Command 0x%08x from module %d, Code %d\n", 
// 							g_pstModule[nModuleIndex].CommandAck.nCmd, g_pstModule[nModuleIndex].sysParam.nModuleID,  
// 							g_pstModule[nModuleIndex].CommandAck.nCode); 			
// 			// WriteLog(msg);
// 			TRACE(msg);
// 
// //			TRACE("Response From Module : Command 0x%x, Data %d\n", g_pstModule[nModuleIndex].CommandAck.nCmd, g_pstModule[nModuleIndex].CommandAck.nCode);
// 			::SetEvent(g_pstModule[nModuleIndex].m_hReadEvent);		
// 		}
// 		return true;
		
	case EP_CMD_TAG_INFO:				//NVRAM Data
		{
			wrSize = sizeof(EP_TAG_INFOMATION);
			if((lpHeader->nLength - SizeofHeader()) == wrSize && lpReadBuff != NULL)
			{
				SBCQueue_DATA QData;// = new SBCQueue_DATA;
				QData.id = (WPARAM)MAKELONG(0, g_pstModule[nModuleIndex].sysParam.nModuleID);
				memcpy(&QData.Data, (LPVOID)lpReadBuff, sizeof(EP_TAG_INFOMATION));
				QData.command = lpHeader->nCommand;
				QData.DataLength = wrSize;

				g_Que.Push(QData);

				::PostMessage(hMsgWnd, 
					EPWM_MODULE_CHANGE_INFO, 
					(WPARAM)MAKELONG(0, g_pstModule[nModuleIndex].sysParam.nModuleID), 
					(LPARAM)wrSize);
					
				static EP_TAG_INFOMATION nvData;
				memcpy(&nvData, (LPVOID)lpReadBuff, sizeof(EP_TAG_INFOMATION));
				if(hMsgWnd)
				{
					::PostMessage(hMsgWnd, EPWM_NVRAM_HEADER, MAKELONG(0, g_pstModule[nModuleIndex].sysParam.nModuleID), (LPARAM)&nvData);			//Send to Parent Wnd to Module State change
				}
			}
			else
			{
				g_pstModule[nModuleIndex].CommandAck.nCode = EP_SIZE_MISMATCH;
				sprintf(msg, "Module %d Group %d, Tag data header size error. %d/%d", 
							g_pstModule[nModuleIndex].sysParam.nModuleID, lpHeader->wGroupNum, lpHeader->nLength, sizeof(EP_TAG_INFOMATION));
				WriteLog(msg);
			}
		}
		return true;

	case EP_CMD_AUTO_CHECK_RESULT:		//Check Result Data
		{
			wrSize = sizeof(EP_CHECK_RESULT);
			
			if((lpHeader->nLength - SizeofHeader() - 2) == wrSize && lpReadBuff != NULL)
			{	
				SBCQueue_DATA QData;// = new SBCQueue_DATA;
				QData.id = (WPARAM)MAKELONG(0, g_pstModule[nModuleIndex].sysParam.nModuleID);
				memcpy(&QData.Data, (LPVOID)lpReadBuff, sizeof(EP_CHECK_RESULT));
				QData.command = lpHeader->nCommand;
				QData.DataLength = wrSize;

				g_Que.Push(QData);

				::PostMessage(hMsgWnd, 
					EPWM_MODULE_CHANGE_INFO, 
					(WPARAM)MAKELONG(0, g_pstModule[nModuleIndex].sysParam.nModuleID), 
					(LPARAM)wrSize);
			}
			else
			{
				g_pstModule[nModuleIndex].CommandAck.nCode = EP_SIZE_MISMATCH;
				sprintf(msg, "Module %d Group %d, Check Result Header Size Error. %d", 
							g_pstModule[nModuleIndex].sysParam.nModuleID, lpHeader->wGroupNum, lpHeader->nLength);
				WriteLog(msg);
				response.nCode = EP_NACK; //send Nack
			}
			break;
		}
	
	case EP_CMD_AUTO_SENSOR_DATA:	//Gas and Temperature Measure Data
		{
			LPEP_GROUP_INFO lpGPData;
			lpGPData = (EP_GROUP_INFO *)g_pstModule[nModuleIndex].gpData[0];
			ASSERT(lpGPData);
			wrSize = lpHeader->nLength - SizeofHeader() - 2;
			if(wrSize == sizeof(EP_SENSOR_DATA) && lpReadBuff != NULL)		//Data Size Check
			{
				memcpy(&lpGPData->gpData.sensorData, lpReadBuff, wrSize);
				SYSTEMTIME systime;
				::GetLocalTime(&systime);

				for(int  i=0; i<EP_MAX_SENSOR_CH; i++)
				{
					if(lpGPData->gpData.sensorMinMax.sensorData1[i].wMaxIndex == 0)		//is it first data?
					{
						lpGPData->gpData.sensorMinMax.sensorData1[i].wMaxIndex = 1;
						lpGPData->gpData.sensorMinMax.sensorData1[i].lMin = lpGPData->gpData.sensorData.sensorData1[i].lData;
						lpGPData->gpData.sensorMinMax.sensorData1[i].lMax = lpGPData->gpData.sensorData.sensorData1[i].lData;
						sprintf(lpGPData->gpData.sensorMinMax.sensorData1[i].szMaxDateTime, "%04d/%02d/%02d %02d:%02d:%02d", systime.wYear, systime.wMonth, systime.wDay, systime.wHour, systime.wMinute, systime.wSecond);
						sprintf(lpGPData->gpData.sensorMinMax.sensorData1[i].szMinDateTime, "%04d/%02d/%02d %02d:%02d:%02d", systime.wYear, systime.wMonth, systime.wDay, systime.wHour, systime.wMinute, systime.wSecond);
					}
					else
					{
						if(lpGPData->gpData.sensorMinMax.sensorData1[i].lMax < lpGPData->gpData.sensorData.sensorData1[i].lData)
						{
							lpGPData->gpData.sensorMinMax.sensorData1[i].lMax = lpGPData->gpData.sensorData.sensorData1[i].lData;
							sprintf(lpGPData->gpData.sensorMinMax.sensorData1[i].szMaxDateTime, "%04d/%02d/%02d %02d:%02d:%02d", systime.wYear, systime.wMonth, systime.wDay, systime.wHour, systime.wMinute, systime.wSecond);
						}
						else if(lpGPData->gpData.sensorMinMax.sensorData1[i].lMin > lpGPData->gpData.sensorData.sensorData1[i].lData)
						{
							lpGPData->gpData.sensorMinMax.sensorData1[i].lMin = lpGPData->gpData.sensorData.sensorData1[i].lData;
							sprintf(lpGPData->gpData.sensorMinMax.sensorData1[i].szMinDateTime, "%04d/%02d/%02d %02d:%02d:%02d", systime.wYear, systime.wMonth, systime.wDay, systime.wHour, systime.wMinute, systime.wSecond);
						}
					}
					
					if(lpGPData->gpData.sensorMinMax.sensorData2[i].wMaxIndex == 0)		//First Temperature Data
					{
						lpGPData->gpData.sensorMinMax.sensorData2[i].wMaxIndex = 1;
						lpGPData->gpData.sensorMinMax.sensorData2[i].lMax = lpGPData->gpData.sensorData.sensorData2[i].lData;
						lpGPData->gpData.sensorMinMax.sensorData2[i].lMin = lpGPData->gpData.sensorData.sensorData2[i].lData;
						sprintf(lpGPData->gpData.sensorMinMax.sensorData2[i].szMaxDateTime, "%04d/%02d/%02d %02d:%02d:%02d", systime.wYear, systime.wMonth, systime.wDay, systime.wHour, systime.wMinute, systime.wSecond);
						sprintf(lpGPData->gpData.sensorMinMax.sensorData2[i].szMinDateTime, "%04d/%02d/%02d %02d:%02d:%02d", systime.wYear, systime.wMonth, systime.wDay, systime.wHour, systime.wMinute, systime.wSecond);
					}
					else
					{
						if(lpGPData->gpData.sensorMinMax.sensorData2[i].lMax < lpGPData->gpData.sensorData.sensorData2[i].lData)
						{
							lpGPData->gpData.sensorMinMax.sensorData2[i].lMax = lpGPData->gpData.sensorData.sensorData2[i].lData;
							sprintf(lpGPData->gpData.sensorMinMax.sensorData2[i].szMaxDateTime, "%04d/%02d/%02d %02d:%02d:%02d", systime.wYear, systime.wMonth, systime.wDay, systime.wHour, systime.wMinute, systime.wSecond);
						}
						else if(lpGPData->gpData.sensorMinMax.sensorData2[i].lMin > lpGPData->gpData.sensorData.sensorData2[i].lData)
						{
							lpGPData->gpData.sensorMinMax.sensorData2[i].lMin = lpGPData->gpData.sensorData.sensorData2[i].lData;
							sprintf(lpGPData->gpData.sensorMinMax.sensorData2[i].szMinDateTime, "%04d/%02d/%02d %02d:%02d:%02d", systime.wYear, systime.wMonth, systime.wDay, systime.wHour, systime.wMinute, systime.wSecond);
						}
					}
				}
			}
			else
			{
				g_pstModule[nModuleIndex].CommandAck.nCode = EP_SIZE_MISMATCH;
				sprintf(msg, "Module %d Group %d, SenSor Data  Size Error. %d", 
							g_pstModule[nModuleIndex].sysParam.nModuleID, lpHeader->wGroupNum, lpHeader->nLength);
				WriteLog(msg);
 				// ::SetEvent(g_pstModule[nModuleIndex].m_hReadEvent);
 				// return false;
			}
		}
		return true;

	case EP_CMD_AUTO_EMG_DATA :
		{
			wrSize = sizeof(EP_CODE);
			
			if((lpHeader->nLength - SizeofHeader() - 2) == wrSize && lpReadBuff != NULL)
			{				
				char errormsg[1024] = {0};
				EP_CODE codeData;
				memcpy(&codeData, (LPVOID)lpReadBuff, sizeof(EP_CODE));
				sprintf(errormsg, "Module %d Group %d, Emergency Detected. (EMG Code %d / %d)", 
					g_pstModule[nModuleIndex].sysParam.nModuleID, lpHeader->wGroupNum, 
					codeData.nCode, codeData.nData);	
					
				WriteLog(errormsg);			

				SBCQueue_DATA QData;// = new SBCQueue_DATA;
				QData.id = (WPARAM)MAKELONG(0, g_pstModule[nModuleIndex].sysParam.nModuleID);
				memcpy(&QData.Data, (LPVOID)lpReadBuff, sizeof(EP_CODE));
				QData.command = lpHeader->nCommand;
				QData.DataLength = wrSize;

				g_Que.Push(QData);

				::PostMessage(hMsgWnd, 
					EPWM_MODULE_CHANGE_INFO, 
					(WPARAM)MAKELONG(0, g_pstModule[nModuleIndex].sysParam.nModuleID), 
					(LPARAM)wrSize);

				LPEP_GROUP_INFO lpGPData = (EP_GROUP_INFO *)g_pstModule[nModuleIndex].gpData[0];
				ASSERT(lpGPData);
			}
			else
			{
				g_pstModule[nModuleIndex].CommandAck.nCode = EP_SIZE_MISMATCH;
				sprintf(msg, "Module %d, Emergency Data Header Size Error. %d", 
							g_pstModule[nModuleIndex].sysParam.nModuleID, lpHeader->nLength);
				WriteLog(msg);
				return false;
			}
		}
		return true;

	//NetWork State Check
	case EP_CMD_HEARTBEAT:	
		break;		//Send Ack

	case EP_CMD_USER_CMD :
		{
			wrSize = sizeof(EP_USER_CMD_INFO);
			
			if((lpHeader->nLength - SizeofHeader() - 2) == wrSize && lpReadBuff != NULL)
			{
				EP_USER_CMD_INFO UserCmd;
				::CopyMemory(&UserCmd, (LPVOID)lpReadBuff, sizeof(EP_USER_CMD_INFO));

				char usermsg[1024] = {0};
				sprintf(usermsg, "Module %d Group %d, User Command Detected. (Command 0x%08x / %d)", 
					g_pstModule[nModuleIndex].sysParam.nModuleID, lpHeader->wGroupNum, 
					UserCmd.nCmd, UserCmd.nData);

				WriteLog(usermsg);

				SBCQueue_DATA QData;
				QData.id = (WPARAM)MAKELONG(0, g_pstModule[nModuleIndex].sysParam.nModuleID);
				memcpy(&QData.Data, (LPVOID)lpReadBuff, sizeof(EP_USER_CMD_INFO));
				QData.command = lpHeader->nCommand;
				QData.DataLength = wrSize;

				g_Que.Push(QData);

				::PostMessage(hMsgWnd, 
					EPWM_MODULE_CHANGE_INFO, 
					(WPARAM)MAKELONG(0, g_pstModule[nModuleIndex].sysParam.nModuleID), 
					(LPARAM)wrSize);
			}
			else
			{
				g_pstModule[nModuleIndex].CommandAck.nCode = EP_SIZE_MISMATCH;
				sprintf(msg, "Module %d User Command Header Size Error. %d", 
							g_pstModule[nModuleIndex].sysParam.nModuleID, lpHeader->nLength);
				WriteLog(msg);
				response.nCode = EP_NACK; //send Nack
			}
		}
		break;

	case EP_CMD_REAL_TIME_DATA:
		{
			LPEP_GROUP_INFO lpGPData;
			lpGPData = (LPEP_GROUP_INFO)g_pstModule[nModuleIndex].gpData[0];
			ASSERT(lpGPData);

			wrSize = lpHeader->nLength - SizeofHeader() - 2;		//Data Size Check
			if( wrSize != sizeof(EP_REAL_TIME_DATA)*g_pstModule[nModuleIndex].sysData.nTotalChNo || lpReadBuff == NULL)
			{
				sprintf(msg, "Module %d number of channel in real time data is error.(%d/%d)", 
								g_pstModule[nModuleIndex].sysParam.nModuleID, 
								wrSize,
								sizeof(EP_REAL_TIME_DATA)*g_pstModule[nModuleIndex].sysData.nTotalChNo
								);
				WriteLog(msg);
				response.nCode = EP_NACK; //send Nack
			}
			else
			{	
				EP_REAL_TIME_DATA	*lpChannel;
				int index;
				for(INDEX ch = 0; ch<g_pstModule[nModuleIndex].sysData.nTotalChNo; ch++)
				{	
					lpChannel = (EP_REAL_TIME_DATA *)((char *)lpReadBuff + ch*sizeof(EP_REAL_TIME_DATA));
					index = GetMappingCh(ch, g_pstModule[nModuleIndex].sysData.wTrayType, lpChannel->nChIndex);
										
					if(index < 0 || index >= g_pstModule[nModuleIndex].sysData.nTotalChNo)
					{
						sprintf(msg, "Module %d channel index error %d.", g_pstModule[nModuleIndex].sysParam.nModuleID, index);
						WriteLog(msg);
						response.nCode = EP_NACK; //send Nack
						break;
					}
					else
					{
						lpChannel = (EP_REAL_TIME_DATA	*)lpGPData->chRealData[index];
						memcpy(lpChannel, (char *)lpReadBuff+sizeof(EP_REAL_TIME_DATA)*ch, sizeof(EP_REAL_TIME_DATA));
						lpChannel->nChIndex = index;
						response.nCode = EP_ACK; //send ack
					}
				}

				if(hMsgWnd && response.nCode == EP_ACK)
				{
					::PostMessage(	hMsgWnd, 
									EPWM_REALTIME_DATA_RECEIVED, 
									MAKELONG(0, g_pstModule[nModuleIndex].sysParam.nModuleID), 
									0
								);			//Send to Parent Wnd to Module State change
				}
			}
		}
		break;

	//Cali=========================================================================================================================
	//MAKELONG 0 : CAL 1: CHK
	case EP_CMD_CAL_RES:
	case EP_CMD_CHK_RES:
		{
			wrSize = sizeof(EP_CALIHCK_RESULT_DATA);
			if((lpHeader->nLength - SizeofHeader() - 2) == wrSize && lpReadBuff != NULL)
			{
				SBCQueue_DATA QData;// = new SBCQueue_DATA;
				
				if(EP_CMD_CAL_RES == lpHeader->nCommand)
				{
					QData.id = (WPARAM)MAKELONG(0, g_pstModule[nModuleIndex].sysParam.nModuleID);
				}
				else
				{
					QData.id = (WPARAM)MAKELONG(1, g_pstModule[nModuleIndex].sysParam.nModuleID);				
				}
							
				memcpy(&QData.Data, (LPVOID)lpReadBuff, sizeof(EP_CALIHCK_RESULT_DATA));
				QData.command = lpHeader->nCommand;
				QData.DataLength = wrSize;

				g_Que.Push(QData);

				::PostMessage(hMsgWnd, 
					EPWM_MODULE_CHANGE_INFO, 
					(WPARAM)MAKELONG(0, g_pstModule[nModuleIndex].sysParam.nModuleID), 
					(LPARAM)wrSize);
			}
			
			/*
			if((lpHeader->nLength - SizeofHeader()) == sizeof(EP_CALIHCK_RESULT_DATA) && lpReadBuff != NULL)
			{
				//static EP_CALIHCK_RESULT_DATA codeData;	
				EP_CALIHCK_RESULT_DATA *codeData;
				codeData = new EP_CALIHCK_RESULT_DATA;
				//ZeroMemory( &codeData, sizeof(EP_CALIHCK_RESULT_DATA));
				ZeroMemory( codeData, sizeof(EP_CALIHCK_RESULT_DATA));
				memcpy( codeData, (LPVOID)lpReadBuff, sizeof(EP_CALIHCK_RESULT_DATA));
				sprintf(msg, "Module %d Group %d, Calibration data received.", g_pstModule[nModuleIndex].sysParam.nModuleID, lpHeader->wGroupNum);
				WriteLog(msg);
				if(hMsgWnd)
				{
					if(EP_CMD_CAL_RES == lpHeader->nCommand)
					{
						::PostMessage(hMsgWnd, EPWM_CAL_DATA_RECEIVED, MAKELONG(0, g_pstModule[nModuleIndex].sysParam.nModuleID),
							(LPARAM)codeData);			//Send to Parent Wnd to Module State change
					}
					else
					{
						::PostMessage(hMsgWnd, EPWM_CAL_DATA_RECEIVED, MAKELONG(1, g_pstModule[nModuleIndex].sysParam.nModuleID),
							(LPARAM)codeData);			//Send to Parent Wnd to Module State change
					}
				}
			}*/
		}
		return true;

	case EP_CMD_CAL_END:
	case EP_CMD_CHK_END:
		{	
			wrSize = sizeof(EP_CAL_SELECT_INFO);

			if((lpHeader->nLength - SizeofHeader() - 2) == wrSize && lpReadBuff != NULL)
			{
				SBCQueue_DATA QData;// = new SBCQueue_DATA;			

				if(EP_CMD_CAL_END == lpHeader->nCommand)
				{
					QData.id = (WPARAM)MAKELONG(0, g_pstModule[nModuleIndex].sysParam.nModuleID);
				}
				else
				{
					QData.id = (WPARAM)MAKELONG(1, g_pstModule[nModuleIndex].sysParam.nModuleID);				
				}

				memcpy(&QData.Data, (LPVOID)lpReadBuff, sizeof(EP_CAL_SELECT_INFO));
				QData.command = lpHeader->nCommand;
				QData.DataLength = wrSize;

				g_Que.Push(QData);

				::PostMessage(hMsgWnd, 
					EPWM_MODULE_CHANGE_INFO, 
					(WPARAM)MAKELONG(0, g_pstModule[nModuleIndex].sysParam.nModuleID), 
					(LPARAM)wrSize);
			}
			
			/*
			if((lpHeader->nLength - SizeofHeader()) == sizeof(EP_CAL_SELECT_INFO) && lpReadBuff != NULL)
			{
				// EP_CAL_SELECT_INFO codeData;
				EP_CAL_SELECT_INFO *codeData;
				codeData = new EP_CAL_SELECT_INFO;
				// memcpy(&codeData, (LPVOID)lpReadBuff, sizeof(EP_CAL_SELECT_INFO));
				ZeroMemory( codeData, sizeof(EP_CAL_SELECT_INFO));
				memcpy( codeData, (LPVOID)lpReadBuff, sizeof(EP_CAL_SELECT_INFO));
				sprintf(msg, "Module %d Group %d, Cali END Info received.", g_pstModule[nModuleIndex].sysParam.nModuleID, lpHeader->wGroupNum);
				WriteLog(msg);
				if(hMsgWnd)
				{
					if(EP_CMD_CAL_END == lpHeader->nCommand)
					{
						::PostMessage(hMsgWnd, EPWM_CAL_END_RECEIVED, MAKELONG(0, g_pstModule[nModuleIndex].sysParam.nModuleID),
							(LPARAM)codeData);			//Send to Parent Wnd to Module State change
					}
					else
					{
						::PostMessage(hMsgWnd, EPWM_CAL_END_RECEIVED, MAKELONG(1, g_pstModule[nModuleIndex].sysParam.nModuleID),
							(LPARAM)codeData);			//Send to Parent Wnd to Module State change
					}
				}
			}
			*/
		}
		return false;

	case EP_CMD_RM_RES:
		{
			wrSize = sizeof(EP_REAL_MEASURE_RESULT_DATA);
			if((lpHeader->nLength - SizeofHeader() - 2) == wrSize && lpReadBuff != NULL)
			{
				SBCQueue_DATA QData;// = new SBCQueue_DATA;

				QData.id = (WPARAM)MAKELONG(0, g_pstModule[nModuleIndex].sysParam.nModuleID);
				memcpy(&QData.Data, (LPVOID)lpReadBuff, sizeof(EP_REAL_MEASURE_RESULT_DATA));
				QData.command = lpHeader->nCommand;
				QData.DataLength = wrSize;

				g_Que.Push(QData);
				
				sprintf(msg, "Module %d Group %d, Real Measure Data received.", 
					g_pstModule[nModuleIndex].sysParam.nModuleID, lpHeader->wGroupNum);
				WriteLog(msg);

				::PostMessage(hMsgWnd, 
					EPWM_MODULE_CHANGE_INFO, 
					(WPARAM)MAKELONG(0, g_pstModule[nModuleIndex].sysParam.nModuleID), 
					(LPARAM)wrSize);
			}			
			/*
			if((lpHeader->nLength - SizeofHeader()) == sizeof(EP_REAL_MEASURE_RESULT_DATA) && lpReadBuff != NULL)
			{
				EP_REAL_MEASURE_RESULT_DATA *codeData;
				codeData = new EP_REAL_MEASURE_RESULT_DATA;
				ZeroMemory( codeData, sizeof(EP_REAL_MEASURE_RESULT_DATA));
				memcpy( codeData, (LPVOID)lpReadBuff, sizeof(EP_REAL_MEASURE_RESULT_DATA));

// 				EP_REAL_MEASURE_RESULT_DATA codeData;
// 				memcpy(&codeData, (LPVOID)lpReadBuff, sizeof(EP_REAL_MEASURE_RESULT_DATA));
				sprintf(msg, "Module %d Group %d, Real Measure Data received.", 
					g_pstModule[nModuleIndex].sysParam.nModuleID, lpHeader->wGroupNum);
				WriteLog(msg);
				if(hMsgWnd)
				{
					::PostMessage(hMsgWnd, EPWM_RM_END_RECEIVED, MAKELONG(0, g_pstModule[nModuleIndex].sysParam.nModuleID),
							(LPARAM)codeData);			//Send to Parent Wnd to Module State change
				}
			}
			*/
		}
		return false;
	case EP_CMD_RM_STOP_RESPONS:
		{
			sprintf(msg, "Module %d Group %d, channel real data -> WORK END received.", 
				g_pstModule[nModuleIndex].sysParam.nModuleID, lpHeader->wGroupNum);
			WriteLog(msg);
			
			wrSize = 0;
			SBCQueue_DATA QData;// = new SBCQueue_DATA;
			QData.id = (WPARAM)MAKELONG(0, g_pstModule[nModuleIndex].sysParam.nModuleID);
			// memcpy(&QData.Data, (LPVOID)lpReadBuff, sizeof(EP_CMD_IO_READINFO));
			QData.command = lpHeader->nCommand;
			QData.DataLength = wrSize;

			g_Que.Push(QData);

			::PostMessage(hMsgWnd, 
				EPWM_MODULE_CHANGE_INFO, 
				(WPARAM)MAKELONG(0, g_pstModule[nModuleIndex].sysParam.nModuleID), 
				(LPARAM)wrSize);
				
			/*
			if(hMsgWnd)
			{
				::PostMessage(hMsgWnd, EPWM_RM_WORK_END_RECEIVED, MAKELONG(0, g_pstModule[nModuleIndex].sysParam.nModuleID), 0);			//Send to Parent Wnd to Module State change
			}
			*/
		}
		return false;
		
		/*
	case EP_CMD_IOTEST_READINFO:
		{		
			wrSize = sizeof(EP_CMD_IO_READINFO);
			
			if((lpHeader->nLength - SizeofHeader() - 2) == wrSize && lpReadBuff != NULL)
			{
				SBCQueue_DATA QData;// = new SBCQueue_DATA;
				QData.id = (WPARAM)MAKELONG(0, g_pstModule[nModuleIndex].sysParam.nModuleID);
				memcpy(&QData.Data, (LPVOID)lpReadBuff, sizeof(EP_CMD_IO_READINFO));
				QData.command = lpHeader->nCommand;
				QData.DataLength = wrSize;

				g_Que.Push(QData);

				::PostMessage(hMsgWnd, 
					EPWM_MODULE_CHANGE_INFO, 
					(WPARAM)MAKELONG(0, g_pstModule[nModuleIndex].sysParam.nModuleID), 
					(LPARAM)wrSize);
			}
		}
		return true;
		*/
		
	//Response of Data Command	
	case EP_CMD_SBC_PARAM_RESPONS:
		{
			wrSize = sizeof(EP_MD_SBC_PARAM);

			if((lpHeader->nLength - SizeofHeader() - 2) == wrSize && lpReadBuff != NULL)
			{

				SBCQueue_DATA QData;// = new SBCQueue_DATA;




				EP_MD_SBC_PARAM recvSysData;
				memcpy(&recvSysData, (const void *)lpReadBuff, sizeof(EP_MD_SBC_PARAM));
				memcpy(g_pstModule[nModuleIndex].sbcParam.lFaultSettingValue, recvSysData.lFaultSettingValue, sizeof(LONG)*40);
				memcpy(g_pstModule[nModuleIndex].sbcParam.lSafetyFlag, recvSysData.lSafetyFlag, sizeof(SHORT)*40);
				memcpy(g_pstModule[nModuleIndex].sbcParam.lTemp, recvSysData.lTemp, sizeof(SHORT)*40);



				QData.id = (WPARAM)MAKELONG(0, g_pstModule[nModuleIndex].sysParam.nModuleID);
				QData.command = EP_CMD_SBC_PARAM_RESPONS;
				g_Que.Push(QData);


				::PostMessage(hMsgWnd, 
					EPWM_MODULE_CHANGE_INFO, 
					(WPARAM)MAKELONG(0, g_pstModule[nModuleIndex].sysParam.nModuleID), 
					(LPARAM)wrSize);

				g_pstModule[nModuleIndex].CommandAck.nCode = EP_ACK;

				::SetEvent(g_pstModule[nModuleIndex].m_hReadEvent);
			}
		}

		return false;
		//20200831KSJ
	case EP_CMD_SAFETY_VERSION_RESPONS:
		{
			wrSize = sizeof(EP_MD_FW_VER_DATA);

			if((lpHeader->nLength - SizeofHeader() - 2) == wrSize && lpReadBuff != NULL)
			{

				SBCQueue_DATA QData;// = new SBCQueue_DATA;




				EP_MD_FW_VER_DATA recvSysData;
				memcpy(&recvSysData, (const void *)lpReadBuff, sizeof(EP_MD_FW_VER_DATA));
				memcpy(&(g_pstModule[nModuleIndex].fwVerData), &recvSysData, sizeof(EP_MD_FW_VER_DATA));


				QData.id = (WPARAM)MAKELONG(0, g_pstModule[nModuleIndex].sysParam.nModuleID);
				QData.command = EP_CMD_SAFETY_VERSION_RESPONS;
				g_Que.Push(QData);


				::PostMessage(hMsgWnd, 
					EPWM_MODULE_CHANGE_INFO, 
					(WPARAM)MAKELONG(0, g_pstModule[nModuleIndex].sysParam.nModuleID), 
					(LPARAM)wrSize);

				g_pstModule[nModuleIndex].CommandAck.nCode = EP_ACK;

				::SetEvent(g_pstModule[nModuleIndex].m_hReadEvent);
			}


		}
		return false;
	case EP_CMD_SENSOR_LIMIT_RES:	
		{
			wrSize = sizeof(EP_SENSOR_SET_DATA);
			int recvSize =lpHeader->nLength - SizeofHeader() - 2 ;
			EP_SENSOR_SET_DATA data;
			memcpy(&data, (LPVOID)lpReadBuff, sizeof(EP_SENSOR_SET_DATA));

			if((lpHeader->nLength - SizeofHeader() - 2) == wrSize && lpReadBuff != NULL)
			{
				SBCQueue_DATA QData;// = new SBCQueue_DATA;

				QData.id = (WPARAM)MAKELONG(0, g_pstModule[nModuleIndex].sysParam.nModuleID);
				memcpy(&QData.Data, (LPVOID)lpReadBuff, sizeof(EP_SENSOR_SET_DATA));
				QData.command = lpHeader->nCommand;
				QData.DataLength = wrSize;

				g_Que.Push(QData);

				sprintf(msg, "Module %d Group %d, Sensor setting Data received.", 
					g_pstModule[nModuleIndex].sysParam.nModuleID, lpHeader->wGroupNum);
				WriteLog(msg);

				::PostMessage(hMsgWnd, 
					EPWM_MODULE_CHANGE_INFO, 
					(WPARAM)MAKELONG(0, g_pstModule[nModuleIndex].sysParam.nModuleID), 
					(LPARAM)wrSize);
			}
		}return false;
		// 20210129 KSCHOI Add ERCD Write Value Function START
	case EP_CMD_WRITE_ERCD_VALUE_RESPONS:
		{
			wrSize = sizeof(EP_ERCD_WRITE_VALUE);
			if((lpHeader->nLength - SizeofHeader() - 2) == wrSize && lpReadBuff != NULL)
			{
				SBCQueue_DATA QData;// = new SBCQueue_DATA;

				QData.id = (WPARAM)MAKELONG(0, g_pstModule[nModuleIndex].sysParam.nModuleID);
				memcpy(&QData.Data, (LPVOID)lpReadBuff, sizeof(EP_ERCD_WRITE_VALUE));
				QData.command = lpHeader->nCommand;
				QData.DataLength = wrSize;

				g_Que.Push(QData);

				sprintf(msg, "Module %d Group %d, ERCD Write Value Data Received.", 
					g_pstModule[nModuleIndex].sysParam.nModuleID, lpHeader->wGroupNum);
				WriteLog(msg);

				::PostMessage(hMsgWnd, 
					EPWM_MODULE_CHANGE_INFO, 
					(WPARAM)MAKELONG(0, g_pstModule[nModuleIndex].sysParam.nModuleID), 
					(LPARAM)wrSize);
			}
		}
		return false;
	case EP_CMD_VER_DATA:
//	case EP_CMD_TESTINFO_DATA:						//Test Information Data	
//	case EP_CMD_TRAY_SERIAL_DATA:					//Tray Serial Data
	case EP_CMD_DIGITAL_IN_DATA:					//Digital Input Port Data
	case EP_CMD_REF_IC_DATA:						//Regulator IC AD Data Request
	case EP_CMD_CONFIG_DATA:
		return true;

	case EP_CMD_MAPPING_DATA:
	case EP_CMD_CAL_POINT_DATA:
	case EP_CMD_GRIPPER_CLAMPING_RES:
	default: 
		{
			//	CCriticalSection m_RxBuffCritic;		//Rx Buffer Critical Section
			if(lpHeader->nLength > SizeofHeader() && lpReadBuff != NULL)		//Body Data Length Check
			{
				wrSize = lpHeader->nLength - SizeofHeader();
				if(wrSize <= _EP_RX_BUF_LEN)			//Rx Buffer Size Check
				{
					char *pReceiveData;
					pReceiveData = new char[wrSize];
					memcpy(pReceiveData, lpReadBuff, wrSize);
					
					//memcpy((char *)g_pstModule[nModuleIndex].szRxBuffer, lpReadBuff, wrSize);
					memcpy((char *)g_pstModule[nModuleIndex].szRxBuffer, pReceiveData, wrSize);
					delete [] pReceiveData;

					g_pstModule[nModuleIndex].nRxLength = wrSize;
					g_pstModule[nModuleIndex].CommandAck.nCode = EP_ACK;

					/*
					TRACE("<<== Response data 0x%08x , Size %d\n", lpHeader->nCommand, g_pstModule[nModuleIndex].nRxLength);
					sprintf(msg, "<<== Response data from module %d : Command 0x%x, Size %d", 
									g_pstModule[nModuleIndex].sysParam.nModuleID, lpHeader->nCommand, 
									wrSize); 			
					WriteLog(msg);*/
				}
				else									//Size Error
				{
					g_pstModule[nModuleIndex].nRxLength = 0;
					g_pstModule[nModuleIndex].CommandAck.nCode = EP_RX_BUFF_OVER_FLOW;
					sprintf(msg, "Module %d Group %d Received Data Length is too Long %d/%d", 
								  g_pstModule[nModuleIndex].sysParam.nModuleID, lpHeader->wGroupNum, wrSize, _EP_RX_BUF_LEN);
					WriteLog(msg);					
				}
				
				::SetEvent(g_pstModule[nModuleIndex].m_hReadEvent);
			}			
		}	
		return true;
	}

	//Send Response Ack/Nack
	lpHeader->nID = EP_ID_FORM_OP_SYSTEM;			//Packet Serial Number
	lpHeader->nCommand =  EP_CMD_RESPONSE;			//Command
	lpHeader->nLength = SizeofHeader() + sizeof(EP_RESPONSE) + 2;	//Length of Packet (From startByte to Body)
	
	USHORT crcFull = 0xFFFF;
	byte crcHigh = 0xFF, crcLow = 0xFF;

	ZeroMemory( g_pstModule[nModuleIndex].szTempTxBuffer, sizeof(g_pstModule[nModuleIndex].szTempTxBuffer));
	memcpy(g_pstModule[nModuleIndex].szTempTxBuffer, lpHeader, SizeofHeader());
	memcpy(&g_pstModule[nModuleIndex].szTempTxBuffer[SizeofHeader()], &response, sizeof(EP_RESPONSE));
	
	crcFull = fnCRC16_ccitt( g_pstModule[nModuleIndex].szTempTxBuffer, lpHeader->nLength-2 );

	crcHigh = (byte)((crcFull>>8)&0xFF);	// CRC[1]
	crcLow = (byte)(crcFull&0xFF);			// CRC[0]

	// TRACE("Send Msg CRC ====> High: 0x%02x, Low: 0x%02x\n", crcHigh, crcLow);

	g_pstModule[nModuleIndex].szTempTxBuffer[lpHeader->nLength-2] = crcLow;
	g_pstModule[nModuleIndex].szTempTxBuffer[lpHeader->nLength-1] = crcHigh;

	// nRtn = clientSide.Write(g_pstModule[nModuleIndex].szTxBuffer, g_pstModule[nModuleIndex].nTxLength);	

	wrSize = pSock->Write(g_pstModule[nModuleIndex].szTempTxBuffer, lpHeader->nLength);
	
	if(wrSize != lpHeader->nLength)
	{
		sprintf(msg, "Module %d, Group %d :: Send Heartbeat Ack Send Fail. %d\n", g_pstModule[nModuleIndex].sysParam.nModuleID, lpHeader->wGroupNum, wrSize);
		WriteLog(msg);
	}
	
	/*
	wrSize = pSock->Write(lpHeader, SizeofHeader());
	wrSize += pSock->Write(&response, sizeof(EP_RESPONSE));

	if(wrSize != lpHeader->nLength)
	{
		sprintf(msg, "Module %d, Group %d :: Send Heartbeat Ack Send Fail. %d\n", g_pstModule[nModuleIndex].sysParam.nModuleID, lpHeader->wGroupNum, wrSize);
		WriteLog(msg);
	}
// 	else
//	{
// 		sprintf(msg, "Module %d, Group %d :: Send Heartbeat Ack %d\n", g_pstModule[nModuleIndex].sysParam.nModuleID, lpHeader->wGroupNum, lpHeader->nID);
//		TRACE(msg);
// 		WriteLog(msg);
// 	}
	*/
	return true;
}

//Moduel ID duplication, Version, registration Check
int CheckModuleIDValidate(LPEP_MD_SYSTEM_DATA pSysData)
{
	int nModuleIndex;

	// if(pSysData->nVersion > _EP_PROTOCOL_VERSION)					return -1;		//Version Mismatch
	if((nModuleIndex = EPGetModuleIndex(pSysData->nModuleID)) < 0)	return -2;		//Not registed 
	if(pSysData->wInstalledBoard > EP_MAX_BD_PER_MD	|| pSysData->wInstalledBoard <= 0)	return -4;
	
	if(pSysData->wChannelPerBoard <= 0  || pSysData->wInstalledBoard >EP_MAX_CH_PER_BD)	return -5;
	if(pSysData->wTotalTrayNo < 0 || pSysData->wTotalTrayNo > 16)						return -7;
	
	int nTotalCh = pSysData->nTotalChNo;

	if(nTotalCh <=0 || nTotalCh >EP_MAX_CH_PER_MD)					return -6;
	
	int nTotalChInTray = 0;

	/************************************************************************/
	/* Error                                                                     */
	/************************************************************************/
	for(int  i =0; i<pSysData->wTotalTrayNo; i++)
	{
		if(pSysData->awChInTray[i] < 0 || pSysData->awChInTray[i] > EP_MAX_CH_PER_MD)	return -8;
		nTotalChInTray += pSysData->awChInTray[i];
	}

	if(nTotalChInTray <=0 || nTotalChInTray >EP_MAX_CH_PER_MD)
		return -10;

	if(nTotalCh != nTotalChInTray)
	{
		TRACE("<<<*****Channel in module and channel in tray count are mismatch*****>>>\n");
	}

//	if(EPGetModuleState(mdSysData.nModuleID) == EP_MD_ST_LINE_ON)	return -3;		//ID duplication

	return nModuleIndex;
}

USHORT fnCRC16_ccitt(const unsigned char *buf, int len)
{
	register int counter;
	register unsigned short crc = 0;

	try
	{
		for( counter = 0; counter < len; counter++)
			crc = (crc<<8) ^ crc16tab[((crc>>8) ^ *(char *)buf++)&0x00FF];
	}
	catch (CException* e)
	{
		
	}
	
	return crc;
}

//Module과 접속 시도 
int	ConnectionSequence(CWizReadWriteSocket *pSocket, HANDLE *hEvent)
{	
	int nRtn;
	int nModuleIndex = 0;
	char msg[128];
	UINT nPortNo;
	char szIP[36];		

	EP_MSG_HEADER msgHeader;
	WSANETWORKEVENTS wsEvents;
	EP_SYSTEM_PARAM *pParam = NULL;
	pSocket->GetPeerName(szIP, sizeof(szIP), nPortNo);	//Get Client Side Ip Address and Port Number
	sprintf(msg, "Connection is attempted from IP %s", szIP);
	WriteLog(msg);

	//------------Send Version Information Request Command--------------------//
	MakeMsgHeader (&msgHeader, 0, 0, EP_CMD_VER_REQUEST);		//Send Version request Command
	
	msgHeader.nLength += 2;
	
	// 1. CRC16 계산 부분을 추가
	USHORT crcFull = 0xFFFF;
	byte crcHigh = 0xFF, crcLow = 0xFF;
	
	unsigned char		szTempTxBuffer[1024];			//CRC 계산을 위한 버퍼
	unsigned char		szTempRxBuffer[1024];			//CRC 계산을 위한 버퍼
	ZeroMemory( szTempRxBuffer, sizeof(szTempRxBuffer));
	ZeroMemory( szTempTxBuffer, sizeof(szTempTxBuffer));
	
	memcpy( szTempTxBuffer, &msgHeader, SizeofHeader());
	crcFull = fnCRC16_ccitt( szTempTxBuffer, SizeofHeader());

	crcHigh = (byte)((crcFull>>8)&0xFF);	// CRC[1]
	crcLow = (byte)(crcFull&0xFF);			// CRC[0]
	
	// TRACE("Send Msg CRC ====> High: 0x%02x, Low: 0x%02x\n", crcHigh, crcLow);

	szTempTxBuffer[SizeofHeader()] = crcLow;
	szTempTxBuffer[SizeofHeader()+1] = crcHigh;

	// nRtn = clientSide.Write(g_pstModule[nModuleIndex].szTxBuffer, g_pstModule[nModuleIndex].nTxLength);	
	
	nRtn = pSocket->Write(szTempTxBuffer, msgHeader.nLength);			//Modlule Version Information Request
	if(nRtn != msgHeader.nLength)
	{ 
		sprintf(msg, "Module version info send error = Code:%d = Return:%d\n", WSAGetLastError(), nRtn);
		nRtn = -1;
		goto CONNECTION_FAIL;
	}

	EP_MD_SYSTEM_DATA sysData;
	ZeroMemory(&sysData, sizeof(sysData));

	//------------wait Version Information Response Header--------------------//
	nRtn = ::WaitForMultipleObjects(2, hEvent, FALSE, _EP_MSG_TIMEOUT );		//Waiting Data Read or Shutdowm
	if(nRtn == WAIT_OBJECT_0)						// Client socket event FD_READ or Shutdown
	{
		Sleep(100);
		nRtn = ::WSAEnumNetworkEvents(pSocket->H(), hEvent[0], &wsEvents);	//Client side Socket Event
		if (wsEvents.lNetworkEvents & FD_READ)								//Read Event
		{
			/*
			nRtn = pSocket->Read(&msgHeader, SizeofHeader());				//Message Head Read
			Sleep(200);
			nRtn += pSocket->Read(&sysData , sizeof(sysData));				//Module Version Info Read

			if(nRtn != msgHeader.nLength || (nRtn-SizeofHeader()) != sizeof(sysData) || msgHeader.nCommand != EP_CMD_VER_DATA)
			{
				sprintf(msg, "Module information read fail %d (%d/%d)\n", WSAGetLastError(), nRtn, msgHeader.nLength);
				nRtn = -2;
				goto CONNECTION_FAIL;
			}		
			*/			
			
			//-----------------Read Module Information and Check validate ---------------------//
			nRtn = pSocket->Read(szTempRxBuffer, SizeofHeader()+sizeof(sysData)+2);				//Message Head Read
						
			memcpy(&msgHeader, szTempRxBuffer, SizeofHeader());
			memcpy(&sysData, &szTempRxBuffer[SizeofHeader()], sizeof(sysData));
			
			crcFull = fnCRC16_ccitt( szTempRxBuffer, msgHeader.nLength - 2 );

			crcHigh = (byte)((crcFull>>8)&0xFF);	// CRC[1]
			crcLow = (byte)(crcFull&0xFF);			// CRC[0]

			TRACE("Recv Msg CRC ====> High: 0x%02x, Low: 0x%02x \n", crcHigh, crcLow);				
			
			if( nRtn != msgHeader.nLength || nRtn - SizeofHeader() != sizeof(sysData)+2 || msgHeader.nCommand != EP_CMD_VER_DATA || 
				(byte)szTempRxBuffer[msgHeader.nLength-2] != crcLow || (byte)szTempRxBuffer[msgHeader.nLength-1] != crcHigh )
			{
				sprintf(msg, "Module information read fail %d (%d/%d)\n", WSAGetLastError(), nRtn, msgHeader.nLength);
				nRtn = -2;
				goto CONNECTION_FAIL;
			}
			
//			TRACE("Module ID %d Receive. Tray Type - %d, Total Jig - %d\n", sysData.nModuleID, sysData.wTrayType, sysData.wTotalJigNo);

			nModuleIndex = CheckModuleIDValidate(&sysData);		//Accepted Module Num and Version Check

// 			//////////////////////////////////////////////////////////////////////////
// 			sysData.wTotalTrayNo = 8;
// 			for(int  i = 0; i<sysData.wTotalTrayNo; i++)
// 			{
// 				sysData.awChInTray[i] = 1;
// 			}
// 			//////////////////////////////////////////////////////////////////////////
			
			if(nModuleIndex< 0)		//Accepted Module Error			
			{
				sprintf(msg, "Connection accepted module %d form %s is not registed. Code(%d).\n", sysData.nModuleID, szIP, nModuleIndex);
				AfxMessageBox(msg);
				nRtn = -3;
				goto CONNECTION_FAIL;
			}
			else
			{
				//ID가 중복된 Module이 접속을 한다.  
				if(g_pstModule[nModuleIndex].state != EP_STATE_LINE_OFF)
				{
					sprintf(msg, "Duplicated ID %s module is attempt to connect.", szIP, sysData.nModuleID);
					nRtn = -12;
					goto CONNECTION_FAIL;
				}
				else	//Accept Module
				{
					strcpy(g_pstModule[nModuleIndex].sysParam.szIPAddr, szIP);
					memcpy(&g_pstModule[nModuleIndex].sysData, &sysData, sizeof(sysData));	//Module Information receive
				}
			}
		}
		else if(wsEvents.lNetworkEvents & FD_CLOSE)		//Client Disconnected
		{
			sprintf(msg, "Module closed ::Module close event detected %d.\n", wsEvents.lNetworkEvents);
			nRtn = -16;
			goto CONNECTION_FAIL;
		}
		else	//wrong event or client Close Event
		{
			sprintf(msg, "Module disconnected ::Wrong event detected %d.\n", wsEvents.lNetworkEvents);
			nRtn = -4;
			goto CONNECTION_FAIL;
		}
	}
	else	//Wrong Event or Time Out
	{
		sprintf(msg, "Module disconnected :: Shutdown befor read module information.\n");
		nRtn = -5;
		goto CONNECTION_FAIL;
	}

	//------------wait Version Information Response Header--------------------//
/*	nRtn = ::WaitForMultipleObjects(2, hEvent, FALSE, _EP_MSG_TIMEOUT );		//Waiting Data Read or Shutdowm
	if(nRtn == WAIT_OBJECT_0)						// Client socket event FD_READ or Shutdown
	{
		Sleep(200);
		nRtn = ::WSAEnumNetworkEvents(pSocket->H(), hEvent[0], &wsEvents);	//Client side Socket Event
		if (wsEvents.lNetworkEvents & FD_READ)								//Read Event
		{
			//-----------------Read Module Information and Check validate ---------------------//
			nRtn = pSocket->Read(&msgHeader, SizeofHeader());				//Message Head Read
			TRACE("Message Header Read %d/%d/%d/%d/%d (%d)\n", msgHeader.nCommand, msgHeader.nID, msgHeader.nLength, msgHeader.wChannelNum, msgHeader.wGroupNum, nRtn);
		}
		else	//wrong event or client Close Event
		{
			sprintf(msg, "Module Disconnected ::Wrong Event Detected %d.\n", wsEvents.lNetworkEvents);
			nRtn = -4;
			goto CONNECTION_FAIL;
		}
	}
	else	//Wrong Event or Time Out
	{
		sprintf(msg, "Module Disconnected :: Shutdown Befor Read Module Information.\n");
		nRtn = -5;
		goto CONNECTION_FAIL;
	}

	//------------wait Version Information Response Body--------------------//
	nRtn = ::WaitForMultipleObjects(2, hEvent, FALSE, _EP_MSG_TIMEOUT );		//Waiting Data Read or Shutdowm
	if(nRtn == WAIT_OBJECT_0)												// Client socket event FD_READ or Shutdown
	{
		Sleep(200);
		nRtn = ::WSAEnumNetworkEvents(pSocket->H(), hEvent[0], &wsEvents);	//Client side Socket Event
		if (wsEvents.lNetworkEvents & FD_READ)								//Read Event
		{
			nRtn += pSocket->Read(&sysData , sizeof(sysData));				//Module Version Info Read
			
		}
		else	//wrong event or client Close Event
		{
			sprintf(msg, "Module Disconnected ::Wrong Event Detected %d.\n", wsEvents.lNetworkEvents);
			nRtn = -4;
			goto CONNECTION_FAIL;
		}
	}
	else	//Wrong Event or Time Out
	{
		sprintf(msg, "Module Disconnected :: Shutdown Befor Read Module Information.\n");
		nRtn = -5;
		goto CONNECTION_FAIL;
	}

	
	if(nRtn != msgHeader.nLength)
	{
		sprintf(msg, "Module Information Read Fail %d\ (%d/%d)\n", WSAGetLastError(), nRtn, msgHeader.nLength);
		nRtn = -2;
		goto CONNECTION_FAIL;
	}
	TRACE("Module ID %d Receive. %d/%d\n", sysData.nModuleID, sysData.wTrayType, sysData.wTotalJigNo);

	nModuleIndex = CheckModuleIDValidate(&sysData);		//Accepted Module Num and Version Check
	
	if(nModuleIndex< 0)		//Accepted Module Error			
	{
		sprintf(msg, "Accepted Module %d is not support. Code(%d).\n", sysData.nModuleID, nModuleIndex);
		WriteLog(msg);
		sprintf(msg, "제공 하지 않는 Module이 접속을 시도 합니다. code %d", nModuleIndex);
		AfxMessageBox(msg);
		nRtn = -3;
		goto CONNECTION_FAIL;
	}
	else
	{
		memcpy(&g_pstModule[nModuleIndex].sysData, &sysData, sizeof(sysData));	//Module Information receive
	}
*/	
	//------------Send Module Set Command --------------------//
//	EP_MD_SET_DATA sResponse;

	EP_MD_SET_DATA1 sMDSetData;
	EP_RESPONSE	response;
	
//	ZeroMemory(&sResponse, sizeof(sResponse));
	ZeroMemory(&sMDSetData, sizeof(sMDSetData));
	pParam = EPGetSysParam(sysData.nModuleID);
	
	if(pParam == NULL)
	{
		TRACE("Module ID %d\n", sysData.nModuleID);
		return -20;					//SBC에서 키보드 입력 대기 이후 종료되면 재접속 현상이 발생 
	}

/*	if(pParam->wModuleType != msgHeader.nID)
	{
		sprintf(msg, "Module %d :: Invalide Module Type(ModuleType %d/Host Type)\n", msgHeader.nID, pParam->wModuleType);
		nRtn = -21;
		goto CONNECTION_FAIL;
	}
*/
	MakeMsgHeader (&msgHeader, 0, 0, EP_CMD_MD_SET_DATA);

/*	if(sysData.nVersion < 0x3004)
		msgHeader.nLength += sizeof(EP_MD_SET_DATA);
	else
*/		
	msgHeader.nLength += sizeof(EP_MD_SET_DATA1) + 2;

/*	if(sysData.nVersion <= 0x3000)
	{
		sResponse.nAutoReportInterval = pParam->nAutoReportInterval;	//default report interval
	}
	else 
	{
*/	//	sResponse.nAutoReportInterval = 0;								//default report interval(not report)
		sMDSetData.nAutoReportInterval = 2;
//	}
	sMDSetData.bConnectionReTry = BYTE(nModuleIndex < 0 ? 0x00 : 0x01);
	sMDSetData.bAutoProcess =(BYTE)EPGetAutoProcess(sysData.nModuleID);
	
	sMDSetData.bUseTemp = (BYTE)pParam->bUseTempLimit;
	sMDSetData.bUSeJigTemp = (BYTE)pParam->bUseJigTempLimit;

	sMDSetData.sWanningTemp = pParam->sWanningTemp;
	sMDSetData.sCutTemp = pParam->sCutTemp;
	sMDSetData.sJigWanningTemp = pParam->sJigWanningTemp;
	sMDSetData.sJigCutTemp = pParam->sJigCutTemp;

	sMDSetData.bTrayReadType = EP_TRAY_REC_NVRAM;	//Tray ID Type		0: NVRAM	1: BarCode Reader
	sMDSetData.vHighLimit = pParam->lMaxVoltage;
	sMDSetData.iHighLimit = pParam->lMaxCurrent;
			
//	TRACE("Monitoring Interval %d\n", sResponse.nAutoReportInterval);
	
	ZeroMemory( g_pstModule[nModuleIndex].szTempTxBuffer, sizeof(g_pstModule[nModuleIndex].szTempTxBuffer));
	memcpy(g_pstModule[nModuleIndex].szTempTxBuffer, &msgHeader, SizeofHeader());
	memcpy(&g_pstModule[nModuleIndex].szTempTxBuffer[SizeofHeader()], &sMDSetData, sizeof(EP_MD_SET_DATA1));
		
	crcFull = 0xFFFF;
	crcHigh = 0xFF;
	crcLow = 0xFF;
	
	crcFull = fnCRC16_ccitt( g_pstModule[nModuleIndex].szTempTxBuffer, msgHeader.nLength - 2 );

	crcHigh = (byte)((crcFull>>8)&0xFF);	// CRC[1]
	crcLow = (byte)(crcFull&0xFF);			// CRC[0]

	// TRACE("Send Msg CRC ====> High: 0x%02x, Low: 0x%02x \n", crcHigh, crcLow);

	g_pstModule[nModuleIndex].szTempTxBuffer[msgHeader.nLength-2] = crcLow;
	g_pstModule[nModuleIndex].szTempTxBuffer[msgHeader.nLength-1] = crcHigh;
	
	nRtn = pSocket->Write(g_pstModule[nModuleIndex].szTempTxBuffer, msgHeader.nLength);
	
	if(nRtn != msgHeader.nLength)
	{
		sprintf(msg, "Module %d :: Write module set data fail (Code %d)\n", sysData.nModuleID, WSAGetLastError());
		nRtn = -6;
		goto CONNECTION_FAIL;
	}

	//------------Waiting for Response of Module Set Command --------------------//
	nRtn = ::WaitForMultipleObjects(2, hEvent, FALSE, _EP_MSG_TIMEOUT/*INFINITE*/);		//Waiting Data Read or Shutdown
	if(nRtn == WAIT_OBJECT_0)						// Client socket event FD_READ or Shutdown
	{
		Sleep(100);
		nRtn = ::WSAEnumNetworkEvents(pSocket->H(), hEvent[0], &wsEvents);	//Client side Socket Event
		if(nRtn == SOCKET_ERROR)
		{
			sprintf(msg, "Socket Error%d\n", WSAGetLastError());
			nRtn = -7;
			goto CONNECTION_FAIL;
		}

		if (wsEvents.lNetworkEvents & FD_READ)					//Read Event
		{
			//-----------------Read Module Information and Check validate ---------------------//
			ZeroMemory(g_pstModule[nModuleIndex].szTempRxBuffer, sizeof(g_pstModule[nModuleIndex].szTempRxBuffer));
			
			nRtn = pSocket->Read(g_pstModule[nModuleIndex].szTempRxBuffer, SizeofHeader() + sizeof(response)+2);
			memcpy(&msgHeader, g_pstModule[nModuleIndex].szTempRxBuffer, SizeofHeader());
			memcpy(&response, &g_pstModule[nModuleIndex].szTempRxBuffer[SizeofHeader()], sizeof(response));			
			
			crcFull = fnCRC16_ccitt( g_pstModule[nModuleIndex].szTempRxBuffer, msgHeader.nLength - 2 );

			crcHigh = (byte)((crcFull>>8)&0xFF);	// CRC[1]
			crcLow = (byte)(crcFull&0xFF);			// CRC[0]		
			
			if( nRtn != msgHeader.nLength || nRtn - SizeofHeader() != sizeof(response)+2 || 
				(byte)g_pstModule[nModuleIndex].szTempRxBuffer[msgHeader.nLength-2] != crcLow || (byte)g_pstModule[nModuleIndex].szTempRxBuffer[msgHeader.nLength-1] != crcHigh )
			{
				sprintf(msg, "Module %d :: Receive module set command response fail(%d/%d). (Code %d)\n", sysData.nModuleID, nRtn, msgHeader.nLength, WSAGetLastError());
				nRtn = -8;
				goto CONNECTION_FAIL;
			}
			else
			{
				if(msgHeader.nCommand == EP_CMD_RESPONSE || response.nCode == EP_ACK)
				{ 
//					TRACE("Receive Ack\n");
					return nModuleIndex;
				}
				else
				{
					sprintf(msg, "Module %d :: module set command receive Nack\n", sysData.nModuleID);
					nRtn = -9;
					goto CONNECTION_FAIL;
				}
			}
		}
		else if(wsEvents.lNetworkEvents & FD_CLOSE)		//Client Disconnected
		{
			sprintf(msg, "Module Closed ::Module close event detected %d.\n", wsEvents.lNetworkEvents);
			nRtn = -15;
			goto CONNECTION_FAIL;
		}
		else	//wrong event or client Close Event
		{
			//FD_READ 0x01/	FD_WRITE 0x02/FD_OOB 0x04/FD_ACCEPT 0x08/FD_CONNECT 0x10/FD_CLOSE 0x20
			sprintf(msg, "Module %d Disconnected :: Wrong event detected (Code %d)", sysData.nModuleID, wsEvents.lNetworkEvents);
			nRtn = -10;
			goto CONNECTION_FAIL;
		}
	}
	else	//wrong event or Server Close Event
	{
		sprintf(msg, "Server Disconnected :: Wrong event detected (Code %d)", nRtn);
		nRtn = -11;
		goto CONNECTION_FAIL;
	}

CONNECTION_FAIL:
	WriteLog(msg);
	TRACE(msg);
	return nRtn;
}

int GetMappingCh(int nSeqIndex, WORD wSystemType, int nMemberIndex)
{
	int index = nSeqIndex;
	if(g_nChannelIndexType == EP_CH_INDEX_SEQUENCE)			//Network로 들어온 순서대로 Channel을 선택
	{
		index = nSeqIndex;
	}
	else if(g_nChannelIndexType == EP_CH_INDEX_CUSTOM)		//LG OffLine
	{
		if(wSystemType == TRAY_TYPE_8_BY_32)
		{
			index = nSeqIndex/32*32+(31-nSeqIndex%32);
		}
		else if(wSystemType == TRAY_TYPE_16_BY_16)
		{
			index = nSeqIndex/16*16+(15-nSeqIndex%16);
		}
		else
		{
			index = nSeqIndex;
		}
	}
	else if(g_nChannelIndexType == EP_CH_INDEX_MAPPING)		//PC "mapping.dat" 파일에 정의된 순으로 정렬 
	{
		index = g_channelMap[nSeqIndex];
	}
	else	//EP_CH_INDEX_MEMBER, 보내준 Structure의 내부 Index에 의해 재배열 
	{
		index = nMemberIndex;
	}
	return index;
}

int CWizRawSocketListener::SetListenPort(int nPort)
{
	m_nPort = nPort;
	return TRUE;
}