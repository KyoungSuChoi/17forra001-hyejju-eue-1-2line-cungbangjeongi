#if !defined(AFX_CHCODERECORDSET_H__62701621_749B_4CFC_822C_F909F1705707__INCLUDED_)
#define AFX_CHCODERECORDSET_H__62701621_749B_4CFC_822C_F909F1705707__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ChCodeRecordSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CChCodeRecordSet recordset

class CChCodeRecordSet : public CRecordset
{
public:
	CChCodeRecordSet(CDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CChCodeRecordSet)

// Field/Param Data
	//{{AFX_FIELD(CChCodeRecordSet, CRecordset)
	long	m_Code;
	CString	m_Messge;
	//}}AFX_FIELD


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChCodeRecordSet)
	public:
	virtual CString GetDefaultConnect();    // Default connection string
	virtual CString GetDefaultSQL();    // Default SQL for Recordset
	virtual void DoFieldExchange(CFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHCODERECORDSET_H__62701621_749B_4CFC_822C_F909F1705707__INCLUDED_)
