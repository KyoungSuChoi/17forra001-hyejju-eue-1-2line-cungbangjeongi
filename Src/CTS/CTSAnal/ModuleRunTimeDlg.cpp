// ModuleRunTimeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "ModuleRunTimeDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CModuleRunTimeDlg dialog

bool CModuleRunTimeDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CModuleRunTimeDlg"), _T("TEXT_CModuleRunTimeDlg_CNT"), _T("TEXT_CModuleRunTimeDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CModuleRunTimeDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CModuleRunTimeDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}


CModuleRunTimeDlg::CModuleRunTimeDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CModuleRunTimeDlg::IDD, pParent)
{
	LanguageinitMonConfig();

	//{{AFX_DATA_INIT(CModuleRunTimeDlg)
	m_bTimeRange = FALSE;
	m_fromTime = 0;
	m_toTime = 0;
	//}}AFX_DATA_INIT
//	m_pDB = NULL;
	m_nDrawType = FALSE;
}

CModuleRunTimeDlg::~CModuleRunTimeDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


void CModuleRunTimeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CModuleRunTimeDlg)
	DDX_Check(pDX, IDC_TIME_RANGE, m_bTimeRange);
	DDX_DateTimeCtrl(pDX, IDC_DATETIMEPICKER1, m_fromTime);
	DDX_DateTimeCtrl(pDX, IDC_DATETIMEPICKER2, m_toTime);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CModuleRunTimeDlg, CDialog)
	//{{AFX_MSG_MAP(CModuleRunTimeDlg)
	ON_BN_CLICKED(IDC_SEARCH, OnSearch)
	ON_BN_CLICKED(IDC_TIME_RANGE, OnTimeRange)
	ON_BN_CLICKED(IDC_RUNT_COUNT_RADIO, OnRuntCountRadio)
	ON_BN_CLICKED(IDC_RUN_TIME_RADIO, OnRunTimeRadio)
	ON_COMMAND(ID_DELETE, OnDelete)
	ON_WM_DESTROY()
	ON_COMMAND(ID_SELECT_ALL, OnSelectAll)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_COMMAND(ID_RELOAD, OnReload)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_GRID_MOVECELL,OnMovedCurrentCell)
	ON_MESSAGE(WM_GRID_RIGHT_CLICK, OnRButtonClickedRowCol)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CModuleRunTimeDlg message handlers


BOOL CModuleRunTimeDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_fromTime = CTime::GetCurrentTime();
	m_toTime = CTime::GetCurrentTime();

	try
	{
		m_dbServer.Open(ODBC_PROD_DB_NAME);		
	}
    catch (CDBException* e)			//DataBase Open Fail
	{
		AfxMessageBox(e->m_strError);
    	e->Delete();
	}

	((CButton *)GetDlgItem(IDC_RUNT_COUNT_RADIO))->SetCheck(TRUE);

	InitTotalGrid();
	InitModuleGrid();
	InitGraph();

	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CModuleRunTimeDlg::InitTotalGrid()
{
	m_wndTotalRunGrid.SubclassDlgItem(IDC_USE_TIMES_GRID, this);
	m_wndTotalRunGrid.m_bSameRowSize = FALSE;
	m_wndTotalRunGrid.m_bSameColSize = FALSE;
//---------------------------------------------------------------------//
	m_wndTotalRunGrid.m_bCustomWidth = TRUE;
	m_wndTotalRunGrid.m_bCustomColor = FALSE;

	CRect rect;
	m_wndTotalRunGrid.GetWindowRect(rect);		//Step Grid Window Position
	ScreenToClient(&rect);

	int width = rect.Width()/100;
	m_wndTotalRunGrid.m_nWidth[1]	= (int)(width* 30);
	m_wndTotalRunGrid.m_nWidth[2]	= (int)(width* 25);
	m_wndTotalRunGrid.m_nWidth[3]	= 0;
	m_wndTotalRunGrid.m_nWidth[4]	= (int)(width* 45);

	m_wndTotalRunGrid.Initialize();
	m_wndTotalRunGrid.LockUpdate();
	m_wndTotalRunGrid.SetColWidth(0, 0, 50);

	m_wndTotalRunGrid.SetRowCount(0);
	m_wndTotalRunGrid.SetColCount(4);
	m_wndTotalRunGrid.SetDefaultRowHeight(18);

	m_wndTotalRunGrid.SetValueRange(CGXRange(0,1), TEXT_LANG[0]);//"Unit"
	m_wndTotalRunGrid.SetValueRange(CGXRange(0,2), TEXT_LANG[1]);//"운영횟수"
	m_wndTotalRunGrid.SetValueRange(CGXRange(0,3), TEXT_LANG[2]+"(sec)");//"운영시간합"
	m_wndTotalRunGrid.SetValueRange(CGXRange(0,4), TEXT_LANG[2]);//"운영시간합"

	m_wndTotalRunGrid.GetParam()->SetSortRowsOnDblClk(TRUE);
	m_wndTotalRunGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	//Row Header Setting
//	m_wndTotalRunGrid.SetStyleRange(CGXRange().SetCols(1),
//			CGXStyle().SetEnabled(TRUE).SetTextColor(RGB(0,0,0)).SetInterior(RGB(192,192,192)));
	m_wndTotalRunGrid.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(9).SetFaceName(TEXT_LANG[3])));//"굴림"
	m_wndTotalRunGrid.EnableCellTips();
	m_wndTotalRunGrid.SetStyleRange(CGXRange().SetTable(), CGXStyle().SetWrapText(FALSE).SetAutoSize(TRUE));
	m_wndTotalRunGrid.SetStyleRange(CGXRange().SetCols(3), CGXStyle().SetHorizontalAlignment(DT_RIGHT));
	m_wndTotalRunGrid.LockUpdate(FALSE);
	m_wndTotalRunGrid.Redraw();
}

void CModuleRunTimeDlg::InitModuleGrid()
{
	m_wndModuleRunGrid.SubclassDlgItem(IDC_DETAIL_TIME_GRID, this);
	m_wndModuleRunGrid.m_bSameRowSize = FALSE;
	m_wndModuleRunGrid.m_bSameColSize = FALSE;
	m_wndModuleRunGrid.m_bCustomWidth = TRUE;
	m_wndModuleRunGrid.m_bCustomColor = FALSE;

	CRect rect;
	m_wndModuleRunGrid.GetWindowRect(rect);		//Step Grid Window Position
	ScreenToClient(&rect);

	int width = rect.Width();
	m_wndModuleRunGrid.m_nWidth[1]	= (int)(width* 0.3f);
	m_wndModuleRunGrid.m_nWidth[2]	= (int)(width* 0.3f);
	m_wndModuleRunGrid.m_nWidth[3]	= 0;
	m_wndModuleRunGrid.m_nWidth[4]	= (int)(width* 0.2f);

	m_wndModuleRunGrid.Initialize();
	m_wndModuleRunGrid.LockUpdate();

	m_wndModuleRunGrid.SetRowCount(0);
	m_wndModuleRunGrid.SetColCount(4);
	m_wndModuleRunGrid.SetDefaultRowHeight(18);
	m_wndModuleRunGrid.SetColWidth(0, 0, 50);

	m_wndModuleRunGrid.SetValueRange(CGXRange(0, 1), TEXT_LANG[4]);//"시작 시각"
	m_wndModuleRunGrid.SetValueRange(CGXRange(0, 2), TEXT_LANG[5]);//"종료 시각"
	m_wndModuleRunGrid.SetValueRange(CGXRange(0, 3), TEXT_LANG[6]);//"ID"
	m_wndModuleRunGrid.SetValueRange(CGXRange(0, 4), TEXT_LANG[7]);//"운영 시간"

	m_wndModuleRunGrid.GetParam()->SetSortRowsOnDblClk(TRUE);
	m_wndModuleRunGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	//Row Header Setting
//	m_wndModuleRunGrid.SetStyleRange(CGXRange().SetCols(1),
//			CGXStyle().SetEnabled(TRUE).SetTextColor(RGB(0,0,0)).SetInterior(RGB(192,192,192)));
	m_wndModuleRunGrid.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(9).SetFaceName(TEXT_LANG[3])));//"굴림"
	m_wndModuleRunGrid.EnableCellTips();
	m_wndModuleRunGrid.SetStyleRange(CGXRange().SetTable(), CGXStyle().SetWrapText(FALSE).SetAutoSize(TRUE));
	m_wndModuleRunGrid.SetStyleRange(CGXRange().SetCols(1, 4), CGXStyle().SetHorizontalAlignment(DT_RIGHT));
	
	m_wndModuleRunGrid.LockUpdate(FALSE);
	m_wndModuleRunGrid.Redraw();
}

void CModuleRunTimeDlg::InitGraph()
{
	CRect rect;
	GetDlgItem(IDC_USE_COUNT_GRAPH)->GetWindowRect(rect);
	ScreenToClient(rect);
	m_hWndPE = PEcreate(PECONTROL_GRAPH, WS_BORDER, &rect, m_hWnd, IDC_ERROR_GRAPH);

	ASSERT(m_hWndPE);

	PEnset(m_hWndPE, PEP_nSUBSETS, 1);
	PEnset(m_hWndPE, PEP_nPOINTS,  256);

	PEnset(m_hWndPE, PEP_bPREPAREIMAGES, TRUE);
//	PEnset(m_hWndPE, PEP_nSCROLLINGSUBSETS , 1);		//동시에 보여지는 그래프 수를 나타 낸다. 
	PEnset(m_hWndPE, PEP_nDATAPRECISION, 0);			//테이블에서 소수점 아래 자리수 정하기
	PEnset(m_hWndPE, PEP_nGRIDLINECONTROL, PEGLC_YAXIS);	//Grid Line 표시 여부
//	PEnset(m_hWndPE, PEP_nALLOWZOOMING, PEAZ_HORZANDVERT);	//Zoom in/Out X,Y
	PEnset(m_hWndPE, PEP_bDATASHADOWS, FALSE);				//Data에 대한 그림자
	PEnset(m_hWndPE, PEP_nGRAPHPLUSTABLE, PEGPT_BOTH);

	PEszset(m_hWndPE, PEP_szMAINTITLEFONT, "굴림");//omit
	PEszset(m_hWndPE, PEP_szSUBTITLEFONT, "굴림");//omit


//	PEnset(m_hWndPE, PEP_bSHOWXAXISANNOTATIONS, TRUE);				//Annotation을 보여 준다.
//    PEnset(m_hWndPE, PEP_bSHOWANNOTATIONS, TRUE);					//Annotation을 사용 한다.
//	PEnset(m_hWndPE, PEP_bALLOWANNOTATIONCONTROL, TRUE);			//사용자가 선택 가능 
//	char MGText[] = "##########";
//	PEszset(m_hWndPE, PEP_szRIGHTMARGIN, MGText);
//	PEnset(m_hWndPE, PEP_nLINEANNOTATIONTEXTSIZE, 100);					//Annotation을 보여 준다.

	PEnset(m_hWndPE, PEP_bAUTOSCALEDATA, FALSE);					//This property controls whether the ProEssentials will automatically scale data that is very small or very large.
	

/*	int mas[2];
	mas[0] = 1;
	mas[1] = 1;
	int nYAxis = 2;
	PEvset(m_hWndPE, PEP_naMULTIAXESSUBSETS, mas, nYAxis);						//각 6개의 축에 보여줄 SubSet 갯수 지정(파일수/파일수/...) 
	PEvset(m_hWndPE, PEP_naOVERLAPMULTIAXES, &nYAxis, 1);						//1 Y축에 선택한 Item Y축을 한꺼번에 표시			
*/
//	PEnset(m_hWndPE, PEP_nALLOWZOOMING, TRUE);

	char szTitle[] = "횟수\t";//omit
	PEvset(m_hWndPE, PEP_szaSUBSETLABELS, szTitle, 1);			//4개의 그래프 라벨

	PEszset(m_hWndPE, PEP_szMAINTITLE, "Unit 별 작업 횟수");//omit
	PEszset(m_hWndPE, PEP_szSUBTITLE, "");
   
    PEszset(m_hWndPE, PEP_szXAXISLABEL, "Unit 번호");//omit

	PEnset (m_hWndPE, PEP_nPLOTTINGMETHOD, PEGPM_BAR);
	PEszset(m_hWndPE, PEP_szYAXISLABEL, "횟수");//omit
	PEnset(m_hWndPE, PEP_nPOINTS,  10);
	float data;
	for(int i =0; i<10; i++)
	{
		data = 0.0f;
		PEvsetcellEx(m_hWndPE, PEP_faYDATA, 0, i, &data);
	}

	PEreinitialize(m_hWndPE);
	PEresetimage(m_hWndPE, 0, 0);
}

void CModuleRunTimeDlg::SetDB(CDatabase *pDB)
{
	ASSERT(pDB);
//	m_pDB = pDB;
}

void CModuleRunTimeDlg::OnSearch() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	
	CWaitCursor wait;
	CRecordset rs(&m_dbServer);		//Module 목록 
	CRecordset rs1(&m_dbServer);
	
	CString strQuery, strTemp;
	char szBuff[36];
	ULONG totalTime = 0;

	strQuery = "SELECT ModuleName, count(*), sum(TotTime) FROM RunTime";
	if(m_bTimeRange)
	{
		strTemp.Format(" WHERE EndDate >= #%s# AND EndDate <= #%s#", m_fromTime.Format("%Y/%m/%d"), m_toTime.Format("%Y/%m/%d"));
		strQuery += strTemp;
	}
	strQuery += " GROUP BY ModuleName ORDER BY ModuleName";
	
	rs.Open( CRecordset::forwardOnly, strQuery);

	CDBVariant va;
	int count;
	if((count = m_wndTotalRunGrid.GetRowCount()) > 0 )
	{
		m_wndTotalRunGrid.RemoveRows(1, count);
	}
	count = 1;
	while(!rs.IsEOF())
	{
		m_wndTotalRunGrid.InsertRows(count, 1);

		rs.GetFieldValue((short)0, strTemp);
		m_wndTotalRunGrid.SetValueRange(CGXRange(count, 1), strTemp);
		rs.GetFieldValue((short)1, va, SQL_C_SLONG);
		m_wndTotalRunGrid.SetValueRange(CGXRange(count, 2), va.m_lVal);		//RunCount
		rs.GetFieldValue((short)2, va, SQL_C_SLONG);
		ConvertTime(szBuff , (ULONG) va.m_lVal);							//DB에는 Second 단위로 저장되어 있음 
		m_wndTotalRunGrid.SetValueRange(CGXRange(count, 3), va.m_lVal);	//Run Time
		m_wndTotalRunGrid.SetValueRange(CGXRange(count, 4), szBuff);		//Run Time
		totalTime += (ULONG)va.m_lVal;
		count++;
		rs.MoveNext();
	}
	rs.Close();

	if(count>1)
	{
		m_wndTotalRunGrid.InsertRows(count, 1);
		ConvertTime(szBuff , (ULONG)totalTime);
		m_wndTotalRunGrid.SetCoveredCellsRowCol(count, 1, count, 2);
		m_wndTotalRunGrid.SetValueRange(CGXRange(count, 1), TEXT_LANG[11]);		//Run Time	//"전체 합계"
		m_wndTotalRunGrid.SetValueRange(CGXRange(count, 3), totalTime);			//Run Time
		m_wndTotalRunGrid.SetValueRange(CGXRange(count, 4), szBuff);			//Run Time
		ShowGraph();
	}
	else
	{
		AfxMessageBox(TEXT_LANG[12]);//"해당 Data가 없습니다."
	}

}

void CModuleRunTimeDlg::OnTimeRange() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	GetDlgItem(IDC_DATETIMEPICKER1)->EnableWindow(m_bTimeRange);
	GetDlgItem(IDC_DATETIMEPICKER2)->EnableWindow(m_bTimeRange);

}

BOOL CModuleRunTimeDlg::ShowGraph()
{
	int row = m_wndTotalRunGrid.GetRowCount()-1;		//전체 수율 제외 
	if(row < 1)		return FALSE;

	PEnset(m_hWndPE, PEP_nPOINTS,  row);
	float data;
	CString str;
//	PEszset(m_hWndPE, PEP_szMAINTITLE, (LPSTR)(LPCTSTR)m_strTitle);
	if(m_nDrawType == FALSE)
	{
		PEszset(m_hWndPE, PEP_szMAINTITLE, "Unit 별 작업 횟수");//omit
   		PEszset(m_hWndPE, PEP_szYAXISLABEL, "횟수");//omit
	}
	else
	{
		PEszset(m_hWndPE, PEP_szMAINTITLE, "Unit 별 작업 시간");//omit
   		PEszset(m_hWndPE, PEP_szYAXISLABEL, "시간");//omit
	}

	for(int i =0; i<row; i++)
	{
		str = m_wndTotalRunGrid.GetValueRowCol(i+1, 1);
		PEvsetcellEx(m_hWndPE, PEP_szaPOINTLABELS, 0, i, (LPSTR)(LPCTSTR)str);//(void FAR*)(const char*)strTemp);
	
		if(m_nDrawType == FALSE)
		{
			str = m_wndTotalRunGrid.GetValueRowCol(i+1, 2);
		}
		else
		{
			//time
			str = m_wndTotalRunGrid.GetValueRowCol(i+1, 3);
		}
		data = (float)atof(str);
		PEvsetcellEx(m_hWndPE, PEP_faYDATA, 0, i, &data);

		TRACE("Graph Point to %d: %s, %f\n", i, str, data);
	}
	PEreinitialize(m_hWndPE);
	PEresetimage(m_hWndPE, 0, 0);
	Invalidate(FALSE);
	return TRUE;
}


LRESULT CModuleRunTimeDlg::OnMovedCurrentCell(WPARAM wParam, LPARAM lParam)
{
	ROWCOL nRow, nCol;
	CString      str;
//	int    row = 1;

	nCol = LOWORD(wParam);
	nRow = HIWORD(wParam);

	if(nRow < 1 || nCol < 2)	return 0;

	CMyGridWnd *pGrid = (CMyGridWnd *)lParam;
	if(pGrid == &m_wndTotalRunGrid)
	{
		CString strName = m_wndTotalRunGrid.GetValueRowCol(nRow, 1);
		SearchModuleLog(strName);
	}

	return  1;
}

LRESULT CModuleRunTimeDlg::OnRButtonClickedRowCol(WPARAM wParam, LPARAM lParam)
{
	ROWCOL nRow, nCol;
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;
	nCol = LOWORD(wParam);
	nRow = HIWORD(wParam);

	if(pGrid ==&m_wndModuleRunGrid)
	{
		if(nRow < 1)	return 0;

		CMenu menu, *pPopup;
		VERIFY(menu.LoadMenu(IDR_POP_UP)); 
		pPopup = menu.GetSubMenu(0); 
		CPoint ptMouse; 
		GetCursorPos(&ptMouse); 
		pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, ptMouse.x, ptMouse.y, this); 				
		
	}
	return 1;
}

void CModuleRunTimeDlg::SearchModuleLog(CString strName)
{
	CRecordset rs(&m_dbServer);
	CString strQuery, strTemp;

	BeginWaitCursor();

	GetDlgItem(IDC_MODULE_LOG)->SetWindowText(strName);

	strQuery.Format("SELECT TotTime, EndDate, ID FROM RunTime WHERE ModuleName = '%s'", strName);
	if(m_bTimeRange)
	{
		strTemp.Format(" HAVING EndDate >= #%s# AND EndDate <= #%s#", m_fromTime.Format("%Y/%m/%d"), m_toTime.Format("%Y/%m/%d"));
		strQuery += strTemp;
	}
	
	rs.Open( CRecordset::forwardOnly, strQuery);

	ULONG	totalTime = 0;
	char szBuff[32];
	CDBVariant va;
	int count;

	if((count = m_wndModuleRunGrid.GetRowCount()) > 0 )
	{
		m_wndModuleRunGrid.RemoveRows(1, count);
	}

	count = 1;
	while(!rs.IsEOF())
	{
		m_wndModuleRunGrid.InsertRows(count, 1);

		rs.GetFieldValue((short)0, va, SQL_C_SLONG);
		ConvertTime(szBuff , (ULONG) va.m_lVal);
		m_wndModuleRunGrid.SetValueRange(CGXRange(count, 4), szBuff);			//Run Time
		CTimeSpan runTime((ULONG)(va.m_lVal));
		totalTime += (ULONG)va.m_lVal;

		rs.GetFieldValue((short)1, va);					//Date Time
		if(va.m_dwType == DBVT_DATE)
		{
			CTime endTime(va.m_pdate->year, va.m_pdate->month, va.m_pdate->day, va.m_pdate->hour, va.m_pdate->minute, va.m_pdate->second);
			CTime startTime;
			startTime = endTime-runTime;
			m_wndModuleRunGrid.SetValueRange(CGXRange(count, 2), endTime.Format("%Y/%m/%d %H:%M:%S"));		//END Time
			m_wndModuleRunGrid.SetValueRange(CGXRange(count, 1), startTime.Format("%Y/%m/%d %H:%M:%S"));	//Start Time
		}

		rs.GetFieldValue((short)2, va, SQL_C_SLONG);
		m_wndModuleRunGrid.SetValueRange(CGXRange(count, 3), va.m_lVal);			//ID
		
		count++;
		rs.MoveNext();
	}

	if(count > 1)
	{
		m_wndModuleRunGrid.InsertRows(count, 1);
		ConvertTime(szBuff , (ULONG)totalTime);
		m_wndModuleRunGrid.SetStyleRange(CGXRange(count, 1, count, 2), CGXStyle().SetHorizontalAlignment(DT_CENTER));
		m_wndModuleRunGrid.SetCoveredCellsRowCol(count, 1, count, 2);
		m_wndModuleRunGrid.SetValueRange(CGXRange(count, 1), TEXT_LANG[11]);	//Run Time    //"전체 합계"
		m_wndModuleRunGrid.SetValueRange(CGXRange(count, 3), szBuff);				//Run Time
	}
	rs.Close();
	EndWaitCursor();
}

void CModuleRunTimeDlg::OnRuntCountRadio() 
{
	// TODO: Add your control notification handler code here
	m_nDrawType = FALSE;
	ShowGraph();
}

void CModuleRunTimeDlg::OnRunTimeRadio() 
{
	// TODO: Add your control notification handler code here
	m_nDrawType = TRUE;
	ShowGraph();	
}

void CModuleRunTimeDlg::OnDelete() 
{
	// TODO: Add your command handler code here
	CRowColArray	awRows;
	m_wndModuleRunGrid.GetSelectedRows(awRows);
	int nSelNo = awRows.GetSize();

	if(nSelNo == 0)  
	{
		AfxMessageBox(TEXT_LANG[15]);//"삭제할 레코드가 없습니다."
		return;
	}
	
	if(AfxMessageBox(TEXT_LANG[16],MB_YESNO|MB_ICONQUESTION) == IDNO)//"레코드가 삭제되어 복구되지 않습니다. 삭제 하시겠습니까?"
		return;
	

	CWaitCursor wait;
	CRecordset rs(&m_dbServer);
	CString strQuery, strTemp;

	ROWCOL nRow;
	m_dbServer.BeginTrans();
	for(int i=0; i<nSelNo; i++)
	{
		nRow = awRows[i];
		if(nRow > 0)
		{
			strQuery.Format("DELETE FROM RunTime WHERE ID = %s", m_wndModuleRunGrid.GetValueRowCol(nRow, 3));
			m_dbServer.ExecuteSQL(strQuery);
		}
	}
	m_dbServer.CommitTrans();

	OnReload();
	OnSearch();
}

void CModuleRunTimeDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here
	if(m_dbServer.IsOpen())
	{
		m_dbServer.Close();
	}
	
}

void CModuleRunTimeDlg::OnSelectAll() 
{
	// TODO: Add your command handler code here
	m_wndModuleRunGrid.SelectRange(CGXRange().SetTable());
}

void CModuleRunTimeDlg::OnEditCopy() 
{
	// TODO: Add your command handler code here
	m_wndModuleRunGrid.Copy();	
}

void CModuleRunTimeDlg::OnReload() 
{
	// TODO: Add your command handler code here
	ROWCOL nRow, nCol;
	if(m_wndTotalRunGrid.GetCurrentCell(nRow, nCol))
	{
		if(nRow > 0)
		{
			CString strName = m_wndTotalRunGrid.GetValueRowCol(nRow, 1);
			SearchModuleLog(strName);
		}
	}
}
