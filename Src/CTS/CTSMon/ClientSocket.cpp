// ClientSocket.cpp : implementation file
//

#include "stdafx.h"
//#include "Telnet.h"
#include "ClientSocket.h"

//#include "TelnetView.h"
//#include "TelnetDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CClientSocket

CClientSocket::CClientSocket(HWND hWnd)
{
	m_pHwnd = hWnd;
}

CClientSocket::~CClientSocket()
{
}


// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CClientSocket, CAsyncSocket)
	//{{AFX_MSG_MAP(CClientSocket)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// CClientSocket member functions

void CClientSocket::OnClose(int nErrorCode) 
{
//	AfxMessageBox("Connection Closed",MB_OK);
	
	CAsyncSocket::OnClose(nErrorCode);
/*	if(!IsWindow(cView->m_hWnd)) return;
	if(!IsWindowVisible(cView->m_hWnd)) return;
	cView->GetDocument()->OnCloseDocument();
*/
	if(m_pHwnd)
		SendMessage(m_pHwnd, WM_SOCKET_DISCONNECTED, 0, 0);
}

void CClientSocket::OnConnect(int nErrorCode) 
{
	if(m_pHwnd)
		SendMessage(m_pHwnd, WM_SOCKET_CONNECTED, 0, 0);

	CAsyncSocket::OnConnect(nErrorCode);
}

void CClientSocket::OnOutOfBandData(int nErrorCode) 
{
	ASSERT(FALSE); //Telnet should not have OOB data
	CAsyncSocket::OnOutOfBandData(nErrorCode);
}

void CClientSocket::OnReceive(int nErrorCode) 
{
//	cView->ProcessMessage(this);
	if(m_pHwnd)
		SendMessage(m_pHwnd, WM_RECEIVE_DATA, 0, 0);
}

void CClientSocket::OnSend(int nErrorCode) 
{
	CAsyncSocket::OnSend(nErrorCode);
}
