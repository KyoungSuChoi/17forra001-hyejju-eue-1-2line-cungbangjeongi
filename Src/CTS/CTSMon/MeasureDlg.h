#if !defined(AFX_MEASUREDLG_H__D255D4FC_DE4E_4391_8FE9_639A99C40CCA__INCLUDED_)
#define AFX_MEASUREDLG_H__D255D4FC_DE4E_4391_8FE9_639A99C40CCA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MeasureDlg.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// CMeasureDlg dialog
#define SELECT_MODE_MANUAL	0
#define SELECT_MODE_AUTO	1
#define MAX_LIST_VALUE		256
//#define MAX_LIST_VALUE		10000
#define TIMER_SENDCHNUM_INTERVAL	1000

#include "CTSMonDoc.h"
#include "MeasurePoint.h"
#include "afxwin.h"

class CMeasureDlg : public CDialog
{
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();
	virtual ~CMeasureDlg();

	// Construction
public:
	void SetModuleID(int nModuleID);
	void FunWorkEndReveive();
	void fnUpdatePointData( int nModuleID );
//	BOOL	m_bCheckReceiveReadyFlag;		// FALSE:대기 TRUE:준비
	BOOL    m_bReceiveDataFlag;				// SBC에서 수신 됨
	BOOL    m_bExit;						// 종료 불가능
	UINT	m_CkDelayReceiveTime;			// SendMessage 한후 데이터 받는 시간을 기다린다.
	BOOL	m_bVCommOpen;
	BOOL	m_bICommOpen;
	CString m_strResultFileName;
	POSITION m_pos;
	BOOL	InitComm();
	float	GetADVal(int nType);
	float	GetDVMData(int nType);
	BOOL	ResetData();
	BOOL	GetSelectdChNum();
	BOOL	SendChDataToSbc();		// SBC 로 측정할 CH 값을 보낸다.	
	BOOL	UpdateResultDataOnList( EP_REAL_MEASURE_RESULT_DATA  * pRealMeasureData );
	BOOL	bFileFinder(CString str_path);
	CCTSMonDoc* m_pDoc;
	CList<LONG, LONG&> m_ChList;
//	INT		m_RemainRepeatNum;
	UINT	m_HeartBeat;
	CMeasurePoint m_pMeasPoint;
	CMeasureDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CMeasureDlg)
	enum { IDD = IDD_MEASURE_DLG };
	CComboBox	m_MeasureTypeSel;
	CComboBox	m_ModuleList;
	CListCtrl	m_List;
	CString	m_strFileName;
	CString	m_strChSelect;
	int		m_nOptSelectChannel;
	UINT	m_uiRecordTime;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMeasureDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	float	m_fShunt;
	UINT	m_nVPort;
	UINT	m_nIPort;
	BOOL	m_bAuto;
//	BOOL	m_bPause;		// FALSE:아님 TRUE:PAUSE 상태에서 재실행
	UINT	m_nMeasureCount;
	UINT	m_nInterval;
	int		m_nModuleID;
	int		m_nChannelIndex;
	BOOL	InitList();
	int		m_nCount;
	BOOL	AddLog();
//	int		m_nTotalCycleNum;
//	int		m_nRemainChNum;
	CSerialPort	    m_PortV;
	CSerialPort	    m_PortI;
	// EPWM_RM_END_RECEIVED
//	CString m_strModuleName;
//	CString m_strGroupName;	
//	int m_nModulePerRack;
//	BOOL m_bUseRackIndex;
//	BOOL m_bUseGroupSet;
//	CString GetModuleName(int nModuleID, int nGroupIndex = 0);

	// Generated message map functions
	//{{AFX_MSG(CMeasureDlg)
	afx_msg void OnFileName();
	afx_msg void OnClear();
	afx_msg void OnTimer(UINT nIDEvent);
	virtual BOOL OnInitDialog();
	afx_msg void OnStart();
	afx_msg void OnStop();
	afx_msg void OnClose();
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnOption();
	afx_msg void OnSelchangeModuleSelCombo();
	afx_msg void OnSelectMeasureOptionManual();
	afx_msg void OnSelectMeasureOptionAuto();
	afx_msg void OnOptAllChannel();
	afx_msg void OnOptPartChannel();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	CComboBox m_ctrlRange;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MEASUREDLG_H__D255D4FC_DE4E_4391_8FE9_639A99C40CCA__INCLUDED_)
