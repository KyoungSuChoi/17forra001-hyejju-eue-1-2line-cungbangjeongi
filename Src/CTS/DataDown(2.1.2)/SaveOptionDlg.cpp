// SaveOptionDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DataDown.h"
#include "SaveOptionDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSaveOptionDlg dialog


CSaveOptionDlg::CSaveOptionDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSaveOptionDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSaveOptionDlg)
	m_nDeltaTime = 1;
	m_nDeltaV = 0;
	m_nDeltaI = 0;
	m_bStepNo = FALSE;
	m_bState = FALSE;
	m_bStepTime = TRUE;
	m_bVoltage = TRUE;
	m_bCurrent = TRUE;
	m_bCapacity = TRUE;
	m_bImpedance = FALSE;
	m_bWatt = FALSE;
	m_bWattHour = TRUE;
	m_bGradeCode = FALSE;
	m_bCellCode = FALSE;
	m_nVUnit = 0;
	m_nTUnit = 0;
	m_nIUnit = 1;
	m_nCUnit = 1;
	m_nZUnit = 1;
	m_nWUnit = 1;
	m_nWhUnit = 1;
	m_nVPrecision = 3;
	m_nIPrecision = 1;
	m_nCPrecision = 1;
	m_nZPrecision = 1;
	m_nWPrecision = 1;
	m_nWhPrecision = 1;
	m_bTimeSum = FALSE;
	m_nTypeSelCombo = 0;
	m_bSaveCharge = FALSE;
	m_bSaveDischarge = FALSE;
	m_bSaveRest = FALSE;
	m_bSaveImpdeacne = FALSE;
	m_nTotTUnit = -1;
	m_bTempData = FALSE;
	m_bStepType = TRUE;
	m_nHistoryCount = 0;
	m_bAuxVData = FALSE;
	m_bSaveDate = FALSE;
	//}}AFX_DATA_INIT

	LoadRegSetting();
}


void CSaveOptionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSaveOptionDlg)
	DDX_Text(pDX, IDC_DELTA_TIME_EDIT, m_nDeltaTime);
	DDX_Text(pDX, IDC_DELTA_V_EDIT, m_nDeltaV);
	DDX_Text(pDX, IDC_DELTA_I_EDIT, m_nDeltaI);
	DDX_Check(pDX, IDC_STEPNO_CHECK, m_bStepNo);
	DDX_Check(pDX, IDC_STATE_CHECK, m_bState);
	DDX_Check(pDX, IDC_TIME_CHECK, m_bStepTime);
	DDX_Check(pDX, IDC_VOTLAGE_CHECK, m_bVoltage);
	DDX_Check(pDX, IDC_CURRENT_CHECK, m_bCurrent);
	DDX_Check(pDX, IDC_CAPACITY_CHECK, m_bCapacity);
	DDX_Check(pDX, IDC_IMP_CHECK, m_bImpedance);
	DDX_Check(pDX, IDC_WATT_CHECK, m_bWatt);
	DDX_Check(pDX, IDC_WATTHOUR_CHECK, m_bWattHour);
	DDX_Check(pDX, IDC_GRADE_CHECK, m_bGradeCode);
	DDX_Check(pDX, IDC_CODE_CHECK, m_bCellCode);
	DDX_CBIndex(pDX, IDC_V_UNIT_COMBO, m_nVUnit);
	DDX_CBIndex(pDX, IDC_TIME_UNIT_COMBO, m_nTUnit);
	DDX_CBIndex(pDX, IDC_I_UNIT_COMBO, m_nIUnit);
	DDX_CBIndex(pDX, IDC_C_UNIT_COMBO, m_nCUnit);
	DDX_CBIndex(pDX, IDC_Z_UNIT_COMBO, m_nZUnit);
	DDX_CBIndex(pDX, IDC_W_UNIT_COMBO, m_nWUnit);
	DDX_CBIndex(pDX, IDC_WH_UNIT_COMBO, m_nWhUnit);
	DDX_CBIndex(pDX, IDC_V_PRECISION_COMBO, m_nVPrecision);
	DDX_CBIndex(pDX, IDC_I_PRECISION_COMBO, m_nIPrecision);
	DDX_CBIndex(pDX, IDC_C_PRECISION_COMBO, m_nCPrecision);
	DDX_CBIndex(pDX, IDC_Z_PRECISION_COMBO, m_nZPrecision);
	DDX_CBIndex(pDX, IDC_W_PRECISION_COMBO, m_nWPrecision);
	DDX_CBIndex(pDX, IDC_WH_PRECISION_COMBO, m_nWhPrecision);
	DDX_Check(pDX, IDC_TIME_SUM_CHECK, m_bTimeSum);
	DDX_CBIndex(pDX, IDC_TYPE_COMBO, m_nTypeSelCombo);
	DDX_Check(pDX, IDC_CHECK1, m_bSaveCharge);
	DDX_Check(pDX, IDC_CHECK2, m_bSaveDischarge);
	DDX_Check(pDX, IDC_CHECK3, m_bSaveRest);
	DDX_Check(pDX, IDC_CHECK4, m_bSaveImpdeacne);
	DDX_CBIndex(pDX, IDC_TIME_UNIT_COMBO2, m_nTotTUnit);
	DDX_Check(pDX, IDC_TEMP_CHECK, m_bTempData);
	DDX_Check(pDX, IDC_TYEP_CHECK, m_bStepType);
	DDX_Text(pDX, IDC_EDIT1, m_nHistoryCount);
//	DDV_MinMaxUInt(pDX, m_nHistoryCount, 1, 10000);
	DDX_Check(pDX, IDC_CHECK_AUX_V, m_bAuxVData);
	DDX_Check(pDX, IDC_CHECK6, m_bSaveDate);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSaveOptionDlg, CDialog)
	//{{AFX_MSG_MAP(CSaveOptionDlg)
	ON_BN_CLICKED(IDC_BUTTON_DIR, OnButtonDir)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSaveOptionDlg message handlers

BOOL CSaveOptionDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	
	LoadRegSetting();

#ifdef _EDLC_TEST_SYSTEM
	((CComboBox *)GetDlgItem(IDC_C_UNIT_COMBO))->ResetContent();
	((CComboBox *)GetDlgItem(IDC_C_UNIT_COMBO))->AddString("F");
	((CComboBox *)GetDlgItem(IDC_C_UNIT_COMBO))->AddString("mF");
#endif

	GetDlgItem(IDC_EDIT_TEMP_DIR)->SetWindowText(m_strTempDir);

	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSaveOptionDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "DeltaT", m_nDeltaTime);
	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "DeltaV", m_nDeltaV);
	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "DeltaI", m_nDeltaI);
	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "SaveType", m_nTypeSelCombo);

	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "StepNo", m_bStepNo);
	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "StepType", m_bStepType);
	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "State", m_bState);
	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "Time", m_bStepTime);
	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "Votlage", m_bVoltage);
	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "Current", m_bCurrent);
	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "Capacity", m_bCapacity);
	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "Impedance", m_bImpedance);
	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "Watt", m_bWatt);
	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "WattHour", m_bWattHour);
	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "GradeCode", m_bGradeCode);
	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "CellCode", m_bCellCode);

	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "TotTUnit", m_nTotTUnit);
	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "TUnit", m_nTUnit);
	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "VUnit", m_nVUnit);
	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "IUnit", m_nIUnit);
	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "CUnit", m_nCUnit);
	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "ZUnit", m_nZUnit);
	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "WUnit", m_nWUnit);
	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "WhUnit", m_nWhUnit);

	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "TimeSum", m_bTimeSum);
	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "VPrecision", m_nVPrecision);
	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "IPrecision", m_nIPrecision);
	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "CPrecision", m_nCPrecision);
	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "ZPrecision", m_nZPrecision);
	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "WPrecision", m_nWPrecision);
	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "WhPrecision", m_nWhPrecision);

	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "TypeCharge", m_bSaveCharge);
	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "TypeDischarge", m_bSaveDischarge);
	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "TypeRest", m_bSaveRest);
	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "TypeImpedance", m_bSaveImpdeacne);

	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "Temperature", m_bTempData);
	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "AuxV", m_bAuxVData);

	AfxGetApp()->WriteProfileInt(REG_SEC_NAME, "Date", m_bSaveDate);
		
//	AfxGetApp()->WriteProfileInt(REG_SEC_NAME,"HistoryCount", m_nHistoryCount);

//	AfxGetApp()->WriteProfileString(REG_SEC_NAME, "TempDir", m_strTempDir);

	CDialog::OnOK();
}

BOOL CSaveOptionDlg::LoadRegSetting()
{
	m_nDeltaTime =	AfxGetApp()->GetProfileInt(REG_SEC_NAME, "DeltaT", 1);
	m_nDeltaV	=	AfxGetApp()->GetProfileInt(REG_SEC_NAME, "DeltaV", 0);
	m_nDeltaI	=	AfxGetApp()->GetProfileInt(REG_SEC_NAME, "DeltaI", 0);
	m_nTypeSelCombo = AfxGetApp()->GetProfileInt(REG_SEC_NAME, "SaveType", 0);

	m_bStepNo = AfxGetApp()->GetProfileInt(REG_SEC_NAME, "StepNo", 0);
	m_bStepType = AfxGetApp()->GetProfileInt(REG_SEC_NAME, "StepType", 1);
	m_bState = AfxGetApp()->GetProfileInt(REG_SEC_NAME, "State", 0);
	m_bStepTime = AfxGetApp()->GetProfileInt(REG_SEC_NAME, "Time", 1);
	m_bVoltage = AfxGetApp()->GetProfileInt(REG_SEC_NAME, "Votlage", 1);
	m_bCurrent = AfxGetApp()->GetProfileInt(REG_SEC_NAME, "Current", 1);
	m_bCapacity = AfxGetApp()->GetProfileInt(REG_SEC_NAME, "Capacity", 1);
	m_bImpedance = AfxGetApp()->GetProfileInt(REG_SEC_NAME, "Impedance", 0);
	m_bWatt = AfxGetApp()->GetProfileInt(REG_SEC_NAME, "Watt", 0);
	m_bWattHour = AfxGetApp()->GetProfileInt(REG_SEC_NAME, "WattHour", 1);
	m_bGradeCode = AfxGetApp()->GetProfileInt(REG_SEC_NAME, "GradeCode", 0);
	m_bCellCode = AfxGetApp()->GetProfileInt(REG_SEC_NAME, "CellCode", 0);

	m_nTotTUnit = AfxGetApp()->GetProfileInt(REG_SEC_NAME, "TotTUnit", 0);
	m_nTUnit =  AfxGetApp()->GetProfileInt(REG_SEC_NAME, "TUnit", 0);
	m_nVUnit =  AfxGetApp()->GetProfileInt(REG_SEC_NAME, "VUnit", 0);
	m_nIUnit =  AfxGetApp()->GetProfileInt(REG_SEC_NAME, "IUnit", 1);
	m_nCUnit =  AfxGetApp()->GetProfileInt(REG_SEC_NAME, "CUnit", 1);
	m_nZUnit =  AfxGetApp()->GetProfileInt(REG_SEC_NAME, "ZUnit", 1);
	m_nWUnit =  AfxGetApp()->GetProfileInt(REG_SEC_NAME, "WUnit", 1);
	m_nWhUnit =  AfxGetApp()->GetProfileInt(REG_SEC_NAME, "WhUnit", 1);

	m_bTimeSum = AfxGetApp()->GetProfileInt(REG_SEC_NAME, "TimeSum", 0);
	m_nVPrecision = AfxGetApp()->GetProfileInt(REG_SEC_NAME, "VPrecision", 3);
	m_nIPrecision = AfxGetApp()->GetProfileInt(REG_SEC_NAME, "IPrecision", 1);
	m_nCPrecision = AfxGetApp()->GetProfileInt(REG_SEC_NAME, "CPrecision", 1);
	m_nZPrecision = AfxGetApp()->GetProfileInt(REG_SEC_NAME, "ZPrecision", 1);
	m_nWPrecision = AfxGetApp()->GetProfileInt(REG_SEC_NAME, "WPrecision", 1);
	m_nWhPrecision = AfxGetApp()->GetProfileInt(REG_SEC_NAME, "WhPrecision", 1);

	m_bSaveCharge = AfxGetApp()->GetProfileInt(REG_SEC_NAME, "TypeCharge", TRUE);
	m_bSaveDischarge = AfxGetApp()->GetProfileInt(REG_SEC_NAME, "TypeDischarge", TRUE);
	m_bSaveRest = AfxGetApp()->GetProfileInt(REG_SEC_NAME, "TypeRest", TRUE);
	m_bSaveImpdeacne = AfxGetApp()->GetProfileInt(REG_SEC_NAME, "TypeImpedance", TRUE);

	m_bTempData = AfxGetApp()->GetProfileInt(REG_SEC_NAME, "Temperature", TRUE);
	m_bAuxVData = AfxGetApp()->GetProfileInt(REG_SEC_NAME, "AuxV", TRUE);

	m_bSaveDate = AfxGetApp()->GetProfileInt(REG_SEC_NAME, "Date", TRUE);

//	m_nHistoryCount = AfxGetApp()->GetProfileInt(REG_SEC_NAME,"HistoryCount", BF_MAX_TEST_REAL_TIME_DATA);
	return TRUE;
}

CString CSaveOptionDlg::ValueString(double dData, int nType)
{
	CString strMsg, strTemp;
	double dTemp;
	char szTemp[8];

	dTemp = dData;
	switch(nType)
	{
	case EP_VOLTAGE:			//voltage
		if(m_nVUnit == 0)		//V단위로 변경 
		{
			dTemp = dTemp / 1000.0f;	//LONG2FLOAT(Value);
		}
		
		if(m_nVPrecision > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nVPrecision);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}		
		break;

	case EP_CURRENT:		//current
//		if(dTemp < 0)	dTemp = -dTemp;			//양수 표기
		
		//current
		if(m_nIUnit == 0)
		{
			dTemp = dTemp/1000.0f;
		}  

		if(m_nIPrecision > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nIPrecision);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}
		break;
			
	case EP_WATT	:				
		if(dTemp < 0)	dTemp = -dTemp;			//양수 표기

		if(m_nWUnit == 0)	//W
		{
			dTemp = dTemp/1000.0f;
		}

		if(m_nWPrecision > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nIPrecision);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}
		break;
	
	case EP_WATT_HOUR:			
	
		if(dTemp < 0)	dTemp = -dTemp;			//양수 표기

		if(m_nWhUnit == 0)
		{
			dTemp = dTemp/1000.0f;
		}

		if(m_nWhPrecision > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nWhPrecision);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}
		break;
	
	case EP_CAPACITY:		//capacity

		if(dTemp < 0)	dTemp = -dTemp;			//양수 표기

		if(m_nCUnit == 0)
		{
			dTemp = dTemp/1000.0f;
		}
		if(m_nCPrecision > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nCPrecision);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}
		break;

	case EP_TOT_TIME:
		//sbc에서 100msec단위 전송되어 온다.
		if(m_nTotTUnit == 1)	//sec 표시
		{
			strMsg.Format("%.1f", dTemp);
		}
		else if(m_nTotTUnit == 2)	//Minute 표시
		{
			strMsg.Format("%.2f", dTemp/60.0f);
		}
		else
		{
			CTimeSpan timeSpan((ULONG)dTemp);
			if(timeSpan.GetDays() > 0)
			{
				strMsg =  timeSpan.Format("%Dd %H:%M:%S");
			}
			else
			{
				strMsg = timeSpan.Format("%H:%M:%S");
			}
		}
		break;

	case EP_STEP_TIME:
		//sbc에서 100msec단위 전송되어 온다.
		if(m_nTUnit == 1)	//sec 표시
		{
			strMsg.Format("%.1f", dTemp);
		}
		else if(m_nTUnit == 2)	//Minute 표시
		{
			strMsg.Format("%.2f", dTemp/60.0f);
		}
		else
		{
			CTimeSpan timeSpan((ULONG)dTemp);
			if(timeSpan.GetDays() > 0)
			{
				strMsg =  timeSpan.Format("%D %H:%M:%S");
			}
			else
			{
				strMsg = timeSpan.Format("%H:%M:%S");
			}
		}
		break;

	case EP_STEP_NO:
	default:
		strMsg.Format("%d", (int)dTemp);
		break;
	}
	
	return strMsg;
}

CString CSaveOptionDlg::UnitString(int type)
{
	CString strMsg;
	switch(type)
	{
	case EP_VOLTAGE:			//voltage
		if(m_nVUnit == 0)		//V단위로 변경 
			strMsg = "V";
		else 
			strMsg = "mV";
		break;

	case EP_CURRENT:		//current
		//current
		if(m_nIUnit == 0)
			strMsg = "A";
		else 
			strMsg = "mA";
		break;
			
	case EP_WATT	:				
		if(m_nWUnit == 0)	//W
			strMsg = "W";
		else 
			strMsg = "mW";
		break;
	
	case EP_WATT_HOUR:			
		if(m_nWhUnit == 0)
			strMsg = "Wh";
		else 
			strMsg = "mWh";
		break;
	
	case EP_CAPACITY:		//capacity
#ifdef _EDLC_TEST_SYSTEM
		if(m_nCUnit == 0)
			strMsg = "F";
		else 
			strMsg = "mF";
#else
		if(m_nCUnit == 0)
			strMsg = "Ah";
		else 
			strMsg = "mAh";
#endif

		break;

	case EP_TOT_TIME:
		//sbc에서 100msec단위 전송되어 온다.
		if(m_nTotTUnit == 1)	//sec 표시
		{
			strMsg = "sec";
		}
		else if(m_nTotTUnit == 2)	//Minute 표시
		{
			strMsg = "min";
		}
		break;
	case EP_STEP_TIME:

		//sbc에서 100msec단위 전송되어 온다.
		if(m_nTUnit == 1)	//sec 표시
		{
			strMsg = "sec";
		}
		else if(m_nTUnit == 2)	//Minute 표시
		{
			strMsg = "min";
		}
		break;
	default:
		strMsg.Empty();
		break;
	}
	return strMsg;
}

void CSaveOptionDlg::OnButtonDir() 
{
	// TODO: Add your control notification handler code here
	CFolderDialog dlg(m_strTempDir);
	if( dlg.DoModal() == IDOK)
	{
		m_strTempDir = dlg.GetPathName();
		GetDlgItem(IDC_EDIT_TEMP_DIR)->SetWindowText(m_strTempDir);
	}	
}
