#if !defined(AFX_SELUNITDLG_H__21435B44_21A7_4B21_9E0B_B3BCC78576BF__INCLUDED_)
#define AFX_SELUNITDLG_H__21435B44_21A7_4B21_9E0B_B3BCC78576BF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelUnitDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSelUnitDlg dialog

class CSelUnitDlg : public CDialog
{
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();
	virtual ~CSelUnitDlg();

// Construction
public:
	void SetTitle(CString str);
	int m_nModuleID;
	void SetModuleID(int nModuleID);
	int GetModuleID();
	CSelUnitDlg(CWnd* pParent = NULL);   // standard constructor
	CString m_strTitle;
// Dialog Data
	//{{AFX_DATA(CSelUnitDlg)
	enum { IDD = IDD_SEL_UNIT_DIALOG };
	CComboBox	m_ctrlUnit;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelUnitDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSelUnitDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeComboUnit();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELUNITDLG_H__21435B44_21A7_4B21_9E0B_B3BCC78576BF__INCLUDED_)
