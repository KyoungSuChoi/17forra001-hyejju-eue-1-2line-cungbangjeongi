// OperationView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "OperationView.h"
#include "MainFrm.h"

#define EDIT_FONT_SIZE 30
#define BTN_FONT_SIZE 30

IMPLEMENT_DYNCREATE(COperationView, CFormView)

COperationView::COperationView()
	: CFormView(COperationView::IDD)
	, m_lModelId(0)
	, m_lProcess(0)
	, m_strBatch(_T(""))
	, m_strTrayId(_T(""))	
{
	m_nCurModuleID = 1;
	m_nPrevStep = 0;
	m_pConfig = NULL;
	m_nDisplayTimer = 0;
	m_pChStsSmallImage = NULL;
	m_strPreTrayId = _T("");
	m_nCurrentChCnt = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "CellInTray", 50);
	LanguageinitMonConfig();
}

COperationView::~COperationView()
{
	if(m_pChStsSmallImage)
	{
		delete m_pChStsSmallImage;
	}
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool COperationView::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_COperationView"), _T("TEXT_COperationView_CNT"), _T("TEXT_COperationView_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_COperationView_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_COperationView"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

void COperationView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, ID_DLG_LABEL_NAME, m_LabelViewName);
	DDX_Control(pDX, IDC_STAGE_NAME, m_CmdTarget);
	DDX_Control(pDX, IDC_STEP_LIST, m_ctrlStepList);
	DDX_Control(pDX, IDC_LABEL_LINEMODE, m_ctrlLineMode);		
	DDX_Control(pDX, ID_LABEL_PROCESS_TEST, m_LabelProcessTest);
	DDX_Control(pDX, ID_LABEL_PROCESS_MODEL, m_LabelProcessModel);	
	DDX_Control(pDX, ID_LABEL_TYPE_SEL, m_LabelTypeSel);	
	DDX_Control(pDX, ID_LABEL_BATCH, m_LabelBatch);
	DDX_Control(pDX, ID_LABEL_TRAYID, m_LabelTrayId);	
	DDX_Control(pDX, IDC_RUN_BTN, m_Btn_Run);
	DDX_Control(pDX, IDC_STOP_BTN, m_Btn_Stop);
	DDX_Control(pDX, IDC_CONTINUE_BTN, m_Btn_Continue);
	DDX_Control(pDX, IDC_STEPOVER_BTN, m_Btn_StepOver);
	DDX_Control(pDX, IDC_PAUSE_BTN, m_Btn_Pause);
	DDX_Control(pDX, IDC_SEND_PROCESS_BTN, m_Btn_SendProcess);
	DDX_Control(pDX, IDC_RESULTDATA_DISP_BTN, m_Btn_ResultDataDisplay);
	DDX_Control(pDX, IDC_CONTACTDATA_DISP_BTN, m_Btn_ContactResultDataDisplay);
	DDX_Control(pDX, IDC_OPERATION_AUTO_BTN, m_Btn_OperationAuto);
	DDX_Control(pDX, IDC_OPERATION_LOCAL_BTN, m_Btn_OperationLocal);		
	DDX_Text(pDX, IDC_EDIT_TYPE, m_lModelId);
	DDX_Text(pDX, IDC_EDIT_PROCESS, m_lProcess);
	DDX_Text(pDX, IDC_EDIT_BATCH, m_strBatch);
	DDX_Text(pDX, IDC_EDIT_TRAYID, m_strTrayId);
	DDX_Control(pDX, ID_LABEL1, m_Label1);
	DDX_Control(pDX, ID_LABEL2, m_Label2);
	DDX_Control(pDX, ID_LABEL3, m_Label3);
	DDX_Control(pDX, IDC_COMB_TABDEEPTH, m_ctrlTabDeepthCombo);
	DDX_Control(pDX, IDC_COMB_TRAYHEIGHT, m_ctrlTrayHeightCombo);
	DDX_Control(pDX, IDC_COMB_TRAYTYPE, m_ctrlTrayTypeCombo);
	DDX_Control(pDX, IDC_COMB_TYPESEL, m_ctrlTypeSelCombo);
	DDX_Text(pDX, IDC_EDIT_CURRENT_CH_CNT, m_nCurrentChCnt);
	DDX_Control(pDX, ID_LABEL4, m_Label4);
}

BEGIN_MESSAGE_MAP(COperationView, CFormView)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_RUN_BTN, &COperationView::OnBnClickedRunBtn)
	ON_BN_CLICKED(IDC_STOP_BTN, &COperationView::OnBnClickedStopBtn)
	ON_BN_CLICKED(IDC_CONTINUE_BTN, &COperationView::OnBnClickedContinueBtn)
	ON_BN_CLICKED(IDC_PAUSE_BTN, &COperationView::OnBnClickedPauseBtn)
	ON_BN_CLICKED(IDC_STEPOVER_BTN, &COperationView::OnBnClickedStepoverBtn)
//	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_OPERATION_AUTO_BTN, &COperationView::OnBnClickedAutoOperationBtn)
	ON_BN_CLICKED(IDC_OPERATION_LOCAL_BTN, &COperationView::OnBnClickedLocalOperationBtn)
	ON_NOTIFY(NM_DBLCLK, IDC_STEP_LIST, &COperationView::OnNMDblclkStepList)
	ON_NOTIFY(LVN_GETDISPINFO, IDC_STEP_LIST, &COperationView::OnGetdispinfoStepList)
	ON_BN_CLICKED(IDC_SEND_PROCESS_BTN, &COperationView::OnBnClickedSendProcessBtn)
	ON_BN_CLICKED(IDC_RESULTDATA_DISP_BTN, &COperationView::OnBnClickedResultdataDispBtn)
	ON_BN_CLICKED(IDC_CONTACTDATA_DISP_BTN, &COperationView::OnBnClickedContactdataDispBtn)
	ON_EN_CHANGE(IDC_EDIT_TRAYID, &COperationView::OnEnChangeEditTrayid)
END_MESSAGE_MAP()


// COperationView 진단입니다.

#ifdef _DEBUG
void COperationView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void COperationView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif

CCTSMonDoc* COperationView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCTSMonDoc)));
	return (CCTSMonDoc*)m_pDocument;
}
#endif //_DEBUG


// COperationView 메시지 처리기입니다.
void COperationView::InitLabel()
{
	m_ctrlLineMode.SetFontSize(20)
		.SetTextColor(RGB_LABEL_FONT)
		.SetBkColor(RGB_LABEL_BACKGROUND)		
		.SetFontBold(TRUE)		
		.SetFontName(TEXT_LANG[0])//"HY헤드라인M"
		.SetText("-");

	m_CmdTarget.SetFontSize(20)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND_STAGENAME)			
		.SetFontBold(TRUE)		
		.SetFontName(TEXT_LANG[0]);//"HY헤드라인M"

	m_LabelViewName.SetFontSize(24)
		.SetBkColor(RGB_MIDNIGHTBLUE)
		.SetTextColor(RGB_WHITE)
		.SetFontBold(TRUE)
		.SetText("Operation");	
		
	m_LabelProcessTest.SetFontSize(20)
		.SetTextColor(RGB_LABEL_FONT)
		.SetBkColor(RGB_LABEL_BACKGROUND)			
		.SetFontBold(TRUE)		
		.SetFontName(TEXT_LANG[0])//"HY헤드라인M"
		.SetText("Process");
		
	m_LabelProcessModel.SetFontSize(20)
		.SetTextColor(RGB_LABEL_FONT)
		.SetBkColor(RGB_LABEL_BACKGROUND)			
		.SetFontBold(TRUE)		
		.SetFontName(TEXT_LANG[0]);//"HY헤드라인M"
//		.SetText("Type");
		
	//20201209 KSJ
	m_LabelTypeSel.SetFontSize(20)
		.SetTextColor(RGB_LABEL_FONT)
		.SetBkColor(RGB_LABEL_BACKGROUND)			
		.SetFontBold(TRUE)		
		.SetFontName(TEXT_LANG[0])
		.SetText("TypeSel");

	
	m_LabelBatch.SetFontSize(20)
		.SetTextColor(RGB_LABEL_FONT)
		.SetBkColor(RGB_LABEL_BACKGROUND)			
		.SetFontBold(TRUE)		
		.SetFontName(TEXT_LANG[0])//"HY헤드라인M"
		.SetText("Batch");
		
	m_LabelTrayId.SetFontSize(20)
		.SetTextColor(RGB_LABEL_FONT)
		.SetBkColor(RGB_LABEL_BACKGROUND)			
		.SetFontBold(TRUE)		
		.SetFontName(TEXT_LANG[0])//"HY헤드라인M"
		.SetText("Tray ID");
		
	m_Label1.SetFontSize(20)
		.SetTextColor(RGB_LABEL_FONT)
		.SetBkColor(RGB_LABEL_BACKGROUND)			
		.SetFontBold(TRUE)		
		.SetFontName(TEXT_LANG[0]);//"HY헤드라인M"
	m_Label2.SetFontSize(20)
		.SetTextColor(RGB_LABEL_FONT)
		.SetBkColor(RGB_LABEL_BACKGROUND)			
		.SetFontBold(TRUE)		
		.SetFontName(TEXT_LANG[0]);//"HY헤드라인M"
	m_Label3.SetFontSize(20)
		.SetTextColor(RGB_LABEL_FONT)
		.SetBkColor(RGB_LABEL_BACKGROUND)			
		.SetFontBold(TRUE)		
		.SetFontName(TEXT_LANG[0]);		//"HY헤드라인M"
	m_Label4.SetFontSize(20)
		.SetTextColor(RGB_LABEL_FONT)
		.SetBkColor(RGB_LABEL_BACKGROUND)			
		.SetFontBold(TRUE)		
		.SetFontName(TEXT_LANG[0]);		//"HY헤드라인M"
}

void COperationView::InitCombo()
{
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetDocument();
	CString strData = _T("");
	strData = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Stopper1", "310");	
	m_ctrlTabDeepthCombo.AddString(strData);
	
	strData = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Stopper2", "250");	
	m_ctrlTabDeepthCombo.AddString(strData);
	
	strData = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Stopper3", "200");
	m_ctrlTabDeepthCombo.AddString(strData);
	
	strData = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Stopper4", "180");
	m_ctrlTabDeepthCombo.AddString(strData);
	m_ctrlTabDeepthCombo.SetCurSel(0);	
	
	m_ctrlTypeSelCombo.AddString(pDoc->m_strTypeSel[0]);
	m_ctrlTypeSelCombo.AddString(pDoc->m_strTypeSel[1]);
	m_ctrlTypeSelCombo.AddString(pDoc->m_strTypeSel[2]);
	m_ctrlTypeSelCombo.AddString(pDoc->m_strTypeSel[3]);
	m_ctrlTypeSelCombo.SetCurSel(0);	


	m_ctrlTrayHeightCombo.AddString("320");
	m_ctrlTrayHeightCombo.AddString("455");
	m_ctrlTrayHeightCombo.SetCurSel(0);
	
	m_ctrlTrayTypeCombo.AddString(TEXT_LANG[1]);//"양방향"
	m_ctrlTrayTypeCombo.AddString(TEXT_LANG[2]);//"단방향"
	m_ctrlTrayTypeCombo.SetCurSel(0);	
}

void COperationView::InitFont()
{	
	LOGFONT LogFont;

	GetDlgItem(IDC_EDIT_TYPE)->GetFont()->GetLogFont(&LogFont);
	
	LogFont.lfWeight = 1000;
	LogFont.lfHeight = EDIT_FONT_SIZE;

	m_Font.CreateFontIndirect( &LogFont );

	GetDlgItem(IDC_EDIT_TYPE)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_PROCESS)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_BATCH)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_TRAYID)->SetFont(&m_Font);
	GetDlgItem(IDC_STATIC1)->SetFont(&m_Font);
	GetDlgItem(IDC_STATIC2)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_CURRENT_CH_CNT)->SetFont(&m_Font);
	GetDlgItem(IDC_COMB_TABDEEPTH)->SetFont(&m_Font);	
	GetDlgItem(IDC_COMB_TRAYHEIGHT)->SetFont(&m_Font);	
	GetDlgItem(IDC_COMB_TRAYTYPE)->SetFont(&m_Font);	
	GetDlgItem(IDC_COMB_TYPESEL)->SetFont(&m_Font);	
}

void COperationView::InitColorBtn()
{
	m_Btn_Run.SetFontStyle(BTN_FONT_SIZE,1);
	m_Btn_Run.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	m_Btn_Stop.SetFontStyle(BTN_FONT_SIZE,1);
	m_Btn_Stop.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
	m_Btn_Continue.SetFontStyle(BTN_FONT_SIZE,1);
	m_Btn_Continue.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	m_Btn_StepOver.SetFontStyle(BTN_FONT_SIZE,1);
	m_Btn_StepOver.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	m_Btn_Pause.SetFontStyle(BTN_FONT_SIZE,1);
	m_Btn_Pause.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	m_Btn_SendProcess.SetFontStyle(18, 1);
	m_Btn_SendProcess.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	m_Btn_ResultDataDisplay.SetFontStyle(18, 1);
	m_Btn_ResultDataDisplay.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	m_Btn_ContactResultDataDisplay.SetFontStyle(18, 1);
	m_Btn_ContactResultDataDisplay.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
	m_Btn_OperationAuto.SetFontStyle(18, 1);
	m_Btn_OperationAuto.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
	m_Btn_OperationLocal.SetFontStyle(18, 1);
	m_Btn_OperationLocal.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
}

void COperationView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetDocument();
	m_pConfig = pDoc->GetTopConfig();						//Top Config

	InitLabel();
	InitFont();	
	InitColorBtn();
	InitCombo();
	InitStepList();
}

void COperationView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(::IsWindow(this->GetSafeHwnd()))
	{
		CRect ctrlRect;
		RECT rect;
		::GetClientRect(m_hWnd, &rect);
				
		if( GetDlgItem(ID_DLG_LABEL_NAME)->GetSafeHwnd() )
		{
			CRect rectGrid;
			CRect rectStageLabel;
			GetDlgItem(ID_DLG_LABEL_NAME)->GetWindowRect(rectGrid);
			ScreenToClient(&rectGrid);
			GetDlgItem(IDC_STAGE_NAME)->GetWindowRect(rectStageLabel);
			ScreenToClient(&rectStageLabel);
			GetDlgItem(ID_DLG_LABEL_NAME)->MoveWindow(rectGrid.left, rectGrid.top, rect.right-rectStageLabel.Width(), rectGrid.Height(), FALSE);
		}
		
		if(::IsWindow(GetDlgItem(IDC_STEP_LIST)->GetSafeHwnd()))
		{
			GetDlgItem(IDC_STEP_LIST)->GetWindowRect(&ctrlRect);
			ScreenToClient(&ctrlRect);
			GetDlgItem(IDC_STEP_LIST)->MoveWindow(ctrlRect.left, ctrlRect.top, ctrlRect.Width(), rect.bottom - ctrlRect.top-5 , FALSE);
		}		
	}	
}

void COperationView::SetCurrentModule(int nModuleID, int nGroupIndex)
{	
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetDocument();

	m_nCurModuleID = nModuleID;
	m_CmdTarget.SetText(GetTargetModuleName());	
	
	m_lProcess = 0;
	m_lModelId = 0;
	m_strBatch = _T("");
	m_strTrayId = _T("");
	
	m_ctrlTabDeepthCombo.ResetContent();
		
	CString strData = _T("");

	strData = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Stopper1", "310");
	m_ctrlTabDeepthCombo.AddString(strData);

	strData = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Stopper2", "250");
	m_ctrlTabDeepthCombo.AddString(strData);

	strData = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Stopper3", "200");
	m_ctrlTabDeepthCombo.AddString(strData);

	strData = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Stopper4", "180");
	m_ctrlTabDeepthCombo.AddString(strData);
	
	m_ctrlTabDeepthCombo.SetCurSel(0);
	m_ctrlTrayHeightCombo.SetCurSel(0);	
	m_ctrlTrayTypeCombo.SetCurSel(0);
	
	m_ctrlTypeSelCombo.ResetContent();

	m_ctrlTypeSelCombo.AddString(pDoc->m_strTypeSel[0]);
	m_ctrlTypeSelCombo.AddString(pDoc->m_strTypeSel[1]);
	m_ctrlTypeSelCombo.AddString(pDoc->m_strTypeSel[2]);
	m_ctrlTypeSelCombo.AddString(pDoc->m_strTypeSel[3]);
	m_ctrlTypeSelCombo.SetCurSel(0);	
	
	UpdateGroupState(nModuleID);
	
	UpdateData(false);
}

CString COperationView::GetTargetModuleName()
{
	CString temp;
	CString strMDName = GetModuleName(m_nCurModuleID);	
	temp.Format("Stage No. %s", strMDName);	
	return temp;
}

void COperationView::InitStepList()
{
	m_pChStsSmallImage = new CImageList;
	//상태별 표시할 이미지 로딩
	m_pChStsSmallImage->Create(IDB_CELL_STATE_ICON,19,24,RGB(255,255,255));
	m_ctrlStepList.SetImageList(m_pChStsSmallImage, LVSIL_SMALL);

	//
	DWORD style = 	m_ctrlStepList.GetExtendedStyle();
	style |= LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_HEADERDRAGDROP|LVS_EX_SUBITEMIMAGES;
	m_ctrlStepList.SetExtendedStyle(style );//|LVS_EX_TRACKSELECT);

	// Column 삽입
	m_ctrlStepList.InsertColumn(0, "Step", LVCFMT_CENTER, 0);	//center 정렬을 0 column을 사용하지 않음 
	m_ctrlStepList.InsertColumn(1, "Step", LVCFMT_CENTER, 40);	
	m_ctrlStepList.InsertColumn(2, "Type", LVCFMT_LEFT, 100);
	m_ctrlStepList.InsertColumn(3, "Status", LVCFMT_LEFT, 80);
	m_ctrlStepList.InsertColumn(4, "Referance", LVCFMT_LEFT, 100);
}

void COperationView::DrawStepList()
{
	m_ctrlStepList.DeleteAllItems();

	CCTSMonDoc *pDoc=  GetDocument();
	CFormModule *pModule = pDoc->GetModuleInfo(m_nCurModuleID);
	
	if(pModule == NULL)		return;
	
	LVITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.mask = LVIF_TEXT|LVIF_IMAGE;

	CTestCondition *pCon = pModule->GetCondition();
	CStep *pStep = NULL;
	char szName[32];
	int nI = 0;
	for(nI = 0; nI <pCon->GetTotalStepNo(); nI++ )
	{
		pStep = pCon->GetStep(nI);
		if(pStep == NULL)	break;
		
		sprintf(szName, "%d", pStep->m_StepIndex+1);
		lvItem.iItem = nI;
		lvItem.iSubItem = 0;
		lvItem.pszText = szName;
		lvItem.iImage = I_IMAGECALLBACK;
		m_ctrlStepList.InsertItem(&lvItem);
		m_ctrlStepList.SetItemData(lvItem.iItem, pStep->m_type);		//==>LVN_ITEMCHANGED 를 발생 기킴 

		m_ctrlStepList.SetItemText(nI, 1, szName);

		sprintf(szName, "%s", ::StepTypeMsg(pStep->m_type));
		m_ctrlStepList.SetItemText(nI, 2, szName);
		if(pStep->m_type == EP_TYPE_CHARGE || pStep->m_type == EP_TYPE_DISCHARGE || pStep->m_type == EP_TYPE_IMPEDANCE)
		{
			sprintf(szName, "%s/%s", pDoc->ValueString(pStep->m_fVref, EP_VOLTAGE, TRUE), pDoc->ValueString(pStep->m_fIref, EP_CURRENT, TRUE));
			m_ctrlStepList.SetItemText(nI, 4, szName);
		}
		else if(pStep->m_type == EP_TYPE_REST)
		{
			sprintf(szName, "%s", pDoc->ValueString(pStep->m_fEndTime, EP_STEP_TIME, TRUE));
			m_ctrlStepList.SetItemText(nI, 4, szName);
		}
	}	

	//Idle Monitoring을 위한 의미 없는 Step Item 추가
	//2008/7/18 KBH
	int nCurStep = pModule->GetCurStepNo();
	//현재 진행 Step에 맞게 Display 한다.
	for(nI = 0; nI < m_ctrlStepList.GetItemCount(); nI++)
	{
		if(pModule->IsComplitedStep(nI))
		{
			//완료된 data는 파일에서 loading한다.
			m_ctrlStepList.SetItemText(nI, 3, ::GetStringTable(IDS_TEXT_END));
		}
		else
		{
			if(nCurStep == nI+1)
			{
				//진행중인 data는 현재값을 표시한다.
				m_ctrlStepList.SetItemText(nI, 3, ::GetStringTable(IDS_TEXT_RUNNING));

				break;
			}
			else
			{
				//아직 진행하지 않은 step은 현재값을 표시한다.
				m_ctrlStepList.SetItemText(nI, 3, ::GetStringTable(IDS_TEXT_STANDBY));
			}
		}
	}
	m_ctrlStepList.Invalidate();
}

void COperationView::ModuleConnected(int nModuleID)
{
	if(nModuleID == m_nCurModuleID)							//현재 선택된 Module이면 Grid를 Setting 시키고
	{	
		UpdateGroupState(nModuleID);
	
		StartMonitoring();	
	}
}

void COperationView::ModuleDisConnected(int nModuleID)
{
	if(nModuleID == m_nCurModuleID)							// 현재 선택된 Module이면 Grid를 Setting 시키고
	{	
		UpdateGroupState(nModuleID);

		StopMonitoring();
	}
}

void COperationView::StopMonitoring()
{
	if(m_nDisplayTimer != 0)	
	{
		KillTimer(m_nDisplayTimer);
		m_nDisplayTimer = 0;
	}
	
	m_strTrayId = _T("");
	m_strBatch = _T("");
	m_lProcess = 0;
	m_lModelId = 0;	
	m_lTypeSel = 0;	//20201209 ksj
	m_nCurrentChCnt = 0;
	m_ctrlTabDeepthCombo.SetCurSel(0);
	m_ctrlTrayHeightCombo.SetCurSel(0);
	m_ctrlTrayTypeCombo.SetCurSel(0);
}

void COperationView::StartMonitoring()
{
	m_nPrevStep = 0;
	
	DrawStepList();
	
	DrawProcessInfo();
}

void COperationView::DrawProcessInfo()
{
	CCTSMonDoc *pDoc = GetDocument();
	CFormModule *pModule = pDoc->GetModuleInfo(m_nCurModuleID);
	
	if(pModule != NULL )
	{	
		m_strTrayId = pModule->GetTrayNo(0);
		m_lProcess = pModule->GetCondition()->GetTestInfo()->lID;
		m_lModelId = pModule->GetCondition()->GetModelInfo()->lID;
		m_strBatch = pModule->GetTrayInfo(0)->strLotNo;
		m_nCurrentChCnt = pModule->GetTrayInfo(0)->GetInputCellCnt();	
		m_lTypeSel = atoi(pModule->GetTrayInfo(0)->strTypeSel);	//20201211 ksj
		m_ctrlTabDeepthCombo.SetCurSel( pModule->GetTrayInfo(0)->m_nTabDeepth );
		m_ctrlTrayHeightCombo.SetCurSel( pModule->GetTrayInfo(0)->m_nTrayHeight);
		m_ctrlTrayTypeCombo.SetCurSel( pModule->GetTrayInfo(0)->m_nTrayType);
			for(int i=0; i<4; i++)
			{
				if( pModule->GetTrayInfo(0)->strTypeSel == pDoc->m_strTypeSel[i] )
				{
					m_ctrlTypeSelCombo.SetCurSel(i);
				}	
			}
		
		UpdateData(FALSE);
	}
}

void COperationView::OnBnClickedRunBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다
	CCTSMonDoc *pDoc = GetDocument();
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();

	CString strTemp = _T(""), strFailMD = _T(""), strLoadFailMD = _T("");

	UpdateData(true);

	WORD state;
	state = EPGetGroupState(m_nCurModuleID);

	//20201215 ksj
	int nCursel = m_ctrlTypeSelCombo.GetCurSel();
	if( nCursel == 0)
	{
		pDoc->m_ProcessNo = 1;
	}
	else
	{
		pDoc->m_ProcessNo = 0;
	}


	
	if( state == EP_STATE_LINE_OFF || pMainFrm->nModuleReadyToStart[m_nCurModuleID] == 1 || state == EP_STATE_RUN )
	{
		strTemp.Format(TEXT_LANG[3]);//"충방전 진행이 가능한 상태가 아닙니다"
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		// AfxMessageBox(TEXT_LANG[4], MB_ICONWARNING);//"충방전 진행이 가능한 상태가 아닙니다."
		return;		
	}

	if( m_nCurrentChCnt == 0 || m_strBatch.IsEmpty() || m_strTrayId.IsEmpty() )
	{
		strTemp.Format(TEXT_LANG[5]);//"충방전 진행에 필요한 정보가 없습니다."
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_WARNNING);
		// AfxMessageBox(TEXT_LANG[5], MB_ICONWARNING);//"충방전 진행에 필요한 정보가 없습니다."
		return;	
	}

	strTemp.Format(GetStringTable(IDS_MSG_RUN_CONFIRM), pDoc->m_strModuleName);
	if(MessageBox(strTemp, "Start", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;

	strTemp.Format("[%s] ", ::GetModuleName(m_nCurModuleID));

	if( state == EP_STATE_IDLE || state == EP_STATE_STANDBY || state == EP_STATE_END || state == EP_STATE_READY )
	{
		if( state != EP_STATE_IDLE )
		{
			pDoc->SendInitCommand(m_nCurModuleID);
			Sleep(200);
		}

		// 1. Idle 상태 체크 후 공정 전송
		STR_CONDITION_HEADER testHeader;
		ZeroMemory( &testHeader, sizeof(STR_CONDITION_HEADER));
		testHeader.lID = m_lProcess;
		testHeader.lNo = m_lModelId;

		CTestCondition testCondition;
		CString strTestSerialNo = _T("");

		if(testCondition.LoadTestCondition(testHeader.lID, m_lModelId) == true)
		{
			CFormModule *pModule = pDoc->GetModuleInfo(m_nCurModuleID);
			if(pModule != NULL)
			{
				COleDateTime curTime = COleDateTime::GetCurrentTime();
				strTestSerialNo.Format("%s%02d", curTime.Format("%Y%m%d%H%M%S"), 1);			
				pModule->GetTrayInfo(0)->SetTrayNo(m_strTrayId);
				pModule->GetTrayInfo(0)->strLotNo = m_strBatch;
				pModule->GetTrayInfo(0)->strTestSerialNo = strTestSerialNo;								
				pModule->GetTrayInfo(0)->SetInputCellCnt(m_nCurrentChCnt);							
				pModule->GetTrayInfo(0)->m_nTabDeepth = m_ctrlTabDeepthCombo.GetCurSel();
				pModule->GetTrayInfo(0)->m_nTrayHeight = m_ctrlTrayHeightCombo.GetCurSel();
				pModule->GetTrayInfo(0)->m_nTrayType = m_ctrlTrayTypeCombo.GetCurSel();

				if(AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "AutoCellCheck", FALSE) == TRUE )
				{
					pModule->GetTrayInfo(0)->m_nContactErrlimit = MAX_CELL_COUNT;					
				}
				else
				{
					pModule->GetTrayInfo(0)->m_nContactErrlimit = pDoc->m_nContactErrlimit;
				}
				
				pModule->GetTrayInfo(0)->m_nCapaErrlimit = pDoc->m_nCapaErrlimit;
				pModule->GetTrayInfo(0)->m_nChErrlimit = pDoc->m_nChErrlimit;
				CString strTemp;
				int nCursel = m_ctrlTypeSelCombo.GetCurSel();
				

				pModule->GetTrayInfo(0)->strTypeSel = pDoc->m_strTypeSel[m_lTypeSel];
				if(pDoc->SendConditionToModule(m_nCurModuleID, 0, &testCondition) == FALSE)
				{
					strFailMD.Format("[%s] ", ::GetModuleName(m_nCurModuleID));
					strTemp.Format("%s (%s)",  ::GetStringTable(IDS_TEXT_CMD_SEND_FAIL),  ::GetModuleName(m_nCurModuleID));		
					pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);					
				}
				else
				{
					if(pDoc->SendLastTrayStateData(m_nCurModuleID, 0) == FALSE)	//현재 Tray의 최종 상태를 Module로 전송
					{						
						strFailMD.Format("[%s] ", ::GetModuleName(m_nCurModuleID));
					}
					else
					{
						pMainFrm->nModuleReadyToStart[m_nCurModuleID] = 1;
						pMainFrm->m_pTab->m_tabWnd.ActivateTab(1);
					}
				}
			}
			else
			{
				strFailMD.Format("[%s] ", ::GetModuleName(m_nCurModuleID));
			}					
		}
		else
		{
			strLoadFailMD.Format("[%s] ", ::GetModuleName(m_nCurModuleID));
		}				

		if( !strLoadFailMD.IsEmpty() )
		{	
			strTemp.Format(TEXT_LANG[6], strLoadFailMD, m_lModelId, testHeader.lID );			//"%s 의 선택 조건 [M:%d - P:%d] Loading 을 실패 하였습니다."
			MessageBox(strTemp, ::GetStringTable(IDS_TEXT_SEND_FAIL), MB_OK|MB_ICONWARNING);							
			return;										
		}

		if( !strFailMD.IsEmpty())
		{
			strTemp.Format(TEXT_LANG[7]);//"명령을 전송할 수 없는 상태이거나 전송에 실패 하였습니다."
			pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
			// strTemp.Format(TEXT_LANG[8], strFailMD);//"%s 는 전송할 수 없는 상태이거나 전송을 실패 하였습니다."
			// MessageBox(strTemp, ::GetStringTable(IDS_TEXT_SEND_FAIL), MB_OK|MB_ICONWARNING);
		}
	}
	else
	{
		strTemp.Format(TEXT_LANG[3]);//"충방전 진행이 가능한 상태가 아닙니다"
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		// AfxMessageBox(TEXT_LANG[4], MB_ICONWARNING);	//"충방전 진행이 가능한 상태가 아닙니다."
	}
}

void COperationView::OnBnClickedStopBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CCTSMonDoc *pDoc = GetDocument();
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();

	CString strTemp = _T(""), strFailMD = _T("");

	pMainFrm->nModuleReadyToStart[m_nCurModuleID] = 0;
	
	WORD state;
	state = EPGetGroupState(m_nCurModuleID);
	if( state == EP_STATE_LINE_OFF )
	{	
		strTemp.Format(TEXT_LANG[9]);//"명령이 전송 가능한 상태가 아닙니다"
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		return;		
	}

	if(state == EP_STATE_RUN || state == EP_STATE_PAUSE )
	{
		strTemp.Format(GetStringTable(IDS_MSG_STOP_CONFIRM), ::GetModuleName(m_nCurModuleID));
		if(MessageBox(strTemp, "Stop", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;

		strTemp.Format("[%s] ", ::GetModuleName(m_nCurModuleID));
		state = EPGetGroupState(m_nCurModuleID);

		if(state == EP_STATE_RUN || state == EP_STATE_PAUSE)	
		{	
			if(pDoc->SendStopCmd(m_nCurModuleID) == FALSE)
			{
				strFailMD += strTemp;
			}
		}
		else
		{
			strFailMD += strTemp;

		}

		if(!strFailMD.IsEmpty())
		{
			strTemp.Format(TEXT_LANG[7]);//"명령을 전송할 수 없는 상태이거나 전송에 실패 하였습니다."
			pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
			// strTemp.Format("%s 는 전송할 수 없는 상태이거나 전송을 실패 하였습니다.", strFailMD);
			// MessageBox(strTemp, ::GetStringTable(IDS_TEXT_SEND_FAIL), MB_OK|MB_ICONWARNING);
		}
		else
		{
			strTemp.Format(TEXT_LANG[10]);//"명령을 전송 하였습니다.\n잠시만 기다려주십시오."
			pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_NORMAL);
		}
	}
	else
	{
		strTemp.Format(TEXT_LANG[9]);//"명령이 전송 가능한 상태가 아닙니다"
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
	}	
}

void COperationView::OnBnClickedContinueBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CCTSMonDoc *pDoc = GetDocument();
	CString strTemp = _T(""), strFailMD = _T("");
	
	WORD state;
	state = EPGetGroupState(m_nCurModuleID);
	if( state == EP_STATE_LINE_OFF )
	{
		return;		
	}	

	strTemp.Format(GetStringTable(IDS_MSG_CONTINUE_CONFIRM), pDoc->m_strModuleName);
	if(MessageBox(strTemp, "Continue", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;

	int nRtn = EP_NACK;

	// 1. 전송할 Unit이 1개일 경우
	strTemp.Format("[%s] ", ::GetModuleName(m_nCurModuleID));
	state = EPGetGroupState(m_nCurModuleID);
	
	if(state == EP_STATE_PAUSE )	
	{
		if(( nRtn = EPSendCommand(m_nCurModuleID, 1, 0, EP_CMD_CONTINUE)) != EP_ACK)
		{
			strFailMD += strTemp;
			strTemp.Format("%s (%s-%s)",  ::GetStringTable(IDS_TEXT_CMD_SEND_FAIL),  ::GetModuleName(m_nCurModuleID), pDoc->CmdFailMsg(nRtn));		
			pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
		}
		// EPSendCommand(m_nCurModuleID);
	}
	else
	{
		strFailMD += strTemp;
		strTemp.Format(TEXT_LANG[11],  ::GetModuleName(m_nCurModuleID));		//"%s는 계속진행을 전송할 수 없는 상태이므로 전송하지 않았습니다."
		pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
	}

	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[8], strFailMD);//"%s 는 전송할 수 없는 상태이거나 전송을 실패 하였습니다."
		MessageBox(strTemp, ::GetStringTable(IDS_TEXT_SEND_FAIL), MB_OK|MB_ICONWARNING);
	}
}

//void COperationView::OnBnClickedNextstepBtn()
//{
//	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
//}

void COperationView::OnBnClickedPauseBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_PERMISSION_ERROR) + " [Start]");
		return ;
	}

	CCTSMonDoc *pDoc = GetDocument();
	CString strTemp = _T(""), strFailMD = _T("");

	strTemp.Format(GetStringTable(IDS_MSG_PAUSE_CONFIRM), pDoc->m_strModuleName);
	if(MessageBox(strTemp, "Pause", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;

	WORD state;
	int nRtn = EP_NACK;

	// 1. 전송할 Unit이 1개일 경우
	strTemp.Format("[%s] ", ::GetModuleName(m_nCurModuleID));
	state = EPGetGroupState(m_nCurModuleID);
	if( state != EP_STATE_LINE_OFF )
	{	
		if(state == EP_STATE_RUN)	
		{
			if(( nRtn = EPSendCommand(m_nCurModuleID, 1, 0, EP_CMD_PAUSE)) != EP_ACK)
			{
				strFailMD += strTemp;
				strTemp.Format("%s (%s-%s)",  ::GetStringTable(IDS_TEXT_CMD_SEND_FAIL),  ::GetModuleName(m_nCurModuleID), pDoc->CmdFailMsg(nRtn));		
				pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
			}
			// EPSendCommand(m_nCurModuleID);
		}
		else
		{
			strFailMD += strTemp;
			strTemp.Format(TEXT_LANG[12],  ::GetModuleName(m_nCurModuleID));//"%s는 잠시멈춤을 전송할 수 없는 상태이므로 전송하지 않았습니다."
			pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
		}
	}

	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[8], strFailMD);//"%s 는 전송할 수 없는 상태이거나 전송을 실패 하였습니다."
		MessageBox(strTemp, ::GetStringTable(IDS_TEXT_SEND_FAIL), MB_OK|MB_ICONWARNING);
	}
}

void COperationView::OnBnClickedStepoverBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CCTSMonDoc *pDoc = GetDocument();
	CString strTemp = _T(""), strFailMD = _T("");

	strTemp.Format(GetStringTable(IDS_MSG_NEXT_STEP_CONFIRM), pDoc->m_strModuleName);
	if(MessageBox(strTemp, "Next Step", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;

	WORD state;
	int nRtn = EP_NACK;

	// 1. 전송할 Unit이 1개일 경우
	strTemp.Format("[%s] ", ::GetModuleName(m_nCurModuleID));
	state = EPGetGroupState(m_nCurModuleID);
	if( state != EP_STATE_LINE_OFF )
	{	
		if( state != EP_STATE_LINE_OFF )
		{
			if(state == EP_STATE_RUN || state == EP_STATE_PAUSE)	
			{
				if(( nRtn = EPSendCommand(m_nCurModuleID, 1, 0, EP_CMD_NEXTSTEP)) != EP_ACK)
				{
					strFailMD += strTemp;
					strTemp.Format("%s (%s-%s)",  ::GetStringTable(IDS_TEXT_CMD_SEND_FAIL),  ::GetModuleName(m_nCurModuleID), pDoc->CmdFailMsg(nRtn));		
					pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
				}
				// EPSendCommand(m_nCurModuleID);
			}
			else
			{
				strFailMD += strTemp;
				strTemp.Format(TEXT_LANG[13],  ::GetModuleName(m_nCurModuleID));		//"%s 는 다음 Step을 전송할 수 없는 상태이므로 전송하지 않았습니다."
				pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
			}			
		}	
	}

	if(!strFailMD.IsEmpty())
	{		
		strTemp.Format(TEXT_LANG[8], strFailMD);//"%s 는 전송할 수 없는 상태이거나 전송을 실패 하였습니다."
		MessageBox(strTemp, ::GetStringTable(IDS_TEXT_SEND_FAIL), MB_OK|MB_ICONWARNING);
	}
}

void COperationView::DrawChannel()
{
	BYTE colorFlag = 0;
	CString  strMsg, strOld, strToolTip, strUnit;

	CCTSMonDoc *pDoc = GetDocument();
	float	fTemp = 0;

	CTestCondition *pProc;
	CStep *pStep = NULL;

	CFormModule *pModule;
	pModule =  pDoc->GetModuleInfo(m_nCurModuleID);
	if(pModule == NULL)		return;

	int nCurStep = pModule->GetCurStepNo();
	pProc = pModule->GetCondition();
	if(pProc != NULL)
	{
		//		TRACE("STEP No : %d\r\n", nCurStep);
		pStep = pProc->GetStep(nCurStep-1);		//Step 번호가 1 Base
	}

	// step 이 변했으면 자동으로 현재 step을 이동한다.
	if(m_nPrevStep != nCurStep)
	{
		m_nPrevStep = nCurStep;
		
		//현재 진행 Step에 맞게 Display 한다.
		for(int item = 0; item < m_ctrlStepList.GetItemCount(); item++)
		{
			if(pModule->IsComplitedStep(item))
			{
				//완료된 data는 파일에서 loading한다.
				m_ctrlStepList.SetItemText(item, 3, ::GetStringTable(IDS_TEXT_END));
			}
			else
			{
				// 작업을 완료한 후 결과 파일이 삭제 되었거나 존재 하지 않을 경우
				// 진행중과 대기중으로 표기된다.
				if(nCurStep == item+1)
				{
					m_ctrlStepList.SetItemText(item, 3, ::GetStringTable(IDS_TEXT_RUNNING));
				}
				else
				{
					m_ctrlStepList.SetItemText(item, 3, ::GetStringTable(IDS_TEXT_STANDBY));
				}
			}
		}
		m_ctrlStepList.Invalidate();
	}
}

void COperationView::OnBnClickedAutoOperationBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( EPGetGroupState(m_nCurModuleID) == EP_STATE_LINE_OFF )
	{
		return;
	}

	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_PERMISSION_ERROR) + " [Change to OnLine]");
		return ;
	}

	CCTSMonDoc *pDoc = GetDocument();
	CString strTemp = _T(""), strFailMD = _T("");

	strTemp.Format(GetStringTable(IDS_MSG_CHANGE_AUTO_OPERATION_CONFIRM), m_nCurModuleID);
	if(MessageBox(strTemp, "Mode Change", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;

	WORD state;

	// 1. 전송할 Unit이 1개일 경우
	strTemp.Format("[%s] ", ::GetModuleName(m_nCurModuleID));
	state = EPGetGroupState(m_nCurModuleID);
	if( state != EP_STATE_LINE_OFF )
	{
		if( pDoc->SetOperationMode(m_nCurModuleID, 0, EP_OPERATION_AUTO) == false )
		{
			strFailMD += strTemp;
			strTemp.Format(TEXT_LANG[14],  ::GetModuleName(m_nCurModuleID));		//"%s는 OnLine mode 변경에 실패 하였습니다."
			pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
		}
	}

	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[8], strFailMD);//"%s 는 전송할 수 없는 상태이거나 전송을 실패 하였습니다."
		MessageBox(strTemp, ::GetStringTable(IDS_TEXT_SEND_FAIL), MB_OK|MB_ICONWARNING);		
	}
}

void COperationView::UpdateGroupState(int nModuleID, int /*nGroupIndex*/)
{
	if( nModuleID != m_nCurModuleID )
	{	
		// 1. 현재 실행중인 모듈이 아니기에 상태값을 변경하지 않는다.
		return;
	}
	
	UpdateBtnEnable(false);
	
	if( EPGetGroupState(nModuleID) == EP_STATE_LINE_OFF )
	{
		m_ctrlLineMode.SetText("Line Off");
	}
	else
	{
		CCTSMonDoc *pDoc =  GetDocument();
		CFormModule *pModule = pDoc->GetModuleInfo(nModuleID);
		if(pModule != NULL)
		{
			if( EPGetGroupState(nModuleID) == EP_STATE_LINE_OFF )
			{
				m_ctrlLineMode.SetText("Line Off");			
			}
			else
			{
				DrawStepList();

				DrawChannel();

				int nLineMode = EP_OFFLINE_MODE;		
				nLineMode = pModule->GetOperationMode();

				switch( nLineMode )
				{
				case EP_OPERATION_LOCAL:
					{
						m_ctrlLineMode.SetText("Local Mode");
						UpdateBtnEnable(true);		
					}
					break;
				case EP_OPEARTION_MAINTENANCE:
					{
						m_ctrlLineMode.SetText("Maintenance Mode");
					}
					break;
				case EP_OPERATION_AUTO:
					{
						m_ctrlLineMode.SetText("Automatic Mode");
						DrawProcessInfo();
					}
					break;
				}	
			}
		}
		else
		{
			m_ctrlLineMode.SetText("Line Off");
		}	
	}
}

void COperationView::UpdateBtnEnable( bool bShow )
{
	GetDlgItem(IDC_EDIT_TYPE)->EnableWindow(bShow);
	GetDlgItem(IDC_EDIT_PROCESS)->EnableWindow(bShow);
	
	GetDlgItem(IDC_EDIT_BATCH)->EnableWindow(bShow);
	GetDlgItem(IDC_EDIT_TRAYID)->EnableWindow(bShow);
	
	GetDlgItem(IDC_COMB_TABDEEPTH)->EnableWindow(bShow);
	GetDlgItem(IDC_COMB_TRAYHEIGHT)->EnableWindow(bShow);
	GetDlgItem(IDC_COMB_TRAYTYPE)->EnableWindow(bShow);
	GetDlgItem(IDC_COMB_TYPESEL)->EnableWindow(bShow);
	GetDlgItem(IDC_EDIT_CURRENT_CH_CNT)->EnableWindow(bShow);
	
	// GetDlgItem(IDC_RESULTDATA_DISP_BTN)->EnableWindow(bShow);
	// GetDlgItem(IDC_CONTACTDATA_DISP_BTN)->EnableWindow(bShow);
	// GetDlgItem(IDC_SEND_PROCESS_BTN)->EnableWindow(bShow);
	GetDlgItem(IDC_RUN_BTN)->EnableWindow(bShow);
	GetDlgItem(IDC_STOP_BTN)->EnableWindow(bShow);
	GetDlgItem(IDC_STEPOVER_BTN)->EnableWindow(bShow);
}


void COperationView::OnBnClickedLocalOperationBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( EPGetGroupState(m_nCurModuleID) == EP_STATE_LINE_OFF )
	{
		return;
	}

	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_PERMISSION_ERROR) + " [Change to OnLine]");
		return ;
	}

	CCTSMonDoc *pDoc = GetDocument();
	CString strTemp = _T(""), strFailMD = _T("");

	strTemp.Format(GetStringTable(IDS_MSG_CHANGE_LOCAL_OPERATION_CONFIRM), m_nCurModuleID);
	if(MessageBox(strTemp, "Mode Change", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;

	WORD state;

	// 1. 전송할 Unit이 1개일 경우
	strTemp.Format("[%s] ", ::GetModuleName(m_nCurModuleID));
	state = EPGetGroupState(m_nCurModuleID);
	if( state != EP_STATE_LINE_OFF )
	{
		if( pDoc->SetOperationMode(m_nCurModuleID, 0, EP_OPERATION_LOCAL) == false )
		{
			strFailMD += strTemp;
			strTemp.Format(TEXT_LANG[15],  ::GetModuleName(m_nCurModuleID));		//"%s는 local mode 변경에 실패 하였습니다."
			pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
		}
	}

	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[8], strFailMD);//"%s 는 전송할 수 없는 상태이거나 전송을 실패 하였습니다."
		MessageBox(strTemp, ::GetStringTable(IDS_TEXT_SEND_FAIL), MB_OK|MB_ICONWARNING);		
	}
}

void COperationView::OnNMDblclkStepList(NMHDR *pNMHDR, LRESULT *pResult)
{
	// LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	pNMHDR = NULL;
	CCTSMonDoc	*pDoc = (CCTSMonDoc *)GetDocument();
	pDoc->ShowTestCondition(m_nCurModuleID);
	
	*pResult = 0;
}

void COperationView::OnGetdispinfoStepList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;
	// TODO: Add your control notification handler code here

	UINT nType, nCurStep = 0;
	DWORD dwImgIndex = 0;
	//	EP_CH_DATA netChData;

	if(pDispInfo->item.mask & LVIF_IMAGE)
	{
		CFormModule *pMD = GetDocument()->GetModuleInfo(m_nCurModuleID);

		if(pDispInfo->item.iSubItem == 2)
		{
			nType = m_ctrlStepList.GetItemData(pDispInfo->item.iItem);	//step type

			switch(nType)
			{
			case EP_TYPE_CHARGE	:		dwImgIndex = 2;		break;
			case EP_TYPE_DISCHARGE:		dwImgIndex = 1;		break;
			case EP_TYPE_REST	:		dwImgIndex = 11;	break;
			case EP_TYPE_OCV	:		dwImgIndex = 7;		break;
			case EP_TYPE_IMPEDANCE:		dwImgIndex = 10;	break;
			case EP_TYPE_END	:		dwImgIndex = 4;		break;
			default				:		dwImgIndex = 0;		break;
			}

			pDispInfo->item.iImage = dwImgIndex + 12;

			if(pMD)		
			{
				if(pMD->IsComplitedStep(pDispInfo->item.iItem))
				{
					pDispInfo->item.iImage = dwImgIndex;
				}
			}
		}
		else if(pDispInfo->item.iSubItem == 1)
		{
			if(pMD)		
			{

				nCurStep = pMD->GetCurStepNo();
				if((UINT)pDispInfo->item.iItem + 1 == nCurStep)
				{
					pDispInfo->item.iImage = 8;				
				}
				else
				{
					pDispInfo->item.iImage = -1;
				}
			}
		}
	}
	*pResult = 0;
}

void COperationView::OnBnClickedSendProcessBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CCTSMonDoc	*pDoc = (CCTSMonDoc *)GetDocument();
	pDoc->ShowTestCondition(m_nCurModuleID);	
}

void COperationView::OnBnClickedResultdataDispBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.	
	GetDocument()->ViewResult(m_nCurModuleID, 0);
}

void COperationView::OnBnClickedContactdataDispBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	GetDocument()->ViewResult(m_nCurModuleID, 0, 1);
}

void COperationView::OnEnChangeEditTrayid()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CFormView::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(true);
	CString strTemp = m_strTrayId;
	if( m_strTrayId.GetLength() > 7 )
	{
		m_strTrayId = m_strPreTrayId;			
	}
	else
	{
		m_strPreTrayId = m_strTrayId;
	}
	
	UpdateData(false);
}
