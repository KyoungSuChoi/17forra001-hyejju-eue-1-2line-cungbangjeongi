// FtpDownLoad.cpp: implementation of the CFtpDownLoad class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "FtpDownLoad.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

bool CFtpDownLoad::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CFtpDownLoad"), _T("TEXT_CFtpDownLoad_CNT"), _T("TEXT_CFtpDownLoad_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CFtpDownLoad_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CFtpDownLoad"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

CFtpDownLoad::CFtpDownLoad()
{
	LanguageinitMonConfig();

	m_pInternetSession = NULL;
	m_pInternetSession = new CInternetSession(_T("ADPFtpDownLoad/1.0"));
}

CFtpDownLoad::~CFtpDownLoad()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}

	if(m_pFtpConnection)
	{
		m_pFtpConnection->Close();
		delete m_pFtpConnection;
		m_pFtpConnection= NULL;
	}

	if(m_pInternetSession)
	{
		m_pInternetSession->Close();
		delete m_pInternetSession;
		m_pInternetSession = NULL;
	}
}

//DownLoad file form module
int CFtpDownLoad::DownLoadFile(CString strLoacalFile, CString strRemoteFile, BOOL bOverWrite)
{
	CWaitCursor cursor;	

	if(strLoacalFile.IsEmpty() || strRemoteFile.IsEmpty())	return -1;
	
	//first call ConnectFtpSession()
	ASSERT(m_pFtpConnection);

#ifdef _DEBUG
	char szBuff[_MAX_PATH];
	DWORD nLength = _MAX_PATH;
	m_pFtpConnection->GetCurrentDirectory(szBuff, &nLength);
	TRACE("FTP current directory is %s\n", szBuff);

	CFtpFileFind *pDebugFileFind = new CFtpFileFind(m_pFtpConnection);
	BOOL bOK = pDebugFileFind->FindFile("*");
	while(bOK)
	{
		bOK = pDebugFileFind->FindNextFile();
		TRACE("%s\n", pDebugFileFind->GetFilePath());
	}
	pDebugFileFind->Close();
	
	delete pDebugFileFind;
	pDebugFileFind = NULL;

#endif

	try		//Ftp Connection
	{
#ifdef _DEBUG
//		BOOL aa = m_pFtpConnection->SetCurrentDirectory("resultData//ch01//");
//		BOOL aa = m_pFtpConnection->SetCurrentDirectory("formation_data/monitoringData/group1/");
//		aa = m_pFtpConnection->SetCurrentDirectory("formation_data");
//		m_pFtpConnection->GetCurrentDirectory(szBuff, &nLength);
//		TRACE("FTP current directory is %s\n", szBuff);
//		aa = m_pFtpConnection->SetCurrentDirectory("monitoringData");
//		m_pFtpConnection->GetCurrentDirectory(szBuff, &nLength);
//		TRACE("FTP current directory is %s\n", szBuff);
//		if(aa == FALSE)
//		{
//			return -6;
//		}
//
//		m_pFtpConnection->GetCurrentDirectory(szBuff, &nLength);
//		TRACE("FTP current directory is %s\n", szBuff);

#endif		
		CFtpFileFind finder(m_pFtpConnection);
		BOOL bContinue = finder.FindFile(strRemoteFile);

		if(bContinue)
		{
			bContinue = finder.FindNextFile();
			if(!finder.IsDirectory())
			{
				if(m_pFtpConnection->GetFile(strRemoteFile , strLoacalFile, !bOverWrite) == FALSE)
				{
					m_strLastErrorMsg.Format("%s down load fail to %s.", strRemoteFile, strLoacalFile);
					finder.Close();
					return -2;	//실패 
				}
				finder.Close();
			}
			else	//direcotry
			{
				m_strLastErrorMsg.Format(" %s is directory", strRemoteFile);
				finder.Close();
				return -3;	//
			}
		}
		else	// file not found
		{
			m_strLastErrorMsg = "File not found";
			finder.Close();
			return -4;	//실패 
		}
	}
	catch(CInternetException *pEx)
	{
		TCHAR sz[1024];
		pEx->GetErrorMessage(sz, 1024);
	    pEx->Delete();
		m_strLastErrorMsg = sz;
		return -5;	//실패 
	}
	return 1;	//성공 		
}

//connect ftp to module
BOOL CFtpDownLoad::ConnectFtpSession(CString strIPAddress, CString strID, CString strPWD)
{
	// TODO: Add your control notification handler code here

	//IP Address Check
	if(strIPAddress.IsEmpty())
	{
		m_strLastErrorMsg = TEXT_LANG[0];//"접속 IP 정보가 입력되지 않았습니다."
		return FALSE;
	}

//	CString strID, strPwd;
//	strID = AfxGetApp()->GetProfileString(FTP_REG_CONFIG, "Login ID", "root");				//root 접근 권한으로 접속
//	strPwd = AfxGetApp()->GetProfileString(FTP_REG_CONFIG, "Login Password", "dusrnth");	//"연구소"
	m_pInternetSession = new CInternetSession( _T("PNE_FtpDownLoad/1.0"));
	m_pInternetSession->SetOption(INTERNET_OPTION_CONTROL_RECEIVE_TIMEOUT, INTERNET_CONNECTION_TIMEOUT);
	
	// 1. ftp 통신에서 사용가능한 옵션
	// m_pInternetSession->SetOption(INTERNET_OPTION_CONNECT_TIMEOUT, 3000);	
	// m_pInternetSession->SetOption(INTERNET_OPTION_DATA_SEND_TIMEOUT, 3000);
	// m_pInternetSession->SetOption(INTERNET_OPTION_DATA_RECEIVE_TIMEOUT, 3000);
	// m_pInternetSession->SetOption(INTERNET_OPTION_CONTROL_SEND_TIMEOUT, 3000);

	ASSERT(m_pInternetSession);

	CWaitCursor cursor;	
	
	try		//Ftp Connection
	{
		m_pFtpConnection = m_pInternetSession->GetFtpConnection(strIPAddress, strID, strPWD);
    }
	catch(CInternetException *pEx)
	{
		TCHAR sz[1024];
		pEx->GetErrorMessage(sz, 1024);
	    pEx->Delete();
		m_strLastErrorMsg = sz;
		m_pFtpConnection = NULL;
		return FALSE;
	}
	return TRUE;
}

CString CFtpDownLoad::GetCurrentDirecotry()
{
	//first call ConnectFtpSession()
	ASSERT(m_pFtpConnection);
	char szBuff[_MAX_PATH];
	DWORD nLength = _MAX_PATH;
	m_pFtpConnection->GetCurrentDirectory(szBuff, &nLength);
	CString strFolder(szBuff);
	return strFolder;

}

BOOL CFtpDownLoad::SetCurrentDirecotry(CString strCurDir)
{
	ASSERT(m_pFtpConnection);
	return m_pFtpConnection->SetCurrentDirectory(strCurDir);
}

CString CFtpDownLoad::GetLastErrorMsg()
{
	return m_strLastErrorMsg;
}
