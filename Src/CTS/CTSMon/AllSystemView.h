#if !defined(AFX_ALLSYSTEMVIEW_H__781ADBA8_2E8D_4FC0_AC1E_2F7CA334FB28__INCLUDED_)
#define AFX_ALLSYSTEMVIEW_H__781ADBA8_2E8D_4FC0_AC1E_2F7CA334FB28__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AllSystemView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CAllSystemView form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

#include "MyGridWnd.h"
#include "IconCombo.h"
#include "afxwin.h"
#include "CTSMonDoc.h"
#include "ColorButton2.h"
#include "PingThread.h"

#define ALL_SYSTEM_GRID_COL		10
#define _GRID_COL_MODULE_ID		0
#define	_GRID_COL_MODULE_NAME	1
#define _GRID_COL_GROUP_NAME	2
#define _GRID_COL_STATE			3
#define _GRID_COL_PROC_NAME		4
#define _GRID_COL_TEMP			5
#define _GRID_COL_STEP_NO		6
#define _GRID_COL_TRAY_STATE	7
#define _GRID_COL_TRAY_NAME		8
#define _GRID_COL_JIG_STATE		9
#define _GRID_COL_DOOR_STATE	10

#define TIMERID_START_JIGTEMP_TARGET_DATA		2
#define TIMERID_SEND_JIGTEMP_TARGET_DATA		3
#define TIMERINTERVAL_SEND_JIGTEMP_TARGET_DATA	1000
#define TIMERINTERVAL_START_JIGTEMP_TARGET_DATA	1000 * 60 * 5
#define TIMERINTERVAL_SAFECHK_CHVOLTAGE			1000 * 3		//ChVoltage 안전조건확인 

#define Tray_Inserted_Color		RGB(255, 220, 220)		//Tray가 Loading되었을때 Color
#define Normal_Color			RGB(255, 255, 255)		//평상시 Color

#define GRID_DATA_FONT_SIZE	16
#define LABEL_LOG_FONT_SIZE		16

#define LOG_SELECT_TYPE_ALL		0
#define LOG_SELECT_TYPE_PREV	1
#define LOG_SELECT_TYPE_NEXT	2
#define LOG_SELECT_TYPE_DELETE  3

class CAllSystemView : public CFormView
{
protected:
	CAllSystemView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CAllSystemView)

// Form Data
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	//{{AFX_DATA(CAllSystemView)
	enum { IDD = IDD_ALL_SYSTEM_VIEW };
	enum { STATUS_VIEW = 0, 
		OPERATION_MODE_VIEW, 
		// ITEM_PASS_TIME_VIEW,
		INSPECTION_PASS_TIME_VIEW, 
		OPERATION_VIEW,
		TEMP_VIEW };

	enum {SELECT_MAIN_ITEM = 0,
		SELECT_SUB_ITEM};

	CComboBox	m_ctrlCodeCombo;
	CIconCombo	m_ctrlStateSelCombo;
	CLabel	m_ctrlRunModule;
	CLabel	m_ctrlIdleModule;
	CLabel	m_ctrlLineOffModule;
	CLabel	m_ctrlTotalModule;
	BOOL	m_bLayoutView;
	BOOL	m_bCodeCount;
	BOOL	m_bUpdateModule;

	INT		m_iFileSaveIntver;
	BOOL	m_bFileFirstSave;
	//}}AFX_DATA

// Attributes
public:
	BOOL m_bRackIndexUp;
	CImageList *m_pImagelist;	
	CPtrArray m_ptCellVoltageDlg;	

// Operations
public:	
	CCTSMonDoc* GetDocument();
	
	BOOL GetSelectedModuleList(CWordArray &awSelModule);	
	void UpdateModuleStateCount();
	void InitStageView();		// Stage 선택시 실행
	// BOOL StateReDraw(int nModuleID);	kky
	int GetCurModuleID();
	int GetModuleID(ROWCOL nRow);
	BOOL AddModuleList(int nModuleID);
	void DrawModuleGrid();
	void ModuleDisConnected(int nModuleID);
	void FnSetFmsCommState(bool bConnected = false);
	bool DrawModule(int nModuleID, BOOL bUseGroup = TRUE );
	bool UpdateModule(int nModuleID = -1);
	void UpdateGroupState(int nModuleID, int nGroupIndex);
	CString		ProcUnitTempData( int nModuleID );
	void		DrawTempData( int nModuleID );
	void		UpdateEmgLog(int nType = LOG_SELECT_TYPE_ALL);		// 0:전체갱신
	BOOL DeleteErrorHistory(int nInterval);
			
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAllSystemView)
	public:
	virtual void OnInitialUpdate();	
	virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo = NULL);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
protected:	
	COLORREF m_crBackground;
	CBrush m_wndbkBrush;	// background brush
	CMyGridWnd	m_wndAllSystemGrid;
	CMyGridWnd	m_wndLayoutGrid;	

	void	InitColorBtn();
	void	InitLayoutGrid();
	void	InitSystemGrid();
	void	InitTempGrid();

	void	InitLabel();
	void	DrawGrid();	
	int		m_nCurErrorCode;
	
	int		m_nCurModuleID;
	int		m_nPrevModuleID;

	int		m_nCurGroup;
	int		m_nLayOutCol;
	int		m_nLayOutRow;
	BOOL	m_bFlash;
	BOOL	m_bSendMsgCmd;							// 사용자 메세지 전송중 유무
	int		m_nModuleID;
	
	int		m_nSelectItem;					// ALL_SYSTEM_VIEW 선택 아이템
	
	int		m_nJigTempAvg;
	int		m_nJigTempSendUnitIndex;
	int		m_nTotalModule;
	long	m_lUnitJigTempAvg;
	
	bool	m_bFmsConnectState;
	int		m_nPreDay;
	int		m_nPreMinute;

	bool m_bWriteTemperatureFlag;
			
	CMenu	menu;	
	int		m_nTotalUnitNum;
	BOOL	m_bLocalReservation[EP_MAX_MODULE_NUM];
	
	CPingThread m_pingFms;
	CPingThread	m_pingPneMonitoringsystme;
	void	Fun_PingChk();
	
	virtual ~CAllSystemView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CAllSystemView)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSelchangeStageSelCombo();
	afx_msg void OnRunModule();
	afx_msg void OnStop();
	afx_msg void OnReconnect();
	afx_msg void OnUpdateReconnect(CCmdUI* pCmdUI);
	afx_msg void OnUpdateRunModule(CCmdUI* pCmdUI);
	afx_msg void OnUpdateStop(CCmdUI* pCmdUI);
	afx_msg void OnPause();
	afx_msg void OnUpdatePause(CCmdUI* pCmdUI);
	afx_msg void OnContinueModule();
	afx_msg void OnUpdateContinueModule(CCmdUI* pCmdUI);
	afx_msg void OnClear();
	afx_msg void OnUpdateClear(CCmdUI* pCmdUI);
	afx_msg void OnStepover();
	afx_msg void OnUpdateStepover(CCmdUI* pCmdUI);
	afx_msg void OnDispType();
	afx_msg void OnOnlineMode();
	afx_msg void OnUpdateOnlineMode(CCmdUI* pCmdUI);
	afx_msg void OnOfflineMode();
	afx_msg void OnUpdateOfflineMode(CCmdUI* pCmdUI);
	afx_msg void OnMaintenanceMode();
	afx_msg void OnUpdateMaintenanceMode(CCmdUI* pCmdUI);
	afx_msg void OnControlMode();
	afx_msg void OnUpdateControlMode(CCmdUI* pCmdUI);
	afx_msg void OnReboot();
	afx_msg void OnFileSave();
	afx_msg void OnUpdateAutoProcOn(CCmdUI* pCmdUI);
	afx_msg void OnUpdateAutoProcOff(CCmdUI* pCmdUI);
	afx_msg void OnAutoProcOn();
	afx_msg void OnAutoProcOff();
	afx_msg void OnRefAdValueUpdate();
	afx_msg void OnTraynoUserInput();
	afx_msg void OnUpdateTraynoUserInput(CCmdUI* pCmdUI);
	afx_msg void OnViewResult();
	afx_msg void OnUpdateViewResult(CCmdUI* pCmdUI);
	afx_msg void OnConditionView();
	afx_msg void OnUpdateConditionView(CCmdUI* pCmdUI);
	afx_msg void OnDataRestore();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnDestroy();
	afx_msg void OnUpdateDataRestore(CCmdUI* pCmdUI);
	afx_msg void OnCheckCode();
	afx_msg void OnEditCopy();
	afx_msg void OnUpdateEditCopy(CCmdUI* pCmdUI);
	afx_msg void OnUpdateChangeTrayType1(CCmdUI* pCmdUI);
	afx_msg void OnUpdateChangeTrayType2(CCmdUI* pCmdUI);
	afx_msg void OnChangeTrayType1();
	afx_msg void OnChangeTrayType2();
	afx_msg void OnLayoutView();
	afx_msg void OnSelchangeErrorcodeSelCombo();
	afx_msg void OnGetProfileData();
	afx_msg void OnNewProc();
	afx_msg void OnUpdateNewProc(CCmdUI* pCmdUI);
	//}}AFX_MSG
	afx_msg LONG OnGridDoubleClick(WPARAM wParam, LPARAM lParam);
	afx_msg	LRESULT OnRButtonClickedRowCol(WPARAM wParam, LPARAM lParam);
	afx_msg LONG OnMovedCurrentCell(WPARAM wParam, LPARAM lParam);
	afx_msg LONG OnGridSelChanged(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRecvPingStatus(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
private:
	int		GetModuleID(ROWCOL nRow, ROWCOL nCol);
	BOOL	GetModuleRowCol(int nModuleI, int &nRow, int &nCol);
public:
	afx_msg void OnBnClickedBtnStatus();
	afx_msg void OnBnClickedBtnOperationMode();
	afx_msg void OnBnClickedBtnOperationView();
	afx_msg void OnBnClickedBtnInspectionPassTime();	
	CLabel m_LabelSelectName;	
	CLabel m_LabelLog;
	CLabel m_LabelLogNum;
	CLabel m_LabelLogTime;
	
	int m_nTemp;
	
protected:
	virtual void OnDraw(CDC* /*pDC*/);
public:
	afx_msg void OnPrecision();
	afx_msg void OnSystemSetting();
	afx_msg void OnErrorHistory();
	CColorButton2 m_Btn_Status;
	CColorButton2 m_Btn_OperationMode;
	CColorButton2 m_Btn_InspectionPassTime;
	CColorButton2 m_Btn_OperationView;	
	CColorButton2 m_Btn_TypeSetting;
	CColorButton2 m_Btn_Temperature;
	afx_msg void OnBnClickedBtnTypeSettingLocal();	
	afx_msg void OnBnClickedBtnLogNext();
	afx_msg void OnBnClickedBtnLogPrev();
	afx_msg void OnBnClickedBtnLogDelete();
	CColorButton2 m_BtnLogPrev;
	CColorButton2 m_BtnLogNext;
	CColorButton2 m_BtnLogDelete;
	afx_msg void OnBnClickedBtnErrorHistory();
	CColorButton2 m_BtnErrorHistory;
	CColorButton2 m_BtnAlarmOff;	
	afx_msg void OnAutoMode();
	afx_msg void OnUpdateAutoMode(CCmdUI *pCmdUI);


	//////////////////////////////////////////////////////////////////////////

	INT strLinker(CHAR *msg, VOID * _dest, INT* mesmap);
	CLabel m_LabelTemp1;
	CLabel m_LabelTemp2;
	CLabel m_LabelTemp3;
	CLabel m_LabelTemp4;
	CLabel m_LabelTemp5;
	CLabel m_LabelTemp6;
	CLabel m_LabelTemp7;
	CLabel m_LabelTemp8;
	CLabel m_Label9;
	CLabel m_Label10;
	CLabel m_Label11;
	CLabel m_Label12;
	CLabel m_Label13;
	CLabel m_Label14;
	CLabel m_Label15;
	CLabel m_Label16;
	CLabel m_LabelFmsConnectState;
	afx_msg void OnBnClickedBtnChargerAlarmoff();
	CLabel m_LabelDate;
	CLabel m_LabelTemp17;
	CLabel m_LabelTemp18;
	CLabel m_LabelTemp19;
	CLabel m_LabelTemp20;
	CLabel m_LabelTemp21;
	CLabel m_LabelTemp22;
	CLabel m_LabelTemp23;
	CLabel m_LabelTemp24;
	CLabel m_Label25;
	CLabel m_Label26;
	CLabel m_Label27;
	CLabel m_Label28;
	CLabel m_Label29;
	CLabel m_Label30;
	CLabel m_Label31;
	CLabel m_Label32;
	afx_msg void OnBnClickedBtnTemperature();	
// 20210127 KSCHOI Add ReAging Enable Stage Function START
	void OnReAgingFunction(int bReAging);
	void OnReAgingFunctionEnable();
	void OnReAgingFunctionDisable();
// 20210127 KSCHOI Add ReAging Enable Stage Function END
};

#ifndef _DEBUG  // debug version in TopView.cpp
inline CCTSMonDoc* CAllSystemView::GetDocument()
   { return (CCTSMonDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ALLSYSTEMVIEW_H__781ADBA8_2E8D_4FC0_AC1E_2F7CA334FB28__INCLUDED_)
