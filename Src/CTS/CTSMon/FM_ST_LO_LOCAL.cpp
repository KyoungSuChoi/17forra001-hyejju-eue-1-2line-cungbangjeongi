#include "stdafx.h"

#include "CTSMon.h"

#include "FMS.h"
#include "FM_Unit.h"

#include "FM_STATE.h"

CFM_ST_LO_LOCAL::CFM_ST_LO_LOCAL(FM_STATUS_ID _Eqstid, FM_STATUS_ID _stid, CFM_Unit* _unit)
:CFM_ST_LOCAL(_Eqstid, _stid, _unit)
{
}


CFM_ST_LO_LOCAL::~CFM_ST_LO_LOCAL(void)
{
}

VOID CFM_ST_LO_LOCAL::fnEnter()
{
	//TRACE("%s \n", g_strSBCState[m_Unit->fnGetModule()->GetState()]);
	// if(m_Unit->fnGetFMS()->fnGetError() == FMS_ST_ERROR)
	if( m_Unit->fnGetModule()->GetTrayInfo(0)->m_nResultFileType
		!= EP_RESULTFILE_TYPE_NORMAL )
	{
		CHANGE_STATE(LOCAL_ST_ERROR);
		return;
	}

	switch(m_Unit->fnGetModule()->GetState())
	{
	case EP_STATE_IDLE:
		if(m_Unit->fnGetModuleTrayState())
		{
			fnSetFMSStateCode(FMS_ST_TRAY_IN);
		}
		else
		{
			fnSetFMSStateCode(FMS_ST_VACANCY);
		}
		break;
	case EP_STATE_STANDBY:
		{
			fnSetFMSStateCode(FMS_ST_TRAY_IN);
		}
		break;
	case EP_STATE_PAUSE:
		{
			fnSetFMSStateCode(FMS_ST_ERROR);
		}
		break;
	case EP_STATE_RUN:
		{
			if(m_Unit->fnGetModuleCheckState())
			{
				fnSetFMSStateCode(FMS_ST_CONTACT_CHECK);
			}
			else
			{
				fnSetFMSStateCode(FMS_ST_RUNNING);
			}
		}
		break;
	case EP_STATE_READY:
		{
			fnSetFMSStateCode(FMS_ST_READY);
		}
		break;
	case EP_STATE_END:
		{
			fnSetFMSStateCode(FMS_ST_END);
		}
		break;
	case EP_STATE_ERROR:
		{
			fnSetFMSStateCode(FMS_ST_ERROR);
		}
		break;
	}
}

VOID CFM_ST_LO_LOCAL::fnProc()
{
}

VOID CFM_ST_LO_LOCAL::fnExit()
{
}

VOID CFM_ST_LO_LOCAL::fnSBCPorcess(WORD _state)
{
	CFM_ST_LOCAL::fnSBCPorcess(_state);
}

FMS_ERRORCODE CFM_ST_LO_LOCAL::fnFMSPorcess(FMS_COMMAND _msgId, st_FMS_PACKET* _recvData)
{
	FMS_ERRORCODE _error = ER_NO_Process;

	switch(_msgId)
	{
	default:
		{
		}
		break;
	}

	return _error;

}