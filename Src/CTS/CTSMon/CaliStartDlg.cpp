// CaliStartDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ctsmon.h"
#include "CaliStartDlg.h"
#include "CTSMonDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCaliStartDlg dialog


CCaliStartDlg::CCaliStartDlg(int nModuleID, int nVRangeCnt, int IRangeCnt,int nTrayType, CWnd* pParent /*=NULL*/)
	: CDialog(CCaliStartDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCaliStartDlg)
	m_strUserName = _T("");
	m_bMultiMode = FALSE;
	//}}AFX_DATA_INIT
	m_unitno = nModuleID;
	n_main = 1;
	n_vi = 0;
	n_board = 1;
	n_cali = 0;
	n_range = 1;
	n_vRangeCnt = nVRangeCnt;
	n_iRangeCnt = IRangeCnt;
	m_bChannelMode = FALSE;
	n_iTrayType = nTrayType;	//ljb 2011519 

	LanguageinitMonConfig();
}

CCaliStartDlg::~CCaliStartDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

bool CCaliStartDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCaliStartDlg"), _T("TEXT_CCaliStartDlg_CNT"), _T("TEXT_CCaliStartDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CCaliStartDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCaliStartDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

void CCaliStartDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCaliStartDlg)
	DDX_Control(pDX, IDC_RANGE_COMBO1, m_ctrlRange);
	DDX_Control(pDX, IDC_VI_COMBO, m_ctrlVI);
	DDX_Control(pDX, IDC_MAIN_COMBO, m_ctrlMain);
	DDX_Control(pDX, IDC_CALI_COMBO, m_ctrlCali);
	DDX_Control(pDX, IDC_BD_COMBO, m_ctrlBD);
	DDX_Text(pDX, IDC_EDIT_USER, m_strUserName);
	DDX_Check(pDX, IDC_CHECK_MULTI, m_bMultiMode);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CCaliStartDlg, CDialog)
	//{{AFX_MSG_MAP(CCaliStartDlg)
	ON_BN_CLICKED(IDC_START_BUTTON, OnStartButton)
	ON_CBN_SELCHANGE(IDC_VI_COMBO, OnSelchangeViCombo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCaliStartDlg message handlers

BOOL CCaliStartDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
		
	// TODO: Add extra initialization here
	m_ctrlMain.ResetContent();
	//m_ctrlMain.AddString("All"); //지원하지 않음
	m_ctrlMain.AddString("Main DA"); //0
	m_ctrlMain.AddString("CH"); //1
	m_ctrlMain.SetCurSel(n_main);

	m_ctrlCali.ResetContent();
	m_ctrlCali.AddString(TEXT_LANG[0]);//"교정 후 확인"
	m_ctrlCali.AddString(TEXT_LANG[1]);//"교정"
	m_ctrlCali.AddString(TEXT_LANG[2]);//"확인"
	m_ctrlCali.SetCurSel(n_cali);

	m_ctrlVI.ResetContent();
//	m_ctrlVI.AddString("All");
	m_ctrlVI.AddString(TEXT_LANG[3]);//"전압"
	m_ctrlVI.AddString(TEXT_LANG[4]);//"전류"
	m_ctrlVI.SetCurSel(n_vi);
	
 	m_ctrlBD.ResetContent();
//	m_ctrlBD.AddString("All");		//지원하지 않음
	EP_MD_SYSTEM_DATA *pSysData = ::EPGetModuleSysData(EPGetModuleIndex(m_unitno));
 	for(int j=0; j<pSysData->wInstalledBoard;j++)
 	{
		CString boardNo;
 		boardNo.Format("Board #%d",j+1);
		m_ctrlBD.AddString(boardNo);
	}

	if(n_board <= pSysData->wInstalledBoard && n_board > 0)
 		m_ctrlBD.SetCurSel(n_board-1);
	else
 		m_ctrlBD.SetCurSel(0);
	
	if(m_bChannelMode == TRUE)
	{
		m_ctrlBD.EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_MULTI)->EnableWindow(FALSE);
	}

	OnSelchangeViCombo();

	GetDlgItem(IDC_UNITNO_STATIC)->SetWindowText(GetModuleName(m_unitno));

	//ljb 2011519 
	CString strTrayName;
	if (n_iTrayType == 0) strTrayName = TEXT_LANG[5];//"교정 TRAY"
	else if (n_iTrayType == 1)	strTrayName = TEXT_LANG[6];//"실측 TRAY"
	else strTrayName.Empty();
	GetDlgItem(IDC_LAB_TRAY_TYPE)->SetWindowText(strTrayName);

	
	//현재 Login ID 읽어온다.
	char szName[512];
	DWORD lLength = 512;
	::GetUserName(szName, &lLength);
	m_strUserName = szName;

	UpdateData(FALSE);	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCaliStartDlg::OnStartButton() 
{
	// TODO: Add your control notification handler code here	
	UpdateData(TRUE);

	n_cali	= m_ctrlCali.GetCurSel();
	n_main	= m_ctrlMain.GetCurSel();
	n_vi	= m_ctrlVI.GetCurSel();
	n_board = m_ctrlBD.GetCurSel()+1;
	n_range = m_ctrlRange.GetCurSel();

	if(IDYES != MessageBox(TEXT_LANG[7], TEXT_LANG[8], MB_YESNO|MB_ICONQUESTION))//"작업 시작을 실행하시겠습니까?"//"전송 확인"
	{
		return;
	}	
	CDialog::OnOK();
}

void CCaliStartDlg::OnSelchangeViCombo() 
{
	// TODO: Add your control notification handler code here

	n_vi = m_ctrlVI.GetCurSel();
	int nRangeCnt = 0;
	if(n_vi == 0)		//voltage
	{
		nRangeCnt = n_vRangeCnt;
	}
	else
	{
		nRangeCnt = n_iRangeCnt;
	}

	m_ctrlRange.ResetContent();
	if(nRangeCnt == 1)
	{
		m_ctrlRange.AddString("ALL Range");
		m_ctrlRange.AddString("Range #1");
	}
	else if(nRangeCnt == 2)
	{
		m_ctrlRange.AddString("ALL Range");
		m_ctrlRange.AddString("Range #1(High)");
		m_ctrlRange.AddString("Range #2(Low)");
	}
	else if(nRangeCnt == 3)
	{
		m_ctrlRange.AddString("ALL Range");
		m_ctrlRange.AddString("Range #1(High)");
		m_ctrlRange.AddString("Range #2(Middle)");
		m_ctrlRange.AddString("Range #3(Low)");
	}
	else if(nRangeCnt == 4)
	{
		m_ctrlRange.AddString("ALL Range");
		m_ctrlRange.AddString("Range #1(High)");
		m_ctrlRange.AddString("Range #2(Middle)");
		m_ctrlRange.AddString("Range #3(Low)");
		m_ctrlRange.AddString("Range #4(Mico)");
	}
		//각 Range 수량에 맞도로 수정
	if(nRangeCnt == 1 )
	{
		m_ctrlRange.SetCurSel(1);	
	}
	else 
	{
		if(n_range < nRangeCnt)
		{
			m_ctrlRange.SetCurSel(n_range);	
		}
		else
		{
			m_ctrlRange.SetCurSel(0);	
		}
	}
}
