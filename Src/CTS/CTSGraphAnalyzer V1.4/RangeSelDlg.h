#if !defined(AFX_RANGESELDLG_H__A77EC8E2_D993_4167_9B81_9D10C613DF44__INCLUDED_)
#define AFX_RANGESELDLG_H__A77EC8E2_D993_4167_9B81_9D10C613DF44__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RangeSelDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CRangeSelDlg dialog
#define MAX_EXCEL_ROW	65536
#define MAX_RECORD_ROW_COUNT	10000
#define MAX_RAW_DATA_COUNT		43200	//12�ð�

class CRangeSelDlg : public CDialog
{
// Construction
public:
	long m_lMax;
	CRangeSelDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CRangeSelDlg();
	CString *TEXT_LANG;	
	BOOL LanguageinitMonConfig();

// Dialog Data
	//{{AFX_DATA(CRangeSelDlg)
	enum { IDD = IDD_RANGE_SEL_DIALOG };
	long	m_lFrom;
	long	m_lTo;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRangeSelDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CRangeSelDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnChangeFromEdit();
	virtual void OnOK();
	afx_msg void OnChangeToEdit();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RANGESELDLG_H__A77EC8E2_D993_4167_9B81_9D10C613DF44__INCLUDED_)
