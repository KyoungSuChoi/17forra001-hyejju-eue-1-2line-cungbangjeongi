VERSION 4.00
Begin VB.Form Form3 
   Caption         =   "Form3"
   ClientHeight    =   5100
   ClientLeft      =   912
   ClientTop       =   1380
   ClientWidth     =   7656
   Height          =   5472
   Left            =   864
   LinkTopic       =   "Form3"
   ScaleHeight     =   5100
   ScaleWidth      =   7656
   Top             =   1056
   Width           =   7752
   Begin PesgoLib.Pesgo Pesgo1 
      Height          =   4932
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   7452
      _version        =   65536
      _extentx        =   13145
      _extenty        =   8700
      _stockprops     =   96
      _allprops       =   "Form3.frx":0000
   End
End
Attribute VB_Name = "Form3"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()

Dim i%, s%, p%
Dim f, f2, f3 As Single
Dim d As Double
Dim dt As Double
dt = Now

Pesgo1.PrepareImages = True
Pesgo1.Subsets = 12
Pesgo1.Points = 200
i% = 0
For p% = 0 To 199
    f = 150 + (Sin(p% * 0.034) * 60) + (Rnd * 30) + (p% / 2)
    f2 = 10 + (Rnd * 10)
    f3 = 35 + (Rnd * 20)
    d = (dt + p%)
    Pesgo1.XData(0, p%) = d
    Pesgo1.YData(0, p%) = f - f3
    
    Pesgo1.XData(1, p%) = d
    Pesgo1.YData(1, p%) = f + f3
    
    Pesgo1.XData(2, p%) = d
    Pesgo1.YData(2, p%) = f - f2
    
    Pesgo1.XData(3, p%) = d
    Pesgo1.YData(3, p%) = f + f2
    
    f = 250 + (Cos(p% * 0.06) * 70) + (Rnd * 40) + (p% / 2)
    Pesgo1.XData(4, p%) = d
    Pesgo1.YData(4, p%) = f
    
    Pesgo1.XData(5, p%) = d
    Pesgo1.YData(5, p%) = f + (Rnd * 90)
    f = -10 + (Rnd * 30)
    Pesgo1.XData(6, p%) = d
    Pesgo1.YData(6, p%) = f
    f = -20 + (Rnd * 50)
    Pesgo1.XData(7, p%) = d
    Pesgo1.YData(7, p%) = f
    f = -50 + (Cos(p% * 0.06) * 70) + (Rnd * 30) + (p%)
    Pesgo1.XData(8, p%) = d
    Pesgo1.YData(8, p%) = f
    f = 100 + (Sin(p% * 0.15) * 100) + (Rnd * 20) + (p%)
    Pesgo1.XData(9, p%) = d
    Pesgo1.YData(9, p%) = f
Next p%

For s% = 10 To 11
    For p% = 0 To 199
        d = dt + p%
        Pesgo1.XData(s%, p%) = d
        Pesgo1.YData(s%, p%) = 15 + (Rnd * 75) + (s% * 100)
    Next p%
Next s%

'** Set SubsetLabels property array for 4 subsets **'
Pesgo1.SubsetLabels(0) = "Texas"
Pesgo1.SubsetLabels(1) = "RY Subset"

For p% = 0 To 13
    Pesgo1.SubsetLineTypes(p%) = 0
Next p%

Pesgo1.SubsetColors(0) = QBColor(9)
Pesgo1.SubsetColors(4) = 0
Pesgo1.SubsetColors(5) = 0

Pesgo1.SubsetColors(6) = QBColor(12) 'bright red
Pesgo1.SubsetColors(7) = QBColor(0)
Pesgo1.SubsetColors(8) = QBColor(10) 'bright green
Pesgo1.SubsetColors(9) = 0

Pesgo1.AxesAnnotationTextSize = 96
Pesgo1.XAxisAnnotation(0) = dt + 16
Pesgo1.XAxisAnnotation(1) = dt + 18
Pesgo1.XAxisAnnotation(2) = dt + 20
Pesgo1.XAxisAnnotation(3) = dt + 22
Pesgo1.XAxisAnnotationText(0) = "Axis Annotation 1"
Pesgo1.XAxisAnnotationText(1) = "Axis Annotation 2"
Pesgo1.XAxisAnnotationText(2) = "Axis Annotation 3"
Pesgo1.XAxisAnnotationText(3) = "Axis Annotation 4"
Pesgo1.XAxisAnnotationColor(0) = QBColor(0)
Pesgo1.XAxisAnnotationColor(1) = QBColor(0)
Pesgo1.XAxisAnnotationColor(2) = QBColor(0)
Pesgo1.XAxisAnnotationColor(3) = QBColor(0)

Pesgo1.HorzLineAnnotationAxis(0) = 0
Pesgo1.HorzLineAnnotation(0) = 300#
Pesgo1.HorzLineAnnotationType(0) = PELT_DASH
Pesgo1.HorzLineAnnotationColor(0) = QBColor(12)
Pesgo1.HorzLineAnnotationText(0) = "UPPER LIMIT"

Pesgo1.HorzLineAnnotationAxis(1) = 2
Pesgo1.HorzLineAnnotation(1) = 100#
Pesgo1.HorzLineAnnotationType(1) = PELT_DASHDOT
Pesgo1.HorzLineAnnotationColor(1) = QBColor(8)
Pesgo1.HorzLineAnnotationText(1) = "median"
 
Pesgo1.VertLineAnnotation(0) = dt + 60#
Pesgo1.VertLineAnnotationType(0) = PELT_DASH
Pesgo1.VertLineAnnotationColor(0) = QBColor(0)
Pesgo1.VertLineAnnotationText(0) = "|c2nd Qrt"
Pesgo1.VertLineAnnotation(1) = dt + 120#
Pesgo1.VertLineAnnotationType(1) = PELT_DASH
Pesgo1.VertLineAnnotationColor(1) = QBColor(0)
Pesgo1.VertLineAnnotationText(1) = "|c3rd Qrt"
Pesgo1.VertLineAnnotation(2) = dt + 180#
Pesgo1.VertLineAnnotationType(2) = PELT_DASH
Pesgo1.VertLineAnnotationColor(2) = QBColor(0)
Pesgo1.VertLineAnnotationText(2) = "|c4th Qrt"

Pesgo1.GraphAnnotationTextSize = 96
Pesgo1.GraphAnnotationAxis(0) = 3
Pesgo1.GraphAnnotationX(0) = dt + 65
Pesgo1.GraphAnnotationY(0) = 950
Pesgo1.GraphAnnotationType(0) = PEGAT_NOSYMBOL 'text only
Pesgo1.GraphAnnotationText(0) = "Annotation Text Only"
Pesgo1.GraphAnnotationColor(0) = QBColor(0)

Pesgo1.GraphAnnotationAxis(1) = 3
Pesgo1.GraphAnnotationX(1) = dt + 78
Pesgo1.GraphAnnotationY(1) = 1161
Pesgo1.GraphAnnotationType(1) = PEGAT_POINTER
Pesgo1.GraphAnnotationText(1) = "Text with Pointer"
Pesgo1.GraphAnnotationColor(1) = QBColor(0)

Pesgo1.GraphAnnotationAxis(2) = 3
Pesgo1.GraphAnnotationX(2) = dt + 138
Pesgo1.GraphAnnotationY(2) = 950
Pesgo1.GraphAnnotationType(2) = PEGAT_SMALLUPTRIANGLESOLID
Pesgo1.GraphAnnotationText(2) = "Symbol w/wo Text"
Pesgo1.GraphAnnotationColor(2) = QBColor(0)

Pesgo1.AllowDataHotSpots = True

'This will disable all subset legends
Pesgo1.SubsetsToLegend(0) = -1

Pesgo1.MultiAxesSubsets(0) = 6
Pesgo1.MultiAxesSubsets(1) = 2
Pesgo1.MultiAxesSubsets(2) = 2
Pesgo1.MultiAxesSubsets(3) = 2

Pesgo1.MultiAxesProportions(0) = 0.5
Pesgo1.MultiAxesProportions(1) = 0.15
Pesgo1.MultiAxesProportions(2) = 0.15
Pesgo1.MultiAxesProportions(3) = 0.2

Pesgo1.WorkingAxis = 0
Pesgo1.PlottingMethod = 11  '15
Pesgo1.SpecificPlotMode = 5
Pesgo1.RYAxisComparisonSubsets = 2
Pesgo1.YAxisLabel = "Y Axis #1"
Pesgo1.RYAxisLabel = "Y Axis #2"
Pesgo1.ManualScaleControlY = 3
Pesgo1.ManualMinY = 1
Pesgo1.ManualMaxY = 400

Pesgo1.WorkingAxis = 1
Pesgo1.PlottingMethod = 9 '5
Pesgo1.YAxisLabel = "Y Axis #3"

Pesgo1.WorkingAxis = 2
Pesgo1.PlottingMethod = 9
Pesgo1.RYAxisComparisonSubsets = 1
Pesgo1.YAxisLabel = "Y Axis #4"
Pesgo1.RYAxisLabel = "Y Axis #5"

Pesgo1.WorkingAxis = 3
Pesgo1.PlottingMethod = 9
Pesgo1.ManualScaleControlY = 1
Pesgo1.ManualMinY = 900
Pesgo1.ManualMaxY = 1500
Pesgo1.YAxisLabel = "Y Axis #6"

Pesgo1.WorkingAxis = 0

Pesgo1.DataShadows = 0
Pesgo1.DataPrecision = 1
Pesgo1.MainTitle = "Multi Y Axes Example"
Pesgo1.SubTitle = ""
Pesgo1.LabelBold = True
Pesgo1.ShowAnnotations = True
Pesgo1.AllowAnnotationControl = True
Pesgo1.AllowZooming = 1
Pesgo1.ScrollingHorzZoom = True
Pesgo1.CursorMode = 2
Pesgo1.CursorPromptStyle = 0
Pesgo1.CursorPromptTracking = True
Pesgo1.MouseCursorControl = True
Pesgo1.GridLineControl = 3
Pesgo1.FontSize = 2
Pesgo1.DateTimeMode = 1
Pesgo1.FocalRect = 0

Pesgo1.AllowZooming = 3
Pesgo1.ScrollingHorzZoom = False
Pesgo1.FontSizeGlobalCntl = 1.1
Pesgo1.LineAnnotationTextSize = 90
Pesgo1.PEactions = 0

End Sub


