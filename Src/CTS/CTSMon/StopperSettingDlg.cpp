// StopperSettingDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "StopperSettingDlg.h"

#define EDIT_FONT_SIZE 30

// CStopperSettingDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CStopperSettingDlg, CDialog)

CStopperSettingDlg::CStopperSettingDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CStopperSettingDlg::IDD, pParent)	
{
	m_strLabelName1= "";
	m_strLabelName2= "";
	m_strLabelName3= "";
	m_strLabelName4= "";
}

CStopperSettingDlg::~CStopperSettingDlg()
{
}

void CStopperSettingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LABEL1, m_Label1);
	DDX_Control(pDX, IDC_LABEL2, m_Label2);
	DDX_Control(pDX, IDC_LABEL3, m_Label3);
	DDX_Control(pDX, IDC_LABEL4, m_Label4);
	DDX_Text(pDX, IDC_EDIT_STOPPER1, m_Stopper1);
	DDX_Text(pDX, IDC_EDIT_STOPPER2, m_Stopper2);
	DDX_Text(pDX, IDC_EDIT_STOPPER3, m_Stopper3);
	DDX_Text(pDX, IDC_EDIT_STOPPER4, m_Stopper4);
}


BEGIN_MESSAGE_MAP(CStopperSettingDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CStopperSettingDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CStopperSettingDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


// CStopperSettingDlg 메시지 처리기입니다.

void CStopperSettingDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(true);
	OnOK();
}

void CStopperSettingDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}

BOOL CStopperSettingDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	InitLabel();
	InitFont();
	
	UpdateData(false);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CStopperSettingDlg::InitLabel()
{
	m_Label1.SetFontSize(20)
		.SetTextColor(RGB_LABEL_FONT)
		.SetBkColor(RGB_LABEL_BACKGROUND)			
		.SetFontBold(TRUE)		
		.SetFontName("맑은고딕")
		.SetText(m_strLabelName1);
		
	m_Label2.SetFontSize(20)
		.SetTextColor(RGB_LABEL_FONT)
		.SetBkColor(RGB_LABEL_BACKGROUND)			
		.SetFontBold(TRUE)		
		.SetFontName("맑은고딕")
		.SetText(m_strLabelName2);
		
	m_Label3.SetFontSize(20)
		.SetTextColor(RGB_LABEL_FONT)
		.SetBkColor(RGB_LABEL_BACKGROUND)			
		.SetFontBold(TRUE)		
		.SetFontName("맑은고딕")
		.SetText(m_strLabelName3);
		
	m_Label4.SetFontSize(20)
		.SetTextColor(RGB_LABEL_FONT)
		.SetBkColor(RGB_LABEL_BACKGROUND)			
		.SetFontBold(TRUE)		
		.SetFontName("맑은고딕")
		.SetText(m_strLabelName4);
}

void CStopperSettingDlg::InitFont()
{
	LOGFONT LogFont;

	GetDlgItem(IDC_EDIT_STOPPER1)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight = 1000;
	LogFont.lfHeight = EDIT_FONT_SIZE;

	m_Font.CreateFontIndirect( &LogFont );

	GetDlgItem(IDC_EDIT_STOPPER1)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_STOPPER2)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_STOPPER3)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_STOPPER4)->SetFont(&m_Font);	
}
