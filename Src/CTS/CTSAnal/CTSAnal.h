// CTSAnal.h : main header file for the CTSAnal application
//

#if !defined(AFX_CTSAnal_H__001D8B13_334E_4802_92D7_6AE5120C94B6__INCLUDED_)
#define AFX_CTSAnal_H__001D8B13_334E_4802_92D7_6AE5120C94B6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols
#include "MsgDefine.h"

/////////////////////////////////////////////////////////////////////////////
// CCTSAnalApp:
// See CTSAnal.cpp for the implementation of this class
//

#ifndef RLT_REG_SECTION
#define RLT_REG_SECTION		"Rlt View"
#endif

class CCTSAnalApp : public CWinApp
{
public:
	CCTSAnalApp();

	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCTSAnalApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CCTSAnalApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	// 
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CTSAnal_H__001D8B13_334E_4802_92D7_6AE5120C94B6__INCLUDED_)
