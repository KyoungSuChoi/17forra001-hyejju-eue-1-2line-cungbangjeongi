// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "CTSGraphAnal.h"

#include "MainFrm.h"
#include "Splash.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_DROPFILES()
	ON_WM_CLOSE()
	ON_WM_COPYDATA()
	//}}AFX_MSG_MAP		
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
	
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

//	CLogDialog aDlg;
//	if(aDlg.DoModal()!=IDOK)	return -1;
	
	if (!m_wndMainToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndMainToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar5\n");
		return -1;      // fail to create
	}	

	if (!m_wndDataToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndDataToolBar.LoadToolBar(IDR_DATA))
	{
		TRACE0("Failed to create toolbar1\n");
		return -1;      // fail to create
	}
	if (!m_wndGraphToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndGraphToolBar.LoadToolBar(IDR_GRAPH))
	{
		TRACE0("Failed to create toolbar2\n");
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
	EnableDocking(CBRS_ALIGN_ANY);

	LoadToolBar(0);

	SetWindowText("CTSGraphAnalyzer");
	
	HICON hIcon;
	hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	SetIcon(hIcon, NULL);	

	// CG: The following line was added by the Splash Screen component.
//	CSplashWnd::ShowSplashScreen(this);

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs
/*	cs.style &= ~FWS_ADDTOTITLE;
	cs.lpszName="PowerBTS-Control System";
	cs.x=0;
	cs.y=0;
	cs.cx=1024;
	cs.cy=768;*/
//	return TRUE;

	cs.style &= ~FWS_ADDTOTITLE;
	cs.lpszName="CTSGraphAnal";
	
//	int x=GetSystemMetrics(SM_CXMAXTRACK);
//	int y=GetSystemMetrics(SM_CYMAXTRACK);

//	cs.x=(int)(x-800)/2;
//	cs.y=(int)(y-600)/2;
//	cs.cx=800;
//	cs.cy=600;

	WNDCLASS wc;
	::GetClassInfo( AfxGetInstanceHandle(), cs.lpszClass, &wc );
    wc.lpszClassName = PS_DATA_ANAL_PRO_CLASS_NAME; 
	cs.lpszClass = PS_DATA_ANAL_PRO_CLASS_NAME;    
	wc.hIcon = ::LoadIcon(AfxGetInstanceHandle(), "IDR_MAINFRAME");
	::RegisterClass( &wc );

	return TRUE; 
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CMDIFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CMDIFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers
void CMainFrame::LoadToolBar(int no)
{
	ShowControlBar(&m_wndMainToolBar, FALSE, TRUE);	
	ShowControlBar(&m_wndDataToolBar, FALSE, TRUE);
	ShowControlBar(&m_wndGraphToolBar, FALSE, TRUE);

	switch(no)
	{
	case MAIN_TOOLBAR:
		ShowControlBar(&m_wndMainToolBar, TRUE, FALSE);
		break;	
	case DATA_TOOLBAR:
		ShowControlBar(&m_wndDataToolBar, TRUE, FALSE);
		break;	
	case GRAPH_TOOLBAR:
		ShowControlBar(&m_wndGraphToolBar, TRUE, FALSE);
		break;	
	}
}

void CMainFrame::OnDropFiles(HDROP hDropInfo) 
{
	CString Path;
	UINT nFiles = ::DragQueryFile(hDropInfo, (UINT)-1, NULL, 0);
	
	CString strChPathName, strTestName;
	m_strCopyMsgString.Empty();
	for (UINT iFile = 0; iFile < nFiles; iFile++)
	{
		TCHAR szFileName[_MAX_PATH];
		ZeroMemory(szFileName, _MAX_PATH);
		::DragQueryFile(hDropInfo, iFile, szFileName, _MAX_PATH);
		
		CString strArgu(szFileName);
		
		/*CFileFind afinder;
		afinder.FindFile(strPath);
		afinder.FindNextFile();
		if(!afinder.IsDots()&&afinder.IsDirectory()) Path+=strPath+"\n";
		*/
		
		if(!strArgu.IsEmpty())
		{
			strArgu.TrimLeft("\"");
			strArgu.TrimRight("\"");
			CString strExt;
			int p1 = strArgu.ReverseFind('.');
			if(p1 > 0)								//파일 확장자 검색 
			{
				strExt =  strArgu.Mid(p1+1);
				strExt.MakeLower();
				if(strExt == PS_RAW_FILE_NAME_EXT || strExt == "rpt" || strExt == PS_RESULT_FILE_NAME_EXT)
				{
					p1 = strArgu.ReverseFind('\\');
					if(p1 >= 0)									//채널 폴더명 분리  
					{
						strChPathName = strArgu.Left(p1);
						//AfxMessageBox(strTemp);
						
						p1 = strChPathName.ReverseFind('\\');	 
						if(p1 > 0)
						{
							CString strTemp;
							strTemp = strChPathName.Left(p1);		//test명 폴더 분리
							//AfxMessageBox(strChPathName);
						
							p1 = strTemp.ReverseFind('\\');
							if(p1 > 0)
							{
								strTestName = strTemp.Mid(p1+1);		//test명 분리 
								//AfxMessageBox(strTestName);
								m_strCopyMsgString += (strChPathName+"\n");
							}
						}
					}
				}
			}
		}
	}
	::DragFinish(hDropInfo);
	
	if(m_strCopyMsgString.GetLength() > 0)
	{
		((CCTSGraphAnalApp *)AfxGetApp())->m_pDataTemplate->OpenDocumentFile(strTestName);
	}
}

void CMainFrame::OnClose() 
{
	while(TRUE)
	{
		CFrameWnd* pFrm = GetActiveFrame();
		if(pFrm==this) break;
		pFrm->SendMessage(WM_CLOSE);
	}

	//
	CMDIFrameWnd::OnClose();
}

BOOL CMainFrame::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct) 
{
	// TODO: Add your message handler code here and/or call default

	//CTSMonPro에서 전달된 Message
	if(pCopyDataStruct->dwData == 3)
	{
		if(pCopyDataStruct->cbData > 0)	//"\n"
		{
			char *pData = new char[pCopyDataStruct->cbData];
			memcpy(pData, pCopyDataStruct->lpData, pCopyDataStruct->cbData);
//			CString strData(pData);
			m_strCopyMsgString = pData;
			delete [] pData;	
			if(!m_strCopyMsgString.IsEmpty())
			{
				CString strTestName("Data1");
				int p1 = m_strCopyMsgString.Find('\n');
				if(p1 >= 0)	
				{
					CString strTemp;
					strTemp = m_strCopyMsgString.Left(p1);
					p1 = strTemp.ReverseFind('\\');
					if(p1 >= 0)
					{
						CString strTemp1;
						strTemp1 = strTemp.Left(p1);
						p1 = strTemp1.ReverseFind('\\');
						if(p1 >=0)
						{
							strTestName = strTemp1.Mid(p1+1);
						}
					}
				}
				//document인자는 overflow 발생하므로 인자로 안넘기고 변수로 처리 
				//((CCTSGraphAnalApp *)AfxGetApp())->m_pDataTemplate->OpenDocumentFile("aa");
				((CCTSGraphAnalApp *)AfxGetApp())->m_pDataTemplate->OpenDocumentFile(strTestName);
			}			
		}
	}
	else if(pCopyDataStruct->dwData == 4)	//Graph View
	{
		if(pCopyDataStruct->cbData > 0)	//"\n"
		{
			char *pData = new char[pCopyDataStruct->cbData];
			memcpy(pData, pCopyDataStruct->lpData, pCopyDataStruct->cbData);
//			CString strData(pData);		
			m_strCopyMsgString = pData;
			delete [] pData;
			if(!m_strCopyMsgString.IsEmpty())
			{
				CString strTestName("Graph1");
				int p1 = m_strCopyMsgString.Find('\n');
				if(p1 >= 0)	
				{
					CString strTemp;
					strTemp = m_strCopyMsgString.Left(p1);
					p1 = strTemp.ReverseFind('\\');
					if(p1 >= 0)
					{
						CString strTemp1;
						strTemp1 = strTemp.Left(p1);
						p1 = strTemp1.ReverseFind('\\');
						if(p1 >=0)
						{
							strTestName = strTemp1.Mid(p1+1);
						}
					}
				}
				//document인자는 overflow 발생하므로 인자로 안넘기고 변수로 처리 
				//((CCTSGraphAnalApp *)AfxGetApp())->m_pGraphTemplate->OpenDocumentFile("aa");
				((CCTSGraphAnalApp *)AfxGetApp())->m_pGraphTemplate->OpenDocumentFile(strTestName);
			}
		}
	}
	//  [6/20/2009 kky ]
	// for CTSMon 프로그램의 DataSearch부분에서 검색된 데이터를 더블클릭시 해당하는 데이터의 폴더 경로를
	// 메세지로 받아서 상세데이터가 있는 폴더경로를 만들어 낸 후 CTSGraphAnal 그래프 모드에서의 데이터
	// 선택 준비단계까지 진행시킨다.
	else if(pCopyDataStruct->dwData == 5)	//Graph View
	{
		if(pCopyDataStruct->cbData > 0)
		{			
			char *pData = new char[pCopyDataStruct->cbData];
			memcpy(pData, pCopyDataStruct->lpData, pCopyDataStruct->cbData);			
			m_strCopyMsgString = pData;	
			delete[] pData;
			
			if(!m_strCopyMsgString.IsEmpty())
			{	
				CString strData;
				int nIndex;
				CFileFind FileFinder;
				
				BOOL bFind = FileFinder.FindFile(m_strCopyMsgString+"\\*");
				while(bFind)
				{
					bFind = FileFinder.FindNextFile();
					if(FileFinder.IsDots())
					{						
						continue;
					}
					else
					{
						CString strFileName(FileFinder.GetFilePath());						
						strFileName += "\n";
						strData += strFileName;
						TRACE( strFileName );
					}
				}
				
				nIndex = strData.GetLength()+1;
				char *pData = new char[nIndex];
				sprintf(pData, "%s", strData);
				pData[nIndex-1] = '\0';
				COPYDATASTRUCT CpStructData;
				CpStructData.dwData = 4;
				CpStructData.cbData = nIndex;
				CpStructData.lpData = pData;
				::SendMessage(AfxGetMainWnd()->m_hWnd, WM_COPYDATA, 0, (LPARAM)&CpStructData);
				delete [] pData;				
			}
		}
	}
	return CMDIFrameWnd::OnCopyData(pWnd, pCopyDataStruct);
}
