// TypeCopyDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSEditor.h"
#include "TypeCopyDlg.h"


// CTypeCopyDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CTypeCopyDlg, CDialog)

CTypeCopyDlg::CTypeCopyDlg(long lSelectType, CWnd* pParent /*=NULL*/)
	: CDialog(CTypeCopyDlg::IDD, pParent)
	, m_strTypeToCopy(_T(""))
{
	m_lSelectType = lSelectType;

	LanguageinitMonConfig();
}

CTypeCopyDlg::~CTypeCopyDlg()	
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

bool CTypeCopyDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CTypeCopyDlg"), _T("TEXT_CTypeCopyDlg_CNT"), _T("TEXT_CTypeCopyDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CTypeCopyDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CTypeCopyDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}


void CTypeCopyDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LABEL1, m_Label1);
	DDX_Control(pDX, IDC_LABEL2, m_Label2);
	DDX_Control(pDX, IDC_LABEL_SELECT_TYPE, m_LabelSelectType);
	DDX_Text(pDX, IDC_TYPE_TO_COPY, m_strTypeToCopy);
}


BEGIN_MESSAGE_MAP(CTypeCopyDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CTypeCopyDlg::OnBnClickedOk)	
	ON_BN_CLICKED(IDCANCEL, &CTypeCopyDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


// CTypeCopyDlg 메시지 처리기입니다.

void CTypeCopyDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(true);
	
	if( m_strTypeToCopy.IsEmpty()  || m_strTypeToCopy.GetLength() > 2 )
	{
		AfxMessageBox(TEXT_LANG[0], MB_ICONERROR);//"잘못된 Type 정보 입니다."
		return;
	}
		
	OnOK();
}

void CTypeCopyDlg::InitLabel()
{
	m_Label1.SetBkColor(RGB_MIDNIGHTBLUE)
		.SetTextColor(RGB_WHITE)
		.SetFontSize(22)
		.SetFontBold(TRUE)
		.SetText(TEXT_LANG[1]);//"선택한 Type"

	m_Label2.SetBkColor(RGB_MIDNIGHTBLUE)
		.SetTextColor(RGB_WHITE)
		.SetFontSize(22)
		.SetFontBold(TRUE)
		.SetText(TEXT_LANG[2]);	//"복사할 Type"
		
	m_LabelSelectType.SetBkColor(RGB_MIDNIGHTBLUE)
		.SetTextColor(RGB_WHITE)
		.SetFontSize(22)
		.SetFontBold(TRUE);
}

void CTypeCopyDlg::InitFont()
{	
	LOGFONT	LogFont;

	GetDlgItem(IDC_TYPE_TO_COPY)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight = 1000;
	LogFont.lfHeight = EDIT_FONT_SIZE;

	font.CreateFontIndirect( &LogFont );

	GetDlgItem(IDC_TYPE_TO_COPY)->SetFont(&font);

}
BOOL CTypeCopyDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	InitLabel();
	InitFont();
	
	CString strTemp;
	strTemp.Format("%ld", m_lSelectType);
	m_LabelSelectType.SetText(strTemp);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CTypeCopyDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}