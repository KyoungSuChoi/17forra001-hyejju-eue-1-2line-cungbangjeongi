// ModuleAddDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSMon.h"
#include "ModuleAddDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CModuleAddDlg dialog

extern CString GetDefaultModuleName(int nModuleID, int nGroupIndex);

CModuleAddDlg::CModuleAddDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CModuleAddDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CModuleAddDlg)
	m_nModuleID = 1;
	m_fVSpec = 0.0f;
	m_fISpec = 0.0f;
	m_nTrayCol = 0;
	m_bUseModeChange = FALSE;
	//}}AFX_DATA_INIT
	m_bAddDlg = TRUE;
	m_nModuleTrayType=0;
	LanguageinitMonConfig();
}


CModuleAddDlg::~CModuleAddDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CModuleAddDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CModuleAddDlg"), _T("TEXT_CModuleAddDlg_CNT"), _T("TEXT_CModuleAddDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CModuleAddDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CModuleAddDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}


void CModuleAddDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CModuleAddDlg)
	DDX_Control(pDX, IDC_CBO_TRAY_TYPE, m_ctrlTrayType);
	DDX_Control(pDX, IDC_COMBO1, m_ctrlModuleType);
	DDX_Control(pDX, IDC_MODULE_SEL_COMBO, m_ctrlModuleSel);
	DDX_Text(pDX, IDC_MODULENUM, m_nModuleID);
	DDX_Text(pDX, IDC_EDIT1, m_fVSpec);
	DDX_Text(pDX, IDC_EDIT3, m_fISpec);
	DDX_Text(pDX, IDC_EDIT_COL_CNT, m_nTrayCol);
	DDV_MinMaxUInt(pDX, m_nTrayCol, 1, 257);
	DDX_Check(pDX, IDC_CHK_CHANGE_MODE, m_bUseModeChange);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CModuleAddDlg, CDialog)
	//{{AFX_MSG_MAP(CModuleAddDlg)
	ON_EN_CHANGE(IDC_MODULENUM, OnChangeModulenum)
	ON_CBN_SELCHANGE(IDC_MODULE_SEL_COMBO, OnSelchangeModuleSelCombo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CModuleAddDlg message handlers

void CModuleAddDlg::OnChangeModulenum() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(m_nModuleID <1 || m_nModuleID > 0x7FFF)
	{
		AfxMessageBox(GetStringTable(IDS_TEXT_VALUE_VALIDE));
	}
	GetDlgItem(IDC_NAME_EDIT)->SetWindowText(::GetDefaultModuleName(m_nModuleID, 0));
}

void CModuleAddDlg::OnOK() 
{
	// TODO: Add extra validation here
	int index = m_ctrlModuleSel.GetCurSel();
	if(index >= 0)
		m_nModuleID = m_ctrlModuleSel.GetItemData(index);

	index = m_ctrlModuleType.GetCurSel();
	m_nType = m_ctrlModuleType.GetItemData(index);
	
	//Tray Type 설정 (name,style,col num)
	m_nModuleTrayType = m_ctrlTrayType.GetCurSel();
	if(index >= 0)
		m_nModuleTrayType++;
	
	GetDlgItem(IDC_NAME_EDIT)->GetWindowText(m_strName);
	if(m_strName.IsEmpty())
	{
		AfxMessageBox(TEXT_LANG[0]);//"이름을 입력하십시요"
		GetDlgItem(IDC_NAME_STATIC)->SetFocus();
		return;
	}

	if(m_nModuleID <1 || m_nModuleID > 0x7FFF)
	{
		AfxMessageBox(GetStringTable(IDS_TEXT_VALUE_VALIDE));
		return;
	}
	CDialog::OnOK();
}

BOOL CModuleAddDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	//Registry에서 CTSMon DataBase 경로를 검색 
	long rtn;
	HKEY hKey = 0;
	BYTE buf[512], buf2[512];
	DWORD size = 511;
	DWORD type;
	
	CString strDBPath;

	rtn = ::RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\CTSMon\\Path", 0, KEY_READ, &hKey);
	if(ERROR_SUCCESS == rtn)
	{	
		//CTSMon DataBase 경로를 읽어온다. 
		rtn = ::RegQueryValueEx(hKey, "DataBase", NULL, &type, buf, &size);
		::RegCloseKey(hKey);
		
		//
		if(ERROR_SUCCESS == rtn && (type == REG_SZ || type == REG_EXPAND_SZ))
		{
			strDBPath.Format("%s\\%s", buf, FORM_SET_DATABASE_NAME);			
		}
	}

	rtn = ::RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\CTSMon\\FormSetting", 0, KEY_READ, &hKey);
	if(ERROR_SUCCESS == rtn)
	{
		rtn = ::RegQueryValueEx(hKey, "Module Name", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && (type == REG_SZ || type == REG_EXPAND_SZ))
		{
			m_strModuleName = buf;
		}
		size = 255;
		rtn = ::RegQueryValueEx(hKey, "Group Name", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && (type == REG_SZ || type == REG_EXPAND_SZ))
		{
			m_strGroupName = buf;
		}
		size = 255;
		rtn = ::RegQueryValueEx(hKey, "Module Per Rack", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && type == REG_DWORD)
		{
			sprintf((char *)buf2, "%ld", *(long *)buf);
			m_nModulePerRack = atol((LPCTSTR)buf2);
		}
		size = 255;
		rtn = ::RegQueryValueEx(hKey, "Use Rack Index", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && type == REG_DWORD)
		{
			sprintf((char *)buf2, "%ld", *(long *)buf);
			m_bUseRackIndex = atol((LPCTSTR)buf2);
		}
		size = 255;
		rtn = ::RegQueryValueEx(hKey, "Use Group", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && type == REG_DWORD)
		{
			sprintf((char *)buf2, "%ld", *(long *)buf);
			m_bUseGroupSet = atol((LPCTSTR)buf2);
		}
		::RegCloseKey(hKey);
	}
		
	if(m_bAddDlg)
	{
		GetDlgItem(IDC_MODULE_SEL_COMBO)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_MODULENUM)->SetFocus();
	}
	else
	{
		GetDlgItem(IDC_MODULENUM)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_EDIT1)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT3)->EnableWindow(FALSE);

		int nCount = 0;
		CString strTemp;
		int nSelIndex = 0;
		CDaoDatabase  db;
		
		try
		{
			db.Open(strDBPath);

			CString strSQL("SELECT ModuleID FROM SystemConfig ORDER BY ModuleID");
			CDaoRecordset rs(&db);
				rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
				while(!rs.IsEOF())
				{
					COleVariant data = rs.GetFieldValue(0);
					strTemp.Format("%s", ::GetDefaultModuleName(data.lVal, 0));
					m_ctrlModuleSel.AddString(strTemp);
					
					if(data.lVal == m_nModuleID)
						nSelIndex = nCount;
					
					m_ctrlModuleSel.SetItemData(nCount++, data.lVal);
					rs.MoveNext();
				}
			rs.Close();
			db.Close();	
		}
		catch (CDaoException *e)
		{
		//	e->GetErrorMessage();
			e->Delete();
		}

		
		m_ctrlModuleSel.SetCurSel(nSelIndex);			//기본 Module 선택
		m_nModuleID = m_ctrlModuleSel.GetItemData(nSelIndex);	//현재 선택된 module ID

		EP_SYSTEM_PARAM *pParam = EPGetSysParam(m_nModuleID);
		if(pParam)
		{
			m_fVSpec =  VTG_PRECISION(pParam->lMaxVoltage);
			m_fISpec =  CRT_PRECISION(pParam->lMaxCurrent);
		}
	}
		
	GetDlgItem(IDC_NAME_EDIT)->SetWindowText(::GetDefaultModuleName(m_nModuleID, 0));

//	m_ctrlModuleType.AddString("범용");
//	m_ctrlModuleType.SetItemData(0, EP_ID_ALL_SYSTEM);
	m_ctrlModuleType.AddString("Formation");
	m_ctrlModuleType.SetItemData(0, EP_ID_FORM_OP_SYSTEM);
	m_ctrlModuleType.AddString("IR/OCV");
	m_ctrlModuleType.SetItemData(1, EP_ID_IROCV_SYSTEM);
	m_ctrlModuleType.AddString("Grading");
	m_ctrlModuleType.SetItemData(2, EP_ID_GRADING_SYSTEM);
	m_ctrlModuleType.AddString("Aging");
	m_ctrlModuleType.SetItemData(3, EP_ID_AGIGN_SYSTEM);

	m_ctrlModuleType.SetCurSel(0);

	//Tray Stype 추가 ljb 2008-12
	m_ctrlTrayType.AddString(TEXT_LANG[1]);//"기본형1"
	m_ctrlTrayType.AddString(TEXT_LANG[2]);//"기본형2"
	m_ctrlTrayType.AddString(TEXT_LANG[3]);//"폴리머형1"
	m_ctrlTrayType.AddString(TEXT_LANG[4]);//"폴리머형2"
	m_ctrlTrayType.SetCurSel(0);

	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

int CModuleAddDlg::GetModuleID()
{
	return m_nModuleID;
}
/*
CString CModuleAddDlg::ModuleName(int nModuleID, int nGroupIndex)
{
	CString strName, strMDName;
	strMDName = m_strModuleName;

	EP_SYSTEM_PARAM *pParam = EPGetSysParam(nModuleID);
	if(pParam)
	{
		if(pParam->wModuleType == EP_ID_IROCV_SYSTEM)
		{
			strMDName = "IROCV";
		}
	}
	
	if(m_bUseRackIndex)
	{
		div_t div_result;
		div_result = div( nModuleID-1, m_nModulePerRack );

		if(m_bUseGroupSet && nGroupIndex >= 0)
		{
			strName.Format("%s %d-%d, %s %d", m_strModuleName, div_result.quot+1, div_result.rem+1, m_strGroupName, nGroupIndex+1);
		}
		else
		{
			strName.Format("%s %d-%d", m_strModuleName, div_result.quot+1, div_result.rem+1);
		}
	}
	else
	{
		if(m_bUseGroupSet && nGroupIndex >= 0)
		{
			strName.Format("%s %d, %s %d", m_strModuleName, nModuleID,  m_strGroupName, nGroupIndex+1);
		}
		else
		{
			strName.Format("%s %d", m_strModuleName, nModuleID);
		}
	}
	return strName;
}
*/
void CModuleAddDlg::OnSelchangeModuleSelCombo() 
{
	// TODO: Add your control notification handler code here
	int index =m_ctrlModuleSel.GetCurSel();
	if(index >= 0)
		m_nModuleID = m_ctrlModuleSel.GetItemData(index);	
	UpdateData(FALSE);
	GetDlgItem(IDC_NAME_EDIT)->SetWindowText(::GetDefaultModuleName(m_nModuleID, 0));
}
