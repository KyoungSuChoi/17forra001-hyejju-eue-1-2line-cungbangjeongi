#pragma once
class CFMSServer : public CFMSRawServer<CFMSObj>
{
public:
	CFMSServer();
	virtual ~CFMSServer();

private:
	INT						mConnectCnt;
	INT						mHeartBeatCheck;	
	BOOL					m_bRunChk;
	//std::list<CFMSObj*>	m_lstFMSObj;

	HWND					m_NotifyHwnd;
	HANDLE					mKeepThreadHandle;
	HANDLE					mKeepThreadDestroyEvent;
	
	HANDLE					mObjDelete;
	
	st_FMS_PACKET			m_Pack;
	
public:
	BOOL	NetBegin(HWND);
	BOOL	NetEnd(VOID);

	VOID	KeepThreadCallback(VOID);

	INT		strLinker(CHAR *msg, VOID * _dest, INT* mesmap);
	INT		strCutter(CHAR *msg, VOID * _dest, INT* mesmap);

	VOID	fnMakeHead(st_FMSMSGHEAD& _head, CHAR* _cmd, UINT _len, CHAR _result);
	VOID	fnSendResult(const CHAR* _Longdata);
	VOID	fnKillNetwork();
protected:

	VOID OnConnected(CFMSObj *poNetObj);
	VOID OnDisconnected(CFMSObj *poNetObj);
	VOID OnRead(CFMSObj *poNetObj, st_FMSMSGHEAD&_hdr,  BYTE *pReadBuf, DWORD dwLen);
	VOID OnWrite(CFMSObj *poNetObj, DWORD dwLen);
};