#if !defined(AFX_CPKSETDLG_H__613C4BD9_AF58_49F8_A148_25DCB491990F__INCLUDED_)
#define AFX_CPKSETDLG_H__613C4BD9_AF58_49F8_A148_25DCB491990F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CpkSetDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCpkSetDlg dialog


class CCpkSetDlg : public CDialog
{
// Construction
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	CCpkSetDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CCpkSetDlg();

	BOOL     m_bHighRange;
	BOOL     m_bLowRange;
	int      m_nItem;
	BOOL     m_bRange;
	FIELD     m_field[FIELDNO];
	int		m_nCount;
	CString m_strTitle;

// Dialog Data
	//{{AFX_DATA(CCpkSetDlg)
	enum { IDD = IDD_CPKSET_DLG };
	CEdit	m_pGraphGap;
	CEdit	m_pHighRange;
	CString	m_strHighRange;
	CString	m_strLowRange;
	CString	m_strGraphGap;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCpkSetDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CCpkSetDlg)
	afx_msg void OnSelchangeCombo1();
	virtual BOOL OnInitDialog();
	afx_msg void OnChangeEditHighrange();
	afx_msg void OnChangeEditLowrange();
	virtual void OnOK();
	afx_msg void OnNorange();
	afx_msg void OnRange();
	afx_msg void OnChangeEditGraphgap();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CPKSETDLG_H__613C4BD9_AF58_49F8_A148_25DCB491990F__INCLUDED_)
