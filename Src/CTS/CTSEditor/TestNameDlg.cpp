// TestNameDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSEditor.h"
#include "TestNameDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTestNameDlg dialog
extern CString g_strDataBaseName;

CTestNameDlg::CTestNameDlg(int nProcess, CWnd* pParent /*=NULL*/)
	: CDialog(CTestNameDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTestNameDlg)
	m_strName = _T("");
	m_strDecription = _T("");
	m_strCreator = _T("");
	//}}AFX_DATA_INIT	
	m_lTestTypeID = 0;
	m_lModelID = 0;
	m_nProcess = nProcess;
	m_bProcessSetting = false;
	LanguageinitMonConfig();
}

CTestNameDlg::~CTestNameDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CTestNameDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CTestNameDlg"), _T("TEXT_CTestNameDlg_CNT"), _T("TEXT_CTestNameDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CTestNameDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CTestNameDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}



void CTestNameDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTestNameDlg)
	DDX_Control(pDX, IDC_PROC_TYPE_COMBO, m_ctrlProcTypeCombo);		
	DDX_Text(pDX, IDC_TEST_DESCRIP_EDIT, m_strDecription);
	DDX_Text(pDX, IDC_TEST_CREATOR_EDIT, m_strCreator);	
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_LABEL_PROCESS, m_LabelProcess);
}


BEGIN_MESSAGE_MAP(CTestNameDlg, CDialog)
	//{{AFX_MSG_MAP(CTestNameDlg)	
	ON_EN_CHANGE(IDC_TEST_DESCRIP_EDIT, OnChangeTestDescripEdit)
	ON_EN_CHANGE(IDC_TEST_CREATOR_EDIT, OnChangeTestCreatorEdit)
	ON_CBN_SELCHANGE(IDC_PROC_TYPE_COMBO, OnSelchangeProcTypeCombo)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, &CTestNameDlg::OnBnClickedOk)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestNameDlg message handlers

void CTestNameDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData();
		
	if(m_strName.IsEmpty())
	{
		AfxMessageBox(TEXT_LANG[0]);		//"공정목록이 입력되지 않았습니다."
		return;
	}
	
	CDialog::OnOK();
}


BOOL CTestNameDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here		
	//////////////////////////////////////////////////////////////////////////
	//공정 선택 Combo 초기화
	//////////////////////////////////////////////////////////////////////////	
	InitLabel();
	
	CString strTemp, strSQL;
	strTemp.Format("%d", m_nProcess);
	m_LabelProcess.SetText(strTemp);	

	strSQL = "SELECT TestType, TestTypeName FROM TestType ORDER BY TestType";
	int nCount = 0;
	int nDefaultIndex = 0;
	COleVariant data;
	
	CDaoDatabase  db;
	db.Open(g_strDataBaseName);
	CDaoRecordset rs(&db);
	
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
		while(!rs.IsEOF())
		{
			data = rs.GetFieldValue(1);
			m_ctrlProcTypeCombo.AddString((LPCTSTR)data.pbVal);

			data = rs.GetFieldValue(0);
			if(m_lTestTypeID == data.lVal) 
			{
				nDefaultIndex = nCount;
			}

			m_ctrlProcTypeCombo.SetItemData(nCount++, data.lVal);
			rs.MoveNext();
		}
		rs.Close();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();	
	}
	db.Close();

	if(m_lTestTypeID == 0 && nCount < 3)	nDefaultIndex = 0;

	m_ctrlProcTypeCombo.SetCurSel(nDefaultIndex);
	m_lTestTypeID = m_ctrlProcTypeCombo.GetItemData(nDefaultIndex);
	int nIndex = m_ctrlProcTypeCombo.GetCurSel();
	if(nIndex != LB_ERR)
	{	
		m_ctrlProcTypeCombo.GetLBText(nIndex, m_strName);
	}

	GetDlgItem(IDC_PROC_TYPE_COMBO)->ShowWindow(SW_SHOW);
	
	UpdateData(FALSE);
	
	GetDlgItem(IDC_DATE_TIME_STATIC)->SetWindowText(m_createdDate.Format());
	
	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CTestNameDlg::OnChangeTestDescripEdit() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	GetDlgItem(IDOK)->EnableWindow();
	
}

void CTestNameDlg::OnChangeTestCreatorEdit() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	GetDlgItem(IDOK)->EnableWindow();	
}

void CTestNameDlg::OnSelchangeProcTypeCombo() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	int nIndex = m_ctrlProcTypeCombo.GetCurSel();
	if(nIndex != LB_ERR)
	{	
		m_lTestTypeID = m_ctrlProcTypeCombo.GetItemData(nIndex);
		m_ctrlProcTypeCombo.GetLBText(nIndex, m_strName);
		UpdateData(FALSE);
	}
}

void CTestNameDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( m_strName != NODEVICE)
	{
		m_bProcessSetting = true;
	}
	
	OnOK();
}

void CTestNameDlg::InitLabel()
{
	m_LabelProcess.SetBkColor(RGB_MIDNIGHTBLUE)
		.SetTextColor(RGB_WHITE)
		.SetFontSize(24)
		.SetBorder(TRUE)
		.SetText("-");
}