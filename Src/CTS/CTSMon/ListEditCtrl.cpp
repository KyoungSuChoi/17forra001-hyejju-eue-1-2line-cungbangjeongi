// ListEditCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "ListEditCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CListEditCtrl

CListEditCtrl::CListEditCtrl()
{
	m_bDigitMode = FALSE;
	m_pEdit = NULL;
	m_fMaxBoundData = 10000000.0f;
//	m_bCheckCali = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Check Cali", 0);
}

CListEditCtrl::~CListEditCtrl()
{
	if( m_pEdit )
		delete m_pEdit;
}


BEGIN_MESSAGE_MAP(CListEditCtrl, CListCtrl)
	//{{AFX_MSG_MAP(CListEditCtrl)
	ON_NOTIFY_REFLECT(NM_DBLCLK, OnDblclk)
	ON_NOTIFY_REFLECT(LVN_ITEMCHANGED, OnItemchanged)
	ON_WM_KEYUP()
	ON_WM_KILLFOCUS()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CListEditCtrl message handlers

void CListEditCtrl::OnDblclk(NMHDR* pNMHDR, LRESULT* pResult) 
{
	MakeEditCtrl();

	*pResult = 0;
}

bool CListEditCtrl::IsDigit(CString strSource)
{
	for( int nI = 0; nI < strSource.GetLength(); nI++ )
	{
		if( strSource.GetAt(nI) == '-' && nI == 0 )
			continue;
		
		if( strSource.GetAt(nI) == '.')
			continue;

		if( !isdigit(strSource.GetAt(nI)) )
			return false;
	}
	return true;
}

BOOL CListEditCtrl::PreTranslateMessage(MSG* pMsg) 
{
	if( pMsg->message == WM_KEYDOWN )
	{
		switch( pMsg->wParam )
		{
		case VK_RETURN:
			{
				UpdateText();
				m_pEdit->ShowWindow(SW_HIDE);
				return FALSE;
			}
		case VK_ESCAPE:
			if( m_pEdit )
			{
				CString strData;
				m_pEdit->GetWindowText(strData);
				m_pEdit->ShowWindow(SW_HIDE);

				if( strData.IsEmpty() )
				{
					DeleteItem(m_nCurSel);
				}
			}
			return FALSE;
		}
	}
	return CListCtrl::PreTranslateMessage(pMsg);
}

void CListEditCtrl::OnItemchanged(NMHDR* pNMHDR, LRESULT* pResult) 
{
//	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	UpdateText();
	*pResult = 0;
}

void CListEditCtrl::UpdateText()
{
	if(m_pEdit == NULL )
		return;
	if(!m_pEdit->IsWindowVisible())	return;
//	CString strTmp;
	CString strData;
	m_pEdit->GetWindowText(strData);
	m_pEdit->ShowWindow(SW_HIDE);

	if( strData.IsEmpty() )
	{
//		m_pEdit->ShowWindow(SW_HIDE);
		DeleteItem(m_nCurSel);
		return ;
	}
	else
	{
		if(m_bDigitMode)
		{
			if( IsDigit(strData) == false )
			{
				DeleteItem(m_nCurSel);
				return ;
			}
			double dblInData = atof((LPSTR)(LPCTSTR)strData);
			if( dblInData < (-1)*m_fMaxBoundData || dblInData > m_fMaxBoundData )
			{
				DeleteItem(m_nCurSel);
				return ;
			}

			strData.Format("%.3f", dblInData);
		}
		SetItemText(m_nCurSel, 1, strData);

		if(m_bCheckCali == TRUE)
		{
			CString strTemp = GetItemText(m_nCurSel+1, 1);
			if(strTemp.IsEmpty())
			{
				double dblInData = atof((LPSTR)(LPCTSTR)strData);
				if(m_nCurSel + 1 == GetItemCount())
					MakeEditCtrl();    
				strTemp.Format("%.3f", -dblInData);
				SetItemText(m_nCurSel, 1, strTemp);                       
			}	
		}
	}
	
//	strTmp.Format("%d", m_nCurSel+1);
//	LV_ITEM lvItem;
//	ZeroMemory(&lvItem, sizeof(lvItem));
//	lvItem.mask = LVIF_TEXT;
//	lvItem.iItem = m_nCurSel;
//	lvItem.iSubItem = 0;
//	lvItem.pszText = (LPSTR)(LPCTSTR)strTmp;
//
//	if( GetItemCount() == m_nCurSel )
//	{
//		InsertItem(&lvItem);
//		SetItemText(m_nCurSel, 1, strData);
//	}
//	else
//	{
		//SetItem(&lvItem);

//		double data = atof(strData);
//		strData.Format("%.3f", data);
//
//		SetItemText(m_nCurSel, 1, strData);
//	}
}

void CListEditCtrl::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	if(nChar == VK_DELETE)
	{
		POSITION pos = GetFirstSelectedItemPosition();
		if(pos)
		{
			int nItem = GetNextSelectedItem(pos);
			DeleteItem(nItem);
		}
	}
	else if(nChar == VK_INSERT)
	{
		MakeEditCtrl();	
	}

	CListCtrl::OnKeyUp(nChar, nRepCnt, nFlags);
}

BOOL CListEditCtrl::MakeEditCtrl()
{
	UpdateText();

	POSITION pos = GetFirstSelectedItemPosition();
	m_nCurSel = GetNextSelectedItem(pos);

//	if( m_nCurSel > 15 )			//Max 15
//		return FALSE;

	//새로 생성한다.
	if( m_nCurSel < 0 )
	{
		m_nCurSel = GetItemCount();
		CString strTmp;
		LV_ITEM lvItem;
		ZeroMemory(&lvItem, sizeof(lvItem));
		lvItem.mask = LVIF_TEXT;
		lvItem.iItem = m_nCurSel+1;
		lvItem.iSubItem = 0;
		strTmp.Format("%d", m_nCurSel+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTmp;
		InsertItem(&lvItem);
	}	
//	LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;
//	m_nCurSel = pDispInfo->item.iItem;

	CRect rect;
	GetClientRect(rect);

	POINT point;
	GetItemPosition(m_nCurSel, &point);



//	CRect rect1;
//	GetItemRect(m_nCurSel, &rect1, LVIR_BOUNDS);
//	BOOL nRtn = GetSubItemRect(
//	   m_nCurSel,
//	   1,
//	   LVIR_BOUNDS,
//	   rect 
//	);

//	int nCol = pDispInfo->item.iSubItem;
	int nWidth = GetColumnWidth(0);


	rect.top	+= point.y;
	rect.bottom  = rect.top+18;
	rect.left += nWidth;

	CString strData;
	strData = GetItemText(m_nCurSel, 1);
	
	if( !m_pEdit )
	{
		m_pEdit = new CEdit;
		m_pEdit->Create(
			ES_LEFT | 
			ES_NOHIDESEL | 
			WS_VISIBLE | 
			WS_BORDER, 
			rect, this, 1);
	}	
	else
	{
		m_pEdit->ShowWindow(SW_SHOW);
		m_pEdit->MoveWindow(rect);
	}

	m_pEdit->SetWindowText(strData);
	m_pEdit->SetSel(0, -1);
	m_pEdit->SetFocus();

	return TRUE;
}

void CListEditCtrl::OnKillFocus(CWnd* pNewWnd) 
{
	CListCtrl::OnKillFocus(pNewWnd);
	
	// TODO: Add your message handler code here
//	UpdateText();
}
