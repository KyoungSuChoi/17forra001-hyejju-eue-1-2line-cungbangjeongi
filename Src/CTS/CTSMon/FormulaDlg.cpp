// FormulaDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "FormulaDlg.h"


// CFormulaDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CFormulaDlg, CDialog)

CFormulaDlg::CFormulaDlg(CCTSMonDoc* pDoc, CWnd* pParent /*=NULL*/)
	: CDialog(CFormulaDlg::IDD, pParent)
	, m_strCapacityFormula(_T(""))
	, m_strDCRFormula(_T(""))
	, m_strCapacity(_T(""))
	, m_strCapTemp(_T(""))
	, m_strDCR(_T(""))
	, m_strDCRTemp(_T(""))
{
	m_bChkDCR_T = false;
	m_bChkCapacity_T = false;

	m_pDoc = pDoc;
}

CFormulaDlg::~CFormulaDlg()
{
}

void CFormulaDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LABEL1, m_Label1);
	DDX_Control(pDX, IDC_LABEL2, m_Label2);
	DDX_Text(pDX, IDC_EDIT1, m_strCapacityFormula);
	DDX_Text(pDX, IDC_EDIT2, m_strDCRFormula);
	DDX_Text(pDX, IDC_EDIT3, m_strCapacity);
	DDX_Text(pDX, IDC_EDIT6, m_strCapTemp);
	DDX_Text(pDX, IDC_EDIT7, m_strDCR);
	DDX_Text(pDX, IDC_EDIT8, m_strDCRTemp);
	DDX_Control(pDX, IDC_LABEL3, m_Label3);
	DDX_Control(pDX, IDC_LABEL4, m_Label4);
}


BEGIN_MESSAGE_MAP(CFormulaDlg, CDialog)
	ON_BN_CLICKED(IDC_BTN_CHECK_CAPACITY_T, &CFormulaDlg::OnBnClickedBtnCheckCapacityT)
	ON_BN_CLICKED(IDC_BTN_CHECK_DCIR_T, &CFormulaDlg::OnBnClickedBtnCheckDcirT)
	ON_BN_CLICKED(IDOK, &CFormulaDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CFormulaDlg 메시지 처리기입니다.
void CFormulaDlg::InitFont()
{	
	LOGFONT LogFont;

	GetDlgItem(IDC_STATIC1)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight = 1000;
	LogFont.lfHeight = 20;

	font.CreateFontIndirect( &LogFont );

	GetDlgItem(IDC_STATIC1)->SetFont(&font);
	GetDlgItem(IDC_STATIC2)->SetFont(&font);
	GetDlgItem(IDC_EDIT1)->SetFont(&font);
	GetDlgItem(IDC_EDIT2)->SetFont(&font);
	GetDlgItem(IDC_EDIT3)->SetFont(&font);
	GetDlgItem(IDC_EDIT6)->SetFont(&font);
	GetDlgItem(IDC_EDIT7)->SetFont(&font);
	GetDlgItem(IDC_EDIT8)->SetFont(&font);
}

void CFormulaDlg::InitLabel()
{	
	m_Label1.SetBkColor(RGB_MIDNIGHTBLUE)
		.SetTextColor(RGB_WHITE)
		.SetFontSize(18)
		.SetFontBold(TRUE)
		.SetText("NG");

	m_Label2.SetBkColor(RGB_MIDNIGHTBLUE)
		.SetTextColor(RGB_WHITE)
		.SetFontSize(18)
		.SetFontBold(TRUE)
		.SetText("NG");

	m_Label3.SetFontSize(18)
		.SetTextColor(RGB(255,255,255))
		.SetBkColor(RGB(50,50,50))		
		.SetFontBold(TRUE)
		.SetText("-");

	m_Label4.SetFontSize(18)
		.SetTextColor(RGB(255,255,255))
		.SetBkColor(RGB(50,50,50))		
		.SetFontBold(TRUE)
		.SetText("-");
}

BOOL CFormulaDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	InitFont();
	InitLabel();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CFormulaDlg::SaveSetting()
{
	UpdateData(TRUE);

	AfxGetApp()->WriteProfileString(FROM_SYSTEM_SECTION, "CapacityFormula", m_strCapacityFormula);
	AfxGetApp()->WriteProfileString(FROM_SYSTEM_SECTION, "DCRFormula", m_strDCRFormula);

	m_pDoc->m_strCapacityFormula = m_strCapacityFormula;
	m_pDoc->m_strDCRFormula = m_strDCRFormula;
}

bool CFormulaDlg::LoadSetting()
{
	m_bChkCapacity_T = FALSE;
	m_bChkDCR_T = FALSE;

	fnUpdateStatus();

	m_strCapacityFormula = m_pDoc->m_strCapacityFormula;
	m_strDCRFormula = m_pDoc->m_strDCRFormula;

	UpdateData(FALSE);

	return true;
}

void CFormulaDlg::fnUpdateStatus()
{
	if( m_bChkCapacity_T == true )
	{
		m_Label1.SetText("OK");
	}
	else
	{
		m_Label1.SetText("NG");
	}

	if( m_bChkDCR_T == true )
	{
		m_Label2.SetText("OK");
	}
	else
	{
		m_Label2.SetText("NG");
	}
}

void CFormulaDlg::fnUpdateCapacityT()
{
	UpdateData(TRUE);

	if( m_strCapTemp.IsEmpty() || m_strCapacity.IsEmpty() )
	{
		return;
	}

	CString strTemp;
	double dResult;
	m_bChkCapacity_T = false;
	m_Label3.SetText("-");

	CString strCapacityFormula;
	strCapacityFormula = m_strCapacityFormula;

	strCapacityFormula = GStr::str_Replace(strCapacityFormula, m_strCapTemp, 'T' );
	strCapacityFormula = GStr::str_Replace(strCapacityFormula, m_strCapacity, 'C' );

	if( GMath::syntaxsearch((LPSTR)(LPCTSTR)strCapacityFormula) != -1 )
	{
		m_bChkCapacity_T = true;

		char *postfix;
		postfix = GMath::infix_to_postfix((LPSTR)(LPCTSTR)strCapacityFormula);
		dResult = GMath::evalPostfix(postfix);

		strTemp.Format("%f", dResult);
		m_Label3.SetText(strTemp);

		delete postfix;
	}

	fnUpdateStatus();
}

void CFormulaDlg::OnBnClickedBtnCheckCapacityT()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	fnUpdateCapacityT();
	fnUpdateStatus();
}

void CFormulaDlg::OnBnClickedBtnCheckDcirT()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	fnUpdateDcirT();
	fnUpdateStatus();
}

void CFormulaDlg::fnUpdateDcirT()
{
	UpdateData(TRUE);

	if( m_strDCRTemp.IsEmpty() || m_strDCR.IsEmpty() )
	{
		return;
	}

	CString strTemp;
	double dResult;
	m_bChkDCR_T = false;
	m_Label4.SetText("-");

	CString strDCRFormula;
	strDCRFormula = m_strDCRFormula;

	strDCRFormula = GStr::str_Replace(strDCRFormula, m_strDCRTemp, 'T' );
	strDCRFormula = GStr::str_Replace(strDCRFormula, m_strDCR, 'D' );

	if( GMath::syntaxsearch((LPSTR)(LPCTSTR)strDCRFormula) != -1 )
	{
		m_bChkDCR_T = true;

		char *postfix;

		postfix = GMath::infix_to_postfix((LPSTR)(LPCTSTR)strDCRFormula);
		dResult = GMath::evalPostfix(postfix);

		strTemp.Format("%f", dResult);
		m_Label4.SetText(strTemp);

		delete postfix;
	}
}

void CFormulaDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( m_bChkDCR_T == true && m_bChkCapacity_T == true )
	{
		SaveSetting();
	}
	else
	{
		AfxMessageBox("Please! Check the formula setting.", MB_ICONERROR);
		return;
	}
	
	OnOK();
}