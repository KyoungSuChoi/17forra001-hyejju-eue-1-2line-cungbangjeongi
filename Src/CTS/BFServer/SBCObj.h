#pragma once

extern EP_STR_MODULE_DATA	*g_pstModule;
extern BOOL	InitGroupData (int nModuleIndex);	//Module Data

//Channel Mapping FTN
extern int g_channelMap[EP_MAX_CH_PER_MD];
extern int g_nChannelIndexType;

extern CSBCCircularQueue <SBCQueue_DATA> g_Que;
extern CSBCCircularQueue <SBCQueue_DATA> g_DlgQue;

class CSBCObj : public CSBCNetObj<CSBCObj>
{
public:

	CSBCObj();
	~CSBCObj(void);
	VOID				Init();
	VOID				InitPacketVector();

	BOOL				SetNotifyWnd(HWND);

	//BOOL				SetPacketVector();
	BOOL				SetPacketVector(NEW_SBC_MESSAGE& _Msg);

	//SBCNetState			GetPacketState(){return mNState;}
	//VOID				SetPacketState(SBCNetState _state){mNState = _state;}

	EP_STR_MODULE_DATA* GetModuleData(){return mpstModule;}
	
	CHAR*				GetPeerName();
	INT					GetModuleID();
	
	BOOL				WriteingPacket();

	BOOL				SavePacket(DWORD command);
	BOOL				SavePacket(EP_MSG_HEADER* header, BYTE* data, DWORD datalen);
	
	VOID				SendCommand();
	BOOL				RecvCommand(UINT);


	//////////////////////////////////////////////////////////////////////////
	SBCPacketError		SendEP_CMD_RESPONSE(EP_MSG_HEADER* _hdr, SBCPacketError Code);
	//////////////////////////////////////////////////////////////////////////
	//Con Seq
	BOOL				SendEP_CMD_MD_SET_DATA();
	SBCPacketError		RecvEP_CMD_VER_DATA(EP_MSG_HEADER*, BYTE*, DWORD);
	//////////////////////////////////////////////////////////////////////////
	//Packet Paser
	SBCPacketError		RecvEP_CMD_RESPONSE(EP_MSG_HEADER*, BYTE*, DWORD);
	SBCPacketError		RecvEP_CMD_STEPDATA(EP_MSG_HEADER*, BYTE*, DWORD);
	SBCPacketError		RecvEP_CMD_AUTO_GP_STATE_DATA(EP_MSG_HEADER*, BYTE*, DWORD);
	SBCPacketError		RecvEP_CMD_AUTO_GP_DATA(EP_MSG_HEADER*, BYTE*, DWORD);
	SBCPacketError		RecvEP_CMD_AUTO_GP_STEP_END_DATA(EP_MSG_HEADER*, BYTE*, DWORD);
	SBCPacketError		RecvEP_CMD_TAG_INFO(EP_MSG_HEADER*, BYTE*, DWORD);
	SBCPacketError		RecvEP_CMD_AUTO_CHECK_RESULT(EP_MSG_HEADER*, BYTE*, DWORD);
	SBCPacketError		RecvEP_CMD_AUTO_SENSOR_DATA(EP_MSG_HEADER*, BYTE*, DWORD);
	SBCPacketError		RecvEP_CMD_AUTO_EMG_DATA(EP_MSG_HEADER*, BYTE*, DWORD);
	SBCPacketError		RecvEP_CMD_USER_CMD(EP_MSG_HEADER*, BYTE*, DWORD);
	SBCPacketError		RecvEP_CMD_REAL_TIME_DATA(EP_MSG_HEADER*, BYTE*, DWORD);
	//////////////////////////////////////////////////////////////////////////
	//cal
	SBCPacketError		RecvEP_CMD_CAL_RES(EP_MSG_HEADER*, BYTE*, DWORD);
	SBCPacketError		RecvEP_CMD_CHK_RES(EP_MSG_HEADER*, BYTE*, DWORD);
	SBCPacketError		RecvEP_CMD_CAL_END(EP_MSG_HEADER*, BYTE*, DWORD);
	SBCPacketError		RecvEP_CMD_CHK_END(EP_MSG_HEADER*, BYTE*, DWORD);
	//
	SBCPacketError		RecvEP_CMD_RM_RES(EP_MSG_HEADER*, BYTE*, DWORD);
	SBCPacketError		RecvEP_CMD_RM_STOP_RESPONS(EP_MSG_HEADER*, BYTE*, DWORD);
	//////////////////////////////////////////////////////////////////////////
	SBCPacketError		RecvEP_CMD_LINE_MODE_DATA(EP_MSG_HEADER*, BYTE*, DWORD);
	SBCPacketError		RecvEP_CMD_MAPPING_DATA(EP_MSG_HEADER*, BYTE*, DWORD);
	SBCPacketError		RecvEP_CMD_CAL_POINT_DATA(EP_MSG_HEADER*, BYTE*, DWORD);
	//////////////////////////////////////////////////////////////////////////SKI-Formation
	//notuse
	//SBCPacketError		RecvEP_CMD_CAL_RESULT(EP_MSG_HEADER*, BYTE*, DWORD);
	//SBCPacketError		RecvEP_CMD_AUTO_GP_STEP_SEC_DATA(EP_MSG_HEADER*, BYTE*, DWORD);
	//SBCPacketError		RecvEP_CMD_REAL_CHAMBER_DATA(EP_MSG_HEADER*, BYTE*, DWORD);	
	//SBCPacketError		RecvEP_CMD_ALL_NG(EP_MSG_HEADER*, BYTE*, DWORD);	
	//SBCPacketError		RecvEP_CMD_SWITCH(EP_MSG_HEADER*, BYTE*, DWORD);

	INT					GetMappingCh(int nSeqIndex, WORD wSystemType, int nMemberIndex);

	//////////////////////////////////////////////////////////////////////////
	INT					CheckModuleIDValidate(LPEP_MD_SYSTEM_DATA pSysData);
	BOOL				InitGroupData ();
	VOID				ModuleConnected();
	VOID				ModuleClosed();
	BOOL				CloseGroupData();
	VOID				GroupChageState();

	VOID				SetNotifyDlgWnd(Request_CMD*);
	VOID				SendNotifyDlgWnd(EP_MSG_HEADER* _hdr, BYTE* data, DWORD datalen);

	
	inline BOOL CheckMsgHeader(LPEP_MSG_HEADER lpHeader)
	{
		CSBCSyncObj SBCSync;

		ASSERT(lpHeader);//cal ???? 
		if(mpstModule)
		{
			if(lpHeader->wChannelNum < 0 || lpHeader->wChannelNum > mTotChNo)	
				return FALSE;
			else
				return TRUE;
			//	if(GetCmdDataSize(lpHeader->nCommand) != (lpHeader->nLength - SizeofHeader()))	return -4;
		}
		return FALSE;
	}
	UINT				GetCloseCnt(){CSBCSyncObj SBCSync; return mCloseCnt;}
	VOID				SetCloseCnt(){CSBCSyncObj SBCSync; mCloseCnt++;}
	VOID				ResetCloseCnt(){CSBCSyncObj SBCSync; mCloseCnt = 0;}

	VOID				SetConnect(){CSBCSyncObj SBCSync; mbIsConnect = TRUE;}
	BOOL				IsConnect(){CSBCSyncObj SBCSync; return mbIsConnect;}

	VOID				SetSendLog(EP_MSG_HEADER*, BYTE*, DWORD);
	VOID				SetRecvLog(EP_MSG_HEADER*, BYTE*, DWORD);
private:
	HWND hMsgWnd;

	SBCNetState mNState;

	UINT mRetryCnt;

	CHAR mszIP[16];
	USHORT mshPort;

	INT				mMsgIdx;
	std::vector<NEW_SBC_MESSAGE*> m_vSendMessage;

	EP_STR_MODULE_DATA*	 mpstModule;			//Module Struct

	std::list<Request_CMD*> m_LstRequestCmd;

	UINT mCloseCnt;

	BOOL mbIsConnect;

	INT mModuleID;
	INT mTotChNo;
};