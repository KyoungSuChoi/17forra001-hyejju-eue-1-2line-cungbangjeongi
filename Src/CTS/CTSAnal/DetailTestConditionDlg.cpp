// DetailTestConditionDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "DetailTestConditionDlg.h"
#include "PrntScreen.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define MAX_LINE_PAGE	48
#define X_PAGE_MARGIN	400
#define Y_PAGE_MARGIN	600
#define TEXT_HEIGHT		120
#define BOTTOM_MARGIN	(TEXT_HEIGHT*4)

/////////////////////////////////////////////////////////////////////////////
// CDetailTestConditionDlg dialog

bool CDetailTestConditionDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CDetailTestConditionDlg"), _T("TEXT_CDetailTestConditionDlg_CNT"), _T("TEXT_CDetailTestConditionDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CDetailTestConditionDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CDetailTestConditionDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

CDetailTestConditionDlg::CDetailTestConditionDlg(CTestCondition *pTestCondition, CWnd* pParent /*=NULL*/)
	: CDialog(CDetailTestConditionDlg::IDD, pParent)
{
	LanguageinitMonConfig();

	//{{AFX_DATA_INIT(CDetailTestConditionDlg)
	//}}AFX_DATA_INIT
	m_pDoc = NULL;
//	m_StepRow = 0;
//	m_Condition = NULL;
	m_pTestCondition = pTestCondition;

	ASSERT(m_pTestCondition);
}

CDetailTestConditionDlg::~CDetailTestConditionDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


void CDetailTestConditionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDetailTestConditionDlg)
	DDX_Control(pDX, IDC_EXCEL_SAVE_BUTTON, m_btnExcel);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDC_PRINT_BUTTON, m_btnPrint);
	DDX_Control(pDX, IDC_DETAIL_STATIC5, m_Static1);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CDetailTestConditionDlg, CDialog)
	//{{AFX_MSG_MAP(CDetailTestConditionDlg)
	ON_BN_CLICKED(IDC_EXCEL_SAVE_BUTTON, OnExcelSaveButton)
	ON_BN_CLICKED(IDC_PRINT_BUTTON, OnPrintButton)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_GRID_MOVECELL, OnMovedCurrentCell)
//	ON_MESSAGE(WM_GRID_DOUBLECLICK, OnGridDoubleClick)
ON_BN_CLICKED(IDOK, &CDetailTestConditionDlg::OnBnClickedOk)
ON_EN_CHANGE(IDC_DETAIL_DATE, &CDetailTestConditionDlg::OnEnChangeDetailDate)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDetailTestConditionDlg message handlers

BOOL CDetailTestConditionDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

//	m_Static1.SetFontName(GetStringTable(IDS_LANG_TEXT_FONT));
	m_Static1.SetFontSize(14);
	m_Static1.SetTextColor(RGB(255,255,0));
	m_Static1.SetBkColor(RGB(0,128,128));

	ASSERT(m_pDoc);

	CString tmp;
	CStringArray aString;

	InitStepGrid();
	InitSafetyGrid();
	InitFailGrid();
	InitGradeGrid();

//	if(m_Condition == NULL)		return	TRUE;

	DisplayCondition();

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
/*
void CDetailTestConditionDlg::InitGrid(CMyGridWnd *pGrid, int id, int rowNum, int colNum, CStringArray *aTitle)
{
	pGrid->SubclassDlgItem(id, this);
	pGrid->Initialize();
	BOOL bLock = pGrid->LockUpdate();
	pGrid->SetRowCount(rowNum);
	pGrid->SetColCount(colNum);
	pGrid->m_bSameColSize = TRUE;
	pGrid->m_bSameRowSize = FALSE;
	pGrid->SetDefaultRowHeight(20);
	
	for(int i=0;i<colNum;i++)
		pGrid->SetValueRange(CGXRange(0, i+1), aTitle->GetAt(i));

	pGrid->LockUpdate(bLock);
	pGrid->Redraw();
	pGrid->GetParam()->EnableSelection(FALSE);
}
*/
LONG CDetailTestConditionDlg::OnMovedCurrentCell(WPARAM wParam, LPARAM lParam)
{
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;
	if(pGrid == &m_StepGrid)
	{
		int nRow, nCol;
		nCol = LOWORD(wParam);
		nRow = HIWORD(wParam);

		if(nRow < 1 || nRow > m_pTestCondition->GetTotalStepNo())	return 0;
		
		DisplayFailGrid(nRow-1);
		DisplayGradeGrid(nRow-1);
	}
	return 0;
}

void CDetailTestConditionDlg::InitSafetyGrid()
{
	m_SafetyGrid.SubclassDlgItem(IDC_DETAIL_SAFETY_GRID, this);
	m_SafetyGrid.Initialize();
	BOOL bLock = m_SafetyGrid.LockUpdate();
	m_SafetyGrid.SetRowCount(0);
	m_SafetyGrid.SetColCount(2);
	m_SafetyGrid.m_bSameColSize = TRUE;
	m_SafetyGrid.m_bSameRowSize = TRUE;
	m_SafetyGrid.SetDefaultRowHeight(20);
	
	m_SafetyGrid.SetValueRange(CGXRange(0, 1), TEXT_LANG[0]);//"항목"
	m_SafetyGrid.SetValueRange(CGXRange(0, 2), TEXT_LANG[1]);//"값"

	m_SafetyGrid.SetStyleRange(CGXRange().SetCols(1), CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(255, 255, 255)));

	m_SafetyGrid.LockUpdate(bLock);
	m_SafetyGrid.Redraw();
	m_SafetyGrid.GetParam()->EnableSelection(FALSE);

}

void CDetailTestConditionDlg::DisplaySafetyGrid()
{
	BOOL bLock = m_SafetyGrid.LockUpdate();
	
	CString strTemp;
	
	ASSERT(m_pTestCondition);

	STR_CHECK_PARAM *pCheckParam = m_pTestCondition->GetCheckParam();
/*	m_SafetyGrid.SetValueRange(CGXRange(1, 1), ::GetStringTable(IDS_TEXT_OCV_UPPER));
//	strTemp.Format("%.3fV",  VTG_PRECISION(m_Condition->boardParam.lOCVUpperValue));
	m_SafetyGrid.SetValueRange(CGXRange(1, 2), m_pDoc->ValueString(m_Condition->checkParam.fOCVUpperValue, EP_VOLTAGE));
	m_SafetyGrid.SetValueRange(CGXRange(2, 1), ::GetStringTable(IDS_TEXT_OCV_LOWER));
//	strTemp.Format("%.3fV",  VTG_PRECISION(m_Condition->boardParam.lOCVLowerValue));
	m_SafetyGrid.SetValueRange(CGXRange(2, 2), m_pDoc->ValueString(m_Condition->checkParam.fOCVLowerValue, EP_VOLTAGE));
*/	

//	m_SafetyGrid.InsertRows(m_SafetyGrid.GetRowCount()+1 , 1);
//	m_SafetyGrid.SetValueRange(CGXRange(1, 1), "검사전압설정");
//	strTemp.Format("%.3fV",  VTG_PRECISION(m_Condition->boardParam.lVRef));
//	m_SafetyGrid.SetValueRange(CGXRange(1, 2), m_pDoc->ValueString(pCheckParam->fVRef, EP_VOLTAGE, TRUE));

	m_SafetyGrid.InsertRows(m_SafetyGrid.GetRowCount()+1 , 1);
	m_SafetyGrid.SetValueRange(CGXRange(1, 1), TEXT_LANG[2]);//"검사전류설정"
//	strTemp.Format("%.1fmA",  CRT_PRECISION(m_Condition->boardParam.lIRef));
	m_SafetyGrid.SetValueRange(CGXRange(1, 2), m_pDoc->ValueString(pCheckParam->fIRef, EP_CURRENT, TRUE));

	m_SafetyGrid.InsertRows(m_SafetyGrid.GetRowCount()+1 , 1);
	m_SafetyGrid.SetValueRange(CGXRange(2, 1), TEXT_LANG[3]);//"검사시간"
//	strTemp.Format("%d sec", m_Condition->boardParam.lTime/10);
	m_SafetyGrid.SetValueRange(CGXRange(2, 2), m_pDoc->ValueString(pCheckParam->fTime, EP_STEP_TIME, TRUE));

	m_SafetyGrid.LockUpdate(bLock);
	m_SafetyGrid.Redraw();
}

void CDetailTestConditionDlg::InitStepGrid()
{
	m_StepGrid.SubclassDlgItem(IDC_DETAIL_STEP_GRID, this);
	m_StepGrid.Initialize();
	BOOL bLock = m_StepGrid.LockUpdate();
	m_StepGrid.SetDefaultRowHeight(20);	
	m_StepGrid.SetRowCount(0);
	m_StepGrid.SetColCount(6);
	m_StepGrid.SetValueRange(CGXRange(0, 1), TEXT_LANG[4]);//"Step"
	m_StepGrid.SetValueRange(CGXRange(0, 2), TEXT_LANG[5]);//"Type"
	m_StepGrid.SetValueRange(CGXRange(0, 3), TEXT_LANG[6]);//"Mode"
	m_StepGrid.SetValueRange(CGXRange(0, 4), TEXT_LANG[12]);//"전압"
	m_StepGrid.SetValueRange(CGXRange(0, 5), TEXT_LANG[13]);//"전류"
	m_StepGrid.SetValueRange(CGXRange(0, 6), TEXT_LANG[9]);//"종료조건"
	m_StepGrid.m_bSameColSize = FALSE;
	m_StepGrid.m_bCustomWidth = TRUE;
	m_StepGrid.m_bSameRowSize = FALSE;
	m_StepGrid.m_nWidth[1] = 50;
	m_StepGrid.m_nWidth[2] = 100;
	m_StepGrid.m_nWidth[3] = 100;
	m_StepGrid.m_nWidth[4] = 100;
	m_StepGrid.m_nWidth[5] = 100;
	m_StepGrid.EnableCellTips();
	m_StepGrid.LockUpdate(bLock);
	m_StepGrid.Redraw();
	m_StepGrid.GetParam()->EnableSelection(FALSE);
}

void CDetailTestConditionDlg::DiaplayStepGrid()
{
	BOOL bLock = m_StepGrid.LockUpdate();

	CStep *pStepInfo;
	STR_COMMON_STEP *pStep, stepData;

	int nStepRow = m_pTestCondition->GetTotalStepNo();
	if(m_StepGrid.GetRowCount() > 0)
	{
		m_StepGrid.RemoveRows(1, m_StepGrid.GetRowCount());
	}

	m_StepGrid.SetRowCount(nStepRow);
	STR_TOP_CONFIG *pConfig = m_pDoc->GetTopConfig();
	BYTE color;

	for(int nRow =0 ; nRow < nStepRow; nRow++)
	{
		pStepInfo = m_pTestCondition->GetStep(nRow);
		if(pStepInfo == NULL) break;	
		stepData= pStepInfo->GetStepData();
		pStep = &stepData;
		
		m_StepGrid.SetValueRange(CGXRange(nRow+1, 1), (LONG)(pStep->stepHeader.stepIndex+1));

		switch(pStep->stepHeader.type)
		{
		case EP_TYPE_CHARGE:
			{
				m_pDoc->GetStateMsg(EP_STATE_CHARGE, color);
				m_StepGrid.SetStyleRange(CGXRange(nRow+1, 2), CGXStyle().SetInterior(pConfig->m_BStateColor[color])
																	.SetValue(StepTypeMsg(pStep->stepHeader.type))
																	.SetTextColor(pConfig->m_TStateColor[color]));
				m_StepGrid.SetValueRange(CGXRange(nRow+1, 3), ModeTypeMsg(pStep->stepHeader.type, pStep->stepHeader.mode));
				m_StepGrid.SetValueRange(CGXRange(nRow+1, 4), m_pDoc->ValueString(pStep->fVref, EP_VOLTAGE, TRUE));
				m_StepGrid.SetValueRange(CGXRange(nRow+1, 5), m_pDoc->ValueString(pStep->fIref, EP_CURRENT, TRUE));
				m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->StepEndString(pStep));
				break;
			}
																	
		case EP_TYPE_DISCHARGE:
				m_pDoc->GetStateMsg(EP_STATE_DISCHARGE, color);
				m_StepGrid.SetStyleRange(CGXRange(nRow+1, 2), CGXStyle().SetInterior(pConfig->m_BStateColor[color])
																	.SetValue(StepTypeMsg(pStep->stepHeader.type))
																	.SetTextColor(pConfig->m_TStateColor[color]));
				m_StepGrid.SetValueRange(CGXRange(nRow+1, 3), ModeTypeMsg(pStep->stepHeader.type, pStep->stepHeader.mode));
				m_StepGrid.SetValueRange(CGXRange(nRow+1, 4), m_pDoc->ValueString(pStep->fVref, EP_VOLTAGE, TRUE));
				m_StepGrid.SetValueRange(CGXRange(nRow+1, 5), m_pDoc->ValueString(pStep->fIref, EP_CURRENT, TRUE));
				m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->StepEndString(pStep));
				break;
		case EP_TYPE_IMPEDANCE:
			{
				m_pDoc->GetStateMsg(EP_STATE_IMPEDANCE, color);
				m_StepGrid.SetStyleRange(CGXRange(nRow+1, 2), CGXStyle().SetInterior(pConfig->m_BStateColor[color])
																	.SetValue(StepTypeMsg(pStep->stepHeader.type))
																	.SetTextColor(pConfig->m_TStateColor[color]));
				m_StepGrid.SetValueRange(CGXRange(nRow+1, 3), ModeTypeMsg(pStep->stepHeader.type, pStep->stepHeader.mode));
				m_StepGrid.SetValueRange(CGXRange(nRow+1, 4), m_pDoc->ValueString(pStep->fVref, EP_VOLTAGE, TRUE));
				m_StepGrid.SetValueRange(CGXRange(nRow+1, 5), m_pDoc->ValueString(pStep->fIref, EP_CURRENT, TRUE));
				m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->StepEndString(pStep));
				break;
			}
		case EP_TYPE_REST:
				m_pDoc->GetStateMsg(EP_STATE_REST, color);
				m_StepGrid.SetStyleRange(CGXRange(nRow+1, 2), CGXStyle().SetInterior(pConfig->m_BStateColor[color])
																	.SetValue(StepTypeMsg(pStep->stepHeader.type))
																	.SetTextColor(pConfig->m_TStateColor[color]));
				m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->StepEndString(pStep));
				break;
		case EP_TYPE_LOOP:
			{
				m_pDoc->GetStateMsg(EP_STATE_REST, color);
				m_StepGrid.SetStyleRange(CGXRange(nRow+1, 2), CGXStyle().SetInterior(pConfig->m_BStateColor[color])
																	.SetValue(StepTypeMsg(pStep->stepHeader.type))
																	.SetTextColor(pConfig->m_TStateColor[color]));
				m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->StepEndString(pStep));
				break;
				break;
			}
		case EP_TYPE_OCV:
				m_pDoc->GetStateMsg(EP_STATE_OCV, color);
				m_StepGrid.SetStyleRange(CGXRange(nRow+1, 2), CGXStyle().SetInterior(pConfig->m_BStateColor[color])
																	.SetValue(StepTypeMsg(pStep->stepHeader.type))
																	.SetTextColor(pConfig->m_TStateColor[color]));
				break;
		case EP_TYPE_END:
			{
				m_pDoc->GetStateMsg(EP_STATE_END, color);
				m_StepGrid.SetStyleRange(CGXRange(nRow+1, 2), CGXStyle().SetInterior(pConfig->m_BStateColor[color])
																	.SetValue(StepTypeMsg(pStep->stepHeader.type))
																	.SetTextColor(pConfig->m_TStateColor[color]));
				break;
			}
		default:
			{
				m_StepGrid.SetValueRange(CGXRange(nRow+1, 1), "Unknown");
				break;
			}
		}
	}

	m_StepGrid.LockUpdate(bLock);
	m_StepGrid.Redraw();
	
}

void CDetailTestConditionDlg::InitFailGrid()
{
	m_FailGrid.SubclassDlgItem(IDC_DETAIL_FAIL_GRID, this);
	m_FailGrid.Initialize();
	BOOL bLock = m_FailGrid.LockUpdate();

#ifdef _EDLC_TEST_SYSTEM
	m_FailGrid.SetRowCount(7);
#else
	m_FailGrid.SetRowCount(6);
#endif
	m_FailGrid.SetColCount(3);
	m_FailGrid.m_bSameColSize = TRUE;
	m_FailGrid.m_bSameRowSize = FALSE;
	m_FailGrid.SetDefaultRowHeight(20);
	
	m_FailGrid.SetValueRange(CGXRange(0, 1), TEXT_LANG[0]);//"항목"
	m_FailGrid.SetValueRange(CGXRange(0, 2), TEXT_LANG[10]);//"하한값"
	m_FailGrid.SetValueRange(CGXRange(0, 3), TEXT_LANG[11]);//"상한값"

	m_FailGrid.SetStyleRange(CGXRange().SetCols(1), CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(255,255,255)));

	m_FailGrid.SetValueRange(CGXRange(1, 1), TEXT_LANG[12]);//"전압"
	m_FailGrid.SetValueRange(CGXRange(2, 1), TEXT_LANG[13]);//"전류"
	m_FailGrid.SetValueRange(CGXRange(3, 1), TEXT_LANG[14]);//"용량"
	m_FailGrid.SetValueRange(CGXRange(4, 1), TEXT_LANG[15]);//"저항"
	m_FailGrid.SetValueRange(CGXRange(5, 1), "dV / dt");
	m_FailGrid.SetValueRange(CGXRange(6, 1), "dI / dt");
#ifdef _EDLC_TEST_SYSTEM
	m_FailGrid.SetValueRange(CGXRange(7, 1), TEXT_LANG[16]"용량검사");
#endif	
	m_FailGrid.LockUpdate(bLock);
	m_FailGrid.Redraw();
	m_FailGrid.GetParam()->EnableSelection(FALSE);
}

void CDetailTestConditionDlg::DisplayFailGrid(int nStepIndex)
{
	ASSERT(nStepIndex >= 0 && nStepIndex < m_pTestCondition->GetTotalStepNo());

	CString strTemp;	
	CStep *pStep = m_pTestCondition->GetStep(nStepIndex);
	if(pStep == NULL)	return;

	BOOL bLock = m_FailGrid.LockUpdate();

#ifdef _EDLC_TEST_SYSTEM
	for(int i =0; i<7; i++)                                                                         
#else
	for(int i =0; i<6; i++)                                                                         
#endif
	{
		m_FailGrid.SetValueRange(CGXRange(i+1, 2), "");
		m_FailGrid.SetValueRange(CGXRange(i+1, 3), "");
	}

	strTemp.Format("2. Step %d %s", nStepIndex+1, TEXT_LANG[17]);//"안전 조건"
	GetDlgItem(IDC_LIMIT_STATIC)->SetWindowText(strTemp);
	strTemp.Format("3. Step %d %s", nStepIndex+1, TEXT_LANG[18]);//"등급조건"
	GetDlgItem(IDC_GRADE_STATIC)->SetWindowText(strTemp);

	switch(pStep->m_type)
	{
	case EP_TYPE_CHARGE:
	case EP_TYPE_DISCHARGE:
		{
//			strTemp.Format("%.3f", VTG_PRECISION(pStep->lOverV));
			m_FailGrid.SetValueRange(CGXRange(1, 3), m_pDoc->ValueString(pStep->m_fHighLimitV, EP_VOLTAGE, TRUE));
//			strTemp.Format("%.3f", VTG_PRECISION(pStep->lLimitV));
			m_FailGrid.SetValueRange(CGXRange(1, 2), m_pDoc->ValueString(pStep->m_fLowLimitV, EP_VOLTAGE, TRUE));
//			strTemp.Format("%.3f", CRT_PRECISION(pStep->lOverI));
			m_FailGrid.SetValueRange(CGXRange(2, 3), m_pDoc->ValueString(pStep->m_fHighLimitI, EP_CURRENT, TRUE));
//			strTemp.Format("%.3f", CRT_PRECISION(pStep->lLimitI));
			m_FailGrid.SetValueRange(CGXRange(2, 2), m_pDoc->ValueString(pStep->m_fLowLimitI, EP_CURRENT, TRUE));
//			strTemp.Format("%.3f", ETC_PRECISION(pStep->lOverC));
			m_FailGrid.SetValueRange(CGXRange(3, 3), m_pDoc->ValueString(pStep->m_fHighLimitC, EP_CAPACITY, TRUE));
//			strTemp.Format("%.3f", ETC_PRECISION(pStep->lLimitC));
			m_FailGrid.SetValueRange(CGXRange(3, 2), m_pDoc->ValueString(pStep->m_fLowLimitC, EP_CAPACITY, TRUE));
//			strTemp.Format("%.3f", VTG_PRECISION(pStep->lDeltaV));
			m_FailGrid.SetValueRange(CGXRange(5, 3), m_pDoc->ValueString(pStep->m_fDeltaV, EP_VOLTAGE, TRUE));
			m_FailGrid.SetValueRange(CGXRange(5, 2), m_pDoc->ValueString(pStep->m_fDeltaTimeV, EP_STEP_TIME, TRUE));
//			strTemp.Format("%.3f", CRT_PRECISION(pStep->lDeltaI));
			m_FailGrid.SetValueRange(CGXRange(6, 3), m_pDoc->ValueString(pStep->m_fDeltaI, EP_CURRENT, TRUE));
			m_FailGrid.SetValueRange(CGXRange(6, 2), m_pDoc->ValueString(pStep->m_fDeltaTimeI, EP_STEP_TIME, TRUE));
#ifdef _EDLC_TEST_SYSTEM			
			m_FailGrid.SetValueRange(CGXRange(7, 3), m_pDoc->ValueString(pStep->m_fParam2, EP_VOLTAGE, TRUE));
			m_FailGrid.SetValueRange(CGXRange(7, 2), m_pDoc->ValueString(pStep->m_fParam1, EP_VOLTAGE, TRUE));
#endif
			break;
		}
	case EP_TYPE_REST:
		{
		//	strTemp.Format("%.3f", VTG_PRECISION(pStep->lOverV));
			m_FailGrid.SetValueRange(CGXRange(1, 3), m_pDoc->ValueString(pStep->m_fHighLimitV, EP_VOLTAGE, TRUE));
		//	strTemp.Format("%.3f", VTG_PRECISION(pStep->lLimitV));
			m_FailGrid.SetValueRange(CGXRange(1, 2), m_pDoc->ValueString(pStep->m_fLowLimitV, EP_VOLTAGE, TRUE));
			break;
		}
	case EP_TYPE_OCV:
		{
		//	strTemp.Format("%.3f", VTG_PRECISION(pStep->lOverV));
			m_FailGrid.SetValueRange(CGXRange(1, 3), m_pDoc->ValueString(pStep->m_fHighLimitV, EP_VOLTAGE, TRUE));
		//	strTemp.Format("%.3f", VTG_PRECISION(pStep->lLimitV));
			m_FailGrid.SetValueRange(CGXRange(1, 2), m_pDoc->ValueString(pStep->m_fLowLimitV, EP_VOLTAGE, TRUE));
			break;
		}
	case EP_TYPE_IMPEDANCE:
		{
		//	strTemp.Format("%.3f", VTG_PRECISION(pStep->lOverV));
			m_FailGrid.SetValueRange(CGXRange(1, 3), m_pDoc->ValueString(pStep->m_fHighLimitV, EP_VOLTAGE, TRUE));
		//	strTemp.Format("%.3f", VTG_PRECISION(pStep->lLimitV));
			m_FailGrid.SetValueRange(CGXRange(1, 2), m_pDoc->ValueString(pStep->m_fLowLimitV, EP_VOLTAGE, TRUE));
		//	strTemp.Format("%.3f", CRT_PRECISION(pStep->lOverI));
			m_FailGrid.SetValueRange(CGXRange(2, 3), m_pDoc->ValueString(pStep->m_fHighLimitI, EP_CURRENT, TRUE));
		//	strTemp.Format("%.3f", CRT_PRECISION(pStep->lLimitI));
			m_FailGrid.SetValueRange(CGXRange(2, 2), m_pDoc->ValueString(pStep->m_fLowLimitI, EP_CURRENT, TRUE));
		//	strTemp.Format("%.3f", ETC_PRECISION(pStep->lOverImpedance));
			m_FailGrid.SetValueRange(CGXRange(4, 3), m_pDoc->ValueString(pStep->m_fHighLimitImp, EP_IMPEDANCE, TRUE));
		//	strTemp.Format("%.3f", ETC_PRECISION(pStep->lLimitImpedance));
			m_FailGrid.SetValueRange(CGXRange(4, 2), m_pDoc->ValueString(pStep->m_fLowLimitImp, EP_IMPEDANCE, TRUE));
			break;
		}
	case EP_TYPE_END:
	default:
		break;
	}

	m_FailGrid.LockUpdate(bLock);
	m_FailGrid.Redraw();
}



void CDetailTestConditionDlg::InitGradeGrid()
{

	m_GradeGrid.SubclassDlgItem(IDC_DETAIL_GRADE_GRID, this);
	m_GradeGrid.Initialize();
	BOOL bLock = m_GradeGrid.LockUpdate();
	m_GradeGrid.SetRowCount(0);
	m_GradeGrid.SetColCount(4);
	m_GradeGrid.m_bSameColSize = TRUE;
	m_GradeGrid.m_bSameRowSize = FALSE;
	m_GradeGrid.SetDefaultRowHeight(20);
	
	m_GradeGrid.SetValueRange(CGXRange(0, 1), TEXT_LANG[19]);//"No"
	m_GradeGrid.SetValueRange(CGXRange(0, 2), TEXT_LANG[20]);//"Low Val."
	m_GradeGrid.SetValueRange(CGXRange(0, 3), TEXT_LANG[21]);//"High Val."
	m_GradeGrid.SetValueRange(CGXRange(0, 4), TEXT_LANG[22]);//"등급 Code"

	m_GradeGrid.LockUpdate(bLock);
	m_GradeGrid.Redraw();
	m_GradeGrid.GetParam()->EnableSelection(FALSE);

}

void CDetailTestConditionDlg::DisplayGradeGrid(int nStepIndex)
{
	ASSERT(nStepIndex >= 0 && nStepIndex < m_pTestCondition->GetTotalStepNo());

	int nRow = m_GradeGrid.GetRowCount();
	if(nRow >0)		m_GradeGrid.RemoveRows(1, nRow);

//	STR_COMMON_STEP *pStep = (STR_COMMON_STEP *)m_Condition->apStepList[nStepIndex];

	CStep *pStep = m_pTestCondition->GetStep(nStepIndex);
	if(pStep == NULL)	return;

	BOOL bLock = m_GradeGrid.LockUpdate();

	CString strTemp, strTemp1;
	GRADE_STEP grade;
	int a = pStep->m_Grading.GetGradeStepSize();
	for(int i=0; i<pStep->m_Grading.GetGradeStepSize(); i++)
	{
		m_GradeGrid.InsertRows(i+1, 1);
		m_GradeGrid.SetValueRange(CGXRange(i+1, 1), (LONG)(i+1));

		grade = pStep->m_Grading.GetStepData(i);
				
		switch(grade.lGradeItem)
		{
		case EP_GRADE_VOLTAGE	:
//			strTemp.Format("%.3f", VTG_PRECISION(pGrade->lValue));
			strTemp =  m_pDoc->ValueString(grade.fMin, EP_VOLTAGE, TRUE);
			strTemp1 =  m_pDoc->ValueString(grade.fMax, EP_VOLTAGE, TRUE);
			break;

		case EP_GRADE_CAPACITY	:
//			strTemp.Format("%.1f", CRT_PRECISION(pGrade->lValue));
			strTemp =  m_pDoc->ValueString(grade.fMin, EP_CAPACITY, TRUE);
			strTemp1 =  m_pDoc->ValueString(grade.fMax, EP_CAPACITY, TRUE);
			break;

		case EP_GRADE_IMPEDANCE		:
//			strTemp.Format("%.1f", ETC_PRECISION(pGrade->lValue));
			strTemp =  m_pDoc->ValueString(grade.fMin, EP_IMPEDANCE, TRUE);
			strTemp1 =  m_pDoc->ValueString(grade.fMax, EP_IMPEDANCE, TRUE);
			break;
		default:
			strTemp.Empty();
			strTemp1.Empty();
			break;
		}
		m_GradeGrid.SetValueRange(CGXRange(i+1, 2), strTemp);
		m_GradeGrid.SetValueRange(CGXRange(i+1, 3), strTemp1);
		m_GradeGrid.SetValueRange(CGXRange(i+1, 4), m_pDoc->ValueString((char )grade.strCode[0], EP_GRADE_CODE));
	}
	m_GradeGrid.LockUpdate(bLock);
	m_GradeGrid.Redraw();
}


void CDetailTestConditionDlg::OnExcelSaveButton() 
{
	// TODO: Add your control notification handler code here
	m_pTestCondition->ExcelSave();

/*	CFileDialog pDlg(FALSE, "", m_Condition->conditionHeader.szName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "csv 파일(*.csv)|*.csv|");
	if(IDOK == pDlg.DoModal())
	{
		FileName = pDlg.GetPathName();
	}
	if(FileName.IsEmpty())
	{
		return;
	}

	FILE *fp = fopen(FileName, "wt");
	if(fp == NULL)	return;

	CString strData;
	fprintf(fp, "[Procedure Name]\n");
	fprintf(fp, "%s,%s\n", ::GetStringTable(IDS_LABEL_PROC_NAME), m_Condition->conditionHeader.szName);
	fprintf(fp, "%s,%s\n", ::GetStringTable(IDS_LABEL_DESCRIPTOR), m_Condition->conditionHeader.szCreator);
	fprintf(fp, "%s,%s\n", ::GetStringTable(IDS_LABEL_CREATE_DATE), m_Condition->conditionHeader.szModifiedTime);
	fprintf(fp, "%s,%s\n\n", ::GetStringTable(IDS_LABEL_SUMMARY), m_Condition->conditionHeader.szDescription);

	fprintf(fp, "[Cell Check Param]\n");
	fprintf(fp, "%s,%s\n", ::GetStringTable(IDS_TEXT_OCV_UPPER), m_pDoc->ValueString(m_Condition->boardParam.lOCVUpperValue, EP_VOLTAGE));
	fprintf(fp, "%s,%s\n", ::GetStringTable(IDS_TEXT_OCV_LOWER), m_pDoc->ValueString(m_Condition->boardParam.lOCVLowerValue, EP_VOLTAGE));
	fprintf(fp, "%s,%s\n", ::GetStringTable(IDS_LABEL_VREF), m_pDoc->ValueString(m_Condition->boardParam.lVRef, EP_VOLTAGE));
	fprintf(fp, "%s,%s\n", ::GetStringTable(IDS_LABEL_IREF), m_pDoc->ValueString(m_Condition->boardParam.lIRef, EP_CURRENT));
	fprintf(fp, "%s,%s\n", ::GetStringTable(IDS_TEXT_TIME), m_pDoc->ValueString(m_Condition->boardParam.lTime, EP_STEP_TIME));
	fprintf(fp, "%s,%s\n", ::GetStringTable(IDS_TEXT_DELTAV), m_pDoc->ValueString(m_Condition->boardParam.lDeltaVoltage, EP_VOLTAGE));
	fprintf(fp, "%s,%d\n\n", ::GetStringTable(IDS_TEXT_MAX_FAULT_NO), m_Condition->boardParam.lMaxFaultBattery);

	fprintf(fp, "[Step]\n");
//	fprintf(fp, "StepNo,Type,Mode,VRef,IRef,EndV,EndI,EndTime,EndC,");
	fprintf(fp, "StepNo,Type,Mode,VRef,IRef,End,");
	fprintf(fp, "V Limit,I Limit,C Limit, Imp Limit,");
	for(int a = 0; a<EP_COMP_POINT; a++)
	{
		fprintf(fp, "Comp V%d/Comp t%d,", a+1, a+1);
	}
	fprintf(fp, "\n");
	
	CString strTemp;
	LPEP_STEP_HEADER	lpStepHeader;

	for(int i = 0; i<m_Condition->apStepList.GetSize(); i++)
	{
		lpStepHeader = (EP_STEP_HEADER *)m_Condition->apStepList[i];
		if(lpStepHeader == NULL) break;	
		
		fprintf(fp, "%d,", lpStepHeader->stepIndex+1);			
		fprintf(fp, "%s,", StepTypeMsg(lpStepHeader->type));
		fprintf(fp, "%s,", ModeTypeMsg(lpStepHeader->type, lpStepHeader->mode));

		switch(lpStepHeader->type)
		{
		case EP_TYPE_CHARGE:
		case EP_TYPE_DISCHARGE:
			{
				EP_CHARGE_STEP *pStep = (EP_CHARGE_STEP *)m_Condition->apStepList[i];
				fprintf(fp, "%s,", m_pDoc->ValueString(pStep->lVref, EP_VOLTAGE));
				fprintf(fp, "%s,", m_pDoc->ValueString(pStep->lIref, EP_CURRENT));
				fprintf(fp, "%s,", m_pDoc->StepEndString(pStep, EP_TYPE_CHARGE));

//				fprintf(fp, "%s,", m_pDoc->ValueString(pStep->lEndV, EP_VOLTAGE));
//				fprintf(fp, "%s,", m_pDoc->ValueString(pStep->lEndI, EP_CURRENT));
//				fprintf(fp, "%s,", m_pDoc->ValueString(pStep->ulEndTime, EP_STEP_TIME));
//				fprintf(fp, "%s,", m_pDoc->ValueString(pStep->lEndC, EP_CAPACITY));
				if(pStep->lLimitV != 0 && pStep->lOverV != 0)
				{
					strTemp.Format("%s≤V≤%s", m_pDoc->ValueString(pStep->lLimitV, EP_VOLTAGE), m_pDoc->ValueString(pStep->lOverV, EP_VOLTAGE));
				}
				else if(pStep->lLimitV != 0)
				{
					strTemp.Format("%s≤V", m_pDoc->ValueString(pStep->lLimitV, EP_VOLTAGE));
				}
				else if(pStep->lOverV != 0)
				{
					strTemp.Format("V≤%s", m_pDoc->ValueString(pStep->lOverV, EP_VOLTAGE));
				}
				else 
					strTemp.Empty();
				fprintf(fp, "%s,", strTemp);
//				fprintf(fp, "%s,", m_pDoc->ValueString(pStep->lOverV, EP_VOLTAGE));
//				fprintf(fp, "%s,", m_pDoc->ValueString(pStep->lLimitV, EP_VOLTAGE));

				if(pStep->lLimitI != 0 && pStep->lOverI != 0)
				{
					strTemp.Format("%s≤I≤%s", m_pDoc->ValueString(pStep->lLimitI, EP_CURRENT), m_pDoc->ValueString(pStep->lOverI, EP_CURRENT));
				}
				else if(pStep->lLimitI != 0)
				{
					strTemp.Format("%s≤I", m_pDoc->ValueString(pStep->lLimitI, EP_CURRENT));
				}
				else if(pStep->lOverI != 0)
				{
					strTemp.Format("I≤%s", m_pDoc->ValueString(pStep->lOverI, EP_CURRENT));
				}
				else 
					strTemp.Empty();
				fprintf(fp, "%s,", strTemp);
//				fprintf(fp, "%s,", m_pDoc->ValueString(pStep->lOverI, EP_CURRENT));
//				fprintf(fp, "%s,", m_pDoc->ValueString(pStep->lLimitI, EP_CURRENT));

				if(pStep->lLimitC != 0 && pStep->lOverC != 0)
				{
					strTemp.Format("%s≤C≤%s", m_pDoc->ValueString(pStep->lLimitC, EP_CAPACITY), m_pDoc->ValueString(pStep->lOverC, EP_CAPACITY));
				}
				else if(pStep->lLimitC != 0)
				{
					strTemp.Format("%s≤C", m_pDoc->ValueString(pStep->lLimitC, EP_CAPACITY));
				}
				else if(pStep->lOverC != 0)
				{
					strTemp.Format("C≤%s", m_pDoc->ValueString(pStep->lOverC, EP_CAPACITY));
				}
				else 
					strTemp.Empty();
				fprintf(fp, "%s,", strTemp);
//				fprintf(fp, "%s,", m_pDoc->ValueString(pStep->lOverC, EP_CAPACITY));
//				fprintf(fp, "%s,", m_pDoc->ValueString(pStep->lLimitC, EP_CAPACITY));
				fprintf(fp, ",");
				for(a = 0; a<EP_COMP_POINT; a++)
				{
					fprintf(fp, "%s/%s,", m_pDoc->ValueString(pStep->lCompV[a], EP_VOLTAGE),m_pDoc->ValueString(pStep->lCompTime[a], EP_STEP_TIME)) ;
				}
				fprintf(fp, "\n");
				break;
			}
/*		case EP_TYPE_DISCHARGE:
			{
				EP_DISCHARGE_STEP *pStep = (EP_DISCHARGE_STEP *)m_Condition->apStepList[i];
				fprintf(fp, "%s,", m_pDoc->ValueString(pStep->lVref, EP_VOLTAGE));
				fprintf(fp, "%s,", m_pDoc->ValueString(pStep->lIref, EP_CURRENT));
				fprintf(fp, "%s,", m_pDoc->StepEndString(pStep, EP_TYPE_CHARGE));

//				fprintf(fp, "%s,", m_pDoc->ValueString(pStep->lEndV, EP_VOLTAGE));
//				fprintf(fp, "%s,", m_pDoc->ValueString(pStep->lEndI, EP_CURRENT));
//				fprintf(fp, "%s,", m_pDoc->ValueString(pStep->ulEndTime, EP_STEP_TIME));
//				fprintf(fp, "%s,", m_pDoc->ValueString(pStep->lEndC, EP_CAPACITY));

				fprintf(fp, "%s,", m_pDoc->ValueString(pStep->lOverV, EP_VOLTAGE));
				fprintf(fp, "%s,", m_pDoc->ValueString(pStep->lLimitV, EP_VOLTAGE));
				fprintf(fp, "%s,", m_pDoc->ValueString(pStep->lOverI, EP_CURRENT));
				fprintf(fp, "%s,", m_pDoc->ValueString(pStep->lLimitI, EP_CURRENT));
				fprintf(fp, "%s,", m_pDoc->ValueString(pStep->lOverC, EP_CAPACITY));
				fprintf(fp, "%s,", m_pDoc->ValueString(pStep->lLimitC, EP_CAPACITY));
				fprintf(fp, ",");
				fprintf(fp, ",");
				for(a = 0; a<EP_COMP_POINT; a++)
				{
					fprintf(fp, "%s, %s", m_pDoc->ValueString(pStep->lCompV[a], EP_VOLTAGE),m_pDoc->ValueString(pStep->lCompTime[a], EP_STEP_TIME)) ;
				}
				fprintf(fp, "\n");
				break;
			}
*/
/*		case EP_TYPE_REST:
			{
				EP_REST_STEP *pStep = (EP_REST_STEP *)m_Condition->apStepList[i];
				fprintf(fp, ",,");
//				fprintf(fp, ",", m_pDoc->ValueString(pStep->ulEndTime, EP_STEP_TIME));
				fprintf(fp, "%s,", m_pDoc->StepEndString(pStep, EP_TYPE_REST));
				fprintf(fp, "\n");
				break;
			}
		case EP_TYPE_OCV:
			{
				EP_OCV_STEP *pStep = (EP_OCV_STEP *)m_Condition->apStepList[i];
				fprintf(fp, ",,,");
				if(pStep->lLimitV != 0 && pStep->lOverV != 0)
				{
					strTemp.Format("%s≤V≤%s", m_pDoc->ValueString(pStep->lLimitV, EP_VOLTAGE), m_pDoc->ValueString(pStep->lOverV, EP_VOLTAGE));
				}
				else if(pStep->lLimitV != 0)
				{
					strTemp.Format("%s≤V", m_pDoc->ValueString(pStep->lLimitV, EP_VOLTAGE));
				}
				else if(pStep->lOverV != 0)
				{
					strTemp.Format("V≤%s", m_pDoc->ValueString(pStep->lOverV, EP_VOLTAGE));
				}
				else 
					strTemp.Empty();
				fprintf(fp, "%s,", strTemp);
				fprintf(fp, "\n");
				break;
			}
		case EP_TYPE_IMPEDANCE:
			{
				EP_IMPEDANCE_STEP *pStep = (EP_IMPEDANCE_STEP *)m_Condition->apStepList[i];
				fprintf(fp, ",,,,,,");
//				fprintf(fp, "%s,", m_pDoc->ValueString(pStep->lOverImpedance, EP_IMPEDANCE));
//				fprintf(fp, "%s,", m_pDoc->ValueString(pStep->lLimitImpedance, EP_IMPEDANCE));
				if(pStep->lLimitImpedance != 0 && pStep->lOverImpedance != 0)
				{
					strTemp.Format("%s≤R≤%s", m_pDoc->ValueString(pStep->lLimitImpedance, EP_IMPEDANCE), m_pDoc->ValueString(pStep->lOverImpedance, EP_IMPEDANCE));
				}
				else if(pStep->lLimitImpedance != 0)
				{
					strTemp.Format("%s≤R", m_pDoc->ValueString(pStep->lLimitImpedance, EP_IMPEDANCE));
				}
				else if(pStep->lOverImpedance != 0)
				{
					strTemp.Format("R≤%s", m_pDoc->ValueString(pStep->lOverImpedance, EP_IMPEDANCE));
				}
				else 
					strTemp.Empty();
				fprintf(fp, "\n");
				break;
			}
		case EP_TYPE_END:
		default:
			{
				break;
			}
		}
	}

//	m_Condition->apGradeList.GetSize();
	fclose(fp);	
*/
}

void CDetailTestConditionDlg::OnPrintButton() 
{
	// TODO: Add your control notification handler code here
	 CPrntScreen * ScrCap;
     ScrCap = new CPrntScreen("Impossible to print!","Error!");
     ScrCap->DoPrntScreen(1,2,TRUE);
     delete ScrCap;
     ScrCap = NULL;		


/*	CDC dc;
    CPrintDialog printDlg(FALSE);

    if (printDlg.DoModal() == IDCANCEL)         // Get printer settings from user
        return;

    dc.Attach(printDlg.GetPrinterDC());         // Attach a printer DC
    dc.m_bPrinting = TRUE;

    CString strTitle;                           // Get the application title
	if(dc.m_hDC == NULL)
	{
		strTitle.Format("Device %s not support.", printDlg.GetPortName());
		AfxMessageBox(strTitle, MB_ICONSTOP|MB_OK);
		return;
	}
    strTitle.LoadString(AFX_IDS_APP_TITLE);

    DOCINFO di;                                 // Initialise print document details
    ::ZeroMemory (&di, sizeof (DOCINFO));
    di.cbSize = sizeof (DOCINFO);
    di.lpszDocName = strTitle;
	

    BOOL bPrintingOK = dc.StartDoc(&di);        // Begin a new print job

    // Get the printing extents and store in the m_rectDraw field of a 
    // CPrintInfo object
    CPrintInfo Info;
    Info.m_rectDraw.SetRect(0,0, 
                            dc.GetDeviceCaps(HORZRES), 
							dc.GetDeviceCaps(VERTRES));

	int nLinePerPage = (dc.GetDeviceCaps(VERTRES)-Y_PAGE_MARGIN-BOTTOM_MARGIN) / TEXT_HEIGHT;
	int a = m_pTestCondition->GetTotalStepNo();
		a = ((a+2)*2+15) / nLinePerPage +1;

	Info.SetMaxPage(a);


	Info.m_pPD->m_pd.Flags &= ~PD_NOSELECTION;
	Info.m_pPD->m_pd.hInstance = AfxGetInstanceHandle();

	CFont font;
	font.CreateFont(75, 45, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, HANGUL_CHARSET, 0, 0, 0, VARIABLE_PITCH, GetStringTable(IDS_LANG_TEXT_FONT));
	dc.SelectObject(&font);

    CRect strRect;
	OnBeginPrinting(&dc, &Info);                // Call your "Init printing" funtion

    for (UINT page = Info.GetMinPage(); 
         page <= Info.GetMaxPage() && bPrintingOK; 
         page++)
    {
        dc.StartPage();                         // begin new page
        Info.m_nCurPage = page;
        OnPrint(&dc, &Info);                    // Call your "Print page" function
		strRect.SetRect(dc.GetDeviceCaps(HORZRES)/2, dc.GetDeviceCaps(VERTRES)-TEXT_HEIGHT*3 ,
						dc.GetDeviceCaps(HORZRES)/2+500, dc.GetDeviceCaps(VERTRES)-TEXT_HEIGHT*2);
		strTitle.Format("-%d-", page);
		dc.DrawText(strTitle, strRect, DT_WORDBREAK | DT_EXPANDTABS);
		bPrintingOK = (dc.EndPage() > 0);       // end page
    }
    OnEndPrinting(&dc, &Info);                  // Call your "Clean up" funtion

    if (bPrintingOK)
        dc.EndDoc();                            // end a print job
    else
        dc.AbortDoc();                          // abort job.

    dc.Detach();    
*/	
}

void CDetailTestConditionDlg::OnBeginPrinting(CDC *pDC, CPrintInfo *pInfo)
{

}

void CDetailTestConditionDlg::OnEndPrinting(CDC *pDC, CPrintInfo *pInfo)
{
	
}

void CDetailTestConditionDlg::OnPrint(CDC *pDC, CPrintInfo *pInfo)
{
/*	int nLinePerPage = (pDC->GetDeviceCaps(VERTRES)-Y_PAGE_MARGIN-BOTTOM_MARGIN) / TEXT_HEIGHT;

	int nLineCount = 0;
	int nPageLineCount = 0;
	CString strTemp, strTemp2;
	CString strLineData;
	LPEP_STEP_HEADER	lpStepHeader;
	STR_COMMON_STEP *pStep;
	int offsetPage = pInfo->m_nCurPage-pInfo->GetMinPage();
	
	if(offsetPage*nLinePerPage <= nLineCount &&  nLineCount < (offsetPage+1)*nLinePerPage)
	{
		strTemp.Format("[ %s ]",  ::GetStringTable(IDS_LABEL_PROC_NAME));
		PrintText(strTemp, nPageLineCount++, pDC);
	}
	nLineCount++;


	if(offsetPage*nLinePerPage <= nLineCount &&  nLineCount < (offsetPage+1)*nLinePerPage)
	{
		GetDlgItem(IDC_DETAIL_NAME)->GetWindowText(strTemp2);
		strTemp.Format("%s	: %s", ::GetStringTable(IDS_LABEL_PROC_NAME), strTemp2);
		PrintText(strTemp, nPageLineCount++, pDC);
	}
	nLineCount++;

	if(offsetPage*nLinePerPage <= nLineCount &&  nLineCount < (offsetPage+1)*nLinePerPage)
	{
		GetDlgItem(IDC_DETAIL_CREATOR)->GetWindowText(strTemp2);
		strTemp.Format("%s	: %s", ::GetStringTable(IDS_LABEL_DESCRIPTOR), strTemp2);
		PrintText(strTemp, nPageLineCount++, pDC);
	}
	nLineCount++;

	if(offsetPage*nLinePerPage <= nLineCount &&  nLineCount < (offsetPage+1)*nLinePerPage)
	{
		GetDlgItem(IDC_DETAIL_DATE)->GetWindowText(strTemp2);
		strTemp.Format("%s	: %s", ::GetStringTable(IDS_LABEL_CREATE_DATE), strTemp2);
		PrintText(strTemp, nPageLineCount++, pDC);
	}
	nLineCount++;

	if(offsetPage*nLinePerPage <= nLineCount &&  nLineCount < (offsetPage+1)*nLinePerPage)
	{
		GetDlgItem(IDC_DETAIL_DESC)->GetWindowText(strTemp2);
		strTemp.Format("%s	: %s", ::GetStringTable(IDS_LABEL_SUMMARY), strTemp2);
		PrintText(strTemp, nPageLineCount++, pDC);
	}
	nLineCount++;

	if(offsetPage*nLinePerPage <= nLineCount &&  nLineCount < (offsetPage+1)*nLinePerPage)
	{
		PrintText("", nPageLineCount++, pDC);
	}
	nLineCount++;

	if(offsetPage*nLinePerPage <= nLineCount &&  nLineCount < (offsetPage+1)*nLinePerPage)
	{
		PrintText("[Cell Check Param]", nPageLineCount++, pDC);
	}
	nLineCount++;

	//safety condition
	for(int line =0; line < m_SafetyGrid.GetRowCount(); line++)
	{
		if(offsetPage*nLinePerPage <= nLineCount &&  nLineCount < (offsetPage+1)*nLinePerPage)
		{
			strTemp.Format("%s	: %s",  m_SafetyGrid.GetValueRowCol(line+1, 1), 
										m_SafetyGrid.GetValueRowCol(line+1, 2));
			PrintText(strTemp, nPageLineCount++, pDC);
		}
		nLineCount++;
	}

	//Step 조건 
	if(offsetPage*nLinePerPage <= nLineCount &&  nLineCount < (offsetPage+1)*nLinePerPage)
	{
		PrintText("", nPageLineCount++, pDC);
	}
	nLineCount++;

	if(offsetPage*nLinePerPage <= nLineCount &&  nLineCount < (offsetPage+1)*nLinePerPage)
	{
		PrintText("[Step]", nPageLineCount++, pDC);
	}
	nLineCount++;
	
	if(offsetPage*nLinePerPage <= nLineCount &&  nLineCount < (offsetPage+1)*nLinePerPage)
	{
		PrintText("StepNo\tType\tMode\tVRef\tIRef\tEnd", nPageLineCount++, pDC);
	}
	nLineCount++;

	for(int i = 0; i<m_Condition->apStepList.GetSize(); i++)
	{
		lpStepHeader = (EP_STEP_HEADER *)m_Condition->apStepList[i];
		if(lpStepHeader == NULL) break;	
		pStep = (STR_COMMON_STEP *)m_Condition->apStepList[i];
		
		strLineData.Format("%d\t%s\t%s", 
			lpStepHeader->stepIndex+1,
			StepTypeMsg(lpStepHeader->type),
			ModeTypeMsg(lpStepHeader->type, lpStepHeader->mode)
		);

		switch(lpStepHeader->type)
		{
		case EP_TYPE_CHARGE:
			{
				strTemp.Format("\t%s\t%s\t%s", 
					m_pDoc->ValueString(pStep->fVref, EP_VOLTAGE),
					m_pDoc->ValueString(pStep->fIref, EP_CURRENT),
					m_pDoc->StepEndString(pStep)
				);
				strLineData += strTemp;
				break;
			}
		case EP_TYPE_DISCHARGE:
			{
				strTemp.Format("\t%s\t%s\t%s", 
					m_pDoc->ValueString(pStep->fVref, EP_VOLTAGE),
					m_pDoc->ValueString(pStep->fIref, EP_CURRENT),
					m_pDoc->StepEndString(pStep)
				);
				strLineData += strTemp;
				break;
			}
		case EP_TYPE_REST:
			{
				strTemp.Format("\t\t\t%s", 
					m_pDoc->StepEndString(pStep)
				);
				strLineData += strTemp;
				break;
			}
		case EP_TYPE_OCV:
		case EP_TYPE_IMPEDANCE:
		case EP_TYPE_END:
		default:
			{
				break;
			}
		}
		if(offsetPage*nLinePerPage <= nLineCount &&  nLineCount < (offsetPage+1)*nLinePerPage)
		{
			PrintText(strLineData, nPageLineCount++, pDC);
		}
		nLineCount++;
	}

	//제한 조건 
	if(offsetPage*nLinePerPage <= nLineCount &&  nLineCount < (offsetPage+1)*nLinePerPage)
	{
		PrintText("", nPageLineCount++, pDC);
	}
	nLineCount++;

	if(offsetPage*nLinePerPage <= nLineCount &&  nLineCount < (offsetPage+1)*nLinePerPage)
	{
		PrintText("[Step Limit]", nPageLineCount++, pDC);
	}
	nLineCount++;

	if(offsetPage*nLinePerPage <= nLineCount &&  nLineCount < (offsetPage+1)*nLinePerPage)
	{
		PrintText("StepNo\tV Limit\tI Limit\tC Limit\tImp Limit", nPageLineCount++, pDC);
	}
	nLineCount++;

	for( i = 0; i<m_Condition->apStepList.GetSize(); i++)
	{
		lpStepHeader = (EP_STEP_HEADER *)m_Condition->apStepList[i];
		if(lpStepHeader == NULL) break;	
		pStep = (STR_COMMON_STEP *)m_Condition->apStepList[i];
		
		strLineData.Format("%d", lpStepHeader->stepIndex+1);

		switch(lpStepHeader->type)
		{
		case EP_TYPE_CHARGE:
		case EP_TYPE_DISCHARGE:
			{
				if(pStep->fLimitV != 0 && pStep->fOverV != 0)
				{
					strTemp.Format("\t%s≤V≤%s", m_pDoc->ValueString(pStep->fLimitV, EP_VOLTAGE), m_pDoc->ValueString(pStep->fOverV, EP_VOLTAGE));
				}
				else if(pStep->fLimitV != 0)
				{
					strTemp.Format("\t%s≤V", m_pDoc->ValueString(pStep->fLimitV, EP_VOLTAGE));
				}
				else if(pStep->fOverV != 0)
				{
					strTemp.Format("\tV≤%s", m_pDoc->ValueString(pStep->fOverV, EP_VOLTAGE));
				}
				else 
					strTemp = "\t";
				strLineData += strTemp;

				if(pStep->fLimitI != 0 && pStep->fOverI != 0)
				{
					strTemp.Format("\t%s≤I≤%s", m_pDoc->ValueString(pStep->fLimitI, EP_CURRENT), m_pDoc->ValueString(pStep->fOverI, EP_CURRENT));
				}
				else if(pStep->fLimitI != 0)
				{
					strTemp.Format("\t%s≤I", m_pDoc->ValueString(pStep->fLimitI, EP_CURRENT));
				}
				else if(pStep->fOverI != 0)
				{
					strTemp.Format("\tI≤%s", m_pDoc->ValueString(pStep->fOverI, EP_CURRENT));
				}
				else 
					strTemp = "\t";
				strLineData += strTemp;

				if(pStep->fLimitC != 0 && pStep->fOverC != 0)
				{
					strTemp.Format("\t%s≤C≤%s", m_pDoc->ValueString(pStep->fLimitC, EP_CAPACITY), m_pDoc->ValueString(pStep->fOverC, EP_CAPACITY));
				}
				else if(pStep->fLimitC != 0)
				{
					strTemp.Format("\t%s≤C", m_pDoc->ValueString(pStep->fLimitC, EP_CAPACITY));
				}
				else if(pStep->fOverC != 0)
				{
					strTemp.Format("\tC≤%s", m_pDoc->ValueString(pStep->fOverC, EP_CAPACITY));
				}
				else 
					strTemp = "\t";
				strLineData += strTemp;

				break;
			}
		case EP_TYPE_OCV:
			{
				if(pStep->fLimitV != 0 && pStep->fOverV != 0)
				{
					strTemp.Format("\t%s≤V≤%s", m_pDoc->ValueString(pStep->fLimitV, EP_VOLTAGE), m_pDoc->ValueString(pStep->fOverV, EP_VOLTAGE));
				}
				else if(pStep->fLimitV != 0)
				{
					strTemp.Format("\t%s≤V", m_pDoc->ValueString(pStep->fLimitV, EP_VOLTAGE));
				}
				else if(pStep->fOverV != 0)
				{
					strTemp.Format("\tV≤%s", m_pDoc->ValueString(pStep->fOverV, EP_VOLTAGE));
				}
				else 
					strTemp = "\t";
				strLineData += strTemp;
				break;
			}
		case EP_TYPE_IMPEDANCE:
			{
				if(pStep->fLimitImp != 0 && pStep->fOverImp != 0)
				{
					strTemp.Format("\t%s≤R≤%s", m_pDoc->ValueString(pStep->fLimitImp, EP_IMPEDANCE), m_pDoc->ValueString(pStep->fOverImp, EP_IMPEDANCE));
				}
				else if(pStep->fLimitImp != 0)
				{
					strTemp.Format("\t%s≤R", m_pDoc->ValueString(pStep->fLimitImp, EP_IMPEDANCE));
				}
				else if(pStep->fOverImp != 0)
				{
					strTemp.Format("\tR≤%s", m_pDoc->ValueString(pStep->fOverImp, EP_IMPEDANCE));
				}
				else 
					strTemp = "\t";
				strLineData += strTemp;
				break;
			}
		case EP_TYPE_REST:
		case EP_TYPE_END:
		default:
			{
				break;
			}
		}
		if(offsetPage*nLinePerPage <= nLineCount &&  nLineCount < (offsetPage+1)*nLinePerPage)
		{
			PrintText(strLineData, nPageLineCount++, pDC);
		}
		nLineCount++;
	}
*/
}

void CDetailTestConditionDlg::PrintText(CString text, int nLine, CDC *pDC)
{
	CRect strRect;
	strRect.SetRect(X_PAGE_MARGIN, Y_PAGE_MARGIN + (nLine*TEXT_HEIGHT) ,
						10000, Y_PAGE_MARGIN + (nLine+1)*TEXT_HEIGHT);
		pDC->DrawText(text, strRect, DT_WORDBREAK | DT_EXPANDTABS);
}

void CDetailTestConditionDlg::DisplayCondition()
{
	STR_CONDITION_HEADER *pConditionHeader = m_pTestCondition->GetTestInfo();	
	GetDlgItem(IDC_DETAIL_NAME)->SetWindowText(pConditionHeader->szName);
	GetDlgItem(IDC_DETAIL_DESC)->SetWindowText(pConditionHeader->szDescription);
	GetDlgItem(IDC_DETAIL_CREATOR)->SetWindowText(pConditionHeader->szCreator);
	GetDlgItem(IDC_DETAIL_DATE)->SetWindowText(pConditionHeader->szModifiedTime);

	DisplaySafetyGrid();
	DiaplayStepGrid();

	if(m_StepGrid.GetRowCount()  > 0)
	{
		DisplayFailGrid();
		DisplayGradeGrid();
	}
}

void CDetailTestConditionDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnOK();
}

void CDetailTestConditionDlg::OnEnChangeDetailDate()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}
