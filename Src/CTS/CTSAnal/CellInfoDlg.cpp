// CellInfoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ctsanal.h"
#include "CellInfoDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCellInfoDlg dialog


bool CCellInfoDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCellInfoDlg"), _T("TEXT_CCellInfoDlg_CNT"), _T("TEXT_CCellInfoDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CCellInfoDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCellInfoDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}


CCellInfoDlg::CCellInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCellInfoDlg::IDD, pParent)
{
	LanguageinitMonConfig();

	//{{AFX_DATA_INIT(CCellInfoDlg)
	m_strTrayNo = _T("");
	m_strCellSerial = _T("");
	m_strTitle = TEXT_LANG[0]+TEXT_LANG[1];//"결과 Data를 MES Server로 결과 Data를 전송합니다." //"Cell의 Tray 번호와 Serial 번호를 입력하십시요."
	//}}AFX_DATA_INIT
}
CCellInfoDlg::~CCellInfoDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


void CCellInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCellInfoDlg)
	DDX_Text(pDX, IDC_EDIT_TRAY, m_strTrayNo);
	DDV_MaxChars(pDX, m_strTrayNo, 63);
	DDX_Text(pDX, IDC_EDIT_CELL_SERIAL, m_strCellSerial);
	DDV_MaxChars(pDX, m_strCellSerial, 63);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCellInfoDlg, CDialog)
	//{{AFX_MSG_MAP(CCellInfoDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCellInfoDlg message handlers

void CCellInfoDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData();
	if(m_strTrayNo.IsEmpty())
	{
		AfxMessageBox(TEXT_LANG[2]);//"Tray 번호를 입력하십시요."
		GetDlgItem(IDC_EDIT_TRAY)->SetFocus();
		return;
	}

	m_strTrayNo.MakeUpper();

	if(m_strCellSerial.IsEmpty())
	{
		AfxMessageBox(TEXT_LANG[3]);//"Cell ID 번호를 입력하십시요."
		GetDlgItem(IDC_EDIT_CELL_SERIAL)->SetFocus();
		return;
	}
	
	CDialog::OnOK();
}

BOOL CCellInfoDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	GetDlgItem(IDC_STATIC_TITLE)->SetWindowText(m_strTitle);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
