#if !defined(AFX_FIELDADDDLG_H__1293D846_EB0E_420A_BCCB_CFF8F37E5907__INCLUDED_)
#define AFX_FIELDADDDLG_H__1293D846_EB0E_420A_BCCB_CFF8F37E5907__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FieldAddDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFieldAddDlg dialog

class CFieldAddDlg : public CDialog
{
// Construction
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	BOOL SetDataTypeCombo(int stepType, int dataType = 0);
	STR_MSG_DATA	m_ProcType;
	int m_nDataType;
	CFieldAddDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CFieldAddDlg();

	CPtrArray *m_apProcType;

// Dialog Data
	//{{AFX_DATA(CFieldAddDlg)
	enum { IDD = IDD_FIELD_ADD_DLG };
	CComboBox	m_ctrlDataTypeCombo;
	CComboBox	m_ctrlProcTypeCombo;
	CString	m_strFieldName;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFieldAddDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFieldAddDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnSelchangeCombo1();
	virtual void OnOK();
	afx_msg void OnExtendFormButton();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIELDADDDLG_H__1293D846_EB0E_420A_BCCB_CFF8F37E5907__INCLUDED_)
