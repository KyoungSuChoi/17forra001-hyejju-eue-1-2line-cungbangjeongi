// TabbedWnd.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTabbedWnd window#include "TopView.h"

//#include "ProcesureFormView.h"
//#include "ResultView.h"
#include "ManualControlView.h"
#include "TopView.h"
#include "ConnectionView.h"
#include "OperationView.h"

class CTabbedWnd : public CWnd
{
protected:
#ifdef WIN32
	DECLARE_DYNCREATE(CTabbedWnd)
#else
	DECLARE_DYNAMIC(CTabbedWnd)
#endif		

// Construction
public:
	CTabbedWnd();

// Attributes
public:
//	CSplitterWnd m_wndSplitter;
	SEC3DTabWnd	m_tabWnd;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTabbedWnd)
	//}}AFX_VIRTUAL

// Implementation
public:
	void UpdateGroupState(int nModuleID, int nGroupIndex=0);
	void StopGroupTimer();

	CString *TEXT_LANG;
	bool LanguageinitMonConfig();
	
	virtual ~CTabbedWnd();

	CManualControlView	*m_pManualControlView;
	CTopView			*m_pTopView;			// ��ü Module & Channel Monitoring
	CConnectionView		*m_pConnectionView;
	COperationView		*m_pOperationView;

//	CAllSystemView		*m_pAllSystemView;
//	CProcesureFormView	*m_pConditionView;
//	CResultView			*m_pResultView;
	
	UINT	m_nCurTabIndex;
	UINT	m_nTabIndex_ManualControlView;
	UINT	m_nTabIndex_MonitoringView;
	UINT	m_nTabIndex_ConnectionView;
	UINT	m_nTabIndex_OperationView;

	UINT	m_nTabIndex_ConditionView;
	UINT	m_nTabIndex_DetailView;
	UINT	m_nTabIndex_ResultView;

protected:
	CFont	m_ActiveFont;
	CFont	m_InactiveFont;
	
//	void InitEmbeddedSplitter(CCreateContext* pContext,CWnd* pParent,int cx,int cy);
//	Generated message map functions
protected:
	//{{AFX_MSG(CTabbedWnd)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	afx_msg LRESULT OnTabSelected(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};
