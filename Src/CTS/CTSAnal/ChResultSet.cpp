// ChResultSet.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "ChResultSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChResultSet

IMPLEMENT_DYNAMIC(CChResultSet, CRecordset)

CChResultSet::CChResultSet(CDatabase* pdb)
	: CRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CChResultSet)
	m_Index = 0;
	m_TestSerialNo = _T("");
	memset(m_ch,0,sizeof(m_ch));
	m_dataType = 0;
	m_ProcedureID = 0;
	m_nFields = 132;
	//}}AFX_FIELD_INIT
	m_nDefaultType = snapshot;
}


CString CChResultSet::GetDefaultConnect()
{
	return _T("ODBC;DSN=CTSProduct");
}

CString CChResultSet::GetDefaultSQL()
{
	return _T("[ChResult]");
}

void CChResultSet::DoFieldExchange(CFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CChResultSet)
	CString   strTemp;
	pFX->SetFieldType(CFieldExchange::outputColumn);
	RFX_Long(pFX, _T("[Index]"), m_Index);
	RFX_Text(pFX, _T("[TestSerialNo]"), m_TestSerialNo);
	RFX_Long(pFX, _T("[ProcedureID]"), m_ProcedureID);
	for(int i = 0; i < 128; i++)
	{
		strTemp.Format("[ch%d]", i+1);
    	RFX_Long(pFX, _T(strTemp),m_ch[i]);
	}
	RFX_Long(pFX, _T("[dataType]"), m_dataType);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CChResultSet diagnostics

#ifdef _DEBUG
void CChResultSet::AssertValid() const
{
	CRecordset::AssertValid();
}

void CChResultSet::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG
