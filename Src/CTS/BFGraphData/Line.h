/*************************************************************
	Copyrighted by LG Production engineering Research Center

	File Name: Line.h

	Written out by 
	Evaluation Thechnology Group
	Kim, Sung-Hoon

	Comment: CLine class is defined

	Revision History:
	Version   Name            Date        Reason
	    1.0   Kim, Sung-Hoon  2001.06.01  Create
*************************************************************/

// Line.h: interface for the CLine class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LINE_H__40068521_EC8D_11D4_88DF_006008CEDA07__INCLUDED_)
#define AFX_LINE_H__40068521_EC8D_11D4_88DF_006008CEDA07__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// #include

//class CTable;
class CAxis;

// #define

// Class definition

//---------------------------------------------------------------------------
//	Class Name: CLine
//
//	Comment:
//
//	Revision History:
//	Version   Name            Date        Reason
//	    1.0   Kim, Sung-Hoon  2001.06.01  Create
//---------------------------------------------------------------------------
class AFX_EXT_CLASS CLine  
{
////////////////
// Attributes //
////////////////
private:
	BOOL						m_bSelect;
	CString                    m_strChPath;
	LONG                       m_lCycle;
	DWORD                      m_dwMode;
	CList<fltPoint, fltPoint&> m_DataList;
	COLORREF                   m_Color;
	int                        m_Width;
	int                        m_LineType;
	CString                    m_strAxisName;
	BOOL					m_bDataPointCheck;
	BOOL					m_bInterLine;
	BOOL					m_nShowLine;

//	fltPoint					*m_pData;

/////////////////////////////////////////////
// Operations: "Constructor and destructor //
/////////////////////////////////////////////
public:
	CLine(LPCTSTR strChPath, LONG lCycle, DWORD dwMode, COLORREF color, LPCTSTR strAxisName);
	virtual ~CLine();

////////////////////////////////////////////////////////
// Operations: "Functions to access member variables" //
////////////////////////////////////////////////////////
public:
	void	ShowLine(BOOL bShow = TRUE)				{	m_nShowLine = bShow;	}			//20090911 KBH
	BOOL	GetShow()								{	return m_nShowLine;		}			//20090911 KBH

	void	RemoveAllData()							{	m_DataList.RemoveAll();	}
	void	SetDrawLine(BOOL bDraw = TRUE)			{	m_bInterLine = bDraw;	}
	BOOL	IsDrawLine()							{	return m_bInterLine;	}
	void	SetMarkDataPoint(BOOL bMark = TRUE)		{	m_bDataPointCheck = bMark;	};
	BOOL	IsDataPointMark()		{	return m_bDataPointCheck;	}
	void	SetSelect(BOOL bSelect = TRUE)	{	m_bSelect = bSelect;	};
	BOOL	GetSelect()		{	return m_bSelect;	};
	CString  GetChPath() { return m_strChPath; };
	LONG     GetCycle()  { return m_lCycle; };
	DWORD    GetMode()   { return m_dwMode; };
	void     AddData(fltPoint pt) { m_DataList.AddTail(pt); };
	int      GetDataCount() { return m_DataList.GetCount(); };
	POSITION GetDataHead() { return m_DataList.GetHeadPosition(); };
	fltPoint GetNextData(POSITION& pos) { return m_DataList.GetNext(pos); };
	POSITION FindDataIndex(int iPos) { return m_DataList.FindIndex(iPos); };
	fltPoint GetDataAt(POSITION pos) { return m_DataList.GetAt(pos); };
	COLORREF GetColor() { return m_Color; };
	void     SetColor(COLORREF color) { m_Color=color; };
	int      GetWidth() { return m_Width; };
	void     SetWidth(int width) { m_Width=width; }
	int      GetType() { return m_LineType; };
	void     SetType(int type) { m_LineType=type; }
	BOOL     GetMinMaxValue(fltPoint& MinValue, fltPoint& MaxValue, BOOL bInit);
	CString  GetName();
	int      GetApproximatePos(fltPoint value);
	CString  GetAxisName() { return m_strAxisName; };
};

#endif // !defined(AFX_LINE_H__40068521_EC8D_11D4_88DF_006008CEDA07__INCLUDED_)
