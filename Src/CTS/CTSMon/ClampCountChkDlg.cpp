// ClampCountChkDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "ClampCountChkDlg.h"
#include "MainFrm.h"

// CClampCountChkDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CClampCountChkDlg, CDialog)

CClampCountChkDlg::CClampCountChkDlg(CCTSMonDoc *pDoc, CWnd* pParent /*=NULL*/)
	: CDialog(CClampCountChkDlg::IDD, pParent)
{
	m_pDoc = pDoc;

	m_nModuleID = 1;
	m_nMaxStageCnt = 0;

	LanguageinitMonConfig();
}

CClampCountChkDlg::~CClampCountChkDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

bool CClampCountChkDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CClampCountChkDlg"), _T("TEXT_CClampCountChkDlg_CNT"), _T("TEXT_CClampCountChkDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CClampCountChkDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CClampCountChkDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

void CClampCountChkDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);	
	DDX_Control(pDX, ID_DLG_LABEL_NAME, m_LabelName);
	DDX_Control(pDX, IDC_BTN_UPDATE, m_BtnUpdate);
	DDX_Control(pDX, IDOK, m_BtnClose);
	DDX_Control(pDX, IDC_CSV_OUTPUT, m_BtnCSVConvert);
	DDX_Control(pDX, IDC_UNIT_SEL_COMBO, m_ctrlMDSelCombo);
	DDX_Control(pDX, IDC_SELECT_ALL_OK_BTN, m_BtnSelectAllOk);
	DDX_Control(pDX, IDC_SELECT_ALL_OFF_BTN, m_BtnSelectAllOff);
	DDX_Control(pDX, IDC_SELECT_CHANNEL_INIT_BTN, m_BtnSelectChannelInit);
}


BEGIN_MESSAGE_MAP(CClampCountChkDlg, CDialog)
	ON_BN_CLICKED(IDC_BTN_UPDATE, &CClampCountChkDlg::OnBnClickedBtnUpdate)
	ON_BN_CLICKED(IDOK, &CClampCountChkDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_CSV_OUTPUT, &CClampCountChkDlg::OnBnClickedCsvOutput)
	ON_BN_CLICKED(IDC_SELECT_CHANNEL_INIT_BTN, &CClampCountChkDlg::OnBnClickedSelectChannelInitBtn)
	ON_CBN_SELCHANGE(IDC_UNIT_SEL_COMBO, &CClampCountChkDlg::OnCbnSelchangeUnitSelCombo)
	ON_BN_CLICKED(IDC_SELECT_ALL_OK_BTN, &CClampCountChkDlg::OnBnClickedSelectAllOkBtn)
	ON_BN_CLICKED(IDC_SELECT_ALL_OFF_BTN, &CClampCountChkDlg::OnBnClickedSelectAllOffBtn)
END_MESSAGE_MAP()


// CClampCountChkDlg 메시지 처리기입니다.
void CClampCountChkDlg::fnUpdateCnt()
{
	BeginWaitCursor();

	LPVOID	pBuffer = NULL;

	CString strData = _T("");

	int i=0;
	int nSize = 0;

	nSize = sizeof( EP_GRIPPER_CLAMPING_RES );

	pBuffer = EPSendDataCmd( m_nModuleID, 0, 0, EP_CMD_GRIPPER_CLAMPING_REQUEST, nSize );

	if( pBuffer != NULL )
	{
		EP_GRIPPER_CLAMPING_RES recvData;
		ZeroMemory( &recvData, sizeof(EP_GRIPPER_CLAMPING_RES));
		memcpy(&recvData, pBuffer, sizeof(EP_GRIPPER_CLAMPING_RES));

		for(i=1; i<=m_wndChGrid.GetRowCount(); i++ )
		{
			strData.Format("%d", recvData.clampingCnt[i-1]);
			m_wndChGrid.SetValueRange(CGXRange(i,2), strData);
		}		
	}

	EndWaitCursor();
}

void CClampCountChkDlg::OnBnClickedBtnUpdate()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	fnUpdateCnt();
}

void CClampCountChkDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.	
	OnOK();
}

BOOL CClampCountChkDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	CenterWindow();

	m_nMaxStageCnt = m_pDoc->GetInstalledModuleNum();

	InitFont();
	InitLabel();
	InitGridWnd();
	InitColorBtn();

	int i=0;
	int nModuleID = 1;	
	int nDefaultIndex = 0;

	for( i=0; i<m_nMaxStageCnt; i++ )
	{
		nModuleID = EPGetModuleID(i);
		if( nModuleID == m_nModuleID )
		{
			nDefaultIndex = i;
		}
		m_ctrlMDSelCombo.AddString(::GetModuleName(nModuleID));
		m_ctrlMDSelCombo.SetItemData(i, nModuleID);
	}

	m_ctrlMDSelCombo.SetCurSel(nDefaultIndex);	

	ReDrawGrid();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CClampCountChkDlg::InitFont()
{	
	LOGFONT LogFont;

	GetDlgItem(IDC_STATIC1)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight = 1000;
	LogFont.lfHeight = 18;

	m_Font.CreateFontIndirect( &LogFont );

	GetDlgItem(IDC_STATIC1)->SetFont(&m_Font);
	GetDlgItem(IDC_UNIT_SEL_COMBO)->SetFont(&m_Font);
}

void CClampCountChkDlg::InitLabel()
{
	m_LabelName.SetBkColor(RGB_MIDNIGHTBLUE);
	m_LabelName.SetTextColor(RGB_WHITE);
	m_LabelName.SetFontSize(24);
	m_LabelName.SetFontBold(TRUE);
	m_LabelName.SetText("CLAMP Count Viewer");
}

void CClampCountChkDlg::InitColorBtn()
{	
	m_BtnUpdate.SetFontStyle(15, 1);
	m_BtnUpdate.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);

	m_BtnSelectAllOk.SetFontStyle(15, 1);
	m_BtnSelectAllOk.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	m_BtnSelectAllOff.SetFontStyle(15, 1);
	m_BtnSelectAllOff.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	m_BtnSelectChannelInit.SetFontStyle(15, 1);
	m_BtnSelectChannelInit.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	

	m_BtnCSVConvert.SetFontStyle(15, 1);
	m_BtnCSVConvert.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	

	m_BtnClose.SetFontStyle(15, 1);
	m_BtnClose.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
}


void CClampCountChkDlg::InitGridWnd()
{
	ASSERT(m_pDoc);

	CRect rectGrid;
	float width;

	m_wndChGrid.SubclassDlgItem(IDC_CHNNEL_GRID, this);
	m_wndChGrid.m_bSameColSize  = FALSE;		
	m_wndChGrid.m_bSameRowSize  = TRUE;
	m_wndChGrid.m_bCustomWidth  = TRUE;

	m_wndChGrid.Initialize();
	
	BOOL bLock = m_wndChGrid.LockUpdate();

	m_wndChGrid.SetRowCount(0);
	m_wndChGrid.SetColCount(3);
	m_wndChGrid.SetDefaultRowHeight(24);

	m_wndChGrid.SetDrawingTechnique(gxDrawUsingMemDC);

	m_wndChGrid.SetValueRange(CGXRange(0,1), "Channel");
	m_wndChGrid.SetValueRange(CGXRange(0,2), "Count");	
	m_wndChGrid.SetValueRange(CGXRange(0,3), "Select");	

	m_wndChGrid.GetClientRect(rectGrid);
	width = (float)rectGrid.Width()/100.0f;

	m_wndChGrid.m_nWidth[1]	= int(width* 30.0f);
	m_wndChGrid.m_nWidth[2]	= int(width* 50.0f);
	m_wndChGrid.m_nWidth[3]	= int(width* 10.0f);

	m_wndChGrid.SetStyleRange(CGXRange().SetCols(3), 
		CGXStyle()
		.SetControl(GX_IDS_CTRL_CHECKBOX3D)
		);

	m_wndChGrid.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, 
		CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));

	m_wndChGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	m_wndChGrid.SetStyleRange(CGXRange().SetCols(1),
		CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(255,255,255)).SetInterior(RGB(192,192,192)));
	//Table setting
	m_wndChGrid.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(14).SetBold(TRUE).SetFaceName(GetStringTable(IDS_LANG_TEXT_FONT))));

	m_wndChGrid.LockUpdate(bLock);
	m_wndChGrid.Redraw();
}

void CClampCountChkDlg::ReDrawGrid()
{
	CFormModule *pModule = m_pDoc->GetModuleInfo(m_nModuleID);	
	if(pModule == NULL)
	{
		return;
	}

	int nTrayChSum = 0;
	int t=0;
	for(t=0; t<pModule->GetTotalJig(); t++)
	{
		nTrayChSum += pModule->GetChInJig(t);
	}

	if(nTrayChSum <=0)	
	{
		return;
	}

	int i = 0;

	BOOL bLock = m_wndChGrid.LockUpdate();

	for( i=1; i<=m_wndChGrid.GetRowCount(); i++ )
	{
		m_wndChGrid.SetValueRange(CGXRange(i, 2), "");
		m_wndChGrid.SetValueRange(CGXRange(i, 3), "");
	}

	if(m_wndChGrid.GetRowCount() != nTrayChSum)
	{
		m_wndChGrid.SetRowCount(nTrayChSum);
		for( i=1; i<=m_wndChGrid.GetRowCount(); i++ )
		{
			m_wndChGrid.SetValueRange(CGXRange(i, 1), (long)i);
		}
	}

	m_wndChGrid.LockUpdate(bLock);
	m_wndChGrid.Redraw();
}

void CClampCountChkDlg::OnBnClickedCsvOutput()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strTime, strData;	

	COleDateTime nowtime(COleDateTime::GetCurrentTime());
	
	strTime.Format("CLAMPINFO_STAGE%d_%d%02d%02d", m_nModuleID, nowtime.GetYear(), nowtime.GetMonth(), nowtime.GetDay());	

	CStdioFile	file;
	CString		strFileName;		

	int i=0;
	int j=0;

	CFileDialog dlg(false, "", strTime, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, TEXT_LANG[0]);//"csv 파일(*.csv)|*.csv|All Files (*.*)|*.*|"

	if(dlg.DoModal() == IDOK)
	{		
		strFileName = dlg.GetPathName();		
		file.Open(strFileName, CFile::modeCreate|CFile::modeReadWrite|CFile::modeNoTruncate);

		int nRowCnt = m_wndChGrid.GetRowCount();
		int nColCnt = m_wndChGrid.GetColCount();

		for( i=0; i<=nRowCnt; i++ )
		{
			for( j=1; j<nColCnt; j++ )
			{
				if( j==1 )
				{
					strData = m_wndChGrid.GetValueRowCol(i, j);
				}
				else
				{
					strData = strData + "," + m_wndChGrid.GetValueRowCol(i, j);
				}
			}
			file.WriteString(strData+"\n");
		}		
		file.Close();
	}
}

void CClampCountChkDlg::OnBnClickedSelectChannelInitBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strTemp=_T("");
	int i=0;
	int nRtn=0;
	int nCnt=0;

	EP_GRIPPER_CLAMPING_INIT cmd;
	ZeroMemory(&cmd, sizeof(EP_GRIPPER_CLAMPING_INIT));

	for( i=1; i<=m_wndChGrid.GetRowCount(); i++ )
	{
		if( m_wndChGrid.GetValueRowCol(i,3) == "1" )
		{
			nCnt++;
			cmd.set_ch[i-1] = 1;
		}
	}

	strTemp.Format(TEXT_LANG[1], nCnt);//"선택된 채널의 Gripper Clamping 누적 횟수를 초기화 하시겠습니까?\n [총: %d]"

	if( MessageBox(strTemp,_T(TEXT_LANG[2]),MB_ICONQUESTION|MB_YESNO) == IDYES )//"초기화 확인"
	{	
		nRtn = EPSendCommand( m_nModuleID, 0, 0, EP_CMD_SET_GRIPPER_CLAMPING_INIT, &cmd, sizeof(EP_GRIPPER_CLAMPING_INIT) );
		if( nRtn !=  EP_ACK)
		{	
			strTemp.Format(_T(TEXT_LANG[3]));//"명령 전송을 실패 하였습니다."
			((CMainFrame *)AfxGetMainWnd())->ShowInformation( m_nModuleID, strTemp, INFO_TYPE_FAULT);
		}
		else
		{
			strTemp.Format(_T(TEXT_LANG[4]));//"명령을 전송 하였습니다."
			((CMainFrame *)AfxGetMainWnd())->ShowInformation( m_nModuleID, strTemp, INFO_TYPE_NORMAL);

			fnUpdateCnt();
		}
	}
}

void CClampCountChkDlg::OnCbnSelchangeUnitSelCombo()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int nCurSel = m_ctrlMDSelCombo.GetCurSel();
	if(nCurSel == LB_ERR)
	{
		return;
	}

	if( m_nModuleID != m_ctrlMDSelCombo.GetItemData(nCurSel) )
	{
		m_nModuleID = m_ctrlMDSelCombo.GetItemData(nCurSel);
		ReDrawGrid();
	}
}

void CClampCountChkDlg::OnBnClickedSelectAllOkBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int i=0;
	for( i=1; i<=m_wndChGrid.GetRowCount(); i++ )
	{
		m_wndChGrid.SetValueRange( CGXRange(i,3), "1");
	}
}

void CClampCountChkDlg::OnBnClickedSelectAllOffBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int i=0;
	for( i=1; i<=m_wndChGrid.GetRowCount(); i++ )
	{
		m_wndChGrid.SetValueRange( CGXRange(i,3), "0");
	}
}
