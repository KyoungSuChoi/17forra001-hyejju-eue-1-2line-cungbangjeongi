// PSCommon.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include <afxdllx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


static AFX_EXTENSION_MODULE PSCommonDLL = { NULL, NULL };
BOOL LoadCodeTable();

extern "C" int APIENTRY
DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		TRACE0("PSCOMMON.DLL Initializing!\n");
		
		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(PSCommonDLL, hInstance))
			return 0;

		// Insert this DLL into the resource chain
		// NOTE: If this Extension DLL is being implicitly linked to by
		//  an MFC Regular DLL (such as an ActiveX Control)
		//  instead of an MFC application, then you will want to
		//  remove this line from DllMain and put it in a separate
		//  function exported from this Extension DLL.  The Regular DLL
		//  that uses this Extension DLL should then explicitly call that
		//  function to initialize this Extension DLL.  Otherwise,
		//  the CDynLinkLibrary object will not be attached to the
		//  Regular DLL's resource chain, and serious problems will
		//  result.

		new CDynLinkLibrary(PSCommonDLL);

		PSGetColorCfgData();	//Load color cfg;

	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		TRACE0("PSCOMMON.DLL Terminating!\n");
		// Terminate the library before destructors are called
		AfxTermExtensionModule(PSCommonDLL);
	}
	return 1;   // ok
}

#include "TopMonitoringConfigDlg.h"
#define COMMON_CONFIG_FILE	"CommConfig.cfg"

typedef struct Code_Information {
	BYTE code;
	char szMsg[32];
	char szDescript[127];
} CODE_INFO;

CList < CODE_INFO, CODE_INFO&>	m_gCodeInfoList;

BOOL bFirstLoaded = FALSE;
PS_COLOR_CONFIG g_strTopConfig;


BOOL LoadCodeTable()
{
	bFirstLoaded = TRUE;

	CDaoDatabase  db;

	ASSERT(m_gCodeInfoList.GetCount() == 0);

	// Get Current Directory Path
	TCHAR szBuff[_MAX_PATH];
	::GetModuleFileName(NULL,szBuff,_MAX_PATH);
	CString strPath = CString(szBuff).Mid(0, CString(szBuff).ReverseFind('\\'))+"\\DataBase\\"+PS_SCHEDULE_DATABASE_NAME;
	
	try
	{
		db.Open(strPath);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	
	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		CString strSQL;		
		strSQL.Format("SELECT Code, Messge, Description FROM ChannelCode");
	
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	
	if(!rs.IsBOF() && !rs.IsEOF())
	{
		COleVariant data;
		CODE_INFO codeInfo;

		while(!rs.IsEOF())
		{
			ZeroMemory(&codeInfo, sizeof(CODE_INFO));
			data = rs.GetFieldValue(0);
			codeInfo.code = (BYTE)data.lVal;
			
			data = rs.GetFieldValue(1);
			if(VT_NULL != data.vt)
			{
				sprintf(codeInfo.szMsg, "%s", data.pbVal);
			}
			
			data = rs.GetFieldValue(2);			
			if(VT_NULL != data.vt)
			{
				sprintf(codeInfo.szDescript, "%s", data.pbVal);
			}

			m_gCodeInfoList.AddTail(codeInfo);
			rs.MoveNext();
		}
	
	}

	rs.Close();
	db.Close();

	return TRUE;
}


//공통 설정값을 Loading한다.
BOOL	WriteColorCfgData()
{
	FILE *fp = fopen(COMMON_CONFIG_FILE, "rb+");
	if(fp != NULL)
	{
		fwrite(&g_strTopConfig, sizeof(PS_COLOR_CONFIG), 1, fp);			//save current setting
		fclose(fp);
	}
	else
	{
		return FALSE;
	}
	return TRUE;
}

//PS_COLOR_CONFIG에서 해당 상태의 Index를 구한다.
BYTE GetStateColorIndex(WORD state, WORD type)
{
	BYTE	colorFlag = 0;
	switch (state)
	{
		//Module State
		case PS_STATE_IDLE			:			colorFlag = 0;	break;
		case PS_STATE_STANDBY		:			colorFlag = 2;	break;
		case PS_STATE_PAUSE		:				colorFlag = 4;	break;
		case PS_STATE_MAINTENANCE	:			colorFlag = 5;	break;
		case PS_STATE_RUN			:
			{
				switch(type)
				{
					case PS_STEP_CHARGE		:			colorFlag = 11;	break;
					case PS_STEP_DISCHARGE	:			colorFlag = 12;	break;
					case PS_STEP_OCV		:			colorFlag = 10;	break;
					case PS_STEP_REST		:			colorFlag = 13;	break;
					case PS_STEP_IMPEDANCE	:			colorFlag = 14;	break;
					case PS_STEP_END		:			colorFlag = 7;	break;
					case PS_STEP_ADV_CYCLE	:			colorFlag = 3;	break;
					case PS_STEP_LOOP		:			colorFlag = 3;	break;					
					case PS_STEP_BALANCE	:			colorFlag = 3;	break;
					default					:			colorFlag = 3;	break;
				}
			}
			break;

		//PC Part
		case PS_STATE_SELF_TEST	:			colorFlag = 5;	break;
		case PS_STATE_FAIL		:			colorFlag = 8;	break;
		case PS_STATE_CHECK		:			colorFlag = 3;	break;
		case PS_STATE_STOP		:			colorFlag = 4;	break;
		case PS_STATE_END		:			colorFlag = 7;	break;
		case PS_STATE_FAULT		:			colorFlag = 8;	break;
		case PS_STATE_READY		:			colorFlag = 1;	break;
		default					:			colorFlag = 0;	break;
	}
	return colorFlag;
}

PS_DLL_API LPCTSTR PSGetStateMsg(WORD State, WORD type)
{
	switch(type)
	{
//	case PS_STEP_PATTERN : return "Pattern";
	case PS_STEP_BALANCE : return "Balance";
	}

	BYTE index = GetStateColorIndex(State, type);	
	ASSERT(index < PS_MAX_STATE_COLOR);
	return g_strTopConfig.stateConfig[index].szMsg;
}

PS_DLL_API LPCTSTR PSGetTypeMsg(WORD type)
{
	switch ( type )
	{
	case PS_STEP_ADV_CYCLE	:	return "Cycle";
	case PS_STEP_LOOP		:	return "Loop";
//	case PS_STEP_PATTERN	:	return "Pattern";
	case PS_STEP_BALANCE	:	return "Balance";
	}
	
	BYTE index = GetStateColorIndex(PS_STATE_RUN, type);
	
	ASSERT(index < PS_MAX_STATE_COLOR);
	
	return g_strTopConfig.stateConfig[index].szMsg;
}

PS_DLL_API void PSGetStateColor(WORD state, WORD type, COLORREF &textColor, COLORREF &bkColor)
{
	BYTE colorIndex = GetStateColorIndex(state, type);
	ASSERT(colorIndex < PS_MAX_STATE_COLOR);

	textColor = g_strTopConfig.stateConfig[colorIndex].TStateColor;
	bkColor = g_strTopConfig.stateConfig[colorIndex].BStateColor;
}

PS_DLL_API void PSGetTypeColor(WORD type, COLORREF &textColor, COLORREF &bkColor)
{
	BYTE colorIndex = GetStateColorIndex(PS_STATE_RUN, type);
	ASSERT(colorIndex < PS_MAX_STATE_COLOR);

	textColor = g_strTopConfig.stateConfig[colorIndex].TStateColor;
	bkColor = g_strTopConfig.stateConfig[colorIndex].BStateColor;
}

PS_DLL_API void PSCellCodeMsg(BYTE code, CString &strMsg, CString &strDescript)
{
	//최초 한번만 호출 하도록 한다.
	//실패가 되던 성공하던 한번만 호출 (실패시 계속 호출하지 않는다.)
	if(!bFirstLoaded)	LoadCodeTable();	

	CODE_INFO codeInfo;
	strMsg.Format("%d", code);		//default string
	
	POSITION pos = m_gCodeInfoList.GetHeadPosition();
	while(pos)
	{
		codeInfo = (CODE_INFO)m_gCodeInfoList.GetNext(pos);
//		TRACE("%d,%s,%s\n", codeInfo.code, codeInfo.szMsg, codeInfo.szDescript);

		if(codeInfo.code == code)
		{
#if _DEBUG
			strMsg.Format("%s(%d)", codeInfo.szMsg, code);
#else
			strMsg.Format("%s", codeInfo.szMsg);
#endif
			strDescript.Format("%s", codeInfo.szDescript);
			break;
		} 
	}
}

//////////////////////////////////////////////////////////////////////////
//공통 설정값을 저장한다. 
PS_DLL_API PS_COLOR_CONFIG*  PSGetColorCfgData(BOOL bShowEditDlg)
{
	memset(&g_strTopConfig, 0, sizeof(PS_COLOR_CONFIG));

	CTopMonitoringConfigDlg *pDlg;

	FILE *fp = fopen(COMMON_CONFIG_FILE, "rb");
	if(fp != NULL)
	{
		fread(&g_strTopConfig, sizeof(PS_COLOR_CONFIG), 1, fp);		//Current Config
		fclose(fp);
	}
	else
	{
		pDlg = new CTopMonitoringConfigDlg();
		g_strTopConfig = pDlg->GetDefaultColorConfig();
		delete pDlg;
	}
	
	//Show color edit dialog
	if(bShowEditDlg)
	{
		pDlg = new CTopMonitoringConfigDlg();
		pDlg->SetColorCfgData(g_strTopConfig);
		if(pDlg->DoModal() == IDOK)
		{
			g_strTopConfig = pDlg->GetColorConfigData();
			WriteColorCfgData();
		}
		delete pDlg;
	}

	return &g_strTopConfig;
}


PS_DLL_API BOOL PSIsCellOk(BYTE code)
{
	if(code == PS_CODE_NORMAL || ( code >= PS_NORMAL_CODE_LOW && code <= PS_NORMAL_CODE_HIGH))	return TRUE;
	
	return FALSE;
}

PS_DLL_API BOOL PSIsCellBad(BYTE code)
{
	if(code == PS_CODE_CELL_FAIL || ( code >= PS_CELL_FAIL_CODE_LOW && code <= PS_CELL_FAIL_CODE_HIGH))	return TRUE;
	
	return FALSE;
}

PS_DLL_API BOOL PSIsNonCell(BYTE code)
{
	return code == PS_CODE_NONCELL ? TRUE : FALSE;
}

PS_DLL_API BOOL PSIsSysFail(BYTE code)
{
	if(code == PS_CODE_SYS_ERROR || ( code >= PS_SYS_FAIL_CODE_LOW && code <= PS_SYS_FAIL_CODE_HIGH))	return TRUE;
	
	return FALSE;

}

PS_DLL_API BOOL PSIsCellCheckFail(BYTE code)
{
	if(code == PS_CODE_CELL_CHECK_FAIL || ( code >= PS_NONCELL_CODE_LOW && code <= PS_NONCELL_CODE_HIGH))	return TRUE;
	
	return FALSE;
}

PS_DLL_API LPCTSTR PSGetItemName(int nCode)
{
	switch(nCode)
	{
	case		PS_STATE			:	return "State";
	case		PS_VOLTAGE			:	return "Voltage";
	case		PS_CURRENT			:	return "Current";
	case		PS_CAPACITY			:	return "Capacity";
	case		PS_IMPEDANCE		:	return "DCIR";
	case		PS_CODE				:	return "Code";
	case		PS_STEP_TIME		:	return "StepTime";
	case		PS_TOT_TIME			:	return "TotTime";
	case		PS_GRADE_CODE		:	return "Grade";
	case		PS_STEP_NO			:	return "StepNo";
#ifdef _EDLC_TEST_SYSTEM
	case		PS_WATT				:	return "Farad";		//20081208 KHS  Power => Farad로 변경
#else
	case		PS_WATT				:	return "Power";
#endif
	case		PS_WATT_HOUR		:	return "wattHour";
	case		PS_TEMPERATURE		:	return "Temp";
	case		PS_PRESSURE			:	return "Press";
	case		PS_STEP_TYPE		:	return "Type";
	case		PS_CUR_CYCLE		:	return "CurCycle";
	case		PS_TOT_CYCLE		:	return "TotCycle";
	case		PS_TEST_NAME		:	return "TestName";
	case		PS_SCHEDULE_NAME	:	return "Schedule";
	case		PS_CHANNEL_NO		:	return "Channel";
	case		PS_MODULE_NO		:	return "Module";
	case		PS_LOT_NO			:	return "Serial";
	case		PS_DATA_SEQ			:	return "DataSequence";		//0x16
	case		PS_AVG_CURRENT		:	return	"Avg. Crt";			//0x17
	case		PS_AVG_VOLTAGE		:	return	"Avg. Vtg";			//0x18
	case		PS_CAPACITY_SUM		:	return	"Capa. Sum";		//0x19
	case		PS_CHARGE_CAP		:	return	"Char. Cap.";		//0x1A
	case		PS_DISCHARGE_CAP	:	return	"Dischar. Cap.";	//0x1B
	case		PS_METER_DATA		:	return	"Meter";
	case		PS_START_TIME		:	return	"Start Time";		//0x1d
	case		PS_END_TIME			:	return	"End Time";			//0x1f
	case		PS_SHARING_INFO		:	return	"SharingInfo";
	case		PS_GOTO_COUNT		:	return	"Goto Count";		//0x20  //20081202 KHS
	case		PS_WATTHOUR_SUM		:	return	"WattHour. Sum";		// 2009_02_12 kky
	case		PS_CHAR_WATTHOUR	:	return	"Char. WattHour";		// 2009_02_12 kky
	case		PS_DISCHAR_WATTHOUR	:	return	"DisChar. WattHour";	// 2009_02_12 kky
	default							:	return "";
	}
}