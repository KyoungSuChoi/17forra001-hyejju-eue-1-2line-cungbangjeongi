//----------------------------------------------------------------------------
// N O L D U S   I N F O R M A T I O N   T E C H N O L O G Y   B . V .
//----------------------------------------------------------------------------
// Filename:      BitmapPickerCombo.cpp
// Project:       EthoVision
// Module:        BitmapPicker
// Programmer:    Anneke Sicherer-Roetman
// Version:       1.00
// Revision Date: 06-10-1999
//----------------------------------------------------------------------------
// Description:   Definition of class CBitmapPickerCombo
//                See CBitmapPickerCombo.h
//----------------------------------------------------------------------------
// Acknowledgements: based on Joel Wahlberg's CIconComboBox
//                  (joel.wahlbergTEXT_LANG[@]enator.se)
//----------------------------------------------------------------------------
// Revision history:
// 06-10-1999 - First implementation
//----------------------------------------------------------------------------
// Bugs: ........
//----------------------------------------------------------------------------
// TEXT_LANG[@]doc
//----------------------------------------------------------------------------

#include "stdafx.h"
#include "BitmapPickerCombo.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
                   
//----------------------------------------------------------------------------
// Function DrawBitmap
// TEXT_LANG[@]func    draws bitmap at specified point in specified device context
// TEXT_LANG[@]rdesc   nothing
// TEXT_LANG[@]parm    const CBitmap | *bitmap | bitmap to draw
// TEXT_LANG[@]parm    const CDC     | *pDC    | device context to draw in
// TEXT_LANG[@]parm    const CPoint  | &point  | top left point of bitmap
//----------------------------------------------------------------------------
// TEXT_LANG[@]prog 
// Anneke Sicherer-Roetman
// TEXT_LANG[@]revs 
// 06-10-1999 - First implementation
//----------------------------------------------------------------------------
// TEXT_LANG[@]todo 
//----------------------------------------------------------------------------
static void DrawBitmap(const CBitmap *bitmap, const CDC *pDC, const CPoint &point)
{
  BITMAP bm; ((CBitmap*)bitmap)->GetBitmap(&bm);
  int w = bm.bmWidth; 
  int h = bm.bmHeight;
  CDC memDC; memDC.CreateCompatibleDC((CDC*)pDC);
  CBitmap *pBmp = memDC.SelectObject((CBitmap*)bitmap);
  ((CDC*)pDC)->BitBlt(point.x, point.y, w, h, &memDC, 0, 0, SRCCOPY);
  memDC.SelectObject(pBmp);
}

//----------------------------------------------------------------------------
// Function DrawBitmap
// TEXT_LANG[@]func    draws bitmap centered in specified rectangle in specified device context
// TEXT_LANG[@]rdesc   nothing
// TEXT_LANG[@]parm    const CBitmap | *bitmap | bitmap to draw
// TEXT_LANG[@]parm    const CDC     | *pDC    | device context to draw in
// TEXT_LANG[@]parm    const CRect   | &rect   | rectangle to center in
//----------------------------------------------------------------------------
// TEXT_LANG[@]prog 
// Anneke Sicherer-Roetman
// TEXT_LANG[@]revs 
// 06-10-1999 - First implementation
//----------------------------------------------------------------------------
// TEXT_LANG[@]todo 
//----------------------------------------------------------------------------
static void DrawBitmap(const CBitmap *bitmap, const CDC *pDC, const CRect &rect)
{
  BITMAP bm; ((CBitmap*)bitmap)->GetBitmap(&bm);
  int w = bm.bmWidth; 
  int h = bm.bmHeight;
  CPoint point;
  point.x = rect.left + ((rect.right - rect.left) / 2) - (w / 2);
  point.y = rect.top + ((rect.bottom - rect.top) / 2) - (h / 2);
  DrawBitmap(bitmap, pDC, point);
}

//----------------------------------------------------------------------------
// Function CBitmapPickerCombo::CBitmapPickerCombo
// TEXT_LANG[@]mfunc   constructor
// TEXT_LANG[@]xref    <c CBitmapPickerCombo>
//----------------------------------------------------------------------------
// TEXT_LANG[@]prog 
// Anneke Sicherer-Roetman
// TEXT_LANG[@]revs 
// 06-10-1999 - First implementation
//----------------------------------------------------------------------------
// TEXT_LANG[@]todo 
//----------------------------------------------------------------------------
CBitmapPickerCombo::CBitmapPickerCombo():
  CComboBox(),
    m_nItemWidth(0),
    m_nItemHeight(0)
{
}

//----------------------------------------------------------------------------
// Function CBitmapPickerCombo::AddBitmap
// TEXT_LANG[@]mfunc   adds bitmap (and string) item to combobox
// TEXT_LANG[@]rdesc   index of item (-1 on failure) (int)
// TEXT_LANG[@]parm    const CBitmap | *bitmap | bitmap to add
// TEXT_LANG[@]parm    const char    | *string | string to add (default NULL)
// TEXT_LANG[@]xref    <c CBitmapPickerCombo>
//----------------------------------------------------------------------------
// TEXT_LANG[@]prog 
// Anneke Sicherer-Roetman
// TEXT_LANG[@]revs 
// 06-10-1999 - First implementation
//----------------------------------------------------------------------------
// TEXT_LANG[@]todo 
//----------------------------------------------------------------------------
int CBitmapPickerCombo::AddBitmap(const CBitmap *bitmap, const char *string)
{
  return InsertBitmap(GetCount(), bitmap, string);
}

//----------------------------------------------------------------------------
// Function CBitmapPickerCombo::InsertBitmap
// TEXT_LANG[@]mfunc   adds bitmap (and string) item to combobox at specified index
// TEXT_LANG[@]rdesc   index of item (-1 on failure) (int)
// TEXT_LANG[@]parm    int           | nIndex  | index at which to insert
// TEXT_LANG[@]parm    const CBitmap | *bitmap | bitmap to add
// TEXT_LANG[@]parm    const char    | *string | string to add (default NULL)
// TEXT_LANG[@]xref    <c CBitmapPickerCombo>
//----------------------------------------------------------------------------
// TEXT_LANG[@]prog 
// Anneke Sicherer-Roetman <nl>
// after: Icon Picker Combo Box by Joel Wahlberg <nl>
// http://www.codeguru.com/combobox/icon_combobox.shtml
// TEXT_LANG[@]revs 
// 06-10-1999 - First implementation
//----------------------------------------------------------------------------
// TEXT_LANG[@]todo 
//----------------------------------------------------------------------------
int CBitmapPickerCombo::InsertBitmap(int nIndex, const CBitmap *bitmap, const char *string)
{
  int n = CComboBox::InsertString(nIndex, string ? string : "");
  if (n != CB_ERR && n != CB_ERRSPACE) {
    SetItemData(n, (DWORD)bitmap);
    BITMAP bm; ((CBitmap*)bitmap)->GetBitmap(&bm);
    SetSize(bm.bmWidth, bm.bmHeight);
  }
  return n;
}

//----------------------------------------------------------------------------
// Function CBitmapPickerCombo::MeasureItem
// TEXT_LANG[@]mfunc   Called by MFC when combo box is created
// TEXT_LANG[@]rdesc   nothing
// TEXT_LANG[@]parm    LPMEASUREITEMSTRUCT | lpMIS | standard parameter
// TEXT_LANG[@]xref    <c CBitmapPickerCombo>
//----------------------------------------------------------------------------
// TEXT_LANG[@]prog 
// Anneke Sicherer-Roetman
// TEXT_LANG[@]revs 
// 06-10-1999 - First implementation
//----------------------------------------------------------------------------
// TEXT_LANG[@]todo 
//----------------------------------------------------------------------------
void CBitmapPickerCombo::MeasureItem(LPMEASUREITEMSTRUCT lpMIS)
{ 
  lpMIS->itemWidth = (m_nItemWidth + 2);
  lpMIS->itemHeight = (m_nItemHeight + 2);
}

//----------------------------------------------------------------------------
// Function CBitmapPickerCombo::DrawItem
// TEXT_LANG[@]mfunc   Called by MFC when visual aspect of combo box changes 
// TEXT_LANG[@]rdesc   nothing
// TEXT_LANG[@]parm    LPDRAWITEMSTRUCT | lpDIS | standard parameter
// TEXT_LANG[@]xref    <c CBitmapPickerCombo>
//----------------------------------------------------------------------------
// TEXT_LANG[@]prog 
// Anneke Sicherer-Roetman <nl>
// after: Icon Picker Combo Box by Joel Wahlberg <nl>
// http://www.codeguru.com/combobox/icon_combobox.shtml
// TEXT_LANG[@]revs 
// 06-10-1999 - First implementation
//----------------------------------------------------------------------------
// TEXT_LANG[@]todo 
//----------------------------------------------------------------------------
void CBitmapPickerCombo::DrawItem(LPDRAWITEMSTRUCT lpDIS)
{
  CDC* pDC = CDC::FromHandle(lpDIS->hDC);

  if (!IsWindowEnabled()) {
    CBrush brDisabled(RGB(192,192,192)); // light gray
    CBrush* pOldBrush = pDC->SelectObject(&brDisabled);
    CPen penDisabled(PS_SOLID, 1, RGB(192,192,192));
    CPen* pOldPen = pDC->SelectObject(&penDisabled);
    OutputBitmap(lpDIS, false);
    pDC->SelectObject(pOldBrush);
    pDC->SelectObject(pOldPen);
    return;
  }

  // Selected
  if ((lpDIS->itemState & ODS_SELECTED) 
    && (lpDIS->itemAction & (ODA_SELECT | ODA_DRAWENTIRE))) {
    CBrush brHighlight(::GetSysColor(COLOR_HIGHLIGHT)); 
    CBrush* pOldBrush = pDC->SelectObject(&brHighlight);
    CPen penHighlight(PS_SOLID, 1, ::GetSysColor(COLOR_HIGHLIGHT));
    CPen* pOldPen = pDC->SelectObject(&penHighlight);
    pDC->Rectangle(&lpDIS->rcItem);
    pDC->SetBkColor(::GetSysColor(COLOR_HIGHLIGHT));
    pDC->SetTextColor(::GetSysColor(COLOR_HIGHLIGHTTEXT));
    OutputBitmap(lpDIS, true);
    pDC->SelectObject(pOldBrush);
    pDC->SelectObject(pOldPen);
  }

  // De-Selected
  if (!(lpDIS->itemState & ODS_SELECTED) 
    && (lpDIS->itemAction & (ODA_SELECT | ODA_DRAWENTIRE))) {
    CBrush brWindow(::GetSysColor(COLOR_WINDOW)); 
    CBrush* pOldBrush = pDC->SelectObject(&brWindow);
    CPen penHighlight(PS_SOLID, 1, ::GetSysColor(COLOR_WINDOW));
    CPen* pOldPen = pDC->SelectObject(&penHighlight);
    pDC->Rectangle(&lpDIS->rcItem);
    pDC->SetBkColor(::GetSysColor(COLOR_WINDOW));
    pDC->SetTextColor(::GetSysColor(COLOR_WINDOWTEXT));
    OutputBitmap(lpDIS, false);
    pDC->SelectObject(pOldBrush);
    pDC->SelectObject(pOldPen);
  }

  // Focus
  if (lpDIS->itemAction & ODA_FOCUS) 
    pDC->DrawFocusRect(&lpDIS->rcItem);
}

//----------------------------------------------------------------------------
// Function CBitmapPickerCombo::OutputBitmap
// TEXT_LANG[@]mfunc   draws bitmap (and string) in item
// TEXT_LANG[@]rdesc   nothing
// TEXT_LANG[@]parm    LPDRAWITEMSTRUCT | lpDIS    | item data
// TEXT_LANG[@]parm    bool             | selected | is the item selected?
// TEXT_LANG[@]xref    <c CBitmapPickerCombo>
//----------------------------------------------------------------------------
// TEXT_LANG[@]prog 
// Anneke Sicherer-Roetman <nl>
// after: Icon Picker Combo Box by Joel Wahlberg <nl>
// http://www.codeguru.com/combobox/icon_combobox.shtml
// TEXT_LANG[@]revs 
// 06-10-1999 - First implementation
//----------------------------------------------------------------------------
// TEXT_LANG[@]todo 
//----------------------------------------------------------------------------
void CBitmapPickerCombo::OutputBitmap(LPDRAWITEMSTRUCT lpDIS, bool /*selected*/)
{
  const CBitmap *bitmap = (const CBitmap*)(lpDIS->itemData);
  if (bitmap && bitmap != (const CBitmap *)(0xffffffff)) {
    CDC* pDC = CDC::FromHandle(lpDIS->hDC);
    CString string; 
    if (lpDIS->itemID != -1) 
      GetLBText(lpDIS->itemID, string); 
    if (string.IsEmpty()) 
      DrawBitmap(bitmap, pDC, lpDIS->rcItem);
    else {
      CPoint point;
      point.x = lpDIS->rcItem.left + 2;
      point.y = lpDIS->rcItem.top + ((lpDIS->rcItem.bottom - lpDIS->rcItem.top) / 2) - (m_nItemHeight / 2); 
      DrawBitmap(bitmap, pDC, point);
      CRect rcText(lpDIS->rcItem); 
      rcText.DeflateRect(m_nItemWidth + 4, 0, 0, 0);
      pDC->DrawText(string, rcText, DT_SINGLELINE |DT_VCENTER ); 
    }
  }
}               

//----------------------------------------------------------------------------
// Function CBitmapPickerCombo::SetSize
// TEXT_LANG[@]mfunc   sets overall item size
// TEXT_LANG[@]rdesc   nothing
// TEXT_LANG[@]parm    int | width  | width of item
// TEXT_LANG[@]parm    int | height | height of item
// TEXT_LANG[@]xref    <c CBitmapPickerCombo>
//----------------------------------------------------------------------------
// TEXT_LANG[@]prog 
// Anneke Sicherer-Roetman
// TEXT_LANG[@]revs 
// 06-10-1999 - First implementation
//----------------------------------------------------------------------------
// TEXT_LANG[@]todo 
//----------------------------------------------------------------------------
void CBitmapPickerCombo::SetSize(int width, int height)
{
  if (width > m_nItemWidth)
    m_nItemWidth = width;
  if (height > m_nItemHeight)
    m_nItemHeight = height;
  for (int i = -1; i < GetCount(); i++) 
    SetItemHeight(i, m_nItemHeight + 6);
}

//----------------------------------------------------------------------------

#ifdef _DEBUG
void CBitmapPickerCombo::PreSubclassWindow() 
{
  CComboBox::PreSubclassWindow();

  // ensure some styles are set
  // modifying style here has NO effect!?!
  ASSERT(GetStyle() & CBS_DROPDOWNLIST);
  ASSERT(GetStyle() & CBS_OWNERDRAWVARIABLE);
  ASSERT(GetStyle() & CBS_HASSTRINGS);
}
#endif

//----------------------------------------------------------------------------
