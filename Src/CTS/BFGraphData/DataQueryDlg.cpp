// DataQueryDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DataQueryDlg.h"
#include "Data.h"
#include "FolderDialog.h"
#include "ResultTestSelDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define CT_CONFIG_REG_SEC "Config"
#define CT_PATH_REG_SEC "Path"

// Begin ////////////
#define MAX_CYCLE_NUM	2147483647	//
// End ///////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// CDataQueryDialog dialog


CDataQueryDialog::CDataQueryDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CDataQueryDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDataQueryDialog)
	m_lFromCycle = 0;
	m_lIntervalCycle = 1;

 ////////////
	m_lToCycle = 1;//MAX_CYCLE_NUM;		//
///////////////////////////////
	//m_lToCycle = 1;

	m_bCycleStringCheck = FALSE;
	m_strCycle = _T("");
	m_iMode = 0x00;
//	m_iOpenTime = 1;
	m_iStartType = 0;
	//}}AFX_DATA_INIT

	m_pDatabase          = NULL;
	m_pbyXAxisIndex      = NULL;
	m_byVoltagePoint     = 0x00;
	m_byTemperaturePoint = 0x00;

	if( LanguageinitMonConfig() == false )
	{
		AfxMessageBox("Could not found [ GraphAnalyzer_Lang.ini ]");
	}
}
CDataQueryDialog::~CDataQueryDialog()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

void CDataQueryDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDataQueryDialog)
//	DDX_Control(pDX, IDC_TEST_FROM_FOLDER_BTN, m_TestFromFolderBtn);
	DDX_Control(pDX, IDC_TEST_NAME_LIST, m_TestNameListCtrl);
	DDX_Control(pDX, IDC_Y_AXIS_LIST, m_YAxisList);
	DDX_Control(pDX, IDC_Y_UNIT_COMBO, m_YUnitCombo);
	DDX_Control(pDX, IDC_X_UNIT_COMBO, m_XUnitCombo);
	DDX_Control(pDX, IDC_X_AXIS_COMBO, m_XAxisCombo);
	DDX_Control(pDX, IDC_SEL_CYCLE_LIST, m_SelCycleList);
	DDX_Control(pDX, IDC_CHANNEL_LIST, m_ChannelListCtrl);
	DDX_Text(pDX, IDC_CYCLE_FROM_EDIT, m_lFromCycle);
	DDV_MinMaxLong(pDX, m_lFromCycle, 0, 2147483647);
	DDX_Text(pDX, IDC_CYCLE_INTERVAL_EDIT, m_lIntervalCycle);
	DDV_MinMaxLong(pDX, m_lIntervalCycle, 1, 2147483647);
	DDX_Check(pDX, IDC_CYCLE_STRING_CHECK, m_bCycleStringCheck);
	DDX_Text(pDX, IDC_CYCLE_STRING_EDIT, m_strCycle);
	DDX_Text(pDX, IDC_CYCLE_TO_EDIT, m_lToCycle);
	DDV_MinMaxLong(pDX, m_lToCycle, 1, 2147483647);
//	DDX_Radio(pDX, IDC_MODE_RADIO, m_iMode);
//	DDX_Radio(pDX, IDC_OPEN_TIME_RADIO, m_iOpenTime);
	DDX_Radio(pDX, IDC_START_TYPE_RADIO, m_iStartType);
	//}}AFX_DATA_MAP

	if(pDX->m_bSaveAndValidate)
	{
		///////////
		// Check //
		///////////

		if(m_TestNameListCtrl.GetItemCount()<=0)
		{
			MessageBox(TEXT_LANG[0], TEXT_LANG[1], MB_ICONEXCLAMATION);
			pDX->Fail();
		}

		if(m_ChannelListCtrl.GetSelectedCount()<=0)
		{
			MessageBox(TEXT_LANG[2], TEXT_LANG[3], MB_ICONEXCLAMATION);
			pDX->Fail();
		}

		CList<LONG, LONG&> CycleList;
		POSITION pos = m_strlistSelCycle.GetHeadPosition();
		while(pos)
		{
			//
			CString strCycle = m_strlistSelCycle.GetNext(pos);
			long from=0L, to=0L, interval=0L;
			int p1=0, p2=0;
			p2=strCycle.Find('~',p1);
			from =      atoi(strCycle.Mid(p1,p2-p1));
			p1=p2+1;
			p2=strCycle.Find(':',p1);
			to   =     atoi(strCycle.Mid(p1,p2-p1));
			interval = atoi(strCycle.Mid(p2+1));
			//
			for(long cyc=from; cyc<=to; cyc+=interval)
			{
				if(CycleList.Find(cyc)==NULL) CycleList.AddTail(cyc);
			}
		}
		if(m_bCycleStringCheck)
		{
			int p1=0, p2=0, p3=0;
			while(TRUE && !m_strCycle.IsEmpty())
			{
				p2=m_strCycle.Find(',',p1);
				p3=m_strCycle.Find('-',p1);
				if     (p2!=-1&&p3==-1)
				{
					long cyc = atoi(m_strCycle.Mid(p1,p2-p1));
					if(CycleList.Find(cyc)==NULL) CycleList.AddTail(cyc);
				}
				else if(p2!=-1&&p3!=-1)
				{
					if(p3<p2)
					{
						long from = atoi(m_strCycle.Mid(p1, p3-p1));
						long to   = atoi(m_strCycle.Mid(p3+1, p2-p3-1));
						for(long cyc=from; cyc<=to; cyc++)
						{
							if(CycleList.Find(cyc)==NULL) CycleList.AddTail(cyc);
						}
					}
				}
				else if(p2==-1&&p3==-1)
				{
					CString str = m_strCycle.Mid(p1);
					if(!str.IsEmpty())
					{
						long cyc = atoi(str);
						if(CycleList.Find(cyc)==NULL) CycleList.AddTail(cyc);
					}
					break;
				}
				else if(p2==-1&&p3!=-1)
				{
					long from = atoi(m_strCycle.Mid(p1, p3-p1));
					long to   = atoi(m_strCycle.Mid(p3+1));
					for(long cyc=from; cyc<=to; cyc++)
					{
						if(CycleList.Find(cyc)==NULL) CycleList.AddTail(cyc);
					}
					break;
				}
			
				p1=p2+1;
			}
		}
		if(CycleList.IsEmpty())
		{
			MessageBox(TEXT_LANG[4], TEXT_LANG[5], MB_ICONEXCLAMATION);
			pDX->Fail();
		}

		if(m_XAxisCombo.GetCurSel()==CB_ERR)
		{
			MessageBox(TEXT_LANG[6], TEXT_LANG[7], MB_ICONEXCLAMATION);
			pDX->Fail();
		}

		BOOL bCheck = FALSE;
		for(int i=0; i<m_YAxisList.GetCount(); i++)
		{
			if(m_YAxisList.GetCheck(i)==1)
			{
				bCheck = TRUE;
				break;
			}
		}
		if(!bCheck)
		{
			MessageBox(TEXT_LANG[8], TEXT_LANG[9], MB_ICONEXCLAMATION);
			pDX->Fail();
		}

		/////////////
		// Channel //
		/////////////
		
		m_pQueryCondition->ResetChPath();
		
		int     nItem, nIndex;
		CString strChPath;
		CString strTestName;

		pos = m_ChannelListCtrl.GetFirstSelectedItemPosition();
		while(pos)
		{
			nIndex = 0;
			nItem     = m_ChannelListCtrl.GetNextSelectedItem(pos);
			nIndex = m_ChannelListCtrl.GetItemData(nItem);
			
/*			strTestName = m_ChannelListCtrl.GetItemText(nItem, 0);

			for(int i=0; i<m_TestNameListCtrl.GetItemCount(); i++)
			{
				if(strTestName == m_TestNameListCtrl.GetItemText(i, 1))
				{
					nIndex = i;
					break;
				}
			}
*/

			strChPath = m_ChannelListCtrl.GetItemText(nItem,1);
			strChPath = m_strlistTestPath.GetAt(m_strlistTestPath.FindIndex(nIndex))+"\\"+m_ChannelListCtrl.GetItemText(nItem,1);
//			strChPath = m_strlistTestPath.GetAt(m_strlistTestPath.FindIndex(nIndex));
//			strChPath.MakeLower();
			m_pQueryCondition->AddChPath(strChPath);
		}

		///////////
		// Cycle //
		///////////

		int NumOfCycle = CycleList.GetCount();
		long* plCycle = new long[NumOfCycle];
		int n=0;
		pos = CycleList.GetHeadPosition();
		while(pos)
		{
			plCycle[n]=CycleList.GetNext(pos);
			n++;
		}
		qsort( (void *)plCycle, (size_t)NumOfCycle, sizeof(long ), CData::LongCompare);

		BOOL bChangeCycleSelection = m_pQueryCondition->UpdateCycleList(plCycle, NumOfCycle);

		delete [] plCycle;


		//////////
		// Axis //
		//////////

		CString strTitle, strUnitNotation;
		float fltUnitTransfer=0.0f;
		CDaoRecordset aRecordset(m_pDatabase);
		CString strQuery;

		////////////
		// X-Axis //
		////////////

		// Title
		m_XAxisCombo.GetLBText(m_XAxisCombo.GetCurSel(), strTitle);
		// Unit Notation
		m_XUnitCombo.GetLBText(m_XUnitCombo.GetCurSel(), strUnitNotation);
		// Unit Transfer
		strQuery.Format("SELECT PropertyIndex FROM AxisInfo WHERE index = %d",
			            m_pbyXAxisIndex[m_XAxisCombo.GetCurSel()]);
		aRecordset.Open(dbOpenDynaset, strQuery);
		COleVariant var;
		aRecordset.GetFieldValue("PropertyIndex", var);
		BYTE PropertyIndex = var.bVal;
		var.Clear();
		aRecordset.Close();
		//
		strQuery.Format("SELECT UnitTransfer FROM Property WHERE index = %d",
			            PropertyIndex);
		aRecordset.Open(dbOpenDynaset, strQuery);
		aRecordset.Move(m_XUnitCombo.GetCurSel());
		aRecordset.GetFieldValue("UnitTransfer", var);
		fltUnitTransfer = var.fltVal;
		var.Clear();
		aRecordset.Close();

		m_iMode = 0;
		int nModeCnt = 0;
		if(((CButton*)GetDlgItem(IDC_CHARGE_CHECK))->GetCheck()==1)	
		{
			m_iMode|= (0x01 << PS_STEP_CHARGE);
			nModeCnt++;
		}
		if(((CButton*)GetDlgItem(IDC_DISCHARGE_CHECK))->GetCheck()==1)
		{
			m_iMode|= (0x01 << PS_STEP_DISCHARGE);
			nModeCnt++;
		}
		if(((CButton*)GetDlgItem(IDC_REST_CHECK))->GetCheck()==1)
		{
			m_iMode|= (0x01 << PS_STEP_REST);
			nModeCnt++;
		}
		if(((CButton*)GetDlgItem(IDC_IMP_CHECK))->GetCheck()==1)
		{
			m_iMode|= (0x01 << PS_STEP_IMPEDANCE);
			nModeCnt++;
		}
		if(((CButton*)GetDlgItem(IDC_PATTERN_CHECK))->GetCheck()==1)
		{
			m_iMode|= (0x01 << PS_STEP_END);
			nModeCnt++;
		}
		if(((CButton*)GetDlgItem(IDC_CHECK_CHECK))->GetCheck()==1)
		{
			m_iMode|= (0x01 << PS_STEP_CHECK);
			nModeCnt++;
		}
		if(((CButton*)GetDlgItem(IDC_OCV_CHECK))->GetCheck()==1)
		{
			m_iMode|= (0x01 << PS_STEP_OCV);
			nModeCnt++;
		}


		if(m_iMode == 0 )
		{
			MessageBox(TEXT_LANG[10], TEXT_LANG[11], MB_ICONEXCLAMATION);
			pDX->Fail();
		}

		//X축이 Cycle 선택되었으면 Y축으로 사용되는 Step의 종류는 1개만 선택되어야 한다.
// 		if(strTitle.CompareNoCase("Cycle") == 0 && nModeCnt != 1)
// 		{
// 			MessageBox("X축이 Cycle이면 표기 Step은 1개만 선택 가능합니다.", "Step 종류 선택", MB_ICONEXCLAMATION);
// 			pDX->Fail();
// 		}

		// Query Condition의 XAxis member변수의 내용에
		// 사용자의 선택을 반영한다.
		CAxis* pXAxis = m_pQueryCondition->GetXAxis();

			//
			// 이번 User선택으로 Query Condition이 Modifying Mode로 되는지 결정.
			//
			m_pQueryCondition->SetModifyingMode(FALSE);
			if((strTitle.CompareNoCase("cycle")==0 ) && (pXAxis->GetTitle().CompareNoCase("cycle") == 0))
			{
				if(!bChangeCycleSelection) 
					m_pQueryCondition->SetModifyingMode(TRUE);
			}
			else if((strTitle.CompareNoCase("cycle") != 0 ) && (pXAxis->GetTitle().CompareNoCase("cycle") != 0))
			{
/*				if(m_iStartType == m_pQueryCondition->GetStartPointOption())
				{
					//Step selection만 변경한 경우 
					if(!bChangeCycleSelection && m_iMode != m_pQueryCondition->GetOutputOption())
					{
						m_pQueryCondition->SetModifyingMode(TRUE);
					}
				}
*/
				if((m_iStartType == 0) && (m_pQueryCondition->GetStartPointOption()==0))
				{
					if(!bChangeCycleSelection && (m_iMode == m_pQueryCondition->GetOutputOption())) 
						m_pQueryCondition->SetModifyingMode(TRUE);
				}
				else if((m_iStartType==1) && (m_pQueryCondition->GetStartPointOption()==1))
				{
					if(m_iMode == m_pQueryCondition->GetOutputOption()) 
						m_pQueryCondition->SetModifyingMode(TRUE);
				}
			}

		pXAxis->SetTitle(strTitle);
		pXAxis->SetUnitNotation(strUnitNotation);
		pXAxis->SetUnitTransfer(fltUnitTransfer);

		////////////
		// Y-Axis //
		////////////

		// Query Condition의 YAxis member변수의 내용에
		// 사용자의 선택을 반영한다.
		m_pQueryCondition->ResetYAxisList();
		for(i=0; i<m_YAxisList.GetCount(); i++)
		{
			if(m_YAxisList.GetCheck(i)==1)
			{
				POSITION pos = m_YAxisPropertyIndex.FindIndex(i);
				CPoint pt = m_YAxisPropertyIndex.GetAt(pos);
				// Title
				m_YAxisList.GetText(i, strTitle);
				// UnitNotation, UnitTransfer
				strQuery.Format("SELECT UnitNotation, UnitTransfer FROM Property WHERE index = %d",
					            pt.x);
				aRecordset.Open(dbOpenDynaset, strQuery);
				aRecordset.Move(pt.y);
				aRecordset.GetFieldValue("UnitNotation", var);
				strUnitNotation = (LPCTSTR)(LPTSTR)var.bstrVal;
				var.Clear();
				aRecordset.GetFieldValue("UnitTransfer", var);
				fltUnitTransfer = var.fltVal;
				var.Clear();
				aRecordset.Close();
				//
				CAxis* pYAxis = new CAxis(strTitle, strUnitNotation, fltUnitTransfer);
				m_pQueryCondition->AddYAxisList(pYAxis);
			}
		}

		///////////////////
		// Output Option //
		///////////////////

		m_pQueryCondition->SetOutputOption(m_iMode);
		m_pQueryCondition->SetStartPointOption(m_iStartType);
		AfxGetApp()->WriteProfileInt(QUERY_REG_SECTION, "Step Option", m_iMode);
		AfxGetApp()->WriteProfileInt(QUERY_REG_SECTION, "Start Option", m_iStartType);
		

		///////////////////////
		// Measurement Point //
		///////////////////////
		if(((CButton*)GetDlgItem(IDC_VOLTAGE1_CHECK))->GetCheck()==1) m_byVoltagePoint|= 0x01;
		else                                                          m_byVoltagePoint&=~0x01;
		if(((CButton*)GetDlgItem(IDC_VOLTAGE2_CHECK))->GetCheck()==1) m_byVoltagePoint|= 0x02;
		else                                                          m_byVoltagePoint&=~0x01;
		if(((CButton*)GetDlgItem(IDC_VOLTAGE3_CHECK))->GetCheck()==1) m_byVoltagePoint|= 0x04;
		else                                                          m_byVoltagePoint&=~0x01;
		if(((CButton*)GetDlgItem(IDC_VOLTAGE4_CHECK))->GetCheck()==1) m_byVoltagePoint|= 0x08;
		else                                                          m_byVoltagePoint&=~0x01;
		if(((CButton*)GetDlgItem(IDC_TEMPERATURE1_CHECK))->GetCheck()==1) m_byTemperaturePoint|= 0x01;
		else                                                              m_byTemperaturePoint&=~0x01;
		if(((CButton*)GetDlgItem(IDC_TEMPERATURE1_CHECK))->GetCheck()==1) m_byTemperaturePoint|= 0x02;
		else                                                              m_byTemperaturePoint&=~0x01;

		WriteRegUserSet();


		//Data가 너무 많은 경우 경고를 띠움 
		//선택 채널 수, cycle 갯수
		int a = m_pQueryCondition ->GetChPathCount();
		int b = a * NumOfCycle;
		//memory 약 80M 사용
		if( b > AfxGetApp()->GetProfileInt(QUERY_REG_SECTION, "MaxWarningCycle", 128))
		{
			if(IDCANCEL == MessageBox(TEXT_LANG[12], "Warning", MB_OKCANCEL|MB_ICONEXCLAMATION))
				pDX->Fail();
		}

	} // if(pDX->m_bSaveAndValidate)
}


BEGIN_MESSAGE_MAP(CDataQueryDialog, CDialog)
	//{{AFX_MSG_MAP(CDataQueryDialog)
	ON_BN_CLICKED(IDC_ADD_SEL_CYCLE_BTN, OnAddSelCycleBtn)
	ON_BN_CLICKED(IDC_DEL_SEL_CYCLE_BTN, OnDelSelCycleBtn)
	ON_BN_CLICKED(IDC_CYCLE_STRING_CHECK, OnCycleStringCheck)
	ON_WM_DESTROY()
	ON_CBN_SELCHANGE(IDC_X_AXIS_COMBO, OnSelchangeXAxisCombo)
	ON_LBN_SELCHANGE(IDC_Y_AXIS_LIST, OnSelchangeYAxisList)
	ON_CBN_SELCHANGE(IDC_Y_UNIT_COMBO, OnSelchangeYUnitCombo)
	ON_BN_CLICKED(IDC_TEST_FROM_FOLDER_BTN, OnTestFromFolderBtn)	
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_CHANNEL_LIST, OnItemchangedChannelList)
	ON_BN_CLICKED(IDC_BTN_EXCEL_EXPORT, OnBtnExcelExport)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDataQueryDialog message handlers

BOOL CDataQueryDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();

	//
	// 1. 모양만들기
	//

	// Test Path ListCtrl
	m_TestNameListCtrl.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);//|LVS_EX_TRACKSELECT);
//	m_TestNameListCtrl.SetHoverTime(1000);
	m_TestNameListCtrl.InsertColumn(1, "No",	LVCFMT_LEFT, 60,  0);
	m_TestNameListCtrl.InsertColumn(2, "Job name", LVCFMT_LEFT, 165, 1);
	m_TestNameListCtrl.InsertColumn(3, "Workday", LVCFMT_LEFT, 165, 2);

	// Test Path Button
//	m_TestFromListBtn.LoadBitmaps(IDB_LIST_UP,IDB_LIST_DOWN,IDB_LIST_FOCUS);
//	m_TestFromFolderBtn.LoadBitmaps(IDB_FOLDER_UP,IDB_FOLDER_DOWN,IDB_FOLDER_FOCUS);

	// Channel ListCtrl
	m_ChannelListCtrl.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);//|LVS_EX_TRACKSELECT);
//	m_ChannelListCtrl.SetHoverTime(10000);
	m_ChannelListCtrl.InsertColumn(1, "Job name", LVCFMT_LEFT, 300,  0);
	m_ChannelListCtrl.InsertColumn(2, "Channel", LVCFMT_LEFT, 100, 1);
	m_ChannelListCtrl.InsertColumn(3, "Step No.",   LVCFMT_RIGHT, 80, 2);
	m_ChannelListCtrl.InsertColumn(4, "Size",   LVCFMT_RIGHT, 60, 3);


	CString path = _T("");
	path = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC ,"Path", "");
	if( path.IsEmpty() )
	{
		TCHAR szBuff[_MAX_PATH];
		::GetModuleFileName(AfxGetApp()->m_hInstance,szBuff,_MAX_PATH);
		path = szBuff;
	}
	
	path = path.Left(path.ReverseFind('\\'))+"\\DataBase\\"+FORM_SET_DATABASE_NAME;
	
	m_pDatabase = new CDaoDatabase;
	m_pDatabase->Open(path);

	//
	// 3. TestPathListCtrl 및 ChannelListCtrl의 내용을 넣는다.
	//
	POSITION pos = m_strlistTestPath.GetHeadPosition();
	int aa =0;
	while(pos)
	{
		AddTestPath(m_strlistTestPath.GetNext(pos), aa++);
	}

	SortChannelListCtrl();

	//
	// Measurement Points
	//
	CFileFind afinder;
	if(!afinder.FindFile(path.Left(path.ReverseFind('\\'))+"\\PackType.txt"))
	{
/*		CRect rect1, rect2;
		GetWindowRect(rect1);
		((CStatic*)GetDlgItem(IDC_STATIC_LINE))->GetWindowRect(rect2);
		MoveWindow(rect1.left,rect1.top,rect1.Width(),rect2.bottom-rect1.top);
*/	}
	else
	{
		if(m_byVoltagePoint&0x01) ((CButton*)GetDlgItem(IDC_VOLTAGE1_CHECK))->SetCheck(1);
		if(m_byVoltagePoint&0x02) ((CButton*)GetDlgItem(IDC_VOLTAGE2_CHECK))->SetCheck(1);
		if(m_byVoltagePoint&0x04) ((CButton*)GetDlgItem(IDC_VOLTAGE3_CHECK))->SetCheck(1);
		if(m_byVoltagePoint&0x08) ((CButton*)GetDlgItem(IDC_VOLTAGE4_CHECK))->SetCheck(1);
		if(m_byTemperaturePoint&0x01) ((CButton*)GetDlgItem(IDC_TEMPERATURE1_CHECK))->SetCheck(1);
		if(m_byTemperaturePoint&0x02) ((CButton*)GetDlgItem(IDC_TEMPERATURE2_CHECK))->SetCheck(1);
	}
	
	m_iStartType = AfxGetApp()->GetProfileInt(QUERY_REG_SECTION, "Start Option", 0);
	m_iMode = AfxGetApp()->GetProfileInt(QUERY_REG_SECTION, "Step Option", 0xffffffff);
	if(m_iMode&(0x01 << PS_STEP_CHARGE)) ((CButton*)GetDlgItem(IDC_CHARGE_CHECK))->SetCheck(1);
	if(m_iMode&(0x01 << PS_STEP_DISCHARGE)) ((CButton*)GetDlgItem(IDC_DISCHARGE_CHECK))->SetCheck(1);
	if(m_iMode&(0x01 << PS_STEP_REST)) ((CButton*)GetDlgItem(IDC_REST_CHECK))->SetCheck(1);
	if(m_iMode&(0x01 << PS_STEP_IMPEDANCE)) ((CButton*)GetDlgItem(IDC_IMP_CHECK))->SetCheck(1);
	if(m_iMode&(0x01 << PS_STEP_END)) ((CButton*)GetDlgItem(IDC_PATTERN_CHECK))->SetCheck(1);
	if(m_iMode&(0x01 << PS_STEP_CHECK)) ((CButton*)GetDlgItem(IDC_CHECK_CHECK))->SetCheck(1);
	if(m_iMode&(0x01 << PS_STEP_OCV)) ((CButton*)GetDlgItem(IDC_OCV_CHECK))->SetCheck(1);

	// Query Condition의 내용에 따라 이전에 선택했던 Channel들을
	// 선택한 상태로 표시해 준다.
	if(m_pQueryCondition->GetChPathCount() > 0)
	{
		int N = m_ChannelListCtrl.GetItemCount();
		for(int nItem=0; nItem<N; nItem++)
		{
			//
	//		int nIndex        = atoi(m_ChannelListCtrl.GetItemText(nItem,0))-1;
			int nIndex        = m_ChannelListCtrl.GetItemData(nItem);
			CString strChPath = m_strlistTestPath.GetAt(m_strlistTestPath.FindIndex(nIndex))
							   +"\\"+m_ChannelListCtrl.GetItemText(nItem,1);
			strChPath.MakeLower();

			m_ChannelListCtrl.SetItemState(nItem, ~LVIS_SELECTED, LVIS_SELECTED);
			//
			POSITION pos = m_pQueryCondition->GetChPathHead();
			while(pos)
			{
				CString strPath   = m_pQueryCondition->GetNextChPath(pos);
				if(strPath.CompareNoCase(strChPath)==0){
					m_ChannelListCtrl.SetItemState(nItem,LVIS_SELECTED,LVIS_SELECTED);
					m_ChannelListCtrl.Update(nItem);
					break;
				}
			}
		}
	}

	//
	// 4. Cycle Selection List
	//

	pos = m_strlistSelCycle.GetHeadPosition();
	while(pos)
	{
		m_SelCycleList.AddString(m_strlistSelCycle.GetNext(pos));
	}

	//
	// 5. Cycle Selection String
	//

	OnCycleStringCheck();

	//
	// 5. X Axis Combo Box
	//
	CDaoRecordset aRecordset(m_pDatabase);
	aRecordset.Open(dbOpenDynaset, "SELECT Index, Name FROM AxisInfo WHERE IsUsedForXAxis=-1");
	aRecordset.MoveLast();
	m_pbyXAxisIndex = new BYTE[aRecordset.GetRecordCount()];
	aRecordset.MoveFirst();

	int i=0;
	//기본 선택 축 Item
//	int nXAxisDefaultIndex = 0;		//Time
//	int nYAxisDefaultIndex = 3;		//Capacity

	CString strXName;
	CString strDefaultXName("Time");
	strDefaultXName = AfxGetApp()->GetProfileString(QUERY_REG_SECTION, "X Axis", "Time");
	
	COleVariant var;
	while(!aRecordset.IsEOF())
	{
		aRecordset.GetFieldValue("Name",var);
		strXName = (LPCTSTR)(LPTSTR)var.bstrVal;
		m_XAxisCombo.AddString(strXName);

//		//X축을 기본으로 cycle 항목을 선택한다.
/*		if( strXName.Compare(strDefaultXName) == 0)
		{
			nXAxisDefaultIndex = m_XAxisCombo.GetCount() - 1;
		}
*/
		var.Clear();
		aRecordset.GetFieldValue("Index",var);
		m_pbyXAxisIndex[i] = var.bVal;
		
		aRecordset.MoveNext();
		i++;
	}
	aRecordset.Close();

	//
	// 6. X Unit Combo Box
	//
	//X 축을 설정 한다.
	CAxis* pXAxis = m_pQueryCondition->GetXAxis();

	if(!pXAxis->GetTitle().IsEmpty())
	{
		strDefaultXName = pXAxis->GetTitle();
	}

	if(m_XAxisCombo.SelectString(0, strDefaultXName) == CB_ERR)
	{
		m_XAxisCombo.SetCurSel(0);	//
	}
//	m_XAxisCombo.GetLBText(m_XAxisCombo.GetCurSel(), strDefaultXName);
	OnSelchangeXAxisCombo();
	

	 // Y Axis ListBox
	pos = m_pQueryCondition->GetYAxisHead();
	if(pos)		////기존 그래프 조건 재설정시에는 기본 설정을 Reset 시키고 이전 조건을 Diaplay한다.
	{	
		//현재 선택을 Reset 시킨다.
		for(int index =0; index< m_YAxisList.GetCount(); index++)
		{
			m_YAxisList.SetCheck(index, 0);
		}

		while(pos)		
		{
			//Y축과 같은 Item이면 선택 표시한다.
			CAxis* pYAxis = m_pQueryCondition->GetNextYAxis(pos);
			for(int index =0; index< m_YAxisList.GetCount(); index++)
			{
				CString str;
				m_YAxisList.GetText(index, str);
				if(str.CompareNoCase(pYAxis->GetTitle())==0) break;
			}
			m_YAxisList.SetCheck(index, 1);

			CString strQuery;
			strQuery.Format("SELECT UnitTransfer From Property WHERE Index = %d",
							m_YAxisPropertyIndex.GetAt(m_YAxisPropertyIndex.FindIndex(index)).x);
			aRecordset.Open(dbOpenDynaset, strQuery);
			int n=0;
			while(!aRecordset.IsEOF())
			{
				COleVariant var;
				aRecordset.GetFieldValue("UnitTransfer", var);
				if(var.fltVal==pYAxis->GetUnitTransfer()) break;
				aRecordset.MoveNext();
				n++;
			}
			aRecordset.Close();

			//Unit 선택
			m_YAxisList.SetCurSel(index);
			m_YAxisPropertyIndex.GetAt(m_YAxisPropertyIndex.FindIndex(index)).y = n;		//Unit의 Combo Index 저장

			OnSelchangeYAxisList();							//
		}
	}
/*
	//X축 선택에 맞추어 기본적인 표시한다.
////////////////////////////
	if( m_XAxisCombo.GetCurSel() == 1  ) {			//
		m_YAxisList.SetCheck(nYAxisDefaultIndex, 1);//
		m_YAxisList.SetCurSel(nYAxisDefaultIndex);	//
	}												//
	OnSelchangeYAxisList();							//
///////////////////////////////////////////
*/	
	//

	UpdateData(FALSE);

	//CTSMonPro에서 호출시
	//파일은 선택되었지만 cycle 구간이 선택이 안되어 있으면 
	//기존적으로 현재 구간 적용
	if(m_SelCycleList.GetCount() < 1 && m_ChannelListCtrl.GetItemCount() > 0)
	{
		OnAddSelCycleBtn();
	}
	
	//구간을 1개만 선택 하면 구간 그리기 옵션을 Disable 시킴 
/*	if(m_SelCycleList.GetCount() < 2)
	{
		GetDlgItem(IDC_START_POINT_GROUP_STATIC)->EnableWindow(FALSE);
		GetDlgItem(IDC_START_TYPE_RADIO)->EnableWindow(FALSE);
		GetDlgItem(IDC_START_TYPE_RADIO1)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_START_POINT_GROUP_STATIC)->EnableWindow(TRUE);
		GetDlgItem(IDC_START_TYPE_RADIO)->EnableWindow(TRUE);
		GetDlgItem(IDC_START_TYPE_RADIO1)->EnableWindow(TRUE);
		
	}
*/
/*	//////////////////////////////////////////////////////////////////////////
	CString str;							//
	if ( m_lToCycle == MAX_CYCLE_NUM )		//
		str.Format("%d", 1);				//
	else									//
		str.Format("%d",m_lToCycle);		//
	SetDlgItemText(IDC_CYCLE_TO_EDIT, str);	//
//////////////////////////////////////////////////////////////////////////
*/

//	OnTestFromFolderBtn();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

//
void CDataQueryDialog::OnAddSelCycleBtn() 
{
	int iFrom=0, iTo=0, iInterval=0;
	CString strTemp;

	//
	GetDlgItem(IDC_CYCLE_FROM_EDIT)->GetWindowText(strTemp);
	iFrom     = atoi(strTemp);
	GetDlgItem(IDC_CYCLE_TO_EDIT)->GetWindowText(strTemp);
	iTo       = atoi(strTemp);
	GetDlgItem(IDC_CYCLE_INTERVAL_EDIT)->GetWindowText(strTemp);
	iInterval = atoi(strTemp);

	if(iFrom < 0 )
	{
		MessageBox(TEXT_LANG[13], "Error", MB_OK|MB_ICONSTOP);
		GetDlgItem(IDC_CYCLE_FROM_EDIT)->SetFocus();
		((CEdit*)GetDlgItem(IDC_CYCLE_FROM_EDIT))->SetSel(0,-1);
		return;
	}

	if( iTo < 1 )
	{
		MessageBox(TEXT_LANG[14], "Error", MB_OK|MB_ICONSTOP);
		GetDlgItem(IDC_CYCLE_TO_EDIT)->SetFocus();
		((CEdit*)GetDlgItem(IDC_CYCLE_TO_EDIT))->SetSel(0,-1);
		return;
	}

	if( iInterval < 1 )
	{
		MessageBox(TEXT_LANG[15], "Error", MB_OK|MB_ICONSTOP);
		GetDlgItem(IDC_CYCLE_INTERVAL_EDIT)->SetFocus();
		((CEdit*)GetDlgItem(IDC_CYCLE_INTERVAL_EDIT))->SetSel(0,-1);
		return;
	}


	if(iFrom > iTo )
	{
		MessageBox(TEXT_LANG[16], "Error", MB_OK|MB_ICONSTOP);
		GetDlgItem(IDC_CYCLE_TO_EDIT)->SetFocus();
		((CEdit*)GetDlgItem(IDC_CYCLE_TO_EDIT))->SetSel(0,-1);
		return;
	}
	
	// 1~1 : 1 형태 입력
	if(iTo == iFrom && iInterval != 1) 
	{
		MessageBox(TEXT_LANG[17], "Error", MB_OK|MB_ICONSTOP);
		GetDlgItem(IDC_CYCLE_INTERVAL_EDIT)->SetFocus();
		((CEdit*)GetDlgItem(IDC_CYCLE_INTERVAL_EDIT))->SetSel(0,-1);
		return;
	}
	else if(iTo > iFrom)
	{
		if((iTo-iFrom) < iInterval)
		{
			MessageBox(TEXT_LANG[17], "Error", MB_OK|MB_ICONSTOP);
			GetDlgItem(IDC_CYCLE_INTERVAL_EDIT)->SetFocus();
			((CEdit*)GetDlgItem(IDC_CYCLE_INTERVAL_EDIT))->SetSel(0,-1);
			return;
		}
	}

	//
	strTemp.Format("%d~%d:%d", iFrom, iTo, iInterval);
	
	POSITION pos = m_strlistSelCycle.GetHeadPosition();
	while(pos)
	{
		if(strTemp == m_strlistSelCycle.GetNext(pos))
		{
//			MessageBox("이미 추가된 구간입니다.", "Error", MB_OK|MB_ICONWARNING);
			return;
		}
	}

	m_SelCycleList.AddString(strTemp);
	m_strlistSelCycle.AddTail(strTemp);
	
	GetDlgItem(IDC_CYCLE_FROM_EDIT)->SetFocus();
	((CEdit*)GetDlgItem(IDC_CYCLE_FROM_EDIT))->SetSel(0,-1);

	//구간을 1개만 선택 하면 구간 그리기 옵션을 Disable 시킴 
/*	if(m_SelCycleList.GetCount() < 2)
	{
		GetDlgItem(IDC_START_POINT_GROUP_STATIC)->EnableWindow(FALSE);
		GetDlgItem(IDC_START_TYPE_RADIO)->EnableWindow(FALSE);
		GetDlgItem(IDC_START_TYPE_RADIO1)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_START_POINT_GROUP_STATIC)->EnableWindow(TRUE);
		GetDlgItem(IDC_START_TYPE_RADIO)->EnableWindow(TRUE);
		GetDlgItem(IDC_START_TYPE_RADIO1)->EnableWindow(TRUE);
		
	}
*/
}

void CDataQueryDialog::OnDelSelCycleBtn()
{
	int iCurSel = m_SelCycleList.GetCurSel();

	CString strTemp;

	if(iCurSel!=LB_ERR)
	{
		POSITION pos = m_strlistSelCycle.FindIndex(iCurSel);
		ASSERT(pos);
		m_strlistSelCycle.RemoveAt(pos);

		m_SelCycleList.DeleteString(iCurSel);
		m_SelCycleList.SetCurSel(0);
		GetDlgItem(IDC_DEL_SEL_CYCLE_BTN)->SetFocus();
	}
	
	//구간을 1개만 선택 하면 구간 그리기 옵션을 Disable 시킴 
/*	if(m_SelCycleList.GetCount() < 2)
	{
		GetDlgItem(IDC_START_POINT_GROUP_STATIC)->EnableWindow(FALSE);
		GetDlgItem(IDC_START_TYPE_RADIO)->EnableWindow(FALSE);
		GetDlgItem(IDC_START_TYPE_RADIO1)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_START_POINT_GROUP_STATIC)->EnableWindow(TRUE);
		GetDlgItem(IDC_START_TYPE_RADIO)->EnableWindow(TRUE);
		GetDlgItem(IDC_START_TYPE_RADIO1)->EnableWindow(TRUE);
		
	}
*/

}

void CDataQueryDialog::OnCycleStringCheck() 
{
	GetDlgItem(IDC_CYCLE_STRING_EDIT)->EnableWindow(((CButton*)GetDlgItem(IDC_CYCLE_STRING_CHECK))->GetCheck());
}

void CDataQueryDialog::OnDestroy() 
{
	CDialog::OnDestroy();
	
	if(m_pDatabase!=NULL)
	{
		if(m_pDatabase->IsOpen()) m_pDatabase->Close();
		delete m_pDatabase;
		m_pDatabase=NULL;
	}

	if(m_pbyXAxisIndex!=NULL)
	{
		delete [] m_pbyXAxisIndex;
		m_pbyXAxisIndex = NULL;
	}
}

void CDataQueryDialog::OnSelchangeXAxisCombo() 
{
	int iCurSel = m_XAxisCombo.GetCurSel();
	if(iCurSel != CB_ERR)
	{
		CDaoRecordset aRecordset(m_pDatabase);
		CString strQuery;
		strQuery.Format("SELECT YAxisIndexList, PropertyIndex FROM AxisInfo WHERE Index = %d" ,
			            m_pbyXAxisIndex[iCurSel]);
		aRecordset.Open(dbOpenDynaset,strQuery);
			//
			COleVariant var;
			aRecordset.GetFieldValue("YAxisIndexList", var);
			CString strYAxisIndex   = (LPCTSTR)(LPTSTR)var.bstrVal;
			var.Clear();
			//
			aRecordset.GetFieldValue("PropertyIndex", var);
			BYTE    byPropertyIndex = var.bVal;
			var.Clear();
		aRecordset.Close();

		// X-Unit ComboBox
		m_XUnitCombo.ResetContent();
		strQuery.Format("SELECT UnitNotation FROM Property WHERE Index = %d", byPropertyIndex);
		aRecordset.Open(dbOpenDynaset, strQuery);
			while(!aRecordset.IsEOF())
			{
				COleVariant var;
				aRecordset.GetFieldValue("UnitNotation", var);
				m_XUnitCombo.AddString((LPCTSTR)(LPTSTR)var.bstrVal);
				aRecordset.MoveNext();
			}
		aRecordset.Close();
//		m_XUnitCombo.SetCurSel(0);

//////////////////////////////////////////////////////////////////////////
//	사용자가 이전에 사용한 설정을 Registry에서 read하여 기본적으로 표시해준다.
//////////////////////////////////////////////////////////////////////////

		//선택 X축 저장 (다음 시작시 기본적으로 이축을 선택하도록 하기 위해)
		CString strDefaultXName;
		m_XAxisCombo.GetLBText(iCurSel, strDefaultXName);
/*		if(strDefaultXName.CompareNoCase("Cycle") == 0)
		{
			GetDlgItem(IDC_CYCLE_WARRING_STATIC)->ShowWindow(SW_SHOW);
		}
		else
		{
			GetDlgItem(IDC_CYCLE_WARRING_STATIC)->ShowWindow(SW_HIDE);			
		}
*/
		int p1=0, p2=0;
		//X축 선택에 따른 기본 설정 Setting
		CString strTemp, strDefaultXSet, strDefaultYSet;
		strTemp = AfxGetApp()->GetProfileString(QUERY_REG_SECTION, strDefaultXName, "sec,0,1,:0,0,");
		strDefaultXSet = strTemp.Left(strTemp.Find(':'));		//X축 이전 설정 상태 
		strDefaultYSet = strTemp.Mid(strTemp.Find(':')+1);		//Y축 이전 설정 Unit

		//X축 Unit 설정
		CAxis* pXAxis = m_pQueryCondition->GetXAxis();
		CString strXUnit, strDefaultXUnit;
		strXUnit = pXAxis->GetUnitNotation();

		//X축 default Unit 설정
		p2=strDefaultXSet.Find(',',p1);
		strDefaultXUnit = strDefaultXSet.Mid(p1,p2-p1);
		if(strXUnit.IsEmpty())
		{
			strXUnit = strDefaultXUnit;
		}
		if(m_XUnitCombo.SelectString(0, strXUnit) == CB_ERR)
		{
			m_XUnitCombo.SetCurSel(0);
		}
		
		//default y축 선택 Item
		CByteArray	yAxisArray, yUnitArray;
		while(p2 >= 0)
		{
			p1=p2+1;		
			p2=strDefaultXSet.Find(',',p1);
			if(p2 < 0)	break;
			yAxisArray.Add((BYTE)atoi(strDefaultXSet.Mid(p1,p2-p1)));
		}

		//Y축 default Unit을 구한다.
		p1=0, p2=0;
		while(p2 >= 0)
		{
			p2=strDefaultYSet.Find(',',p1);
			if(p2 < 0)	break;
			CString s = strDefaultYSet.Mid(p1,p2-p1);
			yUnitArray.Add((BYTE)atoi(strDefaultYSet.Mid(p1,p2-p1)));
			p1=p2+1;		
		}
//////////////////////////////////////////////////////////////////////////		

		// Y-Axis ListBox
		m_YAxisList.ResetContent();
		m_YAxisPropertyIndex.RemoveAll();
		p1=0;
		p2=0;
		int cnt = 0;
		while(TRUE)
		{
			p2=strYAxisIndex.Find(',',p1);
			if(p2 == -1) break;
			BYTE index = (BYTE) atoi(strYAxisIndex.Mid(p1,p2-p1));
			strQuery.Format("SELECT Name, PropertyIndex From AxisInfo WHERE Index = %d", index);
			aRecordset.Open(dbOpenDynaset, strQuery);
			COleVariant var;
			aRecordset.GetFieldValue("Name",var);
			m_YAxisList.AddString((LPCTSTR)(LPTSTR)var.bstrVal);
			var.Clear();
			aRecordset.GetFieldValue("PropertyIndex", var);
			
			//기본적으로 각 축의 Unit은 Combo 0을 선택하고 이전 설정이 있으면 이전 설정을 기본으로 선택한다.
			CPoint pt(var.bVal, 0);
			if(cnt < yUnitArray.GetSize())
			{
				pt.y = yUnitArray[cnt];			//(Propoerty Index, Unit Combo Index)
			}
			cnt++;
			m_YAxisPropertyIndex.AddTail(pt);
			aRecordset.Close();
			p1=p2+1;
		}

		//Y축 기본 설정
		for(int i=0; i<yAxisArray.GetSize(); i++)
		{
			m_YAxisList.SetCheck(yAxisArray[i], 1);
		}

		// Y-Unit Combobox
		// m_YUnitCombo.ResetContent();
		if(yAxisArray.GetSize() > 0)
		{
			m_YAxisList.SetCurSel(yAxisArray[yAxisArray.GetSize()-1]);
			OnSelchangeYAxisList();							//
		}
	}
}

void CDataQueryDialog::OnSelchangeYAxisList() 
{
	int iCurSel = m_YAxisList.GetCurSel();
	if(iCurSel!=LB_ERR)
	{
		//Item이 선택되지 않았을시 
		m_YUnitCombo.ResetContent();
		
		//Item이 선택되었으면
		if(1 == m_YAxisList.GetCheck(iCurSel))
		{
			POSITION pos = m_YAxisPropertyIndex.FindIndex(iCurSel);
			CPoint pt    = m_YAxisPropertyIndex.GetAt(pos);
			BYTE index   = (BYTE) pt.x;

			//
			CDaoRecordset aRecordset(m_pDatabase);
			CString strQuery;
			strQuery.Format("SELECT UnitNotation FROM Property WHERE Index = %d", index);
			aRecordset.Open(dbOpenDynaset, strQuery);
				while(!aRecordset.IsEOF())
				{
					COleVariant var;
					aRecordset.GetFieldValue("UnitNotation", var);
					m_YUnitCombo.AddString((LPCTSTR)(LPTSTR)var.bstrVal);
					aRecordset.MoveNext();
				}
			aRecordset.Close();

			//
			m_YUnitCombo.SetCurSel(pt.y);
		}
	}
}

void CDataQueryDialog::OnSelchangeYUnitCombo() 
{
	int iCurYAxisSel = m_YAxisList.GetCurSel();
	if(iCurYAxisSel!=LB_ERR)
	{
		int iCurYUnitSel = m_YUnitCombo.GetCurSel();
		if(iCurYUnitSel!=CB_ERR)
		{
			POSITION pos = m_YAxisPropertyIndex.FindIndex(iCurYAxisSel);
			m_YAxisPropertyIndex.GetAt(pos).y= iCurYUnitSel;
		}
	}
}

//formaiton 수정 완료 
//20081028
void CDataQueryDialog::OnTestFromFolderBtn() 
{	
	//이전 선택 Folder 기본 지정
	CString strPath = AfxGetApp()->GetProfileString("Control", "ResultDataPath", "");

	CFolderDialog dlg(strPath);
	if(dlg.DoModal() == IDCANCEL)	return;

	//기존 선택에 Add or 새롭게 추가 
	m_strlistTestPath.RemoveAll();
	m_TestNameListCtrl.DeleteAllItems();
	m_ChannelListCtrl.DeleteAllItems();
	m_SelCycleList.ResetContent();

	strPath = dlg.GetPathName();
		
	int aa = 0;
//	while(pos)
//	{	
//		strPath = dlg.GetSelList()->GetNext(pos);
		m_strlistTestPath.AddTail(strPath);
		AddTestPath(strPath, aa++);	//시험목록 리스트와 채널 리스트에 선택 폴더 내용 추가 
//	}
	
	SortChannelListCtrl();	// 표기된 채널 리스트 목록을 sort 시킨다.

	OnAddSelCycleBtn();		// cycle List 추가 

	AfxGetApp()->WriteProfileString("Control", "ResultDataPath", strPath);

	return;
}

// 2005/04/20 
// 선택 폴더를 시험명 리스트에 추가 하고 
// 시험을 실시한 채널 리스트를 채널 리스트 폴더에 추가 한다.
void CDataQueryDialog::AddTestPath(LPCTSTR path, int nIndex)
{
	//
	int nItem = m_TestNameListCtrl.GetItemCount();
	
	CString strLabel;
	strLabel.Format("%d", nItem+1);

	//파일 수를 센다.
	int nFileTotCount =0;
	int nFilecount = 0;
	CString str;
	str.Format("%s\\%s", path, "CH*.csv");
	
	CFileFind afinder;
	BOOL bWork = afinder.FindFile(str);
	while(bWork)
	{
		bWork          = afinder.FindNextFile();
		nFileTotCount++;
	}

	//파일이 하나도 없으면 return
	if(nFileTotCount <=0)	return;

	LVITEM lvitem;
	//1 컬럼에 Count 입기
	lvitem.mask  = LVIF_TEXT;
	lvitem.iItem = nItem;
	lvitem.iSubItem = 0;
	lvitem.pszText = (LPTSTR)(LPCTSTR)strLabel;
	m_TestNameListCtrl.InsertItem(&lvitem);

	//컬럼 2에 Test명(폴더명) 표기 
	CString strPath(path);
	CString strName = strPath.Mid(strPath.ReverseFind('\\')+1);
	m_TestNameListCtrl.SetItemText(nItem, 1, strName);
	m_strTestName = strName;

	//선택 폴더 채널 파일 검색 
	int iStartItem = m_ChannelListCtrl.GetItemCount();
	int iMaxCycle = 0;
	int nMinCycle = 0;		//

//////////////////////////////////////////////////////////////////////////
	CProgressWnd *pProgressWnd = new CProgressWnd;
	pProgressWnd->Create(this, "Loading...", TRUE);
	pProgressWnd->SetText(TEXT_LANG[18]);
	pProgressWnd->Show();
//////////////////////////////////////////////////////////////////////////

	int i=0;
	bWork = afinder.FindFile(str);
	while(bWork)
	{
		bWork          = afinder.FindNextFile();
//////////////////////////////////////////////////////////////////////////		
		pProgressWnd->SetPos(int((float)nFilecount++/(float)nFileTotCount*100.0f));
		if(pProgressWnd->PeekAndPump(FALSE) == FALSE)
		{
			break;
		}
//////////////////////////////////////////////////////////////////////////
		CString ChPath = afinder.GetFileName();
		//최종 data를 얻기위한 별도의 함수를 사용
		pProgressWnd->SetText(TEXT_LANG[18]+afinder.GetFileName());
		
		CTable *record = CChData::GetLastDataRecord(afinder.GetFilePath());
		float fSize = afinder.GetLength()/1000.0f;
		if(fSize < 1)	fSize = 1.0f;

		if(record != NULL)
		{
			long cycle = 0;
			cycle = (long)record->GetStepNo();

			//20090701
			//Online 동작 Data는 최종 StepNo를 구할 수가 없다.
			if(cycle < 2 && record->GetType() == 0x14)
			{
				cycle = 100;
			}


			if(cycle > 0)
			{
				lvitem.iItem = iStartItem+i;

					//번호 표기 
				lvitem.iSubItem = 0;
				lvitem.mask  = LVIF_TEXT|LVIF_PARAM;
				lvitem.pszText = (LPTSTR)(LPCTSTR)m_strTestName;
				m_ChannelListCtrl.InsertItem(&lvitem);
				m_ChannelListCtrl.SetItemData(lvitem.iItem, nIndex);

				//채널명 입력
				ChPath.MakeUpper();
				m_ChannelListCtrl.SetItemText(lvitem.iItem, 1, ChPath);

				//진행 Total Step 구함
				//////////////////////////////////////////////////////////////////////////
				if(cycle < nMinCycle)	nMinCycle = cycle;
				if(cycle > iMaxCycle)	iMaxCycle = cycle;
				//////////////////////////////////////////////////////////////////////////
					
				str.Format("%d", cycle);
				//20090710
				//OnLine TEST 결과는 MAX Step으로 표시
				if(cycle >= 100)	str = "MAX";
				//////////////////////////////////////////////////////////////////////////
				m_ChannelListCtrl.SetItemText(lvitem.iItem, 2, str);

				str.Format("%dKB", (int)fSize);
				m_ChannelListCtrl.SetItemText(lvitem.iItem, 3, str);

				i++;
			}
			delete record;
		}
	}

//////////////////////////////////////////////////////////////////////////
	pProgressWnd->SetPos(100);
	pProgressWnd->Hide();
	delete pProgressWnd;
	pProgressWnd = NULL;
//////////////////////////////////////////////////////////////////////////

 	m_lToCycle = iMaxCycle;
	str.Format("%d", m_lToCycle);
	GetDlgItem(IDC_CYCLE_TO_EDIT)->SetWindowText(str);
}

int CALLBACK CDataQueryDialog::ChannelCompareProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;

   if(pListCtrl->GetItemCount()<2) return 0;

   CString strItem1 = pListCtrl->GetItemText(lParam1, 0);
   CString strItem2 = pListCtrl->GetItemText(lParam2, 0);

   int result = strcmp(strItem1, strItem2);
   if(result==0)
   {
		strItem1 = pListCtrl->GetItemText(lParam1, 1);
		strItem2 = pListCtrl->GetItemText(lParam2, 1);
		result = strcmp(strItem1, strItem2);
   }

   return result;
}

void CDataQueryDialog::SortChannelListCtrl()
{
	
/*	for (int i=0;i < m_ChannelListCtrl.GetItemCount();i++)
	{
	   m_ChannelListCtrl.SetItemData(i, i);
	}
*/	m_ChannelListCtrl.SortItems(ChannelCompareProc, (LPARAM)(&m_ChannelListCtrl));

	//모두 선택 상태로 만든다.
	for (int i = 0; i < m_ChannelListCtrl.GetItemCount(); i++)
	{
		m_ChannelListCtrl.SetItemState(i,LVIS_SELECTED,LVIS_SELECTED);
	}
}

void CDataQueryDialog::OnItemchangedChannelList(NMHDR* pNMHDR, LRESULT* pResult) 
{
//	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	int a = m_ChannelListCtrl.GetSelectedCount();
	CString msg;
	msg.Format(TEXT_LANG[19], a);
	GetDlgItem(IDC_SEL_COUNT_STATIC)->SetWindowText(msg);
	*pResult = 0;
}

//사용자가 선택한 축의 설정을 저장한다.
void CDataQueryDialog::WriteRegUserSet()
{
	//축 선택 저장 
	int iCurSel = m_XAxisCombo.GetCurSel();
	if(iCurSel != CB_ERR)
	{
		CString strDefaultXName;
		m_XAxisCombo.GetLBText(iCurSel, strDefaultXName);
		AfxGetApp()->WriteProfileString(QUERY_REG_SECTION, "X Axis", strDefaultXName);

		//y축 설정 저장
		CString strTemp, strMsg;
		m_XUnitCombo.GetLBText(m_XUnitCombo.GetCurSel(), strTemp);
		strMsg.Format("%s,", strTemp);
		
		for(int i = 0; i < m_YAxisList.GetCount(); i++)
		{
			if(m_YAxisList.GetCheck(i))
			{
				strTemp.Format("%d,", i);
				strMsg += strTemp;
			}
		}
		strMsg += ":";
	
		POSITION pos = m_YAxisPropertyIndex.GetHeadPosition();
		while(pos)
		{
			CPoint pt = m_YAxisPropertyIndex.GetNext(pos);
			strTemp.Format("%d,", pt.y);
			strMsg += strTemp;
		}
			
		AfxGetApp()->WriteProfileString(QUERY_REG_SECTION, strDefaultXName, strMsg);
	}
}

void CDataQueryDialog::OnBtnExcelExport() 
{
	// TODO: Add your control notification handler code here
	
	// 1. *.csv 파일로 변환하기 위한 파일들을 Loading
	// 2. 파일을 메모리 공간에 저장한 후
	// 3. *.csv 파일로 저장한다.

	POSITION pos;
	int nIndex = 0, nItem = 0;
	int i=0;
	int p=0;
	int nTemp = 0;
	CString strChPath;

	int nSelectItemCnt = 0;
	CString strTemp = _T("");
	CString strData = _T("");
	CString strLog = _T("");
	int nChIndex = 0;

	byte nChNum[32] = {0,};
	CStdioFile rfile[32];
	CStdioFile wfile;

	pos = m_ChannelListCtrl.GetFirstSelectedItemPosition();

	while(pos)
	{
		nItem  = m_ChannelListCtrl.GetNextSelectedItem(pos);
		nIndex = m_ChannelListCtrl.GetItemData(nItem);
		
		strChPath = m_strlistTestPath.GetAt(m_strlistTestPath.FindIndex(nIndex)) + "\\" + m_ChannelListCtrl.GetItemText(nItem,1);	
		strTemp = m_ChannelListCtrl.GetItemText(nItem,1);

		if( rfile[nSelectItemCnt].Open(strChPath, CFile::modeRead|CFile::typeText|CFile::shareDenyNone) )
		{
			p = strTemp.Find('_', 0);
			nChIndex = atol(strTemp.Mid(p-3,p)) - 1;
			nChNum[nChIndex] = 1;
			nSelectItemCnt++;
		}
	}
	
	if( nSelectItemCnt == 0 || nSelectItemCnt == 1 )
	{
		AfxMessageBox(TEXT_LANG[20], MB_ICONINFORMATION);
		return;
	}
		
	CString strfileName = _T("");
	int nSize = 0;
	
	COleDateTime nowtime(COleDateTime::GetCurrentTime());
	strfileName.Format("Noname_%s.csv", nowtime.Format("%y%m%d%H%M"));

	CFileDialog dlg(false, "", strfileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "csv 파일(*.csv)|*.csv|All Files (*.*)|*.*|");

	if(dlg.DoModal() == IDOK)
	{
		BeginWaitCursor();

		bool bFirstData = false;

		strfileName = dlg.GetPathName();		
		wfile.Open(strfileName, CFile::modeCreate|CFile::modeReadWrite|CFile::modeNoTruncate);
		
		while(TRUE)
		{
			rfile[0].ReadString(strData);
			nSize = strData.Find(',');
			if( nSize < 1 )
			{	break;	}

			if( bFirstData == false )
			{
				bFirstData = true;

				nSize = GetFindCharCount( strData, ',' );
				
				for( i=0; i<32; i++ )
				{
					if( nChNum[i] == 1 )
					{
						nTemp = nSize;
						strTemp.Format("CH_%03d", i+1);
						while( nTemp>0 )
						{
							strTemp += ",";
							nTemp--;
						}
						
						wfile.WriteString(strTemp);
					}
				}
				
				wfile.WriteString("\n");
			}

			wfile.WriteString(strData);
			
			for( i=1; i<nSelectItemCnt; i++ )
			{
				rfile[i].ReadString(strData);
				
				if( i == nSelectItemCnt-1 )
				{
					wfile.WriteString(strData+"\n");
				}
				else
				{
					wfile.WriteString(strData);
				}
			}
		}	

		wfile.Close();

		EndWaitCursor();
	}
	else
	{
		for( i=0; i<nSelectItemCnt; i++ )
		{
			rfile[i].Close();
		}

		return;
	}
	
	for( i=0; i<nSelectItemCnt; i++ )
	{
		rfile[i].Close();
	}

	strLog.Format( TEXT_LANG[21], strfileName );
	AfxMessageBox( strLog, MB_ICONINFORMATION );
}

int CDataQueryDialog::GetFindCharCount(CString msg, char find_char)
{
	int msg_len = msg.GetLength();
	int find_cnt = 0;
	
	for(int i =0 ; i<msg_len ; i++)
	{
		if(msg[i] == find_char)
		{
			find_cnt++;
		}
	}
	return find_cnt;  
 }

BOOL CDataQueryDialog::LanguageinitMonConfig() 
{
	g_nLanguage = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Language", 0);
	g_strLangPath.Format("%s\\Lang\\GraphAnalyzer_Lang.ini", AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC ,"Path", "C:\Program Files\PNE CTS"));

	switch(g_nLanguage)
	{
	case 0:
		{
			SetThreadUILanguage( MAKELCID( MAKELANGID(LANG_KOREAN , SUBLANG_KOREAN) , SORT_DEFAULT));
			break;
		}

	case 1:
		{
			SetThreadUILanguage( MAKELCID( MAKELANGID(LANG_ENGLISH , SUBLANG_ENGLISH_US) , SORT_DEFAULT));
			break;
		}

	case 2:
		{
			SetThreadUILanguage( MAKELCID( MAKELANGID(LANG_CHINESE_SIMPLIFIED , SUBLANG_CHINESE_SIMPLIFIED) , SORT_DEFAULT));
			break;
		}
	}

	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	CString strMsg = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_DataQueryDlg"), _T("TEXT_DataQueryDlg_CNT"), _T("TEXT_DataQueryDlg_CNT"));
	if( strTemp == "TEXT_DataQueryDlg_CNT" )
	{
		return false;
	}


	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt];		

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_DataQueryDlg_%d", i);
			strMsg = GIni::ini_GetLangText(g_strLangPath, _T("IDD_DataQueryDlg"), strTemp, strTemp, g_nLanguage);
			TEXT_LANG[i] = strMsg;

			if( TEXT_LANG[i] == strTemp || strMsg.IsEmpty() )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Languge error ====> " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}