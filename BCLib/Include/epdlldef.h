#ifndef _ELCDEF_
#define _ELCDEF_

#include <DataForm.h>
#include <BFChannelCode.h>

#define FILE_CREATE		0x01
#define FILE_APPEND		0x02

#ifdef __cplusepluse
extern "C" {
#endif ///__cplusepluse

#ifdef EXPORT_EPDLL_API
#define EPDLL_API	__declspec(dllexport)
#else	//EXPORT_EPDLL_API
#define EPDLL_API	__declspec(dllimport)
#endif	//EXPORT_EPDLL_API

EPDLL_API long ExpFloattoLong(float fvalue, int exponential);
EPDLL_API float ExpLongToFloat(long ldata, int exponential);
EPDLL_API int WriteLogFile(char *szFileName, char *szData, int nMode);
EPDLL_API void ConvertTime(char *szTime, unsigned long time);
EPDLL_API char *WSALastErrorString(int nLastError);
EPDLL_API void FileSize(char *szString, unsigned long nSize);
EPDLL_API void myGetFileName(char *szFullName);
EPDLL_API void myGetPathName(char *szFullName);
EPDLL_API BOOL configFileName(char *szSetFileName);
EPDLL_API char* getconfigFileName();

//EPDLL_API STR_TOP_CONFIG  GetTopConfig();
//EPDLL_API BOOL	WriteTopConfig(STR_TOP_CONFIG topConfig);
//EPDLL_API STR_LOGIN ReadLastLogID();
//EPDLL_API BOOL WriteLastLogID(STR_LOGIN logData);
//EPDLL_API STR_TOP_CONFIG GetDefaultTopConfigSet();

EPDLL_API	BOOL IsSysFail(BYTE code);
EPDLL_API	BOOL IsNonCell(BYTE code);
EPDLL_API	BOOL IsFailCell(BYTE code);
EPDLL_API	BOOL IsNormalCell(BYTE code);
EPDLL_API	BOOL IsCellCheckFail(BYTE code);

EPDLL_API CString GetSelectCodeName(BYTE code);
EPDLL_API BOOL IsSelectBadCell (BYTE code);
EPDLL_API BOOL IsSelectNormalCell (BYTE code);

EPDLL_API CString EmgMsg(int nCode);
EPDLL_API CString DoorStateMsg(WORD State);
EPDLL_API CString TrayStateMsg(WORD State);
EPDLL_API CString JigStateMsg(WORD state);
EPDLL_API CString StepTypeMsg(int nType);
EPDLL_API CString ModeTypeMsg( int nType, int nMode);

//2003/5/26 KBH
EPDLL_API CString ModuleFailMsg(int nCode);

#ifdef __cplusepluse
}	// extern "C" {
#endif //__cplusepluse

#endif	//end _ELCDEF_