//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by BFGraphCom.rc
//
#define IDD_TOP_CONFIG_DLG              159
#define IDI_COLOR_ICON                  198
#define IDB_YELLOW_BAR                  217
#define IDB_RED_BAR                     218
#define IDC_EDIT_C_ENDPOW               1024
#define IDC_I_OVER_COLOR                1059
#define IDC_V_RANGE                     1075
#define IDC_V_OVER_TCOLOR               1124
#define IDC_I_OVER_TCOLOR               1125
#define IDC_V_OVER_RESULT               1126
#define IDC_I_OVER_RESULT               1127
#define IDC_V_OVER_BLINK                1128
#define IDC_I_OVER_BLINK                1129
#define IDC_RESULT_EDIT1                1130
#define IDC_RESULT_EDIT2                1131
#define IDC_RESULT_EDIT3                1132
#define IDC_RESULT_EDIT4                1133
#define IDC_RESULT_EDIT5                1134
#define IDC_RESULT_EDIT6                1135
#define IDC_RESULT_EDIT7                1136
#define IDC_RESULT_EDIT8                1137
#define IDC_RESULT_EDIT9                1138
#define IDC_RESULT_EDIT10               1139
#define IDC_RESULT_EDIT11               1140
#define IDC_RESULT_EDIT12               1141
#define IDC_RESULT_EDIT13               1142
#define IDC_RESULT_EDIT14               1143
#define IDC_RESULT_EDIT15               1144
#define IDC_VI_RANGE_CHECK              1205
#define IDC_V_OVER_COLOR                1206
#define IDC_I_RANGE                     1207
#define IDC_TOPCFG_BCOLOR1              1278
#define IDC_TOPCFG_BCOLOR2              1279
#define IDC_TOPCFG_BCOLOR3              1280
#define IDC_TOPCFG_BCOLOR4              1281
#define IDC_TOPCFG_BCOLOR5              1282
#define IDC_TOPCFG_BCOLOR6              1283
#define IDC_TOPCFG_BCOLOR7              1284
#define IDC_TOPCFG_BCOLOR8              1285
#define IDC_TOPCFG_BCOLOR9              1286
#define IDC_TOPCFG_BCOLOR10             1287
#define IDC_TOPCFG_BCOLOR11             1288
#define IDC_TOPCFG_BCOLOR12             1289
#define IDC_TOPCFG_BCOLOR13             1290
#define IDC_TOPCFG_BCOLOR14             1291
#define IDC_TOPCFG_BCOLOR15             1292
#define IDC_TOPCFG_INVERSE1             1303
#define IDC_TOPCFG_INVERSE2             1304
#define IDC_TOPCFG_INVERSE3             1305
#define IDC_TOPCFG_INVERSE4             1306
#define IDC_TOPCFG_INVERSE5             1307
#define IDC_TOPCFG_INVERSE6             1308
#define IDC_TOPCFG_INVERSE7             1309
#define IDC_TOPCFG_INVERSE8             1310
#define IDC_TOPCFG_INVERSE9             1311
#define IDC_TOPCFG_INVERSE10            1312
#define IDC_TOPCFG_INVERSE11            1313
#define IDC_TOPCFG_INVERSE12            1314
#define IDC_TOPCFG_INVERSE13            1315
#define IDC_TOPCFG_INVERSE14            1316
#define IDC_TOPCFG_INVERSE15            1317
#define IDC_TOPCFGE_RESULT1             1318
#define IDC_TOPCFGE_RESULT2             1319
#define IDC_TOPCFGE_RESULT3             1320
#define IDC_TOPCFGE_RESULT4             1321
#define IDC_TOPCFGE_RESULT5             1322
#define IDC_TOPCFGE_RESULT6             1323
#define IDC_TOPCFGE_RESULT7             1324
#define IDC_TOPCFGE_RESULT8             1325
#define IDC_TOPCFGE_RESULT9             1326
#define IDC_TOPCFGE_RESULT10            1327
#define IDC_TOPCFGE_RESULT11            1328
#define IDC_TOPCFGE_RESULT12            1329
#define IDC_TOPCFGE_RESULT13            1330
#define IDC_TOPCFGE_RESULT14            1331
#define IDC_TOPCFGE_RESULT15            1332
#define IDC_TOPCFG_TCOLOR1              1333
#define IDC_TOPCFG_TCOLOR2              1334
#define IDC_TOPCFG_TCOLOR3              1335
#define IDC_TOPCFG_TCOLOR4              1336
#define IDC_TOPCFG_TCOLOR5              1337
#define IDC_TOPCFG_TCOLOR6              1338
#define IDC_TOPCFG_TCOLOR7              1339
#define IDC_TOPCFG_TCOLOR8              1340
#define IDC_TOPCFG_TCOLOR9              1341
#define IDC_TOPCFG_TCOLOR10             1342
#define IDC_TOPCFG_TCOLOR11             1343
#define IDC_TOPCFG_TCOLOR12             1344
#define IDC_TOPCFG_TCOLOR13             1345
#define IDC_TOPCFG_TCOLOR14             1346
#define IDC_TOPCFG_TCOLOR15             1347
#define IDC_TOPCFG_DISPLAY_COLOR        1352
#define IDC_TOPCFG_DISPLAY_TEXT         1353
#define IDC_TOPCFG_SETDEFAULT           1354
#define IDI_CON_VIEW_ICON               4004
#define IDC_I_UNIT_STATIC               4004
#define IDB_CELL_STATE_ICON_P           4005

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        4006
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         4005
#define _APS_NEXT_SYMED_VALUE           4000
#endif
#endif
