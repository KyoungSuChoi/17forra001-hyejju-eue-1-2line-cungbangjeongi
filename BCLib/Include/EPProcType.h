//////////Procedure_Type_HEADER_FILE//////////////
//												//	
//	EPProcType.h								//	
//	Elico Power Formation System Procedure Type	//	
//	Byung Hum - Kim		2001.7.11				//
//												//			
//////////////////////////////////////////////////

//Elico Power Preocedure Type 
#ifndef _EP_PROCEDURE_TYPE_DEFINE_H_
#define _EP_PROCEDURE_TYPE_DEFINE_H_

#define EP_REPORT_GRADING				10000			//종합 등급 Code 
#define EP_REPORT_CH_CODE				10001			//최종 Cell Code

//Charge 공정
#define EP_PROC_TYPE_CHARGE_LOW		0x10000
#define EP_PROC_TYPE_CHARGE_HIGH	0x1FFFF

#define EP_PROC_TYPE_DISCHARGE_LOW	0x20000
#define EP_PROC_TYPE_DISCHARGE_HIGH	0x2FFFF

#define EP_PROC_TYPE_REST_LOW		0x30000
#define EP_PROC_TYPE_REST_HIGH		0x3FFFF

#define EP_PROC_TYPE_OCV_LOW		0x40000
#define EP_PROC_TYPE_OCV_HIGH		0x4FFFF

#define EP_PROC_TYPE_IMPEDANCE_LOW	0x50000
#define EP_PROC_TYPE_IMPEDANCE_HIGH	0x5FFFF

#define EP_PROC_TYPE_END_LOW		0x60000
#define EP_PROC_TYPE_END_HIGH		0x6FFFF

#define EP_PROC_TYPE_AGING_LOW		0x70000
#define EP_PROC_TYPE_AGING_HIGH		0x7FFFF

#define EP_PROC_TYPE_GRADING_LOW	0x80000
#define EP_PROC_TYPE_GRADING_HIGH	0x8FFFF

#define EP_PROC_TYPE_SELECTING_LOW	0x90000
#define EP_PROC_TYPE_SELECTING_HIGH	0x9FFFF

#define EP_PROC_TYPE_NONE				0

#define GetStepTypeofProcType(type)	((int)type>>16)
#define IsNoneProc(type)			(GetStepTypeofProcType(type) == EP_TYPE_NONE ? TRUE : FALSE)
#define IsChargeProc(type)			(GetStepTypeofProcType(type) == EP_TYPE_CHARGE ? TRUE : FALSE)
#define IsDischargeProc(type)		(GetStepTypeofProcType(type) == EP_TYPE_DISCHARGE ? TRUE : FALSE)
#define IsRestProc(type)			(GetStepTypeofProcType(type) == EP_TYPE_REST ? TRUE : FALSE)
#define IsOcvProc(type)				(GetStepTypeofProcType(type) == EP_TYPE_OCV ? TRUE : FALSE)
#define IsImpedanceProc(type)		(GetStepTypeofProcType(type) == EP_TYPE_IMPEDANCE ? TRUE : FALSE)
#define IsEndProc(type)				(GetStepTypeofProcType(type) == EP_TYPE_END ? TRUE : FALSE)
#define IsAgingProc(type)			(GetStepTypeofProcType(type) == EP_TYPE_AGING ? TRUE : FALSE)
#define IsGradingProc(type)			(GetStepTypeofProcType(type) == EP_TYPE_GRADING ? TRUE : FALSE)
#define IsSelectingProc(type)		(GetStepTypeofProcType(type) == EP_TYPE_SELECTING ? TRUE : FALSE)

#define EP_PROC_TYPE_OCV1	(EP_PROC_TYPE_OCV_LOW)
#define EP_PROC_TYPE_OCV2	(EP_PROC_TYPE_OCV_LOW+1)
#define EP_PROC_TYPE_OCV3	(EP_PROC_TYPE_OCV_LOW+2)
#define EP_PROC_TYPE_OCV4	(EP_PROC_TYPE_OCV_LOW+3)


#endif _EP_PROCEDURE_TYPE_DEFINE_H_