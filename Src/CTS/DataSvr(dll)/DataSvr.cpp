// DataSvr.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "DataSvr.h"

#include "Worker.h"
#include "ThreadDispatcher.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//
//	Note!
//
//		If this DLL is dynamically linked against the MFC
//		DLLs, any functions exported from this DLL which
//		call into MFC must have the AFX_MANAGE_STATE macro
//		added at the very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//

/////////////////////////////////////////////////////////////////////////////
// CDataSvrApp

BEGIN_MESSAGE_MAP(CDataSvrApp, CWinApp)
	//{{AFX_MSG_MAP(CDataSvrApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDataSvrApp construction

CDataSvrApp::CDataSvrApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

BOOL CDataSvrApp::InitInstance() 
{
	return CWinApp::InitInstance();
}

void CDataSvrApp::OnTimer(int nIDEvent)
{
	int nSystemID = nIDEvent - _TIMER_HEARTBEAT;
	::dsSendCommand(DB_CMD_HEARTBEAT, nSystemID);
}


/////////////////////////////////////////////////////////////////////////////
// The one and only CDataSvrApp object

CDataSvrApp theApp;


#ifndef __EP_DATA_SRVER_DLL_FTN
#define __EP_DATA_SRVER_DLL_FTN

/*
#ifdef _DEBUG
#pragma comment(lib, "PowerFormD.lib")
#pragma message("Automatically linking with PowerFormD.lib ")
#else
#pragma comment(lib, "PowerForm.lib")
#pragma message("Automatically linking with PowerForm.lib ")
#endif	//_DEBUG

#include "defdllfun.h"
*/
#define	DATA_SVR_INSTANCE_NAME "Formation Data Server"
#endif	//_EP_FORM_DLL_FTN

static	Worker		m_worker;
static	CWizThreadDispatcher	*m_pDispather = NULL;
CPtrArray m_clientSystem;
//EP_DATA_CLIENT clientSystem[MAX_DATA_CLIENT];
int		nErrorNumber = EP_ERR_EMPTY;
BOOL	m_Initialized = FALSE;
int		m_nIntalledModuleNumber = MAX_DATA_CLIENT;		//Index 0:	Index 1: Index 2:

CDllTimer m_Timer;

#ifdef _DEBUG
	static BOOL bWriteLog = TRUE; 
	char szLogFileName[512] = "DebugLog.txt"; 
#else
	static BOOL bWriteLog = FALSE; 
	char szLogFileName[512]; 
#endif

int ReadAck(DC_DATA_CLIENT& clientModule);

BOOL WriteLog(char *szLog)
{
	if(bWriteLog)
	{
		TRACE("%s", szLog);
		if(strlen(szLogFileName) == 0 || strlen(szLog) == 0)		return FALSE;

		FILE *fp = fopen(szLogFileName, "ab+");
		if(fp == NULL)	return FALSE;

		time_t ltime;
		time( &ltime );
		fprintf(fp, "%s :: %s", ctime(&ltime), szLog );	/* Print local time as a string */
		fclose(fp);
	}
	return TRUE;
}

//Only One Time Execute
BOOL IsFirstInstance()
{
	HANDLE hMutexInstance;
	hMutexInstance = ::OpenMutex(MUTEX_ALL_ACCESS|SYNCHRONIZE, FALSE, _T(DATA_SVR_INSTANCE_NAME));
	if(hMutexInstance)
		return FALSE;
	
	hMutexInstance = ::CreateMutex(NULL, TRUE, _T(DATA_SVR_INSTANCE_NAME));
//	if(hMutexInstance)	::ReleaseMutex(hMutexInstance);
//	::CloseHandle(hMutexInstance);
	return TRUE;
}

void SetUserTimer(int nIDEvent, int nElapsed)
{
	m_Timer.SetTimer(nIDEvent, nElapsed, NULL);
}

void KillUserTimer(int nIDEvent)
{
	m_Timer.KillTimer(nIDEvent);
}

LPDC_DATA_CLIENT CreateClientObject()
{
	LPDC_DATA_CLIENT lpClientSystem = new DC_DATA_CLIENT; 
	if( lpClientSystem == NULL )
		return NULL;

	ZeroMemory(lpClientSystem, sizeof(DC_DATA_CLIENT));
	lpClientSystem->bConnected			= FALSE;
	lpClientSystem->CommandAck.nCode	= EP_NACK;
	lpClientSystem->m_hWriteEvent		= ::CreateEvent(NULL, TRUE, FALSE, NULL);
	lpClientSystem->m_hReadEvent		= ::CreateEvent(NULL, TRUE, FALSE, NULL);

	return lpClientSystem;
}

void DeleteClientObject(int nSystemID, bool bDeleteAll)
{
	LPDC_DATA_CLIENT pClient = NULL;
	int nSystemNum = m_clientSystem.GetSize();

	for( int nI = 0; nI < nSystemNum; nI++ )
	{
		pClient = (LPDC_DATA_CLIENT)m_clientSystem.GetAt(nI);
		if( bDeleteAll == false )
		{
			if( pClient->nSystemID == (UINT)nSystemID )
			{
				KillUserTimer(pClient->nSystemID+_TIMER_HEARTBEAT);
				::CloseHandle(pClient->m_hWriteEvent);
				::CloseHandle(pClient->m_hReadEvent);
				delete pClient;
				m_clientSystem.RemoveAt(nI);
				return;
			}
		}
		else
		{
			KillUserTimer(pClient->nSystemID+_TIMER_HEARTBEAT);
			::CloseHandle(pClient->m_hWriteEvent);
			::CloseHandle(pClient->m_hReadEvent);
			delete pClient;
		}
	}

	if( bDeleteAll == true )
		m_clientSystem.RemoveAll();
}

LPDC_DATA_CLIENT GetClientObject(int nSystemID)
{
	LPDC_DATA_CLIENT pClient = NULL;
	for( INDEX nI = 0; nI < m_clientSystem.GetSize(); nI++ )
	{
		pClient = (LPDC_DATA_CLIENT)m_clientSystem.GetAt(nI);
		if( pClient->nSystemID == (UINT)nSystemID )
		{
			return pClient;
		}
	}

	return NULL;
}


EPDLL_API	char * dsGetClientIP(int nSystemID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if(!m_Initialized)	return NULL;

	LPDC_DATA_CLIENT pClient = NULL;
	for( INDEX nI = 0; nI < m_clientSystem.GetSize(); nI++ )
	{
		pClient = (LPDC_DATA_CLIENT)m_clientSystem.GetAt(nI);
		if( pClient->nSystemID == (UINT)nSystemID )
		{
			return pClient->szIpAddress;
		}
	}
	return NULL;
}

EPDLL_API	int  dsSetFormDataServer(HWND hMsgWnd)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if(!IsFirstInstance())
	{
		WriteLog("Initialize Socket Port Error(Use By other App)\n");
		nErrorNumber = EP_ERR_WINSOCK_INITIALIZE;
		return -1;
	}

	WSADATA wsaData;
	WORD wVersionRequested = MAKEWORD( 2, 2 );		//WinSock Version Greater than 2.0
	int  err = WSAStartup( wVersionRequested, &wsaData );
	if (err != 0) 
	{
		// Tell the user that we couldn't find a usable WinSock DLL.
		nErrorNumber = EP_ERR_WINSOCK_STARTUP;
		WriteLog("Initialize Socket Error.\n");
		return -2;
	}	

	LPDC_DATA_CLIENT lpClientObject;
	for(int i=0; i<MAX_DATA_CLIENT; i++)
	{
		lpClientObject = CreateClientObject();
		lpClientObject->nSystemID = i+1;
		m_clientSystem.Add(lpClientObject);
	}
	m_worker.SetMsgWnd(hMsgWnd);
	m_pDispather = new CWizThreadDispatcher(m_worker, m_nIntalledModuleNumber);	
	m_pDispather->Start();

	m_Initialized = TRUE;
	WriteLog("Formation Data Server Initilaized\n");

	m_Timer.Create(IDD_TIMER_DLG);
	return 0;
}

EPDLL_API	BOOL dsCloseFormDataServer()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if (m_pDispather)				//Close Sever
	{
		m_pDispather->Stop();
		delete m_pDispather;
		m_pDispather = NULL;
	}

	if(m_Initialized)
	{
		DeleteClientObject(0, true); // Delete All Client Object
	}
	
	WSACleanup();
	m_Initialized= FALSE;
	//WriteLog("Formation Data Server Closed\n");
	return TRUE;
}

EPDLL_API	char * dsGetDataServerIP()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if(!m_Initialized)	return NULL;
	return NULL;
//	return stModule[0].sSysParam.IPAddr;
}

EPDLL_API void dsSetLog(BOOL bWrite)
{
	bWriteLog = bWrite;
}

EPDLL_API	int dsSendCommand(int nCommand, int nSystemID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if(!m_Initialized)	return -1;

	int nPacketSize = SizeofHeader();
	
	LPEP_MSG_HEADER pMsgHeader;
	LPDC_DATA_CLIENT lpClientModule = NULL;
	if( (lpClientModule = GetClientObject(nSystemID)) == NULL )
		return EP_FAIL;

//	TRACE("System ID : %d - Send Command : %04X\n", nSystemID, nCommand);
	if(lpClientModule->bConnected ==  FALSE)	return -1;
	
	pMsgHeader = (LPEP_MSG_HEADER)lpClientModule->szTxBuffer;
	ZeroMemory(pMsgHeader, SizeofHeader());
	pMsgHeader->nID = lpClientModule->nSystemID;	//EP_ID_MODULE,		 EP_ID_FORMATION_SYSTEM
												//EP_ID_DATA_SERVER, EP_ID_IROCV_SYSTEM		
												//EP_ID_GRADING_SYSTEM	
	pMsgHeader->nCommand = nCommand;			//EP_CMD_CAP_DATA, EP_CMD_IR_DATA, EP_CMD_OCV_DATA
	pMsgHeader->nLength = nPacketSize;

	lpClientModule->nTxLength = nPacketSize;

	::SetEvent(lpClientModule->m_hWriteEvent);
	Sleep(0);
	return ReadAck(*lpClientModule);
}

EPDLL_API	int dsSendData(int nCommand, LPVOID lpData, int nSize, int nSystemID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if(!m_Initialized)	return -1;
	if(lpData == NULL)	return -1;

	UINT nBodySize = nSize;
	int nPacketSize = SizeofHeader() + nBodySize;		//Total Frame Size
	
	if(nPacketSize >= _EP_TX_BUF_LEN)	return -2;
	
	LPEP_MSG_HEADER pMsgHeader;

	LPDC_DATA_CLIENT clientModule = NULL;
	if( (clientModule = GetClientObject(nSystemID)) == NULL )
		return EP_FAIL;

//	TRACE("System ID : %d - Send Command : %04X\n", nSystemID, nCommand);
	if(clientModule->bConnected ==  FALSE)	return -1;
	
	pMsgHeader = (LPEP_MSG_HEADER)clientModule->szTxBuffer;
	ZeroMemory(pMsgHeader, SizeofHeader());
	pMsgHeader->nID = clientModule->nSystemID;	//EP_ID_MODULE,		 EP_ID_FORMATION_SYSTEM
												//EP_ID_DATA_SERVER, EP_ID_IROCV_SYSTEM		
												//EP_ID_GRADING_SYSTEM	
	pMsgHeader->nCommand = nCommand;			//EP_CMD_CAP_DATA, EP_CMD_IR_DATA, EP_CMD_OCV_DATA
	pMsgHeader->nLength = nPacketSize;

	if(nBodySize>0)		
		memcpy((char *)clientModule->szTxBuffer + SizeofHeader(), lpData, nBodySize);

	clientModule->nTxLength = nPacketSize;

	::SetEvent(clientModule->m_hWriteEvent);
	Sleep(0);
	return ReadAck(*clientModule);
}

//read From Command Response (ACK/NACK/FAIL)
int ReadAck(DC_DATA_CLIENT& clientModule)
{
	ASSERT(m_Initialized);
	ASSERT(clientModule.bConnected);
	
	int nAck = EP_FAIL;
	
	if (::WaitForSingleObject(clientModule.m_hReadEvent, 
							_EP_MSG_TIMEOUT) == WAIT_OBJECT_0 )
	{
		nAck = clientModule.CommandAck.nCode;
	}
	else
	{
		nAck =  EP_TIMEOUT;
	}
	
	::ResetEvent(clientModule.m_hReadEvent);
	clientModule.CommandAck.nCode = EP_FAIL;

	Sleep(0);
	return nAck;
}
