#include "StdAfx.h"

#include "SBCGlobal.h"
#include "SBCLog.h"
#include "SBCCriticalSection.h"
#include "SBCSyncParent.h"
#include "SBCStaticSyncParent.h"
#include "SBCMemoryPool.h"
#include "SBCManagedBuf.h"
#include "SBCCircularQueue.h"

#include "SBCPacketBox.h"

#include "SBCNetObj.h"
#include "SBCIocp.h"

#include "SBCObj.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CSBCObj::CSBCObj() 
{
	CSBCSyncObj SBCSync;

	CSBCNetObj::SetOwnerNetObj(this);

	Init();
}

CSBCObj::~CSBCObj()
{
	CSBCSyncObj SBCSync;

	InitPacketVector();

	if(m_LstRequestCmd.size())
	{
		for (std::list<Request_CMD*>::iterator it=m_LstRequestCmd.begin();it!=m_LstRequestCmd.end();it++)
		{
			Request_CMD *pInfo = (Request_CMD*)(*it);
			delete pInfo;
		}
	}
}
VOID CSBCObj::Init()
{
	mbIsConnect = FALSE;
	mCloseCnt = 0;
	mMsgIdx = -1;
	mRetryCnt = 1;
	mNState = SNS_NONE;

	mpstModule = NULL;
	mModuleID = 0;
	mTotChNo = 0;

	memset(mszIP, 0x00, sizeof(mszIP));
	mshPort = 0;

	mMsgIdx = 0;

	InitPacketVector();

	if(m_LstRequestCmd.size())
	{
		for (std::list<Request_CMD*>::iterator it=m_LstRequestCmd.begin();it!=m_LstRequestCmd.end();it++)
		{
			Request_CMD *pInfo = (Request_CMD*)(*it);
			delete pInfo;
		}
	}
}
BOOL CSBCObj::SetNotifyWnd(HWND _hWnd)
{
	CSBCSyncObj SBCSync;

	hMsgWnd = _hWnd;
	return TRUE;
}

CHAR* CSBCObj::GetPeerName()
{
	CSBCSyncObj SBCSync;

	GetRemoteAddressAfterAccept(mszIP, mshPort);

	return mszIP;
}

INT CSBCObj::GetModuleID()
{
	CSBCSyncObj SBCSync;

	return mModuleID;
}
//Packet 처리 관련
//보낼 패킷 저장소 초기화
VOID CSBCObj::InitPacketVector()
{
	CSBCSyncObj SBCSync;

	for (UINT i = 0; i < m_vSendMessage.size(); i++)
	{
		NEW_SBC_MESSAGE * temp = m_vSendMessage[i];

		delete temp;
	}

	m_vSendMessage.clear();

	mMsgIdx = -1;

}
//패킷 보내기 시작 - 상태 설정
//SetPacketState()
//패킷 보내기
BOOL CSBCObj::WriteingPacket()
{
	CSBCSyncObj SBCSync;


	//TRACE("mNState %s VSize %d msgIdx %d\n"
	//	, g_szSBCNetState[mNState]
	//	, m_vSendMessage.size()
	//	, mMsgIdx );

	switch(mNState)
	{
	case SNS_NONE:
		break;
	case SNS_SEND:
		{
			if(mMsgIdx == -1 && m_vSendMessage.size())
				mMsgIdx = 0;
			mNState = SNS_SENDING;
		}
		break;
	case SNS_SENDING:
		{
			if(mMsgIdx >= 0 && mMsgIdx < m_vSendMessage.size())
			{
				NEW_SBC_MESSAGE *mesg = m_vSendMessage[mMsgIdx];
				if(mesg->isState == SPS_DEL)
				{
					//mMsgIdx++;
					mNState = SNS_RECV_WAIT;
				}
				else
				{
					Write(GetModuleID(), mesg->stHeader
						, (BYTE*)&mesg->byBody
						, mesg->stHeader.nLength - SizeofHeader());

					mesg->isState = SPS_SENDED;

					mRetryCnt = 1;

					mNState = SNS_RECV_WAIT;

					if(mesg->stHeader.nCommand == EP_CMD_VER_REQUEST)
					{
						TRACE("EP_CMD_VER_REQUEST %d \n", GetModuleID());
					}
				}
			}
			else
			{
				InitPacketVector();
				mNState = SNS_NONE;
			}

			TRACE("Vector Size %d \n", m_vSendMessage.size());
		}
		break;
	case SNS_RECV_WAIT:
		{
			mRetryCnt++;
			if(mRetryCnt % 300 == 1)
			{
				mNState = SNS_RESEND;
			}

			//NEW_SBC_MESSAGE* ackmsg = NULL;
			//UINT i = 0;
			//for (; i < m_vSendMessage.size(); i++ )
			//{
			//	if(m_vSendMessage[i]->isState == SPS_DEL)
			//	{
			//		ackmsg = m_vSendMessage[i];
			//		break;
			//	}
			//}

			//if(ackmsg)
			//{
			//	delete ackmsg;
			//	ackmsg = NULL;

			//	m_vSendMessage.erase(m_vSendMessage.begin() + i);
			//}
			if(mMsgIdx >= 0 && mMsgIdx < m_vSendMessage.size())
			{
				NEW_SBC_MESSAGE *mesg = m_vSendMessage[mMsgIdx];
				if(mesg->isState == SPS_DEL)
				{
					mMsgIdx++;
					mNState = SNS_SENDING;
				}
			}

			//if(m_vSendMessage.size())
			//{
			//	mMsgIdx++;
			//	mNState = SNS_SENDING;
			//}
			//else
			//{
			//	mNState = SNS_NONE;
			//}
		}
		break;
	case SNS_RESEND:
		{
			if(mRetryCnt > 90)
			{
				if(mMsgIdx >= 0 && mMsgIdx < m_vSendMessage.size())
				{
					NEW_SBC_MESSAGE *mesg = m_vSendMessage[mMsgIdx];
					
					mesg->isState = SPS_DEL;
					mNState = SNS_RECV_WAIT;
				}
				else
				{
					InitPacketVector();
					mNState = SNS_NONE;
				}

				mRetryCnt = 1;
			}
			else
			{
				if(mMsgIdx >= 0 && mMsgIdx < m_vSendMessage.size())
				{
					NEW_SBC_MESSAGE *mesg = m_vSendMessage[mMsgIdx];

					Write(GetModuleID(), mesg->stHeader
						, (BYTE*)&mesg->byBody
						, mesg->stHeader.nLength - SizeofHeader());

					mNState = SNS_RECV_WAIT;
				}
				else
				{
					InitPacketVector();
					mNState = SNS_NONE;
				}
			}

		}
		break;
	case SNS_RECV:
		{
			
		}
		break;
	}

	return FALSE;
}
//보낼 패킷 저장
BOOL CSBCObj::SetPacketVector(NEW_SBC_MESSAGE& _Msg)
{
	CSBCSyncObj SBCSync;
	//retry packet
	NEW_SBC_MESSAGE * message = new NEW_SBC_MESSAGE;
	memcpy(message, &_Msg, sizeof(NEW_SBC_MESSAGE));

	message->isState = SPS_SEND;

	switch(message->stHeader.nCommand)
	{
	case EP_CMD_VER_REQUEST:
		message->recvCommand = EP_CMD_VER_DATA;
		break;
	default:
		message->recvCommand = message->stHeader.nCommand;
	}

	m_vSendMessage.push_back(message);

	CSBCLog::WritePacketLog(GetModuleID(), "Save =====%x %d========> %d %d %d Command[0x%08X] %d [Vectorcnt: [%d]]\n"
		, this, _Msg.stHeader.nLength
		, _Msg.stHeader.nID
		, _Msg.stHeader.wGroupNum
		, _Msg.stHeader.wChannelNum
		, _Msg.stHeader.nCommand
		, _Msg.stHeader.nLength
		, m_vSendMessage.size());	

	return TRUE;
}
//보낼 패킷 저장 - 메세지 구조체
//BOOL CSBCObj::SetPacketVector(NEW_SBC_MESSAGE* _Msg)
//{
//	mMsgIdx = 0;
//
//	m_vSendMessage.push_back(_Msg);
//
//	return TRUE;
//}
//패킷 저장
BOOL CSBCObj::SavePacket(DWORD command)
{
	CSBCSyncObj SBCSync;

	NEW_SBC_MESSAGE Msg;

	Msg.stHeader.nID = EP_ID_FORM_OP_SYSTEM;
	Msg.stHeader.wGroupNum = (WORD)0;
	Msg.stHeader.wChannelNum = (WORD)0;
	Msg.stHeader.nCommand = command;
	Msg.stHeader.nLength = SizeofHeader();

	ZeroMemory(Msg.byBody, sizeof(Msg.byBody));

	Msg.isState = SPS_SEND;

	return SetPacketVector(Msg);
}
//패킷 저장  - 헤더 데이터 길이
BOOL CSBCObj::SavePacket(EP_MSG_HEADER* _hdr, BYTE* data, DWORD datalen)
{
	CSBCSyncObj SBCSync;

	NEW_SBC_MESSAGE Msg;

	memcpy(&Msg.stHeader, _hdr, SizeofHeader());
	memcpy(&Msg.byBody, data, datalen);
	
	return SetPacketVector(Msg);
}
VOID CSBCObj::SendCommand()
{
	CSBCSyncObj SBCSync;

	if(mNState == SNS_NONE)
	{
		mNState = SNS_SEND;
	}
};

//다음 패킷 보내기
BOOL CSBCObj::RecvCommand(UINT command)
{
	CSBCSyncObj SBCSync;

	//동일 데이터 보내면 전부다 셋됨
	//for (UINT i = 0; i < m_vSendMessage.size(); i++)
	//{
	//	NEW_SBC_MESSAGE * temp = m_vSendMessage[i];
	//	if(temp->recvCommand == command)
	//		temp->isState = SPS_DEL;
	//}

	if(mMsgIdx >= 0 && mMsgIdx < m_vSendMessage.size())
	{
		if(m_vSendMessage[mMsgIdx]->recvCommand == command)
		{
			m_vSendMessage[mMsgIdx]->isState = SPS_DEL;
			return TRUE;
		}
		else
		{
			CSBCLog::WriteErrorLog("RecvCommand Response Command: 0x%08x, Send Command: 0x%08x "
			, command
			, m_vSendMessage[mMsgIdx]->recvCommand);
		}
	}
	else
	{
		CSBCLog::WriteErrorLog("RecvCommand Response Command");
	}

	return FALSE;
}
SBCPacketError CSBCObj::SendEP_CMD_RESPONSE(EP_MSG_HEADER* _hdr, SBCPacketError Code)
{
	CSBCSyncObj SBCSync;

	SBCPacketError _error = SPE_NONE;

	NEW_SBC_MESSAGE mResponseMsg;

	EP_RESPONSE _response;

	_response.nCmd = _hdr->nCommand;
	if(Code == SPE_NONE)
	{
		_response.nCode = EP_ACK;
	}
	else
	{
		_response.nCode = EP_NACK;
	}

	_hdr->nID = EP_ID_FORM_OP_SYSTEM;
	_hdr->nCommand = EP_CMD_RESPONSE;
	_hdr->nLength = SizeofHeader()+ sizeof(EP_RESPONSE);

	memcpy(&mResponseMsg.stHeader, _hdr, SizeofHeader());

	memcpy(&mResponseMsg.byBody, &_response, sizeof(EP_RESPONSE));

	Write(GetModuleID(), mResponseMsg.stHeader
		, (BYTE*)&mResponseMsg.byBody
		, mResponseMsg.stHeader.nLength - SizeofHeader());	


	return _error;
}
//////////////////////////////////////////////////////////////////////////
//connect Seq
BOOL CSBCObj::SendEP_CMD_MD_SET_DATA()
{
	CSBCSyncObj SBCSync;

	NEW_SBC_MESSAGE Msg;

	INT nModuleIndex = 0;
	EP_MD_SET_DATA1 sMDSetData;
	UINT datasize = sizeof(EP_MD_SET_DATA1);
	ZeroMemory(&sMDSetData, datasize);

	nModuleIndex = CheckModuleIDValidate(&mpstModule->sysData);

	Msg.stHeader.nID = EP_ID_FORM_OP_SYSTEM;
	Msg.stHeader.wGroupNum = (WORD)0;
	Msg.stHeader.wChannelNum = (WORD)0;
	Msg.stHeader.nCommand = EP_CMD_MD_SET_DATA;
	Msg.stHeader.nLength = SizeofHeader();

	Msg.stHeader.nLength += datasize;

	sMDSetData.nAutoReportInterval = 1;

	sMDSetData.bConnectionReTry = BYTE(nModuleIndex < 0 ? 0x00 : 0x01);
	sMDSetData.bAutoProcess =(BYTE)EPGetAutoProcess(mpstModule->sysData.nModuleID);
	
	sMDSetData.bUseTemp = (BYTE)mpstModule->sysParam.bUseTempLimit;
	sMDSetData.bUSeJigTemp = (BYTE)mpstModule->sysParam.bUseJigTempLimit;

	sMDSetData.sWanningTemp = mpstModule->sysParam.sWanningTemp;
	sMDSetData.sCutTemp = mpstModule->sysParam.sCutTemp;
	sMDSetData.sJigWanningTemp = mpstModule->sysParam.sJigWanningTemp;
	sMDSetData.sJigCutTemp = mpstModule->sysParam.sJigCutTemp;

	sMDSetData.bTrayReadType = EP_TRAY_REC_NVRAM;	//Tray ID Type		0: NVRAM	1: BarCode Reader
	sMDSetData.vHighLimit = mpstModule->sysParam.lMaxVoltage;
	sMDSetData.iHighLimit = mpstModule->sysParam.lMaxCurrent;

	memcpy(Msg.byBody, &sMDSetData, datasize);

	Msg.isState = SPS_SEND;

	SetPacketVector(Msg);

	SendCommand();

	return TRUE;
}

SBCPacketError CSBCObj::RecvEP_CMD_VER_DATA(EP_MSG_HEADER* _hdr, BYTE* data, DWORD datalen)
{
	CSBCSyncObj SBCSync;

	SBCPacketError _error = SPE_NONE;

	CHAR msg[1024] = {0};

	EP_MD_SYSTEM_DATA sysData;
	ZeroMemory(&sysData, sizeof(sysData));

	if(sizeof(EP_MD_SYSTEM_DATA) == datalen)
	{
		memcpy(&sysData, data, datalen);
		
		INT nModuleIndex = 0;

		memcpy(&sysData, data, datalen);

		nModuleIndex = CheckModuleIDValidate(&sysData);

		mpstModule = &g_pstModule[nModuleIndex];

		mModuleID = mpstModule->sysData.nModuleID;

		mTotChNo = mpstModule->sysData.nTotalChNo;

		if(CheckModuleIDValidate(&sysData) < 0)		//Accepted Module Error			
		{
			sprintf_s(msg, "Accepted module connected form %s is not registed. Code(%x).\n", mszIP, _hdr->nCommand);
			CSBCLog::WriteErrorLog(msg);  //nModuleIndex < 0 작음 CheckModuleIDValidate 검사 해야 함
			_error = SPE_MODULE_ID;
		}
		else
		{
			//ID가 중복된 Module이 접속을 한다.  
			if(GetModuleData()->state != EP_STATE_LINE_OFF)
			{
				sprintf_s(msg, "Duplicated ID %s module is attempt to connect.", mszIP, sysData.nModuleID);
				_error = SPE_IS_CONNECT;
			}
			else	//Accept Module
			{
				strcpy_s(GetModuleData()->sysParam.szIPAddr, mszIP);
				memcpy(&GetModuleData()->sysData, &sysData, sizeof(sysData));	//Module Information receive

				RecvCommand(_hdr->nCommand);
				SendEP_CMD_MD_SET_DATA();
			}
		}
	}
	return _error;
}

//////////////////////////////////////////////////////////////////////////
//for recv Packet Proc Fn
INT CSBCObj::CheckModuleIDValidate(LPEP_MD_SYSTEM_DATA pSysData)
{
	CSBCSyncObj SBCSync;

	INT nModuleIndex;

	//버전 확인
	if(pSysData->nVersion > _EP_PROTOCOL_VERSION)
		return -1;		//Version Mismatch
	//sysParam nModuleID 비교 한 Idx
	if((nModuleIndex = EPGetModuleIndex(pSysData->nModuleID)) < 0)
		return -2;		//Not registed 
	//인스톨 확인
	if(pSysData->wInstalledBoard > EP_MAX_BD_PER_MD	|| pSysData->wInstalledBoard <= 0)
		return -4;
	//채널당 보드
	if(pSysData->wChannelPerBoard <= 0  || pSysData->wInstalledBoard >EP_MAX_CH_PER_BD)
		return -5;
	if(pSysData->wTotalTrayNo < 0 || pSysData->wTotalTrayNo > 16)
		return -7;

	INT nTotalCh = pSysData->nTotalChNo;
	if(nTotalCh <=0 || nTotalCh >EP_MAX_CH_PER_MD)
		return -6;

	INT nTotalChInTray = 0;
	for(INT  i =0; i<pSysData->wTotalTrayNo; i++)
	{
		if(pSysData->awChInTray[i] < 0 || pSysData->awChInTray[i] > EP_MAX_CH_PER_MD)
			return -8;
		nTotalChInTray += pSysData->awChInTray[i];
	}
	if(nTotalChInTray <=0 || nTotalChInTray >EP_MAX_CH_PER_MD)
		return -10;

	if(nTotalCh != nTotalChInTray)
	{
		TRACE("<<<*****Channel in module and channel in tray count are mismatch*****>>>\n");
	}

	//if(EPGetModuleState(mdSysData.nModuleID) == EP_MD_ST_LINE_ON)	return -3;		//ID duplication

	return nModuleIndex;
}
BOOL CSBCObj::CloseGroupData()
{
	CSBCSyncObj SBCSync;

	int nGroupNo =  mpstModule->gpData.GetSize();
	LPEP_GROUP_INFO	pGroup;
	LPEP_CH_DATA	pChannel;

	for(INDEX i =0; i<nGroupNo; i++)
	{
		pGroup = (EP_GROUP_INFO *)mpstModule->gpData[i];	
		ASSERT(pGroup);

		INDEX j = 0;
		for(j =0; j<pGroup->chData.GetSize(); j++)
		{
			pChannel = (EP_CH_DATA *)pGroup->chData[j];
			delete pChannel;
			pChannel = NULL;
			//			TRACE("Release channel end data %d\n", j);
		}
		pGroup->chData.RemoveAll();

		for(j =0; j<pGroup->chRealData.GetSize(); j++)
		{
			EP_REAL_TIME_DATA *pRltData = (EP_REAL_TIME_DATA *)pGroup->chRealData[j];
			delete pRltData;
			pRltData = NULL;
			//			TRACE("Release channel real data %d\n", j);
		}
		pGroup->chRealData.RemoveAll();

		delete pGroup;
		pGroup = NULL;
	}
	mpstModule->gpData.RemoveAll();	
	return TRUE;
}

VOID CSBCObj::GroupChageState()
{
	CSBCSyncObj SBCSync;

	int nModuleID =  mpstModule->sysData.nModuleID;			//Get Module ID
		
	if(hMsgWnd)
		::PostMessage(hMsgWnd, EPWM_MODULE_STATE_CHANGE, MAKELONG(0, nModuleID), 0);
}

BOOL CSBCObj::InitGroupData()
{
	CSBCSyncObj SBCSync;

	if(CloseGroupData() == FALSE)	return FALSE;

	ASSERT(mpstModule->gpData.GetSize() == 0);

	LPEP_GROUP_INFO		pGroup;
	LPEP_CH_DATA		pChannel;
	LPEP_MD_SYSTEM_DATA	pSysData;

	pSysData = &mpstModule->sysData;
	ASSERT(pSysData);

	pGroup = new EP_GROUP_INFO;
	ASSERT(pGroup);
	ZeroMemory(&pGroup->gpData, sizeof(EP_GP_DATA));
	mpstModule->gpData.Add(pGroup);	

	pGroup->gpData.gpState.state = EP_STATE_LINE_OFF;	
	for(INDEX j =0; j<pSysData->nTotalChNo; j++)
	{
		pChannel = new EP_CH_DATA;
		ZeroMemory(pChannel, sizeof(EP_CH_DATA));
		pGroup->chData.Add(pChannel);

		EP_REAL_TIME_DATA *pRltData = new EP_REAL_TIME_DATA;
		ZeroMemory(pRltData, sizeof(EP_REAL_TIME_DATA));
		pGroup->chRealData.Add(pRltData);
	}

	return TRUE;
}

VOID CSBCObj::ModuleConnected()
{
	CSBCSyncObj SBCSync;

	int nModuleID =  mpstModule->sysData.nModuleID;			//Get Module ID
	mpstModule->state = EP_STATE_LINE_ON;						//Module Line On State

	LPEP_GROUP_INFO lpGPData;
	lpGPData = (EP_GROUP_INFO *)mpstModule->gpData[0];
	ASSERT(lpGPData);
	lpGPData->gpData.gpState.state = EP_STATE_LINE_ON;

	if(hMsgWnd)
		::PostMessage(hMsgWnd, EPWM_MODULE_CONNECTED, MAKELONG(0, nModuleID), 0);		//Send to Parent Wnd to Module Conntected 


	if(hMsgWnd)
		::PostMessage(hMsgWnd, EPWM_MODULE_STATE_CHANGE, MAKELONG(0, nModuleID), 0);		//Send to Parent Wnd To Module State Change
	//	TRACE("Module %d Connected\n", nModuleID);
}

VOID CSBCObj::ModuleClosed()
{
	CSBCSyncObj SBCSync;
	//Form Server is closed
	mbIsConnect = FALSE;

	if( mpstModule == NULL )
	{
		return;		
	}
	
	int nModuleID = mpstModule->sysData.nModuleID;				//Get Module ID		
	mpstModule->state = EP_STATE_LINE_OFF;						//Module Line Off State

	LPEP_GROUP_INFO lpGPData;
	lpGPData = (EP_GROUP_INFO *)mpstModule->gpData[0];
	ASSERT(lpGPData);
	lpGPData->gpData.gpState.state = EP_STATE_LINE_OFF;

	if(hMsgWnd)
		::PostMessage(hMsgWnd, EPWM_MODULE_CLOSED, MAKELONG(0, nModuleID), 0);	//Send to Parent Wnd to Module Close Message

	if(hMsgWnd)
		::PostMessage(hMsgWnd, EPWM_MODULE_STATE_CHANGE, MAKELONG(0, nModuleID), 0);		//Send to Parent Wnd To Module State Change
	//	TRACE("Module %d Connected\n", nModuleID);
}

INT CSBCObj::GetMappingCh(int nSeqIndex, WORD wSystemType, int nMemberIndex)
{
	CSBCSyncObj SBCSync;

	int index = nSeqIndex;
	if(g_nChannelIndexType == EP_CH_INDEX_SEQUENCE)			//Network로 들어온 순서대로 Channel을 선택
	{
		index = nSeqIndex;
	}
	else if(g_nChannelIndexType == EP_CH_INDEX_CUSTOM)		//LG OffLine
	{
		if(wSystemType == TRAY_TYPE_8_BY_32)
		{
			index = nSeqIndex/32*32+(31-nSeqIndex%32);
		}
		else if(wSystemType == TRAY_TYPE_16_BY_16)
		{
			index = nSeqIndex/16*16+(15-nSeqIndex%16);
		}
		else
		{
			index = nSeqIndex;
		}
	}
	else if(g_nChannelIndexType == EP_CH_INDEX_MAPPING)		//PC "mapping.dat" 파일에 정의된 순으로 정렬 
	{
		index = g_channelMap[nSeqIndex];
	}
	else	//EP_CH_INDEX_MEMBER, 보내준 Structure의 내부 Index에 의해 재배열 
	{
		index = nMemberIndex;
	}
	return index;
}
//////////////////////////////////////////////////////////////////////////
//Proc Fn
SBCPacketError CSBCObj::RecvEP_CMD_RESPONSE(EP_MSG_HEADER* _hdr, BYTE* data, DWORD datalen)
{
	CSBCSyncObj SBCSync;

	SBCPacketError _error = SPE_NONE;

	_hdr;

	if(data == NULL) return SPE_BODY_EMPTY;

	if(sizeof(EP_RESPONSE) == datalen)
	{
		EP_RESPONSE response;
		memcpy(&response, data, sizeof(EP_RESPONSE));
		switch(response.nCmd)
		{
		case EP_CMD_MD_SET_DATA:
			if(response.nCode == EP_ACK)
			{
				InitGroupData();
				ModuleConnected();				
			}
			else
			{

			}
			break;
		case EP_CMD_SET_AUTO_REPORT:
			if(response.nCode == EP_ACK)
			{
			}
			break;			
		case EP_CMD_TEST_HEADERDATA:
			if(response.nCode == EP_ACK)
			{
			}
			break;
		case EP_CMD_CHECK_PARAM:
			if(response.nCode == EP_ACK)
			{
			}
			break;
		case EP_CMD_STEPDATA:
			if(response.nCode == EP_ACK)
			{
			}
			break;
		case EP_CMD_SET_TRAY_READY:
			if(response.nCode == EP_ACK)
			{
			}
			break;
		case EP_CMD_TRAY_DATA:
			if(response.nCode == EP_ACK)
			{
			}
			break;
		case EP_CMD_RUN:
			if(response.nCode == EP_ACK)
			{
			}
			break;
		case EP_CMD_RESTART:
			if(response.nCode == EP_ACK)
			{
			}
			break;
		case EP_CMD_SET_LINE_MODE://AUTO LOCAL
			if(response.nCode == EP_ACK)
			{
				//mpstModule->nWorkType = mpstModule->poLineModeSet.nWorkType;
				//mpstModule->nLineMode = mpstModule->poLineModeSet.nOnLineMode;
				//mpstModule->nOperationMode = mpstModule->poLineModeSet.nOperationMode;
				
				GroupChageState();
			}
			else if(response.nCode == EP_NACK)
			{

			}
			break;
		default:
			CSBCLog::WriteErrorLog("RESPONSECommand Command: 0x%x, Code: %d "
				, response.nCmd
				, response.nCode);
			break;
		}

		if(response.nCode == EP_ACK)
		{
			RecvCommand(response.nCmd);
		}
		else
		{
			CSBCLog::WriteErrorLog("RESPONSECommand Command NACK : 0x%x, Code: %d "
				, response.nCmd
				, response.nCode);
		}


		SBCQueue_DATA QData;// = new SBCQueue_DATA;
		QData.id = (WPARAM)MAKELONG(0, mpstModule->sysParam.nModuleID);
		QData.command = _hdr->nCommand;
		::CopyMemory(&QData.Data, (LPVOID)&response, sizeof(EP_RESPONSE));
		QData.DataLength = sizeof(EP_RESPONSE);

		g_Que.Push(QData);

		::PostMessage(hMsgWnd,
			EPWM_MODULE_CHANGE_INFO, 
			(WPARAM)MAKELONG(0, mpstModule->sysParam.nModuleID), 
			(LPARAM)datalen);
	}

	return _error;
}
SBCPacketError CSBCObj::RecvEP_CMD_STEPDATA(EP_MSG_HEADER* _hdr, BYTE* data, DWORD datalen)
{
	CSBCSyncObj SBCSync;

	SBCPacketError _error = SPE_NONE; 

	if(data == NULL) return SPE_BODY_EMPTY;
	//길이 비교if(sizeof() == datalen)
	{
		STR_COMMON_STEP stepData;
		ZeroMemory(&stepData, sizeof(STR_COMMON_STEP));

		memcpy(&stepData.stepHeader, data, sizeof(_STEP_HEADER));
		switch(stepData.stepHeader.type)
		{
		case EP_TYPE_CHARGE :
		case EP_TYPE_DISCHARGE:
			{
				EP_CHARGE_STEP charge;
				ZeroMemory(&charge, sizeof(EP_CHARGE_STEP));
				memcpy(&charge, data, sizeof(EP_CHARGE_STEP));

				stepData.fVref = ExpLongToFloat(charge.lVref, EP_VTG_FLOAT)/100;
				stepData.fIref = ExpLongToFloat(charge.lIref, EP_CRT_FLOAT)/10;

				stepData.fEndV = ExpLongToFloat(charge.lEndV, EP_VTG_FLOAT)/100;
				stepData.fEndI = ExpLongToFloat(charge.lEndI, EP_CRT_FLOAT)/10;
				stepData.fEndTime = MD2PCTIME(charge.ulEndTime)/100;
				stepData.fEndC = ExpLongToFloat(charge.lEndC, EP_CRT_FLOAT)/10;
				stepData.fEndDV = ExpLongToFloat(charge.lEndDV, EP_VTG_FLOAT)/100;
				stepData.fEndDI = ExpLongToFloat(charge.lEndDI, EP_CRT_FLOAT)/10;

			}			
			break;
		case EP_TYPE_IMPEDANCE:
			{
				EP_IMPEDANCE_STEP Impedance;
				ZeroMemory(&Impedance, sizeof(EP_IMPEDANCE_STEP));
				memcpy(&Impedance, data, sizeof(EP_IMPEDANCE_STEP));
				
				stepData.fVref = ExpLongToFloat(Impedance.lVref, EP_VTG_FLOAT)/100;
				stepData.fIref = ExpLongToFloat(Impedance.lIref, EP_CRT_FLOAT)/10;
			}
			break;
		case EP_TYPE_REST:
			{
				EP_REST_STEP Rest;
				ZeroMemory(&Rest, sizeof(EP_REST_STEP));
				memcpy(&Rest, data, sizeof(EP_REST_STEP));

				stepData.fEndTime = MD2PCTIME(Rest.ulEndTime)/100;
			}
			break;

		case EP_TYPE_OCV:
		case EP_TYPE_END:
			break;
		}

		SBCQueue_DATA QData;// = new SBCQueue_DATA;
		QData.id = (WPARAM)MAKELONG(0, mpstModule->sysParam.nModuleID);
		QData.command = _hdr->nCommand;
		::CopyMemory(&QData.Data, (LPVOID)&stepData, sizeof(STR_COMMON_STEP));
		QData.DataLength = sizeof(STR_COMMON_STEP);

		g_Que.Push(QData);

		::PostMessage(hMsgWnd,
			EPWM_MODULE_CHANGE_INFO, 
			(WPARAM)MAKELONG(0, mpstModule->sysParam.nModuleID), 
			(LPARAM)datalen);
	}

	return _error;
}
SBCPacketError CSBCObj::RecvEP_CMD_AUTO_GP_STATE_DATA(EP_MSG_HEADER* _hdr, BYTE* data, DWORD datalen)
{
	CSBCSyncObj SBCSync;

	SBCPacketError _error = SPE_NONE; 

	if(data == NULL) return SPE_BODY_EMPTY;

	if(sizeof(EP_GP_STATE_DATA) - 4 == datalen)
	{
		//LPEP_GROUP_INFO lpGPData;
		//EP_GP_STATE_DATA prevState;
		EP_GP_STATE_DATA GpState;
		LPEP_GROUP_INFO lpGPData = (EP_GROUP_INFO *)mpstModule->gpData[0];
		ASSERT(lpGPData);

		//Host PC에서 새로운 Group 상태 추가(Ready, End) 
		////////////////////////////////////////////////////////////////////////
		//	1. Group이 Standby 상태이지만 tray가 Load 안되거나 Door가 Open 된 경우 Ready 상태 유지 (Module은  Standby 상태)
		//  2. Group 이 End 이고 지그가 Down->Up을 갈 경우 계속 End 유지 (Module은 Standby 상태)
		//	2002.7.11 Kim Byung Hum
		/////////////////////////////////////////////////////////////////////////
		//EP_GP_STATE_DATA *lpGpState = (EP_GP_STATE_DATA *)data;
		::CopyMemory(&GpState, data, datalen);
		//TRACE("\n *******===========%d============ \n \
		//	  sensorState[0] : 0x%02x \n \
		//	  sensorState[1] : 0x%02x \n \
		//	  sensorState[2] : 0x%02x \n \
		//	  sensorState[3] : 0x%02x \n \
		//	  sensorState[4] : 0x%02x \n \
		//	  sensorState[5] : 0x%02x \n \
		//	  codeState[0] : 0x%02x \n \
		//	  codeState[1] : 0x%02x \n \
		//	  ********========================= \n \n"
		//	  , nModuleIndex
		//	  , lpGpState->sensorState[0]
		//, lpGpState->sensorState[1]
		//, lpGpState->sensorState[2]
		//, lpGpState->sensorState[3]
		//, lpGpState->sensorState[4]
		//, lpGpState->sensorState[5]
		//, lpGpState->codeState[0]
		//, lpGpState->codeState[1]
		//);
		//if(GpState.state != lpGPData->gpData.gpState.state)
		//	CSBCLog::WriteSysLog("Pre Sate[%s] / Recv State[%s] \n", g_strSBCState[lpGPData->gpData.gpState.state], g_strSBCState[GpState.state]);

		//if(GpState.state == EP_STATE_STANDBY)	
		//{
		//	if(lpGPData->gpData.gpState.state == EP_STATE_RUN)		//Run에서 Stanby로 가면 End 상태로 보고 
		//	{
		//		GpState.state = EP_STATE_END;
		//		CSBCLog::WriteSysLog("Make END Pre Sate[%s] / Recv State[%s] \n", g_strState[lpGPData->gpData.gpState.state], g_strState[GpState.state]);
		//	}
		//	else
		//	{
		//		//시험을 완료 하고 Jig가 Up 되더라도 상태를 End 상태 유지(이후 상태가 바뀌면 Stanby나 Idle로 전환)
		//		if( lpGPData->gpData.gpState.state == EP_STATE_END	&& (lpGPData->gpData.gpState.sensorState[0] != GpState.sensorState[0]))
		//		{
		//			GpState.state = EP_STATE_END;
		//			CSBCLog::WriteSysLog("Make END Pre Sate[%s] / Recv State[%s] \n", g_strState[lpGPData->gpData.gpState.state], g_strState[GpState.state]);
		//		}
		//		else
		//		{
		//			//시험조건이 전송된 상태에서 Tray가 없거나 Door가 Open된 경우
		//			if( GpState.sensorState[1] == 0 || GpState.sensorState[3] == 0)			//Tray가 1개도 Loading 안되거나 Door가 Open되어 있을 경우 
		//			{
		//				GpState.state = EP_STATE_READY;
		//				CSBCLog::WriteSysLog("Make Ready Pre Sate[%s] / Recv State[%s] \n", g_strState[lpGPData->gpData.gpState.state], g_strState[GpState.state]);
		//			}
		//		}
		//	}
		//} 
		
		//////////////////////////////////////////////////////////////////////////
		//TRACE("Group State2: %d, %d, %d, %d\n", lpGpState->state, lpGpState->jigState, lpGpState->doorState, lpGpState->trayState);
		//memcpy(&prevState, &lpGPData->gpData.gpState, sizeof(EP_GP_STATE_DATA) - 4);		//이전 State Back Up
		//memcpy(&lpGPData->gpData.gpState, &GpState, sizeof(EP_GP_STATE_DATA) - 4);


		SBCQueue_DATA QData;// = new SBCQueue_DATA;
		QData.id = (WPARAM)MAKELONG(0, mpstModule->sysParam.nModuleID);
		QData.command = _hdr->nCommand;
		::CopyMemory(&QData.Data, (LPVOID)&GpState, sizeof(EP_GP_STATE_DATA));
		QData.DataLength = datalen;

		g_Que.Push(QData);

		::PostMessage(hMsgWnd, 
			EPWM_MODULE_CHANGE_INFO, 
			(WPARAM)MAKELONG(0, mpstModule->sysParam.nModuleID), 
			(LPARAM)datalen);
	}
	else
	{
		_error = SPE_SIZE;
	}

	return _error;
}

SBCPacketError CSBCObj::RecvEP_CMD_AUTO_GP_DATA(EP_MSG_HEADER* _hdr, BYTE* data, DWORD datalen)
{
	CSBCSyncObj SBCSync;

	SBCPacketError _error = SPE_NONE;

	_hdr;

	if(data == NULL) return SPE_BODY_EMPTY;

	if( SizeofCHData()*mpstModule->sysData.nTotalChNo == datalen)
	{
		LPEP_GROUP_INFO lpGPData;
		LPEP_CH_DATA	lpChannel;
		lpGPData = (EP_GROUP_INFO *)mpstModule->gpData[0];
		INT index = 0, wrSize = 0;
		// lpGPData->gpData.gpState.codeState[0] = 0;
		// lpGPData->gpData.gpState.codeState[1] = 0;
		for(INDEX ch = 0; ch<mpstModule->sysData.nTotalChNo; ch++)
		{				
			wrSize = SizeofCHData()*ch;
			lpChannel = (EP_CH_DATA *)((char *)data+wrSize);

			//Host Mapping
			index = GetMappingCh(ch, mpstModule->sysData.wTrayType, lpChannel->wChIndex);

			if(index < 0 || index >=mpstModule->sysData.nTotalChNo)
			{
				_error = SPE_EP_CMD_AUTO_GP_DATA_Channel_Index_Error;
			}
			else
			{
				if(lpChannel->channelCode >=0 && lpChannel->channelCode < 512)
				{						
					//	lpGPData->gpData.wFailCnt[lpChannel->channelCode]++;				
					switch( lpChannel->channelCode )
					{
					case EP_CODE_NORMAL:
						lpGPData->gpData.wFailCnt[EP_CODE_NORMAL]++;
						break;
					case EP_CODE_CELL_NONE:
						lpGPData->gpData.wFailCnt[EP_CODE_CELL_NONE]++;
						lpGPData->gpData.wFailCnt[EP_CODE_FAIL_ALL]++;
						break;
					case EP_CODE_FAIL_VTG_HIGH:
						lpGPData->gpData.wFailCnt[EP_CODE_FAIL_VTG_HIGH]++;
						lpGPData->gpData.wFailCnt[EP_CODE_FAIL_ALL]++;
						break;
					case EP_CODE_FAIL_VTG_LOW:
						lpGPData->gpData.wFailCnt[EP_CODE_FAIL_VTG_LOW]++;
						lpGPData->gpData.wFailCnt[EP_CODE_FAIL_ALL]++;
						break;
					case EP_CODE_FAIL_CRT_HIGH:
						lpGPData->gpData.wFailCnt[EP_CODE_FAIL_CRT_HIGH]++;
						lpGPData->gpData.wFailCnt[EP_CODE_FAIL_ALL]++;
						break;
					case EP_CODE_FAIL_CRT_LOW:
						lpGPData->gpData.wFailCnt[EP_CODE_FAIL_CRT_LOW]++;
						lpGPData->gpData.wFailCnt[EP_CODE_FAIL_ALL]++;
						break;
					case EP_CODE_FAIL_V_HUNTING:
						lpGPData->gpData.wFailCnt[EP_CODE_FAIL_V_HUNTING]++;
						lpGPData->gpData.wFailCnt[EP_CODE_FAIL_ALL]++;
						break;
					case EP_CODE_FAIL_I_HUNTING:
						lpGPData->gpData.wFailCnt[EP_CODE_FAIL_I_HUNTING]++;
						lpGPData->gpData.wFailCnt[EP_CODE_FAIL_ALL]++;
						break;
					//default:
					//	lpGPData->gpData.wFailCnt[EP_CODE_FAIL_ALL]++;
					}	
				}
				lpChannel =	(EP_CH_DATA *)lpGPData->chData[index];
				ASSERT(lpChannel);
				memcpy(lpChannel, (char *)data+wrSize, SizeofCHData());
				lpChannel->wChIndex = (WORD)index;
			}
		}		
	}
	else
	{
		_error = SPE_SIZE;
	}

	return _error;
}

SBCPacketError CSBCObj::RecvEP_CMD_AUTO_GP_STEP_END_DATA(EP_MSG_HEADER* _hdr, BYTE* data, DWORD datalen)
{
	CSBCSyncObj SBCSync;

	SBCPacketError _error = SPE_NONE;

	if(data == NULL) return SPE_BODY_EMPTY;

	INT nItemResultSize = sizeof(EP_STEP_END_HEADER);

	if((SizeofCHData()*mpstModule->sysData.nTotalChNo+nItemResultSize) == datalen)
	{
		EP_STEP_END_HEADER *pStepEndHeader = (EP_STEP_END_HEADER *)data;
		ASSERT(pStepEndHeader);

		INT wrSize = 0;
		INT index = 0;

		LPEP_GROUP_INFO lpGPData = (EP_GROUP_INFO *)mpstModule->gpData[0];
		LPEP_CH_DATA	lpChDest, lpChSource;

		SBCQueue_DATA QData;// = new SBCQueue_DATA;

		for(INDEX ch = 0; ch<mpstModule->sysData.nTotalChNo; ch++)
		{				
			wrSize = SizeofCHData()*ch;
			lpChSource = (EP_CH_DATA *)((char *)data+nItemResultSize+wrSize);

			//Host Mapping
			index = GetMappingCh(ch, mpstModule->sysData.wTrayType, lpChSource->wChIndex);

			if(index < 0 || index >=mpstModule->sysData.nTotalChNo)
			{
				//response.nCode = EP_NACK;
				//sprintf(msg, "Module %d Group %d, Channel Index Error %d", m_stModule[nModuleIndex].sysParam.nModuleID, lpHeader->wGroupNum, index);
				//WriteLog(msg);
				_error = SPE_EP_CMD_AUTO_GP_STEP_END_DATA_Channel_Index_Error;
				break;
			}
			else
			{
				lpChSource = (EP_CH_DATA *)((char *)data+nItemResultSize+wrSize);
				lpChDest =	(EP_CH_DATA *)lpGPData->chData[index];
				ASSERT(lpChDest);
				memcpy(lpChDest, (char *)lpChSource, SizeofCHData());
				lpChDest->wChIndex = (WORD)index;
				lpChSource->wChIndex = (WORD)index;					
				memcpy((char *)QData.Data+nItemResultSize+index*SizeofCHData(), lpChSource, SizeofCHData());	//Result 결과를 재정렬 한다.
			}
		}

		if(mpstModule->lpDataBuff)
		{
			delete [] mpstModule->lpDataBuff;
			mpstModule->lpDataBuff = NULL;
		}

		mpstModule->lpDataBuff = new CHAR[datalen];
		memcpy(mpstModule->lpDataBuff, QData.Data, datalen);
		QData.id = (WPARAM)MAKELONG(_hdr->wGroupNum-1, mpstModule->sysParam.nModuleID);
		QData.command = _hdr->nCommand;
		QData.DataLength = datalen;

		g_Que.Push(QData);

		::PostMessage(hMsgWnd, 
			EPWM_MODULE_CHANGE_INFO, 
			(WPARAM)MAKELONG(_hdr->wGroupNum-1, mpstModule->sysParam.nModuleID),
			(LPARAM)datalen);
	}
	else
	{
		_error = SPE_SIZE;
	}

	return _error;;
}

SBCPacketError CSBCObj::RecvEP_CMD_TAG_INFO(EP_MSG_HEADER* _hdr, BYTE* data, DWORD datalen)
{
	CSBCSyncObj SBCSync;

	SBCPacketError _error = SPE_NONE;

	if(data == NULL) return SPE_BODY_EMPTY;

	if(sizeof(EP_TAG_INFOMATION) == datalen)
	{
		SBCQueue_DATA QData;// = new SBCQueue_DATA;
		QData.id = (WPARAM)MAKELONG(0, mpstModule->sysParam.nModuleID);
		memcpy(&QData.Data, (LPVOID)data, sizeof(EP_TAG_INFOMATION));
		QData.command = _hdr->nCommand;
		QData.DataLength = datalen;

		g_Que.Push(QData);

		::PostMessage(hMsgWnd, 
			EPWM_MODULE_CHANGE_INFO, 
			(WPARAM)MAKELONG(0, mpstModule->sysParam.nModuleID), 
			(LPARAM)datalen);
	}
	else
	{
		_error = SPE_SIZE;
	}

	return _error;
}

SBCPacketError CSBCObj::RecvEP_CMD_AUTO_CHECK_RESULT(EP_MSG_HEADER* _hdr, BYTE* data, DWORD datalen)
{
	CSBCSyncObj SBCSync;

	SBCPacketError _error = SPE_NONE;

	if(data == NULL) return SPE_BODY_EMPTY;

	if(sizeof(EP_CHECK_RESULT) == datalen)
	{
		SBCQueue_DATA QData;// = new SBCQueue_DATA;
		QData.id = (WPARAM)MAKELONG(0, mpstModule->sysParam.nModuleID);
		memcpy(&QData.Data, (LPVOID)data, sizeof(EP_CHECK_RESULT));
		QData.command = _hdr->nCommand;
		QData.DataLength = datalen;

		g_Que.Push(QData);

		::PostMessage(hMsgWnd, 
			EPWM_MODULE_CHANGE_INFO, 
			(WPARAM)MAKELONG(0, mpstModule->sysParam.nModuleID), 
			(LPARAM)datalen);

	}
	else
	{
		_error = SPE_SIZE;
	}

	return _error;
}

SBCPacketError CSBCObj::RecvEP_CMD_AUTO_SENSOR_DATA(EP_MSG_HEADER* _hdr, BYTE* data, DWORD datalen)
{
	CSBCSyncObj SBCSync;

	SBCPacketError _error = SPE_NONE;

	if(data == NULL) return SPE_BODY_EMPTY;

	if(sizeof(EP_SENSOR_DATA) == datalen)
	{
		LPEP_GROUP_INFO lpGPData = (EP_GROUP_INFO *)mpstModule->gpData[0];
		ASSERT(lpGPData);

		memcpy(&lpGPData->gpData.sensorData, data, datalen);
		SYSTEMTIME systime;
		::GetLocalTime(&systime);

		for(int  i=0; i<EP_MAX_SENSOR_CH; i++)
		{
			if(lpGPData->gpData.sensorMinMax.sensorData1[i].wMaxIndex == 0)		//is it first data?
			{
				lpGPData->gpData.sensorMinMax.sensorData1[i].wMaxIndex = 1;
				lpGPData->gpData.sensorMinMax.sensorData1[i].lMin = lpGPData->gpData.sensorData.sensorData1[i].lData;
				lpGPData->gpData.sensorMinMax.sensorData1[i].lMax = lpGPData->gpData.sensorData.sensorData1[i].lData;
				sprintf(lpGPData->gpData.sensorMinMax.sensorData1[i].szMaxDateTime, "%04d/%02d/%02d %02d:%02d:%02d", systime.wYear, systime.wMonth, systime.wDay, systime.wHour, systime.wMinute, systime.wSecond);
				sprintf(lpGPData->gpData.sensorMinMax.sensorData1[i].szMinDateTime, "%04d/%02d/%02d %02d:%02d:%02d", systime.wYear, systime.wMonth, systime.wDay, systime.wHour, systime.wMinute, systime.wSecond);
			}
			else
			{
				if(lpGPData->gpData.sensorMinMax.sensorData1[i].lMax < lpGPData->gpData.sensorData.sensorData1[i].lData)
				{
					lpGPData->gpData.sensorMinMax.sensorData1[i].lMax = lpGPData->gpData.sensorData.sensorData1[i].lData;
					sprintf(lpGPData->gpData.sensorMinMax.sensorData1[i].szMaxDateTime, "%04d/%02d/%02d %02d:%02d:%02d", systime.wYear, systime.wMonth, systime.wDay, systime.wHour, systime.wMinute, systime.wSecond);
				}
				else if(lpGPData->gpData.sensorMinMax.sensorData1[i].lMin > lpGPData->gpData.sensorData.sensorData1[i].lData)
				{
					lpGPData->gpData.sensorMinMax.sensorData1[i].lMin = lpGPData->gpData.sensorData.sensorData1[i].lData;
					sprintf(lpGPData->gpData.sensorMinMax.sensorData1[i].szMinDateTime, "%04d/%02d/%02d %02d:%02d:%02d", systime.wYear, systime.wMonth, systime.wDay, systime.wHour, systime.wMinute, systime.wSecond);
				}
			}

			if(lpGPData->gpData.sensorMinMax.sensorData2[i].wMaxIndex == 0)		//First Temperature Data
			{
				lpGPData->gpData.sensorMinMax.sensorData2[i].wMaxIndex = 1;
				lpGPData->gpData.sensorMinMax.sensorData2[i].lMax = lpGPData->gpData.sensorData.sensorData2[i].lData;
				lpGPData->gpData.sensorMinMax.sensorData2[i].lMin = lpGPData->gpData.sensorData.sensorData2[i].lData;
				sprintf(lpGPData->gpData.sensorMinMax.sensorData2[i].szMaxDateTime, "%04d/%02d/%02d %02d:%02d:%02d", systime.wYear, systime.wMonth, systime.wDay, systime.wHour, systime.wMinute, systime.wSecond);
				sprintf(lpGPData->gpData.sensorMinMax.sensorData2[i].szMinDateTime, "%04d/%02d/%02d %02d:%02d:%02d", systime.wYear, systime.wMonth, systime.wDay, systime.wHour, systime.wMinute, systime.wSecond);
			}
			else
			{
				if(lpGPData->gpData.sensorMinMax.sensorData2[i].lMax < lpGPData->gpData.sensorData.sensorData2[i].lData)
				{
					lpGPData->gpData.sensorMinMax.sensorData2[i].lMax = lpGPData->gpData.sensorData.sensorData2[i].lData;
					sprintf(lpGPData->gpData.sensorMinMax.sensorData2[i].szMaxDateTime, "%04d/%02d/%02d %02d:%02d:%02d", systime.wYear, systime.wMonth, systime.wDay, systime.wHour, systime.wMinute, systime.wSecond);
				}
				else if(lpGPData->gpData.sensorMinMax.sensorData2[i].lMin > lpGPData->gpData.sensorData.sensorData2[i].lData)
				{
					lpGPData->gpData.sensorMinMax.sensorData2[i].lMin = lpGPData->gpData.sensorData.sensorData2[i].lData;
					sprintf(lpGPData->gpData.sensorMinMax.sensorData2[i].szMinDateTime, "%04d/%02d/%02d %02d:%02d:%02d", systime.wYear, systime.wMonth, systime.wDay, systime.wHour, systime.wMinute, systime.wSecond);
				}
			}
		}
	}
	else
	{
		_error = SPE_SIZE;
	}

	return _error;
}

SBCPacketError CSBCObj::RecvEP_CMD_AUTO_EMG_DATA(EP_MSG_HEADER* _hdr, BYTE* data, DWORD datalen)
{
	CSBCSyncObj SBCSync;

	SBCPacketError _error = SPE_NONE;

	if(data == NULL) return SPE_BODY_EMPTY;

	if(sizeof(EP_CODE) == datalen)
	{
		char errormsg[1024] = {0};
		EP_CODE codeData;
		memcpy(&codeData, (LPVOID)data, sizeof(EP_CODE));
		sprintf(errormsg, "Module %d Group %d, Emergency Detected. (EMG Code %d / %d)", 
			mpstModule->sysParam.nModuleID, _hdr->wGroupNum, 
			codeData.nCode, codeData.nData);

		CSBCLog::WriteErrorLog(errormsg);

		SBCQueue_DATA QData;// = new SBCQueue_DATA;
		QData.id = (WPARAM)MAKELONG(0, mpstModule->sysParam.nModuleID);
		memcpy(&QData.Data, (LPVOID)data, sizeof(EP_CODE));
		QData.command = _hdr->nCommand;
		QData.DataLength = datalen;

		g_Que.Push(QData);

		::PostMessage(hMsgWnd, 
			EPWM_MODULE_CHANGE_INFO, 
			(WPARAM)MAKELONG(0, mpstModule->sysParam.nModuleID), 
			(LPARAM)datalen);

		LPEP_GROUP_INFO lpGPData = (EP_GROUP_INFO *)mpstModule->gpData[0];
		ASSERT(lpGPData);
	}
	else
	{
		_error = SPE_SIZE;
	}

	return _error;
}

SBCPacketError CSBCObj::RecvEP_CMD_USER_CMD(EP_MSG_HEADER* _hdr, BYTE* data, DWORD datalen)
{
	CSBCSyncObj SBCSync;

	SBCPacketError _error = SPE_NONE;

	if(data == NULL) return SPE_BODY_EMPTY;

	if(sizeof(EP_USER_CMD_INFO) == datalen)
	{

		EP_USER_CMD_INFO UserCmd;
		::CopyMemory(&UserCmd, (LPVOID)data, sizeof(EP_USER_CMD_INFO));

		char usermsg[1024] = {0};
		sprintf(usermsg, "Module %d Group %d, User Command Detected. (Command 0x%08x / %d)", 
			mpstModule->sysParam.nModuleID, _hdr->wGroupNum, 
			UserCmd.nCmd, UserCmd.nData);

		CSBCLog::WriteErrorLog(usermsg);


		SBCQueue_DATA QData;// = new SBCQueue_DATA;
		QData.id = (WPARAM)MAKELONG(0, mpstModule->sysParam.nModuleID);
		memcpy(&QData.Data, (LPVOID)data, sizeof(EP_USER_CMD_INFO));
		QData.command = _hdr->nCommand;
		QData.DataLength = datalen;

		g_Que.Push(QData);

		::PostMessage(hMsgWnd, 
			EPWM_MODULE_CHANGE_INFO, 
			(WPARAM)MAKELONG(0, mpstModule->sysParam.nModuleID), 
			(LPARAM)datalen);

	}
	else
	{
		_error = SPE_SIZE;
	}

	return _error;
}

SBCPacketError CSBCObj::RecvEP_CMD_REAL_TIME_DATA(EP_MSG_HEADER* _hdr, BYTE* data, DWORD datalen)
{
	CSBCSyncObj SBCSync;

	SBCPacketError _error = SPE_NONE;

	if(data == NULL) return SPE_BODY_EMPTY;

	if(sizeof(EP_REAL_TIME_DATA) * mpstModule->sysData.nTotalChNo == datalen)
	{
		LPEP_GROUP_INFO lpGPData = (LPEP_GROUP_INFO)mpstModule->gpData[0];
		ASSERT(lpGPData);

		EP_REAL_TIME_DATA	*lpChannel;
		int index;
		for(INDEX ch = 0; ch < mpstModule->sysData.nTotalChNo; ch++)
		{
			lpChannel = (EP_REAL_TIME_DATA *)((char *)data + ch * sizeof(EP_REAL_TIME_DATA));
			index = GetMappingCh(ch, mpstModule->sysData.wTrayType, lpChannel->nChIndex);

			if(index < 0 || index >= mpstModule->sysData.nTotalChNo)
			{
				_error = SPE_EP_CMD_REAL_TIME_DATA_Channel_Index_Error;
				break;
			}
			else
			{
				lpChannel = (EP_REAL_TIME_DATA	*)lpGPData->chRealData[index];
				memcpy(lpChannel, (char *)data+sizeof(EP_REAL_TIME_DATA)*ch, sizeof(EP_REAL_TIME_DATA));
				lpChannel->nChIndex = index;
			}
		}

		if(_error == SPE_NONE)
		{
			SBCQueue_DATA QData;// = new SBCQueue_DATA;
			QData.id = (WPARAM)MAKELONG(0, mpstModule->sysParam.nModuleID);
			memcpy(&QData.Data, lpChannel
				, sizeof(EP_REAL_TIME_DATA) * mpstModule->sysData.nTotalChNo);
			QData.command = _hdr->nCommand;
			QData.DataLength = datalen;

			g_Que.Push(QData);

			::PostMessage(hMsgWnd, 
				EPWM_MODULE_CHANGE_INFO, 
				(WPARAM)MAKELONG(0, mpstModule->sysParam.nModuleID), 
				(LPARAM)datalen);
		}
	}
	else
	{
		_error = SPE_SIZE;
	}

	return _error;
}


//////////////////////////////////////////////////////////////////////////
//cal
SBCPacketError CSBCObj::RecvEP_CMD_CAL_RES(EP_MSG_HEADER* _hdr, BYTE* data, DWORD datalen)
{
	CSBCSyncObj SBCSync;

	SBCPacketError _error = SPE_NONE; 

	if(data == NULL) return SPE_BODY_EMPTY;

	if(sizeof(EP_CALIHCK_RESULT_DATA) == datalen)
	{
		EP_CALIHCK_RESULT_DATA codeData;
		ZeroMemory(&codeData, sizeof(EP_CALIHCK_RESULT_DATA));
		memcpy(&codeData, data, sizeof(EP_CALIHCK_RESULT_DATA));

		char usermsg[1024] = {0};
		sprintf(usermsg, "Module %d Group %d, Calibration data received."
			, mpstModule->sysParam.nModuleID, _hdr->wGroupNum);
		CSBCLog::WriteErrorLog(usermsg);

		SBCQueue_DATA QData;// = new SBCQueue_DATA;

		QData.id = (WPARAM)MAKELONG(0, mpstModule->sysParam.nModuleID);

		memcpy(&QData.Data, (LPVOID)data, sizeof(EP_CALIHCK_RESULT_DATA));
		QData.command = _hdr->nCommand;
		QData.DataLength = datalen;

		g_Que.Push(QData);

		::PostMessage(hMsgWnd, 
			EPWM_MODULE_CHANGE_INFO, 
			(WPARAM)MAKELONG(0, mpstModule->sysParam.nModuleID), 
			(LPARAM)datalen);
		
	}

	return _error;
}
SBCPacketError CSBCObj::RecvEP_CMD_CHK_RES(EP_MSG_HEADER* _hdr, BYTE* data, DWORD datalen)
{
	CSBCSyncObj SBCSync;

	SBCPacketError _error = SPE_NONE; 

	if(data == NULL) return SPE_BODY_EMPTY;

	if(sizeof(EP_CALIHCK_RESULT_DATA) == datalen)
	{
		EP_CALIHCK_RESULT_DATA codeData;
		ZeroMemory(&codeData, sizeof(EP_CALIHCK_RESULT_DATA));
		memcpy(&codeData, data, sizeof(EP_CALIHCK_RESULT_DATA));

		char usermsg[1024] = {0};
		sprintf(usermsg, "Module %d Group %d, Calibration data received."
			, mpstModule->sysParam.nModuleID, _hdr->wGroupNum);
		CSBCLog::WriteErrorLog(usermsg);

		SBCQueue_DATA QData;// = new SBCQueue_DATA;

		QData.id = (WPARAM)MAKELONG(1, mpstModule->sysParam.nModuleID);

		memcpy(&QData.Data, (LPVOID)data, sizeof(EP_CALIHCK_RESULT_DATA));
		QData.command = _hdr->nCommand;
		QData.DataLength = datalen;

		g_Que.Push(QData);

		::PostMessage(hMsgWnd, 
			EPWM_MODULE_CHANGE_INFO, 
			(WPARAM)MAKELONG(1, mpstModule->sysParam.nModuleID), 
			(LPARAM)datalen);

	}

	return _error;
}
SBCPacketError CSBCObj::RecvEP_CMD_CAL_END(EP_MSG_HEADER* _hdr, BYTE* data, DWORD datalen)
{
	CSBCSyncObj SBCSync;

	SBCPacketError _error = SPE_NONE; 

	if(data == NULL) return SPE_BODY_EMPTY;

	if(sizeof(EP_CAL_SELECT_INFO) == datalen)
	{
		EP_CAL_SELECT_INFO codeData;
		ZeroMemory(&codeData, sizeof(EP_CAL_SELECT_INFO));
		memcpy(&codeData, data, sizeof(EP_CAL_SELECT_INFO));

		char usermsg[1024] = {0};
		sprintf(usermsg, "Module %d Group %d, Cali END Info received."
			, mpstModule->sysParam.nModuleID, _hdr->wGroupNum);
		CSBCLog::WriteErrorLog(usermsg);

		SBCQueue_DATA QData;// = new SBCQueue_DATA;

		QData.id = (WPARAM)MAKELONG(0, mpstModule->sysParam.nModuleID);

		memcpy(&QData.Data, (LPVOID)data, sizeof(EP_CAL_SELECT_INFO));
		QData.command = _hdr->nCommand;
		QData.DataLength = datalen;

		g_Que.Push(QData);

		::PostMessage(hMsgWnd, 
			EPWM_MODULE_CHANGE_INFO, 
			(WPARAM)MAKELONG(0, mpstModule->sysParam.nModuleID), 
			(LPARAM)datalen);

	}

	return _error;
}
SBCPacketError CSBCObj::RecvEP_CMD_CHK_END(EP_MSG_HEADER* _hdr, BYTE* data, DWORD datalen)
{
	CSBCSyncObj SBCSync;

	SBCPacketError _error = SPE_NONE; 

	if(data == NULL) return SPE_BODY_EMPTY;

	if(sizeof(EP_CAL_SELECT_INFO) == datalen)
	{
		EP_CAL_SELECT_INFO codeData;
		ZeroMemory(&codeData, sizeof(EP_CAL_SELECT_INFO));
		memcpy(&codeData, data, sizeof(EP_CAL_SELECT_INFO));

		char usermsg[1024] = {0};
		sprintf(usermsg, "Module %d Group %d, Cali END Info received."
			, mpstModule->sysParam.nModuleID, _hdr->wGroupNum);
		CSBCLog::WriteErrorLog(usermsg);

		SBCQueue_DATA QData;// = new SBCQueue_DATA;

		QData.id = (WPARAM)MAKELONG(0, mpstModule->sysParam.nModuleID);

		memcpy(&QData.Data, (LPVOID)data, sizeof(EP_CAL_SELECT_INFO));
		QData.command = _hdr->nCommand;
		QData.DataLength = datalen;

		g_Que.Push(QData);

		::PostMessage(hMsgWnd, 
			EPWM_MODULE_CHANGE_INFO, 
			(WPARAM)MAKELONG(1, mpstModule->sysParam.nModuleID), 
			(LPARAM)datalen);

	}

	return _error;
}
//////////////////////////////////////////////////////////////////////////
//??
SBCPacketError CSBCObj::RecvEP_CMD_RM_RES(EP_MSG_HEADER* _hdr, BYTE* data, DWORD datalen)
{
	CSBCSyncObj SBCSync;

	SBCPacketError _error = SPE_NONE; 

	if(data == NULL) return SPE_BODY_EMPTY;

	if(sizeof(EP_REAL_MEASURE_RESULT_DATA) == datalen)
	{
		EP_REAL_MEASURE_RESULT_DATA codeData;
		ZeroMemory(&codeData, sizeof(EP_REAL_MEASURE_RESULT_DATA));
		memcpy(&codeData, data, sizeof(EP_REAL_MEASURE_RESULT_DATA));

		char usermsg[1024] = {0};
		sprintf(usermsg, "Module %d Group %d, Real Measure Data received."
			, mpstModule->sysParam.nModuleID, _hdr->wGroupNum);
		CSBCLog::WriteErrorLog(usermsg);

		SBCQueue_DATA QData;// = new SBCQueue_DATA;
		QData.id = (WPARAM)MAKELONG(0, mpstModule->sysParam.nModuleID);
		memcpy(&QData.Data, (LPVOID)data, sizeof(EP_REAL_MEASURE_RESULT_DATA));
		QData.command = _hdr->nCommand;
		QData.DataLength = datalen;

		g_Que.Push(QData);

		::PostMessage(hMsgWnd, 
			EPWM_MODULE_CHANGE_INFO, 
			(WPARAM)MAKELONG(0, mpstModule->sysParam.nModuleID), 
			(LPARAM)datalen);

	}

	return _error;
}
SBCPacketError CSBCObj::RecvEP_CMD_RM_STOP_RESPONS(EP_MSG_HEADER* _hdr, BYTE* data, DWORD datalen)
{
	CSBCSyncObj SBCSync;

	SBCPacketError _error = SPE_NONE; 

	if(data == NULL) return SPE_BODY_EMPTY;

	char usermsg[1024] = {0};
	sprintf(usermsg, "Module %d Group %d, Real Measure Data received."
		, mpstModule->sysParam.nModuleID, _hdr->wGroupNum);
	CSBCLog::WriteErrorLog(usermsg);

	::PostMessage(hMsgWnd, EPWM_RM_WORK_END_RECEIVED
		, MAKELONG(0, mpstModule->sysParam.nModuleID), 0);

	return _error;
}

//////////////////////////////////////////////////////////////////////////
SBCPacketError CSBCObj::RecvEP_CMD_LINE_MODE_DATA(EP_MSG_HEADER* _hdr, BYTE* data, DWORD datalen)
{
	CSBCSyncObj SBCSync;

	SBCPacketError _error = SPE_NONE;

	if(data == NULL) return SPE_BODY_EMPTY;

	if(sizeof(EP_LINE_MODE_SET) == datalen)
	{
		EP_LINE_MODE_SET lineSet;
		memcpy(&lineSet, data, sizeof(lineSet));
		mpstModule->nOperationMode = lineSet.nOperationMode;
		mpstModule->nLineMode = lineSet.nOnLineMode;
		mpstModule->nWorkType = lineSet.nWorkType;

		SBCQueue_DATA QData;// = new SBCQueue_DATA;

		memcpy(&QData.Data, (LPVOID)data, sizeof(EP_LINE_MODE_SET));

		QData.command = _hdr->nCommand;
		QData.DataLength = datalen;

		g_Que.Push(QData);
				
		::PostMessage(hMsgWnd, 
			EPWM_MODULE_CHANGE_INFO, 
			MAKELONG(0, GetModuleID()), 
			datalen);
	}
	else
	{
		_error = SPE_SIZE;
	}

	return _error;
}
SBCPacketError CSBCObj::RecvEP_CMD_MAPPING_DATA(EP_MSG_HEADER* _hdr, BYTE* data, DWORD datalen)
{
	CSBCSyncObj SBCSync;

	SBCPacketError _error = SPE_NONE;

	if(sizeof(EP_HW_MAPPIING_DATA) == datalen)
	{
		SendNotifyDlgWnd(_hdr, data, datalen);
	}
	else
	{
		_error = SPE_SIZE;
	}
	return _error;
}
SBCPacketError CSBCObj::RecvEP_CMD_CAL_POINT_DATA(EP_MSG_HEADER* _hdr, BYTE* data, DWORD datalen)
{
	CSBCSyncObj SBCSync;

	SBCPacketError _error = SPE_NONE;

	if(sizeof(EP_CAL_POINT_SET) == datalen)
	{
		SendNotifyDlgWnd(_hdr, data, datalen);
	}
	else
	{
		_error = SPE_SIZE;
	}

	return _error;
}
VOID CSBCObj::SetNotifyDlgWnd(Request_CMD* _pInfo)
{
	CSBCSyncObj SBCSync;

	m_LstRequestCmd.push_back(_pInfo);
}
VOID CSBCObj::SendNotifyDlgWnd(EP_MSG_HEADER* _hdr, BYTE* data, DWORD datalen)
{
	CSBCSyncObj SBCSync;

	Request_CMD *pInfoDel = NULL;
	for (std::list<Request_CMD*>::iterator it=m_LstRequestCmd.begin();it!=m_LstRequestCmd.end();it++)
	{
		//cwm 못지우는 것 하나!!!!!
		Request_CMD *pInfo = (Request_CMD*)(*it);
		if (pInfo->Cmd == _hdr->nCommand)
		{
			if(pInfo->hWnd)
			{
				SBCQueue_DATA QData;// = new SBCQueue_DATA;
				QData.id = (WPARAM)MAKELONG(0, mpstModule->sysParam.nModuleID);

				memcpy(&QData.Data, (LPVOID)data, datalen);

				QData.command = _hdr->nCommand;
				QData.DataLength = datalen;

				g_DlgQue.Push(QData);

				::PostMessage(pInfo->hWnd, 
					EPWM_MODULE_CHANGE_INFO, 
					MAKELONG(0, GetModuleID()), 
					datalen);
			}
			pInfoDel = pInfo;
			break;
		}
	}

	if(pInfoDel)
	{
		m_LstRequestCmd.remove(pInfoDel);
		delete pInfoDel;
	}

}

VOID CSBCObj::SetRecvLog(EP_MSG_HEADER* _hdr, BYTE* data, DWORD datalen)
{
	CSBCSyncObj SBCSync;

	CSBCLog::WritePacketLog(GetModuleID(), "OnRead=====%x %d========> %d %d %d Command[0x%08X] %d [Qcnt: [%d]]\n"
		, this, datalen
		, _hdr->nID
		, _hdr->wGroupNum
		, _hdr->wChannelNum
		, _hdr->nCommand
		, _hdr->nLength
		, g_Que.GetCount());
}
//////////////////////////////////////////////////////////////////////////
//SBCPacketError CSBCObj::RecvEP_CMD_ALL_NG(EP_MSG_HEADER* _hdr, BYTE* data, DWORD datalen)
//{
//	CSBCSyncObj SBCSync;
//
//	SBCPacketError _error = SPE_NONE;
//
//	::PostMessage(hMsgWnd, EPWM_MODULE_ALL_NG, MAKELONG(0, mpstModule->sysParam.nModuleID),0); 
//
//	return _error;
//}
//SBCPacketError CSBCObj::RecvEP_CMD_SWITCH(EP_MSG_HEADER* _hdr, BYTE* data, DWORD datalen)
//{
//	CSBCSyncObj SBCSync;
//
//	SBCPacketError _error = SPE_NONE;
//
//	if(data == NULL) return SPE_BODY_EMPTY;
//
//	if(sizeof(EP_MD_SWITCH_DATA) == datalen)
//	{
//		SBCQueue_DATA QData;// = new SBCQueue_DATA;
//		QData.id = (WPARAM)MAKELONG(0, mpstModule->sysParam.nModuleID);
//		memcpy(&QData.Data, (LPVOID)data, sizeof(EP_MD_SWITCH_DATA));
//		QData.command = _hdr->nCommand;
//		QData.DataLength = datalen;
//
//		g_Que.Push(QData);
//
//		::PostMessage(hMsgWnd, 
//			EPWM_MODULE_CHANGE_INFO, 
//			(WPARAM)MAKELONG(0, mpstModule->sysParam.nModuleID), 
//			(LPARAM)datalen);
//	}
//	else
//	{
//		_error = SPE_SIZE;
//	}
//
//	return _error;
//}
//SBCPacketError CSBCObj::RecvEP_CMD_CAL_RESULT(EP_MSG_HEADER* _hdr, BYTE* data, DWORD datalen)
//{
//	CSBCSyncObj SBCSync;
//
//	SBCPacketError _error = SPE_NONE;
//
//	if(data == NULL) return SPE_BODY_EMPTY;
//
//	if(sizeof(EP_CALI_END_DATA) == datalen)
//	{
//		SBCQueue_DATA QData;// = new SBCQueue_DATA;
//		QData.id = mpstModule->sysParam.nModuleID;
//		QData.command = _hdr->nCommand;
//		memcpy(QData.Data, data, datalen);
//		QData.DataLength = datalen;
//
//		g_Que.Push(QData);
//
//		::PostMessage(hMsgWnd, 
//			EPWM_MODULE_CHANGE_INFO, 
//			(WPARAM)mpstModule->sysParam.nModuleID, 
//			(LPARAM)sizeof(EP_CALI_END_DATA));
//	}
//
//	return _error;
//}
//SBCPacketError CSBCObj::RecvEP_CMD_AUTO_GP_STEP_SEC_DATA(EP_MSG_HEADER* _hdr, BYTE* data, DWORD datalen)
//{
//	CSBCSyncObj SBCSync;
//
//	SBCPacketError _error = SPE_NONE;
//
//	if(data == NULL) return SPE_BODY_EMPTY;
//
//	INT nItemResultSize = sizeof(EP_STEP_END_HEADER);
//
//	if((SizeofCHData()*mpstModule->sysData.nTotalChNo+nItemResultSize) == datalen)
//	{
//		EP_STEP_END_HEADER *pStepEndHeader = (EP_STEP_END_HEADER *)data;
//		ASSERT(pStepEndHeader);
//
//		INT wrSize = 0;
//		INT index = 0;
//
//		LPEP_GROUP_INFO lpGPData = (EP_GROUP_INFO *)mpstModule->gpData[0];
//		LPEP_CH_DATA	lpChDest, lpChSource;
//
//		SBCQueue_DATA QData;// = new SBCQueue_DATA;
//
//		for(INDEX ch = 0; ch<mpstModule->sysData.nTotalChNo; ch++)
//		{				
//			wrSize = SizeofCHData()*ch;
//			lpChSource = (EP_CH_DATA *)((char *)data+nItemResultSize+wrSize);
//
//			//Host Mapping
//			index = GetMappingCh(ch, mpstModule->sysData.wTrayType, lpChSource->wChIndex);
//
//			if(index < 0 || index >=mpstModule->sysData.nTotalChNo)
//			{
//				//response.nCode = EP_NACK;
//				//sprintf(msg, "Module %d Group %d, Channel Index Error %d", m_stModule[nModuleIndex].sysParam.nModuleID, lpHeader->wGroupNum, index);
//				//WriteLog(msg);
//				_error = SPE_EP_CMD_AUTO_GP_STEP_SEC_DATA_Channel_Index_Error;
//				break;
//			}
//			else
//			{
//				lpChSource = (EP_CH_DATA *)((char *)data+nItemResultSize+wrSize);
//				lpChDest =	(EP_CH_DATA *)lpGPData->chData[index];
//				ASSERT(lpChDest);
//				memcpy(lpChDest, (char *)lpChSource, SizeofCHData());
//				lpChDest->wChIndex = (WORD)index;
//				lpChSource->wChIndex = (WORD)index;					
//				memcpy((char *)QData.Data+nItemResultSize+index*SizeofCHData(), lpChSource, SizeofCHData());	//Result 결과를 재정렬 한다.
//			}
//		}
//
//		if(mpstModule->lpDataBuff)
//		{
//			delete [] mpstModule->lpDataBuff;
//			mpstModule->lpDataBuff = NULL;
//		}
//
//		mpstModule->lpDataBuff = new CHAR[datalen];
//		memcpy(mpstModule->lpDataBuff, QData.Data, datalen);
//
//		QData.id = MAKELONG(_hdr->wGroupNum-1, mpstModule->sysParam.nModuleID);
//		QData.command = _hdr->nCommand;
//		QData.DataLength = datalen;
//
//		g_Que.Push(QData);
//
//		::PostMessage(hMsgWnd, 
//			EPWM_MODULE_CHANGE_INFO, 
//			(WPARAM)MAKELONG(_hdr->wGroupNum-1, mpstModule->sysParam.nModuleID), 
//			(LPARAM)datalen);
//	}
//	else
//	{
//		_error = SPE_SIZE;
//	}
//
//	return _error;
//}
//SBCPacketError CSBCObj::RecvEP_CMD_REAL_CHAMBER_DATA(EP_MSG_HEADER* _hdr, BYTE* data, DWORD datalen)
//{
//	CSBCSyncObj SBCSync;
//
//	SBCPacketError _error = SPE_NONE;
//
//	if(data == NULL) return SPE_BODY_EMPTY;
//
//	if(sizeof(EP_REAL_CHAMBER_DATA) == datalen)
//	{
//		EP_REAL_CHAMBER_DATA *pChamberData = (EP_REAL_CHAMBER_DATA *)data;
//		if(pChamberData->bDoorState == 0)pChamberData->bDoorState = 2;
//		SBCQueue_DATA QData;// = new SBCQueue_DATA;
//		QData.id = (WPARAM)MAKELONG(0, mpstModule->sysParam.nModuleID);
//		QData.command = _hdr->nCommand;
//		::CopyMemory(&QData.Data, (LPVOID)data, sizeof(EP_REAL_CHAMBER_DATA));
//		QData.DataLength = datalen;
//
//		g_Que.Push(QData);
//
//		::PostMessage(hMsgWnd, 
//			EPWM_MODULE_CHANGE_INFO, 
//			(WPARAM)MAKELONG(0, mpstModule->sysParam.nModuleID), 
//			(LPARAM)sizeof(EP_REAL_CHAMBER_DATA));
//	}
//	else
//	{
//		_error = SPE_SIZE;
//	}
//
//	return _error;
//}