// CalibratorDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ctsmon.h"
#include "CalibratorDlg.h"
#include "CaliSetDlg.h"
#include "CaliStartDlg.h"
#include "CTSMonDoc.h"
#include "CaliPoint.h"
#include "AccuracySetDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCalibratorDlg dialog


CCalibratorDlg::CCalibratorDlg(int nModuleID,CWnd* pParent /*=NULL*/)
	: CDialog(CCalibratorDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCalibratorDlg)
	m_nDataMode = CAL_DATA_MODE_BEFORE;
	m_nDataType = CAL_DATA_TYPE_VTG;		//default votage cal
	m_nOptTrayType = -1;
	//}}AFX_DATA_INIT

	m_nUnitNo = nModuleID;

	EP_MD_SYSTEM_DATA *pSysData = ::EPGetModuleSysData(EPGetModuleIndex(m_nUnitNo));

 	//Total Channel
 	m_nTotCh = pSysData->nTotalChNo;
	m_nTotBoardNo = pSysData->wInstalledBoard;

#ifdef _DCIR
	m_boardColor[0] = RGB(248, 183, 169);
	m_boardColor[1] = RGB(242, 245, 135);
	m_boardColor[2] = RGB(173, 247, 134);
	m_boardColor[3] = RGB(134, 247, 207);
	m_boardColor[4] = RGB(135, 200, 245);
	m_boardColor[5] = RGB(134, 146, 247);
	m_boardColor[6] = RGB(193, 134, 247);
	m_boardColor[7] = RGB(248, 133, 244);
#else
	int i=0;
	for( i=0; i<EP_MAX_BD_PER_MD; )
	{
		m_boardColor[i++] = RGB(248, 183, 169);
		m_boardColor[i++] = RGB(242, 245, 135);
		m_boardColor[i++] = RGB(173, 247, 134);
		m_boardColor[i++] = RGB(134, 247, 207);
		m_boardColor[i++] = RGB(135, 200, 245);
		m_boardColor[i++] = RGB(134, 146, 247);
		m_boardColor[i++] = RGB(193, 134, 247);
		m_boardColor[i++] = RGB(248, 133, 244);
		m_boardColor[i++] = RGB(203, 255, 154);
		m_boardColor[i++] = RGB(184, 255, 247);
	}
#endif	

	
	
	m_nTimeOutCnt = 0;
	m_nState = CAL_STATE_IDLE;

	m_nOnePointElapsedTime = (CAL_TIME_OUT_SEC-1)*1000;
	m_nCalType = CAL_TYPE_CALCHECK;		//교정 AND Check
	m_nBoardNo = 1;		//default Board #1

	m_bMultiMode = FALSE;

	LanguageinitMonConfig();
}

CCalibratorDlg::~CCalibratorDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

bool CCalibratorDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCalibratorDlg"), _T("TEXT_CCalibratorDlg_CNT"), _T("TEXT_CCalibratorDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CCalibratorDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCalibratorDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

void CCalibratorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCalibratorDlg)
	DDX_Control(pDX, IDC_STATIC_STATE, m_wndCalState);
	DDX_CBIndex(pDX, IDC_COMBO_MODE, m_nDataMode);
	DDX_CBIndex(pDX, IDC_COMBO_TYPE, m_nDataType);
	DDX_Radio(pDX, IDC_OPT_TRAY_TYPE_1, m_nOptTrayType);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCalibratorDlg, CDialog)
	//{{AFX_MSG_MAP(CCalibratorDlg)
	ON_BN_CLICKED(IDC_BTN_SET_POINT, OnBtnSetPoint)	
	ON_BN_CLICKED(IDC_CALI_START_BTN, OnCaliStartBtn)
	ON_BN_CLICKED(IDC_STOP_BTN, OnStopBtn)
	ON_BN_CLICKED(IDC_PAUSE_BTN, OnPauseBtn)
	ON_BN_CLICKED(IDC_CALI_DATA_SEARCH, OnCaliDataSearch)
	ON_BN_CLICKED(IDC_RESUME_BTN, OnResumeBtn)
	ON_BN_CLICKED(IDC_CALI_DATA_UPDATE, OnCaliDataUpdate)
	ON_CBN_SELCHANGE(IDC_COMBO_MODE, OnSelchangeComboMode)
	ON_CBN_SELCHANGE(IDC_COMBO_TYPE, OnSelchangeComboType)
	ON_COMMAND(ID_SEL_START, OnSelStart)
	ON_BN_CLICKED(IDC_BUTTON_ACCU, OnButtonAccu)
	ON_UPDATE_COMMAND_UI(ID_SEL_START, OnUpdateSelStart)
	ON_WM_TIMER()
	ON_COMMAND(ID_CAL_STOP, OnCalStop)
	ON_COMMAND(ID_CAL_DATA_UPDATE, OnCalDataUpdate)
	ON_BN_CLICKED(IDC_OPT_TRAY_TYPE_1, OnOptTrayType1)
	ON_BN_CLICKED(IDC_OPT_TRAY_TYPE_2, OnOptTrayType2)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_GRID_RIGHT_CLICK,	OnRButtonClickedRowCol)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCalibratorDlg message handlers

void CCalibratorDlg::OnBtnSetPoint() 
{
	// TODO: Add your control notification handler code here	
	CCaliSetDlg dlg(&m_calibrtaionData.m_PointData, 0, this);

	if( dlg.DoModal() == IDOK)
	{
		//Main cal point 하달 필요
		//Ch cal point 하달 
		WORD i=0, j=0;
		
		EP_CAL_POINT_SET	calPointData;		//sbc 전송용 구조체 
 		ZeroMemory(&calPointData, sizeof(EP_CAL_POINT_SET));
		
		calPointData.nMain = 1;		//ch point
		calPointData.byValidVRangeNum = (BYTE)m_calibrtaionData.m_PointData.GetVRangeCnt();
		calPointData.byValidIRangeNum = (BYTE)m_calibrtaionData.m_PointData.GetIRangeCnt();

		for(i=0; i<calPointData.byValidVRangeNum; i++)
		{
 			calPointData.vCalPoinSet[i].byValidPointNum = (BYTE)m_calibrtaionData.m_PointData.GetVtgSetPointCount(i);
			for(j=0; j<calPointData.vCalPoinSet[i].byValidPointNum; j++)
			{
 				calPointData.vCalPoinSet[i].lCaliPoint[j] = long(m_calibrtaionData.m_PointData.GetVSetPoint(i, j)*1000.0f);
			}
		}
		for( i=0; i<calPointData.byValidIRangeNum; i++)
		{
			calPointData.iCalPoinSet[i].byValidPointNum = (BYTE)m_calibrtaionData.m_PointData.GetCrtSetPointCount(i);
			for(j=0; j<calPointData.iCalPoinSet[i].byValidPointNum; j++)
			{
				calPointData.iCalPoinSet[i].lCaliPoint[j] = long(m_calibrtaionData.m_PointData.GetISetPoint(i, j)*1000.0f);
			}
		}
		for( i=0; i<calPointData.byValidVRangeNum; i++)
		{
			calPointData.vCalPoinSet[i].byCheckPointNum = (BYTE)m_calibrtaionData.m_PointData.GetVtgCheckPointCount(i);
			for(j=0; j<calPointData.vCalPoinSet[i].byCheckPointNum; j++)
			{
				calPointData.vCalPoinSet[i].lCheckPoint[j] = long(m_calibrtaionData.m_PointData.GetVCheckPoint(i, j)*1000.0f);
			}
		}
		for( i=0; i<calPointData.byValidIRangeNum; i++)
		{
			calPointData.iCalPoinSet[i].byCheckPointNum = (BYTE)m_calibrtaionData.m_PointData.GetCrtCheckPointCount(i);
			for(j=0; j<calPointData.iCalPoinSet[i].byCheckPointNum; j++)
			{
				calPointData.iCalPoinSet[i].lCheckPoint[j] = long(m_calibrtaionData.m_PointData.GetICheckPoint(i, j)*1000.0f);
			}
		}

		//ShuntR 추가
		for(i =0; i<CAL_MAX_BOARD_NUM; i++)
		{
			calPointData.lCaliShuntR[i] = m_calibrtaionData.m_PointData.GetShuntR(i)*10000.0f;
			calPointData.lCaliShuntT[i] = m_calibrtaionData.m_PointData.GetShuntT(i)*10000.0f;
			sprintf(&calPointData.byCaliShuntSerail[i][0], "%s", m_calibrtaionData.m_PointData.GetShuntSerial(i));
		}
		
		//[Send]-------------------------------------------------------------------------------------------------------------
		int nRtn;
		//Cali Point Set PC->SBC
		if((nRtn = EPSendCommand(m_nUnitNo, 0, 0, EP_CMD_SET_CAL_POINT, &calPointData, sizeof(calPointData)))!= EP_ACK)
		{
			CString str;
			str.Format(TEXT_LANG[0], GetModuleName(m_nUnitNo));//"[%s]에 교정 Point 전송을 실패 하였습니다."
			AfxMessageBox(str);	
		}

		ReDrawGrid(m_nDataMode, m_nDataType);
		DisplayData(m_nDataMode, m_nDataType);
	}
}

void CCalibratorDlg::InitCalGrid()
{
	m_wndCalData.SubclassDlgItem(IDC_CAL_DATA_GRID, this);
	m_wndCalData.m_bSameRowSize = FALSE;
	m_wndCalData.m_bSameColSize = FALSE;
	m_wndCalData.m_bCustomWidth = FALSE;

	m_wndCalData.Initialize();
	m_wndCalData.EnableCellTips();

	BOOL bLock = m_wndCalData.LockUpdate();

	m_wndCalData.SetDefaultRowHeight(18);
	m_wndCalData.SetDefaultColWidth(80);
	m_wndCalData.SetRowCount(m_nTotCh+_FROZEN_ROW_CNT_);
	m_wndCalData.SetColCount(_FROZEN_COL_CNT_);
	m_wndCalData.SetScrollBarMode(SB_VERT, gxnEnabled | gxnEnhanced);	//Only Vertical Scroll Bar

	m_wndCalData.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	//Use MemDc
	m_wndCalData.SetDrawingTechnique(gxDrawUsingMemDC);
	m_wndCalData.GetParam()->SetSortRowsOnDblClk(TRUE);


	m_wndCalData.SetValueRange(CGXRange(0, 0), "No");
	m_wndCalData.SetValueRange(CGXRange(0, _MON_CH_COL_), "Mon Ch");
	m_wndCalData.SetValueRange(CGXRange(0, _HW_CH_COL), "BD-CH(H/W)");	
	m_wndCalData.SetColWidth(0 ,0, 0);
	m_wndCalData.SetColWidth(_MON_CH_COL_ ,_MON_CH_COL_, 60);
	m_wndCalData.SetColWidth(_HW_CH_COL ,_HW_CH_COL, 90);
	m_wndCalData.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(9).SetFaceName(GetStringTable(IDS_LANG_TEXT_FONT)).SetBold(FALSE)));
	m_wndCalData.SetFrozenCols(_FROZEN_COL_CNT_, _FROZEN_COL_CNT_);
	m_wndCalData.SetFrozenRows(_FROZEN_ROW_CNT_, _FROZEN_ROW_CNT_);
	for(int n=0; n<_FROZEN_ROW_CNT_; n++)
	{
		m_wndCalData.SetCoveredCellsRowCol(n+1, 1, n+1, _FROZEN_COL_CNT_);
	}
	m_wndCalData.SetStyleRange(CGXRange().SetRows(1, _FROZEN_ROW_CNT_), CGXStyle().SetInterior(RGB(230,230,230)));

	m_wndCalData.SetValueRange(CGXRange().SetCells(1,1), "MIN");
	m_wndCalData.SetValueRange(CGXRange().SetCells(2,1), "MAX");
	m_wndCalData.SetValueRange(CGXRange().SetCells(3,1), "AVG");
	m_wndCalData.SetValueRange(CGXRange().SetCells(4,1), "STDEV");

	m_wndCalData.EnableGridToolTips();
    m_wndCalData.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, 
         CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));

	m_wndCalData.SetStyleRange(CGXRange().SetCols(_HW_CH_COL, _HW_CH_COL), 
								CGXStyle()
								.SetTextColor(RGB(0,0,0))									// blue progress bar
								.SetInterior(RGB(192, 192, 192))							// on white background
								);

	m_wndCalData.LockUpdate(bLock);
	m_wndCalData.Redraw();
}

BOOL CCalibratorDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// TODO: Add extra initialization here
	// 1. 화면 크기에 맞춰서 Dlg 크기를 늘인 후
	// 2. 그리드의 크기를 늘린다.
	DEVMODE mode;
	CRect rt;
	
    memset(&mode, 0, sizeof(DEVMODE));
	::EnumDisplaySettings(NULL,ENUM_CURRENT_SETTINGS, &mode);
	
	rt.left = 10;
	rt.top = 10;
	rt.bottom = mode.dmPelsHeight-50;
	rt.right = mode.dmPelsWidth-10;
	this->MoveWindow( rt );	

	InitCalGrid();

	m_nOptTrayType = 0;	//ljb 2011519 Tray Type 초기화

	CRect ctrlRect;				
	if(m_wndCalData.GetSafeHwnd())
	{
		m_wndCalData.GetWindowRect(&ctrlRect);
		ScreenToClient(&ctrlRect);
		m_wndCalData.MoveWindow(ctrlRect.left, ctrlRect.top, rt.right-ctrlRect.left-20, rt.bottom-ctrlRect.top-50, FALSE);
		m_wndCalData.Redraw();
	}

	m_bCheckConnectLine = FALSE;
	
	//Point 정보는 읽지 않은(모듈에서 받을 값을 사용)
	m_calibrtaionData.m_PointData.LoadPointData(FALSE);
	//Send PC->SBC EP_GET_POINT_INFO
	//Receive SBC->PC EP_CMD_CAL_POINT_DATA	

	//1. Calibration Data loading form loacal folder
	CString str;
	str.Format("%s\\Calibration\\%s.cbd", m_pdoc->m_strCurFolder, GetModuleName(m_nUnitNo));
	m_calibrtaionData.LoadDataFromFile(str);
	str.Format("%s [%s]", m_calibrtaionData.GetUpdateTime(), m_calibrtaionData.GetUserName());
	GetDlgItem(IDC_STATIC_DATE)->SetWindowText(str);

	//2. sbc에서 교정 Point를 받아 온다. (접속이되지 않을 경우 과거 교정 Point Data 사용)
	CalPointInit();

	DisplayChNoColumn();
	ReDrawGrid(m_nDataMode, m_nDataType);
	DisplayData(m_nDataMode, m_nDataType);

// 	m_nChkDA_V = AfxGetApp()->GetProfileInt("Calibration", "CHK DVM V", 0);
// 	m_nChkDA_I = AfxGetApp()->GetProfileInt("Calibration", "CHK DVM I", 0);
// 	m_nChkDVM_V = AfxGetApp()->GetProfileInt("Calibration", "CHK DA V", 0);
// 	m_nChkDVM_I = AfxGetApp()->GetProfileInt("Calibration", "CHK DA I", 0);

	GetDlgItem(IDC_CALI_UNIT_STATIC)->SetWindowText(GetModuleName(m_nUnitNo));
	str.Format("%d Set" ,m_nTotBoardNo);
	GetDlgItem(IDC_TOT_BDNO)->SetWindowText(str);
	str.Format("%d CH" ,m_nTotCh);
	GetDlgItem(IDC_TOT_BDNO2)->SetWindowText(str);
	
	m_wndCalState.SetBkColor(RGB(255,255,255));
	m_wndCalState.SetTextColor(RGB(255,0,0));

	CFormModule *pModule = m_pdoc->GetModuleInfo(m_nUnitNo);
	if(pModule != NULL)
	{
		WORD state = pModule->GetState();
		if( state == EP_STATE_STANDBY || state == EP_STATE_IDLE || state == EP_STATE_MAINTENANCE || state == EP_STATE_READY )
		{
			SetCalState(CAL_STATE_IDLE);
		}
		else
		{
			SetCalState(CAL_STATE_LINEOFF);
		}
	}
	else
	{	
		SetCalState(CAL_STATE_LINEOFF);
	}

	for( int i=0; i<256; i++ )
	{
		m_CaliModuleState[m_nUnitNo] = EP_STATE_LINE_OFF;
	}

	SetTimer(150, 1000, NULL);

	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCalibratorDlg::OnCaliStartBtn() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

	CFormModule *pModule = m_pdoc->GetModuleInfo(m_nUnitNo);
	if(pModule == NULL)	return;

	if(m_nState != CAL_STATE_IDLE)
	{
		return;
	}

	WORD state = pModule->GetState();

	if(state != EP_STATE_IDLE && state != EP_STATE_STANDBY && state != EP_STATE_MAINTENANCE && state != EP_STATE_READY)
	{
		CString strTemp;
		strTemp.Format(TEXT_LANG[1], GetModuleName(m_nUnitNo));//"%s는 교정 가능한 상태가 아닙니다."
		AfxMessageBox(strTemp, MB_OK|MB_ICONSTOP);
		return; 
	}

// 	AfxGetApp()->WriteProfileInt("Calibration", "CHK DVM V", m_nChkDA_V);
// 	AfxGetApp()->WriteProfileInt("Calibration", "CHK DVM I", m_nChkDA_I);
// 	AfxGetApp()->WriteProfileInt("Calibration", "CHK DA V", m_nChkDVM_V);
// 	AfxGetApp()->WriteProfileInt("Calibration", "CHK DA I", m_nChkDVM_I);

	CCaliStartDlg dlg( m_nUnitNo, m_calibrtaionData.m_PointData.GetVRangeCnt(), m_calibrtaionData.m_PointData.GetIRangeCnt(), m_nOptTrayType, this );	

	int nRtn = 0;
	EP_CAL_SELECT_INFO selInfo;
	ZeroMemory(&selInfo, sizeof(EP_CAL_SELECT_INFO));
	dlg.n_cali = m_nCalType;
	dlg.n_vi = m_nDataType;
	dlg.n_board = m_nBoardNo;
	dlg.m_bMultiMode = m_bMultiMode;

	if(dlg.DoModal() == IDOK)
	{	
		m_calibrtaionData.SetUserName(dlg.m_strUserName);
	
		m_nCalType = dlg.n_cali;			//0: Cal & check, 1: Cal, 2: Check			
		
		//Main/CH
		selInfo.nMain = dlg.n_main;			//main DA or CH (0: Main, 1:Channel)
		
		//V/I Type
		selInfo.nType = dlg.n_vi;			//전압/전류(0: Voltage, 1: Current)

		selInfo.nRange = dlg.n_range;		//교정 Range(1~N, 0:ALL Range)
		// m_nRange = dlg.n_range;				

		selInfo.nBoardNo = dlg.n_board;		//Board No (1~N)
		m_nBoardNo = dlg.n_board;

		int chCnt = GetHWChCnt(selInfo.nBoardNo);
		memset(selInfo.byCnSel, 1, chCnt);	//default 모든 CH

		selInfo.bMutiMode = dlg.m_bMultiMode;	//Multi Mode
		m_bMultiMode = dlg.m_bMultiMode;

		selInfo.bTrayType = m_nOptTrayType;		//ljb 2011519 TRAY TYPE

		 if(m_nCalType == CAL_TYPE_CHECK)	//확인 작업 
		{
			if((nRtn = EPSendCommand(m_nUnitNo, 0, 0, EP_CMD_CHK_START, &selInfo, sizeof(EP_CAL_SELECT_INFO)))!= EP_ACK)
			{
				AfxMessageBox(TEXT_LANG[2]);		//"교정 확인 명령 전송을 실패 하였습니다."
				return;
			}
		}
		else
		{
			if((nRtn = EPSendCommand(m_nUnitNo, 0, 0, EP_CMD_CAL_START, &selInfo, sizeof(EP_CAL_SELECT_INFO)))!= EP_ACK)
			{
				AfxMessageBox(TEXT_LANG[3]);//"교정 명령 전송을 실패 하였습니다."
				return;
			}			
		}
		m_nTimeOutCnt = 0;
		SetCalState(CAL_STATE_RUN);
		GetDlgItem(IDC_STATIC_PROGRESS_TITLE)->SetWindowText(TEXT_LANG[4]);//"교정을 시작합니다."

		//화면 표시를 자동으로 전환 
		if( m_nCalType == CAL_TYPE_CHECK )		{	m_nDataMode = CAL_DATA_MODE_AFTER;	}
		else									{	m_nDataMode = CAL_DATA_MODE_BEFORE;	}
		m_nDataType = selInfo.nType;

		ReDrawGrid(m_nDataMode, m_nDataType);
		DisplayData(m_nDataMode, m_nDataType);
		UpdateData(FALSE);
	}
}

void CCalibratorDlg::OnStopBtn() 
{
	// TODO: Add your control notification handler code here
	if(m_nState == CAL_STATE_RUN)
	{
		int nRtn;
		if(IDYES==MessageBox(TEXT_LANG[5], TEXT_LANG[6], MB_YESNO|MB_ICONQUESTION))//"교정/확인 작업을 중단하시겠습니까?"//"전송 확인"
		{	
			if((nRtn = EPSendCommand(m_nUnitNo, 0, 0, EP_CMD_CAL_STOP, NULL,0))!= EP_ACK)
			{
				AfxMessageBox(TEXT_LANG[7]);	//"작업 중단 전송이 실패하였습니다."
			}
			else
			{
				SetCalState(CAL_STATE_IDLE);
				AfxMessageBox(TEXT_LANG[8]);//"작업을 중단 하였습니다."
			}
		}
	}
}

void CCalibratorDlg::OnPauseBtn() 
{
	// TODO: Add your control notification handler code here
	int nRtn;

	if(IDYES==MessageBox(TEXT_LANG[9], TEXT_LANG[6], MB_YESNO|MB_ICONQUESTION))//"교정/확인 작업을 일시정지 하시겠습니까?"//"전송 확인"
	{
		if((nRtn = EPSendCommand(m_nUnitNo, 0, 0, EP_CMD_CAL_PAUSE, NULL,0))!= EP_ACK)
		{
			AfxMessageBox(TEXT_LANG[10]);//"작업 일시정지 전송이 실패하였습니다."
		}
		else
		{
			SetCalState(CAL_STATE_PAUSE);
			AfxMessageBox(TEXT_LANG[11]);//"작업을 일시정지 하였습니다."
		}
		
	}
}

void CCalibratorDlg::OnResumeBtn() 
{
	// TODO: Add your control notification handler code here
	if(IDYES==MessageBox(TEXT_LANG[12], TEXT_LANG[6], MB_YESNO|MB_ICONQUESTION))//"교정/확인 작업을 [계속 실행] 하시겠습니까?"//"전송 확인"
	{
		int nRtn = 0;
		if((nRtn = EPSendCommand(m_nUnitNo, 0, 0, EP_CMD_CAL_PAUSE, NULL,0))!= EP_ACK)
		{
			AfxMessageBox(TEXT_LANG[13]);//"작업 [계속실행] 전송이 실패하였습니다."
		}
		else
		{
			SetCalState(CAL_STATE_RUN);
		}
	}	
}

void CCalibratorDlg::OnCaliDataUpdate() 
{
	CFormModule *pModule = m_pdoc->GetModuleInfo(m_nUnitNo);
	if(pModule == NULL)	return;

	WORD state = pModule->GetState();
	if(state != EP_STATE_IDLE && state != EP_STATE_STANDBY && state != EP_STATE_MAINTENANCE && state != EP_STATE_READY)
	{
		CString strTemp;
		strTemp.Format(TEXT_LANG[14], GetModuleName(m_nUnitNo));//"%s는 교정 Data를 적용 가능한 상태가 아닙니다."
		AfxMessageBox(strTemp, MB_OK|MB_ICONSTOP);
		return; 
	}
	
	if(CheckOverSpec())
	{
		//교정 에러 범위 초과 Ch에 대한 경고
		if(IDNO == MessageBox(TEXT_LANG[15], TEXT_LANG[6], MB_YESNO|MB_ICONWARNING|MB_DEFBUTTON2))//"교정 결과 오차 범위를 초과하는 채널이 존재합니다.\n\n무시하고 진행 하시겠습니까?"//"전송 확인"
		{
			return;
		}
	}

	// TODO: Add your control notification handler code here
	if(IDYES == MessageBox(TEXT_LANG[16], TEXT_LANG[6], MB_YESNO|MB_ICONWARNING|MB_DEFBUTTON2))//"현재 교정된 Data를 적용 하시겠습니까?\n\n주의:적용 이후 이전 교정 Data는 삭제되어 복구 할 수 없습니다."//"전송 확인"
	{
		int nRtn = 0;
		EP_CAL_SELECT_INFO selInfo;
		ZeroMemory(&selInfo, sizeof(EP_CAL_SELECT_INFO));
		
		//전압 Udpate
		selInfo.nMain = 1;		//ch
		selInfo.nType = 0;		//voltage
		selInfo.nRange = 0;		//All Update
		selInfo.nBoardNo = 0;	//All Update
		selInfo.bTrayType = m_nOptTrayType;		//ljb 2011523 TRAY TYPE
		memset(selInfo.byCnSel, 1, sizeof(selInfo.byCnSel));

		if((nRtn = EPSendCommand(m_nUnitNo, 0, 0, EP_CMD_CAL_UPDATE, &selInfo, sizeof(EP_CAL_SELECT_INFO)))!= EP_ACK)
		{
			AfxMessageBox(TEXT_LANG[17]);//"교정 Data를 적용에 실패 하였습니다."
			return;
		}

/*		//전류 Update
		ZeroMemory(&selInfo, sizeof(EP_CAL_SELECT_INFO));
		selInfo.nMain = 1;		//ch
		selInfo.nType = CAL_DATA_TYPE_CRT;		//current
		selInfo.nRange = 0;		//All Update
		selInfo.nBoardNo = 0;	//All Update	
		memset(selInfo.byCnSel, 1, sizeof(selInfo.byCnSel));
		if((nRtn = EPSendCommand(m_nUnitNo, 0, 0, EP_CMD_CAL_UPDATE, &selInfo, sizeof(EP_CAL_SELECT_INFO)))!= EP_ACK)
		{
			AfxMessageBox("전류교정 Data를 적용에 실패 하였습니다.");
			return;
		}
*/
		SaveUpdateResultData();	
		
		CString str;
		str.Format("%s\\Calibration\\%s.cbd", m_pdoc->m_strCurFolder, GetModuleName(m_nUnitNo));
		m_calibrtaionData.SaveDataToFile(str);
		str.Format("%s [%s]", m_calibrtaionData.GetUpdateTime(), m_calibrtaionData.GetUserName());
		GetDlgItem(IDC_STATIC_DATE)->SetWindowText(str);
	}
}

/*
void CCalibratorDlg::DisplayData( int nDataMode, int nDataType )
{
	CString strTemp;
	CString str;
	double dwADData, dwMeterData;
	for(int row=0; row<m_wndCalData.GetRowCount(); row++)
	{
		int nCh = atoi(m_wndCalData.GetValueRowCol(row+1, _MON_CH_COL_));
		if(nCh > 0 && nCh <= EP_MAX_CH_PER_MD)
		{
			int nBoardNo, nChNo;

			m_calibrtaionData.m_ChCaliData[nCh-1].GetHWChNo(nBoardNo, nChNo);

			if(nBoardNo == 0 || nChNo == 0)	
				continue;

			BOOL bLock = m_wndCalData.LockUpdate();
			strTemp.Format("%02d-%02d", nBoardNo, nChNo);
			m_wndCalData.SetValueRange(CGXRange(row+1, _HW_CH_COL), strTemp);
			
			if( nDataMode == 0)		//교정전
			{
				if( nDataType == 0)		//전압
				{
					int nCnt = _FROZEN_COL_CNT_+1;		//Column 1 은 HW CH-BD
					for(int r = 0; r<m_calibrtaionData.m_PointData.GetVRangeCnt(); r++)
					{
						for(int p=0; p<m_calibrtaionData.m_PointData.GetVtgSetPointCount(r); p++)
						{
							double dwSetData = m_calibrtaionData.m_PointData.GetVSetPoint(r,p);
							str.Format("%s", m_pdoc->ValueString(dwSetData, EP_VOLTAGE));
							m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);
							m_calibrtaionData.m_ChCaliData[nCh-1].GetVCalData(p, dwADData, dwMeterData);
							str.Format("%s", m_pdoc->ValueString(dwADData, EP_VOLTAGE));
							m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);
							str.Format("%s", m_pdoc->ValueString(dwMeterData, EP_VOLTAGE));
							m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);
							str.Format("%s", m_pdoc->ValueString(dwADData-dwMeterData, EP_VOLTAGE));
							m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);
							str.Format("%s", m_pdoc->ValueString(dwMeterData-dwSetData, EP_VOLTAGE));
							m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);
						}
					}
				}
				else	//전류
				{
					int nCnt = _FROZEN_COL_CNT_+1;		//Column 1 은 HW CH-BD
					for(int r = 0; r<m_calibrtaionData.m_PointData.GetIRangeCnt(); r++)
					{
						for(int p=0; p<m_calibrtaionData.m_PointData.GetCrtSetPointCount(r); p++)
						{
							double dwSetData = m_calibrtaionData.m_PointData.GetISetPoint(r,p);
							str.Format("%s", m_pdoc->ValueString(dwSetData, EP_CURRENT));
							m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);
							m_calibrtaionData.m_ChCaliData[nCh-1].GetICalData(p, dwADData, dwMeterData);
							str.Format("%s", m_pdoc->ValueString(dwADData, EP_CURRENT));
							m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);
							str.Format("%s", m_pdoc->ValueString(dwMeterData, EP_CURRENT));
							m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);
							str.Format("%s", m_pdoc->ValueString(dwADData-dwMeterData, EP_CURRENT));
							m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);
							str.Format("%s", m_pdoc->ValueString(dwMeterData-dwSetData, EP_CURRENT));
							m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);
						}
					}
				}
			}
			else
			{
				if( nDataType == 0)		//전압
				{
					int nCnt = _FROZEN_COL_CNT_+1;		//Column 1 은 HW CH-BD
					for(int r = 0; r<m_calibrtaionData.m_PointData.GetVRangeCnt(); r++)
					{
						for(int p=0; p<m_calibrtaionData.m_PointData.GetVtgCheckPointCount(r); p++)
						{
							double dwSetData = m_calibrtaionData.m_PointData.GetVCheckPoint(r,p);
							str.Format("%s", m_pdoc->ValueString(dwSetData, EP_VOLTAGE));
							m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);
							m_calibrtaionData.m_ChCaliData[nCh-1].GetVCheckData(p, dwADData, dwMeterData);
							str.Format("%s", m_pdoc->ValueString(dwADData, EP_VOLTAGE));
							m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);
							str.Format("%s", m_pdoc->ValueString(dwMeterData, EP_VOLTAGE));
							m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);
							str.Format("%s", m_pdoc->ValueString(dwADData-dwMeterData, EP_VOLTAGE));
							m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);
							str.Format("%s", m_pdoc->ValueString(dwMeterData-dwSetData, EP_VOLTAGE));
						}
					}
				}
				else	//전류
				{
					for(int r = 0; r<m_calibrtaionData.m_PointData.GetIRangeCnt(); r++)
					{
					int nCnt = _FROZEN_COL_CNT_+1;		//Column 1 은 HW CH-BD
						for(int p=0; p<m_calibrtaionData.m_PointData.GetCrtCheckPointCount(r); p++)
						{
							double dwSetData = m_calibrtaionData.m_PointData.GetICheckPoint(r,p);
							str.Format("%s", m_pdoc->ValueString(dwSetData, EP_CURRENT));
							m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);
							m_calibrtaionData.m_ChCaliData[nCh-1].GetICheckData(p, dwADData, dwMeterData);
							str.Format("%s", m_pdoc->ValueString(dwADData, EP_CURRENT));
							m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);
							str.Format("%s", m_pdoc->ValueString(dwMeterData, EP_CURRENT));
							m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);
							str.Format("%s", m_pdoc->ValueString(dwADData-dwMeterData, EP_CURRENT));
							m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);
							str.Format("%s", m_pdoc->ValueString(dwMeterData-dwSetData, EP_CURRENT));
							m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);
						}
					}
				}
			}			
			m_wndCalData.LockUpdate(bLock);
			m_wndCalData.Redraw();
		}		
	}
}
*/
void CCalibratorDlg::SaveUpdateResultData(CWordArray *pawSelMonCh)
{
	UpdateData(TRUE);
	COleDateTime datetime = COleDateTime::GetCurrentTime();
	char tempName[512];
	CString strFileName;
	CString strCurFolder;
	CString	strResultData = __T("");

	strFileName.Format("%s-%s", GetModuleName(m_nUnitNo), datetime.Format("%m%d%H%M%S"));
	strCurFolder = m_pdoc->m_strCurFolder;
	strFileName += ".csv";
	sprintf(tempName, "%s\\Calibration\\%s", strCurFolder, strFileName);

	m_pdoc->WriteLog("===========> CALIBRATION UPDATE DATA FILE WRITE STRART! <===========");

	FILE *fp;
	fp = fopen(tempName, "at+");
	if(fp == NULL)	
		return;

	int nMode;
	int nType;
	int row, col;

	// [8/29/2009 경여니]
	// 교정전 전압, 전류 확인 => 교정후 전압, 전류 확인
	for( nMode=0; nMode<2; nMode++ )	// 교정전nMode:0, 교정후nMode:1
	{
		for( nType=0; nType<2; nType++ )// 전압nType:0, 전류nType:1
		{
			if( nMode == 0 && nType == 0 )
			{
				fprintf(fp, TEXT_LANG[19]);//"교정전 전압 결과 데이터\n"
				
				ReDrawGrid( nMode, nType );
				DisplayData( nMode, nType );

				for( row=0; row<=m_wndCalData.GetRowCount(); row++ )
				{
					for( col=1; col<=m_wndCalData.GetColCount(); col++ )
					{
						strResultData += m_wndCalData.GetValueRowCol( row, col );
						if( col != m_wndCalData.GetColCount() )
						{
							strResultData += ",";
						}						
					}
					strResultData += "\n";
					fprintf(fp, (LPSTR)(LPCSTR)strResultData);
					strResultData = __T("");
				}
			}
			else if( nMode == 0 && nType == 1)
			{
				fprintf(fp, TEXT_LANG[20]);//"교정전 전류 결과 데이터\n"

				ReDrawGrid( nMode, nType );
				DisplayData( nMode, nType );

				for( row=0; row<=m_wndCalData.GetRowCount(); row++ )
				{
					for( col=1; col<=m_wndCalData.GetColCount(); col++ )
					{
						strResultData += m_wndCalData.GetValueRowCol( row, col );
						if( col != m_wndCalData.GetColCount() )
						{
							strResultData += ",";
						}						
					}
					strResultData += "\n";
					fprintf(fp, (LPSTR)(LPCSTR)strResultData);
					strResultData = __T("");
				}
			}
			else if( nMode == 1 && nType == 0 )
			{
				fprintf(fp, TEXT_LANG[21]);//"교정후 전압 결과 데이터\n"

				ReDrawGrid( nMode, nType );
				DisplayData( nMode, nType );

				for( row=0; row<=m_wndCalData.GetRowCount(); row++ )
				{
					for( col=1; col<=m_wndCalData.GetColCount(); col++ )
					{
						strResultData += m_wndCalData.GetValueRowCol( row, col );
						if( col != m_wndCalData.GetColCount() )
						{
							strResultData += ",";
						}						
					}
					strResultData += "\n";
					fprintf(fp, (LPSTR)(LPCSTR)strResultData);
					strResultData = __T("");
				}
			}
			else if( nMode == 1 && nType == 1 )
			{
				fprintf(fp, TEXT_LANG[22]);//"교정후 전류 결과 데이터\n"

				ReDrawGrid( nMode, nType );
				DisplayData( nMode, nType );

				for( row=0; row<=m_wndCalData.GetRowCount(); row++ )
				{
					for( col=1; col<=m_wndCalData.GetColCount(); col++ )
					{
						strResultData += m_wndCalData.GetValueRowCol( row, col );
						if( col != m_wndCalData.GetColCount() )
						{
							strResultData += ",";
						}						
					}
					strResultData += "\n";
					fprintf(fp, (LPSTR)(LPCSTR)strResultData);
					strResultData = __T("");
				}
			}
		}
	}	
	fclose(fp);
	m_pdoc->WriteLog("===========> CALIBRATION UPDATE DATA FILE WRITE DONE! <===========");	
	
	// [8/29/2009 경여니]
	// 데이타 쓰기후 그리드를 원래 보기 설정으로
	ReDrawGrid( m_nDataMode, m_nDataType );
	DisplayData( m_nDataMode, m_nDataType );
	AfxMessageBox(TEXT_LANG[23]);//"교정 데이터 적용을 완료하였습니다."
}

// [8/29/2009 경여니]
// 명확한 기능확인 후 구현
void CCalibratorDlg::OnCaliDataSearch() 
{
	// TODO: Add your control notification handler code here
	CString strTempPath;
	strTempPath.Format("%s\\Calibration\\", m_pdoc->m_strCurFolder);
	
	CFileDialog dlg(TRUE, "csv", NULL, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT|OFN_ALLOWMULTISELECT,TEXT_LANG[24]);//"Profile Data(*.csv)|*.csv|모든 파일 (*.*)|*.*|"
	dlg.m_ofn.lpstrTitle = TEXT_LANG[25];//"캘리브레이션 결과 파일 불러오기"
	dlg.m_ofn.lpstrInitialDir = strTempPath;

	if( dlg.DoModal() == IDOK )
	{
		/*
		m_strProFileName = dlg.GetPathName();
		pDoc->m_ListValuesArray.RemoveAll();		// 배열 초기화
		
		CStdioFile sourceFile;			
		CFileException ex;

		CString saveFilePath;
		CString strReadData;			
		CString strIndex;
		int nSize = -1;

		saveFilePath = dlg.GetPathName();
		
		if( !sourceFile.Open( saveFilePath, CFile::modeRead, &ex ))
		{
			CString strMsg;
			ex.GetErrorMessage((LPSTR)(LPCSTR)strMsg,1024);
			AfxMessageBox(strMsg);
			return;
		}
		
		while(TRUE)	
		{			
			sourceFile.ReadString(strReadData);    // 한 문자열씩 읽는다.	
			if(strReadData.IsEmpty())
			{
				break;
			}			
			pDoc->m_ListValuesArray.Add(strReadData);
		}
		sourceFile.Close();	

		m_ProfileList.DeleteAllItems();
		int nIndex = pDoc->m_ListValuesArray.GetSize();

		CString strTemp1,strTemp2,strTemp3,strTemp4,strTemp5;
		CString strRunnningTime;

		for( int i=0; i<nIndex; i++ )
		{
			strReadData = pDoc->m_ListValuesArray.GetAt(i);

			AfxExtractSubString(strTemp1, strReadData, 0, ',');			
			AfxExtractSubString(strTemp2, strReadData, 1, ',');			
			AfxExtractSubString(strTemp3, strReadData, 2, ',');			
			AfxExtractSubString(strTemp4, strReadData, 3, ',');			
			AfxExtractSubString(strTemp5, strReadData, 4, ',');

			strRunnningTime.Format("%02d:%02d:%02d.%03d", atoi(strTemp5), atoi(strTemp4), atoi(strTemp3), atoi(strTemp2));
		
			strTemp2.Format("%d",i+1);
			m_ProfileList.InsertItem(i,strTemp2, 1);
			m_ProfileList.SetItemText(i, 1,  strRunnningTime);
			m_ProfileList.SetItemText(i, 2,  strTemp1);
			
 			AfxExtractSubString(strTemp3, strReadData, 44, ',');		// 반복횟수
 			AfxExtractSubString(strTemp4, strReadData, 45, ',');		// 종료유지
		}
		if (strTemp3.IsEmpty()) strTemp3 = "1";
		m_iSpinNum = atoi(strTemp3);
		m_bCleanDataAfterSchedule = atoi(strTemp4);
		GetTotalTime();
		GetDlgItem(IDC_BUTTON42)->EnableWindow(FALSE);
		UpdateData(FALSE);
		*/
	}		
	MessageBox("Cali Data Search..");	
}

//==================================================================================
void CCalibratorDlg::CalPointInit()
{	
	LPVOID	pBuffer;
	CString strTemp;

	CFormModule *pModule = m_pdoc->GetModuleInfo(m_nUnitNo);
	if(pModule == NULL)	return;
	WORD state = pModule->GetState();
	if(state == EP_STATE_LINE_OFF)
	{
		return ;
	}

	CString strErrorMsg;
	double shuntData[CAL_MAX_BOARD_NUM];

	//1. H/W Mapping data request
	EP_HW_MAPPIING_DATA hwMappingData;
	int nSize = sizeof(EP_HW_MAPPIING_DATA);

	//pBuffer = EPSendDataCmd(m_nUnitNo, 0, 0, EP_CMD_MAPPING_INFO, nSize);
	//Sleep(200);
	if((pBuffer = EPSendDataCmd(m_nUnitNo, 0, 0, EP_CMD_MAPPING_INFO, nSize)) != NULL)
	{
		EP_MD_SYSTEM_DATA *pSysData = ::EPGetModuleSysData(EPGetModuleIndex(m_nUnitNo));
		memcpy(&hwMappingData, pBuffer, sizeof(EP_HW_MAPPIING_DATA));
		for(int i=0; i<m_nTotCh; i++)
		{
			if(hwMappingData.mapping[i].wMonCh > 0 && hwMappingData.mapping[i].wMonCh<= EP_MAX_CH_PER_MD)
			{
				m_calibrtaionData.m_ChCaliData[hwMappingData.mapping[i].wMonCh-1].SetHWChNo(hwMappingData.mapping[i].wBoardNo, hwMappingData.mapping[i].wHwCh);
			}
			//board no error (mapping information error)
			if(m_nTotBoardNo < hwMappingData.mapping[i].wBoardNo)
			{
				strTemp.Format("[BD %d] ", hwMappingData.mapping[i].wBoardNo);
				strErrorMsg += strTemp;
			}
			else
			{
 				//Channel count per board error
				if(pSysData && pSysData->wChannelPerBoard < hwMappingData.mapping[i].wHwCh)
				{
					strTemp.Format("[CH %d] ", hwMappingData.mapping[i].wHwCh);
					strErrorMsg += strTemp;
				}
			}

		}
		if(strErrorMsg.IsEmpty() == FALSE)
		{
			strErrorMsg += TEXT_LANG[26];//"는 등록된 Board가 아닙니다.(Mapping 정보를 확인 하세요.)"
			AfxMessageBox(strErrorMsg);
			return;
		}
	}
	else
	{
		AfxMessageBox("H/W Mapping data request file!!");
		return;
	}
	
	//2. calibration point data request
	EP_CAL_POINT_SET GetCalPointData;
	nSize = sizeof(EP_CAL_POINT_SET);
	//Main 교정 Point 요청 구현 필요
	//Channel 교정 Point 요청 
	if((pBuffer =  EPSendDataCmd(m_nUnitNo, 0, 0, EP_CMD_GET_CH_CAL_POINT, nSize)) != NULL)
	{
		WORD i = 0;
		int j = 0;

		memcpy(&GetCalPointData, pBuffer, sizeof(EP_CAL_POINT_SET));
	
		CCaliPoint calPointData;	
		//PC의 설정값을 사용할지 SBC의 설정값을 사용할지 여부???

		//SBC Point data를 이용한다. 
		m_calibrtaionData.m_PointData.SetVRangeCnt(GetCalPointData.byValidVRangeNum);
		m_calibrtaionData.m_PointData.SetIRangeCnt(GetCalPointData.byValidIRangeNum);

		//cal V Point data
		for(i = 0 ; i < GetCalPointData.byValidVRangeNum; i++)
		{
			double dwPointData[CAL_MAX_CALIB_SET_POINT];
			memset(dwPointData, 0, sizeof(dwPointData));
			for(j=0; j<GetCalPointData.vCalPoinSet[i].byValidPointNum; j++)
			{
				dwPointData[j] = GetCalPointData.vCalPoinSet[i].lCaliPoint[j]/1000.0f;		//
			}
			m_calibrtaionData.m_PointData.SetVSetPointData(GetCalPointData.vCalPoinSet[i].byValidPointNum, dwPointData, i);
		}
		//cal I Point data
		for(i = 0 ; i < GetCalPointData.byValidIRangeNum; i++)
		{
			double dwPointData[CAL_MAX_CALIB_SET_POINT];
			memset(dwPointData, 0, sizeof(dwPointData));
			for(j=0; j<GetCalPointData.iCalPoinSet[i].byValidPointNum; j++)
			{
				dwPointData[j] = GetCalPointData.iCalPoinSet[i].lCaliPoint[j]/1000.0f;		//
			}
			m_calibrtaionData.m_PointData.SetISetPointData(GetCalPointData.iCalPoinSet[i].byValidPointNum, dwPointData, i);
		}

		//Check V point
		for( i = 0 ; i < GetCalPointData.byValidVRangeNum; i++)
		{
			double dwPointData[CAL_MAX_CALIB_SET_POINT];
			memset(dwPointData, 0, sizeof(dwPointData));
			for(j=0; j<GetCalPointData.vCalPoinSet[i].byCheckPointNum; j++)
			{
				dwPointData[j] = GetCalPointData.vCalPoinSet[i].lCheckPoint[j]/1000.0f;		//
			}
			m_calibrtaionData.m_PointData.SetVCheckPointData(GetCalPointData.vCalPoinSet[i].byCheckPointNum, dwPointData, i);
		}

		//Check I point
		for( i = 0 ; i < GetCalPointData.byValidIRangeNum; i++)
		{
			double dwPointData[CAL_MAX_CALIB_SET_POINT];
			memset(dwPointData, 0, sizeof(dwPointData));
			for(j=0; j<GetCalPointData.iCalPoinSet[i].byCheckPointNum; j++)
			{
				dwPointData[j] = GetCalPointData.iCalPoinSet[i].lCheckPoint[j]/1000.0f;		//
			}
			m_calibrtaionData.m_PointData.SetICheckPointData(GetCalPointData.iCalPoinSet[i].byCheckPointNum, dwPointData, i);
		}

		//ShuntR 추가
		for(i =0; i<CAL_MAX_BOARD_NUM; i++)
		{
			shuntData[i] = GetCalPointData.lCaliShuntR[i]/10000.0f;
		}		

		m_calibrtaionData.m_PointData.SetShuntR( shuntData );

		//ShuntT 추가
		for(i =0; i<CAL_MAX_BOARD_NUM; i++)
		{
			shuntData[i] = GetCalPointData.lCaliShuntT[i]/10000.0f;
		}		
		m_calibrtaionData.m_PointData.SetShuntT( shuntData );

		//Shunt Serial 추가
		for(i =0; i<CAL_MAX_BOARD_NUM; i++)
		{
			m_calibrtaionData.m_PointData.SetShuntSerial(i, &GetCalPointData.byCaliShuntSerail[i][0] );				
		}

		GetDlgItem(IDC_STATIC_PROGRESS_TITLE)->SetWindowText(TEXT_LANG[27]);//"교정 Point 정보가 갱신되었습니다."
	}
	else
	{
		AfxMessageBox(TEXT_LANG[28]);//"교정 Point 정보 갱신 FAIL!!"
		return;
	}	
}
//=========================================================================================

//Cali Update ResultData
BOOL CCalibratorDlg::UpdateResultData(long nModuleID, WORD nMode, EP_CALIHCK_RESULT_DATA pRcalData)
{
	m_nTimeOutCnt = 0;

	if(nModuleID != m_nUnitNo)		
		return FALSE;

	CChCaliData *pCalData = m_calibrtaionData.GetChCalData(pRcalData.nMonCh-1);
	if(pCalData == NULL)		return FALSE;

	CString str_time;

	float fSetData = 0.0f;
	CCaliPoint *pCalPoint = m_calibrtaionData.GetCalPointData();

	if(nMode == 0)		//Cal
	{
		str_time =TEXT_LANG[29];//"교정"
		if(pRcalData.nMain == 0) //Main Da
		{
		}
		else
		{
			if(pRcalData.nMonCh > 0 && pRcalData.nMonCh <= EP_MAX_CH_PER_MD)
			{
				pCalData->SetHWChNo(pRcalData.nBoardNo, pRcalData.nHwCh);

				if(pRcalData.nType == CAL_DATA_TYPE_VTG)
				{
					pCalData->SetVCalData(pRcalData.nRange-1, pRcalData.nPtIndex, pRcalData.lAD/1000.0f, pRcalData.lDVM/1000.0f);
					fSetData = pCalPoint->GetVSetPoint(pRcalData.nRange-1, pRcalData.nPtIndex);
				}
				else if(pRcalData.nType == CAL_DATA_TYPE_CRT)
				{
					CString strTemp = _T("");
					
					pCalData->SetICalData(pRcalData.nRange-1, pRcalData.nPtIndex, pRcalData.lAD/1000.0f, pRcalData.lDVM/1000.0f);
					fSetData = pCalPoint->GetISetPoint(pRcalData.nRange-1, pRcalData.nPtIndex);
				}

				//교정 Data 가 수정 되었음 
				pCalData->SetState();
			}
		}
	}
	else // nMode == 1 Check
	{
		str_time =TEXT_LANG[30];//"확인"
		if(pRcalData.nMain == 0) //Main Da
		{
		}
		else
		{
			if(pRcalData.nMonCh > 0 && pRcalData.nMonCh <= EP_MAX_CH_PER_MD)
			{
				pCalData->SetHWChNo(pRcalData.nBoardNo, pRcalData.nHwCh);

				if(pRcalData.nType == CAL_DATA_TYPE_VTG)
				{
					pCalData->SetVCheckData(pRcalData.nRange-1, pRcalData.nPtIndex, pRcalData.lAD/1000.0f, pRcalData.lDVM/1000.0f);
					fSetData = pCalPoint->GetVCheckPoint(pRcalData.nRange-1, pRcalData.nPtIndex);
				}
				else if(pRcalData.nType == CAL_DATA_TYPE_CRT)
				{
					pCalData->SetICheckData(pRcalData.nRange-1, pRcalData.nPtIndex, pRcalData.lAD/1000.0f, pRcalData.lDVM/1000.0f);
					fSetData = pCalPoint->GetICheckPoint(pRcalData.nRange-1, pRcalData.nPtIndex);
				}

				//교정 Data 가 수정 되었음 
				pCalData->SetState();
			}
		}
	}

	CString str_Type;
	int nItem;
	if(pRcalData.nType == CAL_DATA_TYPE_VTG)
	{
		str_Type= TEXT_LANG[31];//"전압"
		nItem = EP_VOLTAGE;
	}
	else if(pRcalData.nType == CAL_DATA_TYPE_CRT)
	{
		str_Type = TEXT_LANG[32];//"전류"
		nItem = EP_CURRENT;
	}

	CString strTemp;
	strTemp.Format(TEXT_LANG[33], //"CH %d의 %s %s Data를 수신하였습니다.(RANGE %d, P%d: %s)"
					pRcalData.nMonCh, str_Type, str_time, pRcalData.nRange, pRcalData.nPtIndex+1, m_pdoc->ValueString(fSetData, nItem,TRUE));
	GetDlgItem(IDC_STATIC_PROGRESS_TITLE)->SetWindowText(strTemp);

//	ReDrawGrid(m_nDataMode, m_nDataType);
	// kky0703
	DisplayData( m_nDataMode, m_nDataType, pRcalData.nRange );
	return TRUE;
}

//OnTimer
//m_calibrtaionData의 data를 현재 설정에 맞춰서 Display
void CCalibratorDlg::DisplayData( int nDataMode, int nDataType, int nRange )
{
	CString strTemp;
	CString str;
	double dwADData, dwMeterData;
	CCaliPoint *pCalPoint = m_calibrtaionData.GetCalPointData();

	BOOL bLock = m_wndCalData.LockUpdate();
	double diffMeter = 0;
	double diffAd = 0;	 
	double diffFeedback = 0;

	for(int row=0; row<m_wndCalData.GetRowCount(); row++)
	{
		int nCh = atoi(m_wndCalData.GetValueRowCol(row+1, _MON_CH_COL_));		//monitoring ch
		CChCaliData *pChCalData = m_calibrtaionData.GetChCalData(nCh-1);
		if(pChCalData)
		{
			if(pChCalData->GetState() == CAL_DATA_EMPTY)	continue;

			int nBoardNo, nChNo;
			pChCalData->GetHWChNo(nBoardNo, nChNo);
			if(nBoardNo == 0 || nChNo == 0)	 				continue;

// 			strTemp.Format("%02d-%02d", nBoardNo, nChNo);	
// 			m_wndCalData.SetValueRange(CGXRange(row+1, _HW_CH_COL), strTemp);
			
			if(nDataMode == CAL_DATA_MODE_BEFORE)		//교정전
			{
				if(nDataType == CAL_DATA_TYPE_VTG)		//전압
				{
					int nCnt = _FROZEN_COL_CNT_+1;		//Column 1 은 HW CH-BD
					for(WORD r=0; r<pCalPoint->GetVRangeCnt(); r++)
					{
						for(WORD p=0; p<pCalPoint->GetVtgSetPointCount(r); p++)
						{
							double dwSetData = pCalPoint->GetVSetPoint(r,p);
							str.Format("%s", m_pdoc->ValueString(dwSetData, EP_VOLTAGE));
							m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);
						
							pChCalData->GetVCalData(p, dwADData, dwMeterData);
							
							str.Format("%s", m_pdoc->ValueString(dwADData, EP_VOLTAGE));
							m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);
							str.Format("%s", m_pdoc->ValueString(dwMeterData, EP_VOLTAGE));
							m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);
							
							diffAd = dwSetData - dwADData;
							str.Format("%s", m_pdoc->ValueString(diffAd, EP_VOLTAGE));
							if( fabs(diffAd) > pCalPoint->m_dVADAccuracy[r]) 
							{
								m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(150,255,255)));
							}
							else 
							{
								m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(255,255,255)));
							}							
							
							diffMeter = dwSetData - dwMeterData;
							str.Format("%s", m_pdoc->ValueString(diffMeter, EP_VOLTAGE));
							if( fabs(diffMeter) > pCalPoint->m_dVDAAccuracy[r] ) {
								m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(150,255,255)));
							}
							else {
								m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(255,255,255)));
							}

							diffFeedback = diffAd - diffMeter;
							str.Format("%s", m_pdoc->ValueString(diffFeedback, EP_VOLTAGE));
							if( fabs(diffFeedback) > pCalPoint->m_dVDAAccuracy[r] ) {
								m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(150,255,255)));
							}
							else {
								m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(255,255,255)));
							}
						}
					}
				}
				else	//전류
				{
					// 모든 전류 Range에 대해서 교정을 실시
					if( nRange == 0 )
					{
						int nCnt = _FROZEN_COL_CNT_+1;		//Column 1 은 HW CH-BD	
						TRACE(TEXT_LANG[34]);//"실행되면 안되는 구역\n"
						for( WORD r = 0; r<pCalPoint->GetIRangeCnt(); r++)
						{	
							for(WORD p=0; p<pCalPoint->GetCrtSetPointCount(r); p++)
							{							
								double dwSetData = pCalPoint->GetISetPoint(r,p);
								str.Format("%s", m_pdoc->ValueString(dwSetData, EP_CURRENT));
								m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);

								pChCalData->GetICalData(r, p, dwADData, dwMeterData);
															
								str.Format("%s", m_pdoc->ValueString(dwADData, EP_CURRENT));
								m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);
								str.Format("%s", m_pdoc->ValueString(dwMeterData, EP_CURRENT));
								m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);
								
								diffAd = dwSetData - dwADData;
								str.Format("%s", m_pdoc->ValueString(diffAd, EP_CURRENT));
								if( fabs(diffAd) > pCalPoint->m_dIADAccuracy[r] ) {
									m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(150,255,255)));
								}
								else {
									m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(255,255,255)));
								}
								
								diffMeter = dwSetData - dwMeterData;
								str.Format("%s", m_pdoc->ValueString(diffMeter, EP_CURRENT));
								if( fabs(diffMeter) > pCalPoint->m_dIDAAccuracy[r] ) {
									m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(150,255,255)));
								}
								else {
									m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(255,255,255)));
								}	
								
								diffFeedback = diffAd - diffMeter;
								str.Format("%s", m_pdoc->ValueString(diffFeedback, EP_CURRENT));
								if( fabs(diffFeedback) > pCalPoint->m_dIDAAccuracy[r] ) {
									m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(150,255,255)));
								}
								else {
									m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(255,255,255)));
								}
							}
						}
					}
					// 특정 Range 에 대해서 교정을 실시
					else
					{
						int nCnt = _FROZEN_COL_CNT_+1;		//Column 1 은 HW CH-BD	
						WORD r = 0;
						r = nRange - 1;
						for( WORD nTemp=0; nTemp<r; nTemp++ )
						{						
							nCnt = nCnt + (pCalPoint->GetCrtSetPointCount(nTemp)*6);
						}
						for(WORD p=0; p<pCalPoint->GetCrtSetPointCount(r); p++)
						{							
							double dwSetData = pCalPoint->GetISetPoint(r,p);
							str.Format("%s", m_pdoc->ValueString(dwSetData, EP_CURRENT));
							m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);

							pChCalData->GetICalData(r, p, dwADData, dwMeterData);

							CString strTemp;
							strTemp.Format(TEXT_LANG[35], //"CH %d의 Data를 수신하였습니다.( AD : %f, DVM : %f)\n"
								p, dwADData, dwMeterData);
							TRACE(strTemp);
							
							str.Format("%s", m_pdoc->ValueString(dwADData, EP_CURRENT));
							m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);
							str.Format("%s", m_pdoc->ValueString(dwMeterData, EP_CURRENT));
							m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);
							
							diffAd = dwSetData - dwADData;
							str.Format("%s", m_pdoc->ValueString(diffAd, EP_CURRENT));
							if( fabs(diffAd) > pCalPoint->m_dIADAccuracy[r] ) {
								m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(150,255,255)));
							}
							else {
								m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(255,255,255)));
							}
							
							diffMeter = dwSetData - dwMeterData;
							str.Format("%s", m_pdoc->ValueString(diffMeter, EP_CURRENT));
							if( fabs(diffMeter) > pCalPoint->m_dIDAAccuracy[r] ) {
								m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(150,255,255)));
							}
							else {
								m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(255,255,255)));
							}

							diffFeedback = diffAd - diffMeter;
							str.Format("%s", m_pdoc->ValueString(diffFeedback, EP_CURRENT));
							if( fabs(diffFeedback) > pCalPoint->m_dIDAAccuracy[r] ) {
								m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(150,255,255)));
							}
							else {
								m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(255,255,255)));
							}
						}	
					}					
				}
			}
			else if( nDataMode == CAL_DATA_MODE_AFTER )
			{
				if(nDataType == CAL_DATA_TYPE_VTG)		// 전압
				{
					int nCnt = _FROZEN_COL_CNT_+1;		//Column 1 은 HW CH-BD
					for(WORD r = 0; r<pCalPoint->GetVRangeCnt(); r++)
					{
						for(WORD p=0; p<pCalPoint->GetVtgCheckPointCount(r); p++)
						{
							double dwSetData = pCalPoint->GetVCheckPoint(r,p);
							str.Format("%s", m_pdoc->ValueString(dwSetData, EP_VOLTAGE));
							m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);
							pChCalData->GetVCheckData(p, dwADData, dwMeterData);

							str.Format("%s", m_pdoc->ValueString(dwADData, EP_VOLTAGE));
							m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);
							str.Format("%s", m_pdoc->ValueString(dwMeterData, EP_VOLTAGE));
							m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);
							
							diffAd = dwSetData - dwADData;
							str.Format("%s", m_pdoc->ValueString(diffAd, EP_VOLTAGE));
							if( fabs(diffAd) > pCalPoint->m_dVADAccuracy[r] ) {
								m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(150,255,255)));
							}
							else {
								m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(255,255,255)));
							}
							
							diffMeter = dwSetData - dwMeterData;
							str.Format("%s", m_pdoc->ValueString(diffMeter, EP_VOLTAGE));
							if( fabs(diffMeter) > pCalPoint->m_dVDAAccuracy[r] ) {
								m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(150,255,255)));
							}
							else {
								m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(255,255,255)));
							}
							
							diffFeedback = diffAd - diffMeter;
							str.Format("%s", m_pdoc->ValueString(diffFeedback, EP_VOLTAGE));
							if( fabs(diffFeedback) > pCalPoint->m_dVDAAccuracy[r] ) {
								m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(150,255,255)));
							}
							else {
								m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(255,255,255)));
							}
						}
					}
				}
				else		// 전류
				{	
					if( nRange == 0 )
					{
						int nCnt = _FROZEN_COL_CNT_+1;		//Column 1 은 HW CH-BD						
						for(WORD r = 0; r<pCalPoint->GetIRangeCnt(); r++)
						{						
							for(WORD p=0; p<pCalPoint->GetCrtCheckPointCount(r); p++)
							{								
								double dwSetData = pCalPoint->GetICheckPoint(r,p);
								str.Format("%s", m_pdoc->ValueString(dwSetData, EP_CURRENT));
								m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);

								pChCalData->GetICheckData(r, p, dwADData, dwMeterData );
								
								str.Format("%s", m_pdoc->ValueString(dwADData, EP_CURRENT));
								m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);
								str.Format("%s", m_pdoc->ValueString(dwMeterData, EP_CURRENT));
								m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);
								
								diffAd = dwSetData - dwADData;
								str.Format("%s", m_pdoc->ValueString(diffAd, EP_CURRENT));
								if( fabs(diffAd) > pCalPoint->m_dIADAccuracy[r] ) {
									m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(150,255,255)));
								}
								else {
									m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(255,255,255)));
								}
								
								diffMeter = dwSetData - dwMeterData;
								str.Format("%s", m_pdoc->ValueString(diffMeter, EP_CURRENT));
								if( fabs(diffMeter) > pCalPoint->m_dIDAAccuracy[r] ) {
									m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(150,255,255)));
								}
								else {
									m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(255,255,255)));
								}

								diffFeedback = diffAd - diffMeter;
								str.Format("%s", m_pdoc->ValueString(diffFeedback, EP_CURRENT));
								if( fabs(diffFeedback) > pCalPoint->m_dIDAAccuracy[r] ) {
									m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(150,255,255)));
								}
								else {
									m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(255,255,255)));
								}
							}
						}
					}
					else
					{	
						int nCnt = _FROZEN_COL_CNT_+1;		//Column 1 은 HW CH-BD	
						WORD r = 0;
						r = nRange - 1;
						for( WORD nTemp=0; nTemp<r; nTemp++ )
						{						
							nCnt = nCnt + (pCalPoint->GetCrtCheckPointCount(nTemp)*6);
						}

						for(WORD p=0; p<pCalPoint->GetCrtCheckPointCount(r); p++)
						{							
							double dwSetData = pCalPoint->GetICheckPoint(r,p);
							str.Format("%s", m_pdoc->ValueString(dwSetData, EP_CURRENT));
							m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);

							pChCalData->GetICheckData( r, p, dwADData, dwMeterData );	
							
							str.Format("%s", m_pdoc->ValueString(dwADData, EP_CURRENT));
							m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);
							str.Format("%s", m_pdoc->ValueString(dwMeterData, EP_CURRENT));
							m_wndCalData.SetValueRange(CGXRange(row+1, nCnt++), str);
							
							diffAd = dwSetData - dwADData;
							str.Format("%s", m_pdoc->ValueString(diffAd, EP_CURRENT));
							if( fabs(diffAd) > pCalPoint->m_dIADAccuracy[r] ) {
								m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(150,255,255)));
							}
							else {
								m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(255,255,255)));
							}
							
							diffMeter = dwSetData - dwMeterData;
							str.Format("%s", m_pdoc->ValueString(diffMeter, EP_CURRENT));
							if( fabs(diffMeter) > pCalPoint->m_dIDAAccuracy[r] ) {
								m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(150,255,255)));
							}
							else {
								m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(255,255,255)));
							}

							diffFeedback = diffAd - diffMeter;
							str.Format("%s", m_pdoc->ValueString(diffFeedback, EP_CURRENT));
							if( fabs(diffFeedback) > pCalPoint->m_dIDAAccuracy[r] ) {
								m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(150,255,255)));
							}
							else {
								m_wndCalData.SetStyleRange(CGXRange(row+1, nCnt++), CGXStyle().SetValue(str).SetInterior(RGB(255,255,255)));
							}
						}
					}				
				}
			}			
		}		
	}

	//Min, Max, Average, STDDEV 표시
	for(int col = _FROZEN_COL_CNT_+1; col<=m_wndCalData.GetColCount(); col++)
	{
		CCalAvg avg;
		for(int row=_FROZEN_ROW_CNT_+1; row<=m_wndCalData.GetRowCount(); row++)
		{
			strTemp = m_wndCalData.GetValueRowCol(row, col);
			if(strTemp.IsEmpty() == FALSE)
			{
				avg.AddData(atoi(m_wndCalData.GetValueRowCol(row, _MON_CH_COL_)), atof(strTemp));
			}
		}
		int nItem;
		if(nDataType == CAL_DATA_TYPE_VTG)
		{
			nItem = EP_VOLTAGE;
		}
		else
		{
			nItem = EP_CURRENT;
		}

		if(avg.GetDataCount() > 0)
		{
			if((col-_FROZEN_COL_CNT_-1) % ONE_POINT_COL_COUNT == 0)	//설정값 표시 Column
			{
				//min
				strTemp.Format("%s", m_pdoc->ValueString(avg.GetMinValue(), nItem));
				m_wndCalData.SetValueRange(CGXRange().SetCells(1, col), strTemp);
				//max
				strTemp.Format("%s", m_pdoc->ValueString(avg.GetMaxValue(), nItem));
				m_wndCalData.SetValueRange(CGXRange().SetCells(2, col), strTemp);
			}
// 			else if((col-_FROZEN_COL_CNT_+2) % ONE_POINT_COL_COUNT == 0)		//오차 표시 Column
// 			{
// 				//min
// 				strTemp.Format("%s(C%d)", m_pdoc->ValueString(avg.GetMinValue(), nItem), avg.GetMinIndex());
// 				if( fabs(avg.GetMinValue()) > pCalPoint->m_dVADAccuracy[r] ) 
// 				{
// 					m_wndCalData.SetStyleRange(CGXRange().SetCells(1, col), CGXStyle().SetValue(strTemp).SetInterior(RGB(150,255,255)));
// 				}
// 				else 
// 				{
// 					m_wndCalData.SetStyleRange(CGXRange().SetCells(1, col), CGXStyle().SetValue(strTemp).SetInterior(RGB(255,255,255)));
// 				}
// 				//max
// 				strTemp.Format("%s(C%d)", m_pdoc->ValueString(avg.GetMaxValue(), nItem), avg.GetMaxIndex());
// 				if( fabs(avg.GetMaxValue()) > pCalPoint->m_dVADAccuracy[r] ) 
// 				{
// 					m_wndCalData.SetStyleRange(CGXRange().SetCells(2, col), CGXStyle().SetValue(strTemp).SetInterior(RGB(150,255,255)));
// 				}
// 				else 
// 				{
// 					m_wndCalData.SetStyleRange(CGXRange().SetCells(2, col), CGXStyle().SetValue(strTemp).SetInterior(RGB(255,255,255)));
// 				}
// 			}
// 			else if((col-_FROZEN_COL_CNT_+3) % ONE_POINT_COL_COUNT == 0)	//오차 표시 Column
// 			{
// 				//min
// 				strTemp.Format("%s(C%d)", m_pdoc->ValueString(avg.GetMinValue(), nItem), avg.GetMinIndex());
// 				if( fabs(avg.GetMinValue()) > pCalPoint->m_dVDAAccuracy[r] ) 
// 				{
// 					m_wndCalData.SetStyleRange(CGXRange().SetCells(1, col), CGXStyle().SetValue(strTemp).SetInterior(RGB(150,255,255)));
// 				}
// 				else 
// 				{
// 					m_wndCalData.SetStyleRange(CGXRange().SetCells(1, col), CGXStyle().SetValue(strTemp).SetInterior(RGB(255,255,255)));
// 				}
// 				//max
// 				strTemp.Format("%s(C%d)", m_pdoc->ValueString(avg.GetMaxValue(), nItem), avg.GetMaxIndex());
// 				if( fabs(avg.GetMaxValue()) > pCalPoint->m_dVDAAccuracy[r] ) 
// 				{
// 					m_wndCalData.SetStyleRange(CGXRange().SetCells(2, col), CGXStyle().SetValue(strTemp).SetInterior(RGB(150,255,255)));
// 				}
// 				else 
// 				{
// 					m_wndCalData.SetStyleRange(CGXRange().SetCells(2, col), CGXStyle().SetValue(strTemp).SetInterior(RGB(255,255,255)));
// 				}
// 			}
			else
			{
				//min
				strTemp.Format("%s(C%d)", m_pdoc->ValueString(avg.GetMinValue(), nItem), avg.GetMinIndex());
				m_wndCalData.SetValueRange(CGXRange().SetCells(1, col), strTemp);
				//max
				strTemp.Format("%s(C%d)", m_pdoc->ValueString(avg.GetMaxValue(), nItem), avg.GetMaxIndex());
				m_wndCalData.SetValueRange(CGXRange().SetCells(2, col), strTemp);
			}
			//avreage
			strTemp.Format("%s", m_pdoc->ValueString(avg.GetAvg(), nItem));
			m_wndCalData.SetValueRange(CGXRange().SetCells(3, col), strTemp);
			//stdev
			strTemp.Format("%s", m_pdoc->ValueString(avg.GetSTDD(), nItem));
			m_wndCalData.SetValueRange(CGXRange().SetCells(4, col), strTemp);
		}
	}

	m_wndCalData.LockUpdate(bLock);
	m_wndCalData.Redraw();

}

// 지령에 대한 교정을 완료 하였음
// 교정 선택 Mode에 따라 다음 교정 명령 하달 
BOOL CCalibratorDlg::EndResultData(long nModuleID, WORD nMode, EP_CAL_SELECT_INFO calEndInfo)
{
	if(nModuleID != m_nUnitNo)	return FALSE;

	m_nTimeOutCnt = 0;

	CString strTemp;
	
	UpdateData(TRUE);

	if( m_nCalType == CAL_TYPE_CALCHECK )	// 교정 후 확인
	{
		if( nMode == 1 )	// 확인 작업이 끝난 상태 (완전 완료 )
		{
			if(calEndInfo.nType == CAL_VOLTAGE)
			{
				strTemp.Format(TEXT_LANG[36], calEndInfo.nBoardNo, calEndInfo.nRange);//"Board %d 전압 교정 및 확인을 완료 하였습니다.(Range %d)"
			}
			else
			{
				strTemp.Format(TEXT_LANG[37], calEndInfo.nBoardNo, calEndInfo.nRange);//"Board %d 전류 교정 및 확인을 완료 하였습니다.(Range %d)"
			}
			
			SetCalState(CAL_STATE_IDLE);
			GetDlgItem(IDC_STATIC_PROGRESS_TITLE)->SetWindowText(strTemp);
			AfxMessageBox(strTemp);
		}
		else if(nMode == 0)	//교정 작업 완료 => 확인 작업 명령 송부 
		{
// 			EP_CAL_SELECT_INFO selInfo;
// 			memcpy(&selInfo, calEndInfo, sizeof(EP_CAL_SELECT_INFO));
//			selInfo.bMutiMode = m_bMultiMode;
			calEndInfo.bMutiMode = m_bMultiMode;
			if(EPSendCommand(m_nUnitNo, 0, 0, EP_CMD_CHK_START, &calEndInfo, sizeof(EP_CAL_SELECT_INFO)) != EP_ACK)
			{
				AfxMessageBox(TEXT_LANG[2]);//"교정 확인 명령 전송을 실패 하였습니다."
			}

			//화면 모드 자동 전환
			m_nDataMode = CAL_DATA_MODE_AFTER;		//화면을 완료 data mode로 변경
			m_nDataType = calEndInfo.nType;
			ReDrawGrid(m_nDataMode, m_nDataType);
			DisplayData(m_nDataMode, m_nDataType);
			UpdateData(FALSE);

			if(calEndInfo.nType == CAL_VOLTAGE)
			{
				strTemp.Format(TEXT_LANG[38], calEndInfo.nBoardNo, calEndInfo.nRange);//"Board %d 전압 교정완료 후 확인 작업을 시작합니다.(Range %d)"
			}
			else
			{
				strTemp.Format(TEXT_LANG[39], calEndInfo.nBoardNo, calEndInfo.nRange);//"Board %d 전류 교정완료 후 확인 작업을 시작합니다.(Range %d)"
			}
			GetDlgItem(IDC_STATIC_PROGRESS_TITLE)->SetWindowText(strTemp);
		}
	}
	else		//교정이나 확인 작업만  선택시 	
	{
		if(nMode == 0)			//if( m_nCalType == CAL_TYPE_CALIB)	// 교정
		{
			if(calEndInfo.nType == CAL_VOLTAGE)
			{
				strTemp.Format(TEXT_LANG[40], calEndInfo.nBoardNo, calEndInfo.nRange);//"Board %d 전압 교정을 완료 하였습니다.(Range %d)"
			}
			else
			{
				strTemp.Format(TEXT_LANG[41], calEndInfo.nBoardNo, calEndInfo.nRange);//"Board %d 전류 교정을 완료 하였습니다.(Range %d)"
			}
		}
		else if(nMode == 1)		//if( m_nCalType == CAL_TYPE_CHECK )	// 확인
		{
			if(calEndInfo.nType == CAL_VOLTAGE)
			{
				strTemp.Format(TEXT_LANG[42], calEndInfo.nBoardNo, calEndInfo.nRange);//"Board %d 전압 확인을 완료 하였습니다.(Range %d)"
			}
			else
			{
				strTemp.Format(TEXT_LANG[43], calEndInfo.nBoardNo, calEndInfo.nRange);//"Board %d 전류 확인을 완료 하였습니다.(Range %d)"
			}
		}
		SetCalState(CAL_STATE_IDLE);
		GetDlgItem(IDC_STATIC_PROGRESS_TITLE)->SetWindowText(strTemp);
		AfxMessageBox(strTemp);
	}
	return TRUE;
}

void CCalibratorDlg::ReDrawGrid(int nMode, int nType)
{
	BOOL bLock = m_wndCalData.LockUpdate();

	m_wndCalData.SetColCount(_FROZEN_COL_CNT_);
	CString str;

	TRACE("=================Redraw grid\n");

	COLORREF boldColor = RGB(255, 128, 0);
	int nTotRow = m_wndCalData.GetRowCount();

	if(nMode == CAL_DATA_MODE_BEFORE)		//교정전
	{
		if(nType == CAL_DATA_TYPE_VTG)		//전압
		{
			for(WORD r = 0; r<m_calibrtaionData.m_PointData.GetVRangeCnt(); r++)
			{
				for(WORD p=0; p<m_calibrtaionData.m_PointData.GetVtgSetPointCount(r); p++)
				{
					int nCnt = m_wndCalData.GetColCount()+1;
					m_wndCalData.InsertCols(nCnt, ONE_POINT_COL_COUNT);
					m_wndCalData.SetColWidth(nCnt, nCnt, 100);
//					m_wndCalData.SetStyleRange(CGXRange(0, nCnt, 0, nCnt+5-1), CGXStyle().SetBorders(gxBorderTop, CGXPen().SetWidth(1).SetColor(boldColor)));
					m_wndCalData.SetStyleRange(CGXRange(0, nCnt, nTotRow, nCnt), CGXStyle().SetBorders(gxBorderLeft, CGXPen().SetWidth(1).SetColor(boldColor)));
					m_wndCalData.SetStyleRange(CGXRange(0, nCnt+ONE_POINT_COL_COUNT-1, nTotRow, nCnt+ONE_POINT_COL_COUNT-1), CGXStyle().SetBorders(gxBorderRight, CGXPen().SetWidth(1).SetColor(boldColor)));
					//double click sort시 이동하므로 표시하지 않음
//					m_wndCalData.SetStyleRange(CGXRange(nTotRow, nCnt, nTotRow, nCnt+5-1), CGXStyle().SetBorders(gxBorderBottom, CGXPen().SetWidth(1).SetColor(boldColor)));

					str.Format(TEXT_LANG[44], p+1, m_pdoc->ValueString(m_calibrtaionData.m_PointData.GetVSetPoint(r,p), EP_VOLTAGE, TRUE));//"설정%d(%s)"
					m_wndCalData.SetValueRange(CGXRange(0, nCnt++), str);
					str.Format(TEXT_LANG[45], m_pdoc->m_strVUnit);//"측정(%s)"
					m_wndCalData.SetValueRange(CGXRange(0, nCnt++), str);
					str.Format(TEXT_LANG[46], m_pdoc->m_strVUnit);//"계측기(%s)"
					m_wndCalData.SetValueRange(CGXRange(0, nCnt++), str);
					str.Format(TEXT_LANG[47], m_pdoc->m_strVUnit);//"측정오차(%s)"
					m_wndCalData.SetValueRange(CGXRange(0, nCnt++), str);
					str.Format(TEXT_LANG[48], m_pdoc->m_strVUnit);//"계측오차(%s)"
					m_wndCalData.SetValueRange(CGXRange(0, nCnt++), str);
					str.Format(TEXT_LANG[49], m_pdoc->m_strVUnit);//"피드백오차(%s)"
					m_wndCalData.SetValueRange(CGXRange(0, nCnt++), str);
				}
			}
		}
		else	//전류
		{
			for(WORD r = 0; r<m_calibrtaionData.m_PointData.GetIRangeCnt(); r++)
			{
				for(WORD p=0; p<m_calibrtaionData.m_PointData.GetCrtSetPointCount(r); p++)
				{
					int nCnt = m_wndCalData.GetColCount()+1;
					m_wndCalData.InsertCols(nCnt, ONE_POINT_COL_COUNT);
					m_wndCalData.SetColWidth(nCnt, nCnt, 100);
//					m_wndCalData.SetStyleRange(CGXRange(0, nCnt, 0, nCnt+5-1), CGXStyle().SetBorders(gxBorderTop, CGXPen().SetWidth(1).SetColor(boldColor)));
					m_wndCalData.SetStyleRange(CGXRange(0, nCnt, nTotRow, nCnt), CGXStyle().SetBorders(gxBorderLeft, CGXPen().SetWidth(1).SetColor(boldColor)));
					m_wndCalData.SetStyleRange(CGXRange(0, nCnt+ONE_POINT_COL_COUNT-1, nTotRow, nCnt+ONE_POINT_COL_COUNT-1), CGXStyle().SetBorders(gxBorderRight, CGXPen().SetWidth(1).SetColor(boldColor)));
					//double click sort시 이동하므로 표시하지 않음
//					m_wndCalData.SetStyleRange(CGXRange(nTotRow, nCnt, nTotRow, nCnt+5-1), CGXStyle().SetBorders(gxBorderBottom, CGXPen().SetWidth(1).SetColor(boldColor)));

					str.Format(TEXT_LANG[44], p+1, m_pdoc->ValueString(m_calibrtaionData.m_PointData.GetISetPoint(r,p), EP_CURRENT, TRUE));//"설정%d(%s)"
					m_wndCalData.SetValueRange(CGXRange(0, nCnt++), str);
					str.Format(TEXT_LANG[45], m_pdoc->m_strIUnit);//"측정(%s)"
					m_wndCalData.SetValueRange(CGXRange(0, nCnt++), str);
					str.Format(TEXT_LANG[46], m_pdoc->m_strIUnit);//"계측기(%s)"
					m_wndCalData.SetValueRange(CGXRange(0, nCnt++), str);
					str.Format(TEXT_LANG[47], m_pdoc->m_strIUnit);//"측정오차(%s)"
					m_wndCalData.SetValueRange(CGXRange(0, nCnt++), str);
					str.Format(TEXT_LANG[48], m_pdoc->m_strIUnit);//"계측오차(%s)"
					m_wndCalData.SetValueRange(CGXRange(0, nCnt++), str);
					str.Format(TEXT_LANG[49], m_pdoc->m_strIUnit);//"피드백오차(%s)"
					m_wndCalData.SetValueRange(CGXRange(0, nCnt++), str);
				}
			}
		}
	}
	else
	{
		if(nType == CAL_DATA_TYPE_VTG)		// 전압
		{
			for(WORD r = 0; r<m_calibrtaionData.m_PointData.GetVRangeCnt(); r++)
			{
				for(WORD p=0; p<m_calibrtaionData.m_PointData.GetVtgCheckPointCount(r); p++)
				{
					int nCnt = m_wndCalData.GetColCount()+1;
					m_wndCalData.InsertCols(nCnt, ONE_POINT_COL_COUNT);
					m_wndCalData.SetColWidth(nCnt, nCnt, 100);
//					m_wndCalData.SetStyleRange(CGXRange(0, nCnt, 0, nCnt+5-1), CGXStyle().SetBorders(gxBorderTop, CGXPen().SetWidth(1).SetColor(boldColor)));
					m_wndCalData.SetStyleRange(CGXRange(0, nCnt, nTotRow, nCnt), CGXStyle().SetBorders(gxBorderLeft, CGXPen().SetWidth(1).SetColor(boldColor)));
					m_wndCalData.SetStyleRange(CGXRange(0, nCnt+ONE_POINT_COL_COUNT-1, nTotRow, nCnt+ONE_POINT_COL_COUNT-1), CGXStyle().SetBorders(gxBorderRight, CGXPen().SetWidth(1).SetColor(boldColor)));
					//double click sort시 이동하므로 표시하지 않음
//					m_wndCalData.SetStyleRange(CGXRange(nTotRow, nCnt, nTotRow, nCnt+5-1), CGXStyle().SetBorders(gxBorderBottom, CGXPen().SetWidth(1).SetColor(boldColor)));

					str.Format(TEXT_LANG[44], p+1, m_pdoc->ValueString(m_calibrtaionData.m_PointData.GetVCheckPoint(r,p), EP_VOLTAGE, TRUE));//"설정%d(%s)"
					m_wndCalData.SetValueRange(CGXRange(0, nCnt++), str);
					str.Format(TEXT_LANG[45], m_pdoc->m_strVUnit);//"측정(%s)"
					m_wndCalData.SetValueRange(CGXRange(0, nCnt++), str);
					str.Format(TEXT_LANG[46], m_pdoc->m_strVUnit);//"계측기(%s)"
					m_wndCalData.SetValueRange(CGXRange(0, nCnt++), str);
					str.Format(TEXT_LANG[47], m_pdoc->m_strVUnit);//"측정오차(%s)"
					m_wndCalData.SetValueRange(CGXRange(0, nCnt++), str);
					str.Format(TEXT_LANG[48], m_pdoc->m_strVUnit);//"계측오차(%s)"
					m_wndCalData.SetValueRange(CGXRange(0, nCnt++), str);
					str.Format(TEXT_LANG[49], m_pdoc->m_strVUnit);//"피드백오차(%s)"
					m_wndCalData.SetValueRange(CGXRange(0, nCnt++), str);
				}
			}
		}
		else	// 전류
		{
			for(WORD r = 0; r<m_calibrtaionData.m_PointData.GetIRangeCnt(); r++)
			{
				for(WORD p=0; p<m_calibrtaionData.m_PointData.GetCrtCheckPointCount(r); p++)
				{
					int nCnt = m_wndCalData.GetColCount()+1;
					m_wndCalData.InsertCols(nCnt, ONE_POINT_COL_COUNT);
					m_wndCalData.SetColWidth(nCnt, nCnt, 100);
//					m_wndCalData.SetStyleRange(CGXRange(0, nCnt, 0, nCnt+5-1), CGXStyle().SetBorders(gxBorderTop, CGXPen().SetWidth(1).SetColor(boldColor)));
					m_wndCalData.SetStyleRange(CGXRange(0, nCnt, nTotRow, nCnt), CGXStyle().SetBorders(gxBorderLeft, CGXPen().SetWidth(1).SetColor(boldColor)));
					m_wndCalData.SetStyleRange(CGXRange(0, nCnt+ONE_POINT_COL_COUNT-1, nTotRow, nCnt+ONE_POINT_COL_COUNT-1), CGXStyle().SetBorders(gxBorderRight, CGXPen().SetWidth(1).SetColor(boldColor)));
					//double click sort시 이동하므로 표시하지 않음
//					m_wndCalData.SetStyleRange(CGXRange(nTotRow, nCnt, nTotRow, nCnt+5-1), CGXStyle().SetBorders(gxBorderBottom, CGXPen().SetWidth(1).SetColor(boldColor)));

					str.Format(TEXT_LANG[44], p+1, m_pdoc->ValueString(m_calibrtaionData.m_PointData.GetICheckPoint(r,p), EP_CURRENT, TRUE));//"설정%d(%s)"
					m_wndCalData.SetValueRange(CGXRange(0, nCnt++), str);
					str.Format(TEXT_LANG[45], m_pdoc->m_strIUnit);//"측정(%s)"
					m_wndCalData.SetValueRange(CGXRange(0, nCnt++), str);
					str.Format(TEXT_LANG[46], m_pdoc->m_strIUnit);//"계측기(%s)"
					m_wndCalData.SetValueRange(CGXRange(0, nCnt++), str);
					str.Format(TEXT_LANG[47], m_pdoc->m_strIUnit);//"측정오차(%s)"
					m_wndCalData.SetValueRange(CGXRange(0, nCnt++), str);
					str.Format(TEXT_LANG[48], m_pdoc->m_strIUnit);//"계측오차(%s)"
					m_wndCalData.SetValueRange(CGXRange(0, nCnt++), str);
					str.Format(TEXT_LANG[49], m_pdoc->m_strIUnit);//"피드백오차(%s)"
					m_wndCalData.SetValueRange(CGXRange(0, nCnt++), str);
				}
			}
		}
	}

	m_wndCalData.LockUpdate(bLock);
	m_wndCalData.Redraw();
}

void CCalibratorDlg::OnSelchangeComboMode() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	ReDrawGrid(m_nDataMode, m_nDataType);
	DisplayData(m_nDataMode, m_nDataType);
}

void CCalibratorDlg::OnSelchangeComboType() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	ReDrawGrid(m_nDataMode, m_nDataType);
	DisplayData(m_nDataMode, m_nDataType);
}

LRESULT CCalibratorDlg::OnRButtonClickedRowCol(WPARAM wParam, LPARAM lParam)
{
	ROWCOL nRow, nCol;
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;
	nCol = LOWORD(wParam);
	nRow = HIWORD(wParam);
	
	ASSERT(pGrid);
	if(nRow < 1)	return 0;
	
	CPoint point;
	::GetCursorPos(&point);
	
	if (point.x == -1 && point.y == -1)
	{
		//keystroke invocation
		CRect rect;
		pGrid->GetClientRect(rect);
		pGrid->ClientToScreen(rect);

		point = rect.TopLeft();
		point.Offset(5, 5);
	}


	CMenu menu, *pPopup;
	if(pGrid == (CMyGridWnd *)&m_wndCalData)
	{
		VERIFY(menu.LoadMenu(IDR_MENU_CAL));
		pPopup = menu.GetSubMenu(0);
		ASSERT(pPopup != NULL);
		
		CWnd* pWndPopupOwner = this;
		while (pWndPopupOwner->GetStyle() & WS_CHILD)
			pWndPopupOwner = pWndPopupOwner->GetParent();

		pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, pWndPopupOwner);
	}
	
	return TRUE;
}

void CCalibratorDlg::OnSelStart() 
{
	// TODO: Add your command handler code here
	CFormModule *pModule = m_pdoc->GetModuleInfo(m_nUnitNo);
	if(pModule == NULL)	return;

	if(m_nState != CAL_STATE_IDLE)
	{
		return;
	}

	WORD state = pModule->GetState();
	if(state != EP_STATE_IDLE && state != EP_STATE_STANDBY && state != EP_STATE_MAINTENANCE && state != EP_STATE_READY)
	{
		CString strTemp;
		strTemp.Format(TEXT_LANG[1], GetModuleName(m_nUnitNo));//"%s는 교정 가능한 상태가 아닙니다."
		AfxMessageBox(strTemp, MB_OK|MB_ICONSTOP);
		return; 
	}

	CString strTemp;
	WORD nBoardNo = 0;
	BYTE nChSel[EP_MAX_CH_PER_BD];
	ZeroMemory(nChSel, sizeof(nChSel));

	CRowColArray	awRows;
	m_wndCalData.GetSelectedRows(awRows);

	int nSelNo = awRows.GetSize();
	int nSelCnt = 0;
	for(int i = 0; i<nSelNo; i++)	//선택한 모든 Row
	{
		int nCh = atoi(m_wndCalData.GetValueRowCol(awRows[i], 1));		//monitoring ch
		if(nCh > 0 && nCh <= EP_MAX_CH_PER_MD)
		{
			int wBoard, wChNo;
			m_calibrtaionData.m_ChCaliData[nCh-1].GetHWChNo(wBoard, wChNo);
			if(nBoardNo == 0)
			{
				nBoardNo = (WORD)wBoard;
				if(wChNo > 0 && wChNo <= EP_MAX_CH_PER_BD)
				{
					nChSel[wChNo-1]	= 1;
					nSelCnt++;
				}
			}
			else
			{
				if(nBoardNo != wBoard)		//같은 Board 번호만 선택가능함
				{
					strTemp.Format(TEXT_LANG[50], nBoardNo, nSelCnt );//"2개 이상의 Board에서 채널이 선택되었습니다. Board %d에 선택된 %d개 채널만 진행 하시겠습니까?"
					if(MessageBox(strTemp, TEXT_LANG[51], MB_YESNO|MB_ICONQUESTION) == IDNO)//"교정진행"
					{
						return;
					}
					break;
				}
				else
				{
					if(wChNo > 0 && wChNo <= EP_MAX_CH_PER_BD)
					{
						nChSel[wChNo-1]	= 1;
						nSelCnt++;
					}
				}
			}
		}
	}

	//교정 가능한 CH이 있으면
	if(nSelCnt > 0)
	{
		CCaliStartDlg dlg( m_nUnitNo, m_calibrtaionData.m_PointData.GetVRangeCnt(), m_calibrtaionData.m_PointData.GetIRangeCnt(), m_nOptTrayType, this );	
		int nRtn = 0;
		EP_CAL_SELECT_INFO selInfo;
		ZeroMemory(&selInfo, sizeof(EP_CAL_SELECT_INFO));
		dlg.n_board = nBoardNo;
		dlg.m_bChannelMode = TRUE;
		dlg.n_cali = m_nCalType;
		dlg.n_vi = m_nDataType;
		dlg.n_iTrayType = m_nOptTrayType;		//ljb 2011523 TRY TYPE
		
		//CH 교정에서는 Multi Mode를 지원하지 않음
		//dlg.m_bMultiMode = m_bMultiMode;

		if(dlg.DoModal() == IDOK)
		{	
			m_calibrtaionData.SetUserName(dlg.m_strUserName);
			m_nCalType = dlg.n_cali;

			selInfo.nMain = dlg.n_main;			//main DA or CH
			selInfo.nType = dlg.n_vi;			//전압/전류

			selInfo.nRange = dlg.n_range;		//교정 Range			
			selInfo.bTrayType = m_nOptTrayType;		//ljb 2011523 TRAY TYPE
			
			selInfo.nBoardNo = dlg.n_board;
			m_nBoardNo = dlg.n_board;

			memcpy(selInfo.byCnSel, nChSel, sizeof(selInfo.byCnSel));	//default 모든 CH

			selInfo.bMutiMode = dlg.m_bMultiMode;
			m_bMultiMode = dlg.m_bMultiMode;

			if(m_nCalType == CAL_TYPE_CHECK)	//확인 작업 
			{
				if((nRtn = EPSendCommand(m_nUnitNo, 0, 0, EP_CMD_CHK_START, &selInfo, sizeof(EP_CAL_SELECT_INFO)))!= EP_ACK)
				{
					AfxMessageBox(TEXT_LANG[52]);				//"교정 확인 시작 명령 전송을 실패 하였습니다."
				}
			}
			else	//교정 후 확인 or 교정만 선택
			{
				if((nRtn = EPSendCommand(m_nUnitNo, 0, 0, EP_CMD_CAL_START, &selInfo, sizeof(EP_CAL_SELECT_INFO)))!= EP_ACK)
				{
					AfxMessageBox(TEXT_LANG[53]);//"교정 시작 명령 전송을 실패 하였습니다."
				}			
			}
			GetDlgItem(IDC_STATIC_PROGRESS_TITLE)->SetWindowText(TEXT_LANG[4]);//"교정을 시작합니다."
			
			m_nTimeOutCnt = 0;
			SetCalState(CAL_STATE_RUN);

			//화면 모양을 자동으로 전환
			if( m_nCalType == CAL_TYPE_CHECK )		{	m_nDataMode = CAL_DATA_MODE_AFTER;	}	//확인 작업일 경우 Diaply 화면 변경
			else									{	m_nDataMode = CAL_DATA_MODE_BEFORE;	}	//교정이나 교정후 확인 작업일 경우 화면을 교정전 화면으로 변경
			m_nDataType = selInfo.nType;
			ReDrawGrid(m_nDataMode, m_nDataType);
			DisplayData(m_nDataMode, m_nDataType, selInfo.nRange );
			UpdateData(FALSE);
		}
	}
	
	return;
}

void CCalibratorDlg::OnButtonAccu() 
{
	// TODO: Add your control notification handler code here
	CAccuracySetDlg *pDlg = new CAccuracySetDlg(&m_calibrtaionData.m_PointData, this);
	pDlg->DoModal();

	delete pDlg;
	pDlg = NULL;
}

void CCalibratorDlg::OnUpdateSelStart(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);
	CFormModule *pModule = m_pdoc->GetModuleInfo(m_nUnitNo);
	if(pModule != NULL)
	{
		WORD state = pModule->GetState();
		if(state == EP_STATE_IDLE || state == EP_STATE_STANDBY || state == EP_STATE_MAINTENANCE || state == EP_STATE_READY)
		{
			pCmdUI->Enable(TRUE);
		}
	}
}

void CCalibratorDlg::OnCancel() 
{
	CString strMsg;
	if(m_nState == CAL_STATE_RUN)
	{
		strMsg.Format(TEXT_LANG[54], ::GetModuleName(m_nUnitNo));//"%s 현재 교정 동작중이므로 종료 할 수 없습니다."
		AfxMessageBox(strMsg, MB_OK|MB_ICONSTOP);
		return;
	}

	BOOL bEdited = CheckEditedCh();
	if(bEdited)
	{
		strMsg.Format(TEXT_LANG[55], ::GetModuleName(m_nUnitNo));//"%s 교정 Data가 수정 되었습니다. 지금 Update 하시겠습니까?"
		//수정 data update
		int nRtn;
		if((nRtn = MessageBox(strMsg, "Data Update", MB_YESNOCANCEL|MB_ICONQUESTION)) == IDYES)
		{
			OnCaliDataUpdate(); 
		}
		else if(nRtn == IDCANCEL)		//return;
		{
			return;
		}
		else
		{
			//수정된 내용은 모두 무시된다.
		}
	}
	CDialog::OnCancel();
}

void CCalibratorDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	CString strTemp;
	static BOOL bBlank = FALSE;
	BOOL bLock = m_wndCalData.LockUpdate();
	for(int row=0; row<m_wndCalData.GetRowCount(); row++)
	{
		int nCh = atoi(m_wndCalData.GetValueRowCol(row+1, _MON_CH_COL_));		//monitoring ch
		CChCaliData *pChCalData = m_calibrtaionData.GetChCalData(nCh-1);
		if(pChCalData)
		{
			if(pChCalData->GetState() == CAL_DATA_MODIFY && bBlank)
			{
				m_wndCalData.SetStyleRange(CGXRange(row+1, _MON_CH_COL_), CGXStyle().SetInterior(RGB(255,128,128)));
			}
			else
			{
				m_wndCalData.SetStyleRange(CGXRange(row+1, _MON_CH_COL_), CGXStyle().SetInterior(RGB(255,255,255)));
			}
		}
	}
	m_wndCalData.LockUpdate(bLock);
	m_wndCalData.Redraw();

	/*
	m_nTimeOutCnt++;

	if(m_nTimeOutCnt == CAL_TIME_OUT_SEC && m_nState == CAL_STATE_RUN)
	{
		//send stop cmd
		if(EPSendCommand(m_nUnitNo, 0, 0, EP_CMD_CAL_STOP, NULL,0)!= EP_ACK)
		{


		}

		SetCalState(CAL_STATE_IDLE);

		strTemp.Format("%s에서 교정 응답이 없습니다.", GetModuleName(m_nUnitNo));
		AfxMessageBox(strTemp);
	}
	*/

	if(m_nState == CAL_STATE_RUN)
	{
		if(bBlank)	m_wndCalState.SetBkColor(RGB(255,255,255));
		else		m_wndCalState.SetBkColor(RGB(192,192,192));

	}

	CFormModule *pModule = m_pdoc->GetModuleInfo(m_nUnitNo);
	if(pModule)
	{
		WORD curState = pModule->GetState();
		if( m_CaliModuleState[m_nUnitNo] != EP_STATE_LINE_OFF && curState == EP_STATE_LINE_OFF)
		{
			m_CaliModuleState[m_nUnitNo] = curState;
			SetCalState(CAL_STATE_LINEOFF);
			strTemp.Format(TEXT_LANG[56], GetModuleName(m_nUnitNo));//"%s 와 통신이 두절되었습니다. 연결상태를 확인하시기 바랍니다."
			AfxMessageBox(strTemp);			
		}
		m_CaliModuleState[m_nUnitNo] = curState;
	}

	bBlank = !bBlank;
	CDialog::OnTimer(nIDEvent);
}

void CCalibratorDlg::OnCalStop() 
{
	// TODO: Add your command handler code here
	OnStopBtn();
}

//선택 CH 교정 Data 적용
void CCalibratorDlg::OnCalDataUpdate() 
{
	// TODO: Add your command handler code here
//	OnCaliDataUpdate();

	if(LoginPremissionCheck(PMS_MODULE_CAL_UPDATE) == FALSE)
	{
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_PERMISSION_ERROR) +" [Calibration data update]");
		return ;
	}

	CFormModule *pModule = m_pdoc->GetModuleInfo(m_nUnitNo);
	if(pModule == NULL)	return;

	WORD state = pModule->GetState();
	if(state != EP_STATE_IDLE && state != EP_STATE_STANDBY && state != EP_STATE_MAINTENANCE && state != EP_STATE_READY)
	{
		CString strTemp;
		strTemp.Format(TEXT_LANG[14], GetModuleName(m_nUnitNo));//"%s는 교정 Data를 적용 가능한 상태가 아닙니다."
		AfxMessageBox(strTemp, MB_OK|MB_ICONSTOP);
		return; 
	}

	//선택 Ch에 대한 overspec 확인 
	if(CheckOverSpec(FALSE))
	{
		//교정 에러 범위 초과 Ch에 대한 경고
		if(IDNO == MessageBox(TEXT_LANG[57], TEXT_LANG[6], MB_YESNO|MB_ICONWARNING|MB_DEFBUTTON2))//"선택 채널 교정 결과 중 오차 범위를 초과하는 채널이 존재합니다.\n\n무시하고 진행 하시겠습니까?"//"전송 확인"
		{
			return;
		}
	}

	CString strTemp;
	WORD nBoardNo = 0;
	BYTE nChSel[EP_MAX_CH_PER_BD];
	ZeroMemory(nChSel, sizeof(nChSel));
	
	CWordArray awSelMonCh;

	CRowColArray	awRows;
	m_wndCalData.GetSelectedRows(awRows);
	int nSelNo = awRows.GetSize();
	int nSelCnt = 0;
	for(int i = 0; i<nSelNo; i++)	//선택한 모든 Row
	{
		int nCh = atoi(m_wndCalData.GetValueRowCol(awRows[i], _MON_CH_COL_));		//monitoring ch
		if(nCh > 0 && nCh <= EP_MAX_CH_PER_MD)
		{
			int wBoard, wChNo;
			m_calibrtaionData.m_ChCaliData[nCh-1].GetHWChNo(wBoard, wChNo);
			if(nBoardNo == 0)
			{
				nBoardNo = (WORD)wBoard;
				if(wChNo > 0 && wChNo <= EP_MAX_CH_PER_BD)
				{
					nChSel[wChNo-1]	= 1;
					nSelCnt++;

					awSelMonCh.Add((WORD)nCh);
				}
			}
			else
			{
				if(nBoardNo != wBoard)		//같은 Board 번호만 선택가능함
				{
					strTemp.Format(TEXT_LANG[50], nBoardNo, nSelCnt );//"2개 이상의 Board에서 채널이 선택되었습니다. Board %d에 선택된 %d개 채널만 진행 하시겠습니까?"
					if(MessageBox(strTemp, TEXT_LANG[51], MB_YESNO|MB_ICONQUESTION) == IDNO)//"교정진행"
					{
						return;
					}
					break;
				}
				else
				{
					if(wChNo > 0 && wChNo <= EP_MAX_CH_PER_BD)
					{
						nChSel[wChNo-1]	= 1;
						nSelCnt++;

						awSelMonCh.Add((WORD)nCh);
					}
				}
			}
		}
	}

	if(nSelCnt <= 0)
	{
		AfxMessageBox(TEXT_LANG[58]);//"선택 채널이 없습니다."
		return;
	}

	// TODO: Add your control notification handler code here
	if(IDYES == MessageBox(TEXT_LANG[59], TEXT_LANG[6], MB_YESNO|MB_ICONWARNING|MB_DEFBUTTON2))//"선택 채널에 현재 교정된 Data를 적용 하시겠습니까?\n\n주의:적용 이후 이전 교정 Data는 삭제되어 복구 할 수 없습니다."//"전송 확인"
	{
		int nRtn = 0;
		EP_CAL_SELECT_INFO selInfo;
		ZeroMemory(&selInfo, sizeof(EP_CAL_SELECT_INFO));
		
		//전압 Udpate
		selInfo.nMain = 1;						//ch
		selInfo.nType = 0;		//voltage
		selInfo.nRange = 0;						//All Update
		selInfo.nBoardNo = nBoardNo;			//지정 Board
		selInfo.bTrayType = m_nOptTrayType;		//ljb 2011523 TRAY TYPE
		memcpy(selInfo.byCnSel, nChSel, sizeof(selInfo.byCnSel));	//default 모든 CH

		if((nRtn = EPSendCommand(m_nUnitNo, 0, 0, EP_CMD_CAL_UPDATE, &selInfo, sizeof(EP_CAL_SELECT_INFO)))!= EP_ACK)
		{
			AfxMessageBox(TEXT_LANG[60]);//"선택 채널 교정 Data를 적용에 실패 하였습니다."
			return;
		}

// 		//전류 Update
// 		ZeroMemory(&selInfo, sizeof(EP_CAL_SELECT_INFO));
// 		selInfo.nMain = 1;						//ch
// 		selInfo.nType = CAL_DATA_TYPE_CRT;		//current
// 		selInfo.nRange = 0;						//All Update
// 		selInfo.nBoardNo = nBoardNo;			//지정 Board	
// 			memcpy(selInfo.byCnSel, nChSel, sizeof(selInfo.byCnSel));	//default 모든 CH
// 		if((nRtn = EPSendCommand(m_nUnitNo, 0, 0, EP_CMD_CAL_UPDATE, &selInfo, sizeof(EP_CAL_SELECT_INFO)))!= EP_ACK)
// 		{
// 			AfxMessageBox("전류교정 Data를 적용에 실패 하였습니다.");
// 			return;
// 		}

		// [8/27/2009 경여니]
		// 캘리브레이션 결과파일 저장
		SaveUpdateResultData(&awSelMonCh);	
		
		CString str;
		str.Format("%s\\Calibration\\%s.cbd", m_pdoc->m_strCurFolder, GetModuleName(m_nUnitNo));
		m_calibrtaionData.SaveDataToFile(str, &awSelMonCh);
		str.Format("%s [%s]", m_calibrtaionData.GetUpdateTime(), m_calibrtaionData.GetUserName());
		GetDlgItem(IDC_STATIC_DATE)->SetWindowText(str);
	}
}

//sepc에 벗어난 CH이 있는지 검사 
BOOL CCalibratorDlg::CheckOverSpec(BOOL bAllData)
{
	CRowColArray	awRows;
	double	diffAd = 0;
	double	diffMeter = 0;
	double	diffFeedback = 0;

	if(bAllData)
	{
		for(int row=0; row<m_wndCalData.GetRowCount(); row++)
		{
			awRows.Add(DWORD(row+1));
		}
	}
	else
	{
		m_wndCalData.GetSelectedRows(awRows);
	}
	
	BOOL bOverSpec = FALSE;
	double dwSetData, dwADData, dwMeterData;
	CCaliPoint *pCalPoint = m_calibrtaionData.GetCalPointData();

	WORD r=0, p=0;
	int row = 0;

	for(row=0; row<awRows.GetSize(); row++)
	{
		int nCh = atoi(m_wndCalData.GetValueRowCol(awRows[row], _MON_CH_COL_));		//monitoring ch
		CChCaliData *pChCalData = m_calibrtaionData.GetChCalData(nCh-1);		
		if(pChCalData)
		{
			for(r = 0; r<pCalPoint->GetVRangeCnt(); r++)
			{
				for(p=0; p<pCalPoint->GetVtgCheckPointCount(r); p++)
				{
					double	diffAd = 0;
					double	diffMeter = 0;

					dwSetData = pCalPoint->GetVCheckPoint(r,p);
					pChCalData->GetVCheckData(p, dwADData, dwMeterData);

					diffAd = dwSetData - dwADData;
					if( fabs(diffAd) > pCalPoint->m_dVADAccuracy[r] )
					{
						bOverSpec = TRUE;
					}
				}
			}
			for(r = 0; r<pCalPoint->GetIRangeCnt(); r++)
			{
				for(p=0; p<pCalPoint->GetCrtCheckPointCount(r); p++)
				{
					dwSetData = pCalPoint->GetICheckPoint(r,p);
					pChCalData->GetICheckData(p, dwADData, dwMeterData);

					diffAd = dwSetData - dwADData;
					if( fabs(diffAd) > pCalPoint->m_dIADAccuracy[r] ) 
					{
						bOverSpec = TRUE;
					}
					diffMeter = dwSetData - dwMeterData;
					if( fabs(diffMeter) > pCalPoint->m_dIDAAccuracy[r] ) 
					{
						bOverSpec = TRUE;
					}
				}
			}
		}
	}
	return bOverSpec;
}

BOOL CCalibratorDlg::CheckEditedCh()
{
	BOOL bEdited = FALSE;
	for(int row=0; row<m_wndCalData.GetRowCount(); row++)
	{
		int nCh = atoi(m_wndCalData.GetValueRowCol(row+1, _MON_CH_COL_));		//monitoring ch
		CChCaliData *pChCalData = m_calibrtaionData.GetChCalData(nCh-1);
		if(pChCalData)
		{
			if(pChCalData->GetState() == CAL_DATA_MODIFY)
			{
				bEdited = TRUE;
				break;
			}
		}
	}
	return bEdited;
}

void CCalibratorDlg::SetCalState(int nState)
{
	m_nState = nState;
	CString strTemp;
	switch(nState)
	{
	case CAL_STATE_LINEOFF:
		strTemp = TEXT_LANG[61];//"통신 두절"
		GetDlgItem(IDC_STOP_BTN)->EnableWindow(FALSE);
		GetDlgItem(IDC_PAUSE_BTN)->EnableWindow(FALSE);
		GetDlgItem(IDC_RESUME_BTN)->EnableWindow(FALSE);

		GetDlgItem(IDC_CALI_DATA_UPDATE)->EnableWindow(FALSE);	
		GetDlgItem(IDC_CALI_START_BTN)->EnableWindow(FALSE);
		GetDlgItem(IDC_BTN_SET_POINT)->EnableWindow(FALSE);		
		break;
	case CAL_STATE_IDLE :
		strTemp = TEXT_LANG[62];//"대기중..."
		GetDlgItem(IDC_STOP_BTN)->EnableWindow(FALSE);
		GetDlgItem(IDC_PAUSE_BTN)->EnableWindow(FALSE);
		GetDlgItem(IDC_RESUME_BTN)->EnableWindow(FALSE);

		GetDlgItem(IDC_CALI_DATA_UPDATE)->EnableWindow(CheckEditedCh());	
		GetDlgItem(IDC_CALI_START_BTN)->EnableWindow(TRUE);
		GetDlgItem(IDC_BTN_SET_POINT)->EnableWindow(TRUE);		
		break;
	case CAL_STATE_RUN :
		strTemp = TEXT_LANG[63];		//"작업중"
		GetDlgItem(IDC_STOP_BTN)->EnableWindow(TRUE);
		GetDlgItem(IDC_PAUSE_BTN)->EnableWindow(TRUE);
		GetDlgItem(IDC_RESUME_BTN)->EnableWindow(FALSE);
		GetDlgItem(IDC_CALI_DATA_UPDATE)->EnableWindow(FALSE);
		
		GetDlgItem(IDC_CALI_START_BTN)->EnableWindow(FALSE);
		GetDlgItem(IDC_BTN_SET_POINT)->EnableWindow(FALSE);		
		break;
	case CAL_STATE_PAUSE:	
		strTemp = TEXT_LANG[64];	//"잠시멈춤"
		GetDlgItem(IDC_STOP_BTN)->EnableWindow(TRUE);
		GetDlgItem(IDC_PAUSE_BTN)->EnableWindow(FALSE);
		GetDlgItem(IDC_RESUME_BTN)->EnableWindow(TRUE);
		GetDlgItem(IDC_CALI_DATA_UPDATE)->EnableWindow(CheckEditedCh());
		
		GetDlgItem(IDC_CALI_START_BTN)->EnableWindow(FALSE);
		GetDlgItem(IDC_BTN_SET_POINT)->EnableWindow(FALSE);		
		break;
	}
	GetDlgItem(IDC_STATIC_STATE)->SetWindowText(strTemp);
	m_wndCalState.SetBkColor(RGB(255,255,255));
}

//mapping 정보에서 선택 Board의 Channel 수를 Count 한다.
int CCalibratorDlg::GetHWChCnt(int nBoardNo)
{
	//default
	EP_MD_SYSTEM_DATA *pSysData = ::EPGetModuleSysData(EPGetModuleIndex(m_nUnitNo));
	int nTotch = 0;

	if(nBoardNo == 0)
	{
		nTotch = pSysData->wChannelPerBoard;
	}
	else
	{
		nTotch = m_calibrtaionData.GetHWChCount(nBoardNo);
	}

	if(nTotch < 0 || nTotch > EP_MAX_CH_PER_BD)
		nTotch = EP_MAX_CH_PER_BD;
	
	return nTotch;
}

int CCalibratorDlg::GetTotalCalCount(EP_CAL_SELECT_INFO &selInfo)
{
// 	int nCnt = 1;
// 	if(m_nCalType == CAL_TYPE_CALCHECK)	nCnt *= 2;
// 
// 	if(selInfo.nType == CAL_VOLTAGE_CURRENT)		nCnt *= 2;
// 	if(selInfo.nMain == CAL_ADC_DAC)				nCnt += 2;			//main DA or CH
// 
// 	if(selInfo.nRange == 0)							nCnt *= 
// 	selInfo.nBoardNo = dlg.n_board;
// 
// 	int chCnt = GetHWChCnt(selInfo.nBoardNo);
// 	memset(selInfo.byCnSel, 1, chCnt);	//default 모든 CH
	return 0;
}

void CCalibratorDlg::DisplayChNoColumn()
{
	//Baord 별 색상을 다르게 표시 
	BOOL bLock = m_wndCalData.LockUpdate();
	CString strTemp;
	long nCh = 0;
	for(int row = _FROZEN_ROW_CNT_+1; row <=m_wndCalData.GetRowCount(); row++)
	{
		if(nCh >= 0 && nCh < EP_MAX_CH_PER_MD)
		{
			int nBoard, nChNo;
			m_calibrtaionData.m_ChCaliData[nCh].GetHWChNo(nBoard, nChNo);
			strTemp.Format("%02d_%02d", nBoard, nChNo);	

			if(nBoard > 0 && nBoard <= EP_MAX_BD_PER_MD)
			{
				m_wndCalData.SetStyleRange(CGXRange().SetCells(row, _HW_CH_COL), CGXStyle().SetValue(strTemp).SetInterior(m_boardColor[nBoard-1]));
			}
		}
		m_wndCalData.SetStyleRange(CGXRange().SetCells(row, _MON_CH_COL_), CGXStyle().SetValue(++nCh).SetInterior(RGB(255,255,255)));
	}
	m_wndCalData.LockUpdate(bLock);
	m_wndCalData.Redraw();
}


void CCalibratorDlg::PostNcDestroy() 
{
	// TODO: Add your specialized code here and/or call the base class
	delete this;
	CDialog::PostNcDestroy();
}

void CCalibratorDlg::OnOptTrayType1() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
}

void CCalibratorDlg::OnOptTrayType2() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
}
