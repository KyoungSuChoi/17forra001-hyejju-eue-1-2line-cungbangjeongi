#pragma once

//////////////////////////////////////////////////////////////////////////

static INT g_iDCIR_TRAYID_NOTIFY_Map[] = 
{
	3
	, 7
	, 0
};

typedef struct _st_DCIR_TRAYID_NOTIFY
{
	CHAR Equipment_Num[3+1];
	CHAR Tray_ID[7+1];
}st_DCIR_TRAYID_NOTIFY;


static INT g_iDCIR_TRAYID_RESPONSE_Map[] = 
{
	3
	, 7
	, 0
};

typedef struct _st_DCIR_TRAYID_RESPONSE
{
	CHAR Equipment_Num[3+1];
	CHAR Tray_ID[7+1];
}st_DCIR_TRAYID_RESPONSE;

// 입고 가능 장비 요구	// 0301
// 입고 예약			// 0302
typedef struct _st_DCIR_Cell_ID
{
	CHAR Cell_ID[16+1];
}st_DCIR_Cell_ID;

static INT g_iDCIR_INRESEVE_F_Map[] = 
{
	3
	, 3
	, 2
	, 20
	, 7
	, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16
	, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16
	, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16
	, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16	
	, 1
	, 3
	, 0
};

typedef struct _st_DCIR_INRESEVE_F
{
	CHAR MachinID[3+1];
	CHAR Type[3+1];
	CHAR Process_No[2+1];
	CHAR Batch_No[20+1];
	CHAR Tray_ID[7+1];
	st_DCIR_Cell_ID arCell_ID[MAX_CELL_COUNT];	
	CHAR Tray_Type[1+1];
	CHAR Total_Step[3+1];

}st_DCIR_INRESEVE_F;
typedef enum tag_DCIR_INRESEVE_F
{
	DCIR_MACHINID
	, DCIR_TYPE
	, DCIR_PROCESS_NO
	, DCIR_BATCH_NO
	, DCIR_TRAY_ID
	, DCIR_ARCELL_ID	
	, DCIR_TRAY_TYPE
	, DCIR_TOTAL_STEP
	, DCIR_CIF_END

}DCIR_INRESEVE_F;
static const TCHAR g_str_DCIR_INRESEVE_F[DCIR_CIF_END][32] = 
{
	TEXT("MachinID")
	, TEXT("Type")
	, TEXT("Process_No")
	, TEXT("Batch_No")
	, TEXT("Tray_ID")
	, TEXT("arCell_ID")	
	, TEXT("Tray_Type")
	, TEXT("Total_Step")
};
//////////////////////////////////////////////////////////////////////////


//Step 갯수변화
static INT g_iDCIR_Step_Set_Map[] = 
{
	3
	, 1
	, 6
	, 5
	, 5
	, 6
	, 6
	, 6
	, 5
	, 5
	, 0
};
typedef struct _st_DCIR_Step_Set
{
	CHAR Step_ID[3+1];
	CHAR Step_Type[1+1];
	CHAR Current[6+1];
	CHAR Volt[5+1];
	CHAR Time[5+1];
	CHAR CutOff[6+1];
	CHAR CutOff_SOC[6+1];
	CHAR CutOff_A[6+1];
	CHAR GetTime_Setting[5+1];
	CHAR Step_LimitChkTime[5+1];
}st_DCIR_Step_Set;

typedef enum tag_DCIR_Step_Set
{
	DCIR_STEP_ID
	, DCIR_STEP_TYPE
	, DCIR_CURRENT
	, DCIR_VOLT
	, DCIR_TIME
	, DCIR_CUTOFF
	, DCIR_CUTOFF_SOC
	, DCIR_CUTOFF_A
	, DCIR_GETTIME_T
	, DCIR_SS_END

}DCIR_Step_Set;
static const TCHAR g_str_DCIR_Step_Set[DCIR_SS_END][32] = 
{
	TEXT("Step_ID")
	, TEXT("Step_Type")
	, TEXT("Current")
	, TEXT("Volt")
	, TEXT("Time")
	, TEXT("CutOff")
	, TEXT("CutOff_SOC")
	, TEXT("CutOff_A")
	, TEXT("GetTime_T")
};
//////////////////////////////////////////////////////////////////////////

static INT g_iDCIR_Protect_Limit_Set_Map[] = 
{
	5
	, 5
	, 6
	, 6
	, 6
	, 6

	, 5
	, 5
	, 6
	, 6
	, 6
	, 6

	, 5
	, 5
	, 4

	, 5
	, 3
	, 5
	, 6
	, 3
	, 6

	, 5
	, 5
	, 0
};

typedef struct _st_DCIR_Protect_Limit_Set
{
	CHAR Charge_H_Vol[5+1];
	CHAR Charge_L_Vol[5+1];
	CHAR Charge_H_Cur[6+1];
	CHAR Charge_L_Cur[6+1];
	CHAR Charge_H_Cap[6+1];
	CHAR Charge_L_Cap[6+1];

	CHAR DisCharge_H_Vol[5+1];
	CHAR DisCharge_L_Vol[5+1];
	CHAR DisCharge_H_Cur[6+1];
	CHAR DisCharge_L_Cur[6+1];
	CHAR DisCharge_H_Cap[6+1];
	CHAR DisCharge_L_Cap[6+1];//68

	CHAR Charge_H_Time_Vol[5+1];		//78
	CHAR Charge_L_Time_Vol[5+1];		//83
	CHAR Charge_Vol_Get_Time[4+1];		//88


	CHAR reserved_1[5+1];	// CHAR Charge_Comp_CC_Vol[5+1];		
	CHAR reserved_2[3+1]; // CHAR Charge_Comp_CC_Time[3+1];		
	CHAR reserved_3[5+1]; // CHAR Charge_Comp_CC_DeltaVol[5+1];
	CHAR reserved_4[6+1];	// CHAR Charge_Comp_CV_Cur[6+1];
	CHAR reserved_5[3+1]; // CHAR Charge_Comp_CV_Time[3+1];
	CHAR reserved_6[6+1];	// CHAR Charge_Comp_Cv_DeltaCur[6+1];

	CHAR OCV_H_Vol[5+1];
	CHAR OCV_L_Vol[5+1];
}st_DCIR_Protect_Limit_Set;

static INT g_iDCIR_Contact_Set_Map[] = 
{
	6
	, 2
	, 5
	, 6
	, 5
	, 5
	, 21
	, 0
};
typedef struct _st_DCIR_Contact_Set
{
	CHAR Charge_Cur[6+1];
	CHAR Charge_Time[2+1];
	CHAR Inverse_Vol[5+1];
	CHAR Contact_Reg[6+1];
	CHAR Upper_Vol_Check[5+1];	// 전압 상한치 확인
	CHAR Lower_Vol_Check[5+1];	// 전압 하한치 확인
	CHAR Reserve[21+1];

}st_DCIR_Contact_Set;

typedef enum tag_DCIR_Contact_Set
{
	DCIR_CHARGE_CUR
	, DCIR_CHARTE_TIME
	, DCIR_INVERSE_VOL
	, DCIR_CONTACT_REG
	, DCIR_CS_RESERVE
	, DCIR_CS_END

}DCIR_Contact_Set;
static const TCHAR g_str_DCIR_Contact_Set[DCIR_CS_END][32] = 
{
	TEXT("Charge_Cur")
	, TEXT("Charge_Time")
	, TEXT("Inverse_Vol")
	, TEXT("Contact_Reg")
	, TEXT("CS_Reserve")
};
//////////////////////////////////////////////////////////////////////////

static INT g_iDCIR_Setting_Map[] = 
{
	4
	, 5
	, 11
	, 0
};

typedef struct _st_DCIR_Setting
{
	CHAR Reference_Temp[4+1];
	CHAR Resistance_Change[5+1];	
	CHAR Reserve[11+1];
}st_DCIR_Setting;

typedef enum tag_DCIR_Setting
{
	DCIR_Reference_Temp
	, DCIR_Resistance_Change
	, DCIR_S_RESERVE
	, DCIR_S_END
}DCIR_Setting;

static const TCHAR g_str_DCIR_Setting[DCIR_S_END][32] = 
{
	TEXT("Reference_Temp")
	, TEXT("Resistance_Change")
	, TEXT("S_Reserve")
};

//////////////////////////////////////////////////////////////////////////

static INT g_iDCIR_Cell_Info_Map[] = 
{
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1
	, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
	, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
	, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1	
	, 0
};

typedef struct _st_DCIR_CELL_INFO
{
	CHAR Cell_Info[1+1];
}st_DCIR_CELL_INFO;

static INT g_iDCIR_INRESEVE_B_Map[] = 
{
	5
	, 5
	, 6
	, 6
	, 6
	, 6

	, 5
	, 5
	, 6
	, 6
	, 6
	, 6

	, 5
	, 5
	, 4

	, 5
	, 3
	, 5
	, 6
	, 3
	, 6
	, 5
	, 5

	, 6
	, 2
	, 5
	, 6
	, 5
	, 5
	, 21
	, 4
	, 5
	, 11
	, 3
	, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
	, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
	, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
	, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1	
	, 0
};
typedef struct _st_DCIR_INRESEVE_B
{
	st_DCIR_Protect_Limit_Set Protect;
	st_DCIR_Contact_Set Contact;
	st_DCIR_Setting Setting;
	CHAR Contact_Error_Setting[3+1];		
	st_DCIR_CELL_INFO arCellInfo[MAX_CELL_COUNT];
}st_DCIR_INRESEVE_B;

typedef struct _st_DCIR_INRESEVE
{
	st_DCIR_INRESEVE_F F_Value;
	st_DCIR_Step_Set *Step_Info;
	st_DCIR_INRESEVE_B B_Value;

}st_DCIR_INRESEVE;

// 입고 완료 통지		// 0303
static INT g_iDCIR_IN_Map[] = 
{
	3
	, 7
	, 0
};

typedef struct _st_DCIR_IN
{
	CHAR Equipment_Num[3+1];
	CHAR Tray_ID[7+1];
}st_DCIR_IN;

// 검사 종료 응답 // 0304
static INT g_iDCIR_WORKEND_RESPONSE_Map[] = 
{
	3
	, 7
	, 0
};

typedef struct _st_DCIR_WORKEND_RESPONSE
{
	CHAR Equipment_Num[3+1];
	CHAR Tray_ID[7+1];
}st_DCIR_WORKEND_RESPONSE;
// 결과 FILE 요구 // 0305

static INT g_iDCIR_RESULT_Map[] = 
{
	3
	, 7
	, 0
};

typedef struct _st_DCIR_RESULT
{
	CHAR Equipment_Num[3+1];
	CHAR Tray_ID[7+1];
}st_DCIR_RESULT;

// 결과 FILE 수신 확인 통지 // 0306
static INT g_iDCIR_RESULT_RECV_Map[] = 
{
	3
	, 1
	, 7
	, 0
};

typedef struct _st_DCIR_RESULT_RECV
{
	CHAR Equipment_Num[3+1];
	CHAR Notify_Mode[1+1];
	CHAR Tray_ID[7+1];
}st_DCIR_RESULT_RECV;

// 출고 완료 통지 // 0307
static INT g_iDCIR_OUT_Map[] = 
{
	3
	, 7
	, 0
};

typedef struct _st_DCIR_OUT
{
	CHAR Equipment_Num[3+1];
	CHAR Tray_ID[7+1];
}st_DCIR_OUT;

// Mode 변경 요구 // 0308
static INT g_iDCIR_MODE_CHANGE_Map[] = 
{
	3
	, 1
	, 0
};

typedef struct _st_DCIR_MODE_CHANGE
{
	CHAR Equipment_Num[3+1];
	CHAR Equipment_State[1+1];
}st_DCIR_MODE_CHANGE;

// 설비 상태 통지 응답 // 0109
static INT g_iDCIR_STATE_RESPONSE_Map[] = 
{
	3
	, 0
};

typedef struct _st_DCIR_STATE_RESPONSE
{
	CHAR Equipment_Num[3+1];
}st_DCIR_STATE_RESPONSE;

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// 입고 가능 장비 응답 // 1301
typedef struct _st_DCIR_IMPOSSBLE_STATE
{
	CHAR imState[2+1];
}st_DCIR_IMPOSSBLE_STATE;

static INT g_iDCIR_IMPOSSBLE_RESPONSE_Map[] = 
{
	2, 2
	,0
};

typedef struct _st_DCIR_IMPOSSBLE_RESPONSE
{
	st_DCIR_IMPOSSBLE_STATE EQ_State[MAX_SBC_DCIR_COUNT];
}st_DCIR_IMPOSSBLE_RESPONSE;
// 입고 예약 응답 // 1302
static INT g_iDCIR_INRESEVE_RESPONSE_Map[] = 
{
	3
	, 7
	, 0
};

typedef struct _st_DCIR_INRESEVE_RESPONSE
{
	CHAR Equipment_Num[3+1];
	CHAR Tray_ID[7+1];
}st_DCIR_INRESEVE_RESPONSE;

// 입고 완료 응답 // 1103
static INT g_iDCIR_IN_RESPONSE_Map[] = 
{
	3
	, 7
	, 0
};

typedef struct _st_DCIR_IN_RESPONSE
{
	CHAR Equipment_Num[3+1];
	CHAR Tray_ID[7+1];
}st_DCIR_IN_RESPONSE;

// 검사 종료 통지 // 1304
static INT g_iDCIR_WORKEND_Map[] = 
{
	3
	, 7
	, 0
};

typedef struct _st_DCIR_WORKEND
{
	CHAR Equipment_Num[3+1];
	CHAR Tray_ID[7+1];
}st_DCIR_WORKEND;

//결과파일 전송 //1305


// 결과 FILE 수신 확인 응답 // 1306
static INT g_iDCIR_RESULT_RECV_RESPONSE_map[] = 
{
	3
	, 1
	, 7
	, 0
};

typedef struct _st_DCIR_RESULT_RECV_RESPONSE
{
	CHAR Equipment_Num[3+1];
	CHAR Notify_Mode[1+1];
	CHAR Tray_ID[7+1];
}st_DCIR_RESULT_RECV_RESPONSE;

// 출고 완료 응답 // 1307
static INT g_iDCIR_OUT_RESPONSE_Map[] = 
{
	3
	, 0
};
typedef struct _st_DCIR_OUT_RESPONSE
{
	CHAR Equipment_Num[3+1];
}st_DCIR_OUT_RESPONSE;

// Mode 변경 응답 // 1308
static INT g_iDCIR_MODE_CHANGE_RESPONSE_Map[] = 
{
	3
	, 0
};

typedef struct _st_DCIR_MODE_CHANGE_RESPONSE
{
	CHAR Equipment_Num[3+1];
}st_DCIR_MODE_CHANGE_RESPONSE;

// 설비 상태 통지 // 1309
static INT g_iDCIR_STATE_Map[] = 
{
	3
	, 2
	, 0
};

typedef struct _st_DCIR_STATE
{
	CHAR Equipment_Num[3+1];
	CHAR Equipment_State[2+1];
}st_DCIR_STATE;

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// 결과 FILE 응답 - Stage No ~ Total Step 까지 // 1305

// DC-IR 결과 - DC-IR
static INT g_iDCIR_DC_IR_Map[] = 
{
	6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6	
	, 0
};

typedef struct _st_DCIR_DC_IR
{
	CHAR DC_IR[6+1]; // ##.### m옴
}st_DCIR_DC_IR;
//////////////////////////////////////////////////////////////////////////

// DC-IR 결과 - V1
static INT g_iDCIR_V1_Map[] = 
{
	5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5	
	, 0
};
typedef struct _st_DCIR_V1
{
	CHAR V1[5+1]; // mV
}st_DCIR_V1;
//////////////////////////////////////////////////////////////////////////

// DC-IR 결과 - V1
static INT g_iDCIR_V2_Map[] = 
{
	5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5	
	, 0
};
typedef struct _st_DCIR_V2
{
	CHAR V2[5+1];  // mV
}st_DCIR_V2;
//////////////////////////////////////////////////////////////////////////

// DC-IR 결과 - 방전 시간
static INT g_iDCIR_Discharge_Time_Map[] = 
{
	5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5	
	, 0
};
typedef struct _st_DCIR_Discharge_Time
{
	CHAR Discharge_Time[5+1];  //Sec
}st_DCIR_Discharge_Time;
//////////////////////////////////////////////////////////////////////////

// DC-IR 결과 - 종지 전류값
static INT g_iDCIR_CutOff_Map[] = 
{
	6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 0
};
typedef struct _st_DCIR_CutOff
{
	CHAR CutOff[6+1]; // mA
}st_DCIR_CutOff;
//////////////////////////////////////////////////////////////////////////

// DC-IR 결과 - 평균 온도
static INT g_iDCIR_Age_temp_Map[] = 
{
	5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 0
};
typedef struct _st_DCIR_Age_temp
{
	CHAR Age_temp[5+1];  // ###.# C
}st_DCIR_Age_temp;
//////////////////////////////////////////////////////////////////////////


// Result Data 1 ~ 7 까지
static INT g_iDCIR_Result_File_Response_L_Map[] = 
{
	3
	, 3
	, 2
	, 20
	, 7
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6	

	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5

	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5

	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6

	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 3
	, 0
};

typedef struct _st_DCIR_RESULT_FILE_RESPONSE_L
{
	CHAR Stage_No[3+1];
	CHAR Type[3+1];
	CHAR Process_ID[2+1];
	CHAR Batch_No[20+1];
	CHAR Tray_ID[7+1];
	
	st_DCIR_DC_IR			DC_IR[MAX_CELL_COUNT];
	st_DCIR_V1				V1[MAX_CELL_COUNT];
	st_DCIR_V2				V2[MAX_CELL_COUNT];
	st_DCIR_Discharge_Time	Discharge_Time[MAX_CELL_COUNT];
	st_DCIR_CutOff			CutOff[MAX_CELL_COUNT];
	st_DCIR_Age_temp		Age_temp[MAX_CELL_COUNT];

	CHAR Total_Step[3+1];

}st_DCIR_RESULT_FILE_RESPONSE_L;
//////////////////////////////////////////////////////////////////////////

// Step 전압값
static INT g_iDCIR_Voltage_Worth_Map[] = 
{
	5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 0
};
typedef struct _st_DCIR_VOLTAGE_WORTH_RESPONSE
{
	CHAR Voltage_Worth[5+1];  // mV
}st_DCIR_VOLTAGE_WORTH_RESPONSE;
//////////////////////////////////////////////////////////////////////////

// Step 용량값
static INT g_iDCIR_Capacity_Worth_Map[] = 
{
	6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 0
};

typedef struct _st_DCIR_CAPACITY_WORTH_RESPONSE
{
	CHAR Capacity_Worth[6+1]; // mAh
}st_DCIR_CAPACITY_WORTH_RESPONSE;
//////////////////////////////////////////////////////////////////////////

// Step 종지 전류값(discharge)
static INT g_iDCIR_Discharge_Current_Worth_Map[] = 
{
	6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
	, 0
};
typedef struct _st_DCIR_DISCHARGE_CURRENT_WORTH_RESPONSE
{
	CHAR Discharge_Current_Worth[6+1];  // mV
}st_DCIR_DISCHARGE_CURRENT_WORTH_RESPONSE;
//////////////////////////////////////////////////////////////////////////

// Step 종료 시간
static INT g_iDCIR_Step_End_Time_Map[] = 
{
	5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
	, 0
};
typedef struct _st_DCIR_STEP_END_TIME_RESPONSE
{
	CHAR Step_End_Time[5+1];
}st_DCIR_STEP_END_TIME_RESPONSE;
//////////////////////////////////////////////////////////////////////////

static INT g_iDCIR_Result_R_Step_No_Map[] = 
{
	3
	, 0
};
//////////////////////////////////////////////////////////////////////////

static INT g_iDCIR_Result_R_Action_Map[] = 
{
	1
	, 0
};
//////////////////////////////////////////////////////////////////////////

static INT g_iDCIR_Result_R_Temp_Map[] =
{
	4
	, 0
};

typedef struct _st_DCIR_Result_Step_Set
{
	CHAR R_Step_No[3+1];
	CHAR R_Action[1+1];

	CHAR R_JigTemp1[4+1];
	CHAR R_JigTemp2[4+1];
	CHAR R_JigTemp3[4+1];
	CHAR R_JigTemp4[4+1];
	CHAR R_JigTemp5[4+1];
	CHAR R_JigTemp6[4+1];
	CHAR R_JigTempMax[4+1];
	CHAR R_JigTempMin[4+1];
	CHAR R_JigTempAvg[4+1];

	st_DCIR_VOLTAGE_WORTH_RESPONSE  R_Voltage_Worth[MAX_CELL_COUNT];	
	st_DCIR_CAPACITY_WORTH_RESPONSE  R_Capacity_Worth[MAX_CELL_COUNT];
	st_DCIR_DISCHARGE_CURRENT_WORTH_RESPONSE  R_Discharge_Current_Worth[MAX_CELL_COUNT];
	st_DCIR_STEP_END_TIME_RESPONSE  R_Step_End_Time[MAX_CELL_COUNT];
}st_DCIR_Result_Step_Set;

// 결과 File 응답 Cell_Info
static INT g_iDCIR_Result_File_Response_Cell_Info_Map[] = 
{
	2, 2, 2, 2, 2, 2, 2, 2, 2, 2
	, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
	, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
	, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
	,0
};

typedef struct _st_DCIR_RESULT_FILE_RESPONSE_CELL_INFO
{
	CHAR Cell_Info[2+1];
}st_DCIR_RESULT_FILE_RESPONSE_CELL_INFO;

// 결과 FILE 응답 - 현재 진행 Step 번호 ~ 검사 종료 일시 까지 // 1105
static INT g_iDCIR_Result_File_Response_R_Map[] = 
{
	3
	, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
	, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
	, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
	, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
	, 14
	, 14
	, 0
};

typedef struct _st_DCIR_RESULT_FILE_RESPONSE_R
{
	CHAR Now_Step_No[3+1];
	st_DCIR_RESULT_FILE_RESPONSE_CELL_INFO arCellInfo[MAX_CELL_COUNT];
	CHAR Test_Begin_Date[14+1];
	CHAR Test_Finish_Date[14+1];

}st_DCIR_RESULT_FILE_RESPONSE_R;

//////////////////////////////////////////////////////////////////////////