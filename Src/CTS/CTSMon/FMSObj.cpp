#include "StdAfx.h"
#include "CTSMon.h"

#include "FMSGlobal.h"
#include "FMSLog.h"
#include "FMSCriticalSection.h"
#include "FMSSyncParent.h"
#include "FMSStaticSyncParent.h"
#include "FMSMemoryPool.h"
#include "FMSCircularQueue.h"

#include "FMSPacketBox.h"

#include "FMSNetObj.h"
#include "FMSIocp.h"

#include "FMSObj.h"

CFMSObj::CFMSObj() 
{
	CFMSNetObj::SetOwnerNetObj(this);
	////////////////////////////////////////////////////////////////////////////
	//CHAR* temp = "10595502                    SPNE01PNE1000000000001PNE1000000000002PNE1000000000003PNE1000000000004PNE1000000000005PNE1000000000006PNE1000000000007PNE1000000000008PNE1000000000009PNE1000000000010PNE1000000000011PNE1000000000012PNE1000000000013PNE1000000000014PNE1000000000015PNE1000000000016                                                                                                                                                                                                                                                                1  002    13     0    0    1     0     0     0  21   123 2500    3   100     1     3 425050000  2500     0  2500   180  100 4210     1     0     0     0    1          10010 2500 1.234                               1  1  00000000000000002222222222222222";
	//fnSaveWorkInfo((BYTE*)temp, strlen(temp));
}

CFMSObj::~CFMSObj()
{

}

INT CFMSObj::strLinker(CHAR *msg, VOID * _dest, INT* mesmap)
{
	CHAR *stTemp = (CHAR *)_dest;

	UINT totlink = 0;
	UINT totidx = 0;
	UINT i = 0;
	while(mesmap[i])
	{
		INT iLink = mesmap[i++];
		memcpy(msg + totlink, stTemp + totidx, iLink); 

		totidx += iLink + 1;
		totlink += iLink;
	}

	return totidx;
}

INT CFMSObj::strCutter(CHAR *msg, VOID * _dest, INT* mesmap)
{
	CHAR *stTemp = (CHAR *)_dest;

	UINT totcut = 0;
	UINT totidx = 0;
	UINT szEndIdx = 0;
	UINT i = 0;
	while(mesmap[i])
	{
		INT iCut = mesmap[i++];
		memcpy(stTemp + totcut, msg + totidx, iCut);

		totidx += iCut;
		totcut += iCut;
		stTemp[totcut] = 0;
		totcut++;
	}

	return totidx;
}

INT64 CFMSObj::fnSaveWorkInfo(st_FMSMSGHEAD&_hdr, BYTE *pReadBuf, DWORD dwLen)
{
	CFMSSyncObj FMSSync;
	//For test
	//CHAR Temp[9192] = {"14195502                    SPNE12PNEFMT1000000201PNEFMT1000000202PNEFMT1000000203PNEFMT1000000204PNEFMT1000000205PNEFMT1000000206PNEFMT1000000207PNEFMT1000000208PNEFMT1000000209PNEFMT1000000210PNEFMT1000000211PNEFMT1000000212PNEFMT1000000213PNEFMT1000000214PNEFMT1000000215PNEFMT1000000216                                                                                                                                                                                                                                                                0  008    13     0    0    1     0     0     0  21   250 4000    3   100     0  3000  32   250 2500    3  3000     0  3000  43     0    0    3     0     0     0  54     0    0    0     0     0     0  63     0    0    1     0     0     0  76   250 4000    3  3500     0  3000  87   250 4000    3  3500     0  3000 4380 3000 26000   200  3000     0 4000 2500 26000   200  3000     0    1          25010 1000 3.000                               1  1  00000000000000002222222222222222"};
	CHAR Temp[9192] = {0};
	memcpy(Temp, pReadBuf, dwLen);
	
	INT sqlerr = 0;
	CString strModuleId;

	CHAR _moduleID[4] = {0};
	memcpy(_moduleID, Temp, 3);
	strModuleId.Format("%s", _moduleID);

	INT Moduleid = atoi(strModuleId.Right(3));

	CString strSqliteDBPathNName = theApp.GetProfileString(FORM_PATH_REG_SECTION, "DataBase");
	CString dbname;
	dbname.Format("\\FMS_DB\\CTSMonDB_%s.db3", strModuleId.Right(3));
	strSqliteDBPathNName += dbname;

	sqlite3* mSqlite;

	sqlerr = sqlite3_open(strSqliteDBPathNName, &mSqlite);

	sqlite3_stmt * stmt = 0;

	sqlite3_prepare( mSqlite,  "REPLACE into FMSWorkInfo(\
							   idx \
							   , moduleID \
							   , F_Info \
							   , StepInfo \
							   , B_Info \
							   , Send_Complet \
							   ) values(?, ?, ?, ?, ?, ?);", -1, &stmt, 0 );

	COleDateTime OleRunStartTime = COleDateTime::GetCurrentTime();

	INT64 idx = OleRunStartTime.GetYear()	* 10000000000;
	idx += OleRunStartTime.GetMonth()		* 100000000;
	idx += OleRunStartTime.GetDay()			* 1000000;
	idx += OleRunStartTime.GetHour()		* 10000;
	idx += OleRunStartTime.GetMinute()		* 100;
	idx += OleRunStartTime.GetSecond();

	INT k = 1;
	sqlite3_bind_int64(stmt, k++, idx);
	//////////////////////////////////////////////////////////////////////////
	sqlite3_bind_int(stmt, k++, Moduleid);
	//////////////////////////////////////////////////////////////////////////
	st_CHARGER_INRESEVE_F _F;
	ZeroMemory(&_F, sizeof(st_CHARGER_INRESEVE_F));
	INT cutlen = strCutter(Temp, &_F, g_iCHARGER_INRESEVE_F_Map);

	sqlite3_bind_blob(stmt, k++, &_F
		, sizeof(st_CHARGER_INRESEVE_F)
		, SQLITE_TRANSIENT);
	//////////////////////////////////////////////////////////////////////////
	CHAR* pSrcStep = Temp + cutlen;
	INT stepCut = 0;
	INT istep_tot = atoi(_F.Total_Step);
	st_Step_Set *Step_Info = new st_Step_Set[istep_tot];
	ZeroMemory(Step_Info, sizeof(st_Step_Set) * istep_tot);
	for (INT i = 0; i < istep_tot; i++)
	{
		stepCut += strCutter(pSrcStep + stepCut
			, &Step_Info[i]
			, g_iStep_Set_Map);
	}

	sqlite3_bind_blob(stmt, k++, Step_Info
		, sizeof(st_Step_Set) * istep_tot
		, SQLITE_TRANSIENT);
	cutlen += stepCut;
	//////////////////////////////////////////////////////////////////////////
	st_CHARGER_INRESEVE_B _B;
	ZeroMemory(&_B, sizeof(st_CHARGER_INRESEVE_B));
	cutlen += strCutter(Temp + cutlen, &_B, g_iCHARGER_INRESEVE_B_Map);

	sqlite3_bind_blob(stmt, k++, &_B
		, sizeof(st_CHARGER_INRESEVE_B)
		, SQLITE_TRANSIENT);

	sqlite3_bind_int(stmt, k++, RS_NONE);

	sqlerr = sqlite3_step(stmt);

	if(SQLITE_DONE != sqlerr)
	{
		LPSTR szError = (LPSTR)sqlite3_errmsg(mSqlite);
		CFMSLog::WriteLog(szError);

		int nRowsChanged = sqlite3_changes(mSqlite);

		sqlerr = sqlite3_reset(stmt);

		if (sqlerr != SQLITE_OK)
		{
			szError = (LPSTR)sqlite3_errmsg(mSqlite);
			CFMSLog::WriteLog(szError);
		}
	}

	sqlite3_finalize(stmt);
	sqlite3_close(mSqlite);

	return idx;
}

INT64 CFMSObj::fnSaveWorkInfo(BYTE *pReadBuf, DWORD dwLen)
{
	CFMSSyncObj FMSSync;
	
	CHAR Temp[9192] = {0};
	memcpy(Temp, pReadBuf, dwLen);

	INT sqlerr = 0;

	CHAR _moduleID[4] = {0};
	memcpy(_moduleID, Temp, 3);

	CString strModuleId;
	strModuleId.Format("%s", _moduleID);

	INT Moduleid = atoi(strModuleId.Right(3));

	CString strSqliteDBPathNName = theApp.GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");
	CString dbname;
	dbname.Format("\\FMS_DB\\CTSMonDB_%s.db3", strModuleId.Right(3));
	strSqliteDBPathNName += dbname;

	sqlite3* mSqlite;

	sqlerr = sqlite3_open(strSqliteDBPathNName, &mSqlite);

	sqlite3_stmt * stmt = 0;

	sqlite3_prepare( mSqlite,  "REPLACE into FMSWorkInfo(\
							   idx \
							   , moduleID \
							   , F_Info \
							   , StepInfo \
							   , B_Info \
							   , Send_Complet \
							   ) values(?, ?, ?, ?, ?, ?);", -1, &stmt, 0 );


	COleDateTime OleRunStartTime = COleDateTime::GetCurrentTime();

	INT64 idx = OleRunStartTime.GetYear()	* 10000000000;
	idx += OleRunStartTime.GetMonth()		* 100000000;
	idx += OleRunStartTime.GetDay()			* 1000000;
	idx += OleRunStartTime.GetHour()		* 10000;
	idx += OleRunStartTime.GetMinute()		* 100;
	idx += OleRunStartTime.GetSecond();

	INT k = 1;
	sqlite3_bind_int64(stmt, k++, idx);
	//////////////////////////////////////////////////////////////////////////
	sqlite3_bind_int(stmt, k++, Moduleid);
	//////////////////////////////////////////////////////////////////////////
	st_CHARGER_INRESEVE_F _F;
	ZeroMemory(&_F, sizeof(st_CHARGER_INRESEVE_F));
	INT cutlen = strCutter(Temp, &_F, g_iCHARGER_INRESEVE_F_Map);

	sqlite3_bind_blob(stmt, k++, &_F
		, sizeof(st_CHARGER_INRESEVE_F)
		, SQLITE_TRANSIENT);
	//////////////////////////////////////////////////////////////////////////
	CHAR* pSrcStep = Temp + cutlen;
	INT stepCut = 0;
	INT istep_tot = atoi(_F.Total_Step);
	st_Step_Set *Step_Info = new st_Step_Set[istep_tot];
	ZeroMemory(Step_Info, sizeof(st_Step_Set) * istep_tot);
	for (INT i = 0; i < istep_tot; i++)
	{
		stepCut += strCutter(pSrcStep + stepCut
			, &Step_Info[i]
		, g_iStep_Set_Map);
	}

	sqlite3_bind_blob(stmt, k++, Step_Info
		, sizeof(st_Step_Set) * istep_tot
		, SQLITE_TRANSIENT);
	cutlen += stepCut;
	//////////////////////////////////////////////////////////////////////////
	st_CHARGER_INRESEVE_B _B;
	ZeroMemory(&_B, sizeof(st_CHARGER_INRESEVE_B));
	cutlen += strCutter(Temp + cutlen, &_B, g_iCHARGER_INRESEVE_B_Map);

	sqlite3_bind_blob(stmt, k++, &_B
		, sizeof(st_CHARGER_INRESEVE_B)
		, SQLITE_TRANSIENT);

	sqlite3_bind_int(stmt, k++, 0);

	sqlerr = sqlite3_step(stmt);

	if(SQLITE_DONE != sqlerr)
	{
		LPSTR szError = (LPSTR)sqlite3_errmsg(mSqlite);
		CFMSLog::WriteLog(szError);

		int nRowsChanged = sqlite3_changes(mSqlite);

		sqlerr = sqlite3_reset(stmt);

		if (sqlerr != SQLITE_OK)
		{
			szError = (LPSTR)sqlite3_errmsg(mSqlite);
			CFMSLog::WriteLog(szError);
		}
	}

	sqlite3_finalize(stmt);
	sqlite3_close(mSqlite);

	return idx;
}

BOOL CFMSObj::fnSendResult(const CHAR* _Longdata)
{
	CFMSSyncObj FMSSync;

	BOOL _err = FALSE;

	INT iBufferlen = strlen(_Longdata);
	BYTE* pbySendBuffer = new BYTE[iBufferlen + 10];
	ZeroMemory(pbySendBuffer, iBufferlen + 10);

	memcpy(pbySendBuffer, _Longdata, strlen(_Longdata));

/*
	INT sendcount = 0;
	INT namuji = 0;
	if(sendcount = iBufferlen / MAX_BUFFER_LENGTH)
	{
		namuji = iBufferlen % MAX_BUFFER_LENGTH;
		INT i = 0;
		for(i  = 0; i < sendcount; i++)
		{
			Write(pbySendBuffer + (i * MAX_BUFFER_LENGTH), MAX_BUFFER_LENGTH);
		}
		
		Write(pbySendBuffer + (i * MAX_BUFFER_LENGTH), namuji);
	}
	else
	{
		Write(pbySendBuffer, iBufferlen);
	}
*/

	Write(pbySendBuffer, iBufferlen);

	delete [] pbySendBuffer;

	return TRUE;
}

INT64 CFMSObj::fnSaveDCIRWorkInfo(st_FMSMSGHEAD&_hdr, BYTE *pReadBuf, DWORD dwLen)
{
	CFMSSyncObj FMSSync;

	CHAR Temp[4096] = {0};
	memcpy(Temp, pReadBuf, dwLen);

	INT sqlerr = 0;

	CHAR _moduleID[4] = {0};
	memcpy(_moduleID, Temp, 3);
	INT Moduleid = atoi(_moduleID);
	
	CString strSqliteDBPathNName = theApp.GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");
	CString dbname;
	dbname.Format("\\FMS_DB\\CTSMonDB_%03d.db3", Moduleid);
	strSqliteDBPathNName += dbname;

	sqlite3* mSqlite;

	sqlerr = sqlite3_open(strSqliteDBPathNName, &mSqlite);

	sqlite3_stmt * stmt = 0;

	sqlite3_prepare( mSqlite,  "REPLACE into FMSWorkInfo(\
							   idx \
							   , moduleID \
							   , F_Info \
							   , StepInfo \
							   , B_Info \
							   , Send_Complet \
							   ) values(?, ?, ?, ?, ?, ?);", -1, &stmt, 0 );


	COleDateTime OleRunStartTime = COleDateTime::GetCurrentTime();

	INT64 idx = OleRunStartTime.GetYear()	* 10000000000;
	idx += OleRunStartTime.GetMonth()		* 100000000;
	idx += OleRunStartTime.GetDay()			* 1000000;
	idx += OleRunStartTime.GetHour()		* 10000;
	idx += OleRunStartTime.GetMinute()		* 100;
	idx += OleRunStartTime.GetSecond();

	INT k = 1;
	sqlite3_bind_int64(stmt, k++, idx);
	//////////////////////////////////////////////////////////////////////////
	sqlite3_bind_int(stmt, k++, Moduleid);
	//////////////////////////////////////////////////////////////////////////
	st_DCIR_INRESEVE_F _F;
	ZeroMemory(&_F, sizeof(st_DCIR_INRESEVE_F));
	INT cutlen = strCutter(Temp, &_F, g_iDCIR_INRESEVE_F_Map);

	sqlite3_bind_blob(stmt, k++, &_F
		, sizeof(st_DCIR_INRESEVE_F)
		, SQLITE_TRANSIENT);
	//////////////////////////////////////////////////////////////////////////
	CHAR* pSrcStep = Temp + cutlen;
	INT stepCut = 0;
	INT istep_tot = atoi(_F.Total_Step);
	st_DCIR_Step_Set *Step_Info = new st_DCIR_Step_Set[istep_tot];
	ZeroMemory(Step_Info, sizeof(st_DCIR_Step_Set) * istep_tot);
	for (INT i = 0; i < istep_tot; i++)
	{
		stepCut += strCutter(pSrcStep + stepCut
			, &Step_Info[i]
		, g_iDCIR_Step_Set_Map);
	}

	sqlite3_bind_blob(stmt, k++, Step_Info
		, sizeof(st_DCIR_Step_Set) * istep_tot
		, SQLITE_TRANSIENT);
	cutlen += stepCut;
	//////////////////////////////////////////////////////////////////////////
	st_DCIR_INRESEVE_B _B;
	ZeroMemory(&_B, sizeof(st_DCIR_INRESEVE_B));
	cutlen += strCutter(Temp + cutlen, &_B, g_iDCIR_INRESEVE_B_Map);

	sqlite3_bind_blob(stmt, k++, &_B
		, sizeof(st_DCIR_INRESEVE_B)
		, SQLITE_TRANSIENT);

	sqlite3_bind_int(stmt, k++, RS_NONE);

	sqlerr = sqlite3_step(stmt);

	if(SQLITE_DONE != sqlerr)
	{
		LPSTR szError = (LPSTR)sqlite3_errmsg(mSqlite);
		CFMSLog::WriteLog(szError);

		int nRowsChanged = sqlite3_changes(mSqlite);

		sqlerr = sqlite3_reset(stmt);

		if (sqlerr != SQLITE_OK)
		{
			szError = (LPSTR)sqlite3_errmsg(mSqlite);
			CFMSLog::WriteLog(szError);
		}
	}

	sqlite3_finalize(stmt);
	sqlite3_close(mSqlite);

	return idx;
}