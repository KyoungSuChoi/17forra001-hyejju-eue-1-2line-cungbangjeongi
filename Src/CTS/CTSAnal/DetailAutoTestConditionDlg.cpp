// DetailAutoTestConditionDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "DetailAutoTestConditionDlg.h"


// CDetailAutoTestConditionDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDetailAutoTestConditionDlg, CDialog)

bool CDetailAutoTestConditionDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CDetailAutoTestConditionDlg"), _T("TEXT_CDetailAutoTestConditionDlg_CNT"), _T("TEXT_CDetailAutoTestConditionDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CDetailAutoTestConditionDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CDetailAutoTestConditionDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}

CDetailAutoTestConditionDlg::CDetailAutoTestConditionDlg(CTestCondition *pTestCondition, CWnd* pParent /*=NULL*/)
	: CDialog(CDetailAutoTestConditionDlg::IDD, pParent)
	, m_fDCIR_RegTemp(0)
	, m_fDCIR_ResistanceRate(0)
	, m_nStepLimitCheckTime(0)
	, m_nChargeVoltageChkTime(0)
	, m_strChgCCVtg(_T(""))
	, m_strChgCCTime(_T(""))
	, m_strChgCCDeltaV(_T(""))
	, m_strChgCVCrt(_T(""))
	, m_strChgCVTime(_T(""))
	, m_strChgCVDeltaA(_T(""))
	, m_strVIGetTime(_T(""))
	, m_strOCVHighVtg(_T(""))
	, m_strOCVLowVtg(_T(""))
{
	LanguageinitMonConfig();

	m_pDoc = NULL;
	m_pTestCondition = pTestCondition;	
}

CDetailAutoTestConditionDlg::~CDetailAutoTestConditionDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

void CDetailAutoTestConditionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LABEL1, m_Label1);
	DDX_Control(pDX, IDC_LABEL2, m_Label2);
	DDX_Control(pDX, IDC_LABEL3, m_Label3);
	DDX_Control(pDX, IDC_LABEL_TYPE, m_LabelType);
	DDX_Control(pDX, IDC_LABEL_PROCESS, m_LabelProcess);
	DDX_Text(pDX, IDC_EDIT_REG_TEMP, m_fDCIR_RegTemp);
	DDX_Text(pDX, IDC_EDIT_RESISTANCE_RATE, m_fDCIR_ResistanceRate);
	DDX_Text(pDX, IDC_STEP_LIMIT_CHK_TIME, m_nStepLimitCheckTime);
	DDX_Text(pDX, IDC_CHARGE_VOLTAGE_CHK_TIME, m_nChargeVoltageChkTime);
	DDX_Text(pDX, IDC_CHARGE_FANOFF_CHK, m_strChargeFanOffChk);
	DDX_Text(pDX, IDC_COMP_CHG_CC_VTG, m_strChgCCVtg);
	DDX_Text(pDX, IDC_COMP_CHG_CC_TIME, m_strChgCCTime);
	DDX_Text(pDX, IDC_COMP_CHG_CC_DELTA_VTG, m_strChgCCDeltaV);
	DDX_Text(pDX, IDC_COMP_CHG_CV_CRT, m_strChgCVCrt);
	DDX_Text(pDX, IDC_COMP_CHG_CV_TIME, m_strChgCVTime);
	DDX_Text(pDX, IDC_COMP_CHG_CV_DELTA_CRT, m_strChgCVDeltaA);
	DDX_Text(pDX, IDC_VI_GET_TIME, m_strVIGetTime);
	DDX_Control(pDX, IDC_REPORT_TIME, m_ReportTime);
	DDX_Text(pDX, IDC_OCV_VTG_HIGH, m_strOCVHighVtg);
	DDX_Text(pDX, IDC_OCV_VTG_LOW, m_strOCVLowVtg);
}


BEGIN_MESSAGE_MAP(CDetailAutoTestConditionDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CDetailAutoTestConditionDlg::OnBnClickedOk)
	ON_MESSAGE(WM_GRID_MOVECELL, OnMovedCurrentCell)
END_MESSAGE_MAP()


// CDetailAutoTestConditionDlg 메시지 처리기입니다.

void CDetailAutoTestConditionDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnOK();
}

BOOL CDetailAutoTestConditionDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	InitLabel();
	InitFont();

	m_ReportTime.SetFormat("HH:mm:ss");
	m_ReportTime.SetTime(DATE(0));
	
	InitProtectSettingGrid();
	InitStepGrid();
	InitContactSettingGrid();
	
	DisplayCondition();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

LONG CDetailAutoTestConditionDlg::OnMovedCurrentCell(WPARAM wParam, LPARAM lParam)
{
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;
	if(pGrid == &m_StepGrid)
	{
		int nRow, nCol;
		nCol = LOWORD(wParam);
		nRow = HIWORD(wParam);

		if(nRow < 1 || nRow > m_pTestCondition->GetTotalStepNo())	
		{
			return 0;
		}

		CString strTemp;
		strTemp.Format("[ %d Step Setting ]", nRow);
		GetDlgItem(IDC_STATIC5)->SetWindowText(strTemp);
		DisplayStepGrid(nRow-1);
	}
	return 0;
}

VOID CDetailAutoTestConditionDlg::InitLabel()
{
	m_Label1.SetBkColor(RGB_MIDNIGHTBLUE)
		.SetTextColor(RGB_WHITE)
		.SetFontSize(22)
		.SetFontBold(TRUE)
		.SetText(TEXT_LANG[0]);//"공정"
		
	m_Label2.SetBkColor(RGB_MIDNIGHTBLUE)
		.SetTextColor(RGB_WHITE)
		.SetFontSize(22)
		.SetFontBold(TRUE)
		.SetText(TEXT_LANG[1]);//"타입"
		
	m_Label3.SetBkColor(RGB_MIDNIGHTBLUE)
		.SetTextColor(RGB_WHITE)
		.SetFontSize(22)
		.SetFontBold(TRUE)
		.SetText(TEXT_LANG[2]);	//"번호"
	
	m_LabelType.SetBkColor(RGB_BLACK)
		.SetTextColor(RGB_WHITE)
		.SetFontSize(22)
		.SetFontBold(TRUE)
		.SetText("-");
		
	m_LabelProcess.SetBkColor(RGB_BLACK)
		.SetTextColor(RGB_WHITE)
		.SetFontSize(22)
		.SetFontBold(TRUE)
		.SetText("-");
}


VOID CDetailAutoTestConditionDlg::DisplayStepGrid( int nStepIndex )
{
	CString strData = _T("");
	CStep *pStepInfo;
	STR_COMMON_STEP *pStep, stepData;
	STR_TOP_CONFIG *pConfig = m_pDoc->GetTopConfig();
	BYTE color;

	BOOL bLock = m_StepGrid.LockUpdate();

	int nStepRow = m_pTestCondition->GetTotalStepNo();

	if( nStepIndex == -1 )
	{
		if(m_StepGrid.GetRowCount() > 0)
		{
			m_StepGrid.RemoveRows(1, m_StepGrid.GetRowCount());
		}

		m_StepGrid.SetRowCount(nStepRow);

		for(int nRow =0 ; nRow < nStepRow; nRow++)
		{
			pStepInfo = m_pTestCondition->GetStep(nRow);
			if(pStepInfo == NULL) break;	
			stepData= pStepInfo->GetStepData();
			pStep = &stepData;

			m_StepGrid.SetValueRange(CGXRange(nRow+1, 1), (LONG)(pStep->stepHeader.stepIndex+1));			

			switch(pStep->stepHeader.type)
			{
			case EP_TYPE_CHARGE:
				m_pDoc->GetStateMsg(EP_STATE_CHARGE, color);
				m_StepGrid.SetStyleRange(CGXRange().SetRows(nRow+1), CGXStyle().SetInterior(pConfig->m_BStateColor[color]).SetTextColor(pConfig->m_TStateColor[color]));
				m_StepGrid.SetStyleRange(CGXRange(nRow+1, 2), CGXStyle().SetValue(StepTypeMsg(pStep->stepHeader.type)));		
				m_StepGrid.SetValueRange(CGXRange(nRow+1, 3), ModeTypeMsg(pStep->stepHeader.type, pStep->stepHeader.mode));		// Action
				m_StepGrid.SetValueRange(CGXRange(nRow+1, 4), m_pDoc->ValueString(pStep->fIref, EP_CURRENT));				// 정전류
				m_StepGrid.SetValueRange(CGXRange(nRow+1, 5), m_pDoc->ValueString(pStep->fVref, EP_VOLTAGE));				// 정전압

				m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->ValueString(pStep->fEndTime, EP_STEP_TIME));		// 시간
				m_StepGrid.SetValueRange(CGXRange(nRow+1, 7), m_pDoc->ValueString(pStep->fEndI, EP_CURRENT));			// 종지전류
				m_StepGrid.SetValueRange(CGXRange(nRow+1, 8), m_pDoc->ValueString(pStep->fEndV, EP_VOLTAGE));			// 종지전압

				strData.Format("%0.1f", pStep->fSocRate);				
				m_StepGrid.SetValueRange(CGXRange(nRow+1, 9), strData);													// SOC
				m_StepGrid.SetValueRange(CGXRange(nRow+1, 10), m_pDoc->ValueString(pStep->fEndC, EP_CAPACITY));			// 종지용량
// 				strData.Format("%d", (UINT)pStep->fRecVIGetTime/60);
// 				m_StepGrid.SetValueRange(CGXRange(nRow+1, 11), strData);				

				if( nRow == 0 )
				{
					m_nStepLimitCheckTime = (UINT)(pStep->fCompTimeV[0]/60);
					m_nChargeVoltageChkTime = (UINT)(pStep->fCompTimeV[1]/60);

					m_strChgCCVtg = m_pDoc->ValueString(pStep->fCompChgCcVtg, EP_VOLTAGE);
					m_strChgCCTime.Format("%d", (UINT)pStep->fCompChgCcTime);
					m_strChgCCDeltaV = m_pDoc->ValueString(pStep->fCompChgCcDeltaVtg, EP_VOLTAGE);
					m_strChgCVCrt = m_pDoc->ValueString(pStep->fCompChgCvCrt, EP_CURRENT);
					m_strChgCVTime.Format("%d", (UINT)pStep->fCompChgCvTime);
					m_strChgCVDeltaA = m_pDoc->ValueString(pStep->fCompChgCvDeltaCrt, EP_CURRENT);
				}
				break;

			case EP_TYPE_DISCHARGE:
				m_pDoc->GetStateMsg(EP_STATE_DISCHARGE, color);
				m_StepGrid.SetStyleRange(CGXRange().SetRows(nRow+1), CGXStyle().SetInterior(pConfig->m_BStateColor[color]).SetTextColor(pConfig->m_TStateColor[color]));
				m_StepGrid.SetStyleRange(CGXRange(nRow+1, 2), CGXStyle().SetValue(StepTypeMsg(pStep->stepHeader.type)));		
				m_StepGrid.SetValueRange(CGXRange(nRow+1, 3), ModeTypeMsg(pStep->stepHeader.type, pStep->stepHeader.mode));		// Action
				m_StepGrid.SetValueRange(CGXRange(nRow+1, 4), m_pDoc->ValueString(pStep->fIref, EP_CURRENT));			// 정전류
				m_StepGrid.SetValueRange(CGXRange(nRow+1, 5), m_pDoc->ValueString(pStep->fVref, EP_VOLTAGE));			// 정전압
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->StepEndString(pStep));

				m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->ValueString(pStep->fEndTime, EP_STEP_TIME));		// 시간
				m_StepGrid.SetValueRange(CGXRange(nRow+1, 7), m_pDoc->ValueString(pStep->fEndI, EP_CURRENT));			// 종지전류
				m_StepGrid.SetValueRange(CGXRange(nRow+1, 8), m_pDoc->ValueString(pStep->fEndV, EP_VOLTAGE));			// 종지전압

				strData.Format("%0.1f", pStep->fSocRate);				
				m_StepGrid.SetValueRange(CGXRange(nRow+1, 9), strData);													// SOC
				m_StepGrid.SetValueRange(CGXRange(nRow+1, 10), m_pDoc->ValueString(pStep->fEndC, EP_CAPACITY));			// 종지용량
// 				strData.Format("%d", (UINT)pStep->fRecVIGetTime/60);
// 				m_StepGrid.SetValueRange(CGXRange(nRow+1, 11), strData);	

				if( nRow == 0 )
				{
					m_nStepLimitCheckTime = (UINT)(pStep->fCompTimeV[0]/60);
				}
				break;
			case EP_TYPE_IMPEDANCE:
				m_pDoc->GetStateMsg(EP_STATE_IMPEDANCE, color);
				m_StepGrid.SetStyleRange(CGXRange().SetRows(nRow+1), CGXStyle().SetInterior(pConfig->m_BStateColor[color]).SetTextColor(pConfig->m_TStateColor[color]));
				m_StepGrid.SetStyleRange(CGXRange(nRow+1, 2), CGXStyle().SetValue(StepTypeMsg(pStep->stepHeader.type)));	// Action	
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 3), ModeTypeMsg(pStep->stepHeader.type, pStep->stepHeader.mode));		
				m_StepGrid.SetValueRange(CGXRange(nRow+1, 4), m_pDoc->ValueString(pStep->fIref, EP_CURRENT));				// 정전류
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 5), m_pDoc->ValueString(pStep->fVref, EP_VOLTAGE));				// 정전압
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->StepEndString(pStep));

				m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->ValueString(pStep->fEndTime, EP_STEP_TIME));		// 시간
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 7), m_pDoc->ValueString(pStep->fEndI, EP_CURRENT));				// 종지전류
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 8), m_pDoc->ValueString(pStep->fEndV, EP_VOLTAGE));				// 종지전압

				// strData.Format("%0.1f", pStep->fSocRate);				
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 9), strData);													// SOC
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 10), m_pDoc->ValueString(pStep->fEndC, EP_CAPACITY));			// 종지용량
				// strData.Format("%d", (UINT)pStep->fRecVIGetTime/100);
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 11), strData);	

				m_fDCIR_RegTemp = (float)(pStep->lDCIR_RegTemp/10);
				m_fDCIR_RegTemp = (float)(pStep->lDCIR_ResistanceRate/100.0);
				break;
			case EP_TYPE_REST:
				m_pDoc->GetStateMsg(EP_STATE_REST, color);
				m_StepGrid.SetStyleRange(CGXRange().SetRows(nRow+1), CGXStyle().SetInterior(pConfig->m_BStateColor[color]).SetTextColor(pConfig->m_TStateColor[color]));
				m_StepGrid.SetStyleRange(CGXRange(nRow+1, 2), CGXStyle().SetValue(StepTypeMsg(pStep->stepHeader.type)));		// Action	
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 3), ModeTypeMsg(pStep->stepHeader.type, pStep->stepHeader.mode));		
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 4), m_pDoc->ValueString(pStep->fIref, EP_CURRENT));				// 정전류
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 5), m_pDoc->ValueString(pStep->fVref, EP_VOLTAGE));				// 정전압
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->StepEndString(pStep));

				m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->ValueString(pStep->fEndTime, EP_STEP_TIME));				// 시간
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 7), m_pDoc->ValueString(pStep->fEndI, EP_CURRENT));				// 종지전류
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 8), m_pDoc->ValueString(pStep->fEndV, EP_VOLTAGE));				// 종지전압

				// strData.Format("%0.1f", pStep->fSocRate);				
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 9), strData);														// SOC
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 10), m_pDoc->ValueString(pStep->fEndC, EP_CAPACITY));			// 종지용량
				// strData.Format("%d", (UINT)pStep->fRecVIGetTime/100);
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 11), strData);	
				break;
			case EP_TYPE_OCV:
				if( nRow == 0 )
				{
					m_strOCVHighVtg = m_pDoc->ValueString(pStep->fOverV, EP_VOLTAGE);
					m_strOCVLowVtg = m_pDoc->ValueString(pStep->fLimitV, EP_VOLTAGE);
				}
				m_pDoc->GetStateMsg(EP_STATE_OCV, color);
				m_StepGrid.SetStyleRange(CGXRange().SetRows(nRow+1), CGXStyle().SetInterior(pConfig->m_BStateColor[color]).SetTextColor(pConfig->m_TStateColor[color]));
				m_StepGrid.SetStyleRange(CGXRange(nRow+1, 2), CGXStyle().SetValue(StepTypeMsg(pStep->stepHeader.type)));	// Action	
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 3), ModeTypeMsg(pStep->stepHeader.type, pStep->stepHeader.mode));		
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 4), m_pDoc->ValueString(pStep->fIref, EP_CURRENT));				// 정전류
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 5), m_pDoc->ValueString(pStep->fVref, EP_VOLTAGE));				// 정전압
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->StepEndString(pStep));

				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->ValueString(pStep->fEndTime, EP_STEP_TIME));		// 시간
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 7), m_pDoc->ValueString(pStep->fEndI, EP_CURRENT));				// 종지전류
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 8), m_pDoc->ValueString(pStep->fEndV, EP_VOLTAGE));				// 종지전압

				// strData.Format("%0.1f", pStep->fSocRate);				
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 9), strData);													// SOC
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 10), m_pDoc->ValueString(pStep->fEndC, EP_CAPACITY));			// 종지용량
				// strData.Format("%d", (UINT)pStep->fRecVIGetTime/100);
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 11), strData);	
				break;
			case EP_TYPE_END:
				m_pDoc->GetStateMsg(EP_STATE_END, color);
				m_StepGrid.SetStyleRange(CGXRange().SetRows(nRow+1), CGXStyle().SetInterior(pConfig->m_BStateColor[color]).SetTextColor(pConfig->m_TStateColor[color]));
				m_StepGrid.SetStyleRange(CGXRange(nRow+1, 2), CGXStyle().SetValue(StepTypeMsg(pStep->stepHeader.type)));	// Action	
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 3), ModeTypeMsg(pStep->stepHeader.type, pStep->stepHeader.mode));		
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 4), m_pDoc->ValueString(pStep->fIref, EP_CURRENT));				// 정전류
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 5), m_pDoc->ValueString(pStep->fVref, EP_VOLTAGE));				// 정전압
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->StepEndString(pStep));

				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->ValueString(pStep->fEndTime, EP_STEP_TIME));		// 시간
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 7), m_pDoc->ValueString(pStep->fEndI, EP_CURRENT));				// 종지전류
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 8), m_pDoc->ValueString(pStep->fEndV, EP_VOLTAGE));				// 종지전압

				// strData.Format("%0.1f", pStep->fSocRate);				
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 9), strData);													// SOC
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 10), m_pDoc->ValueString(pStep->fEndC, EP_CAPACITY));			// 종지용량
				// strData.Format("%d", (UINT)pStep->fRecVIGetTime/100);
				// m_StepGrid.SetValueRange(CGXRange(nRow+1, 11), strData);	
				break;
			}
		}
	}
	else
	{
		if( nStepRow < nStepIndex )
		{
			return;
		}

		pStepInfo = m_pTestCondition->GetStep(nStepIndex);
		if(pStepInfo == NULL) 
			return;	

		stepData= pStepInfo->GetStepData();
		pStep = &stepData;

		int nRow = nStepIndex;

		m_StepGrid.SetValueRange(CGXRange(nRow+1, 1), (LONG)(pStep->stepHeader.stepIndex+1));

		m_strChgCCVtg.Empty();
		m_strChgCCTime.Empty();
		m_strChgCCDeltaV.Empty();
		m_strChgCVCrt.Empty();
		m_strChgCVTime.Empty();
		m_strChgCVDeltaA.Empty();

		m_nStepLimitCheckTime = 0;
		m_nChargeVoltageChkTime = 0;
				
		COleDateTime time;
		div_t result;
		int hour;
		ULONG lSecond = (UINT)pStep->fRecDeltaTime;
		result = div(lSecond, 3600);
		hour = result.quot;
		result = div(result.rem, 60) ;
		time.SetTime(hour, result.quot, result.rem);		
		m_ReportTime.SetTime(time);
		
		switch(pStep->stepHeader.type)
		{
		case EP_TYPE_CHARGE:
			m_pDoc->GetStateMsg(EP_STATE_CHARGE, color);
			m_StepGrid.SetStyleRange(CGXRange().SetRows(nRow+1), CGXStyle().SetInterior(pConfig->m_BStateColor[color]).SetTextColor(pConfig->m_TStateColor[color]));
			m_StepGrid.SetStyleRange(CGXRange(nRow+1, 2), CGXStyle().SetValue(StepTypeMsg(pStep->stepHeader.type)));		
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 3), ModeTypeMsg(pStep->stepHeader.type, pStep->stepHeader.mode));		// Action
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 4), m_pDoc->ValueString(pStep->fIref, EP_CURRENT));				// 정전류
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 5), m_pDoc->ValueString(pStep->fVref, EP_VOLTAGE));				// 정전압

			m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->ValueString(pStep->fEndTime, EP_STEP_TIME));		// 시간
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 7), m_pDoc->ValueString(pStep->fEndI, EP_CURRENT));			// 종지전류
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 8), m_pDoc->ValueString(pStep->fEndV, EP_VOLTAGE));			// 종지전압

			strData.Format("%0.1f", pStep->fSocRate);				
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 9), strData);													// SOC
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 10), m_pDoc->ValueString(pStep->fEndC, EP_CAPACITY));			// 종지용량
// 			strData.Format("%d", (UINT)pStep->fRecVIGetTime/60);
// 			m_StepGrid.SetValueRange(CGXRange(nRow+1, 11), strData);	

			m_nStepLimitCheckTime = (UINT)(pStep->fCompTimeV[0]/60);
			m_nChargeVoltageChkTime = (UINT)(pStep->fCompTimeV[1]/60);

			m_strChgCCVtg = m_pDoc->ValueString(pStep->fCompChgCcVtg, EP_VOLTAGE);
			m_strChgCCTime.Format("%d", (UINT)pStep->fCompChgCcTime);
			m_strChgCCDeltaV = m_pDoc->ValueString(pStep->fCompChgCcDeltaVtg, EP_VOLTAGE);
			m_strChgCVCrt = m_pDoc->ValueString(pStep->fCompChgCvCrt, EP_CURRENT);
			m_strChgCVTime.Format("%d", (UINT)pStep->fCompChgCvTime);
			m_strChgCVDeltaA = m_pDoc->ValueString(pStep->fCompChgCvDeltaCrt, EP_CURRENT);
			break;

		case EP_TYPE_DISCHARGE:
			m_pDoc->GetStateMsg(EP_STATE_DISCHARGE, color);
			m_StepGrid.SetStyleRange(CGXRange().SetRows(nRow+1), CGXStyle().SetInterior(pConfig->m_BStateColor[color]).SetTextColor(pConfig->m_TStateColor[color]));
			m_StepGrid.SetStyleRange(CGXRange(nRow+1, 2), CGXStyle().SetValue(StepTypeMsg(pStep->stepHeader.type)));		
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 3), ModeTypeMsg(pStep->stepHeader.type, pStep->stepHeader.mode));		// Action
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 4), m_pDoc->ValueString(pStep->fIref, EP_CURRENT));			// 정전류
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 5), m_pDoc->ValueString(pStep->fVref, EP_VOLTAGE));			// 정전압
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->StepEndString(pStep));

			m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->ValueString(pStep->fEndTime, EP_STEP_TIME));		// 시간
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 7), m_pDoc->ValueString(pStep->fEndI, EP_CURRENT));			// 종지전류
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 8), m_pDoc->ValueString(pStep->fEndV, EP_VOLTAGE));			// 종지전압

			strData.Format("%0.1f", pStep->fSocRate);				
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 9), strData);													// SOC
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 10), m_pDoc->ValueString(pStep->fEndC, EP_CAPACITY));			// 종지용량
// 			strData.Format("%d", (UINT)pStep->fRecVIGetTime/60);
// 			m_StepGrid.SetValueRange(CGXRange(nRow+1, 11), strData);

			m_nStepLimitCheckTime = (UINT)(pStep->fCompTimeV[0]/60);
			break;
		case EP_TYPE_IMPEDANCE:
			m_pDoc->GetStateMsg(EP_STATE_IMPEDANCE, color);
			m_StepGrid.SetStyleRange(CGXRange().SetRows(nRow+1), CGXStyle().SetInterior(pConfig->m_BStateColor[color]).SetTextColor(pConfig->m_TStateColor[color]));
			m_StepGrid.SetStyleRange(CGXRange(nRow+1, 2), CGXStyle().SetValue(StepTypeMsg(pStep->stepHeader.type)));	// Action	
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 3), ModeTypeMsg(pStep->stepHeader.type, pStep->stepHeader.mode));		
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 4), m_pDoc->ValueString(pStep->fIref, EP_CURRENT));				// 정전류
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 5), m_pDoc->ValueString(pStep->fVref, EP_VOLTAGE));				// 정전압
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->StepEndString(pStep));

			m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->ValueString(pStep->fEndTime, EP_STEP_TIME));		// 시간
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 7), m_pDoc->ValueString(pStep->fEndI, EP_CURRENT));				// 종지전류
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 8), m_pDoc->ValueString(pStep->fEndV, EP_VOLTAGE));				// 종지전압

			// strData.Format("%0.1f", pStep->fSocRate);				
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 9), strData);													// SOC
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 10), m_pDoc->ValueString(pStep->fEndC, EP_CAPACITY));			// 종지용량
			// strData.Format("%d", (UINT)pStep->fRecVIGetTime/100);
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 11), strData);	

			m_fDCIR_RegTemp = (float)(pStep->lDCIR_RegTemp/10);
			m_fDCIR_RegTemp = (float)(pStep->lDCIR_ResistanceRate/100.0);
			break;
		case EP_TYPE_REST:
			m_pDoc->GetStateMsg(EP_STATE_REST, color);
			m_StepGrid.SetStyleRange(CGXRange().SetRows(nRow+1), CGXStyle().SetInterior(pConfig->m_BStateColor[color]).SetTextColor(pConfig->m_TStateColor[color]));
			m_StepGrid.SetStyleRange(CGXRange(nRow+1, 2), CGXStyle().SetValue(StepTypeMsg(pStep->stepHeader.type)));		// Action	
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 3), ModeTypeMsg(pStep->stepHeader.type, pStep->stepHeader.mode));		
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 4), m_pDoc->ValueString(pStep->fIref, EP_CURRENT));				// 정전류
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 5), m_pDoc->ValueString(pStep->fVref, EP_VOLTAGE));				// 정전압
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->StepEndString(pStep));

			m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->ValueString(pStep->fEndTime, EP_STEP_TIME));				// 시간
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 7), m_pDoc->ValueString(pStep->fEndI, EP_CURRENT));				// 종지전류
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 8), m_pDoc->ValueString(pStep->fEndV, EP_VOLTAGE));				// 종지전압

			// strData.Format("%0.1f", pStep->fSocRate);				
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 9), strData);														// SOC
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 10), m_pDoc->ValueString(pStep->fEndC, EP_CAPACITY));			// 종지용량
			// strData.Format("%d", (UINT)pStep->fRecVIGetTime/100);
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 11), strData);	
			break;
		case EP_TYPE_OCV:
			if( nRow == 0 )
			{
				m_strOCVHighVtg = m_pDoc->ValueString(pStep->fOverV, EP_VOLTAGE);
				m_strOCVLowVtg = m_pDoc->ValueString(pStep->fLimitV, EP_VOLTAGE);
			}

			m_pDoc->GetStateMsg(EP_STATE_OCV, color);
			m_StepGrid.SetStyleRange(CGXRange().SetRows(nRow+1), CGXStyle().SetInterior(pConfig->m_BStateColor[color]).SetTextColor(pConfig->m_TStateColor[color]));
			m_StepGrid.SetStyleRange(CGXRange(nRow+1, 2), CGXStyle().SetValue(StepTypeMsg(pStep->stepHeader.type)));	// Action	
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 3), ModeTypeMsg(pStep->stepHeader.type, pStep->stepHeader.mode));		
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 4), m_pDoc->ValueString(pStep->fIref, EP_CURRENT));				// 정전류
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 5), m_pDoc->ValueString(pStep->fVref, EP_VOLTAGE));				// 정전압
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->StepEndString(pStep));

			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->ValueString(pStep->fEndTime, EP_STEP_TIME));		// 시간
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 7), m_pDoc->ValueString(pStep->fEndI, EP_CURRENT));				// 종지전류
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 8), m_pDoc->ValueString(pStep->fEndV, EP_VOLTAGE));				// 종지전압

			// strData.Format("%0.1f", pStep->fSocRate);				
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 9), strData);													// SOC
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 10), m_pDoc->ValueString(pStep->fEndC, EP_CAPACITY));			// 종지용량
			// strData.Format("%d", (UINT)pStep->fRecVIGetTime/100);
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 11), strData);	
			break;
		case EP_TYPE_END:
			m_pDoc->GetStateMsg(EP_STATE_END, color);
			m_StepGrid.SetStyleRange(CGXRange().SetRows(nRow+1), CGXStyle().SetInterior(pConfig->m_BStateColor[color]).SetTextColor(pConfig->m_TStateColor[color]));
			m_StepGrid.SetStyleRange(CGXRange(nRow+1, 2), CGXStyle().SetValue(StepTypeMsg(pStep->stepHeader.type)));	// Action	
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 3), ModeTypeMsg(pStep->stepHeader.type, pStep->stepHeader.mode));		
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 4), m_pDoc->ValueString(pStep->fIref, EP_CURRENT));				// 정전류
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 5), m_pDoc->ValueString(pStep->fVref, EP_VOLTAGE));				// 정전압
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->StepEndString(pStep));

			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->ValueString(pStep->fEndTime, EP_STEP_TIME));		// 시간
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 7), m_pDoc->ValueString(pStep->fEndI, EP_CURRENT));				// 종지전류
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 8), m_pDoc->ValueString(pStep->fEndV, EP_VOLTAGE));				// 종지전압

			// strData.Format("%0.1f", pStep->fSocRate);				
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 9), strData);													// SOC
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 10), m_pDoc->ValueString(pStep->fEndC, EP_CAPACITY));			// 종지용량
			// strData.Format("%d", (UINT)pStep->fRecVIGetTime/100);
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 11), strData);	
			break;
		}
	}

	m_StepGrid.LockUpdate(bLock);
	m_StepGrid.Redraw();

	UpdateData(false);
}

VOID CDetailAutoTestConditionDlg::DisplayProtectSettingGrid()
{
	CString strData = _T("");
	CStep *pStepInfo;
	STR_COMMON_STEP *pStep, stepData;

	int nStepRow = m_pTestCondition->GetTotalStepNo();
	int nRow = 0;

	BOOL bLock = m_ProtectSettingGrid.LockUpdate();

	for(nRow =0 ; nRow < nStepRow; nRow++)
	{
		pStepInfo = m_pTestCondition->GetStep(nRow);
		if(pStepInfo == NULL) break;	
		stepData = pStepInfo->GetStepData();
		pStep = &stepData;

		switch(pStep->stepHeader.type)
		{
		case EP_TYPE_CHARGE:
			{	
				m_ProtectSettingGrid.SetValueRange(CGXRange(1, 2), m_pDoc->ValueString(pStep->fOverV, EP_VOLTAGE));	// 충전상한전압
				m_ProtectSettingGrid.SetValueRange(CGXRange(2, 2), m_pDoc->ValueString(pStep->fLimitV, EP_VOLTAGE)); // 충전하한전압
				m_ProtectSettingGrid.SetValueRange(CGXRange(3, 2), m_pDoc->ValueString(pStep->fOverI, EP_CURRENT)); // 충전상한전류
				m_ProtectSettingGrid.SetValueRange(CGXRange(4, 2), m_pDoc->ValueString(pStep->fLimitI, EP_CURRENT)); // 충전하한전류
				m_ProtectSettingGrid.SetValueRange(CGXRange(5, 2), m_pDoc->ValueString(pStep->fOverC, EP_CAPACITY)); // 충전상한용량
				m_ProtectSettingGrid.SetValueRange(CGXRange(6, 2), m_pDoc->ValueString(pStep->fLimitC, EP_CAPACITY)); // 충전하한용량
				m_ProtectSettingGrid.SetValueRange(CGXRange(7, 2), m_pDoc->ValueString(pStep->fCompVHigh[1], EP_VOLTAGE)); // 충전상한취득전압
				m_ProtectSettingGrid.SetValueRange(CGXRange(8, 2), m_pDoc->ValueString(pStep->fCompVLow[1], EP_VOLTAGE)); // 충전하한취득전압

				if( pStep->m_nFanOffFlag == 0 )
				{
					m_strChargeFanOffChk = _T("NO");
				}
				else
				{
					m_strChargeFanOffChk = _T("YES");
				}
			}
			break;
		case EP_TYPE_DISCHARGE:
			{
				m_ProtectSettingGrid.SetValueRange(CGXRange(9, 2), m_pDoc->ValueString(pStep->fOverV, EP_VOLTAGE));	// 방전상한전압
				m_ProtectSettingGrid.SetValueRange(CGXRange(10, 2), m_pDoc->ValueString(pStep->fLimitV, EP_VOLTAGE)); // 방전하한전압
				m_ProtectSettingGrid.SetValueRange(CGXRange(11, 2), m_pDoc->ValueString(pStep->fOverI, EP_CURRENT)); // 방전상한전류
				m_ProtectSettingGrid.SetValueRange(CGXRange(12, 2), m_pDoc->ValueString(pStep->fLimitI, EP_CURRENT)); // 방전하한전류
				m_ProtectSettingGrid.SetValueRange(CGXRange(13, 2), m_pDoc->ValueString(pStep->fOverC, EP_CAPACITY)); // 방전상한용량
				m_ProtectSettingGrid.SetValueRange(CGXRange(14, 2), m_pDoc->ValueString(pStep->fLimitC, EP_CAPACITY)); // 방전하한용량
			}
			break;
		}
	}

	m_ProtectSettingGrid.LockUpdate(bLock);
	m_ProtectSettingGrid.Redraw();
}

VOID CDetailAutoTestConditionDlg::DisplayContactSettingGrid()
{
	ASSERT(m_pTestCondition);

	STR_CHECK_PARAM *pCheckParam = m_pTestCondition->GetCheckParam();
	CString strData = _T("");

	BOOL bLock = m_ContactSettingGrid.LockUpdate();

	m_ContactSettingGrid.SetValueRange(CGXRange(1, 2), m_pDoc->ValueString(pCheckParam->fIRef, EP_CURRENT));		// 충전전류
	m_ContactSettingGrid.SetValueRange(CGXRange(2, 2), m_pDoc->ValueString(pCheckParam->fTime, EP_STEP_TIME));		// 충전시간
	m_ContactSettingGrid.SetValueRange(CGXRange(3, 2), m_pDoc->ValueString(pCheckParam->fMaxFaultBattery, EP_VOLTAGE));		// 역전압
	m_ContactSettingGrid.SetValueRange(CGXRange(3, 2), m_pDoc->ValueString(pCheckParam->fVRef, EP_VOLTAGE));
	m_ContactSettingGrid.SetValueRange(CGXRange(4, 2), m_pDoc->ValueString(pCheckParam->fOCVUpperValue, EP_VOLTAGE));
	m_ContactSettingGrid.SetValueRange(CGXRange(5, 2), m_pDoc->ValueString(pCheckParam->fOCVLowerValue, EP_VOLTAGE));
	m_ContactSettingGrid.SetValueRange(CGXRange(6, 2), m_pDoc->ValueString(pCheckParam->fDeltaVoltage, EP_CURRENT));
	m_ContactSettingGrid.SetValueRange(CGXRange(7, 2), m_pDoc->ValueString(pCheckParam->fMaxFaultBattery, EP_CURRENT));
	m_ContactSettingGrid.SetValueRange(CGXRange(8, 2), m_pDoc->ValueString(pCheckParam->fDeltaVoltageLimit, EP_VOLTAGE));

	m_ContactSettingGrid.LockUpdate(bLock);
	m_ContactSettingGrid.Redraw();
}

VOID CDetailAutoTestConditionDlg::DisplayCondition()
{	
	CString strData = _T("");
	
	strData.Format("%d", m_pTestCondition->GetModelInfo()->lID);
	m_LabelType.SetText(strData);
	
	strData.Format("%d", m_pTestCondition->GetTestInfo()->lID);	
	m_LabelProcess.SetText(strData);
	
	DisplayStepGrid();
	
	if( m_StepGrid.GetRowCount() > 0 )
	{
		DisplayContactSettingGrid();
		DisplayProtectSettingGrid();
	}

	UpdateData(FALSE);
}

VOID CDetailAutoTestConditionDlg::InitFont()
{
	LOGFONT	LogFont;

	GetDlgItem(IDC_STATIC1)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight = 1000;
	LogFont.lfHeight = 15;

	m_Font.CreateFontIndirect( &LogFont );

	GetDlgItem(IDC_STATIC1)->SetFont(&m_Font);
	GetDlgItem(IDC_STATIC2)->SetFont(&m_Font);
	GetDlgItem(IDC_STATIC3)->SetFont(&m_Font);
	GetDlgItem(IDC_STATIC4)->SetFont(&m_Font);
	GetDlgItem(IDC_STATIC5)->SetFont(&m_Font);
	GetDlgItem(IDC_STATIC6)->SetFont(&m_Font);

	GetDlgItem(IDC_REG_TEMP_STATIC)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_REG_TEMP)->SetFont(&m_Font);
	GetDlgItem(IDC_RESISTANCE_RATE_STATIC)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_RESISTANCE_RATE)->SetFont(&m_Font);
	
	GetDlgItem(IDC_STEP_LIMIT_CHK_TIME)->SetFont(&m_Font);
	GetDlgItem(IDC_CHARGE_VOLTAGE_CHK_TIME_STATIC)->SetFont(&m_Font);
	GetDlgItem(IDC_CHARGE_VOLTAGE_CHK_TIME)->SetFont(&m_Font);
	GetDlgItem(IDC_FANOFF_STATIC)->SetFont(&m_Font);	
	GetDlgItem(IDC_CHARGE_FANOFF_CHK)->SetFont(&m_Font);

	GetDlgItem(IDC_STATIC_COMP_CHG_CC_VTG)->SetFont(&m_Font);
	GetDlgItem(IDC_STATIC_COMP_CHG_CC_TIME)->SetFont(&m_Font);
	GetDlgItem(IDC_STATIC_COMP_CHG_CC_DELTA_VTG)->SetFont(&m_Font);
	GetDlgItem(IDC_STATIC_COMP_CHG_CV_CRT)->SetFont(&m_Font);
	GetDlgItem(IDC_STATIC_COMP_CHG_CV_TIME)->SetFont(&m_Font);
	GetDlgItem(IDC_STATIC_COMP_CHG_CV_DELTA_CRT)->SetFont(&m_Font);

	GetDlgItem(IDC_COMP_CHG_CC_VTG)->SetFont(&m_Font);
	GetDlgItem(IDC_COMP_CHG_CC_TIME)->SetFont(&m_Font);
	GetDlgItem(IDC_COMP_CHG_CC_DELTA_VTG)->SetFont(&m_Font);
	GetDlgItem(IDC_COMP_CHG_CV_CRT)->SetFont(&m_Font);
	GetDlgItem(IDC_COMP_CHG_CV_TIME)->SetFont(&m_Font);
	GetDlgItem(IDC_COMP_CHG_CV_DELTA_CRT)->SetFont(&m_Font);

	GetDlgItem(IDC_STEP_RANGE4)->SetFont(&m_Font);
	GetDlgItem(IDC_RPT_TIME_STATIC)->SetFont(&m_Font);
	GetDlgItem(IDC_RPT_VI_GET_TIME_STATIC)->SetFont(&m_Font);
	GetDlgItem(IDC_REPORT_TIME)->SetFont(&m_Font);
	GetDlgItem(IDC_VI_GET_TIME)->SetFont(&m_Font);

	GetDlgItem(IDC_OCV_V_STATIC2)->SetFont(&m_Font);
	GetDlgItem(IDC_OCV_V_STATIC)->SetFont(&m_Font);	
	GetDlgItem(IDC_OCV_VTG_HIGH)->SetFont(&m_Font);
	GetDlgItem(IDC_OCV_VTG_LOW)->SetFont(&m_Font);
}

VOID CDetailAutoTestConditionDlg::InitContactSettingGrid()
{
	m_ContactSettingGrid.SubclassDlgItem(IDC_DETAIL_CONTACT_GRID, this);
	m_ContactSettingGrid.Initialize();
	
	CString strItem = _T("");
	
	BOOL bLock = m_ContactSettingGrid.LockUpdate();
	m_ContactSettingGrid.SetRowCount(8);
	m_ContactSettingGrid.SetColCount(2);
	m_ContactSettingGrid.m_bSameColSize = TRUE;
	m_ContactSettingGrid.m_bSameRowSize = TRUE;
	// m_ContactSettingGrid.SetDefaultRowHeight(20);
	m_ContactSettingGrid.SetRowHeight(0,1,0);
	
	m_ContactSettingGrid.SetStyleRange(CGXRange().SetCols(1), CGXStyle().SetFont(CGXFont().SetSize(10).SetBold(TRUE)).SetTextColor(RGB(255,255,255)).SetInterior(RGB_LABEL_BACKGROUND));
	m_ContactSettingGrid.SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetFont(CGXFont().SetSize(10).SetBold(TRUE)));

	strItem.Format(TEXT_LANG[3], m_pDoc->m_strIUnit );//"충전전류(%s)"
	m_ContactSettingGrid.SetValueRange(CGXRange(1, 1), strItem);
	m_ContactSettingGrid.SetValueRange(CGXRange(2, 1), TEXT_LANG[4]);//"충전시간"
	strItem.Format("Start voltage(%s)", m_pDoc->m_strVUnit );
	m_ContactSettingGrid.SetValueRange(CGXRange(3, 1), strItem);
	// m_ContactSettingGrid.SetValueRange(CGXRange(4, 1), TEXT_LANG[6]);//"접촉저항(mΩ)"

	strItem.Format("Upper voltage(%s)", m_pDoc->m_strVUnit );
	m_ContactSettingGrid.SetValueRange(CGXRange(4, 1), strItem);
	strItem.Format("Low voltage(%s)", m_pDoc->m_strVUnit );
	m_ContactSettingGrid.SetValueRange(CGXRange(5, 1), strItem);

	strItem.Format("Upper current(%s)", m_pDoc->m_strIUnit );
	m_ContactSettingGrid.SetValueRange(CGXRange(6, 1), strItem);

	strItem.Format("Low current(%s)", m_pDoc->m_strIUnit );
	m_ContactSettingGrid.SetValueRange(CGXRange(7, 1), strItem);

	strItem.Format("Delta Voltage Limit(%s)", m_pDoc->m_strVUnit ); //"Delta Voltage Limit(%s)"
	m_ContactSettingGrid.SetValueRange(CGXRange(8, 1), strItem);
	
	/*
	strItem.Format("역전압(%s)", m_pDoc->m_strVUnit );
	m_ContactSettingGrid.SetValueRange(CGXRange(3, 1), strItem);
	m_ContactSettingGrid.SetValueRange(CGXRange(4, 1), "접촉저항(mΩ)");
	*/
	
	m_ContactSettingGrid.LockUpdate(bLock);
	m_ContactSettingGrid.Redraw();
	m_ContactSettingGrid.GetParam()->EnableSelection(FALSE);
}

VOID CDetailAutoTestConditionDlg::InitStepGrid()
{	
	m_StepGrid.SubclassDlgItem(IDC_DETAIL_STEP_GRID, this);
	m_StepGrid.Initialize();
	
	CString strItem = _T("");
	CString strTemp = _T("");
	
	BOOL bLock = m_StepGrid.LockUpdate();
	m_StepGrid.SetDefaultRowHeight(30);	
	m_StepGrid.SetRowHeight(0,0,40);
	m_StepGrid.SetRowCount(0);
	m_StepGrid.SetColCount(10);
		
	m_StepGrid.SetStyleRange(CGXRange().SetCols(1), CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(0,0,0)).SetInterior(RGB_LABEL_BACKGROUND));
	m_StepGrid.SetStyleRange(CGXRange().SetRows(0,0), CGXStyle().SetFont(CGXFont().SetSize(10).SetBold(TRUE)).SetTextColor(RGB(255,255,255)).SetInterior(RGB_LABEL_BACKGROUND));
	
	m_StepGrid.SetValueRange(CGXRange(0, 1), "Step");
	m_StepGrid.SetValueRange(CGXRange(0, 2), "Action");
	m_StepGrid.SetValueRange(CGXRange(0, 3), "Mode");

	strItem.Format("%s\n(%s)", TEXT_LANG[7], m_pDoc->m_strIUnit);//"정전류\n(%s)"
	m_StepGrid.SetValueRange(CGXRange(0, 4), strItem);
	strItem.Format("%s\n(%s)", TEXT_LANG[8], m_pDoc->m_strVUnit );//"정전압\n(%s)"
	m_StepGrid.SetValueRange(CGXRange(0, 5), strItem);
	m_StepGrid.SetValueRange(CGXRange(0, 6), TEXT_LANG[9]);//"시간"
	
	strItem.Format("%s\n(%s)", TEXT_LANG[10], m_pDoc->m_strIUnit );//"종지전류\n(%s)"
	m_StepGrid.SetValueRange(CGXRange(0, 7), strItem);

	strItem.Format("%s\n(%s)", TEXT_LANG[11], m_pDoc->m_strVUnit );//"종지전압\n(%s)"	
	m_StepGrid.SetValueRange(CGXRange(0, 8), strItem);
	m_StepGrid.SetValueRange(CGXRange(0, 9), "SOC(%)");

	strItem.Format("%s\n(%s)", TEXT_LANG[12], m_pDoc->m_strCUnit);//"종지용량\n(%s)"	
	m_StepGrid.SetValueRange(CGXRange(0, 10), strItem);
	
	
	m_StepGrid.m_bSameColSize = TRUE;
	// m_StepGrid.m_bCustomWidth = TRUE;
	m_StepGrid.m_bSameRowSize = FALSE;
	/*
	m_StepGrid.m_nWidth[1] = 50;	
	m_StepGrid.m_nWidth[2] = 100;
	m_StepGrid.m_nWidth[3] = 80;
	m_StepGrid.m_nWidth[4] = 80;
	m_StepGrid.m_nWidth[5] = 80;
	m_StepGrid.m_nWidth[6] = 80;
	m_StepGrid.m_nWidth[7] = 80;
	m_StepGrid.m_nWidth[8] = 80;
	m_StepGrid.m_nWidth[9] = 80;
	m_StepGrid.m_nWidth[10] = 80;
	m_StepGrid.m_nWidth[11] = 80;
	*/
		
	m_StepGrid.LockUpdate(bLock);
	m_StepGrid.Redraw();
	m_StepGrid.GetParam()->EnableSelection(FALSE);
}

VOID CDetailAutoTestConditionDlg::InitProtectSettingGrid()
{
	m_ProtectSettingGrid.SubclassDlgItem(IDC_DETAIL_PROTECT_GRID, this);
	m_ProtectSettingGrid.Initialize();
	BOOL bLock = m_ProtectSettingGrid.LockUpdate();

	m_ProtectSettingGrid.SetRowCount(14);
	m_ProtectSettingGrid.SetColCount(2);
	m_ProtectSettingGrid.m_bSameColSize = TRUE;
	m_ProtectSettingGrid.m_bSameRowSize = TRUE;
	// m_ProtectSettingGrid.SetDefaultRowHeight(25);
	m_ProtectSettingGrid.SetRowHeight(0,1,0);

	// m_ProtectSettingGrid.SetStyleRange(CGXRange().SetCols(1), CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(0,0,0)).SetInterior(RGB_LABEL_BACKGROUND));
	m_ProtectSettingGrid.SetStyleRange(CGXRange().SetCols(1), CGXStyle().SetFont(CGXFont().SetSize(10).SetBold(TRUE)).SetTextColor(RGB(255,255,255)).SetInterior(RGB_LABEL_BACKGROUND));
	m_ProtectSettingGrid.SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetFont(CGXFont().SetSize(10).SetBold(TRUE)));
	m_ProtectSettingGrid.SetValueRange(CGXRange(1, 1), TEXT_LANG[14]);//"충전상한전압"
	m_ProtectSettingGrid.SetValueRange(CGXRange(2, 1), TEXT_LANG[15]);//"충전하한전압"
	m_ProtectSettingGrid.SetValueRange(CGXRange(3, 1), TEXT_LANG[16]);//"충전상한전류"
	m_ProtectSettingGrid.SetValueRange(CGXRange(4, 1), TEXT_LANG[17]);//"충전하한전류"
	m_ProtectSettingGrid.SetValueRange(CGXRange(5, 1), TEXT_LANG[18]);//"충전상한용량"
	m_ProtectSettingGrid.SetValueRange(CGXRange(6, 1), TEXT_LANG[19]);//"충전하한용량"
	m_ProtectSettingGrid.SetValueRange(CGXRange(7, 1), TEXT_LANG[20]);//"충전상한취득전압"
	m_ProtectSettingGrid.SetValueRange(CGXRange(8, 1), TEXT_LANG[21]);//"충전하한취득전압"
	
	m_ProtectSettingGrid.SetValueRange(CGXRange(9, 1), TEXT_LANG[22]);//"방전상한전압"
	m_ProtectSettingGrid.SetValueRange(CGXRange(10, 1), TEXT_LANG[23]);//"방전하한전압"
	m_ProtectSettingGrid.SetValueRange(CGXRange(11, 1), TEXT_LANG[24]);//"방전상한전류"
	m_ProtectSettingGrid.SetValueRange(CGXRange(12, 1), TEXT_LANG[25]);//"방전하한전류"
	m_ProtectSettingGrid.SetValueRange(CGXRange(13, 1), TEXT_LANG[26]);//"방전상한용량"
	m_ProtectSettingGrid.SetValueRange(CGXRange(14, 1), TEXT_LANG[27]);//"방전하한용량"
	
	
	m_ProtectSettingGrid.LockUpdate(bLock);
	m_ProtectSettingGrid.Redraw();
	m_ProtectSettingGrid.GetParam()->EnableSelection(FALSE);

	CString strFormat;
	strFormat.Format(TEXT_LANG[28], m_pDoc->m_strVUnit);
	GetDlgItem(IDC_STATIC_COMP_CHG_CC_VTG)->SetWindowText(strFormat);
	strFormat.Format(TEXT_LANG[29]);
	GetDlgItem(IDC_STATIC_COMP_CHG_CC_TIME)->SetWindowText(strFormat);
	strFormat.Format(TEXT_LANG[30], m_pDoc->m_strVUnit);
	GetDlgItem(IDC_STATIC_COMP_CHG_CC_DELTA_VTG)->SetWindowText(strFormat);

	strFormat.Format(TEXT_LANG[31], m_pDoc->m_strIUnit);
	GetDlgItem(IDC_STATIC_COMP_CHG_CV_CRT)->SetWindowText(strFormat);
	strFormat.Format(TEXT_LANG[32]);
	GetDlgItem(IDC_STATIC_COMP_CHG_CV_TIME)->SetWindowText(strFormat);
	strFormat.Format(TEXT_LANG[33], m_pDoc->m_strIUnit);
	GetDlgItem(IDC_STATIC_COMP_CHG_CV_DELTA_CRT)->SetWindowText(strFormat);

	strFormat.Format("1st OCV Upper Limit(%s)", m_pDoc->m_strVUnit);
	GetDlgItem(IDC_OCV_V_STATIC2)->SetWindowText(strFormat);
	strFormat.Format("1st OCV Low Limit(%s)", m_pDoc->m_strVUnit);
	GetDlgItem(IDC_OCV_V_STATIC)->SetWindowText(strFormat);
}