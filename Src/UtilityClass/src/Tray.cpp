// Tray.cpp: implementation of the CTray class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Tray.h"

//#include <DataBaseAll.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTray::CTray()
{
	InitData();
	m_bBCRScaned = FALSE;
}

CTray::CTray(CString trayNo)
{
	InitData();
	m_bBCRScaned = FALSE;
	m_strTrayNo = trayNo;
}

CTray::CTray(long traySerial)
{
	InitData();
	m_bBCRScaned = FALSE;
	m_lTraySerial = traySerial;

}

CTray::~CTray()
{

}

void CTray::operator=(const CTray &tray) 
{
	m_lTraySerial = tray.m_lTraySerial;
	strJigID = tray.strJigID;
//	lTeskID = tray.lTeskID;
	m_registedTime = tray.m_registedTime;
	m_strTrayNo = tray.m_strTrayNo;
	m_strTrayName = tray.m_strTrayName;
	m_strUserName = tray.m_strUserName;
	m_strDescription = tray.m_strDescription;
	lModelKey = tray.lModelKey;					// Battery Model DB Primary Key
	lTestKey = tray.lTestKey;					// Test	Paramary Key
//	lStepKey = tray.lStepKey;
	strLotNo = tray.strLotNo;					// 사용자 입력 Lot No  Max 31Byte
	strTypeSel = tray.strTypeSel;					// 20201211 ksj
	strTestSerialNo = tray.strTestSerialNo;			// 현재 공정에 대한 고유 번호 23Byte(날짜+Random()수를 이용한 고유 번호)
	testDateTime = tray.testDateTime;
	lCellNo = tray.lCellNo;
	lInputCellCount = tray.lInputCellCount;
	nNormalCount = tray.nNormalCount;
	nFailCount = tray.nFailCount;
//	strCellCode = tray.strCellCode;					// 128
//	strCellGradeCode = tray.strCellCode;
	strOperatorID = tray.strOperatorID;
	memcpy(cellCode, tray.cellCode, sizeof(cellCode));	//256
	memcpy(cellGradeCode, tray.cellGradeCode, sizeof(cellGradeCode));	//256
	ModuleID = tray.ModuleID;
	GroupIndex = tray.GroupIndex;
	m_bNewProcTray = tray.m_bNewProcTray;
//	m_nTrayCellType = tray.m_nTrayCellType;
	m_strFileName = tray.m_strFileName;
//	m_bNewTest = tray.m_bNewTest;

}

void CTray::InitData(BOOL	bAllData)
{
	if(bAllData)
	{
		m_TrayType = 0;
		m_lTraySerial = 0;
		m_registedTime = COleDateTime::GetCurrentTime();
		m_strTrayNo = "";
		m_strTrayName ="";
		m_strUserName ="";
		m_strDescription = "";
	
//		m_bBCRScaned = FALSE;
		m_bTestReserved = FALSE;
	}
	strJigID = "";
//	lTeskID = 0;
	lModelKey = 0;					//Battery Model DB Primary Key
	lTestKey = 0;					//Test	Paramary Key
//	lStepKey = 0;
	strLotNo = "";					//사용자 입력 Lot No  Max 31Byte
	strTypeSel = "";					//사용자 입력 Lot No  Max 31Byte
	strTestSerialNo = "";			//현재 공정에 대한 고유 번호 23Byte(날짜+Random()수를 이용한 고유 번호)
	testDateTime = COleDateTime::GetCurrentTime();
	lCellNo = 1;
	nNormalCount = 0;
	nFailCount = 0;
//	strCellCode = "";
//	strCellGradeCode = "";
	strOperatorID = "Unuse";
	ModuleID = 0;
	GroupIndex = 0;
	m_bNewProcTray = TRUE;
	m_strFileName.Empty();
	lInputCellCount = 0;
	
	for(int i =0 ; i<EP_MAX_CH_PER_MD; i++)
	{
		cellCode[i] = EP_CODE_NORMAL;	//128
		cellGradeCode[i] = 0;
	}
	
	ZeroMemory(&m_ResultCellSerial, sizeof(m_ResultCellSerial));		//20090920
	
	m_nTabDeepth = 0;
	m_nTrayHeight = 0;
	m_nTrayType = 0;
	m_nContactErrlimit = 0;			// Cell Check limit
	m_nCapaErrlimit = 0;			// Cell Capa limit
	m_nChErrlimit = 0;
	m_nResultFileType = 0;
}

BOOL CTray::ResetTrayData()
{
	
	//1. 변수 Reset
	InitData(FALSE);

	//2.  Data Base reset
	WriteTrayData();

/*	CString strTemp;
	strTemp = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");	//Get DataBase Folder(CTSMon Folde)
	if(strTemp.IsEmpty())
	{
		return FALSE;
	}
	strTemp = strTemp + "\\" +FORM_SET_DATABASE_NAME;

	CDaoDatabase  db;
	try
	{
		db.Open(strTemp);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	CString strSQL,strWhere;
	if(!strDestTrayNo.IsEmpty())
	{
		strWhere.Format(" WHERE TrayNo = '%s'", strDestTrayNo);
	}
	else
	{
		strWhere.Format(" WHERE TraySerial = %ld", m_lTraySerial);
	}
	strSQL.Format("UPDATE Tray SET JigID = '%s', ModelKey = %d, TestKey=%d, LotNo ='%s', TestSerialNo = '%s', TestDateTime = '%s', CellNo = %d, NormalCount = %d, FailCount = %d, strOperatorID ='%s', lInputCellCount = %d, ModuleID = %d, GroupIndex =%d", 
								strJigID,  
								lModelKey,
								lTestKey,
								strLotNo, 
								strTestSerialNo,
								testDateTime.Format(),
								lCellNo,
								nNormalCount,
								nFailCount,
								strOperatorID,
								lInputCellCount,
								ModuleID,
								GroupIndex
							);

	strSQL += strWhere;
	db.Execute(strSQL);

	//3. Cell state reset
	strWhere.Format(" WHERE TraySerial = %ld", m_lTraySerial);
	strSQL.Format("UPDATE CellState SET CellCode = %d, CellGrade = ''",  EP_CODE_NORMAL);
	strSQL += strWhere;
	db.Execute(strSQL);

	db.Close();
	return TRUE;
*/
/*	CTrayRecordSet	recordSet;

	if(!strDestTrayNo.IsEmpty())
	{
		recordSet.m_strFilter.Format("[TrayNo] = '%s'", strDestTrayNo);
	}
	else
	{
		recordSet.m_strFilter.Format("[TraySerial] = %ld", m_lTraySerial);
	}
	recordSet.m_strSort.Format("[ID]");
	
	try
	{
		recordSet.Open();
	}
	catch (CDBException* e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();
		return FALSE;
	}
	
	if(recordSet.IsBOF() || recordSet.IsEOF())
	{
		recordSet.Close();
		return FALSE;
	}

	InitData(FALSE);
	recordSet.Edit();

//	recordSet.m_TraySerial = pTrayData->lTraySerial;
//	recordSet.m_TrayNo = pTrayData->strTrayNo;
//	recordSet.m_TrayName = pTrayData->strTrayName;
//	recordSet.m_UserName = pTrayData->strUserName;
//	recordSet.m_Description = pTrayData->strDescription;
//	recordSet.m_TrayType = m_nTrayType;

	recordSet.m_JigID = strJigID;
//	recordSet.m_TeskID = lTeskID;

	recordSet.m_ModelKey = lModelKey;		
	recordSet.m_TestKey = lTestKey;	
//	recordSet.m_StepIndex = lStepKey;
	recordSet.m_LotNo = strLotNo;					
	recordSet.m_TestSerialNo = strTestSerialNo;	

	recordSet.m_TestDateTime = testDateTime; 

	recordSet.m_CellNo = lCellNo;
	recordSet.m_NormalCount = nNormalCount;
	recordSet.m_FailCount = nFailCount;
	recordSet.m_OperatorID = strOperatorID;
	recordSet.m_InputCellNo = lInputCellCount;
	recordSet.m_ModuleID = ModuleID;
	recordSet.m_GroupIndex = GroupIndex;
	
	try
	{
		recordSet.Update();
		recordSet.Close();
	}
	catch (CDBException* e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();
		return FALSE;
	}

	CCellStateRecordSet	recordSet1;
	recordSet1.m_strFilter.Format("[TraySerial] = %ld", m_lTraySerial);
	recordSet1.m_strSort.Format("[ChNo]");

	try
	{
		recordSet1.Open();
	}
	catch (CDBException* e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();
		return FALSE;
	}

	while(!recordSet1.IsEOF())
	{
		recordSet1.Edit();
		recordSet1.m_CellCode = EP_CODE_NORMAL;
		recordSet1.m_CellGrade = "";
		recordSet1.Update();
		recordSet1.MoveNext();
	}
	recordSet1.Close();
*/
	return TRUE;
}

/*
BOOL CTray::ResetTrayDataA()
{
	if(!m_db.IsOpen())	return FALSE;

	InitData(FALSE);

	WriteTrayData();

	return TRUE;
}
*/

BOOL CTray::LoadTrayData(CString strLoadTrayNo, BOOL bAllData)
{
	CString strTemp;
	strTemp = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");	//Get DataBase Folder(CTSMon Folde)
	if(strTemp.IsEmpty())
	{
		return FALSE;
	}

	strTemp = strTemp + "\\" +FORM_SET_DATABASE_NAME;
	
	CDaoDatabase  db;
	try
	{
		db.Open(strTemp);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	
	//1.  Data Base reset
	CString strSQL,strWhere;
	if(!strLoadTrayNo.IsEmpty())
	{
		strWhere.Format(" FROM Tray WHERE TrayNo = '%s'", strLoadTrayNo);
	}
	else
	{
		strWhere.Format(" FROM Tray WHERE TraySerial = %ld", m_lTraySerial);
	}

	strSQL = "SELECT TraySerial, RegistedTime, TrayNo, TrayName, UserName, Description";
	if(bAllData)
	{
		strTemp = ", JigID, ModelKey, TestKey, LotNo, TestSerialNo, TestDateTime, CellNo, NormalCount, FailCount, OperatorID, InputCellNo, ModuleID, GroupIndex";
		strSQL += strTemp;
	}
	strSQL += strWhere;

	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return FALSE;
	}	
	
	if(rs.IsBOF() || rs.IsEOF())
	{
		rs.Close();
		db.Close();
		return FALSE;
	}

	InitData(TRUE);
	
	COleDateTime tt;
	rs.GetFieldValue("TraySerial", data);		//TraySerial
	m_lTraySerial = data.lVal;
	data = rs.GetFieldValue(1);		//RegistedTime
	m_registedTime = data;
	data = rs.GetFieldValue(2);		//TrayNo
	if(data.vt != VT_NULL)	m_strTrayNo = data.pbVal;
	data = rs.GetFieldValue(3);		//TrayName
	if(data.vt != VT_NULL)	m_strTrayName = data.pbVal;
	data = rs.GetFieldValue(4);		//UserName
	if(data.vt != VT_NULL)	m_strUserName = data.pbVal;
	data = rs.GetFieldValue(5);		//Description
	if(data.vt != VT_NULL)	m_strDescription = data.pbVal;
	if(bAllData)
	{
		data = rs.GetFieldValue(6);		//JigID
		if(data.vt != VT_NULL)	strJigID = data.pbVal;
		data = rs.GetFieldValue(7);		//ModelKey
		lModelKey = data.lVal;
		data = rs.GetFieldValue(8);		//TestKey
		lTestKey = data.lVal;
		data = rs.GetFieldValue(9);		//LotNo
		if(data.vt != VT_NULL)	strLotNo = data.pbVal;
		data = rs.GetFieldValue(10);	//TestSerialNo
		if(data.vt != VT_NULL)	strTestSerialNo = data.pbVal;
		data = rs.GetFieldValue(11);	//TestDateTime
		testDateTime = data;
		data = rs.GetFieldValue(12);	//CellNo
		lCellNo = data.lVal;
		data = rs.GetFieldValue(13);	//NormalCount
		nNormalCount = data.lVal;
		data = rs.GetFieldValue(14);	//FailCount
		nFailCount = data.lVal;
		data = rs.GetFieldValue(15);	//OperatorID
		if(data.vt != VT_NULL)	strOperatorID = data.pbVal;
		data = rs.GetFieldValue(16);	//InputCellNo
		lInputCellCount = data.lVal;
		data = rs.GetFieldValue(17);	//ModuleID
		ModuleID = data.lVal;
		data = rs.GetFieldValue(18);	//GroupIndex
		GroupIndex = data.lVal;
	}
	rs.Close();	

	if(bAllData)
	{
		strSQL = "SELECT CellCode, CellGrade, ChNo";
		strWhere.Format(" FROM CellState WHERE TraySerial = %d ORDER BY ChNo", m_lTraySerial);
		strSQL += strWhere;

		COleVariant data;
		CDaoRecordset rs1(&db);
		try
		{
			rs1.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
		}
		catch (CDaoException* e)
		{
			AfxMessageBox(e->m_pErrorInfo->m_strDescription);
			e->Delete();
			db.Close();
			return FALSE;
		}	
		
		int chNo;
		while(!rs1.IsEOF())
		{
			data = rs1.GetFieldValue(2);		//ChNo
			chNo = data.lVal;
			if(chNo>=0 && chNo <EP_MAX_CH_PER_MD)
			{
				strTemp.Empty();
				data = rs1.GetFieldValue(0);		//CellCode
				cellCode[chNo] = (BYTE)data.lVal;
				if(data.vt != VT_NULL)	data = rs1.GetFieldValue(1);		//CellGrade
				strTemp = data.pbVal;
				if(strTemp.IsEmpty())
				{
					cellGradeCode[chNo] = 0;
				}
				else
				{
					cellGradeCode[chNo] = strTemp[0];
				}
			}
			rs1.MoveNext();
		}
		rs1.Close();
	}

	db.Close();
	return TRUE;


/*	CTrayRecordSet	recordSet;
	if(strLoadTrayNo.IsEmpty())
	{
		recordSet.m_strFilter.Format("[TraySerial] = %ld", m_lTraySerial);
	}
	else
	{
		recordSet.m_strFilter.Format("[TrayNo] = '%s'", strLoadTrayNo);
	}
	recordSet.m_strSort.Format("[TraySerial]");
	
	try
	{
		recordSet.Open();
	}
	catch (CDBException* e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();
		return FALSE;
	}
	
	if(recordSet.IsBOF() || recordSet.IsEOF())
	{
		recordSet.Close();
		return FALSE;
	}
	
	m_lTraySerial = recordSet.m_TraySerial;
//	lTeskID = recordSet.m_TeskID;
//	m_nTrayCellType = recordSet.m_TrayType;

	m_registedTime.SetDateTime(	recordSet.m_RegistedTime.GetYear(), 
										recordSet.m_RegistedTime.GetMonth(),
										recordSet.m_RegistedTime.GetDay(), 
										recordSet.m_RegistedTime.GetHour(), 
										recordSet.m_RegistedTime.GetMinute(), 
										recordSet.m_RegistedTime.GetSecond()
									); 
	m_strTrayNo = recordSet.m_TrayNo;
	m_strTrayName = recordSet.m_TrayName;
	m_strUserName = recordSet.m_UserName;
	m_strDescription = recordSet.m_Description;

	if(bAllData)
	{
		strJigID = recordSet.m_JigID;
		lModelKey = recordSet.m_ModelKey;		
		lTestKey = recordSet.m_TestKey;	
//		lStepKey= recordSet.m_StepIndex;
		strLotNo = recordSet.m_LotNo;					
		strTestSerialNo = recordSet.m_TestSerialNo;	

		testDateTime.SetDateTime(			recordSet.m_TestDateTime.GetYear(), 
											recordSet.m_TestDateTime.GetMonth(),
											recordSet.m_TestDateTime.GetDay(), 
											recordSet.m_TestDateTime.GetHour(), 
											recordSet.m_TestDateTime.GetMinute(), 
											recordSet.m_TestDateTime.GetSecond()
										); 

		lCellNo = recordSet.m_CellNo;
		nNormalCount = recordSet.m_NormalCount;
		nFailCount = recordSet.m_FailCount;
		strOperatorID = recordSet.m_OperatorID;
		lInputCellCount = recordSet.m_InputCellNo;
		ModuleID = recordSet.m_ModuleID;
		GroupIndex = recordSet.m_GroupIndex;

		CCellStateRecordSet	recordSet1;
		recordSet1.m_strFilter.Format("[TraySerial] = %ld", m_lTraySerial);
		recordSet1.m_strSort.Format("[ChNo]");

		try
		{
			recordSet1.Open();
		}
		catch (CDBException* e)
		{
			AfxMessageBox(e->m_strError);
			e->Delete();
			return FALSE;
		}

		while(!recordSet1.IsEOF())
		{
			cellCode[recordSet1.m_ChNo] = (BYTE)recordSet1.m_CellCode;
			if(recordSet1.m_CellGrade.GetLength() > 0)
			{
				cellGradeCode[recordSet1.m_ChNo] = (BYTE)recordSet1.m_CellGrade[0];
			}
			else
			{
				cellGradeCode[recordSet1.m_ChNo] = 0;
			}
			recordSet1.MoveNext();
		}
		recordSet1.Close();
	}
	recordSet.Close();

	return TRUE;
*/
}

BOOL CTray::WriteTrayData()
{
	CString strTemp;
	strTemp = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");	//Get DataBase Folder(CTSMon Folde)
	if(strTemp.IsEmpty())
	{
		return FALSE;
	}

	strTemp = strTemp + "\\" +FORM_SET_DATABASE_NAME;
		
	CDaoDatabase  db;
	try
	{
		db.Open(strTemp);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	
	//1.  Data Base reset
	CString strSQL,strWhere;
	strWhere.Format(" WHERE TraySerial = %ld", m_lTraySerial);
	strSQL.Format("UPDATE Tray SET JigID = '%s', ModelKey = %d, TestKey=%d, LotNo ='%s', TestSerialNo = '%s', TestDateTime = '%s', CellNo = %d, NormalCount = %d, FailCount = %d, OperatorID ='%s', InputCellNo = %d, ModuleID = %d, GroupIndex =%d", 
								strJigID,  
								lModelKey,
								lTestKey,
								strLotNo, 
								strTestSerialNo,
								testDateTime.Format(),
								lCellNo,
								nNormalCount,
								nFailCount,
								strOperatorID,
								lInputCellCount,
								ModuleID,
								GroupIndex
							);

	strSQL += strWhere;
	db.Execute(strSQL);

	int ch = 0;

	//2. Cell state reset
	for(ch = 0; ch<EP_MAX_CH_PER_MD; ch++);
	{
		strWhere.Format(" WHERE TraySerial = %ld AND ChNo = %d", m_lTraySerial, ch);
		strTemp.Format("%c", cellGradeCode[ch]);
		strSQL.Format("UPDATE CellState SET CellCode = %d, CellGrade = '%s'",  cellCode[ch], strTemp);
		strSQL += strWhere;
		db.Execute(strSQL);
	}

	db.Close();
	return TRUE;

/*
	CString strTemp;
	CTrayRecordSet	recordSet;
	recordSet.m_strFilter.Format("[TraySerial] = %ld", m_lTraySerial);
	recordSet.m_strSort.Format("[ID]");
	
	try
	{
		recordSet.Open();
	}
	catch (CDBException* e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();
		return	FALSE;
	}

	if(recordSet.IsEOF() || recordSet.IsBOF())
	{
		recordSet.Close();
		return FALSE;
	}

	recordSet.Edit();

//	recordSet.m_TraySerial = pTrayData->lTraySerial;
//	recordSet.m_RegistedTime = pTrayData->registedTime;
//	recordSet.m_TrayNo = pTrayData->strTrayNo;
//	recordSet.m_TrayName = pTrayData->strTrayName;
//	recordSet.m_UserName = pTrayData->strUserName;

	recordSet.m_JigID = strJigID;
//	recordSet.m_TeskID = lTeskID;
	recordSet.m_ModelKey = lModelKey;
	recordSet.m_TestKey = lTestKey;
//	recordSet.m_StepIndex = lStepKey;
	recordSet.m_LotNo = strLotNo;
	recordSet.m_TestSerialNo = strTestSerialNo;
	recordSet.m_TestDateTime = testDateTime;
	recordSet.m_CellNo = lCellNo;
	recordSet.m_NormalCount = nNormalCount;
	recordSet.m_FailCount = nFailCount;
//	recordSet.m_CellCode = cellCode;
//	recordSet.m_GradeCode = cellGradeCode;
	recordSet.m_OperatorID = strOperatorID;
	recordSet.m_InputCellNo = lInputCellCount;
	recordSet.m_ModuleID = ModuleID;
	recordSet.m_GroupIndex = GroupIndex;

	try
	{
		recordSet.Update();
		recordSet.Close();
	}
	catch (CDBException* e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();
		return FALSE;
	}

	CCellStateRecordSet	recordSet1;
	recordSet1.m_strFilter.Format("[TraySerial] = %ld", m_lTraySerial);
	recordSet1.m_strSort.Format("[ChNo]");

	try
	{
		recordSet1.Open();
	}
	catch (CDBException* e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();
		return FALSE;
	}

	while(!recordSet1.IsEOF())
	{
		recordSet1.Edit();
		recordSet1.m_CellCode = cellCode[recordSet1.m_ChNo];
		strTemp.Format("%c", cellGradeCode[recordSet1.m_ChNo]);
		recordSet1.m_CellGrade = strTemp;
		
		recordSet1.Update();
		recordSet1.MoveNext();
	}
	recordSet1.Close();

	return TRUE;
*/
}

BOOL CTray::AddNewTray(CString trayNo, CString trayName, CString UserName, CString descrip, int trayType)
{
	if(trayNo.IsEmpty())	return FALSE;

	//1. 중복 Tray 삭제 
	DeleteTray(trayNo);

	CString strTemp;
	strTemp = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");	//Get DataBase Folder(CTSMon Folde)
	if(strTemp.IsEmpty())
	{
		return FALSE;
	}

	strTemp = strTemp + "\\" +FORM_SET_DATABASE_NAME;
	
	CDaoDatabase  db;
	try
	{
		db.Open(strTemp);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	

	//2. Max SerialNo search
	CString strSQL;
	strSQL.Format("SELECT MAX(TraySerial) FROM Tray");
	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return FALSE;
	}	
	
	if(rs.IsBOF() || rs.IsEOF())
	{
		rs.Close();
		db.Close();
		return FALSE;
	}
	
	data = rs.GetFieldValue(0);		//TraySerial
	int nTraySerial = data.lVal+1;
	rs.Close();


	strSQL.Format("INSERT INTO Tray (TraySerial, TrayNo, UserName, TrayName, Description, TrayType) VALUES (%d, '%s', '%s', '%s', '%s', %d)", 
					nTraySerial, trayNo, UserName, trayName, descrip, trayType);

	db.Execute(strSQL);


	for(int i=0; i<EP_MAX_CH_PER_MD; i++)
	{
		strSQL.Format("INSERT INTO CellState (TraySerial, ChNo, CellCode, CellGrade) VALUES (%d, %d, %d, '%s')", 
						nTraySerial, i, EP_CODE_NORMAL, "");

		db.Execute(strSQL);
	}

	db.Close();
	return TRUE;

/*	CTrayRecordSet	recordSet;
	recordSet.m_strFilter.Format("[TrayNo] = '%s'", trayNo);
	recordSet.m_strSort.Format("[TraySerial]");
	try
	{
		recordSet.Open();
	}
	catch (CDBException* e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();
		return FALSE;
	}

	CString strTemp;

	if(!recordSet.IsBOF() && !recordSet.IsEOF())
	{
		while(!recordSet.IsEOF())
		{
			recordSet.Delete();
			recordSet.MoveNext();
		}
	}
*/	
/*	recordSet.m_strFilter.Format("");
	recordSet.m_strSort.Format("[TraySerial]");
	recordSet.Requery();
	
	int nNum = 0;
	int nNextTraySerial = 1;


	while(!recordSet.IsEOF())
	{
		recordSet.MoveNext();
	}
	nNum = recordSet.GetRecordCount();
	if(nNum <= 0)
	{
		nNextTraySerial =1;
	}
	else
	{
		recordSet.MovePrev();
		nNextTraySerial= recordSet.m_TraySerial+1;
	}
	
	try
	{
		recordSet.AddNew();
		recordSet.m_TraySerial = nNextTraySerial;
		recordSet.m_Description = descrip;
		recordSet.m_TrayName = trayName;
		recordSet.m_UserName = UserName;
		recordSet.m_TrayNo =  trayNo;
		recordSet.m_TrayType = trayType;

		recordSet.Update();
		recordSet.Close();
	}
	catch (CDBException* e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();
		return FALSE;
	}
*/
/*	CCellStateRecordSet	recordSet1;
	recordSet1.m_strFilter.Format("[TraySerial] = %ld", nNextTraySerial);
	recordSet1.m_strSort.Format("[ChNo]");

	try
	{
		recordSet1.Open();
	}
	catch (CDBException* e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();
		return FALSE;
	}

	//이미 등록되어 있는 상태가 있으면 지우고 다시 등록 
	while(!recordSet1.IsEOF())
	{
		try
		{
			recordSet1.Delete();
			recordSet1.MoveNext();
		}
		catch (CDBException* e)
		{
			AfxMessageBox(e->m_strError);
			e->Delete();
			recordSet1.Close();
			return FALSE;
		}
	}

	for(int i=0; i<EP_MAX_CH_PER_MD; i++)
	{
		recordSet1.AddNew();
		recordSet1.m_TraySerial = nNextTraySerial;
		recordSet1.m_ChNo = i;
		recordSet1.m_CellCode = EP_CODE_NORMAL;
		recordSet1.m_CellGrade = "";
		recordSet1.Update();
	}
	recordSet1.Close();
*/
	InitData();

	m_lTraySerial = nTraySerial;
	m_strTrayNo = trayNo;
	m_strTrayName = trayName;
	m_strUserName = UserName;
	m_strDescription = descrip;
	return TRUE;
}

BOOL CTray::DeleteTray(CString strDestTrayNo)
{
	CString strTemp;
	strTemp = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");	//Get DataBase Folder(CTSMon Folde)
	if(strTemp.IsEmpty())
	{
		return FALSE;
	}

	strTemp = strTemp + "\\" +FORM_SET_DATABASE_NAME;
		
	CDaoDatabase  db;
	try
	{
		db.Open(strTemp);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	
	//1.  Data Base reset
	CString strSQL,strWhere;
	if(!strDestTrayNo.IsEmpty())
	{
		strWhere.Format(" WHERE TrayNo = '%s'", strDestTrayNo);
	}
	else
	{
		strWhere.Format(" WHERE TraySerial = %ld", m_lTraySerial);
	}

	strSQL = "DELETE FROM Tray" + strWhere;
	db.Execute(strSQL);
	db.Close();
	return TRUE;

/*	CTrayRecordSet	recordSet;
	if(!strDestTrayNo.IsEmpty())
	{
		recordSet.m_strFilter.Format("[TrayNo] = '%s'", strDestTrayNo);
	}
	else
	{
		recordSet.m_strFilter.Format("[TraySerial] = %ld", m_lTraySerial);
	}
	recordSet.m_strSort.Format("[ID]");
	
	try
	{
		recordSet.Open();
	}
	catch (CDBException* e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();
		return FALSE;
	}
	
	if(recordSet.IsBOF() || recordSet.IsEOF())
	{
		recordSet.Close();
		return FALSE;
	}

	while(!recordSet.IsEOF())
	{
		recordSet.Delete();
		recordSet.MoveNext();
	}
	recordSet.Close();
	return TRUE;
*/
}

BOOL CTray::IsNewProcedureTray()
{
	if(lTestKey <= 0 || m_bNewProcTray)		return TRUE;

	return FALSE;
}

void CTray::SetNewTray()
{
	lTestKey = 0;
	m_lTraySerial = 0;
	m_strTrayNo.Empty();
	m_strTrayName.Empty();
	m_strUserName.Empty();
	m_strDescription.Empty();
}

BOOL CTray::UpdateStartData()
{
	CString strTemp;
	strTemp = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");	//Get DataBase Folder(CTSMon Folde)
	if(strTemp.IsEmpty())
	{
		return FALSE;
	}

	strTemp = strTemp + "\\" +FORM_SET_DATABASE_NAME;
		
	CDaoDatabase  db;
	try
	{
		db.Open(strTemp);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	
	//1.  Data Base reset
	CString strSQL,strWhere;
	strWhere.Format(" WHERE TraySerial = %ld", m_lTraySerial);
	strSQL.Format("UPDATE Tray SET JigID = '%s', LotNo ='%s', TestSerialNo = '%s', TestDateTime = '%s', CellNo = %d, NormalCount = %d, FailCount = %d, strOperatorID ='%s', lInputCellCount = %d, ModuleID = %d, GroupIndex =%d", 
								strJigID,  
								strLotNo, 
								strTestSerialNo,
								testDateTime.Format(),
								lCellNo,
								nNormalCount,
								nFailCount,
								strOperatorID,
								lInputCellCount,
								ModuleID,
								GroupIndex
							);

	strSQL += strWhere;
	db.Execute(strSQL);

	db.Close();
	return TRUE;

/*	CString strTemp;
	CTrayRecordSet	recordSet;
	recordSet.m_strFilter.Format("[TraySerial] = %ld", m_lTraySerial);
	recordSet.m_strSort.Format("[ID]");
	
	try
	{
		recordSet.Open();
	}
	catch (CDBException* e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();
		return	FALSE;
	}

	if(recordSet.IsEOF() || recordSet.IsBOF())
	{
		recordSet.Close();
		return FALSE;
	}

	recordSet.Edit();

	recordSet.m_JigID = strJigID;
//	recordSet.m_TeskID = lTeskID;
//	recordSet.m_ModelKey = lModelKey;
//	recordSet.m_TestKey = lTestKey;
//	recordSet.m_StepIndex = lStepKey;
	recordSet.m_LotNo = strLotNo;
	recordSet.m_TestSerialNo = strTestSerialNo;
	recordSet.m_TestDateTime = testDateTime;
	recordSet.m_CellNo = lCellNo;
	recordSet.m_NormalCount = nNormalCount;
	recordSet.m_FailCount = nFailCount;
	recordSet.m_OperatorID = strOperatorID;
	recordSet.m_InputCellNo = lInputCellCount;
	recordSet.m_ModuleID = ModuleID;
	recordSet.m_GroupIndex = GroupIndex;

	try
	{
		recordSet.Update();
		recordSet.Close();
	}
	catch (CDBException* e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();
		return FALSE;
	}
	return TRUE;
*/
}


/*
//속도 향상을 위해 WriteTrayData()를 Record Set을 이용하지 않고 Query 처리
//2003/10/20	
//KBH
BOOL CTray::WriteTrayDataA()
{
	if(!m_db.IsOpen())	return FALSE;

	CString strSQL, strUpdate, strWhere, strQuery, strTemp;
	
	m_db.BeginTrans();

	strSQL = "UPDATE Tray SET ";
	strTemp.Format("JigID = '%s', TeskID = %d, ModelKey = %d, TestKey = %d, ", 
					strJigID, lTeskID, lModelKey,  lTestKey);
	strUpdate += strTemp;
	strTemp.Format("StepIndex = %d, LotNo = '%s', TestSerialNo = '%s',", 
					lStepKey,   strLotNo,  strTestSerialNo);
	strUpdate += strTemp;
	strTemp.Format("TestDateTime = '%s', CellNo = %d, NormalCount = %d, FailCount = %d, OperatorID = %d,", 
					testDateTime.Format(), lCellNo, nNormalCount, nFailCount, strOperatorID);
	strUpdate += strTemp;
	strTemp.Format("InputCellNo = %d, ModuleID = %d, GroupIndex = %d ", 
					lInputCellCount, ModuleID, GroupIndex);
	strUpdate += strTemp;

	strWhere.Format("WHERE TraySerial = %d", lTraySerial);

	strQuery = strSQL + strUpdate + strWhere;
	m_db.ExecuteSQL( strQuery );

	strSQL = "UPDATE CellState SET ";
	for(int ch = 0 ; ch < 128; ch++)
	{
		strTemp.Format("%c", cellGradeCode[ch]);
		strUpdate.Format("CellCode = %d, CellGrade = '%s' ", cellCode[ch], strTemp);
		strWhere.Format("WHERE TraySerial = %ld AND ChNo = %d", lTraySerial, ch);

		strQuery = strSQL + strUpdate + strWhere;
		m_db.ExecuteSQL( strQuery );
	}
	
	m_db.CommitTrans();
	return TRUE;
}
*/

/*
BOOL CTray::LoadTrayDataA(CString strLoadTrayNo)
{
	if(!m_db.IsOpen())	return FALSE;

/*	CString strSQL;

	if(strLoadTrayNo.IsEmpty())
	{
		strSQL.Format("SELECT * FROM Tray WHERE TraySerial = %d", lTraySerial);
	}
	else
	{
		strSQL.Format("SELECT * FROM Tray WHERE TrayNo = '%s'", strLoadTrayNo);
	}

	CRecordset rs(&m_db);
	CDBVariant va;
	CString strQuery;

	rs.Open( CRecordset::forwardOnly, strSQL);
	
	rs.GetFieldValue(va, );
	rs.GetFieldValue((short)j , val, SQL_C_SLONG);
	lTraySerial = recordSet.m_TraySerial;
	strJigID = recordSet.m_JigID;
	lTeskID = recordSet.m_TeskID;
	m_nTrayCellType = recordSet.m_TrayType;
	while(!rs.IsEOF())
	{
		rs.MoveNext();
	}
	rs.Close();

*/

/*	CTrayRecordSet	recordSet;
	if(strLoadTrayNo.IsEmpty())
	{
		recordSet.m_strFilter.Format("[TraySerial] = %ld", lTraySerial);
	}
	else
	{
		recordSet.m_strFilter.Format("[TrayNo] = '%s'", strLoadTrayNo);
	}
	recordSet.m_strSort.Format("[TraySerial]");
	
	try
	{
		recordSet.Open();
	}
	catch (CDBException* e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();
		return FALSE;
	}
	
	if(recordSet.IsBOF() || recordSet.IsEOF())
	{
		recordSet.Close();
		return FALSE;
	}
	
	lTraySerial = recordSet.m_TraySerial;
	strJigID = recordSet.m_JigID;
	lTeskID = recordSet.m_TeskID;
	m_nTrayCellType = recordSet.m_TrayType;

	registedTime.SetDateTime(	recordSet.m_RegistedTime.GetYear(), 
										recordSet.m_RegistedTime.GetMonth(),
										recordSet.m_RegistedTime.GetDay(), 
										recordSet.m_RegistedTime.GetHour(), 
										recordSet.m_RegistedTime.GetMinute(), 
										recordSet.m_RegistedTime.GetSecond()
									); 
	strTrayNo = recordSet.m_TrayNo;
	strTrayName = recordSet.m_TrayName;
	strUserName = recordSet.m_UserName;
	strDescription = recordSet.m_Description;
	lModelKey = recordSet.m_ModelKey;		
	lTestKey = recordSet.m_TestKey;	
	lStepKey= recordSet.m_StepIndex;
	strLotNo = recordSet.m_LotNo;					
	strTestSerialNo = recordSet.m_TestSerialNo;	

	testDateTime.SetDateTime(			recordSet.m_TestDateTime.GetYear(), 
										recordSet.m_TestDateTime.GetMonth(),
										recordSet.m_TestDateTime.GetDay(), 
										recordSet.m_TestDateTime.GetHour(), 
										recordSet.m_TestDateTime.GetMinute(), 
										recordSet.m_TestDateTime.GetSecond()
									); 

	lCellNo = recordSet.m_CellNo;
	nNormalCount = recordSet.m_NormalCount;
	nFailCount = recordSet.m_FailCount;
	strOperatorID = recordSet.m_OperatorID;
	lInputCellCount = recordSet.m_InputCellNo;
	ModuleID = recordSet.m_ModuleID;
	GroupIndex = recordSet.m_GroupIndex;

	recordSet.Close();

	CCellStateRecordSet	recordSet1;
	recordSet1.m_strFilter.Format("[TraySerial] = %ld", lTraySerial);
	recordSet1.m_strSort.Format("[ChNo]");

	try
	{
		recordSet1.Open();
	}
	catch (CDBException* e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();
		return FALSE;
	}

	while(!recordSet1.IsEOF())
	{
		cellCode[recordSet1.m_ChNo] = (BYTE)recordSet1.m_CellCode;
		if(recordSet1.m_CellGrade.GetLength() > 0)
		{
			cellGradeCode[recordSet1.m_ChNo] = (BYTE)recordSet1.m_CellGrade[0];
		}
		else
		{
			cellGradeCode[recordSet1.m_ChNo] = 0;
		}
		recordSet1.MoveNext();
	}
	recordSet1.Close();


	return TRUE;
}

*/


void CTray::SetTrayData(CTray &tray)
{
	m_lTraySerial = tray.m_lTraySerial;
	strJigID = tray.strJigID;
	m_registedTime = tray.m_registedTime;
	m_strTrayNo = tray.m_strTrayNo;
	m_strTrayName = tray.m_strTrayName;
	m_strUserName = tray.m_strUserName;
	m_strDescription = tray.m_strDescription;
	lModelKey = tray.lModelKey;					//Battery Model DB Primary Key
	lTestKey = tray.lTestKey;					//Test	Paramary Key
	strLotNo = tray.strLotNo;					//사용자 입력 Lot No  Max 31Byte
	strTestSerialNo = tray.strTestSerialNo;			//현재 공정에 대한 고유 번호 23Byte(날짜+Random()수를 이용한 고유 번호)
	testDateTime = tray.testDateTime;
	lCellNo = tray.lCellNo;
	lInputCellCount = tray.lInputCellCount;
	nNormalCount = tray.nNormalCount;
	nFailCount = tray.nFailCount;
//	strCellCode = tray.strCellCode;					//128
//	strCellGradeCode = tray.strCellCode;
	strOperatorID = tray.strOperatorID;
	memcpy(cellCode, tray.cellCode, sizeof(cellCode));	//256
	memcpy(cellGradeCode, tray.cellGradeCode, sizeof(cellGradeCode));	//256
	ModuleID = tray.ModuleID;
	GroupIndex = tray.GroupIndex;
	m_bNewProcTray = tray.m_bNewProcTray;
//	m_nTrayCellType = tray.m_nTrayCellType;
	m_strFileName = tray.m_strFileName;	
}

BOOL	CTray::GetBcrRead()					
{	
	if(!m_strTrayNo.IsEmpty() && m_bBCRScaned)	return TRUE;	

	return FALSE;
}

BOOL CTray::IsWorkingTray()
{
	if(lInputCellCount > 0 && m_strTrayNo.IsEmpty() == FALSE)
		return TRUE;

	return FALSE;
}

//20090920 KBH
PNE_RESULT_CELL_SERIAL * CTray::GetResultCellSerial()
{
	return &m_ResultCellSerial;
}


CString CTray::GetCellSerial(int nChIndex)
{
	CString serial;
	if(nChIndex >= 0 && nChIndex < EP_MAX_CH_PER_MD) 
	{
		serial =  m_ResultCellSerial.szCellNo[nChIndex];
	}

	return serial;
}

void	CTray::SetCellSerial(int nChIndex, CString str)	
{	
	if(nChIndex >= 0 && nChIndex < EP_MAX_CH_PER_MD) 
	{
		sprintf(m_ResultCellSerial.szCellNo[nChIndex], "%s", str.Left(31));
	}
}
