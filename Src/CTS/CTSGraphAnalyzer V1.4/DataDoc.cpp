// DataDoc.cpp : implementation file
//

#include "stdafx.h"
#include "CTSGraphAnal.h"
#include "DataDoc.h"

#include "MainFrm.h"
#include "FolderDialog.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDataDoc

IMPLEMENT_DYNCREATE(CDataDoc, CDocument)

CDataDoc::CDataDoc()
{
	LanguageinitMonConfig();
//	m_strDataPath = AfxGetApp()->GetProfileString("Control", "ResultDataPath", NULL);
}
CDataDoc::~CDataDoc()
{
	if( TEXT_LANG != NULL )					
	{					
		delete[] TEXT_LANG;					
		TEXT_LANG = NULL;					
	}
}

BOOL CDataDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	
//	m_strDataPath = AfxGetApp()->GetProfileString("Control", "ResultDataPath", NULL);
//	m_Data.QueryData(m_strDataPath+"?");

	if(SelectTestDlg()== FALSE)		return FALSE;

	return TRUE;
}




BEGIN_MESSAGE_MAP(CDataDoc, CDocument)
	//{{AFX_MSG_MAP(CDataDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDataDoc diagnostics

#ifdef _DEBUG
void CDataDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CDataDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CDataDoc serialization

void CDataDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CDataDoc commands

CStringList * CDataDoc::GetSelFileList()
{
	return &m_strFileList;
}


void CDataDoc::SetTestList(CStringList *pstrList)
{
	ASSERT(pstrList);

	m_strFileList.RemoveAll();

	CString strFileName;
	POSITION pos = pstrList->GetHeadPosition();
	while(pos)
	{	
		strFileName = pstrList->GetNext(pos);
		m_strFileList.AddTail(strFileName);
	}
}

BOOL CDataDoc::GetCellLastState(CString strChPath)
{
	return m_ChData.IsOKCell();
}

//선택 파일을 Load한다.
BOOL CDataDoc::SetChPath(CString strChPath)
{
	m_ChData.SetPath(strChPath);
	return m_ChData.Create(FALSE);
}

CChData * CDataDoc::GetChData()
{
	return &m_ChData;
}

BOOL CDataDoc::SelectTestDlg()
{
/*	CResultTestSelDlg	dlg;

	dlg.SetTestList(GetSelFileList());
	if( dlg.DoModal() == IDCANCEL)		return FALSE;
	
//	SetDataPath(dlg.m_strFileFolder);	
*/

	CString str = AfxGetApp()->GetProfileString("Control", "ResultDataPath", "");
	CFolderDialog dlg(str);
	if( dlg.DoModal() == IDOK)
	{
		str = dlg.GetPathName();
		CStringList strFolderList;
		strFolderList.AddTail(str);
		SetTestList(&strFolderList);

		AfxGetApp()->WriteProfileString("Control", "ResultDataPath", str);

		return TRUE;
	}		

	return FALSE;
}
BOOL CDataDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
//	if (!CDocument::OnOpenDocument(lpszPathName))
//		return FALSE;
	
	// TODO: Add your specialized creation code here
	CStringList selDataFolerList;
	CString str;
	str = ((CMainFrame *)AfxGetMainWnd())->m_strCopyMsgString;

	if(!str.IsEmpty())
	{
		//여러 폴더명 분리
		CStringList selFileList;
		CString strPath;
		int p1=0,p2=0;
		while(TRUE)
		{
			// '\n'로 분리된 각 folder의 Path를 얻어서
			p2 = str.Find('\n',p1);
			if(p2 < 0) break;
			strPath = str.Mid(p1,p2-p1);
			p1=p2+1;

			selFileList.AddTail(strPath);
		}

		//CString strChName;
		BOOL bSameFlag = FALSE;
		POSITION pos, pos2;
		//각 이름을 Foler와 Channel 번호로 분리한다.
		for(int i =0; i<selFileList.GetCount(); i++)
		{
			pos = selFileList.GetHeadPosition();
			while(pos)
			{
				strPath = selFileList.GetNext(pos);
				//foler와 채널을 분리
				{
					//strPath => Full Path Name\\M01C02 형태
					str = strPath.Left(strPath.ReverseFind('\\'));				//Full Path Name
					//strChName = strPath.Mid(strPath.ReverseFind('\\')+1);		//M01C02
					
					//선택 파일의 폴더 리스트에서 동일 폴더가 이미 있는지 확인
					pos2 = selDataFolerList.GetHeadPosition();
					bSameFlag = FALSE;
					while(pos2)
					{
						if(str == selDataFolerList.GetNext(pos2))
						{
							 bSameFlag = TRUE;
							//동일 폴더가 있을 경우 채널 번호만 추가 
							 break;
						}
					}
					//동일 폴더가 있지 않으면 새롭게 추가한다.
					if( bSameFlag == FALSE)
					{
						selDataFolerList.AddTail(str);			//폴더 추가
					}
				}
			}
		}

		SetTestList(&selDataFolerList);
	}
	//lpszPathName Null로 올 수 없음 
/*	if(lpszPathName == NULL || selDataFolerList.GetCount() == 0 )
	{
		if(SelectTestDlg()== FALSE)		return FALSE;
	}
*/

	return TRUE;
}

BOOL CDataDoc::DeleteFolder(CString strFolder)
{
	if(strFolder.IsEmpty())		return FALSE;

	BOOL	bReturn = TRUE;
	CFileFind finder;
	char szFrom[_MAX_PATH];
	ZeroMemory(szFrom, _MAX_PATH);	//Double NULL Terminate

	//pFrom에 Full Path 제공하고 
	//pTo에 Path명을 제공하지 않고 
	//fFlags Option에 FOF_ALLOWUNDO 부여하고 
	//Delete 동작 하면 휴지통으로 삭제 된다.
	LPSHFILEOPSTRUCT lpFileOp = new SHFILEOPSTRUCT;
	ZeroMemory(lpFileOp, sizeof(SHFILEOPSTRUCT));
	lpFileOp->hwnd = NULL; 
	lpFileOp->wFunc = FO_DELETE ; 
	lpFileOp->pFrom = szFrom; 
	lpFileOp->pTo = NULL; 
	lpFileOp->fFlags = FOF_FILESONLY|FOF_NOCONFIRMATION|FOF_ALLOWUNDO ;//FOF_NOCONFIRMATION | FOF_NOERRORUI | FOF_FILESONLY ; 
	lpFileOp->fAnyOperationsAborted = FALSE; 
	lpFileOp->hNameMappings = NULL; 
	lpFileOp->lpszProgressTitle = NULL; 

	sprintf(szFrom, "%s", strFolder);

	//삭제 파일을 휴지통으로 보냄 			
	BeginWaitCursor();
	if(SHFileOperation(lpFileOp) != 0)		//Move File
	{
		AfxMessageBox(strFolder+TEXT_LANG[0]); //000596
		bReturn = FALSE;
	}
	EndWaitCursor();

	//파일 삭제시 
//	// build a string with wildcards
//	CString strWildcard(strFolder);
//	strWildcard += _T("\\*.*");
//
//	// start working for files
//	BOOL bWorking = finder.FindFile(strWildcard);
//
//	BeginWaitCursor();
//	while (bWorking)
//	{
//		bWorking = finder.FindNextFile();
//
//		// skip . and .. files; otherwise, we'd
//		// recur infinitely!
//
//		if (finder.IsDots())
//		   continue;
//
//		// if it's a directory, recursively search it
//
//		if (finder.IsDirectory())
//		{
//			if(DeleteFolder(finder.GetFilePath()) == FALSE)
//			{
//				EndWaitCursor();
//				delete lpFileOp;
//				lpFileOp = NULL;
//				finder.Close();
//				return FALSE;
//			}
//		}
//		else
//		{
//			  CString strFileName = finder.GetFilePath();
//			//바로 삭제 
///*			if(_unlink((LPSTR)(LPCTSTR)strTemp) != 0)
//			{
//				strTemp =  strTemp+" 파일을 삭제 할 수 없습니다."; 
//				AfxMessageBox(strTemp);
//				continue;
//			}
//*/			
//			ZeroMemory(szFrom, _MAX_PATH);	//Double NULL Terminate
//			sprintf(szFrom, "%s", strFileName);
//
//			//삭제 파일을 휴지통으로 보냄 			
//			if(SHFileOperation(lpFileOp) != 0)		//Move File
//			{
//				EndWaitCursor();
//				delete lpFileOp;
//				lpFileOp = NULL;
//				finder.Close();
//				AfxMessageBox(strFileName+"를 삭제할 수 없습니다.\n");
//				return FALSE;
//			}
//		}
//	}
//	EndWaitCursor();
//
//	//자기 자신 폴더 삭제 
//	{
//		
//	}
//
//	delete lpFileOp;
//	lpFileOp = NULL;
//	
//	finder.Close();
	return bReturn;
}
/*
CString CDataDoc::GetExcelPath()
{
	CString strExcelPath = AfxGetApp()->GetProfileString(REG_CONFIG_SECTION,"Excel Path");
	CFileFind finder;
	if(finder.FindFile(strExcelPath) == FALSE)
	{
		AfxMessageBox("Excel 위치가 설정되어 있지 않습니다. 위치를 지정하여 주십시요.");
		CFileDialog pDlg(TRUE, "exe", strExcelPath, OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, "exe file(*.exe)|*.exe|All Files(*.*)|*.*|");
		pDlg.m_ofn.lpstrTitle = "Excel 파일 위치";
		if(IDOK != pDlg.DoModal())
		{
			return	"";
		}
		strExcelPath = pDlg.GetPathName();
		AfxGetApp()->WriteProfileString(REG_CONFIG_SECTION,"Excel Path", strExcelPath);
	}
	return strExcelPath;
}
*/

/*
float CDataDoc::CalImpedance(CString &strName, float fTime1, float fTime2)
{
	FILE *fp = fopen(strName, "rt");
	if(fp)
	{
		char szBuff[64];
		long lIndex1 = 0, lIndex2 = 0;
		float fPrevTime = -1.0f, fVtg1 = 0.0f, fVtg2 = 0.0f, fCrt1 = 0.0f, fCrt2 = 0.0f;
		float fStepTime, fVoltage, fCurrent, fCapacity, fWattHour;

#ifdef _EDLC_TEST_SYSTEM
		if(fscanf(fp, "%s,%s,%s,", szBuff, szBuff, szBuff) > 0)	//Skip Header
		{
			while(fscanf(fp, "%f,%f,%f,", &fStepTime, &fVoltage, &fCurrent) > 0)
			{
#else
		if(fscanf(fp, "%s,%s,%s,%s,%s", szBuff, szBuff, szBuff, szBuff, szBuff) > 0)	//Skip Header
		{
			while(fscanf(fp, "%f,%f,%f,%f,%f", &fStepTime, &fVoltage, &fCurrent, &fCapacity, &fWattHour) > 0)
			{
#endif
				if(fPrevTime < fStepTime && fStepTime <= fTime1)
				{
					lIndex1++;
					fVtg1 = fVoltage;
					fCrt1 = fCurrent;
				}

				if(fPrevTime < fStepTime && fStepTime <= fTime2)
				{
					lIndex2++;
					fVtg2 = fVoltage;
					fCrt2 = fCurrent;
				}
			}
		}
		fclose(fp);
		if(lIndex1 < lIndex2 && fCrt2 != fCrt1)
		{
			strName.Format("(%d, %d):R=(%.3f-%.3f)/(%.3f-%.3f)=%fOhm", lIndex1, lIndex2, fVtg2, fVtg1, fCrt2, fCrt1, (fVtg2-fVtg1)/(fCrt2-fCrt1));
			return  (fVtg2-fVtg1)/(fCrt2-fCrt1)*1000.0f;
		}
	}
	return -1.0f;
}
*/

float CDataDoc::CalImpedance(CString &strFile, float fTime1, float fTime2)
{
	CTextParsing dataSheet;
	if(dataSheet.SetFile(strFile) == FALSE)
	{
		return -1.0f;
	}

	BOOL bUseFitLine = 	AfxGetApp()->GetProfileInt("Settings", "UseFitLine", FALSE);

	float fVData1, fIData1;
	float fVData2, fIData2;
	int nDataIndex1, nDataIndex2;

	nDataIndex1 = FindPointData(&dataSheet, fTime1, fVData1, fIData1, bUseFitLine, TRUE);
	if(nDataIndex1 < 0)
	{
		AfxMessageBox(TEXT_LANG[1]); //000597
		return -1.0f;
	}

	nDataIndex2 = FindPointData(&dataSheet, fTime2, fVData2, fIData2, bUseFitLine, FALSE);
	if(nDataIndex2 < 0)
	{
		AfxMessageBox(TEXT_LANG[2]); // 000598
		return -1.0f;
	}

	float fResist = 0.0f;
	float fDeltaI, fDeltaV;
	fDeltaI = fIData1-fIData2;
	fDeltaV = fVData1-fVData2;
	if(fDeltaI != 0.0f)
	{
		fResist = fDeltaV/fDeltaI*1000.0f;
	}

	CString str, str1;
	str.Format("%.3f", fVData1);
	str.Format("%.3f", fVData2);
	str.Format("%.3f", fIData1);
	str.Format("%.3f", fIData2);
	str.Format("%.3f", fResist);

	strFile.Format("V(%.3f, %.3f), I(%.3f, %.3f), R : %f", fVData1, fVData2, fIData1, fIData2, fResist);


	return fResist;
}

//주어진 파일에서 ftime의 data에서 전압(fData1)과 전류(fData2)를 구한다.
int CDataDoc::FindPointData(CTextParsing *pSheet, float fTime, float &fData1, float &fData2, BOOL bUseFitLine, BOOL bUsePrevData)
{
	fData1 = 0.0f;
	fData2 = 0.0f;

	int nPrevExcludeCount = AfxGetApp()->GetProfileInt("Settings", "PrevExCount", 0);
	int nPrevIncludeCount = AfxGetApp()->GetProfileInt("Settings", "PrevInCount", 100);
	int nAfterExcludeCount = AfxGetApp()->GetProfileInt("Settings", "AfterExCount", 0);
	int nAfterIncludeCount = AfxGetApp()->GetProfileInt("Settings", "AfterInCount", 100);
	
	int nTot = pSheet->GetRecordCount();

	CString strTemp;

	//Get Time Data
	int nTimeIndex = 0;
	int nVoltageindex = 1;
	int nCurrentIndex = 2;
	float *pfDataT, *pfDataV, *pfDataI;

	pfDataT = pSheet->GetColumnData(nTimeIndex);	
	pfDataV = pSheet->GetColumnData(nVoltageindex);		
	pfDataI = pSheet->GetColumnData(nCurrentIndex);		

	//Find time index
	if(nTot < 1)	return -1;
	if( fTime < pfDataT[0] || pfDataT[nTot-1] < fTime)		return -1;
	int index = 0;
	for(int i=1; i<nTot; i++)
	{
		if(pfDataT[i-1] <= fTime && fTime < pfDataT[i])
		{
			index = i-1;
			break;
		}
	}

	if(bUseFitLine == FALSE)
	{
		//Voltage Data
		fData1 = pfDataV[index];	
		//Current Data
		fData2 = pfDataI[index];
	}
	else
	{
		float a = 0, b=0;
		//최대 근사 1차 방정식을 구한다.(y=aX+b)
		//sigma(y) = n*b+a*sigma(x)
		//sigma(x*y) = b*sigma(x)+a*sigma(x*x)
		// a = (n*sigma(x*y)-sigma(x)*sigma(y))/(n*sigma(x*x)-sigma(x)*sigma(x))
		// b = (sigma(y)-a*sigma(x))/n

		int n = 0;
		int nDataIndex = 0;
		//이전 Data로 현재값 유추 
		if(bUsePrevData)
		{
			n = index+1-nPrevExcludeCount;
			if(n < 1)
			{
				return -1;
			}
			if(n > nPrevIncludeCount)	n = nPrevIncludeCount;

			float fTime2 = 0.0f;
			float fSigmaXY = 0.0f, fSigmaX = 0.0f, fSigmaY = 0.0f, fSigmaXX = 0.0f;
			//voltage
			if(n == 1)
			{
				a = 0;
				b = pfDataV[index-nPrevExcludeCount];
			}
			else
			{
				for(int i =0; i<n; i++)
				{
					nDataIndex = index-i-nPrevExcludeCount;
					//1. sigma(x*y)
					fSigmaXY += (pfDataT[nDataIndex]*pfDataV[nDataIndex]);
					//2. sigma(x)
					fSigmaX += pfDataT[nDataIndex];
					//3. sigma(y)
					fSigmaY += pfDataV[nDataIndex];
					//4. sigma(x*x)
					fSigmaXX += pfDataT[nDataIndex]*pfDataT[nDataIndex];
					fTime2 = pfDataT[nDataIndex];
				}

				a = (n*fSigmaXY-fSigmaX*fSigmaY)/(n*fSigmaXX-fSigmaX*fSigmaX);
				b = (fSigmaY-a*fSigmaX)/n;

			}
			fData1 = a*fTime+b;

//			strTemp.Format("Y=%.3fX+%.3f", a, b);
//			m_wndGraph.AddLineAnnotation(0, fTime2, a*fTime2+b, fTime, fData1, strTemp);

			//Current
			fSigmaXY = 0.0f; fSigmaX = 0.0f; fSigmaY = 0.0f; fSigmaXX = 0.0f;
			if(n == 1)
			{
				a = 0;
				b = pfDataI[index-nPrevExcludeCount];
			}
			else
			{
				for(i =0; i<n; i++)
				{
					nDataIndex = index-i-nPrevExcludeCount;
					//1. sigma(x*y)
					fSigmaXY += (pfDataT[nDataIndex]*pfDataI[nDataIndex]);
					//2. sigma(x)
					fSigmaX += pfDataT[nDataIndex];
					//3. sigma(y)
					fSigmaY += pfDataI[nDataIndex];
					//4. sigma(x*x)
					fSigmaXX += pfDataT[nDataIndex]*pfDataT[nDataIndex];
					fTime2 = pfDataT[nDataIndex];
				}

				a = (n*fSigmaXY-fSigmaX*fSigmaY)/(n*fSigmaXX-fSigmaX*fSigmaX);
				b = (fSigmaY-a*fSigmaX)/n;
			}
			fData2 = a*fTime+b;
			strTemp.Format("Y=%.3fX+%.3f",a, b);
			TRACE("Crt1 : %s\n", strTemp);

//			m_wndGraph.AddLineAnnotation(0, fTime2, a*fTime2+b, fTime, fData2, strTemp);
//			m_wndGraph.AddLineAnnotation(fTime, fData2, fTime2, a*fTime2+b, strTemp);

		}
		else	//이후 Data로 현재값 유추
		{
			int startindex = index+nAfterExcludeCount;
			n = nTot-startindex;
			if(n < 1)
			{
				return -1;
			}
			if(n > nAfterIncludeCount)	n = nAfterIncludeCount;
			
			float fTime2 = 0.0f;
			float fSigmaXY = 0.0f, fSigmaX = 0.0f, fSigmaY = 0.0f, fSigmaXX = 0.0f;
			//voltage
			if(n ==1)
			{
				a = 0; 
				b = pfDataV[startindex];
			}
			else
			{
				for(int i =0; i<n; i++)
				{
					nDataIndex = startindex+i;
					//1. sigma(x*y)
					fSigmaXY += (pfDataT[nDataIndex]*pfDataV[nDataIndex]);
					//2. sigma(x)
					fSigmaX += pfDataT[nDataIndex];
					//3. sigma(y)
					fSigmaY += pfDataV[nDataIndex];
					//4. sigma(x*x)
					fSigmaXX += pfDataT[nDataIndex]*pfDataT[nDataIndex];
					fTime2 = pfDataT[nDataIndex];
					TRACE("%f\n", pfDataV[nDataIndex]);
				}

				a = (n*fSigmaXY-fSigmaX*fSigmaY)/(n*fSigmaXX-fSigmaX*fSigmaX);
				b = (fSigmaY-a*fSigmaX)/n;
			}
			fData1 = a*fTime+b;
//			strTemp.Format("Y=%.3fX+%.3f", a, b);
//			m_wndGraph.AddLineAnnotation(1, fTime, fData1, fTime2, a*fTime2+b, strTemp);

			//Current
			fSigmaXY = 0.0f; fSigmaX = 0.0f; fSigmaY = 0.0f; fSigmaXX = 0.0f;
			if(n ==1)
			{
				a = 0; 
				b = pfDataI[startindex];
			}
			else
			{
				for(i =0; i<n; i++)
				{
					nDataIndex = startindex+i;
					//1. sigma(x*y)
					fSigmaXY += (pfDataT[nDataIndex]*pfDataI[nDataIndex]);
					//2. sigma(x)
					fSigmaX += pfDataT[nDataIndex];
					//3. sigma(y)
					fSigmaY += pfDataI[nDataIndex];
					//4. sigma(x*x)
					fSigmaXX += pfDataT[nDataIndex]*pfDataT[nDataIndex];
					TRACE("%f %f\n", pfDataT[nDataIndex], pfDataI[nDataIndex]);
					fTime2 = pfDataT[nDataIndex];
				}

				a = (n*fSigmaXY-fSigmaX*fSigmaY)/(n*fSigmaXX-fSigmaX*fSigmaX);
				b = (fSigmaY-a*fSigmaX)/n;
			}
			fData2 = a*fTime+b;
			
			strTemp.Format("Y=%.3fX+%.3f", a, b);				
//			m_wndGraph.AddLineAnnotation(1, fTime, fData2, fTime2, a*fTime2+b, strTemp);
			TRACE("Crt2 : %s\n", strTemp);
//			m_wndGraph.AddLineAnnotation(fTime, fData2, fTime2, a*fTime2+b, strTemp);

		}
	}
	return index;
}



BOOL CDataDoc::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CDataDoc"), _T("TEXT_CDataDoc_CNT"), _T("TEXT_CDataDoc_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt];

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CDataDoc_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CDataDoc"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Languge error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}