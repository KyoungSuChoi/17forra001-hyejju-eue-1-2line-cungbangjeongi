#if !defined(AFX_RESULTFILELOADDLG_H__DB3FAE88_9A36_4CD9_BF6F_A234ECF61C11__INCLUDED_)
#define AFX_RESULTFILELOADDLG_H__DB3FAE88_9A36_4CD9_BF6F_A234ECF61C11__INCLUDED_

#include "MyGridWnd.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ResultFileLoadDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CResultFileLoadDlg dialog
//#include "Label.h"
#include "CTSAnalDoc.h"

class CResultFileLoadDlg : public CDialog
{
// Construction
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	CResultFileLoadDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CResultFileLoadDlg();

	BOOL FindResultFile(CString strFolder, BOOL bFindSubFolder = TRUE);
	BOOL InsertFormFileList(CString strFile);
	BOOL AddFileList(CString strFolder);
	BOOL SearchFile();
	CCTSAnalDoc *m_pDoc;
	BOOL WaitThreadClose();
	CString m_strSelFileName;
	CString GetSelFileName();
	CMyGridWnd m_wndFileListGrid;
	void InitFileListGrid();
	
//	HANDLE m_hThread;
	CWinThread* m_pThread;
	volatile BOOL m_bStopThreadFlag;
// Dialog Data
	//{{AFX_DATA(CResultFileLoadDlg)
	enum { IDD = IDD_RESULT_FILE_LOAD_DLG };
	CXPButton	m_btnOK;
	CXPButton	m_btnCancel;
	CXPButton	m_btnRefresh;
	CXPButton	m_btnFolderSel;
	CXPButton	m_btnDelFile;
	CXPButton	m_btnCpCpk;
	CXPButton	m_btnAllDataList;
	CLabel	m_totalFileCount;
	CString	m_strFileFolder;
	CString	m_strLotFilter;
	CString	m_strTrayFilter;
	BOOL	m_bTodayWork;
	CTime	m_fromTime;
	CTime	m_toTime;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CResultFileLoadDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CResultFileLoadDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnRefresh();
	afx_msg void OnDeleteFile();
	afx_msg void OnFolderSel();
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnReload();
	afx_msg void OnSelectAll();
	afx_msg void OnEditCopy();
	afx_msg void OnDelete();
	afx_msg void OnToDayButton();
	afx_msg void OnCheckToday();
	afx_msg void OnButtonCpcpk();
	afx_msg void OnButtonFail();
	//}}AFX_MSG
	afx_msg LONG OnGridDoubleClick(WPARAM wParam, LPARAM lParam);
	afx_msg LONG OnMovedCurrentCell(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRButtonClickedRowCol(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RESULTFILELOADDLG_H__DB3FAE88_9A36_4CD9_BF6F_A234ECF61C11__INCLUDED_)
