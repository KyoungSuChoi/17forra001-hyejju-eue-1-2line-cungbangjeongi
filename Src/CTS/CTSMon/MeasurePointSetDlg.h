#pragma once

// CMeasurePointSetDlg 대화 상자입니다.
#include "MeasurePoint.h"
#include "ListEditCtrl.h"
#include "afxcmn.h"

class CMeasurePointSetDlg : public CDialog
{
	DECLARE_DYNAMIC(CMeasurePointSetDlg)
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

public:
	CMeasurePointSetDlg(CMeasurePoint* pMeasPoint, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CMeasurePointSetDlg();

	CMeasurePoint *m_pMeasPoint;

	void LoadMeasureData();
	void SaveMeasureData();

	int m_nSelPresetNum;
	void InitLabel();
	BOOL fnLoadPreset( int nSelNum );
	BOOL fnSavePreset( int nSelNum );

	void fnUpdatePreset();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_MEASURE_POINT_SET_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();
	CListEditCtrl m_wndListPointV;
	CListEditCtrl m_wndListPointI;
	afx_msg void OnBnClickedBtnPreset1();
	afx_msg void OnBnClickedBtnPreset2();
	afx_msg void OnBnClickedBtnPreset3();
	afx_msg void OnBnClickedBtnPreset4();
	afx_msg void OnBnClickedBtnPresetSave();
	CLabel m_Label1;
	CListEditCtrl m_wndShuntT;
	CListEditCtrl m_wndShuntR;
	CListEditCtrl m_wndShuntSerial;
	CListEditCtrl m_wndListPointV2;
	CListEditCtrl m_wndListPointV3;
	CListEditCtrl m_wndListPointI2;
	CListEditCtrl m_wndListPointI3;
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedBtnPresetLoad();
};
