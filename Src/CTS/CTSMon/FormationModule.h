// FormationModule.h: interface for the CFormModule class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FORMATIONMODULE_H__E9C58EF5_681D_400D_8FE6_21F967329B89__INCLUDED_)
#define AFX_FORMATIONMODULE_H__E9C58EF5_681D_400D_8FE6_21F967329B89__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "FormChannel.h"

#define INSTALL_TRAY_PER_MD		8
#define INSTALL_TRAY_PER_MD_1	1

#if EP_MAX_TRAY_PER_MD < INSTALL_TRAY_PER_MD
#pragma message( "------------KBH Message-------------" )
#pragma message( "Intalled tray number define error!!!" )
#pragma message( "------------------------------------" )
#error Tray no Define Error
#endif

typedef struct TrayTypeData{
	WORD iTrayStyle;
	WORD iTrayCol;
	CString strName;
} TRAY_TYPE;


class CFormModule  
{
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

public:
	void	SetTrayTypeData(CString strTrayType);
	int		GetTotalInstallCh();
	int		GetFirstWorkingTrayNo();
	int		GetWrokingTrayCount();
	void	SetModuleID(int nModuleID, CString strModuleName);
	BOOL	UpdateLoadResultData(int nTrayIndex = -1);
	WORD	GetState(BOOL bDetail = FALSE);
	BOOL	SetState(EP_GP_STATE_DATA& _state);
	float	GetTotRunTime();
	float	GetStepTime();
	int		GetCellCountInTray();
	int		GetStartChIndex(int nTrayIndex);
	int		GetChInJig(int nJigIndex);
	BOOL	IsAllTrayReaded();
	void	ResetTestReserve();
	void	SetTestReserve(STR_CONDITION_HEADER &testInfo);
	BOOL	GetTestReserved();
	BOOL	GetBCRState();
	int		GetTotalJig();
	BOOL	IsComplitedStep(int nStepIndex, int nTrayIndex = 0);
	void	LoadResultData(int nTrayIndex = -1);
	BOOL	GetChannelStepData(STR_SAVE_CH_DATA &chSaveData,  int nChannelIndex,  int nTrayIndex = 0, int nStepIndex = -1);
	int		GetCurStepNo();
	BYTE	GetUserSelect(int nChCode, int nGradeCode);
	int		GetModuleID()	{	return m_nModuleID;		}
	BOOL	CreateTrayResultFile(int nTrayIndex = 0);
	BOOL	CreateTrayCellCheckResultFile( int nTrayIndex = 0, CString strResultFileName = _T("") );
	int		ReadEPFileHeader(int nFile, FILE *fp);
	BOOL	WriteEPFileHeader(int nFile, FILE *fp);

//	void	SetLineMode(int nMode)	{	m_nLineMode = nMode;	}
	int		GetLineMode();
	int		GetOperationMode();
	
	CString GetResultFileName(int nTrayIndex = 0)					{	return	m_TrayData[nTrayIndex].m_strFileName;	}
	void	SetResultFileName(CString strName, int nTrayIndex = 0)	{	m_TrayData[nTrayIndex].m_strFileName = strName;	}
	
	CTestCondition * GetCondition();
	CString GetIPAddress();
	BOOL RestoreLostData();
	BOOL RestoreLostData_new();		// 20131217 kky
	_MAPPING_DATA * GetSensorMap(int nNo, int nChIndex = 0);
	CFormChannel* GetChannelInfo(int nChIndex);
	STR_SAVE_CH_DATA ConvertChSaveData( int nModuleID, int nChIndex/*Tray Base Index*/, LPEP_CH_DATA pChData, CString strCapacityFormula = _T(""), CString strDCRFormula = _T("") );
	BOOL ConvertChSaveData(int nTrayIndex, int nChIndex, LPEP_CH_DATA pChData, LPSTR_SAVE_CH_DATA pChSaveData, CString strCapacityFormula = _T(""), CString strDCRFormula = _T("") );
	void ResetGroupData();
	long m_nGroupCount;
	void MakeGroupStruct(int nGroupCount);
	CFormModule(int nModuleID, CString strName);
	virtual ~CFormModule();

	COleDateTime & GetStepStartTime()				{	return m_OleStepStartTime;	}
	COleDateTime & GetRunStartTime()				{	return m_OleRunStartTime ;	}

	void	UpdateStartTime(COleDateTime *pStartT = NULL);
	void	UpdateStepStartTime();

	CString GetTestName()							{	return	m_Condition.GetTestName();	}
	long GetTestSequenceNo()						{	return	m_Condition.GetTestInfo()->lNo;	}

	CString GetLotNo(int nTrayIndex = 0)			{	return m_TrayData[nTrayIndex].strLotNo;		}
	CString GetTypeSel(int nTrayIndex = 0)				{	return m_TrayData[nTrayIndex].strTypeSel;		}
	CString GetOperatorID(int nTrayIndex = 0)		{	return m_TrayData[nTrayIndex].strOperatorID;	}
	CString GetTestSerialNo(int nTrayIndex = 0)		{	return m_TrayData[nTrayIndex].strTestSerialNo;	}
	CString GetTrayNo(int nTrayIndex = 0)			{	return m_TrayData[nTrayIndex].GetTrayNo();	}
	long	GetTraySerialNo(int nTrayIndex = 0)		{	return m_TrayData[nTrayIndex].GetTraySerialNo();	}
	CString GetTrayName(int nTrayIndex = 0)			{	return m_TrayData[nTrayIndex].GetTrayUserName();		}
	long	GetCellNo(int nTrayIndex = 0)			{	return m_TrayData[nTrayIndex].lCellNo;		}
	void	SetCellNo(int nCellNo, int nTrayIndex = 0)			{	m_TrayData[nTrayIndex].lCellNo = nCellNo;		}
	long	GetInputCellCount(int nTrayIndex = 0)	{	return m_TrayData[nTrayIndex].lInputCellCount;	}
	long	GetProcessSeqNo(int nTrayIndex = 0)			{	return m_TrayData[nTrayIndex].GetProcessSeqNo();			}
	int		GetSettingTypeSel()							{	return m_nSetTypeSel;			}	//20201215 KSJ
	void	SetSettingTypeSel(int nTypeSel)				{	m_nSetTypeSel = nTypeSel;	}	//20201215 KSJ
// 20210127 KSCHOI Add ReAging Enable Stage Function START
	BOOL	GetReAgingEnable()							{	return m_bReAgingEnable;	}
	void	SetReAgingEnable(BOOL bReAging)				{	m_bReAgingEnable = bReAging;}
// 20210127 KSCHOI Add ReAging Enable Stage Function END

	CTray * GetTrayInfo(int nTrayIndex = 0)		{	return &m_TrayData[nTrayIndex];	}

	BOOL ReadTestLogFile();
	BOOL ClearTestLogTempFile();
	BOOL UpdateTestLogTempFile();
	BOOL UpdateTestLogHeaderTempFile();
	BOOL WriteTestLogTempFile(CTestCondition *pCondition);
	BOOL ReadTrayTestLogFile(int nTrayIndex = 0);

//	EP_NVRAM_DATA			dataNVRam;
	int		ReadTestConditionFile(FILE *fp, STR_CONDITION *pCondition, char *fileID = NULL, char *fileVer = NULL);
	BOOL	SaveConditionFile(FILE *fp, CTestCondition *pCondition,  BOOL bSaveFileHeader = TRUE);

	//작업이 예약되어 있는지 여부 Flag
//	void	SetTestReserved(BOOL bSet = TRUE)	{	m_bTestReserved = bSet;	}
//	BOOL	GetTestReserved()					{	return m_bTestReserved;	}

	STR_CONDITION_HEADER  m_ReservedtestInfo;
	CString GetModuleName()						{	return m_strModuleName;		}
	
	CFormResultFile	 m_resultFile;		// CTSMon 에서 데이터 복구를 위해서 사용
	CFormResultFile	m_aResultData[INSTALL_TRAY_PER_MD];

	enum {sensorType1 = 0, sensorType2 = 1};

	UINT	m_iTrayTypeCount;		//ljb 2008-11-12  I/O 형 변환 가능 개수
	TRAY_TYPE *m_sTrayTypeData;		//ljb 2008-11-12  I/O 형 변환 구조체
	CString		m_strLotNo;	
protected:
	long m_nCellCountInTray;
	CString m_strModuleName;
	LONG m_nModuleID;
	BOOL ClearTrayTestLogFile(int nTrayIndex = 0);
	BOOL UpdateTrayTestLogHeaderFile(int nTrayIndex = 0);	
	BOOL UpdateTrayTestLogFile(int nTrayIndex = 0);
	BOOL WriteTrayTestLogFile(CTestCondition *pCondition, int nTrayIndex = 0);
	CString GetRemoteLocation(int nGroupNo, CString strTestSerial, CString strIp);
	BOOL RemoveCondition(STR_CONDITION *pCondition);
	COleDateTime	m_OleRunStartTime;
	COleDateTime	m_OleStepStartTime;
//	int m_nLineMode;
	CTestCondition	m_Condition;	
//	CString m_strResultFileName;

//	CWordArray	m_awSensorMap;
	_MAPPING_DATA	m_SensorMap1[EP_MAX_SENSOR_CH];
	_MAPPING_DATA	m_SensorMap2[EP_MAX_SENSOR_CH];

	void RemoveGroupStruct();

	CTray	m_TrayData[INSTALL_TRAY_PER_MD];

// 20210127 KSCHOI Add ReAging Enable Stage Function START
	int			m_bReAgingEnable;
	CString		m_csReAgingRejectValue;
// 20210127 KSCHOI Add ReAging Enable Stage Function END
	int			m_nSetTypeSel;			//20201215 ksj	//P : 가능 T : Disable
			
	CString		m_strOperatorID;
	
	int			m_nInputCellCnt;
};

#endif // !defined(AFX_FORMATIONMODULE_H__E9C58EF5_681D_400D_8FE6_21F967329B89__INCLUDED_)
