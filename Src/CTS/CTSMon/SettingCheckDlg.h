#pragma once

#include "afxwin.h"
#include "MyGridWnd.h"
#include "CTSMonDoc.h"

#define MAX_HW_FAULT_CNT		40
#define MAX_INTERNAL_FALG_CNT	40
#define MAX_TEMP_CNT			40

// CSettingCheckView 폼 뷰입니다.

class CSettingCheckDlg : public CDialog
{
public:
	CSettingCheckDlg(CWnd* pParent , CCTSMonDoc* pDoc);           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CSettingCheckDlg();

public:
	enum { IDD = IDD_SETTING_CHECK };


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:

	void initCtrl();
	void initGrid();
	void initLabel();
	void initFont();
	void initCombo();
	void initName();
	void initValue();
	void initGridNum();
	void ModuleConnected(int nModuleID);
	void ModuleDisConnected(int nModuleID);
	void SetCurrentModule(int nModuleID);
	void RecvData(int nModuleID);
	void UpdateAllData();
	void ClipboardChangeJustText();//클립보드내용을 문자만 남도록 변경
	CString GetTargetModuleName();
	

	CCTSMonDoc* m_pDoc;
	int m_nCurModuleID;
	int m_nMaxStageCnt;

	CMyGridWnd      m_gridInternalFlag;
	CMyGridWnd		m_gridTemp;
	CMyGridWnd		m_gridHwFaultSet;
	CLabel			m_labelHwFault;
	CLabel			m_labelInternalFlag;
	CLabel			m_labelTemp;

	CLabel			m_LabelViewName;
	CLabel			m_CmdTarget;

	CFont			m_Font;

	bool			m_bHwFaultEmpty[MAX_HW_FAULT_CNT];
	bool			m_bInternalFlagEmpty[MAX_INTERNAL_FALG_CNT];
	bool			m_bTempEmpty[MAX_TEMP_CNT];
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBtnRefresh();
	afx_msg void OnBnClickedBtnSaveName();
	afx_msg void OnEditCopy();
	afx_msg void OnEditPaste();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	CComboBox m_comboStageID;
	afx_msg void OnCbnSelchangeComboStageChange();
	afx_msg void OnBnClickedBtnEqualsCheck();
	afx_msg void OnBnClickedBtnRefresh2();
	afx_msg void OnBnClickedBtnSaveExcel();
};

