#if !defined(AFX_MODULEMODIFYDLG_H__DDAFA191_584E_435C_97EC_CEE0BD64A8A9__INCLUDED_)
#define AFX_MODULEMODIFYDLG_H__DDAFA191_584E_435C_97EC_CEE0BD64A8A9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ModuleModifyDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CModuleModifyDlg dialog

class CModuleModifyDlg : public CDialog
{
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();
	virtual ~CModuleModifyDlg();

// Construction
public:
	CModuleModifyDlg(CWnd* pParent = NULL);   // standard constructor
	
	int		m_nType;
	CString	m_strName;
	BOOL	m_bAddDlg;
	int		m_nModulePerRack;
	UINT	m_nModuleTrayType;
	BOOL	m_bUseGroupSet;
	BOOL	m_bUseRackIndex;
	CString m_strGroupName;
	CString m_strModuleName;

// Dialog Data
	//{{AFX_DATA(CModuleModifyDlg)
	enum { IDD = IDD_MODULE_MODIFY_DLG };
	CComboBox	m_ctrlTrayType;
	CComboBox	m_ctrlModuleType;
	float	m_flSpec;
	float	m_fVSpec;
	UINT	m_nTrayCol;
	BOOL	m_bUseModeChange;		
	UINT	m_nModuleID;
	//}}AFX_DATA
public:
	BOOL SetCurrentModuleInfo( UINT nModuleID );


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CModuleModifyDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CModuleModifyDlg)
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MODULEMODIFYDLG_H__DDAFA191_584E_435C_97EC_CEE0BD64A8A9__INCLUDED_)
