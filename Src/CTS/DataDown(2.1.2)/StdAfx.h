// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__AA831E5E_2873_4294_8423_804A11D8CF59__INCLUDED_)
#define AFX_STDAFX_H__AA831E5E_2873_4294_8423_804A11D8CF59__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxsock.h>		// MFC socket extensions
#include <afxdao.h>			// MFC DAO database classes
#include "resource.h"

#include <toolkit/secall.h>

#include "../../../Include/Formall.h"
#include "../../UtilityClass/FormUtility.h"

#include "../../../Include/IniParser.h"

extern int g_nLanguage;			
extern CString g_strLangPath;
enum LanguageType {LANGUAGE_KOR=0, LANGUAGE_ENG, LANGUAGE_CHI };


#define REG_SEC_NAME	"SaveSetting"

#define TIMER_ID_SHOWMODE		10
#define	TIMER_ID_AUTOSAVE		100
#define TIMER_ID_USERINTERFACE	200
#define TIMER_INTERVAL_SHOWMODE			100
#define	TIMER_INTERVAL_AUTOSAVE			1000
#define TIMER_INTERVAL_USERINTERFACE	1000

#ifdef _DEBUG
#pragma comment(lib, "../../../Lib/BFCommonD.lib")
#pragma message("Automatically linking with BFCommonD.lib By K.B.H ")
#else
#pragma comment(lib, "../../../Lib/BFCommon.lib")
#pragma message("Automatically linking with BFCommon.lib By K.B.H")
#endif	//_DEBUG

#ifdef _DEBUG
#pragma comment(lib, "../../../Lib/BFCtrlD.lib")
#pragma message("Automatically linking with BFCtrlD.lib By K.B.H ")
#else
#pragma comment(lib, "../../../Lib/BFCtrl.lib")
#pragma message("Automatically linking with BFCtrl.lib By K.B.H")
#endif	//_DEBUG



//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__AA831E5E_2873_4294_8423_804A11D8CF59__INCLUDED_)
