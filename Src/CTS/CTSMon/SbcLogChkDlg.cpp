// SbcLogChkDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "SbcLogChkDlg.h"


// CSbcLogChkDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSbcLogChkDlg, CDialog)

CSbcLogChkDlg::CSbcLogChkDlg(CCTSMonDoc *pDoc, CWnd* pParent /*=NULL*/)
	: CDialog(CSbcLogChkDlg::IDD, pParent)
{
	m_pDoc = pDoc;
	
	m_nLayOutCol = 0;
	m_nLayOutRow = 0;
	m_nMaxStageCnt = 0;
	m_nKeywordIndex = 0;
	
	m_bRackIndexUp = false;
}

CSbcLogChkDlg::~CSbcLogChkDlg()
{
}

void CSbcLogChkDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BTN_SEARCH, m_BtnSearch);
	DDX_Control(pDX, IDC_CSV_OUTPUT, m_BtnCSVConvert);
	DDX_Control(pDX, IDOK, m_BtnIDOK);
	DDX_Control(pDX, ID_DLG_LABEL_NAME, m_LabelName);
	DDX_Control(pDX, IDC_SELECT_KEYWORD, m_ctrlKeyword);
	DDX_Control(pDX, IDC_PROGRESS_GET_LOG, m_CtrlProgressGetLog);
	DDX_Control(pDX, IDC_DATETIME, m_ctrlDateTime);
}


BEGIN_MESSAGE_MAP(CSbcLogChkDlg, CDialog)
	ON_BN_CLICKED(IDC_BTN_SEARCH, &CSbcLogChkDlg::OnBnClickedBtnSearch)
	ON_BN_CLICKED(IDOK, &CSbcLogChkDlg::OnBnClickedOk)
//	ON_WM_SIZE()
ON_BN_CLICKED(IDC_CSV_OUTPUT, &CSbcLogChkDlg::OnBnClickedCsvOutput)
END_MESSAGE_MAP()


// CSbcLogChkDlg 메시지 처리기입니다.
BOOL CSbcLogChkDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_bRackIndexUp = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "RackIndexUp", TRUE);	
	
	m_nMaxStageCnt = m_pDoc->GetInstalledModuleNum();
	
	// 1. Progress bar setting
	m_CtrlProgressGetLog.SetRange(0, m_nMaxStageCnt);
	
	InitFont();
	InitLabel();
	InitGridWnd();
	InitColorBtn();
	
	int index = 0;
	m_ctrlKeyword.AddString("SMOKE SENSOR");	
	m_ctrlKeyword.SetItemData( index++, EP_EMG_SMOKE_LEAK );
	m_ctrlKeyword.SetCurSel(m_nKeywordIndex);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CSbcLogChkDlg::InitLabel()
{
	m_LabelName.SetBkColor(RGB_MIDNIGHTBLUE);
	m_LabelName.SetTextColor(RGB_WHITE);
	m_LabelName.SetFontSize(24);
	m_LabelName.SetFontBold(TRUE);
	m_LabelName.SetText("SBC Log Viewer");
}

void CSbcLogChkDlg::InitFont()
{
	LOGFONT LogFont;

	GetDlgItem(IDC_STATIC1)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight = 1000;
	LogFont.lfHeight = 18;

	m_Font.CreateFontIndirect( &LogFont );

	GetDlgItem(IDC_STATIC1)->SetFont(&m_Font);
	GetDlgItem(IDC_STATIC2)->SetFont(&m_Font);
	GetDlgItem(IDC_SELECT_KEYWORD)->SetFont(&m_Font);	
	GetDlgItem(IDC_DATETIME)->SetFont(&m_Font);	
}

void CSbcLogChkDlg::InitColorBtn()
{	
	m_BtnSearch.SetFontStyle(18, 1);
	m_BtnSearch.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	

	m_BtnCSVConvert.SetFontStyle(18, 1);
	m_BtnCSVConvert.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	

	m_BtnIDOK.SetFontStyle(18, 1);
	m_BtnIDOK.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
}

void CSbcLogChkDlg::InitGridWnd()
{
	ASSERT(m_pDoc);
	
	m_wndLayoutGrid.SubclassDlgItem(IDC_LAYOUT_GRID, this);	
	m_wndLayoutGrid.m_bSameRowSize = FALSE;
	m_wndLayoutGrid.m_bSameColSize = FALSE;
	m_wndLayoutGrid.m_bCustomWidth = TRUE;

	m_wndLayoutGrid.Initialize();
	
	BOOL bLock = m_wndLayoutGrid.LockUpdate();
	m_nLayOutRow = m_pDoc->m_nModulePerRack;
	m_wndLayoutGrid.SetRowCount(m_nLayOutRow);

	m_nLayOutCol =  m_pDoc->GetInstalledModuleNum()/m_nLayOutRow;
	if( (m_nMaxStageCnt%m_nLayOutRow) > 0) 
	{
		m_nLayOutCol++;
	}
		
	m_wndLayoutGrid.SetDrawingTechnique(gxDrawUsingMemDC);
	m_wndLayoutGrid.SetColCount(m_nLayOutCol*2);
	
	m_wndLayoutGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);

	CString strTemp = _T("");
	int i = 0;
	int nIndex = 0;	
	
	for(i =0; i<m_nLayOutCol; i++)
	{		
		m_wndLayoutGrid.SetValueRange(CGXRange().SetCols((i+1)*2), "[ Empty ]");
		m_wndLayoutGrid.SetStyleRange(CGXRange().SetCols((i+1)*2), CGXStyle().SetEnabled(FALSE));

		m_wndLayoutGrid.SetValueRange(CGXRange().SetCols((i+1)*2-1), "---");
		m_wndLayoutGrid.SetStyleRange(CGXRange().SetCols((i+1)*2-1), CGXStyle().SetEnabled(FALSE).SetDraw3dFrame(gxFrameRaised));				
	}	

	for( i=0; i<m_nLayOutRow; i++ )
	{
		if( m_bRackIndexUp == TRUE )
		{	// 밑에서 위로
			nIndex = m_nLayOutRow - i;
			strTemp.Format("%d", nIndex);
			m_wndLayoutGrid.SetStyleRange(CGXRange(i+1, 0), CGXStyle().SetValue(strTemp));			
		}
		else
		{	// 위에서 아래로
			nIndex = i+1;
			strTemp.Format("%d",nIndex);
			m_wndLayoutGrid.SetStyleRange(CGXRange(i+1, 0), CGXStyle().SetValue(strTemp));
		}
	}

	int nRow, nCol;
	for( i =0; i<m_nMaxStageCnt; i++)
	{
		int nMD = ::EPGetModuleID(i);
		GetModuleRowCol(nMD, nRow, nCol);
		if(nRow <= m_nLayOutRow && nCol <= m_nLayOutCol*2)
		{	
			m_wndLayoutGrid.SetStyleRange(CGXRange(nRow, nCol), 
				CGXStyle().SetValue("-")				
				.SetEnabled(TRUE));
			strTemp = ::GetModuleName(nMD);	//Module Name			
			m_wndLayoutGrid.SetStyleRange(CGXRange(nRow, nCol-1), CGXStyle().SetEnabled(FALSE).SetDraw3dFrame(gxFrameRaised).SetValue(strTemp));
			m_wndLayoutGrid.SetStyleRange(CGXRange(nRow, nCol), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(strTemp)));
		}
		//열단위 굵은 라인 표시 
		m_wndLayoutGrid.SetStyleRange(CGXRange().SetCols(nCol), 
			CGXStyle().SetBorders(gxBorderLeft, CGXPen().SetWidth(2).SetColor(RGB(255, 128 ,0)))
			);
		if( nCol == m_nLayOutCol*2 )
		{
			m_wndLayoutGrid.SetStyleRange(CGXRange().SetCols(nCol), 
				CGXStyle().SetBorders(gxBorderRight, CGXPen().SetWidth(2).SetColor(RGB(255, 128 ,0)))
				);
		}
	}
	
	//열단위 굵은 라인 표시 
	m_wndLayoutGrid.SetStyleRange(CGXRange().SetCols(1), 
		CGXStyle()
		.SetBorders(gxBorderLeft, CGXPen().SetWidth(2).SetColor(RGB(255, 128 ,0))));
	m_wndLayoutGrid.SetStyleRange(CGXRange().SetRows(1), 
		CGXStyle()
		.SetBorders(gxBorderTop, CGXPen().SetWidth(2).SetColor(RGB(255, 128 ,0))));
	m_wndLayoutGrid.SetStyleRange(CGXRange().SetRows(m_nLayOutRow), 
		CGXStyle()
		.SetBorders(gxBorderBottom, CGXPen().SetWidth(2).SetColor(RGB(255, 128 ,0))));

	// 1열/2열 표시 (상단)

	int nBayStartNum = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "BayStartNum", 1);

	for( i = 0; i<m_nLayOutCol; i++)
	{
		m_wndLayoutGrid.SetCoveredCellsRowCol(0, i*2+1, 0, (i+1)*2);			//
		strTemp.Format("%d %s", i+nBayStartNum, ::GetStringTable(IDS_TEXT_COLUMN_UNIT));	//1열 2열 표시 
		m_wndLayoutGrid.SetStyleRange(CGXRange(0, i*2+1), CGXStyle().SetValue(strTemp).SetTextColor(RGB_LIGHTBLUE));
	}
	
	m_wndLayoutGrid.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(GRID_DATA_FONT_SIZE).SetBold(TRUE).SetFaceName(::GetStringTable(IDS_LANG_TEXT_FONT)))		
		);

	m_wndLayoutGrid.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));
	
	// 1. 그리드의 크기 조절
	CRect ctrlRect;
	m_wndLayoutGrid.GetWindowRect(ctrlRect);	//Step Grid Window Position
	ScreenToClient(ctrlRect);

	m_wndLayoutGrid.GetClientRect(&ctrlRect);
	int nTotCol = m_wndLayoutGrid.GetColCount()/2;			
	float width = (float)(ctrlRect.Width())/nTotCol;				
	int height = (int)(ctrlRect.Height()-20)/(m_wndLayoutGrid.GetRowCount());

	if(  nTotCol <= 10 && nTotCol > 0 )
	{	
		for(int c = 0; c<nTotCol&&c<32; c++)
		{	
			m_wndLayoutGrid.m_nWidth[c*2+1]	= int(width* 0.3f);					
			m_wndLayoutGrid.m_nWidth[(c+1)*2] = int(width* 0.7f);
		}
		m_wndLayoutGrid.SetDefaultRowHeight( height );
		m_wndLayoutGrid.SetColWidth(0, 0, 0);
	}
	else if( nTotCol > 10 && nTotCol > 0 )
	{	
		for(int c = 0; c<nTotCol&&c<32; c++)
		{
			m_wndLayoutGrid.m_nWidth[c*2+1]	= 0;					
			m_wndLayoutGrid.m_nWidth[(c+1)*2] = int(width* 1.0f);									
		}
		m_wndLayoutGrid.SetRowHeight(0,0,50);
		m_wndLayoutGrid.SetDefaultRowHeight( height );
	}	
	
	m_wndLayoutGrid.LockUpdate(bLock);
	m_wndLayoutGrid.Redraw();
}

BOOL CSbcLogChkDlg::GetModuleRowCol(int nModuleID, int &nRow, int &nCol)
{
	int nModuleIndex = EPGetModuleIndex(nModuleID);
	if( nModuleIndex < 0 )	return FALSE;

	if( m_bRackIndexUp )
	{
		nCol = (nModuleIndex / m_nLayOutRow+1)*2;						//0~11 => 2
		nRow = m_nLayOutRow - ((nModuleIndex) % m_nLayOutRow);			//12-0~11
	}
	else
	{
		nCol = (nModuleIndex / m_nLayOutRow+1)*2;						
		nRow = ((nModuleIndex) % m_nLayOutRow)+1;			
	}
	return TRUE;
}

void CSbcLogChkDlg::Fun_ClearGrid()
{
	int i = 0;
	int nRow, nCol;
	CString strTemp = _T("");
	
	for( i =0; i<m_nMaxStageCnt; i++ )
	{
		int nMD = ::EPGetModuleID(i);
		GetModuleRowCol(nMD, nRow, nCol);
		if(nRow <= m_nLayOutRow && nCol <= m_nLayOutCol*2)
		{	
			if( m_pDoc->m_nDeviceID == DEVICE_FORMATION1 
				|| m_pDoc->m_nDeviceID == DEVICE_FORMATION2 )
			{
				if( i == 0 || i == 1 )		
				{
					continue;
				}
			}					
			else if( m_pDoc->m_nDeviceID == DEVICE_FORMATION3 )
			{
				if( i == 4 || i == 5 || i == 6 || i == 7 )		
				{
					continue;
				}
			}
			else if( m_pDoc->m_nDeviceID == DEVICE_FORMATION4 )
			{
				if( i == 6 || i == 7 || i == 8 || i == 9 )		
				{
					continue;
				}
			}
			
			m_wndLayoutGrid.SetStyleRange(CGXRange(nRow, nCol), CGXStyle().SetValue("-")
			.SetInterior(RGB_WHITE));
		}		
	}
}


void CSbcLogChkDlg::OnBnClickedBtnSearch()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int i = 0;	
	int nMD = 0;
	
	int nRow, nCol;
	CString strTemp = _T("");
	CString strIP = _T("");
	CString strSelFileName = _T("");
	CString toFileName;
	CString strLoginID = AfxGetApp()->GetProfileString(FORM_FTP_SECTION, "Login ID", "root");
	CString strPassword = AfxGetApp()->GetProfileString(FORM_FTP_SECTION, "Login Password", "dusrnth");
	CString strLocation = AfxGetApp()->GetProfileString(FORM_FTP_SECTION, "Log Location", "formation_data/log");
	
	COleDateTime OleSelectTime;
	m_ctrlDateTime.GetTime(OleSelectTime);
	
	strSelFileName.Format("DEBUG_LOG%02d%02d.log", OleSelectTime.GetMonth(), OleSelectTime.GetDay());
	
	CFtpConnection *pFtpConnection = NULL;
	CInternetSession *pFtpSession = NULL; 
	
	CString strKeyword = _T("");
	m_nKeywordIndex = m_ctrlKeyword.GetCurSel();
	m_ctrlKeyword.GetLBText(m_nKeywordIndex, strKeyword);
	
	int nKeywordCnt = -1;
	int nSize = -1;
	
	CStdioFile sourceFile;			
	CFileException ex;					
	CString strReadData;
	
	BeginWaitCursor();
	
	for( i=0; i<m_nMaxStageCnt; i++ )
	{		
		nMD = ::EPGetModuleID(i);
		CFormModule *pModule = m_pDoc->GetModuleInfo(nMD);
		
		nKeywordCnt = -1;
		
		if( pModule != NULL )
		{
			if( pModule->GetState() != EP_STATE_LINE_OFF )
			{
				// 1. FTP 로 로그 파일 다운로드 후 로그 분석을 진행		
				strIP = m_pDoc->GetModuleIPAddress(nMD);
				
				try
				{	
					pFtpSession = new CInternetSession();					
					pFtpSession->SetOption(INTERNET_OPTION_CONNECT_TIMEOUT, 3000);
					pFtpConnection = pFtpSession->GetFtpConnection(strIP, strLoginID, strPassword);

					if(!pFtpConnection->SetCurrentDirectory(strLocation))
					{
						// AfxMessageBox(GetStringTable(IDS_TEXT_SAVE_DIR_ERROR));
					}
					else
					{	
						toFileName.Format("%s\\Temp\\Unitlog.txt", AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION, "CTSMon"));
						if(pFtpConnection->GetFile(strSelFileName, toFileName, FALSE, FILE_ATTRIBUTE_NORMAL, FTP_TRANSFER_TYPE_ASCII) == FALSE)
						{
							// 1. 경로에 로그 파일이 없는 경우							
						}
						else
						{						
							nKeywordCnt = 0;
							
							// 2. 파일을 다운로드 받은 경우 'Keyword' 를 검색해서 화면에 숫자를 표시한다.
							if( sourceFile.Open( toFileName, CFile::modeRead, &ex ))
							{
								while(TRUE)
								{			
									sourceFile.ReadString(strReadData);    // 한 문자열씩 읽는다.
									nSize = strReadData.Find(':');
									if( nSize > 0 )
									{	
										if( KMP( (LPSTR)(LPCTSTR)strReadData, (LPSTR)(LPCTSTR)strKeyword ) != -1 )
										{
											// 1. 파일에서 keyword 값을 찾은 경우
											nKeywordCnt++;									
										}
									}			
									else
									{
										break;				
									}
								}	  
								sourceFile.Close();
							}
						}
					}
					
					if(pFtpConnection) 
					{
						pFtpConnection->Close();				
					}
					
					pFtpSession->Close();
					delete pFtpSession;					
					
					pFtpConnection = NULL;
				}
				catch(CInternetException *e)
				{
					if( pFtpSession != NULL ) 
					{
						pFtpSession->Close();				
					}
					
					pFtpConnection = NULL;
					
					TCHAR sz[1024];
					e->GetErrorMessage(sz, 1024);
					// AfxMessageBox(sz);
					e->Delete();
				}	
				
				MSG msg;
				while(::PeekMessage(&msg, NULL, NULL, NULL, PM_REMOVE))
				{
					::TranslateMessage(&msg);
					::DispatchMessage(&msg);
				}			
			}
		}
		
		m_CtrlProgressGetLog.SetPos(i+1);
		
		GetModuleRowCol(nMD, nRow, nCol);
		if(nRow <= m_nLayOutRow && nCol <= m_nLayOutCol*2)
		{	
			strTemp.Format("%d", nKeywordCnt);
			if( nKeywordCnt == -1)
			{
				m_wndLayoutGrid.SetStyleRange(CGXRange(nRow, nCol), CGXStyle().SetValue("-")
					.SetInterior(RGB_YELLOW));									
			}
			else if( nKeywordCnt == 0 )
			{
				m_wndLayoutGrid.SetStyleRange(CGXRange(nRow, nCol), CGXStyle().SetValue(strTemp)
					.SetInterior(RGB_WHITE));						
			}
			else
			{
				m_wndLayoutGrid.SetStyleRange(CGXRange(nRow, nCol), CGXStyle().SetValue(strTemp)
					.SetInterior(RGB_RED));
			}
		}
	}
	
	EndWaitCursor();
}

void CSbcLogChkDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_CtrlProgressGetLog.SetPos(0);
	Fun_ClearGrid();
	OnOK();
}

void CSbcLogChkDlg::OnBnClickedCsvOutput()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.	
	CString strTime, strData;	
	
	COleDateTime OleSelectTime;
	m_ctrlDateTime.GetTime(OleSelectTime);
	strTime.Format("SMOKE_%d%02d%02d", OleSelectTime.GetYear(), OleSelectTime.GetMonth(), OleSelectTime.GetDay());	
	
	CStdioFile	file;
	CString		strFileName;		

	int i=0;
	int j=0;

	CFileDialog dlg(false, "", strTime, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "csv 파일(*.csv)|*.csv|All Files (*.*)|*.*|");

	if(dlg.DoModal() == IDOK)
	{		
		strFileName = dlg.GetPathName();		
		file.Open(strFileName, CFile::modeCreate|CFile::modeReadWrite|CFile::modeNoTruncate);

		int nRowCnt = m_wndLayoutGrid.GetRowCount();
		int nColCnt = m_wndLayoutGrid.GetColCount();

		for( i=1; i<=nRowCnt; i++ )
		{
			for( j=1; j<=nColCnt; j++ )
			{
				if( j==1 )
				{
					strData = m_wndLayoutGrid.GetValueRowCol(i, j);
				}
				else
				{
					strData = strData + "," + m_wndLayoutGrid.GetValueRowCol(i, j);
				}
			}
			file.WriteString(strData+"\n");
		}		
		file.Close();
	}
}

int CSbcLogChkDlg::KMP(char *s, char *p)
{
	int f[256];
	int sl, pl;
	int i, q;

	pl = strlen(p);
	sl = strlen(s);

	make_f(p, f);
	q = -1;
	for( i=0; i<sl; ++i)
	{
		while( q >= 0 && p[q+1] != s[i] ) q = f[q];
		if(p[q+1] == s[i]) q++;
		if(q == pl-1) return i-pl+1;
	}
	return -1;
}

void CSbcLogChkDlg::make_f(char *p, int *f)
{
	int pl;
	int q, k;
	pl = strlen(p);
	f[0] = -1;
	k = -1;
	for( q=1; q<pl; ++q)
	{
		while( k>=0 && p[k+1] != p[q] ) k = f[k];
		if(p[k+1] == p[q]) k++;
		f[q] = k;
	}
}