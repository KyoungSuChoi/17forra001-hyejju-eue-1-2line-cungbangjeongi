// TextParsing.h: interface for the CTextParsing class.
//
//////////////////////////////////////////////////////////////////////


#if !defined(AFX_TEXTPARSING_H__AEFEAF96_7399_4E67_92EC_8C8278213743__INCLUDED_)
#define AFX_TEXTPARSING_H__AEFEAF96_7399_4E67_92EC_8C8278213743__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
typedef struct PS_TIMESET
{
	int nHour;
	int nMin;
	int nSec;
	int nMs;
}PSTimeSet;

class CTextParsing  
{
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	CTextParsing();
	virtual ~CTextParsing();

	CString GetStringTimePoint(CString strTime,  BOOL IsRunningTime = FALSE);
	void Clear();
	void SetParent(CWnd * pWnd);
	BOOL SetSimulationFile(CString strFileName, CString &strErrorMsg);

	CString GetColumnHeader(int nColIndex);
	float * GetColumnData(int nColIndex);
	BOOL SetFile(CString strFileName);
	
	int	GetRecordCount()	{	return m_nRecordCount;	}
	int	GetColumnSize()		{	return m_nColumnSize;	}
	

protected:
	float **m_ppData;
	int m_nColumnSize;
	int m_nRecordCount;
	CString m_strFileName;
	CStringList m_TitleList;
	PSTimeSet	oldTime;
	
	CWnd * pWnd;
};

#endif // !defined(AFX_TEXTPARSING_H__AEFEAF96_7399_4E67_92EC_8C8278213743__INCLUDED_)
