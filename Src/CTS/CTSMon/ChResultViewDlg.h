#if !defined(AFX_CHRESULTVIEWDLG_H__DEC05941_005D_401F_B5AB_AB69801CCEAF__INCLUDED_)
#define AFX_CHRESULTVIEWDLG_H__DEC05941_005D_401F_B5AB_AB69801CCEAF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ChResultViewDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CChResultViewDlg dialog
#include "MyGridWnd.h"
#include "CTSMonDoc.h"	// Added by ClassView

class CChResultViewDlg : public CDialog
{
// Construction
public:
	CString m_strModule;
	CCTSMonDoc *m_pDoc;
	int m_nChannelIndex;
	BOOL DisplayChannelData();
	void InitChGrid();
	CChResultViewDlg(CWnd* pParent = NULL);   // standard constructor
//	CPtrArray	*m_stepResult;				//Array of STR_STEP_DATA
	CFormResultFile *m_pResultFile;
	// Dialog Data
	//{{AFX_DATA(CChResultViewDlg)
	enum { IDD = IDD_CHANNEL_RESULT_DLG };
	CLabel	m_ctrlChDisp;
	//}}AFX_DATA
//	STR_BOARD_RESULT * m_pBoard;
	CMyGridWnd		m_wndChGrid;
	STR_TOP_CONFIG	*m_pConfig;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChResultViewDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CChResultViewDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnFileSave();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHRESULTVIEWDLG_H__DEC05941_005D_401F_B5AB_AB69801CCEAF__INCLUDED_)
