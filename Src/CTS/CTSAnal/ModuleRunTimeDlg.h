#if !defined(AFX_MODULERUNTIMEDLG_H__81F1EECC_5FDD_483B_B768_73D18FE99A28__INCLUDED_)
#define AFX_MODULERUNTIMEDLG_H__81F1EECC_5FDD_483B_B768_73D18FE99A28__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ModuleRunTimeDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CModuleRunTimeDlg dialog
#include "MyGridWnd.h"
#include "PEGRPAPI.h"
#include "pemessag.h"

class CModuleRunTimeDlg : public CDialog
{
// Construction
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	CModuleRunTimeDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CModuleRunTimeDlg();

	void SearchModuleLog(CString strName);
	BOOL ShowGraph();
	void SetDB(CDatabase *pDB);
	void InitGraph();
	void InitModuleGrid();
	void InitTotalGrid();
	

// Dialog Data
	//{{AFX_DATA(CModuleRunTimeDlg)
	enum { IDD = IDD_MODULE_USE };
	BOOL	m_bTimeRange;
	CTime	m_fromTime;
	CTime	m_toTime;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CModuleRunTimeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	BOOL m_nDrawType;
	CMyGridWnd m_wndTotalRunGrid;
	CMyGridWnd m_wndModuleRunGrid;
	HWND	m_hWndPE;
	CDatabase m_dbServer;
	// Generated message map functions
	//{{AFX_MSG(CModuleRunTimeDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSearch();
	afx_msg void OnTimeRange();
	afx_msg void OnRuntCountRadio();
	afx_msg void OnRunTimeRadio();
	afx_msg void OnDelete();
	afx_msg void OnDestroy();
	afx_msg void OnSelectAll();
	afx_msg void OnEditCopy();
	afx_msg void OnReload();
	//}}AFX_MSG
	afx_msg LRESULT OnMovedCurrentCell(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRButtonClickedRowCol(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MODULERUNTIMEDLG_H__81F1EECC_5FDD_483B_B768_73D18FE99A28__INCLUDED_)
