// VewSet.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "ViewSet.h"


// CVewSet 대화 상자입니다.

IMPLEMENT_DYNAMIC(CViewSet, CDialog)

CViewSet::CViewSet(CWnd* pParent /*=NULL*/)
: CDialog(CViewSet::IDD, pParent)
, m_LayoutView(FALSE)
, m_DataDown_Message(FALSE)
, m_RackIndexUp(FALSE)
, m_Small_ch_layout(FALSE)
, m_Show_ChannelMonitoringView(FALSE)
, m_Show_ChannelResultView(FALSE)
, m_Show_SbcInfo(FALSE)
, m_Show_Sensor(FALSE)
, m_Show_Temp(FALSE)

, m_CellInTray(0)
, m_TopGridColCout(0)
, m_Module_Per_Rack(0)
, m_nTemperatureSaveInterval(0)
{
	m_Lang =0;
}

CViewSet::~CViewSet()
{
}

void CViewSet::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CHECK_LAYOUTVIEW, m_LayoutView);
	DDX_Check(pDX, IDC_CHECK_DATADOWN_MESSAGE, m_DataDown_Message);
	DDX_Check(pDX, IDC_CHECK_RACKINDEXUP, m_RackIndexUp);
	DDX_Check(pDX, IDC_CHECK_SMALL_CH_LAYOUT, m_Small_ch_layout);
	DDX_Check(pDX, IDC_CHECK_SHOW_MONITERING, m_Show_ChannelMonitoringView);
	DDX_Check(pDX, IDC_CHECK_SHOW_RESULT, m_Show_ChannelResultView);
	DDX_Check(pDX, IDC_CHECK_SHOW_SBCINFO, m_Show_SbcInfo);
	DDX_Check(pDX, IDC_CHECK_SHOW_SENSOR, m_Show_Sensor);
	DDX_Check(pDX, IDC_CHECK_SHOW_TEMP, m_Show_Temp);

	DDX_Text(pDX, IDC_EDIT_CELLINTARY, m_CellInTray);
	DDX_Text(pDX, IDC_EDIT_TOPGRIDCOLCOUT, m_TopGridColCout);
	DDX_Text(pDX, IDC_EDIT_MODULE_PER_RACK, m_Module_Per_Rack);

	DDX_Control(pDX, IDC_CHECK_KOR, m_Check_Korea);
	DDX_Control(pDX, IDC_CHECK_ENG, m_Check_English);
	DDX_Control(pDX, IDC_CHECK_CHIN, m_Check_Chinese);
	DDX_Control(pDX, IDC_COMBO1, m_ComboDeviceId);
	DDX_Control(pDX, IDC_COMBO2, m_CbMachineTraytype);
	DDX_Text(pDX, IDC_EDIT_TEMP_SAVE_INTERVAL, m_nTemperatureSaveInterval);
}


BEGIN_MESSAGE_MAP(CViewSet, CDialog)
	ON_BN_CLICKED(IDOK, &CViewSet::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CViewSet::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_CHECK_KOR, &CViewSet::OnBnClickedCheckKor)
	ON_BN_CLICKED(IDC_CHECK_ENG, &CViewSet::OnBnClickedCheckEng)
	ON_BN_CLICKED(IDC_CHECK_CHIN, &CViewSet::OnBnClickedCheckChin)
END_MESSAGE_MAP()


// CVewSet 메시지 처리기입니다.
BOOL CViewSet::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	Langinit();
	Reginit();

	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CViewSet::Reginit()
{
	m_LayoutView = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "LayoutView", FALSE);
	m_DataDown_Message = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "DataDown Message", FALSE);
	m_RackIndexUp = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "RackIndexUp", FALSE);
	m_Small_ch_layout = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "Small_Ch_Layout", FALSE);
	m_Show_SbcInfo = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "Show SbcInfo", FALSE);
	m_Show_Temp = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "Show Temp", FALSE);
	m_Show_ChannelMonitoringView = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "Show ChannelMonitoringView", FALSE);
	m_Show_ChannelResultView = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "Show ChannelResultView", FALSE);
	m_Show_Sensor = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "Show Sensor", FALSE);

	m_Module_Per_Rack = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION , "Module Per Rack", 3);
	m_CellInTray = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "CellInTray", 128);
	m_TopGridColCout = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "TopGridColCout", 16);

	m_ComboDeviceId.SetCurSel( AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "DeviceID", 0) );
	m_CbMachineTraytype.SetCurSel( AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "MachineTraytype", 0) );

	m_nTemperatureSaveInterval = AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "TemperatureSaveInterval", 5);
}

void CViewSet::Langinit()
{
	long rtn1, rtn2, rtn3, rtn4,  rtn5,  rtn6;
	DWORD type;
	HKEY hKey = 0;
	DWORD size = 511;
	BYTE buf[512], buf2[512];

	rtn1 = ::RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\CTSMon\\FormSetting", 0, KEY_READ, &hKey);
	rtn2 = RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\CTSEditor\\Config", 0, KEY_READ, &hKey);
	rtn3 = RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\CTSAnalyzer\\FormSetting", 0, KEY_READ, &hKey);
	rtn4 = RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\CTSGraphAnal\\Config", 0, KEY_READ, &hKey);
	rtn5 = RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\DataDown\\SaveSetting", 0, KEY_READ, &hKey);
	rtn6 = RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\ExcelTrans\\SaveSetting", 0, KEY_READ, &hKey);
	if (rtn1 == ERROR_SUCCESS && rtn2 == ERROR_SUCCESS && rtn3 == ERROR_SUCCESS 
		&& rtn4 == ERROR_SUCCESS && rtn5 == ERROR_SUCCESS && rtn6 == ERROR_SUCCESS)
	{
		size = 255;
		RegQueryValueEx(hKey, "LANGUAGE", NULL, &type, buf, &size);
		sprintf((char *)buf2, "%ld", *(long *)buf);
		m_Lang = atol((LPCTSTR)buf2);

		if (m_Lang==0)
		{
			m_Check_Korea.SetCheck(BST_CHECKED);	
		}
		else if (m_Lang==1)
		{
			m_Check_English.SetCheck(BST_CHECKED);
		}
		else if (m_Lang==2)
		{
			m_Check_Chinese.SetCheck(BST_CHECKED);
		}
	}
}

void CViewSet::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);

	LangSet();
	RegSet();

	OnOK();
}
void CViewSet::LangSet()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	HKEY hKey;
	DWORD pBuf=0;

	pBuf = m_Lang;

	RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\CTSMon\\FormSetting", 0, KEY_ALL_ACCESS, &hKey);
	RegSetValueEx(hKey, TEXT("LANGUAGE"), 0, REG_DWORD, (BYTE*)&pBuf, sizeof(DWORD));
	RegCloseKey(hKey);
	RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\CTSEditor\\Config", 0, KEY_ALL_ACCESS, &hKey);
	RegSetValueEx(hKey, TEXT("LANGUAGE"), 0, REG_DWORD, (BYTE*)&pBuf, sizeof(DWORD));
	RegCloseKey(hKey);
	RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\CTSAnalyzer\\FormSetting", 0, KEY_ALL_ACCESS, &hKey);
	RegSetValueEx(hKey, TEXT("LANGUAGE"), 0, REG_DWORD, (BYTE*)&pBuf, sizeof(DWORD));
	RegCloseKey(hKey);
	RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\CTSGraphAnal\\Config", 0, KEY_ALL_ACCESS, &hKey);
	RegSetValueEx(hKey, TEXT("LANGUAGE"), 0, REG_DWORD, (BYTE*)&pBuf, sizeof(DWORD));
	RegCloseKey(hKey);
	RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\DataDown\\SaveSetting", 0, KEY_ALL_ACCESS, &hKey);
	RegSetValueEx(hKey, TEXT("LANGUAGE"), 0, REG_DWORD, (BYTE*)&pBuf, sizeof(DWORD));
	RegCloseKey(hKey);
	RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\ExcelTrans\\SaveSetting", 0, KEY_ALL_ACCESS, &hKey);
	RegSetValueEx(hKey, TEXT("LANGUAGE"), 0, REG_DWORD, (BYTE*)&pBuf, sizeof(DWORD));
	RegCloseKey(hKey);	
}

void CViewSet::RegSet()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "LayoutView", m_LayoutView);
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "DataDown Message", m_DataDown_Message);
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "RackIndexUp", m_RackIndexUp);
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "Small_Ch_Layout", m_Small_ch_layout);
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "Show SbcInfo", m_Show_SbcInfo);
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "Show Temp", m_Show_Temp);
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "Show ChannelMonitoringView", m_Show_ChannelMonitoringView);
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "Show ChannelResultView", m_Show_ChannelResultView);
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "Show Sensor", m_Show_Sensor);

	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "Module Per Rack", m_Module_Per_Rack);
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "CellInTray", m_CellInTray);
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "TopGridColCout", m_TopGridColCout);

	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "DeviceID", m_ComboDeviceId.GetCurSel());
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "MachineTraytype", m_CbMachineTraytype.GetCurSel());

	AfxGetApp()->WriteProfileInt(FROM_SYSTEM_SECTION, "TemperatureSaveInterval", m_nTemperatureSaveInterval);
}

void CViewSet::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}

void CViewSet::OnBnClickedCheckKor()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_Lang =0;
	m_Check_Korea.SetCheck(BST_CHECKED);
	m_Check_English.SetCheck(BST_UNCHECKED);
	m_Check_Chinese.SetCheck(BST_UNCHECKED);
}

void CViewSet::OnBnClickedCheckEng()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_Lang =1;
	m_Check_Korea.SetCheck(BST_UNCHECKED);
	m_Check_English.SetCheck(BST_CHECKED);
	m_Check_Chinese.SetCheck(BST_UNCHECKED);
}

void CViewSet::OnBnClickedCheckChin()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_Lang =2;
	m_Check_Korea.SetCheck(BST_UNCHECKED);
	m_Check_English.SetCheck(BST_UNCHECKED);
	m_Check_Chinese.SetCheck(BST_CHECKED);
}
