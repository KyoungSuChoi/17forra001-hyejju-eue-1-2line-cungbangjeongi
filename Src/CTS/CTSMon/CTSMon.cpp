// CTSMon.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "CTSMon.h"

#include "MainFrm.h"
#include "CTSMonDoc.h"
#include "CTSMonView.h"

#include "LoginDlg.h"
#include "PasswordChkDlg.h"

#include "./Global/Mysql/PneApp.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CCTSMonApp

BEGIN_MESSAGE_MAP(CCTSMonApp, CWinApp)
	//{{AFX_MSG_MAP(CCTSMonApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	// NOTE - the ClassWizard will add and remove mapping macros here.
	//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard file based document commands
//	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
//	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
	// Standard print setup command
	ON_COMMAND(ID_FILE_PRINT_SETUP, CWinApp::OnFilePrintSetup)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCTSMonApp construction

CCTSMonApp::CCTSMonApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CCTSMonApp object

CCTSMonApp theApp;
PneApp mainPneApp;

/////////////////////////////////////////////////////////////////////////////
// CCTSMonApp initialization
STR_LOGIN	g_LoginData = {"Unuse", "Unuse", PS_USER_SUPER, "Unuse", "Unuse", "Unuse",  FALSE,  0};

BOOL CCTSMonApp::InitInstance()
{
	// AfxGetModuleState()->m_dwVersion = 0x0601;
	AfxGetStaticModuleState() ->m_dwVersion = 0x0601;

	SetRegistryKey(_T("PNE CTS"));
	LoadStdProfileSettings();
	
	//Read Host Type
	CString strWindowTitle;
	{
		DWORD lSystemType = GetProfileInt(FORMSET_REG_SECTION, "System Type", -1);
		if(lSystemType == -1)
		{
			lSystemType = 0;		// O is All System
			WriteProfileInt(FORMSET_REG_SECTION, "System Type", lSystemType);
			// EP_ID_FORM_OP_SYSTEM => Online 모드 가능
		}	
		strWindowTitle = SetSystemType(lSystemType);
	}

/*	HWND FirsthWnd, FirstChildhWnd;
	if ((FirsthWnd = ::FindWindow(CTSMON_CLASS_NAME, NULL)) != NULL)
	{
		FirstChildhWnd = GetLastActivePopup(FirsthWnd);
		SetActiveWindow(FirsthWnd);	
		SetForegroundWindow(FirsthWnd);
		if (FirsthWnd != FirstChildhWnd)
		{
			SetActiveWindow(FirstChildhWnd);	
			SetForegroundWindow(FirstChildhWnd);
		}

		ShowWindow(FirsthWnd, SW_SHOWMAXIMIZED); 	
		return FALSE;
	}
*/

	CWnd *pWndPrev, *pWndChild;
    // Determine if a window with the class name exists...
    if ((pWndPrev = CWnd::FindWindow(_T(CTSMON_CLASS_NAME),NULL)) != NULL)
    {
		// If so, does it have any popups?
        pWndChild = pWndPrev->GetLastActivePopup();

        // If iconic, restore the main window.
        if (pWndPrev->IsIconic())
			pWndPrev->ShowWindow(SW_RESTORE);

        // Bring the main window or its popup to the foreground
        pWndChild->SetForegroundWindow();

        // and you are done activating the other application.
        return FALSE;
	}
	GXInit();
//	if (!AfxSocketInit())
//	{
//		AfxMessageBox("Windows sockets initialization failed.");
//		return FALSE;
//	}
	AfxEnableControlContainer();
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	// Change the registry key under which our settings are stored.
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization.
	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.

	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CCTSMonDoc),
		RUNTIME_CLASS(CMainFrame),       // main SDI frame window
		RUNTIME_CLASS(CCTSMonView));
	AddDocTemplate(pDocTemplate);

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	if( LanguageinitMonConfig() == false )
	{
		AfxMessageBox("Could not found [ CTSMon_Lang.ini ]");
		return false;
	}

	// Dispatch commands specified on the command line
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// The one and only window has been initialized, so show and update it.
	m_pMainWnd->ShowWindow(SW_SHOWMAXIMIZED);
	m_pMainWnd->UpdateWindow();


	return TRUE;
}

CString CCTSMonApp::SetSystemType(DWORD lSystemType)
{
	m_bySystemType = lSystemType;

	if(lSystemType == EP_ID_FORM_OP_SYSTEM)
	{
		return _T("CTSMon");
	}
	else if(lSystemType == EP_ID_IROCV_SYSTEM)
	{
		return _T(TEXT_LANG[0]);//"IR/OCV 측정기"
	}
	else if(lSystemType == EP_ID_AGIGN_SYSTEM)
	{
		return _T("Aging");
	}
	else
	{
		return _T("CTSMon");
	}

 }

DWORD CCTSMonApp::GetSystemType()
{
	return m_bySystemType;
}

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	CLabel	m_ctrlVer;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	DDX_Control(pDX, IDC_PNE_VER, m_ctrlVer);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CCTSMonApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// CCTSMonApp message handlers


int CCTSMonApp::ExitInstance() 
{
	// TODO: Add your specialized code here and/or call the base class
	// m_server.End();
	WSACleanup();

	GXTerminate();

	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}

	return CWinApp::ExitInstance();
}

BOOL CAboutDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CString strTemp;
	//strTemp.Format("Ver %d, Built 51124(Built Type %d)", _EP_FORM_VERSION, CUSTOMER_TYPE);
	
	strTemp.Format("VER : Built 20130213, Protocol %x", _EP_FORM_VERSION);
	m_ctrlVer.SetText(strTemp);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL LoginCheck()
{
	CLoginDlg *pDlg;
	pDlg = new CLoginDlg;
	ASSERT(pDlg);

	if(pDlg->DoModal() != IDOK)
	{
		delete pDlg;
		pDlg = NULL;
		return FALSE;
	}
	
	memcpy(&g_LoginData, &pDlg->m_LoginInfo, sizeof(STR_LOGIN)); 

	delete pDlg;
	pDlg = NULL;
	return TRUE;
}

BOOL PasswordCheck()
{
	CPasswordChkDlg *pDlg;
	pDlg = new CPasswordChkDlg;
	ASSERT(pDlg);

	if( pDlg->DoModal() != IDOK)
	{
		delete pDlg;
		pDlg = NULL;
		return FALSE;
	}

	delete pDlg;
	pDlg = NULL;
	return TRUE;
}

CString GetStringTable(UINT nStringResourceID)
{
/*	WCHAR wMsg[MAX_PATH];
	LoadStringW(AfxGetApp()->m_hInstance, nStringResourceID, wMsg, MAX_PATH);

	CString strRetVal=wMsg;
*/
	CString strRetVal;
	strRetVal.LoadString(nStringResourceID);
	return strRetVal;
}


bool CCTSMonApp::LanguageinitMonConfig() 
{
	CString strInstallPath = _T("");

	g_nLanguage = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "Language", 0);
	g_strLangPath.Format("%s\\Lang\\CTSMon_Lang.ini", GetProfileString(FORM_PATH_REG_SECTION ,"CTSMon","C:\Program Files\PNE CTS"));

	switch(g_nLanguage)
	{
	case 0:
		{
			SetThreadUILanguage( MAKELCID( MAKELANGID(LANG_KOREAN , SUBLANG_KOREAN) , SORT_DEFAULT));
			break;
		}

	case 1:
		{
			SetThreadUILanguage( MAKELCID( MAKELANGID(LANG_ENGLISH , SUBLANG_ENGLISH_US) , SORT_DEFAULT));
			break;
		}

	case 2:
		{
			SetThreadUILanguage( MAKELCID( MAKELANGID(LANG_CHINESE_SIMPLIFIED , SUBLANG_CHINESE_SIMPLIFIED) , SORT_DEFAULT));
			break;
		}
	}

	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCTSMonApp"), _T("TEXT_CCTSMonApp_CNT"), _T("TEXT_CCTSMonApp_CNT"));
	if( strTemp == "TEXT_CCTSMonApp_CNT" )
	{
		return false;
	}

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt];		

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CCTSMonApp_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCTSMonApp"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Languge error ====> " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}