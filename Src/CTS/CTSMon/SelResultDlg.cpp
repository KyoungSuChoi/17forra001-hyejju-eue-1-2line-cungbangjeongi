// SelResultDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ctsmon.h"
#include "SelResultDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelResultDlg dialog


CSelResultDlg::CSelResultDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelResultDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSelResultDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_nTrayIndex = 0;
	m_pModuleInfo = NULL;
}


void CSelResultDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelResultDlg)
	DDX_Control(pDX, IDC_SEL_LIST, m_wndSelList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelResultDlg, CDialog)
	//{{AFX_MSG_MAP(CSelResultDlg)
	ON_NOTIFY(NM_DBLCLK, IDC_SEL_LIST, OnDblclkSelList)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelResultDlg message handlers

BOOL CSelResultDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	ASSERT(m_pModuleInfo);
	
	// TODO: Add extra initialization here
	DWORD style = 	m_wndSelList.GetExtendedStyle();
	style |= LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_SUBITEMIMAGES;
	m_wndSelList.SetExtendedStyle(style );

	m_wndSelList.InsertColumn(0, "No", LVCFMT_LEFT, 50);
	m_wndSelList.InsertColumn(1, "Tray ID", LVCFMT_LEFT, 100);
	m_wndSelList.InsertColumn(2, "File name", LVCFMT_LEFT, 350);

	LVITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.mask = LVIF_TEXT|LVIF_IMAGE;
	char szName[32];

	for(int nI = 0; nI < m_pModuleInfo->GetTotalJig(); nI++)
	{	
		if( m_pModuleInfo->GetTrayNo(nI) == "" )
		{
			continue;
		}

		sprintf(szName, "%d", nI+1);
		lvItem.iItem = nI;
		lvItem.iSubItem = 0;
		lvItem.pszText = szName;
		m_wndSelList.InsertItem(&lvItem);
		m_wndSelList.SetItemData(lvItem.iItem, nI);		//==>LVN_ITEMCHANGED �� �߻� ��Ŵ 
		m_wndSelList.SetItemText(nI, 1, m_pModuleInfo->GetTrayNo(nI));
		m_wndSelList.SetItemText(nI, 2, m_pModuleInfo->GetResultFileName(nI));
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control	    
}

void CSelResultDlg::OnDblclkSelList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	OnOK() ;
	
	*pResult = 0;
}

void CSelResultDlg::OnOK() 
{
	// TODO: Add extra validation here

	POSITION pos = m_wndSelList.GetFirstSelectedItemPosition();
	if(pos == NULL)	return;
	m_nTrayIndex = m_wndSelList.GetNextSelectedItem(pos);
	
	CDialog::OnOK();
}
