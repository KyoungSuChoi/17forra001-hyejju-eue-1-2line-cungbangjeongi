// FormationModule.cpp: implementation of the CFormModule class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ctsmon.h"
#include "FormationModule.h"
#include "ModuleStepEndData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

extern STR_LOGIN g_LoginData;

CFormModule::CFormModule(int nModuleID, CString strName)
{
	m_nGroupCount = 0;
	m_iTrayTypeCount = 0;
//	m_nLineMode = EP_OFFLINE_MODE;

	for(int i=0; i<EP_MAX_SENSOR_CH; i++)
	{
		m_SensorMap1[i].nChannelNo = -1;
		sprintf(m_SensorMap1[i].szName, "T %d", i+1);	
		m_SensorMap2[i].nChannelNo = -1;
		sprintf(m_SensorMap2[i].szName, "V %d", i+1);
	}
	m_nModuleID = nModuleID;
	m_strModuleName = strName;
	m_nCellCountInTray = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "CellInTray", 128);
	m_sTrayTypeData=NULL;
	LanguageinitMonConfig();
// 20210127 KSCHOI Add ReAging Enable Stage Function START
	CString strKey;
	strKey.Format("ReAgingEnable_%d", nModuleID);
	m_bReAgingEnable = AfxGetApp()->GetProfileInt(REAGING_REG_SECTION, strKey, 0);
// 20210127 KSCHOI Add ReAging Enable Stage Function END
}

CFormModule::~CFormModule()
{
	if(	m_sTrayTypeData !=NULL)
	{
		delete [] m_sTrayTypeData;
		m_sTrayTypeData=NULL;
	}
//	delete m_sTrayTypeData;
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

bool CFormModule::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CFormModule"), _T("TEXT_CFormModule_CNT"), _T("TEXT_CFormModule_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CFormModule_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CFormModule"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

void CFormModule::MakeGroupStruct(int nGroupCount)
{
	ASSERT(nGroupCount > 0);

	if(m_nGroupCount == nGroupCount)
	{
		ResetGroupData();
		return;
	}
	
	m_nGroupCount = nGroupCount;
//	RemoveGroupStruct();
	
//	pGroup = new STR_GROUP_INFO[nGroupCount];	
//	ASSERT(pGroup);

	ResetGroupData();
}

void CFormModule::RemoveGroupStruct()
{
/*	if(pGroup)
	{
		delete [] pGroup;
		pGroup = NULL;
	}
*/
}

void CFormModule::ResetGroupData()
{
	for(int n=0; n< m_nGroupCount; n++)
	{
//		ZeroMemory(&pGroup[n].sTestResultFileHeader, sizeof(RESULT_FILE_HEADER));
//		ZeroMemory(&pGroup[n].testHeader, sizeof(EP_TEST_HEADER));
//		ZeroMemory(&pGroup[n].conditionHeader, sizeof(STR_CONDITION_HEADER));
//		ZeroMemory(&pGroup[n].dataNVRam, sizeof(EP_NVRAM_DATA));
//		pGroup[n].sTestResultFileHeader.nModuleID = m_nModuleID;
//		pGroup[n].sTestResultFileHeader.nGroupIndex = n;
//		strcpy(pGroup[n].sTestResultFileHeader.szOperatorID, g_LoginData.szLoginID);
//		strcpy(pGroup[n].sTestResultFileHeader.szModuleIP, EPGetModuleIP(m_nModuleID));
//		pGroup[n].dataTray.InitData();
//		pGroup[n].dataTray.lInputCellCount = EPGetChInGroup(m_nModuleID, n);

		for(int t =0; t<INSTALL_TRAY_PER_MD_1; t++)
		{
			m_TrayData[t].InitData();
			m_TrayData[t].ModuleID = m_nModuleID;
			m_TrayData[t].GroupIndex = 0;
			m_TrayData[t].lInputCellCount =  GetCellCountInTray();// EPGetChInGroup(m_nModuleID, n);
			m_aResultData[t].ClearResult();
		}

		m_Condition.ResetCondition();

/*		if(EPGetLineMode(m_nModuleID, n+1, nLineMode, nControlMode) == FALSE)
		{
			TRACE("%s Line Mode Request Fail\n", ::GetModuleName(m_nModuleID, n));
		}
*/
//		pGroup[n].nLineMode = nLineMode;
	}
	
	ResetTestReserve();	
//	m_nLineMode = nLineMode;
}

STR_SAVE_CH_DATA CFormModule::ConvertChSaveData( int nModuleID, int nChIndex/*Tray Base Index*/, LPEP_CH_DATA pChData, CString strCapacityFormula, CString strDCRFormula )
{
	//1 Tray Index를 구함 
	STR_SAVE_CH_DATA ChSaveData;
	ZeroMemory( &ChSaveData, sizeof(STR_SAVE_CH_DATA));

	//모든 단위를 m단위로 변환하여 저장한다.
	// pChSaveData->wModuleChIndex	= GetStartChIndex(nTrayIndex)+nChIndex;
	ChSaveData.wModuleChIndex	= nChIndex;
	ChSaveData.wChIndex	= nChIndex;
	ChSaveData.state	= pChData->state;
	//사용자 Grading 실시 

	CTestCondition *pCon = m_resultFile.GetTestCondition();	

	CStep *pStep = pCon->GetStep(pChData->nStepNo-1);
	if(pStep && pStep->m_Grading.GetGradeStepSize() > 0)
	{
		//Grading
		ChSaveData.grade = GetUserSelect(pChData->channelCode, pChData->grade);
	}
	else
	{
		ChSaveData.grade = pChData->grade;
	}

	ChSaveData.channelCode = pChData->channelCode;
	ChSaveData.nStepNo	= pChData->nStepNo;
	ChSaveData.fStepTime =  MD2PCTIME(pChData->ulStepTime);				//sec 단위로 변환  
	ChSaveData.fTotalTime = MD2PCTIME(pChData->ulTotalTime);			//sec 단위로 변환
	ChSaveData.fVoltage =	 VTG_PRECISION(pChData->lVoltage);
	ChSaveData.fCurrent = CRT_PRECISION(pChData->lCurrent);
	ChSaveData.fWatt	= ETC_PRECISION(pChData->lWatt);
	ChSaveData.fWattHour = ETC_PRECISION(pChData->lWattHour);
	ChSaveData.fCapacity = ETC_PRECISION(pChData->lCapacity);
	ChSaveData.fImpedance = ETC_PRECISION(pChData->lImpedance);	

	ChSaveData.fCcCapacity = CRT_PRECISION(pChData->lCcCapacity);
	ChSaveData.fCvCapacity = CRT_PRECISION(pChData->lCvCapacity);

	ChSaveData.fCcRunTime = MD2PCTIME(pChData->ulCcRunTime);
	ChSaveData.fCvRunTime = MD2PCTIME(pChData->ulCvRunTime);
	ChSaveData.fDCIR_V1 = VTG_PRECISION(pChData->lDCIR_V1);
	ChSaveData.fDCIR_V2 = VTG_PRECISION(pChData->lDCIR_V2);
	ChSaveData.fDCIR_AvgCurrent = CRT_PRECISION(pChData->lDCIR_AvgCurrent);
	ChSaveData.fDCIR_AvgTemp = ETC_PRECISION(pChData->lDCIR_AvgTemp);
	ChSaveData.fTimeGetChargeVoltage = VTG_PRECISION(pChData->lTimeGetChargeVoltage);

	ChSaveData.fComDCIR = ETC_PRECISION(pChData->lComDCIR);
	ChSaveData.fComCapacity = ETC_PRECISION(pChData->lComCapacity);

	//20210223 ksj
	ChSaveData.fDeltaOCV = VTG_PRECISION(pChData->lDeltaOCV);
	ChSaveData.fStartVoltage = VTG_PRECISION(pChData->lStartVoltage);
	ChSaveData.fDeltaCapacity = ETC_PRECISION(pChData->lDeltaCapacity);


	ChSaveData.currentFaultLevel1Cnt = pChData->currentFaultLevel1Cnt;
	ChSaveData.currentFaultLevel2Cnt = pChData->currentFaultLevel2Cnt;
	ChSaveData.currentFaultLevel3Cnt = pChData->currentFaultLevel3Cnt;
	ChSaveData.voltageFaultLevelV1Cnt = pChData->voltageFaultLevelV1Cnt;
	ChSaveData.voltageFaultLevelV2Cnt = pChData->voltageFaultLevelV2Cnt;
	ChSaveData.voltageFaultLevelV3Cnt = pChData->voltageFaultLevelV3Cnt;
	ChSaveData.ulTotalSamplingVoltageCnt = pChData->ulTotalSamplingVoltageCnt;	
	ChSaveData.ulTotalSamplingCurrentCnt = pChData->ulTotalSamplingCurrentCnt;

	int i=0;
	for(i=0; i<6; i++ )
	{
		ChSaveData.fJigTemp[i] = ETC_PRECISION(pChData->ulJigTemp[i]);
	}

	ChSaveData.fTempMax = ETC_PRECISION(pChData->ulTempMax);
	ChSaveData.fTempMin = ETC_PRECISION(pChData->ulTempMin);
	ChSaveData.fTempAvg = ETC_PRECISION(pChData->ulTempAvg);

	ChSaveData.fAuxData[0] = ETC_PRECISION(pChData->ulTempAvg);

	if( ChSaveData.state == EP_STATE_CHARGE || ChSaveData.state == EP_STATE_DISCHARGE || ChSaveData.state == EP_STATE_IMPEDANCE )
	{
		if( ChSaveData.fTempAvg < 0.0f || ChSaveData.fTempAvg > 120.0f
			|| strCapacityFormula.IsEmpty() || strDCRFormula.IsEmpty() )
		{
			return ChSaveData;
		}

		double dResult;
		CString strValue;
		CString strTemp;
		
		CString strCCCapacityT;
		CString strCVCapacityT;
		CString strCapacityT;
		CString strDCRT;

		strCapacityT = strCapacityFormula;		

		strValue.Format("%.1f", ChSaveData.fTempAvg);
		strCapacityT = GStr::str_Replace(strCapacityT, strValue, 'T' );

		strCCCapacityT = strCapacityT;
		strCVCapacityT = strCapacityT;

		// 1. 용량
		strValue.Format("%.4f", ChSaveData.fCapacity/1000.0f);
		strCapacityT = GStr::str_Replace(strCapacityT, strValue, 'C' );	

		char *postfix;
		postfix = GMath::infix_to_postfix((LPSTR)(LPCTSTR)strCapacityT);
		dResult = GMath::evalPostfix(postfix);
		delete postfix;
		
		strTemp.Format("%f", dResult);
		ChSaveData.fComCapacity = dResult*1000.0f;

		// 2. CC용량
		strValue.Format("%.4f", ChSaveData.fCcCapacity/1000.0f);
		strCCCapacityT = GStr::str_Replace(strCCCapacityT, strValue, 'C' );	
		
		postfix = GMath::infix_to_postfix((LPSTR)(LPCTSTR)strCCCapacityT);
		dResult = GMath::evalPostfix(postfix);
		delete postfix;
		
		strTemp.Format("%f", dResult);		
		ChSaveData.fAuxData[1] = dResult*1000.0f;
						
		// 3. CV용량
		strValue.Format("%.4f", ChSaveData.fCvCapacity/1000.0f);
		strCVCapacityT = GStr::str_Replace(strCVCapacityT, strValue, 'C' );	

		postfix = GMath::infix_to_postfix((LPSTR)(LPCTSTR)strCVCapacityT);
		dResult = GMath::evalPostfix(postfix);
		delete postfix;
		
		strTemp.Format("%f", dResult);
		ChSaveData.fAuxData[2] = dResult*1000.0f;

		// 4. DCIR
		strDCRT = strDCRFormula;		
		strValue.Format("%.1f", ChSaveData.fTempAvg);
		strDCRT = GStr::str_Replace(strDCRT, strValue, 'T' );

		strValue.Format("%.2f", ChSaveData.fImpedance);
		strDCRT = GStr::str_Replace(strDCRT, strValue, 'D' );	
		
		postfix = GMath::infix_to_postfix((LPSTR)(LPCTSTR)strDCRT);
		dResult = GMath::evalPostfix(postfix);
		delete postfix;

		strTemp.Format("%f", dResult);
		ChSaveData.fComDCIR = dResult;
	}

	return ChSaveData;	
}

BOOL CFormModule::ConvertChSaveData(int nTrayIndex, int nChIndex/*Tray Base Index*/, LPEP_CH_DATA pChData, LPSTR_SAVE_CH_DATA pChSaveData, CString strCapacityFormula, CString strDCRFormula)
{
	EP_GP_DATA gpData = EPGetGroupData(m_nModuleID, 0);
	ZeroMemory(pChSaveData, sizeof(STR_SAVE_CH_DATA));

	//모든 단위를 m단위로 변환하여 저장한다.
	pChSaveData->wModuleChIndex	= GetStartChIndex(nTrayIndex) + nChIndex;
	pChSaveData->wChIndex = nChIndex;
	pChSaveData->state = pChData->state;

	//사용자 Grading 실시 
	CStep *pStep = m_Condition.GetStep(pChData->nStepNo-1);
	if(pStep && pStep->m_Grading.GetGradeStepSize() > 0)
	{
		//Grading
		pChSaveData->grade	= GetUserSelect(pChData->channelCode, pChData->grade);
	}
	else
	{
		pChSaveData->grade = pChData->grade;
	}

	//Cell check 불량은 모두 NonCell로 처리한다.
	if(IsCellCheckFail(pChSaveData->channelCode))
	{
		pChSaveData->channelCode = EP_CODE_NONCELL; 
	}
	else
	{
		pChSaveData->channelCode = pChData->channelCode;
	}

	pChSaveData->nStepNo = pChData->nStepNo;
	pChSaveData->fStepTime =  MD2PCTIME(pChData->ulStepTime);			//sec 단위로 변환  
	pChSaveData->fTotalTime = MD2PCTIME(pChData->ulTotalTime);			//sec 단위로 변환
	pChSaveData->fVoltage =	 VTG_PRECISION(pChData->lVoltage);
	pChSaveData->fCurrent = CRT_PRECISION(pChData->lCurrent);
	pChSaveData->fWatt	= ETC_PRECISION(pChData->lWatt);
	pChSaveData->fWattHour = ETC_PRECISION(pChData->lWattHour);
	pChSaveData->fCapacity = ETC_PRECISION(pChData->lCapacity);
	pChSaveData->fImpedance = ETC_PRECISION(pChData->lImpedance);

	pChSaveData->fCcCapacity = CRT_PRECISION(pChData->lCcCapacity);
	pChSaveData->fCvCapacity = CRT_PRECISION(pChData->lCvCapacity);
	pChSaveData->fCcRunTime = MD2PCTIME(pChData->ulCcRunTime);
	pChSaveData->fCvRunTime = MD2PCTIME(pChData->ulCvRunTime);
	pChSaveData->fDCIR_V1 = VTG_PRECISION(pChData->lDCIR_V1);
	pChSaveData->fDCIR_V2 = VTG_PRECISION(pChData->lDCIR_V2);

	pChSaveData->fDCIR_AvgCurrent = CRT_PRECISION(pChData->lDCIR_AvgCurrent);
	pChSaveData->fDCIR_AvgTemp = ETC_PRECISION(pChData->lDCIR_AvgTemp);

	pChSaveData->fTimeGetChargeVoltage = VTG_PRECISION(pChData->lTimeGetChargeVoltage);

	pChSaveData->fComDCIR = ETC_PRECISION(pChData->lComDCIR);
	pChSaveData->fComCapacity = ETC_PRECISION(pChData->lComCapacity);
	//20210225 ksj
    pChSaveData->fDeltaOCV = VTG_PRECISION(pChData->lDeltaOCV);
	pChSaveData->fStartVoltage = VTG_PRECISION(pChData->lStartVoltage);
	pChSaveData->fDeltaCapacity = ETC_PRECISION(pChData->lDeltaCapacity);


	pChSaveData->currentFaultLevel1Cnt = pChData->currentFaultLevel1Cnt;
	pChSaveData->currentFaultLevel2Cnt = pChData->currentFaultLevel2Cnt;
	pChSaveData->currentFaultLevel3Cnt = pChData->currentFaultLevel3Cnt;
	pChSaveData->voltageFaultLevelV1Cnt = pChData->voltageFaultLevelV1Cnt;
	pChSaveData->voltageFaultLevelV2Cnt = pChData->voltageFaultLevelV2Cnt;
	pChSaveData->voltageFaultLevelV3Cnt = pChData->voltageFaultLevelV3Cnt;
	pChSaveData->ulTotalSamplingVoltageCnt = pChData->ulTotalSamplingVoltageCnt;	
	pChSaveData->ulTotalSamplingCurrentCnt = pChData->ulTotalSamplingCurrentCnt;	

	int i=0;
	for(i=0; i<6; i++ )
	{
		pChSaveData->fJigTemp[i] = ETC_PRECISION(pChData->ulJigTemp[i]);
	}

	pChSaveData->fTempMax = ETC_PRECISION(pChData->ulTempMax);
	pChSaveData->fTempMin = ETC_PRECISION(pChData->ulTempMin);
	pChSaveData->fTempAvg = ETC_PRECISION(pChData->ulTempAvg);

	pChSaveData->fAuxData[0] = ETC_PRECISION(pChData->ulTempAvg);

	
	if( pChSaveData->state == EP_STATE_CHARGE || pChSaveData->state == EP_STATE_DISCHARGE || pChSaveData->state == EP_STATE_IMPEDANCE )
	{
		if( pChSaveData->fTempAvg < 0.0f || pChSaveData->fTempAvg > 120.0f
			|| strCapacityFormula.IsEmpty() || strDCRFormula.IsEmpty() )
		{
			return TRUE;
		}

		double dResult;
		CString strValue;
		CString strTemp;

		CString strCCCapacityT;
		CString strCVCapacityT;
		CString strCapacityT;
		CString strDCRT;

		strCapacityT = strCapacityFormula;		

		strValue.Format("%.1f", pChSaveData->fTempAvg);
		strCapacityT = GStr::str_Replace(strCapacityT, strValue, 'T' );

		strCCCapacityT = strCapacityT;
		strCVCapacityT = strCapacityT;

		// 1. 용량
		strValue.Format("%.4f", pChSaveData->fCapacity/1000.0f);
		strCapacityT = GStr::str_Replace(strCapacityT, strValue, 'C' );	

		char *postfix;
		postfix = GMath::infix_to_postfix((LPSTR)(LPCTSTR)strCapacityT);
		dResult = GMath::evalPostfix(postfix);
		delete postfix;

		strTemp.Format("%f", dResult);
		pChSaveData->fComCapacity = dResult*1000.0f;


		// 2. CC용량
		strValue.Format("%.4f", pChSaveData->fCcCapacity/1000.0f);
		strCCCapacityT = GStr::str_Replace(strCCCapacityT, strValue, 'C' );	

		postfix = GMath::infix_to_postfix((LPSTR)(LPCTSTR)strCCCapacityT);
		dResult = GMath::evalPostfix(postfix);
		delete postfix;

		strTemp.Format("%f", dResult);		
		pChSaveData->fAuxData[1] = dResult*1000.0f;

		// 3. CV용량
		strValue.Format("%.4f", pChSaveData->fCvCapacity/1000.0f);
		strCVCapacityT = GStr::str_Replace(strCVCapacityT, strValue, 'C' );	

		postfix = GMath::infix_to_postfix((LPSTR)(LPCTSTR)strCVCapacityT);
		dResult = GMath::evalPostfix(postfix);
		delete postfix;

		strTemp.Format("%f", dResult);
		pChSaveData->fAuxData[2] = dResult*1000.0f;

		// 4. DCIR
		strDCRT = strDCRFormula;		
		strValue.Format("%.1f", pChSaveData->fTempAvg);  
		strDCRT = GStr::str_Replace(strDCRT, strValue, 'T' );

		strValue.Format("%.2f", pChSaveData->fImpedance);
		strDCRT = GStr::str_Replace(strDCRT, strValue, 'D' );	

		postfix = GMath::infix_to_postfix((LPSTR)(LPCTSTR)strDCRT);
		dResult = GMath::evalPostfix(postfix);
		delete postfix;

		strTemp.Format("%f", dResult);
		pChSaveData->fComDCIR = dResult;		
	}
	return TRUE;
}

CFormChannel* CFormModule::GetChannelInfo(int nChIndex)
{
//	if(pGroup == NULL)	return NULL;
	ASSERT(0);
	return NULL;
}

//Sensor의 Mapping
_MAPPING_DATA * CFormModule::GetSensorMap(int nNo, int nChIndex)
{
	_MAPPING_DATA *pMap = NULL;
	if(nChIndex >=0 && nChIndex < EP_MAX_SENSOR_CH)
	{
		if(nNo == sensorType1)
		{
			pMap =  &m_SensorMap1[nChIndex];
		}
		else if(nNo == sensorType2)
		{
			pMap =  &m_SensorMap2[nChIndex];
		}
	}
	return pMap;
}

// 손실된 data구간을 검색하여 복구한다.
// 모듈에서 download한 파일과 비교하여 data를 복구한다.
BOOL CFormModule::RestoreLostData_new() 
{	
	CString strTemp = _T("");
	CString strSelFileName = _T("");

	//Get ip address
	CString strIPAddress = GetIPAddress();
	if(strIPAddress.IsEmpty())
	{
		// strTemp.Format("%s 의 IP address를 찾을 수 없습니다.", ::GetModuleName(m_nModuleID));
		// AfxMessageBox(strTemp);
		return FALSE;
	}
	
	strSelFileName = GetResultFileName(0);
	CFormResultFile formFile;
	if(formFile.ReadFile(strSelFileName) < 0)
	{
		return ER_File_Not_Found;
	}
	
	char szBuff[_MAX_PATH];
	::GetTempPath(_MAX_PATH, szBuff);		//Get Windows temp. folder

	CString strRemoteLocation;
	CString strLocalFileName;
	CString strWinTempFolder(szBuff);
	
	EP_MD_SYSTEM_DATA *pSysData = EPGetModuleSysData(EPGetModuleIndex(m_nModuleID));
	if(pSysData == NULL)	return FALSE;

	//원격지에서 현재 SerialNo의 결과 data Folder를 구한다.
		
	CString strStepEndFile = _T("");
	CString strRemoteFileName = _T("");
	CString strSyncCode = _T("");

	RESULT_FILE_HEADER *pResultHeader = formFile.GetResultHeader();

// 	COleDateTime sycTime;
// 	sycTime.ParseDateTime(pResultHeader->szDateTime);	
// 	strSyncCode = sycTime.Format("%Y%m%d%H%M%S");

	strRemoteLocation = GetRemoteLocation(pSysData->nModuleGroupNo, formFile.GetTestSerialNo(), strIPAddress);
	if(strRemoteLocation.IsEmpty())
	{
		// strTemp.Format("%s에서 복구용 data를 찾을 수 없습니다.", strIPAddress);
		// AfxMessageBox(strTemp);
		return FALSE;
	}

	//DownLoad Channel result file from remote...
	CFtpDownLoad *pDownLoad = new CFtpDownLoad;
	BOOL bConnect = pDownLoad->ConnectFtpSession(strIPAddress);
	if(bConnect == FALSE)
	{
		delete pDownLoad;
		return FALSE;
	}

	int nChTotNo = ::EPGetChInGroup(m_nModuleID, 0);
	for(int nChIndex = 0; nChIndex <nChTotNo ; nChIndex++)
	{
		//1. Step 종료값을 DownLoad한다.		
		strRemoteFileName.Format("%s/ch%03d_SaveData.csv", strRemoteLocation, nChIndex+1);
		strStepEndFile.Format("%s\\%03d_stepEndData_ch%03d.csv", strWinTempFolder, GetModuleID(), nChIndex+1);
		if(pDownLoad->DownLoadFile(strStepEndFile, strRemoteFileName) < 1)
		{
			delete pDownLoad;
			// strTemp.Format("%s에서 channel %d의 data를 찾을 수 없습니다.", strIPAddress, nChIndex+1);
			// AfxMessageBox(strTemp);
			return FALSE;
		}
	}
	delete pDownLoad;

	//Module에서 Download한 파일을 Parsing한다.
	int nMaxStepCount = 0, nTemp;
	CModuleStepEndData *lpChData = new CModuleStepEndData[nChTotNo];
	for(int ch = 0; ch <nChTotNo; ch++)
	{	
		strStepEndFile.Format("%s\\%03d_stepEndData_ch%03d.csv", strWinTempFolder, GetModuleID(), ch+1);
		nTemp = lpChData[ch].LoadData(ch, strStepEndFile);

		if(nTemp  > nMaxStepCount)
		{
			nMaxStepCount = nTemp;
		}
	}

	//저장된 Step data가 없는 채널이 있음 
	if(nMaxStepCount < 2)
	{
		delete[] lpChData;
		// strTemp.Format("%s에서 시행한 Step 결과가 없습니다.", strIPAddress);
		// AfxMessageBox(strTemp);
		return FALSE;
	}
	else
	{
		// 마지막 Step 종료 메세지는 무시
		nMaxStepCount = nMaxStepCount - 1;
	}
	
	FILE *fp = NULL;
	//Create Result File=> 파일 존재 여부 검사	
	strSelFileName.Replace("\\", "\\\\");
	fp = fopen(strSelFileName, "wb+");	
	
	EP_FILE_HEADER *pFileHeader = formFile.GetFileHeader();
	if(fwrite(pFileHeader, sizeof(EP_FILE_HEADER), 1, fp) < 1)
	{
		fclose(fp);
		return FALSE;
	}
	
	RESULT_FILE_HEADER *pHeader = formFile.GetResultHeader();
	if(fwrite(pHeader, sizeof(RESULT_FILE_HEADER), 1, fp) < 1)
	{
		fclose(fp);
		return FALSE;
	}

	EP_MD_SYSTEM_DATA *lpGroupSysData = formFile.GetMDSysData();
	if(fwrite(lpGroupSysData, sizeof(EP_MD_SYSTEM_DATA), 1, fp) < 1)
	{
		fclose(fp);
		return FALSE;
	}

	STR_FILE_EXT_SPACE *pExtData =  formFile.GetExtraData();
	if(fwrite(pExtData, sizeof(STR_FILE_EXT_SPACE), 1, fp) < 1)	
	{
		fclose(fp);
		return FALSE;
	}

	PNE_RESULT_CELL_SERIAL *lpResultCellSerial= formFile.GetResultCellSerial();
	if(fwrite(lpResultCellSerial, sizeof(PNE_RESULT_CELL_SERIAL), 1, fp) < 1)
	{
		fclose(fp);
		return FALSE;
	}

	SENSOR_MAPPING_SAVE_TABLE sensorMap;	
	sensorMap.nModuleID = formFile.GetModuleID();
	memcpy(&sensorMap.mapData1, formFile.GetSensorMap(CFormResultFile::sensorType1), sizeof(_MAPPING_DATA)*EP_MAX_SENSOR_CH);
	memcpy(&sensorMap.mapData2, formFile.GetSensorMap(CFormResultFile::sensorType2), sizeof(_MAPPING_DATA)*EP_MAX_SENSOR_CH);
		
	if(fwrite(&sensorMap, sizeof(SENSOR_MAPPING_SAVE_TABLE), 1, fp) < 1)
	{
		fclose(fp);
		return FALSE;
	}

	CTestCondition *pCon = formFile.GetTestCondition();	
	if(fwrite(pCon->GetModelInfo(), sizeof(STR_CONDITION_HEADER), 1, fp) != 1)
	{
		fclose(fp);
		return FALSE;
	}

	if(fwrite(pCon->GetTestInfo(), sizeof(STR_CONDITION_HEADER), 1, fp) != 1)
	{
		fclose(fp);
		return FALSE;
	}

	EP_TEST_HEADER testHeader;
	ZeroMemory(&testHeader, sizeof(EP_TEST_HEADER));	
	testHeader.totalStep = (byte)pCon->GetTotalStepNo();
	testHeader.totalGrade = (byte)pCon->GetCountIsGradeStep();

	if(fwrite(&testHeader, sizeof(EP_TEST_HEADER), 1, fp) != 1)			
	{
		fclose(fp);
		return FALSE;
	}
	
	if(fwrite(pCon->GetCheckParam(), sizeof(STR_CHECK_PARAM), 1, fp) != 1)			
	{
		fclose(fp);
		return FALSE;
	}

	int step = 0;
	for(step = 0; step < pCon->GetTotalStepNo(); step++)
	{
		if(fwrite(&pCon->GetStep(step)->GetStepData(), sizeof(STR_COMMON_STEP), 1, fp) != 1)
		{
			fclose(fp);			
			return FALSE;
		}
	}	

	EP_STEP_END_HEADER endHeader;
	STR_STEP_RESULT StepResult;
	_MAPPING_DATA *pMapData;
	int ch = 0;

	STR_STEP_RESULT* pStepResult = formFile.GetStepData(step);		

	CString strCapacityFormula;
	CString strDCRFormula;

// 	strCapacityFormula = AfxGetApp()->GetProfileString(FROM_SYSTEM_SECTION, "CapacityFormula", _T("C+(0.003341-0.03567*(T-25)+0.003342*((T-25)^2)-0.00011*((T-25)^3))"));
// 	strDCRFormula = AfxGetApp()->GetProfileString(FROM_SYSTEM_SECTION, "DCRFormula", _T("D/(0.000140218657265343*(2.71^(2626.15545994168/(273+T))))"));
	
	for( step = 0; step < pCon->GetTotalStepNo(); step++ )
	{
		//////////////////////////////////////////////////////////////////////////
		// 1. Step에서 결과데이터가 존재하면 기존의 데이터를 사용하고
		// 2. Step에서 손실된 데이터가 있으면 SBC에서 저장한 데이터를 복구한다. 
		//////////////////////////////////////////////////////////////////////////

		pStepResult = formFile.GetStepData(step);
		if( pStepResult != NULL )
		{
			if(fwrite(&pStepResult->gpStateSave, sizeof(GROUP_STATE_SAVE), 1, fp) != 1)
			{
				fclose(fp);			
				return FALSE;
			}
			
			if(fwrite(&pStepResult->stepHead, sizeof(RESULT_STEP_HEADER), 1, fp) != 1)
			{
				fclose(fp);			
				return FALSE;
			}
			
			if(fwrite(&pStepResult->stepCondition, sizeof(STR_COMMON_STEP), 1, fp) != 1)
			{
				fclose(fp);			
				return FALSE;
			}
			
			endHeader.wStep = step;
			if(fwrite(&endHeader, sizeof(EP_STEP_END_HEADER), 1, fp) != 1)			
			{
				fclose(fp);
				return FALSE;
			}
			
			for(ch =0; ch < formFile.GetTotalCh(); ch++)	
			{
				STR_SAVE_CH_DATA* lpChannel = (STR_SAVE_CH_DATA *)pStepResult->aChData[ch];

				if(fwrite(lpChannel, sizeof(STR_SAVE_CH_DATA), 1, fp) != 1)
				{
					fclose(fp);			
					return FALSE;
				}			
			}
		}
		else
		{
			//////////////////////////////////////////////////////////////////////////
			// 1. GROUP_STATE_SAVE
			//////////////////////////////////////////////////////////////////////////

			ZeroMemory(&StepResult.gpStateSave, sizeof(GROUP_STATE_SAVE));
			
			if(fwrite(&StepResult.gpStateSave, sizeof(GROUP_STATE_SAVE), 1, fp) < 1)
			{
				fclose(fp);			
				return FALSE;
			}		

			//////////////////////////////////////////////////////////////////////////
			// 2. RESULT_STEP_HEADER
			//////////////////////////////////////////////////////////////////////////
			
			COleDateTime curTime = COleDateTime::GetCurrentTime();		//완료 시각
			ZeroMemory( &StepResult.stepHead, sizeof(RESULT_STEP_HEADER));				
			
			StepResult.stepHead.nModuleID = formFile.GetModuleID();
			StepResult.stepHead.wGroupIndex = pSysData->nModuleGroupNo;
			StepResult.stepHead.wJigIndex = 0;							
			sprintf(StepResult.stepHead.szEndDateTime, "%s", curTime.Format("%Y-%m-%d %H:%M:%S"));
			curTime = COleDateTime::GetCurrentTime();		// pModule->GetStepStartTime();
			sprintf(StepResult.stepHead.szStartDateTime, "%s", curTime.Format("%Y-%m-%d %H:%M:%S"));
			sprintf(StepResult.stepHead.szTestSerialNo, "%s", m_resultFile.GetTestSerialNo());
			sprintf(StepResult.stepHead.szModuleIP,	"%s", strIPAddress);
			sprintf(StepResult.stepHead.szTrayNo, "%s", m_resultFile.GetTrayNo());
			sprintf(StepResult.stepHead.szTraySerialNo, "%s", m_resultFile.GetTrayNo());
			
			if(fwrite(&StepResult.stepHead, sizeof(RESULT_STEP_HEADER), 1, fp) < 1)
			{
				fclose(fp);			
				return FALSE;
			}
			
			//////////////////////////////////////////////////////////////////////////
			// 3. STR_COMMON_STEP - 현재 Step의  조건을 기록 한다.
			//////////////////////////////////////////////////////////////////////////
			
			ZeroMemory(&StepResult.stepCondition, sizeof(STR_COMMON_STEP));
			CTestCondition *pCondition = formFile.GetTestCondition();
			CStep *pStep = pCondition->GetStep(step);
			if(pStep) 
			{
				StepResult.stepCondition = pStep->GetStepData();
			}
			
			if(fwrite(&StepResult.stepCondition, sizeof(STR_COMMON_STEP), 1, fp) < 1)
			{
				fclose(fp);			
				return FALSE;
			}		
			
			//////////////////////////////////////////////////////////////////////////
			// 4. EP_STEP_END_HEADER - 전체널 Step 결과에 대한 종합값 사용 안함 2006/2/16 (의미 없는 data)
			//////////////////////////////////////////////////////////////////////////
			
			EP_STEP_END_HEADER endHaeder;
			endHaeder.wStep = StepResult.stepCondition.stepHeader.stepIndex+1;
			if(fwrite(&endHaeder, sizeof(EP_STEP_END_HEADER), 1, fp) < 1)
			{
				fclose(fp);
				return FALSE;
			}

					
			//////////////////////////////////////////////////////////////////////////
			// 5. STR_SAVE_CH_DATA - Channel data structure type changed by KBH 2005/12/27
			//////////////////////////////////////////////////////////////////////////
			
			STR_SAVE_CH_DATA CChannerl;


			for( ch =0; ch<formFile.GetTotalCh(); ch++)	
			{
				ZeroMemory(&CChannerl, sizeof(STR_SAVE_CH_DATA));
				
				CChannerl = ConvertChSaveData( formFile.GetModuleID(), ch, lpChData[ch].GetStepData(step+1), strCapacityFormula, strDCRFormula );
				
				if(fwrite(&CChannerl, sizeof(STR_SAVE_CH_DATA), 1, fp) < 1)
				{
					fclose(fp);
					return FALSE;
				}
			}
		}
	}

	fclose(fp);
	return TRUE;
}

//손실된 data구간을 검색하여 복구한다.
//모듈에서 download한 파일과 비교하여 data를 복구한다.
BOOL CFormModule::RestoreLostData()
{
	int nTrayIndex = 2;
	CWaitCursor wait;
	CString strTemp;
	
	//Get ip address
	CString strIPAddress = GetIPAddress();
	if(strIPAddress.IsEmpty())
	{
		// strTemp.Format("%s 의 IP address를 찾을 수 없습니다.", ::GetModuleName(m_nModuleID));
		// AfxMessageBox(strTemp);
		return FALSE;
	}

	char szBuff[_MAX_PATH];
	::GetTempPath(_MAX_PATH, szBuff);		//Get Windows temp. folder
	CString strRemoteLocation;
	CString strRemoteFileName, strLocalFileName;
	CString strWinTempFolder(szBuff);
	CString strStepEndFile;

	EP_MD_SYSTEM_DATA *pSysData = EPGetModuleSysData(EPGetModuleIndex(m_nModuleID));
	if(pSysData == NULL)	return FALSE;

	//원격지에서 현재 SerialNo의 결과 data Folder를 구한다.
//	strRemoteLocation = GetRemoteLocation(GetTestSerialNo(), strIPAddress);
	strRemoteLocation = GetRemoteLocation(pSysData->nModuleGroupNo, GetTestSerialNo(), strIPAddress);
	if(strRemoteLocation.IsEmpty())
	{
//		strTemp.Format("%s에서 복구용 data를 찾을 수 없습니다.", strIPAddress);
//		AfxMessageBox(strTemp);
		return FALSE;
	}

	//DownLoad Channel result file from remote...
	CFtpDownLoad *pDownLoad = new CFtpDownLoad;
	BOOL bConnect = pDownLoad->ConnectFtpSession(strIPAddress);
	if(bConnect == FALSE)
	{
		delete pDownLoad;
		// strTemp.Format("%s에 접속 할 수 없습니다.", strIPAddress);
		// AfxMessageBox(strTemp);
		return FALSE;
	}

	int nChTotNo = ::EPGetChInGroup(m_nModuleID, 0);
	for(int nChIndex = 0; nChIndex <nChTotNo ; nChIndex++)
	{
		//1. Step 종료값을 DownLoad한다.
		strRemoteFileName.Format("%s/ch%03d_SaveData.csv", strRemoteLocation, nChIndex+1);
		strStepEndFile.Format("%s\\stepEndData_ch%03d.csv", strWinTempFolder, nChIndex+1);
		if(pDownLoad->DownLoadFile(strStepEndFile, strRemoteFileName) < 1)
		{
			delete pDownLoad;
			// strTemp.Format("%s에서 channel %d의 data를 찾을 수 없습니다.", strIPAddress, nChIndex+1);
			// AfxMessageBox(strTemp);
			return FALSE;
		}
	}
	delete pDownLoad;

	//Module에서 Download한 파일을 Parsing한다.
	int nMaxStepCount = 0, nTemp;
	CModuleStepEndData *lpChData = new CModuleStepEndData[nChTotNo];
	for(int ch = 0; ch <nChTotNo; ch++)
	{
		strStepEndFile.Format("%s\\stepEndData_ch%03d.csv", strWinTempFolder, ch+1);
		nTemp = lpChData[ch].LoadData(ch, strStepEndFile);
	
		if(nTemp  > nMaxStepCount)
		{
			nMaxStepCount = nTemp;
		}
	}

	//저장된 Step data가 없는 채널이 있음 
	if(nMaxStepCount < 2)
	{
		delete[] lpChData;
		strTemp.Format(TEXT_LANG[0], strIPAddress);//"%s에서 시행한 Step 결과가 없습니다."
		AfxMessageBox(strTemp);
		return FALSE;
	}
	else
	{
		// 마지막 Step 종료 메세지는 무시
		nMaxStepCount = nMaxStepCount - 1;
	}

	for(int j=0; j<GetTotalJig(); j++)
	{
		//Backup original file
		CString strResultFileName = GetResultFileName(j);
		if(strResultFileName.IsEmpty() == FALSE)
		{
			char	drive[_MAX_PATH]; 
			char	dir[_MAX_PATH]; 
			char	fname[_MAX_PATH]; 
			char	ext[_MAX_PATH]; 
			_splitpath((LPSTR)(LPCTSTR)strResultFileName, drive, dir, fname, ext);

			CString srcFileName, destFileName;
			srcFileName =  strResultFileName;								//복구하고자 하는 파일
			destFileName.Format("%s\\%s\\%s.bak", drive, dir, fname);		//복구해서 임시로 생성하는 파일
			_unlink(destFileName);											//동일명으로 backup file이 있으면 삭제한다.
			rename(strResultFileName, destFileName);						//현재 파일을 확장자 .bak으로 해서 이름을 바꾼다.
		}
	}
	
	EP_CH_DATA chData;
	LPEP_CH_DATA lpMDChData;
	int nSize = nChTotNo*SizeofCHData() + sizeof(EP_STEP_END_HEADER);
	LPVOID lpBuff = new char[nSize];

	//각 step의 종료값을 구한다.
	for(int step = 0; step < nMaxStepCount; step ++)
	{
		//모든 채널 검색 
		for(int ch = 0; ch <nChTotNo; ch++)
		{
			ZeroMemory(&chData, sizeof(EP_CH_DATA));
			lpMDChData = lpChData[ch].GetStepData(step+1);
			if(lpMDChData)
			{
				memcpy(&chData, lpMDChData, sizeof(EP_CH_DATA));
			}
			else
			{
				chData.wChIndex = ch;
				chData.nStepNo = step+1;
			}
			//전체널 Step data 검색
			memcpy((char *)lpBuff+ (sizeof(EP_STEP_END_HEADER) + ch * SizeofCHData()), (char *)&chData, SizeofCHData());
		}
		
		//현재 Network에서 결과 data가 전송되는 경우 data 순서가 바뀔수 있다. 따라서 Run이 아닌 경우에 복구 하도록 한다.
		::SendMessage(	AfxGetMainWnd()->m_hWnd, 
						EPWM_STEP_ENDDATA_RECEIVE, 
						(WPARAM)MAKELONG(0,  m_nModuleID), 
						(LPARAM)lpBuff
					);			//Send to Parent Wnd Step End Data Received
	}

	delete[] lpChData;
	delete[] lpBuff;	
	return TRUE;
}

CString CFormModule::GetIPAddress()
{
	return ::EPGetModuleIP(m_nModuleID);
}

CTestCondition * CFormModule::GetCondition()
{
	return &m_Condition;
}

//Log Temp 파일을 읽어서 
//결과 파일을 생성한다.
BOOL CFormModule::CreateTrayResultFile(int nTrayIndex)
{
	CString strLogTempFile, strTemp;
	strTemp = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"CTSMon");	//Get Current Folder(CTSMon Folde)
	if(strTemp.IsEmpty())
	{
		return FALSE;
	}

	strLogTempFile.Format("%s\\Temp\\md%djig%d.log", strTemp, m_nModuleID, nTrayIndex+1);

	CString strResultFileName = GetResultFileName(nTrayIndex);
	//Test Result file Name Check
	if(strResultFileName.IsEmpty() || strLogTempFile.IsEmpty())
	{
		return FALSE;
	}

	//Test Log File Open
	FILE *fpLogStream = fopen(strLogTempFile, "rb+");												//Open Test Log File 
	if(fpLogStream == NULL)
	{
		return FALSE;
	}

	fseek(fpLogStream, 0, SEEK_END);
	int nFileSize = ftell(fpLogStream);		//Get File Size
	fseek(fpLogStream, 0, SEEK_SET);			

	///Test Log File Format and Module Number Check
	if(ReadEPFileHeader(TEST_LOG_FILE, fpLogStream) < 0)		//File Format Check;
	{
		fclose(fpLogStream);
		fpLogStream = NULL;
//		strLogTempFile.Format("%s Test Log File M%dG%dTest.log Format Error.", ::GetModuleName(nModuleID, nGroupIndex), nModuleID, nGroupIndex+1);
//		WriteLog((LPSTR)(LPCTSTR)strLogTempFile);
		return FALSE;
	}
	
	//Read Test File Header
	RESULT_FILE_HEADER fileHeader;
	if(fread(&fileHeader, sizeof(RESULT_FILE_HEADER), 1, fpLogStream) < 1)		//Read File Header
	{
		fclose(fpLogStream);
		fpLogStream = NULL;
		return FALSE;
	}

	//File Data Check
	if(fileHeader.nModuleID != m_nModuleID)	// || pFileHeader->nGroupIndex != nGroupIndex
												// || (strcmp(pFileHeader->szTestSerialNo, m_pModule[nModuleIndex].pGroup[nGroupIndex].sTestResultFileHeader.szTestSerialNo)!= 0))
	{
		fclose(fpLogStream);
		fpLogStream = NULL;
		return FALSE;
	}
	
	//Create Test Result File
	//해당 폴더가 존재 하지 않을 경우 
	FILE *fpRltStream = fopen(strResultFileName, "wb");
	if(fpRltStream == NULL)																
	{
		fclose(fpLogStream);
		fpLogStream = NULL;
		return FALSE;
	}

	//Copy Result File Data From Test Log File.
	//////////////////////////////////////////////////////////////////////////
	int nRtn = TRUE;
	LPVOID lpBuffer = new char[nFileSize];
	ASSERT(lpBuffer);
	fseek(fpLogStream, 0, SEEK_SET);
	if(fread(lpBuffer, nFileSize, 1, fpLogStream) < 1)		//Read Test Log file 
	{
		nRtn = FALSE;
		goto _EXIT;
	}
	if(fwrite(lpBuffer, nFileSize, 1, fpRltStream) < 1)		//Write Result File
	{
		nRtn = FALSE;
		goto _EXIT;
	}
	//////////////////////////////////////////////////////////////////////////

	//Over write information
	//////////////////////////////////////////////////////////////////////////
	fseek(fpRltStream, 0, SEEK_SET);						//Write File ID and Version
	if(WriteEPFileHeader(RESULT_FILE, fpRltStream) == FALSE)
	{
		nRtn = FALSE;
		goto _EXIT;
	}
/*	sprintf(fileHeader.szDateTime, "%s", GetRunStartTime().Format());
	if(fwrite(&fileHeader, sizeof(RESULT_FILE_HEADER), 1, fpRltStream) < 1)		//Write Result File
	{
		nRtn = FALSE;
		goto _EXIT;
	}
*/	//////////////////////////////////////////////////////////////////////////

	fseek(fpRltStream, sizeof(RESULT_FILE_HEADER),SEEK_CUR);
	fseek(fpRltStream, sizeof(EP_MD_SYSTEM_DATA),SEEK_CUR);
	fseek(fpRltStream, sizeof(STR_FILE_EXT_SPACE),SEEK_CUR);

	//20090920 Cell Serail No	//file version up
	if(fwrite(m_TrayData[nTrayIndex].GetResultCellSerial(), sizeof(PNE_RESULT_CELL_SERIAL), 1, fpRltStream) <1)
	{
		nRtn = FALSE;
		goto _EXIT;
	}

	//Mapping data를 Update 함 
	SENSOR_MAPPING_SAVE_TABLE	sensorMap;
	sensorMap.nModuleID = m_nModuleID;
	memcpy(&sensorMap.mapData1, GetSensorMap(sensorType1), sizeof(_MAPPING_DATA)*EP_MAX_SENSOR_CH);
	memcpy(&sensorMap.mapData2, GetSensorMap(sensorType2), sizeof(_MAPPING_DATA)*EP_MAX_SENSOR_CH);
	if(fwrite(&sensorMap, sizeof(SENSOR_MAPPING_SAVE_TABLE), 1, fpRltStream) <1)
	{
		nRtn = FALSE;
		goto _EXIT;
	}

_EXIT:
	delete[] lpBuffer;		lpBuffer = NULL;
	fclose(fpLogStream);	fpLogStream = NULL;
	fclose(fpRltStream);	fpRltStream = NULL;
	
	if(nRtn == FALSE)
	{
		TRACE("Test result file %s creat fail\n", strResultFileName);
	}
	else
	{
		TRACE("Test result file %s created\n", strResultFileName);
	}
	return nRtn;
}

BOOL CFormModule::CreateTrayCellCheckResultFile( int nTrayIndex, CString strResultFileName )
{
	CString strLogTempFile, strTemp;
	strTemp = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"CTSMon");	//Get Current Folder(CTSMon Folde)
	if(strTemp.IsEmpty())
	{
		return FALSE;
	}

	strLogTempFile.Format("%s\\Temp\\md%djig%d.log", strTemp, m_nModuleID, nTrayIndex+1);

	//Test Result file Name Check
	if(strResultFileName.IsEmpty() || strLogTempFile.IsEmpty())
	{
		return FALSE;
	}

	//Test Log File Open
	FILE *fpLogStream = fopen(strLogTempFile, "rb+");												//Open Test Log File 
	if(fpLogStream == NULL)
	{
		return FALSE;
	}

	fseek(fpLogStream, 0, SEEK_END);
	int nFileSize = ftell(fpLogStream);		//Get File Size
	fseek(fpLogStream, 0, SEEK_SET);			

	///Test Log File Format and Module Number Check
	if(ReadEPFileHeader(TEST_LOG_FILE, fpLogStream) < 0)		//File Format Check;
	{
		fclose(fpLogStream);
		fpLogStream = NULL;
//		strLogTempFile.Format("%s Test Log File M%dG%dTest.log Format Error.", ::GetModuleName(nModuleID, nGroupIndex), nModuleID, nGroupIndex+1);
//		WriteLog((LPSTR)(LPCTSTR)strLogTempFile);
		return FALSE;
	}
	
	//Read Test File Header
	RESULT_FILE_HEADER fileHeader;
	if(fread(&fileHeader, sizeof(RESULT_FILE_HEADER), 1, fpLogStream) < 1)		//Read File Header
	{
		fclose(fpLogStream);
		fpLogStream = NULL;
		return FALSE;
	}

	//File Data Check
	if(fileHeader.nModuleID != m_nModuleID)	// || pFileHeader->nGroupIndex != nGroupIndex
												// || (strcmp(pFileHeader->szTestSerialNo, m_pModule[nModuleIndex].pGroup[nGroupIndex].sTestResultFileHeader.szTestSerialNo)!= 0))
	{
		fclose(fpLogStream);
		fpLogStream = NULL;
		return FALSE;
	}
	
	//Create Test Result File
	//해당 폴더가 존재 하지 않을 경우 
	FILE *fpRltStream = fopen(strResultFileName, "wb");
	if(fpRltStream == NULL)																
	{
		fclose(fpLogStream);
		fpLogStream = NULL;
		return FALSE;
	}

	//Copy Result File Data From Test Log File.
	//////////////////////////////////////////////////////////////////////////
	int nRtn = TRUE;
	LPVOID lpBuffer = new char[nFileSize];
	ASSERT(lpBuffer);
	fseek(fpLogStream, 0, SEEK_SET);
	if(fread(lpBuffer, nFileSize, 1, fpLogStream) < 1)		//Read Test Log file 
	{
		nRtn = FALSE;
		goto _EXIT;
	}
	if(fwrite(lpBuffer, nFileSize, 1, fpRltStream) < 1)		//Write Result File
	{
		nRtn = FALSE;
		goto _EXIT;
	}
	//////////////////////////////////////////////////////////////////////////

	//Over write information
	//////////////////////////////////////////////////////////////////////////
	fseek(fpRltStream, 0, SEEK_SET);						//Write File ID and Version
	if(WriteEPFileHeader(RESULT_FILE, fpRltStream) == FALSE)
	{
		nRtn = FALSE;
		goto _EXIT;
	}
/*	sprintf(fileHeader.szDateTime, "%s", GetRunStartTime().Format());
	if(fwrite(&fileHeader, sizeof(RESULT_FILE_HEADER), 1, fpRltStream) < 1)		//Write Result File
	{
		nRtn = FALSE;
		goto _EXIT;
	}
*/	//////////////////////////////////////////////////////////////////////////

	fseek(fpRltStream, sizeof(RESULT_FILE_HEADER),SEEK_CUR);
	fseek(fpRltStream, sizeof(EP_MD_SYSTEM_DATA),SEEK_CUR);
	fseek(fpRltStream, sizeof(STR_FILE_EXT_SPACE),SEEK_CUR);

	//20090920 Cell Serail No	//file version up
	if(fwrite(m_TrayData[nTrayIndex].GetResultCellSerial(), sizeof(PNE_RESULT_CELL_SERIAL), 1, fpRltStream) <1)
	{
		nRtn = FALSE;
		goto _EXIT;
	}

	//Mapping data를 Update 함 
	SENSOR_MAPPING_SAVE_TABLE	sensorMap;
	sensorMap.nModuleID = m_nModuleID;
	memcpy(&sensorMap.mapData1, GetSensorMap(sensorType1), sizeof(_MAPPING_DATA)*EP_MAX_SENSOR_CH);
	memcpy(&sensorMap.mapData2, GetSensorMap(sensorType2), sizeof(_MAPPING_DATA)*EP_MAX_SENSOR_CH);
	if(fwrite(&sensorMap, sizeof(SENSOR_MAPPING_SAVE_TABLE), 1, fpRltStream) <1)
	{
		nRtn = FALSE;
		goto _EXIT;
	}

_EXIT:
	delete[] lpBuffer;		lpBuffer = NULL;
	fclose(fpLogStream);	fpLogStream = NULL;
	fclose(fpRltStream);	fpRltStream = NULL;
	
	if(nRtn == FALSE)
	{
		TRACE("Test result file %s creat fail\n", strResultFileName);
	}
	else
	{
		TRACE("Test result file %s created\n", strResultFileName);
	}
	return nRtn;
}

BOOL CFormModule::WriteEPFileHeader(int nFile, FILE *fp)
{
	//Write ElicoPower FileHeader
	if(fp == NULL)	return FALSE;
	
	LPEP_FILE_HEADER	lpEpFileHeader;
	lpEpFileHeader = new EP_FILE_HEADER;
	ASSERT(lpEpFileHeader);
	ZeroMemory(lpEpFileHeader, sizeof(EP_FILE_HEADER));

	COleDateTime dateTime = COleDateTime::GetCurrentTime();
	sprintf(lpEpFileHeader->szCreateDateTime, "%s", dateTime.Format());

	switch(nFile)
	{
	case RESULT_FILE:
		sprintf(lpEpFileHeader->szFileID, RESULT_FILE_ID);
		sprintf(lpEpFileHeader->szFileVersion, RESULT_FILE_VER3);
		sprintf(lpEpFileHeader->szDescrition, "PNE Cell Test System Result File");
		break;
	case TEST_LOG_FILE:
		sprintf(lpEpFileHeader->szFileID, TEST_LOG_FILE_ID);
		sprintf(lpEpFileHeader->szFileVersion, TEST_LOG_FILE_VER);
		sprintf(lpEpFileHeader->szDescrition, "PNE Cell Test System Test Log File");
		break;
	case CONDITION_FILE:
		sprintf(lpEpFileHeader->szFileID, CONDITION_FILE_ID);
		sprintf(lpEpFileHeader->szFileVersion, CONDITION_FILE_VER);
		sprintf(lpEpFileHeader->szDescrition, "PNE Cell Test System Condition File");
		break;
	case GRADE_CODE_FILE:
		sprintf(lpEpFileHeader->szFileID, GRADE_CODE_FILE_ID);
		sprintf(lpEpFileHeader->szFileVersion, GRADE_CODE_FILE_VER);
		sprintf(lpEpFileHeader->szDescrition, "PNE Cell Test System Grade File");
		break;
	case IROCV_FILE:
		sprintf(lpEpFileHeader->szFileID, IROCV_FILE_ID);
		sprintf(lpEpFileHeader->szFileVersion, IROCV_FILE_VER1);
		sprintf(lpEpFileHeader->szDescrition, "PNE Cell Test System IROCV File");
		break;
	default:
		delete lpEpFileHeader;
		lpEpFileHeader = NULL;
		return FALSE;
	}
	
	lpEpFileHeader->nResultFileType = GetTrayInfo(0)->m_nResultFileType;
	lpEpFileHeader->nContactErrlimit = GetTrayInfo(0)->m_nContactErrlimit;
	lpEpFileHeader->nCapaErrlimit = GetTrayInfo(0)->m_nCapaErrlimit;
	lpEpFileHeader->nChErrlimit = GetTrayInfo(0)->m_nChErrlimit;
	lpEpFileHeader->nTabDeepth = GetTrayInfo(0)->m_nTabDeepth;
	lpEpFileHeader->nTrayHeight = GetTrayInfo(0)->m_nTrayHeight;
	lpEpFileHeader->nTrayType = GetTrayInfo(0)->m_nTrayType;
	lpEpFileHeader->nInputCellCount = GetTrayInfo(0)->GetInputCellCnt();

	fwrite(lpEpFileHeader, sizeof(EP_FILE_HEADER), 1, fp);
	delete lpEpFileHeader;
	lpEpFileHeader = NULL;
	return TRUE;
}

int CFormModule::ReadEPFileHeader(int nFile, FILE *fp)
{
	int nRtn = 0;
	LPEP_FILE_HEADER	lpEpFileHeader, lpTempHeader;
	lpEpFileHeader = new EP_FILE_HEADER;
	lpTempHeader = new EP_FILE_HEADER;

	ASSERT(lpEpFileHeader);
	ASSERT(lpTempHeader);
	ASSERT(fp);
	
	fread(lpEpFileHeader, sizeof(EP_FILE_HEADER), 1, fp);

	switch(nFile)
	{
	case RESULT_FILE:
		sprintf(lpTempHeader->szFileID, RESULT_FILE_ID);
		sprintf(lpTempHeader->szFileVersion, RESULT_FILE_VER3);
		break;
	case TEST_LOG_FILE:
		sprintf(lpTempHeader->szFileID, TEST_LOG_FILE_ID);
		sprintf(lpTempHeader->szFileVersion, TEST_LOG_FILE_VER);
		break;
	case CONDITION_FILE:
		sprintf(lpTempHeader->szFileID, CONDITION_FILE_ID);
		sprintf(lpTempHeader->szFileVersion, CONDITION_FILE_VER);
		break;
	case GRADE_CODE_FILE:
		sprintf(lpTempHeader->szFileID, GRADE_CODE_FILE_ID);
		sprintf(lpTempHeader->szFileVersion, GRADE_CODE_FILE_VER);
		break;
	case IROCV_FILE:
		sprintf(lpTempHeader->szFileID, IROCV_FILE_ID);
		sprintf(lpTempHeader->szFileVersion, IROCV_FILE_VER1);
		break;

	default:
		nRtn = -1;
	}

	if(strcmp(lpEpFileHeader->szFileID, lpTempHeader->szFileID) != 0)
	{
		nRtn = -2;
	}
	delete lpTempHeader;
	lpTempHeader = NULL;
	delete lpEpFileHeader;
	lpEpFileHeader = NULL;
	return nRtn;
}

void CFormModule::UpdateStartTime(COleDateTime *pStartT)
{
	if(pStartT == NULL)
	{
		m_OleRunStartTime = COleDateTime::GetCurrentTime();
	}
	else
	{
		m_OleRunStartTime.SetDateTime(pStartT->GetYear(), pStartT->GetMonth(), pStartT->GetDay(), pStartT->GetHour(), pStartT->GetMinute(), pStartT->GetSecond());
	}

	UpdateStepStartTime();
}

void	CFormModule::UpdateStepStartTime()
{
	m_OleStepStartTime = COleDateTime::GetCurrentTime();
}

//모듈에 전송한 최종 시험조건을 저장한다.
BOOL CFormModule::WriteTestLogTempFile(CTestCondition *pCondition)
{
	for(int nTray = 0; nTray < INSTALL_TRAY_PER_MD_1; nTray++)
	{
		if(WriteTrayTestLogFile(pCondition, nTray)== FALSE)
		{
			TRACE("Module %d Jig %d Test log file create fail!!!\n", m_nModuleID, nTray+1);
			return FALSE;
		}
	}
	m_Condition.SetTestConditon(pCondition);
	return TRUE;
}

//임시파일(모듈에 전송한 최종 시험조건)을 읽어서 
//최종 시작 정보로 갱신한다.
BOOL CFormModule::ClearTestLogTempFile()
{
	for(int nTray = 0; nTray < INSTALL_TRAY_PER_MD_1 ; nTray++)
	{
		if(ClearTrayTestLogFile(nTray)==FALSE)
		{
			TRACE("Module %d Jig %d Test log file update fail!!!\n", m_nModuleID, nTray+1);
		}
	}
	return TRUE;
}


BOOL CFormModule::UpdateTestLogTempFile()
{
	for(int nTray = 0; nTray < INSTALL_TRAY_PER_MD_1; nTray++)
	{
		if(UpdateTrayTestLogFile(nTray) == FALSE)
		{
			TRACE("Module %d Jig %d Test log file update fail!!!\n", m_nModuleID, nTray+1);
		}
	}
	return TRUE;
}

BOOL CFormModule::UpdateTestLogHeaderTempFile()
{
	for(int nTray = 0; nTray < INSTALL_TRAY_PER_MD_1; nTray++)
	{
		if(UpdateTrayTestLogHeaderFile(nTray)==FALSE)
		{
			TRACE("Module %d Jig %d Test log header file update fail!!!\n", m_nModuleID, nTray+1);
		}
	}
	return TRUE;
}

//이어서 작업을 하기 위해 기존의 저장해 놓은 Tmep 파일에서 Data 저장 파일을 읽어 들인다.
BOOL CFormModule::ReadTestLogFile()
{
	//Clear All Data
	ResetGroupData();

	for(int nTray = 0; nTray < INSTALL_TRAY_PER_MD_1; nTray++)
	{
		if(ReadTrayTestLogFile(nTray) == FALSE)
		{
			TRACE("Module %d Jig %d Test log file reading fail!!!\n", m_nModuleID, nTray+1);
		}
	}
	return TRUE;
}

//Save test Condition File
BOOL CFormModule::SaveConditionFile(FILE *fp, CTestCondition *pCondition, BOOL bSaveFileHeader)
{
	ASSERT(fp != NULL);
	ASSERT(pCondition != NULL);

	if(bSaveFileHeader)
	{
		//Write ElicoPower FileHeader
		if(WriteEPFileHeader(CONDITION_FILE, fp) == FALSE)
			return FALSE;
	}

	STR_CONDITION_HEADER *pModelData = pCondition->GetModelInfo();
	if(fwrite(pModelData, sizeof(STR_CONDITION_HEADER), 1, fp) != 1)		return FALSE;
	STR_CONDITION_HEADER *pTestData = pCondition->GetTestInfo();
	if(fwrite(pTestData, sizeof(STR_CONDITION_HEADER), 1, fp) != 1)			return FALSE;

	EP_TEST_HEADER testHeader;
	ZeroMemory(&testHeader, sizeof(EP_TEST_HEADER));
	testHeader.totalStep = pCondition->GetTotalStepNo();
	
	if(fwrite(&testHeader, sizeof(EP_TEST_HEADER), 1, fp) != 1)				return FALSE;
	STR_CHECK_PARAM *pCheckParam = pCondition->GetCheckParam();
	if(fwrite(pCheckParam, sizeof(STR_CHECK_PARAM), 1, fp) != 1)			return FALSE;

	STR_COMMON_STEP comStep;
	CStep	*pStep;
	for(int i =0; i<pCondition->GetTotalStepNo(); i++)
	{
		pStep = pCondition->GetStep(i);
		if(pStep)
		{
			comStep = pStep->GetStepData();

		}
		if(fwrite(&comStep, sizeof(STR_COMMON_STEP), 1, fp) != 1)			return FALSE; 
	}
	return TRUE;
}

int CFormModule::ReadTestConditionFile(FILE *fp, STR_CONDITION *pCondition, char *fileID, char *fileVer)
{
	if(fp == NULL || pCondition == NULL)	return FALSE;

	if(fread(&pCondition->modelHeader, sizeof(STR_CONDITION_HEADER), 1, fp) != 1)			return FALSE;
	if(fread(&pCondition->conditionHeader, sizeof(STR_CONDITION_HEADER), 1, fp) != 1)		return FALSE;
	if(fread(&pCondition->testHeader, sizeof(EP_TEST_HEADER), 1, fp) != 1)					return FALSE;
	if(fread(&pCondition->checkParam, sizeof(STR_CHECK_PARAM), 1, fp) != 1)				return FALSE;

	STR_COMMON_STEP *lpBuffer = NULL;
	if(fileID != NULL && fileVer != NULL && strcmp(fileID, RESULT_FILE_ID) == 0)	//공정 결과 파일인 경우 
	{
		//호환을 위해 Version에 맞게 read
		//Result File의 File Ver이 RESULT_FILE_VER1인 것만 STEP_V1 structure로 되어 있다.
		for(int i =0; i<pCondition->testHeader.totalStep; i++)
		{
			if(strcmp(fileVer, RESULT_FILE_VER1) == 0)
			{
				lpBuffer = new STR_COMMON_STEP;
				if(fread((char *)lpBuffer, sizeof(STR_COMMON_STEP), 1, fp) != 1)		return FALSE;
				pCondition->apStepList.Add(lpBuffer);
			}
		}	
	}
	else	//공정 결과 파일이 아닌 경우 
	{
		for(int i =0; i<pCondition->testHeader.totalStep; i++)
		{
			lpBuffer = new STR_COMMON_STEP;
			if(fread((char *)lpBuffer, sizeof(STR_COMMON_STEP), 1, fp) != 1)		return FALSE;
			pCondition->apStepList.Add(lpBuffer);
		}
	}
	return TRUE;
}
/*
int CFormModule::GetStepDataSize(int nStepIndex, STR_CONDITION *pCondition)
{
	EP_STEP_HEADER * lpStepHeader;
	int nStepDataSize;
	nStepDataSize = pCondition->testHeader.totalStep;
	ASSERT(pCondition->testHeader.totalStep == pCondition->apStepList.GetSize());
	if(nStepDataSize <=0 )	return -1;
	if(nStepIndex < 0 || nStepIndex >= nStepDataSize)	return -1; 
		
	lpStepHeader = (EP_STEP_HEADER *)pCondition->apStepList[nStepIndex];
	ASSERT(lpStepHeader);
	
	switch(lpStepHeader->type)
	{
	case EP_TYPE_CHARGE	:		nStepDataSize = sizeof(EP_CHARGE_STEP);		break;
	case EP_TYPE_DISCHARGE:		nStepDataSize = sizeof(EP_DISCHARGE_STEP);	break;
	case EP_TYPE_IMPEDANCE	:	nStepDataSize = sizeof(EP_IMPEDANCE_STEP);	break;
	case EP_TYPE_OCV		:	nStepDataSize = sizeof(EP_OCV_STEP);		break;
	case EP_TYPE_REST		:	nStepDataSize = sizeof(EP_REST_STEP);		break;
	case EP_TYPE_END		:	nStepDataSize = sizeof(EP_END_STEP);		break;
	case EP_TYPE_LOOP:			nStepDataSize = sizeof(EP_LOOP_STEP);		break;
	default:					nStepDataSize = 0;							break;
	}
	return nStepDataSize;
}

int CFormModule::GetGradeDataSize(int nStepIndex, STR_CONDITION *pCondition)
{
	EP_GRADE_HEADER	*lpGradeHeader;
	int nDataSize = 0;
	nDataSize = pCondition->testHeader.totalGrade;
	ASSERT(pCondition->apGradeList.GetSize() == nDataSize);
	if(nDataSize <=0 )	return -1;
	if(nStepIndex < 0 || nStepIndex >= nDataSize)	return -1; 
	lpGradeHeader = (EP_GRADE_HEADER *)pCondition->apGradeList[nStepIndex];
	ASSERT(lpGradeHeader);

	nDataSize = lpGradeHeader->totalStep*sizeof(EP_GRADE) + sizeof(EP_GRADE_HEADER);
	return nDataSize;
}
*/

//Remove Test Condition List
BOOL CFormModule::RemoveCondition(STR_CONDITION *pCondition)
{
	if(pCondition == NULL)	return TRUE;

	int nSize = pCondition->apStepList.GetSize();
	char *pObject; 

	if(nSize > 0)
	{
		for (int i = nSize-1 ; i>=0 ; i--)
		{
			if( (pObject = (char *)pCondition->apStepList[i]) != NULL )
			{
				delete[] pObject; // Delete the original element at 0.
				pObject = NULL;
				pCondition->apStepList.RemoveAt(i);  
			}
		}
		pCondition->apStepList.RemoveAll();
	}

/*	nSize = pCondition->apGradeList.GetSize();
	char *pObject1; 

	if(nSize > 0)
	{
		for (int i = nSize-1 ; i>=0 ; i--)
		{
			if( (pObject1 = (char *)pCondition->apGradeList[i]) != NULL )
			{
				delete [] pObject1; // Delete the original element at 0.
				pObject1 = NULL;
				pCondition->apGradeList.RemoveAt(i);  
			}
		}
		pCondition->apGradeList.RemoveAll();
	}
*/
	delete pCondition;
	pCondition = NULL;

	return TRUE;
}

CString CFormModule::GetRemoteLocation(int nGroupNo, CString strTestSerial, CString strIp)
{
	CString strTemp;

//	strTestSerial = "200702021100";
	if(strIp.IsEmpty() || strTestSerial.IsEmpty())	
	{
		strTemp.Format("Parameter error!!!(%s, %s)", strIp, strTestSerial);
		AfxMessageBox(strTemp);
		return "";
	}
	
	CString strRemoteLocation;
	CFtpDownLoad *pDownLoad = new CFtpDownLoad;
	BOOL bConnect = pDownLoad->ConnectFtpSession(strIp);
	if(bConnect == FALSE)
	{
		strTemp.Format(TEXT_LANG[1], strIp);//"원격 위치 %s에 접속할 수 없습니다."
		AfxMessageBox(strTemp);
		delete pDownLoad;
		return "";
	}

	CString strPathName = AfxGetApp()->GetProfileString(FORM_FTP_SECTION, "Step Data Location", "formation_data/resultData");

	char szBuff[_MAX_PATH];
	::GetTempPath(_MAX_PATH, szBuff);		//Get Windows temp. folder
	CString strRemoteFileName, strLocalFileName;

	//1. 목록 Index 파일을 Download한 후 주어진 test serialno의 폴더를 찾는다.
	strLocalFileName.Format("%s\\downtemp.csv", szBuff);
	strRemoteFileName.Format("%s/group%d/savingFileIndex_start.csv", strPathName, nGroupNo);

 	if(pDownLoad->DownLoadFile(strLocalFileName, strRemoteFileName) < 1)
	{
		delete pDownLoad;
		strTemp.Format(TEXT_LANG[2], strIp);//"원격 위치 %s에서 Index 파일을 찾을 수 없습니다."
		AfxMessageBox(strTemp);
		return "";
	}
	delete pDownLoad;
	pDownLoad = NULL;

	//  [5/25/2009 kky ]
	// for kky 데이터복구 실행시 해당데이터를 찾지 못하는 에러 발생
	/*
	// 저장 Format : 
	// mointorIndex 1, open_year 6, open_month 2, open_day 2, test_serial,
	FILE *fp;
	fp = fopen(strLocalFileName, "rb");
	if(fp == NULL)
	{
	strTemp.Format("Local에서 Index 파일을 열수 없습니다.");
	AfxMessageBox(strTemp);
	return "";
	}

	while(fscanf(fp,	"%s %d, %s %d, %s %d, %s %d, %s %s", 
		szBuff, &nIndex, szBuff, &nYear, szBuff, &nMonth, szBuff, &nDay, szBuff, szBuff) > 1)
	{
		if(strTestSerial == CString(szBuff))
		{
			bFind = TRUE;
			break;
		}
	}
	fclose(fp);
	_unlink(strLocalFileName);
	//주어진 Serial의 Test가 목록에 존재 하지 않을 경우 	
	if(bFind == FALSE)
	{
		strTemp.Format("Index 파일에서 작업 번호 %s 정보를 찾을 수 없습니다.", strTestSerial);
		AfxMessageBox(strTemp);
		return "";
	}*/
	//Download list file open
	CStdioFile sourceFile;			
	CFileException ex;	
	CString saveFilePath;
	CString strReadData;
	CString strData;
	CString strIndex;
	BOOL bFind = FALSE;
	int nIndex = 0;
	int nSize = -1;

	if( !sourceFile.Open( strLocalFileName, CFile::modeRead, &ex ))
	{
		CString strMsg;
		ex.GetErrorMessage((LPSTR)(LPCSTR)strMsg,1024);
		AfxMessageBox(strMsg);	
		return "";
	}

	while(TRUE)	
	{			
		sourceFile.ReadString(strReadData);    // 한 문자열씩 읽는다.	
		nSize = strReadData.Find(',');
		
		if( nSize > 0 )
		{
			AfxExtractSubString(strTemp, strReadData, 3,',');
			AfxExtractSubString(strData, strTemp, 2,' ');
			if( strData  == strTestSerial )
			{
				bFind = TRUE;
				break;
			}			
		}			
		else
		{
			break;				
		}
	}			  
	sourceFile.Close();
	_unlink(strLocalFileName);

	if(bFind == FALSE)
	{
		strTemp.Format(TEXT_LANG[3], strTestSerial);//"Index 파일에서 작업 번호 %s 정보를 찾을 수 없습니다."
		AfxMessageBox(strTemp);
		return "";
	}

	AfxExtractSubString(strTemp, strReadData, 0,',');
	AfxExtractSubString(strData, strTemp, 1,' ');
	strRemoteLocation.Format("%s/group%d/data%d", strPathName, nGroupNo, atoi(strData));
	// strRemoteLocation.Format("%s/group%d/data%d", strPathName, nGroupNo, nIndex);
	//2. 주어진 test serialno의 폴더의 channel 목록을 표시한다.
	return strRemoteLocation;
}

BYTE CFormModule::GetUserSelect(int nChCode, int nGradeCode)
{
	BYTE code = 0;
	//사용자 Grading 실시 
	if(::IsNonCell(nChCode))
	{
		code = EP_SELECT_CODE_NONCELL;
	}
	else if(IsCellCheckFail(nChCode))
	{
		//code = EP_SELECT_CODE_CELL_CHECK_FAIL;
		code = EP_SELECT_CODE_NONCELL;
	}
	else if(::IsFailCell(nChCode))
	{
		if(nChCode == EP_CODE_END_USER_STOP)
		{
			code = EP_SELECT_DEFAULT_NORMAL_CODE;
		}
		else if(nChCode == EP_CODE_FAIL_IMPEDANCE_HIGH || nChCode == EP_CODE_FAIL_IMPEDANCE_LOW)
		{
			code = EP_SELECT_CODE_IMPEDANCE;
		}
		else if(nChCode == EP_CODE_FAIL_CAP_HIGH || nChCode == EP_CODE_FAIL_CAP_LOW)
		{
			code = EP_SELECT_CODE_CAP_FAIL;
		}
		else if(nChCode == EP_CODE_FAIL_CRT_HIGH || nChCode == EP_CODE_FAIL_CRT_LOW)// || nChCode == EP_CODE_FAIL_I_HUNTING) =>system code 영역 
		{
			code = EP_SELECT_CODE_CRT_FAIL;
		}
		else if(nChCode == EP_CODE_FAIL_VTG_HIGH || nChCode == EP_CODE_FAIL_VTG_LOW)// || nChCode == EP_CODE_FAIL_V_HUNTING) =>  =>system code 영역 
		{
			code = EP_SELECT_CODE_VTG_FAIL;
		}
		else
		{
			code = EP_SELECT_CODE_SAFETY_FAULT;
		}
	}
	else if(::IsNormalCell(nChCode))
	{
		if(nGradeCode == 0)
		{
			//정상 Cell인데 code를 부여 받지 못한 경우 
			code = EP_SELECT_CODE_NO_GRADE;
		}
		else
		{
			code = nGradeCode;
		}
	}
	else //if()
	{
		if(nChCode ==  EP_CODE_FAIL_V_HUNTING)
		{
			code = EP_SELECT_CODE_VTG_FAIL;
		}
		else if(nChCode == EP_CODE_FAIL_I_HUNTING)
		{
			code = EP_SELECT_CODE_CRT_FAIL;
		}
		else
		{
			code = EP_SELECT_CODE_SYS_ERROR;
		}
	}
	return code;
}

BOOL CFormModule::GetChannelStepData(STR_SAVE_CH_DATA &chSaveData, int nChannelIndex/*Tray based index*/, int nTrayIndex, int nStepIndex)
{
	int nModuleChIndex = GetStartChIndex(nTrayIndex)+nChannelIndex;
	EP_CH_DATA netChData = EPGetChannelData(m_nModuleID, 0, nModuleChIndex); 

	//현재 진행하지 않은 step이면 현재값을 표시한다.
	if(nStepIndex < 0 || m_aResultData[nTrayIndex].IsLoaded() == FALSE)
	{
		return ConvertChSaveData(nTrayIndex, nChannelIndex, &netChData, &chSaveData);
	}
	else	//진행을 완료한 step인 경우는 파일에서 Loading한다.
	{	
		if( nStepIndex+1 == GetCurStepNo() )
		{
			return ConvertChSaveData(nTrayIndex, nChannelIndex, &netChData, &chSaveData);
		}
		else
		{
			STR_STEP_RESULT *pStepResult = m_aResultData[nTrayIndex].GetStepData(nStepIndex);
			if(pStepResult)
			{
				if(nChannelIndex < 0 || pStepResult->aChData.GetSize() <= nChannelIndex)
				{
					ZeroMemory(&chSaveData, sizeof(STR_SAVE_CH_DATA));
					chSaveData.wChIndex = nChannelIndex;
					chSaveData.wModuleChIndex = nModuleChIndex;
				}
				else
				{
					memcpy(&chSaveData, pStepResult->aChData[nChannelIndex], sizeof(STR_SAVE_CH_DATA));
				}
			}
			else
			{
				return ConvertChSaveData(nTrayIndex, nChannelIndex, &netChData, &chSaveData);
			}
		}
	}
	return TRUE;
}

//nTrayIndex = -1 : 현재 작업중인 Tray 중에서..
//nTrayIndex >= 0 : 지정 Tray에서 
BOOL CFormModule::IsComplitedStep(int nStepIndex, int nTrayIndex)
{
	//현재 진행하지 않은 step이면 현재값을 표시한다.
	if(nStepIndex < 0 || m_aResultData[nTrayIndex].IsLoaded() == FALSE)
	{
		return FALSE;
	}
	else	//진행을 완료한 step인 경우는 파일에서 Loading한다.
	{		
		if(m_aResultData[nTrayIndex].GetStepData(nStepIndex) == NULL)
		{
			return FALSE;
		}
	}
	return TRUE;
}

BOOL CFormModule::UpdateLoadResultData(int nTrayIndex)
{
	BOOL bRtn = TRUE;
	if(nTrayIndex < 0 || nTrayIndex >= INSTALL_TRAY_PER_MD)
	{
		for(int i=0; i<INSTALL_TRAY_PER_MD; i++)
		{
			if(m_aResultData[i].UpdateReadFile()== FALSE)
			{
				bRtn = FALSE;
			}
		}
	}
	else
	{
		bRtn = m_aResultData[nTrayIndex].UpdateReadFile();
	}
	return bRtn;
}

void CFormModule::LoadResultData(int nTrayIndex)
{
	if(nTrayIndex < 0 || nTrayIndex >= INSTALL_TRAY_PER_MD)
	{
		for(int i=0; i<INSTALL_TRAY_PER_MD; i++)
		{
			m_aResultData[i].ReadFile(GetResultFileName(i));
		}
	}
	else
	{
		m_aResultData[nTrayIndex].ReadFile(GetResultFileName(nTrayIndex));
	}
}


int CFormModule::GetTotalJig()
{
	int nModuleIndex = EPGetModuleIndex(m_nModuleID);
	EP_MD_SYSTEM_DATA *pSysData = EPGetModuleSysData(nModuleIndex);
	if(pSysData)
	{
		return pSysData->wTotalTrayNo;
	}
	return 0;
}

//각 Jig에 할당된 Ch수 
int CFormModule::GetChInJig(int nJigIndex)
{
	int nModuleIndex = EPGetModuleIndex(m_nModuleID);
	EP_MD_SYSTEM_DATA *pSysData = EPGetModuleSysData(nModuleIndex);
	if(pSysData)
	{
		if(nJigIndex >= 0 && nJigIndex < pSysData->wTotalTrayNo)
		{
			return pSysData->awChInTray[nJigIndex];
		}
	}
	return 0;	
}

int CFormModule::GetTotalInstallCh()
{
	int nModuleIndex = EPGetModuleIndex(m_nModuleID);
	EP_MD_SYSTEM_DATA *pSysData = EPGetModuleSysData(nModuleIndex);
	if(pSysData)
	{
		return pSysData->nTotalChNo;
	}
	return 0;
}


BOOL CFormModule::GetBCRState()
{
	BOOL bBCRScaned = FALSE;
	CTray *pTray;
	for(int t=0; t<GetTotalJig(); t++)
	{
		pTray = GetTrayInfo(t);
		if(pTray)
		{
			//BCR이 Scan 되었는지 여부, 1개라도 Scan 되었으면 Scan 한것으로 
			if(pTray->GetBcrRead())
			{
				bBCRScaned = TRUE;
			}
		}
	}
	//작업이 정상완료되는 시점에서 Reset됨
	return bBCRScaned;
}

BOOL CFormModule::GetTestReserved()
{
	//예약 상태 Check
	if(m_ReservedtestInfo.lNo <= 0)	
	{
		return FALSE;
	}
	return TRUE;
}

void CFormModule::SetTestReserve(STR_CONDITION_HEADER &testInfo)
{
	memcpy(&m_ReservedtestInfo, &testInfo, sizeof(STR_CONDITION_HEADER));
}

void CFormModule::ResetTestReserve()
{
	ZeroMemory(&m_ReservedtestInfo,sizeof(STR_CONDITION_HEADER));
}

//수량 입력이 되어 있고 BCR이 입력되어 있으면
BOOL CFormModule::IsAllTrayReaded()
{
	CTray *pTray;
	for(int t=0; t<GetTotalJig(); t++)
	{
		pTray = GetTrayInfo(t);
		if(pTray && pTray->GetTestReserved())	//수량이 입력되었으면 
		{
			if(pTray->GetBcrRead() == FALSE)	//BCR을 읽어야 한다.
			{
				return FALSE;
			}
		}
	}
	return TRUE;
}


BOOL CFormModule::ReadTrayTestLogFile(int nTrayIndex)
{
	CString strTempFileName, strTemp;
	strTemp = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"CTSMon");	//Get Current Folder(CTSMon Folde)
	if(strTemp.IsEmpty())
	{
		return FALSE;
	}
	
	strTempFileName.Format("%s\\Temp", strTemp);						//Get temp Folder
	strTemp.Format("%s\\md%djig%d.log", strTempFileName, m_nModuleID, nTrayIndex+1);

	FILE *fp = fopen(strTemp, "rb");
	if(fp == NULL)	return FALSE;			//File Open fail

	if(ReadEPFileHeader(TEST_LOG_FILE, fp) < 0)
	{
		strTemp.Format("%s Test Log File Header Error.", ::GetModuleName(m_nModuleID));
		fclose(fp);
		fp = NULL;
		return FALSE;
	}
	
	EP_FILE_HEADER fileHeader;	
	fseek(fp, 0, SEEK_SET);
	fread(&fileHeader, sizeof(EP_FILE_HEADER), 1, fp);	

	RESULT_FILE_HEADER	resultFileHeader;
	if(fread(&resultFileHeader, sizeof(RESULT_FILE_HEADER), 1, fp)<1)		//File Header Read
	{
		strTemp.Format("%s Test log file header read fail..", ::GetModuleName(m_nModuleID));
		fclose(fp);
		fp = NULL;
		return FALSE;		
	}

	//File Check
	if(resultFileHeader.nModuleID != m_nModuleID)
	{
		strTemp.Format("%s Test log file mismatch...", ::GetModuleName(m_nModuleID));
		fclose(fp);
		fp = NULL;
		return FALSE;
	}

	fseek(fp, sizeof(EP_MD_SYSTEM_DATA), SEEK_CUR);

	STR_FILE_EXT_SPACE *pExtData = new STR_FILE_EXT_SPACE;
	if(fread(pExtData, sizeof(STR_FILE_EXT_SPACE), 1, fp) <1)
	{
		strTemp.Format("%s test log file mismatch...", ::GetModuleName(m_nModuleID));
		fclose(fp);
		fp = NULL;
		delete pExtData;
		return FALSE;
	}

	//20090920 KBH
	//Cell Serial 를 입력한다. 
	//File Version Up RESULT_FILE_VER3
	PNE_RESULT_CELL_SERIAL cellSerial;
	ZeroMemory(&cellSerial, sizeof(PNE_RESULT_CELL_SERIAL));
	if(fread(&cellSerial, sizeof(PNE_RESULT_CELL_SERIAL), 1, fp) <1)
	{
		fclose(fp);		fp = NULL;
		return FALSE;
	}
	//20090920
	memcpy(m_TrayData[nTrayIndex].GetResultCellSerial(), &cellSerial,  sizeof(PNE_RESULT_CELL_SERIAL));
	//////////////////////////////////////////////////////////////////////////


	//Tray의 기본 정보를 Loading 한다.
	// m_TrayData[nTrayIndex].LoadTrayData(resultFileHeader.szTrayNo); kky

	//이전 Data를 복구한다.
	m_TrayData[nTrayIndex].SetTrayNo(resultFileHeader.szTrayNo);
	SetResultFileName(pExtData->szResultFileName, nTrayIndex);
	m_TrayData[nTrayIndex].ModuleID = m_nModuleID;
	m_TrayData[nTrayIndex].GroupIndex = nTrayIndex;
	m_TrayData[nTrayIndex].strJigID.Format("%d", nTrayIndex);
	m_TrayData[nTrayIndex].lCellNo = pExtData->nCellNo;
	m_TrayData[nTrayIndex].lInputCellCount = pExtData->nInputCellNo;
	m_TrayData[nTrayIndex].strLotNo = resultFileHeader.szLotNo;
	m_TrayData[nTrayIndex].strOperatorID = resultFileHeader.szOperatorID;
	m_TrayData[nTrayIndex].strTestSerialNo = resultFileHeader.szTestSerialNo;

	m_TrayData[nTrayIndex].m_nTabDeepth = fileHeader.nTabDeepth;
	m_TrayData[nTrayIndex].m_nTrayHeight = fileHeader.nTrayHeight;
	m_TrayData[nTrayIndex].m_nTrayType = fileHeader.nTrayType;
	m_TrayData[nTrayIndex].m_nContactErrlimit = fileHeader.nContactErrlimit;
	m_TrayData[nTrayIndex].m_nCapaErrlimit = fileHeader.nCapaErrlimit;
	m_TrayData[nTrayIndex].m_nChErrlimit = fileHeader.nChErrlimit;
	m_TrayData[nTrayIndex].m_nResultFileType = fileHeader.nResultFileType;
	m_TrayData[nTrayIndex].SetInputCellCnt(fileHeader.nInputCellCount);
	
	if(m_TrayData[nTrayIndex].GetTrayNo().IsEmpty() == FALSE)
	{
		TRACE("Read Tray %s ==> Cell No: %d,  Input Cell : %d\n", resultFileHeader.szTrayNo, pExtData->nCellNo, pExtData->nInputCellNo);
	}
	//////////////////////////////////////////////////////////////////////////
	delete pExtData;

	//정전시 Data 복구 필요(Tray별로 모두 같은 code 이어야 한다.)
	//Update시 모든 Tray 임시 파일를 Update시켜야 한다.
	COleDateTime cTime;
	cTime.ParseDateTime(resultFileHeader.szDateTime);
	
	UpdateStartTime(&cTime);

	fseek(fp, sizeof(SENSOR_MAPPING_SAVE_TABLE), SEEK_CUR);

	//전송된 시험 조건을 Load 한다.	//Added by k.b.h 2002/10/28
	STR_CONDITION	*pConditionMain;
	pConditionMain = new STR_CONDITION;
	ASSERT(pConditionMain);
	if(ReadTestConditionFile(fp, pConditionMain) == FALSE)					//Read Test Condition
	{
		RemoveCondition(pConditionMain);
		pConditionMain = NULL;
		fclose(fp);
		fp = NULL;
		return FALSE;
	}
	m_Condition.SetProcedure(pConditionMain);
	RemoveCondition(pConditionMain);
	pConditionMain = NULL;

	//이전 Data 복구 
	m_TrayData[nTrayIndex].lTestKey = m_Condition.GetTestInfo()->lID;
	m_TrayData[nTrayIndex].lModelKey = m_Condition.GetModelID();

	fclose(fp);
	fp = NULL;
	return TRUE;
}

//모듈에 전송된 시험 조건을 저장하는데 주목적이 있다.
BOOL CFormModule::WriteTrayTestLogFile(CTestCondition *pCondition, int nTrayIndex)
{
	int nModuleIndex = 0;
	EP_MD_SYSTEM_DATA *lpGroupSysData = EPGetModuleSysData(nModuleIndex);
	if(lpGroupSysData == NULL)	return FALSE;

	CString strTempFilePath, strTemp;
	strTemp = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"CTSMon");	//Get Current Folder(CTSMon Folder)
	if(strTemp.IsEmpty())
	{
		return FALSE;
	}

	strTempFilePath.Format("%s\\Temp", strTemp);						//Get temp Folder
	strTemp.Format("%s\\md%djig%d.log", strTempFilePath, m_nModuleID, nTrayIndex+1);

	FILE *fp = fopen(strTemp, "wb");		//Test Log File 생성 Open 
	if(fp == NULL)	return FALSE;
		
	//Write ElicoPower FileHeader
	if(WriteEPFileHeader(TEST_LOG_FILE, fp) == FALSE)
	{
		fclose(fp);		fp = NULL;
		return FALSE;
	}
	
	//Write Result File Header
	RESULT_FILE_HEADER sTestResultFileHeader;
	ZeroMemory(&sTestResultFileHeader, sizeof(RESULT_FILE_HEADER));
	sTestResultFileHeader.nModuleID = m_nModuleID;
	sTestResultFileHeader.wJigIndex = nTrayIndex;
	if(fwrite(&sTestResultFileHeader, sizeof(RESULT_FILE_HEADER), 1, fp) < 1)
	{
		fclose(fp);		fp = NULL;
		return FALSE;
	}	

	//Write Module Set Parameter
	if(fwrite(lpGroupSysData, sizeof(EP_MD_SYSTEM_DATA), 1, fp) < 1)
	{
		fclose(fp);		fp = NULL;
		return FALSE;
	}
	
	//Write Extra Data
	STR_FILE_EXT_SPACE *pExtraData = new STR_FILE_EXT_SPACE;		
//	ZeroMemory(pExtraData, sizeof(STR_FILE_EXT_SPACE));
	//모델 정보는 UpdateTestLogTempFile()에서 저장한다.
//	extraData.modelData = pCondition-> m_pModule[nModuleIndex].pGroup[nGroupIndex].condition.m_modelHeader;
	if(fwrite(pExtraData, sizeof(STR_FILE_EXT_SPACE), 1, fp) <1)
	{
		delete pExtraData;
		fclose(fp);		fp = NULL;
		return FALSE;
	}
	delete pExtraData;

	//20090920 KBH
	//Cell Serial 를 입력한다. 
	//File Version Up RESULT_FILE_VER3
	PNE_RESULT_CELL_SERIAL cellSerial;
	ZeroMemory(&cellSerial, sizeof(PNE_RESULT_CELL_SERIAL));
	memcpy(&cellSerial, m_TrayData[nTrayIndex].GetResultCellSerial(), sizeof(PNE_RESULT_CELL_SERIAL));
	if(fwrite(&cellSerial, sizeof(PNE_RESULT_CELL_SERIAL), 1, fp) <1)
	{
		fclose(fp);		fp = NULL;
		return FALSE;
	}
	//////////////////////////////////////////////////////////////////////////

	SENSOR_MAPPING_SAVE_TABLE	*pSensorMap = new SENSOR_MAPPING_SAVE_TABLE;
	memcpy(pSensorMap->mapData1, GetSensorMap(sensorType1), sizeof(_MAPPING_DATA)*EP_MAX_SENSOR_CH);
	memcpy(pSensorMap->mapData2, GetSensorMap(sensorType2), sizeof(_MAPPING_DATA)*EP_MAX_SENSOR_CH);
	pSensorMap->nModuleID = m_nModuleID;
	if(fwrite(pSensorMap, sizeof(SENSOR_MAPPING_SAVE_TABLE), 1, fp) <1)
	{
		delete pSensorMap;
		fclose(fp);		fp = NULL;
		return FALSE;
	}
	delete pSensorMap;

	//Write Tested Test Condition
	if(SaveConditionFile(fp, pCondition, FALSE) == FALSE)
	{
		fclose(fp);		fp = NULL;
		return FALSE;
	}

	//Create Success
	fclose(fp);
	fp = NULL;

	TRACE(TEXT_LANG[4], m_nModuleID, nTrayIndex+1);//"Module %d Jig %d 결과 저장 임시 파일 Create\n"

	return TRUE;
}

BOOL CFormModule::UpdateTrayTestLogHeaderFile(int nTrayIndex)
{
	int nModuleIndex = EPGetModuleIndex(m_nModuleID);
	EP_MD_SYSTEM_DATA *lpGroupSysData = EPGetModuleSysData(nModuleIndex);
	if(lpGroupSysData == NULL)		return FALSE;

	CString strTempFileName, strTemp;
	strTemp = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION , "CTSMon");	//Get Current Folder(CTSMon Folde)
	if(strTemp.IsEmpty())
	{
		return FALSE;
	}

	strTempFileName.Format("%s\\Temp", strTemp);						//Get temp Folder
	strTemp.Format("%s\\md%djig%d.log", strTempFileName, m_nModuleID, nTrayIndex+1);

	EP_FILE_HEADER	fileHeader;
	FILE *fp = fopen((LPCTSTR)strTemp, "rb+");
	if(fp != NULL)
	{
		//File Header Read
		fread(&fileHeader, sizeof(EP_FILE_HEADER), 1, fp);

		fileHeader.nResultFileType = GetTrayInfo(0)->m_nResultFileType;			
		
		fseek(fp, 0, SEEK_SET);

		fwrite(&fileHeader, sizeof(EP_FILE_HEADER), 1, fp);

		fclose(fp);	
		fp = NULL;
	}
	else
	{
		return false;
	}
	
	return true;
}

//모듈에 전송된 시험조건 + 모듈에서 시행된 최종 작업자 입력값 저장이 주목적
BOOL CFormModule::ClearTrayTestLogFile(int nTrayIndex)
{
	int nModuleIndex = EPGetModuleIndex(m_nModuleID);
	EP_MD_SYSTEM_DATA *lpGroupSysData = EPGetModuleSysData(nModuleIndex);
	if(lpGroupSysData == NULL)		return FALSE;

	CString strTempFileName, strTemp;
	strTemp = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION , "CTSMon");	//Get Current Folder(CTSMon Folde)
	if(strTemp.IsEmpty())
	{
		return FALSE;
	}

	strTempFileName.Format("%s\\Temp", strTemp);						//Get temp Folder
	strTemp.Format("%s\\md%djig%d.log", strTempFileName, m_nModuleID, nTrayIndex+1);

	EP_FILE_HEADER	fileHeader;
	FILE *fp = fopen((LPCTSTR)strTemp, "rb+");
	if(fp != NULL)
	{
		//File Header Read
		fread(&fileHeader, sizeof(EP_FILE_HEADER), 1, fp);
		
		fileHeader.nResultFileType = 0;
		fileHeader.nContactErrlimit = 0;
		fileHeader.nCapaErrlimit = 0;
		fileHeader.nTabDeepth = 0;
		fileHeader.nTrayHeight = 0;
		fileHeader.nTrayType = 0;
		fileHeader.nInputCellCount = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "CellInTray", 50);
		
		fseek(fp, 0, SEEK_SET);

		fwrite(&fileHeader, sizeof(EP_FILE_HEADER), 1, fp);
		
		RESULT_FILE_HEADER sTestResultFileHeader;
		ZeroMemory(&sTestResultFileHeader, sizeof(RESULT_FILE_HEADER));
		sTestResultFileHeader.nModuleID = m_nModuleID;
		sTestResultFileHeader.wJigIndex = nTrayIndex;
		if(fwrite(&sTestResultFileHeader, sizeof(RESULT_FILE_HEADER), 1, fp) < 1)
		{
			fclose(fp);		fp = NULL;
			return FALSE;
		}	
		
		if(fwrite(lpGroupSysData, sizeof(EP_MD_SYSTEM_DATA), 1, fp) < 1)
		{
			fclose(fp);		fp = NULL;
			return FALSE;
		}
		
		STR_FILE_EXT_SPACE *pExtraData = new STR_FILE_EXT_SPACE;
		if(fwrite(pExtraData, sizeof(STR_FILE_EXT_SPACE), 1, fp) <1)
		{
			delete pExtraData;
			fclose(fp);		fp = NULL;
			return FALSE;
		}
		delete pExtraData;
		
		PNE_RESULT_CELL_SERIAL cellSerial;
		ZeroMemory(&cellSerial, sizeof(PNE_RESULT_CELL_SERIAL));
		memcpy(&cellSerial, m_TrayData[nTrayIndex].GetResultCellSerial(), sizeof(PNE_RESULT_CELL_SERIAL));
		if(fwrite(&cellSerial, sizeof(PNE_RESULT_CELL_SERIAL), 1, fp) <1)
		{
			fclose(fp);		fp = NULL;
			return FALSE;
		}
		
		SENSOR_MAPPING_SAVE_TABLE	*pSensorMap = new SENSOR_MAPPING_SAVE_TABLE;
		memcpy(pSensorMap->mapData1, GetSensorMap(sensorType1), sizeof(_MAPPING_DATA)*EP_MAX_SENSOR_CH);
		memcpy(pSensorMap->mapData2, GetSensorMap(sensorType2), sizeof(_MAPPING_DATA)*EP_MAX_SENSOR_CH);
		pSensorMap->nModuleID = m_nModuleID;
		if(fwrite(pSensorMap, sizeof(SENSOR_MAPPING_SAVE_TABLE), 1, fp) <1)
		{
			delete pSensorMap;
			fclose(fp);		fp = NULL;
			return FALSE;
		}
		delete pSensorMap;

		//Write Tested Test Condition
		CTestCondition *pCondition = GetCondition();
		if(SaveConditionFile(fp, pCondition, FALSE) == FALSE)
		{
			fclose(fp);		fp = NULL;
			return FALSE;
		}

		fclose(fp);	
		fp = NULL;
	}
	else
	{
		return false;
	}

	return true;
}


BOOL CFormModule::UpdateTrayTestLogFile(int nTrayIndex)
{
	int nModuleIndex = EPGetModuleIndex(m_nModuleID);
	EP_MD_SYSTEM_DATA *lpGroupSysData = EPGetModuleSysData(nModuleIndex);
	if(lpGroupSysData == NULL)		return FALSE;

	CString strTempFileName, strTemp;
	strTemp = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"CTSMon");	//Get Current Folder(CTSMon Folde)
	if(strTemp.IsEmpty())
	{
		return FALSE;
	}

	strTempFileName.Format("%s\\Temp", strTemp);						//Get temp Folder
	strTemp.Format("%s\\md%djig%d.log", strTempFileName, m_nModuleID, nTrayIndex+1);

//	strcpy(m_pModule[nModuleIndex].pGroup[nGroupIndex].sTestResultFileHeader.szOperatorID, g_LoginData.szLoginID);	//Log ID Update

	FILE *fp = fopen(strTemp, "rb+");		//Test Log File 생성 Open 
	if(fp == NULL)	
	{
		return FALSE;
	}
		
	//Write ElicoPower FileHeader
	if(WriteEPFileHeader(TEST_LOG_FILE, fp) == FALSE)
	{
		fclose(fp);
		return FALSE;
	}

	RESULT_FILE_HEADER sTestResultFileHeader;
	ZeroMemory(&sTestResultFileHeader, sizeof(RESULT_FILE_HEADER));
	sTestResultFileHeader.nModuleID = m_nModuleID;	
	sTestResultFileHeader.wJigIndex = nTrayIndex;	//TrayNo로 사용

//	str.Format("%s, %s, %s, %s, %s, %s, %s, %s", GetRunStartTime().Format(), GetLotNo(), GetIPAddress(), GetOperatorID(),
//		GetTestSerialNo(), GetTrayNo(), GetTraySerialNo());
//	AfxMessageBox(str);
//	sprintf(sTestResultFileHeader.szDateTime, "%s", GetRunStartTime().Format());

	sprintf(sTestResultFileHeader.szDateTime,	"%s",	GetRunStartTime().Format("%Y-%m-%d %H:%M:%S"));
	sprintf(sTestResultFileHeader.szLotNo,		"%s",	GetLotNo(nTrayIndex));
	sprintf(sTestResultFileHeader.szTypeSel,	"%s",	GetTypeSel(nTrayIndex));
	sprintf(sTestResultFileHeader.szModuleIP,	"%s",	GetIPAddress());
	sprintf(sTestResultFileHeader.szOperatorID, "%s",	GetOperatorID(nTrayIndex));
	sprintf(sTestResultFileHeader.szTestSerialNo, "%s", GetTestSerialNo(nTrayIndex));
	sprintf(sTestResultFileHeader.szTrayNo,		"%s",	GetTrayNo(nTrayIndex));
	sprintf(sTestResultFileHeader.szTraySerialNo, "%d", GetTraySerialNo(nTrayIndex));

	//Write Result File Header
	if(fwrite(&sTestResultFileHeader, sizeof(RESULT_FILE_HEADER), 1, fp) < 1)
	{
		fclose(fp);
		return FALSE;
	}

	//Write Module Set Parameter
	if(fwrite(lpGroupSysData, sizeof(EP_MD_SYSTEM_DATA), 1, fp) < 1)
	{
		fclose(fp);
		return FALSE;
	}	

	STR_FILE_EXT_SPACE *pExtData =  new STR_FILE_EXT_SPACE;
	ASSERT(pExtData);
	ZeroMemory(pExtData, sizeof(STR_FILE_EXT_SPACE));
	pExtData->nCellNo = GetCellNo(nTrayIndex);
	pExtData->nInputCellNo = GetInputCellCount(nTrayIndex);
	pExtData->nReserved = GetCellCountInTray();
	pExtData->nSystemID = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "System ID", 1);
	pExtData->nSystemType = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "System Type", 0);
	sprintf(pExtData->szModuleName, "%s", GetModuleName());

	//현재 Tray가 공정에 사용되는지 여부를 저장(결과 data 저장시 공정에 사용되는 Tray만 data를 저장한다.)
	sprintf(pExtData->szResultFileName, "%s", GetResultFileName(nTrayIndex));
	TRACE("Save ==> Cell No: %d,  Input Cell : %d\n", pExtData->nCellNo, pExtData->nInputCellNo);
	if(fwrite(pExtData, sizeof(STR_FILE_EXT_SPACE), 1, fp) < 1)
	{
		delete pExtData;
		fclose(fp);
		return FALSE;
	}
	delete pExtData;

	//20090920 KBH
	//Cell Serial 를 입력한다. 
	//File Version Up RESULT_FILE_VER3
	PNE_RESULT_CELL_SERIAL cellSerial;
	ZeroMemory(&cellSerial, sizeof(PNE_RESULT_CELL_SERIAL));
	memcpy(&cellSerial, m_TrayData[nTrayIndex].GetResultCellSerial(), sizeof(PNE_RESULT_CELL_SERIAL));
	if(fwrite(&cellSerial, sizeof(PNE_RESULT_CELL_SERIAL), 1, fp) <1)
	{
		fclose(fp);		fp = NULL;
		return FALSE;
	}
	//////////////////////////////////////////////////////////////////////////

	SENSOR_MAPPING_SAVE_TABLE	*pSensorMap = new SENSOR_MAPPING_SAVE_TABLE;
	memcpy(pSensorMap->mapData1, GetSensorMap(sensorType1), sizeof(_MAPPING_DATA)*EP_MAX_SENSOR_CH);
	memcpy(pSensorMap->mapData2, GetSensorMap(sensorType2), sizeof(_MAPPING_DATA)*EP_MAX_SENSOR_CH);
	pSensorMap->nModuleID = m_nModuleID;
	if(fwrite(pSensorMap, sizeof(SENSOR_MAPPING_SAVE_TABLE), 1, fp) <1)
	{
		delete pSensorMap;
		fclose(fp);		fp = NULL;
		return FALSE;
	}
	delete pSensorMap;

	//Create Success
	fclose(fp);
	fp = NULL;
	TRACE(TEXT_LANG[5], m_nModuleID, nTrayIndex+1);//"Module %d Jig %d 결과 저장 임시 파일 Updated\n"
	return TRUE;
}

int CFormModule::GetStartChIndex(int nTrayIndex)
{
	int nIndex = 0;
	int nModuleIndex = EPGetModuleIndex(m_nModuleID);
	EP_MD_SYSTEM_DATA *pSysData = EPGetModuleSysData(nModuleIndex);
	if(pSysData)
	{
		for(int j = 0; j<pSysData->wTotalTrayNo && j<nTrayIndex; j++)
		{
			nIndex += pSysData->awChInTray[j];
		}
	}
	return nIndex;	
}

//Tray별 Cell 수량 
int CFormModule::GetCellCountInTray()
{
	return m_nCellCountInTray;
}

//최대값을 갖는 채널을 대표채널로 한다.
float CFormModule::GetStepTime()
{
	WORD state = GetState();
	if( state == EP_STATE_IDLE || state == EP_STATE_READY ||
		state == EP_STATE_STANDBY || state == EP_STATE_END)
	{
		return 0.0f;
	}

	int nTotch = EPGetChInGroup(m_nModuleID, 0);
	EP_CH_DATA netChData;
	ULONG ulStepTime = 0;
	for(int c =0; c<nTotch; c++)
	{
		netChData = ::EPGetChannelData(m_nModuleID, 0, c); 	
		if(netChData.ulStepTime > ulStepTime)
		{
			ulStepTime = netChData.ulStepTime;
		}
//		TRACE("Step Ch%d:%f\n", c+1, MD2PCTIME(ulStepTime));
	}
	return MD2PCTIME(ulStepTime);	//1Base Channel No
}

//최대값을 갖는 채널을 대표채널로 한다.
float CFormModule::GetTotRunTime()
{
	WORD state = GetState();
	if( state == EP_STATE_IDLE || state == EP_STATE_READY ||
		state == EP_STATE_STANDBY || state == EP_STATE_END)
	{
		return 0.0f;
	}

	int nTotch = EPGetChInGroup(m_nModuleID, 0);
	EP_CH_DATA netChData;
	ULONG ulStepTime = 0;
	for(int c =0; c<nTotch; c++)
	{
		netChData = ::EPGetChannelData(m_nModuleID, 0, c); 	
		if(netChData.ulTotalTime > ulStepTime)
		{
			ulStepTime = netChData.ulTotalTime;
		}
//		TRACE("Tot Ch%d:%f\n", c+1, MD2PCTIME(ulStepTime));
	}
	return MD2PCTIME(ulStepTime);	//1Base Channel No
}

//현재 진행중인 Step번호를 구한다.
//1번 채널을 대표채널로 한다.
//SBC에서 Channel이 Fault나도 StepNo를 증가 시킨다.(20070608)
int CFormModule::GetCurStepNo()
{
	EP_CH_DATA netChData = ::EPGetChannelData(	m_nModuleID, 0, 0); 
	return netChData.nStepNo;	//1Base Channel No
}
BOOL CFormModule::SetState(EP_GP_STATE_DATA& _state)
{
	return EPSetGroupState(m_nModuleID, _state);
}

WORD CFormModule::GetState(BOOL bDetail)
{
	WORD state = EPGetGroupState(m_nModuleID);

	if(bDetail)
	{
		if(GetLineMode() == EP_ONLINE_MODE)
		{
			//Channel data에서 동작 상태를 추출 
			int nTotch = EPGetChInGroup(m_nModuleID);
			EP_CH_DATA netChData;
			for(int c =0; c<nTotch; c++)
			{
				netChData = ::EPGetChannelData(m_nModuleID, 0, c);
				if(netChData.state == EP_STATE_CHARGE)
				{
					state = EP_STATE_CHARGE;
					break;
				}
				if(netChData.state == EP_STATE_DISCHARGE)
				{
					state = EP_STATE_DISCHARGE;
					break;
				}
				if(netChData.state == EP_STATE_REST)
				{
					state = EP_STATE_REST;
					break;
				}
				if(netChData.state == EP_STATE_OCV)
				{
					state = EP_STATE_OCV;
					break;
				}
				if(netChData.state == EP_STATE_IMPEDANCE)
				{
					state = EP_STATE_IMPEDANCE;
					break;
				}
				if(netChData.state == EP_STATE_CHECK)
				{
					state = EP_STATE_CHECK;
					break;
				}
// 				if(netChData.state == EP_STATE_END)
// 				{
// 					state = EP_STATE_END;
// 					break;
// 				}
			}
		}
		else
		{
			CTestCondition *pTestCon = GetCondition();
			CStep *pStep = pTestCon->GetStep(GetCurStepNo()-1);
			if(state == EP_STATE_RUN && pStep)
			{
				if(pStep->m_type == EP_TYPE_CHARGE)				state = EP_STATE_CHARGE;
				else if(pStep->m_type == EP_TYPE_DISCHARGE)		state = EP_STATE_DISCHARGE;
				else if(pStep->m_type == EP_TYPE_REST)			state = EP_STATE_REST;
				else if(pStep->m_type == EP_TYPE_IMPEDANCE)		state = EP_STATE_IMPEDANCE;
				else if(pStep->m_type == EP_TYPE_OCV)			state = EP_STATE_OCV;
//				else if(pStep->m_type == EP_TYPE_END)			state = EP_STATE_END;
			}
		}
	}

	return state;
}

void CFormModule::SetModuleID(int nModuleID, CString strModuleName)
{
	m_nModuleID = nModuleID;
	m_strModuleName = strModuleName;
}

int CFormModule::GetWrokingTrayCount()
{
	int nCnt = 0;
	for(int j =0; j<GetTotalJig(); j++)
	{
		if(GetTrayInfo(j)->IsWorkingTray())
		{
			nCnt++;
		}
	}
	return nCnt;
}

int CFormModule::GetFirstWorkingTrayNo()
{
	for(int j =0; j<GetTotalJig(); j++)
	{
		if(GetTrayInfo(j)->IsWorkingTray())
		{
			return j+1;
		}
	}
	return 0;
}

int CFormModule::GetLineMode()
{	
	int nOperationMode = 0;
	int nLineMode = 0;
	int nCellTypeMode = 0;	

	::EPGetLineMode(m_nModuleID, 0, nLineMode, nOperationMode, nCellTypeMode);
	return nLineMode;		
}

int CFormModule::GetOperationMode()
{
	int nOperationMode = 0;
	int nLineMode = 0;
	int nCellTypeMode = 0;	

	::EPGetLineMode(m_nModuleID, 0, nLineMode, nOperationMode, nCellTypeMode);
	return nOperationMode;
}

void CFormModule::SetTrayTypeData(CString strTrayData)
{
	//ljb 2008-11-12	strTrayData = 트레이 이름,트레이 형,트레이 Col 개수 형식을 구조체에 입력함
	CStringArray strArrTrayType;
	CString strSource,strTemp;
	int nCnt = 0, nCount=0;
	int index;
	TRACE(strTrayData);
	strTemp = strSource = strTrayData;
	while((index = strSource.Find("@")) >=0 )
	{
		strTemp = strSource.Left(index);
		if (!strTemp.IsEmpty())	strArrTrayType.Add(strTemp);
		strTemp = strSource.Mid(index+1);				
		strSource = strTemp;
		
		m_iTrayTypeCount ++;
	}
	if (!strSource.IsEmpty())
	{
		strArrTrayType.Add(strTemp);
		m_iTrayTypeCount ++;
	}

	if(	m_sTrayTypeData !=NULL)
	{
		delete [] m_sTrayTypeData;
		m_sTrayTypeData=NULL;
	}

	m_sTrayTypeData = new TRAY_TYPE[m_iTrayTypeCount];
	ASSERT(m_sTrayTypeData);

	for (nCnt=0;nCnt < strArrTrayType.GetSize();nCnt++)
	{
		strTemp = strSource = strArrTrayType.GetAt(nCnt);
		while((index = strSource.Find(",")) >=0 )
		{
			strTemp = strSource.Left(index);
			if (nCount==0) m_sTrayTypeData[nCnt].strName=strTemp;
			if (nCount==1) m_sTrayTypeData[nCnt].iTrayStyle = atoi(strTemp);
			strTemp = strSource.Mid(index+1);				
			strSource = strTemp;
			nCount ++;
		}
		//  [6/29/2009 kky ]
		// for 
		if (!strSource.IsEmpty())
			if (nCount==2) m_sTrayTypeData[nCnt].iTrayCol = atoi(strSource);
		nCount =0 ;
	}
}