#if !defined(AFX_PROGRESSPROCDLG_H__576E3218_7E8B_40E0_8887_A6CAA89F5D3C__INCLUDED_)
#define AFX_PROGRESSPROCDLG_H__576E3218_7E8B_40E0_8887_A6CAA89F5D3C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ProgressProcDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CProgressProcDlg dialog

#include "MyGridWnd.h"
#include "TestLogSet.h"
#include "ModuleRecordSet.h"
#include "TestRecordSet.h"
#include "ChValueRecordSet.h"
#include "ChResultSet.h"


class CProgressProcDlg : public CDialog
{
// Construction
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	CProgressProcDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CProgressProcDlg();

	BOOL DeleteModuleLogList(CString strProcedureID);
	BOOL UpdateLastTrayState(CString strProcedureID);
	BOOL RequeryTestList(CString strProcID);
	BOOL RequeryDataSerarch(CString strTestLogID);
	void SetDataBase(CDatabase *pDB, CString strTestLogID = "");
	CString m_strTestLogID;
	CString m_strData;
	int m_nDspType;
	BOOL RequeryCellCount(long lTestID, CString strTestSerialNo, int &nInputCount, int &nNormalCount, long nProcdureID = 0);
	void UpdateTestList();
	void RequeryTestList(int nType);
	void UpdateDataList();
	BOOL RequeryDataSerarch(int dataType);
	BOOL InitDataListGrid();
		
	

// Dialog Data
	//{{AFX_DATA(CProgressProcDlg)
	enum { IDD = IDD_PROGRESS_PROC_DLG };
	CComboBox	m_dataTypeSelect;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CProgressProcDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CArray<DATA_LIST ,DATA_LIST> m_ptArray;
	CArray<PROC_LIST, PROC_LIST> m_ptTestArray;
	CTestRecordSet      m_Test;
	CChValueRecordSet   m_ChValue;
	CDatabase *m_pDBServer;
	CCTSAnalDoc *m_pDoc;

	CMyGridWnd m_wndProcList;
	CMyGridWnd m_wndDataList;

	BOOL InitProcListGrid();
	// Generated message map functions
	//{{AFX_MSG(CProgressProcDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnClose();
	afx_msg void OnSelchangeTypeSelect();
	afx_msg void OnAllDataRadio();
	afx_msg void OnEndedDataRadio();
	afx_msg void OnWorkingDataRadio();
	afx_msg void OnDeleteProcButton();
	//}}AFX_MSG
	afx_msg LONG OnMovedCurrentCell(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROGRESSPROCDLG_H__576E3218_7E8B_40E0_8887_A6CAA89F5D3C__INCLUDED_)
