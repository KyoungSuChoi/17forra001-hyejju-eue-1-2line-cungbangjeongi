#if !defined(AFX_FAILMSGDLG_H__66BA4576_2F45_49E4_9511_A1D63A9F7E56__INCLUDED_)
#define AFX_FAILMSGDLG_H__66BA4576_2F45_49E4_9511_A1D63A9F7E56__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FailMsgDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFailMsgDlg dialog

class CFailMsgDlg : public CDialog
{
// Construction
public:
	void LoadEmgList();
	BOOL GetFailMsg(CString &strMsg, CString &strDescript);
	void SetErrorCode(BYTE code, CString strDataBase = "");
	void SetModuleName(CString strModuleName, CString ipAddress);
	CFailMsgDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CFailMsgDlg)
	enum { IDD = IDD_FAIL_MSG_DIALOG };
	CListBox	m_ctrlEmgList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFailMsgDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	stingray::foundation::SECJpeg *m_pJpeg;
	CSize m_sizeImg;
	CString m_strDataBase;
	int m_errCode;
	CString m_strModuleName;
	CString m_strIpAddress;

	// Generated message map functions
	//{{AFX_MSG(CFailMsgDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonAll();
	afx_msg void OnButtonLog();
	afx_msg void OnButton2();
	afx_msg void OnPaint();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FAILMSGDLG_H__66BA4576_2F45_49E4_9511_A1D63A9F7E56__INCLUDED_)
