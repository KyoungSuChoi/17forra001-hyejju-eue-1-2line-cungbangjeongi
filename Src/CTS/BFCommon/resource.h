//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by BFCommon.rc
//
#define IDS_TEXT_NONE                   1
#define IDS_TEXT_OPEN                   2
#define IDS_TEXT_CLOSE                  3
#define IDS_TEXT_RUN                    4
#define IDS_TEXT_ERROR                  5
#define IDS_TEXT_UNLOAD                 6
#define IDS_TEXT_LOAD                   7
#define IDS_TEXT_MOVING                 8
#define IDS_TEXT_DOWN                   9
#define IDS_TEXT_UP                     10
#define IDS_TEXT_GOING_DOWN             11
#define IDS_TEXT_GOING_UP               12
#define IDS_TEXT_CHARGE                 13
#define IDS_TEXT_DISCHARGE              14
#define IDS_TEXT_REST                   15
#define IDS_TEXT_OCV                    16
#define IDS_TEXT_IMPEDANCE              17
#define IDS_TEXT_END                    18
#define IDS_TEXT_LOOP                   19

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        1001
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           1000
#endif
#endif
