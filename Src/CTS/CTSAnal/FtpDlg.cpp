// FtpDlg1.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "FtpDlg.h"
#include "IpSetDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFtpDlg dialog

bool CFtpDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CFtpDlg"), _T("TEXT_CFtpDlg_CNT"), _T("TEXT_CFtpDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CFtpDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CFtpDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}


CFtpDlg::CFtpDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFtpDlg::IDD, pParent)
{
	LanguageinitMonConfig();

	//{{AFX_DATA_INIT(CFtpDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_nModuleNo = -1;
	m_nChNo  = -1;
	m_nModuleNum = MODULENUM;
	connect = NULL;
	m_imagelist = NULL;
	m_imageTree = NULL;

	m_bUseGroupSet = FALSE;
	m_bUseRackIndex = FALSE;
	m_nModulePerRack = 3;
	m_strGroupName = "Group";
	m_strModuleName = "Module";
	m_nInstallModule = 1;
}


void CFtpDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFtpDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	DDX_Control(pDX, IDC_COMBO, m_pDriveCombo);
	DDX_Control(pDX, IDC_LIST, m_List);
	DDX_Control(pDX, IDC_TREE1, m_treeCtrl);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFtpDlg, CDialog)
	//{{AFX_MSG_MAP(CFtpDlg)
	ON_NOTIFY(TVN_SELCHANGED, IDC_TREE1, OnSelchangedTree1)
	ON_BN_CLICKED(IDC_DOWN, OnDown)
	ON_BN_CLICKED(IDC_TOT_DOWN, OnTotDown)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST, OnItemchangedList)
	ON_CBN_SELCHANGE(IDC_COMBO, OnSelchangeCombo)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST, OnDblclkList)
	ON_WM_CONTEXTMENU()
	ON_BN_CLICKED(IDC_IPADDRESS_SET, OnIpaddressSet)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_STEP_DATA_DOWN, OnStepDataDown)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFtpDlg message handlers
void CFtpDlg::InitDlg()
{
	m_strModuleName = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Module Name", "Rack");	//Get Current Folder(PowerFormation Folde)
	m_strGroupName = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Group Name", "Group");	//Get Current Folder(PowerFormation Folde)
	m_nModulePerRack = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION , "Module Per Rack", 3);
	m_bUseRackIndex = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION , "Use Rack Index", TRUE);
	m_bUseGroupSet = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION , "Use Group", FALSE);
	
	m_nInstallModule = GetModuleTotModule();

	SetIcon(m_hIcon, TRUE);
	SetIcon(m_hIcon, FALSE);

	m_imageTree = new CImageList;
	m_imageTree->Create(IDB_STATE_ICON1, 20,1,RGB(255, 255, 255));
	m_treeCtrl.SetImageList(m_imageTree,TVSIL_NORMAL);

	TV_INSERTSTRUCT  tvstruct;
	HTREEITEM        item, item2;
	CString	     	 strTreeTitle;

	for (int i = 1; i <= m_nInstallModule; i++)
	{
//		strTreeTitle.Format("Rack %d-%d", (i-1)/3+1, (i-1)%3+1);
		strTreeTitle = ModuleName(i);
		tvstruct.hParent = NULL;
	   	tvstruct.hInsertAfter = TVI_LAST;
    	tvstruct.item.iImage = 21;
     	tvstruct.item.iSelectedImage = 10;
    	tvstruct.item.pszText = (char *)(LPCTSTR)strTreeTitle;		//Display Module ID String
	   	tvstruct.item.cchTextMax = 64;
    	tvstruct.item.mask = TVIF_IMAGE | TVIF_TEXT | TVIF_SELECTEDIMAGE;
     	item = m_treeCtrl.InsertItem(&tvstruct);
		m_treeCtrl.SetItemData(item, i);

		for(int j = 1; j <= CHANNELNUM; j++)
		{		
			strTreeTitle.Format("Ch %d",j);
			tvstruct.hParent = item;
	    	tvstruct.hInsertAfter = TVI_LAST;
    		tvstruct.item.iImage = 20;
    		tvstruct.item.iSelectedImage = 9;
    		tvstruct.item.pszText = (char *)(LPCTSTR)strTreeTitle;		//Display Module ID String
	    	tvstruct.item.cchTextMax = 64;
    		tvstruct.item.mask = TVIF_IMAGE | TVIF_TEXT | TVIF_SELECTEDIMAGE;
			item2 = m_treeCtrl.InsertItem(&tvstruct);
			m_treeCtrl.SetItemData(item2, j);
		}
	}

	m_CurDir = _T("\\");
	m_beforeDir = _T("\\");
	m_CurDrive = "C:\\";
	
	InitList();
	ViewListCtrl(m_CurDir);			// 리스트 컨트롤에 파일을 나타내 줌
	DriveInitialize();
//	m_pDriveCombo.SetCurSel(0);
	m_pDriveCombo.SelectString(0, m_CurDir);
}

void CFtpDlg::InitImgList()
{
	m_imagelist = new CImageList;
	ASSERT(m_imagelist);
	m_imagelist->Create(IDB_STATE_ICON1, 18, 1, RGB(255,255,255));
}

void CFtpDlg::InitList()
{
	m_imagelist = new CImageList;
    m_imagelist->Create(16,16,TRUE, 3,3);

    HICON hIcon;
    hIcon = AfxGetApp()->LoadIcon(IDI_ICON1);
	m_imagelist->Add(hIcon);

    hIcon = AfxGetApp()->LoadIcon(IDI_ICON2);
    m_imagelist->Add(hIcon);

    hIcon = AfxGetApp()->LoadIcon(IDI_ICON3);
    m_imagelist->Add(hIcon);

	m_List.SetImageList(m_imagelist, LVSIL_SMALL);

  // 리스트 박스에  Column을 추가 (파일명, 크기)
	   LV_COLUMN lvc;

	   lvc.mask = LVCF_FMT|LVCF_WIDTH|LVCF_TEXT|LVCF_SUBITEM;
	   lvc.fmt = LVCFMT_RIGHT;

	   lvc.pszText = (LPTSTR)_T("파일명");//omit
	   lvc.cx = m_List.GetStringWidth(lvc.pszText)+120;
	   lvc.iSubItem = 0;
	   m_List.InsertColumn(0,&lvc);  

	   lvc.pszText = (LPTSTR)_T("크기(Byte)");//omit
	   lvc.cx = m_List.GetStringWidth(lvc.pszText)+20;
	   lvc.iSubItem = 1;

	   m_List.InsertColumn(1, &lvc);  
}

void CFtpDlg::DriveInitialize()
{
	 int nPos = 0;
	 int nDrivesAdded = 0;
	 CString strDrive = "?:\\";
	 DWORD dwDriveList = ::GetLogicalDrives();

	 while(dwDriveList)
	 {
		 if(dwDriveList & 1) 
		 {
			 strDrive.SetAt(0,0x41 + nPos);
			 if(AddDriveNode(strDrive))
			   nDrivesAdded++;
		 }
		 dwDriveList>>=1;
		 nPos++;
	 }
}

BOOL CFtpDlg::AddDriveNode(CString& strDrive)
{
	UINT nType = ::GetDriveType((LPCTSTR)strDrive);
	switch(nType)
	{
	case DRIVE_REMOVABLE :  //A
		 m_pDriveCombo.AddString((LPCTSTR)strDrive);
		 break;
	case DRIVE_FIXED :   //c
		 m_pDriveCombo.AddString((LPCTSTR)strDrive);
		break;
	case DRIVE_REMOTE :
		 m_pDriveCombo.AddString((LPCTSTR)strDrive);
		break;
	case DRIVE_CDROM :	//d	e	  
	 	 m_pDriveCombo.AddString((LPCTSTR)strDrive);
		break;
	case DRIVE_RAMDISK :
		 m_pDriveCombo.AddString((LPCTSTR)strDrive);
		break;
	default :
		return FALSE;
	}
	return TRUE;
}

CFtpDlg::~CFtpDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

void CFtpDlg::OnSelchangedTree1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	hCurItem = pNMTreeView->itemNew.hItem;
	CString  module;
//	int      address = 130;
	
	HTREEITEM hItem = m_treeCtrl.GetSelectedItem();
	HTREEITEM hParentItem;
	if (hItem != NULL)
	{
		if((hParentItem = m_treeCtrl.GetParentItem(hItem)) == NULL)
		{
			m_nModuleNo = m_treeCtrl.GetItemData(hItem);
//			module.Format("Rack %d-%d", (m_nModuleNo-1)/3+1, (m_nModuleNo-1)%3+1);
			GetDlgItem(IDC_MODULE)->SetWindowText(ModuleName(m_nModuleNo));
		}
		else
		{
			m_nModuleNo = m_treeCtrl.GetItemData(hParentItem);
			m_nChNo =  m_treeCtrl.GetItemData(hItem);
//			module.Format("Rack %d-%d Ch %d", (m_nModuleNo-1)/3+1, (m_nModuleNo-1)%3+1, m_nChNo);
			GetDlgItem(IDC_MODULE)->SetWindowText(ModuleName(m_nModuleNo));
		}
	}

//	address = address + m_nModuleNo;
	m_IpAddress = GetIPAddress(m_nModuleNo);

//	m_IpAddress.Format("192.168.1.%d", address);
//	m_IpAddress = "61.77.154.225";
//	m_IpAddress = "210.183.201.71";

	*pResult = 0;
}

void CFtpDlg::OnDown() 
{
	DownFile(0);
}

void CFtpDlg::OnTotDown() 
{
	// TODO: Add your control notification handler code here
	CString strID = AfxGetApp()->GetProfileString(FORM_FTP_SECTION, "Login ID", "root");
	CString strPwd = AfxGetApp()->GetProfileString(FORM_FTP_SECTION, "Login Password", "dusrn1");
	CString path = AfxGetApp()->GetProfileString(FORM_FTP_SECTION, "Monitoring Location", "/Project/SKC/current/form/App/DataBase/monitoringData");

	CString   downData, downPath;
//	int       address = 130;
	
	//Database에서 Module ID를 가져 오도록 수정 하여야 한다.

	for(int i = 1; i <= m_nInstallModule; i++)
	{
//		m_IpAddress.Format("192.168.1.%d",address + i);
		m_IpAddress = GetIPAddress(i);
		
		if(m_IpAddress.IsEmpty())
		{
			downData.Format(TEXT_LANG[2], ModuleName(i));//"%s 의 IP Address를 찾을 수 없습니다."
			AfxMessageBox(downData, MB_ICONSTOP|MB_OK);
			break;
		}

		if(connect != NULL)
		{
			CloseSession();
			connect = NULL;
		}

    	try
		{
    		CloseSession();
			SetCursor(LoadCursor(NULL,IDC_WAIT));
			connect = session.GetFtpConnection(m_IpAddress, strID, strPwd);
			if(connect != NULL)
				connect->SetCurrentDirectory(path);
		}
    	catch(CInternetException *pEx)
		{
      		AfxMessageBox(TEXT_LANG[3]);//"연결이 되지 않습니다."
    		pEx->Delete();
    		return;
		}
    	for(int j = 1; j <= 256; j++)
		{
    		downData.Format("m%dc%d.csv",i,j);
    		downPath = m_CurDir + downData;
//    		downData = path + downData ;
      		connect->GetFile(downData,downPath);
		}
		AfxMessageBox(TEXT_LANG[4],MB_OK | MB_ICONINFORMATION);//"다운로드를 완료하였습니다."
	}
}

void CFtpDlg::ViewListCtrl(CString &Dir)
{
   BOOL find_ok;
   CFileFind filefind;
   CString ClientFile , str = "\\";
   CString RootName;    // 현재 Root드라이버
   int nActualIndex;    // 찾은 아이템의 갯수
   char cDir[500];
     
   m_List.DeleteAllItems(); 
   SetCurrentDirectory(Dir); // 현재 디렉토리 정보를 얻음 

   m_beforeDir = m_CurDir = Dir;

   LV_ITEM lvItem;

   find_ok = filefind.FindFile();

   if(!find_ok)
   {
     filefind.Close();
	 AfxMessageBox(TEXT_LANG[5], MB_ICONQUESTION);//"장치가 준비 되지 않았습니다."
	 return;
   }

   while(find_ok)
   {
     lvItem.mask = LVIF_TEXT|LVIF_IMAGE;
     lvItem.state = lvItem.stateMask = 0;
     lvItem.iImage = lvItem.lParam = 0;
     lvItem.iSubItem = 0;
   
     find_ok = filefind.FindNextFile();
	 ClientFile = filefind.GetFileName();
	 RootName = filefind.GetRoot();
   	 
	 lvItem.iItem = m_List.GetItemCount() + 1;
     lvItem.pszText =(LPTSTR)(LPCTSTR)ClientFile;

	 if(filefind.IsDirectory())                      // 찾은 내용이 디렉토리일 경우
	 {
   
		if(ClientFile !="." && ClientFile !="..")
		{
			lvItem.iImage = 0; 
			nActualIndex = m_List.InsertItem(&lvItem);
		}
		else if(ClientFile !=".")
		{  
			lvItem.iImage = 2;
		    nActualIndex = m_List.InsertItem(&lvItem);
		}
	 }
   }
    filefind.Close();
    GetCurrentDirectory(500, cDir);
	RootName = RootName + str;
	if(strcmp(cDir, RootName))
     	m_CurDir = cDir + str;
	else
		m_CurDir = cDir;

	CStatic* pStatic = (CStatic*)GetDlgItem(IDC_CLIENT);
    pStatic->SetWindowText(m_CurDir); // 현재 디렉토리 이름을 static 
//	FileViewListCtrl(m_CurDir);
}

void CFtpDlg::FileViewListCtrl(CString &Dir)
{
	BOOL find_ok;
	CFileFind filefind;
	CString ClientFile;
	DWORD dSize;
	char cSize[100];

	int nActualIndex;    // 찾은 아이템의 갯수
    
	LV_ITEM lvItem;
	   lvItem.mask = LVIF_TEXT|LVIF_IMAGE;
	   lvItem.state = lvItem.stateMask = 0;
	   lvItem.iImage = lvItem.lParam = 0;
  
	find_ok = filefind.FindFile();

	if(!find_ok)
	{
		 filefind.Close();
	}

	while(find_ok)
	{
      find_ok = filefind.FindNextFile();
	  ClientFile = filefind.GetFileName();
      dSize =  filefind.GetLength();
      wsprintf(cSize, "%i", dSize);
	  
	 
	 lvItem.iItem = m_List.GetItemCount() + 1;
     lvItem.pszText =(LPTSTR)(LPCTSTR)ClientFile;


      if(!filefind.IsDirectory())
	  {
		   lvItem.mask = LVIF_TEXT|LVIF_IMAGE;
		   lvItem.iImage = 1;
		   lvItem.iSubItem = 0;
		   nActualIndex = m_List.InsertItem(&lvItem);

		   lvItem.mask = LVIF_TEXT;
		   lvItem.iItem = nActualIndex;  
		   lvItem.iSubItem = 1;
		   lvItem.pszText = cSize;  //_tcscpy
		   m_List.SetItem(&lvItem);
     } 
   }
   filefind.Close();
}

void CFtpDlg::OnItemchangedList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here

	CString   str = "\\";
	int  nSel = pNMListView->iItem;
	if(nSel != -1)
	{
		CString strText = m_List.GetItemText(nSel, 0);
		m_CurDir = m_CurDrive + strText + str;
		m_beforeDir = m_CurDrive + strText;
   		SetDlgItemText(IDC_CLIENT, m_CurDir);
	}
	*pResult = 0;
}

void CFtpDlg::OnSelchangeCombo() 
{
	// TODO: Add your control notification handler code here
	UINT i;
	CString  szDir;

	i = m_pDriveCombo.GetCurSel();
	m_pDriveCombo.GetLBText(i, szDir);
	m_CurDrive = szDir;
	ViewListCtrl(szDir);
}

void CFtpDlg::OnDblclkList(NMHDR* /*pNMHDR*/, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	LV_ITEM  lvitem;
	// 선택된 아이템의 index값을 얻어옴 
	int nIndex = m_List.GetNextItem(-1, LVNI_SELECTED);

	CString sSelectedItem = m_List.GetItemText(nIndex, 0);
	CString str;
	str = "\\";

//	lvitem.mask = LVIF_TEXT;//|LVIF_IMAGE;
	lvitem.iItem = nIndex;
	lvitem.state = LVIS_SELECTED;
	lvitem.pszText =(LPTSTR)(LPCTSTR)sSelectedItem;
	lvitem.iImage = 0 ;
	lvitem.iSubItem = 1;

	if(m_List.GetItem(&lvitem))
	{
		if((lvitem.iImage == 0 || lvitem.iImage == 2) && sSelectedItem.GetLength() != 0)
		{
			ViewListCtrl(m_beforeDir);
			m_CurDir.ReleaseBuffer();
		}
	}
	m_CurDrive = m_CurDir;
	*pResult = 0;
}

void CFtpDlg::FtpFileFind()
{
	CFtpFileFind* m_pFileFind = new CFtpFileFind(connect);
	CString       strFile;

	BOOL  bFind = m_pFileFind->FindFile("*.csv");
	while(bFind)
	{
		bFind = m_pFileFind->FindNextFile();
		strFile = m_pFileFind->GetFileName();
	}
	m_pFileFind->Close();
	delete m_pFileFind;
}

void CFtpDlg::OnContextMenu(CWnd* pWnd, CPoint /*point*/) 
{
	// TODO: Add your message handler code here
	CRect  rect;
//	CTreeCtrl pFrame = (CTreeCtrl*)GetDlgItem(IDC_TREE1);
//	pFrame->GetWindowRect(rect);

//	if(rect.PtInRect(point))
//	{
//	}
}

void CFtpDlg::OnIpaddressSet() 
{
	// TODO: Add your control notification handler code here
/*	CIpSetDlg   *dlg;
	dlg = new CIpSetDlg;
	dlg->Create(IDD_IPSET_DLG, this);
   	dlg->ShowWindow(SW_SHOW);
	dlg->m_ModuleNum = MODULENUM;
*/
}

BOOL CFtpDlg::DestroyWindow() 
{
	// TODO: Add your specialized code here and/or call the base class
//	CFtpConnection::Close();
	CloseSession();
	delete m_imageTree;
	if(m_imagelist)
	{
		delete m_imagelist;
		m_imagelist = NULL;
	}
	return CDialog::DestroyWindow();
}

BOOL CFtpDlg::CloseSession()
{
	if(connect != NULL)
	{
		connect->Close();
		delete connect;
		connect = NULL;
	}
	return TRUE;
}

void CFtpDlg::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	CloseSession();
	delete m_imageTree;
	if(m_imagelist)
	{
		delete m_imagelist;
		m_imagelist = NULL;
	}
	session.Close();
	
	CDialog::OnClose();
}

void CFtpDlg::OnStepDataDown() 
{
	// TODO: Add your control notification handler code here
	DownFile(1);
}

BOOL CFtpDlg::DownFile(int nData)
{
	// TODO: Add your control notification handler code here
	CString strID = AfxGetApp()->GetProfileString(FORM_FTP_SECTION, "Login ID", "root");
	CString strPwd = AfxGetApp()->GetProfileString(FORM_FTP_SECTION, "Login Password", "dusrn1");
	CString path;
	

	if(nData == 0)		//Monitoring Data Down
	{
//		path = "/project/SKC_DEMO/current/form/App/DataBase/monitoringData/";
		path = AfxGetApp()->GetProfileString(FORM_FTP_SECTION, "Monitoring Location", "/Project/SKC/current/form/App/DataBase/monitoringData/");
	}
	else
	{
//		path = "/project/SKC_DEMO/current/form/App/DataBase/stepData/";
		path = AfxGetApp()->GetProfileString(FORM_FTP_SECTION, "Step Data Location", "/Project/SKC/current/form/App/DataBase/monitoringData/");
	}
	CString   downData, downPath;

	if(m_nChNo != -1)
	{
    	downData.Format("m%dc%d.csv",m_nModuleNo,m_nChNo);
    	downPath = m_CurDir + downData;
 //   	downData = path + downData ;
	
		try
		{
    		CloseSession();
			SetCursor(LoadCursor(NULL,IDC_WAIT));
        	connect = session.GetFtpConnection(m_IpAddress, strID, strPwd);
			if(connect != NULL)
				connect->SetCurrentDirectory(path);
		}
    	catch(CInternetException *pEx)
		{
    		AfxMessageBox(TEXT_LANG[3]);//"연결이 되지 않습니다."
			pEx->Delete();
			return FALSE;
		}
		try
		{
			if(connect->GetFile(downData,downPath) == FALSE)
			{
				LPVOID lpMsgBuf;
				FormatMessage( 
					FORMAT_MESSAGE_ALLOCATE_BUFFER | 
					FORMAT_MESSAGE_FROM_SYSTEM | 
					FORMAT_MESSAGE_IGNORE_INSERTS,
					NULL,
					GetLastError(),
					MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
					(LPTSTR) &lpMsgBuf,
					0,
					NULL 
				);
				// Process any inserts in lpMsgBuf.
				// ...
				// Display the string.
//				MessageBox((LPCTSTR)lpMsgBuf, "Error", MB_OK | MB_ICONWARNING );
				MessageBox(TEXT_LANG[6],TEXT_LANG[7] ,MB_OK | MB_ICONERROR);//"파일 다운로드 실패!\n원본 파일 확인 바람","Error"
				// Free the buffer.
				LocalFree( lpMsgBuf );

			}
			else
			{
				AfxMessageBox(TEXT_LANG[4],MB_OK | MB_ICONINFORMATION);//"다운로드를 완료하였습니다."
			}
		}
    	catch(CInternetException *pEx)
		{
			char buff[128];
			pEx->GetErrorMessage(buff, 127);
			AfxMessageBox(buff);
			pEx->Delete();
		}
	}
	else
	{
//			CString strTemp;
//			strTemp.Format("GetFile From %s->%s %s", m_IpAddress, strID, strPwd);
//			AfxMessageBox(strTemp);
		try
		{
			CloseSession();
			SetCursor(LoadCursor(NULL,IDC_WAIT));
        	connect = session.GetFtpConnection(m_IpAddress, strID, strPwd);
			if(connect != NULL)
				connect->SetCurrentDirectory(path);
		}
		catch(CInternetException *pEx)
		{
			char buff[128];
			pEx->GetErrorMessage(buff, 127);
			AfxMessageBox(buff);
			pEx->Delete();
			return FALSE;
		} 
		for(int i = 1; i <= 256; i++)
		{
			if(nData == 0)		//Monitoring Data Down
			{
				downData.Format("m%dc%d.csv", m_nModuleNo, i);
				downPath = m_CurDir + downData;
//				downData = path + downData ;
       			if(connect->GetFile(downData, downPath)== FALSE)
				{
				}
			}
			else
			{
				for(int k = 0; k<EP_MAX_STEP; k++)
				{
					downData.Format("m%dc%ds%d.csv", m_nModuleNo, i, k+1);
					downPath = m_CurDir + downData;
//					downData = path + downData ;
       				if(connect->GetFile(downData, downPath)== FALSE)
					{
						break;
					}
				}
			}
		}
		AfxMessageBox(TEXT_LANG[4],MB_OK | MB_ICONINFORMATION);//"다운로드를 완료하였습니다."
	}
	return TRUE;
}

CString CFtpDlg::GetIPAddress(int nModuleID)
{
	CString address;
	CDaoDatabase  db;
	
	CString strDataBaseName = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION, "DataBase");
	if(strDataBaseName.IsEmpty())	return  address;

	strDataBaseName = strDataBaseName+ "\\"+FORM_SET_DATABASE_NAME;
		
	db.Open(strDataBaseName);

	CString strSQL;
	strSQL.Format("SELECT IP_Address FROM SystemConfig WHERE ModuleID = %d", nModuleID);
	
	CDaoRecordset rs(&db);
	rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
	if(!rs.IsBOF())
	{
		COleVariant data = rs.GetFieldValue(0);
		address = data.pbVal;
	}

	rs.Close();
	db.Close();

	//수동 입력 
	if(address.IsEmpty())
	{
		CIPSetDlg dlg;
		if(dlg.DoModal() == IDOK)
			address = dlg.GetIPAddress();
	}

	return address;
}


inline CString CFtpDlg::ModuleName(int nModuleID, int nGroupIndex)
{
	CString strName;
	
	if(m_bUseRackIndex)
	{
		div_t div_result;
		div_result = div( nModuleID-1, m_nModulePerRack );

		if(m_bUseGroupSet && nGroupIndex >= 0)
		{
			strName.Format("%s %d-%d, %s %d", m_strModuleName, div_result.quot+1, div_result.rem+1, m_strGroupName, nGroupIndex+1);
		}
		else
		{
			strName.Format("%s %d-%d", m_strModuleName, div_result.quot+1, div_result.rem+1);
		}
	}
	else
	{
		if(m_bUseGroupSet && nGroupIndex >= 0)
		{
			strName.Format("%s %d, %s %d", m_strModuleName, nModuleID,  m_strGroupName, nGroupIndex+1);
		}
		else
		{
			strName.Format("%s %d", m_strModuleName, nModuleID);
		}
	}
	return strName;
}

int CFtpDlg::GetModuleTotModule()
{
	int count =1;
	CDaoDatabase  db;
	
	CString strDataBaseName = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION, "DataBase");

	strDataBaseName = strDataBaseName+ "\\"+FORM_SET_DATABASE_NAME;
	
	db.Open(strDataBaseName);

	CString strSQL;
	strSQL.Format("SELECT count(*) FROM SystemConfig");
	
	CDaoRecordset rs(&db);
	rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
	if(!rs.IsBOF())
	{
		COleVariant data = rs.GetFieldValue(0);
		count = data.intVal;
	}

	rs.Close();
	db.Close();

	return count;
}
