#pragma once

// CPrecisionDlg 대화 상자입니다.

#include "stdafx.h"
#include "CTSMon.h"
#include "MyGridWnd.h"
#include "afxwin.h"
#include "CTSMonDoc.h"
#include "afxdtctl.h"

typedef struct _st_PRECISION_RESULT_INFO
{
	int nCh;
	unsigned long nFaultCnt;
	unsigned long nFaultLevelCnt;
}st_PRECISION_RESULT_INFO;

class CPrecisionDlg : public CDialog
{
	DECLARE_DYNAMIC(CPrecisionDlg)
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

public:
	CPrecisionDlg(CCTSMonDoc *pDoc, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CPrecisionDlg();
	
	enum { SELECT_CC=0, SELECT_CV };
	enum { LEVEL1=0, LEVEL2, LEVEL3 };
	enum { TYPE_COUNT=0, TYPE_RATIO };
	

	// 대화 상자 데이터입니다.
	enum { IDD = IDD_PRECISION_DLG };
	
public:
	CMyGridWnd		m_ListChInfoGrid;	
	CCTSMonDoc		*m_pDoc;	
	CFont m_Font;
	
	int m_nSelectMode;		// CC/CV 
	int	m_nSelectLevel;		// Level1, 2, 3
	int m_bSelectData;		// date 범위
	
public:
	void IntGridWnd();
	void Fun_ClearGrid();
	void InitLabel();
	void InitFont();
	void InitColorBtn();
	int Search(int nModuleID, CString strQuery);
		
	bool Fun_LoadStageList();
	void Fun_UpdateBtnState();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedAll();
	afx_msg void OnBnClickedStaticMonth();	
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedClose();
	CLabel	m_LabelPrecisionName;
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedLevel1();
	CColorButton2 m_BtnCurrentCC;
	CColorButton2 m_BtnVoltageCV;
	CColorButton2 m_BtnLevel1;
	CColorButton2 m_BtnLevel2;
	CColorButton2 m_BtnLevel3;
	CColorButton2 m_BtnPeriodToday;
	CColorButton2 m_BtnPeriodWeek;
	CColorButton2 m_BtnPeriodMonth;
	CColorButton2 m_BtnPeriodAll;
	afx_msg void OnBnClickedCurrentCc();
	CComboBox m_ctrlStage;
	CColorButton2 m_BtnCSVConvert;
	CColorButton2 m_BtnAllChClear;
	afx_msg void OnBnClickedLevel3();
	afx_msg void OnBnClickedVoltageCv();
	int m_nResultType;
	afx_msg void OnBnClickedRadio();
	afx_msg void OnBnClickedRadio5();
	afx_msg void OnBnClickedLevel2();
	afx_msg void OnBnClickedPeriodWeek();
	afx_msg void OnBnClickedPeriodMonth();
	CDateTimeCtrl m_DateTimeEnd;
	CDateTimeCtrl m_DateTimeStart;
	CColorButton2 m_Btn_Search;
	afx_msg void OnClickedBtnSearch();
	afx_msg void OnBnClickedPeriodAll();
	afx_msg void OnBnClickedPeriodToday();
	afx_msg void OnBnClickedCsvOutput();
	afx_msg void OnBnClickedAllChClear();
};
