// SelectSimulDataDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ctseditor.h"
#include "SelectSimulDataDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#define COLUMN_DATA 1
#define MAX_PATTERN_ROW_COUNT 20000
#define WM_SET_RANGE		WM_USER + 100
#define WM_STEP_IT			WM_USER + 101
#define WM_PROGRESS_SHOW	WM_USER + 102
/////////////////////////////////////////////////////////////////////////////
// CSelectSimulDataDlg dialog


CSelectSimulDataDlg::CSelectSimulDataDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectSimulDataDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSelectSimulDataDlg)
	m_strDataTitle = _T("");
	m_strEndTimeCycle = TEXT_LANG[0];//"1 Cycle 종료시간 :  0일 00:00:00.0"
	m_bCheck = FALSE;
	//}}AFX_DATA_INIT
	m_strSelFile = _T("");
	
	isChangeSelFile = FALSE;

	lLimitHigh = 0;
	lLimitLow = 0;
	m_chMode = 0;
	IsChange = FALSE;

	LanguageinitMonConfig();

}

CSelectSimulDataDlg::~CSelectSimulDataDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CSelectSimulDataDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CSelectSimulDataDlg"), _T("TEXT_CSelectSimulDataDlg_CNT"), _T("TEXT_CSelectSimulDataDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CSelectSimulDataDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CSelectSimulDataDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}


void CSelectSimulDataDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelectSimulDataDlg)
	DDX_Text(pDX, IDC_EDIT_DATA_TITLE, m_strDataTitle);
	DDX_Text(pDX, IDC_STATIC_END_TIME_CYCLE, m_strEndTimeCycle);
	DDX_Check(pDX, IDC_CHECK1, m_bCheck);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelectSimulDataDlg, CDialog)
	//{{AFX_MSG_MAP(CSelectSimulDataDlg)
	ON_BN_CLICKED(IDC_BUTTON_DIR, OnButtonDir)
	ON_EN_CHANGE(IDC_EDIT_DATA_TITLE, OnChangeEditDataTitle)
	ON_BN_CLICKED(IDC_BUTTON_OPEN_EXCEL, OnButtonOpenExcel)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_SET_RANGE, OnProgressSetRange)
	ON_MESSAGE(WM_STEP_IT, OnProgressStepIt)
	ON_MESSAGE(WM_PROGRESS_SHOW, OnProgressShow)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelectSimulDataDlg message handlers

void CSelectSimulDataDlg::OnButtonDir() 
{
	// TODO: Add your control notification handler code here
	
	CString strFile = SelectRawFile(m_strSelFile);
	m_strSelFile = strFile;

	if(strFile != "NULL")
	{

		isChangeSelFile = TRUE;		//적용시에 파일을 변경하기 위해
		CString strDir(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION ,"DataBase"));
		CString strSrc = strDir + "\\" + strFile;
		m_strNewSelFile = strSrc;

		UpdateRawDataInfo(m_strNewSelFile);
		IsChange = TRUE;
		GetDlgItem(IDOK)->EnableWindow(TRUE);
	}
	
}

CString CSelectSimulDataDlg::GetPathName()
{
	return m_strSelFile;
}

CString CSelectSimulDataDlg::GetDataTitle()
{
	return m_strDataTitle;
}

CString CSelectSimulDataDlg::SelectRawFile(CString strFile)
{
	CString strSelFile;
	CString strNull = "NULL";
	CFileDialog dlg(TRUE, "", strSelFile, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, TEXT_LANG[1]);//"csv 파일(*.csv)|*.csv|All Files (*.*)|*.*|"
	if(dlg.DoModal() == IDOK)
	{
		CString strFileName = dlg.GetPathName();
		if(strFileName.GetLength() >= 112)
		{
			AfxMessageBox(TEXT_LANG[2]);//"파일명을 영문 112글자(한글56자) 이내로 지정하십시요."
			return "";
		}

		
		if(LoadData(strFileName) == FALSE)
			return strNull;	
		
		//1.1 Field 수 및 Record 수
		//1.2 출력값의 MAX값 확인

		
		
		//1.3 시간(최소 출력 간격 시간 및 시간 연전 등)
	
		CString strDir(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION ,"DataBase"));
		
		//2. 기존 Rawdata file 삭제 
		if(strFile.IsEmpty() == FALSE)
		{
			DeleteFile(strDir+"\\"+m_strSelFile);
		}

		//3. copy data file to database folder

		CTime ct = CTime::GetCurrentTime();
		strSelFile.Format("%s.tmp", ct.Format("%Y%m%d%H%M%S"));	
		if(CopyFile(strFileName, strDir+"\\"+strSelFile, TRUE) == FALSE)
		//if(MakeSimulationFile(strFileName, strDir+"\\"+strSelFile) == FALSE)
		{
			AfxMessageBox(strFileName+TEXT_LANG[3]);//" Data를 지정할 수 없습니다.(중복 파일)"
			return "";
		}
	}

	return strSelFile;
}

BOOL CSelectSimulDataDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_lMaxRefI = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Max Current", 5000);
	m_nMinRefI = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Min Ref Current", 25);

	m_wndGraph.SubclassDlgItem(IDC_SIMUL_GRAPH, this);

	//Progress 생성
	m_pProgress = new CProgressDlg;
	m_pProgress->Create(174);

	m_SheetData.SetParent(this);
	
	// TODO: Add extra initialization here
	if(m_strSelFile.IsEmpty())		//현재 등록된 파일이 없으면
	{
		//1. Select raw data file
		m_strNewSelFile = SelectRawFile(m_strSelFile);
		if(m_strNewSelFile.IsEmpty())	//파일이 등록되지 않았다면.
		{
			OnCancel();				//종료
		}
		else						//파일이 등록되면 그래프를 그린다.
		{
			isChangeSelFile = TRUE;
			CString strDir(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION ,"DataBase"));
			CString strSrc = strDir + "\\" + m_strNewSelFile;
			m_strNewSelFile = strSrc;			//파일정보로 절대 경로를 갖는다.
			UpdateRawDataInfo(m_strNewSelFile);
		}
		GetDlgItem(IDOK)->EnableWindow(TRUE);
	}
	else							//헌재 등록된 파일이 있다면
	{
		//Title를 찾아서 화면에 보여준다.//////
		CString strFile(m_strSelFile);
		int nStart=strFile.Find('-');
		int nEnd = strFile.ReverseFind('.');

		if(nStart >= 0 && nEnd >= 0 && nStart < nEnd)
		{
			m_strDataTitle = strFile.Mid(nStart+1, nEnd-nStart-1);
		}
		/////////////////////////////////////////

		if(LoadData(m_strSelFile) == FALSE)	//파일을 읽어들인다.
			return FALSE;	
		else								//그래프를 그린다.
			UpdateRawDataInfo(m_strSelFile);			
			
	}	


	UpdateData(FALSE);

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CSelectSimulDataDlg::UpdateRawDataInfo(CString strFile)
{
	DrawGraph();		//그래프를 그린다.
	return TRUE;
}

void CSelectSimulDataDlg::OnOK() 
{
	// TODO: Add extra validation here
	
	UpdateData();
//	AfxMessageBox("현 장비는 Pattern 기능을 제공하지 않습니다. \r\nPattern 기능 사용을 원하시면 (주)피앤이솔루션으로 문의하시기 바랍니다. ");
//	return;

	if(m_strDataTitle.IsEmpty())
	{
		AfxMessageBox(TEXT_LANG[4]);//"Data 명을 입력하십시요"
		return;
	}

	//파일명 최종 Update한다.
	CString strSimulationFile;
	CString strSrc;
	CString strDir(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION ,"DataBase"));	
	
	if(isChangeSelFile)				//파일을 새로 등록했다면
	{
		int nStart = m_strNewSelFile.ReverseFind('\\')+1;
		int nEnd = m_strNewSelFile.ReverseFind('.') - nStart;
		strSimulationFile.Format("%s-%s.csv",
			m_strNewSelFile.Mid(nStart,nEnd ),m_strDataTitle);

		//이전 파일 삭제
		if(m_strSelFile != "")
		{		
			CFileFind aFind;
			BOOL bFind = aFind.FindFile(m_strSelFile);
			while(bFind)
			{
				bFind = aFind.FindNextFile();
				DeleteFile(aFind.GetFilePath());
			}
		}

		m_strSelFile = m_strNewSelFile;		//다시 등록*/

	}
	else
	{
		CTime ct = CTime::GetCurrentTime();
		strSimulationFile.Format("%s-%s.csv", ct.Format("%Y%m%d%H%M%S"),  m_strDataTitle);
	}

	//file rename 
	strSrc = m_strSelFile;
	m_strSelFile = strDir + "\\" + strSimulationFile;
//	if(rename(strSrc, m_strSelFile) !=0)
//	{
	//	AfxMessageBox("Data 명이 유효하지 않습니다.");
	//	return;
		
//	}

	int nFile= 2;
	while(rename(strSrc, m_strSelFile) !=0)
	{
		CString strFileNum;
		strFileNum.Format("(%d)", nFile);
		m_strSelFile.Insert(m_strSelFile.ReverseFind('.'), strFileNum);
	}


	//임시 파일 삭제
	DeleteTempFile();
	if(m_pProgress != NULL)
	{
		m_pProgress->DestroyWindow();
		delete m_pProgress;
	}

	if(m_bCheck)
	{
		pStep->ulEndTime =atol(m_strTimeEndCycle);
	}

	CDialog::OnOK();
}

void CSelectSimulDataDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	if(IsChange)
	{
		if(AfxMessageBox(TEXT_LANG[5], MB_YESNO) != IDYES)//"Pattern 데이터가 변경되었습니다..\r\n작업을 취소하시겠습니까?"
			return;
	}
	//임시 파일 삭제
	
	DeleteTempFile();
	if(m_pProgress != NULL)
	{
		m_pProgress->DestroyWindow();
		delete m_pProgress;
	}
	
	CDialog::OnCancel();
}

BOOL CSelectSimulDataDlg::SetDataSheet(CString strFile)
{
	return m_SheetData.SetSimulationFile(strFile, m_strErrorMsg);
}

BOOL CSelectSimulDataDlg::LoadData(CString strFile)
{
	long low = 0;
	long high = 0;
	if(SetDataSheet(strFile))
	{
		//1. 파일 읽기 및 파일 유효성 체크
		if(m_SheetData.GetColumnHeader(0) != "T1" && m_SheetData.GetColumnHeader(0) != "t1"
			&&m_SheetData.GetColumnHeader(0) != "T2" && m_SheetData.GetColumnHeader(0) != "t2")
		{
			AfxMessageBox(TEXT_LANG[6]);//"파일 형식이 유효하지 않습니다."
			return FALSE;
		}
		
		if(m_SheetData.GetColumnHeader(COLUMN_DATA) != "P" && m_SheetData.GetColumnHeader(COLUMN_DATA) != "p" &&
			m_SheetData.GetColumnHeader(COLUMN_DATA) != "I" && m_SheetData.GetColumnHeader(COLUMN_DATA) != "i")
		{
			AfxMessageBox(TEXT_LANG[6]);//"파일 형식이 유효하지 않습니다."
			return FALSE;
		}

		if(m_SheetData.GetRecordCount() > MAX_PATTERN_ROW_COUNT)
		{

			CString strErrorMsg;
			strErrorMsg.Format(TEXT_LANG[7], MAX_PATTERN_ROW_COUNT);//"최대 Row 수를 넘었습니다. (최대 : %d)"
			AfxMessageBox(strErrorMsg );
			return FALSE;
		}

		//2. 최소값 최대값 
		float * fValue = m_SheetData.GetColumnData(COLUMN_DATA);
		if(fValue != NULL)
		{
			 
			low = fabs(fValue[0]) * 1000;
						
			for(int i = 0 ; i < m_SheetData.GetRecordCount(); i++)
			{
				if(high < fabs(fValue[i])*1000)
					high = fabs(fValue[i]) * 1000;
				
				if(low > fabs(fValue[i])*1000)
					low = fabs(fValue[i]) * 1000;
			}
		}

		//3. 조건값과 비교
		//3.1 전류 상하한값 비교
		if(low < 0)
		{
			CString m_strError;
			m_strError.Format(TEXT_LANG[8],  0, m_lMaxRefI);//"입력값이 범위를 벗어났습니다.(%d~%d)"
			AfxMessageBox(m_strError);								
			return FALSE;
		}
		//3.2 전류 안전조건 상하한값 비교
		//전류 제한 상한값을 입력하였을 경우 전류 설정값보다 반드시 커야 한다.
		if(pStep->fILimitHigh > 0)
		{
			if(high >=pStep->fILimitHigh)
			{
				CString m_strError;
				m_strError.Format(TEXT_LANG[9],  pStep->fILimitHigh, high);//"안전 전류 상한값이(%.0f mA) Pattern 입력 최대 전류(%ld mA)보다 작습니다."
				AfxMessageBox(m_strError);								
				return FALSE;
			}
		}

		//3.3 1Cycle 종료시간 계산
		fValue = m_SheetData.GetColumnData(0);
		if(fValue != NULL)
		{
			if(m_SheetData.GetColumnHeader(0) == "T1" )
			{
				float fTime = 0.0f;
				if(m_SheetData.GetRecordCount() > 0)
				{
					CString strTime;
					strTime.Format("%.1f", fValue[m_SheetData.GetRecordCount()-1]*10);
					m_strTimeEndCycle.Format("%.1f", fValue[m_SheetData.GetRecordCount()-1]*100);
					
					ULONG lTime = atol(strTime);
					ULONG lSecond = (ULONG)fValue[m_SheetData.GetRecordCount()-1];
					ULONG lmSec = (lTime % 10);
					COleDateTime endTime;

					
					int nHour = lSecond % 86400;
					int nDay = lSecond / 86400;
					endTime.SetTime(nHour/3600, (nHour%3600)/60, (nHour%3600)%60);
					m_strEndTimeCycle.Format(TEXT_LANG[10], nDay, endTime.GetHour(), //"1 Cycle 종료시간 : %d일 %02d:%02d:%02d.%d"
						endTime.GetMinute(), endTime.GetSecond(), lmSec);
				
					UpdateData(FALSE);
				}
			}
		}

		lLimitHigh = high;
		lLimitLow = low;
		if(m_SheetData.GetColumnHeader(COLUMN_DATA) == "I" || m_SheetData.GetColumnHeader(COLUMN_DATA) == "i")
			m_chMode = PS_MODE_CC;
		else
			m_chMode = PS_MODE_CP;
		
	}
	else
	{
		if(m_strErrorMsg == "")
			AfxMessageBox(TEXT_LANG[11]);//"파일 열기를 실패 하였습니다."
		else
			AfxMessageBox(m_strErrorMsg);
		IsChange = FALSE;
		return FALSE;
		
	}

	return TRUE;
}

BOOL CSelectSimulDataDlg::DrawGraph()
{
	int i = 0;
	int n = m_SheetData.GetRecordCount();	
	BOOL IsK = FALSE;
	
	m_wndGraph.ClearGraph();
	for(i=0;  i<PE_MAX_SUBSET; i++)
	{
		m_wndGraph.ShowSubset(i, FALSE);
	}

	//Simulation Data은 T1 (누적시간) or T2(동작시간), P or I  2개의 열로 구성되어 있다.
	//그래프를 그리는 용으로 T1 or T2 는 X(시간)축  /  P or I 열 Y(데이터)축
	float *fXData = m_SheetData.GetColumnData(0);		//T
	float *fData = m_SheetData.GetColumnData(COLUMN_DATA);		//P or I		

	for(i = 0 ; i < n ; i++)
	{
		if(fData[i] > 1000)
		{
			IsK = TRUE;
			break;
			
		}
	}
	if(IsK == TRUE)
	{
		for(i = 0 ; i < n ; i++)
		{
			fData[i] = fData[i] / 1000;
		}
	}
	
	m_pProgress->SetWindowText(TEXT_LANG[12]);//"그래프를 표현하고 있습니다."
	m_wndGraph.SetDataPtr(0, n, fXData, fData);
	m_wndGraph.ShowSubset(0, TRUE);

	//Set X Axis Label
	m_wndGraph.SetXAxisLabel(m_SheetData.GetColumnHeader(0));
	//Set Y Axil Label
	if(!IsK)
	{
		if(m_SheetData.GetColumnHeader(COLUMN_DATA) == "I")
			m_wndGraph.SetYAxisLabel(0, "A");
		else
			m_wndGraph.SetYAxisLabel(0, "W");
	}
	else
	{
		if(m_SheetData.GetColumnHeader(COLUMN_DATA) == "I")
			m_wndGraph.SetYAxisLabel(0, "KA");
		else
			m_wndGraph.SetYAxisLabel(0, "KW");
	}
	


	//Set Title
	CString strTemp;
	CString strFile = m_strSelFile;
	strTemp = strFile.Mid(strFile.ReverseFind('-')+1, strFile.ReverseFind('.'));
	m_wndGraph.SetMainTitle(strTemp.Left(strTemp.ReverseFind('.')));

	m_SheetData.Clear();
	
	return TRUE;
}

BOOL CSelectSimulDataDlg::DeleteTempFile(CString strFile)
{
	CString strDir(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION ,"DataBase"));
	CFileFind aFind;
	BOOL bFind;

	if(strFile == "")
		bFind = aFind.FindFile(strDir + "\\" + "??????????????.tmp");
	else
		bFind = aFind.FindFile(strFile);

	while(bFind)
	{
		bFind = aFind.FindNextFile();
		DeleteFile(aFind.GetFilePath());
	}

	return TRUE;
}

BOOL CSelectSimulDataDlg::MakeSimulationFile(CString strExistingFileName, CString strNexFileName)
{
	CStdioFile aFile;
	CFileException e;

	if( !aFile.Open( strExistingFileName, CFile::modeRead|CFile::shareDenyWrite, &e ) )
	{
		#ifdef _DEBUG
		   afxDump << "File could not be opened " << e.m_cause << "\n";
		#endif
		   return FALSE;
	}

	CStringList		strDataList;
	CString			strTemp;
	while(aFile.ReadString(strTemp))
	{
		if(strTemp == "STX")			//여기서부터 실제 Simulation Data -> 이 문자 위로는 주석문(처리안함)
		{
			while(aFile.ReadString(strTemp))
			{
				if(!strTemp.IsEmpty())	
				{
					strDataList.AddTail(strTemp);
				}
			}

		}	
	}	
	aFile.Close();


	if( !aFile.Open( strNexFileName, CFile::modeWrite|CFile::modeCreate, &e ) )
	{
		#ifdef _DEBUG
		   afxDump << "File could not be Created " << e.m_cause << "\n";
		#endif
		   return FALSE;
	}

	POSITION pos = strDataList.GetHeadPosition();
	while(pos)
	{
		aFile.WriteString(strDataList.GetAt(pos));
		aFile.WriteString("\r\n");
		strDataList.GetNext(pos);
	}
	
	aFile.Close();



	return TRUE;
}



void CSelectSimulDataDlg::OnChangeEditDataTitle() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	GetDlgItem(IDOK)->EnableWindow(TRUE);
	
}

LRESULT CSelectSimulDataDlg::OnProgressSetRange(WPARAM wParam, LPARAM lParam)
{
	int nRange = (int)wParam;
	m_pProgress->SetRange(nRange);
	return 1;
}

LRESULT CSelectSimulDataDlg::OnProgressStepIt(WPARAM wParam, LPARAM lParam)
{
	m_pProgress->StepIt();
	return 1;
}

LRESULT CSelectSimulDataDlg::OnProgressShow(WPARAM wParam, LPARAM lParam)
{
	int nShow = (int)wParam;
	m_pProgress->ShowWindow(nShow);
	return 1;
}

void CSelectSimulDataDlg::OnButtonOpenExcel() 
{
	//Excel 로 열기 위해 CScheduleData의 Excel 기능을 사용한다.
	CScheduleData scData;
	scData.ExecuteExcel(m_strNewSelFile);
}
