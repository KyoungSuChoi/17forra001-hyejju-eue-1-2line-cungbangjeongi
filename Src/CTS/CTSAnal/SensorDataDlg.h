#if !defined(AFX_SENSORDATADLG_H__39D330CE_700F_4763_A88A_67064C69C696__INCLUDED_)
#define AFX_SENSORDATADLG_H__39D330CE_700F_4763_A88A_67064C69C696__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SensorDataDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSensorDataDlg dialog
#include "MyGridWnd.h"
#include "CTSAnalDoc.h"

#define  EP_MAX_JIGTEMP 9

class CSensorDataDlg : public CDialog
{
// Construction
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	CSensorDataDlg(CFormResultFile *pResult, CWnd* pParent = NULL);   // standard constructor
	virtual ~CSensorDataDlg();

	void SetDocumnet(CCTSAnalDoc *pDoc);
	

// Dialog Data
	//{{AFX_DATA(CSensorDataDlg)
	enum { IDD = IDD_SENSOR_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSensorDataDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CCTSAnalDoc *m_pDoc;
	BOOL DisplayData_1();
	BOOL DisplayData();
	CMyGridWnd m_wndGrid;
	void InitGrid();
	CFormResultFile *m_pResultFile;
	// Generated message map functions
	//{{AFX_MSG(CSensorDataDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonExcel();
	afx_msg void OnButtonPrint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SENSORDATADLG_H__39D330CE_700F_4763_A88A_67064C69C696__INCLUDED_)
