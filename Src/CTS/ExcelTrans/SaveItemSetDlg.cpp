// SaveItemSetDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ExcelTrans.h"
#include "SaveItemSetDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSaveItemSetDlg dialog


CSaveItemSetDlg::CSaveItemSetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSaveItemSetDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSaveItemSetDlg)
	m_bState = TRUE;
	m_bTime = TRUE;
	m_bVoltage = TRUE;
	m_bCurrent = TRUE;
	m_bCapacity = TRUE;
	m_bCapEff = TRUE;
	m_bImpedance = TRUE;
	m_bWatt = TRUE;
	m_bWattHour = TRUE;
	m_bGradeCode = TRUE;
	m_bCellCode = TRUE;
	m_bType = TRUE;
	m_bSumCap = FALSE;

	m_bTimeGetChargeVoltage = FALSE;
	m_bComCapacity = FALSE;
	m_bCCTime = FALSE;
	m_bComDcir = FALSE;
	m_bDcir = FALSE;
	m_bV1 = FALSE;
	m_bV2 = FALSE;
	m_bAvgJigTemp = FALSE;
	m_bAvgCurrent = FALSE;
	//}}AFX_DATA_INIT
}


void CSaveItemSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSaveItemSetDlg)
	DDX_Check(pDX, IDC_CHECK2, m_bState);
	DDX_Check(pDX, IDC_CHECK3, m_bTime);
	DDX_Check(pDX, IDC_CHECK4, m_bVoltage);
	DDX_Check(pDX, IDC_CHECK5, m_bCurrent);
	DDX_Check(pDX, IDC_CHECK6, m_bCapacity);
	DDX_Check(pDX, IDC_CHECK12, m_bCapEff);
	DDX_Check(pDX, IDC_CHECK7, m_bImpedance);
	DDX_Check(pDX, IDC_CHECK8, m_bWatt);
	DDX_Check(pDX, IDC_CHECK9, m_bWattHour);
	DDX_Check(pDX, IDC_CHECK10, m_bGradeCode);
	DDX_Check(pDX, IDC_CHECK11, m_bCellCode);
	DDX_Check(pDX, IDC_CHECK1, m_bType);
	DDX_Check(pDX, IDC_CHECK13, m_bSumCap);
	DDX_Check(pDX, IDC_CHECK14, m_bTimeGetChargeVoltage);
	DDX_Check(pDX, IDC_CHECK15, m_bComCapacity);
	DDX_Check(pDX, IDC_CHECK16, m_bCCTime);
	DDX_Check(pDX, IDC_CHECK17, m_bComDcir);
	DDX_Check(pDX, IDC_CHECK18, m_bDcir);
	DDX_Check(pDX, IDC_CHECK19, m_bV1);
	DDX_Check(pDX, IDC_CHECK20, m_bV2);
	DDX_Check(pDX, IDC_CHECK21, m_bAvgJigTemp);
	DDX_Check(pDX, IDC_CHECK22, m_bAvgCurrent);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSaveItemSetDlg, CDialog)
	//{{AFX_MSG_MAP(CSaveItemSetDlg)

END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSaveItemSetDlg message handlers
void CSaveItemSetDlg::InitFont()
{
	LOGFONT	LogFont;

	GetDlgItem(IDC_STATIC1)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight = 1000;
	LogFont.lfHeight = 15;

	m_Font.CreateFontIndirect( &LogFont );

	GetDlgItem(IDC_STATIC1)->SetFont(&m_Font);
	GetDlgItem(IDC_STATIC2)->SetFont(&m_Font);

	GetDlgItem(IDC_CHECK1)->SetFont(&m_Font);
	GetDlgItem(IDC_CHECK2)->SetFont(&m_Font);
	GetDlgItem(IDC_CHECK3)->SetFont(&m_Font);
	GetDlgItem(IDC_CHECK4)->SetFont(&m_Font);
	GetDlgItem(IDC_CHECK5)->SetFont(&m_Font);
	GetDlgItem(IDC_CHECK6)->SetFont(&m_Font);
	GetDlgItem(IDC_CHECK7)->SetFont(&m_Font);
	GetDlgItem(IDC_CHECK8)->SetFont(&m_Font);
	GetDlgItem(IDC_CHECK9)->SetFont(&m_Font);
	GetDlgItem(IDC_CHECK10)->SetFont(&m_Font);
	GetDlgItem(IDC_CHECK11)->SetFont(&m_Font);
	GetDlgItem(IDC_CHECK12)->SetFont(&m_Font);
	GetDlgItem(IDC_CHECK13)->SetFont(&m_Font);
	GetDlgItem(IDC_CHECK14)->SetFont(&m_Font);
	GetDlgItem(IDC_CHECK15)->SetFont(&m_Font);
	GetDlgItem(IDC_CHECK16)->SetFont(&m_Font);
	GetDlgItem(IDC_CHECK17)->SetFont(&m_Font);
	GetDlgItem(IDC_CHECK18)->SetFont(&m_Font);
	GetDlgItem(IDC_CHECK19)->SetFont(&m_Font);
	GetDlgItem(IDC_CHECK20)->SetFont(&m_Font);
	GetDlgItem(IDC_CHECK21)->SetFont(&m_Font);
	GetDlgItem(IDC_CHECK22)->SetFont(&m_Font);
}

void CSaveItemSetDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
	
	AfxGetApp()->WriteProfileInt(REG_SECTION, "State", m_bState);
	AfxGetApp()->WriteProfileInt(REG_SECTION, "Time", m_bTime);
	AfxGetApp()->WriteProfileInt(REG_SECTION, "Voltage", m_bVoltage);
	AfxGetApp()->WriteProfileInt(REG_SECTION, "Current", m_bCurrent);
	AfxGetApp()->WriteProfileInt(REG_SECTION, "Capacity", m_bCapacity);
	AfxGetApp()->WriteProfileInt(REG_SECTION, "Impedance", m_bImpedance);
	AfxGetApp()->WriteProfileInt(REG_SECTION, "Watt", m_bWatt);
	AfxGetApp()->WriteProfileInt(REG_SECTION, "WattHour", m_bWattHour);
	AfxGetApp()->WriteProfileInt(REG_SECTION, "GradeCode", m_bGradeCode);
	AfxGetApp()->WriteProfileInt(REG_SECTION, "CellCode", m_bCellCode);
	AfxGetApp()->WriteProfileInt(REG_SECTION, "Type", m_bType);
	AfxGetApp()->WriteProfileInt(REG_SECTION, "CapEff", m_bCapEff);
	AfxGetApp()->WriteProfileInt(REG_SECTION, "CapaSum", m_bSumCap);

	AfxGetApp()->WriteProfileInt(REG_SECTION, "TimeGetChargeVoltage", m_bTimeGetChargeVoltage);
	AfxGetApp()->WriteProfileInt(REG_SECTION, "ComCapacity", m_bComCapacity);
	AfxGetApp()->WriteProfileInt(REG_SECTION, "CCTime", m_bCCTime);
	AfxGetApp()->WriteProfileInt(REG_SECTION, "ComDcir", m_bComDcir);
	AfxGetApp()->WriteProfileInt(REG_SECTION, "Dcir", m_bDcir);
	AfxGetApp()->WriteProfileInt(REG_SECTION, "V1", m_bV1);
	AfxGetApp()->WriteProfileInt(REG_SECTION, "V2", m_bV2);
	AfxGetApp()->WriteProfileInt(REG_SECTION, "AvgJigTemp", m_bAvgJigTemp);
	AfxGetApp()->WriteProfileInt(REG_SECTION, "AvgCurrent", m_bAvgCurrent);

	CDialog::OnOK();
}

BOOL CSaveItemSetDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	InitFont();

	m_bState = AfxGetApp()->GetProfileInt(REG_SECTION, "State", TRUE);
	m_bTime = AfxGetApp()->GetProfileInt(REG_SECTION, "Time", TRUE);
	m_bVoltage = AfxGetApp()->GetProfileInt(REG_SECTION, "Voltage", TRUE);
	m_bCurrent = AfxGetApp()->GetProfileInt(REG_SECTION, "Current", TRUE);
	m_bCapacity = AfxGetApp()->GetProfileInt(REG_SECTION, "Capacity", TRUE);
	m_bImpedance = AfxGetApp()->GetProfileInt(REG_SECTION, "Impedance", TRUE);
	m_bWatt = AfxGetApp()->GetProfileInt(REG_SECTION, "Watt", TRUE);
	m_bWattHour = AfxGetApp()->GetProfileInt(REG_SECTION, "WattHour", TRUE);
	m_bGradeCode = AfxGetApp()->GetProfileInt(REG_SECTION, "GradeCode", TRUE);
	m_bCellCode = AfxGetApp()->GetProfileInt(REG_SECTION, "CellCode", TRUE);
	m_bType = AfxGetApp()->GetProfileInt(REG_SECTION, "Type", TRUE);
	m_bCapEff = AfxGetApp()->GetProfileInt(REG_SECTION, "CapEff", TRUE);
	m_bSumCap = AfxGetApp()->GetProfileInt(REG_SECTION, "CapaSum", FALSE);

	m_bTimeGetChargeVoltage = AfxGetApp()->GetProfileInt(REG_SECTION, "TimeGetChargeVoltage", TRUE);
	m_bComCapacity = AfxGetApp()->GetProfileInt(REG_SECTION, "ComCapacity", TRUE);
	m_bCCTime = AfxGetApp()->GetProfileInt(REG_SECTION, "CCTime", TRUE);
	m_bComDcir = AfxGetApp()->GetProfileInt(REG_SECTION, "ComDcir", TRUE);
	m_bDcir = AfxGetApp()->GetProfileInt(REG_SECTION, "Dcir", TRUE);
	m_bV1 = AfxGetApp()->GetProfileInt(REG_SECTION, "V1", TRUE);
	m_bV2 = AfxGetApp()->GetProfileInt(REG_SECTION, "V2", TRUE);
	m_bAvgJigTemp = AfxGetApp()->GetProfileInt(REG_SECTION, "AvgJigTemp", TRUE);
	m_bAvgCurrent = AfxGetApp()->GetProfileInt(REG_SECTION, "AvgCurrent", TRUE);

	UpdateData(FALSE);
	
	// TODO: Add extra initialization here
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

