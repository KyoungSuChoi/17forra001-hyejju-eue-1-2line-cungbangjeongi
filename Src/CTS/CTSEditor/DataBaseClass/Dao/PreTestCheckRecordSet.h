#if !defined(AFX_PRETESTCHECKRECORDSET_H__CED5C4DF_3393_4EEC_A9A0_6259C0E77AB3__INCLUDED_)
#define AFX_PRETESTCHECKRECORDSET_H__CED5C4DF_3393_4EEC_A9A0_6259C0E77AB3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PreTestCheckRecordSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPreTestCheckRecordSet DAO recordset

//Cell �˻� ���� 

class CPreTestCheckRecordSet : public CDaoRecordset
{
public:
	CPreTestCheckRecordSet(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CPreTestCheckRecordSet)

// Field/Param Data
	//{{AFX_FIELD(CPreTestCheckRecordSet, CDaoRecordset)
	long	m_CheckID;
	long	m_TestID;
	long	m_ModelID;
	float	m_MaxV;
	float	m_MinV;
	float	m_CurrentRange;
	float	m_OCVLimit;
	float	m_TrickleCurrent;
	long	m_TrickleTime;
	float	m_DeltaVoltage;
	float	m_MaxFaultBattery;
	COleDateTime	m_AutoTime;
	BOOL	m_AutoProYN;
	BOOL	m_PreTestCheck;
	float	m_DeltaVoltageLimit;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPreTestCheckRecordSet)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PRETESTCHECKRECORDSET_H__CED5C4DF_3393_4EEC_A9A0_6259C0E77AB3__INCLUDED_)
