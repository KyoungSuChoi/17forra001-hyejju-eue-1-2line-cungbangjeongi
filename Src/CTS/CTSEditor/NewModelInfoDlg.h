#if !defined(AFX_NEWMODELINFODLG_H__13160C4C_1284_4E5C_8DF3_6978926DD030__INCLUDED_)
#define AFX_NEWMODELINFODLG_H__13160C4C_1284_4E5C_8DF3_6978926DD030__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NewModelInfoDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CNewModelInfoDlg dialog

class CNewModelInfoDlg : public CDialog
{
// Construction
public:
	CNewModelInfoDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CNewModelInfoDlg)
	enum { IDD = IDD_NEW_MODEL_DIALOG };
	CString	m_strName;
	CString	m_strDescript;
	CString	m_strID;
	float	m_fCapacity;
	float	m_fWidth;
	float	m_fHeight;
	float	m_fDepth;
	CString	m_strBarCode;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNewModelInfoDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CNewModelInfoDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NEWMODELINFODLG_H__13160C4C_1284_4E5C_8DF3_6978926DD030__INCLUDED_)
