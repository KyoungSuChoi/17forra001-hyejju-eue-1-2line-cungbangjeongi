#include <direct.h>
#include <fcntl.h>
#include <io.h>


typedef struct 
{
	BOOL	  bIsDirectory;   //디렉토리
	CString   strPath;        //절대패스
	CString   strFileName;    //파일명 
	CString   strTempPath;    //파일 비교를 위한 변수
	ULONGLONG nFileSize;      //파일크기 
	CString   sFileSize;      //형식파일크기 
	CString   strType;        //파일타입 
	FILETIME  ftLastWriteTime;//파일갱신타임
	int       nImage;         //윈도우기본아이콘번호
	int       nPlaytime;      //재생시간

} FILEINFO;

class GFile 
{
public:   
	GFile();
    ~GFile();
    	
	
	static BOOL file_Finder(CString path)
	{//file find	
        if(path.IsEmpty() || path==_T("")) return FALSE;

		BOOL bWork;
		CFileFind finder;
		bWork=finder.FindFile(path);		
		finder.Close();		

		return bWork;
	}

	static BOOL file_FinderA(CString path)
	{//file find	
		BOOL flag=FALSE;
		HANDLE hFile=::CreateFile(path,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,0,NULL);
		if (hFile!=INVALID_HANDLE_VALUE) flag=TRUE;

		::CloseHandle(hFile);
		return flag;
	}

	static CString file_GetAppPath(HMODULE module=NULL)
	{//자기 자신 패스		
		return file_GetPathTrim(file_GetAppFilePath(module));
	}

	static CString file_GetPathTrim(CString file)
	{//전체 파일에서 경로만 구함
   		CString regPath=file_GetPath(file);	
		regPath.TrimRight(_T("\\"));
		regPath.TrimRight(_T("/"));
		
		return regPath;
	}

	static CString file_GetPath(LPCTSTR lpszFilePath)
	{//전체 파일에서 경로만 구함
		TCHAR szDir[_MAX_DIR];
		TCHAR szDrive[_MAX_DRIVE];
		_tsplitpath(lpszFilePath, szDrive, szDir, NULL, NULL);
			
		return  CString(szDrive) + CString(szDir);
	}

	static CString file_GetAppFilePath(HMODULE module)
	{//자기 자신 패스&파일이름        
		#ifdef _UNICODE	
		    wchar_t bpath[_MAX_PATH];
			GetModuleFileNameW(module, bpath, _MAX_PATH);
		#else
		    char bpath[_MAX_PATH];
			GetModuleFileName(module, bpath, _MAX_PATH);		
		#endif		

		CString spath=(CString)bpath;
		if(spath.IsEmpty() || spath==_T("")) return _T("");				
		return spath;	
	}

	/*static CPtrArray* file_LocalDirectoryList(CString spath,BOOL bRecursive=TRUE)
	{//directory file list
		if(!file_DirectoryExists(spath)) return NULL;
	    	
		CPtrArray *FileList=new CPtrArray;
		spath.TrimRight(_T("\\"));
		spath.TrimRight(_T("/"));
		CString CurDir=spath+_T("\\*");

		CFileFind finder;	
		BOOL bWorking = finder.FindFile(CurDir);		

		while (bWorking)
		{	  
		  bWorking = finder.FindNextFile();	
		  
		  if(finder.IsDots()) continue;
		  if(finder.GetFileName()==_T(".") || finder.GetFileName()==_T("..")) continue;
		  if(finder.GetFileName()==_T("Thumbs.db")) continue;

		  FILEINFO *FileInfo=new FILEINFO;		  
		  CString strFileName=finder.GetFileName();

		  FileInfo->bIsDirectory=finder.IsDirectory();
		  FileInfo->strPath=finder.GetFilePath();		 
		  FileInfo->strFileName=strFileName;
		  FileInfo->nFileSize=finder.GetLength();
		  FileInfo->sFileSize=GMath::math_FormatSize((DWORD)FileInfo->nFileSize);
		  
		  finder.GetLastWriteTime(&FileInfo->ftLastWriteTime);
		  
		  FileInfo->strType = GWin::win_GetTypeName(strFileName);
		  if(finder.IsDirectory()) FileInfo->strType=_T("Folder");
		  FileInfo->nImage = GWin::win_GetIconIndex(strFileName, finder.IsDirectory());
		  
		  FileList->Add(FileInfo);

		  if(bRecursive==TRUE && finder.IsDirectory())
		  {
			 CPtrArray *SubList=file_LocalDirectoryList(finder.GetFilePath());
			 if(!SubList || SubList->GetSize()<1) continue;
			 
			 FileList->InsertAt(FileList->GetSize(),SubList);
		  }	
		  Sleep(1);
		}
		finder.Close();	
		return FileList;
	}*/
	
	static BOOL file_DirectoryExists(LPCTSTR lpszDir)
	{//디렉토리 존재 여부
		#ifdef _UNICODE			    			
			wchar_t curPath[_MAX_PATH];   // Get the current working directory:

			if (!_wgetcwd(curPath, _MAX_PATH))
			{
				return FALSE;
			}
			if (_wchdir(lpszDir))	// retruns 0 if error
			{
				return FALSE;
			}
			_wchdir(curPath);
		#else
			char curPath[_MAX_PATH];   // Get the current working directory:
			
			if (!_getcwd(curPath, _MAX_PATH))
			{
				return FALSE;
			}
			if (_chdir(lpszDir))	// retruns 0 if error
			{
				return FALSE;
			}
            _chdir(curPath); 
		#endif	
		
		return TRUE;
	}

	static FILEINFO* file_FileInfoCopy(FILEINFO *FileInfo)
	{
		if(!FileInfo) return NULL;

		FILEINFO *tFileInfo=new FILEINFO;
		tFileInfo->bIsDirectory   =FileInfo->bIsDirectory;    
		tFileInfo->strPath        =FileInfo->strPath;         
		tFileInfo->strFileName    =FileInfo->strFileName;     
		tFileInfo->strTempPath    =FileInfo->strTempPath; 		
		tFileInfo->nFileSize      =FileInfo->nFileSize;       		
		tFileInfo->sFileSize      =FileInfo->sFileSize;   		
		tFileInfo->strType        =FileInfo->strType;         
		tFileInfo->ftLastWriteTime=FileInfo->ftLastWriteTime; 
		tFileInfo->nImage         =FileInfo->nImage;          
		tFileInfo->nPlaytime      =FileInfo->nPlaytime;  
		return tFileInfo;
	}

	static BOOL file_ForceDirectory(LPCTSTR lpDirectory)
	{//폴더 생성
		CString szDirectory = lpDirectory;
		szDirectory.TrimRight(_T("\\"));

		if ((file_GetPath(szDirectory) == szDirectory) ||
			(file_Exists(szDirectory) == -1))
			return TRUE;
		if (!file_ForceDirectory(file_GetPath(szDirectory)))
			return FALSE;
		if (!CreateDirectory(szDirectory, NULL))
			return FALSE;
		return TRUE;
	}

	static int file_Exists(LPCTSTR lpszName)
	{//파일 & 디렉토리 이 존재 하는지 체크
		CFileFind fileFind;
		if (!fileFind.FindFile(lpszName))
		{
			if (file_DirectoryExists(lpszName))  // if root ex. "C:\"
				return -1;
			return 0;
		}
		fileFind.FindNextFile();
		return fileFind.IsDirectory() ? -1 : 1;
	} 

	static BOOL file_AddWrite(CString filename,CString data)
	{//파일에 문자열 추가		
		FILE *fWrite=NULL;
		
		try
		{
			#ifdef _UNICODE			   			
				if(file_Finder(filename)==FALSE)
				{				
					fWrite = _wfopen(filename,_T("w+"));
					if(!fWrite) return FALSE;
							
					//한글utf-8
					//WORD mark = 0xFEFF;
					//fwrite(&mark, sizeof(WORD), 1, fWrite);
				} 
				else 
				{
					fWrite = _wfopen(filename,_T("r+"));
					if(!fWrite) return FALSE;
							
					fseek(fWrite, 0, SEEK_END);
				}
				fwprintf(fWrite, data);		
			#else
				if(file_Finder(filename)==FALSE)
				{				
					fWrite=fopen(filename,_T("w+"));	
					if(!fWrite) return FALSE;
				} 
				else 
				{
					fWrite=fopen(filename,_T("r+"));	
					if(!fWrite) return FALSE;
							
					fseek(fWrite, 0, SEEK_END);
				}
				fwrite(data,sizeof(char),data.GetLength(),fWrite);	
			#endif
		}
		catch (CMemoryException *e)
		{	
			TRACE("file_AddWrite()CMemoryException\n");
		}
		catch (CFileException *e)
		{
			TRACE("file_AddWrite()CMemoryException\n");
		}
		catch (CException *e)
		{
			TRACE("file_AddWrite()CException\n");
		}
		
		if(fWrite) fclose(fWrite);	
		return TRUE;
	}

	static DWORD file_SizeA(CString str_path)
	{//file size
		BOOL bWork;
		CFileFind finder;		
		bWork=finder.FindFile(str_path);		
		DWORD filesize=0;
		if(bWork==TRUE)
		{			
			finder.FindNextFile();			
			filesize=(DWORD)finder.GetLength();	
		}
		finder.Close();		
		return filesize;
	}
    /*
	static BOOL file_SetFileTime(CString file,CString stime)
	{		
		HANDLE hFile;
		hFile=CreateFile(file, GENERIC_ALL, FILE_SHARE_READ|FILE_SHARE_WRITE,0 , OPEN_EXISTING,0,0);
		if(hFile==NULL || hFile== INVALID_HANDLE_VALUE) return FALSE;
		
		FILETIME ft;		
		file_SystemTime(stime, ft);		
		BOOL bflag=SetFileTime(hFile, &ft, &ft, &ft);
		
		if(hFile) CloseHandle(hFile);
		hFile=NULL;
		return bflag;
	}

	static CString file_LastWriteTime(CString str_path)
	{//file size
		BOOL bWork;
		CFileFind finder;
		bWork=finder.FindFile(str_path);	
		CString strTime=_T("");
		if(bWork==TRUE)
		{
			finder.FindNextFile();
			CTime ftime;
			finder.GetLastWriteTime(ftime);					
			strTime.Format(_T("%04d-%02d-%02d %02d:%02d:%02d"),
							  ftime.GetYear(),ftime.GetMonth(), ftime.GetDay(),
							  ftime.GetHour(), ftime.GetMinute(), ftime.GetSecond());
		}
		finder.Close();
		return strTime;
	}

	static BOOL file_IsDir(CString str_filepath)
	{//is folder		
		str_filepath.TrimRight(_T("\\"));

		if(::GetFileAttributes(str_filepath) == FILE_ATTRIBUTE_DIRECTORY)
		{				
			return TRUE;       
		} else {
			return FALSE;        	 
		}
	}

	static void file_DelPtFileInfo(CPtrArray *pFileList)
	{
		if(!pFileList) return;

		int nCount=pFileList->GetSize();
		if(nCount<1) return;

		for(int i=0;i<nCount;i++)
		{
			FILEINFO *FileInfo=(FILEINFO*)pFileList->GetAt(i);
			if(!FileInfo) continue;
			delete FileInfo;
			FileInfo=NULL;

		}
		pFileList->RemoveAll();		
	}

	static BOOL file_RenameDir(CString s_path,CString t_path)
	{//파일이름 및 디렉토리 이름 변경
		BOOL m_flag;
   		
		//BeginWaitCursor();
		if( MoveFile(s_path,t_path) != 0)
		{
			m_flag=TRUE;
		} 
		else 
		{
			m_flag=FALSE;
		}
		//EndWaitCursor();
		return m_flag;
	}*/

	static BOOL file_ChkFileAccess(CString sPath,int iMode)
	{//iMode=0(exist), 2(write), 4(read)
		int nRet=_access((LPSTR)(LPCTSTR)sPath,iMode);
		if(nRet==0)
		{
			return TRUE;
		}		
		return FALSE;
	}
	
	static BOOL file_NewWrite(CString filename,CString data)
	{//새로운 파일 생성	
		FILE *fWrite=NULL;	

		try
		{
			#ifdef _UNICODE	
				fWrite = _wfopen(filename,_T("w+"));
				if(!fWrite) return FALSE;
						
				WORD mark = 0xFEFF;
				fwrite(&mark, sizeof(WORD), 1, fWrite);
						
				fwprintf(fWrite, data);					
			#else
				fWrite=fopen(filename,_T("w+"));
				if(!fWrite) return FALSE;
						
				fwrite(data,sizeof(char),data.GetLength(),fWrite);	
			#endif	
		}
		catch (CMemoryException *e)
		{
			
		}
		catch (CFileException *e)
		{
		}
		catch (CException *e)
		{
		}

		if(fWrite) fclose(fWrite);
		return TRUE;
	}

    /*
	static CString file_GetFileVer(CString strFilename, CString strQuery)
	{//파일 버전 "FILEVERSION"

		if(strQuery ==_T("")) return _T("");
		if(file_Finder(strFilename)==FALSE) return _T("");  
    
		#ifdef _UNICODE	
			wchar_t *lptstrFilename=(wchar_t *)strFilename.GetBuffer();
			wchar_t *lptstrQuery=(wchar_t *)strQuery.GetBuffer();			
		#else
			char* lptstrFilename=(LPSTR)(LPCTSTR)strFilename;
			char* lptstrQuery=(LPSTR)(LPCTSTR)strQuery;		
		#endif	

		CString strVer=_T("");
		DWORD dwHandle;
		DWORD dwVersionInfoSize = GetFileVersionInfoSize(lptstrFilename, &dwHandle);
		if(dwVersionInfoSize == 0) return _T("");

		LPVOID pFileInfo = (LPVOID) HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, dwVersionInfoSize);
		GetFileVersionInfo(lptstrFilename, dwHandle, dwVersionInfoSize, pFileInfo);

		LPVOID ptr;
		UINT uLength;
		VerQueryValue(pFileInfo, TEXT("\\VarFileInfo\\Translation"), &ptr, &uLength);

		WORD *id = (WORD*) ptr;
		
		#ifdef _UNICODE	
			wchar_t szString[255];
			wsprintf(szString, _T("\\StringFileInfo\\%04X%04X\\%s"), id[0], id[1], lptstrQuery);
		#else
			char szString[255];
			sprintf(szString, _T("\\StringFileInfo\\%04X%04X\\%s"), id[0], id[1], lptstrQuery);
		#endif	

		VerQueryValue(pFileInfo, szString, &ptr, &uLength);

		if(uLength == 0) return _T("");

		char sOut[255];
		memset(sOut, 0x00, 255);
		strcpy(sOut, (char *) ptr);
		HeapFree(GetProcessHeap(), 0, pFileInfo);
		return (CString)sOut;
	}		

	static BOOL file_ResourceToFile(CString title,UINT resource,CString fileName,HINSTANCE hInstance=NULL)
	{//리소스를 파일로 변환		
		if(file_Finder(fileName)==TRUE && file_Size(fileName)>0) return TRUE;
	
		if(hInstance==NULL) hInstance=AfxGetResourceHandle();

		HRSRC hrSrc = FindResource(hInstance, MAKEINTRESOURCE(resource), title);
		if(hrSrc == NULL)	return FALSE;

		HGLOBAL hGlobal = LoadResource(hInstance, hrSrc);
		if(hGlobal == NULL)	return FALSE;

		LPVOID lpExe = LockResource(hGlobal);
		if(lpExe == NULL) return FALSE;

		//Create the new archive from the extractor in the resources
		CFile file;
		if(!file.Open(fileName,CFile::modeCreate | CFile::modeWrite | CFile::shareDenyNone))
		{			
			return FALSE;
		} 
		else
		{
			//Write the extractor exe
			file.Write(lpExe, (UINT)SizeofResource(hInstance, hrSrc));
			
			//Close the archive
			file.Close();
		}
		return TRUE;
	}
	
	static BOOL file_ReadFileData(CString file,LPBYTE &lpBinData,DWORD &dwFileSize)
	{//read file
		HANDLE hFile2 = CreateFile(file,GENERIC_READ,
										 FILE_SHARE_READ,
										 NULL,
										 OPEN_EXISTING,
										 FILE_ATTRIBUTE_NORMAL,
										 NULL);
	   if(hFile2==NULL || hFile2==INVALID_HANDLE_VALUE) return FALSE;

	   dwFileSize = GetFileSize(hFile2, NULL);
	   lpBinData = (LPBYTE)LocalAlloc(LPTR, dwFileSize);
	   ZeroMemory(lpBinData, dwFileSize);
	   SetFilePointer(hFile2, 0, 0, FILE_BEGIN);
	   
	   DWORD dwByteRead=0;
	   ReadFile(hFile2,lpBinData,dwFileSize,&dwByteRead,NULL);
	   if(hFile2) CloseHandle(hFile2);
	   hFile2 = NULL;

	   return TRUE;
	}

	static BOOL file_WriteFileData(CString file,CString title,UINT resource,
								   LPBYTE lpBinData,DWORD dwFileSize)
	{//exe file resource file to data write    
	   HANDLE hFile = BeginUpdateResource (file, FALSE);
	   if (hFile==NULL || hFile==INVALID_HANDLE_VALUE) return FALSE;
		
	   BOOL bflag=UpdateResource (hFile,title,MAKEINTRESOURCE(resource),
								  MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US),
								  lpBinData,
								  dwFileSize);
	   if(bflag==FALSE)
	   {
		   if(hFile) CloseHandle(hFile);
			hFile = NULL;
			return FALSE;
	   }

	   if (EndUpdateResource (hFile, FALSE)==FALSE)
	   {
			if(hFile) CloseHandle(hFile);
			hFile = NULL;
			return FALSE;
	   }
	   return TRUE;
	}*/

	static LPCTSTR file_ResorceToStr(CString title,UINT resource)
	{
		HRSRC hrSrc = FindResource(AfxGetResourceHandle(), MAKEINTRESOURCE(resource), title);
		if(hrSrc == NULL)	return _T("");

		HGLOBAL hGlobal = LoadResource(AfxGetResourceHandle(), hrSrc);
		if(hGlobal == NULL) return _T("");

		LPCTSTR lpcHtml = static_cast<LPCTSTR>(LockResource(hGlobal));
		if (lpcHtml == NULL) return _T("");
   
		return lpcHtml;

	}
/*(
	static CString file_TempPath()
	{//temporary 폴더
		#ifdef _UNICODE	
			wchar_t TempPath[_MAX_PATH];
		#else
			char TempPath[_MAX_PATH];
		#endif

		GetTempPath(_MAX_PATH, TempPath);
    
		CString path=(CString)TempPath;
		path.TrimRight(_T("\\"));
		path.TrimRight(_T("/"));
				
		return path;
	}
	
	static BOOL file_MoveBoot(CString s_filename, CString t_filename) 
	{//부팅한후 파일 이동
		BOOL flag;
   		
		//BeginWaitCursor();
		if(MoveFileEx(s_filename, t_filename, MOVEFILE_DELAY_UNTIL_REBOOT) != 0)
		{
			flag=TRUE;
		} else {
			flag=FALSE;
		}
		//EndWaitCursor();
		
		return flag;
	}
	
	static BOOL file_MoveEx(CString s_filename, CString t_filename) 
	{//파일 이동
		BOOL flag;
   		
		//BeginWaitCursor();
		if(MoveFileEx(s_filename, t_filename, MOVEFILE_REPLACE_EXISTING) != 0)
		{
			flag=TRUE;
		} else {
			flag=FALSE;
		}
		//EndWaitCursor();		
		return flag;
	}
	
	static BOOL file_MoveCopy(CString s_file, CString t_file) 
	{//파일 이동
		BOOL flag;
   		
		//BeginWaitCursor();
		if(MoveFileEx(s_file, t_file, MOVEFILE_COPY_ALLOWED | MOVEFILE_REPLACE_EXISTING) != 0)
		{
			flag=TRUE;
		} else {
			flag=FALSE;
		}
		//EndWaitCursor();		
		return flag;
	}

	static BOOL file_Copy(CString s_filename, CString t_filename) 
	{//파일 복사
		BOOL m_flag;
   		
		//BeginWaitCursor();
		if( CopyFile(s_filename,t_filename,FALSE) != 0)
		{
			m_flag=TRUE;
		} else {
			m_flag=FALSE;
		}
		//EndWaitCursor();
		return m_flag;
	}
	
	static void file_Dirlist(CString str_path,CStringArray &Data,int itype=0)	
	{//특정폴더의 폴더 리스트 필터입력
		BOOL bWorking;
		CFileFind finder;
		
		bWorking=finder.FindFile(str_path);
		
		while (bWorking)
		{	  
		  bWorking = finder.FindNextFile();	
		  
		  if(finder.GetFileName()==_T(".") || finder.GetFileName()==_T("..")) continue;
		  if(finder.GetFileName()==_T("Thumbs.db")) continue;

		  if(finder.IsDirectory()==FALSE) continue;
		  
		  if(itype==1) Data.Add(finder.GetFilePath());
		  else         Data.Add(finder.GetFileName());
		} 
		finder.Close();
	}

	static void file_EncryptfileList(CString path)
	{//폴더에 있는 파일 암호화 하여 저장   
		CFileFind finder;
		DWORD fsize=0;
		BOOL bWorking = finder.FindFile(path);
		
		while (bWorking){	  
		  bWorking = finder.FindNextFile();	
		  
		  if(finder.GetFileName()==_T(".") || finder.GetFileName()==_T("..")) continue;
		  if(finder.GetFileName()==_T("Thumbs.db")) continue;

		  if(finder.IsDirectory()){		   
		  } else {	
			  CString data=file_Read(finder.GetFilePath());				  
              CString TargetFile=file_GetPathTrim(finder.GetFilePath())+_T("\\")+finder.GetFileTitle()+_T(".dat");
			  
			  file_Encrypt(TargetFile,data); //암호화             
		  }
		}
		finder.Close();
	}

	static void file_DecryptfileList(CString path)
	{//폴더에 있는 파일 복호화 하여 저장   
		CFileFind finder;
		DWORD fsize=0;
		BOOL bWorking = finder.FindFile(path);
		
		while (bWorking){	  
		  bWorking = finder.FindNextFile();	
		  
		  if(finder.GetFileName()==_T(".") || finder.GetFileName()==_T("..")) continue;
		  if(finder.GetFileName()==_T("Thumbs.db")) continue;

		  if(finder.IsDirectory()){		   
		  } else {	
			  CString data=file_Decrypt(finder.GetFilePath());//복호화 		  
              CString TargetFile=file_GetPathTrim(finder.GetFilePath())+_T("\\")+finder.GetFileTitle()+_T(".ini");
			  
			  file_NewWrite(TargetFile,data);			  			  
		  }
		}
		finder.Close();
	}

	static void file_Encrypt(CString fileName,CString data)
	{//바이너리 암호화하여 파일에 저장
		if(data.IsEmpty() || data==_T("")) return;

		int len=data.GetLength();
		char *buf=new char[len];
		memcpy(buf,data,len);
		
		for(int i = 0; i < len; i++){//암호화
			buf[i]^=6281;	
		}
				
		#ifdef _UNICODE	
			FILE* file=_wfopen(fileName,_T("wb+"));
		#else
			FILE* file=fopen(fileName,_T("wb+")); //바이너리 상태로 쓴다.
		#endif
		if(file==NULL) return;

		fseek( file, 0L, SEEK_SET);
		fwrite(buf, len, 1, file);

		fclose(file);
	}

	static CString file_Decrypt(CString fileName)
	{//텍스트 복호화	
		if(file_Finder(fileName)==FALSE) return _T("");
				
		#ifdef _UNICODE	
			FILE* file=_wfopen(fileName,_T("r"));
		#else
			FILE *file = fopen(fileName,_T("r")); //바이너리 상태로 읽는다.
		#endif
		
		if(file==NULL) return _T("");

		//파일사이즈를 알아낸다
		fseek(file, 0L, SEEK_END);//파일의 끝
		int len = ftell(file);
		fseek(file, 0L, SEEK_SET);//파일의 시작
		
		char* buf = new char[len];
		fread(buf, len, 1, file); //변화없음
 
		CString data=_T("");
		for(int i = 0; i <len; i++)// 복호화
		{
			buf[i] ^= 6281;

			CString temp=_T("");
			temp.Format(_T("%c"),buf[i]);
			data=data+temp;		 
		}	
		fclose(file);

		return data;
	}
	
	static void file_DeletefileList(CString path)
	{//폴더에 있는 파일 지우기	        
		CFileFind finder;
		DWORD fsize=0;
		BOOL bWorking = finder.FindFile(path);
		
		while (bWorking){	  
		  bWorking = finder.FindNextFile();	
		  
		  if(finder.GetFileName()==_T(".") || finder.GetFileName()==_T("..")) continue;
		  if(finder.GetFileName()==_T("Thumbs.db")) continue;

		  if(finder.IsDirectory()){		   
		  } else {				  
			  ::DeleteFile(finder.GetFilePath());
		  }
		}
		finder.Close();
	}
	
	static BOOL file_DeleteFolder(CString strFolder,BOOL bflag=FALSE)
	{//폴더삭제-모든 파일 삭제함.주의
		SHFILEOPSTRUCT FileOp = {0};

		#ifdef _UNICODE	
			wchar_t szTemp[MAX_PATH];
			wcscpy(szTemp, strFolder);
			szTemp[strFolder.GetLength() + 1] = NULL;    // NULL문자가 두개 들어가야 한다.
		#else
			char szTemp[MAX_PATH];
			strcpy(szTemp, strFolder);
			szTemp[strFolder.GetLength() + 1] = NULL;    // NULL문자가 두개 들어가야 한다.
		#endif
				
		FileOp.hwnd = NULL;
		FileOp.wFunc = FO_DELETE;
		FileOp.pFrom = NULL;
		FileOp.pTo = NULL;
		if(bflag==FALSE)
		{ // Mesg no show
			FileOp.fFlags = FOF_NOCONFIRMATION | FOF_NOERRORUI; 
		}
		FileOp.fAnyOperationsAborted = false;
		FileOp.hNameMappings = NULL;
		FileOp.lpszProgressTitle = NULL;
		
		FileOp.pFrom = szTemp;
		SHFileOperation(&FileOp);
    
		return TRUE;
	}

	static BOOL file_CopyFolder(CString sFolder,CString tFolder,BOOL bflag=FALSE)
	{//폴더복사
		SHFILEOPSTRUCT FileOp = {0};

		sFolder.TrimRight(_T("\\"));
		tFolder.TrimRight(_T("\\"));
		
		FileOp.hwnd = NULL;
		FileOp.wFunc = FO_COPY;
		FileOp.pFrom = sFolder;
		FileOp.pTo = tFolder;
		if(bflag==FALSE)
		{ // 확인메시지가 안뜨도록 설정
			FileOp.fFlags = FOF_NOCONFIRMATION | FOF_NOERRORUI; 
		}
		FileOp.fAnyOperationsAborted = false;
		FileOp.hNameMappings = NULL;
		FileOp.lpszProgressTitle = NULL;
				
		SHFileOperation(&FileOp);
    
		return TRUE;
	}

	static DWORD file_Size(CString path)
	{//파일 크기
		DWORD dwNeed=0,size=0;

		HANDLE hFile=::CreateFile(path,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,0,NULL);
		if (hFile!=INVALID_HANDLE_VALUE)
		{
			size=::GetFileSize(hFile,&dwNeed);
		}	

		::CloseHandle(hFile);
		return size;
	}
	
	static CString file_Title(CString str_path)	
	{//전체패스에서 파일타이틀만 구하기
		BOOL bWorking;
		CFileFind finder;
		CString fileName=_T("");
		
		bWorking=finder.FindFile(str_path);
		if(bWorking)
		{
		  bWorking = finder.FindNextFile();	
		  fileName=finder.GetFileTitle();			  
		} 
		finder.Close();
		return fileName;
	}

	static CString file_Name(CString str_path)	
	{//전체패스에서 파일이름만 구하기
		BOOL bWorking;
		CFileFind finder;
		CString fileName=_T("");
		
		bWorking=finder.FindFile(str_path);
		if(bWorking)
		{
		  bWorking = finder.FindNextFile();	
		  fileName=finder.GetFileName();		  		  
		} 
		finder.Close();
		return fileName;
	}

	static BOOL file_SelfCopy(CString sfileName,CString tfileName)
	{//자기 복사
	   
	   int fd;	    

	   #ifdef _UNICODE	
			 if((fd=_wopen(sfileName, _O_RDONLY|_O_BINARY ))==-1)  return FALSE;   
	   #else
			 if((fd=_open(sfileName, _O_RDONLY|_O_BINARY ))==-1)  return FALSE;   
	   #endif
	   
	   _lseek(fd, 0, SEEK_END);
		int len = _tell(fd);    //    파일 사이즈를 얻고..
		_lseek(fd, 0, SEEK_SET);

	   char* buffer = new char[len];
	   memset(buffer,0,len);

	   if(_read( fd, buffer, len )<=0 ) return FALSE;
     
	   _close( fd );

	   //write
	   #ifdef _UNICODE	
			FILE* file = _wfopen(tfileName,_T("wb+"));
	   #else
			FILE* file=fopen(tfileName,_T("wb+")); //바이너리 상태로 쓴다.
	   #endif
	   if(file==NULL) return FALSE;

	   fseek(file, 0L, SEEK_SET);
	   fwrite(buffer, len, 1, file);

	   fclose(file);
	   return TRUE;
	}


	static CString file_Read(CString filename)
	{
		if(file_Finder(filename)==FALSE) return _T("");

		CString data;
		FILE *fRead;
		
		#ifdef _UNICODE					
			fRead = _wfopen(filename,_T("rb+"));
			if(!fRead) return _T("");
			
			fseek(fRead, 0, SEEK_END);
			int fileSize = ftell(fRead);
			rewind(fRead);
    
			wchar_t * buf = new wchar_t[fileSize+1];
			memset(buf,0,fileSize+1);

            fread(buf, sizeof(wchar_t), fileSize, fRead);

	        data=(CString)buf;
			delete buf;
		#else			
			fRead=fopen(filename,_T("r+"));
			if(!fRead) return _T("");

			fseek(fRead, 0, SEEK_END);
			int fileSize = ftell(fRead);
			rewind(fRead);

			char * buf = new char[fileSize+1];
			memset(buf,0,fileSize+1);

			fread(buf, sizeof(char), fileSize, fRead);
			
			data=(CString)buf;			
			delete buf;
		#endif
		        		    
		fclose(fRead);

		return data;
	}

	static CString file_Reads(CString fileName)
	{//텍스트 복호화	
		if(file_Finder(fileName)==FALSE) return _T("");
				
		#ifdef _UNICODE	
			FILE* file = _wfopen(fileName,_T("r"));
		#else
			FILE *file = fopen(fileName,"r"); //바이너리 상태로 읽는다.
		#endif		
		if(file==NULL) return _T("");

		//파일사이즈를 알아낸다
		fseek(file, 0L, SEEK_END);//파일의 끝
		int len = ftell(file);
		fseek(file, 0L, SEEK_SET);//파일의 시작
		
		char* buf = new char[len];
		fread(buf, len, 1, file); //변화없음
 		
		fclose(file);

		return (CString)buf;
	}

	static CString file_ReadLast(CString fileName)
	{		
		if(file_Finder(fileName)==FALSE)  return _T("");

		CString data;
		
		FILE *fRead;	
  		
		#ifdef _UNICODE	
		    wchar_t buff[2048];
			fRead = _wfopen(fileName,_T("r+"));
		#else
			char buff[2048];
			fRead=fopen(fileName,_T("r+"));
		#endif	

		if(!fRead) return _T("");

		//fseek(fRead, 0, SEEK_END);	
		//int len = ftell(fRead);		
		//if(len>2048) len=2048;
		int len=2048;
		
		fseek(fRead, -(len), SEEK_END);	

		#ifdef _UNICODE	
		   fread((wchar_t*)buff,len,1,fRead);	
		#else
			fread((char*)buff,len,1,fRead);	
		#endif	

		CString str=(CString)buff;
		int count=GStr::str_Count(str,_T("\n"));		
		AfxExtractSubString(data,str,count-1,'\n');				

		fclose(fRead);
		return data;
	}

	static BOOL file_IsAlreadyOpen(CString filename)
	{
		 HFILE theFile = HFILE_ERROR;
		 DWORD lastErr  = NO_ERROR;

		 // Attempt to open the file exclusively.
		 #ifdef _UNICODE	
		 	theFile = _lopen((LPSTR)(LPCTSTR)filename, OF_READ | OF_SHARE_EXCLUSIVE);
		 #else
			theFile = _lopen(filename, OF_READ | OF_SHARE_EXCLUSIVE);
		 #endif

		 if (theFile == HFILE_ERROR){ // Save last error...       
		   lastErr = GetLastError();
		 } else {  // If the open was successful, close the file.        
			_lclose(theFile);
		 }

		 // Return TRUE if there is a sharing-violation.
		 return ((theFile == HFILE_ERROR) &&
				 (lastErr == ERROR_SHARING_VIOLATION));
	}
 
	static CString file_LoadProc(CString filename)  
	{ 
		if(file_Finder(filename)==FALSE) return _T("");

		DWORD dwBuffer; 
		char lpReadBuffer[_MAX_PATH];
    
		HANDLE hFile = CreateFile(filename, GENERIC_READ, 0, NULL, OPEN_EXISTING, 
											FILE_ATTRIBUTE_NORMAL, NULL); 
		if(hFile!=NULL) ReadFile(hFile, lpReadBuffer, _MAX_PATH, &dwBuffer, NULL); 

		CloseHandle(hFile); 	
		return (CString)lpReadBuffer;
	} 

	static void file_SaveProc(CString filename,CString data)  
	{ 
		DWORD dwBuffer; 	
		int slen=data.GetLength();

		#ifdef _UNICODE	
			wchar_t *lpReadBuffer=new wchar_t[slen];
		#else
			char *lpReadBuffer=new char[slen];
		#endif

		for(int i=0;i<slen;i++)
		{
			lpReadBuffer[i]=data[i];
		}
    
		HANDLE hFile = CreateFile(filename, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, 
											FILE_ATTRIBUTE_NORMAL, NULL); 
		if(hFile!=NULL) WriteFile(hFile, lpReadBuffer, slen, &dwBuffer, NULL); 

		CloseHandle(hFile); 	
	} 

	static BOOL file_IsOpenCheck(CString str_filepath)
	{
		if(::GetFileAttributes(str_filepath) & FILE_ATTRIBUTE_READONLY){				
			return FALSE;       //read-only  
		} else {
			return TRUE;        //write	 
		}
	}

	static int CALLBACK file_BrowseCallbackProc(HWND hwnd, UINT uMsg, LPARAM lParam, LPARAM lpData)
	{//get folder
		TCHAR szDir[MAX_PATH];
		switch(uMsg)
		{
		  case BFFM_INITIALIZED:
			if (lpData)
			{	
				SendMessage(hwnd, BFFM_SETSELECTION, TRUE, lpData);
			}
			break;
		  case BFFM_SELCHANGED:
		   if(SHGetPathFromIDList((LPITEMIDLIST) lParam ,szDir))
		   {
			  SendMessage(hwnd,BFFM_SETSTATUSTEXT,0,(LPARAM)szDir);
		   }
		   break;
		  default:
		   break;
		}         
		return 0;
	}
   
    //if(file_GetFolder(path,"Select folder.",this->m_hWnd,NULL, NULL)){
	static BOOL file_GetFolder(CString &strSelectedFolder,
		                       CString lpszTitle,HWND hwndOwner,
							   CString strRootFolder,CString strStartFolder)
	{		
		LPITEMIDLIST lpID;
		BROWSEINFOA bi;
		strSelectedFolder=_T("");

		#ifdef _UNICODE	
			wchar_t pszDisplayName[MAX_PATH];
		#else
			char pszDisplayName[MAX_PATH];
		#endif
		
		bi.hwndOwner = hwndOwner;
		if (strRootFolder.IsEmpty() || strRootFolder==_T("")) bi.pidlRoot = NULL;
		else
		{
		   LPITEMIDLIST  pIdl = NULL;
		   IShellFolder* pDesktopFolder;		   
		   OLECHAR       olePath[MAX_PATH];
		   ULONG         chEaten;
		   ULONG         dwAttributes;

		   #ifdef _UNICODE	
				wchar_t szPath[MAX_PATH];
				wcscpy(szPath, strRootFolder);
		   #else
				char szPath[MAX_PATH];
				strcpy(szPath, (LPCTSTR)strRootFolder);
		   #endif

		   if (SUCCEEDED(SHGetDesktopFolder(&pDesktopFolder)))
		   {
			   #ifdef _UNICODE	
					MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, (LPCSTR)szPath, -1, olePath, MAX_PATH);
			   #else
					MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, szPath, -1, olePath, MAX_PATH);
			   #endif

			   pDesktopFolder->ParseDisplayName(NULL, NULL, olePath, &chEaten, &pIdl, &dwAttributes);
			   pDesktopFolder->Release();
		   }
		   bi.pidlRoot = pIdl;
		}
		bi.pszDisplayName = (LPSTR)pszDisplayName;
		
		#ifdef _UNICODE	
			int isize = WideCharToMultiByte(CP_ACP, 0, lpszTitle, -1, 0, 0, 0, 0)+1;
			 
			char buf[100]={0};
			int iret=WideCharToMultiByte(CP_ACP,0,lpszTitle,lpszTitle.GetLength(),buf,isize,NULL,NULL);

			bi.lpszTitle=buf;
		#else
			bi.lpszTitle = (LPSTR)(LPCTSTR)lpszTitle;
		#endif

		bi.ulFlags = BIF_RETURNONLYFSDIRS | BIF_STATUSTEXT;
		bi.lpfn = file_BrowseCallbackProc;
		bi.lParam = (LPARAM)(LPCTSTR)strStartFolder;
		bi.iImage = NULL;

		lpID = SHBrowseForFolderA(&bi);
		if (lpID != NULL)
		{
			BOOL b = SHGetPathFromIDList(lpID, pszDisplayName);
			if (b == TRUE)
			{
				strSelectedFolder.Format(_T("%s"),pszDisplayName);
				return TRUE;
			}
		} else {
			strSelectedFolder.Empty();
		}
		return FALSE;
	}

	static CString file_SaveFile(CString sFilter,CString sExt)
	{//win_SaveFile("Data files(*.xml)|*.xml|All files(*.*)|*.*|",".xml");
		CFileDialog dlg(FALSE,sExt,NULL,OFN_OVERWRITEPROMPT,sFilter );
			
		if(IDOK == dlg.DoModal()) 
		{
		   return dlg.GetPathName(); 
		}	
		return _T("");
	}

	static CString file_OpenFile(CString sFilter,CString sExt)
	{
		CFileDialog dlg(TRUE,sExt,NULL,OFN_OVERWRITEPROMPT,sFilter );
			
		if(IDOK == dlg.DoModal()) 
		{
			return dlg.GetPathName();			
		}
		return _T("");
	}

	static void file_Filelist(CString str_path,CString str_Ext,CStringArray &Data,BOOL bRecursive=FALSE)	
	{//folder file list	
		CFileFind finder;
		str_path.TrimRight(_T("\\"));

		BOOL bWorking=finder.FindFile(str_path+_T("\\")+str_Ext);
		while (bWorking)
		{	  
		  bWorking = finder.FindNextFile();	
		  
		  if(finder.GetFileName()==_T(".") || finder.GetFileName()==_T("..")) continue;
		  if(finder.GetFileName()==_T("Thumbs.db")) continue;

		  CString filename=finder.GetFileName();
		  CString path=finder.GetFilePath();
		  DWORD filesize=(DWORD)finder.GetLength();
		 		 		  
		  CString str;
		  str.Format(_T("%d|%d|%s|%s|"),finder.IsDirectory(),filesize,filename,path);
		  Data.Add(str);			  
			
		  if(finder.IsDirectory())
		  {
			  if(bRecursive==TRUE)
			  {
				  path.TrimRight(_T("\\"));
				  file_Filelist(path,str_Ext,Data,bRecursive);	
				  Sleep(1);
			  }			  
		  }		 
		} 
		finder.Close();
	}

	static BOOL file_SystemTime(CString sTime, FILETIME &ftLastWriteTime)
	{//8
		CString syear=GStr::str_GetSplit(sTime,0,' ');
		CString stime=GStr::str_GetSplit(sTime,1,' ');

		SYSTEMTIME now;
		GetLocalTime(&now);

		#ifdef _UNICODE			   			
			now.wYear=_wtoi(GStr::str_GetSplit(syear,0,'-'));
			now.wMonth=_wtoi(GStr::str_GetSplit(syear,1,'-'));
			now.wDay=_wtoi(GStr::str_GetSplit(syear,2,'-'));

			now.wHour=_wtoi(GStr::str_GetSplit(stime,0,':'));
			now.wMinute=_wtoi(GStr::str_GetSplit(stime,1,':'));
			now.wSecond=_wtoi(GStr::str_GetSplit(stime,2,':'));	
		#else
		    now.wYear=atoi(GStr::str_GetSplit(syear,0,'-'));
			now.wMonth=atoi(GStr::str_GetSplit(syear,1,'-'));
			now.wDay=atoi(GStr::str_GetSplit(syear,2,'-'));

			now.wHour=atoi(GStr::str_GetSplit(stime,0,':'));
			now.wMinute=atoi(GStr::str_GetSplit(stime,1,':'));
			now.wSecond=atoi(GStr::str_GetSplit(stime,2,':'));	
		#endif
		
		return SystemTimeToFileTime(&now, &ftLastWriteTime);		
	}*/

} ;