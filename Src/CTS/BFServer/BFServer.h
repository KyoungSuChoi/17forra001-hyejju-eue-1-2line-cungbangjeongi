// BFServer.h : main header file for the BFServer DLL
//

#if !defined(AFX_BFServer_H__37DF22FD_3FCF_423F_A107_2563F6B2D3B4__INCLUDED_)
#define AFX_BFServer_H__37DF22FD_3FCF_423F_A107_2563F6B2D3B4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
#include "SBCLog.h"
#include "SBCCriticalSection.h"
#include "SBCStaticSyncParent.h"
#include "SBCSyncParent.h"
#include "SBCMemoryPool.h"
#include "SBCManagedBuf.h"
#include "SBCCircularQueue.h"

#include "SBCPacketBox.h"

#include "SBCNetObj.h"
#include "SBCIocp.h"
//========================================================================
#include "SBCNetIOCP.h"
#include "SBCRawServer.h"

#include "SBCObj.h"

#include "SBCServer.h"
//////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// CBFServerApp
// See BFServer.cpp for the implementation of this class
//

class CBFServerApp : public CWinApp
{
public:
	CBFServerApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBFServerApp)
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CBFServerApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BFServer_H__37DF22FD_3FCF_423F_A107_2563F6B2D3B4__INCLUDED_)
