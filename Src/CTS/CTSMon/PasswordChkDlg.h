#pragma once


// CPasswordChkDlg 대화 상자입니다.

class CPasswordChkDlg : public CDialog
{
	DECLARE_DYNAMIC(CPasswordChkDlg)

public:
	CPasswordChkDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CPasswordChkDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_PASSWORD_CHECK_DLG };

	BOOL PasswordCheck();
	BOOL m_bPasswordChagneChk;
	CFont font;

	BOOL	m_bDiffPassword;		//20210105ksj
	CString m_strDiffPassword;		//20210105ksj
	CString m_strTitle;		//20210105ksj

	void InitFont();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	virtual BOOL OnInitDialog();
	CString m_strPassword;
	CString m_strNewPassword;
	afx_msg void OnBnClickedBtnPasswordChange();
};
