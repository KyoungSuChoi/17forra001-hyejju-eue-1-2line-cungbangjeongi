#if !defined(AFX_CELLINFODLG_H__BA8B3436_7CED_4F99_91CE_ECEF606010B4__INCLUDED_)
#define AFX_CELLINFODLG_H__BA8B3436_7CED_4F99_91CE_ECEF606010B4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CellInfoDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCellInfoDlg dialog

class CCellInfoDlg : public CDialog
{
// Construction
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	CString m_strTitle;
	CCellInfoDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CCellInfoDlg();

// Dialog Data
	//{{AFX_DATA(CCellInfoDlg)
	enum { IDD = IDD_WORK_INFO_DIALOG };
	CString	m_strTrayNo;
	CString	m_strCellSerial;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCellInfoDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCellInfoDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CELLINFODLG_H__BA8B3436_7CED_4F99_91CE_ECEF606010B4__INCLUDED_)
