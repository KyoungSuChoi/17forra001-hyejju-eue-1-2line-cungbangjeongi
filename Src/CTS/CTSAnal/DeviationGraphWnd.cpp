// DeviationGraphWnd.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "DeviationGraphWnd.h"
#include <math.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDeviationGraphWnd

bool CDeviationGraphWnd::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CDeviationGraphWnd"), _T("TEXT_CDeviationGraphWnd_CNT"), _T("TEXT_CDeviationGraphWnd_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CDeviationGraphWnd_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CDeviationGraphWnd"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

CDeviationGraphWnd::CDeviationGraphWnd()
{
	LanguageinitMonConfig();

	m_hWndPE = NULL;
	m_ulDataCount = 10;
	m_pData = NULL;

	m_dDev = 0;
	m_dAvg = 0;
	m_dMin = 0;
	m_dMax = 0;
	m_ulMaxIndex = 0;
	m_ulMinIndex = 0;

	m_dYAxisMinMax[0][0] = 0.0f;
	m_dYAxisMinMax[0][1] = 1.0f;
	m_dYAxisMinMax[1][0] = 0.0f;
	m_dYAxisMinMax[1][1] = 100.0f;
	m_dYDataMinMax[0][0] = 0.0f;
	m_dYDataMinMax[0][1] = 0.0f;
	m_dYDataMinMax[1][0] = 0.0f;
	m_dYDataMinMax[1][1] = 0.0f;
	m_bUseAutoYScale = TRUE;

	m_dRangeMin = 0;
	m_dRangeMax = 0;
	m_bAutoRagne = TRUE;
	m_dDivision = 1.0;
	m_nAnnoCount = 0;

	m_dHighLimit = 0.0;
	m_dLowLimit = 0.0;

	memset(m_szVLAT, 0, sizeof(m_szVLAT));

}

CDeviationGraphWnd::~CDeviationGraphWnd()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}

	if(m_pData)
	{
		delete[] m_pData;
		m_pData = NULL;
	}
	PEdestroy(m_hWndPE);
}


BEGIN_MESSAGE_MAP(CDeviationGraphWnd, CStatic)
	//{{AFX_MSG_MAP(CDeviationGraphWnd)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDeviationGraphWnd message handlers



void CDeviationGraphWnd::PreSubclassWindow() 
{
	// TODO: Add your specialized code here and/or call the base class
	CRect trect, rect;

	GetClientRect(trect);
	rect.left = 0;
	rect.right = trect.Width();// - 2;
	rect.top = 0;
	rect.bottom = trect.Height();// - 2;

	m_hWndPE = PEcreate(PECONTROL_SGRAPH, WS_VISIBLE|WS_CHILD|WS_BORDER, &rect, m_hWnd, 10000);

	if(!m_hWndPE)
	{
		TRACE("Creation Error");
		return;
	}

	InitGraph();


//***********************
/*	int	nArray[8];
	int num=0;
	for(int i=0;i<m_SetsetNum;i++)
	{
		if(m_bShowArray[i])
		{
			nArray[num] = i;
			num++;
		}
	}
	SetV(PEP_naRANDOMSUBSETSTOGRAPH, nArray, num);
//***********************************************
*/
	
	PEreinitialize(m_hWndPE);
	PEresetimage(m_hWndPE, 0, 0);

	CStatic::PreSubclassWindow();
}

BOOL CDeviationGraphWnd::InitGraph()
{
	PEnset(m_hWndPE, PEP_nSUBSETS, 2);
	PEnset(m_hWndPE, PEP_nPOINTS, 10000);

	PEnset(m_hWndPE, PEP_bPREPAREIMAGES, TRUE);
	PEnset(m_hWndPE, PEP_nSCROLLINGSUBSETS , 1);		//동시에 보여지는 그래프 수를 나타 낸다. 
	PEnset(m_hWndPE, PEP_nDATAPRECISION, 1);			//테이블에서 소수점 아래 자리수 정하기
	PEnset(m_hWndPE, PEP_nGRAPHPLUSTABLE, PEGPT_BOTH);
	PEnset(m_hWndPE, PEP_nGRIDLINECONTROL, PEGLC_YAXIS);				//Grid Line 표시 여부
	PEnset(m_hWndPE, PEP_nALLOWZOOMING, PEAZ_HORZANDVERT);	//Zoom in/Out X,Y
	PEnset(m_hWndPE, PEP_bDATASHADOWS, FALSE);				//Data에 대한 그림자

	PEszset(m_hWndPE, PEP_szMAINTITLEFONT, "굴림");//omit
	PEszset(m_hWndPE, PEP_szSUBTITLEFONT, "굴림");//omit


//	PEnset(m_hWndPE, PEP_bSHOWXAXISANNOTATIONS, TRUE);					//Annotation을 보여 준다.
    PEnset(m_hWndPE, PEP_bSHOWANNOTATIONS, TRUE);					//Annotation을 사용 한다.
	PEnset(m_hWndPE, PEP_bALLOWANNOTATIONCONTROL, TRUE);			//사용자가 선택 가능 
//	char MGText[] = "##########";
//	PEszset(m_hWndPE, PEP_szRIGHTMARGIN, MGText);
	PEnset(m_hWndPE, PEP_nLINEANNOTATIONTEXTSIZE, 100);				//Annotation을 보여 준다.

	PEnset(m_hWndPE, PEP_bAUTOSCALEDATA, FALSE);					//This property controls whether the ProEssentials will automatically scale data that is very small or very large.
	

	int mas[2];
	mas[0] = 1;
	mas[1] = 1;
	int nYAxis = 2;
	PEvset(m_hWndPE, PEP_naMULTIAXESSUBSETS, mas, nYAxis);						//각 6개의 축에 보여줄 SubSet 갯수 지정(파일수/파일수/...) 
	PEvset(m_hWndPE, PEP_naOVERLAPMULTIAXES, &nYAxis, 1);						//1 Y축에 선택한 Item Y축을 한꺼번에 표시			

	PEnset(m_hWndPE, PEP_nALLOWZOOMING, TRUE);
//	PEnset(m_hWndPE, PEP_nGRAPHPLUSTABLE, PEGPT_GRAPH);				//그래프와 테이블 동시에 

	char szTitle[] = "Histogram\t정규 분포\t";//omit
	PEvset(m_hWndPE, PEP_szaSUBSETLABELS, szTitle, 2);			//4개의 그래프 라벨

	PEszset(m_hWndPE, PEP_szMAINTITLE, "분포도");//omit
	PEszset(m_hWndPE, PEP_szSUBTITLE, "");
   
    PEszset(m_hWndPE, PEP_szXAXISLABEL, "Value");

	PEnset (m_hWndPE, PEP_nWORKINGAXIS, 0);			// Set first y axis parameters
	PEnset (m_hWndPE, PEP_nPLOTTINGMETHOD, PEGPM_BAR);
	PEszset(m_hWndPE, PEP_szYAXISLABEL, "Cell 수");//omit

	PEnset (m_hWndPE, PEP_nWORKINGAXIS, 1);			// Set second y axis parameters
	PEnset (m_hWndPE, PEP_nPLOTTINGMETHOD, PEGPM_SPLINE);
	PEszset(m_hWndPE, PEP_szYAXISLABEL, "정규분포");//omit
	
	COLORREF color[2] = {RGB(64, 192, 0), RGB(0, 128, 255) };
	PEvset(m_hWndPE, PEP_dwaSUBSETCOLORS, color, 2);
	PEnset(m_hWndPE, PEP_dwDESKCOLOR, RGB(213,213,213));
//	PEnset(m_hWndPE, PEP_dwGRAPHBACKCOLOR, RGB(0,0,0));
//	PEnset(m_hWndPE, PEP_dwGRAPHFORECOLOR, RGB(255,255,255));
	PEnset(m_hWndPE, PEP_dwGRAPHBACKCOLOR, RGB(255,255,255));
	PEnset(m_hWndPE, PEP_dwGRAPHFORECOLOR, RGB(0,0,0));

	PEreinitialize(m_hWndPE);
	PEresetimage(m_hWndPE, 0, 0);
	return TRUE;  // return TRUE unless you set the focus to a control

}

BOOL CDeviationGraphWnd::SetData(int nSize, double *data)
{
	if(nSize <=0)	return FALSE;
	
	if(m_pData != NULL)
	{
		delete[] m_pData;
		m_pData = NULL;
	}
	m_pData = new double[nSize];
	ASSERT(m_pData);

	if(data != NULL)
	{
		memcpy(m_pData, data, sizeof(double)*nSize);
	}
	m_ulDataCount = nSize;

	return TRUE;
}

BOOL CDeviationGraphWnd::ProcessData()
{
	if(m_ulDataCount ==0 )	return FALSE;

	m_dAvg = 0.0;
	m_dDev = 0.0;
	m_ulMinIndex = 0;
	m_ulMaxIndex = 0;

	int i = 0;
	
	//평균과 표준 편차 계산 
	for(i = 0; i < m_ulDataCount; i++)
	{
//		TRACE("Data %d: %f\n", i, m_pData[i]);
		m_dAvg += m_pData[i];
		m_dDev += (m_pData[i] * m_pData[i]);

		if( i == 0)
		{
			m_dMin = m_pData[i];
			m_dMax = m_pData[i];
		}
		else
		{
			if(m_dMin > m_pData[i])										//Data Min	
			{	
				m_dMin = m_pData[i];
				m_ulMinIndex = i;
			}

			if(m_dMax < m_pData[i])										//Data Max
			{
				m_dMax = m_pData[i];
				m_ulMaxIndex = i;
			}
		}
	}
	m_dAvg /= m_ulDataCount;											//Mean
	m_dDev = sqrt(fabs(m_dDev/m_ulDataCount - m_dAvg * m_dAvg));		//Deviation

	//각 구간별 수량 검사
	int count1 = 0;
	float  fVal = 0.0f;
	count1 = int((m_dMax-m_dMin)/m_dDivision)+1;

	int *pCountData = new int[count1];
	ZeroMemory(pCountData, sizeof(int)*count1);
	for(i = 0; i < m_ulDataCount; i++)
	{
		for(int j = 0; j < count1; j++)
		{
			if(m_dMin + m_dDivision*j <=  m_pData[i] && m_pData[i] < m_dMin+m_dDivision*(j+1))
			{
				pCountData[j]++;
				break;
			}
		}
	}	

	//정규 분포 그래프 Data 산출  
	double dVal = 0.0;
	int count2 = 0;
	double dIncrease;
	double dBoundHigh, dBoundLow;
	dBoundHigh = m_dMax < m_dHighLimit ? m_dHighLimit : m_dMax;
	dBoundLow = m_dMin > m_dLowLimit ? m_dLowLimit: m_dMin;

//	dIncrease = m_dDivision < 0.1f ? m_dDivision : 0.1f;	//최소 0.1 간격으로 계산 
	dIncrease = (dBoundHigh - dBoundLow)/1000.0;		//무조건 1000 등분해서 계산 
	if(dIncrease < 0)	return FALSE;

	count2 = (dBoundHigh - dBoundLow)/dIncrease;

	PEnset(m_hWndPE, PEP_nPOINTS, count1 < count2 ? count2 : count1);

	//Histogram Display
	for(i=0; i < count1; i++)
	{
		fVal = (float)pCountData[i];
		PEvsetcellEx(m_hWndPE, PEP_faYDATA, 0, i, &fVal);
		fVal = (float)((0.5f+(float)i)*m_dDivision+m_dMin);
		PEvsetcellEx(m_hWndPE, PEP_faXDATA, 0, i, &fVal);
	}
	delete[] pCountData;
	pCountData = NULL;

	//정규 분포 곡선 산출 
	count2 = 0;
	for(double  data = dBoundLow;  data<= dBoundHigh; data += dIncrease)
	{
		if(m_dDev == 0.0 || m_dMax == m_dMin)	//분산이 0인 경우 
		{
			dVal = 0.0;
		}
		else
		{
			dVal = exp(-(data-m_dAvg)*(data-m_dAvg) / (2 * m_dDev * m_dDev)) / (m_dDev * sqrt(3.1415926f*2.0f));
		}
		
//		if(dVal <=0)	dVal = 0.0;

		if(count2 == 0)
		{
			m_dYDataMinMax[1][0] = dVal;									//Deviation Min
			m_dYDataMinMax[1][1] = dVal;									//Deviation Max
		}
		else
		{
			if(m_dYDataMinMax[1][0] > dVal)		
				m_dYDataMinMax[1][0] = dVal;
			if(m_dYDataMinMax[1][1] < dVal)	
				m_dYDataMinMax[1][1] = dVal;
		}
		fVal = (float)dVal;
		PEvsetcellEx(m_hWndPE, PEP_faYDATA, 1, count2, &fVal);
		fVal = (float)data;
		PEvsetcellEx(m_hWndPE, PEP_faXDATA, 1, count2, &fVal);
//		TRACE("Data( %f,  %f)\n", data, dVal);
		count2++;
	}
	TRACE("Min Val %f , Max Val %f\n", m_dYDataMinMax[1][0], m_dYDataMinMax[1][1]); 

	PEnset(m_hWndPE, PEP_nWORKINGAXIS, 1);
	PEnset(m_hWndPE, PEP_nMANUALSCALECONTROLY, PEMSC_MINMAX);
	dVal = 0.0;
	PEvset(m_hWndPE, PEP_fMANUALMINY, &dVal, 1);
	dVal = m_dYDataMinMax[1][1]+0.001;
	PEvset(m_hWndPE, PEP_fMANUALMAXY, &dVal, 1);
		
	char szBuff[64];
	sprintf(szBuff, "Mean : %.3f, STD D : %.3f", m_dAvg, m_dDev);
	PEszset(m_hWndPE, PEP_szSUBTITLE, szBuff);
	
	PEnset(m_hWndPE, PEP_nMANUALSCALECONTROLX, PEMSC_MINMAX);
	dVal = dBoundLow;
	PEvset(m_hWndPE,  PEP_fMANUALMINX , &dVal, 1);
	dVal = dBoundHigh;
	PEvset(m_hWndPE, PEP_fMANUALMAXX, &dVal, 1);
	
	PEreinitialize(m_hWndPE);
	PEresetimage(m_hWndPE, 0, 0);

	return TRUE;
}

BOOL CDeviationGraphWnd::SetDataRange(double dMin, double dMax)
{
	ASSERT(dMin < dMax);

	m_dRangeMin = dMin;
	m_dRangeMax = dMax;
	return TRUE;
}

BOOL CDeviationGraphWnd::SetDivision(double dDivision)
{
	if(dDivision > 0 )
	{
		m_dDivision = dDivision;
	}
	else
		return FALSE;
	
	return TRUE;
}

BOOL CDeviationGraphWnd::SetTitle(CString main, CString subTitle)
{
	PEszset(m_hWndPE, PEP_szMAINTITLE, (LPSTR)(LPCTSTR)main);
	PEszset(m_hWndPE, PEP_szSUBTITLE, (LPSTR)(LPCTSTR)subTitle);
	return TRUE;
}

void CDeviationGraphWnd::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	// TODO: Add your message handler code here
	::InvalidateRect(m_hWndPE, NULL, FALSE);
	// Do not call CStatic::OnPaint() for painting messages
}

BOOL CDeviationGraphWnd::AddXAnnotation(double dPoint, CString strString, COLORREF color, int nLineType)
{
	char szBuff[MAX_ADD_ANNO_NO * 55];			//Max Anno Size is 48 + Seperator
	memset(szBuff, 0, sizeof(szBuff));

	m_dVLA[m_nAnnoCount] = dPoint;				//Point
	m_nVLAT[m_nAnnoCount] = nLineType;			//Line Style
	m_lVLAC[m_nAnnoCount] = color;				//Line Color	
	sprintf(m_szVLAT[m_nAnnoCount], "|t%s\t", strString );

	m_nAnnoCount++;
	for(int i = 0; i<m_nAnnoCount; i++)
	{
		strcat(szBuff, m_szVLAT[i]);
	}

	PEvset(m_hWndPE, PEP_faVERTLINEANNOTATION, m_dVLA, m_nAnnoCount);
	PEvset(m_hWndPE, PEP_szaVERTLINEANNOTATIONTEXT, szBuff, m_nAnnoCount);
	PEvset(m_hWndPE, PEP_naVERTLINEANNOTATIONTYPE, m_nVLAT, m_nAnnoCount);
	PEvset(m_hWndPE, PEP_dwaVERTLINEANNOTATIONCOLOR, m_lVLAC, m_nAnnoCount);
	return TRUE;
}

BOOL CDeviationGraphWnd::SetLimitVal(double dHigh, double dLow)
{
	m_dHighLimit = dHigh;
	m_dLowLimit = dLow;
	return TRUE;
}

void CDeviationGraphWnd::PrintData()
{
	PElaunchprintdialog(m_hWndPE, TRUE, NULL);
}

void CDeviationGraphWnd::SaveFile()
{
	PElaunchexport(m_hWndPE);
//	PElaunchtextexport(m_hWndPE, TRUE, "aa.csv");
}

void CDeviationGraphWnd::SetXAxisLabel(CString strName)
{
    PEszset(m_hWndPE, PEP_szXAXISLABEL, (LPSTR)(LPCTSTR)strName);
}
