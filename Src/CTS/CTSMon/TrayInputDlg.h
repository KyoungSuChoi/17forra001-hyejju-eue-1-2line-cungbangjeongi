#if !defined(AFX_TRAYINPUTDLG_H__78E39440_5187_436C_A1B9_14C0CC45C87A__INCLUDED_)
#define AFX_TRAYINPUTDLG_H__78E39440_5187_436C_A1B9_14C0CC45C87A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TrayInputDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTrayInputDlg dialog
#include "MyGridWnd.h"
#include "CTSMonDoc.h"


class CTrayInputDlg : public CDialog
{
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();
	virtual ~CTrayInputDlg();

// Construction
public:
	void DrawTopGrid();
	void Fun_SetBarCodeToForcusCell(CString strCell);
	void Fun_SetClearCellNum(int nRow,int nCol);
	void InitCellGrid();
	int m_nSelJigNo;
	int m_nSelModuleID;
	void SetUnitInput(BOOL bUnit = TRUE);
	void SetTitle(CString str);
	BOOL SearchTrayID(CString strID);
	BOOL SearchLocation(CString strID, int &nModuleID, int &nJigID);
	void SendTrayNoReadMsg(CString strTray, int nModuleID, int nJigNo = 1);
	UINT m_nTimer;
	void SetHideDelayTime(int nMiliSec);
	BOOL ReadUnitNo(CString strData);
	BOOL ReadTrayNo(CString strData);
	// ljb 2009925 S	/////////////////
	BOOL ReadCellNo(CString strData);
	// ljb 2009925 E /////////////////
	CTrayInputDlg(CCTSMonDoc *pDoc,CWnd* pParent = NULL);   // standard constructor
public:
	int Fun_GetCellPosition(ROWCOL nRow, ROWCOL nCol);
	BOOL Fun_SetCellDataToCTray();
	int m_nModuleID, m_nJigID;
	CWordArray m_warryChannelCnt;
	BOOL Fun_GetTrayAllData(int nModuleID, int nJigID);
//	char	*m_pTopColorFlag;
//	STR_TOP_CONFIG		*m_pConfig;
	int m_nTotalChannelNum;
	int m_nTopGridRowCount;
	int m_nTopGridColCount;	

// Dialog Data
	//{{AFX_DATA(CTrayInputDlg)
	enum { IDD = IDD_TRAYNO_DLG };
	CComboBox	m_ctrlJigListCombo;
	CLabel	m_ctrlTitle;
	CString	m_strTrayNo;
	CString	m_strUnitNo;
	CString	m_strCellBCR;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTrayInputDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CMyGridWnd	m_wndCellGrid;
	CString m_strTitleText;
	BOOL m_bModeLocation;
	// ljb 2009925 S /////////////////
	CCTSMonDoc* m_pDoc;
	BOOL m_bReadCellStart;		//Cell Read ���� ���� Flag
//	BOOL m_bReadCellEnd;		//Last Cell read Flag 
	BOOL m_bUseCellBarCode;
	// ljb 2009925 E /////////////////
	CWnd *m_ParentWnd;

	// Generated message map functions
	//{{AFX_MSG(CTrayInputDlg)
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT nIDEvent);
	virtual void OnOK();
	afx_msg void OnSelchangeJigListCombo();
	virtual BOOL OnInitDialog();
	afx_msg void OnChangeEdit1();
	virtual void OnCancel();
	afx_msg void OnBtnInput();
	afx_msg void OnBtnAllClear();
	afx_msg void OnBtnCellClear();
	//}}AFX_MSG
	afx_msg LRESULT OnBcrscaned(UINT wParam,LONG lParam);

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TRAYINPUTDLG_H__78E39440_5187_436C_A1B9_14C0CC45C87A__INCLUDED_)
