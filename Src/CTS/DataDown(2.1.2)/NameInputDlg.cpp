// NameInputDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DataDown.h"
#include "NameInputDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CNameInputDlg dialog


CNameInputDlg::CNameInputDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CNameInputDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CNameInputDlg)
	m_strName = _T("");
	LanguageinitMonConfig();
	//}}AFX_DATA_INIT
}
CNameInputDlg::~CNameInputDlg()
{
	if( TEXT_LANG != NULL )					
	{					
		delete[] TEXT_LANG;					
		TEXT_LANG = NULL;					
	}
}

void CNameInputDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNameInputDlg)
	DDX_Text(pDX, IDC_NAME_EDIT, m_strName);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CNameInputDlg, CDialog)
	//{{AFX_MSG_MAP(CNameInputDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNameInputDlg message handlers

BOOL CNameInputDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	GetDlgItem(IDC_TITLE_STATIC)->SetWindowText(m_strTitle);
	UpdateData(FALSE);

	GetDlgItem(IDC_NAME_EDIT)->SetFocus();
	((CEdit *)GetDlgItem(IDC_NAME_EDIT))->SetSel(0,-1);

	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
bool CNameInputDlg::LanguageinitMonConfig() 					
{					
	int i=0;				
	int nTextCnt = 0;				
	TEXT_LANG = NULL;				

	CString strErr = _T("");				
	CString strTemp = _T("");				
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CNameInputDlg"), _T("TEXT_CNameInputDlg_CNT"), _T("TEXT_CNameInputDlg_CNT"));				

	nTextCnt = atoi(strTemp);				

	if( nTextCnt > 0 )				
	{				
		TEXT_LANG = new CString[nTextCnt];			

		for( i=0; i<nTextCnt; i++ )			
		{			
			strTemp.Format("TEXT_CNameInputDlg_%d", i);		
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CNameInputDlg"), strTemp, strTemp, g_nLanguage);		

			if( TEXT_LANG[i] == strTemp )		
			{		
				if( strErr.IsEmpty() )	
				{	
					strErr = "Languge error " + strTemp; 
				}	
				else	
				{	
					strErr += "," + strTemp;
				}	
			}		
		}			

		if( !strErr.IsEmpty() )			
		{			
			AfxMessageBox(strErr);		
			return false;		
		}			
	}				

	return true;				
}				
void CNameInputDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);

	if(m_strName.FindOneOf("\\/?:\"*<>")>= 0)
	{
		GetDlgItem(IDC_NAME_EDIT)->SetFocus();
		((CEdit *)GetDlgItem(IDC_NAME_EDIT))->SetSel(0,-1);
		AfxMessageBox(TEXT_LANG[0]); //폴더명으로 사용할 수 없는 문자가 있습니다. //100112
		return;
	}

	if(m_strName.GetLength() >= _MAX_PATH)
	{
		GetDlgItem(IDC_NAME_EDIT)->SetFocus();
		((CEdit *)GetDlgItem(IDC_NAME_EDIT))->SetSel(0,-1);
		AfxMessageBox(TEXT_LANG[1]); //폴더명의 길이가 너무 깁니다. 100113
		return;
	}

	CDialog::OnOK();
}
