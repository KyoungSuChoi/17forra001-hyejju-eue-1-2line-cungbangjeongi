#ifndef _MYDEF
#define _MYDEF

#include "parsingdata.h"

#define MAX_MUTI_AXIS		6
#define MAX_SEL_AXIS		3
#define MAX_XAXIS_POINT		129600					//30 Day Data Display (10 Sec Interval)
#define MAX_CHANNEL_NUM		8
#define MAX_ADD_ANNO_NO		50
#define MAX_H_LINE_NO		1000

#define CAPACITY_FILE		2
#define GENERAL_FILE		1

#define X_TIME				0
#define X_DATAPOINT			1
#define X_VOLTAGE			2
#define X_CURRENT			3
#define X_CAPACITY			4
#define X_IMPEDANCE			5
#define X_TEMPERATURE		6
#define X_PRESSURE			7

typedef struct tag_AxisYSetting {
	COLORREF	axisColor;
	int			nLineStyles;
	int			nYItemUnit;
	BOOL		bItemSel;
	float		fAxisMin;
	float		fAxisMax;
	char		szSubsetName[16];
} STR_Y_AXIS_SET;

typedef struct tag_SubSetSetting {
	COLORREF	color;
	int			nLineType;
} STR_SUBSET_SET;


typedef struct tag_ResultData {
	int moduleID;
	int channelID;
	long procID;
	char procName[512];
	char fname1[512];
	char fname2[512];
	char date[128];
	char flag[2];
} STR_RESULT_DATA;

typedef struct tag_CapacityData
{
	int state;
	int type;
	int mode;
	unsigned long cycleNum;
	unsigned long totalTime;
	unsigned long stepNo;
	unsigned long stepTime;
	double Voltage;
	double Capacity;
} STR_CAP_DATA;

typedef struct tag_GeneralData
{
	int state;
	int type;
	int mode;
	unsigned long cycleNum;
	unsigned long totalTime;
	unsigned long stepNo;
	unsigned long stepTime;
	double Voltage;
	double Current;
	double Capacity;
	double Impedance;
	double Temperature;
	double Pressure;
} STR_GEN_DATA;

typedef struct tag_ReadData {
	LONG	index;
	BYTE	state;
	BYTE	type;
	BYTE	mode;
	BYTE	stepNo;
	LONG	totalTime;
	LONG	stepTime;
}	STR_READ_DATA;

typedef struct tag_File_Information {
	CString		strFileName;
	FILE		*fp;
	int			nDataSize;
	int			nSubSetSize;
	float		fSecPerData;
	STR_RESULT_DATA	stFileHeader;
	int			*pnDataIndexArray;
//	STR_READ_DATA	*pDataHeader;
} STR_FILE_INFO;

typedef struct tag_File_InformationA {
	CString		strFileName;
	FILE		*fp;
	int			nDataSize;
	int			nSubSetSize;
	float		fSecPerData;
	int			nTotalTime;
	STR_RESULT_DATA	stFileHeader;
	int			*pnDataIndexArray;
	//CDataFormat m_pData;
} STR_FILE_INFO_A;

typedef struct tag_Graph_Information {
	int		nMaxData;
	int		DataPerSubset;
	BYTE	SecPerData;
}	STR_GRAPH_INFO;

typedef struct tag_Line_Horizential_Annotation {
	float	fVal;
	COLORREF	colorLine;
	BYTE	lineStyle;
	char	szComment[128];
} STR_H_ANNO;

typedef struct tag_Graph_Annotation {
	BOOL	bAutoGrid;
	BOOL	bUserAddGrid;
	BYTE	select;
	BYTE	devided;
	float	fVal[3];
	COLORREF	colorLine;
	BYTE	lineStyle;
	BOOL	bAdded;
	int nLineNo;
	STR_H_ANNO	lineInfo[MAX_ADD_ANNO_NO];
}	STR_GRID_SET;

#define GetReadingBuffSize(x)	((x) == GENERAL_FILE ? sizeof(STR_GEN_DATA): sizeof(STR_CAP_DATA))

#endif