// AccuracySetDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ctsmon.h"
#include "AccuracySetDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAccuracySetDlg dialog


CAccuracySetDlg::CAccuracySetDlg(CCaliPoint* pCalPoint, CWnd* pParent /*=NULL*/)
	: CDialog(CAccuracySetDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAccuracySetDlg)
	m_fIRange = 0.0f;
	m_fVRange = 0.0f;
	m_fIRange1 = 0.0f;
	m_fVRange1 = 0.0f;
	//}}AFX_DATA_INIT

	m_pCalPoint = pCalPoint;
	ASSERT(pCalPoint);
	LanguageinitMonConfig();
}


CAccuracySetDlg::~CAccuracySetDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CAccuracySetDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CAccuracySetDlg"), _T("TEXT_CAccuracySetDlg_CNT"), _T("TEXT_CAccuracySetDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CAccuracySetDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CAccuracySetDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

void CAccuracySetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAccuracySetDlg)
	DDX_Control(pDX, IDC_I_RANGE_LIST, m_IRangeList);
	DDX_Text(pDX, IDC_I_RANGE_EDIT, m_fIRange);
	DDX_Text(pDX, IDC_V_RANGE_EDIT, m_fVRange);
	DDX_Control(pDX, IDC_V_RANGE_LIST, m_VRangeList);
	DDX_Text(pDX, IDC_I_RANGE_EDIT2, m_fIRange1);
	DDX_Text(pDX, IDC_V_RANGE_EDIT2, m_fVRange1);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAccuracySetDlg, CDialog)
	//{{AFX_MSG_MAP(CAccuracySetDlg)
	ON_LBN_SELCHANGE(IDC_I_RANGE_LIST, OnSelchangeIRangeList)
	ON_LBN_SELCHANGE(IDC_V_RANGE_LIST, OnSelchangeVRangeList)
	ON_EN_CHANGE(IDC_V_RANGE_EDIT, OnChangeVRangeEdit)
	ON_EN_CHANGE(IDC_V_RANGE_EDIT2, OnChangeVRangeEdit2)
	ON_EN_CHANGE(IDC_I_RANGE_EDIT, OnChangeIRangeEdit)
	ON_EN_CHANGE(IDC_I_RANGE_EDIT2, OnChangeIRangeEdit2)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAccuracySetDlg message handlers

/**
@author  
@brief   다이얼로그 폼을 생성할 때 처음으로 어떤 값을 가지고 시작할지를 정해줄 때 사용한다. 
@bug    
@code   
@date    2013-06-13 
@endcode 
@param   
@remark  OnselchangeVRangeList() 전압의 범위의 값을 컨트롤에서 가져오는 부분.
		 OnselchangeIRangeList() 전류의 법위의 값을 컨트롤에서 가져오는 부분.
@return  TRUE  
@see  
@todo    
*/
BOOL CAccuracySetDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CString strMsg;	
	int  i = 0;

	for(i =0; i<m_pCalPoint->GetVRangeCnt(); i++)
	{
		strMsg.Format("Range #%d", i+1);
		m_VRangeList.AddString(strMsg);
	}
	for(i =0; i<m_pCalPoint->GetIRangeCnt(); i++)
	{
		strMsg.Format("Range #%d", i+1);
		m_IRangeList.AddString(strMsg);
	}
	m_VRangeList.SetCurSel(0);
	OnSelchangeVRangeList();
	
	m_IRangeList.SetCurSel(0);
	OnSelchangeIRangeList();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

/**
@author  
@brief 전압의 범위의 값을 컨트롤에서 가져오는 부분.
@bug    
@code   
@date   2013 - 06 -13
@endcode 
@param   
@remark  
@return  없음
@see  
@todo    
*/
void CAccuracySetDlg::OnSelchangeIRangeList() 
{
	// TODO: Add your control notification handler code here

	
	int a = m_IRangeList.GetCurSel();
	if(a >=0 && a<CAL_MAX_CURRENT_RANGE)
	{
		m_fIRange = m_pCalPoint->m_dIDAAccuracy[a];
		m_fIRange1 = m_pCalPoint->m_dIADAccuracy[a];
		
		CString str;
		m_IRangeList.GetText(a, str);
		GetDlgItem(IDC_I_RANGE_STATIC)->SetWindowText(TEXT_LANG[0]+str);//"전류 "
	}
	UpdateData(FALSE);
}

/**
@author  
@brief 전류의 법위의 값을 컨트롤에서 가져오는 부분.
@bug    
@code   
@date   2013 - 06 -13
@endcode 
@param   
@remark  
@return  없음
@see  
@todo    
*/
void CAccuracySetDlg::OnSelchangeVRangeList() 
{
	// TODO: Add your control notification handler code here
	int a = m_VRangeList.GetCurSel();
	if(a >=0 && a<CAL_MAX_VOLTAGE_RANGE)
	{
		m_fVRange = m_pCalPoint->m_dVDAAccuracy[a];
		m_fVRange1 = m_pCalPoint->m_dVADAccuracy[a];
	
		CString str;
		m_IRangeList.GetText(a, str);
		GetDlgItem(IDC_V_RANGE_STATIC)->SetWindowText(TEXT_LANG[1]+str);//"전압 "
	}
	UpdateData(FALSE);	
}

/**
@author  
@brief 전류의 법위의 컨트롤 값을 변경해주는 부분 
@bug    
@code   
@date   2013 - 06 -13
@endcode 
@param   
@remark  
@return  없음
@see  
@todo    
*/
void CAccuracySetDlg::OnChangeVRangeEdit() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	int a = m_VRangeList.GetCurSel();
	if(a >=0 && a<CAL_MAX_VOLTAGE_RANGE)
	{
		m_pCalPoint->m_dVDAAccuracy[a]	= m_fVRange;
	}
}
/**
@author  
@brief 전류의 법위의 컨트롤 값을 변경해주는 부분 2
@bug    
@code   
@date   2013 - 06 -13
@endcode 
@param   
@remark  
@return  없음
@see  
@todo    
*/
void CAccuracySetDlg::OnChangeVRangeEdit2() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	int a = m_VRangeList.GetCurSel();
	if(a >=0 && a<CAL_MAX_VOLTAGE_RANGE)
	{
		m_pCalPoint->m_dVADAccuracy[a] = m_fVRange1;
	}	
}

/**
@author  
@brief 전압의 법위의 컨트롤 값을 변경해주는 부분 
@bug    
@code   
@date   2013 - 06 -13
@endcode 
@param   
@remark  
@return  없음
@see  
@todo    
*/
void CAccuracySetDlg::OnChangeIRangeEdit() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	int a = m_IRangeList.GetCurSel();
	if(a >=0 && a<CAL_MAX_CURRENT_RANGE)
	{
		m_pCalPoint->m_dIDAAccuracy[a] = m_fIRange;
	}
	
}

/**
@author  
@brief 전압의 법위의 컨트롤 값을 변경해주는 부분 2
@bug    
@code   
@date   2013 - 06 -13
@endcode 
@param   
@remark  
@return  없음
@see  
@todo    
*/
void CAccuracySetDlg::OnChangeIRangeEdit2() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	int a = m_IRangeList.GetCurSel();
	if(a >=0 && a<CAL_MAX_CURRENT_RANGE)
	{
		m_pCalPoint->m_dIADAccuracy[a] = m_fIRange1;
	}
}

/**
@author  
@brief 데이터 값을 SavePointData에 보내는 부분
@bug    
@code   
@date   2013 - 06 -13
@endcode 
@param   
@remark  
@return  없음
@see  
@todo    
*/
void CAccuracySetDlg::OnOK() 
{
	// TODO: Add extra validation here
	m_pCalPoint->SavePointData(FALSE);

	CDialog::OnOK();
}
