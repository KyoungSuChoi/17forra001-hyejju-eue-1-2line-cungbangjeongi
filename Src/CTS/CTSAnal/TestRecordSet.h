#if !defined(AFX_TESTRECORDSET_H__BD55FC8A_9787_4D74_A163_8D890668CE09__INCLUDED_)
#define AFX_TESTRECORDSET_H__BD55FC8A_9787_4D74_A163_8D890668CE09__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TestRecordSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTestRecordSet recordset

class CTestRecordSet : public CRecordset
{
public:
	CTestRecordSet(CDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CTestRecordSet)

// Field/Param Data
	//{{AFX_FIELD(CTestRecordSet, CRecordset)
	long	m_TestIndex;
	long	m_ProcedureID;
	CString	m_TestSerialNo;
	long	m_StepNo;
	long	m_StepType;
	CTime	m_DateTime;
//	CString	m_TestResultFileName;
	float	m_Average;
	float	m_MaxVal;
	long	m_MaxChIndex;
	float	m_MinVal;
	long	m_MinChIndex;
	float	m_STDD;
	long	m_NormalNo;
//	float	m_ErrorRate;
	long	m_ProgressID;
	long	m_TotalChannel;
	long	m_DataType;
//	long	m_CellNo;
	long	m_Temperature;
	long	m_Gas;
	long	m_ProcType;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestRecordSet)
	public:
	virtual CString GetDefaultConnect();    // Default connection string
	virtual CString GetDefaultSQL();    // Default SQL for Recordset
	virtual void DoFieldExchange(CFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTRECORDSET_H__BD55FC8A_9787_4D74_A163_8D890668CE09__INCLUDED_)
