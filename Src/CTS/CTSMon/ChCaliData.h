// ChCaliData.h: interface for the CChCaliData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CHCALIDATA_H__9F7DDAEF_1928_40D2_83F2_86298988C4DD__INCLUDED_)
#define AFX_CHCALIDATA_H__9F7DDAEF_1928_40D2_83F2_86298988C4DD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "CaliPoint.h"

#define CAL_DATA_EMPTY	0
#define CAL_DATA_SAVED	1
#define CAL_DATA_MODIFY	2
#define CAL_DATA_ERROR	3	


typedef struct s_Calibraton_Data 
{
	double	dAdData;
	double	dMeterData;
}	CAL_DATA;

class CChCaliData : public CCaliPoint 
{
public:
	void SetHWChNo(int nBoardNo, int nChNo)		{	m_nBoardNo = nBoardNo;  m_nChannelNo =nChNo ;	}
	void GetHWChNo(int &nBoardNo, int &nChNo)	{	nBoardNo = m_nBoardNo;  nChNo = m_nChannelNo;	}
	void SetState(int nState = CAL_DATA_MODIFY)	{	 m_nEdited = nState;	}
	BOOL GetState()	{	return m_nEdited;	}
	BOOL WriteDataToFile(FILE *fp);
	BOOL ReadDataFromFile(FILE *fp);
	//Multi Range FTN
	void GetVCalData(WORD wRange, WORD wPoint, double &dAdData, double &dMeterData);
	void GetVCheckData(WORD wRange, WORD wPoint, double &dAdData, double &dMeterData);
	void GetICalData(WORD wRange, WORD wPoint, double &dAdData, double &dMeterData);
	void GetICheckData(WORD wRange, WORD wPoint, double &dAdData, double &dMeterData);

	//1 Range FTN
	void GetVCalData(WORD wPoint, double &dAdData, double &dMeterData);
	void GetVCheckData(WORD wPoint, double &dAdData, double &dMeterData);
	void GetICalData(WORD wPoint, double &dAdData, double &dMeterData);
	void GetICheckData(WORD wPoint, double &dAdData, double &dMeterData);

	//Multi Range FTN
	void SetVCalData(WORD wRange, WORD wPoint, double dAdData, double dMeterData);
	void SetVCheckData(WORD wRange, WORD wPoint, double dAdData, double dMeterData);
	void SetICalData(WORD wRange, WORD wPoint, double dAdData, double dMeterData);
	void SetICheckData(WORD wRange, WORD wPoint, double dAdData, double dMeterData);

	CChCaliData();
	virtual ~CChCaliData();

protected:
	int			m_nBoardNo;		// 1Base
	int			m_nChannelNo;	// 1Base
	int			m_nEdited;
	CAL_DATA	m_VCalData[CAL_MAX_VOLTAGE_RANGE][CAL_MAX_CALIB_SET_POINT];
	CAL_DATA	m_VCheckData[CAL_MAX_VOLTAGE_RANGE][CAL_MAX_CALIB_CHECK_POINT];
	CAL_DATA	m_ICalData[CAL_MAX_CURRENT_RANGE][CAL_MAX_CALIB_SET_POINT];
	CAL_DATA	m_ICheckData[CAL_MAX_CURRENT_RANGE][CAL_MAX_CALIB_CHECK_POINT];
};

#endif // !defined(AFX_CHCALIDATA_H__9F7DDAEF_1928_40D2_83F2_86298988C4DD__INCLUDED_)
