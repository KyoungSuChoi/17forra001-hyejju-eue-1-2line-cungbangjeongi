// CommandLogRecordSet.cpp : implementation file
//

#include "stdafx.h"
#include "CTSMon.h"
#include "CommandLogRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCommandLogRecordSet

IMPLEMENT_DYNAMIC(CCommandLogRecordSet, CDaoRecordset)

CCommandLogRecordSet::CCommandLogRecordSet(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CCommandLogRecordSet)
	m_Index = 0;
	m_ProcedureID = 0;
	m_Action = _T("");
	m_DateTime = (DATE)0;
	m_nFields = 4;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString CCommandLogRecordSet::GetDefaultDBName()
{
	return _T(GetLogDataBaseName());
}

CString CCommandLogRecordSet::GetDefaultSQL()
{
	return _T("[ActionLog]");
}

void CCommandLogRecordSet::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CCommandLogRecordSet)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Long(pFX, _T("[Index]"), m_Index);
	DFX_Long(pFX, _T("[ProcedureID]"), m_ProcedureID);
	DFX_Text(pFX, _T("[Action]"), m_Action);
	DFX_DateTime(pFX, _T("[DateTime]"), m_DateTime);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CCommandLogRecordSet diagnostics

#ifdef _DEBUG
void CCommandLogRecordSet::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CCommandLogRecordSet::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
