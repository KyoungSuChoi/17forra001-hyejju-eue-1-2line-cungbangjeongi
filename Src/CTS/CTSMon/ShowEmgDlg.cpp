// ShowEmgDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ctsmon.h"
#include "ShowEmgDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ShowEmgDlg dialog


ShowEmgDlg::ShowEmgDlg(CWnd* pParent /*=NULL*/)
	: CDialog(ShowEmgDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(ShowEmgDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_nModuleID = 0;
	LanguageinitMonConfig();
}


ShowEmgDlg::~ShowEmgDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool ShowEmgDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_ShowEmgDlg"), _T("TEXT_ShowEmgDlg_CNT"), _T("TEXT_ShowEmgDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_ShowEmgDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_ShowEmgDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}


void ShowEmgDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ShowEmgDlg)
	DDX_Control(pDX, IDC_STATIC_EMG, m_ctrlEmgLabel);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_STATIC_EMG_MSG, m_ctrlEmgMsg);
}


BEGIN_MESSAGE_MAP(ShowEmgDlg, CDialog)
	//{{AFX_MSG_MAP(ShowEmgDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, &ShowEmgDlg::OnBnClickedOk)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ShowEmgDlg message handlers
BOOL ShowEmgDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_ctrlEmgLabel.SetFontSize(24)			
		.SetTextColor(RGB(50, 100, 200))
		.SetBkColor(RGB(234,28,120))
		.SetGradientColor(RGB(255,255,255))
		.SetFontBold(TRUE)
		.SetGradient(TRUE)
		.SetFontName(TEXT_LANG[0])//"HY헤드라인M"
		.FlashBackground(TRUE)						 
		.SetSunken(TRUE);
		
	m_ctrlEmgMsg.SetFontSize(24)			
		.SetTextColor(RGB(50, 100, 200))
		.SetBkColor(RGB(234,28,120))
		.SetGradientColor(RGB(255,255,255))
		.SetFontBold(TRUE)
		.SetGradient(TRUE)
		.SetFontName(TEXT_LANG[0])		//"HY헤드라인M"
		.SetSunken(TRUE);
		
	this->SetWindowText(strMsg);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void ShowEmgDlg::SetEmgData(CString strModuleName, CString strCodeMsg, long lValue)
{	
	CenterWindow();
	
	strEmgValue.Format(TEXT_LANG[1], strModuleName);	//"%s에서 비상 상태가 발생하였습니다."
	m_strEmgMsg.Format("%s", strCodeMsg);
	
	DrawEmgInfo();
}

void ShowEmgDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnOK();
}

void ShowEmgDlg::DrawEmgInfo()
{
	m_ctrlEmgLabel.SetText(strEmgValue);
	m_ctrlEmgMsg.SetText(m_strEmgMsg);
}

void ShowEmgDlg::SetModuleID(int nModuleID)
{
	m_nModuleID = nModuleID;
}

int ShowEmgDlg::GetModuleID()
{
	return m_nModuleID;
}