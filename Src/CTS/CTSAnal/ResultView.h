#if !defined(AFX_RESULTVIEW_H__FA59683D_CEB8_4BD0_9181_0B17544940C8__INCLUDED_)
#define AFX_RESULTVIEW_H__FA59683D_CEB8_4BD0_9181_0B17544940C8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ResultView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CResultView form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

#include "MyGridWnd.h"
//#include "PowerFormationDoc.h"
#include "FileSaveDlg.h"
#include "ResultFileLoadDlg.h"
//#include "FormResultFile.h"
//#include "../src/Utility/FormResultFile.h"

#include "IconCombo.h"
#include "./ShellDirTree/ShellTreeCtrl.h"
#include "./ShellDirTree/ProgressFX.h"
#include "./ShellDirTree/HourglassFX.h"

// #define TYPE_CELL_LIST		0
#define TYPE_CHANNEL_LIST	0
#define TYPE_TRAY_LIST		1

#define DEVICE_FORMATION	2
#define DEVICE_DCIR			1

typedef struct tagCellListData {
	int stepIndex;
	int nType;
	int nDataType;
	CString strName;
} Display_Data_List;

#define IDM_FIRST_SHELLMENUID	20000
#define IDM_LAST_SHELLMENUID	(IDM_FIRST_SHELLMENUID+1000)

class CResultView : public CFormView
{
protected:
	CResultView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CResultView)

// Form Data
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	//{{AFX_DATA(CResultView)
	enum { IDD = IDD_RESULT_VIEW };
	CXPButton	m_btnRestoreResultData;
	CXPButton	m_btnMesUpdate;
	CXPButton	m_btnTestCon;
	CXPButton	m_btnExcelSave;
	CXPButton	m_btnProfile;
	CXPButton	m_btnReload;
	CXPButton	m_btnSearch;
	CXPButton	m_btnCp;
	CXPButton	m_btnLotAll;
	CComboBox	m_ctrlDisplayMode;
	CLabel	m_ctrlRate;
	CLabel	m_ctrlErrorRate;
	CLabel	m_ctrlNormalRate;
	CLabel	m_ctrlOperator;
	CLabel	m_ctrlTypeSel;
	CLabel	m_ctrlTestedModule;
	CLabel	m_ctrlTestName;
	CLabel	m_ctrlTestedDateTime;
	CLabel	m_ctrlTestLotNo;
	CLabel	m_ctrlFileName;
	CIconCombo	m_ctrlStepSelCombo;
	//}}AFX_DATA
	CProgressFX<CHourglassFX<CShellTreeCtrl> > m_wndDataTree;

// Attributes
public:
	CCTSAnalDoc* GetDocument();
	bool OnResultFileOpen();
	void OnRltFileOpen();
	CShellContextMenu m_shellMenu;


// Operations
public:
	/*
	BOOL CreateTrayResultFile(int nTrayIndex);
	BOOL SaveResultData(int nModuleID, int nTrayIndex, int nStepIndex, LPVOID lpData, STR_STEP_RESULT &rStepResult);
	*/

	int Fun_RestoreResultdata();
	BYTE GetUserSelect(int nChCode, int nGradeCode);
	
	STR_SAVE_CH_DATA ConvertChSaveData( int nModuleID, int nChIndex/*Tray Base Index*/, LPEP_CH_DATA pChData, CString strCapacityFormula = _T(""), CString strDCRFormula = _T("") );
	CString GetRemoteLocation(int nGroupNo, CString strTestSerial, CString strIp);
	
	int RunGraphAnalyzer();
	
	BOOL FindSameLotFile(CString strFolder, CString strFilerLot, CStringArray &aFile, BOOL bIncludeSubFolder = TRUE);
	BOOL SearchSameLotFile(CStringArray &aFileList);
	BOOL DownLoadProfile();
	void ViewCpCpk();
	static CMenu* FindMenuItemByText(CMenu* pMenu, LPCTSTR pszText, UINT &nIndex, BOOL bRecursive = TRUE);
	bool LoadDataTree();
	int GetStepTypeImageIndex(int nType);
	BOOL m_bUseTempSensor;
	void UpdateDataUnit();
	BOOL m_bDispCapSum;
	BOOL DisplayFile(CString strFile);
	int  ReadGroupFile(char * szFileName);
	int m_nCurrentStepIndex;

	bool m_bDblclkDataTree;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CResultView)
	public:
	virtual void OnInitialUpdate();
	virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo = NULL);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
protected:

	CImageList	*m_pImageList;
	void DisplayMinMaxGrid(int nStepIndex);

//	int m_nInputCellCount;
//	int m_nFailCount;
//	int m_nNormalCount;
	
	CArray<Display_Data_List, Display_Data_List> m_dataList;

	virtual ~CResultView();
	void InitMinMaxGrid();
	void DisplayTestCondition();
	STR_TOP_CONFIG	*m_pConfig;
	CFormResultFile	 m_resultFile;
//	CResultFileLoadDlg *m_pLoadDlg;

	CString m_strSelFolder;
	CString m_strSelFileName;

	CMyGridWnd m_wndStepListGrid;
	CMyGridWnd m_wndChannelListGrid;
	CMyGridWnd m_wndMinMaxGrid;
	CMyGridWnd m_wndCellListGrid;
	CMyGridWnd m_wndTrayChGrid;
	CMyGridWnd *m_pCurrentGrid;

//	EP_FILE_HEADER		m_fileHaeder;
//	RESULT_FILE_HEADER	m_resultFileHeader;
//	EP_MD_SYSTEM_DATA	m_sysData;
//	STR_FILE_EXT_SPACE	m_extData;
//	STR_CONDITION		*m_pConditionMain;
//	CPtrArray			m_stepData;		//Array of STR_STEP_RESULT

	void InitCellListGrid();
	void InitChannelListGrid();
	void InitStepListGrid();
	void InitTrayTypeGrid();
//	BOOL ClearResult();
//	int  GetStepSize();
	BOOL SaveCapEfficencyFile(CFileSaveDlg *pDlg);
	BOOL SaveChFileSave(CFileSaveDlg *pDlg);
	BOOL SaveStepFileSave(CFileSaveDlg *pDlg);
	BOOL SaveExelFile(CString strFileName);
	BOOL DisplayData();
	BOOL DisplayTrayChData(int nItem = 0);

	BOOL DisplayCellListAll();
	BOOL DisplayCellList();
	BOOL DisplayChannelData(int nStepIndex = 0);
	void SetDisplayMode(BYTE mode);
	BYTE m_nDisplayMode;
	BYTE m_DeviceMode;
	void OnAllFileData();


//	STR_STEP_RESULT * GetStepData(UINT nStepIndex);

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CResultView)
	afx_msg void OnTestConditonView();
	afx_msg void OnDestroy();
	afx_msg void OnSelchangeCombo1();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnUpdateFileSaveAs(CCmdUI* pCmdUI);
	afx_msg void OnFileSaveAs();
	afx_msg void OnReload();
	afx_msg void OnFormatSetting();
	afx_msg void OnSelchangeDisplayMode();
	afx_msg void OnFileSave();
	afx_msg void OnDataSort();
	afx_msg void OnSysparamShow();
	afx_msg void OnUpdateDataSort(CCmdUI* pCmdUI);
	afx_msg void OnEditCopy();
	afx_msg void OnSelectAll();
	afx_msg void OnDblclkDataTree(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDataPath();
	afx_msg void OnFieldSet();
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnCpButton();
	afx_msg void OnButtonSearch();
	afx_msg void OnButtonLotAll();
	afx_msg void OnSensorButton();
	afx_msg void OnGradeColorSet();
	afx_msg void OnMesUpdateButton();
	afx_msg void OnFileRawView();
	afx_msg void OnUpdateFileRawView(CCmdUI* pCmdUI);
	afx_msg void OnRestoreResultdata();
	//}}AFX_MSG
	afx_msg LRESULT OnGridDoubleClick(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMovedCurrentCell(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRButtonClickedRowCol(WPARAM wParam, LPARAM lParam);
	afx_msg void OnShellCommand(UINT nID);

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedViewSelect1Btn();
	afx_msg void OnBnClickedViewSelect2Btn();
};

#ifndef _DEBUG  // debug version in PowerFormationView.cpp
inline CCTSAnalDoc* CResultView::GetDocument()
   { return (CCTSAnalDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RESULTVIEW_H__FA59683D_CEB8_4BD0_9181_0B17544940C8__INCLUDED_)
