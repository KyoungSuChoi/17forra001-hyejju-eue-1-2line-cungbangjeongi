// EditorSetDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSEditor.h"
#include "EditorSetDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEditorSetDlg dialog


CEditorSetDlg::CEditorSetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CEditorSetDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CEditorSetDlg)
	m_bVHigh = FALSE;
	m_bVLow = FALSE;
	m_bIHigh = FALSE;
	m_bILow = FALSE;
	m_bCHigh = FALSE;
	m_bCLow = FALSE;
	m_fCLowVal = 0.0f;
	m_fCHighVal = 0.0f;
	m_fILowVal = 0.0f;
	m_fIHighVal = 0.0f;
	m_fVLowVal = 0.0f;
	m_fVHighVal = 0.0f;
	m_bUseLogin = FALSE;
	m_lVRefLow = 0;
	m_lVRefHigh = 0;
	//}}AFX_DATA_INIT
}


void CEditorSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEditorSetDlg)
	DDX_Control(pDX, IDC_I_UNIT_COMBO, m_ctrlIUnit);
	DDX_Control(pDX, IDC_C_UNIT_COMBO, m_ctrlCUnit);
	DDX_Control(pDX, IDC_V_UNIT_COMBO, m_ctrlVUnit);
	DDX_Check(pDX, IDC_V_H_CHECK, m_bVHigh);
	DDX_Check(pDX, IDC_V_L_CHECK, m_bVLow);
	DDX_Check(pDX, IDC_I_H_CHECK, m_bIHigh);
	DDX_Check(pDX, IDC_I_L_CHECK, m_bILow);
	DDX_Check(pDX, IDC_C_H_CHECK, m_bCHigh);
	DDX_Check(pDX, IDC_C_L_CHECK, m_bCLow);
	DDX_Text(pDX, IDC_C_LOW_EDIT, m_fCLowVal);
	DDX_Text(pDX, IDC_C_HIGH_EDIT, m_fCHighVal);
	DDX_Text(pDX, IDC_I_LOW_EDIT, m_fILowVal);
	DDX_Text(pDX, IDC_I_HIGH_EDIT, m_fIHighVal);
	DDX_Text(pDX, IDC_V_LOW_EDIT, m_fVLowVal);
	DDX_Text(pDX, IDC_V_HIGH_EDIT, m_fVHighVal);
	DDX_Check(pDX, IDC_USE_LOGIN_CHECK, m_bUseLogin);
	DDX_Text(pDX, IDC_CC_V_LOW_EDIT, m_lVRefLow);
	DDX_Text(pDX, IDC_CC_V_HIGH_EDIT, m_lVRefHigh);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CEditorSetDlg, CDialog)
	//{{AFX_MSG_MAP(CEditorSetDlg)
	ON_BN_CLICKED(IDOK, OnOk)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEditorSetDlg message handlers

BOOL CEditorSetDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	
	m_bVHigh = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Auto_V_High", FALSE);
	m_bVLow = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Auto_V_Low", FALSE);
	m_bIHigh = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Auto_I_High", FALSE);
	m_bILow = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Auto_I_Low", FALSE);
	m_bCHigh = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Auto_C_High", FALSE);
	m_bCLow = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Auto_C_Low", FALSE);
	m_fVLowVal = atof(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION , "Auto_V_Low_Val", "0"));
	m_fVHighVal = atof(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION , "Auto_V_High_Val", "0"));
	m_fILowVal = atof(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION , "Auto_I_Low_Val", "0"));
	m_fIHighVal = atof(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION , "Auto_I_High_Val", "0"));
	m_fCLowVal = atof(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION , "Auto_C_Low_Val", "0"));
	m_fCHighVal = atof(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION , "Auto_C_High_Val", "0"));

	m_lVRefHigh = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "CCHighRef", 0);
	m_lVRefLow = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "CCLowRef", 0);

	m_bUseLogin = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "UseLogin", FALSE);
	int nCurrentUnitMode = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "Crt Unit Mode", 0);

	//Ni-MH Cell은 Step eidt에서 CC모드만 지원하고 전체적으로 사용할 전압 범위를 설정한다. 
/*	if(nCurrentUnitMode)
	{
		GetDlgItem(IDC_I_HIGH_LIMIT_UNIT_STATIC)->SetWindowText("uA");
		GetDlgItem(IDC_I_LOW_LIMIT_UNIT_STATIC)->SetWindowText("uA");
#ifdef _EDLC_CELL_
		GetDlgItem(IDC_C_HIGH_UNIT_STATIC)->SetWindowText("mF");
		GetDlgItem(IDC_C_LOW_UNIT_STATIC)->SetWindowText("mF");
#else
		GetDlgItem(IDC_C_HIGH_LIMIT_UNIT_STATIC)->SetWindowText("uAh");
		GetDlgItem(IDC_C_LOW_LIMIT_UNIT_STATIC)->SetWindowText("uAh");	
#endif
	}
	else
	{
		GetDlgItem(IDC_I_HIGH_LIMIT_UNIT_STATIC)->SetWindowText("mA");
		GetDlgItem(IDC_I_LOW_LIMIT_UNIT_STATIC)->SetWindowText("mA");
#ifdef _EDLC_CELL_
		GetDlgItem(IDC_C_HIGH_LIMIT_UNIT_STATIC)->SetWindowText("F");
		GetDlgItem(IDC_C_LOW_LIMIT_UNIT_STATIC)->SetWindowText("F");	
#else
		GetDlgItem(IDC_C_HIGH_LIMIT_UNIT_STATIC)->SetWindowText("mAh");
		GetDlgItem(IDC_C_LOW_LIMIT_UNIT_STATIC)->SetWindowText("mAh");	
#endif
	}
*/
	
	
	CString strTemp;
	char szBuff[64], szUnit[32], szDecimalPoint[32];
	//Current Unit
	sprintf(szBuff, AfxGetApp()->GetProfileString(EDITOR_REG_SECTION, "I Unit", "mA 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	GetDlgItem(IDC_I_HIGH_LIMIT_UNIT_STATIC)->SetWindowText(szUnit);
	GetDlgItem(IDC_I_LOW_LIMIT_UNIT_STATIC)->SetWindowText(szUnit);

	//Capacity Unit
	sprintf(szBuff, AfxGetApp()->GetProfileString(EDITOR_REG_SECTION, "C Unit", "mAh 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	GetDlgItem(IDC_C_HIGH_LIMIT_UNIT_STATIC)->SetWindowText(szUnit);
	GetDlgItem(IDC_C_LOW_LIMIT_UNIT_STATIC)->SetWindowText(szUnit);	

/*
	if(nCurrentUnitMode)
	{
		m_ctrlIUnit.AddString("uA");
		m_ctrlIUnit.AddString("mA");
#ifdef _EDLC_CELL_
		m_ctrlCUnit.AddString("mF");
		m_ctrlCUnit.AddString("F");
#else
		m_ctrlCUnit.AddString("uAh");
		m_ctrlCUnit.AddString("mAh");
#endif
	}
	else
	{
		m_ctrlIUnit.AddString("mA");
		m_ctrlIUnit.AddString("A");
#ifdef _EDLC_CELL_
		m_ctrlCUnit.AddString("F");
		m_ctrlCUnit.AddString("kF");
#else
		m_ctrlCUnit.AddString("mAh");
		m_ctrlCUnit.AddString("Ah");
#endif
	}

	m_ctrlVUnit.AddString("mV");
	m_ctrlVUnit.AddString("V");

//	UnitUpdate();
*/
	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CEditorSetDlg::OnOk() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

	CString strMsg;
/*	if(m_lVRefLow >= m_lVRefHigh)
	{
		GetDlgItem(IDC_CC_V_LOW_EDIT)->SetFocus();
		AfxMessageBox("전압사용범위의 하한값이 상한값보다 큽니다.");
		return;
	}

	long lMaxV = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Max Voltage", 5000);
	if(m_lVRefHigh > lMaxV)
	{
		strMsg.Format("전압사용범위의 상한값이 정격전압(%dmV)보다 높습니다.", lMaxV);
		GetDlgItem(IDC_CC_V_HIGH_EDIT)->SetFocus();
		AfxMessageBox(strMsg);
		return;
	}
	if(m_lVRefLow < 0)
	{
		strMsg.Format("전압사용범위의 하한값이 0 보다 낮습니다.");
		GetDlgItem(IDC_CC_V_LOW_EDIT)->SetFocus();
		AfxMessageBox(strMsg);
		return;
	}
*/	

	CString strTemp;
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION , "Auto_V_High", m_bVHigh);
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION , "Auto_V_Low", m_bVLow);
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION , "Auto_I_High", m_bIHigh);
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION , "Auto_I_Low", m_bILow);
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION , "Auto_C_High", m_bCHigh);
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION , "Auto_C_Low", m_bCLow);

	strTemp.Format("%.3f", m_fCLowVal);
	AfxGetApp()->WriteProfileString(EDITOR_REG_SECTION , "Auto_C_Low_Val", strTemp);
	strTemp.Format("%.3f", m_fCHighVal);
	AfxGetApp()->WriteProfileString(EDITOR_REG_SECTION , "Auto_C_High_Val", strTemp);
	strTemp.Format("%.3f", m_fILowVal);
	AfxGetApp()->WriteProfileString(EDITOR_REG_SECTION , "Auto_I_Low_Val", strTemp);
	strTemp.Format("%.3f", m_fIHighVal);
	AfxGetApp()->WriteProfileString(EDITOR_REG_SECTION , "Auto_I_High_Val", strTemp);
	
	//convert to mA to V
	strTemp.Format("%.3f", m_fVLowVal);
	AfxGetApp()->WriteProfileString(EDITOR_REG_SECTION , "Auto_V_Low_Val", strTemp);
	strTemp.Format("%.3f", m_fVHighVal);
	AfxGetApp()->WriteProfileString(EDITOR_REG_SECTION , "Auto_V_High_Val", strTemp);

	
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION , "UseLogin", m_bUseLogin);

	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION , "CCLowRef", m_lVRefLow);
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION , "CCHighRef", m_lVRefHigh);
	
	CDialog::OnOK();
}

void CEditorSetDlg::UnitUpdate()
{
	CString str;
	str = "mA";
	int index = m_ctrlIUnit.GetCurSel();
	if(index != LB_ERR)
	{
		m_ctrlIUnit.GetLBText(index, str);
	}
	GetDlgItem(IDC_I_HIGH_LIMIT_UNIT_STATIC)->SetWindowText(str);
	GetDlgItem(IDC_I_LOW_LIMIT_UNIT_STATIC)->SetWindowText(str);

	str = "mAh";
	index = m_ctrlCUnit.GetCurSel();
	if(index != LB_ERR)
	{
		m_ctrlCUnit.GetLBText(index, str);
	}
	GetDlgItem(IDC_C_HIGH_LIMIT_UNIT_STATIC)->SetWindowText(str);
	GetDlgItem(IDC_C_LOW_LIMIT_UNIT_STATIC)->SetWindowText(str);	

	str = "mV";
	index = m_ctrlVUnit.GetCurSel();
	if(index != LB_ERR)
	{
		m_ctrlVUnit.GetLBText(index, str);
	}
	GetDlgItem(IDC_V_HIGH_LIMIT_UNIT_STATIC)->SetWindowText(str);
	GetDlgItem(IDC_V_LOW_LIMIT_UNIT_STATIC)->SetWindowText(str);	
	GetDlgItem(IDC_V_HIGH_REF_UNIT_STATIC)->SetWindowText(str);
	GetDlgItem(IDC_V_LOW_REF_UNIT_STATIC)->SetWindowText(str);		
}
