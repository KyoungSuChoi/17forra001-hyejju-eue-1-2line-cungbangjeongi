// MyGridWnd.cpp: implementation of the CMyGridWnd class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "NewGridWnd.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMyGridWnd::CMyGridWnd()
{

}

CMyGridWnd::~CMyGridWnd()
{

}

BEGIN_MESSAGE_MAP(CMyGridWnd, CGXGridWnd)
	//{{AFX_MSG_MAP(CMyGridWnd)
	ON_WM_KILLFOCUS()
	ON_WM_SETFOCUS()
	ON_WM_CONTEXTMENU()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CMyGridWnd::OnInitialUpdate()
{
	CGXGridWnd::OnInitialUpdate();

//	CGXGridParam *pParam = GetParam();

	BOOL bLock = LockUpdate();
	EnableIntelliMouse();
	EnableCellTips();

		GetParam()->SetSmartResize(TRUE);

		// Enable the new grid line drawing
		GetParam()->SetNewGridLineMode(TRUE);
			// use dotted grid lines
		GetParam()->SetGridLineStyle(PS_DOT);
			// and don't draw grid lines between header cells
		ChangeColHeaderStyle(CGXStyle().SetBorders(gxBorderAll, CGXPen().SetStyle(PS_NULL)));
		ChangeRowHeaderStyle(CGXStyle().SetBorders(gxBorderAll, CGXPen().SetStyle(PS_NULL)));

		StandardStyle()
		.SetInterior(RGB(255, 255, 220));

		SetScrollBarMode(SB_VERT, gxnEnabled | gxnEnhanced);	//Only Vertial Scroll Bar

/*
//	pParam->EnableUndo(FALSE);
//	pParam->EnableMoveRows(FALSE);
//	pParam->EnableMoveCols(FALSE);
	pParam->SetSyncCurrentCell(TRUE);
//	pParam->GetProperties()->SetDisplayHorzLines(FALSE);
//	pParam->GetProperties()->SetDisplayVertLines(FALSE);
	pParam->GetProperties()->SetMarkRowHeader(FALSE);
	pParam->GetProperties()->SetMarkColHeader(FALSE);
	pParam->EnableTrackColWidth(FALSE);	//no Resize Col Width
	pParam->SetActivateCellFlags(GX_CAFOCUS_DBLCLICKONCELL);
	pParam->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL &  ~GX_SELMULTIPLE);

	ChangeStandardStyle(StandardStyle()
		.SetControl(GX_IDS_CTRL_EDIT)
		.SetVerticalAlignment(DT_VCENTER)
		.SetHorizontalAlignment(DT_CENTER)
//		.SetDraw3dFrame(gxFrameRaised)
		.SetFont(
			CGXFont()
				.SetSize(9)
				.SetFaceName("����")
				.SetBold(FALSE)
		)

	);
	
	ChangeRowHeaderStyle(CGXStyle()
		.SetInterior(RGB(0,0,128))
		.SetTextColor(RGB(255,255,255))
		.SetBorders(
			gxBorderAll, 
			CGXPen()
				.SetStyle(PS_NULL)
		)
		.SetFont(
			CGXFont()
				.SetSize(10)
				.SetFaceName("����")
				.SetBold(FALSE)
		)
	);
	
	ChangeColHeaderStyle(CGXStyle()
		.SetInterior(RGB(0,0,128))
		.SetTextColor(RGB(255,255,255))
		.SetBorders(
			gxBorderAll, 
			CGXPen()
				.SetStyle(PS_NULL)
		)
		.SetFont(
			CGXFont()
				.SetSize(10)
				.SetFaceName("����")
				.SetBold(FALSE)
		)
	);

	EnableHints(FALSE);
	SetScrollBarMode(SB_VERT, gxnEnabled | gxnEnhanced);	//Only Vertial Scroll Bar
	SetDefaultRowHeight(20);
//	SetDefaultColWidth(80);
*/
/*	m_bRefreshOnSetCurrentCell = TRUE;
	if(m_bRowSelection)
	{
		pParam->SetActivateCellFlags(FALSE);
		pParam->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL &  ~GX_SELMULTIPLE);
		pParam->SetSpecialMode(GX_MODELBOX_SS);
		pParam->SetHideCurrentCell(GX_HIDE_ALLWAYS);
	}
*/	LockUpdate(bLock);
}

BOOL CMyGridWnd::OnLButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)
{
//	WPARAM value = ((WPARAM)(pt.x) << 16) | ((WPARAM)(pt.y));
	WPARAM value = ((WPARAM)(nRow) << 16) | ((WPARAM)(nCol));
	CGXGridWnd::OnLButtonClickedRowCol(nRow, nCol, nFlags, pt);
	CWnd *pWnd = GetParent();
	if(pWnd)
		pWnd->PostMessage(WM_GRID_CLICK, (WPARAM) value, (LPARAM) this);
	return TRUE;
}

BOOL CMyGridWnd::OnRButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)
{
	WPARAM value = ((WPARAM)(nRow) << 16) | ((WPARAM)(nCol));
//	WPARAM value = ((WPARAM)(pt.x) << 16) | ((WPARAM)(pt.y));
	CGXGridWnd::OnRButtonClickedRowCol(nRow, nCol, nFlags, pt);
	CWnd *pWnd = GetParent();
	if(pWnd)
		pWnd->PostMessage(WM_GRID_RIGHT_CLICK, (WPARAM) value, (LPARAM) this);
	return TRUE;
}

void CMyGridWnd::OnKillFocus(CWnd* pNewWnd) 
{
	CGXGridWnd::OnKillFocus(pNewWnd);
	
	GetParent()->PostMessage(WM_GRID_KILLFOCUS, 0, (LPARAM)this);	
}

void CMyGridWnd::OnSetFocus(CWnd* pOldWnd) 
{
	CGXGridWnd::OnSetFocus(pOldWnd);
	
	GetParent()->PostMessage(WM_GRID_SETFOCUS, 0, (LPARAM)this);	
}

void CMyGridWnd::OnMovedCurrentCell(ROWCOL nRow, ROWCOL nCol)
{
	CGXGridWnd::OnMovedCurrentCell(nRow, nCol);
	if(nRow && nCol)
	{
//		if((nRow != m_nOutlineRow) || (nCol != m_nOutlineCol) )
//		{
			BOOL bLock = LockUpdate();
			if(bLock==FALSE)
			{	
				WPARAM value = ((WPARAM)(nRow) << 16) | ((WPARAM)(nCol));
				GetParent()->PostMessage(WM_GRID_MOVECELL, (WPARAM) value, (LPARAM) this);
			}
			LockUpdate(bLock);
//		}
	}
}

BOOL CMyGridWnd::OnSelDragRowsMove(ROWCOL nFirstRow, ROWCOL nLastRow, ROWCOL nDestRow)
{
   return nDestRow > GetFrozenRows();
}

BOOL CMyGridWnd::OnSelDragRowsDrop(ROWCOL nFirstRow, ROWCOL nLastRow, ROWCOL nDestRow)
{
	CGXGridWnd::OnSelDragRowsDrop(nFirstRow, nLastRow, nDestRow);
	WPARAM value = 0;
	value = ((WPARAM)(nFirstRow) << 20) | ((WPARAM)(nLastRow) << 10) | ((WPARAM)(nDestRow));

	CWnd *pWnd = GetParent();
	if(pWnd)
		pWnd->PostMessage(WM_GRID_ROW_DRAG_DROP, (WPARAM) value, (LPARAM) this);
	return TRUE;
}

BOOL CMyGridWnd::OnLeftCell(ROWCOL nRow, ROWCOL nCol, ROWCOL nNewRow, ROWCOL nNewCol)
{
	if(nRow && nCol)
	{
		WPARAM value = ((WPARAM)(nRow) << 16) | ((WPARAM)(nCol));
		GetParent()->PostMessage(WM_GRID_MOVEROW, (WPARAM)value, (LPARAM) this);
	}
	return CGXGridWnd::OnLeftCell(nRow, nCol, nNewRow, nNewCol);
}
BOOL CMyGridWnd::OnStartEditing(ROWCOL nRow, ROWCOL nCol)
{
	if(nRow && nCol)
	{
		WPARAM value = ((WPARAM)(nRow) << 16) | ((WPARAM)(nCol));
		GetParent()->PostMessage(WM_GRID_EDIT_START, (WPARAM)value, (LPARAM) this);
	}

	return CGXGridWnd::OnStartEditing(nRow, nCol);
}

BOOL CMyGridWnd::OnEndEditing(ROWCOL nRow, ROWCOL nCol)
{
	if(nRow && nCol)
	{
		WPARAM value = ((WPARAM)(nRow) << 16) | ((WPARAM)(nCol));
		GetParent()->PostMessage(WM_GRID_EDIT_END, (WPARAM)value, (LPARAM) this);
	}
	return CGXGridWnd::OnEndEditing(nRow, nCol);
}

BOOL CMyGridWnd::GetStyleRowCol(ROWCOL nRow, ROWCOL nCol, CGXStyle& style, GXModifyType mt, int nType)
{
	BOOL bRet = CGXGridWnd::GetStyleRowCol(nRow, nCol, style, mt, nType);
	if(IsPrinting())
		return bRet;

	if(style.GetIncludeEnabled())
	{
		if(!style.GetEnabled())
			style.SetInterior(RGB(192,192,192));
		else
			style.SetInterior(RGB(255,255,255));
	}
	
	if(nRow > 0 && nCol > 0 && IsCurrentCell(nRow, nCol))
	{
// 		style.SetInterior(RGB(255, 0, 0));
		style.SetDraw3dFrame(gxFrameInset);
	}
	else
	{
// 		style.SetInterior(RGB(0, 0, 255));
	}


/*	if (nType == 0 && GetInvertStateRowCol(nRow, nCol, GetParam()->GetRangeList()))
 	{	
 		style.SetInterior(m_SelColor);
		style.SetDraw3dFrame(gxFrameInset);
 		bRet = TRUE;
 	}
	else if(nRow > 0 && nCol > 0 && IsCurrentCell(nRow, nCol))
	{
 		style.SetInterior(m_SelColor);
		style.SetDraw3dFrame(gxFrameInset);
		bRet = TRUE;
	}
	else if(nRow > 0 && nCol > 0)
	{
			if(m_bCustomColor)
			{
				if(m_CustomColorRange.IsCellInRange(nRow, nCol))
				{
					ROWCOL y = nRow - m_CustomColorRange.top;
					ROWCOL x = nCol - m_CustomColorRange.left;
					ROWCOL width = m_CustomColorRange.GetWidth();
					int index = m_pCustomColorFlag[y* width + x];
					if(index<0 || index > 15) index = 15;
					COLORREF bkColor = m_ColorArray[index].BackColor;
					COLORREF fgColor = m_ColorArray[index].TextColor;
					style
						.SetInterior(bkColor)
						.SetTextColor(fgColor);
				}
				else
					style.SetInterior(m_BackColor);
			}
			else
			{
				if(nCol >= m_nCol1 && nCol <= m_nCol2)
					style
						.SetInterior(m_ColHeaderBgColor[nCol])
						.SetTextColor(m_ColHeaderFgColor[nCol])
						;
				else
				{
					style.SetInterior(m_BackColor);
				}
			}
	}
*/	return bRet;
}

void CMyGridWnd::OnContextMenu(CWnd* pWnd, CPoint point) 
{
	// TODO: Add your message handler code here
	
}
