// CaliPoint.h: interface for the CCaliPoint class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CALIPOINT_H__6323937C_1920_4C27_93C2_B5E4D26084A2__INCLUDED_)
#define AFX_CALIPOINT_H__6323937C_1920_4C27_93C2_B5E4D26084A2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#define POINT_ALL	0
#define VTG_CAL_SET_POINT	1
#define VTG_CAL_CHECK_POINT	2
#define CRT_CAL_SET_POINT	3
#define CRT_CAL_CHECK_POINT	4

#define CAL_RANGE_ALL	0xffff	
#define CAL_RANGE1	0
#define CAL_RANGE2	1
#define CAL_RANGE3	2
#define CAL_RANGE4	3
#define CAL_RANGE5	4

#define REG_CAL_SECTION	"Calibration"

class CCaliPoint  
{
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

//	SFT_CALI_SET_POINT GetNetworkFormatPoint();
	void	SetVRangeCnt(int nRangeCnt = 1	)	{	m_nVRangeCnt = 	nRangeCnt; }
	void	SetIRangeCnt(int nRangeCnt = 1	)	{	m_nIRangeCnt = 	nRangeCnt; }
	int	GetVRangeCnt()	{	return  m_nVRangeCnt;	}
	int	GetIRangeCnt()	{	return	m_nIRangeCnt;	}

	BOOL WriteDataToFile(FILE *fp);
	BOOL ReadDataFromFile(FILE *fp);
	CCaliPoint& operator = (CCaliPoint& OrgPoint);

	BOOL SavePointData(BOOL bAll = TRUE);
	BOOL LoadPointData(BOOL bAll = TRUE);
//	BOOL AddPointData(WORD wRange = CAL_RANGE1, int nDataType = POINT_ALL);

	//set Data FTN
	BOOL SetVSetPointData(int nDataCount, double *pdData, WORD wRange = 0);
	BOOL SetVCheckPointData(int nDataCount, double *pdData, WORD wRange = 0);
	BOOL SetISetPointData(int nDataCount, double *pdData, WORD wRange = 0);
	BOOL SetICheckPointData(int nDataCount, double *pdData, WORD wRange = 0);

	BOOL SetShuntT( double *pdData );
	BOOL SetShuntR( double *pdData );
	BOOL SetShuntSerial( int nBoardIndexNum, CHAR *pdData );

	//One Range Ftn
	double GetVSetPoint(WORD nPoint);
	double GetVCheckPoint(WORD nPoint);
	double GetISetPoint(WORD nPoint);
	double GetICheckPoint(WORD nPoint);

	double GetShuntT( int nBoardIndexNum );
	double GetShuntR( int nBoardIndexNum );
	CString GetShuntSerial( int nBoardIndexNum );

	//Muti Range Ftn
	double GetVSetPoint(WORD wRange, WORD nPoint);
	double GetVCheckPoint(WORD wRange, WORD nPoint);
	double GetISetPoint(WORD wRange, WORD nPoint);
	double GetICheckPoint(WORD wRange, WORD nPoint);

	void ResetPoint(WORD wRange = CAL_RANGE_ALL, int nDataType = POINT_ALL);
	WORD GetVtgSetPointCount(WORD wRange = CAL_RANGE1)		{		return m_nVCalPointNum[wRange];			};
	WORD GetVtgCheckPointCount(WORD wRange = CAL_RANGE1)	{		return m_nVCheckPointNum[wRange];		};
	WORD GetCrtSetPointCount(WORD wRange = CAL_RANGE1)		{		return m_nICalPointNum[wRange];			};
	WORD GetCrtCheckPointCount(WORD wRange = CAL_RANGE1)	{		return m_nICheckPointNum[wRange];		};
	CCaliPoint();
	virtual ~CCaliPoint();

	double	m_dVDAAccuracy[CAL_MAX_VOLTAGE_RANGE];			//각 Range별 전압 정밀도 
	double	m_dIDAAccuracy[CAL_MAX_CURRENT_RANGE];			//각 Range별 전류 정밀도
	double	m_dVADAccuracy[CAL_MAX_VOLTAGE_RANGE];			//각 Range별 전압 정밀도 
	double	m_dIADAccuracy[CAL_MAX_CURRENT_RANGE];			//각 Range별 전류 정밀도

	void SortData();
protected:
	int m_nVRangeCnt;
	int	m_nIRangeCnt;
	WORD	m_nVCalPointNum[CAL_MAX_VOLTAGE_RANGE];
	WORD	m_nVCheckPointNum[CAL_MAX_VOLTAGE_RANGE];
	WORD	m_nICalPointNum[CAL_MAX_CURRENT_RANGE];
	WORD	m_nICheckPointNum[CAL_MAX_CURRENT_RANGE];
	double	m_VCalPoint[CAL_MAX_VOLTAGE_RANGE][CAL_MAX_CALIB_SET_POINT];
	double	m_VCheckPoint[CAL_MAX_VOLTAGE_RANGE][CAL_MAX_CALIB_CHECK_POINT];
	double	m_ICalPoint[CAL_MAX_CURRENT_RANGE][CAL_MAX_CALIB_SET_POINT];
	double	m_ICheckPoint[CAL_MAX_CURRENT_RANGE][CAL_MAX_CALIB_CHECK_POINT];
	double  m_ShuntT[CAL_MAX_BOARD_NUM];
	double	m_ShuntR[CAL_MAX_BOARD_NUM];
	CHAR	m_ShuntSerial[CAL_MAX_BOARD_NUM][CAL_MAX_SHUNT_SERIAL_SIZE];
};

#endif // !defined(AFX_CALIPOINT_H__6323937C_1920_4C27_93C2_B5E4D26084A2__INCLUDED_)
