#include "StdAfx.h"
#include "MySql.h"


MySql::MySql(void)
{
	m_bCon=FALSE;
	m_bConfg=FALSE;

	m_dxMesg=_T("");

	m_dwSqlfile=0;

	m_strErr=_T("");
	m_iErr=0;
}

MySql::~MySql(void)
{
}

void MySql::DBInit(CString sIp,CString sId,CString sPwd,CString sDb,int iPort,BOOL bMbox)
{	
	m_bMbox=bMbox;	
	m_sIp=sIp;	
	m_sId=sId;
	m_sPwd=sPwd;
	m_sDb=sDb;
	m_iPort=iPort;
}

BOOL MySql::ReConn()
{
	if(m_sIp.IsEmpty() || m_sIp==_T(""))   return FALSE;
	if(m_sId.IsEmpty() || m_sId==_T(""))   return FALSE;
	if(m_sDb.IsEmpty() || m_sDb==_T(""))   return FALSE;

	m_bCon=FALSE;
	CString sIp=m_sIp;
	CString sId=m_sId;
	CString sPwd=m_sPwd;
	int iPort=m_iPort;
	BOOL bMbox=m_bMbox;
	CString sDb=m_sDb;

	mysql_refresh(&m_Mysql,0);
	mysql_close(&m_Mysql);

	mysql_init(&m_Mysql);
	m_Mysql.reconnect=1;//재접속
	m_strErr=_T("");
	m_iErr=0;

	#ifdef _UNICODE				
		char *buf1=GStr::str_All(sIp);
		char *buf2=GStr::str_All(sId);
		char *buf3=GStr::str_All(sPwd);
		
		MYSQL *ret=mysql_real_connect(&m_Mysql,buf1,buf2,buf3,NULL,iPort,(char*)NULL,0);//131072 	
		
		delete [] buf1;
		delete [] buf2;
		delete [] buf3;
		
		if(!ret)
		{
			DBError(bMbox);		
			return m_bCon=FALSE;
		}
	#else
		if(!mysql_real_connect(&m_Mysql,sIp,sId,sPwd,_T(""),iPort,(char*)NULL,0))//131072 	
		{		
			DBError(bMbox);		
			return m_bCon=FALSE;
		}	
	#endif
	
	m_bCon=TRUE;
	SetQuery(_T("SET NAMES EUCKR"));	
	SetQuery(_T("SET WAIT_TIMEOUT=28800"));
	SetQuery(_T("SET GLOBAL WAIT_TIMEOUT=2147483"));//24day

	DBOption();
	if(DBUse(sDb)==FALSE) return m_bCon=FALSE;//select database

	log_save(_T("db"),_T("ReConn ok-")+sDb);
	return TRUE;
}

BOOL MySql::DBConn(CString sIp,CString sId,CString sPwd,CString sDb,int iPort,
				   BOOL bMbox,BOOL bCreate)
{				
	if(m_bCon==TRUE) return TRUE;	
	if(sIp.IsEmpty() || sIp==_T(""))   return FALSE;
	if(sId.IsEmpty() || sId==_T(""))   return FALSE;
	if(sDb.IsEmpty() || sDb==_T(""))   return FALSE;
	if(sDb==_T("mysql"))               return FALSE;
	if(sDb==_T("infomation_schema"))   return FALSE;

	DBInit(sIp,sId,sPwd,sDb,iPort,bMbox);
		
	mysql_init(&m_Mysql);	
	m_Mysql.reconnect=1;//재접속
	m_strErr=_T("");
	m_iErr=0;

	DBOption();

	#ifdef _UNICODE				
		char *buf1=GStr::str_All(sIp);
		char *buf2=GStr::str_All(sId);
		char *buf3=GStr::str_All(sPwd);
		
		MYSQL *ret=mysql_real_connect(&m_Mysql,buf1,buf2,buf3,NULL,iPort,(char*)NULL,0);//131072 	
		
		delete buf1;
		delete buf2;
		delete buf3;

		if(!ret)
		{		
			DBError(bMbox);		
			return m_bCon=FALSE;
		}
	#else			
		if(!mysql_real_connect(&m_Mysql,sIp,sId,sPwd,_T(""),iPort,(char*)NULL,0))//131072 	
		{		
			DBError(bMbox);		
			return m_bCon=FALSE;
		}	
	#endif

	m_bCon=TRUE;
	SetQuery(_T("SET NAMES EUCKR"));
	SetQuery(_T("SET WAIT_TIMEOUT=28800"));//8hour	
	SetQuery(_T("SET GLOBAL WAIT_TIMEOUT=2147483"));//24day	

	DBOption();	
	
	if(bCreate==TRUE) DBUserInit();//server execute
	
	if(DBUse(sDb)==FALSE) return m_bCon=FALSE;//select database

	log_save(_T("db"),_T("Conn ok-")+sDb);
	return TRUE;		
}

void MySql::log_save(CString sType,CString sLog)
{ 		
	GLog::log_Save(_T("mysql"),sType,sLog,NULL,TRUE);
}
	
BOOL MySql::DBUserInit()
{
	if(m_bCon==FALSE) return FALSE;
	
	CString dbname=m_sDb;
	CString dbname1=dbname;    
	dbname1.MakeLower();

	if(ChkDatabase(dbname)==FALSE && 
	   ChkDatabase(dbname1)==FALSE)
	{//database not found

		CString sql;
		sql.Format(_T("CREATE DATABASE `%s`"),dbname);
		if(SetQuery(sql)==FALSE) return FALSE;	
		
		if(m_dwSqlfile>0)
		{
			//server execute
			CString sql=GFile::file_ResorceToStr(_T("INSTALLER"),m_dwSqlfile);
			sql=GStr::str_UnicodeConvert(sql);
			if(SetQuery(sql)==FALSE) return FALSE;	
		}
		m_bConfg=TRUE;
	}

	return TRUE;
}

BOOL MySql::DBOption()
{
	#ifdef _UNICODE	   
	    char *buf1=GStr::str_All(_T("EUCKR"));
		char *buf2=GStr::str_All(_T("SET NAMES EUCKR"));
		char *buf3=GStr::str_All(_T("SET WAIT_TIMEOUT=28800"));
		char *buf4=GStr::str_All(_T("SET GLOBAL WAIT_TIMEOUT=2147483"));

		mysql_options(&m_Mysql,MYSQL_SET_CHARSET_NAME,buf1);
		mysql_options(&m_Mysql,MYSQL_INIT_COMMAND,buf2);
		mysql_options(&m_Mysql,MYSQL_INIT_COMMAND,buf3);
		mysql_options(&m_Mysql,MYSQL_INIT_COMMAND,buf4);

		delete buf1; 
		delete buf2;
		delete buf3;
		delete buf4;
	#else			
		mysql_options(&m_Mysql,MYSQL_SET_CHARSET_NAME,_T("EUCKR"));
		mysql_options(&m_Mysql,MYSQL_INIT_COMMAND,_T("SET NAMES EUCKR"));
		mysql_options(&m_Mysql,MYSQL_INIT_COMMAND,_T("SET WAIT_TIMEOUT=28800"));
		mysql_options(&m_Mysql,MYSQL_INIT_COMMAND,_T("SET GLOBAL WAIT_TIMEOUT=2147483"));
	#endif

	#if MYSQL_VERSION_ID >= 50003
	{	
		my_bool reconnect = (my_bool)1;
		if(mysql_options(&m_Mysql, MYSQL_OPT_RECONNECT, &reconnect))
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	#endif

	return FALSE;
}

BOOL MySql::DBUse(CString db)
{
	if(m_bCon==FALSE) return FALSE;

	#ifdef _UNICODE				
	    char *buf=GStr::str_All(db);		
		int iresult=mysql_select_db(&m_Mysql,buf);//use databases;
		delete buf;
	#else			
		int iresult=mysql_select_db(&m_Mysql,db);//use databases;
	#endif

    if(iresult!=0) return FALSE;
	
	return TRUE;
}

BOOL MySql::DBClose()
{	
	if(m_bCon==TRUE)
	{
		mysql_refresh(&m_Mysql,0);
		mysql_close(&m_Mysql);
	}
	m_bCon=FALSE;
	return TRUE;
	
}

BOOL MySql::DBStat()
{
	if(mysql_stat(&m_Mysql)==0)
	{						
		DBError(m_bMbox);
		return FALSE;
	}
	
	return TRUE;
}

BOOL MySql::DBPing()
{		
	int ret = mysql_ping(&m_Mysql) ;
    if(ret==-1)
	{				
		DBError(m_bMbox);
		return FALSE;
	}
	return TRUE;
}

BOOL MySql::DBConnCheck()
{				
	if(m_bCon==FALSE) 
	{				
		if ( !DBConn(m_sIp,m_sId,m_sPwd,m_sDb,m_iPort,m_bMbox))
		{
			return FALSE;
		}
	}		
	return TRUE;			
}

CString MySql::DBError(BOOL bflag)
{	
	CString sErr=_T("");		
	int nErr = mysql_errno(&m_Mysql);
	m_iErr = nErr;
	if(nErr==0) return _T("");

	if(nErr==2003 || nErr==2006 || nErr==2013)
	{//lost close
		m_bCon=FALSE;
	}

	CString str=(CString)mysql_error(&m_Mysql);
	sErr.Format(_T("%d : %s"),nErr,str);
	m_strErr=sErr;
	
	log_save(_T("err"),sErr);

	if(bflag)
	{		
		::MessageBox( NULL, sErr, _T("DB ERROR"), MB_OK );
	}	

	return sErr;
}

BOOL MySql::DBRefresh()
{
	if(m_bCon==FALSE)
	{				
		return DBConnCheck();
	}
	else
	{
		mysql_refresh(&m_Mysql,0);	
		int nErr=mysql_errno(&m_Mysql);
		
		if(nErr==2003 || nErr==2013)
		{//lost close
			DBClose();
			return DBConnCheck();
		}
	}
	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////////////
CStringArray* MySql::list_Query(CString sql)
{	
	if(m_bCon==FALSE) return NULL;

	CStringArray *list=NULL;
	int i=0;
	
	//if(mysql_real_query(&m_Mysql,sql,sql.GetLength())==0)

	#ifdef _UNICODE		
	    char *buf=GStr::str_All(sql);		
		int iresult=mysql_query(&m_Mysql,buf);	
		delete buf;
	#else
		int iresult=mysql_query(&m_Mysql,sql);	
	#endif

	if(iresult==0)	
	{				
		MYSQL_RES *res = mysql_store_result(&m_Mysql);
		int fields = mysql_field_count(&m_Mysql);
		int records    = (int)mysql_num_rows(res);
				
		if(res && fields>0 && records>0) //NULL이 아닌 부분 즉 레코드가 있는 경우
		{
		   list=new CStringArray[records+1];
		   MYSQL_FIELD *fd = mysql_fetch_fields(res);	
		   
		   for(i=0;i<fields;i++)
		   {		
			  CString str=(CString)fd[i].name;			 			
			  list[0].Add(str); //0 row에 field입력
		   }
		   list[0].Add(GStr::str_IntToStr(records));		  
		  
		   i=1;
		   MYSQL_ROW row;
		   //while((row=mysql_fetch_row(res)))
		   while(TRUE)
		   {
			 row=mysql_fetch_row(res);
			 if(!row) break;

			 for(int j = 0; j < fields; j++)
			 {
				 CString str=(CString)row[j];				
				 list[i].Add(str);
			 }
			 i=i+1;
		   }
		}
		mysql_free_result(res);
	} 
	else 
	{
		DBError(m_bMbox);//에러 표시		
	}	

	mysql_refresh(&m_Mysql,0);		
	return list;	
}

int MySql::list_RecordCnt(CStringArray* list)
{
	if(list==NULL || !list) return 0;
	if(list->GetSize()<1)   return 0;//field remove
	
	return _ttoi(list[0].GetAt(list[0].GetSize()-1));
}

int MySql::list_FieldCnt(CStringArray* list)
{
	return list[0].GetSize()-1;
}

CString MySql::list_FD(CStringArray *list,int record,CString name)  
{
	if(list_RecordCnt(list)<record) return _T("");

	for(int i=0;i<list[record].GetSize();i++)
	{
		if(list[0].GetAt(i)==name)
		{			
			return GStr::str_BTrim(list[record].GetAt(i));
		}
	}
	return _T("");
}

int MySql::list_FieldNum(CStringArray *list,CString name)  
{	
	if(list_ChkList(list)==FALSE) return -1;

	for(int i=0;i<list[0].GetSize();i++)
	{
		if(list[0].GetAt(i)==name)
		{			
			return i;
		}
	}
	return -1;
}

CString MySql::list_FDA(CStringArray *list,int record,CString name)  
{
	if(list_RecordCnt(list)<record) return _T("");

	for(int i=0;i<list[record].GetSize();i++)
	{
		if(list[0].GetAt(i)==name)
		{
			return GStr::str_BTrim(list[record].GetAt(i));
		}
	}
	return _T("");
}


BOOL MySql::list_ChkList(CStringArray *list)  
{		
	if(list_RecordCnt(list)<1)
	{			
		GWin::win_DelList(list);		
		return FALSE;
	}
	return TRUE;
}

void MySql::list_FreeList(CStringArray *list)  
{
	if(list==NULL || !list) return;		
	if(list_ChkList(list)==FALSE) return;
	 
	for(int i=0;i<list_RecordCnt(list)+1;i++)
	{
		list[i].RemoveAll();
		//delete &list[i];//사용하지않음(에러발생)
	}		
	delete[] list;	
	list=NULL;
}

///////////////////////////////////////////////////////////////////////////////////////
BOOL MySql::SetQuery(CString arraySql)
{//input,update,delete	
	if(m_bCon==FALSE)      return FALSE;
	if(arraySql.IsEmpty() || arraySql==_T("")) return TRUE;
				
	int count=GStr::str_Count(arraySql,_T(";"));
	if(count>0)
	{
	  CString sql;
	  for(int i=0;i<count;i++)
	  {
		sql=GStr::str_GetSplit(arraySql,i,';');
		if(sql.IsEmpty() || sql==_T("")) continue;

		#ifdef _UNICODE		
		    char *buf=GStr::str_All(sql);	
			int nresult=mysql_query(&m_Mysql,buf);	
			delete buf;	
		#else				
			int nresult=mysql_query(&m_Mysql,sql);	
		#endif

		if(nresult!=0)
		{
			DBError(m_bMbox);
			return FALSE;
		}
	  }
	} 
	else 
	{	
		#ifdef _UNICODE		
		    char *buf=GStr::str_All(arraySql);	
			int nresult=mysql_query(&m_Mysql,buf);	
			delete buf;
		#else
			int nresult=mysql_query(&m_Mysql,arraySql);	
		#endif

		if(nresult!=0)
		{			
			DBError(m_bMbox);
			return FALSE;
		}
	}	
	return TRUE;
}

CStringArray* MySql::ShowTable()
{//show tables
	if(m_bCon==FALSE) return FALSE;

    MYSQL_ROW row;
	MYSQL_RES *list = mysql_list_tables(&m_Mysql,NULL); 
	if(list==NULL) return NULL;

	CStringArray *tblist=new CStringArray;
    //while (row = mysql_fetch_row(list)) 
	while(TRUE)
	{//table list

		row = mysql_fetch_row(list);
		if(!row) break;

	    tblist->Add((CString)row[0]);
	}
	return tblist;
}

BOOL MySql::ChkTable(CString table)
{
	if(m_bCon==FALSE) return FALSE;
	if(table.IsEmpty() || table=="") return FALSE;
	
	CString sql;
	sql.Format(_T("SHOW TABLES LIKE '%s' "),table);

	MYSQLINFO *MysqlInfo=obj_Result(sql);
	if(!MysqlInfo){delete MysqlInfo; return 0;}

	BOOL bflag=TRUE;
	if(MysqlInfo->nrecord<1) bflag=FALSE;

	obj_InfoRefresh(MysqlInfo);//res,mysql,delete clear
	return bflag;
}

BOOL MySql::ChkDatabase(CString db)
{
	if(m_bCon==FALSE) return FALSE;
	if(db.IsEmpty() || db=="") return FALSE;
	
	CString sql=_T("");
	sql.Format(_T("SHOW DATABASES LIKE '%s' "),db);
	
	MYSQLINFO *MysqlInfo=obj_Result(sql);
	if(!MysqlInfo)
	{
		delete MysqlInfo; 
		return FALSE;
	}

	BOOL bflag=TRUE;
	if(MysqlInfo->nrecord<1) bflag=FALSE;
	
	obj_InfoRefresh(MysqlInfo);//res,mysql,delete clear
	return bflag;
}

CString MySql::GetDBTime()
{	
	if(m_bCon==FALSE) return _T("");

	CString sql=_T("SELECT NOW() AS DBDATE");

	CStringArray *list=list_Query(sql);	
	if(list_ChkList(list)==FALSE) return _T("");

	CString value=list_FD(list,1,_T("DBDATE"));
	list_FreeList(list);
	return value;
}

CString MySql::GetDBTimeFmt()
{	
	if(m_bCon==FALSE) return _T("");

	CString sql=_T("SELECT DATE_FORMAT(NOW(),'%Y%m%d%H%i%s') AS DBDATE");
	MYSQLINFO *MysqlInfo=obj_Result(sql);
	if(!MysqlInfo)
	{
		obj_MyDelete(MysqlInfo);
		return _T("");
	}

	CString svalue=_T("");
	if(MysqlInfo && MysqlInfo->res && MysqlInfo->nrecord>0)
	{								
		while(MysqlInfo->row=mysql_fetch_row(MysqlInfo->res))
		{	
			svalue=(CString)MysqlInfo->row[0];
			break;
		}
	}
	
	obj_InfoRefresh(MysqlInfo);//res,mysql,delete clear
	return svalue;

	/*CStringArray *list=list_Query(sql);	
	if(list_ChkList(list)==FALSE) return _T("");

	CString value=list_FD(list,1,_T("DBDATE"));
	list_FreeList(list);
	return value;*/
}

///////////////////////////////////////////////////////////////////////////////////////
MYSQLINFO* MySql::obj_Result(CString sql)
{
	if(m_bCon==FALSE) return FALSE;

	MYSQLINFO *MysqlInfo=new MYSQLINFO;
	//if(mysql_real_query(&m_Mysql,sql,sql.GetLength())==0) //(unsigned long)strlen(sql)

	#ifdef _UNICODE		
		int isize = WideCharToMultiByte(CP_ACP, 0, sql, -1, 0, 0, 0, 0);

		char *buf=new char[isize+1];
		::memset(buf,0,isize);//////////////////
		int iret=WideCharToMultiByte(CP_ACP,0,sql,sql.GetLength(),buf,isize,NULL,NULL);		
		int nresult=mysql_query(&m_Mysql,buf);	
		delete buf;
	#else			
		int nresult=mysql_query(&m_Mysql,sql);
	#endif

	if(nresult==0)
	{		
		MysqlInfo->res     = mysql_store_result(&m_Mysql);
		MysqlInfo->nfield  = mysql_num_fields(MysqlInfo->res);        
		MysqlInfo->nrecord = (int)mysql_num_rows(MysqlInfo->res); //static_cast<int>(mysql_num_rows(MysqlInfo->res));
	
		if(MysqlInfo->res) //NULL이 아닌 부분 즉 레코드가 있는 경우
		{
			MysqlInfo->fields = mysql_fetch_fields(MysqlInfo->res);
			return MysqlInfo;
		}		
        obj_ResRefresh(MysqlInfo->res);//res		
	} 
	else 
	{				
		DBError(m_bMbox);//에러 표시
	}	
	obj_MyRefresh();//mysql
	obj_MyDelete(MysqlInfo);//delete	
	return NULL;
}

CString MySql::obj_FD(int nfield,MYSQL_ROW row,MYSQL_FIELD *fields,CString f)
{	
	if(nfield<1) return _T("");
	if(!fields)  return _T("");
	if(!row)     return _T("");

	for(int i = 0; i < nfield; i++)
	{				
		CString str=(CString)fields[i].name;		
		if(str==f) return (CString)row[i];		
	}
	return _T("");
}

int MySql::obj_FDInt(MYSQLINFO *MysqlInfo,CString f)
{	
	if(MysqlInfo->nfield<1) return -1;
	if(!MysqlInfo->fields)  return -1;
	if(!MysqlInfo->row)     return -1;

	for(int i = 0; i < MysqlInfo->nfield; i++)
	{				
		CString str=(CString)MysqlInfo->fields[i].name;				
		if(str==f) return i;
	}
	return -1;
}

CString MySql::obj_FD(MYSQLINFO *MysqlInfo,CString f)
{	
	if(MysqlInfo->nfield<1) return _T("");
	if(!MysqlInfo->fields)  return _T("");
	if(!MysqlInfo->row)     return _T("");

	for(int i = 0; i < MysqlInfo->nfield; i++)
	{				
		CString str=(CString)MysqlInfo->fields[i].name;				
		if(str==f) return (CString)MysqlInfo->row[i];		
	}
	return _T("");
}

void MySql::obj_InfoRefresh(MYSQLINFO* MysqlInfo)
{//res,mysql,delete
	obj_ResRefresh(MysqlInfo->res);
	obj_MyRefresh();
	obj_MyDelete(MysqlInfo);
}

void MySql::obj_ResMyRefresh(MYSQL_RES*  res)
{//res,mysql
	obj_ResRefresh(res);
	obj_MyRefresh();
}

void MySql::obj_ResRefresh(MYSQL_RES*  res)
{//res
	if(res) mysql_free_result(res);
}

void MySql::obj_MyRefresh()
{//mysql
	mysql_refresh(&m_Mysql,0);
}

void MySql::obj_MyDelete(MYSQLINFO* MysqlInfo)
{//delete
	delete MysqlInfo;
}

BOOL MySql::obj_ChkMysqlInfo(MYSQLINFO* MysqlInfo)
{	
	if(!MysqlInfo)
	{
		obj_MyDelete(MysqlInfo);
		return FALSE;
	}
	return TRUE;
}

CString MySql::obj_GetData(CString sql,CString f)
{
	if(m_bCon==FALSE) return _T("");

	MYSQLINFO *MysqlInfo=obj_Result(sql);
	if(!MysqlInfo)
	{
		delete MysqlInfo; 
		return _T("");
	}
	
	CString value=_T("");
	if(MysqlInfo->res)
	{				
		//while((MysqlInfo->row=mysql_fetch_row(MysqlInfo->res)))
		while(TRUE)
		{			
			MysqlInfo->row=mysql_fetch_row(MysqlInfo->res);
			if(!MysqlInfo->row) break;

			value=obj_FD(MysqlInfo,f);			
			break;//first record		
		}		
	}
	obj_InfoRefresh(MysqlInfo);//res,mysql,delete clear
	return value;	
}
///////////////////////////////////////////////////////////////////////////////////////

CString MySql::test1()
{	
	if(m_bCon==FALSE) return _T("");

	
	CString sql=_T("CALL BSVALUEMT('test')");
	
	CStringArray *list=list_Query(sql);	
	if(list_ChkList(list)==FALSE) return _T("");

	CString value=list_FD(list,1,_T("SVALUE"));
	list_FreeList(list);
	return value;

	/*int ret = mysql_ping(&m_Mysql) ;
    if(ret==-1)
	{
		DBError(m_bMbox);
		return "";
	}

	
	int status = mysql_query(&m_Mysql,"CALL BSVALUEMT('test');");
	if (status)
	{
		DBError(m_bMbox);
        return "";
	}

	MYSQL_RES*  res;
	//MYSQL_ROW   row;
	//MYSQL_FIELD *fields;

	//res=mysql_store_result(&m_Mysql);            
	//row=mysql_fetch_row(res);

	res     = mysql_store_result(&m_Mysql);
	int nfield  = mysql_num_fields(res);        
	int nrecord = static_cast<int>(mysql_num_rows(res));
	   	
    mysql_free_result(res);*/

	return _T("");
}

CString MySql::GetData1(CString table,CString n1,CString v1,CString f)
{
	if(m_bCon==FALSE) return _T("");

	CString sql=_T("");
	sql.Format(_T("SELECT * FROM %s WHERE %s='%s'"),table,n1,v1);

	CStringArray *list=list_Query(sql);	
	if(list_ChkList(list)==FALSE) return _T("");

	CString value=list_FD(list,1,f);	
	list_FreeList(list);
	return value;
}

CString MySql::GetData2(CString table,CString n1,CString v1,CString n2,CString v2,CString f)
{
	if(m_bCon==FALSE) return _T("");

	CString sql;
	sql.Format(_T("SELECT * FROM %s \
			     WHERE %s='%s' \
	             AND %s='%s'"),table,n1,v1,n2,v2);

	CStringArray *list=list_Query(sql);	
	if(list_ChkList(list)==FALSE) return _T("");

	CString value=list_FD(list,1,f);
	list_FreeList(list);
	return value;
}


CString MySql::GetData3(CString table,CString n1,CString v1,CString n2,CString v2,CString n3,CString v3,CString f)
{
	if(m_bCon==FALSE) return _T("");

	CString sql;
	sql.Format(_T("SELECT * FROM %s \
			     WHERE %s='%s' \
	             AND %s='%s' \
				 AND %s='%s' "),table,n1,v1,n2,v2,n3,v3);
	
	CStringArray *list=list_Query(sql);	
	if(list_ChkList(list)==FALSE) return _T("");

	CString value=list_FDA(list,1,f);
	list_FreeList(list);
	return value;
}


CString MySql::GetData4(CString table,CString n1,CString v1,CString n2,CString v2,
						              CString n3,CString v3,CString n4,CString v4,
									  CString f)
{
	if(m_bCon==FALSE) return _T("");

	CString sql;
	sql.Format(_T("SELECT * FROM %s \
			     WHERE %s='%s' \
	             AND %s='%s' \
				 AND %s='%s' \
				 AND %s='%s' "),table,n1,v1,n2,v2,n3,v3,n4,v4);
	
	CStringArray *list=list_Query(sql);	
	if(list_ChkList(list)==FALSE) return _T("");

	CString value=list_FDA(list,1,f);
	list_FreeList(list);
	return value;
}

CString MySql::GetData5(CString table,CString n1,CString v1,CString n2,CString v2,
						              CString n3,CString v3,CString n4,CString v4,
									  CString n5,CString v5,
									  CString f)
{
	if(m_bCon==FALSE) return _T("");

	CString sql;
	sql.Format(_T("SELECT * FROM %s \
			     WHERE %s='%s' \
	             AND %s='%s' \
				 AND %s='%s' \
				 AND %s='%s' \
				 AND %s='%s' "),table,n1,v1,n2,v2,n3,v3,n4,v4,n5,v5);
	
	CStringArray *list=list_Query(sql);	
	if(list_ChkList(list)==FALSE) return _T("");

	CString value=list_FDA(list,1,f);
	list_FreeList(list);
	return value;
}


CString MySql::GetData6(CString table,CString n1,CString v1,CString n2,CString v2,
						              CString n3,CString v3,CString n4,CString v4,
									  CString n5,CString v5,CString n6,CString v6,
									  CString f)
{
	if(m_bCon==FALSE) return _T("");

	CString sql;
	sql.Format(_T("SELECT * FROM %s \
			     WHERE %s='%s' \
	             AND %s='%s' \
				 AND %s='%s' \
				 AND %s='%s' \
				 AND %s='%s' \
				 AND %s='%s' "),table,n1,v1,n2,v2,n3,v3,n4,v4,n5,v5,n6,v6);
	
	CStringArray *list=list_Query(sql);	
	if(list_ChkList(list)==FALSE) return _T("");

	CString value=list_FDA(list,1,f);
	list_FreeList(list);
	return value;
}


CString MySql::GetData7(CString table,CString n1,CString v1,CString n2,CString v2,
						              CString n3,CString v3,CString n4,CString v4,
									  CString n5,CString v5,CString n6,CString v6,
									  CString n7,CString v7,
									  CString f)
{
	if(m_bCon==FALSE) return _T("");

	CString sql;
	sql.Format(_T("SELECT * FROM %s \
			     WHERE %s='%s' \
	             AND %s='%s' \
				 AND %s='%s' \
				 AND %s='%s' \
				 AND %s='%s' \
				 AND %s='%s' \
				 AND %s='%s' "),table,n1,v1,n2,v2,n3,v3,n4,v4,n5,v5,n6,v6,n7,v7);
	
	CStringArray *list=list_Query(sql);	
	if(list_ChkList(list)==FALSE) return _T("");

	CString value=list_FDA(list,1,f);
	list_FreeList(list);
	return value;
}


CString MySql::GetData8(CString table,CString n1,CString v1,CString n2,CString v2,
						              CString n3,CString v3,CString n4,CString v4,
									  CString n5,CString v5,CString n6,CString v6,
									  CString n7,CString v7,CString n8,CString v8,
									  CString f)
{
	if(m_bCon==FALSE) return _T("");

	CString sql;
	sql.Format(_T("SELECT * FROM %s \
			     WHERE %s='%s' \
	             AND %s='%s' \
				 AND %s='%s' \
				 AND %s='%s' \
				 AND %s='%s' \
				 AND %s='%s' \
				 AND %s='%s' \
				 AND %s='%s' "),table,n1,v1,n2,v2,n3,v3,n4,v4,n5,v5,n6,v6,n7,v7,n8,v8);
	
	CStringArray *list=list_Query(sql);	
	if(list_ChkList(list)==FALSE) return _T("");

	CString value=list_FDA(list,1,f);
	list_FreeList(list);
	return value;
}
// db insert
BOOL MySql::GetTbName_L( CString &strTbName, CString &strIdx )
{
	if(m_bCon==FALSE) return FALSE;
	
	CString sql;
	sql.Format(_T("SELECT * FROM tb_workinfo ORDER BY STARTDATE DESC, IDX DESC LIMIT 1"));
	CStringArray *list=list_Query(sql);	
	if(list_ChkList(list)==FALSE) return FALSE;

	CString value=list_FDA(list,1,_T("IDX"));
	strIdx = value;
	
	value += list_FDA(list,1,_T("EQUIPNM"));
	value += list_FDA(list,1,_T("BLOCKNM"));
	list_FreeList(list);

	strTbName = value;
	return TRUE;
}
//E
LONGLONG MySql::GetMax(CString table,CString f)
{
	if(m_bCon==FALSE) return 0;

	CString sql=_T("");
	sql.Format(_T("SELECT MAX(%s) AS SMAX FROM %s "),f,table);
	
	CStringArray *list=list_Query(sql);	
	if(list_ChkList(list)==FALSE) return 0;

	CString value=list_FD(list,1,_T("SMAX"));
	list_FreeList(list);
	
	if(GStr::str_ChkNumeric(value)==FALSE) return _ttoi64(value);
	return 0;
}

int MySql::GetCount(CString table)
{
	if(m_bCon==FALSE) return 0;

	CString sql;
	sql.Format(_T("SELECT count(*) AS NCOUNT FROM %s "),table);
		
	CStringArray *list=list_Query(sql);	
	if(list_ChkList(list)==FALSE) return 0;

	CString value=list_FD(list,1,_T("NCOUNT"));
	list_FreeList(list);
	
	if(GStr::str_ChkNumeric(value)==FALSE) return _ttoi(value);
	return 0;	
}

int MySql::GetCount1(CString table,CString n1,CString v1)
{
	if(m_bCon==FALSE) return 0;

	CString sql;
	sql.Format(_T("SELECT count(*) AS NCOUNT FROM %s WHERE %s='%s' "),table,n1,v1);
		
	CStringArray *list=list_Query(sql);	
	if(list_ChkList(list)==FALSE) return 0;

	CString value=list_FD(list,1,_T("NCOUNT"));
	list_FreeList(list);
	
	if(GStr::str_ChkNumeric(value)==FALSE) return _ttoi(value);
	return 0;	
}
/*
int MySql::GetCount2(CString table,CString n1,CString v1,CString n2,CString v2)
{
	if(m_bCon==FALSE) return 0;

	CString sql="";
	sql.Format("SELECT count(*) AS NCOUNT FROM %s \
	            WHERE %s='%s' \
	            AND %s='%s' ",table,n1,v1,n2,v2);
			
	CStringArray *list=list_Query(sql);	
	if(list_ChkList(list)==FALSE) return 0;

	CString value=list_FD(list,1,"NCOUNT");
	list_FreeList(list);
	
	if(GStr::str_ChkNumeric(value)==FALSE) return atoi(value);
	return 0;	
}


int MySql::GetCount3(CString table,CString n1,CString v1,
					               CString n2,CString v2,
								   CString n3,CString v3)
{
	if(m_bCon==FALSE) return 0;

	CString sql="";
	sql.Format("SELECT count(*) AS NCOUNT FROM %s \
				WHERE %s='%s' \
				AND %s='%s' \
			    AND %s='%s' ",table,n1,v1,n2,v2,n3,v3);
			
	CStringArray *list=list_Query(sql);	
	if(list_ChkList(list)==FALSE) return 0;

	CString value=list_FD(list,1,"NCOUNT");
	list_FreeList(list);
	
	if(GStr::str_ChkNumeric(value)==FALSE) return atoi(value);
	return 0;	
}


int MySql::GetCount4(CString table,CString n1,CString v1,
					               CString n2,CString v2,
								   CString n3,CString v3,
								   CString n4,CString v4)
{
	if(m_bCon==FALSE) return 0;

	CString sql="";
	sql.Format("SELECT count(*) AS NCOUNT FROM %s \
				WHERE %s='%s' \
				AND %s='%s' \
			    AND %s='%s' \
				AND %s='%s' ",table,n1,v1,n2,v2,n3,v3,n4,v4);
	
	CStringArray *list=list_Query(sql);		
	if(list_ChkList(list)==FALSE) return 0;	

	CString value=list_FD(list,1,"NCOUNT");	
	list_FreeList(list);	
	
	if(GStr::str_ChkNumeric(value)==FALSE) return atoi(value);
	return 0;
}
*/

BOOL MySql::SetDelete1(CString table,CString n1,CString v1) 
{
	if(m_bCon==FALSE) return FALSE;

	CString sql=_T("");
	sql.Format(_T("DELETE FROM %s \
	            WHERE %s='%s' "),table,n1,v1);
	
	return SetQuery(sql);
}

/*
BOOL MySql::SetDelete2(CString table,CString n1,CString v1,CString n2,CString v2) 
{
	if(m_bCon==FALSE) return FALSE;

	CString sql="";
	sql.Format("DELETE FROM %s \
	            WHERE %s='%s' \
	            AND %s='%s' ",table,n1,v1,n2,v2);

	return SetQuery(sql);
}*/

BOOL MySql::SetEditData1(CString table,CString n1,CString v1,CString n2,CString v2)
{
	if(m_bCon==FALSE) return FALSE;
	
    CString sql=_T("");
	sql.Format(_T("UPDATE %s SET \
	            %s='%s' \
	            WHERE %s='%s' "),table,n1,v1,n2,v2);

	return SetQuery(sql);
}

BOOL MySql::SetEditData2(CString table,CString n1,CString v1,
						               CString n2,CString v2,
									   CString n3,CString v3)
{
	if(m_bCon==FALSE) return FALSE;
	
    CString sql=_T("");
	sql.Format(_T("UPDATE %s SET \
	            %s='%s', \
				%s='%s'  \
	            WHERE %s='%s' "),table,n1,v1,n2,v2,n3,v3);
  
	return SetQuery(sql);
}

BOOL MySql::SetEditData3(CString table,CString n1,CString v1,
						               CString n2,CString v2,
									   CString n3,CString v3,
									   CString n4,CString v4)
{
	if(m_bCon==FALSE) return FALSE;

	CString sql=_T("");
	sql.Format(_T("UPDATE %s SET \
	            %s='%s', \
				%s='%s', \
				%s='%s'  \
	            WHERE %s='%s' "),table,n1,v1,n2,v2,n3,v3,n4,v4);

	return SetQuery(sql);
}

BOOL MySql::SetEditData1Con6(CString table,CString n1,CString v1,
										   CString c1,CString d1,CString c2,CString d2,
										   CString c3,CString d3,CString c4,CString d4,
										   CString c5,CString d5,CString c6,CString d6)
{
	if(m_bCon==FALSE) return FALSE;
	
    CString sql=_T("");
	sql.Format(_T("UPDATE %s SET \
								 %s='%s' \
								 WHERE %s='%s' \
								 AND %s='%s' \
								 AND %s='%s' \
								 AND %s='%s' \
								 AND %s='%s' \
								 AND %s='%s' "),table,n1,v1,
								                 c1,d1,c2,d2,c3,d3,c4,d4,c5,d5,c6,d6);
	
	return SetQuery(sql);
}



/*
BOOL MySql::SetEditData1Con2(CString table,CString n1,CString v1,
										   CString n2,CString v2,
										   CString n3,CString v3)
{
	if(m_bCon==FALSE) return FALSE;

    CString sql= "";
	sql.Format(" UPDATE %s SET \
	             %s='%s' \
	             WHERE %s='%s' \
	             AND %s='%s' ",table,n1,v1,n2,v2,n3,v3);

	return SetQuery(sql);
}

BOOL MySql::SetEditPwd1Con2(CString table,CString n1,CString v1,
										   CString n2,CString v2,
										   CString n3,CString v3)
{
	if(m_bCon==FALSE) return FALSE;

    CString sql= "";
	sql.Format(" UPDATE %s SET \
			     %s=Password('%s') \
	             WHERE %s='%s' \
	             AND %s='%s' ",table,n1,v1,n2,v2,n3,v3);

	return SetQuery(sql);
}


BOOL MySql::SetEditNow1(CString table,CString n1,CString n2,CString v2)
{
	if(m_bCon==FALSE) return FALSE;

	CString sql="";
	sql.Format("UPDATE %s SET \
	            %s=NOW() \
	            WHERE %s='%s' ",table,n1,n2,v2);

	return SetQuery(sql);
}

BOOL MySql::SetEditNow3(CString table, CString n1,CString v1,
						               CString n2,CString v2,
									   CString n3,CString v3,
									   CString n4,
									   CString n5,CString v5)
{
	if(m_bCon==FALSE) return FALSE;

	CString sql="";
	sql.Format("UPDATE %s SET \
                %s='%s', \
				%s='%s', \
				%s='%s', \
	            %s=NOW() \
	            WHERE %s='%s' ",table,n1,v1,n2,v2,n3,v3,n4,n5,v5);	
	
	return SetQuery(sql);
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

CStringArray* MySql::db_Schedlist()
{//스케줄 리스트
	if(m_bCon==FALSE) return NULL;

	CString sql="SELECT * FROM BSSCHEDMT sched \
	             LEFT JOIN BSCLIENTMT client ON client.NOCLIENT=sched.NOADMIN \
	             WHERE sched.STAT='1' \
	             ORDER BY sched.DTREG,sched.NMSCHED ASC  ";

	CStringArray *list=list_Query(sql);	
	if(list_ChkList(list)==FALSE) return NULL;

	return list;
}

CStringArray* MySql::db_Datalist(CString tfilename)
{//컨텐츠 리스트	
	if(m_bCon==FALSE) return NULL;
	if(tfilename.IsEmpty() || tfilename=="") return NULL;
					
	CString sql="SELECT * FROM BSDATASMT WHERE STAT='1' ";

	if(tfilename!="")
	{
		CString str1="";
		str1.Format("AND TFILENAME LIKE '%s%s' \
		            AND LOCATE('/',TFILENAME,OCTET_LENGTH('%s')+2)=0 ",tfilename,"%",tfilename);		
        sql.Append(str1);
	}
	sql.Append(" ORDER BY DTREG ASC ");
	
	CStringArray *list=list_Query(sql);	
	if(list_ChkList(list)==FALSE) return NULL;
	
	return list;
}

CString MySql::db_GetPlayTime(CStringArray* list,CString tfilename)
{//동영상일경우 길이 가져오기
	if(m_bCon==FALSE) return "10";
	if(tfilename.IsEmpty() || tfilename=="") return "10";
	if(list_ChkList(list)==FALSE) return "10";
		
	for(int i=1;i<list_RecordCnt(list)+1;i++)
	{		
		CString filename=list_FD(list,i,"TFILENAME"); 		
		if(filename==tfilename)
		{
			CString playtime=list_FD(list,i,"PLAYTIME");			
			if(GStr::str_ChkNumeric(playtime)==TRUE) playtime="10";				
			return playtime;
		}
	}
	return "10";
}

void MySql::db_GetFileInfo(CString tfilename,CString &sfilename,CString &splaytime,CString &sfilesize)
{
	sfilename="";
	splaytime="";
	sfilesize="0";			
	if(m_bCon==FALSE) return;
	if(tfilename.IsEmpty() || tfilename=="") return;
				
	CString sql="";
	sql.Format(" SELECT * FROM BSDATASMT \
	             WHERE TFILENAME='%s' ",tfilename);

	CStringArray *list=list_Query(sql);	
	if(list_ChkList(list)==FALSE) return;

	sfilename=list_FD(list,1,"SFILENAME");
	splaytime=list_FD(list,1,"PLAYTIME");
	sfilesize=list_FD(list,1,"FILESIZE");

	if(GStr::str_ChkNumeric(sfilesize)==TRUE) sfilesize="0";

	list_FreeList(list);	
}

BOOL MySql::db_InsertGroup(CString noadmin,CString nmgroup)
{//그룹 명 삽입
	if(m_bCon==FALSE) return FALSE;

    CString sql="";
	sql.Format("INSERT INTO BSGROUPMT (NMGROUP,ETC,STAT,NOADMIN,DTREG) \
	            VALUES ('%s','','1','%s',NOW() ) ",nmgroup,noadmin);

	return SetQuery(sql);
}


BOOL MySql::db_InsertClient(CString noadmin,CString nogroup,CString nmclient,
							CString clientid,CString clientpwd,CString authkey)
{//클라이언트 명 삽입
	if(m_bCon==FALSE) return FALSE;

    CString sql="";
	sql.Format("INSERT INTO BSCLIENTMT \
	            (NOGROUP,CLIENTID,CLIENTPWD,NMCLIENT, \
	             HORI,VERT,IPADDR,MACADDR, \
	             ETC,AUTHKEY,AUTH,STAT,NOADMIN,DTREG) \
	            VALUES \
	            ('%s','%s','%s','%s', \
	             '','','','', \
	             '','%s','%d','1','%s',NOW() ) ",nogroup,clientid,clientpwd,nmclient,
				                                 authkey,AUTH_CLIENT,noadmin);

	return SetQuery(sql);
}

BOOL MySql::db_InsertAdmin(CString noadmin,CString nmclient,
							CString clientid,CString clientpwd,CString authkey)
{//클라이언트 명 삽입
	if(m_bCon==FALSE) return FALSE;

    CString sql="";
	sql.Format("INSERT INTO BSCLIENTMT \
	            (NOGROUP,CLIENTID,CLIENTPWD,NMCLIENT, \
	             HORI,VERT,IPADDR,MACADDR, \
	             ETC,AUTHKEY,AUTH,STAT,NOADMIN,DTREG) \
	            VALUES \
	            ('','%s','%s','%s', \
	             '','','','', \
	             '','%s','%d','1','%s',NOW() ) ",clientid,clientpwd,nmclient,
				                            authkey,AUTH_ADMIN,noadmin);

	return SetQuery(sql);
}

BOOL MySql::db_InsertSched(CString noadmin,CString nmsched,CString hori,CString vert,
						   CString xmldata,CString etc)
{//스케줄 명 삽입
	if(m_bCon==FALSE) return FALSE;

    CString sql="";
	sql.Format("INSERT INTO BSSCHEDMT \
	            (NMSCHED,XMLDATA,HORI,VERT, \
	             ETC,STAT,NOADMIN,DTREG) \
	            VALUES \
				('%s','%s','%s','%s', \
	             '%s','1','%s',NOW() ) ",nmsched,xmldata,hori,vert,
				                         etc,noadmin);		
	return SetQuery(sql);
}

BOOL MySql::db_InsertDatas(CString noadmin,CString sfilename,
						   CString tfilename,CString filesize,
						   CString tpmedia,CString playtime)
{//컨텐츠 삽입
	if(m_bCon==FALSE) return FALSE;
	
	if(GetCount1("BSDATASMT","TFILENAME",tfilename)>0)
	{
		SetEditData3("BSDATASMT","PLAYTIME",playtime,
			                     "FILESIZE",filesize,
								 "TPMEDIA",tpmedia,
								 "TFILENAME",tfilename);
		return TRUE;//already exist
	}
	tfilename.Replace("'","");
	tfilename.Replace(",","");

    CString sql="";
	sql.Format("INSERT INTO BSDATASMT \
	            (SFILENAME,TFILENAME,FILESIZE,TPMEDIA,PLAYTIME, \
	             STAT,NOADMIN,DTREG) \
	             VALUES \
				 ('%s','%s','%s', \
				  '%s','%s', \
	              '1','%s',NOW() ) ",sfilename,tfilename,filesize,tpmedia,playtime,noadmin);	
	
	return SetQuery(sql);
}


BOOL MySql::db_InsertContents(CString noadmin,CString nosched,CStringArray &FileList)
{//스케줄명에 따른 컨텐츠 리스트 저장	
	if(m_bCon==FALSE) return FALSE;
    if(nosched.IsEmpty() || nosched=="") return FALSE;

    SetDelete1("POCONTSMT","NOSCHED",nosched); //기존 데이타 모드 삭제

	if(FileList.GetSize()<1) return TRUE;
	
	CString sql="";
    for(int i=0;i<FileList.GetSize();i++)
	{
		CString tfileName=FileList.GetAt(i);
		CString sfileName=GStr::str_GetFile(tfileName,'/');				
		if(tfileName.IsEmpty() || tfileName=="") continue;
		if(sfileName.IsEmpty() || sfileName=="") continue;

		int nCount=GetCount2("POCONTSMT","TFILENAME",tfileName,"NOSCHED",nosched);
		if(nCount>0) continue;

		sql="";
		sql.Format("INSERT INTO POCONTSMT (NOSCHED,SFILENAME,TFILENAME,NOADMIN,DTREG) \
		            VALUES ('%s','%s','%s','%s',NOW()) ",nosched,sfileName,tfileName,noadmin);
		
		SetQuery(sql);
	}
	return TRUE;
}

BOOL MySql::db_deleteSched(CString noclient,CString nosched)
{//스케줄 명 삭제
	if(m_bCon==FALSE) return FALSE;
	if(noclient.IsEmpty() || noclient=="")  return FALSE;
	if(nosched.IsEmpty() || nosched=="")  return FALSE;
	
	BOOL bFlag=SetDelete2("BSSCHEDMT","NOSCHED",nosched,"NOADMIN",noclient);   
	if(bFlag==TRUE)
	{//컨텐츠 데이타 삭제
		return SetDelete1("POCONTSMT","NOSCHED",nosched);   
	}
	return FALSE;
}


int MySql::db_deleteGroup(CString nogroup)
{//그룹 명 삭제
	if(m_bCon==FALSE) return FALSE;
	if(nogroup.IsEmpty() || nogroup=="")  return FALSE;
	
	if(GetCount1("BSCLIENTMT","NOGROUP",nogroup)>0) return 2;

	return SetDelete1("BSGROUPMT","NOGROUP",nogroup);   		
}

int MySql::db_deleteClient(CString noclient)
{//클라이언트 명 삭제
	if(m_bCon==FALSE) return FALSE;
	if(noclient.IsEmpty() || noclient=="")  return FALSE;
	
	//if(GetData1("BSCLIENTMT","NOCLIENT",noclient,"NOSCHED")!="") return 2;

	return SetDelete1("BSCLIENTMT","NOCLIENT",noclient);   		
}

int MySql::db_deleteAdmin(CString noclient)
{//관리자 명 삭제
	if(m_bCon==FALSE) return FALSE;
	if(noclient.IsEmpty() || noclient=="")  return FALSE;
		
	return SetDelete1("BSCLIENTMT","NOCLIENT",noclient);   		
}



int MySql::db_deleteSchedResv(CString noresvs)
{//예약 스케줄삭제
	if(m_bCon==FALSE) return FALSE;
	if(noresvs.IsEmpty() || noresvs=="")  return FALSE;
		
	return SetDelete1("PORESVSMT","NORESVS",noresvs);   		
}


int MySql::db_deleteTickerResv(CString nomessg)
{//예약 메세지삭제
	if(m_bCon==FALSE) return FALSE;
	if(nomessg.IsEmpty() || nomessg=="")  return FALSE;
		
	return SetDelete1("POMESSGMT","NOMESSG",nomessg);   		
}

BOOL MySql::db_LoginA(CString clientid,CString clientpwd,int iauth)
{//클라이언트 로그인
	
	if(m_bCon==FALSE) return FALSE;
	if(clientid.IsEmpty() || clientid=="") return FALSE;
		
	if(GetCount4("BSCLIENTMT","CLIENTID",clientid,
		                      "CLIENTPWD",clientpwd,
							  "AUTH",GStr::str_IntToStr(iauth),
							  "STAT","1")>0)
	{
		return TRUE;
	}

	return FALSE;
}

BOOL MySql::db_LoginB(CString clientid,CString clientpwd,int iauth,
					 CString &nogroup,CString &noclient,CString &nmclient,
					 CString &authkey)
{//클라이언트 로그인
	nogroup=noclient=nmclient="";
	if(m_bCon==FALSE) return FALSE;
	if(clientid.IsEmpty() || clientid=="") return FALSE;
	
	CString sql="";	
	sql.Format("SELECT * FROM BSCLIENTMT \
			    WHERE STAT='1' \
				AND CLIENTID='%s' \
	            AND CLIENTPWD='%s' \
	            AND AUTH='%d' ",clientid,clientpwd,iauth);
	
	CStringArray *list=list_Query(sql);	
	if(list_RecordCnt(list)<1) return FALSE;

	nogroup=list_FD(list,1,"NOGROUP");
	noclient=list_FD(list,1,"NOCLIENT");
	nmclient=list_FD(list,1,"NMCLIENT");
	authkey=list_FD(list,1,"AUTHKEY");
	
	list_FreeList(list);
	return TRUE;
}


CStringArray* MySql::db_Grouplist()
{//그룹 리스트
	if(m_bCon==FALSE) return NULL;
					
	CString sql="SELECT * FROM BSGROUPMT \
	            WHERE STAT='1' \
	            ORDER BY DTREG ASC ";
       
	CStringArray *list=list_Query(sql);	
	if(list_RecordCnt(list)<1) return NULL;

	return list;
}

CStringArray* MySql::db_Adminlist()
{//관리자 리스트
	if(m_bCon==FALSE) return NULL;
					
	CString sql="";
	sql.Format("SELECT * FROM BSCLIENTMT client \
	            WHERE client.STAT='1' \
	            AND client.AUTH='%d' \
	            ORDER BY client.DTREG ASC  ",AUTH_ADMIN);

	CStringArray *list=list_Query(sql);	
	if(list_RecordCnt(list)<1) return NULL;

	return list;
}



CStringArray* MySql::db_Clientlist(CString nogroup,int itype)
{//클라이언트 리스트
	if(m_bCon==FALSE) return NULL;
					
	CString sql="SELECT client.NOCLIENT  AS NOCLIENT,  \
						client.CLIENTID  AS CLIENTID,  \
						client.CLIENTPWD AS CLIENTPWD, \
						client.NMCLIENT  AS NMCLIENT,  \
						client.NOGROUP   AS NOGROUP,   \
						sgroup.NMGROUP   AS NMGROUP,   \
						client.NOSCHED   AS NOSCHED,   \
						sched.NMSCHED    AS NMSCHED,   \
						client.HORI      AS HORI,      \
						client.VERT      AS VERT,      \
						client.IPADDR    AS IPADDR,    \
						client.MACADDR   AS MACADDR,   \
						client.DISKUSAGE AS DISKUSAGE, \
						client.DTREG     AS DTREG      \
	              FROM BSCLIENTMT client \
	              LEFT JOIN BSGROUPMT sgroup ON sgroup.NOGROUP=client.NOGROUP \
	              LEFT JOIN BSSCHEDMT sched ON sched.NOSCHED=client.NOSCHED \
	              WHERE client.STAT='1' ";
	
	if(itype==0)
	{	
		CString str1="";
		if(nogroup.IsEmpty() || nogroup=="")
		{
			str1=" AND (client.NOGROUP IS NULL OR client.NOGROUP='') ";
		} 
		else
		{
			str1.Format(" AND (client.NOGROUP='%s') ",nogroup);
		}
		sql.Append(str1);
	}

	CString str2="";
	str2.Format(" AND client.AUTH='%d' \
	              ORDER BY client.DTREG ASC  ",AUTH_CLIENT);
	sql.Append(str2);
		    	    	
	CStringArray *list=list_Query(sql);	
	if(list_RecordCnt(list)<1) return NULL;

	return list;
}

CStringArray* MySql::db_Contentslist(CString nosched)
{//클라이언트 리스트
	if(m_bCon==FALSE) return NULL;

	CString sql="";
	sql.Format(" SELECT conts.NOCONTS   AS NOCONTS, \
	                    conts.NOSCHED   AS NOSCHED, \
	                    conts.SFILENAME AS SFILENAME, \
	                    conts.TFILENAME AS TFILENAME, \
	                    conts.DTREG     AS DTREG      \
	              FROM POCONTSMT conts \
	              WHERE NOSCHED='%s'  \
	              ORDER BY DTREG ASC  ",nosched);
    		    	
	CStringArray *list=list_Query(sql);	
	if(list_RecordCnt(list)<1) return NULL;

	return list;
}

CString MySql::db_GetClientSched(CString nogroup,CString noclient,CString &xmlData)
{//클라이언트별 스케줄 가져오기	
	if(m_bCon==FALSE) return "";
	
	CString sql="SELECT * FROM PORESVSMT resvs \
	             LEFT JOIN BSSCHEDMT sched ON sched.NOSCHED=resvs.NOSCHED \
	             WHERE resvs.STAT='1' \
	             AND (CASE ";

	if(noclient!="")
	{//noclient
		CString str1="";
		str1.Format(" WHEN resvs.NOCLIENT='%s' THEN 1 ",noclient);//client 우선1
		sql.Append(str1);
	}
	if(nogroup!="")
	{//group
		CString str2="";
		str2.Format(" WHEN (resvs.NOGROUP='%s' AND (resvs.NOCLIENT IS NULL OR resvs.NOCLIENT='')) THEN 2 ",nogroup);//client 우선2
		sql.Append(str2);
	}
		
	CString str3=" WHEN (resvs.NOGROUP IS NULL OR resvs.NOGROUP='') \
				       AND (resvs.NOCLIENT IS NULL OR resvs.NOCLIENT='') THEN 3 \
	              END ) \
	              ORDER BY resvs.DTREG DESC \
				  LIMIT 1 ";//한개만
	//client 우선3
	sql.Append(str3);
		
	CStringArray *list=list_Query(sql);
	if(list_ChkList(list)==FALSE) return "";//기본 스케줄
	
	CString nosched="";	
	CString CurriDay=GWin::win_CurrentTime("iday");//
	CString CurrWeek=GWin::win_CurrentTime("iweek");//1(sunday)부터시작
	CString CurrDay=GWin::win_CurrentTime("filelog");

	for(int i=1;i<list_RecordCnt(list)+1;i++)
	{					
		CString noresvs=list_FD(list,i,"NORESVS");	
		CString tpresvs=list_FD(list,i,"TPRESVS");	
		CString month=list_FD(list,i,"MONTH");//월간
		CString weeks=list_FD(list,i,"WEEKS");//주간			
		CString dtresvs=list_FD(list,i,"DTRESVS");		
		
		if(tpresvs=="5")//즉시
		{		            						
			//즉시는 모두완료처리
			SetEditData1Con2("PORESVSMT","STAT","2","NOCLIENT",noclient,"TPRESVS","5");
			nosched=list_FD(list,1,"NOSCHED");
			xmlData=list_FD(list,1,"XMLDATA");
			break;
		}
		else if(tpresvs=="4")
		{//월간			
			if(month==CurriDay)
			{
				nosched=list_FD(list,1,"NOSCHED");
				xmlData=list_FD(list,1,"XMLDATA");
				break;
			}		  
		}
		else if(tpresvs=="3")
		{//주간			
			if(weeks==CurrWeek)
			{
				nosched=list_FD(list,1,"NOSCHED");
				xmlData=list_FD(list,1,"XMLDATA");
				break;
			}
		}
		else if(tpresvs=="2")
		{//일자			
			 if(dtresvs==CurrDay)
			 {
				 nosched=list_FD(list,1,"NOSCHED");
				 xmlData=list_FD(list,1,"XMLDATA");
				 break;
			 }
		}
		else if(tpresvs=="1")
		{//매일
			 nosched=list_FD(list,1,"NOSCHED");
			 xmlData=list_FD(list,1,"XMLDATA");
			 break;
		}			
	}
	list_FreeList(list);//memory clear	
	return nosched;
}

CStringArray* MySql::db_GetClientMesg(CString nogroup,CString noclient)
{//클라이언트별 자막예약 가져오기
	if(m_bCon==FALSE) return NULL;
	if(noclient.IsEmpty() || noclient=="") return NULL;
	m_dxMesg="";

	CString sql="SELECT * FROM POMESSGMT messg WHERE STAT='1' AND (CASE ";
	
	//client 우선1
	if(noclient!="")
	{//noclient
		CString str1="";
		str1.Format(" WHEN messg.NOCLIENT='%s' THEN 1 ",noclient);//client 우선1
		sql.Append(str1);
	}
	//group  우선2
	if(nogroup!="")
	{//group
		CString str2="";
		str2.Format(" WHEN (messg.NOGROUP='%s' AND (messg.NOCLIENT IS NULL OR messg.NOCLIENT='')) THEN 2 ",nogroup);//client 우선2
		sql.Append(str2);
	}
	//All 우선3			
	CString str3=" WHEN (messg.NOGROUP IS NULL OR messg.NOGROUP='') \
				       AND (messg.NOCLIENT IS NULL OR messg.NOCLIENT='') THEN 3 \
	              END ) \
	              ORDER BY messg.DTREG DESC ";
	sql.Append(str3);
		
	CStringArray *list=list_Query(sql);
	if(list_ChkList(list)==FALSE) return NULL;
		
	CStringArray *Mesglist=new CStringArray[1];
	for(int i=1;i<list_RecordCnt(list)+1;i++)
	{	
		CString noresvs=list_FD(list,i,"NOMESSG");	
		CString tpresvs=list_FD(list,i,"TPMESSG");
		CString month=list_FD(list,i,"MONTH");//월간
		CString weeks=list_FD(list,i,"WEEKS");//주간		
		CString dtresvs=list_FD(list,i,"DTMESSG");
		CString dttime=list_FD(list,i,"DTTIME");
		CString smessg=list_FD(list,i,"MESSAGE");

		if(tpresvs=="5")
		{//즉시	            						
			//즉시는 모두완료처리
			SetEditData1Con2("POMESSGMT","STAT","2","NOCLIENT",noclient,"TPMESSG","5");						
			m_dxMesg=smessg;	
			continue;
		}
		else
		{
			CString str=tpresvs+","+month+","+weeks+","+dtresvs+","+dttime+","+smessg;
			Mesglist->Add(str);
		}		
	}	
	list_FreeList(list);//memory clear
	return Mesglist;
}

CStringArray* MySql::db_GetClientCommd(CString nogroup,CString noclient)
{//클라이언트별 작업예약 가져오기
	if(m_bCon==FALSE) return NULL;
	if(noclient.IsEmpty() || noclient=="") return NULL;

	CString sql="SELECT * FROM POCOMMDMT commd WHERE STAT='1' AND (CASE ";		
	//client 우선1
	if(noclient!="")
	{//noclient
		CString str1="";
		str1.Format(" WHEN commd.NOCLIENT='%s' THEN 1 ",noclient);//client 우선1
		sql.Append(str1);
	}
	//group  우선2
	if(nogroup!="")
	{//group
		CString str2="";
		str2.Format(" WHEN (commd.NOGROUP='%s' AND (commd.NOCLIENT IS NULL OR commd.NOCLIENT='')) THEN 2 ",nogroup);//client 우선2
		sql.Append(str2);
	}
	//All 우선3	
	CString str3=" WHEN (commd.NOGROUP IS NULL OR commd.NOGROUP='') \
				       AND (commd.NOCLIENT IS NULL OR commd.NOCLIENT='') THEN 3 \
	              END ) \
	              ORDER BY commd.DTREG DESC ";
	sql.Append(str3);	
	
	CStringArray *list=list_Query(sql);
	if(list_ChkList(list)==FALSE) return NULL;
		
	CStringArray *Commdlist=new CStringArray[1];
	for(int i=1;i<list_RecordCnt(list)+1;i++)
	{	
		CString noresvs=list_FD(list,i,"NOCOMMD");	
		CString tpresvs=list_FD(list,i,"TPCOMMD");
		CString month=list_FD(list,i,"MONTH");//월간
		CString weeks=list_FD(list,i,"WEEKS");//주간		
		CString dtresvs=list_FD(list,i,"DTCOMMD");
		CString dttime=list_FD(list,i,"DTTIME");
		CString scommd=list_FD(list,i,"COMMAND");

		if(tpresvs=="5")
		{//즉시	            						
			//즉시는 모두완료처리
			SetEditData1Con2("POCOMMDMT","STAT","2","NOCLIENT",noclient,"TPCOMMD","5");	
			continue;
		}
		else 
		{
			CString str=tpresvs+","+month+","+weeks+","+dtresvs+","+dttime+","+scommd;			
			Commdlist->Add(str);
		}			
	}	
	list_FreeList(list);//memory clear
	return Commdlist;
}


CStringArray* MySql::db_GetClientLanBoot()
{//클라이언트별 랜 부팅
	if(m_bCon==FALSE) return NULL;
	
	CString sql="SELECT * FROM POCOMMDMT commd WHERE STAT='1' AND COMMAND='CLB' ";//lan booting
	
	CStringArray *list=list_Query(sql);
	if(list_ChkList(list)==FALSE) return NULL;
		
	CStringArray *CommdLanBootlist=new CStringArray[1];
	for(int i=1;i<list_RecordCnt(list)+1;i++)
	{	
		CString noresvs=list_FD(list,i,"NOCOMMD");	
		CString tpresvs=list_FD(list,i,"TPCOMMD");
		CString month=list_FD(list,i,"MONTH");//월간
		CString weeks=list_FD(list,i,"WEEKS");//주간		
		CString dtresvs=list_FD(list,i,"DTCOMMD");
		CString dttime=list_FD(list,i,"DTTIME");
		CString scommd=list_FD(list,i,"COMMAND");		
	}
	return list;	
}


CString MySql::db_GetSchedXml(CString nosched)
{	
	if(m_bCon==FALSE) return "";
	if(nosched.IsEmpty() || nosched=="") return "";

	return GetData1("BSSCHEDMT","NOSCHED",nosched,"XMLDATA");
}


BOOL MySql::db_SetClientInfo(CString noclient,CString nosched,
							int hori,int vert,
							CString ipaddr,CString macaddr,
							CString diskusage,
							CString version)
{//클라이언트 정보저장	
	if(m_bCon==FALSE) return FALSE;

	CString sipaddr="";
	CString smacaddr="";
	if(ipaddr!="" && ipaddr!="0.0.0.0")             sipaddr.Format(" IPADDR='%s', ",ipaddr);	
	if(macaddr!="" && macaddr!="00-00-00-00-00-00") smacaddr.Format(" MACADDR='%s', ",macaddr);	
	
	CString sql="";
	sql.Format(" UPDATE BSCLIENTMT SET \
	             NOSCHED='%s', \
	             HORI='%d', \
	             VERT='%d', \
	             %s \
	             %s \
				 DISKUSAGE='%s', \
	             VERSION='%s' \
	             WHERE NOCLIENT='%s' ",nosched,hori,vert,
				                       sipaddr,smacaddr,diskusage,version,noclient); 	
	return SetQuery(sql);		
}

BOOL MySql::db_InsertLog(CString noclient,CString type,CString log,CString playtime)
{//로그등록
	if(m_bCon==FALSE) return FALSE;

	CString table="LG"+GWin::win_CurrentTime("tblog")+"MT";
	
	if(ChkTable(table)==FALSE)
	{	
	   CString sql="";
	   sql.Format("CREATE TABLE %s ( \
	               NOCLIENT varchar(4)    default NULL, \
	               TPLOG    varchar(10)   default NULL, \
                   ETC      varchar(1024) default NULL, \
	               PLAYTIME varchar(10)   default '0' , \
	               DTREG    datetime      default NULL  \
                   ) ENGINE=MyISAM DEFAULT CHARSET=euckr; ",table);		

	   if(SetQuery(sql)==FALSE) return FALSE;
	}
	
	CString sql="";
	sql.Format("INSERT INTO %s \
	            (NOCLIENT,TPLOG,ETC,PLAYTIME,DTREG) \
				VALUES \
				('%s','%s','%s','%s',NOW()); ",
				table,noclient,type,log,playtime);	

	return SetQuery(sql);	
}



CStringArray* MySql::db_Loglist(int iauth,CString noclient,CString tplogs,CString stime,CString etime)
{//로그 리스트
	if(m_bCon==FALSE) return NULL;
		
	CString stable="",etable;
	CString syear=GStr::str_GetSplit(stime,0,'-');
	CString smonth=GStr::str_GetSplit(stime,1,'-');
	
	CString eyear=GStr::str_GetSplit(etime,0,'-');
	CString emonth=GStr::str_GetSplit(etime,1,'-');
	
	syear.Delete(0,2);
	smonth=GStr::str_IntToStr(atoi(smonth),2);
	stable="LG"+syear+smonth+"MT";

	eyear.Delete(0,2);
	emonth=GStr::str_IntToStr(atoi(emonth),2);
	etable="LG"+eyear+emonth+"MT";
	
	if(syear==eyear && smonth==emonth)
	{
		if(ChkTable(stable)==FALSE) return NULL;
		
		CString sql="";
		sql.Format("SELECT logs.NOCLIENT   AS NOCLIENT, \
		                   client.NMCLIENT AS NMCLIENT, \
		                   client.CLIENTID AS CLIENTID, \
		                   logs.TPLOG      AS TPLOG, \
		                   logs.ETC        AS ETC, \
		                   logs.PLAYTIME   AS PLAYTIME, \
		                   logs.DTREG      AS DTREG \
		             FROM %s logs \
		             LEFT JOIN BSCLIENTMT client ON client.NOCLIENT=logs.NOCLIENT \
		             WHERE \
		             (client.AUTH='%d') \
		             AND (logs.DTREG>='%s' AND logs.DTREG<='%s') ",stable,iauth,stime,etime);		
	
		if(noclient!="")
		{
			CString str1="";
			str1.Format(" AND logs.NOCLIENT='%s' ",noclient);
			sql.Append(str1);

		}		
		CString str2=" ORDER BY logs.DTREG DESC \
					   LIMIT 2000 ";//2000개
		sql.Append(str2);
	    		
		CStringArray *list=list_Query(sql);	
		if(list_RecordCnt(list)<1) return NULL;

		return list;
	} 
	return NULL;
}


CStringArray* MySql::db_SchedResvslist()
{//예약 스케줄 리스트
	if(m_bCon==FALSE) return NULL;
		
	CString sql="SELECT  resvs.NORESVS     AS NORESVS, \
						 resvs.NOGROUP     AS NOGROUP, \
						 sgroup.NMGROUP    AS NMGROUP, \
						 resvs.NOCLIENT    AS NOCLIENT, \
						 client.NMCLIENT   AS NMCLIENT, \
						 client.CLIENTID   AS CLIENTID, \
						 resvs.NOADMIN     AS NOADMIN, \
						 admin.CLIENTID    AS CLIENTID, \
						 admin.NMCLIENT    AS NMADMIN, \
						 resvs.NOSCHED     AS NOSCHED, \
						 sched.NMSCHED     AS NMSCHED, \
						 resvs.TPRESVS     AS TPRESVS, \
						 resvs.DTRESVS     AS DTRESVS, \
						 resvs.WEEKS       AS WEEKS, \
						 resvs.MONTH       AS MONTH, \
						 resvs.ETC         AS ETC, \
						 resvs.STAT        AS STAT, \
						 resvs.DTREG       AS DTREG \
				FROM PORESVSMT resvs  \
			    LEFT JOIN BSGROUPMT sgroup  ON sgroup.NOGROUP=resvs.NOGROUP  \
			    LEFT JOIN BSCLIENTMT client ON client.NOCLIENT=resvs.NOCLIENT \
			    LEFT JOIN BSCLIENTMT admin  ON admin.NOCLIENT=resvs.NOADMIN \
			    LEFT JOIN BSSCHEDMT sched   ON sched.NOSCHED=resvs.NOSCHED \
			    ORDER BY resvs.DTREG DESC ";
	    	
	CStringArray *list=list_Query(sql);	
	if(list_RecordCnt(list)<1) return NULL;

	return list;	
}


BOOL MySql::db_InsertSchedResv(CString noadmin,CString nogroup,CString noclient,
							   CString tpresvs,CString nosched,CString dtresvs,
							   CString weeks)
{//예약 스케줄 등록	
	if(m_bCon==FALSE) return FALSE;

	CString sql="";
	sql.Format("INSERT INTO PORESVSMT \
	            (TPRESVS,NOGROUP,NOCLIENT,NOSCHED, \
	             DTRESVS,WEEKS,ETC,STAT,NOADMIN,DTREG) \
				 VALUES \
				 ('%s','%s','%s','%s','%s','%s','','1','%s',NOW()); ",
				 tpresvs,nogroup,noclient,nosched,dtresvs,weeks,noadmin);

   return SetQuery(sql);	
}

CStringArray* MySql::db_TickerResvslist()
{//예약 자막 리스트
	if(m_bCon==FALSE) return NULL;
		
	CString sql="SELECT mesg.NOMESSG      AS NOMESSG, \
				        mesg.NOGROUP      AS NOGROUP, \
					    sgroup.NMGROUP    AS NMGROUP, \
					    mesg.NOCLIENT     AS NOCLIENT, \
					    client.NMCLIENT   AS NMCLIENT, \
					    client.CLIENTID   AS CLIENTID, \
					    mesg.NOADMIN      AS NOADMIN, \
					    admin.CLIENTID    AS CLIENTID, \
					    admin.NMCLIENT    AS NMADMIN, \
					    mesg.TPMESSG      AS TPMESSG, \
					    mesg.DTMESSG      AS DTMESSG, \
					    mesg.DTTIME       AS DTTIME, \
					    mesg.WEEKS        AS WEEKS, \
					    mesg.MONTH        AS MONTH, \
					    mesg.MESSAGE      AS MESSAGE, \
					    mesg.ETC          AS ETC, \
					    mesg.STAT         AS STAT, \
					    mesg.DTREG        AS DTREG \
				 FROM POMESSGMT mesg \
				 LEFT JOIN BSGROUPMT sgroup  ON sgroup.NOGROUP=mesg.NOGROUP \
				 LEFT JOIN BSCLIENTMT client ON client.NOCLIENT=mesg.NOCLIENT \
				 LEFT JOIN BSCLIENTMT admin  ON admin.NOCLIENT=mesg.NOADMIN \
				 ORDER BY mesg.DTREG DESC ";

	CStringArray *list=list_Query(sql);	
	if(list_RecordCnt(list)<1) return NULL;
	
	return list;	
}

BOOL MySql::db_InsertTickerResv(CString noadmin,CString nogroup,CString noclient,
							   CString tpmessg,CString dtmessg,CString weeks,CString dttime,
							   CString sdata)
{//예약 자막 등록	
	if(m_bCon==FALSE) return FALSE;

	CString sql="";
	sql.Format("INSERT INTO POMESSGMT \
	            (TPMESSG,NOGROUP,NOCLIENT, \
	             DTMESSG,WEEKS,DTTIME, \
	             MESSAGE,ETC,STAT,NOADMIN,DTREG) \
				 VALUES \
				 ('%s','%s','%s','%s','%s','%s','%s','','1','%s',NOW()); ",
				 tpmessg,nogroup,noclient,dtmessg,weeks,dttime,sdata,noadmin);
	
    return SetQuery(sql);	
}

CStringArray* MySql::db_GetOption()
{//환경변수
	if(m_bCon==FALSE) return NULL;
		
	CString sql="SELECT * FROM BSCONFGMT WHERE STAT='1'";
	    
	CStringArray *list=list_Query(sql);	
	if(list_RecordCnt(list)<1) return NULL;
	
	return list;	
}

BOOL MySql::db_SetOption(CString variable,CString value,CString etc)
{
	if(m_bCon==FALSE) return FALSE;

	if(GetCount1("BSCONFGMT","VARIABLE",variable)>0)
	{		
		return SetEditData1("BSCONFGMT","VALUE",value,"VARIABLE",variable);
	}
	else
	{
		CString sql="";
		sql.Format("INSERT INTO BSCONFGMT (VARIABLE,VALUE,ETC,STAT,DTREG) \
		            VALUES \
					('%s','%s','%s','1',NOW()) ",variable,value,etc);

		return SetQuery(sql);	
	}
}


CStringArray* MySql::db_CommResvslist()
{//작업 예약리스트
	if(m_bCon==FALSE) return NULL;
		
	CString sql="SELECT commd.NOCOMMD     AS NOCOMMD, \
				        commd.NOGROUP     AS NOGROUP, \
					    sgroup.NMGROUP    AS NMGROUP, \
					    commd.NOCLIENT    AS NOCLIENT, \
					    client.NMCLIENT   AS NMCLIENT, \
					    client.CLIENTID   AS CLIENTID, \
					    commd.NOADMIN     AS NOADMIN, \
					    admin.CLIENTID    AS CLIENTID, \
					    admin.NMCLIENT    AS NMADMIN, \
					    commd.TPCOMMD     AS TPCOMMD, \
					    commd.DTCOMMD     AS DTCOMMD, \
					    commd.DTTIME      AS DTTIME, \
					    commd.WEEKS       AS WEEKS, \
					    commd.MONTH       AS MONTH, \
						commd.COMMAND     AS COMMAND, \
					    commd.ETC         AS ETC, \
					    commd.STAT        AS STAT, \
					    commd.DTREG       AS DTREG \
				 FROM POCOMMDMT commd \
				 LEFT JOIN BSGROUPMT sgroup  ON sgroup.NOGROUP=commd.NOGROUP \
				 LEFT JOIN BSCLIENTMT client ON client.NOCLIENT=commd.NOCLIENT \
				 LEFT JOIN BSCLIENTMT admin  ON admin.NOCLIENT=commd.NOADMIN \
				 ORDER BY commd.DTREG DESC ";

	CStringArray *list=list_Query(sql);	
	if(list_RecordCnt(list)<1) return NULL;
	
	return list;	
}

BOOL MySql::db_InsertCommdResv(CString noadmin,CString nogroup,CString noclient,
							   CString tpcommd,CString dtcommd,CString weeks,CString dttime,
							   CString sdata)
{//예약 작업 등록	
	if(m_bCon==FALSE) return FALSE;

	CString sql="";
	sql.Format("INSERT INTO POCOMMDMT \
	            (TPCOMMD,NOGROUP,NOCLIENT, \
	             DTCOMMD,WEEKS,DTTIME, \
	             COMMAND,ETC,STAT,NOADMIN,DTREG) \
				 VALUES \
				 ('%s','%s','%s','%s','%s','%s','%s','','1','%s',NOW()); ",
				 tpcommd,nogroup,noclient,dtcommd,weeks,dttime,sdata,noadmin);
	
    return SetQuery(sql);	
}


int MySql::db_deleteCommdResv(CString nocommd)
{//예약 작업 삭제
	if(m_bCon==FALSE) return FALSE;
	if(nocommd.IsEmpty() || nocommd=="")  return FALSE;
		
	return SetDelete1("POCOMMDMT","NOCOMMD",nocommd);   		
}*/


/*/////////////////////////////////////////////////////////////////////////
m_Mysql.options.write_timeout=0;
m_Mysql.options.connect_timeout=0;
m_Mysql.options.read_timeout=0;

SetQuery("SET SESSION CHARACTER_SET_CONNECTION=EUCKR");
SetQuery("SET SESSION CHARACTER_SET_CLIENT=EUCKR");
SetQuery("SET SESSION CHARACTER_SET_RESULTS=EUCKR");	
SetQuery("SET COLLATION_CONNECTION=EUCKR_KOREAN_CI");

ALTER DATABASE dreambuilder10 DEFAULT CHARACTER SET 'euckr'
ALTER TABLE BSCLIENTMT DEFAULT CHARACTER SET euckr COLLATE euckr_korean_ci;
ALTER TABLE BSCONFGMT DEFAULT CHARACTER SET euckr COLLATE euckr_korean_ci;
ALTER TABLE BSDATASMT DEFAULT CHARACTER SET euckr COLLATE euckr_korean_ci;
ALTER TABLE BSGROUPMT DEFAULT CHARACTER SET euckr COLLATE euckr_korean_ci;
ALTER TABLE BSSCHEDMT DEFAULT CHARACTER SET euckr COLLATE euckr_korean_ci;
ALTER TABLE LG0904MT DEFAULT CHARACTER SET euckr COLLATE euckr_korean_ci;
ALTER TABLE POCOMMDMT DEFAULT CHARACTER SET euckr COLLATE euckr_korean_ci;
ALTER TABLE POCONTSMT DEFAULT CHARACTER SET euckr COLLATE euckr_korean_ci;
ALTER TABLE POMESSGMT DEFAULT CHARACTER SET euckr COLLATE euckr_korean_ci;
ALTER TABLE PORESVSMT DEFAULT CHARACTER SET euckr COLLATE euckr_korean_ci;
/////////////////////////////////////////////////////////////////////////*/

/*BOOL bflag=FALSE;
	if(DBUse(_T("mysql"))==FALSE) return FALSE;	
	
	if(GetCount2("user","User",DB_NAME,"Host","%")<1)
	{	
		CString sql="";
		sql.Format("GRANT USAGE ON *.* TO '%s'@'%s' IDENTIFIED BY '%s' ",DB_NAME,"%","_dream@");		
		SetQuery(sql);	
		bflag=TRUE;
	}
	if(GetCount3("db","Host","%","Db",DB_NAME,"User",DB_NAME)<1)
	{
		CString sql="";
		sql.Format("INSERT INTO db VALUES \
				    ('%s','%s','%s','Y','Y','Y','Y','Y', \
					 'Y','Y','Y','Y','Y','Y','Y','Y','Y', \
					 'Y','Y','Y','Y','Y')","%",DB_NAME,DB_NAME);		
		SetQuery(sql);	
		bflag=TRUE;
	}
	//if(bflag) SetQuery("FLUSH PRIVILEGES");
	*/	