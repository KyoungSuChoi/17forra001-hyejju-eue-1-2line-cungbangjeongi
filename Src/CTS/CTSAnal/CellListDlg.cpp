// CellListDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "CellListDlg.h"

#include "ProgressProcDlg.h"
#include "SelCellCodeDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCellListDlg dialog
bool CCellListDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCellListDlg"), _T("TEXT_CCellListDlg_CNT"), _T("TEXT_CCellListDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CCellListDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCellListDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}


CCellListDlg::CCellListDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCellListDlg::IDD, pParent)
{
	LanguageinitMonConfig();

	//{{AFX_DATA_INIT(CCellListDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_pDBServer = NULL;
	m_pDoc = NULL;
	m_nTotalCh = 128;
	m_nMode = 0;
	m_nFailCount = 0;
	m_nNormalCount = 0;
}
CCellListDlg::~CCellListDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


void CCellListDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCellListDlg)
	DDX_Control(pDX, IDC_MODEL_NAME, m_wndModel);
	DDX_Control(pDX, IDC_TRAY_LABEL, m_wndTray);
	DDX_Control(pDX, IDC_LOT_LABEL, m_wndLot);
	DDX_Control(pDX, IDC_DATA_SELECT_COMBO, m_ctrlDataCombo);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCellListDlg, CDialog)
	//{{AFX_MSG_MAP(CCellListDlg)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_CELL_LIST_RADIO, OnCellListRadio)
	ON_BN_CLICKED(IDC_RESULT_DATA, OnResultData)
	ON_CBN_SELCHANGE(IDC_DATA_SELECT_COMBO, OnSelchangeDataSelectCombo)
	ON_BN_CLICKED(IDC_EXCEL_SAVE, OnExcelSave)
	ON_BN_CLICKED(IDC_PRINT_DATA, OnPrintData)
	ON_BN_CLICKED(IDC_PROCEDURE_HISTORY, OnProcedureHistory)
	ON_BN_CLICKED(IDC_TEMP_GAS, OnTempGas)
	ON_COMMAND(ID_USER_NONCELL_CODE, OnUserNonCellCode)
	ON_COMMAND(ID_USER_FAIL_CELL, OnUserFailCell)
	ON_COMMAND(ID_USER_NORMAL_CELL, OnUserNormalCell)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_GRID_RIGHT_CLICK, OnRButtonClickedRowCol)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCellListDlg message handlers

BOOL CCellListDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_btnSave.AttachButton(IDC_EXCEL_SAVE, stingray::foundation::SECBitmapButton::Al_Left, IDB_SAVE, this);
	m_btnPrint.AttachButton(IDC_PRINT_DATA, stingray::foundation::SECBitmapButton::Al_Left, IDB_PRINT, this);

	CRecordset rs(m_pDBServer);
	CString strQuery;
	CDBVariant val;
	strQuery.Format("SELECT TestSerialNo, LotNo, TrayNo, CellNo, ModelName FROM TestLog WHERE TestLogID = %s", m_strTestLogID);
	
	rs.Open( CRecordset::forwardOnly, strQuery);
	if(!rs.IsBOF())
	{
		rs.GetFieldValue((short)0, m_strTestSerialNo);		//Lot
		rs.GetFieldValue((short)1, m_strLotNo);		//Lot
		rs.GetFieldValue((short)2, m_strTrayNo);	//Tray
		rs.GetFieldValue((short)3, val, SQL_C_SLONG);	//Cell No
		m_nCellNo  = val.m_lVal;
		rs.GetFieldValue((short)4, m_strModelName);		//
	}
	rs.Close();

	InitListGrid();
	m_wndModel.SetText(m_strModelName);
	m_wndTray.SetText(m_strTrayNo);
	m_wndLot.SetText(m_strLotNo);

	((CButton *)GetDlgItem(IDC_CELL_LIST_RADIO))->SetCheck(TRUE);
	RequeryData(m_strTestSerialNo);

	
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCellListDlg::InitListGrid()
{
	m_wndListGrid.SubclassDlgItem(IDC_CELL_LIST_DLG, this);
	m_wndListGrid.m_bSameRowSize = FALSE;
	m_wndListGrid.m_bSameColSize = TRUE;
//---------------------------------------------------------------------//
	m_wndListGrid.m_bCustomWidth = FALSE;
	m_wndListGrid.m_bCustomColor = FALSE;

	m_wndListGrid.Initialize();
	m_wndListGrid.LockUpdate();

	SetGridType();

	m_wndListGrid.SetRowHeight(0, 0, 0);
	for(int i = 0; i < m_nFieldNo ; i++)
	{
		m_ctrlDataCombo.AddString(m_Field[i].FieldName);
	}
	m_ctrlDataCombo.SetCurSel(0);

	m_wndListGrid.EnableGridToolTips();

	//Row Header Setting
	m_wndListGrid.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(9).SetFaceName(TEXT_LANG[0])));//"굴림"

	m_wndListGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);

	m_wndListGrid.HideRows(0, GRID_ROW_OFFSET-1);
	m_wndListGrid.SetStyleRange(CGXRange().SetRows(GRID_ROW_OFFSET), CGXStyle().SetEnabled(FALSE));
	m_wndListGrid.SetStyleRange(CGXRange().SetRows(1, GRID_ROW_OFFSET-1), CGXStyle().SetHorizontalAlignment(DT_LEFT).SetFont(CGXFont().SetSize(11)));
	m_wndListGrid.LockUpdate(FALSE);
	m_wndListGrid.Redraw();
}

BOOL CCellListDlg::RequeryData(CString strTestSerialNo)
{
	if(strTestSerialNo.IsEmpty() || m_pDBServer == NULL)		return FALSE;
	
//	m_wndListGrid.SetRowCount(0);
	CString strSQL, strString, strQuery;
	int nCol, nRow;
	CRecordset rs(m_pDBServer);
	CDBVariant val;
	m_nNormalCount = 0;
	m_nFailCount = 0;


	CCalAvg	calAverage;
	int i = 0;

	strSQL = "SELECT";
	for(i=1; i<=m_nTotalCh; i++)
	{
		if(i ==1)
		{
			strString.Format(" b.ch%d", i);
		}
		else
		{
			strString.Format(", b.ch%d", i);
		}
		strSQL += strString;
	}


	for(i =0; i<m_nFieldNo; i++)
	{
		strQuery = strSQL;
		if(m_Field[i].ID == EP_REPORT_CH_CODE)		//Channel Result=> 해당 Test SerialNo의 최종 Code Search 한다.
		{
			strString.Format(" FROM ChResult b WHERE b.TestSerialNo = '%s' AND b.dataType = %d ORDER BY b.Index DESC", strTestSerialNo, EP_CH_CODE);
		}
		else if(m_Field[i].ID == EP_REPORT_GRADING)	//Channel Grade Code
		{
			strString.Format(" FROM ChResult b WHERE b.TestSerialNo = '%s' AND b.dataType = %d ORDER BY b.Index DESC", strTestSerialNo, EP_GRADE_CODE);
		}
		else	//Channel 결과 Value 검색 
		{
			strQuery = strQuery + ", a.Average, a.MaxVal, a.MinVal, a.STDD";
			strString.Format(" FROM Test a, ChValue b  WHERE b.TestIndex = a.TestIndex AND a.TestIndex = (SELECT max(c.TestIndex) FROM Test c WHERE c.TestSerialNo = '%s'", strTestSerialNo);
			strQuery += strString;
			strString.Format(" AND c.ProcType = %d AND c.DataType = %d)", m_Field[i].ID, m_Field[i].DataType);			
		}
		strQuery += strString;
		
		try
		{
			rs.Open( CRecordset::forwardOnly, _T(strQuery));
		}
       	catch (CDBException* e)
		{
			AfxMessageBox(e->m_strError);
       		e->Delete();
       		return FALSE;						//DataBase Open Fail
		}
		
		nCol = i+2;
		if(!rs.IsBOF() && !rs.IsEOF())           //처음이 아니면..(선택된것이 있으면..)
		{
			for(int j = 0; j<rs.GetODBCFieldCount( ); j++)
			{
        		if(	j >= m_nTotalCh+4)		break;

				nRow = GRID_ROW_OFFSET+j+1;

				
				switch(m_Field[i].DataType)
				{
//				case EP_OCV:
				case EP_VOLTAGE:
					rs.GetFieldValue((short)j , val, SQL_C_FLOAT);
					strString.Format("%.3f", val.m_fltVal);
					break;

				case EP_CURRENT:

					rs.GetFieldValue((short)j , val, SQL_C_FLOAT);

					if(val.m_fltVal < 0)
						strString.Format("%.1f", -val.m_fltVal);
					else
						strString.Format("%.1f", val.m_fltVal);

					break;

				case EP_STEP_TIME:
				case EP_TOT_TIME:
					rs.GetFieldValue((short)j , val, SQL_C_FLOAT);
					strString = m_pDoc->ValueString((LONG)(val.m_fltVal*10.0f), EP_STEP_TIME);

					break;

				case EP_CH_CODE:
					rs.GetFieldValue((short)j , val, SQL_C_SLONG);
					strString = m_pDoc->ChCodeMsg((unsigned short)val.m_lVal);
					if(IsNormalCell(val.m_lVal))	// 정상
					{
						m_wndListGrid.SetStyleRange(CGXRange(nRow, 2, nRow, m_nFieldNo+1),
							CGXStyle().SetTextColor(RGB(0,0,0)).SetInterior(RGB(255,255,255)));
						m_nNormalCount++;
					}
   					else if(IsNonCell(val.m_lVal))  // NonCell
					{
						m_wndListGrid.SetStyleRange(CGXRange(nRow, 2, nRow, m_nFieldNo+1),
							CGXStyle().SetTextColor(RGB(0,0,255)).SetInterior(RGB(255,255,255)));

						m_wndListGrid.SetValueRange(CGXRange(nRow, 2, nRow, m_nFieldNo+1), "");
					
					}
					else	//IsFailCell(), IsSysFail()
					{
						m_wndListGrid.SetStyleRange(CGXRange(nRow, 2, nRow, m_nFieldNo+1),
							CGXStyle().SetTextColor(RGB(255,0,0)).SetInterior(RGB(255,255,255)));
						m_nFailCount++;
					}
					break;

				case EP_GRADE_CODE:
					rs.GetFieldValue((short)j , val, SQL_C_SLONG);
					strString.Format("%c", (char)val.m_lVal);
					break;

				case EP_CAPACITY:
				case EP_IMPEDANCE:
				default:
					rs.GetFieldValue((short)j , val, SQL_C_FLOAT);
					strString.Format("%.1f", val.m_fltVal);
					break;
				}
				m_wndListGrid.SetStyleRange(CGXRange(nRow , nCol), CGXStyle().SetValue(strString));
			}
		}
		else
		{
			m_wndListGrid.SetStyleRange(CGXRange(GRID_ROW_OFFSET+1, nCol, m_wndListGrid.GetRowCount(), nCol), CGXStyle().SetValue("").SetTextColor(RGB(0,0,0)).SetInterior(RGB(255,255,255)));
		}
		rs.Close();
	}

	



	strString.Format("Tray No: %s\nLot No: %s\nModel Name: %s\n\nNormal: %d\nFail: %d",
		m_strTrayNo, m_strLotNo, m_strModelName, m_nNormalCount, m_nFailCount);

	m_wndListGrid.SetValueRange(CGXRange(1, 1), strString);

/*	strString.Format("Batch: %s", m_strLotNo);
	m_wndListGrid.SetValueRange(CGXRange(2, 1), strString);
	strString.Format("Model 명: %s", m_strModelName);
	m_wndListGrid.SetValueRange(CGXRange(3, 1), strString);
	strString.Format("정상수: %d", m_nNormalCount);
	m_wndListGrid.SetValueRange(CGXRange(5, 1), strString);
	strString.Format("불량수: %d", m_nFailCount);
	m_wndListGrid.SetValueRange(CGXRange(6, 1), strString);
*/
	return TRUE;
}

void CCellListDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	if(::IsWindow(this->GetSafeHwnd()))
	{
		RECT rect;
		::GetClientRect(m_hWnd, &rect);

		CRect rectGrid;
		if(m_wndListGrid.GetSafeHwnd())
		{
			m_wndListGrid.GetWindowRect(rectGrid);	//Step Grid Window Position
			ScreenToClient(&rectGrid);
			m_wndListGrid.MoveWindow(rectGrid.left, rectGrid.top, rect.right-rectGrid.left-10, rect.bottom - rectGrid.top - 5, FALSE);
			//m_wndListGrid.Invalidate();
		}
		Invalidate();
	}
}

void CCellListDlg::SetGridType()
{
	m_wndListGrid.LockUpdate();
	CString strTemp;
	int nCol = m_pDoc->m_nTrayColSize;

	if(m_nMode == 0)		//Cell List Type
	{
//		m_wndListGrid.m_bRowSelection = TRUE;
		m_wndListGrid.SetRowCount(GRID_ROW_OFFSET+m_nTotalCh+4);
		m_wndListGrid.SetColCount(m_nFieldNo+1);  
		
		m_wndListGrid.SetDefaultRowHeight(20);
		int i = 0;
		for(i = 0; i < m_nFieldNo ; i++)
		{
			m_wndListGrid.SetValueRange(CGXRange(GRID_ROW_OFFSET, i+2), m_Field[i].FieldName);
		}
		m_ctrlDataCombo.EnableWindow(FALSE);

//		m_wndListGrid.HideRows(0, GRID_ROW_OFFSET-1);
//		m_wndListGrid.SetStyleRange(CGXRange().SetRows(GRID_ROW_OFFSET), CGXStyle().SetEnabled(FALSE));
//		m_wndListGrid.SetCoveredCellsRowCol(1, 1, GRID_ROW_OFFSET-1, m_wndListGrid.GetColCount());
		
		if(nCol < 1)	nCol = 8;

		for(i = 0; i< m_nTotalCh; i++)
		{
			int nT = (i/nCol)/26;	//알파벳
			if(nT < 1)
			{
				strTemp.Format("%c-%02d(%03d)", 'A'+ (i/nCol)%26, i%nCol+1, m_nCellNo+i);
			}
			else
			{
				strTemp.Format("%c%c-%02d(%03d)", 'A'+nT-1, 'A'+ (i/nCol)%26, i%nCol+1, m_nCellNo+i);
			}
				
			m_wndListGrid.SetValueRange(CGXRange(i+1+GRID_ROW_OFFSET, 1), strTemp);		
		}
		m_wndListGrid.SetValueRange(CGXRange( GRID_ROW_OFFSET+m_nTotalCh+1, 1), "Avg");
		m_wndListGrid.SetValueRange(CGXRange( GRID_ROW_OFFSET+m_nTotalCh+2, 1), "Max");
		m_wndListGrid.SetValueRange(CGXRange( GRID_ROW_OFFSET+m_nTotalCh+3, 1), "Min");
		m_wndListGrid.SetValueRange(CGXRange( GRID_ROW_OFFSET+m_nTotalCh+4, 1), "STDDEV");
		m_wndListGrid.SetStyleRange(CGXRange( GRID_ROW_OFFSET+m_nTotalCh+1, 1, GRID_ROW_OFFSET+m_nTotalCh+4, m_nFieldNo+1), CGXStyle().SetEnabled(FALSE));
	}
	else
	{
//		m_wndListGrid.m_bRowSelection = FALSE;
		m_wndListGrid.SetColCount(nCol+1);  
//		int nTotCell = EP_BATTERY_PER_TRAY;
		int row = EP_BATTERY_PER_TRAY/nCol;
		if(EP_BATTERY_PER_TRAY%nCol)	
		{
			row++;
		}

		int i = 0;
		m_wndListGrid.SetRowCount(GRID_ROW_OFFSET+row);
		m_wndListGrid.SetDefaultRowHeight(30);
		for(i = 0; i < nCol ; i++)
		{
			m_wndListGrid.SetValueRange(CGXRange(GRID_ROW_OFFSET, i+2), ROWCOL(i+1));
		}
		m_ctrlDataCombo.EnableWindow(TRUE);
	
		for(i = 0; i < row ; i++)
		{
			int nT = i/26;	//알파벳
			if(nT < 1)
			{
				strTemp.Format("%c(%03d)", 'A'+ i%26, m_nCellNo+i*nCol);
			}
			else
			{
				strTemp.Format("%c%c(%03d)", 'A'+nT-1, 'A'+ i%26, m_nCellNo+i*nCol);
			}
			m_wndListGrid.SetValueRange(CGXRange(i+1+GRID_ROW_OFFSET, 1), strTemp);
		}
	}
	m_wndListGrid.SetCoveredCellsRowCol(1, 1, GRID_ROW_OFFSET-1, m_wndListGrid.GetColCount());
	m_wndListGrid.SetStyleRange(CGXRange(GRID_ROW_OFFSET, 1, m_wndListGrid.GetRowCount(), 1),
			CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(0,0,0)));

	m_wndListGrid.LockUpdate(FALSE);
	m_wndListGrid.Redraw();
}

void CCellListDlg::OnCellListRadio() 
{
	// TODO: Add your control notification handler code here
	m_nMode =0;
	SetGridType();
	RequeryData(m_strTestSerialNo);

}

void CCellListDlg::OnResultData() 
{
	// TODO: Add your control notification handler code here
	m_nMode =1;
	SetGridType();
	OnSelchangeDataSelectCombo();	
}

void CCellListDlg::OnSelchangeDataSelectCombo() 
{
	// TODO: Add your control notification handler code here
	if( m_pDBServer == NULL)		return;
	int nDataIndex = m_ctrlDataCombo.GetCurSel();
	if(nDataIndex == LB_ERR)	return;
	
	CString strSQL, strString, strQuery;
	CRecordset rs(m_pDBServer);
	CRecordset codeRs(m_pDBServer);
	CDBVariant val;
	int nRow, nCol;

	strSQL = "SELECT";	
	for(int i =1; i<=m_nTotalCh; i++)
	{
		if(i == 1)
		{
			strString.Format(" b.ch%d", i);
		}
		else
		{
			strString.Format(", b.ch%d", i);
		}
		strSQL += strString;
	}

	strQuery = strSQL;
	CString	codQuery;

	strString.Format(" FROM ChResult b WHERE b.TestSerialNo = '%s' AND b.dataType = %d ORDER BY index DESC", m_strTestSerialNo, EP_CH_CODE);
	codQuery = strQuery+strString;

	if(m_Field[nDataIndex].ID == EP_REPORT_CH_CODE)			//Channel Result
	{
//		strString.Format(" FROM ChResult b WHERE b.TestSerialNo = '%s' AND b.dataType = %d", m_strFilterString, EP_CH_CODE);
	}
	else if(m_Field[nDataIndex].ID == EP_REPORT_GRADING)	//Channel Grade Code
	{
		strString.Format(" FROM ChResult b WHERE TestSerialNo = '%s' AND dataType = %d ORDER BY Index DESC", m_strTestSerialNo, EP_GRADE_CODE);
	}
	else													//Value
	{
		strQuery = strQuery + ", a.Average, a.MaxVal, a.MinVal, a.STDD";
		strString.Format(" FROM Test a, ChValue b  WHERE b.TestIndex = a.TestIndex AND a.TestIndex = (SELECT max(c.TestIndex) FROM Test c WHERE c.TestSerialNo = '%s'", m_strTestSerialNo);
		strQuery += strString;
		strString.Format(" AND c.ProcType = %d AND c.DataType = %d)", m_Field[nDataIndex].ID, m_Field[nDataIndex].DataType);			
	}
	strQuery += strString;
 
	try
	{
		codeRs.Open( CRecordset::forwardOnly, _T(codQuery));
		rs.Open( CRecordset::forwardOnly, _T(strQuery));
	}
    catch (CDBException* e)			//DataBase Open Fail
	{
		AfxMessageBox(e->m_strError);
    	e->Delete();
     	return ;						
	}
	
	int nTotCol = m_pDoc->m_nTrayColSize;
	
	if(!rs.IsBOF() && !rs.IsEOF())           //처음이 아니면..(선택된것이 있으면..)
	{
		for(int j =0; j<rs.GetODBCFieldCount( ); j++)
		{
			if(j >= m_nTotalCh)		break;
        	nRow = j/nTotCol + 1+GRID_ROW_OFFSET;
			nCol = j%nTotCol + 2;

			
			switch(m_Field[nDataIndex].DataType)
			{
//			case EP_OCV:
			case EP_VOLTAGE:
				rs.GetFieldValue((short)j , val, SQL_C_FLOAT);
				strString.Format("%.3f", val.m_fltVal);
				break;

			case EP_CURRENT:
				rs.GetFieldValue((short)j , val, SQL_C_FLOAT);
				strString.Format("%.1f", val.m_fltVal);
				break;

			case EP_STEP_TIME:
			case EP_TOT_TIME:
				rs.GetFieldValue((short)j , val, SQL_C_FLOAT);
//				ConvertTime((LPSTR)(LPCTSTR)strString, (LONG)val.m_fltVal);
				strString = m_pDoc->ValueString((LONG)(val.m_fltVal*10.0f), EP_STEP_TIME);
				break;

			case EP_CH_CODE:
				rs.GetFieldValue((short)j , val, SQL_C_SLONG);
				strString = m_pDoc->ChCodeMsg((unsigned short)val.m_lVal);
				if(IsNormalCell((BYTE)val.m_lVal))	// 정상
				{
					m_wndListGrid.SetStyleRange(CGXRange(nRow, nCol),
						CGXStyle().SetTextColor(RGB(0,0,0)).SetInterior(RGB(255,255,255)));
				}
   				else if(IsNonCell((BYTE)val.m_lVal))  // NonCell
				{
					m_wndListGrid.SetStyleRange(CGXRange(nRow, nCol),
						CGXStyle().SetTextColor(RGB(0,0,255)).SetInterior(RGB(255,255,255)));
				}
				else	//IsFailCell(), IsSysFail()
				{
					m_wndListGrid.SetStyleRange(CGXRange(nRow, nCol),
						CGXStyle().SetTextColor(RGB(255,0,0)).SetInterior(RGB(255,255,255)));
				}
				break;

			case EP_GRADE_CODE:
				rs.GetFieldValue((short)j , val, SQL_C_SLONG);
				strString.Format("%c", (char)val.m_lVal);
				break;

			case EP_CAPACITY:
			case EP_IMPEDANCE:
			default:
				rs.GetFieldValue((short)j , val, SQL_C_FLOAT);
				strString.Format("%.1f", val.m_fltVal);
				break;
			}
			m_wndListGrid.SetStyleRange(CGXRange(nRow, nCol), CGXStyle().SetValue(strString));

			if(!codeRs.IsBOF() && !codeRs.IsEOF())
			{
				codeRs.GetFieldValue((short)j , val, SQL_C_SLONG);
//				strString = m_pDoc->ChCodeMsg((BYTE)val.m_lVal);
				if(IsNormalCell(val.m_lVal))	// 정상
				{
					m_wndListGrid.SetStyleRange(CGXRange(nRow, nCol),
						CGXStyle().SetTextColor(RGB(0,0,0)).SetInterior(RGB(255,255,255)));
				}
   				else if(IsNonCell(val.m_lVal))  // NonCell
				{
					m_wndListGrid.SetStyleRange(CGXRange(nRow, nCol),
						CGXStyle().SetTextColor(RGB(0,0,255)).SetInterior(RGB(255,255,255)));
					m_wndListGrid.SetValueRange(CGXRange(nRow, nCol), "");
				}
				else	//IsFailCell(), IsSysFail()
				{
					m_wndListGrid.SetStyleRange(CGXRange(nRow, nCol),
						CGXStyle().SetTextColor(RGB(255,0,0)).SetInterior(RGB(255,255,255)));
				}
			}
		}
	}
	else
	{
		m_wndListGrid.SetStyleRange(CGXRange(1+GRID_ROW_OFFSET, 2, 8+GRID_ROW_OFFSET, 17), CGXStyle().SetValue("").SetTextColor(RGB(0,0,0)).SetInterior(RGB(255,255,255)));
	}
	rs.Close();
	return; 	
}

void CCellListDlg::OnExcelSave() 
{
	// TODO: Add your control notification handler code here
	CString strFileName;
	strFileName.Format("%s_%s.csv", m_strLotNo, m_strTrayNo);
		
	CFileDialog pDlg(FALSE, "", strFileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "csv 파일(*.csv)|*.csv|");

	if(IDOK == pDlg.DoModal())
	{
		strFileName = pDlg.GetPathName();
		if(strFileName.IsEmpty())	return ;

		FILE *fp = fopen(strFileName, "wt");
		if(fp == NULL)	return ;

		BeginWaitCursor();

		fprintf(fp, TEXT_LANG[2]+":, %s\n", (LPSTR)(LPCTSTR)m_strModelName);	// FileName	//모델명
		fprintf(fp, TEXT_LANG[3]+":, %s\n", (LPSTR)(LPCTSTR)m_strTrayNo);//TrayNo
		fprintf(fp, TEXT_LANG[4]+":, %s\n",  (LPSTR)(LPCTSTR)m_strLotNo);			// Lot No	//Batch

//		fprintf(fp, "모듈No:, 모듈 %d-%d\n",  m_resultFileHeader.nModuleID, m_resultFileHeader.nGroupIndex+1);					// Module name 
//		fprintf(fp, "공정_조건명:,%s\n",  m_pConditionMain->conditionHeader.szName);		// 시험조건명
//		fprintf(fp, "설명:, %s\n",  m_pConditionMain->conditionHeader.szDescription);		// Battery No
//		fprintf(fp, "시작_시각:, %s\n",  m_resultFileHeader.szDateTime);		// 시작시간
//		fprintf(fp, "운영자:, %s\n\n",  m_resultFileHeader.szOperatorID);					// 총시간
		fprintf(fp, "\n");
		fprintf(fp, TEXT_LANG[5]+":, %d\n", m_nNormalCount);			// Lot No	//정상수
		fprintf(fp, TEXT_LANG[6]+":, %d\n", m_nFailCount);			// Lot No	//불량수


		fprintf(fp, "\n");
		
		for(int row = GRID_ROW_OFFSET; row<m_wndListGrid.GetRowCount()+1; row++)
		{
			for(int col = 1; col<m_wndListGrid.GetColCount()+1; col++)
			{
				fprintf(fp, "%s,", m_wndListGrid.GetValueRowCol(row, col));
			}
			fprintf(fp, "\n");
		}
		fclose(fp);
		EndWaitCursor();
	}	
}

void CCellListDlg::OnPrintData() 
{
	// TODO: Add your control notification handler code here
	CDC dc;
    CPrintDialog printDlg(FALSE);

    if (printDlg.DoModal() == IDCANCEL)         // Get printer settings from user
        return;

    dc.Attach(printDlg.GetPrinterDC());         // Attach a printer DC
    dc.m_bPrinting = TRUE;

     CString strTitle;                           // Get the application title
	if(dc.m_hDC == NULL)
	{
		strTitle.Format(TEXT_LANG[7], printDlg.GetPortName());	//"%s 장치를 사용할 수 없습니다."
		AfxMessageBox(strTitle, MB_ICONSTOP|MB_OK);
		return;
	}
   strTitle.LoadString(AFX_IDS_APP_TITLE);

    DOCINFO di;                                 // Initialise print document details
    ::ZeroMemory (&di, sizeof (DOCINFO));
    di.cbSize = sizeof (DOCINFO);
    di.lpszDocName = strTitle;

    BOOL bPrintingOK = dc.StartDoc(&di);        // Begin a new print job

    // Get the printing extents and store in the m_rectDraw field of a 
    // CPrintInfo object
    CPrintInfo Info;
	 Info.m_rectDraw.SetRect(0,0, 
                            dc.GetDeviceCaps(HORZRES), 
                            dc.GetDeviceCaps(VERTRES));
  	Info.SetMaxPage(0xffff);

	Info.m_pPD->m_pd.Flags &= ~PD_NOSELECTION;
	Info.m_pPD->m_pd.hInstance = AfxGetInstanceHandle();

	CFont font;
	font.CreateFont(70, 50, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, HANGUL_CHARSET, 0, 0, 0, VARIABLE_PITCH, TEXT_LANG[0]);  //"굴림"
	dc.SelectObject(&font);

    OnBeginPrinting(&dc, &Info);                // Call your "Init printing" function
    for (UINT page = Info.GetMinPage(); 
         page <= Info.GetMaxPage() && bPrintingOK; 
         page++)
    {
        dc.StartPage();                         // begin new page
        Info.m_nCurPage = page;
        OnPrint(&dc, &Info);                    // Call your "Print page" function
        bPrintingOK = (dc.EndPage() > 0);       // end page
    }
    OnEndPrinting(&dc, &Info);                  // Call your "Clean up" function

    if (bPrintingOK)
        dc.EndDoc();                            // end a print job
    else
        dc.AbortDoc();                          // abort job.

    dc.Detach();              	
}

void CCellListDlg::OnBeginPrinting(CDC *pDC, CPrintInfo *pInfo)
{
/*	int nXMargin = 400;
	int nYMargin = 1000;
	int nTextWidth = 150;
	int nLineCount = 0;
	CString strTemp;
	CRect strRect;
	
	strRect.SetRect(nXMargin, nYMargin + (nLineCount*nTextWidth) ,10000, nYMargin + (nLineCount+1)*nTextWidth);
	strTemp.Format("Tray ID: %s", m_strTrayNo);
	pDC->DrawText( _T(strTemp), strRect, DT_WORDBREAK | DT_EXPANDTABS);

	nLineCount++;
	strRect.SetRect(nXMargin, nYMargin + (nLineCount*nTextWidth) ,10000, nYMargin + (nLineCount+1)*nTextWidth);
	strTemp.Format("Batch: %s", m_strLotNo);
	pDC->DrawText( _T(strTemp), strRect, DT_WORDBREAK | DT_EXPANDTABS);

	nLineCount++;
	strRect.SetRect(nXMargin, nYMargin + (nLineCount*nTextWidth) ,10000, nYMargin + (nLineCount+1)*nTextWidth);
	strTemp.Format("Model 명: %s", m_strModelName);
	pDC->DrawText( _T(strTemp), strRect, DT_WORDBREAK | DT_EXPANDTABS);

	nLineCount = nLineCount+2;
	strRect.SetRect(nXMargin, nYMargin + (nLineCount*nTextWidth) ,10000, nYMargin + (nLineCount+1)*nTextWidth);
	strTemp.Format("정상수: %d", m_nNormalCount);
	pDC->DrawText( _T(strTemp), strRect, DT_WORDBREAK | DT_EXPANDTABS);

	nLineCount++;
	strRect.SetRect(nXMargin, nYMargin + (nLineCount*nTextWidth) ,10000, nYMargin + (nLineCount+1)*nTextWidth);
	strTemp.Format("불량수: %d", m_nFailCount);
	pDC->DrawText( _T(strTemp), strRect, DT_WORDBREAK | DT_EXPANDTABS);

*/
	m_wndListGrid.GetParam()->GetProperties()->SetBlackWhite(FALSE);
	int nLineCount = m_wndListGrid.GetRowCount();
	if(nLineCount > 16)
	{
		m_wndListGrid.GetParam()->GetProperties()->SetMargins(0, 10, 0, 10);
	}
	else
	{
		m_wndListGrid.GetParam()->GetProperties()->SetMargins(100, 10, 0, 10);
	}

	nLineCount = m_wndListGrid.GetColCount();

	if(nLineCount > 8)
	{
		if(700/nLineCount < 40)
		{
			m_wndListGrid.SetColWidth(1, nLineCount, 40);
		}
		else
			m_wndListGrid.SetColWidth(1, nLineCount, 700/nLineCount);
	}
	else
	{
		m_wndListGrid.SetColWidth(1, nLineCount, 80);
	}
	m_wndListGrid.HideRows(0, GRID_ROW_OFFSET-1, FALSE);
	m_wndListGrid.OnGridBeginPrinting(pDC, pInfo);

}

void CCellListDlg::OnEndPrinting(CDC *pDC, CPrintInfo *pInfo)
{
	m_wndListGrid.HideRows(0, GRID_ROW_OFFSET-1);
	m_wndListGrid.OnGridEndPrinting(pDC, pInfo);
}

void CCellListDlg::OnPrint(CDC *pDC, CPrintInfo *pInfo)
{
/*	CGXData& Footer	= m_wndListGrid.GetParam()->GetProperties()->GetDataFooter();
	Footer.StoreStyleRowCol(1, 1, CGXStyle().SetValue(""), gxOverride);
	Footer.StoreStyleRowCol(1, 2, CGXStyle().SetValue("$P/$N"),gxOverride);
	Footer.StoreStyleRowCol(1, 3, CGXStyle().SetValue("$d{%Y/%m/%d %H:%M:%S}"), gxOverride);
*/
	m_wndListGrid.OnGridPrint(pDC, pInfo);
}


void CCellListDlg::OnProcedureHistory() 
{
	// TODO: Add your control notification handler code here
	CProgressProcDlg	dlg;
	dlg.SetDataBase(m_pDBServer, m_strTestLogID);
	dlg.DoModal();
}

void CCellListDlg::OnTempGas() 
{
	// TODO: Add your control notification handler code here
	if( m_pDBServer == NULL)		return;
	
	CString strSQL;
	CRecordset rs(m_pDBServer);
	CDBVariant val1, val2;

	strSQL.Format("SELECT AVG(c.Gas), AVG(c.Temperature) FROM TestLog a, Module b, Test c WHERE a.TestLogID = %s AND a.TestLogID = b.TestLogID AND b.ProcedureID = c.ProcedureID AND b.ModuleID < 33 GROUP BY c.ProcedureID ORDER BY c.ProcedureID", m_strTestLogID);
	try
	{
		rs.Open( CRecordset::forwardOnly, _T(strSQL));
	}
    catch (CDBException* e)			//DataBase Open Fail
	{
		AfxMessageBox(e->m_strError);
    	e->Delete();
     	return ;						
	}
	CString str, msg;
	int nCount =1;
	while(!rs.IsEOF())
	{
		rs.GetFieldValue((short)0, val1, SQL_C_FLOAT);
		rs.GetFieldValue((short)1, val2, SQL_C_FLOAT);
		str.Format("%.1f, %.1f ℃\n", val1.m_fltVal, val2.m_fltVal);
		msg += str;
		nCount++;
		rs.MoveNext();
	}
	rs.Close();
	AfxMessageBox(msg);
	
	return ;
}


//Contol Context Menu를 보여 준다.
LRESULT CCellListDlg::OnRButtonClickedRowCol(WPARAM wParam, LPARAM lParam)
{
	if(m_nMode != 0)	
	{
		return 0;		//List 표기 일때만 
	}
	
	
	ROWCOL nRow, nCol;
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;
	nCol = LOWORD(wParam);
	nRow = HIWORD(wParam);
	
	ASSERT(pGrid);
	if(nCol<2 || nRow < GRID_ROW_OFFSET+1 )
	{
		return 0;
	}
	
	CPoint point;
	::GetCursorPos(&point);
	
	if (point.x == -1 && point.y == -1)
	{
		//keystroke invocation
		CRect rect;
		pGrid->GetClientRect(rect);
		pGrid->ClientToScreen(rect);

		point = rect.TopLeft();
		point.Offset(5, 5);
	}


	CMenu menu, *pPopup;

	if(pGrid == (CMyGridWnd *)&m_wndListGrid)
	{
		VERIFY(menu.LoadMenu(IDR_CELL_TREAT));
	
		pPopup = menu.GetSubMenu(0);
		ASSERT(pPopup != NULL);
		
		CWnd* pWndPopupOwner = this;
		while (pWndPopupOwner->GetStyle() & WS_CHILD)
			pWndPopupOwner = pWndPopupOwner->GetParent();

		pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, pWndPopupOwner);
	}

	return 1;
}

void CCellListDlg::OnUserNonCellCode() 
{
	// TODO: Add your command handler code here
	UserUpdateCode(EP_CODE_NONCELL);
}

void CCellListDlg::OnUserFailCell() 
{
	// TODO: Add your command handler code here
	CSelCellCodeDlg dlg;
	dlg.m_pChCodeMsg = &m_pDoc->m_apChCodeMsg;
	if(dlg.DoModal() !=	IDOK)	return;
	UserUpdateCode(dlg.m_selCode.nCode);
}

void CCellListDlg::OnUserNormalCell() 
{
	// TODO: Add your command handler code here
	UserUpdateCode(EP_CODE_END_STEP);
}

BOOL CCellListDlg::UserUpdateCode(BYTE code)
{
	ASSERT(m_pDBServer);

	CString strMsg, strSQL, strTmp;
	CRowColArray	awRows;
	CDBVariant val;

	m_wndListGrid.GetSelectedRows(awRows);

	int nSelNo = awRows.GetSize();
	int nRow;

	if(nSelNo <= 0)  
	{
		AfxMessageBox(TEXT_LANG[8]);//"선택한 Cell이 없습니다."
		return FALSE;
	}
	else if(nSelNo == 1)
	{
		strMsg.Format(TEXT_LANG[9],	//"Tray [%s]와 Lot [%s]의 Cell %s의 상태 [%s]를 [%s]로 변경 하시겠습니까?"
					   m_strTrayNo, m_strLotNo, 
					   m_wndListGrid.GetValueRowCol(awRows[0], 1), 
					   m_wndListGrid.GetValueRowCol(awRows[0], m_wndListGrid.GetColCount()),
					   m_pDoc->ChCodeMsg(code));
	}
	else
	{
		strMsg.Format(TEXT_LANG[10], //"Tray [%s]와 Lot [%s]의 선택한 %d개의 상태를 [%s]로 변경 하시겠습니까?"
					m_strTrayNo, m_strLotNo, nSelNo, m_pDoc->ChCodeMsg(code));
	}
	
	if(MessageBox(strMsg, TEXT_LANG[11], MB_ICONQUESTION|MB_YESNO) != IDYES)	//"확인"
	{
		return FALSE;
	}

	//현재 TestSerial의 Channel Code를 변경 한다.
	strSQL.Format("UPDATE ChResult SET :field_data WHERE TestSerialNo = '%s' AND dataType = %d", m_strTestSerialNo, EP_CH_CODE);	
	
	int i = 0;
	
	for(i = (nSelNo-1); i >= 0; i--)
	{
		nRow = awRows[i]-GRID_ROW_OFFSET;		//Current Selected Row
		if(nRow > 0 && nRow <= 128)
		{
			strMsg.Format("ch%d = %d, ", nRow, code);
			strTmp += strMsg;
		}
	}

	strTmp.TrimRight(", ");
	strSQL.Replace(":field_data", strTmp);
	
	try
	{
		m_pDBServer->BeginTrans();
		m_pDBServer->ExecuteSQL(strSQL);
		m_pDBServer->CommitTrans();
	}
    catch (CDBException* e)			
	{
		AfxMessageBox(e->m_strError);
    	e->Delete();
		return FALSE;
	}

	//Cell Code가 Display 가 설정 되어 있지 않으면 먼저 Update시킨 CellCode를 Read 하여 수량 Check 실시.
	BOOL	bSet = FALSE;
	for(i =0; i<m_nFieldNo; i++)
	{
		if(m_Field[i].ID == EP_REPORT_CH_CODE)		//Channel Result=> 해당 Test SerialNo의 최종 Code Search 한다.
		{
			bSet = TRUE;
		}
	}

	if(!bSet)	//Cell Code Display 설정되지 않았을 경우 최종 CellCode 검색 
	{
		strSQL = "SELECT";	
		for(int i =1; i<=m_nTotalCh; i++)
		{
			if(i ==1)
			{
				strTmp.Format(" b.ch%d", i);
			}
			else
			{
				strTmp.Format(", b.ch%d", i);
			}
			strSQL += strTmp;
		}
		strTmp.Format(" FROM ChResult b WHERE b.TestSerialNo = '%s' AND b.dataType = %d ORDER BY b.Index DESC", m_strTestSerialNo, EP_CH_CODE);
		strSQL += strTmp;

		CRecordset rs(m_pDBServer);
		
		try
		{
			rs.Open( CRecordset::forwardOnly, _T(strSQL));
		}
		catch (CDBException* e)
		{
			AfxMessageBox(e->m_strError);
    		e->Delete();
    		return FALSE;						//DataBase Open Fail
		}

		m_nNormalCount = 0;
		m_nFailCount = 0;

		if(!rs.IsEOF())  
		{
			for(int j = 0; j<rs.GetODBCFieldCount( ); j++)
			{
				rs.GetFieldValue((short)j , val, SQL_C_SLONG);
				strTmp = m_pDoc->ChCodeMsg((BYTE)val.m_lVal);
				if(IsNormalCell((BYTE)val.m_lVal))	// 정상
				{
					m_nNormalCount++;
				}
 				else if(IsNonCell((BYTE)val.m_lVal))  // NonCell
				{
				}
				else	//IsFailCell(), IsSysFail()
				{
					m_nFailCount++;
				}
			}
		}
		rs.Close();
	}
	
	//2004/5/7
	//최종 공정의 Cell 수량도 Update 하여야 함 
		

	//2004/4/21 V 1.7.0
	//현재 Tray의  Cell 상태도 변경하여 다음공정에서 수정 되도록 수정 
	CDatabase conditionDB;
	try
	{
		conditionDB.Open(ODBC_SCH_DB_NAME);
	}
	catch (CDBException* e)
	{
    	AfxMessageBox(e->m_strError);
    	e->Delete();     //DataBase Open Fail
		return FALSE;
	}

	//현재 진행 중인 Tray의 CellState 정보를 변경 한다.
	conditionDB.BeginTrans();
	try
	{
		for(i = (nSelNo-1); i >= 0; i--)
		{
			nRow = awRows[i]-GRID_ROW_OFFSET;		//Current Selected Row
			if(nRow > 0 && nRow <= 128)
			{
				strSQL.Format("UPDATE Tray a, CellState b SET b.CellCode = %d WHERE a.TestSerialNo = '%s' AND a.TrayNo = '%s' AND a.TraySerial = b.TraySerial AND b.ChNo = %d", 
								code, m_strTestSerialNo, m_strTrayNo, nRow-1);	
				conditionDB.ExecuteSQL(strSQL);
			}
		}

		//Tray 수량을 변경 한다. //CellCode Diaplay Check를 하지 않았으면 수량 Update에 문제 있음 
		strSQL.Format("UPDATE Tray SET NormalCount = %d, FailCount = %d WHERE TestSerialNo = '%s' AND TrayNo = '%s'", 
						m_nNormalCount,  m_nFailCount, m_strTestSerialNo, m_strTrayNo);
		
		conditionDB.ExecuteSQL(strSQL);
	}
    catch (CDBException* e)			
	{
		conditionDB.Close();
		AfxMessageBox(e->m_strError);
    	e->Delete();
		return FALSE;
	}
	
	conditionDB.CommitTrans();
	conditionDB.Close();
	///////////////////////////////////

	//Data 다시 표시 	
	RequeryData(m_strTestSerialNo);

	return TRUE;
}
