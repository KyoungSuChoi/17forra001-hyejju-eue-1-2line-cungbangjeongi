//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by BFGraphData.rc
//
#define IDD_RESTULT_SEL_DLG             200
#define IDC_DATA_FOLER_SEL_BUTTON       1001
#define IDC_CUR_FOLDER                  1002
#define IDC_RESULT_TEST_LIST            1024
#define IDD_DATA_QUERY_DIALOG           13000
#define IDC_TEST_NAME_LIST              13000
#define IDC_TEST_FROM_LIST_BTN          13001
#define IDC_TEST_FROM_FOLDER_BTN        13002
#define IDB_LIST_UP                     13002
#define IDC_CHANNEL_LIST                13003
#define IDB_FOLDER_UP                   13003
#define IDB_FOLDER_DOWN                 13004
#define IDC_CYCLE_FROM_EDIT             13005
#define IDB_LIST_DOWN                   13005
#define IDC_CYCLE_TO_EDIT               13006
#define IDB_FOLDER_FOCUS                13006
#define IDC_CYCLE_INTERVAL_EDIT         13007
#define IDB_LIST_FOCUS                  13007
#define IDC_SEL_CYCLE_LIST              13008
#define IDB_NETWORK                     13008
#define IDC_CYCLE_STRING_EDIT           13009
#define IDB_HDD                         13009
#define IDD_SORT_TYPE_DLG               13009
#define IDC_ADD_SEL_CYCLE_BTN           13010
#define IDB_RUN                         13010
#define IDR_POPUP_MENU                  13010
#define IDC_DEL_SEL_CYCLE_BTN           13011
#define IDC_CYCLE_STRING_CHECK          13012
#define IDI_FOLDER                      13015
#define IDI_FOLDER2                     13016
#define IDC_START_TYPE_RADIO            13020
#define IDC_START_TYPE_RADIO1           13021
#define IDC_MODE_RADIO                  13022
#define IDC_MODE_RADIO1                 13023
#define IDC_MODE_RADIO2                 13024
#define IDC_OPEN_TIME_RADIO             13025
#define IDC_OPEN_TIME_RADIO1            13026
#define IDC_X_AXIS_COMBO                13027
#define IDC_X_UNIT_COMBO                13028
#define IDC_Y_AXIS_LIST                 13029
#define IDC_Y_UNIT_COMBO                13030
#define IDC_COMM_LIST                   13031
#define IDC_FIRST1                      13034
#define IDC_FIRST2                      13035
#define IDC_SECOND1                     13036
#define IDC_STATIC_LINE                 13037
#define IDC_SECOND2                     13037
#define IDC_VOLTAGE1_CHECK              13038
#define IDC_VOLTAGE2_CHECK              13039
#define IDC_LABEL_TOP                   13039
#define IDC_VOLTAGE3_CHECK              13040
#define IDC_EDIT_SIMPLE                 13040
#define IDC_VOLTAGE4_CHECK              13041
#define IDC_TEMPERATURE1_CHECK          13042
#define IDC_TEMPERATURE2_CHECK          13043
#define IDC_BTN_SEARCH                  13044
#define IDC_EDIT_SEARCH_KEY             13046
#define IDC_RANGE_TYPE_RADIO1           13050
#define IDC_RANGE_TYPE_RADIO2           13051
#define IDC_SEL_COUNT_STATIC            13052
#define IDC_CHARGE_CHECK                13053
#define IDC_DISCHARGE_CHECK             13054
#define IDC_REST_CHECK                  13055
#define IDC_START_POINT_GROUP_STATIC    13057
#define IDC_CYCLE_WARRING_STATIC        13058
#define IDC_IMP_CHECK                   13059
#define IDC_PATTERN_CHECK               13060
#define IDC_OCV_CHECK                   13061
#define IDC_CHECK_CHECK                 13062
#define IDC_BTN_EXCEL_EXPORT            13063
#define IDM_DATA_BRING                  32771
#define IDM_DATA_MOVE                   32772
#define IDM_DATA_DELETE                 32773
#define IDM_DATA_DESTROY                32777

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        13017
#define _APS_NEXT_COMMAND_VALUE         32778
#define _APS_NEXT_CONTROL_VALUE         13064
#define _APS_NEXT_SYMED_VALUE           13000
#endif
#endif
