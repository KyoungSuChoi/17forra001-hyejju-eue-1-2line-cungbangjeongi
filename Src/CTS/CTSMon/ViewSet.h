#pragma once

// CVewSet 대화 상자입니다.

class CViewSet : public CDialog
{
	DECLARE_DYNAMIC(CViewSet)

public:
	CViewSet(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CViewSet();

	// 대화 상자 데이터입니다.
	enum { IDD = IDD_VIEW_SET };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	virtual BOOL OnInitDialog();
	BOOL m_LayoutView;
	BOOL m_DataDown_Message;
	BOOL m_RackIndexUp;
	BOOL m_Small_ch_layout;
	BOOL m_Show_ChannelMonitoringView;
	BOOL m_Show_ChannelResultView;
	BOOL m_Show_SbcInfo;
	BOOL m_Show_Sensor;
	BOOL m_Show_Temp;

	int m_Lang;

	CButton m_Check_Korea;
	CButton m_Check_English;
	CButton m_Check_Chinese;

	void Langinit();
	void Reginit();
	void LangSet();
	void RegSet();

	int m_CellInTray;
	int m_TopGridColCout;
	afx_msg void OnBnClickedCancel();
	int m_Module_Per_Rack;
	afx_msg void OnBnClickedCheckKor();
	afx_msg void OnBnClickedCheckEng();
	afx_msg void OnBnClickedCheckChin();
	CComboBox m_ComboDeviceId;
	CComboBox m_CbMachineTraytype;
	UINT m_nTemperatureSaveInterval;
};
