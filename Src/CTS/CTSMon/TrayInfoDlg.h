#if !defined(AFX_TRAYINFODLG_H__9D19D61B_77B2_43D7_8BB1_A4CAFE5D19DD__INCLUDED_)
#define AFX_TRAYINFODLG_H__9D19D61B_77B2_43D7_8BB1_A4CAFE5D19DD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TrayInfoDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTrayInfoDlg dialog
#include "MyGridWnd.h"

class CTrayInfoDlg : public CDialog
{
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();
	virtual ~CTrayInfoDlg();

// Construction
public:
	void UpdateProcessState();
	int GetCurProcessRow();
	void InitCellGrid();
	void InitProcessGrid();
	CTrayInfoDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CTrayInfoDlg)
	enum { IDD = IDD_TRAY_INFO_DIALOG };
	CString	m_strTrayNo;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTrayInfoDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CTray m_TrayInfo;
	CMyGridWnd m_wndProcessGrid;
	CMyGridWnd m_wndCellStateGrid;
	// Generated message map functions
	//{{AFX_MSG(CTrayInfoDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSearchBtn();
	afx_msg void OnButtonCancle2();
	afx_msg void OnButtonCancle();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TRAYINFODLG_H__9D19D61B_77B2_43D7_8BB1_A4CAFE5D19DD__INCLUDED_)
