// CellGradeListDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "CellGradeListDlg.h"
#include "PrntScreen.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCellGradeListDlg dialog

bool CCellGradeListDlg::LanguageinitMonConfig()
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCellGradeListDlg"), _T("TEXT_CCellGradeListDlg_CNT"), _T("TEXT_CCellGradeListDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CCellGradeListDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCellGradeListDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}




CCellGradeListDlg::CCellGradeListDlg(CCTSAnalDoc *pDoc, CWnd* pParent /*=NULL*/)
	: CDialog(CCellGradeListDlg::IDD, pParent)
{
	LanguageinitMonConfig();

	ASSERT(pDoc);
	m_pDoc = pDoc;
	//{{AFX_DATA_INIT(CCellGradeListDlg)
	m_strTrayNo = _T("");
	m_strLotNo = _T("");
	//}}AFX_DATA_INIT
	m_nTrayType = TRAY_TYPE_8_BY_32;
//	m_nTrayType = TRAY_TYPE_16_BY_16;

	m_nNormalCount = 0;
	m_nFailCount = 0;
	m_nNonCellCount = 0;
	m_nTrayColSize = EP_DEFAULT_TRAY_COL_COUNT;
	m_nChannelSize = EP_DEFAULT_CH_PER_MD;
	ZeroMemory(m_cellStateCount, sizeof(m_cellStateCount));
}
CCellGradeListDlg::~CCellGradeListDlg(){	if( TEXT_LANG != NULL )	{		delete[] TEXT_LANG;		TEXT_LANG = NULL;	}}

void CCellGradeListDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCellGradeListDlg)
	DDX_Control(pDX, IDC_DELTA_OCV2_FAIL, m_ctrlDeltaOCV2);
	DDX_Control(pDX, IDC_OCV4_FAIL, m_ctrlOCV4Fail);
	DDX_Control(pDX, IDC_IMPEDANCE_FAIL, m_ctrlImpFail);
	DDX_Control(pDX, IDC_DISCHARGE_CAP_FAIL, m_ctrlDisCapFail);
	DDX_Control(pDX, IDC_DELTA_OCV1_FAIL, m_ctrlDeltaOCV1Fail);
	DDX_Control(pDX, IDC_OCV1_FAIL_COUNT, m_ctrlOCV1Fail);
	DDX_Control(pDX, IDC_INPUT_COUNT, m_ctrlInputCell);
	DDX_Control(pDX, IDC_FAULT_COUNT_LABEL, m_faultCount);
	DDX_Control(pDX, IDC_NORMAL_COUNT_LABEL, m_normalCout);
	DDX_Text(pDX, IDC_EDIT1, m_strTrayNo);
	DDX_Text(pDX, IDC_EDIT2, m_strLotNo);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCellGradeListDlg, CDialog)
	//{{AFX_MSG_MAP(CCellGradeListDlg)
	ON_BN_CLICKED(IDC_PRINT_RESULT, OnPrintResult)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCellGradeListDlg message handlers

BOOL CCellGradeListDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_faultCount.SetTextColor(RGB(255, 0, 0));
	m_normalCout.SetTextColor(RGB(0, 0, 255));
	m_ctrlDeltaOCV2.SetTextColor(RGB(255, 0, 0));
	m_ctrlOCV4Fail.SetTextColor(RGB(255, 0, 0));
	m_ctrlImpFail.SetTextColor(RGB(255, 0, 0));
	m_ctrlDisCapFail.SetTextColor(RGB(255, 0, 0));
	m_ctrlDeltaOCV1Fail.SetTextColor(RGB(255, 0, 0));
	m_ctrlOCV1Fail.SetTextColor(RGB(255, 0, 0));
//	m_ctrlInputCell;		//Black

	m_nTrayColSize = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "TrayColSize", EP_DEFAULT_TRAY_COL_COUNT);
	
	InitChannelGrid();
	DisplayData();
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CCellGradeListDlg::InitChannelGrid()
{
	m_ChannelGrid.SubclassDlgItem(IDC_CELL_GRADE_LIST, this);
	m_ChannelGrid.m_bSameRowSize = TRUE;
	m_ChannelGrid.m_bSameColSize = TRUE;
//---------------------------------------------------------------------//
	m_ChannelGrid.m_bCustomWidth = FALSE;
	m_ChannelGrid.m_bCustomColor = FALSE;

	m_ChannelGrid.Initialize();
	m_ChannelGrid.LockUpdate();
	
	int rowsize;
	rowsize = m_nChannelSize/m_nTrayColSize;
	if(m_nChannelSize%m_nTrayColSize > 0)	rowsize++;

	m_ChannelGrid.SetRowCount(rowsize);
	m_ChannelGrid.SetColCount(m_nTrayColSize*2);
	
	m_ChannelGrid.SetDefaultRowHeight(20);
	m_ChannelGrid.EnableGridToolTips();

	CString strTemp;
/*	for(int i = 1; i<m_ChannelGrid.GetColCount(); i++)
	{
		strTemp.Format("%c", 'A'+i-1);
		m_ChannelGrid.SetValueRange(CGXRange(0, i+1), strTemp);
	}
	m_ChannelGrid.SetValueRange(CGXRange(0,1), "/");
	
	for( i = 0; i<m_ChannelGrid.GetRowCount(); i++)
	{
		strTemp.Format("%d", i+1);
		m_ChannelGrid.SetValueRange(CGXRange(i+1, 1), strTemp);
	}

	m_ChannelGrid.SetValueRange(CGXRange(0,1), "/");
*/
	int nCh = 0;
	m_ChannelGrid.HideRows(0, 0);

#ifdef DSP_TYPE_VERTICAL
	for(int j = 0; j<m_ChannelGrid.GetColCount()/2; j++)
	{
		for(int i = 0; i<m_ChannelGrid.GetRowCount(); i++)
		{
#else
	for(int i = 0; i<m_ChannelGrid.GetRowCount(); i++)
	{
		for(int j = 0; j<m_ChannelGrid.GetColCount()/2; j++)
		{
#endif
			if(nCh >= m_nChannelSize)	break;
			strTemp.Format("%d", nCh+1);
			m_ChannelGrid.SetValueRange(CGXRange(i+1, j*2+1), strTemp);
			nCh++;
			m_ChannelGrid.SetStyleRange(CGXRange(i+1, j*2+1),
					CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(255,255,255)).SetInterior(RGB(192,192,192)).SetDraw3dFrame(gxFrameRaised));
		}
	}

	m_ChannelGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	//Row Header Setting
	m_ChannelGrid.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(9).SetFaceName(TEXT_LANG[0])));//굴림
	
	m_ChannelGrid.LockUpdate(FALSE);
	m_ChannelGrid.Redraw();
}

BOOL CCellGradeListDlg::DisplayData()
{
	int nChNo = 0;
	CString strTemp;
	int nNonCellCount = 0, nBadCellCount = 0;

#ifdef DSP_TYPE_VERTICAL
	for(int col = 0; col<m_ChannelGrid.GetColCount()/2; col++)
	{
		for(int row = 0; row < m_ChannelGrid.GetRowCount(); row++)
		{
#else 
	for(int row = 0; row < m_ChannelGrid.GetRowCount(); row++)
	{
		for(int col = 0; col<m_ChannelGrid.GetColCount()/2; col++)
		{
#endif
			if(nChNo >= m_nChannelSize)	break;
			m_ChannelGrid.SetStyleRange(CGXRange(row+1, (col+1)*2), 
										CGXStyle().SetValue(::GetSelectCodeName(cellState[nChNo]))
													.SetInterior(m_pDoc->GetGradeColor(m_pDoc->ValueString(cellState[nChNo], EP_GRADE_CODE))));	
			
						
			if(::IsSelectBadCell (cellState[nChNo]))
			{
				//m_ChannelGrid.SetStyleRange(CGXRange(row+1, (col+1)*2), CGXStyle().SetTextColor(RGB(0, 0, 0)).SetInterior(RGB(255, 150, 150)));		
				nBadCellCount++;
			}
			if(cellState[nChNo] == EP_SELECT_CODE_NONCELL)	nNonCellCount++;
			
			if(cellState[nChNo] >=0 && cellState[nChNo] < EP_MAX_CH_PER_MD)
				m_cellStateCount[cellState[nChNo]]++;		//각 코드의 cell 수를 계산
			
			nChNo++;
		}
	}

	strTemp.Format("%d"+TEXT_LANG[1], m_cellStateCount[EP_SELECT_CODE_DELTA_OCV2]);//개
	m_ctrlDeltaOCV2.SetText(strTemp);
	strTemp.Format("%d"+TEXT_LANG[1], m_cellStateCount[EP_SELECT_CODE_OCV_FAIL4]);
	m_ctrlOCV4Fail.SetText(strTemp);
	strTemp.Format("%d"+TEXT_LANG[1], m_cellStateCount[EP_SELECT_CODE_IMPEDANCE]);
	m_ctrlImpFail.SetText(strTemp);
	strTemp.Format("%d"+TEXT_LANG[1], m_cellStateCount[EP_SELECT_CODE_CAP_FAIL]);
	m_ctrlDisCapFail.SetText(strTemp);
	strTemp.Format("%d"+TEXT_LANG[1], m_cellStateCount[EP_SELECT_CODE_DELTA_OCV1]);
	m_ctrlDeltaOCV1Fail.SetText(strTemp);
	strTemp.Format("%d"+TEXT_LANG[1], m_cellStateCount[EP_SELECT_CODE_OCV_FAIL1]);
	m_ctrlOCV1Fail.SetText(strTemp);
	strTemp.Format("%d"+TEXT_LANG[1], m_cellStateCount[EP_SELECT_CODE_SAFETY_FAULT]);
	m_faultCount.SetText(strTemp);
	strTemp.Format("%d"+TEXT_LANG[1], m_nChannelSize - nNonCellCount);
	m_ctrlInputCell.SetText(strTemp);
	m_nNormalCount = m_nChannelSize - nBadCellCount;		//NonCell이 포함된 수
	strTemp.Format("%d"+TEXT_LANG[1], m_nNormalCount);
	m_normalCout.SetText(strTemp);

	m_nFailCount = nBadCellCount - nNonCellCount;
	m_nNonCellCount = nNonCellCount;

	return TRUE;
}

void CCellGradeListDlg::OnPrintResult() 
{
	// TODO: Add your control notification handler code here
/*	CDC dc;
    CPrintDialog printDlg(FALSE);

    if (printDlg.DoModal() == IDCANCEL)         // Get printer settings from user
        return;

    dc.Attach(printDlg.GetPrinterDC());         // Attach a printer DC
    dc.m_bPrinting = TRUE;

    CString strTitle;                           // Get the application title
	if(dc.m_hDC == NULL)
	{
		strTitle.Format("%s 장치를 사용할 수 없습니다.", printDlg.GetPortName());
		AfxMessageBox(strTitle, MB_ICONSTOP|MB_OK);
		return;
	}
    strTitle.LoadString(AFX_IDS_APP_TITLE);

    DOCINFO di;                                 // Initialise print document details
    ::ZeroMemory (&di, sizeof (DOCINFO));
    di.cbSize = sizeof (DOCINFO);
    di.lpszDocName = strTitle;
	

    BOOL bPrintingOK = dc.StartDoc(&di);        // Begin a new print job

    // Get the printing extents and store in the m_rectDraw field of a 
    // CPrintInfo object
    CPrintInfo Info;
    Info.m_rectDraw.SetRect(0,0, 
                            dc.GetDeviceCaps(HORZRES), 
                         dc.GetDeviceCaps(VERTRES));
   	Info.SetMaxPage(0xffff);

	Info.m_pPD->m_pd.Flags &= ~PD_NOSELECTION;
	Info.m_pPD->m_pd.hInstance = AfxGetInstanceHandle();

	
	
	
	CFont font;
	font.CreateFont(75, 45, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, HANGUL_CHARSET, 0, 0, 0, VARIABLE_PITCH, "굴림");
	dc.SelectObject(&font);

    OnBeginPrinting(&dc, &Info);                // Call your "Init printing" funtion
    for (UINT page = Info.GetMinPage(); 
         page <= Info.GetMaxPage() && bPrintingOK; 
         page++)
    {
        dc.StartPage();                         // begin new page
        Info.m_nCurPage = page;
        OnPrint(&dc, &Info);                    // Call your "Print page" function
        bPrintingOK = (dc.EndPage() > 0);       // end page
    }
    OnEndPrinting(&dc, &Info);                  // Call your "Clean up" funtion

    if (bPrintingOK)
        dc.EndDoc();                            // end a print job
    else
        dc.AbortDoc();                          // abort job.

    dc.Detach();      
	*/
	 CPrntScreen * ScrCap;
     ScrCap = new CPrntScreen("Impossible to print!","Error!");
     ScrCap->DoPrntScreen(1,2,TRUE);
     delete ScrCap;
     ScrCap = NULL;	

}
/*
void CCellGradeListDlg::OnBeginPrinting(CDC *pDC, CPrintInfo *pInfo)
{
	int nXMargin = 400;
	int nYMargin = 600;
	int nTextHeight = 120;
	int nLineCount = 0;
	CString strTemp;
	CRect strRect;
	
	strRect.SetRect(nXMargin, nYMargin + (nLineCount*nTextHeight) ,10000, nYMargin + (nLineCount+1)*nTextHeight);
	strTemp.Format("Tray ID: %s", m_strTrayNo);
	pDC->DrawText( _T(strTemp), strRect, DT_WORDBREAK | DT_EXPANDTABS);

	nLineCount++;
	strRect.SetRect(nXMargin, nYMargin + (nLineCount*nTextHeight) ,10000, nYMargin + (nLineCount+1)*nTextHeight);
	strTemp.Format("Batch: %s", m_strLotNo);
	pDC->DrawText( _T(strTemp), strRect, DT_WORDBREAK | DT_EXPANDTABS);

	nLineCount = nLineCount+2;
	strRect.SetRect(nXMargin, nYMargin + (nLineCount*nTextHeight) ,10000, nYMargin + (nLineCount+1)*nTextHeight);
	strTemp.Format("투입수량:  %d", EP_MAX_CH_PER_MD - m_nNonCellCount);
	pDC->DrawText( _T(strTemp), strRect, DT_WORDBREAK | DT_EXPANDTABS);

	nLineCount++;
	strRect.SetRect(nXMargin, nYMargin + (nLineCount*nTextHeight) ,10000, nYMargin + (nLineCount+1)*nTextHeight);
	strTemp.Format("양품(A~D):  %d", m_nNormalCount);
	pDC->DrawText( _T(strTemp), strRect, DT_WORDBREAK | DT_EXPANDTABS);

	nLineCount++;
	strRect.SetRect(nXMargin, nYMargin + (nLineCount*nTextHeight) ,10000, nYMargin + (nLineCount+1)*nTextHeight);
	strTemp.Format("OCV1 불량(S):  %d", m_cellStateCount[EP_SELECT_CODE_DELTA_OCV1]);
	pDC->DrawText( _T(strTemp), strRect, DT_WORDBREAK | DT_EXPANDTABS);

	nLineCount++;
	strRect.SetRect(nXMargin, nYMargin + (nLineCount*nTextHeight) ,10000, nYMargin + (nLineCount+1)*nTextHeight);
	strTemp.Format("DeltaOCV(1-2)(E):  %d", m_cellStateCount[EP_SELECT_CODE_DELTA_OCV1]);
	pDC->DrawText( _T(strTemp), strRect, DT_WORDBREAK | DT_EXPANDTABS);

	nLineCount++;
	strRect.SetRect(nXMargin, nYMargin + (nLineCount*nTextHeight) ,10000, nYMargin + (nLineCount+1)*nTextHeight);
	strTemp.Format("방전용량불량(F):  %d", m_cellStateCount[EP_SELECT_CODE_DISCHARGE_CAP]);
	pDC->DrawText( _T(strTemp), strRect, DT_WORDBREAK | DT_EXPANDTABS);

	nLineCount++;
	strRect.SetRect(nXMargin, nYMargin + (nLineCount*nTextHeight) ,10000, nYMargin + (nLineCount+1)*nTextHeight);
	strTemp.Format("임피던스불량(I):  %d", m_cellStateCount[EP_SELECT_CODE_IMPEDANCE]);
	pDC->DrawText( _T(strTemp), strRect, DT_WORDBREAK | DT_EXPANDTABS);

	nLineCount++;
	strRect.SetRect(nXMargin, nYMargin + (nLineCount*nTextHeight) ,10000, nYMargin + (nLineCount+1)*nTextHeight);
	strTemp.Format("OCV4불량(G):  %d", m_cellStateCount[EP_SELECT_CODE_OCV_FAIL4]);
	pDC->DrawText( _T(strTemp), strRect, DT_WORDBREAK | DT_EXPANDTABS);

	nLineCount++;
	strRect.SetRect(nXMargin, nYMargin + (nLineCount*nTextHeight) ,10000, nYMargin + (nLineCount+1)*nTextHeight);
	strTemp.Format("DeltaOCV(3-4)(E1):  %d", m_cellStateCount[EP_SELECT_CODE_DELTA_OCV2]);
	pDC->DrawText( _T(strTemp), strRect, DT_WORDBREAK | DT_EXPANDTABS);

	nLineCount++;
	strRect.SetRect(nXMargin, nYMargin + (nLineCount*nTextHeight) ,10000, nYMargin + (nLineCount+1)*nTextHeight);
	strTemp.Format("안전불량(N):  %d", m_cellStateCount[EP_SELECT_CODE_SAFETY_FAULT]);
	pDC->DrawText( _T(strTemp), strRect, DT_WORDBREAK | DT_EXPANDTABS);

	m_ChannelGrid.GetParam()->GetProperties()->SetBlackWhite(FALSE);
	m_ChannelGrid.GetParam()->GetProperties()->SetMargins(180,10,80,10);
	m_ChannelGrid.SetColWidth(1, m_ChannelGrid.GetColCount(), 40);
	m_ChannelGrid.OnGridBeginPrinting(pDC, pInfo);

}

void CCellGradeListDlg::OnEndPrinting(CDC *pDC, CPrintInfo *pInfo)
{
	m_ChannelGrid.OnGridEndPrinting(pDC, pInfo);
	
}

void CCellGradeListDlg::OnPrint(CDC *pDC, CPrintInfo *pInfo)
{
	m_ChannelGrid.OnGridPrint(pDC, pInfo);
}
*/