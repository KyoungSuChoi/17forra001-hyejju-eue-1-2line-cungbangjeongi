// TrayInfoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ctsmon.h"
#include "TrayInfoDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTrayInfoDlg dialog


CTrayInfoDlg::CTrayInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTrayInfoDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTrayInfoDlg)
	m_strTrayNo = _T("");
	//}}AFX_DATA_INIT
	LanguageinitMonConfig();
}

CTrayInfoDlg::~CTrayInfoDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CTrayInfoDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CTrayInfoDlg"), _T("TEXT_CTrayInfoDlg_CNT"), _T("TEXT_CTrayInfoDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CTrayInfoDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CTrayInfoDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}


void CTrayInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTrayInfoDlg)
	DDX_Text(pDX, IDC_EDIT1, m_strTrayNo);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTrayInfoDlg, CDialog)
	//{{AFX_MSG_MAP(CTrayInfoDlg)
	ON_BN_CLICKED(IDC_SEARCH_BTN, OnSearchBtn)
	ON_BN_CLICKED(IDC_BUTTON_CANCLE2, OnButtonCancle2)
	ON_BN_CLICKED(IDC_BUTTON_CANCLE, OnButtonCancle)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTrayInfoDlg message handlers

BOOL CTrayInfoDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	InitProcessGrid();
	InitCellGrid();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CTrayInfoDlg::InitProcessGrid()
{
	m_wndProcessGrid.SubclassDlgItem(IDC_PROCESS_GRID, this);	
	m_wndProcessGrid.m_bSameColSize  = FALSE;
	m_wndProcessGrid.m_bSameRowSize  = FALSE;
	m_wndProcessGrid.m_bCustomWidth = TRUE;
	m_wndProcessGrid.m_nWidth[1] = 50;
	m_wndProcessGrid.m_nWidth[2] = 150;
	m_wndProcessGrid.m_nWidth[3] = 100;

	m_wndProcessGrid.Initialize();
	m_wndProcessGrid.SetRowCount(0);
	m_wndProcessGrid.SetColCount(3);
	m_wndProcessGrid.SetDefaultRowHeight(18);
//	m_wndProcessGrid.SetColWidth(0,0, 100);

	//Enable Tooltips
	m_wndProcessGrid.EnableGridToolTips();
    m_wndProcessGrid.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, 
         CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));

	m_wndProcessGrid.SetValueRange(CGXRange(0,1),  TEXT_LANG[0]);//"순서"
	m_wndProcessGrid.SetValueRange(CGXRange(0,2),  TEXT_LANG[1]);//"공정명"
	m_wndProcessGrid.SetValueRange(CGXRange(0,3),  TEXT_LANG[2]);//"상태"
	m_wndProcessGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	m_wndProcessGrid.SetStyleRange(CGXRange().SetCols(1), CGXStyle().SetEnabled(FALSE));
	
	//Table setting
	m_wndProcessGrid.SetStyleRange(CGXRange().SetTable(),
			CGXStyle().SetFont(CGXFont().SetSize(9).SetFaceName(GetStringTable(IDS_LANG_TEXT_FONT))));
}

void CTrayInfoDlg::InitCellGrid()
{
	m_wndCellStateGrid.SubclassDlgItem(IDC_CELL_STATE_GRID, this);
	m_wndCellStateGrid.m_bSameColSize  = TRUE;
	m_wndCellStateGrid.m_bSameRowSize  = TRUE;

	//m_wndCellStateGrid.m_bRowSelection = TRUE;
	//m_wndCellStateGrid.m_bCustomWidth  = TRUE;

	m_wndCellStateGrid.Initialize();

	m_wndCellStateGrid.SetRowCount(16);
	m_wndCellStateGrid.SetColCount(8);
	
	m_wndCellStateGrid.SetDefaultRowHeight(18);
	m_wndCellStateGrid.SetColWidth(0, 0, 50);

	//Enable Tooltips
	m_wndCellStateGrid.EnableGridToolTips();
    m_wndCellStateGrid.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, 
         CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));

/*	m_wndCellStateGrid.SetValueRange(CGXRange(0,1),  ::GetStringTable(IDS_LABEL_CHANNEL));
	m_wndCellStateGrid.SetValueRange(CGXRange(0,2),  ::GetStringTable(IDS_LABEL_CELL_NO));
	m_wndCellStateGrid.SetValueRange(CGXRange(0,3),  ::GetStringTable(IDS_LABEL_STEP));
	m_wndCellStateGrid.SetValueRange(CGXRange(0,4),  ::GetStringTable(IDS_LABEL_STATUS));
	m_wndCellStateGrid.SetValueRange(CGXRange(0,5),  ::GetStringTable(IDS_LABEL_BAD));
	m_wndCellStateGrid.SetValueRange(CGXRange(0,6),  ::GetStringTable(IDS_LABEL_GRADE));
	m_wndCellStateGrid.SetValueRange(CGXRange(0,7),  ::GetStringTable(IDS_LABEL_STEP_TIME));
	m_wndCellStateGrid.SetValueRange(CGXRange(0,8),  ::GetStringTable(IDS_LABEL_VOLTAGE));
	m_wndCellStateGrid.SetValueRange(CGXRange(0,9),  ::GetStringTable(IDS_LABEL_CURRENT));
	m_wndCellStateGrid.SetValueRange(CGXRange(0,10), ::GetStringTable(IDS_LABEL_CAPACITY));
	m_wndCellStateGrid.SetValueRange(CGXRange(0,11), ::GetStringTable(IDS_LABEL_POWER));
	m_wndCellStateGrid.SetValueRange(CGXRange(0,12), ::GetStringTable(IDS_LABEL_ENERGY));
	m_wndCellStateGrid.SetValueRange(CGXRange(0,13), ::GetStringTable(IDS_LABEL_IMPEDANCE));
*/
	
	m_wndCellStateGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
//	m_wndCellStateGrid.SetStyleRange(CGXRange().SetCols(1),	CGXStyle().SetEnabled(FALSE));
	//Table setting
	m_wndCellStateGrid.SetStyleRange(CGXRange().SetTable(),
			CGXStyle().SetFont(CGXFont().SetSize(9).SetFaceName(GetStringTable(IDS_LANG_TEXT_FONT))));
}

void CTrayInfoDlg::OnSearchBtn() 
{
	// TODO: Add your control notification handler code here
	UpdateData();

	if(m_strTrayNo.IsEmpty())
	{
		AfxMessageBox(TEXT_LANG[3]);//"Tray 번호를 입력하십시요."
		return;
	}

	if(m_TrayInfo.LoadTrayData(m_strTrayNo) == FALSE)
	{
		AfxMessageBox(m_strTrayNo +TEXT_LANG[4]);//"를 찾을 수 없습니다."
		return;
	}

	UpdateProcessState();

	GetDlgItem(IDC_BUTTON_CANCLE)->EnableWindow(TRUE);
	GetDlgItem(IDC_BUTTON_CANCLE2)->EnableWindow(TRUE);
}

BOOL CTrayInfoDlg::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if( pMsg->message == WM_KEYDOWN )
	{
		switch( pMsg->wParam )
		{
		case VK_RETURN:
			OnSearchBtn();
			return TRUE;
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

//공정 예약 Skip
void CTrayInfoDlg::OnButtonCancle2() 
{
	// TODO: Add your control notification handler code here
	int nRow = GetCurProcessRow();
	if(nRow < 1)
	{
		AfxMessageBox(TEXT_LANG[5]);//"현재 공정의 위치를 찾을 수 없습니다."
		return;
	}

	if(nRow == m_wndProcessGrid.GetRowCount())
	{
		//완료 처리함 
	}
	else
	{
		m_TrayInfo.lTestKey  = atol(m_wndProcessGrid.GetValueRowCol(nRow, 0));
		UpdateProcessState();
	}
}

//공정 실행 취소 
void CTrayInfoDlg::OnButtonCancle() 
{
	// TODO: Add your control notification handler code here
	int nRow = GetCurProcessRow();
	if(nRow < 1)
	{
		AfxMessageBox(TEXT_LANG[5]);//"현재 공정의 위치를 찾을 수 없습니다."
		return;
	}

	if(nRow == 1)
	{
		//최초 공정으로 초기화  
	}
	else
	{
		m_TrayInfo.lTestKey  = atol(m_wndProcessGrid.GetValueRowCol(nRow, 0));
		UpdateProcessState();
	}	
}

int CTrayInfoDlg::GetCurProcessRow()
{
	for(int i = 0; i<m_wndProcessGrid.GetRowCount(); i++)
	{
		if(m_TrayInfo.lTestKey == atol(m_wndProcessGrid.GetValueRowCol(i+1, 0)))
		{
			return i+1;
		}
	}
	return 0;
}

void CTrayInfoDlg::UpdateProcessState()
{
	if(m_wndProcessGrid.GetRowCount() > 0)
	{
		m_wndProcessGrid.RemoveRows(1, m_wndProcessGrid.GetRowCount());
	}
	//Display schedule process
	CDaoDatabase  db;
	try
	{
		db.Open(::GetDataBaseName());
		CString strSQL;
		strSQL.Format("SELECT TestID, TestNo, TestName, Description, ModifiedTime FROM TestName WHERE ModelID = %d ORDER BY TestNo", m_TrayInfo.lModelKey);
		CDaoRecordset rs(&db);
		COleVariant data;
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
		ROWCOL nRow =1;
		
		while(!rs.IsEOF())
		{
			m_wndProcessGrid.InsertRows(nRow, 1);

			rs.GetFieldValue("TestID", data);
			m_wndProcessGrid.SetValueRange(CGXRange(nRow, 0), data.lVal);
			if(data.lVal < m_TrayInfo.lTestKey)
			{
				strSQL = TEXT_LANG[6];//"완료"
				m_wndProcessGrid.SetStyleRange(CGXRange().SetRows(nRow), CGXStyle().SetInterior(RGB(230,230,230)).SetTextColor(RGB(128,128,128)));
			}
			else 
			{
				strSQL = TEXT_LANG[7];//"대기"
			}
			m_wndProcessGrid.SetValueRange(CGXRange(nRow, 3), strSQL);

			rs.GetFieldValue("TestNo", data);
			m_wndProcessGrid.SetValueRange(CGXRange(nRow, 1), data.lVal);
						
			rs.GetFieldValue("TestName", data);
			m_wndProcessGrid.SetValueRange(CGXRange(nRow, 2), data.pcVal);


			rs.GetFieldValue("Description", data);
			rs.GetFieldValue("ModifiedTime", data);
			rs.MoveNext();
			nRow++;
		}
		rs.Close();
		db.Close();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
	}	
	
	int nTotRow = m_wndCellStateGrid.GetRowCount();
	int nTotCol = m_wndCellStateGrid.GetColCount();


	//////////////////////////////////////////////////////////////////////////
	//Display cell state
	int nCnt = 0;
	BYTE code;
	CString str;
	COLORREF color = RGB(255, 255, 255);

// 	WORD nProjectType = GetDocument()->m_nProjectType;
// 	switch (nProjectType)
// 	{
// 	case TRAY_TYPE_PB5_COL_ROW:	//각형 Col 먼저 증가
// 		break;
// 	case TRAY_TYPE_PB5_ROW_COL:	//각형 Row 먼저 증가
// 		break;
// 	case TRAY_TYPE_CUSTOMER:
// 	case TRAY_TYPE_LG_COL_ROW:	//가로 번호 체계 
// 		break;
// 	case TRAY_TYPE_LG_ROW_COL:	//세로 번호 체계
// 		break;
// 	}
#if CUSTOMER_TYPE == LG_CHEMICAL || CUSTOMER_TYPE == LGC_PB5
	for(int i=0; i<nTotRow; i++)
	{
		for(int j = 0; j<nTotCol; j++)
		{
#else
	for(int j=0; j<nTotCol; j++)
	{
		for(int i=0; i<nTotRow; i++)
		{
#endif
			code = m_TrayInfo.cellCode[nCnt++];
			if( ::IsNonCell(code))
			{
				str ="X";	
				color = RGB(230, 230, 230);
			}
			else if(::IsNormalCell(code))
			{
				str ="G";					
				color = RGB(255, 255, 255);
			}	
			else 
			{
				str = "F";
				color = RGB(255, 100, 100);
			}
			m_wndCellStateGrid.SetStyleRange(CGXRange(i+1, j+1), CGXStyle().SetValue(str).SetInterior(color));

			if(nCnt >= EP_MAX_CH_PER_MD)	break;
		}
	}
}
