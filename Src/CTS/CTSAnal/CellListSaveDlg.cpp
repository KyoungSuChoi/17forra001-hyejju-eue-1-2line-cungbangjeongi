// CellListSaveDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "CellListSaveDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCellListSaveDlg dialog


CCellListSaveDlg::CCellListSaveDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCellListSaveDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCellListSaveDlg)
	m_strFileName = _T("CellList.csv");
	m_fVal1 = 0.0f;
	//}}AFX_DATA_INIT
	m_pField = NULL;
	m_nFieldCount = 0;
	m_nSelItem = -1;		//All Data
}


void CCellListSaveDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCellListSaveDlg)
	DDX_Control(pDX, IDC_OP_COMBO, m_ctrlOPCombo);
	DDX_Control(pDX, IDC_COMBO1, m_ctrlFieldListCombo);
	DDX_Text(pDX, IDC_EDIT1, m_strFileName);
	DDX_Text(pDX, IDC_VALUE_EDIT, m_fVal1);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCellListSaveDlg, CDialog)
	//{{AFX_MSG_MAP(CCellListSaveDlg)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	ON_BN_CLICKED(IDOK, OnOk)
	ON_CBN_SELCHANGE(IDC_COMBO1, OnSelchangeCombo1)
	//}}AFX_MSG_MAP
	ON_STN_CLICKED(IDC_UNIT_LABEL, &CCellListSaveDlg::OnStnClickedUnitLabel)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCellListSaveDlg message handlers

BOOL CCellListSaveDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_ctrlFieldListCombo.AddString("All");
	for(int i = 0; i<m_nFieldCount; i++)
	{
		m_ctrlFieldListCombo.AddString(m_pField[i].FieldName);
	}
	m_ctrlFieldListCombo.SetCurSel(0);
	m_ctrlOPCombo.SetCurSel(0);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCellListSaveDlg::OnButton1() 
{
	// TODO: Add your control notification handler code here
	
	CFileDialog pDlg(FALSE, "", m_strFileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "csv 파일(*.csv)|*.csv|");

	if(IDOK != pDlg.DoModal())		return;

	m_strFileName = pDlg.GetPathName();
	GetDlgItem(IDC_EDIT1)->SetWindowText(m_strFileName);
//	UpdateData(FALSE);
}

void CCellListSaveDlg::OnOk() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	m_nSelItem = m_ctrlFieldListCombo.GetCurSel()-1;	//-1 is All Data
	m_nOpType = m_ctrlOPCombo.GetCurSel();
	
	CDialog::OnOK();
}

void CCellListSaveDlg::OnSelchangeCombo1() 
{
	// TODO: Add your control notification handler code here
	int nCurItem = m_ctrlFieldListCombo.GetCurSel();
	if(nCurItem <= 0)
	{
		GetDlgItem(IDC_VALUE_EDIT)->EnableWindow(FALSE);
		GetDlgItem(IDC_OP_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_UNIT_LABEL)->SetWindowText("");
	}
	else
	{
		GetDlgItem(IDC_VALUE_EDIT)->EnableWindow(TRUE);
		GetDlgItem(IDC_OP_COMBO)->EnableWindow(TRUE);
		
		ASSERT(nCurItem <= m_nFieldCount);
		
		switch(m_pField[nCurItem-1].DataType)
		{
			case EP_VOLTAGE:		GetDlgItem(IDC_UNIT_LABEL)->SetWindowText("V");				break;
			case EP_CURRENT:		GetDlgItem(IDC_UNIT_LABEL)->SetWindowText("mA");			break;
			case EP_CAPACITY:		GetDlgItem(IDC_UNIT_LABEL)->SetWindowText("mAh");			break;
			case EP_IMPEDANCE:		GetDlgItem(IDC_UNIT_LABEL)->SetWindowText("mΩ");			break;
			case EP_WATT:			GetDlgItem(IDC_UNIT_LABEL)->SetWindowText("mW");			break;
			case EP_WATT_HOUR:		GetDlgItem(IDC_UNIT_LABEL)->SetWindowText("mWh");			break;
			case EP_STEP_TIME:
			case EP_TOT_TIME:		GetDlgItem(IDC_UNIT_LABEL)->SetWindowText("s");			break;
			default:				GetDlgItem(IDC_UNIT_LABEL)->SetWindowText("");			break;
		}	
	}
}

void CCellListSaveDlg::SetField(FIELD *pField, UINT nCount)
{
	m_pField = pField;
	m_nFieldCount = nCount;
}


void CCellListSaveDlg::OnStnClickedUnitLabel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}
