// CTSMonDoc.cpp : implementation of the CCTSMonDoc class
//

#include "stdafx.h"
#include "CTSMon.h"
#include "CTSMonDoc.h"
#include "Mainfrm.h"

#include "RunDlg.h"
#include "TestModuleRecordSet.h"
#include "CommandLogRecordSet.h"
#include "ResultLogRecordSet.h"
#include "ProcedureSelDlg.h"

#include "TestLotInputDlg.h"
#include "DetailAutoTestConditionDlg.h"
#include "FailMsgDlg.h"

#include "IPSetDlg.h"
#include "SelResultDlg.h"
#include "./Global/Mysql/PneApp.h"

// 20090504 kky for 파일저장 관련 오류
// #ifdef _DEBUG
#include "Mmsystem.h"

#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
// #endif

/////////////////////////////////////////////////////////////////////////////
// CCTSMonDoc
extern STR_LOGIN g_LoginData;

IMPLEMENT_DYNCREATE(CCTSMonDoc, CDocument)

BEGIN_MESSAGE_MAP(CCTSMonDoc, CDocument)
	//{{AFX_MSG_MAP(CCTSMonDoc)
//	ON_COMMAND(ID_CALIBRATION_SET, OnCalibrationSet)
	//}}AFX_MSG_MAP	
	ON_COMMAND(ID_INSPECTED_RESULT_RUN, &CCTSMonDoc::OnInspectedResultRun)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCTSMonDoc construction/destruction

extern	BOOL	g_bUseRackIndex;
extern	int		g_nModulePerRack;
extern	BOOL	g_bUseGroupSet;
extern	CString	g_strModuleName;
extern	CString	g_strGroupName;
extern  CString GetDefaultModuleName(int nModuleID, int nGroupIndex);

DWORD WINAPI TotalMonThread(LPVOID parameter)
{
	CCTSMonDoc *Owner = (CCTSMonDoc*) parameter;
	Owner->TotalMonThread();

	return 0;
}

VOID CCTSMonDoc::onDbInit()
{	
	CString Address = AfxGetApp()->GetProfileString(FROM_SYSTEM_SECTION, "MonIP", "localhost");	
	CString UserID = AfxGetApp()->GetProfileString(FROM_SYSTEM_SECTION, "UserID", "mondev");
	CString DB_Name = AfxGetApp()->GetProfileString(FROM_SYSTEM_SECTION, "DB_Name", "totalmonitor");
	CString Password = AfxGetApp()->GetProfileString(FROM_SYSTEM_SECTION, "Password", "xhdgkq~!");

	mainPneApp.m_MySql.DBConn(Address, UserID, Password, DB_Name, 3306, FALSE, TRUE);
}

BOOL CCTSMonDoc::onWorkDeal()
{
	CString strdata = _T("");
	int i=0;

	int nSize = mainPneApp.mesData.GetCount();
	if(nSize<1)
	{
		return FALSE;
	}

	if( mainPneApp.m_MySql.m_bCon == FALSE ) 
	{
		if( nSize > 3000 )
		{
			mainPneApp.mesData.RemoveAll();
		}

		return FALSE;
	}	

	mainPneApp.SetSendFlag(true);

	strdata.Format("Update Sql DataSize ==== [%d]", nSize);
	GLog::log_Save("mysql", "sql", strdata, NULL, TRUE);

	for( i=0;i<nSize;i++ )
	{
		POSITION pos = mainPneApp.mesData.GetHeadPosition();
		if(!pos) break;

		strdata = mainPneApp.mesData.GetAt(pos);

		if( mainPneApp.m_MySql.SetQuery(strdata) == TRUE )
		{
			mainPneApp.mesData.RemoveAt(pos);
			// GLog::log_Save("mysql", "sql", strdata, NULL, TRUE);
		}
		else
		{
			break;
		}
	}

	mainPneApp.SetSendFlag(false);

	return TRUE;
}

void CCTSMonDoc::ReConnectError()
{	
	if( mainPneApp.m_MySql.m_bCon == FALSE ||
		mainPneApp.m_MySql.m_iErr == 2003 ||
		mainPneApp.m_MySql.m_iErr == 2013 ||
		mainPneApp.m_MySql.m_iErr == 2060
		)	
	{
		mainPneApp.m_MySql.ReConn();
	}
}

VOID CCTSMonDoc::TotalMonThread(VOID)
{
	onDbInit();

	while(TRUE)
	{
		DWORD Result = WaitForSingleObject(mTotalMonThreadDestroyEvent, 2000);
		if( Result == WAIT_OBJECT_0 )
		{
			return;
		}

		if( m_bUsePneMonitoringSystem == true )
		{
			ReConnectError();

			onWorkDeal();
		}
		else
		{
			if( mainPneApp.mesData.GetSize() > 0 )
			{
				mainPneApp.mesData.RemoveAll();
			}
		}
	}

	/*
	int i=0;
	int nQuerySize = 0;
	
	BOOL bMysqlConnect = FALSE;	

	while(TRUE)
	{
		DWORD Result = WaitForSingleObject(mTotalMonThreadDestroyEvent, 2000);
		if( Result == WAIT_OBJECT_0 )
		{
			return;
		}

		if( m_bUsePneMonitoringSystem == true )
		{
			if( bMysqlConnect == FALSE )
			{
				if( m_MySqlServer->Fun_Connect() == true )
				{
					WriteLog("=> PNE Monitoring system connect succeed");
					bMysqlConnect = TRUE;
				}
				else
				{
					WriteLog("=> PNE Monitoring system connect fail!");
					AfxMessageBox("PNE 모니터링 시스템 연결에 실패했습니다.", MB_ICONERROR);					
				}				
			}

			if( bMysqlConnect == TRUE )
			{
				nQuerySize = m_ArraySqlQuery.GetSize();				

				if( nQuerySize != 0 )
				{
					if( m_MySqlServer->IsMySqlLive() == true )
					{				
						m_MySqlServer->SetSendFlag(true);

						for( i=0; i<nQuerySize; i++ )
						{
							if( m_MySqlServer->Fun_ExecuteQuery(m_ArraySqlQuery.GetAt(0)) != 0 )
							{
								break;
							}
							else
							{
								m_ArraySqlQuery.RemoveAt(0);
							}
						}
						
						for( i=0; i<nQuerySize; i++ )
						{	
							if( m_MySqlServer->Fun_ExecuteQuery(m_ArraySqlQuery.GetAt(i)) != 0 )
							{
								m_bUsePneMonitoringSystem = false;
								break;
							}
						}
						m_ArraySqlQuery.RemoveAll();

						for( i=0; m_ArrayEmgSqlQuery.GetSize() ; i++ )
						{
							m_ArrayEmgSqlQuery.GetData();

						}						
						
						m_MySqlServer->SetSendFlag(false);
					}
					else
					{
						bMysqlConnect = FALSE;
						m_ArraySqlQuery.RemoveAll();
						m_bUsePneMonitoringSystem = false;			
					}	
				}
			}
			else
			{
				nQuerySize = m_ArraySqlQuery.GetSize(); 

				if( nQuerySize != 0 )
				{
					m_ArraySqlQuery.RemoveAll();
				}

				m_bUsePneMonitoringSystem = false;
			}
		}
		else
		{
			if( bMysqlConnect == TRUE )
			{
				bMysqlConnect= FALSE;
				m_MySqlServer->Fun_DisConnect();
			}

			nQuerySize = m_ArraySqlQuery.GetSize(); 

			if( nQuerySize != 0 )
			{
				m_ArraySqlQuery.RemoveAll();
			}
		}
	}
	*/
}


CCTSMonDoc::CCTSMonDoc()
{
	// TODO: add one-time construction code here
	m_Initialized = FALSE;
	m_imagelist = NULL;
	m_nInstalledModuleNum = 0;
	m_bProgressInit = FALSE;
	m_pProgressWnd = NULL;
//	m_pModule = NULL;
	
	m_bUseGroupSet = FALSE;

	m_strModuleName = "Rack";
	m_strGroupName = "Group";

	m_nModulePerRack = 3;
	m_bUseRackIndex = TRUE;
	m_bUseOnLine=FALSE;				// On Line 사용여부

	m_bRunCmd = FALSE;				//사용자가 같은 Cmd를 연속 해서 전송하는 것을 방지하기 위한 Flag
	m_bPauseCmd = FALSE;
	m_bContinueCmd = FALSE;
	m_bStopCmd = FALSE;
	m_bInitCmd = FALSE;
	m_bNextStepCmd = FALSE;
	m_nCmdInterval = 5000;

	m_nTrayColSize = EP_DEFAULT_TRAY_COL_COUNT;
	m_bFolderTime = FALSE;
	m_bFolderModuleName = FALSE;
	m_bFolderLot= FALSE;
	m_bFolderTray= FALSE;
	m_nTimeFolderInterval = 30;
	m_nContactErrlimit = 3;
	m_nCapaErrlimit = 3;
	m_nChErrlimit = 2;
	m_nDeviceID = DEVICE_FORMATION1;
	m_bUsePneMonitoringSystem = false;
	
//	m_MySqlServer = NULL;
//	m_pszFileBuff = NULL;
	m_bUseFaultAlarm = FALSE;

	mTotalMonThreadHandle = NULL;
	mTotalMonThreadDestroyEvent = NULL;
	LanguageinitMonConfig();
	m_ProcessNo = 0; //20200411 엄륭 프로세스 넘버 P : 1
	nFlag = 0; //20200807 엄륭
}

CCTSMonDoc::~CCTSMonDoc()
{
	/*
	if( m_MySqlServer != NULL )
	{
		m_MySqlServer->Fun_DisConnect();
		delete m_MySqlServer;
	}
	*/
	
	if(m_imagelist)				//delete Img List
	{
		delete m_imagelist;
		m_imagelist = NULL;
	}
	if(m_pProgressWnd)
	{
		delete	m_pProgressWnd;
		m_pProgressWnd = NULL;
	}
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}

}

bool CCTSMonDoc::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCTSMonDoc"), _T("TEXT_CCTSMonDoc_CNT"), _T("TEXT_CCTSMonDoc_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CCTSMonDoc_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCTSMonDoc"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

BOOL CCTSMonDoc::OnNewDocument()
{
	//Login check
	BOOL bUseLogin = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "UseLogin", 1);
	if(bUseLogin)
	{
		if(::LoginCheck() == FALSE)	return FALSE;
	}

	if(PermissionCheck(PMS_FORM_START) == FALSE)
	{
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_PERMISSION_ERROR) +" [Power Formation Program start]");
		return FALSE;
	}

	//////////////////////////////////////////////////////////////////////////
	char msg[512];
	::configFileName(FORM_SETTING_SAVE_FILE);

	//Read registry configuration information
	if(!LoadSetting())
	{	
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_ERROR_LOAD_SETTING));
		return FALSE;
	}

	sprintf(msg, "=================> CTSMon started <=================");
	WriteLog(msg);

	m_bUsePneMonitoringSystem = (bool)AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "UseMon", 0);
	m_bUsePneMonitoringSystem = false;
	// m_MySqlServer = new CMySql_DB();
	
	//Get Installed Module count
	m_nInstalledModuleNum = LoadInstalledModuleNum();
	if(m_nInstalledModuleNum <= 0)
	{
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_ERROR_LOADSETTING) + m_strLastErrorString);
		return FALSE;
	}

	//EPSetChIndexType(EP_CH_INDEX_MAPPING);		//PC에서 Mapping 한다.
	//Make formation server		
	if(!::EPOpenFormServer(m_nInstalledModuleNum, AfxGetMainWnd()->m_hWnd))
	{	
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_ERROR_EPFORM));
		return FALSE;
	}

	m_Initialized = TRUE;

	if(LoadSystemParam() != m_nInstalledModuleNum )
	{
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_ERROR_LOADSETTING) + m_strLastErrorString);
		return FALSE;
	}
	
	m_nDeviceID = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "DeviceID", 2);
	
	//Port listen start
	if(EPServerStart() == FALSE)
	{
		WriteLog("Formation Server Start Fail");
		return FALSE;
	}

	/* Total Monitoring Thread 
	HANDLE ThreadHandle;
	DWORD dwThreadID;

	mTotalMonThreadHandle = CreateThread(NULL, 0, ::TotalMonThread, this, 0, NULL);	
	mTotalMonThreadDestroyEvent = CreateEvent(NULL, FALSE, FALSE, NULL);	
	*/
	
	//20090920 KBH
	// UpdateTrayCellStartNo();
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CCTSMonDoc serialization

void CCTSMonDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CCTSMonDoc diagnostics

#ifdef _DEBUG
void CCTSMonDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CCTSMonDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCTSMonDoc commands
int CCTSMonDoc::LoadInstalledModuleNum()
{
	CDaoDatabase  db;
	int nInstallCount = 0;

	CString strDBName;
	strDBName = ::GetDataBaseName();

	if(strDBName.IsEmpty())		return 0;

// ljb 2009106 S	/////////////////
// 	CDatabase  db2;
// 	try
// 	{
// 		db2.OpenEx("DSN=CTSSchedule;",0);
// 		
// 	}
// 	catch (CDaoException* e)
// 	{
// 		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
// 		e->Delete();
// 		return 0;
// 	}
// //	db2.ExecuteSQL( strSQL ); 
// 	CString strSQL2;
// 	strSQL2 = "SELECT COUNT(ModuleID) FROM SystemConfig";
// 
// 	COleVariant data2;
// 	CRecordset rs2(&db2);
// 	try
// 	{
// 		rs2.Open( AFX_DB_USE_DEFAULT_TYPE, strSQL2, CRecordset::none);	
// 	}
// 	catch (CDaoException* e)
// 	{
// 		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
// 		e->Delete();
// 		db2.Close();
// 		return 0;
// 	}	
// 	if(!rs2.IsBOF() && !rs2.IsEOF())
// 	{
// 		nInstallCount = rs2.GetRecordCount();
// 		// = data.lVal;
// 	}
// ljb 2009106 E /////////////////	

	try
	{
		db.Open(strDBName);
		
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return 0;
	}	
	
	CString strSQL, strTemp;
	DWORD sysID = ((CCTSMonApp*)AfxGetApp())->GetSystemType();
	strSQL = "SELECT COUNT(ModuleID) FROM SystemConfig";

	if(sysID != 0)		//O is All system
	{
		strTemp.Format(" WHERE ModuleType=%d", sysID);
		strSQL += strTemp;
	}
	
	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return 0;
	}	
	
	if(!rs.IsBOF() && !rs.IsEOF())
	{
		data = rs.GetFieldValue(0);
		nInstallCount = data.lVal;
	}

	if(nInstallCount <= 0)	//Installed Module Number check
	{
		nInstallCount = 0;
		m_strLastErrorString.LoadString(IDS_TEXT_MISMATCH_HOST_TYPE);
		strTemp = ::GetStringTable(IDS_TEXT_MISMATCH_HOST_TYPE);
		m_strLastErrorString.Format("%s (Host Type = %d)", strTemp, ((CCTSMonApp *)AfxGetApp())->GetSystemType());
	}
	else if(nInstallCount > EP_MAX_MODULE_NUM)
	{
		strTemp = ::GetStringTable(IDS_TEXT_MAX_MD_ERROR);
		m_strLastErrorString.Format("%s. Setting Count: %d, Max Count: %d", strTemp, nInstallCount, EP_MAX_MODULE_NUM);
		nInstallCount = EP_MAX_MODULE_NUM;
	}
		
	rs.Close();
	db.Close();

	return nInstallCount;
}

//20200831ksj
void CCTSMonDoc::ReLoadSensorSetting()
{

	return ;
}

int CCTSMonDoc::LoadSystemParam()
{
	//////////////////////////////////////////////////////////////////////////
	//New Version 2005/11/14 by KBH
	//////////////////////////////////////////////////////////////////////////
	//Make module structure		
	CDaoDatabase  db;
	COleVariant data;
	
	int nModuleIndex = 0;
	int nModuleID, nModuleType;
	CString strTemp, strSource;
	int index;
	CString	strTrayType;		
	try
	{
		db.Open(GetDataBaseName());
		
		DWORD sysType = ((CCTSMonApp*)AfxGetApp())->GetSystemType();
		

		CString strSQL("SELECT ModuleID, IP_Address, VSpec, ISpec, ModuleType, InstallChCount, ControllerType, ProgramVer");
		strSQL += ", MaxVoltage, MinVoltage, MaxCurrent, MinCurrent, WarnTemperature, DangerTemperature, UseUnitLimitFlag, UseJigLimitFlag, AutoProcess, SaveInterval, Data1, Data2, Data3, Data4, UseJigTargetTemp, JigTargetTemp, JigTargetTempInterval";
		strSQL += " FROM SystemConfig";
	
		//모두 수용 
		if(sysType != 0)
		{
			strTemp.Format(" WHERE ModuleType=%d", ((CCTSMonApp*)AfxGetApp())->GetSystemType());
			strSQL += strTemp;
		}

		strSQL += " ORDER BY ModuleID";
		
		CDaoRecordset rs(&db);
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
		
		EP_SYSTEM_PARAM *pSysParam;
		pSysParam = new EP_SYSTEM_PARAM;
		ASSERT(pSysParam);
		
		int nCnt = 0, nTmp = 0;
		//Load Module Configuration
		while(!rs.IsEOF())
		{
			::ZeroMemory(pSysParam, sizeof(EP_SYSTEM_PARAM));
			
			rs.GetFieldValue("ModuleID", data);
			if(VT_NULL == data.vt)		break;
			nModuleID = data.lVal;
			
			rs.GetFieldValue("ModuleType", data);
			if(VT_NULL == data.vt)		break;
			nModuleType = data.lVal;

			pSysParam->wModuleType = (WORD)nModuleType;
			pSysParam->nModuleID = nModuleID;

			rs.GetFieldValue("IP_Address", data);
			if(VT_NULL != data.vt)
			{
				strcpy(pSysParam->szIPAddr, data.pcVal);		
			}
			
			//최종 접속한 모듈 설정을 DB에서 Loading하여 초기 모양 설정
			EP_MD_SYSTEM_DATA *pMDSys;
			pMDSys = EPGetModuleSysData(nModuleIndex);
			if(pMDSys)
			{
				//Install Channel 수를 Loading한다.
				rs.GetFieldValue("InstallChCount", data);
				if(VT_NULL != data.vt)
				{
					pMDSys->nTotalChNo = (WORD)data.lVal;
				}

				//Tray 설정을 Loading한다.
				//Data 저장형식 "32,32,32,32"
				rs.GetFieldValue("Data2", data);	
				if(VT_NULL != data.vt)
				{		
					nCnt = 0;
					nTmp = 0;
					WORD awTray[INSTALL_TRAY_PER_MD];
					strSource = data.pcVal;
					while((index = strSource.Find(",")) >=0 )
					{
						strTemp = strSource.Left(index);
						nTmp = atol(strTemp);
						strTemp = strSource.Mid(index+1);				//, 제외
						strSource = strTemp;

						if(nTmp >0 && nTmp <= EP_MAX_CH_PER_MD)	awTray[nCnt++] = nTmp;
					}
					if(!strSource.IsEmpty())
					{
						nTmp = atol(strSource);
						if(nTmp >0 && nTmp <= EP_MAX_CH_PER_MD)	awTray[nCnt++] = nTmp;
					}
					if(nCnt>0 && nCnt <= INSTALL_TRAY_PER_MD && nCnt<= EP_MAX_TRAY_PER_MD)
					{
						pMDSys->wTotalTrayNo = nCnt;
						for(int t=0; t<nCnt; t++)
						{
							pMDSys->awChInTray[t] = awTray[t];
							
						}
					}
				}
				//TrayType 설정을 Loading한다. 
				//0 : 사용자형, 1:일반가로형, 2:일반 세로형, 3:각형 가로형, 4:가형 세로형
				//Data 저장형식 "TYPE=2,COL=16@TYPE=3,COL=32"
				rs.GetFieldValue("Data4", data);
				if(VT_NULL != data.vt)
				{
					strTrayType = data.pcVal;
				}
			}
			//////////////////////////////////////////////////////////////////////////	
			rs.GetFieldValue("MaxVoltage", data);	
			pSysParam->lMaxVoltage =  (long)(data.fltVal)* EP_VTG_M_EXP;
			rs.GetFieldValue("MinVoltage", data);	
			pSysParam->lMinVoltage =  (long)(data.fltVal)* EP_VTG_M_EXP;
			rs.GetFieldValue("MaxCurrent", data);	
			pSysParam->lMaxCurrent =  ExpFloattoLong(data.fltVal, EP_CRT_FLOAT);
			rs.GetFieldValue("MinCurrent", data);	
			pSysParam->lMinCurrent =  ExpFloattoLong(data.fltVal, EP_CRT_FLOAT);
			
			// 경고값 전원부, 지그부
			rs.GetFieldValue("WarnTemperature", data);
			if(VT_NULL != data.vt)
			{
				nCnt = 0, nTmp=0;
				strSource = data.pcVal;
				
				index = strSource.Find(",");
				if( index == -1 )
				{
					pSysParam->sWanningTemp = 0;
					pSysParam->sJigWanningTemp = 0;
				}
				else
				{
					strTemp = strSource.Left(index);
					pSysParam->sWanningTemp = atol(strTemp)*EP_ETC_EXP;
					strTemp = strSource.Right(index);
					pSysParam->sJigWanningTemp = atol(strTemp)*EP_ETC_EXP;
				}				
			}

			// 위험값 전원부, 지그부			
			rs.GetFieldValue("DangerTemperature", data);				
			pSysParam->sCutTemp = data.lVal*EP_ETC_EXP;

			if(VT_NULL != data.vt)
			{
				nCnt = 0, nTmp=0;
				strSource = data.pcVal;

				index = strSource.Find(",");
				if( index == -1 )
				{
					pSysParam->sCutTemp = 0;
					pSysParam->sJigCutTemp = 0;
				}
				else
				{
					strTemp = strSource.Left(index);
					pSysParam->sCutTemp = atol(strTemp)*EP_ETC_EXP;
					strTemp = strSource.Right(index);
					pSysParam->sJigCutTemp = atol(strTemp)*EP_ETC_EXP;
				}				
			}
			
			rs.GetFieldValue("UseUnitLimitFlag", data);	
			pSysParam->bUseTempLimit =  data.boolVal == false ? FALSE : TRUE;

			rs.GetFieldValue("UseJigLimitFlag", data);	
			pSysParam->bUseJigTempLimit =  data.boolVal == false ? FALSE : TRUE;

			pSysParam->lV24Over = 250000;		//25V
			pSysParam->lV24Limit =  230000;		//23V
			pSysParam->lV12Over =  130000;		//13V
			pSysParam->lV12Limit =  110000;		//11V

			rs.GetFieldValue("AutoProcess", data);	
			pSysParam->bAutoProcess =  data.boolVal == false ? FALSE : TRUE;

			//nAutoReportInterval는 최초 전송시는 저장 간격을 의미함 Protocol Version 0x3003 이후 (초단위)
			rs.GetFieldValue("SaveInterval", data);	
	 		pSysParam->nAutoReportInterval = data.lVal;
			//////////////////////////////////////////////////////////////////////////

			// 트레이 채널 경고 수량 1,2
			rs.GetFieldValue("Data1", data);	
			if(VT_NULL != data.vt)
			{
				char szBuff[64];
				int nTemp1, nTemp2, nTemp3;
				sprintf(szBuff, "%s", data.pcVal);
				if(sscanf(szBuff, "%d %d %d", &nTemp1, &nTemp2, &nTemp3) > 0)
				{
					pSysParam->wWarningNo1 = nTemp1;
					pSysParam->wWarningNo2 = nTemp2;
					pSysParam->bUseTrayError = nTemp3;
				}
			}

			//Module Name
			rs.GetFieldValue("Data3", data);	
			if(VT_NULL != data.vt)
			{
				strTemp = data.pcVal;
			}
			else
			{
				strTemp = GetDefaultModuleName(nModuleID, 0);
			}

			// 0425 kky0703
			rs.GetFieldValue("UseJigTargetTemp", data);	
			pSysParam->bUseJigTargetTemp = data.boolVal == false ? FALSE : TRUE;
			rs.GetFieldValue("JigTargetTemp", data);	
			pSysParam->nJigTargetTemp = data.intVal;
			rs.GetFieldValue("JigTargetTempInterval", data);	
			pSysParam->nJigTempInterval = data.intVal;

			CFormModule *pModule = new CFormModule(nModuleID, strTemp);
			
			// TrayType 설정 I/O 출력 부분	ljb -> 문자열로 추가 (이름,TrayType,Tray Col )
			if (!strTrayType.IsEmpty())
			{
				pModule->SetTrayTypeData(strTrayType);
				strTrayType.Empty();
			}
			
			m_apModuleInfo.Add(pModule);

			//Set Module ID and Parameter
			if(!EPInitSysParam(nModuleIndex,  pSysParam))
			{
				index = -1;
				break;
			}
								
			rs.MoveNext();
			nModuleIndex++;
		}
		delete pSysParam;
		rs.Close();
		db.Close();	
	}
	catch (CDaoException *e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}

	//Load Channel Code table 
	if(LoadCodeTable() == FALSE)		
	{
		WriteLog("Code tabel information loading fail!!!");
		return 0;
	}
	
	return nModuleIndex;
}


void CCTSMonDoc::OnCloseDocument() 
{
	CString strTemp;
	int i;

	//Module 구조 해제
	for(int a = 0; a<m_apModuleInfo.GetSize(); a++)
	{
		CFormModule *pModule = (CFormModule *)m_apModuleInfo.GetAt(a);
		delete  pModule;
		pModule = NULL;
	}
	m_apModuleInfo.RemoveAll();

	if(m_Initialized)
	{
		for(i =0; i<m_apModuleInfo.GetSize(); i++)
		{
/*			nModuleID = EPGetModuleID(i);
			if(EPGetModuleState(nModuleID) == EP_STATE_LINE_OFF)	continue;
			gpData = EPGetGroupData(nModuleID, 0);

			strTemp.Format("%s Max Temp: %.1f℃(%s), Min Temp: %.1f℃(%s)", 
				::GetModuleName(nModuleID), 
				gpData.sensorMinMax.tempData.lMax/EP_ETC_EXP, 
				gpData.sensorMinMax.tempData.szMaxDateTime,
				gpData.sensorMinMax.tempData.lMin/EP_ETC_EXP,
				gpData.sensorMinMax.tempData.szMinDateTime
			);
			WriteLog((LPSTR)(LPCTSTR)strTemp);
			
			strTemp.Format("%s Max Gas: %.1f℃(%s), Min Gas: %.1f℃(%s)", 
				::GetModuleName(nModuleID), 
				gpData.sensorMinMax.gasData.lMax/EP_ETC_EXP, 
				gpData.sensorMinMax.gasData.szMaxDateTime,
				gpData.sensorMinMax.gasData.lMin/EP_ETC_EXP,
				gpData.sensorMinMax.gasData.szMinDateTime
			);
			WriteLog((LPSTR)(LPCTSTR)strTemp);
*/		}		
		EPCloseFormServer();
	}

	STR_MSG_DATA *pCode;
	for( i = 0; i< m_apChCodeMsg.GetSize(); i++ )
	{
		pCode = (STR_MSG_DATA *)m_apChCodeMsg[i];
		if(pCode)
		{
			delete pCode;
			pCode = NULL;
		}
	}
	m_apChCodeMsg.RemoveAll();

	STR_MSG_DATA *pObject;
	for(i = 0; i<m_apProcType.GetSize(); i++)
	{
		pObject = (STR_MSG_DATA *)m_apProcType[i];
		if(pObject)
		{
			delete pObject;
			pObject = NULL;
		}
	}
	m_apProcType.RemoveAll();	
	
	CMainFrame *pFrame = (CMainFrame *)AfxGetMainWnd();
	pFrame->ClearFrmDlg();

	char msg[512];
	sprintf(msg, "CTSMon closed by %s\r\n\r\n", g_LoginData.szLoginID);
	WriteLog(msg);

	if (mTotalMonThreadHandle && mTotalMonThreadDestroyEvent)
	{
		SetEvent(mTotalMonThreadDestroyEvent);
		WaitForSingleObject(mTotalMonThreadHandle, INFINITE);
		CloseHandle(mTotalMonThreadDestroyEvent);
		CloseHandle(mTotalMonThreadHandle);

		if( mainPneApp.m_MySql.m_bCon == TRUE )
		{
			mainPneApp.m_MySql.DBClose();
		}
	}
	
	CDocument::OnCloseDocument();
}

void CCTSMonDoc::InitImgList()
{
	m_imagelist = new CImageList;
	ASSERT(m_imagelist);
//	m_imagelist->Create(IDB_STATE_ICON1, 20, 18, RGB(255,255,255));
	m_imagelist->Create(IDB_STATE_ICON2, 35, 20, RGB(255,255,255));
}

void CCTSMonDoc::InitTree(SECTreeCtrl *pTreeX, UINT nResourceID, CWnd *pWnd)
{
	ASSERT(pTreeX);
	if(m_imagelist == NULL)		//Image initialize
	{
		InitImgList();
	}
	
	pTreeX->SubclassTreeCtrlId(nResourceID, pWnd);
	pTreeX->SetImageList(m_imagelist, TVSIL_NORMAL);

	int	 iItem;
	TV_INSERTSTRUCT		tvstruct;
	HTREEITEM	m_rghItem;
	int nModuleID;
	CString		strTreeTitle;

	pTreeX->DeleteAllItems();
	
	for (iItem = 0; iItem < m_nInstalledModuleNum; iItem ++)
	{
		nModuleID = EPGetModuleID(iItem);			//Get Moduel ID
		// strTreeTitle.Format("%s", ::GetModuleName(nModuleID));	// 20090630 kky
		strTreeTitle.Format("%d", nModuleID);	// 20090630 kky		
		tvstruct.hParent = NULL;
		tvstruct.hInsertAfter = TVI_LAST;
		tvstruct.item.iImage = I_IMAGECALLBACK;
		tvstruct.item.iSelectedImage = I_IMAGECALLBACK;
		tvstruct.item.pszText = (char *)(LPCTSTR)strTreeTitle;		//Display Module ID String
		tvstruct.item.cchTextMax = 64;
		tvstruct.item.mask = TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_TEXT;
		m_rghItem = pTreeX->InsertItem(&tvstruct);
		pTreeX->SetItemData(m_rghItem, (DWORD)nModuleID);			//Set Tree Data to Module ID
	}
	pTreeX->Invalidate();
	pTreeX->SetFocus();
	pTreeX->ShowScrollBar(SB_VERT, TRUE);
}

void CCTSMonDoc::DrawTree(SECTreeCtrl *pTreeX, int nSelModuleID, BOOL /*bShowGroup*/)
{
	TV_INSERTSTRUCT		tvstruct;
	HTREEITEM	hMdItem, hGpItem, hTempItem;
	int nModuleID;
	CString		strTreeTitle;

	hMdItem = pTreeX->GetRootItem();
	while(hMdItem)
	{
		nModuleID = pTreeX->GetItemData(hMdItem);
		if(nSelModuleID < 0 || nSelModuleID == nModuleID)
		{
			int nGroupCount = EPGetGroupCount(nModuleID);
		
			hGpItem = pTreeX->GetChildItem(hMdItem);
			
			for(int i =0; i<nGroupCount; i++)
			{
				if(hGpItem == NULL)
				{
					strTreeTitle.Format("Group %d ", i+1);	
					tvstruct.hParent = hMdItem;
					tvstruct.hInsertAfter = TVI_LAST;
					tvstruct.item.pszText = (char *)(LPCTSTR)strTreeTitle;
					tvstruct.item.cchTextMax = 64;
					tvstruct.item.lParam = i;
					tvstruct.item.iImage = I_IMAGECALLBACK;
					tvstruct.item.iSelectedImage = I_IMAGECALLBACK;
					tvstruct.item.mask = TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_TEXT;
					hGpItem = pTreeX->InsertItem(&tvstruct);
					pTreeX->SetItemData(hGpItem, (DWORD)i);
				}
				else
				{
					pTreeX->SetItemData(hGpItem, (DWORD)i);
				}
				pTreeX->HideItem(hGpItem, !m_bUseGroupSet);
				
				hGpItem = pTreeX->GetNextItem(hGpItem, TVGN_NEXT);
			}
			
			if(hGpItem)
			{
				hTempItem = pTreeX->GetNextItem(hGpItem, TVGN_NEXT);
				while(hTempItem)
				{
					pTreeX->DeleteItem(hTempItem);
					hTempItem = pTreeX->GetNextItem(hGpItem, TVGN_NEXT);
				}
				if(pTreeX->DeleteItem(hGpItem) ==0)
				{
					TRACE("Tree Item Delete Error\n");
				}
			}
		}
		hMdItem = pTreeX->GetNextItem(hMdItem,  TVGN_NEXT );
	}
	pTreeX->Invalidate();
}


void CCTSMonDoc::UpdateGetdispinfoTree(TV_DISPINFO* pTVDispInfo, SECTreeCtrl *pTreeCtrlX)
{
	// TODO: Add your control notification handler code here
	ASSERT(pTVDispInfo);
	ASSERT(pTreeCtrlX);

	DWORD dwModuleID, dwGroupIndex, dwImgIndex;
	if (pTVDispInfo->item.mask & TVIF_IMAGE || pTVDispInfo->item.mask & TVIF_SELECTEDIMAGE)
	{
		if(!pTreeCtrlX->ItemHasChildren(pTVDispInfo->item.hItem) && pTreeCtrlX->GetParentItem(pTVDispInfo->item.hItem))		//Group Item
		{
			dwModuleID = pTreeCtrlX->GetItemData(pTreeCtrlX->GetParentItem(pTVDispInfo->item.hItem));
			dwGroupIndex = pTreeCtrlX->GetItemData(pTVDispInfo->item.hItem);
			dwImgIndex = GetStateImgIndex(EPGetGroupState(dwModuleID, dwGroupIndex));
		}
		else	//Module Item
		{
			
			dwModuleID = pTreeCtrlX->GetItemData(pTVDispInfo->item.hItem);
//			if( pTreeCtrlX->IsExpanded(pTVDispInfo->item.hItem) )
//				pTVDispInfo->item.iImage = GetModuleState(dwModuleID);
			
			if(m_bUseGroupSet)
				dwImgIndex = GetStateImgIndex(EPGetModuleState(dwModuleID));
			else
				dwImgIndex = GetStateImgIndex(EPGetGroupState(dwModuleID, 0));

		}

		//OnLine Icon 표시
/*		if(GetLineMode(dwModuleID) == EP_OFFLINE_MODE)
		{
			pTVDispInfo->item.iImage = dwImgIndex;	
			pTVDispInfo->item.iSelectedImage = dwImgIndex;	
		}
		else
		{
			pTVDispInfo->item.iImage = dwImgIndex + TOT_STATE_IMG_NO;	
			pTVDispInfo->item.iSelectedImage = dwImgIndex + TOT_STATE_IMG_NO;
		}
*/
		//OnLine Icon 표시
		if(dwModuleID > 0)
		{
			if(GetLineMode(dwModuleID) == EP_OFFLINE_MODE)
			{
				if(::EPGetAutoProcess(dwModuleID))
				{
					pTVDispInfo->item.iImage = dwImgIndex + TOT_STATE_IMG_NO;	
					pTVDispInfo->item.iSelectedImage = dwImgIndex + TOT_STATE_IMG_NO;
				}
				else
				{
					pTVDispInfo->item.iImage = dwImgIndex;	
					pTVDispInfo->item.iSelectedImage = dwImgIndex;
				}
			}
			else
			{
				pTVDispInfo->item.iImage = dwImgIndex + TOT_STATE_IMG_NO*2;	
				pTVDispInfo->item.iSelectedImage = dwImgIndex + TOT_STATE_IMG_NO*2;	
			}
		}
	}

/*	if (pTVDispInfo->item.mask & TVIF_SELECTEDIMAGE)
	{
		if(!pTreeCtrlX->ItemHasChildren(pTVDispInfo->item.hItem) && pTreeCtrlX->GetParentItem(pTVDispInfo->item.hItem))
		{
			dwModuleID = pTreeCtrlX->GetItemData(pTreeCtrlX->GetParentItem(pTVDispInfo->item.hItem));
			dwGroupIndex = pTreeCtrlX->GetItemData(pTVDispInfo->item.hItem);
			dwImgIndex = GetStateImgIndex(EPGetGroupState(dwModuleID, dwGroupIndex));
		}
		else
		{
			dwModuleID = pTreeCtrlX->GetItemData(pTVDispInfo->item.hItem);
			dwImgIndex = GetStateImgIndex(EPGetModuleState(dwModuleID));
		}
		pTVDispInfo->item.iImage = dwImgIndex;			
		pTVDispInfo->item.iSelectedImage = dwImgIndex + TOT_STATE_IMG_NO;
	}
*/
}

void CCTSMonDoc::WriteLog(char *szLog)
{
	TRACE("%s\n", szLog);

	char tempName[512];
	COleDateTime oleCurDate(COleDateTime::GetCurrentTime());
	sprintf(tempName, "%s\\Log\\%s", m_strCurFolder, oleCurDate.Format("\\%Y%m%d.log"));

	if(strcmp(m_szLogFileName, tempName) != 0)			//날짜가 바뀌면은 Log 파일 이름 변경
	{
		strcpy(m_szLogFileName, tempName);
		::EPSetLogFileName(m_szLogFileName);		//Set Log File Name
	}
	
	if(strlen(m_szLogFileName) == 0 || szLog == NULL)	return;
	
	WriteLogFile(m_szLogFileName, szLog, FILE_APPEND);
}

void CCTSMonDoc::WriteLog(CString strMsg)
{
	WriteLog((LPSTR)(LPCTSTR)strMsg);

/*	TRACE("%s\n", strMsg);
	
	if(strlen(m_szLogFileName) == 0 || strMsg.IsEmpty())	return;
	char szLog[512];
	sprintf(szLog, "%s", strMsg);
	WriteLogFile(m_szLogFileName, szLog, FILE_APPEND);
*/
}

void CCTSMonDoc::WriteEMGLog(CString strLog)
{
	//log file name 200401_EMG.log type
	char tempName[MAX_PATH];
	COleDateTime oleCurDate(COleDateTime::GetCurrentTime());
	sprintf(tempName, "%s\\Log\\%s", m_strCurFolder, oleCurDate.Format("\\%Y%m_EMG.csv"));
	
	//log string
	char szLog[512];
	sprintf(szLog, "%s", strLog);
	if(strlen(tempName) == 0 || strlen(szLog) == 0)		return;
	WriteLogFile(tempName, szLog, FILE_APPEND);
}

int CCTSMonDoc::GetInstalledModuleNum()
{
	return m_nInstalledModuleNum;
}

BOOL CCTSMonDoc::LoadSetting()
{
	char szBuff[512];
	if(GetCurrentDirectory(511, szBuff) < 1)	//Get Current Direcotry Name
	{
		sprintf(szBuff, "File Path Name Read Error %d\n", GetLastError());
		AfxMessageBox(szBuff);
		return FALSE;	
	}
	m_strCurFolder = szBuff;															//CTSMon Folder
	
	theApp.m_strCurFolder = m_strCurFolder;

	CWinApp* pApp = AfxGetApp();

	m_strCurFolder = pApp->GetProfileString(FORM_PATH_REG_SECTION ,"CTSMon");	//Get Current Folder(CTSMon Folder)
	if(m_strCurFolder.IsEmpty())
	{
		m_strCurFolder = szBuff;														
//		if(pApp->WriteProfileString(strSection, "PowerForm", m_strCurFolder) == 0)	return FALSE;	//Install Program에서 기록
		return FALSE;
	}

	m_strTempFolder.Format("%s\\Temp", m_strCurFolder);						//Get temp Folder
	
	m_strDBFolder = pApp->GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");		//Get DataBase Folder
	if(m_strDBFolder.IsEmpty())
	{
		m_strDBFolder.Format("%s\\Database", m_strCurFolder);
		if(pApp->WriteProfileString(FORM_PATH_REG_SECTION, "DataBase", m_strDBFolder) == 0)	return FALSE;
	}

	//DataBase Name
	m_strDataBaseName = m_strDBFolder+"\\"+FORM_SET_DATABASE_NAME;						
	
	//Log DataBase Name
	m_strLogDataBaseName = m_strDBFolder+"\\"+LOG_DATABASE_NAME;		


	//Get Data Folder
	m_strDataFolder = pApp->GetProfileString(FORM_PATH_REG_SECTION ,"Data");		
	if(m_strDataFolder.IsEmpty())
	{
		m_strDataFolder.Format("%s\\Data", m_strCurFolder);
		if(pApp->WriteProfileString(FORM_PATH_REG_SECTION, "Data", m_strDataFolder) == 0)	return FALSE;
	}
	
	//사용할 log 파일명 생성 (하루 단위로 생성)
	COleDateTime oleCurDate(COleDateTime::GetCurrentTime());
	sprintf(m_szLogFileName, "%s\\Log\\%s", m_strCurFolder, oleCurDate.Format("\\%Y%m%d.log"));
	::EPSetLogFileName(m_szLogFileName);		//Set Log File Name

	//명령 재전송 방지를 위한 Disable 시간 
	m_nCmdInterval = pApp->GetProfileInt(FORMSET_REG_SECTION, "CmdBlocking", m_nCmdInterval);
	
	//사용 할 Module의 이름 
	m_strModuleName = pApp->GetProfileString(FORMSET_REG_SECTION , "Module Name", "Stage");	//Get Current Folder(CTSMon Folde)
	//사용 할 Group의 이름 
	m_strGroupName = pApp->GetProfileString(FORMSET_REG_SECTION , "Group Name", "Group");	//Get Current Folder(CTSMon Folde)
	//한 Rack에 stack된 모듈 수 
	m_nModulePerRack = pApp->GetProfileInt(FORMSET_REG_SECTION , "Module Per Rack", 3);
	//Rack에 stack 형태의 모듈 번호 지정 
	m_bUseRackIndex = pApp->GetProfileInt(FORMSET_REG_SECTION , "Use Rack Index", TRUE);
	//Module=>Group 이용 
	m_bUseGroupSet = FALSE;	//not use 2006/11/27 pApp->GetProfileInt(FORMSET_REG_SECTION , "Use Group", FALSE);
	g_bUseGroupSet = m_bUseGroupSet;
	//OFF LINE, ON LINE 설정 
	m_bUseOnLine = pApp->GetProfileInt(FORMSET_REG_SECTION , "Use ON LINE", FALSE);
	
	//Top View에 Display 하는 TrayType
	m_nViewTrayType = TRAY_TYPE_CUSTOMER;

	m_bFolderModuleName = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "ModuleFolder", FALSE);
	m_bFolderTime = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "TimeFolder", FALSE);
	m_nTimeFolderInterval = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "FolderTimeInterval", 30);
	m_bFolderLot = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "LotFolder", FALSE);
	m_bFolderTray = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "TrayFolder", FALSE);
	
	g_strModuleName = m_strModuleName;
	g_strGroupName = m_strGroupName;
	g_nModulePerRack = m_nModulePerRack;
	g_bUseRackIndex	= m_bUseRackIndex;

//	m_bDemoMode = pApp->GetProfileInt(FORMSET_REG_SECTION, "Demo Mode", FALSE);
	//사용자 설정 Tray 보기 형태에서 사용할 Tray 배열에서 세로 줄 수 
	m_nTrayColSize = pApp->GetProfileInt(FORMSET_REG_SECTION, "Tray Col Size", EP_DEFAULT_TRAY_COL_COUNT);
	
	sprintf(szBuff, "%s\\%s", m_strCurFolder, FORM_SETTING_SAVE_FILE);
	::configFileName(szBuff);

	//Temp Monitoring 의 색상값 Loading
	m_TempScaleConfig = ::GetTempScaleConfig();
	//Temp Monitoring 의 색상값 Loading
	m_TempConfig = ::GetTempConfig();
	//Top Monitoring 의 색상값 Loading
	m_TopConfig = ::GetTopConfig();
	
	if(m_TopConfig.m_ChannelInterval == -1)
	{
		m_TopConfig = ::GetDefaultTopConfigSet();
//		WritePrivateProfileStruct("CONFIG", "TOPCONFIG", 
//						&m_TopConfig, sizeof(STR_TOP_CONFIG), INI_FILE);
	}
	m_gradeColorConfig = ::GetGradeColorConfig();

	//현재 프로그램의 server type 설정(ACIR/Formation)
	m_lSystemID = pApp->GetProfileInt(FORMSET_REG_SECTION, "System ID", 0);

	m_bHideNonCellData = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "HideNonCellData", FALSE);

	m_lDangerChVoltage = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "SafeChDangerVoltage", 4700);
	m_lWarnningChVoltage = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "SafeChWarnningVoltage", 4400);
	m_nAfterSafeVoltageError = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "AfterSafeVoltageError", 0);

	m_nContactErrlimit = AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "ContactCheckErrlimit", 1);	
	m_nCapaErrlimit = AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "CapaErrlimit", 3);	
	m_nChErrlimit = AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "ChErrlimit", 2);

	m_bUseFaultAlarm = AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "UseFaultAlarm", FALSE);	

	m_strCapacityFormula = AfxGetApp()->GetProfileString(FROM_SYSTEM_SECTION, "CapacityFormula", _T("C+(0.003341-0.03567*(T-25)+0.003342*((T-25)^2)-0.00011*((T-25)^3))"));
	m_strDCRFormula = AfxGetApp()->GetProfileString(FROM_SYSTEM_SECTION, "DCRFormula", _T("D/(0.000140218657265343*(2.71^(2626.15545994168/(273+T))))"));

	m_strTypeSel[0] = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION, "TypeSel1", "000");
	m_strTypeSel[1] = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION, "TypeSel2", "100");
	m_strTypeSel[2] = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION, "TypeSel3", "900");
	m_strTypeSel[3] = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION, "TypeSel4", "700");

	UpdateUnitSetting();
	return TRUE;
}

/*
//Remove Test Condition List
BOOL CCTSMonDoc::RemoveCondition(STR_CONDITION *pCondition)
{
	if(pCondition == NULL)	return TRUE;

	int nSize = pCondition->apStepList.GetSize();
	char *pObject; 

	if(nSize > 0)
	{
		for (int i = nSize-1 ; i>=0 ; i--)
		{
			if( (pObject = (char *)pCondition->apStepList[i]) != NULL )
			{
				delete[] pObject; // Delete the original element at 0.
				pObject = NULL;
				pCondition->apStepList.RemoveAt(i);  
			}
		}
		pCondition->apStepList.RemoveAll();
	}

	delete pCondition;
	pCondition = NULL;

	return TRUE;
}
*/

void CCTSMonDoc::InitProgressWnd()
{
	if(m_bProgressInit == FALSE)
	{
		m_pProgressWnd = new CProgressWnd;
		m_pProgressWnd->Create(AfxGetApp()->m_pMainWnd, "Progress", TRUE, FALSE);
		m_pProgressWnd->Hide();
		m_bProgressInit = TRUE;
	}
}

void CCTSMonDoc::SetProgressWnd(UINT nMin, UINT nMax, CString m_strTitle)
{
	if(!m_bProgressInit)
		InitProgressWnd();
	m_pProgressWnd->Clear();
	m_pProgressWnd->SetRange(nMin, nMax);
	m_pProgressWnd->SetText(m_strTitle);
//	m_pProgressWnd->PeekAndPump(FALSE);
	m_pProgressWnd->SetPos(nMin);
	m_pProgressWnd->Show();
//	BeginWaitCursor();
}

void CCTSMonDoc::SetProgressPos(int nPos)
{
	m_pProgressWnd->SetPos(nPos);
	m_pProgressWnd->PeekAndPump(FALSE);
}

void CCTSMonDoc::HideProgressWnd()
{
	m_pProgressWnd->Hide();
//	EndWaitCursor();
}

STR_TOP_CONFIG * CCTSMonDoc::GetTopConfig()
{
	return &m_TopConfig;
}

STR_TEMP_CONFIG * CCTSMonDoc::GetTempConfig()
{
	return &m_TempConfig;
}

STR_TEMPSCALE_CONFIG * CCTSMonDoc::GetTempScaleConfig()
{
	return &m_TempScaleConfig;
}

//////////////////////////////////////////////////////////
//														//
//					결과 파일 저장 구조					//
//														//		
//////////////////////////////////////////////////////////
//	1. EP_FILE_HEADER									//
//	2. RESULT_FILE_HEADER								//
//	3. EP_MD_SYSTEM_DATA								//
//	4. STR_FILE_EXT_SPACE								//	
//	5. EP_TEST_HEADER									//
//	6. STR_CONDITION_HEADER								//
//	7. EP_PRETEST_PARAM									//
//	8. 각 Step 구조 * Step 수							//
//////////////////////////////////////////////////////////			
//		9~12 항을 진행 Step 수 만큼 저장 한다.			//
//	9. GROUP_STATE_SAVE									//	
//	10. RESULT_FILE_HEADER								//
//	11	STR_COMMON_STEP									//
//	12. EP_STEP_END_HEADER								//
//	13. STR_SAVE_CH_DATA * Channel 수					//
//														//
//////////////////////////////////////////////////////////
//Step의 결과 Data를 현재 저장 파일의 가장 마지막에 덛붙여 쓴다.
BOOL CCTSMonDoc::SaveResultData(int nModuleID, int nTrayIndex, int nStepIndex, LPVOID lpData, STR_STEP_RESULT &rStepResult/*, EP_STEP_SUMMERY *pStepSum*/)
{
	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule == NULL)	
	{
		//20070514 가끔 Error 발생하는 것으로 추정됨
		//지속적인 로그 확인 필요 
		m_strLastErrorString.Format(TEXT_LANG[0], nModuleID);//"결과파일 저장시 모듈정보 error!!! (Code = %d)\n"
		WriteLog(m_strLastErrorString);		
		return FALSE;
	}
	
	CString strResultFile = GetResultFileName(nModuleID, nTrayIndex);
	if(strResultFile.IsEmpty())
	{
		m_strLastErrorString.Format(TEXT_LANG[1], ::GetModuleName(nModuleID));//"%s:: 결과 Data가 전송 되었으나 저장하지 않습니다.(결과 파일명이 없음)\n"
		WriteLog(m_strLastErrorString);
		return FALSE;
	}
	
	int nChCount = pModule->GetChInJig(nTrayIndex);
	//int nChCount = pModule->GetCellCountInTray();
	if(nChCount < 0)
	{
		m_strLastErrorString.Format("%s:: Step end data save fail.(Channel number in tray fail)\n", ::GetModuleName(nModuleID));
		WriteLog(m_strLastErrorString);
		return FALSE;
	}
	
	// 파일 존재 여부를 검사 하여 존재 하지 않으면 새로 만든다.
	// 공정이 진행 중인데 결과 파일이 삭제되면 새롭게 생성되고 
	// 그 다음 Step결과 부터 저장된다.
	FILE *fp = NULL;
	
	//Create Result File=> 파일 존재 여부 검사	
	if((fp = fopen(strResultFile, "r")) == NULL)
	{
		if(pModule->CreateTrayResultFile(nTrayIndex) == FALSE)
		{
			m_strLastErrorString.Format("%s:: Result file %s create fail.\n", ::GetModuleName(nModuleID), strResultFile);
			WriteLog(m_strLastErrorString);
			return FALSE;
		}
	}
	else
	{
		fclose(fp);
		fp = NULL;
	}

	//결과 파일 Open
	fp = fopen(strResultFile, "ab+");
	if(fp == NULL)
	{
		m_strLastErrorString.Format("%s :: Step end data save fail.(%s File open)\n", ::GetModuleName(nModuleID), strResultFile);
		WriteLog(m_strLastErrorString);
		return FALSE;
	}

	//////////////////////////////////////////////////////////////////////////
	//현재 Group의 상태 기록 
	//Sensor Data의 channel은 HW channel이다.
	EP_GP_DATA gpData = EPGetGroupData(nModuleID, 0);
/*	if(fwrite(&gpData, sizeof(EP_GP_DATA), 1, fp) < 1)
	{
		fclose(fp);
		m_strLastErrorString.Format("%s :: Step end data save fail(Group data)\n", ::GetModuleName(nModuleID));
		WriteLog(m_strLastErrorString);
		return FALSE;
	}
*/
	//Make Group Save Data
	ZeroMemory(&rStepResult.gpStateSave, sizeof(GROUP_STATE_SAVE));	
	rStepResult.gpStateSave.gpState.state = gpData.gpState.state;
	rStepResult.gpStateSave.gpState.failCode = gpData.gpState.failCode;
	rStepResult.gpStateSave.gpState.sensorState1 = gpData.gpState.sensorState[0];
	rStepResult.gpStateSave.gpState.sensorState2 = gpData.gpState.sensorState[1];
	rStepResult.gpStateSave.gpState.sensorState3 = gpData.gpState.sensorState[2];
	rStepResult.gpStateSave.gpState.sensorState4 = gpData.gpState.sensorState[3];
	rStepResult.gpStateSave.gpState.sensorState5 = gpData.gpState.sensorState[4];
	rStepResult.gpStateSave.gpState.sensorState6 = gpData.gpState.sensorState[5];
	rStepResult.gpStateSave.gpState.sensorState7 = gpData.gpState.sensorState[6];
	rStepResult.gpStateSave.gpState.sensorState8 = gpData.gpState.sensorState[7];

	_MAPPING_DATA *pMapData;
	for(int s=0; s<EP_MAX_SENSOR_CH; s++)
	{
		rStepResult.gpStateSave.sensorChData1[s].fData = ETC_PRECISION(gpData.sensorData.sensorData1[s].lData);
		pMapData = pModule->GetSensorMap(CFormModule::sensorType1, s);
 		if(pMapData)
 		{
 			rStepResult.gpStateSave.sensorChData1[s].lReserved = pMapData->nChannelNo;
 //			strcpy(rStepResult.gpStateSave.sensorChData1[s].szName, pMapData->szName);
 		}
		memcpy(&rStepResult.gpStateSave.sensorChData1[s].minmaxData, &gpData.sensorMinMax.sensorData1[s], sizeof(_MINMAX_DATA));

		rStepResult.gpStateSave.sensorChData2[s].fData = VTG_PRECISION(gpData.sensorData.sensorData2[s].lData);
 		pMapData = pModule->GetSensorMap(CFormModule::sensorType2, s);
 		if(pMapData)
 		{
 			rStepResult.gpStateSave.sensorChData2[s].lReserved = pMapData->nChannelNo;
 //			strcpy(rStepResult.gpStateSave.sensorChData2[s].szName, pMapData->szName);
 		}
		memcpy(&rStepResult.gpStateSave.sensorChData2[s].minmaxData, &gpData.sensorMinMax.sensorData2[s], sizeof(_MINMAX_DATA));
	}

	//Save
//	_STEP_END_HEADER sAuxData;
//	memcpy(gpStateSaveData.stepHeader, sizeof(_STEP_END_HEADER));

	if(fwrite(&rStepResult.gpStateSave, sizeof(GROUP_STATE_SAVE), 1, fp) < 1)
	{
		fclose(fp);
		m_strLastErrorString.Format("%s :: Step end data save fail(Group data)\n", ::GetModuleName(nModuleID));
		WriteLog(m_strLastErrorString);
		return FALSE;
	}
	//////////////////////////////////////////////////////////////////////////
	
	//////////////////////////////////////////////////////////////////////////
	// Step Header 정보 기록
	COleDateTime curTime = COleDateTime::GetCurrentTime();		//완료 시각 
	rStepResult.stepHead.nModuleID = nModuleID;
	rStepResult.stepHead.wGroupIndex = 0;
	rStepResult.stepHead.wJigIndex = nTrayIndex;							
	sprintf(rStepResult.stepHead.szEndDateTime, "%s", curTime.Format("%Y-%m-%d %H:%M:%S"));
	sprintf(rStepResult.stepHead.szModuleIP,	"%s", pModule->GetIPAddress());
	sprintf(rStepResult.stepHead.szOperatorID,	"%s", g_LoginData.szLoginID);	
	curTime = pModule->GetStepStartTime();
	sprintf(rStepResult.stepHead.szStartDateTime, "%s", curTime.Format("%Y-%m-%d %H:%M:%S"));
	sprintf(rStepResult.stepHead.szTestSerialNo, "%s", pModule->GetTestSerialNo(nTrayIndex));
	sprintf(rStepResult.stepHead.szTrayNo,		"%s", pModule->GetTrayNo(nTrayIndex));
	sprintf(rStepResult.stepHead.szTraySerialNo, "%d", pModule->GetTraySerialNo(nTrayIndex));

	if(fwrite(&rStepResult.stepHead, sizeof(RESULT_STEP_HEADER), 1, fp) < 1)
	{
		fclose(fp);
		m_strLastErrorString.Format("%s :: Step end data save fail(step header)\n", ::GetModuleName(nModuleID));
		WriteLog(m_strLastErrorString);
		return FALSE;
	}
	//////////////////////////////////////////////////////////////////////////
	
	/////////////////////////////////////////////////////////////////////////
	//현재 Step의  조건을 기록 한다.
	ZeroMemory(&rStepResult.stepCondition, sizeof(STR_COMMON_STEP));
	CTestCondition *pCondition = pModule->GetCondition();
	CStep *pStep = pCondition->GetStep(nStepIndex);
	if(pStep) 
	{
		rStepResult.stepCondition = pStep->GetStepData();
	}
	
	if(fwrite(&rStepResult.stepCondition, sizeof(STR_COMMON_STEP), 1, fp) < 1)
	{
		fclose(fp);
		m_strLastErrorString.Format("%s :: Step end data save fail(Step condition)\n", ::GetModuleName(nModuleID));
		WriteLog(m_strLastErrorString);
		return FALSE;
	}
	//////////////////////////////////////////////////////////////////////////
	
	///////////////////////////////////////////////////////////////////////////
	//전체널 Step 결과에 대한 종합값 사용 안함 2006/2/16 (의미 없는 data)
	EP_STEP_END_HEADER endHeader;
	endHeader.wStep = rStepResult.stepCondition.stepHeader.stepIndex+1;
	if(fwrite(&endHeader, sizeof(EP_STEP_END_HEADER), 1, fp) < 1)
	{
		fclose(fp);
		m_strLastErrorString.Format("%s :: Step end data save fail.(summary of step data)\n", ::GetModuleName(nModuleID));
		WriteLog(m_strLastErrorString);
		return FALSE;
	}
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	//Channel data structure type changed by KBH 2005/12/27
	//각 Channel의 측정값 기록
	if(fwrite(lpData, sizeof(STR_SAVE_CH_DATA)*nChCount, 1, fp) < 1)
	{
		fclose(fp);
		m_strLastErrorString.Format("%s :: Step end data save fail(Channel data)\n", ::GetModuleName(nModuleID));
		WriteLog(m_strLastErrorString);
		return FALSE;
	}
	//////////////////////////////////////////////////////////////////////////
	fclose(fp);
	return TRUE;
}

BOOL CCTSMonDoc::SaveCellChkResultData(int nModuleID, int nTrayIndex, int nStepIndex, LPVOID lpData, STR_STEP_RESULT &rStepResult/*, EP_STEP_SUMMERY *pStepSum*/)
{
	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule == NULL)	
	{
		//20070514 가끔 Error 발생하는 것으로 추정됨
		//지속적인 로그 확인 필요 
		m_strLastErrorString.Format(TEXT_LANG[0], nModuleID);//"결과파일 저장시 모듈정보 error!!! (Code = %d)\n"
		WriteLog(m_strLastErrorString);		
		return FALSE;
	}
	
	CString strResultFile = GetResultFileName(nModuleID, nTrayIndex);
	CString strTemp = _T("");
	strTemp = strResultFile.Left( strResultFile.ReverseFind('.'));
	strResultFile = strTemp + "_CON.Fmt";
	
	if(strResultFile.IsEmpty())
	{
		m_strLastErrorString.Format(TEXT_LANG[1], ::GetModuleName(nModuleID));//"%s:: 결과 Data가 전송 되었으나 저장하지 않습니다.(결과 파일명이 없음)\n"
		WriteLog(m_strLastErrorString);
		return FALSE;
	}
	
	int nChCount = pModule->GetChInJig(nTrayIndex);
	//int nChCount = pModule->GetCellCountInTray();
	if(nChCount < 0)
	{
		m_strLastErrorString.Format("%s:: Step end data save fail.(Channel number in tray fail)\n", ::GetModuleName(nModuleID));
		WriteLog(m_strLastErrorString);
		return FALSE;
	}

	// Cell Contact check data를 확인해서 있으면 동일한 파일이 있으면 삭제한다.
	FILE *fp = NULL;
	// Create Result File=> 파일 존재 여부 검사	
	if((fp = fopen(strResultFile, "r")) != NULL )
	{
		fclose(fp);
		fp = NULL;

		DeleteFile(strResultFile);		
	}	
	
	if(pModule->CreateTrayCellCheckResultFile(nTrayIndex, strResultFile) == FALSE )
	{
		m_strLastErrorString.Format("%s:: Result file %s create fail.\n", ::GetModuleName(nModuleID), strResultFile);
		WriteLog(m_strLastErrorString);
		return FALSE;
	}	

	//결과 파일 Open
	fp = fopen(strResultFile, "ab+");
	if(fp == NULL)
	{
		m_strLastErrorString.Format("%s :: Step end data save fail.(%s File open)\n", ::GetModuleName(nModuleID), strResultFile);
		WriteLog(m_strLastErrorString);
		return FALSE;
	}

	//////////////////////////////////////////////////////////////////////////
	//현재 Group의 상태 기록 
	//Sensor Data의 channel은 HW channel이다.
	EP_GP_DATA gpData = EPGetGroupData(nModuleID, 0);
/*	if(fwrite(&gpData, sizeof(EP_GP_DATA), 1, fp) < 1)
	{
		fclose(fp);
		m_strLastErrorString.Format("%s :: Step end data save fail(Group data)\n", ::GetModuleName(nModuleID));
		WriteLog(m_strLastErrorString);
		return FALSE;
	}
*/
	//Make Group Save Data
	ZeroMemory(&rStepResult.gpStateSave, sizeof(GROUP_STATE_SAVE));	
	rStepResult.gpStateSave.gpState.state = gpData.gpState.state;
	rStepResult.gpStateSave.gpState.failCode = gpData.gpState.failCode;
	rStepResult.gpStateSave.gpState.sensorState1 = gpData.gpState.sensorState[0];
	rStepResult.gpStateSave.gpState.sensorState2 = gpData.gpState.sensorState[1];
	rStepResult.gpStateSave.gpState.sensorState3 = gpData.gpState.sensorState[2];
	rStepResult.gpStateSave.gpState.sensorState4 = gpData.gpState.sensorState[3];
	rStepResult.gpStateSave.gpState.sensorState5 = gpData.gpState.sensorState[4];
	rStepResult.gpStateSave.gpState.sensorState6 = gpData.gpState.sensorState[5];
	rStepResult.gpStateSave.gpState.sensorState7 = gpData.gpState.sensorState[6];
	rStepResult.gpStateSave.gpState.sensorState8 = gpData.gpState.sensorState[7];

	_MAPPING_DATA *pMapData;
	for(int s=0; s<EP_MAX_SENSOR_CH; s++)
	{
		rStepResult.gpStateSave.sensorChData1[s].fData = ETC_PRECISION(gpData.sensorData.sensorData1[s].lData);
		pMapData = pModule->GetSensorMap(CFormModule::sensorType1, s);
 		if(pMapData)
 		{
 			rStepResult.gpStateSave.sensorChData1[s].lReserved = pMapData->nChannelNo;
 //			strcpy(rStepResult.gpStateSave.sensorChData1[s].szName, pMapData->szName);
 		}
		memcpy(&rStepResult.gpStateSave.sensorChData1[s].minmaxData, &gpData.sensorMinMax.sensorData1[s], sizeof(_MINMAX_DATA));

		rStepResult.gpStateSave.sensorChData2[s].fData = VTG_PRECISION(gpData.sensorData.sensorData2[s].lData);
 		pMapData = pModule->GetSensorMap(CFormModule::sensorType2, s);
 		if(pMapData)
 		{
 			rStepResult.gpStateSave.sensorChData2[s].lReserved = pMapData->nChannelNo;
 //			strcpy(rStepResult.gpStateSave.sensorChData2[s].szName, pMapData->szName);
 		}
		memcpy(&rStepResult.gpStateSave.sensorChData2[s].minmaxData, &gpData.sensorMinMax.sensorData2[s], sizeof(_MINMAX_DATA));
	}

	//Save
//	_STEP_END_HEADER sAuxData;
//	memcpy(gpStateSaveData.stepHeader, sizeof(_STEP_END_HEADER));	
	if(fwrite(&rStepResult.gpStateSave, sizeof(GROUP_STATE_SAVE), 1, fp) < 1)
	{
		fclose(fp);
		m_strLastErrorString.Format("%s :: Step end data save fail(Group data)\n", ::GetModuleName(nModuleID));
		WriteLog(m_strLastErrorString);
		return FALSE;
	}
	//////////////////////////////////////////////////////////////////////////
	
	//////////////////////////////////////////////////////////////////////////
	// Step Header 정보 기록
	COleDateTime curTime = COleDateTime::GetCurrentTime();		//완료 시각 
	rStepResult.stepHead.nModuleID = nModuleID;
	rStepResult.stepHead.wGroupIndex = 0;
	rStepResult.stepHead.wJigIndex = nTrayIndex;							
	sprintf(rStepResult.stepHead.szEndDateTime, "%s", curTime.Format());
	sprintf(rStepResult.stepHead.szModuleIP,	"%s", pModule->GetIPAddress());
	sprintf(rStepResult.stepHead.szOperatorID,	"%s", g_LoginData.szLoginID);	
	curTime = pModule->GetStepStartTime();
	sprintf(rStepResult.stepHead.szStartDateTime, "%s", curTime.Format("%Y-%m-%d %H:%M:%S"));
	sprintf(rStepResult.stepHead.szTestSerialNo, "%s", pModule->GetTestSerialNo(nTrayIndex));
	sprintf(rStepResult.stepHead.szTrayNo,		"%s", pModule->GetTrayNo(nTrayIndex));
	sprintf(rStepResult.stepHead.szTraySerialNo, "%d", pModule->GetTraySerialNo(nTrayIndex));

	if(fwrite(&rStepResult.stepHead, sizeof(RESULT_STEP_HEADER), 1, fp) < 1)
	{
		fclose(fp);
		m_strLastErrorString.Format("%s :: Step end data save fail(step header)\n", ::GetModuleName(nModuleID));
		WriteLog(m_strLastErrorString);
		return FALSE;
	}
	//////////////////////////////////////////////////////////////////////////
	
	/////////////////////////////////////////////////////////////////////////
	//현재 Step의  조건을 기록 한다.
	ZeroMemory(&rStepResult.stepCondition, sizeof(STR_COMMON_STEP));
	CTestCondition *pCondition = pModule->GetCondition();
	CStep *pStep = pCondition->GetStep(nStepIndex);
	if(pStep) 
	{
		rStepResult.stepCondition = pStep->GetStepData();
		rStepResult.stepCondition.stepHeader.nProcType = EP_PROC_TYPE_END1;
	}
	
	if(fwrite(&rStepResult.stepCondition, sizeof(STR_COMMON_STEP), 1, fp) < 1)
	{
		fclose(fp);
		m_strLastErrorString.Format("%s :: Step end data save fail(Step condition)\n", ::GetModuleName(nModuleID));
		WriteLog(m_strLastErrorString);
		return FALSE;
	}
	//////////////////////////////////////////////////////////////////////////
	
	///////////////////////////////////////////////////////////////////////////
	//전체널 Step 결과에 대한 종합값 사용 안함 2006/2/16 (의미 없는 data)
	EP_STEP_END_HEADER endHeader;
	endHeader.wStep = rStepResult.stepCondition.stepHeader.stepIndex+1;
	if(fwrite(&endHeader, sizeof(EP_STEP_END_HEADER), 1, fp) < 1)
	{
		fclose(fp);
		m_strLastErrorString.Format("%s :: Step end data save fail.(summary of step data)\n", ::GetModuleName(nModuleID));
		WriteLog(m_strLastErrorString);
		return FALSE;
	}
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	//Channel data structure type changed by KBH 2005/12/27
	//각 Channel의 측정값 기록
	if(fwrite(lpData, sizeof(STR_SAVE_CH_DATA)*nChCount, 1, fp) < 1)
	{
		fclose(fp);
		m_strLastErrorString.Format("%s :: Step end data save fail(Channel data)\n", ::GetModuleName(nModuleID));
		WriteLog(m_strLastErrorString);
		return FALSE;
	}
	//////////////////////////////////////////////////////////////////////////
	fclose(fp);
	return TRUE;
}

// 1. Step End 일 경우
// 2. 공정 마지막 Charge, Discharge step 에 대해서만 체크하고
// 3. 변수에 데이터 입력 후 DB접속은 한번만 시도하는 걸로 한다.
bool CCTSMonDoc::Fun_UpdatePrecisionData( int nModuleID )
{
	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule == NULL)	
	{
		m_strLastErrorString.Format(TEXT_LANG[2], nModuleID);//"결과파일 저장시 모듈정보 error! (Code = %d)\n"
		WriteLog(m_strLastErrorString);		
		return FALSE;
	}
	
	bool bRtn = false;
	int sqlerr = 0;
	sqlite3 *mSqlite;
	
	// 1. precision DB에 insert 하기위해 취합할 데이터
	// 2. CH 단위로 나눠짐.
	unsigned long nCVFaultCnt = 0;	
	unsigned long nCVLevel1Fault = 0; 
	unsigned long nCVLevel2Fault = 0; 
	unsigned long nCVLevel3Fault = 0; 
	unsigned long nCCFaultCnt = 0;
	unsigned long nCCLevel1Fault = 0; 
	unsigned long nCCLevel2Fault = 0; 
	unsigned long nCCLevel3Fault = 0;
	
	CString dbname = _T("");
	dbname.Format("%s\\StageInfo\\Stage_%02d.db", m_strDBFolder, nModuleID);
	
	sqlerr = sqlite3_open( dbname, &mSqlite );
	
	sqlite3_stmt * stmt = 0;
	
	COleDateTime OleUpdateTime = COleDateTime::GetCurrentTime();
	
	INT64 idx = OleUpdateTime.GetYear()	* 10000000000;
	idx += OleUpdateTime.GetMonth()		* 100000000;
	idx += OleUpdateTime.GetDay()		* 1000000;
	// idx += OleUpdateTime.GetHour()		* 10000;
	// idx += OleUpdateTime.GetMinute()		* 100;
	// idx += OleUpdateTime.GetSecond();
	
	CTestCondition *pProc;
	CStep *pStep = NULL;
	STR_SAVE_CH_DATA chSaveData;	
	
	int nStepIndex = 0;
	int nMaxChannel = 0;
	int nChannelIndex = 0;
	int nCh = 0;
	int k = 0;
	pProc = pModule->GetCondition();
	nMaxChannel = pModule->GetChInJig(0);
	
	if(pProc != NULL)
	{
		sqlite3_prepare( mSqlite, "INSERT into ResultData(\
								  ChNo \
								  , UpdateTime \
								  , CVFaultCnt \
								  , CVLevel3Fault \
								  , CVLevel2Fault \
								  , CVLevel1Fault \
								  , CCFaultCnt \
								  , CCLevel3Fault \
								  , CCLevel2Fault \
								  , CCLevel1Fault \
								  ) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?);", -1, &stmt, 0 );

		for( nChannelIndex = 0; nChannelIndex<nMaxChannel; nChannelIndex++ )
		{							  
			for( nStepIndex = 0; nStepIndex<pProc->GetTotalStepNo(); nStepIndex++ )
			{
				pStep = pProc->GetStep(nStepIndex);		//Step 번호가 1 Base	
				if( pStep != NULL )
				{
					if( pStep->m_type == EP_TYPE_CHARGE || pStep->m_type == EP_TYPE_DISCHARGE )
					{
						pModule->GetChannelStepData( chSaveData, nChannelIndex, 0, nStepIndex);

						nCVFaultCnt += chSaveData.ulTotalSamplingVoltageCnt;
						nCCFaultCnt += chSaveData.ulTotalSamplingCurrentCnt;

						nCCLevel1Fault += chSaveData.currentFaultLevel1Cnt;
						nCCLevel2Fault += chSaveData.currentFaultLevel2Cnt;
						nCCLevel3Fault += chSaveData.currentFaultLevel3Cnt;

						nCVLevel1Fault += chSaveData.voltageFaultLevelV1Cnt;
						nCVLevel2Fault += chSaveData.voltageFaultLevelV2Cnt;
						nCVLevel3Fault += chSaveData.voltageFaultLevelV3Cnt;
					}				
				}
			}

			k = 1;
			sqlite3_bind_int(stmt, k++, nChannelIndex+1);
			sqlite3_bind_int64(stmt, k++, idx);

			sqlite3_bind_int64(stmt, k++, nCVFaultCnt);
			sqlite3_bind_int64(stmt, k++, nCVLevel3Fault);
			sqlite3_bind_int64(stmt, k++, nCVLevel2Fault);
			sqlite3_bind_int64(stmt, k++, nCVLevel1Fault);

			sqlite3_bind_int64(stmt, k++, nCCFaultCnt);
			sqlite3_bind_int64(stmt, k++, nCCLevel3Fault);
			sqlite3_bind_int64(stmt, k++, nCCLevel2Fault);
			sqlite3_bind_int64(stmt, k++, nCCLevel1Fault);

			sqlerr = sqlite3_step(stmt);

			if(SQLITE_DONE != sqlerr)
			{
				LPSTR szError = (LPSTR)sqlite3_errmsg(mSqlite);
				CFMSLog::WriteLog(szError);

				sqlerr = sqlite3_reset(stmt);

				if (sqlerr != SQLITE_OK)
				{
					szError = (LPSTR)sqlite3_errmsg(mSqlite);
					CFMSLog::WriteLog(szError);
				}
			}

			sqlerr = sqlite3_reset(stmt);

			if (sqlerr != SQLITE_OK)
			{
				LPSTR szError = (LPSTR)sqlite3_errmsg(mSqlite);
				CFMSLog::WriteLog(szError);
			}

			nCVFaultCnt = 0;
			nCCFaultCnt = 0;
			nCCLevel1Fault = 0;
			nCCLevel2Fault = 0;
			nCCLevel3Fault = 0;
			nCVLevel1Fault = 0;
			nCVLevel2Fault = 0;
			nCVLevel3Fault = 0;
		}
	}

	sqlite3_finalize(stmt);
	sqlite3_close(mSqlite);
	
	return true;
}

//Step의 결과 Data를 Products.mdb Cell BarCode 별로 저장 한다
BOOL CCTSMonDoc::SaveToProductsMDB(int nModuleID, int nTrayIndex, int nStepIndex, EP_CH_DATA *lpData, CTray *pTrayInfo/*, EP_STEP_SUMMERY *pStepSum*/)
{
	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule == NULL)	
	{
		//20070514 가끔 Error 발생하는 것으로 추정됨
		//지속적인 로그 확인 필요 
		m_strLastErrorString.Format(TEXT_LANG[0], nModuleID);//"결과파일 저장시 모듈정보 error!!! (Code = %d)\n"
		WriteLog(m_strLastErrorString);		
		return FALSE;
	}

#ifdef _DEBUG		
	DWORD dwStartTime = timeGetTime();
	m_strLastErrorString.Format("%s :: Step end data save(Product.mdb) start", ::GetModuleName(nModuleID));
	WriteLog(m_strLastErrorString);
#endif

	CString strResultFile = GetResultFileName(nModuleID, nTrayIndex);
	if(strResultFile.IsEmpty())
	{
		m_strLastErrorString.Format(TEXT_LANG[1], ::GetModuleName(nModuleID));//"%s:: 결과 Data가 전송 되었으나 저장하지 않습니다.(결과 파일명이 없음)\n"
		WriteLog(m_strLastErrorString);
		return FALSE;
	}
	
	int nChCount = pModule->GetChInJig(nTrayIndex);
	if(nChCount < 0)
	{
		m_strLastErrorString.Format("%s:: Step end data save fail.(Channel number in tray fail)\n", ::GetModuleName(nModuleID));
		WriteLog(m_strLastErrorString);
		return FALSE;
	}

	CString strCellBCR;
	/////////////////////////////////////////////////////////////////////////
	//현재 Step의 DATA 기록 한다.
	CTestCondition *pCondition = pModule->GetCondition();
	CStep *pStep = pCondition->GetStep(nStepIndex);
	if(pStep) 
	{
			// Cell BarCode에 해당하는 데이터 기록
			// 각 Channel의 측정값 Products.mdb -> ReportCellData에 기록
			
			CDatabase  db;
			try
			{				
				db.OpenEx(DSN_DATABASE_NAME,0);
				for (int nCh=0 ; nCh < nChCount ; nCh++)
				{
					
					strCellBCR = pTrayInfo->GetCellSerial(nCh);
					if (strCellBCR != "")
					{
						//Database에 업데이트 한다
						if (pStep->m_type == EP_TYPE_END)
						{
							if (Fun_UpdateCellLastTime(db,strCellBCR) == FALSE)
							{
								m_strLastErrorString.Format("%s:: Update LastEndTime Fail (BCR : %s) \n", ::GetModuleName(nModuleID),strCellBCR);
								WriteLog(m_strLastErrorString);
							}
						}
						else
						{
							if (Fun_UpdateCellData(db,strCellBCR,pModule->GetLotNo(),pTrayInfo->GetTrayNo()
								,pStep->m_lProcType,lpData[nCh].lVoltage,lpData[nCh].lCurrent,lpData[nCh].lCapacity,lpData[nCh].ulStepTime) == FALSE)
							{
								m_strLastErrorString.Format("%s:: BCR : %s , Update Fail \n", ::GetModuleName(nModuleID),strCellBCR);
								TRACE(m_strLastErrorString);
								//WriteLog(m_strLastErrorString);
							}
						}
					}
				}
				db.Close();
			}
			catch (CDBException* e)
			{
//				AfxMessageBox(e->m_pErrorInfo->m_strDescription);
				AfxMessageBox(e->m_strError+e->m_strStateNativeOrigin); 
				e->Delete();
				return FALSE;
			}
// 			catch(CMemoryException *e) 
// 			{ 
// 				AfxMessageBox(e->m_szMessage); 
// 			} 
	}
	//////////////////////////////////////////////////////////////////////////

// // 20090504 kky for 저장파일 로고 끝
	#ifdef _DEBUG
		dwStartTime = timeGetTime() - dwStartTime;
		m_strLastErrorString.Format("%s :: Step End Data Save(Product.mdb) (Escaped Time %d msec)\n", ::GetModuleName(nModuleID), dwStartTime);
		WriteLog(m_strLastErrorString);
	#endif
	return TRUE;
}

bool CCTSMonDoc::SaveEmgToProductsMDB(int nModuleID, int nOperationMode, CString strCode, CString strMessage, CString strTrayNo, int nType )
{	
	CString strTemp = _T("");
	CString strSQL = _T("");
	
	CDaoDatabase  db;
	COleVariant data;	
	
	try
	{
		db.Open(m_strLogDataBaseName);	
		
		strSQL.Format("Insert into EMG_Log(Stage, OperationMode, Code, Message, TrayNo, Type) VALUES( '%s', %d, '%s', '%s', '%s', %d);", 
					GetModuleName(nModuleID), nOperationMode, strCode, strMessage, strTrayNo, nType
		);
		
		db.Execute(strSQL);
	}
	catch (CDBException* e)
	{	
		AfxMessageBox(e->m_strError+e->m_strStateNativeOrigin); 
		e->Delete();
		db.Close();
		return FALSE;
	}	
	db.Close();	
	return TRUE;
}

int CCTSMonDoc::LoadEmgFromProductsMDB()
{	
	ZeroMemory(m_EmgList, sizeof(STR_EMG_LOG)*200);
	m_nEmgLogIndex = 0;
	m_nEmgMaxCnt = 0;
	
	CString strTemp = _T("");
	CString strSQL = _T("");	

	CDaoDatabase  db;
	COleVariant data;
	
	int nCount = -1;
	
	try
	{				
		db.Open(m_strLogDataBaseName);
			
		strSQL.Format("Select * From EMG_Log where [CheckFlag] = 0 order by DateTime DESC");

		CDaoRecordset rs(&db);
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);

		while(!rs.IsEOF())
		{
			nCount++;			
			m_EmgList[nCount].bUse = true;
			data = rs.GetFieldValue(0);
			m_EmgList[nCount].nNo = data.lVal;					
			data = rs.GetFieldValue(1); 
			sprintf(m_EmgList[nCount].szStage, "%s", data.pbVal);
			data = rs.GetFieldValue(2);
			m_EmgList[nCount].nOperationMode = data.lVal;			
			data = rs.GetFieldValue(3);
			sprintf(m_EmgList[nCount].szCode, "%s", data.pbVal);			
			data = rs.GetFieldValue(4);
			sprintf(m_EmgList[nCount].szMessage, "%s", data.pbVal);			
			data = rs.GetFieldValue(5);
			COleDateTime dt(data.date);
			sprintf(m_EmgList[nCount].curTime, "%s", dt.Format("%y/%m/%d %H:%M:%S") ); //.Format("%Y%m%d"));
			
			if( nCount > 98 )
			{
				break;						
			}
			else
			{
				rs.MoveNext();			
			}
		}
		rs.Close();
	}
	catch (CDBException* e)
	{	
		AfxMessageBox(e->m_strError+e->m_strStateNativeOrigin); 
		e->Delete();
		return FALSE;
	}
	db.Close();
	return nCount;
}

int CCTSMonDoc::DeleteEmgFromProductsMDB( int nLogIndex )
{	
	CString strTemp = _T("");
	CString strSQL = _T("");	

	CDaoDatabase  db;
	COleVariant data;

	int nCount = -1;

	try
	{				
		db.Open(m_strLogDataBaseName);		
		strSQL.Format("UPDATE EMG_Log SET [CheckFlag]=1 WHERE [No] = %d", m_EmgList[nLogIndex].nNo );
		db.Execute(strSQL);
	}
	catch (CDBException* e)
	{	
		AfxMessageBox(e->m_strError+e->m_strStateNativeOrigin); 
		e->Delete();
		return FALSE;
	}

	try
	{
		ZeroMemory(m_EmgList, sizeof(STR_EMG_LOG)*200);
		m_nEmgLogIndex = 0;
		m_nEmgMaxCnt = 0;

		strSQL.Format("Select * From EMG_Log where [CheckFlag] = 0 order by DateTime DESC");

		CDaoRecordset rs(&db);
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	

		while(!rs.IsEOF())
		{	
			nCount++;
			m_EmgList[nCount].bUse = true;
			data = rs.GetFieldValue(0);
			m_EmgList[nCount].nNo = data.lVal;
			data = rs.GetFieldValue(1); 			
			sprintf(m_EmgList[nCount].szStage, "%s", data.pbVal);			
			data = rs.GetFieldValue(2);
			m_EmgList[nCount].nOperationMode = data.lVal;			
			data = rs.GetFieldValue(3);
			sprintf(m_EmgList[nCount].szCode, "%s", data.pbVal);
			data = rs.GetFieldValue(4);
			sprintf(m_EmgList[nCount].szMessage, "%s", data.pbVal);			
			data = rs.GetFieldValue(5);
			COleDateTime dt(data.date);
			sprintf(m_EmgList[nCount].curTime, "%s", dt.Format("%y/%m/%d %H:%M:%S") ); //.Format("%Y%m%d"));
			
			if( nCount > 98 )
			{
				break;						
			}
			else
			{
				rs.MoveNext();			
			}
		}
		rs.Close();
	}
	catch (CDBException* e)
	{	
		AfxMessageBox(e->m_strError+e->m_strStateNativeOrigin); 
		e->Delete();
		return FALSE;
	}
	db.Close();
	return nCount;
}

CString CCTSMonDoc::GetTestName(int nModuleID, int nGroupIndex)
{
	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule == NULL)		return "";
	
	if(GetLineMode(nModuleID, nGroupIndex) == EP_ONLINE_MODE)
	{
		return pModule->GetTrayName();
	}
	else
	{
		return pModule->GetTestName();
	}
}

CString CCTSMonDoc::GetResultFileName(int nModuleID, int nTrayIndex)
{	
	CString strTemp;
	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule == NULL)		return "";
	
	return pModule->GetResultFileName(nTrayIndex);
}


CString CCTSMonDoc::GetStateMsg(WORD State, BYTE &colorFlag)
{
	CString strMsg;
	switch (State)
	{
		case EP_STATE_IDLE		:		strMsg = ::GetStringTable(IDS_TEXT_IDLE);			colorFlag = 0;	break;
		case EP_STATE_SELF_TEST	:		strMsg = ::GetStringTable(IDS_TEXT_SELF_TEST);		colorFlag = 3;	break;
		case EP_STATE_STANDBY	:		strMsg = ::GetStringTable(IDS_TEXT_STANDBY);		colorFlag = 2;	break;
		case EP_STATE_RUN		:		strMsg = ::GetStringTable(IDS_TEXT_RUN);			colorFlag = 4;	break;
		// case EP_STATE_PAUSE	:		strMsg = ::GetStringTable(IDS_TEXT_PAUSE);			colorFlag = 6;	break;
		case EP_STATE_PAUSE		:		strMsg = "Error";									colorFlag = 5;	break;
		case EP_STATE_FAIL		:		strMsg = "Error";									colorFlag = 5;	break;
		case EP_STATE_MAINTENANCE:		strMsg = ::GetStringTable(IDS_TEXT_MAINTENANCE);	colorFlag = 8;	break;
		case EP_STATE_OCV		:		strMsg = ::GetStringTable(IDS_TEXT_OCV);			colorFlag = 10;	break;
		case EP_STATE_CHARGE	:		strMsg = ::GetStringTable(IDS_TEXT_CHARGE);			colorFlag = 11;	break;
		case EP_STATE_DISCHARGE	:		strMsg = ::GetStringTable(IDS_TEXT_DISCHARGE);		colorFlag = 12;	break;
		case EP_STATE_REST		:		strMsg = ::GetStringTable(IDS_TEXT_REST);			colorFlag = 13;	break;
		case EP_STATE_IMPEDANCE	:		strMsg = ::GetStringTable(IDS_TEXT_IMPEDANCE);		colorFlag = 14;	break;
		case EP_STATE_CHECK		:		strMsg = ::GetStringTable(IDS_TEXT_CHECK);			colorFlag = 3;	break;
		case EP_STATE_STOP		:		strMsg = ::GetStringTable(IDS_TEXT_STOP)	;		colorFlag = 1;	break;
		case EP_STATE_END		:		strMsg = ::GetStringTable(IDS_TEXT_END);			colorFlag = 7;	break;
		case EP_STATE_FAULT		:		strMsg = ::GetStringTable(IDS_TEXT_FAULT);			colorFlag = 5;	break;
		case EP_STATE_READY		:		strMsg = ::GetStringTable(IDS_TEXT_READY);			colorFlag = 0;	break;
		case EP_STATE_LINE_OFF	:		strMsg = ::GetStringTable(IDS_TEXT_LINE_OFF);		colorFlag = 9;	break;	
		case EP_STATE_LINE_ON	:		strMsg = ::GetStringTable(IDS_TEXT_LINE_ON);		colorFlag = 0;	break;
		case EP_STATE_EMERGENCY	:		strMsg = ::GetStringTable(IDS_TEXT_EMG);			colorFlag = 9;	break;
		case EP_STATE_NONCELL   :		strMsg = ::GetStringTable(IDS_TEXT_NONCELL);		colorFlag = 5;	break;
		case EP_STATE_COMMON	:		
		case EP_STATE_ERROR		:		
		default:						strMsg = "Error";		colorFlag = 9;	break;
	}
	return strMsg;
}

CString CCTSMonDoc::GetAutoStateMsg(WORD State, BYTE &colorFlag)
{		
	CString strMsg = _T("");
	// 7:Red, 8:blue
	switch (State)
	{
		case FMS_ST_OFF:			strMsg = "Line Off";	colorFlag = 4;	break;
		case FMS_ST_VACANCY	:		strMsg = "Vacancy";		colorFlag = 0;	break;
		case FMS_ST_TRAY_IN	:		strMsg = "Tray In";		colorFlag = 1;	break;
		case FMS_ST_RUNNING	:		strMsg = "Running";		colorFlag = 2;	break;
		case FMS_ST_END		:		strMsg = "End";			colorFlag = 3;	break;
		case FMS_ST_ERROR	:		strMsg = "Error";		colorFlag = 4;	break;
		case FMS_ST_READY	:		strMsg = "Ready";		colorFlag = 5;	break;
		case FMS_ST_CONTACT_CHECK:	strMsg = "Contact Check";		colorFlag = 6;	break;
		case FMS_ST_RED_READY:		strMsg = "Ready";		colorFlag = 7;	break;
		case FMS_ST_RED_END:		strMsg = "End";			colorFlag = 7;	break;
		case FMS_ST_BLUE_END:		strMsg = "End";			colorFlag = 8;	break;
		case FMS_ST_RED_TRAY_IN:	strMsg = "Tray In";		colorFlag = 7;	break;
		default:					strMsg = "Error";		colorFlag = 4;	break;
	}
	
	return strMsg;
}

UINT CCTSMonDoc::GetStateImgIndex(WORD state)
{
	UINT	nIndex;
	switch (state)
	{
		case EP_STATE_IDLE		:		nIndex = 1;		break;
		case EP_STATE_SELF_TEST	:		nIndex = 0;		break;
		case EP_STATE_STANDBY	:		nIndex = 2;		break;
		case EP_STATE_RUN		:		nIndex = 4;		break;
		case EP_STATE_PAUSE		:		nIndex = 6;		break;
		case EP_STATE_FAIL		:		nIndex = 5;		break;	
		case EP_STATE_MAINTENANCE:		nIndex = 8;		break;
		case EP_STATE_OCV		:		nIndex = 4;		break;
		case EP_STATE_CHARGE	:		nIndex = 4;		break;
		case EP_STATE_DISCHARGE	:		nIndex = 4;		break;
		case EP_STATE_REST		:		nIndex = 2;		break;	
		case EP_STATE_IMPEDANCE	:		nIndex = 3;		break;
		case EP_STATE_CHECK		:		nIndex = 3;		break;
		case EP_STATE_STOP		:		nIndex = 7;		break;
		case EP_STATE_END		:		nIndex = 7;		break;
		case EP_STATE_FAULT		:		nIndex = 5;		break;
		case EP_STATE_READY		:		nIndex = 1;		break;
		case EP_STATE_LINE_OFF	:		nIndex = 10;	break;	
		case EP_STATE_LINE_ON	:		nIndex = 1;		break;
		case EP_STATE_EMERGENCY	:		nIndex = 9;		break;
		case EP_STATE_NONCELL   :		nIndex = 5;		break;
		case EP_STATE_COMMON	:		
		case EP_STATE_ERROR		:		
		default:						nIndex = 9;		break;


	}
	return nIndex;
}

bool CCTSMonDoc::LoadBfSensorMap()
{
	CString strPath = _T("");
	strPath.Format("%s\\%s", m_strCurFolder, BF_SENSOR_MAPPING_FILE_NAME);
	//Loading sensor config file
	FILE *fp = fopen((LPCTSTR)strPath, "rb");
	if(fp)
	{
		EP_FILE_HEADER *pHeader = new EP_FILE_HEADER;
		if(fread(pHeader, sizeof(EP_FILE_HEADER), 1, fp) > 0)
		{
			CFormModule *pModuleInfo;
			_MAPPING_DATA *pMapData;
			SENSOR_MAPPING_SAVE_TABLE *pData = new SENSOR_MAPPING_SAVE_TABLE;
			while(fread(pData,  sizeof(SENSOR_MAPPING_SAVE_TABLE), 1, fp) > 0)
			{
				pModuleInfo = GetModuleInfo(pData->nModuleID);
				if(pModuleInfo)
				{
					for(int m=0; m<EP_MAX_SENSOR_CH; m++)
					{
						pMapData = pModuleInfo->GetSensorMap(CFormModule::sensorType1, m);
						memcpy(pMapData, &pData->mapData1[m], sizeof(_MAPPING_DATA));
						pMapData = pModuleInfo->GetSensorMap(CFormModule::sensorType2, m);
						memcpy(pMapData, &pData->mapData2[m], sizeof(_MAPPING_DATA));
					}
				}
			}
			delete pData;
		}
		delete pHeader;
		fclose(fp);
	}

	return true;
}

bool CCTSMonDoc::SaveBfSensorMap()
{
	int nTempID = 0;	

	CString strPath = _T("");
	strPath.Format("%s\\%s", m_strCurFolder, BF_SENSOR_MAPPING_FILE_NAME);
	
	FILE *fp = fopen( (LPCTSTR)strPath, "wb" );
	if(fp)
	{
		_MAPPING_DATA *pMapData;

		COleDateTime curT = COleDateTime::GetCurrentTime();
		EP_FILE_HEADER *pHeader= new EP_FILE_HEADER;
		strcpy(pHeader->szFileID, "20061017");
		strcpy(pHeader->szDescrition, "PNE sensor mapping file\n");
		strcpy(pHeader->szFileVersion, "1");
		sprintf(pHeader->szCreateDateTime, "%s", curT.Format());
		fwrite(pHeader, sizeof(EP_FILE_HEADER), 1, fp);
		delete pHeader;

		SENSOR_MAPPING_SAVE_TABLE *pData = new SENSOR_MAPPING_SAVE_TABLE;

		
		for(int i=0; i< GetInstalledModuleNum(); i++)
		{
			ZeroMemory(pData, sizeof(SENSOR_MAPPING_SAVE_TABLE));
			nTempID = EPGetModuleID(i);
			pData->nModuleID = nTempID;

			CFormModule *pModuleInfo = GetModuleInfo(nTempID);
			if(pModuleInfo)
			{
				for(int m=0; m<EP_MAX_SENSOR_CH; m++)
				{
					pMapData = pModuleInfo->GetSensorMap(0, m);
					memcpy(&pData->mapData1[m], pMapData, sizeof(_MAPPING_DATA));
					pMapData = pModuleInfo->GetSensorMap(1, m);
					memcpy(&pData->mapData2[m], pMapData, sizeof(_MAPPING_DATA));
				}
				fwrite(pData,  sizeof(SENSOR_MAPPING_SAVE_TABLE), 1, fp);
			}
		}

		delete pData;
		fclose(fp);
	}

	return true;
}

BOOL CCTSMonDoc::LoadCodeTable()
{
	CDaoDatabase  db;
	try
	{
		db.Open(m_strDataBaseName);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	
	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		CString strSQL;

		if( g_nLanguage == LANGUAGE_ENG )
		{
			strSQL.Format("SELECT Code, Messge, Description FROM ChannelCode_ENG");
		}
		else if( g_nLanguage == LANGUAGE_CHI )
		{
			strSQL.Format("SELECT Code, Messge, Description FROM ChannelCode_CHI");
		}
		else
		{
			strSQL.Format("SELECT Code, Messge, Description FROM ChannelCode");
		}

		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	
	if(!rs.IsBOF() && !rs.IsEOF())
	{
		COleVariant data;
		STR_MSG_DATA *pCode;

		while(!rs.IsEOF())
		{
			pCode = new STR_MSG_DATA;
			ASSERT(pCode);
			data = rs.GetFieldValue(0);		//code
			pCode->nCode = data.lVal;
			
			data = rs.GetFieldValue(1);		//message
			if(VT_NULL != data.vt)
			{
				sprintf(pCode->szMessage, "%s", data.pbVal);
			}
			
			data = rs.GetFieldValue(2);			//description
			if(VT_NULL != data.vt)
			{
				sprintf(pCode->szDescript, "%s", data.pbVal);
			}		

			m_apChCodeMsg.Add(pCode);
			rs.MoveNext();
		}
	}
	rs.Close();
	db.Close();

	LoadBfSensorMap();

	return RequeryProcType();
}

CString CCTSMonDoc::ChCodeMsg(WORD code, BOOL bDetail)
{
	STR_MSG_DATA *pCode;
	CString strMsg;
	strMsg.Format("%d", code);
	for(INDEX i=0; i<m_apChCodeMsg.GetSize(); i++)
	{
		pCode = (STR_MSG_DATA *)m_apChCodeMsg[i];
		ASSERT(pCode);
		if(pCode->nCode == (int)code)
		{
#if _DEBUG
			if(bDetail)
			{
				strMsg.Format("%s:%s", pCode->szMessage, pCode->szDescript);
				// strMsg.Format("%s(%d):%s", pCode->szMessage, code, pCode->szDescript);
			}
			else
			{
				// strMsg.Format("%s(%d)", pCode->szMessage, code);
				strMsg.Format("%s", pCode->szMessage);
			}
#else
			if(bDetail)
			{
				strMsg.Format("%s:%s", pCode->szMessage, pCode->szDescript);				
			}
			else
			{
				strMsg.Format("%s", pCode->szMessage);
			}
#endif
			return strMsg;
		} 
	}
	return strMsg;
}

int CCTSMonDoc::GetStepCount(int nModuleID, int nCurGroup)
{
	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule == NULL)		return 0;

	CTestCondition *pCondition = pModule->GetCondition();
	if(pCondition)
	{
		return pCondition->GetTotalStepNo();
	}
	return 0;
}


//접속이후 모듈이 Run이거나 Pause이면 이전 마지막 시험 정보를 Laoding한다.
BOOL CCTSMonDoc::CheckPrevTestLog(int nModuleID, int nTrayIndex)
{
	CString strRltName;
	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule == NULL)		
		return FALSE;

	strRltName = pModule->GetResultFileName(nTrayIndex);
	//Program이 재실행된 경우 
	//현재 모듈에서 시행한 최종 정보를 Laoding한다.
	if(strRltName.IsEmpty())
	{
		//모듈 접속시에 무조건 이전 최종 시험정보를 Loading하므로 호출 되지 않을 것임 
		//if(CheckAndLoadPrevTestInfo(nModuleID) == FALSE)
		if(pModule->ReadTrayTestLogFile(nTrayIndex) == FALSE)
		{
			m_strLastErrorString.Format(GetStringTable(IDS_MSG_NOT_FOUND), ::GetModuleName(nModuleID));
			return FALSE;
		}
	}
	
	//자동 공정일 경우 Tray serial No를 읽어와서 현재 Tray와 비교 하고 맞으면 최근 Data를 Load 하여야 한다.//
/*	if(EPGetAutoProcess(nModuleID))
	{
//		CTray *pTray = pModule->GetTrayInfo();
		EP_TRAY_SERIAL	traySerial;
		LPVOID	pBuffer;				//Module에 현재 진입된 Tray Serial No 요구 
		int nSize = sizeof(EP_TRAY_SERIAL);
		if((pBuffer =  EPSendDataCmd(nModuleID, nGroupIndex+1, 0, EP_CMD_TRAY_SERIAL_REQUEST, nSize)) != NULL)	
		{
			memcpy(&traySerial, pBuffer, sizeof(EP_TRAY_SERIAL));
			
			EP_NVRAM_DATA nvRamData;
			sprintf(nvRamData.szTraySerialNo, traySerial.szTraySerialNo);
			sprintf(nvRamData.szTrayNo, traySerial.szTrayNo);
			TRACE("Tray %s, %s Scaned\n", traySerial.szTrayNo, traySerial.szTraySerialNo);

			//Tray 등록 검사 ==>> 최종 사용자 입력값 Load
//			if(TrayRegCheck(nModuleID, nGroupIndex, *pTray, nvRamData) == FALSE)	return FALSE;
//			if(pTray->IsNewProcedureTray() == FALSE)
//			{
/*				if( m_pModule[nModuleIndex].pGroup[nGroupIndex].sTestResultFileHeader.nModuleID != nModuleID ||
					m_pModule[nModuleIndex].pGroup[nGroupIndex].sTestResultFileHeader.nGroupIndex != 	nGroupIndex ||
					strcmp(m_pModule[nModuleIndex].pGroup[nGroupIndex].sTestResultFileHeader.szTraySerialNo, traySerial.szTraySerialNo) != 0)
				
				{
					strTemp.Format(GetStringTable(IDS_MSG_PAUSE_TRAYNO_ERROR), ::GetModuleName(nModuleID, nGroupIndex), 
						m_pModule[nModuleIndex].pGroup[nGroupIndex].sTestResultFileHeader.szTrayNo, traySerial.szTrayNo);
						AfxMessageBox(strTemp, MB_ICONSTOP|MB_OK);
						return FALSE;
				}
*/
//		}
//			m_pModule[nModuleIndex].pGroup[nGroupIndex].dataTray.lTestKey = m_pModule[nModuleIndex].pGroup[nGroupIndex].conditionHeader.lID;
			
			//시험조건이 수정되었을 수 있으므로 DataBase에서 Loading하지 않고 
			//LogTemp파일에서 읽은 최종 전송 시험조건을 이용한다. 2005/12/29
			//////////////////////////////////////////////////////////////////////////
			//이전 공정 정보 Load
			//STR_CONDITION *pCondition = new STR_CONDITION;
			//RequeryTestCondition(m_pModule[nModuleIndex].pGroup[nGroupIndex].dataTray.lTestKey, 
			//pCondition, &m_pModule[nModuleIndex].pGroup[nGroupIndex].condition);
			//RemoveCondition(pCondition);
			//pCondition = NULL;
			//////////////////////////////////////////////////////////////////////////
			
/*		}
		else	//Response Error
		{
			return FALSE;
		}
	}
*/	
	//재시작인데 이전에 결과 파일명을 입력하지 않았을 경우 재입력 가능하도록  
/*	strRltName = pModule->GetResultFileName(nTrayIndex);
	if(strRltName.IsEmpty())		
	{
		strTemp.Format(GetStringTable(IDS_MSG_NAME_EMPTY), ::GetModuleName(nModuleID));

		if(AfxMessageBox(strTemp, MB_ICONQUESTION|MB_YESNO) != IDYES)	//select save    
		{
			return FALSE;
		}
		else	//set File Name
		{
			strTemp.Format("%s\\%s_%s_%04d_%s.fmt", m_strDataFolder, pModule->GetLotNo(), pModule->GetTrayNo(),
							pModule->GetCellNo(nTrayIndex),
							pModule->GetTestName(nTrayIndex)
							);	
			CFileDialog pDlg(FALSE, "fmt", strTemp, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "Formation Data(*.fmt)|*.fmt|");
			pDlg.m_ofn.lpstrTitle = "Formation File 저장";
			if(pDlg.DoModal() == IDOK)
			{
				strTemp  = pDlg.GetPathName();
				_unlink(strTemp);					//Overwirte 있으면 삭제
				pModule->SetResultFileName(strTemp, nTrayIndex);
			}
			else
			{
				return FALSE;
			}
		}
		return TRUE;
	}
*/
	//Pause이후 Restart하는데 이전에 사용한 결과 파일이 없을 경우 
	strRltName = pModule->GetResultFileName(nTrayIndex);
	CFileFind FileFinder;
	if(FileFinder.FindFile(strRltName) == FALSE)		//파일을 발견할 수 없음(1 Step 에서 잠시 멈춤 되었거나 파일이 이동, 삭제 되었을 경우) 
	{
		m_strLastErrorString.Format(GetStringTable(IDS_MSG_PAUSE_FILE_NAME_ERROR), ::GetModuleName(nModuleID), strRltName);
		return FALSE;
	}
	return TRUE;
}

//return 0 ==>> Send Restart Command
//return 1 ==>> 새로운 공정의 시작 (새로운 Tray)
//return -1 ==>> Cancel
int CCTSMonDoc::ShowRunInformation(int nModuleID, int nGroupIndex, BOOL bNewFileName, long lProcPK)
{
	CString strTemp;
	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule == FALSE)	
		return 0;
	
	CTray *pTray;
	BOOL bAuto = EPGetAutoProcess(nModuleID);

	///폴더와 기본 파일명 생성 ///
	CString strDataPath;
	if(bNewFileName)
	{
		for(int j=0; j<pModule->GetTotalJig(); j++)
		{
			pTray = pModule->GetTrayInfo();
			if(pTray && bAuto == FALSE)	//수동 공정이면 모두 처음 공정처럼 처리 	
			{
				pTray->SetFirstProcess(TRUE);
			}
			
			
			COleDateTime nowtime(COleDateTime::GetCurrentTime());

			strDataPath = CreateResultFilePath(nModuleID, m_strDataFolder, pModule->GetLotNo(j), pModule->GetTrayNo(j));			
			
			// SKI kky 결과파일 형식 변경( ProcessNo(00)_TrayId(000000)_일시(130826134333).fmt
			strTemp.Format("%s\\%02d_%s_%s.fmt", 
				strDataPath,
				pModule->GetCondition()->GetModelID(),
				pModule->GetTrayNo(j),
				nowtime.Format("%y%m%d%H%M%S")				
				);
			/*
			strTemp.Format("%s\\%d_%s_%s_%s_T%02d.fmt", 
							strDataPath,
							pModule->GetTestSequenceNo(),
							pModule->GetTestName(),
							pModule->GetLotNo(j),
							pModule->GetTrayNo(j),
							j+1
							);	
			*/
			pModule->SetResultFileName(strTemp, j);
		}
	}

	//////////////////////////////////////////////////////////////////////////	
	//자동 진행에서 자동 시작 설정시는 새로운 Tray가 아니고 Group이 Standby 상태 이면 바로 진행 
	//모두 자동 진행 설정시에는 공정 조건이 자동으로 보내지고 Module이 Stanby 상태가 되기전에 Start Command가 전송되는 경우가 있어서 상태를 검사 한다.
	BOOL bAutoRun = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "AutoRun", 0);
	
	STR_CONDITION_HEADER testInfo = GetPrevTest(lProcPK);
	BOOL bNewTray = testInfo.lID <=0 ? TRUE: FALSE;	//pTray->IsNewProcedureTray();

	// if((bAutoRun && bAuto && !bNewTray && EPGetGroupState(nModuleID, nGroupIndex) == EP_STATE_STANDBY)) 
	if( bAutoRun && 
		( EPGetGroupState(nModuleID, nGroupIndex) == EP_STATE_STANDBY || 
		EPGetGroupState(nModuleID, nGroupIndex) == EP_STATE_END || 
		EPGetGroupState(nModuleID, nGroupIndex) == EP_STATE_READY )
	)
	{
		BOOL bAllOk = TRUE;
		for(int j=0; j<pModule->GetTotalJig(); j++)
		{
			CString strTemp;
			if(CheckFileNameValidate(pModule->GetResultFileName(j), strTemp, nModuleID) == FALSE)
			{
				bAllOk = FALSE;
			}
		}
		if(bAllOk)	return 1;	//파일명이 모두 정상이면 자동 실행하고 

		//파일명에 문제가 있으면 사용자 화면을 표시하여 파일명을 검사하도록 한다.
	}

	//사용자 입력을 받아 들인다.
	//자동공정 진행중인 경우는 파일명 외에는 새롭게 받아 들이는 정보가 없음 
	CRunDlg *pDlg;												//새파일 이름과 Lot 번호를 부여 받는다	
	pDlg = new CRunDlg;
	pDlg->m_pDoc = this;
	pDlg->m_bNewTray = bNewTray;
	pDlg->m_lpModuleInfo = pModule;
	if(pDlg->DoModal() != IDOK)
	{
		delete pDlg;
		pDlg = NULL;
		return -1;
	}
	delete pDlg;
	pDlg = NULL;

	return 1;
}

//실시간 Data 저장 한다.
BOOL CCTSMonDoc::SaveRealTimeData(int nModuleID)
{
#ifdef _DEBUG
	DWORD dwStartTime = timeGetTime();
#endif

	CFormModule *pModule = GetModuleInfo(nModuleID);
	EP_GROUP_INFO* pMDData = EPGetGroupInfo(nModuleID);
	if(pModule && pMDData)
	{
		CTray *pTray;
		for(int j=0; j<pModule->GetTotalJig(); j++)
		{
			pTray = pModule->GetTrayInfo(j);
			if(pTray && EPTrayState(nModuleID, j) == EP_TRAY_LOAD)
			{
				CString strData = pModule->GetResultFileName(j);
				CString strFile = strData.Left(strData.ReverseFind('.')) + "_DCR.csv";

				//파일명_DCR.csv 형태로 저장
				//한공정에 DCR Step이 여러개 있게되면 최종 DCR Data만 저장된다.
				int nStartCh = pModule->GetStartChIndex(j);
				int nChannelCount =  pModule->GetChInJig(j);
				int nTotCh = pMDData->chRealData.GetSize();
				TRACE(TEXT_LANG[3], pTray->GetTrayNo(), j+1);//"Tray %s DCR 측정 Data 수신(%d)\n"

				float fDeltaV, fDeltaI, fData;
				if(nStartCh >= 0 && nStartCh+nChannelCount <= nTotCh)
				{
					FILE *fp = fopen(strFile, "wt");
					if(fp != NULL)
					{
						fprintf(fp, "CH,time(sec),Voltage(%s),Current(%s),DCR(mOhm),", m_strVUnit, m_strIUnit);
						EP_REAL_TIME_DATA *pChData;
						for(int nChannel = 0; nChannel < nChannelCount ; nChannel++)
						{
							pChData = (EP_REAL_TIME_DATA *)pMDData->chRealData[nChannel+nStartCh];
							for(int p=0; p<10; p++)		//10 Point save
							{
								fDeltaV = VTG_PRECISION(pChData->dataPoint[0].lVoltage-pChData->dataPoint[p].lVoltage);
								fDeltaI = CRT_PRECISION(pChData->dataPoint[0].lCurrent-pChData->dataPoint[p].lCurrent);
								if(fDeltaI == 0.0f)
								{
									fData = 0;
								}
								else
								{
									fData =  fDeltaV/fDeltaI*1000.0f;	//mOhm
								}
								fprintf(fp, "%d,%.1f,%s,%s,%.2f\n", 
											pChData->nChIndex+1,
											MD2PCTIME(pChData->dataPoint[p].ulTime), 
											ValueString(pChData->dataPoint[p].lVoltage, EP_VOLTAGE), 
											ValueString(pChData->dataPoint[p].lCurrent, EP_CURRENT),
											fData
										);
							}
						}
						fclose(fp);
					}		
				}
			}
		}
	}
	
#ifdef _DEBUG
	dwStartTime = timeGetTime() - dwStartTime;
//	TRACE("Data Save Escaped Time %d ms\n", dwStartTime);
#endif
	return TRUE;
}


CString CCTSMonDoc::StepEndString(STR_COMMON_STEP *pStep) 
{
	CString strTemp, strTemp1;
	switch(pStep->stepHeader.type)
	{
	case EP_TYPE_CHARGE:			//Charge
		{
			if(pStep->fEndV > 0L)	
			{
				strTemp.Format("V >%s", ValueString(pStep->fEndV, EP_VOLTAGE));
			}
	
			if(pStep->fEndI > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strTemp1.Format("I < %s", ValueString(pStep->fEndI, EP_CURRENT));
				strTemp += strTemp1;
			}
			
			if(pStep->fEndTime >0)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
//				ConvertTime(szTemp, pStep->ulEndTime);
				strTemp1.Format("t > %s", ValueString(pStep->fEndTime, EP_STEP_TIME));
				strTemp += strTemp1;
			}
			if(pStep->fEndC > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strTemp1.Format("C > %s", ValueString(pStep->fEndC, EP_CAPACITY));
				strTemp += strTemp1;
			}
			if(pStep->fEndDV > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strTemp1.Format("dV > %s", ValueString(pStep->fEndDV, EP_VOLTAGE));
				strTemp += strTemp1;
			}
			if(pStep->fEndDI > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strTemp1.Format("dI > %s", ValueString(pStep->fEndDI, EP_CURRENT));
				strTemp += strTemp1;
			}
			break;
		}

	case EP_TYPE_DISCHARGE:		//Discharge
		{
			if(pStep->fEndV > 0L)	
			{
				strTemp.Format("V < %s", ValueString(pStep->fEndV, EP_VOLTAGE));
			}
			if(pStep->fEndI < 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strTemp1.Format("I > %s", ValueString(pStep->fEndI, EP_CURRENT));
				strTemp += strTemp1;
			}
			if(pStep->fEndTime > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
//				ConvertTime(szTemp, pStep->ulEndTime);
				strTemp1.Format("t > %s", ValueString(pStep->fEndTime, EP_STEP_TIME));
				strTemp += strTemp1;
			}
			if(pStep->fEndC > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strTemp1.Format("C > %s", ValueString(pStep->fEndC, EP_CAPACITY));
				strTemp += strTemp1;
			}
			if(pStep->fEndDV < 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strTemp1.Format("dV < %s", ValueString(pStep->fEndDV, EP_VOLTAGE));
				strTemp += strTemp1;
			}
			if(pStep->fEndDI > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strTemp1.Format("dI > %s", ValueString(pStep->fEndDI, EP_CURRENT));
				strTemp += strTemp1;
			}

			break;
		}

	case EP_TYPE_REST:				//Rest
		{
//			ConvertTime(szTemp, pStep->ulEndTime);
			strTemp.Format("t > %s", ValueString(pStep->fEndTime, EP_STEP_TIME)); 
			break;
		}
	case EP_TYPE_OCV:				//Ocv
	case EP_TYPE_IMPEDANCE:		//Impedance
	case EP_TYPE_END:				//End
	case -1:					//Not Seleted
		strTemp.Empty();
		break;

	case EP_TYPE_LOOP:
		{
			strTemp.Format("Goto(%d) Cycle(%d)", (int)pStep->fVref+1, (int)pStep->fIref); 
			break;
		}
		
	default:
		strTemp = "Unknown step type";
		break;
	
	}
	return strTemp;
}

CString CCTSMonDoc::StepEndString(CStep *pStep) 
{
	CString strTemp, strTemp1;
	switch(pStep->m_type)
	{
	case EP_TYPE_CHARGE:			//Charge
		{
			if(pStep->m_fEndV > 0L)	
			{
				strTemp.Format("V >%s", ValueString(pStep->m_fEndV, EP_VOLTAGE));
			}
	
			if(pStep->m_fEndI > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strTemp1.Format("I < %s", ValueString(pStep->m_fEndI, EP_CURRENT));
				strTemp += strTemp1;
			}
			
			if(pStep->m_fEndTime >0)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
//				ConvertTime(szTemp, pStep->ulEndTime);
				strTemp1.Format("t > %s", ValueString(pStep->m_fEndTime, EP_STEP_TIME));
				strTemp += strTemp1;
			}
			if(pStep->m_fEndC > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strTemp1.Format("C > %s", ValueString(pStep->m_fEndC, EP_CAPACITY));
				strTemp += strTemp1;
			}
			if(pStep->m_fEndDV > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strTemp1.Format("dV > %s", ValueString(pStep->m_fEndDV, EP_VOLTAGE));
				strTemp += strTemp1;
			}
			if(pStep->m_fEndDI > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strTemp1.Format("dI > %s", ValueString(pStep->m_fEndDI, EP_CURRENT));
				strTemp += strTemp1;
			}
			break;
		}

	case EP_TYPE_DISCHARGE:		//Discharge
		{
			if(pStep->m_fEndV > 0L)	
			{
				strTemp.Format("V < %s", ValueString(pStep->m_fEndV, EP_VOLTAGE));
			}
			if(pStep->m_fEndI < 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strTemp1.Format("I > %s", ValueString(pStep->m_fEndI, EP_CURRENT));
				strTemp += strTemp1;
			}
			if(pStep->m_fEndTime > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
//				ConvertTime(szTemp, pStep->ulEndTime);
				strTemp1.Format("t > %s", ValueString(pStep->m_fEndTime, EP_STEP_TIME));
				strTemp += strTemp1;
			}
			if(pStep->m_fEndC > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strTemp1.Format("C > %s", ValueString(pStep->m_fEndC, EP_CAPACITY));
				strTemp += strTemp1;
			}
			if(pStep->m_fEndDV < 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strTemp1.Format("dV < %s", ValueString(pStep->m_fEndDV, EP_VOLTAGE));
				strTemp += strTemp1;
			}
			if(pStep->m_fEndDI > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strTemp1.Format("dI > %s", ValueString(pStep->m_fEndDI, EP_CURRENT));
				strTemp += strTemp1;
			}

			break;
		}

	case EP_TYPE_REST:				//Rest
		{
//			ConvertTime(szTemp, pStep->ulEndTime);
			strTemp.Format("t > %s", ValueString(pStep->m_fEndTime, EP_STEP_TIME)); 
			break;
		}
	case EP_TYPE_OCV:				//Ocv
	case EP_TYPE_IMPEDANCE:		//Impedance
	case EP_TYPE_END:				//End
	case -1:					//Not Seleted
		strTemp.Empty();
		break;

	case EP_TYPE_LOOP:
		{
			strTemp.Format("Goto(%d) Cycle(%d)", int(pStep->m_fVref)+1, int(pStep->m_fIref)); 
			break;
		}
		
	default:
		strTemp = "Unknown step type.";
		break;
	
	}
	return strTemp;
}


CString CCTSMonDoc::GetLotNo(int nModuleID, int nTrayIndex)
{
	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule == NULL)		return "";

	return pModule->GetLotNo(nTrayIndex);//.pGroup[nGroupIndex].sTestResultFileHeader.szLotNo;
}

CString CCTSMonDoc::GetTrayNo(int nModuleID, int nTrayIndex)
{
	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule == NULL)		return "";

	return pModule->GetTrayNo(nTrayIndex);
}

BOOL CCTSMonDoc::WriteEPFileHeader(int nFile, FILE *fp)
{
	//Write ElicoPower FileHeader
	if(fp == NULL)	return FALSE;
	LPEP_FILE_HEADER	lpEpFileHeader;
	lpEpFileHeader = new EP_FILE_HEADER;
	ZeroMemory(lpEpFileHeader, sizeof(EP_FILE_HEADER));
	ASSERT(lpEpFileHeader);

	COleDateTime dateTime = COleDateTime::GetCurrentTime();
	sprintf(lpEpFileHeader->szCreateDateTime, "%s", dateTime.Format());

	switch(nFile)
	{
	case RESULT_FILE:
		sprintf(lpEpFileHeader->szFileID, RESULT_FILE_ID);
		sprintf(lpEpFileHeader->szFileVersion, RESULT_FILE_VER2);
		sprintf(lpEpFileHeader->szDescrition, "PNE Cell Test System Result File");
		break;
	case TEST_LOG_FILE:
		sprintf(lpEpFileHeader->szFileID, TEST_LOG_FILE_ID);
		sprintf(lpEpFileHeader->szFileVersion, TEST_LOG_FILE_VER);
		sprintf(lpEpFileHeader->szDescrition, "PNE Cell Test System Test Log File");
		break;
	case CONDITION_FILE:
		sprintf(lpEpFileHeader->szFileID, CONDITION_FILE_ID);
		sprintf(lpEpFileHeader->szFileVersion, CONDITION_FILE_VER);
		sprintf(lpEpFileHeader->szDescrition, "PNE Cell Test System Condition File");
		break;
	case GRADE_CODE_FILE:
		sprintf(lpEpFileHeader->szFileID, GRADE_CODE_FILE_ID);
		sprintf(lpEpFileHeader->szFileVersion, GRADE_CODE_FILE_VER);
		sprintf(lpEpFileHeader->szDescrition, "PNE Cell Test System Grade File");
		break;
	case IROCV_FILE:
		sprintf(lpEpFileHeader->szFileID, IROCV_FILE_ID);
		sprintf(lpEpFileHeader->szFileVersion, IROCV_FILE_VER1);
		sprintf(lpEpFileHeader->szDescrition, "PNE Cell Test System IROCV File");
		break;
	default:
		delete lpEpFileHeader;
		lpEpFileHeader = NULL;
		return FALSE;
	}

	fwrite(lpEpFileHeader, sizeof(EP_FILE_HEADER), 1, fp);
	delete lpEpFileHeader;
	lpEpFileHeader = NULL;
	return TRUE;
}

int CCTSMonDoc::ReadEPFileHeader(int nFile, FILE *fp)
{
	int nRtn = 0;
	LPEP_FILE_HEADER	lpEpFileHeader, lpTempHeader;
	lpEpFileHeader = new EP_FILE_HEADER;
	lpTempHeader = new EP_FILE_HEADER;

	ASSERT(lpEpFileHeader);
	ASSERT(lpTempHeader);
	ASSERT(fp);
	
	fread(lpEpFileHeader, sizeof(EP_FILE_HEADER), 1, fp);

	switch(nFile)
	{
	case RESULT_FILE:
		sprintf(lpTempHeader->szFileID, RESULT_FILE_ID);
		sprintf(lpTempHeader->szFileVersion, RESULT_FILE_VER1);
		break;
	case TEST_LOG_FILE:
		sprintf(lpTempHeader->szFileID, TEST_LOG_FILE_ID);
		sprintf(lpTempHeader->szFileVersion, TEST_LOG_FILE_VER);
		break;
	case CONDITION_FILE:
		sprintf(lpTempHeader->szFileID, CONDITION_FILE_ID);
		sprintf(lpTempHeader->szFileVersion, CONDITION_FILE_VER);
		break;
	case GRADE_CODE_FILE:
		sprintf(lpTempHeader->szFileID, GRADE_CODE_FILE_ID);
		sprintf(lpTempHeader->szFileVersion, GRADE_CODE_FILE_VER);
		break;
	case IROCV_FILE:
		sprintf(lpEpFileHeader->szFileID, IROCV_FILE_ID);
		sprintf(lpEpFileHeader->szFileVersion, IROCV_FILE_VER1);
		break;

	default:
		nRtn = -1;
	}

	if(strcmp(lpEpFileHeader->szFileID, lpTempHeader->szFileID) != 0)
	{
		nRtn = -2;
	}

/*	if(strcmp(lpEpFileHeader->szFileVersion, lpTempHeader->szFileVersion) != 0)
	{
		nRtn = -3;
	}
*/

	delete lpTempHeader;
	lpTempHeader = NULL;
	delete lpEpFileHeader;
	lpEpFileHeader = NULL;
	return nRtn;
}

int CCTSMonDoc::SendRunCommand(int nModuleID, int nGroupIndex )
{
	// TODO: Add your command handler code here
	CFormModule *pModule;
	pModule =  GetModuleInfo(nModuleID);
	if( pModule == NULL )		return 0;
	if( pModule->GetLineMode() == EP_ONLINE_MODE )
	{
		return 0;	
	}	

	CString strTemp;
	WORD state = EPGetGroupState(nModuleID, 0);
	if(state == EP_STATE_IDLE)
	{
		//새로운 시험조건 전송
		STR_CONDITION_HEADER testHeader = SelectTest(FALSE);
		if(testHeader.lID == 0)		return -1;
		CTestCondition *pCondition = new CTestCondition;
		if(pCondition->LoadTestCondition(testHeader.lID)==FALSE)
		{
			strTemp.Format(TEXT_LANG[4], testHeader.szName);		//"선택 조건 [%s] Loading을 실패 하였습니다."
			AfxMessageBox(strTemp);
			delete pCondition;
			return -1;
		}
		
		if(SendConditionToModule(nModuleID, 0, pCondition) == FALSE)
		{
			strTemp.Format(TEXT_LANG[5], testHeader.szName);		//"선택 조건 [%s] 전송을 실패 하였습니다."
			AfxMessageBox(strTemp);
			return -1;
		}
		Sleep(100);
		delete pCondition;
	}

	//이전 정보를 Reset하고 보여줄지 여부 
	if(ShowRunInformation(nModuleID, 0, TRUE) < 1)
	{
		return -1;
	}
	
	//Module에 전송 정보 저장 (파일생성 이전에 )
	pModule->UpdateStartTime();
	pModule->UpdateTestLogTempFile();	//CheckEnableRun()에서 파일을 생성하므로 그 이전에 Update해야 한다.	
	
	//작업 가능상태인지 검사 
	//결과 파일도 생성한다.
	DWORD dwJigFlag = CheckEnableRun(nModuleID);
	if(dwJigFlag == 0)
	{
		AfxMessageBox(GetLastErrorString());
		return -1;
	}
	
	//최종 상태 검사 
	BYTE byTemp;
	state = EPGetGroupState(nModuleID, 0);
	if(state != EP_STATE_STANDBY && state != EP_STATE_READY && state != EP_STATE_END && state != EP_STATE_IDLE)
	{
		strTemp.Format(TEXT_LANG[6], ::GetModuleName(nModuleID), GetStateMsg(state, byTemp));//"%s :: [작업시작] 명령 전송가능 상태가 아닙니다.(state : %s)"
		WriteLog(strTemp);
		return 0;
	}

	//Module에 Run Command 전송 
	int nRtn = EP_NACK;
	EP_RUN_CMD_INFO	runInfo;
	ZeroMemory(&runInfo, sizeof(EP_RUN_CMD_INFO));
	
	runInfo.nTabDeepth = pModule->GetTrayInfo(0)->m_nTabDeepth;	
	runInfo.nTrayHeight = pModule->GetTrayInfo(0)->m_nTrayHeight;
	runInfo.nTrayType = pModule->GetTrayInfo(0)->m_nTrayType;	
	runInfo.nCurrentCellCnt = pModule->GetTrayInfo(0)->GetInputCellCnt();
	runInfo.nContactErrlimit = pModule->GetTrayInfo(0)->m_nContactErrlimit;
	runInfo.nCappErrlimit = pModule->GetTrayInfo(0)->m_nCapaErrlimit;
	runInfo.nChErrlimit = pModule->GetTrayInfo(0)->m_nChErrlimit;
	sprintf(runInfo.szTrayID, "%s", pModule->GetTrayInfo(0)->GetTrayNo() );
	
	//TestSerialNo 전송(실시간 저장 Data를 구별하는데 사용)
	//20061025 => 새로운 번호 생성(TestSerialNo는 Tray간의 고유번호로 사용되므로 다른 번호 생성)
	sprintf(runInfo.szTestSerialNo, pModule->GetTestSerialNo());
	//사용할 지그번호 전송(사용안하는 지그는 Down하지 않음) cf. Local에서 Up/Down하면 모든 Jig Up/Down 실시 
	runInfo.dwUseFlag = dwJigFlag;
	runInfo.nTempGetPosition = atol(AfxGetApp()->GetProfileString(FORMSET_REG_SECTION, "Temp Measuring Position", "0"));

	CTray *pTray;
	for(int nTrayIndex=0; nTrayIndex < INSTALL_TRAY_PER_MD; nTrayIndex++)
	{
		pTray = pModule->GetTrayInfo(nTrayIndex);
		if(pTray)
		{
			runInfo.nInputCell[nTrayIndex] = pTray->lInputCellCount;
		}
	}

	//ljb Send Run Command
	if((nRtn = EPSendCommand(nModuleID, 1, 0, EP_CMD_RUN, &runInfo, sizeof(EP_RUN_CMD_INFO))) != EP_ACK)
	{
		strTemp.Format("%s (%s-%s)",  ::GetStringTable(IDS_TEXT_CMD_SEND_FAIL), ::GetModuleName(nModuleID), CmdFailMsg(nRtn));
		AfxMessageBox(strTemp, MB_ICONSTOP|MB_OK);
		return -1;
	}
	
	fnWriteTemperature(nModuleID, FALSE);

	//2006/03/22 KBH
	//완료된 Step의 결과 data를 표시할 수 있도록 최종 data file reset 한다.
	pModule->LoadResultData();
	
	return 1;
}

BOOL CCTSMonDoc::SendContinueCommand(int nModuleID, int nGroupIndex)
{
	//각 상황에 맞추어 처리 2002/4/18	
	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule == NULL)		return FALSE;

	CTray *pTray;
	for(int t=0; t<pModule->GetTotalJig(); t++)
	{
		pTray = pModule->GetTrayInfo(t);
		if(pTray)
		{
			if(pTray->IsWorkingTray() && pTray->m_strFileName.IsEmpty())
			{
				//접속 하면서 그룹 상태가 변화 했을시 이미 Loading 되었으므로 
				//이부분은 호출이 호출 되지 않음 
				//void CMainFrame::OnModuleStateChange(WPARAM wParam, LPARAM lParam)
				//에서 CheckAndLoadPrevTestInfo() 호출  
				if(CheckPrevTestLog(nModuleID, t) == FALSE)
				{
					m_strLastErrorString.Format(TEXT_LANG[7], pTray->GetTrayNo());//"%s의 이전 정보를 찾을 수 없습니다."
					AfxMessageBox(m_strLastErrorString);
					return FALSE;
				}
			}
		}
	}

	CString strTemp;
	/*
	BYTE byTemp;	
	
	WORD state = EPGetGroupState(nModuleID, nGroupIndex);
	if(state != EP_STATE_PAUSE)
	{
		strTemp.Format("%s :: [작업 연속] 명령이 전송가능 상태가 아닙니다.(state : %d)", ::GetModuleName(nModuleID, nGroupIndex), GetStateMsg(state, byTemp));
		WriteLog(strTemp);
		return FALSE;
	}
*/
	int nRtn;
	if((nRtn = EPSendCommand(nModuleID, nGroupIndex+1, 0, EP_CMD_CONTINUE)) != EP_ACK)
	{
		strTemp.Format(GetStringTable(IDS_MSG_COMMAND_SEND_FAILE), ::GetModuleName(nModuleID, nGroupIndex));
		strTemp = strTemp + " ("+ CmdFailMsg(nRtn) +")";
		return FALSE;
	}
	
	// EPSendCommand(nModuleID);
	
	pModule->GetTrayInfo(0)->m_nResultFileType = EP_RESULTFILE_TYPE_NORMAL;	
	pModule->UpdateTestLogHeaderTempFile();
	m_fmst.fnClearReset(nModuleID);
	
	return TRUE;
}

BOOL CCTSMonDoc::SendReStartCommand(int nModuleID, int nGroupIndex)
{	
	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule == NULL)		return FALSE;

	CTray *pTray;
	for(int t=0; t<pModule->GetTotalJig(); t++)
	{
		pTray = pModule->GetTrayInfo(t);
		if(pTray)
		{
			if(pTray->IsWorkingTray() && pTray->m_strFileName.IsEmpty())
			{
				if(CheckPrevTestLog(nModuleID, t) == FALSE)
				{
					m_strLastErrorString.Format(TEXT_LANG[7], pTray->GetTrayNo());//"%s의 이전 정보를 찾을 수 없습니다."
					AfxMessageBox(m_strLastErrorString);
					return FALSE;
				}
			}
		}
	}
	
	CString strTemp;
	/*
	BYTE byTemp;
	WORD state = EPGetGroupState(nModuleID, nGroupIndex);
	if(state != EP_STATE_PAUSE)
	{
		strTemp.Format("%s :: [작업 재시작] 명령이 전송가능 상태가 아닙니다.(state : %s)", ::GetModuleName(nModuleID, nGroupIndex), GetStateMsg(state, byTemp));
		WriteLog(strTemp);
		return FALSE;
	}
	*/
	int nRtn;
	if((nRtn = EPSendCommand(nModuleID, nGroupIndex+1, 0, EP_CMD_RESTART)) != EP_ACK)
	{
		strTemp.Format(GetStringTable(IDS_MSG_COMMAND_SEND_FAILE), ::GetModuleName(nModuleID, nGroupIndex));
		strTemp = strTemp + " ("+ CmdFailMsg(nRtn) +")";
		return FALSE;
	}

	// EPSendCommand(nModuleID);
	pModule->GetTrayInfo(0)->m_nResultFileType = EP_RESULTFILE_TYPE_NORMAL;	
	pModule->UpdateTestLogHeaderTempFile();
	m_fmst.fnClearReset(nModuleID);
	return TRUE;
}

BOOL CCTSMonDoc::SendInitCommand(int nModuleID, int nGroupIndex)
{
	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule == NULL)	return FALSE;

	WORD state = EPGetGroupState(nModuleID, nGroupIndex);
	BYTE byTemp;
//	if(state != EP_STATE_STANDBY && state != EP_STATE_READY && state != EP_STATE_END)
	if(state == EP_STATE_RUN)	
	{
		m_strLastErrorString.Format(TEXT_LANG[8], ::GetModuleName(nModuleID, nGroupIndex), GetStateMsg(state, byTemp));//"%s :: [초기화] 명령 전송가능 상태가 아닙니다.(state : %s)"
		WriteLog(m_strLastErrorString);
		return FALSE;
	}

	int nRtn;
	if((nRtn = EPSendCommand(nModuleID, nGroupIndex+1, 0, EP_CMD_CLEAR)) != EP_ACK)
	{
		m_strLastErrorString.Format(GetStringTable(IDS_MSG_COMMAND_SEND_FAILE), ::GetModuleName(nModuleID));
		m_strLastErrorString = m_strLastErrorString + " ("+ CmdFailMsg(nRtn) +")";
		AfxMessageBox(m_strLastErrorString);
		return FALSE;
	}
	
	pModule->GetTrayInfo(0)->m_nResultFileType = EP_RESULTFILE_TYPE_NORMAL;	
	pModule->UpdateTestLogHeaderTempFile();
	// EPSendCommand(nModuleID);
	//예약된 공정을 Reset 시킨다.
	pModule->ResetGroupData();
	
	pModule->ClearTestLogTempFile();
	
	m_fmst.fnClearReset(nModuleID);

	return TRUE;
}

//Run Command 시 공정한 Log를 등록 기록
BOOL CCTSMonDoc::WriteProcedureLog(int nModuleID, int nGroupIndex)
{
	int nModuleIndex = EPGetModuleIndex(nModuleID);
	if (nModuleIndex <0 || nModuleIndex >= m_nInstalledModuleNum)	return FALSE;

	ASSERT(nGroupIndex >=0 && nGroupIndex < EPGetGroupCount(nModuleID));

/*	CTestModuleRecordSet	recordSet;
//	recordSet.m_strFilter.Format("ModuleID = %ld", nNewModuleID);
//	recordSet.m_strSort.Format("ModuleID");
	
	try
	{
		recordSet.Open();
		recordSet.AddNew();
		recordSet.m_ModuleID = nModuleID;
		recordSet.m_GroupIndex = nGroupIndex;
		recordSet.m_UserID = g_LoginData.szLoginID;
		recordSet.m_TrayNo = m_pModule[nModuleIndex].pGroup[nGroupIndex].sTestResultFileHeader.szTrayNo;
	//	recordSet.m_State = m_pModule[nModuleIndex].pGroup[nGroupIndex].sTestResultFileHeader.szTrayNo;
	//	recordSet.m_TraySerial = m_pModule[nModuleIndex].pGroup[nGroupIndex].sTestResultFileHeader.szTrayNo;
		recordSet.m_TestSerialNo = m_pModule[nModuleIndex].pGroup[nGroupIndex].sTestResultFileHeader.szTestSerialNo;
		recordSet.m_LotNo = m_pModule[nModuleIndex].pGroup[nGroupIndex].sTestResultFileHeader.szLotNo;
//		recordSet.m_DateTime = m_pModule[nModuleIndex].pGroup[nGroupIndex].sTestResultFileHeader.szDateTime;
		recordSet.Update();
		recordSet.Close();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}
*/
/*	int nRtn;
	CMainFrame *pFrame = (CMainFrame *)AfxGetMainWnd();
	if(pFrame->m_bDBSvrConnect)
	{
		EP_PROCEDURE_DATA	procData;
		ZeroMemory(&procData, sizeof(procData));
		procData.nModuleID = m_pModule[nModuleIndex].pGroup[nGroupIndex].sTestResultFileHeader.nModuleID;
		procData.nGroupIndex = m_pModule[nModuleIndex].pGroup[nGroupIndex].sTestResultFileHeader.nGroupIndex;
		strcpy(procData.szDateTime, m_pModule[nModuleIndex].pGroup[nGroupIndex].sTestResultFileHeader.szDateTime);			//Init. -> SendConditionStep()에서
		strcpy(procData.szModuleIP, m_pModule[nModuleIndex].pGroup[nGroupIndex].sTestResultFileHeader.szModuleIP);			//SaveResultFileHeader()에서
		strcpy(procData.szTrayNo, m_pModule[nModuleIndex].pGroup[nGroupIndex].sTestResultFileHeader.szTrayNo);			//Init. -> SendConditionStep()에서
		strcpy(procData.szLotNo,  m_pModule[nModuleIndex].pGroup[nGroupIndex].sTestResultFileHeader.szLotNo);				//Init. -> SendConditionStep()에서
		strcpy(procData.szOperatorID,  m_pModule[nModuleIndex].pGroup[nGroupIndex].sTestResultFileHeader.szOperatorID);
		strcpy(procData.szTestSerialNo,  m_pModule[nModuleIndex].pGroup[nGroupIndex].sTestResultFileHeader.szTestSerialNo);
		procData.lTraySerialNo =  atoi(m_pModule[nModuleIndex].pGroup[nGroupIndex].sTestResultFileHeader.szTraySerialNo);
		procData.lTestID =  m_pModule[nModuleIndex].pGroup[nGroupIndex].conditionHeader.lID;
		strcpy(procData.szTestName, m_pModule[nModuleIndex].pGroup[nGroupIndex].conditionHeader.szName);
		sprintf(procData.szResultFileName, m_pModule[nModuleIndex].pGroup[nGroupIndex].testHeader.szResultFileName);
//		procData.nCellNo = m_pModule[nModuleIndex].pGroup[nGroupIndex].nCellNo;		
		procData.nCellNo = m_pModule[nModuleIndex].pGroup[nGroupIndex].dataTray.lCellNo;
		procData.lModelID = m_pModule[nModuleIndex].pGroup[nGroupIndex].condition.GetModelID();
		strcpy(procData.szModelName, m_pModule[nModuleIndex].pGroup[nGroupIndex].condition.m_modelHeader.szName);

		procData.procedureType = m_pModule[nModuleIndex].pGroup[nGroupIndex].condition.m_lProcType;
		
		nRtn = dcSendData(EP_CMD_PROCEDURE_DATA, &procData, sizeof(EP_PROCEDURE_DATA));
		
		if(nRtn != EP_ACK)
		{
			CString strTemp;
			strTemp.Format("Procedure Information Send to Data Server Fail. Return %d\n", CmdFailMsg(nRtn));
			WriteLog((LPSTR)(LPCTSTR)strTemp);
		}
	}
*/
//		CTray *pTrayData = &m_pModule[nModuleIndex].pGroup[nGroupIndex].dataTray;
		//pTrayData->strJigID;
		//pTrayData->lTeskID;
//		pTrayData->strTrayNo = m_pModule[nModuleIndex].pGroup[nGroupIndex].sTestResultFileHeader.szTrayNo;
//		pTrayData->strOperatorID = m_pModule[nModuleIndex].pGroup[nGroupIndex].sTestResultFileHeader.szOperatorID;
//		pTrayData->lModelKey = m_pModule[nModuleIndex].pGroup[nGroupIndex].conditionHeader.;
//		pTrayData->lTestKey = m_pModule[nModuleIndex].pGroup[nGroupIndex].conditionHeader.lID;
//		pTrayData->strLotNo = m_pModule[nModuleIndex].pGroup[nGroupIndex].sTestResultFileHeader.szLotNo;
//		pTrayData->strTestSerialNo = m_pModule[nModuleIndex].pGroup[nGroupIndex].sTestResultFileHeader.szTestSerialNo;
//		pTrayData->testDateTime.ParseDateTime(m_pModule[nModuleIndex].pGroup[nGroupIndex].sTestResultFileHeader.szDateTime);
//		pTrayData->lCellNo = m_pModule[nModuleIndex].pGroup[nGroupIndex].nCellNo;
//		pTrayData->lInputCellCount
//		pTrayData->ModuleID = nModuleID;
//		pTrayData->GroupIndex = nGroupIndex;
		
	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(EPGetAutoProcess(nModuleID) && pModule)	//현재 Module이 자동 공정으로 설정 되어 있으면 
	{
		CTray *pTrayData;
		for(int t=0; t<pModule->GetTotalJig(); t++)
		{
			pTrayData = pModule->GetTrayInfo(t);
			if(pTrayData->IsWorkingTray())
			{
				pTrayData->testDateTime = pModule->GetRunStartTime();
				pTrayData->UpdateStartData();			//Tray의 사용자 입력 값을 저장 한다.
			}
		}
		//Pause 시 사용 
	}
	return TRUE;	
}

STR_CONDITION_HEADER CCTSMonDoc::GetTestData(long lTestID)
{
	STR_CONDITION_HEADER	testHeader;
	ZeroMemory(&testHeader, sizeof(STR_CONDITION_HEADER));

/*	CTestListRecordSet	recordSet;
	recordSet.m_strFilter.Format("TestID = %ld", lTestID);
	try
	{
		recordSet.Open();
		if(!recordSet.IsBOF())
		{
			testHeader.lID = recordSet.m_TestID;
			testHeader.lNo = recordSet.m_TestNo;
			strcpy(testHeader.szCreator, recordSet.m_Creator);
			strcpy(testHeader.szDescription, recordSet.m_Description);
			strcpy(testHeader.szName, recordSet.m_TestName);
			strcpy(testHeader.szModifiedTime, recordSet.m_ModifiedTime.Format());
		}
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return testHeader;
	}
	recordSet.Close();
*/
	CDaoDatabase  db;
	try
	{
		db.Open(m_strDataBaseName);
		CDaoRecordset rs(&db);
		CString strSQL;
		strSQL.Format("SELECT TestID, TestNo, Creator, Description, TestName, ModifiedTime FROM TestName WHERE TestID = %d", 
					   lTestID);
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
		if(!rs.IsBOF())
		{
			COleVariant data;
			data = rs.GetFieldValue(0);		//TestID
			testHeader.lID = data.lVal;
			data = rs.GetFieldValue(1);		//TestNo
			testHeader.lNo = data.lVal;
			data = rs.GetFieldValue(2);		//Creator
			if(VT_NULL != data.vt)
			{
				strcpy(testHeader.szCreator, data.pcVal);
			}
			data = rs.GetFieldValue(3);		//Description
			if(VT_NULL != data.vt)
			{
				strcpy(testHeader.szDescription, data.pcVal);
			}
			data = rs.GetFieldValue(4);		//TestName
			if(VT_NULL != data.vt)
			{
				strcpy(testHeader.szName, data.pcVal);
			}
			data = rs.GetFieldValue(5);		//ModifiedTime
			COleDateTime  time = data;
			if(time.GetStatus() == COleDateTime::valid)
			{
				sprintf(testHeader.szModifiedTime, "%s", time.Format());
			}
		}
		rs.Close();
		db.Close();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
	}	
	return testHeader;
}

STR_CONDITION_HEADER CCTSMonDoc::GetNextTest(long lTestID)
{
	STR_CONDITION_HEADER	testHeader;
	ZeroMemory(&testHeader, sizeof(STR_CONDITION_HEADER));

/*	CTestListRecordSet	recordSet;
	recordSet.m_strFilter.Format("TestID = %ld", lTestID);
	try
	{
		recordSet.Open();
		if(recordSet.IsBOF())
		{
			recordSet.Close();
			m_strLastErrorString.Format("공정 조건 ID %d를 찾을 수 없습니다.", lTestID);
			return testHeader;
		}

		CString strFilter;
		strFilter.Format("[ModelID] = %ld AND [TestNo] = %ld", recordSet.m_ModelID, recordSet.m_TestNo+1);
		recordSet.m_strFilter = "";
		recordSet.Requery();

		if(recordSet.Find(AFX_DAO_FIRST, strFilter) == 0)			//다음 공정이 없음 
		{
			m_strLastErrorString.Format("공정 조건 ID %d 이후 공정을 찾을 수 없습니다.", lTestID);
			recordSet.Close();
			return testHeader;
		}
		
		testHeader.lID = recordSet.m_TestID;
		testHeader.lNo = recordSet.m_TestNo;
		strcpy(testHeader.szCreator, recordSet.m_Creator);
		strcpy(testHeader.szDescription, recordSet.m_Description);
		strcpy(testHeader.szName, recordSet.m_TestName);
		strcpy(testHeader.szModifiedTime, recordSet.m_ModifiedTime.Format());
	
		recordSet.Close();
	}
	catch (CDaoException* e)
	{
		m_strLastErrorString.Format("%s", e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return testHeader;
	}
*/	
	CDaoDatabase  db;
	try
	{
		db.Open(m_strDataBaseName);
		CDaoRecordset rs(&db);
		CString strSQL;
		strSQL.Format("SELECT a.TestID, a.TestNo, a.Creator, a.Description, a.TestName, a.ModifiedTime FROM TestName a, TestName b WHERE b.TestID = %d AND b.ModelID = a.ModelID AND a.TestNo = b.TestNo+1", 
					   lTestID);
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
		if(!rs.IsBOF())
		{
			COleVariant data;
			data = rs.GetFieldValue(0);		//TestID
			testHeader.lID = data.lVal;
			data = rs.GetFieldValue(1);		//TestNo
			testHeader.lNo = data.lVal;
			data = rs.GetFieldValue(2);		//Creator
			if(VT_NULL != data.vt)
			{
				strcpy(testHeader.szCreator, data.pcVal);
			}
			data = rs.GetFieldValue(3);		//Description
			if(VT_NULL != data.vt)
			{
				strcpy(testHeader.szDescription, data.pcVal);
			}
			data = rs.GetFieldValue(4);		//TestName
			if(VT_NULL != data.vt)
			{
				strcpy(testHeader.szName, data.pcVal);
			}
			data = rs.GetFieldValue(5);		//ModifiedTime
			COleDateTime  time = data;
			if(time.GetStatus() == COleDateTime::valid)
			{
				sprintf(testHeader.szModifiedTime, "%s", time.Format());
			}
		}
		rs.Close();
		db.Close();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
	}	
	return testHeader;
}

STR_CONDITION_HEADER CCTSMonDoc::GetPrevTest(long lTestID)
{
	STR_CONDITION_HEADER	testHeader;
	ZeroMemory(&testHeader, sizeof(STR_CONDITION_HEADER));

/*	CTestListRecordSet	recordSet;
	recordSet.m_strFilter.Format("TestID = %ld", lTestID);
	try
	{
		recordSet.Open();
		if(recordSet.IsBOF())
		{
			recordSet.Close();
			m_strLastErrorString.Format("Can't find process condition of ID %d.", lTestID);
			return testHeader;
		}

		if(recordSet.m_TestNo < 1)		//이전 공정이 없음 
		{
			m_strLastErrorString.Format("Process condition of ID %d is first data.", lTestID);
			recordSet.Close();
			return testHeader;
		}

		CString strFilter;
		strFilter.Format("[ModelID] = %ld AND [TestNo] = %ld", recordSet.m_ModelID, recordSet.m_TestNo-1);

		recordSet.m_strFilter = "";
		recordSet.Requery();

		if(recordSet.Find(AFX_DAO_FIRST, strFilter) == 0)
		{
			m_strLastErrorString.Format("Can't find previous process condition of IDd %d.", lTestID);
			recordSet.Close();
			return testHeader;
		}
	
		testHeader.lID = recordSet.m_TestID;
		testHeader.lNo = recordSet.m_TestNo;
		strcpy(testHeader.szCreator, recordSet.m_Creator);
		strcpy(testHeader.szDescription, recordSet.m_Description);
		strcpy(testHeader.szName, recordSet.m_TestName);
		strcpy(testHeader.szModifiedTime, recordSet.m_ModifiedTime.Format());	
		
		recordSet.Close();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return testHeader;
	}
*/
	CDaoDatabase  db;
	try
	{
		db.Open(m_strDataBaseName);
		CDaoRecordset rs(&db);
		CString strSQL;
		strSQL.Format("SELECT a.TestID, a.TestNo, a.Creator, a.Description, a.TestName, a.ModifiedTime FROM TestName a, TestName b WHERE b.TestID = %d AND b.ModelID = a.ModelID AND a.TestNo = b.TestNo-1", 
					   lTestID);
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
		if(!rs.IsBOF())
		{
			COleVariant data;
			data = rs.GetFieldValue(0);		//TestID
			testHeader.lID = data.lVal;
			data = rs.GetFieldValue(1);		//TestNo
			testHeader.lNo = data.lVal;
			data = rs.GetFieldValue(2);		//Creator
			if(VT_NULL != data.vt)
			{
				strcpy(testHeader.szCreator, data.pcVal);
			}
			data = rs.GetFieldValue(3);		//Description
			if(VT_NULL != data.vt)
			{
				strcpy(testHeader.szDescription, data.pcVal);
			}
			data = rs.GetFieldValue(4);		//TestName
			if(VT_NULL != data.vt)
			{
				strcpy(testHeader.szName, data.pcVal);
			}
			data = rs.GetFieldValue(5);		//ModifiedTime
			COleDateTime  time = data;
			if(time.GetStatus() == COleDateTime::valid)
			{
				sprintf(testHeader.szModifiedTime, "%s", time.Format());
			}
		}
		rs.Close();
		db.Close();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
	}	
	return testHeader;
}

//시험 조건을 Module에 전송 한다.
BOOL CCTSMonDoc::SendConditionToModule(int nModuleID, int nGroupIndex, CTestCondition *lpProc)
{
	CString strTemp;
	int nRtn= TRUE, nDataSize;
	ASSERT(lpProc != NULL);

	CFormModule *pModule = GetModuleInfo(nModuleID);
	
	if( pModule == NULL )
	{
		return FALSE;
	}

	int nModuleIndex = EPGetModuleIndex(nModuleID);
	if (nModuleIndex <0 || nModuleIndex >= m_nInstalledModuleNum)	return FALSE;
	ASSERT(nGroupIndex >=0 && nGroupIndex < EPGetGroupCount(nModuleID));

	//장비에 전송 가능한 조건인지 확인
	//////////////////////////////////////////////////////////////////////////
	if(CheckProcAndSystemType(lpProc->GetTestInfo()->lType) == FALSE)
	{
		strTemp.Format(TEXT_LANG[9], lpProc->GetTestName(), GetLastErrorString());//"[%s] 조건을 전송할 수 없습니다.\n%s"
		AfxMessageBox(strTemp);
		return FALSE;	
	}
	//////////////////////////////////////////////////////////////////////////

	EP_TEST_HEADER testHeader;
	ZeroMemory(&testHeader, sizeof(EP_TEST_HEADER));
	testHeader.totalStep = (BYTE)lpProc->GetTotalStepNo();
	testHeader.totalGrade = (BYTE)lpProc->GetCountIsGradeStep();
	
	if((nRtn = EPSendCommand(nModuleID, nGroupIndex, 0, EP_CMD_TEST_HEADERDATA, &testHeader, sizeof(EP_TEST_HEADER)))	!= EP_ACK)	
	{
		strTemp.Format("Module %d :: Step Head Data Send Fail. (%s)\n", nModuleID, CmdFailMsg(nRtn));
		WriteLog((LPSTR)(LPCTSTR)strTemp);
		return FALSE;
	}

	TRACE("1. Module %d :: CMD => EP_CMD_TEST_HEADERDATA Data Send\n", nModuleID);
	
	EP_PRETEST_PARAM checkParam;
	ZeroMemory( &checkParam, sizeof( EP_PRETEST_PARAM));

	checkParam = lpProc->GetNetCheckParam();

	if((nRtn = EPSendCommand(nModuleID, nGroupIndex, 0, EP_CMD_CHECK_PARAM, &checkParam, sizeof(EP_PRETEST_PARAM))) != EP_ACK)
	{
		strTemp.Format("Module %d :: Check Parameter Send Fail. (%s)\n", nModuleID, CmdFailMsg(nRtn));
		WriteLog((LPSTR)(LPCTSTR)strTemp);
		return FALSE;
	}

	TRACE("2. Module %d :: CMD => EP_CMD_CHECK_PARAM Data Send\n", nModuleID);

//	TRACE("Module %d :: Step Head Data Send\n", nModuleID);
	int nProgressStep = testHeader.totalStep * 2;
	SetProgressWnd(0, nProgressStep, "Sending condition to module...");

	CStep *pStep;
	LPVOID lpData = NULL;
	nProgressStep = 0;
	int j = 0;

	for(j=0; j< testHeader.totalStep; j++)
	{
		pStep = lpProc->GetStep(j);		

		if(pStep->m_type == 3 && nFlag == 0) //20200807 엄륭
		{
			nFlag = 1;			
		}
		else if(pStep->m_type == 3 && nFlag !=0)
		{
			m_ProcessNo = 0;
		}
		
		if(pStep->GetNetStepData(lpData, nDataSize, m_ProcessNo) == FALSE)	//ljb Step Send
		{
			HideProgressWnd();

			//20201215 ksj
			if(pStep->m_bOverCChk == TRUE)
			{
				pStep->m_bOverCChk = FALSE;
				strTemp.Format("Module %d :: Step %d Capacity Upper Limit 0 check. (%s)\n", nModuleID, j+1, CmdFailMsg(nRtn));
			}			
			else
			{
				strTemp.Format("Module %d :: Step %d Data Convert Fail. (%s)\n", nModuleID, j+1, CmdFailMsg(nRtn));
			}
//			strTemp.Format("Module %d :: Step %d Data Send Fail. (%s)\n", nModuleID, j+1, CmdFailMsg(nRtn));
			WriteLog((LPSTR)(LPCTSTR)strTemp);
			return FALSE;
		}

		// 1. Fan Test 코드 추가		
		if((nRtn = EPSendCommand(nModuleID, nGroupIndex, 0, EP_CMD_STEPDATA, lpData, nDataSize)) != EP_ACK)	
		{
			HideProgressWnd();
			strTemp.Format("Module %d :: Step %d Data Send Fail. (%s)\n", nModuleID, j+1, CmdFailMsg(nRtn));
			WriteLog((LPSTR)(LPCTSTR)strTemp);
			return FALSE;
		}
		delete lpData;

		nProgressStep++;
		SetProgressPos(nProgressStep);
		// Sleep(50);
	}

	nFlag = 0; //20200807 엄륭
	for(j=0; j< testHeader.totalStep; j++)
	{
		pStep = lpProc->GetStep(j);
		if(pStep->GetNetStepGradeData(lpData, nDataSize))
		{
			if((nRtn = EPSendCommand(nModuleID, nGroupIndex, 0, EP_CMD_GRADEDATA, lpData, nDataSize)) != EP_ACK)	
			{
				HideProgressWnd();
				strTemp.Format("Module %d :: Grade %d Data Send Fail. (%s)\n", nModuleID, j+1, CmdFailMsg(nRtn));
				WriteLog((LPSTR)(LPCTSTR)strTemp);
				return FALSE;
			}
			delete lpData;
		}
		
		nProgressStep++;
		SetProgressPos(nProgressStep);
		// Sleep(50);
	}

	if(pModule->WriteTestLogTempFile(lpProc) == FALSE)	//전송 공정 조건 Log 기록 파일 
	{
		HideProgressWnd();
		strTemp.Format("Module %d :: Test Condition Log File Creatation Fail.\n", nModuleID, nGroupIndex+1);
		WriteLog((LPSTR)(LPCTSTR)strTemp);
		return FALSE;
	}

	// EPSendCommand(nModuleID);
	
	HideProgressWnd();	return TRUE;
}


CString CCTSMonDoc::CmdFailMsg(int nCode)
{
	CString strMsg;
	switch(nCode)
	{
	case 0x01:		strMsg = ::GetStringTable(IDS_TEXT_NORMAL);				break;
	case 0x011:		strMsg = ::GetStringTable(IDS_TEXT_NOT_DEFINE_CMD);		break;
	case 0x012:		strMsg = ::GetStringTable(IDS_TEXT_ID_MISMATCH);		break;
	case 0x013:		strMsg = ::GetStringTable(IDS_TEXT_UNKNOWN_CODE);		break;
	case 0x014:		strMsg = ::GetStringTable(IDS_TEXT_GROUP_ID_ERROR);		break;
	case 0x015:		strMsg = ::GetStringTable(IDS_TEXT_UNKNOWN_STEP_TYPE);	break;
	case 0x016:		strMsg = ::GetStringTable(IDS_TEXT_CONDITION_NOT_SEND);	break;
	case 0x017:		strMsg = "";											break;
	case 0x0100:	strMsg = ::GetStringTable(IDS_TEXT_NOT_IDLE);			break;
	case 0x0101:	strMsg = ::GetStringTable(IDS_TEXT_NOT_STANDBY_END);	break;
	case 0x0102:	strMsg = ::GetStringTable(IDS_TEXT_NOT_RUN_PAUSE);		break;
	case 0x0103:	strMsg = ::GetStringTable(IDS_TEXT_NOT_RUN);			break;
	case 0x0104:	strMsg = ::GetStringTable(IDS_TEXT_NOT_PAUSE);			break;
	case 0x0105:	strMsg = ::GetStringTable(IDS_TEXT_ALL_FAULT);			break;
	case 0x0200:	strMsg = ::GetStringTable(IDS_TEXT_CH_NOT_RUN);			break;
	case 0x0300:	strMsg = ::GetStringTable(IDS_TEXT_JIG_NOT_READY);		break;
	case 0x0301:	strMsg = ::GetStringTable(IDS_TEXT_LOCAL_STATE);		break;
	case 0x0302:	strMsg = ::GetStringTable(IDS_TEXT_DOOR_OPEN);			break;

	case EP_TIMEOUT:		strMsg = ::GetStringTable(IDS_TEXT_RESPONSE_TIME_OUT);		break;				//0x00000002
	case EP_SIZE_MISMATCH:	strMsg = ::GetStringTable(IDS_TEXT_DATA_LENGTH_ERROR);		break;			//0x00000003
	case EP_RX_BUFF_OVER_FLOW:	strMsg = ::GetStringTable(IDS_TEXT_RX_OVERFLOW);		break;	//0x00000004
	case EP_TX_BUFF_OVER_FLOW:	strMsg = ::GetStringTable(IDS_TEXT_TX_OVERFLOW);		break;	//0x00000005
	case EP_FAIL:	strMsg.Format("Fail Code %d", EPGetLastError());	break;

	default:	strMsg.Format("Code %d", nCode);	break;
	}
	return strMsg;
}

BOOL CCTSMonDoc::SaveAutoProcSetToDB(int nModuleID, BOOL bAutoSet)
{
	CDaoDatabase  db;
	CString strTemp;
	int sysType = ((CCTSMonApp*)AfxGetApp())->GetSystemType();

	try
	{
 		db.Open(GetDataBaseName());
		CString strSQL;
		strSQL.Format("UPDATE SystemConfig SET AutoProcess = %d WHERE ModuleID = %d", 
				bAutoSet,
				nModuleID
			);
		if(sysType != 0)
		{
			strTemp.Format(" AND ModuleType = %d", sysType);
			strSQL += strTemp;
		}
			
		db.Execute(strSQL);
		db.Close();
	}
	catch(CDaoException *e)
	{
      // Simply show an error message to the user.
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
	}

	return TRUE;
}


//CTray CCTSMonDoc::FindTrayFromDB(int nTraySerial)
BOOL CCTSMonDoc::FindTrayFromDB(CString strTrayNo, CTray &trayData)
{
/*	if(trayData.lTraySerial <= 0)
	{
		CString strData;
		strData.Format("Tray Seial Error. Serial [%d], ID [%s]", trayData.lTraySerial, trayData.strTrayNo);
		WriteLog(strData);
		return FALSE;
	}
*/
	return trayData.LoadTrayData(strTrayNo);
	
/*	CTrayRecordSet	recordSet;
	recordSet.m_strFilter.Format("[TraySerial] = %ld", trayData.lTraySerial);
	recordSet.m_strSort.Format("[ID]");
	
	try
	{
		recordSet.Open();
	}
	catch (CDBException* e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();
		return FALSE;
	}
	
	if(recordSet.IsBOF() || recordSet.IsEOF())
	{
		recordSet.Close();
		return FALSE;
	}
	
	trayData.lTraySerial = recordSet.m_TraySerial;
	trayData.strJigID = recordSet.m_JigID;
	trayData.lTeskID = recordSet.m_TeskID;

	trayData.registedTime.SetDateTime(	recordSet.m_RegistedTime.GetYear(), 
										recordSet.m_RegistedTime.GetMonth(),
										recordSet.m_RegistedTime.GetDay(), 
										recordSet.m_RegistedTime.GetHour(), 
										recordSet.m_RegistedTime.GetMinute(), 
										recordSet.m_RegistedTime.GetSecond()
									); 
	trayData.strTrayNo = recordSet.m_TrayNo;
	trayData.strTrayName = recordSet.m_TrayName;
	trayData.strUserName = recordSet.m_UserName;
	trayData.strDescription = recordSet.m_Description;
	trayData.lModelKey = recordSet.m_ModelKey;		
	trayData.lTestKey = recordSet.m_TestKey;		
	trayData.strLotNo = recordSet.m_LotNo;					
	trayData.strTestSerialNo = recordSet.m_TestSerialNo;	

	trayData.testDateTime.SetDateTime( recordSet.m_TestDateTime.GetYear(), 
										recordSet.m_TestDateTime.GetMonth(),
										recordSet.m_TestDateTime.GetDay(), 
										recordSet.m_TestDateTime.GetHour(), 
										recordSet.m_TestDateTime.GetMinute(), 
										recordSet.m_TestDateTime.GetSecond()
									); 

	trayData.lCellNo = recordSet.m_CellNo;
	trayData.nNormalCount = recordSet.m_NormalCount;
	trayData.nFailCount = recordSet.m_FailCount;
	trayData.strOperatorID = recordSet.m_OperatorID;
	trayData.lInputCellCount = recordSet.m_InputCellNo;
	
	CString fileName;
	fileName.Format("%s\\Tray\\%s.try", m_strCurFolder, recordSet.m_TrayNo);
	FILE *fp = fopen(fileName, "rb");
	if(fp )
	{
		if(fread(trayData.cellCode, sizeof(trayData.cellCode), 1, fp) < 1)
		{
			for(int i =0; i<EP_MAX_CH_PER_MD; i++)
			{
				trayData.cellCode[i] = EP_CODE_NORMAL;
			}
		}
		if(fread(trayData.cellGradeCode, sizeof(trayData.cellGradeCode), 1, fp) < 1)
		{
			for(int i =0; i<EP_MAX_CH_PER_MD; i++)
			{
				trayData.cellGradeCode[i] = 0;
			}
		}
		fclose(fp);
	}
	else
	{
		for(int i =0; i<EP_MAX_CH_PER_MD; i++)
		{
			trayData.cellCode[i] = EP_CODE_NORMAL;
			trayData.cellGradeCode[i] = 0;
		}
	}
	
	recordSet.Close();
	return TRUE;
*/
}

STR_CONDITION_HEADER CCTSMonDoc::SelectTest(BOOL bSelectFirstTest)
{
	STR_CONDITION_HEADER testHeader;
	ZeroMemory(&testHeader, sizeof(STR_CONDITION_HEADER));

	CProcedureSelDlg	*pDlg = NULL;
	pDlg = new CProcedureSelDlg();
	if(bSelectFirstTest)	
	{
		pDlg->m_bShowTestSelect = FALSE;
	}
	ASSERT(pDlg);
//	pDlg->m_strModuleName = ::GetModuleName(nModuleID, nGroupIdnex);
	if(pDlg->DoModal() != IDOK)
	{
		delete pDlg;
		pDlg= NULL;
		return	testHeader; 
	}

	testHeader = pDlg->m_TestHeader;
	delete pDlg;
	pDlg = NULL;

	return testHeader;
}

BOOL CCTSMonDoc::FileNameCheck(char *szfileName)
{
	//현재 파일명이 사용 가능한 파일명인지 확인(특수 문자등의 입력으로 파일생성 안되는것을 방지) 
	CString strTemp;
	FILE *fp = NULL;
	if(strlen(szfileName) <= 0)	return  FALSE;

	if((fp = fopen(szfileName, "wb")) != NULL)
	{
		fclose(fp);
		fp = NULL;
		_unlink(szfileName);	//확인 됐으면 삭제(불 필요한 파일 증가 막기 위해) 
		return TRUE;
	}
	else
	{
		strTemp.Format(GetStringTable(IDS_MSG_FILE_NAME_INVALID), szfileName);
		AfxMessageBox(strTemp);
	}
	return FALSE;
}

//Tray의 최종 상태를 기록 
BOOL CCTSMonDoc::UpdateTrayTestData(int nModuleID, int nTrayIndex)
{
	CTray *pTrayData = GetTrayData(nModuleID, nTrayIndex);
	if(pTrayData == NULL)	return FALSE;;
	
	//가장 마지막 Test 공정 이면 공정 완료 처리 한다.
	if(CheckLastProcess(pTrayData))
	{
		TRACE("Tray ID %s is Process Ended. Delete Tray Process Infromation\n", pTrayData->GetTrayNo());

		//Tray 등록을 삭제 하면 가장 마지막 공정을 완료 하면 이전 공정을 재작업 하지 못함 
		//pTrayData->DeleteTray();

		//Reset 시키고 작업 시작시 Reset 된 Tray 정보 삭제 
		//pTrayData->ResetTrayData();
		//pTrayData->WriteTrayData();
	}
	pTrayData->WriteTrayData();
	return TRUE;
}

//모듈의 최종 상태로 전송해야 한다.
//Cell상태만 전송한다.
BOOL CCTSMonDoc::SendLastTrayStateData(int nModuleID, int nGroupIndex)
{
	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule == NULL)	return FALSE;

	int nTotalHWCh = EPGetChInGroup(nModuleID, nGroupIndex);	//전송해야할 채널수 
	int nTraychSum = 0;
	for(int t=0; t<pModule->GetTotalJig(); t++)
	{
		nTraychSum += pModule->GetChInJig(t);
	}

	CString strTemp;	
	EP_TRAY_STATE	trayState;
	ZeroMemory(&trayState, sizeof(trayState));

	CTray *pTray;
	for(int nTrayIndex=0; nTrayIndex<INSTALL_TRAY_PER_MD; nTrayIndex++)
	{
		pTray = pModule->GetTrayInfo(nTrayIndex);
		if(pTray)
		{
			int nStartChIndex = pModule->GetStartChIndex(nTrayIndex);
			int nChInJig = pModule->GetChInJig(nTrayIndex);
//			int nChInTray = pModule->GetCellCountInTray();
			int nCnt = 0;
			
			for(int ch = nStartChIndex; ch<nStartChIndex+nChInJig; ch++)
			{
				if(pTray->IsNewProcedureTray() == TRUE || nTraychSum == nTotalHWCh)	//1:1 Matching	
				{
					if( nCnt < pTray->lInputCellCount)
					{
						trayState.cellCode[ch] = EP_CODE_NORMAL;
					}
					else
					{
						trayState.cellCode[ch] = 0x02;
					}
				}
				else		//이전 공정의 결과 상태를 전송 //1:1 Mpping이 아니면 모두 양품으로 전송
				{
					trayState.cellCode[ch] = pTray->cellCode[ch];
				}
				nCnt++;
			}
		}
	}

	int nRtn;
	if((nRtn = EPSendCommand(nModuleID, nGroupIndex+1, 0, EP_CMD_TRAY_DATA, &trayState, sizeof(trayState))) != EP_ACK)
	{
		m_strLastErrorString.Format("%s :: TrayData Send Fail. (Code: %s)\n", ::GetModuleName(nModuleID), CmdFailMsg(nRtn));
		WriteLog((LPSTR)(LPCTSTR)m_strLastErrorString);
		return FALSE;
	}

	return TRUE;
}

CTray * CCTSMonDoc::GetTrayData(int nModuleID, int nTrayIndex)
{
	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule == NULL)		return NULL;
	return pModule->GetTrayInfo(nTrayIndex);
}

BOOL CCTSMonDoc::RequeryBatteryModel(long lModelID, STR_CONDITION_HEADER *pModel)
{
	CTestCondition a;
	if(a.RequeryBatteryModel(lModelID, pModel))
	{
		return TRUE;
	}
	return FALSE;


/*	CBatteryModelRecordSet recordSet;
	recordSet.m_strFilter.Format("[ModelID] = %ld", lModelID);
	
	try
	{
		recordSet.Open();
	}
	catch (CDBException* e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();
		return FALSE;
	}

	if(recordSet.IsBOF() || recordSet.IsEOF())
	{
		recordSet.Close();
		return FALSE;
	}

	ZeroMemory(pModel, sizeof(STR_CONDITION_HEADER));
	
	pModel->lID = recordSet.m_ModelID;
	sprintf(pModel->szName, "%s", recordSet.m_ModelName);
	sprintf(pModel->szDescription, "%s", recordSet.m_Description);
	sprintf(pModel->szModifiedTime, "%s", recordSet.m_CreatedTime.Format());

	recordSet.Close();
	return TRUE;
*/
}

CTestCondition * CCTSMonDoc::GetCondition(int nModuleID, int nGroupIndex)
{
	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule == NULL)		return NULL;

	return pModule->GetCondition();
}

BOOL CCTSMonDoc::SendProcedureLogToDataBase(int nModuleID, int nTrayIndex)
{
	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule == NULL)		return FALSE;

	EP_MD_SYSTEM_DATA *pSysData = EPGetModuleSysData(EPGetModuleIndex(nModuleID));

	int nRtn;
	CMainFrame *pFrame = (CMainFrame *)AfxGetMainWnd();
	if(pFrame->m_bDBSvrConnect)	//자동 공정 모드이고 DB가 연결되어 있으면 전송 
	{
		CTestCondition *pCondition = pModule->GetCondition();
		EP_PROCEDURE_DATA	procData;
		ZeroMemory(&procData, sizeof(procData));
		
		//////////////////////////////////////////////////////////////////////////
		sprintf(procData.szModuleName, "%s", ::GetModuleName(pModule->GetModuleID()));
		procData.nModuleID = pModule->GetModuleID();
		procData.wModuleGroupNo = pSysData->nModuleGroupNo;
		procData.wJigIndex = nTrayIndex;
		sprintf(procData.szDateTime,	"%s", pModule->GetRunStartTime().Format("%Y-%m-%d %H:%M:%S"));			
		sprintf(procData.szModuleIP,	"%s", pModule->GetIPAddress());	
		sprintf(procData.szTestName,	"%s", pModule->GetTestName());
		
		sprintf(procData.szTrayNo,		"%s", pModule->GetTrayNo(nTrayIndex));			
		sprintf(procData.szLotNo,		"%s", pModule->GetLotNo(nTrayIndex));				
		sprintf(procData.szOperatorID,  "%s", pModule->GetOperatorID(nTrayIndex));
		sprintf(procData.szTestSerialNo, "%s",  pModule->GetTestSerialNo(nTrayIndex));
		sprintf(procData.szResultFileName, "%s", pModule->GetResultFileName(nTrayIndex));
		procData.nCellNo		= pModule->GetCellNo(nTrayIndex);
		procData.lTestSeqenceNo	=  pModule->GetTestSequenceNo();
		procData.lTestID		=  pCondition->GetTestInfo()->lID;
		procData.lModelID		= pCondition->GetModelID();
		strcpy(procData.szModelName, pCondition->GetModelInfo()->szName);
		procData.procedureType	= (BYTE)pCondition->GetTestProcType();
//		procData.installTotalChCount = 	pModule->GetChInJig(nTrayIndex);	//BYTE)::EPGetChInGroup(nModuleID, 0);
		procData.wCellInTrayCount = pModule->GetCellCountInTray();
		procData.nInputCellCount = pModule->GetInputCellCount(nTrayIndex);
		procData.bAutoProcess = EPGetAutoProcess(nModuleID);
		
		memcpy(&procData.mdSysData, ::EPGetModuleSysData(EPGetModuleIndex(nModuleID)), sizeof(EP_MD_SYSTEM_DATA));
		memcpy(&procData.sensorMap1, pModule->GetSensorMap(CFormModule::sensorType1), sizeof(_MAPPING_DATA)*EP_MAX_SENSOR_CH);
		memcpy(&procData.sensorMap2, pModule->GetSensorMap(CFormModule::sensorType2), sizeof(_MAPPING_DATA)*EP_MAX_SENSOR_CH);

		//////////////////////////////////////////////////////////////////////////
					
		nRtn = ::dcSendData(DB_CMD_PROCEDURE_DATA, &procData, sizeof(EP_PROCEDURE_DATA));
		
		if(nRtn != EP_ACK)
		{
			CString strTemp;
			strTemp.Format("Procedure Information Send to Data Server Fail. Return %d\n", CmdFailMsg(nRtn));
			WriteLog((LPSTR)(LPCTSTR)strTemp);
			return FALSE;
		}
	}
	return TRUE;
}

CString CCTSMonDoc::ValueString(double dData, int item, BOOL bUnit)
{
	CString strMsg, strTemp;
	double dTemp;
	BYTE color;
	char szTemp[8];

	dTemp = dData;
	switch(item)
	{
	case EP_STATE:		strMsg = GetStateMsg((WORD)dTemp, color);	
		break;
		
	case EP_VOLTAGE:		//voltage
		//mV단위로 변경 
		if(m_strVUnit == "V")
		{
			dTemp = dTemp / 1000.0f;	//LONG2FLOAT(Value);
		}
		//else //if(m_strVUnit == "mV")
		//{
		//}
		
		if(m_nVDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nVDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}		
		if(bUnit) strMsg += (" "+m_strVUnit);
		break;

//		CUnit vtgUnit(DSP_UNIT_FLOAT);
//		strMsg = vtgUnit.GetData(Value);

	case EP_CURRENT:		//current
//		if(dTemp < 0)	dTemp = -dTemp;			//양수 표기
		
		//current
		if(m_strIUnit == "A")
		{
			dTemp = dTemp/1000.0f;
		}  
		else if(m_strIUnit == "uA")
		{
			dTemp = dTemp * 1000.0f;		// LONG2FLOAT(Value)/1000.0f;
		}
		//else if(m_strIUnit == "mA")	//mA
		//{
		//}

		if(m_nIDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nIDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}
		if(bUnit) strMsg += (" "+m_strIUnit);
		break;
			
	case EP_WATT	:				

		if(dTemp < 0)	dTemp = -dTemp;			//양수 표기

		if(m_strWUnit == "W")
		{
			dTemp = dTemp/1000.0f;
		}
		//else //if(m_strCUnit == "mAh" || m_strCUnit == "mF")
		//{
		//}

		if(m_nCDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nCDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}
		if(bUnit) strMsg += m_strWUnit;
		break;
	
	case EP_WATT_HOUR:			
	
		if(dTemp < 0)	dTemp = -dTemp;			//양수 표기

		if(m_strWhUnit == "Wh")
		{
			dTemp = dTemp/1000.0f;
		}
		//else //if(m_strCUnit == "mAh" || m_strCUnit == "mF")
		//{
		//}

		if(m_nCDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nCDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}
		if(bUnit) strMsg += m_strWhUnit;
		break;
	
	case EP_CAPACITY:		//capacity

		if(dTemp < 0)	dTemp = -dTemp;			//양수 표기

		if(m_strCUnit == "Ah" || m_strCUnit == "F")
		{
			dTemp = dTemp/1000.0f;
		}
		//else //if(m_strCUnit == "mAh" || m_strCUnit == "mF")
		//{
		//}

		if(m_nCDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nCDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}
		if(bUnit) strMsg += (" "+m_strCUnit);
		break;

	case EP_IMPEDANCE:	
		strMsg.Format("%.2f", dTemp);
		if(bUnit) strMsg += " mOhm";
		break;

	case EP_CH_CODE:	//failureCode
		strMsg = ChCodeMsg((WORD)dTemp);
		break;

	case EP_TOT_TIME:
	case EP_STEP_TIME:
		//sbc에서 100msec단위 전송되어 온다.
		if(m_nTimeUnit == 1)	//sec 표시
		{
			strMsg.Format("%.1f", dTemp);
			if(bUnit) strMsg += " sec";
		}
		else if(m_nTimeUnit == 2)	//Minute 표시
		{
			strMsg.Format("%.2f", dTemp/60.0f);
		}
		else
		{
			CTimeSpan timeSpan((ULONG)dTemp);
			if(timeSpan.GetDays() > 0)
			{
				strMsg =  timeSpan.Format("%Dd %H:%M:%S");
			}
			else
			{
				strMsg = timeSpan.Format("%H:%M:%S");
			}
		}
		break;

	case EP_GRADE_CODE:	
		strMsg = ::GetSelectCodeName((BYTE)dTemp);

		break;

	case EP_TEMPER:
		if(EP_NOT_USE_DATA == (float)dTemp)
		{
			strMsg = "None";
		}
		else
		{
			strMsg.Format("%.1f", dTemp);
			if(bUnit) strMsg += " ℃";
		}
		
		break;
		
	case EP_STEP_NO:
	default:
		strMsg.Format("%d", (int)dTemp);
		break;
	}
	
	return strMsg;
}

CString CCTSMonDoc::GetProcTypeName(int nType, int nStepType)
{
	STR_MSG_DATA *pMsg;
	CString strTemp("Unknown");

	if(nType == EP_PROC_TYPE_NONE || m_apProcType.GetSize() == 0)
		return strTemp = StepTypeMsg(nStepType);


	for(int i =0; i<m_apProcType.GetSize(); i++)
	{
		pMsg = (STR_MSG_DATA *)m_apProcType[i];
		if(nType == pMsg->nCode)
		{
			strTemp.Format("%s", pMsg->szMessage);
			return strTemp;
		}
	}
	return strTemp;
}

BOOL CCTSMonDoc::RequeryProcType()
{
	CDaoDatabase  db;

	ASSERT(m_apProcType.GetSize() == 0);

	if(m_strDataBaseName.IsEmpty())		return FALSE;
	db.Open(m_strDataBaseName);
	

	CString strSQL;
	strSQL.Format("SELECT ProcType, ProcID, Description FROM ProcType WHERE ProcType >= %d ORDER BY ProcType", EP_PROC_TYPE_CHARGE_LOW);
	
		COleVariant data;
	CDaoRecordset rs(&db);
	rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
	
	if(!rs.IsBOF() && !rs.IsEOF())
	{
		COleVariant data;
		STR_MSG_DATA	*pObject;
				
		pObject = new STR_MSG_DATA;
		pObject->nCode = 0;
		pObject->nData = 0;
		// sprintf(pObject->szMessage, "임의공정");
		sprintf(pObject->szMessage, "None");
		m_apProcType.Add(pObject);		

		while(!rs.IsEOF())
		{
			pObject = new STR_MSG_DATA;
			ASSERT(pObject);
			ZeroMemory(pObject, sizeof(STR_MSG_DATA));

			data = rs.GetFieldValue(0);
			pObject->nCode = data.lVal;
			data = rs.GetFieldValue(1);
			pObject->nData = data.lVal;
			data = rs.GetFieldValue(2);
			if(VT_NULL != data.vt) sprintf(pObject->szMessage, "%s", data.pbVal);
			
			m_apProcType.Add(pObject);
			rs.MoveNext();
		}	
	}
	rs.Close();
	db.Close();
	return TRUE;
}

int CCTSMonDoc::GetLineMode(int nModuleID, int nGroupIndex)
{
	int nLineMode = EP_OFFLINE_MODE;
	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule == NULL)		return EP_OFFLINE_MODE;

	if(pModule->GetState() != EP_STATE_LINE_OFF)
	{
		nLineMode = pModule->GetLineMode();
	}
	return nLineMode;
}

int CCTSMonDoc::GetOperationMode(int nModuleID, int nGroupIndex)
{
	int nOperationMode = EP_OPERATION_LOCAL;
	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule == NULL)		return EP_OPERATION_LOCAL;

	if(pModule->GetState() != EP_STATE_LINE_OFF)
	{
		nOperationMode = pModule->GetOperationMode();
	}
	return nOperationMode;
}

BOOL CCTSMonDoc::SetLineMode(int nModuleID, int nGroupIndex, int nLineMode)
{
	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule == NULL)		return FALSE;

	CString strTemp;

	//Send Line State to Module
	if(EPSetLineMode(nModuleID, nGroupIndex+1, EP_LINE_MODE, nLineMode) == FALSE)
	{
		strTemp.Format("%s Line Mode Setting Fail.",  ::GetModuleName(nModuleID, nGroupIndex));		
		WriteLog((LPSTR)(LPCTSTR)strTemp);
		return FALSE;
	}
	
	return TRUE;
}

BOOL CCTSMonDoc::SetOperationMode(int nModuleID, int nGroupIndex, int nOperationMode)
{
	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule == NULL)		return FALSE;

	CString strTemp;

	//Send Control State to Module (Send to Online scheduler Module is Maintenance state)
	if(EPSetLineMode(nModuleID, nGroupIndex+1, EP_OPERATION_MODE, nOperationMode) == FALSE)
	{
		strTemp.Format("%s Control Mode Setting Fail.",  ::GetModuleName(nModuleID, nGroupIndex));		
		WriteLog((LPSTR)(LPCTSTR)strTemp);
		return FALSE;
	}

	return TRUE;
}


VOID CCTSMonDoc::SetAutoReport(int nNewModuleID, int nOldModuleID)
{
	CString msg;
			
	// 이전 모듈의 모니터링 간격을 0으로 한다. 
	// 20090713 KBH
	// 현재 모니터링 하지 않는 Unit도 일정 간격(느리게) 보고 하도록 수정
	// 20091116  설치 모듈 수가 작을 때는 Report 간격 조절하지 않음
	// 20130328 Unit 수가 32개 이상일 경우는	
	
	//현재 모듈의 모니터링 간격을 Setting 한다.
	if(nNewModuleID > 0 && ::EPGetModuleState(nNewModuleID) != EP_STATE_LINE_OFF)
	{
		EP_SYSTEM_PARAM *pSysParam = ::EPGetSysParam(nNewModuleID);
		if(::EPSetAutoReport(nNewModuleID, 1, m_TopConfig.m_ChannelInterval/*sec*/, pSysParam->nAutoReportInterval*1000) == FALSE)
		{
			msg.Format("%s Monitoring Interval Setting Fail", ::GetModuleName(nNewModuleID));
			WriteLog(msg);
		}
	}	

	if( GetInstalledModuleNum() > 64 )
	{
		if( nOldModuleID > 0 && ::EPGetModuleState(nOldModuleID) != EP_STATE_LINE_OFF && nNewModuleID != nOldModuleID)
		{
			EP_SYSTEM_PARAM *pSysParam = ::EPGetSysParam(nOldModuleID);
			if(::EPSetAutoReport(nOldModuleID, 1, 10, pSysParam->nAutoReportInterval*1000) == FALSE)  //10 sec
			{
				msg.Format("%s Monitoring Interval Reset Fail", ::GetModuleName(nNewModuleID));
				WriteLog(msg);
			}
		}
	}
}

//Log Temp 파일에서 시험조건을 읽어서 표시 한다.
void CCTSMonDoc::ShowTestCondition(int nModuleID, int nGroupIndex)
{
		//전송된 시험 조건을 Display
	int nModuleIndex = EPGetModuleIndex(nModuleID);
	ASSERT(nModuleIndex >= 0 && nModuleIndex < GetInstalledModuleNum());
	ASSERT(nGroupIndex >= 0 && nGroupIndex < EPGetGroupCount(nModuleID));
	
	CString strTemp;
	WORD state = EPGetGroupState(nModuleID, nGroupIndex);
	// if(	state == EP_STATE_LINE_OFF || state == EP_STATE_IDLE)
	if(	state == EP_STATE_LINE_OFF )
	{
		CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();
		pMainFrm->ShowInformation( nModuleID, TEXT_LANG[15], INFO_TYPE_WARNNING);//"결과데이터를 찾을 수 없습니다."
		// strTemp.Format(GetStringTable(IDS_TEXT_CONDITION_NOT_FOUND));//, ::GetModuleName(nModuleID));
		// AfxMessageBox(strTemp);
		return;
	}

	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule)
	{
		CTestCondition *pTestConditon = pModule->GetCondition();
		CDetailAutoTestConditionDlg *pDlg;
		pDlg = new CDetailAutoTestConditionDlg(pTestConditon);
		pDlg->m_pDoc = (CCTSMonDoc *)this;
		pDlg->DoModal();
		delete pDlg;
		pDlg = NULL;
	}
	
	//	CString strFileName;
//	strFileName.Format("%s\\M%dG%dTest.log", m_strTempFolder, nModuleID, nGroupIndex+1);
/*	FILE *fp = fopen(strFileName, "rb");
	if(fp == NULL)
	{
		strTemp.Format(" [%s]", ::GetModuleName(nModuleID, nGroupIndex));
		AfxMessageBox(GetStringTable(IDS_TEXT_CONDITION_NOT_FOUND) + strTemp);
		return;
	}
	if(ReadEPFileHeader(TEST_LOG_FILE, fp) < 0)
	{
		fclose(fp);
		fp = NULL;
		return;
	}
	RESULT_FILE_HEADER	resultFileHeader;
	if(fread(&resultFileHeader, sizeof(resultFileHeader), 1, fp)<1)		//File Header Read
	{
		fclose(fp);
		fp = NULL;
		return;		
	}
	if(resultFileHeader.nModuleID != nModuleID || resultFileHeader.nGroupIndex != nGroupIndex)
	{
		fclose(fp);
		fp = NULL;
		return;
	}
	fseek(fp, sizeof(EP_MD_SYSTEM_DATA)+sizeof(STR_FILE_EXT_SPACE), SEEK_CUR);
	STR_CONDITION	*pCondition;
	pCondition = new STR_CONDITION;
	ASSERT(pCondition);
	if(m_pModule[nModuleIndex].ReadTestConditionFile(fp, pCondition) == FALSE)					//Read Test Condition
	{
		RemoveCondition(pCondition);
		pCondition = NULL;
		fclose(fp);
		fp = NULL;
		return;
	}
	fclose(fp);
	fp = NULL;
*/
}

void CCTSMonDoc::ShowGroupFailCode(int nModuleID, int nGroupIndex)
{
	BYTE gpFailCode = EPGetFailCode(nModuleID);
	CFailMsgDlg *pDlg = new CFailMsgDlg();
	ASSERT(pDlg);
	pDlg->SetModuleName(::GetModuleName(nModuleID), GetModuleIPAddress(nModuleID));
	pDlg->SetErrorCode(gpFailCode, m_strDataBaseName);
	pDlg->DoModal();
	
	delete pDlg;
	pDlg = NULL;
}

void CCTSMonDoc::SendAutoProcessOn(CWordArray& awRows, BOOL bOn)
{
	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_PERMISSION_ERROR) + " [Change to auto processing]");
		return ;
	}

//	int nGroupNo;
	int nSelNo = awRows.GetSize();
	CString strTemp;
	if(bOn)	strTemp.Format(GetStringTable(IDS_MSG_CHANGE_AUTO_PROCESS2), nSelNo, m_strModuleName);
	else 	strTemp.Format(GetStringTable(IDS_MSG_CHANGE_MANUAL_PROCESS2), nSelNo, m_strModuleName);
	
	if(MessageBox(NULL, strTemp, "Mode Change", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;

//	EP_NVRAM_SET	nvRamSetData;
//	nvRamSetData.nUseNVRam = TRUE;
	int mID;//, nRtn;
	for (int i = 0; i<nSelNo; i++)				//선택한 모든 Row
	{
		mID = awRows[i];			//선택 Row의 ID를 구한다.

//		nRtn = EPSendCommand(mID, 0, 0, EP_CMD_NVRAM_SET, &nvRamSetData, sizeof(EP_NVRAM_SET));
//		if(nRtn != EP_ACK)
//		{
//			strTemp.Format("%s Auto Process Setting Fail... (%s)", ::GetModuleName(mID), CmdFailMsg(nRtn));
//			AfxMessageBox(strTemp);
//		}
//		else
//		{
			::EPSetAutoProcess(mID, bOn);
			SaveAutoProcSetToDB(mID, bOn);		//Save Setting to DB
//		}
	}	
}
/*
void CCTSMonDoc::SendAutoProcessOff(CRowColArray& awRows)
{
	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_PERMISSION_ERROR) + " [Change to manual processing]");
		return ;
	}

//	int nGroupNo;
	int nSelNo = awRows.GetSize();

	CString strTemp;
	strTemp.Format(GetStringTable(IDS_MSG_CHANGE_MANUAL_PROCESS2), nSelNo, m_strModuleName);
	if(MessageBox(NULL, strTemp, "Mode Change", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;

	EP_NVRAM_SET	nvRamSetData;
	nvRamSetData.nUseNVRam = FALSE;
	int mID, nRtn;
	for (int i = 0; i<nSelNo; i++)				//선택한 모든 Row
	{
		mID = awRows[i];			//선택 Row의 ID를 구한다.

		nRtn = EPSendCommand(mID, 0, 0, EP_CMD_NVRAM_SET, &nvRamSetData, sizeof(EP_NVRAM_SET));
		if(nRtn != EP_ACK)
		{
			strTemp.Format("%s Auto Process Setting Fail... (%s)", ::GetModuleName(mID), CmdFailMsg(nRtn));
			AfxMessageBox(strTemp);
		}
		else
		{
			::EPSetAutoProcess(mID, FALSE);
			SaveAutoProcSetToDB(mID, FALSE);
		}
	}	
}
*/
BOOL CCTSMonDoc::SendSetSaveIntervalCommand(int nSelModuleID)
{
	int nSaveInterval=::EPGetSysParam(nSelModuleID)->nAutoReportInterval;
	return ::EPSetAutoReport(nSelModuleID, 1, m_TopConfig.m_ChannelInterval, nSaveInterval*1000);
}

//모듈이 꺼져도 되는지여부를 응답해 준다.
void CCTSMonDoc::OnReceiveModuleShutdownCmd(int nModuleID, int nReceiveCmd)
{
// #if CUSTOMER_TYPE == FINE_CELL
// 	//IR / OCV 는 연계된 장비가 없으므로 ACK 전송 
// 	if(((CCTSMonApp*)AfxGetApp())->GetSystemType() != EP_ID_IROCV_SYSTEM )
// 	{
// 		int nPairModuleID = nModuleID+m_nModulePerRack;
// 		
// 		WORD wdState;
// 		
// 		//	if( (nModuleID%2) && (nPairModuleID<=::EPGetModuleID(m_nInstalledModuleNum-1)) )
// 			
// 		if((nModuleID-1)/3%2)	//MC가 부착 Module
// 		{
// 			wdState = ::EPGetGroupState(nPairModuleID, 0);
// 			if( EP_STATE_RUN == wdState)
// 				nCode = EP_NACK;
// 			else
// 				nCode = EP_ACK;
// 
// 		}
// 		else					//MC가 부착 되어 있지 않은 장비 
// 		{
// 			wdState = ::EPGetGroupState(nModuleID, 0);
// 			if( EP_STATE_RUN == wdState)
// 				nCode = EP_NACK;
// 			else
// 				nCode = EP_ACK;
// 		}
// 	}
// #endif
	int nCode = EP_ACK;
	WORD wdState;
	wdState = ::EPGetGroupState(nModuleID, 0);
	if( EP_STATE_RUN == wdState)
		nCode = EP_NACK;
	else
		nCode = EP_ACK;

	::EPSendResponse(nModuleID, nReceiveCmd, nCode);
}

//해당 Tray에 예약되어 있는 Schedule 정보를 초기화 하고 DB에서 현재까지 진행한 이력 정보를 삭제한다.
BOOL CCTSMonDoc::ResetTrayProcSchedule(CString strTrayNo)
{
	//현재 사용중인 tray인지 검사 
	CString strTemp;
	BOOL  nFindState = FALSE;
	WORD state;
	int nModuleID = 0;
	for(int i=0; i<GetInstalledModuleNum(); i++)
	{
		nModuleID = EPGetModuleID(i);
		strTemp = GetTrayNo(nModuleID, 0);
		if(strTemp == strTrayNo)
		{
			state = EPGetGroupState(nModuleID, 0);
			if(state == EP_STATE_RUN || state == EP_STATE_PAUSE)
			{
				nFindState = TRUE;
				break;
			}
		}
	}

	if(nFindState)	//사용중인 Tray이므로 reset 시킬수 없음 
	{
		m_strLastErrorString.Format(TEXT_LANG[10], strTrayNo, ::GetModuleName(nModuleID));	//"[%s]는 현재 %s에서 작업중이므로 초기화 할 수 없습니다."
		AfxMessageBox(m_strLastErrorString);
		return FALSE;
	}
	
	CTray tray;
	CString strModuleTray;
	if(tray.LoadTrayData(strTrayNo) == FALSE)
	{
		m_strLastErrorString.Format(TEXT_LANG[11], strTrayNo);//"[%s] 등록 정보를 찾을 수 없습니다."
		AfxMessageBox(m_strLastErrorString);
		return FALSE;
	}

	STR_CONDITION_HEADER testHeader = GetNextTest(tray.GetProcessSeqNo());
	if(testHeader.lID != 0)
	{
		strModuleTray.Format(TEXT_LANG[12], strTrayNo, testHeader.szName);//"tray [%s]는 [%s] 공정이 예약되어 있습니다. 공정 예약을 초기화 하시겠습니까?"
		if(AfxMessageBox(strModuleTray, MB_ICONQUESTION|MB_YESNO) != IDYES)		return FALSE;
	}

	if(tray.ResetTrayData())
	{
		strModuleTray.Format(TEXT_LANG[13], strTrayNo);//"tray [%s] 예약 공정 초기화 되었습니다."
		WriteLog(strModuleTray);
		
		//DataBase 에서 현재 Tray에 대한 처리 방법을 확인 한다.
		strTemp.Format(::GetStringTable(IDS_MSG_TEST_LOG_DELETE_CONFIRM), strTrayNo);
		CMainFrame *pFrame = (CMainFrame *)AfxGetMainWnd();
		if(pFrame->m_bDBSvrConnect && EPGetAutoProcess(nModuleID) && !tray.strTestSerialNo.IsEmpty())	//자동 공정 모드이고 DB가 연결되어 있으면 전송 
		{
			if(AfxMessageBox(strTemp, MB_YESNO|MB_ICONQUESTION) == IDYES)
			{
				//DataBase Server에 취소된 Data를 삭제 하도록 명령을 보냄 
				EP_DATABASE_MANAGE_DATA	tData;
				ZeroMemory(&tData, sizeof(tData));
				tData.nType = EP_DB_TEST_LOG_DELETE;
				sprintf(tData.szData, "%s", tray.strTestSerialNo);
				if(::dcSendData(DB_CMD_DATA_MANAGE, &tData, sizeof(tData)) <0)
				{
					WriteLog("Data Send Fail to DataBase\n");
				}
				else
				{
					strModuleTray.Format(TEXT_LANG[14], strTrayNo);//"tray [%s] 진행한 이전 공정 정보 삭제 완료."
					WriteLog(strModuleTray);
				}
			}
		}
	}
	else
	{
		return FALSE;
	}

	return TRUE;
}



//2004/4/16
//입력 Tray의 최종 상태를 Tray Table에서 Search 하지 않고 
//공정 결과 table에서 검색 한다. 
BOOL CCTSMonDoc::SearchLastTrayState(CString strTrayNo, int nModuleID, int /*nGroupIndex*/)
{
/*	if(nModuleID < 1 || nGroupIndex < 0 || strTrayNo.IsEmpty())		return FALSE;

	COleVariant data;
	CString strQuery, strSQL, strSerial;
	strSQL = "SELECT";

	for(int i =1; i<=128; i++)
	{
		if(i == 1)
		{
			strSerial.Format(" b.ch%d", i);
		}
		else
		{
			strSerial.Format(", b.ch%d", i);
		}
		strSQL += strSerial;
	}

	CString strTemp;
	strQuery.Format(" FROM Module a, ChResult b WHERE a.ProcedureID = b.ProcedureID", );
	
	strSQL += strQuery;

	CDaoDatabase  db;
	db.Open(m_strDataBaseName);

	CDaoRecordset rs(&db);
	rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
	{
		COleVariant data = rs.GetFieldValue(0);
	}

	while(!rs.IsEOF())
	{
		for(int i = 0; i<rs.GetFieldCount(); i++)
		{
			data = rs.GetFieldValue(i);
		}
		rs.MoveNext();
	}
	rs.Close();
	db.Close();
*/

	return TRUE;
}

BOOL CCTSMonDoc::CheckLastProcess(CTray *pTrayData)
{
	if(pTrayData == NULL)	return FALSE;
	//다음 공정 검색 

	STR_CONDITION_HEADER	testHeader;
	testHeader = GetNextTest(pTrayData->GetProcessSeqNo());

	TRACE("Find Next Process ID	: %d\n", testHeader.lID);
	TRACE("Find Next Test Name	: %s\n", testHeader.szName);
	TRACE("Find Next Test		: %s\n", testHeader.szDescription);
			
	if(testHeader.lID == 0)		//다음 시험 검색 실패나 없을 경우  
	{
		return TRUE;
	}
	
	return FALSE;
}

BOOL CCTSMonDoc::ExecuteIOTest(int nModuleID, BOOL bWaitExit)
{
	// TODO: Add your command handler code here
	CString strIP;
	CString strTemp;
	int nRtn;
	BOOL bUseOutPut = TRUE;
	WORD state = EPGetGroupState(nModuleID, 0);
		
	if(state != EP_STATE_MAINTENANCE)
	{		
		strIP.Format(::GetStringTable(IDS_TEXT_CHANGE_MAINTEN), ::GetModuleName(nModuleID)) ;
		nRtn = AfxMessageBox(strIP, MB_YESNOCANCEL|MB_ICONQUESTION);
		if(nRtn == IDCANCEL)	return FALSE;		//취소 
		else if(nRtn == IDYES)						//전환 
		{
			if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)		//권한 검사 
			{
				AfxMessageBox(GetStringTable(IDS_LANG_MSG_PERMISSION_ERROR));
				return FALSE;
			}

			if(state != EP_STATE_RUN && GetLineMode(nModuleID, 0) == EP_OFFLINE_MODE)	//변경 가능 상태 
			{
				SetOperationMode(nModuleID, 0, EP_OPEARTION_MAINTENANCE);		
			}
			else		//변경 가능 상태가 아닐 경우 
			{
				strIP.Format("%s [%s]", ::GetStringTable(IDS_TEXT_NOT_CHANGING_MODE), ::GetModuleName(nModuleID));
				AfxMessageBox(strIP);
				return FALSE;
			}
		}
		else		//Not Use OutPut Port 
		{
			bUseOutPut = FALSE;
		}
	}

	strIP = GetModuleIPAddress(nModuleID);
	if(strIP.IsEmpty())
	{
		strIP.Format(" [%s]", ::GetModuleName(nModuleID));
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_ERROR_FOUND_IP) + strIP);
		return FALSE;
	}

	// 동일한 프로그램이 실행중인지 확인한다.
// 	HWND FirsthWnd;
// 	FirsthWnd = ::FindWindow(NULL, "IOTest");
// 	if (FirsthWnd)
// 	{
// 		::SetActiveWindow(FirsthWnd);	
// 		::SetForegroundWindow(FirsthWnd);
// 		return FALSE;
// 	}

	STARTUPINFO	stStartUpInfo;
	PROCESS_INFORMATION	ProcessInfo;
	ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
	ZeroMemory(&ProcessInfo, sizeof(PROCESS_INFORMATION));
	
	stStartUpInfo.cb = sizeof(STARTUPINFO);
	stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
	stStartUpInfo.wShowWindow = SW_NORMAL;

	m_IoInfoModuleArray.RemoveAll();
	m_IoInfoModuleArray.FreeExtra();

	CStdioFile sourceFile;			
	CFileException ex;
	
	CString saveFilePath;
	CString strReadData;			
	CString strIndex;
	int nSize = -1;

	strTemp.Format("%s\\IoModuleInfo.csv", m_strCurFolder );		
	
	if( !sourceFile.Open( (LPSTR)(LPCTSTR)strTemp, CFile::modeRead, &ex ))
	{
		CString strMsg;
		ex.GetErrorMessage((LPSTR)(LPCSTR)strMsg,1024);
		AfxMessageBox(strMsg);
		return FALSE;
	}
	int iCnt=0;
	while(TRUE)	
	{			
		sourceFile.ReadString(strReadData);    // 한 문자열씩 읽는다.	
		nSize = strReadData.Find(',');
		
		if( nSize > 0)
		{
			m_IoInfoModuleArray.Add(strReadData);
			iCnt++;
		}			
		else
		{
			break;				
		}
	}
	sourceFile.Close();

	if( iCnt > nModuleID-1 )
	{
		strReadData = m_IoInfoModuleArray.GetAt(nModuleID-1);
		
		AfxExtractSubString(strTemp, strReadData, 0, ',');
		
		if( atoi(strTemp) != nModuleID )
		{
			strTemp.Format("Information of the unit is can not be found in '%s'", saveFilePath);
			AfxMessageBox(strTemp, MB_ICONWARNING);
		}
		else
		{
			AfxExtractSubString(strTemp, strReadData, 1, ',');
			if( atoi(strTemp) == 1 )
			{
				strTemp.Format("%s\\IOTest-S.exe %s %d", m_strCurFolder, strIP, MAKELONG(bUseOutPut, TRUE));	//Set Argument IP Address and simple io test mode
				
				BOOL bFlag = CreateProcess(NULL, (LPSTR)(LPCTSTR)strTemp, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &stStartUpInfo, &ProcessInfo);
				if(bFlag == FALSE)
				{
					m_strLastErrorString = "IOTest-S Program is not Found";
					AfxMessageBox(m_strLastErrorString,  MB_OK|MB_ICONSTOP);
				}
			}
			else
			{
				strTemp.Format("%s\\IOTest-L.exe %s %d", m_strCurFolder, strIP, MAKELONG(bUseOutPut, TRUE));	//Set Argument IP Address and simple io test mode
				
				BOOL bFlag = CreateProcess(NULL, (LPSTR)(LPCTSTR)strTemp, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &stStartUpInfo, &ProcessInfo);
				if(bFlag == FALSE)
				{
					m_strLastErrorString = "IOTest-L Program is not Found";
					AfxMessageBox(m_strLastErrorString,  MB_OK|MB_ICONSTOP);
				}
			}
		}
	}
	else
	{
		strTemp.Format("%s\\IOTest.exe %s %d", m_strCurFolder, strIP, MAKELONG(bUseOutPut, TRUE));	//Set Argument IP Address and simple io test mode
		
		BOOL bFlag = CreateProcess(NULL, (LPSTR)(LPCTSTR)strTemp, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &stStartUpInfo, &ProcessInfo);
		if(bFlag == FALSE)
		{
			m_strLastErrorString = "IOTest Program is not Found";
			AfxMessageBox(m_strLastErrorString,  MB_OK|MB_ICONSTOP);
		}
	}

	CString strDirTemp;
/*	{
		TCHAR szCurDir[MAX_PATH];
		::GetModuleFileName(AfxGetApp()->m_hInstance, szCurDir, MAX_PATH);
		strDirTemp = CString(szCurDir).Mid(0, CString(szCurDir).ReverseFind('\\'));
	}
	strTemp.Format("%s\\IOTest.exe %s %d", strDirTemp, strIP, bUseOutPut);	//IP를 인자로 넘김 
*/	
	/*
	strTemp.Format("%s\\IOTest.exe %s %d", m_strCurFolder, strIP, MAKELONG(bUseOutPut, TRUE));	//Set Argument IP Address and simple io test mode

	BOOL bFlag = CreateProcess(NULL, (LPSTR)(LPCTSTR)strTemp, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &stStartUpInfo, &ProcessInfo);
	if(bFlag == FALSE)
	{
		m_strLastErrorString = "IO Test Program is not Found";
		AfxMessageBox(m_strLastErrorString,  MB_OK|MB_ICONSTOP);
	}
	*/


	if(bWaitExit)
	{
//		((CMainFrame *)AfxGetMainWnd())->ShowWindow(SW_HIDE);
		
		::WaitForSingleObject( ProcessInfo.hProcess, INFINITE);
//		((CMainFrame *)AfxGetMainWnd())->ShowWindow(SW_SHOW);
//		::SetForegroundWindow(((CMainFrame *)AfxGetMainWnd())->m_hWnd);
//		SetActiveWindow(((CMainFrame *)AfxGetMainWnd())->m_hWnd);
	}
/*	while(::WaitForSingleObject( ProcessInfo.hProcess, 100 ) != WAIT_OBJECT_0)
	{
		MSG msg;
		if(GetMessage(&msg, NULL, 0, 0))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
*/
//	::CloseHandle( ProcessInfo.hProcess );
//	::CloseHandle( ProcessInfo.hThread );

	return TRUE;
}


//When Module is connected 
//Write Module IP Address to SystemConfig DataBase
BOOL CCTSMonDoc::WriteModuleIPToDataBase(int nModuleID, LPSTR szIPAddress)
{
	if(szIPAddress == NULL || strlen(szIPAddress) <= 0)	return FALSE;
	
	CDaoDatabase  db;
	try
	{
		EP_MD_SYSTEM_DATA *pSysData = EPGetModuleSysData(EPGetModuleIndex(nModuleID));
		if(pSysData)
		{
			CString strTrayCh, strTemp;;
			for(int t=0; t<pSysData->wTotalTrayNo; t++)
			{
				strTemp.Format("%d,", pSysData->awChInTray[t]);
				strTrayCh += strTemp;
			}

  			db.Open(m_strDataBaseName);

			CString strSQL;
			if(((CCTSMonApp*)AfxGetApp())->GetSystemType() != 0)
			{
				//strSQL.Format("UPDATE SystemConfig SET IP_Address = '%s', InstallChCount = %d, Data2='%s' WHERE ModuleID = %d AND ModuleType = %d", 
				//ljb 2008-12 group number 추가
				strSQL.Format("UPDATE SystemConfig SET IP_Address = '%s', InstallChCount = %d, Data2='%s', Data5 = %d WHERE ModuleID = %d AND ModuleType = %d", 
								szIPAddress,  
								pSysData->nTotalChNo,
								strTrayCh,
								pSysData->nModuleGroupNo,
								nModuleID, 
								((CCTSMonApp*)AfxGetApp())->GetSystemType()
							);
			}
			else
			{
				//ljb 2008-12 group number 추가
				strSQL.Format("UPDATE SystemConfig SET IP_Address = '%s', InstallChCount = %d, Data2='%s', Data5 = %d WHERE ModuleID = %d", 
								szIPAddress, 
								pSysData->nTotalChNo,
								strTrayCh,
								pSysData->nModuleGroupNo,
								nModuleID
							);
			}
			db.Execute(strSQL);
			db.Close();
		}
	}
	catch(CDaoException *e)
	{
      // Simply show an error message to the user.
		m_strLastErrorString = e->m_pErrorInfo->m_strDescription;
		e->Delete();
		return FALSE;
	}
	return TRUE;
}

CString CCTSMonDoc::GetModuleIPAddress(int nModuleID)
{
	CString address;
	// 접속된 모듈 
	if(EPGetModuleState(nModuleID) != EP_STATE_LINE_OFF)
	{
		address = EPGetModuleIP(nModuleID);
	}
	if(!address.IsEmpty())	return address;
	
	//DataBase 검색 
	CDaoDatabase  db;
	try
	{
		db.Open(m_strDataBaseName);
		CString strSQL;
		if(((CCTSMonApp*)AfxGetApp())->GetSystemType() != 0)
			strSQL.Format("SELECT IP_Address FROM SystemConfig WHERE ModuleID = %d AND ModuleType = %d", nModuleID, ((CCTSMonApp*)AfxGetApp())->GetSystemType());
		else
			strSQL.Format("SELECT IP_Address FROM SystemConfig WHERE ModuleID = %d", nModuleID);

		CDaoRecordset rs(&db);
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
		if(!rs.IsBOF())
		{
			COleVariant data = rs.GetFieldValue(0);
			address = data.pbVal;
		}

		rs.Close();
		db.Close();
	}
	catch(CDaoException *e)
	{
      // Simply show an error message to the user.
		e->Delete();
	}

	//수동 입력 
	if(address.IsEmpty())
	{
		CIPSetDlg dlg;
		if(dlg.DoModal() == IDOK)
			address = dlg.GetIPAddress();
		
	}

	return address;	
}

void CCTSMonDoc::ShellExecute(CString strName, CString strOption)
{

}

bool CCTSMonDoc::GetModuleTroubeMsg(WORD code, CString &strCode, CString &strMsg )
{
	strCode ="None";

	CDaoDatabase  db;
	if(m_strDataBaseName.IsEmpty())		return false;
	db.Open(m_strDataBaseName);
	
	//////////////////////////////////////////////////////////////////////////
	//EMGCode Formation Emg Code Table
	bool bFind = false;
	CString strTemp;

	if( g_nLanguage == LANGUAGE_ENG )
	{
		strTemp.Format("SELECT Message, Description FROM EmgCode_ENG WHERE Code = %d", code);
	}
	else if( g_nLanguage == LANGUAGE_CHI )
	{
		strTemp.Format("SELECT Message, Description FROM EmgCode_CHI WHERE Code = %d", code);
	}
	else
	{
		strTemp.Format("SELECT Message, Description FROM EmgCode WHERE Code = %d", code);
	}
	
	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strTemp, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return false;
	}	
	
	if(!rs.IsBOF() && !rs.IsEOF())
	{
		bFind = true;
		data = rs.GetFieldValue(0);
		if(VT_NULL != data.vt)	strCode = data.pbVal;	
		data = rs.GetFieldValue(1);
		if(VT_NULL != data.vt)	strMsg = data.pbVal;
	}

	rs.Close();
	db.Close();
	
	if( bFind == false )
	{
		return false;
	}
	
	return true;
}

// 1. bAuto 가 true 일 경우 Group index 0 에 대한 결과데이터를 바로 보여준다.
void CCTSMonDoc::ViewResult(int nModule, int nTrayIndex, int nDataType)
{
	// TODO: Add your command handler code here
	CFormModule *pModule = GetModuleInfo(nModule);
	if(pModule == NULL)		return;	

	if( nTrayIndex < 0 )
	{
		nTrayIndex = pModule->GetFirstWorkingTrayNo()-1;

		CSelResultDlg dlg;
		dlg.m_pModuleInfo = pModule;
		dlg.m_nTrayIndex = nTrayIndex;
		if(dlg.DoModal() != IDOK)
		{
			return;
		}

		nTrayIndex = dlg.m_nTrayIndex;
	}
	
	if(nTrayIndex < 0)		return;

	//////////////////////////////////////////////////////////////////////////
	CString strData = _T("");

	strData = pModule->GetResultFileName(nTrayIndex);
	if( strData.IsEmpty() )
	{
		CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();
		pMainFrm->ShowInformation( nModule, TEXT_LANG[15], INFO_TYPE_WARNNING);//"결과데이터를 찾을 수 없습니다."
		return;
	}
	
	CString strProgramName;
	strProgramName.Format("%s\\CTSAnalyzer.exe", m_strCurFolder);
	
	strData = pModule->GetResultFileName(nTrayIndex);
	if( strData.IsEmpty() )
	{
		// AfxMessageBox("결과데이터를 찾을 수 없습니다.", MB_ICONINFORMATION);
		CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();
		pMainFrm->ShowInformation( nModule, TEXT_LANG[15], INFO_TYPE_WARNNING);//"결과데이터를 찾을 수 없습니다."
		return;
	}
	
	switch( nDataType )
	{
	case 0:
		{

		}
		break;
	case 1:
		{
			CString strTemp = _T("");
			strTemp = strData.Left( strData.ReverseFind('.'));
			strData = strTemp + "_CON.Fmt";
		}
		break;
	}
	
	if(ExecuteProgram(strProgramName, "", ANAL_CLASS_NAME, "", FALSE, TRUE))
	{
		//Message를 전송한다.
		HWND pWnd = ::FindWindow(ANAL_CLASS_NAME, NULL);
		if(pWnd)
		{
			int nSize = strData.GetLength()+1;
			char *pData = new char[nSize];
			ZeroMemory(pData, nSize);
			sprintf(pData, "%s", strData);
			pData[nSize-1] = '\0';

			COPYDATASTRUCT CpStructData;
			CpStructData.dwData = 2;		//CTS Program 구별 Index
			CpStructData.cbData = nSize;
			CpStructData.lpData = pData;
			
			::SendMessage(pWnd, WM_COPYDATA, 0, (LPARAM)&CpStructData);
			
			delete [] pData;
		}
	}
}

BOOL CCTSMonDoc::ExecuteProgram(CString strProgram, CString strArgument/* = ""*/, CString strClassName /*= ""*/, CString strWindowName/*= ""*/, BOOL bNewExe/* = FALSE*/, 	BOOL bWait/* = FALSE*/, BOOL bForground/* = TRUE*/)
{
	if(strProgram.IsEmpty())	return FALSE;

//	TCHAR szBuff[_MAX_PATH];
//	::GetModuleFileName(AfxGetApp()->m_hInstance,szBuff,_MAX_PATH);
//	CString path = CString(szBuff).Mid(0, CString(szBuff).ReverseFind('\\'));

	//새롭게 실행 하지 않으면 주어진 이름으로 검색한다.
	if(!bNewExe)
	{
		CWnd *pWndPrev = NULL, *pWndChild = NULL;

		if(!strWindowName.IsEmpty())
		{
			pWndPrev = CWnd::FindWindow(NULL, strWindowName);
		}

		if(!strClassName.IsEmpty())
		{
			pWndPrev = CWnd::FindWindow(strClassName, NULL);
		}

		// Determine if a window with the class name exists...
		if(pWndPrev != NULL)
		{
			// If so, does it have any popups?
			if(bForground)
			{
				pWndChild = pWndPrev->GetLastActivePopup();

				// If iconic, restore the main window.
				if (pWndPrev->IsIconic())
					pWndPrev->ShowWindow(SW_RESTORE);

				// Bring the main window or its popup to the foreground
				pWndChild->SetForegroundWindow();

			}
			// and you are done activating the other application.
			return TRUE;
		}
		/*
		HWND FirsthWnd = NULL;
		if(!strWindowName.IsEmpty())
		{
			FirsthWnd = ::FindWindow(NULL, strWindowName);
		}
		if(!strClassName.IsEmpty())
		{
			FirsthWnd = ::FindWindow(strClassName, NULL);
		}
		if (FirsthWnd)		//실행 되어 있으면 
		{
//			::SetActiveWindow(FirsthWnd);	
			::SetForegroundWindow(FirsthWnd);
			return TRUE;
		}
		//else	//실행된게 없으면 새롭게 실행 
		*/
	}

	CString strFullName;

	if(!strArgument.IsEmpty())
	{
		strFullName.Format("%s \"%s\"", strProgram, strArgument);
	}
	else
	{
		strFullName.Format("%s", strProgram);
	}

	STARTUPINFO	stStartUpInfo;
	PROCESS_INFORMATION	pi;
	ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
	ZeroMemory(&pi, sizeof(PROCESS_INFORMATION));
		
	stStartUpInfo.cb = sizeof(STARTUPINFO);
	stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
	stStartUpInfo.wShowWindow = SW_SHOWNORMAL;
	
	
	BOOL bFlag = ::CreateProcess(	NULL, 
									(LPSTR)(LPCTSTR)strFullName, 
									NULL, 
									NULL, 
									FALSE, 
									NORMAL_PRIORITY_CLASS, 
									NULL, 
									NULL, 
									&stStartUpInfo, 
									&pi
								);

	if(bFlag == FALSE)
	{		
//		AfxMessageBox(strFullName, MB_OK|MB_ICONSTOP);
		return FALSE;
	}
    
	if(bWait)
	{
		DWORD dwRet = WaitForInputIdle(pi.hProcess, 15000);
		if (WAIT_TIMEOUT != dwRet)  return TRUE;
	}

	CloseHandle( pi.hProcess );
	CloseHandle( pi.hThread );
	
	return TRUE;
}

BOOL CCTSMonDoc::ExecuteProgram1(CString strProgram, CString strArgument/* = ""*/, CString strClassName /*= ""*/, CString strWindowName/*= ""*/, BOOL bNewExe/* = FALSE*/, 	BOOL bWait/* = FALSE*/, BOOL bForground/* = TRUE*/)
{
	if(strProgram.IsEmpty())	
		return FALSE;

//	TCHAR szBuff[_MAX_PATH];
//	::GetModuleFileName(AfxGetApp()->m_hInstance,szBuff,_MAX_PATH);
//	CString path = CString(szBuff).Mid(0, CString(szBuff).ReverseFind('\\'));

	HWND	hWnd = ::FindWindow(NULL, strClassName);
	
	if( hWnd != NULL )
	{
		::SetForegroundWindow(hWnd);
		return true;	
	}

	//새롭게 실행 하지 않으면 주어진 이름으로 검색한다.
	if(!bNewExe)
	{
		CWnd *pWndPrev = NULL, *pWndChild = NULL;

		if(!strWindowName.IsEmpty())
		{
			pWndPrev = CWnd::FindWindow(NULL, strWindowName);
		}

		if(!strClassName.IsEmpty())
		{
			pWndPrev = CWnd::FindWindow(strClassName, NULL);
		}

		// Determine if a window with the class name exists...
		if(pWndPrev != NULL)
		{
			// If so, does it have any popups?
			if(bForground)
			{
				pWndChild = pWndPrev->GetLastActivePopup();

				// If iconic, restore the main window.
				if (pWndPrev->IsIconic())
					pWndPrev->ShowWindow(SW_RESTORE);

				// Bring the main window or its popup to the foreground
				pWndChild->SetForegroundWindow();

			}
			// and you are done activating the other application.
			return TRUE;
		}
		/*
		HWND FirsthWnd = NULL;
		if(!strWindowName.IsEmpty())
		{
			FirsthWnd = ::FindWindow(NULL, strWindowName);
		}
		if(!strClassName.IsEmpty())
		{
			FirsthWnd = ::FindWindow(strClassName, NULL);
		}
		if (FirsthWnd)		//실행 되어 있으면 
		{
//			::SetActiveWindow(FirsthWnd);	
			::SetForegroundWindow(FirsthWnd);
			return TRUE;
		}
		//else	//실행된게 없으면 새롭게 실행 
		*/
	}

	CString strFullName;
	if(!strArgument.IsEmpty())
	{
		strFullName.Format("%s \"%s\"", strProgram, strArgument);
	}
	else
	{
		strFullName.Format("%s", strProgram);
	}

	STARTUPINFO	stStartUpInfo;
	PROCESS_INFORMATION	pi;
	ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
	ZeroMemory(&pi, sizeof(PROCESS_INFORMATION));
	
	stStartUpInfo.cb = sizeof(STARTUPINFO);
	stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
	stStartUpInfo.wShowWindow = SW_SHOWNORMAL;

		
	BOOL bFlag = ::CreateProcess(	NULL, 
									(LPSTR)(LPCTSTR)strFullName, 
									NULL, 
									NULL, 
									FALSE, 
									NORMAL_PRIORITY_CLASS, 
									NULL, 
									NULL, 
									&stStartUpInfo, 
									&pi
								);
	if(bFlag == FALSE)
	{
		strFullName += TEXT_LANG[16];//"를 실행할 수 없습니다."
//		AfxMessageBox(strFullName, MB_OK|MB_ICONSTOP);
		return FALSE;
	}
    
	if(bWait)
	{
		DWORD dwRet = WaitForInputIdle(pi.hProcess, 15000);
		if (WAIT_TIMEOUT != dwRet)  return TRUE;
	}

	CloseHandle( pi.hProcess );
	CloseHandle( pi.hThread );
	
	return TRUE;
}

BOOL CCTSMonDoc::ExecuteEditor(CString strModel, CString strTest, BOOL bNewWnd)
{
	CString strTemp, strArgu;
/*	TCHAR szCurDir[MAX_PATH];
	::GetModuleFileName(AfxGetApp()->m_hInstance, szCurDir, MAX_PATH);
	strTemp = CString(szCurDir).Mid(0, CString(szCurDir).ReverseFind('\\')) + "\\CTSEditor.exe";
	strTemp.Format("%s\\CTSEditor.exe", CString(szCurDir).Mid(0, CString(szCurDir).ReverseFind('\\')));
*/
	
	strTemp.Format("%s\\CTSEditor.exe", m_strCurFolder);
	strArgu.Format("%s,%s", strModel,  strTest);

	if(ExecuteProgram(strTemp, strArgu, "", EDITOR_APP_NAME, bNewWnd, TRUE)== FALSE)	return FALSE;

	//Argument가 있으면 
	if(!strModel.IsEmpty())
	{
		HWND FirsthWnd;
		FirsthWnd = ::FindWindow(NULL, EDITOR_APP_NAME);
		if (FirsthWnd)
		{
			int nSize = strModel.GetLength()+strTest.GetLength()+2;
			char *pData = new char[nSize];
			ZeroMemory(pData, nSize);		
			sprintf(pData, "%s,%s", strModel, strTest);
			pData[nSize-1] = '\0';

			COPYDATASTRUCT CpStructData;
			CpStructData.dwData = 3;		//CTSMon Program 구별 Index
			CpStructData.cbData = nSize;
			CpStructData.lpData = pData;
			
			::SendMessage(FirsthWnd, WM_COPYDATA, 0, (LPARAM)&CpStructData);
			
			delete [] pData;
		}
	}
	return TRUE;
}


CFormModule * CCTSMonDoc::GetModuleInfo(int nModuleID)
{
	int nModuleIndex =  EPGetModuleIndex(nModuleID);
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)		return NULL;
	
	return (CFormModule * )m_apModuleInfo.GetAt(nModuleIndex);
}


//Added By KBH 2006/01/03
//접속이후 모듈이 Run 이거나 Pause 이면 이전 최종 시험 정보를 Loading 한다.
BOOL CCTSMonDoc::CheckAndLoadPrevTestInfo(int nModuleID, int nGroupIndex)
{
	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule == FALSE)	return FALSE;

	// 1. 모듈에 결과파일 정보가 없으면 
	// 2. 이전 결과를 표시하기 위해서 Temp 폴더에서 결과파일을 Loading 한다.
	// 3. 파일이 길면 오래 걸릴수 있슴
	CString strFile = pModule->GetResultFileName();
	
	strFile.Trim();
	
	if( strFile.IsEmpty() )
	{
		// 1. 정보가 없으면 이전 정보를 읽어온다.
		if(pModule->ReadTestLogFile() == FALSE)		//전송된 공정 Log 파일을 읽는다.
		{
			m_strLastErrorString.Format(GetStringTable(IDS_MSG_NOT_FOUND), ::GetModuleName(nModuleID, nGroupIndex));
			WriteLog(m_strLastErrorString);
			return FALSE;
		}
		
		//파일이 길며 오래 거릴수 있음 
		pModule->LoadResultData();
	}
	
	return TRUE;
}
/*
int CCTSMonDoc::GetStepDataSize(int nStepIndex, STR_CONDITION *pCondition)
{
	EP_STEP_HEADER * lpStepHeader;
	int nStepDataSize;
	nStepDataSize = pCondition->testHeader.totalStep;
	ASSERT(pCondition->testHeader.totalStep == pCondition->apStepList.GetSize());
	if(nStepDataSize <=0 )	return -1;
	if(nStepIndex < 0 || nStepIndex >= nStepDataSize)	return -1; 
		
	lpStepHeader = (EP_STEP_HEADER *)pCondition->apStepList[nStepIndex];
	ASSERT(lpStepHeader);
	
	switch(lpStepHeader->type)
	{
/*	case EP_TYPE_CHARGE	:		nStepDataSize = sizeof(EP_CHARGE_STEP);		break;
	case EP_TYPE_DISCHARGE:		nStepDataSize = sizeof(EP_DISCHARGE_STEP);	break;
	case EP_TYPE_IMPEDANCE	:	nStepDataSize = sizeof(EP_IMPEDANCE_STEP);	break;
	case EP_TYPE_OCV		:	nStepDataSize = sizeof(EP_OCV_STEP);		break;
	case EP_TYPE_REST		:	nStepDataSize = sizeof(EP_REST_STEP);		break;
	case EP_TYPE_END		:	nStepDataSize = sizeof(EP_END_STEP);		break;
	case EP_TYPE_LOOP:			nStepDataSize = sizeof(EP_LOOP_STEP);		break;
*/
/*	default:					nStepDataSize = 0;							break;
	}
	return nStepDataSize;
}

int CCTSMonDoc::GetGradeDataSize(int nStepIndex, STR_CONDITION *pCondition)
{
//	EP_GRADE_HEADER	*lpGradeHeader;
	int nDataSize = 0;
/*	nDataSize = pCondition->testHeader.totalGrade;
	ASSERT(pCondition->apGradeList.GetSize() == nDataSize);
	if(nDataSize <=0 )	return -1;
	if(nStepIndex < 0 || nStepIndex >= nDataSize)	return -1; 
	lpGradeHeader = (EP_GRADE_HEADER *)pCondition->apGradeList[nStepIndex];
	ASSERT(lpGradeHeader);

	nDataSize = lpGradeHeader->totalStep*sizeof(EP_GRADE) + sizeof(EP_GRADE_HEADER);
	return nDataSize;
}
*/

void CCTSMonDoc::UpdateUnitSetting()
{
	char szBuff[32], szUnit[16], szDecimalPoint[16];

	sprintf(szBuff, AfxGetApp()->GetProfileString(FORMSET_REG_SECTION, "V Unit", "V 0"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strVUnit = szUnit;
	m_nVDecimal = atoi(szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(FORMSET_REG_SECTION, "I Unit", "mA 0"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strIUnit = szUnit;
	m_nIDecimal = atoi(szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(FORMSET_REG_SECTION, "C Unit", "mAh 0"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strCUnit = szUnit;
	m_nCDecimal = atoi(szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(FORMSET_REG_SECTION, "Time Unit", "0"));
	m_nTimeUnit = atol(szBuff);

	if(m_strCUnit == "Ah" || m_strCUnit == "F")
	{
		m_strWUnit = "W";
		m_strWhUnit = "Wh";
	}
	else
	{
		m_strWUnit = "mW";
		m_strWhUnit = "mWh";
	}
}
/*
BOOL CCTSMonDoc::DownLoadProfileData(int nModuleID, BOOL bAuto)
{
	CString strProgramName;
	strProgramName.Format("%s\\DataDown.exe", m_strCurFolder);
	if(ExecuteProgram(strProgramName, "", "", DATA_DOWN_TITLE_NAME, FALSE, TRUE))
	{
		//Message를 전송한다.
		HWND pWnd = ::FindWindow(NULL, DATA_DOWN_TITLE_NAME);
		if(pWnd)
		{
			CFormModule *pModule = GetModuleInfo(nModuleID);
			if(pModule == NULL)		return FALSE;

			for(int i=0; i<pModule->GetTotalJig(); i++)
			{
				if(pModule->GetTrayInfo(i)->IsWorkingTray())
				{
					CString strData;
					strData = GetResultFileName(nModuleID, i);
					if(strData.IsEmpty() && bAuto)		//자동일 경우는 파일명이 반드시 있어야 함(수동일 경우는 있어도 되고 없어도됨)
					{
						return FALSE;
					}
					int nSize = strData.GetLength()+1;
					char *pData = new char[nSize];
					ZeroMemory(pData, nSize);
					sprintf(pData, "%s", strData);
					pData[nSize-1] = '\0';

					COPYDATASTRUCT CpStructData;
					if(bAuto)
					{
						CpStructData.dwData = 2;		//자동 DownLoad
					}
					else
					{
						CpStructData.dwData = 3;		//사용자 파일 선택 모드 
					}
					
					CpStructData.cbData = nSize;
					CpStructData.lpData = pData;
					::SendMessage(pWnd, WM_COPYDATA, 0, (LPARAM)&CpStructData);
					delete [] pData;
				}
						
			}
		}
		else
		{
			return FALSE;
		}
	}
	else
	{
		return FALSE;
	}
	return TRUE;
}
*/
/*
BOOL CCTSMonDoc::DownLoadProfileData(CString strFileName, CWordArray &awSelCh)
{
	CString strProgramName;
	strProgramName.Format("%s\\DataDown.exe", m_strCurFolder);
	if(ExecuteProgram(strProgramName, "", "", DATA_DOWN_TITLE_NAME, FALSE, TRUE))
	{
		//Message를 전송한다.
		HWND pWnd = ::FindWindow(NULL, DATA_DOWN_TITLE_NAME);
		if(pWnd)
		{
			CString strData;
			strData = strFileName;
			if(strData.IsEmpty() || awSelCh.GetSize() <= 0)		
			{
				return FALSE;
			}
			int nSize = strData.GetLength()+1;
			char *pData = new char[nSize];
			ZeroMemory(pData, nSize);
			sprintf(pData, "%s", strData);
			pData[nSize-1] = '\0';

			COPYDATASTRUCT CpStructData;
			CpStructData.dwData = 2;			
			CpStructData.cbData = nSize;
			CpStructData.lpData = pData;
			::SendMessage(pWnd, WM_COPYDATA, 0, (LPARAM)&CpStructData);
			
			delete [] pData;
		}
		else
		{
			return FALSE;
		}
	}
	else
	{
		return FALSE;
	}
	return TRUE;
}
*/

BOOL CCTSMonDoc::DownLoadProfileData(int nModule, CString strFileName, BOOL bAuto)
{
	CString strProgramName;
	strProgramName.Format("%s\\DataDown.exe", m_strCurFolder);	
	
	if(ExecuteProgram(strProgramName, "", "", DATA_DOWN_TITLE_NAME, FALSE, TRUE))
	{
		// Registry DataDown Message 추가 // 0 : file, 1 : message 
		if(AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "DataDown Message", 0) || bAuto == FALSE) // Data Down 방식을 메시지로 한다면. KHM
		{
			//Message를 전송한다.
			HWND pWnd = ::FindWindow(NULL, DATA_DOWN_TITLE_NAME);
			if(pWnd)
			{
				CString strData;
				strData = strFileName;
				if(strData.IsEmpty() && bAuto)		//자동일 경우는 파일명이 반드시 있어야 함(수동일 경우는 있어도 되고 없어도됨)
				{
					return FALSE;
				}
				int nSize = strData.GetLength()+1;
				char *pData = new char[nSize];
				ZeroMemory(pData, nSize);
				sprintf(pData, "%s", strData);
				pData[nSize-1] = '\0';

				COPYDATASTRUCT CpStructData;
				if(bAuto == FALSE)
				{
					CpStructData.dwData = 2;		//자동 DownLoad && Execute GraphAnalyzer
				}
				else
				{
					CpStructData.dwData = 3;		//완료 후 자동 Download
				}
				
				CpStructData.cbData = nSize;
				CpStructData.lpData = pData;
				::SendMessage(pWnd, WM_COPYDATA, 0, (LPARAM)&CpStructData);
				
				delete [] pData;
			}
			else
			{
				return FALSE;
			}
		}
		else  // Data Down 방식을 메시지가 아닌 File로 한다면. KHM
		{			
			// COleDateTime curTime = COleDateTime::GetCurrentTime()	;	//완료 시각 
			CTime cT=CTime::GetCurrentTime();
			CString tmpStr;			
			
			tmpStr.Format("%s", cT.Format("%H%M%S")); // 파일명에 쓸 현재 시각 
			DWORD fileNameNo = atol(tmpStr);
			
			tmpStr.Format("%s", cT.Format("%Y%m%d")); // 파일명에 쓸 현재 시각 
			
			//Profilelist 폴더 생성
			CString strTemp;
			strTemp.Format("%s\\Profilelist", m_strCurFolder); // 파일 명
			CFileFind ff;
			if(ff.FindFile(strTemp))
			{
				ff.FindNextFile();
				if(ff.IsDirectory()== FALSE)
				{
					CreateDirectory(strTemp, NULL);
				}
			}
			else
			{
				CreateDirectory(strTemp, NULL);
			}
			ff.Close();

			for (int i = 0 ; i < 100; i++)
			{			
				strTemp.Format("%s\\Profilelist\\%s%06d.pfl", m_strCurFolder, tmpStr, fileNameNo); // 파일 명
				if(ff.FindFile(strTemp) == FALSE)
				{
					break;
				}
				else
				{
					fileNameNo++;
				}
			}
			
			CFormModule *pModule = GetModuleInfo(nModule);
			if(pModule != NULL)
			{
				strTemp.Format("%s\\Profilelist\\%s%06d.pfl", m_strCurFolder, tmpStr, fileNameNo); // 파일 명
				CStdioFile DataDownListFile;  // 파일이 사용중이면 
				if(DataDownListFile.Open(strTemp, CFile::modeWrite | CFile::modeNoTruncate | CFile::modeCreate))
				{
					//online
					if(pModule->GetLineMode() == EP_ONLINE_MODE)
					{
						strTemp.Format("[Result] = ""\n");
						DataDownListFile.WriteString((LPCTSTR)strTemp);   // <-- 이곳에 파일 내용을 넣자. 넣기 위한 내용은 위에서 만들자.
						strTemp.Format("[SaveDir] = %s\n", strFileName);
						DataDownListFile.WriteString((LPCTSTR)strTemp);
					}
					else//offline
					{				
						strTemp.Format("[Result] = %s\n",strFileName);
						DataDownListFile.WriteString((LPCTSTR)strTemp);   // <-- 이곳에 파일 내용을 넣자. 넣기 위한 내용은 위에서 만들자.
						strTemp.Format("[SaveDir] = %s\n", strFileName.Left(strFileName.ReverseFind('.')));
						DataDownListFile.WriteString((LPCTSTR)strTemp);
					}
					strTemp.Format("[TrayID] = %s\n", pModule->GetTrayNo());
					DataDownListFile.WriteString((LPCTSTR)strTemp);
					strTemp.Format("[UNIT_ID] = %d\n", pModule->GetModuleID());
					DataDownListFile.WriteString((LPCTSTR)strTemp);
					strTemp.Format("[UNIT_IP] = %s\n", pModule->GetIPAddress());
					DataDownListFile.WriteString((LPCTSTR)strTemp);					
					strTemp.Format("[UNIT_NAME] = %s\n", pModule->GetModuleName());
					DataDownListFile.WriteString((LPCTSTR)strTemp);				
					DataDownListFile.Close();
				}
				else
				{
					strTemp.Format("%s profile list file create fail!!!", strFileName);
					WriteLog(strTemp);
					return FALSE;
				}
			}
		}
	}
	else
	{
		return FALSE;
	}
	return TRUE;
}

BOOL CCTSMonDoc::SendChangeTrayTypeCmd(int nModuleID, int nGroupIndex,int nTrayType)
{
	int nRtn = EP_ACK;
	
	WORD state = EPGetGroupState(nModuleID, nGroupIndex);
	BYTE byTemp;
	if( state != EP_STATE_IDLE)// || state != EP_STATE_STANDBY)
	{
		m_strLastErrorString.Format(TEXT_LANG[17], ::GetModuleName(nModuleID, nGroupIndex), GetStateMsg(state, byTemp));//"%s :: [형 변환] 명령 전송가능 상태가 아닙니다.(state : %d)"
		WriteLog(m_strLastErrorString);
		return FALSE;
	}
	switch (nTrayType)
	{
	case EP_CHANGE_TRAY_MODE0:
		nRtn = EPSendCommand(nModuleID, nGroupIndex+1, 0, EP_CMD_CHANGE_MODULE0);
		break;
	case EP_CHANGE_TRAY_MODE1:
		nRtn = EPSendCommand(nModuleID, nGroupIndex+1, 0, EP_CMD_CHANGE_MODULE1);
		break;
	default :
		nRtn = EPSendCommand(nModuleID, nGroupIndex+1, 0, EP_CMD_CHANGE_MODULE0);
	}
	if(nRtn != EP_ACK)
	{
		m_strLastErrorString.Format("%s (%s-%s)",  ::GetStringTable(IDS_TEXT_CMD_SEND_FAIL), ::GetModuleName(nModuleID, nGroupIndex), CmdFailMsg(nRtn));		
		WriteLog((LPSTR)(LPCTSTR)m_strLastErrorString);
	}

	// EPSendCommand(nModuleID);
	
// 	//2006/02/07 DownData에 실시간 저장파일을 자동 DownLoad하도록 Message 전송 
// 	//사용자 Step에대한 정보 저장 필요 
// 	if(AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "DataSave", 0))
// 	{
// 		if(DownLoadProfileData(nModuleID, TRUE) == FALSE)
// 		{
// 			m_strLastErrorString.Format("%s real time data down load fail!!", ::GetModuleName(nModuleID, nGroupIndex));
// 			WriteLog(m_strLastErrorString);
// 		}
// 	}
	return nRtn;
}

BOOL CCTSMonDoc::SendStopCmd(int nModuleID, int nGroupIndex)
{
	int nRtn = EP_ACK;

	WORD state = EPGetGroupState(nModuleID, nGroupIndex);
	BYTE byTemp;
	if(state != EP_STATE_RUN && state != EP_STATE_PAUSE)
	{
		m_strLastErrorString.Format(TEXT_LANG[18], ::GetModuleName(nModuleID, nGroupIndex), GetStateMsg(state, byTemp));//"%s :: [작업 중지] 명령 전송가능 상태가 아닙니다.(state : %s)"
		WriteLog(m_strLastErrorString);
		return FALSE;
	}

	if(( nRtn = EPSendCommand(nModuleID, nGroupIndex+1, 0, EP_CMD_STOP)) != EP_ACK)
	{
		m_strLastErrorString.Format("%s (%s-%s)",  ::GetStringTable(IDS_TEXT_CMD_SEND_FAIL), ::GetModuleName(nModuleID, nGroupIndex), CmdFailMsg(nRtn));		
		WriteLog((LPSTR)(LPCTSTR)m_strLastErrorString);
	}	
	
	m_fmst.fnClearReset(nModuleID);	

	//Cell Check이후 Pause 상태에서 Stop을 전송하면 Profile을 받을 필요 없다. 
	//결과 파일이 존재하지 않으므로 저장되지 않음
	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule)
	{
		pModule->GetTrayInfo(0)->m_nResultFileType = EP_RESULTFILE_TYPE_NORMAL;	
		
		pModule->UpdateTestLogHeaderTempFile();
		
		pModule->GetTrayInfo(0)->m_nResultFileType = EP_RESULTFILE_TYPE_STP;		

		fnWriteTemperature( nModuleID, TRUE );
		
		//2006/02/07 DownData에 실시간 저장파일을 자동 DownLoad하도록 Message 전송 
		//사용자 Step에대한 정보 저장 필요 
		if(AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "DataSave", 0))
		{
			for(int t=0; t<pModule->GetTotalJig(); t++)
			{
				CTray *pTrayInfo = pModule->GetTrayInfo(t);
				if(pTrayInfo->IsWorkingTray())
				{
					if(DownLoadProfileData(nModuleID, pTrayInfo->m_strFileName, false) == FALSE)
					{
						m_strLastErrorString.Format("%s real time data down load fail!!", ::GetModuleName(nModuleID, nGroupIndex));
						WriteLog(m_strLastErrorString);
					}
				}
			}
		}
	}

	return nRtn;
}

// -----------------------------------------------------------------------------
//  [5/1/2009 kky]
// 파일저장 폴더 생성 부분
// -----------------------------------------------------------------------------
CString CCTSMonDoc::CreateResultFilePath(int nModuleID, CString strDataFolder, CString strLot, CString strTray)
{
	//모듈별로 분리해서 저장 	
	CFileFind aDirChecker;
	CString strTempFolderName(strDataFolder);

	//1. Lot 구별 폴더 
	if(m_bFolderLot && !strLot.IsEmpty())
	{
		strTempFolderName =  strTempFolderName + "\\" + strLot;
		if(aDirChecker.FindFile(strTempFolderName) == FALSE)
		{
			if(::CreateDirectory(strTempFolderName, NULL) == FALSE)
			{
				return "";
			}
		}
		aDirChecker.Close();
	}

	//2. Tray별 구별 폴더 
	if(m_bFolderTray && !strTray.IsEmpty())
	{
		strTempFolderName =  strTempFolderName + "\\" + strTray;
		if(aDirChecker.FindFile(strTempFolderName) == FALSE)
		{
			if(::CreateDirectory(strTempFolderName, NULL) == FALSE)
			{
				return "";
			}
		}
		aDirChecker.Close();
	}

	//3. 폴더 구별
	if(m_bFolderModuleName)
	{
		strTempFolderName =  strTempFolderName + "\\" + ::GetModuleName(nModuleID);
		if(aDirChecker.FindFile(strTempFolderName) == FALSE)
		{
			if(::CreateDirectory(strTempFolderName, NULL) == FALSE)
			{
				return "";
			}
		}
		aDirChecker.Close();
	}
	
	//4. 시간 별로 폴더 분리
	if(m_bFolderTime)
	{
		COleDateTime oleTime = COleDateTime::GetCurrentTime();
		if(m_nTimeFolderInterval <= 1)			//1일 단위 생성 
		{
			strTempFolderName += oleTime.Format("\\%Y%m%d");
		}
		else if(m_nTimeFolderInterval <= 10)	//10일 단위 생성
		{
			if(oleTime.GetDay() <= 10)
			{
				strTempFolderName += oleTime.Format("\\%Y%m01");
			}
			else if(oleTime.GetDay() <= 20)
			{
				strTempFolderName += oleTime.Format("\\%Y%m11");
			}
			else
			{
				strTempFolderName += oleTime.Format("\\%Y%m21");				
			}
		}
		else if(m_nTimeFolderInterval <= 20)		//15일 간격 생성 
		{
			if(oleTime.GetDay() <= 15)
			{
				strTempFolderName += oleTime.Format("\\%Y%m01");
			}
			else
			{
				strTempFolderName += oleTime.Format("\\%Y%m16");				
			}

		}
		else
		{
			strTempFolderName += oleTime.Format("\\%Y%m");				
		}

		if(aDirChecker.FindFile(strTempFolderName) == FALSE)
		{
			if(::CreateDirectory(strTempFolderName, NULL) == FALSE)
			{
				return "";
			}
		}
		aDirChecker.Close();
	}

	return strTempFolderName;
}

BOOL CCTSMonDoc::CheckFileNameValidate(CString strFileName, CString &strNewFileName, int nModuleID, int nGroupIndex )
{
	//다른 Module에서 사용중인 파일이지 검사
	CString strTemp;
	int ID;
	for(int i =0; i<GetInstalledModuleNum(); i++)
	{
		ID = EPGetModuleID(i);
		for(int j=0; j<EPGetGroupCount(ID); j++)
		{
			if(nModuleID  != ID || nGroupIndex != j)	//자기 자신은 제외
			{
				if(GetResultFileName(ID, j) == strFileName && !strFileName.IsEmpty())
				{
					m_strLastErrorString.Format(GetStringTable(IDS_MSG_FILE_NAME_USED), strFileName, ::GetModuleName(ID, j));
					AfxMessageBox(m_strLastErrorString);
					return FALSE;
				}
			}
		}
	}
	
	//파일이 중복되는지 검사 
	CFileFind fileFinder;
	if(fileFinder.FindFile(strFileName))			//Find Board Test Result File
	{
		if(AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "NoOverWrite", FALSE))
		{
			//Over write 방지 (for LSC)
			strTemp.Format(TEXT_LANG[19], strFileName);//"%s는 중복된 이름입니다. 다른 이름을 입력하거나 이전 결과를 삭제 후 시작하십시요."
			AfxMessageBox(strTemp, MB_ICONSTOP|MB_OK);
			return FALSE;
		}
		else
		{
			//사용자에게 Overwite여부를 물음
			strTemp.Format(GetStringTable(IDS_MSG_FILE_OVER_WIRTE_CONFIRM), strFileName);
			if(AfxMessageBox(strTemp, MB_ICONQUESTION|MB_YESNO) == IDNO)
			{
				// 1. 결과파일이 있을경우 파일 이름을 변경해서 저장한다.
				CString strTemp = _T("");
				CString strNewFileName = _T("");

				AfxExtractSubString(strTemp, strFileName, 1,'.');

				if( strTemp == "fmt" )
				{					
					COleDateTime curTime = COleDateTime::GetCurrentTime();
					AfxExtractSubString(strTemp, strFileName, 0,'.');					
					strFileName.Format("%s_%s", strTemp, curTime.Format("%H%M%S"));					
				}
				else
				{
					return false;
				}
			}
			else
			{
				char szFrom[256], szTo[256];
				ZeroMemory(szFrom, 256);	//Double NULL Terminate
				ZeroMemory(szTo, 256);		//Double NULL Terminate
					
				//pFrom에 Full Path 제공하고 
				//pTo에 Path명을 제공하지 않고 
				//fFlags Option에 FOF_ALLOWUNDO 부여하고 
				//Delete 동작 하면 휴지통으로 삭제 된다.

				sprintf(szFrom, "%s", strFileName);
				LPSHFILEOPSTRUCT lpFileOp = new SHFILEOPSTRUCT;
				ZeroMemory(lpFileOp, sizeof(SHFILEOPSTRUCT));
				lpFileOp->hwnd = NULL; 
				lpFileOp->wFunc = FO_DELETE ; 
				lpFileOp->pFrom = szFrom; 
				lpFileOp->pTo = szTo; 
				lpFileOp->fFlags = FOF_FILESONLY|FOF_NOCONFIRMATION|FOF_ALLOWUNDO ;//FOF_NOCONFIRMATION | FOF_NOERRORUI | FOF_FILESONLY ; 
				lpFileOp->fAnyOperationsAborted = FALSE; 
				lpFileOp->hNameMappings = NULL; 
				lpFileOp->lpszProgressTitle = NULL; 

				if(SHFileOperation(lpFileOp) != 0)		//Move File
				{
					m_strLastErrorString.Format("%s File delete Fail..", strFileName);
					WriteLog((LPSTR)(LPCTSTR)m_strLastErrorString);
					delete	lpFileOp;
					lpFileOp = NULL;
					return FALSE;
				}			
				delete	lpFileOp;
				lpFileOp = NULL;
			}
		}		
	}

	strNewFileName = strFileName;
	//파일명으로 유효 여부 검사 
	return FileNameCheck((LPSTR)(LPCTSTR)strNewFileName);
}

//Tray Sensor와 Tray유무 판정
//같은 번호의 Tray가 존재하는지 검사 
BOOL CCTSMonDoc::TrayMatchingCheck(int nModuleID)
{
	BOOL bLoaded;
	CString strData, strTemp;
	CTray *pTray;
	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule == NULL)		return FALSE;

	for(int t=0; t<pModule->GetTotalJig(); t++)
	{
		pTray = pModule->GetTrayInfo(t);
		if(pTray)
		{
			strData = pTray->GetTrayNo();
			bLoaded = EPTrayState(nModuleID, t);
			//BCR이 Scan 되었는지 여부 
			if(!pTray->GetBcrRead() && bLoaded == EP_TRAY_LOAD)
			{
				//BCR이 읽혀지지 않은 경우 
				m_strLastErrorString.Format(TEXT_LANG[20], //"%s의 Jig%d 번에 Tray가 Loading되었으나 TrayNo가 입력되지 않았습니다.\n\n작업에 포함할 경우 Tray번호를 입력하고, 제외할 경우 Tray를 배출한 후 재시도해 주십시요."
								::GetModuleName(nModuleID), t+1);
				return FALSE;
			}
						
			if(pTray->GetBcrRead() && bLoaded != EP_TRAY_LOAD)
			{
				//Tray가 Loading되지 않은 경우 
				strTemp.Format(TEXT_LANG[21], //"%s의 Jig%d 번에 TrayNo [%s]가 입력되었는데 Tray가 Loading되지 않았습니다.\n\nTray [%s]번호 입력을 취소 하시겠습니까?"
								::GetModuleName(nModuleID), t+1, strData, strData);
				if(AfxMessageBox(strTemp, MB_YESNO|MB_ICONQUESTION) ==IDYES)
				{
					pTray->SetBcrRead(FALSE);
					pTray->SetTrayNo("");
					pTray->lInputCellCount = 0;
					SendTrayReady(nModuleID, t+1, FALSE);	//BCR Lamp off
					
					//취소하고 Scan된 Tray가 1개도 없으면 실행 못함 
					if(pModule->GetBCRState() == FALSE)
					{
						m_strLastErrorString.Format(TEXT_LANG[22], ::GetModuleName(nModuleID));//"%s에 입련된 Tray번호가 존재하지 않아 진행할 수 없습니다."
						return FALSE;
					}
				}
				else
				{
					//Tray를 삽입해야함 
					m_strLastErrorString.Format(TEXT_LANG[23], //"%s의 Jig%d 번에 Tray 번호 [%s]를 투입한 후 다시 시작하십시요."
									::GetModuleName(nModuleID), t+1, strData);
					return FALSE;	
				}
			}
		}
	}
	return TRUE;
	//////////////////////////////////////////////////////////////////////////
}

DWORD CCTSMonDoc::CheckEnableRun(int nModuleID)
{
	CString strTemp;
	DWORD bStateFlag = 0;
	
	//TrayLoad Check & Crate file 
	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule == NULL)		return 0;

	CTray *pTrayInfo;
	for(int t =0; t<pModule->GetTotalJig(); t++)
	{
		//준비된 Tray
		pTrayInfo = pModule->GetTrayInfo(t);
		if(pTrayInfo->IsWorkingTray())
		{
			//Tray 감지 Sensor Check;
			if(EPTrayState(nModuleID, t) == EP_TRAY_UNLOAD)
			{
				m_strLastErrorString.Format(TEXT_LANG[24], //"%s의 Jig %d번에 Tray[%s]가 감지되지 않아 시작할 수 없습니다."
											::GetModuleName(nModuleID), t+1, pTrayInfo->GetTrayNo());
				return 0;
			}
			else
			{

			}

			//Create result file
			//20060523
			//기존에 최소 1개의 step이 종료되면 결과 파일이 생성되던 방식을 시험 시작시 결과 파일을 생성하도록 수정
			//실시간 DataDown시 Step1번의 진행 data 경과를 보기 위해  			
			if(pModule->CreateTrayResultFile(t) == FALSE)
			{
				m_strLastErrorString.Format(pModule->GetResultFileName()+" file create fail!!!\n");
				WriteLog(m_strLastErrorString);
				return 0;
			}

			
			// void CMainFrame::OnSaveDataReceive(WPARAM wParam, LPARAM lParam) 에서 현재 위치로 변경 //2007/02/13
			// DataBase 구조를 파일 구조로 바꾸면서 Step1이 완료되기 이전에도 Step진행 이력을 조회할 수 있도록
			// Step1이 완료는 시점에서 보고하던 부분을 시작과 동시에 보고 하도록 수정(중복시 자동 Overwrite됨)
			// 1. DataServer 로 공정 시작 정보를 보내는 부분
			SendProcedureLogToDataBase(nModuleID, t);

			bStateFlag |= (0x01<<t);
		}
	}	
	
	if(bStateFlag == 0)
	{
		m_strLastErrorString.Format(TEXT_LANG[25],	::GetModuleName(nModuleID));//"%s에 작업가능한 Tray번호나 수량이 입력되지 않았습니다.\n작업할 Tray를 삽입한 후 Tray번호와 수량을 정확히 입력 후 재시도 하십시요."
	}
	return bStateFlag;
}

//진입된 Tray가 모두 같은 공정을 진행하였으면 다음 공정을 찾는다.
int CCTSMonDoc::CheckSameProcAndFindNextProc(int nModuleID, int nJigNo, CString strTrayNo)
{
	//Scan된 Tray가 모두 같은 작업 종류의 Tray인지 검사
	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule == FALSE)		return -1;

	STR_CONDITION_HEADER testHeader;
	BOOL bSameProc = TRUE;
	long bProcID = 0;
	int nPrevIndex = 0;
	CTray *pTray;
	CString strTemp;

	//포함해서 검사한다.
	int nCnt = 0;
	CString strLot;
	CTray *pTmpTray = new CTray;
	if(strTrayNo.IsEmpty() == FALSE)
	{
		if(pTmpTray->LoadTrayData(strTrayNo))
		{
			nCnt = 1;
			testHeader = GetNextTest(pTmpTray->GetProcessSeqNo());
			bProcID = testHeader.lID;
			strLot = pTmpTray->strLotNo;
		}	//시험조건에서 삭제후 새로 만들면 다른 공정으로 인식한다.
		else
		{
			m_strLastErrorString.Format(TEXT_LANG[26], strTrayNo);//"%s는 등록된 Tray가 아닙니다. 먼저 등록 하십시요."
			delete pTmpTray;
			return -1;
		}
	}

	for(int t=0; t<pModule->GetTotalJig(); t++)
	{
		pTray = pModule->GetTrayInfo(t);
		if(pTray && pTray->GetBcrRead())
		{
			pTmpTray->LoadTrayData(pTray->GetTrayNo());			
			testHeader = GetNextTest(pTmpTray->GetProcessSeqNo());
			if(nCnt == 0)	
			{	
				bProcID = testHeader.lID;
				strLot = pTmpTray->strLotNo;
			}	//시험조건에서 삭제후 새로 만들면 다른 공정으로 인식한다.

			//자기 자신 Tray인 경우는 Update하도록 한다.
			if(bProcID != testHeader.lID && nJigNo > 0 && nJigNo != t+1 )
			{
				bSameProc = FALSE;
				
				//다른 종류의 공정 Tray가 있다. 
				CTray *pDataTray = new CTray;
				if(strTrayNo.IsEmpty() == FALSE)
				{
					pDataTray->LoadTrayData(strTrayNo);
				}
				else
				{
					pDataTray->LoadTrayData(pModule->GetTrayInfo(nPrevIndex)->GetTrayNo());
				}

				//???? 새로운 Tray를 이전 공정을 모두 생략하고 지금 공정부터 진입하게 할지 여부 
				//2006/11/8: 진입 시킬수 없도록 Setting
				STR_CONDITION_HEADER prevHeader = GetNextTest(pDataTray->GetProcessSeqNo());
				CString prevTestName(prevHeader.szName);
				CString testName(testHeader.szName);
				if(prevTestName.IsEmpty())		prevTestName = TEXT_LANG[39];
				if(testName.IsEmpty())			testName = TEXT_LANG[39];

				m_strLastErrorString.Format(TEXT_LANG[27], ::GetModuleName(nModuleID), pDataTray->GetTrayNo(),	prevTestName, pTmpTray->GetTrayNo(), testName);

				delete pDataTray;
				delete pTmpTray;
				return -1;
			}

			nPrevIndex = t;
			nCnt++;
		}
	}
	
	delete pTmpTray;
	
	if(bSameProc)
	{		
		return 	bProcID;
	}

	return -1;
}

//공정이 예약되어 있을 경우 진입한 Tray가 시행 가능한지 여부 
BOOL CCTSMonDoc::IsExeReservedTestTray(int nModuleID)
{
	//Scan된 Tray가 모두 같은 작업 종류의 Tray인지 검사
	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule == FALSE)		return FALSE;

	//공정이 예약되어 있어야 함 
	if(pModule->GetTestReserved() == FALSE)		return FALSE;

	STR_CONDITION_HEADER testHeader;
	BOOL bSameProc = TRUE;

	CTray *pTray;
	CTray trayInfo;
	for(int t=0; t<pModule->GetTotalJig(); t++)
	{
		pTray = pModule->GetTrayInfo(t);		//새롭게 예약된 Tray정보 
		if(pTray && pTray->IsWorkingTray())
		{
			if(trayInfo.LoadTrayData(pTray->GetTrayNo()))
			{
				//예약 공정이 최초 공정이면 
				if(pModule->m_ReservedtestInfo.lNo == 1)
				{
					//진입한 Tray는 예약된 공정이 없어야 한다.
					if(CheckLastProcess(&trayInfo)==FALSE)
					{
						bSameProc = FALSE;
					}
				}
				else	//진행중인 공정일 경우 
				{
					testHeader = GetNextTest(trayInfo.lTestKey);	//다음공정 검색
					//다음공정과 정확히 Matching
					if(testHeader.lID != pModule->m_ReservedtestInfo.lID)
					{
						bSameProc = FALSE;
					}
				}
			}
			else
			{
				m_strLastErrorString.Format(TEXT_LANG[28], pTray->GetTrayNo());//"Tray [%s]의 정보를 Loading할 수 없습니다."
				AfxMessageBox(m_strLastErrorString);
				return FALSE;
			}
		}
	}
	return bSameProc;
}

//BCR이 중복된것이 입력되었는지 검사 
int CCTSMonDoc::FindJigNoTrayNo(int nModuleID, CString strID)
{
	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule == FALSE)	return FALSE;

	CTray *pTrayInfo;
	for(int t =0; t<pModule->GetTotalJig(); t++)
	{
		pTrayInfo = pModule->GetTrayInfo(t);
		if(pTrayInfo->GetBcrRead())
		{
			if(strID == pTrayInfo->GetTrayNo())
			{
				return t+1;
			}
		}
	}
	return 0;
}

//입력 지그가 Stopper가 맞게 설정되어 있는지 검사 
BOOL CCTSMonDoc::CheckStopperSensor(int nModuleID, int nJigNo)
{
	if(AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "CheckStopper", FALSE) == FALSE)		return TRUE;
	
	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule == FALSE)	return FALSE;

	//모든 Jig 검사 
	if(nJigNo == 0)
	{
		for(int i=0; i<pModule->GetTotalJig(); i++)
		{
			if(CheckStopperSensor(nModuleID, i+1) == FALSE)
			{
				return FALSE;
			}
		}
		return TRUE;
	}

	//Get Module Stopper Height setting 
	WORD wStopper= EPStopperState(nModuleID, nJigNo-1);

	//1. 해당 Jig가 예약된 모델 정보와 맞게 Setting되었는지 검사 
	long lHeight = _STOPPER_HEIGHT_0;
	if(wStopper != EP_OFF)
	{
		lHeight = _STOPPER_HEIGHT_1;
	}

	int nRtn = TRUE;
//	if(pModule->GetTestReserved())
//	{
		CString strSQL;
		//모델에 맞는 Stopper 높이 및 Barcode 체계 확인 
		strSQL.Format("SELECT a.Name, a.Width, a.Height, a.BCRMask FROM ModelInfo a, BatteryModel b, TestName c WHERE c.TestID=%d AND c.ModelID=b.ModelID AND b.Description=a.Name",
						pModule->GetCondition()->GetTestInfo()->lID);// pModule->m_ReservedtestInfo.lID);
		
		//strSQL.Format("SELECT a.Name, a.Width, a.Height, a.BCRMask FROM ModelInfo AS a WHERE Name = '45mm'");
		CDaoDatabase  db;
		COleVariant data;
		try
		{
			db.Open(::GetDataBaseName());
		
			CDaoRecordset rs(&db);
			rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
			if(rs.IsBOF() == FALSE)
			{
				//
				rs.GetFieldValue("Name", data);
				if(VT_NULL != data.vt)
				{
					
				}

				//
				rs.GetFieldValue("Width", data);

				//
				rs.GetFieldValue("Height", data);
				if((long)data.fltVal != lHeight && (long)data.fltVal != 0)		//높이 설정이 잘못되었음 
				{
					m_strLastErrorString.Format(TEXT_LANG[29], //"%s Jig %d의 높이설정이 진행할 모델에 맞게 설정되지 않았습니다. [Jig 설정 %dmm, 작업모델: %dmm]"
												GetModuleName(nModuleID), nJigNo, lHeight, long(data.fltVal));
					nRtn = FALSE;
				}

				//
				rs.GetFieldValue("BCRMask", data);
			}
			else	//해당 모델 정보가 존재 하지 않을 경우 
			{

			}
			rs.Close();
			db.Close();
		}
		catch (CDaoException* e)
		{
			m_strLastErrorString = e->m_pErrorInfo->m_strDescription;////AfxMessageBox(e->m_pErrorInfo->m_strDescription);
			e->Delete();
			return FALSE;
		}	
	return nRtn;
}

CString CCTSMonDoc::GetTrayLoadState(int nModuleID)
{
	CString strMsg, strTemp, strT;

	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule == NULL)	return "";
	
	int nCount = 0;	//진입한 Tray 갯수 
	for(int j =0; j<pModule->GetTotalJig(); j++)
	{		
		if(EPTrayState(nModuleID, j) == EP_TRAY_LOAD)
		{
//			Count++;
			strTemp.Format("T%d", j+1);
			if(strT.IsEmpty())	strT = strTemp;
			else				strT = strT + ", "+ strTemp;		//T1, T2진입 형태 
			// strTemp = GetTrayNo(nModuleID, j);	//Load된 이름
			strTemp = TrayStateMsg(EP_TRAY_LOAD);
			return strTemp;
		}
	}
	
	if(nCount == 0)
	{
		strMsg = TrayStateMsg(EP_TRAY_UNLOAD);		//Tray가 없으면 
	}
	else
	{
		strMsg = strT;
	}
/*	else if(nCount == pModule->GetTotalJig())		//전체 Loading 시 
	{
		strMsg = TrayStateMsg(EP_TRAY_LOAD);		
		if(nCount == 1)
		{
			//진입수량이 1개이면 Tray번호를 표시 
			if(!strTemp.IsEmpty()) strMsg = strTemp;	//Tray이름 표시 
		}
	}
	else											//일부만 Loading 시 
	{
		//1개 이상이면 Tray 수량을 표시
		strMsg.Format("%d개 %s", nCount, TrayStateMsg(EP_TRAY_LOAD));
	}
*/
	return strMsg;
}

CString CCTSMonDoc::GetDoorState(int nModuleID)
{
	CString strMsg;
	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule == NULL)	return "";
	
	int nCount = 0;	//진입한 Tray 갯수 
	for(int j =0; j<pModule->GetTotalJig(); j++)
	{
		if(EPDoorState(nModuleID, j) == EP_DOOR_CLOSE)
		{
			nCount++;
		}
	}
		
	if(nCount == 0)
	{
		strMsg = DoorStateMsg(EP_DOOR_OPEN);		
	}
	else if(nCount == pModule->GetTotalJig())
	{
		strMsg = DoorStateMsg(EP_DOOR_CLOSE);		
	}
	else 
	{
		strMsg.Format(TEXT_LANG[30], nCount,  DoorStateMsg(EP_DOOR_CLOSE));		//"%d개 %s"
	}
	return strMsg;
}

CString CCTSMonDoc::GetJigState(int nModuleID)
{
	CString strMsg;
	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule == NULL)		return "";
	
	int nCount = 0;	//진입한 Tray 갯수 
	for(int j =0; j<pModule->GetTotalJig(); j++)
	{
		if(EPJigState(nModuleID, j) == EP_JIG_DOWN)
		{
			nCount++;
		}
	}
	
	if(nCount == 0)
	{
		strMsg = JigStateMsg(EP_JIG_UP);		//Tray가 없으면 Loading표시 
	}
	else if(nCount == pModule->GetTotalJig())
	{
		strMsg = JigStateMsg(EP_JIG_DOWN);		//Tray가 없으면 Loading표시 
	}
	else 
	{
		strMsg.Format(TEXT_LANG[30], nCount,  JigStateMsg(EP_JIG_DOWN));		//Tray가 없으면 Loading표시 //"%d개 %s"
	}
	return strMsg;
}

//해당 Jig의 BCR이 읽혀 사용 가능함 여부 (BCR Lamp On/Off)
BOOL CCTSMonDoc::SendTrayReady(int nModule, int nJigNo, BOOL bReady)
{
	//Send to Module BCR Ready command 
	EP_CODE *pCode = new EP_CODE;

	pCode->nCode = nJigNo;
	pCode->nData = EP_NACK;
	if(bReady)
	{
		//Tray가 Load되어 있고 Barcode가 읽힌 경우만 Ready
		if(EPTrayState(nModule, nJigNo-1) == EP_TRAY_LOAD)
		{
			pCode->nData = EP_ACK;
		}
		else
		{

		}
	}		
	
	int nRtn = EPSendCommand(nModule, 0, 0, EP_CMD_SET_TRAY_READY, pCode, sizeof(EP_CODE));
	if(nRtn	!= EP_ACK)
	{
		CString strTemp;
//		strTemp.Format("%s (%s-%s)",  ::GetStringTable(IDS_TEXT_CMD_SEND_FAIL), ::GetModuleName(nModule), CmdFailMsg(nRtn));
		strTemp.Format(TEXT_LANG[31], ::GetModuleName(nModule), nJigNo, CmdFailMsg(nRtn));//"%s의 Jig %d에 Tray가 준비되지 않았습니다. 입력한 Tray번호의 Tray를 진입시킨 후 다시 시도 하십시요. (%s)"
		AfxMessageBox(strTemp);
		nRtn = FALSE;
	}
	else
	{
		nRtn = TRUE;
	}
	delete pCode;

	// EPSendCommand(nModule);

	return nRtn;
}

int CCTSMonDoc::SendAutoProcessRun(int nModuleID)
{
	CFormModule *pModule = GetModuleInfo(nModuleID);
	if(pModule == NULL)		return 0;

	CString strTemp;
	CString strData;
	CTray *pTray;
	// CTray *pTray1 = pModule->GetTrayInfo();
	STR_CONDITION_HEADER  testHeader;

	BOOL bTestReserved	= pModule->GetTestReserved();		//예약 상태 Check
	BOOL bBCRScaned		= pModule->GetBCRState();				//작업이 정상완료되는 시점에서 Reset됨

	if(bTestReserved == FALSE)
	{
		//입력된 Tray의 정보를 DB에서 Loading한다.
		for(int t=0; t<pModule->GetTotalJig(); t++)
		{
			pTray = pModule->GetTrayInfo(t);
			if(pTray && pTray->GetBcrRead())
			{
				pTray->LoadTrayData(pTray->GetTrayNo());
			}
		}
	}

	if(!bTestReserved || (bTestReserved && !pModule->IsAllTrayReaded()))
	{
		ZeroMemory(&testHeader, sizeof(STR_CONDITION_HEADER));

		if(bBCRScaned)	//Tray Barcode를 Scan하고 Run을 하거나, 예약이후 Tray 번호를 모두 입력하지 않은 경우
		{
			//Tray가 모두 같은 공정의 Tray인지 검사(최초로 Scan된 Tray와 모두 같은 공정을 진행할 Tray이어야 함) 
			long bProcID = CheckSameProcAndFindNextProc(nModuleID);			
			if(bProcID < 0)
			{
				//같은 공정을 진행할 Tray가 아닌 경우 
				AfxMessageBox(GetLastErrorString());
				return -1;
			}
			else
			{
				testHeader = GetTestData(bProcID);
				//같은 공정을 진행할 계획인데 Tray에 할당된 공정이 없음(최초 Tray이거나 공정이 완료된 Tray)
				if(testHeader.lID <= 0)		
				{
					if(bTestReserved)		//초기 공정이고 예약공정이 있으면 사용 
					{
						testHeader = pModule->m_ReservedtestInfo;
					}
					else					//없으면 작업자 선택화면 
					{
						testHeader = SelectTest(TRUE);
					}

					//취소된 경우 
					if(testHeader.lID == 0)	
					{
						return -1;		//Selection Canceled
					}
				}	
				
				//예약 진행중인 Tray를 먼저 Barcode Scan하고 Run을 실시한 경우
/*				if(bTestReserved == FALSE)
				{
					//Tray 정보 초기화 
					for(int t=0; t<pModule->GetTotalJig(); t++)
					{
						pTray = pModule->GetTrayInfo(t);
						if(pTray && pTray->GetBcrRead())
						{
							//처음 공정 
							if(testHeader.lID <= 0)
							{
								if(bTestReserved == FALSE)
								{
									pTray->ResetTrayData(pTray->GetTrayNo());
								}
							}
							else	//다음 공정이 있음 
							{
								pTray->SetFirstProcess();
							}
						}
					}
				}
*/
			}		
		}
		else	//Tray Barcode Scan을 하지 않고 run Button을 눌러서 시작할 경우(공정을 지정하여 예약 한다.) 
		{
			if(bTestReserved == FALSE)
			{
				//Tray를 빼지 않고 같은 자리에서 

				testHeader = SelectTest(TRUE);
				if(testHeader.lID <= 0)
				{
					return -1;		//작업자에 의해 취소됨 
				}
		
				//Tray가 아직 안읽힌 상태 이므로 모든 Tray의 정보를 Reset한다.
				for(int t=0; t<pModule->GetTotalJig(); t++)
				{
					pTray = pModule->GetTrayInfo(t);
					if(pTray)
					{
						pTray->InitData();
					}
				}
				//모듈을 예약상태로 만든다.
			}
		}

		//예약한 공정을 전송한다.
		if(bTestReserved == FALSE)
		{
			WORD state = EPGetGroupState(nModuleID, 0);
			if(state == EP_STATE_IDLE || state == EP_STATE_READY || state == EP_STATE_STANDBY || state == EP_STATE_END)
			{
				CWaitCursor wait;
				if(testHeader.lID == 0)		return -1;

				//Loading test condition
				CTestCondition testCondition;
				if(testCondition.LoadTestCondition(testHeader.lID))
				{
					if(SendConditionToModule(nModuleID, 0, &testCondition)== FALSE)
					{
						return 0;
					}
					//Standby 상태로 전이되도록 기다림
					//Sleep(100);
				}
				else
				{
					strTemp.Format(TEXT_LANG[4], testHeader.szName);		//"선택 조건 [%s] Loading을 실패 하였습니다."
					AfxMessageBox(strTemp);
					return 0;
				}
			}
			else
			{
				//전송할 수 없는 상태
				return 0;
			}
		}

		//이전 정보를 Reset하고 보여줄지 여부 
		if(ShowRunInformation(nModuleID, 0, !bTestReserved, testHeader.lID) < 1)
		{
			return -1;
		}

		//작업을 예약한다.	최초 한번 예약하면 예약된 공정에 맞는 Tray만 투입해야 한다.
		if(bTestReserved == FALSE)
		{
			pModule->SetTestReserve(testHeader);		
/*			if(CheckStopperSensor(nModuleID) == FALSE)
			{
				strTemp.Format("%s\n\n%s 에는 작업할 모델에 맞지 않는 지그 높이 설정이 존재합니다. 작업 시작전에 반드시 해당 모델에 맞도록 지그 높이를 조정 하시기 바랍니다.",
								GetLastErrorString(), GetModuleName(nModuleID)); 
				AfxMessageBox(strTemp);
			}
*/		}
		TRACE("Find Next Test ID(%d) :: %d, %s, %s\n",	testHeader.lID, testHeader.lNo, testHeader.szName, testHeader.szDescription);
		
	}
	//////////////////////////////////////////////////////////////////////////

	//사용자가 모두 입력 했으면 Run한다.
	if(pModule->IsAllTrayReaded() == FALSE)
	{
		AfxMessageBox(TEXT_LANG[32]);//"투입수량을 입력한 Jig위치에 Tray를 투입하시고 BCR을 이용하여 Tray 번호를 스캔 하십시요"
		return -1;
	}

	//Tray Loading과 Sensor와 Matching Check		
	if(TrayMatchingCheck(nModuleID) == FALSE)
	{
		pModule->ResetTestReserve();			//예약된 정보를 Reset하여야 재입력 화면이 나타난다.
		
		AfxMessageBox(GetLastErrorString());
		return -1;
	}
	
	for(int i=0; i<pModule->GetTotalJig(); i++)
	{
		if(EP_TRAY_LOAD == EPTrayState(nModuleID, i))
		{
			if(CheckStopperSensor(nModuleID, i+1) == FALSE)
			{
				strTemp.Format(TEXT_LANG[33],//"%s\n\n%s 에는 작업할 모델에 맞지 않는 지그 높이 설정이 존재합니다. 작업 시작전에 반드시 해당 모델에 맞도록 지그 높이를 조정 하시기 바랍니다."
								GetLastErrorString(), GetModuleName(nModuleID)); 
				AfxMessageBox(strTemp);
				return -1;
			}
		}
	}
	
	//Module에 전송 정보 저장 (파일생성 이전에 )
	pModule->UpdateStartTime();
	pModule->UpdateTestLogTempFile();	//CheckEnableRun()에서 파일을 생성하므로 그 이전에 Update해야 한다.

	//예약된 공정과 진행할 Tray의 공정이 맞는지 검사 
	//수동으로 예약 공정과 다른 Tray를 넣으면 작업불가 
	if(IsExeReservedTestTray(nModuleID) == FALSE)
	{
		AfxMessageBox(TEXT_LANG[34], MB_OK|MB_ICONSTOP);		//"예약공정과 맞지 않는 Tray가 존재합니다."
		return -1;
	}

	//작업 가능상태인지 검사 
	//결과 파일도 생성한다.
	DWORD dwJigFlag = CheckEnableRun(nModuleID);
	if(dwJigFlag == 0)
	{
		AfxMessageBox(GetLastErrorString());
		return -1;
	}

	//자동 공정이 아니더라도 Cell 상태를 Setting하여 보내야 한다.   //2002.5.1
	if(SendLastTrayStateData(nModuleID, 0) == FALSE)	//현재 Tray의 최종 상태를 Module로 전송
	{
		AfxMessageBox(m_strLastErrorString, MB_ICONSTOP|MB_OK);
		return -1;
	}
	//Sleep(100);

	//Delete Real Time Save File
/*	strTemp.Format("%s\\Data\\Temp\\%s_C*.csv", m_strCurFolder, ::GetModuleName(nModuleID));
	CFileFind 	finder;
	BOOL   bWorking = finder.FindFile(strTemp);
	while (bWorking)
	{
		bWorking	= finder.FindNextFile();
		_unlink(finder.GetFilePath());
	}
*/
	//최종 상태 검사 
	BYTE byTemp;
	WORD state = EPGetGroupState(nModuleID, 0);
	if(state != EP_STATE_STANDBY && state != EP_STATE_READY && state != EP_STATE_END && state != EP_STATE_IDLE)
	{
		strTemp.Format(TEXT_LANG[6], ::GetModuleName(nModuleID), GetStateMsg(state, byTemp));//"%s :: [작업시작] 명령 전송가능 상태가 아닙니다.(state : %s)"
		WriteLog(strTemp);
		return 0;
	}

	//Module에 Run Command 전송 
	int nRtn = EP_NACK;
	EP_RUN_CMD_INFO	runInfo;
	ZeroMemory(&runInfo, sizeof(EP_RUN_CMD_INFO));

	//TestSerialNo 전송(실시간 저장 Data를 구별하는데 사용)
	//20061025 => 새로운 번호 생성(TestSerialNo는 Tray간의 고유번호로 사용되므로 다른 번호 생성)
	sprintf(runInfo.szTestSerialNo, pModule->GetTestSerialNo());

	//사용할 지그번호 전송(사용안하는 지그는 Down하지 않음) cf. Local에서 Up/Down하면 모든 Jig Up/Down 실시 
	runInfo.dwUseFlag = dwJigFlag;
	runInfo.nTempGetPosition = atol(AfxGetApp()->GetProfileString(FORMSET_REG_SECTION, "Temp Measuring Position", "0"));

	int nTrayIndex = 0;
	for( nTrayIndex = 0; nTrayIndex<INSTALL_TRAY_PER_MD; nTrayIndex++ )
	{
		pTray = pModule->GetTrayInfo(nTrayIndex);
		if(pTray)
		{
			runInfo.nInputCell[nTrayIndex] = pTray->lInputCellCount;
		}
	}

	TRACE("%s :: Send run cmd, Serial (%s), Flag (0x%x)\n", GetModuleName(nModuleID), runInfo.szTestSerialNo, runInfo.dwUseFlag);

	if((nRtn = EPSendCommand(nModuleID, 1, 0, EP_CMD_RUN, &runInfo, sizeof(EP_RUN_CMD_INFO))) != EP_ACK)
	{
		strTemp.Format("%s (%s-%s)",  ::GetStringTable(IDS_TEXT_CMD_SEND_FAIL), ::GetModuleName(nModuleID), CmdFailMsg(nRtn));
		AfxMessageBox(strTemp, MB_ICONSTOP|MB_OK);
		return -1;
	}
	// EPSendCommand(nModuleID);

	//예약된 시험이 있으면 Reset시킨다.
	pModule->ResetTestReserve();
	for(int t=0; t<pModule->GetTotalJig(); t++)
	{
		pTray = pModule->GetTrayInfo(t);
		if(pTray && pTray->GetBcrRead())
		{
			pTray->SetTestReserved(FALSE);	//수량입력으로 예약된 경우 
		}
	}
	
	//2006/03/22 KBH
	//완료된 Step의 결과 data를 표시할 수 있도록 최종 data file reset 한다.
	pModule->LoadResultData();

//	WriteProcedureLog(nModuleID, nGroupIndex);		

	return 1;
}

BOOL CCTSMonDoc::CheckProcAndSystemType(long lProcTypeID)
{
	//장비 종류만 확인 (ex.Formation인데 Aging으로 진입)
	//TestType Tabel에서 검색	
	int nSysType = ((CCTSMonApp*)AfxGetApp())->GetSystemType();

	if(nSysType == EP_ID_ALL_SYSTEM)	return TRUE;
	
	CString str;
	str.Format("SELECT TestTypeName, ExeStep FROM TestType WHERE TestType = %d", lProcTypeID);	
	CDaoDatabase  db;
	try
	{
		db.Open(::GetDataBaseName());
		CDaoRecordset rs(&db);
		rs.Open(dbOpenSnapshot, str, dbReadOnly);	
	
		str.Empty();
		if(!rs.IsBOF() && !rs.IsEOF())
		{
			COleVariant data;
			data = rs.GetFieldValue(0);
			data = rs.GetFieldValue(1);
			str = data.pcVal;
		}
		rs.Close();
		db.Close();
	}
	catch (CDaoException* e)
	{
		e->Delete();
	}
	
	if(str.GetLength() > 2)
	{
		if(str.Left(2) == EP_PGS_AGING_CODE)	//Aging 장비 
		{
			if(nSysType != EP_ID_AGIGN_SYSTEM)
			{
				m_strLastErrorString.Format(TEXT_LANG[35], str.Left(2), nSysType);//"Aging System에서 진행할 수 없습니다.[Code : %s, System : %d]"
				return FALSE;
			}
		}
		else if(str.Left(2) == EP_PGS_PREIMPEDANCE_CODE || str.Left(2) == EP_PGS_IROCV_CODE || str.Left(2) == EP_PGS_OCV_CODE )			//IROCV 측정기 
		{
			if(nSysType != EP_ID_IROCV_SYSTEM)
			{
				m_strLastErrorString.Format(TEXT_LANG[36], str.Left(2), nSysType);//"IR/OCV System에서 진행할 수 없습니다.[Code : %s, System : %d]"
				return FALSE;
			}
		}
		else if(str.Left(2) == EP_PGS_GRADING_CODE || str.Left(2) == EP_PGS_SELECTOR_CODE)								//Selecting 설비 
		{
			if(nSysType != EP_ID_GRADING_SYSTEM)
			{
				m_strLastErrorString.Format(TEXT_LANG[37], str.Left(2), nSysType);//"Grading/Selecting System에서 진행할 수 없습니다.[Code : %s, System : %d]"
				return FALSE;
			}
		}
		else //if(strProcess == "PC" || strProcess == "FO" || strProcess == "FC" )	//충방전기 설비 
		{
			if(nSysType != EP_ID_FORM_OP_SYSTEM)
			{
				m_strLastErrorString.Format(TEXT_LANG[38], str.Left(2), nSysType);//"Formation System에서 진행할 수 없습니다.[Code : %s, System : %d]"
				return FALSE;
			}
		}
	}
	return TRUE;
}

COLORREF CCTSMonDoc::GetGradeColor(CString strCode)
{
	if(strCode.IsEmpty() == FALSE)
	{
		for(int i=0; i<MAX_GRADE_COLOR; i++)
		{
			if(m_gradeColorConfig.colorData[i].bUse)
			{
				if(CString(m_gradeColorConfig.colorData[i].szCode) == strCode)
				{
					return m_gradeColorConfig.colorData[i].dwColor;
				}
			}
		}
	}

	return RGB(255,255,255);
}

UINT CCTSMonDoc::file_SummaryDataWrite( char* szFileName, char* szData, int nMode )
{
	if( szFileName == NULL || szData == NULL )
	{
		return 0;
	}

	FILE *fp = NULL;
	if( nMode == FILE_CREATE )
	{
		fp = fopen(szFileName, "wb");
	}
	else if( nMode == FILE_APPEND )
	{
		fp = fopen(szFileName, "ab+");
	}

	if( fp )
	{
		fprintf(fp, "%s\r\n", szData );
		fclose(fp);
	}	

	return 1;
}
// VOID CCTSMonDoc::file_newWrite( CString filename, CString data )
// {
// 	FILE *fWrite;	
// 	if( filename.IsEmpty() )
// 	{
// 		CString strTemp;
// 		strTemp.Format("%s <= Data 저장 에러 : 저장폴더 미생성", data);
// 		WriteLog((LPSTR)(LPCTSTR)strTemp);		
// 	}
// 	fWrite = fopen(filename, "wb");
// 	if( fWrite )
// 	{
// 		fprintf(fWrite, "%s\r\n", data );
// 	}
// 	fclose(fWrite);
// }

BOOL CCTSMonDoc::file_Finder(CString str_path)
{
	BOOL bWork;
	CFileFind finder;
    bWork=finder.FindFile(str_path);		
	finder.Close();
	return bWork;
}

BOOL CCTSMonDoc::ForceDirectory(LPCTSTR lpDirectory)
{
	CString szDirectory = lpDirectory;
	szDirectory.TrimRight("\\");
	
	if ((GetFilePath(szDirectory) == szDirectory) ||
		(FileExists(szDirectory) == -1))
		return TRUE;
	if (!ForceDirectory(GetFilePath(szDirectory)))
		return FALSE;
	if (!CreateDirectory(szDirectory, NULL))
		return FALSE;
	return TRUE;
}

CString CCTSMonDoc::GetFilePath(LPCTSTR lpszFilePath)
{
	TCHAR szDir[_MAX_DIR];
	TCHAR szDrive[_MAX_DRIVE];
	_tsplitpath(lpszFilePath, szDrive, szDir, NULL, NULL);
	
	return  CString(szDrive) + CString(szDir);
}

int CCTSMonDoc::FileExists(LPCTSTR lpszName)
{
	CFileFind fileFind;
	if (!fileFind.FindFile(lpszName))
	{
		if (DirectoryExists(lpszName))  // if root ex. "C:\"
			return -1;
		return 0;
	}
	fileFind.FindNextFile();
	return fileFind.IsDirectory() ? -1 : 1;
}

BOOL CCTSMonDoc::DirectoryExists(LPCTSTR lpszDir)
{
	char curPath[_MAX_PATH];   /* Get the current working directory: */
	if (!_getcwd(curPath, _MAX_PATH))
		return FALSE;
	if (_chdir(lpszDir))	// retruns 0 if error
		return FALSE;
	_chdir(curPath);
	return TRUE;
}


BOOL CCTSMonDoc::ConvertToExcel(CString strFileName)
{
	// TODO: Add your control notification handler code here
	if(strFileName.IsEmpty())
	{
//		AfxMessageBox("Load된 파일이 없습니다.");
		return FALSE;
	}

	CString strTemp;
	CString strDirTemp;
	{
		TCHAR szCurDir[MAX_PATH];
		::GetModuleFileName(AfxGetApp()->m_hInstance, szCurDir, MAX_PATH);
		strDirTemp = CString(szCurDir).Mid(0, CString(szCurDir).ReverseFind('\\'));
	}
	strTemp.Format("%s\\ExcelTrans.exe", strDirTemp);	

	if(ExecuteProgram(strTemp, "", "", "ExcelTrans", FALSE, TRUE, FALSE) == FALSE)
	{
		return FALSE;
	}

	CWnd *pWndPrev = NULL;
	pWndPrev = CWnd::FindWindow(NULL, "ExcelTrans");
	if(pWndPrev != NULL)
	{
/*		if(m_pszFileBuff)
		{
			delete [] m_pszFileBuff;
		}
		int nSize = strFileName.GetLength()+1;
		m_pszFileBuff = new char[nSize];
		ZeroMemory(m_pszFileBuff, nSize);
		sprintf(m_pszFileBuff, "%s", strFileName);
		m_pszFileBuff[nSize-1] = '\0';

		COPYDATASTRUCT CpStructData;
		CpStructData.dwData = 2;		//CTS Program 구별 Index
		CpStructData.cbData = nSize;
		CpStructData.lpData = m_pszFileBuff;
*/		
		CString str;
		str.Format("%s\\AutoFileName.tmp", m_strTempFolder);
		FILE *fp = fopen(str, "wt");
		if(fp)
		{
			fprintf(fp, "%s", strFileName);
			fclose(fp);
			
			::PostMessage(pWndPrev->m_hWnd, WM_AUTO_FILE_TRANS, 0, 0);

			return TRUE;
		}
		
	}
	return FALSE;
}

//연속 Ch Display시 
int CCTSMonDoc::GetModuleStartChNo(int nModuleID, int nTrayIndex)
{
	int nStartChNo = 1;
	int i=0, j=0;
	int nModuleIndex = EPGetModuleIndex(nModuleID);
	for(i=0; i<nModuleIndex; i++)
	{
		EP_MD_SYSTEM_DATA * pSysData = EPGetModuleSysData(i);
		nStartChNo += pSysData->nTotalChNo;	
	}
	
	EP_MD_SYSTEM_DATA * pSysData = EPGetModuleSysData(i);
	for(j=0; j<nTrayIndex; j++)
	{
		nStartChNo += pSysData->awChInTray[j];	
	}

	return nStartChNo;
}

void CCTSMonDoc::UpdateTrayCellStartNo( int nModuleID )
{
	CFormModule *lpModuleInfo;
	
	if( nModuleID != -1 )
	{	
		lpModuleInfo = GetModuleInfo(nModuleID);
		if(lpModuleInfo)
		{
			for(int t=0; t<lpModuleInfo->GetTotalJig() ; t++)
			{
				int nStartCh = GetModuleStartChNo(nModuleID, t);
				lpModuleInfo->SetCellNo(nStartCh, t);
			}
		}	
	}	
}

//DataBase에 LastEndTime 기록 한다.
BOOL CCTSMonDoc::Fun_UpdateCellLastTime(CDatabase &db,CString strCell)
{
	CString strSQL;
	COleDateTime curTime = COleDateTime::GetCurrentTime();		//완료 시각 
	
	//strSQL = "SELECT COUNT(ID) FROM ReportCellData";
	strSQL.Format("UPDATE ReportCellData SET LastEndTime = '%s' WHERE BarCode='%s'",curTime.Format(),strCell);
	
	// 	db.BeginTrans();
	try
	{
//		db.BeginTrans();
		db.ExecuteSQL(strSQL);
//		db.CommitTrans();
		return TRUE;
	}
	catch (CException* e)
	{
		return FALSE;
	}
	catch (CMemoryException* e)
	{
		
	}	
}

BOOL CCTSMonDoc::Fun_UpdateCellData(CDatabase &db,CString strCell,CString strTestName,CString strTrayName,long lProType,long lVolt,long lCurrent,long lCapa, unsigned long lTime)
{
	long  lDBTime=0;
	float fVolt,fCurrent,fCapa,fTime;
    if (lTime > 0) 
	{
		fTime =  MD2PCTIME(lTime);			//sec 단위로 변환  
		lDBTime = fTime;
	}
    if (lVolt != 0 )	fVolt =	 VTG_PRECISION(lVolt);
    if (lCurrent != 0 ) fCurrent = CRT_PRECISION(lCurrent)	;
	if (lCapa != 0 )	fCapa =	 ETC_PRECISION(lCapa);
//     pChSaveData->fTotalTime = MD2PCTIME(pChData->ulTotalTime);			//sec 단위로 변환
//     pChSaveData->fWatt	= ETC_PRECISION(pChData->lWatt)			;
//     pChSaveData->fWattHour = ETC_PRECISION(pChData->lWattHour)	;
//     pChSaveData->fCapacity = ETC_PRECISION(pChData->lCapacity)	;
//     pChSaveData->fImpedance = ETC_PRECISION(pChData->lImpedance);

	// 	Product.mdb -> ReportCellData Table 확인 없으면 생성(나중에 추가해야 함)
	// 	Cell BarCode 검사 -> 있으면 Update, 없으면 Insert
	//DataBase에 DATA 기록 한다.
	CString strSQL,strTemp;
	CString strField1,strField2,strField3,strField4;
	int nRecordCnt=0;
	BOOL bInsertFlag(TRUE);
	COleDateTime curTime = COleDateTime::GetCurrentTime();		//시작시간 (Insert)
	
// 	CRecordset rs2(&db2);
	CRecordset rs(&db);
	strSQL.Format("SELECT StartTime FROM ReportCellData WHERE BarCode='%s'",strCell);
	try
	{
		rs.Open( AFX_DB_USE_DEFAULT_TYPE, strSQL, CRecordset::none);
	}
	catch (CException* e)
	{
//		AfxMessageBox(e->GetErrorMessage());
//		e->Delete();
//		db.Close();
//		rs.Close();
		return FALSE;
	}
	catch (CMemoryException* e)
	{
		AfxMessageBox("UpdateCellData MemoryException");
	}

	if(!rs.IsBOF() && !rs.IsEOF())
	{
		// 레코드가 존재.
		bInsertFlag = FALSE;
	}
	else
	{
		// 레코드가 비었다.
		bInsertFlag = TRUE;
	}
//	rs.Close();

	strField1 = strField2 = strField3 = strField4 = "";
	switch (lProType)
	{
	case EP_PROC_TYPE_CHARGE:
		strField1 = "ChargeV";
		strField2 = "ChargeI";
		strField3 = "ChargeT";
		strField4 = "ChargeC";
		break;
	case EP_PROC_TYPE_CHARGE1:
		strField1 = "Charge1V";
		strField2 = "Charge1I";
		strField3 = "Charge1T";
		strField4 = "Charge1C";
		break;
	case EP_PROC_TYPE_CHARGE2:
		strField1 = "Charge2V";
		strField2 = "Charge2I";
		strField3 = "Charge2T";
		strField4 = "Charge2C";
		break;
	case EP_PROC_TYPE_CHARGE3:
		strField1 = "Charge3V";
		strField2 = "Charge3I";
		strField3 = "Charge3T";
		strField4 = "Charge3C";
		break;
	case EP_PROC_TYPE_CHARGE4:
		strField1 = "Charge4V";
		strField2 = "Charge4I";
		strField3 = "Charge4T";
		strField4 = "Charge4C";
		break;
	case EP_PROC_TYPE_CHARGE5:
		strField1 = "Charge5V";
		strField2 = "Charge5I";
		strField3 = "Charge5T";
		strField4 = "Charge5C";
		break;
	case EP_PROC_TYPE_CHARGE6:
		strField1 = "Charge6V";
		strField2 = "Charge6I";
		strField3 = "Charge6T";
		strField4 = "Charge6C";
		break;
	case EP_PROC_TYPE_DISCHARGE:
		strField1 = "DisChargeV";
		strField2 = "DisChargeI";
		strField3 = "DisChargeT";
		strField4 = "DisChargeC";
		break;
	case EP_PROC_TYPE_DISCHARGE1:
		strField1 = "DisCharge1V";
		strField2 = "DisCharge1I";
		strField3 = "DisCharge1T";
		strField4 = "DisCharge1C";
		break;
	case EP_PROC_TYPE_DISCHARGE2:
		strField1 = "DisCharge2V";
		strField2 = "DisCharge2I";
		strField3 = "DisCharge2T";
		strField4 = "DisCharge2C";
		break;
	case EP_PROC_TYPE_DISCHARGE3:
		strField1 = "DisCharge3V";
		strField2 = "DisCharge3I";
		strField3 = "DisCharge3T";
		strField4 = "DisCharge3C";
		break;
	case EP_PROC_TYPE_DISCHARGE4:
		strField1 = "DisCharge4V";
		strField2 = "DisCharge4I";
		strField3 = "DisCharge4T";
		strField4 = "DisCharge4C";
		break;
	case EP_PROC_TYPE_DISCHARGE5:
		strField1 = "DisCharge5V";
		strField2 = "DisCharge5I";
		strField3 = "DisCharge5T";
		strField4 = "DisCharge5C";
		break;
	case EP_PROC_TYPE_DISCHARGE6:
		strField1 = "DisCharge6V";
		strField2 = "DisCharge6I";
		strField3 = "DisCharge6T";
		strField4 = "DisCharge6C";
		break;
	case EP_PROC_TYPE_REST:
		strField1 = "RestV";
		strField2 = "";
		strField3 = "RestT";
		break;
	case EP_PROC_TYPE_REST1:
		strField1 = "Rest1V";
		strField2 = "";
		strField3 = "Rest1T";
		break;
	case EP_PROC_TYPE_OCV:
		strField1 = "OCV";
		strField2 = "";
		strField3 = "";
		break;
	case EP_PROC_TYPE_OCV1:
		strField1 = "OCV1";
		break;
	case EP_PROC_TYPE_OCV2:
		strField1 = "OCV2";
		strField3 = "";
		break;
	case EP_PROC_TYPE_OCV3:
		strField1 = "OCV3";
		break;
	case EP_PROC_TYPE_OCV4:
		strField1 = "OCV4";
		break;
	case EP_PROC_TYPE_OCV5:
		strField1 = "OCV5";
		break;
	case EP_PROC_TYPE_OCV6:
		strField1 = "OCV6";
		break;
// 	case EP_PROC_TYPE_IMPEDANCE:
// 		break;
	default :
		strField1 = strField2 = strField3 = "";
		break;
	}

	if (bInsertFlag)
	{
		//데이터가 없으므로 INSERT 한다.
		strSQL="INSERT INTO ReportCellData(BarCode, LotID,TrayName, StartTime";
		if (strField1 != ""){ strTemp.Format(", %s",strField1);		strSQL = strSQL + strTemp; }
		if (strField2 != ""){ strTemp.Format(", %s",strField2);		strSQL = strSQL + strTemp; }
		if (strField3 != ""){ strTemp.Format(", %s",strField3);		strSQL = strSQL + strTemp; }
		if (strField4 != ""){ strTemp.Format(", %s",strField4);		strSQL = strSQL + strTemp; }
		strTemp.Format(") VALUES ('%s','%s','%s','%s'",strCell,strTestName,strTrayName,curTime.Format());   strSQL = strSQL + strTemp;
		if (strField1 != ""){ strTemp.Format(", %f",fVolt);		strSQL = strSQL + strTemp; }
		if (strField2 != ""){ strTemp.Format(", %f",fCurrent);		strSQL = strSQL + strTemp; }
		if (strField3 != ""){ strTemp.Format(", %ld",lDBTime);		strSQL = strSQL + strTemp; }
		if (strField4 != ""){ strTemp.Format(", %f",fCapa);			strSQL = strSQL + strTemp; }
		strTemp = ")";
		strSQL = strSQL + strTemp;

		//ExpFloattoLong(m_fIref, EP_CRT_FLOAT);
		//PC2MDTIME
		//strSQL.Format("INSERT INTO ReportCellData(BarCode, TestName,TrayName, StartTime) VALUES ('%s','%s','%s','%s')",
		//	strCell,strTestName,strTrayName,curTime.Format(),);
	}
	else
	{
		//데이터가 있으므로 UPDATE 한다.
		strSQL.Format("UPDATE ReportCellData SET LotID = '%s'", strTestName);
		strTemp.Format(", TrayName = '%s'", strTrayName);
		strSQL = strSQL + strTemp;
		if (strField1 != ""){ strTemp.Format(", %s = %f",strField1, fVolt);		strSQL = strSQL + strTemp; }
		if (strField2 != ""){ strTemp.Format(", %s = %f",strField2, fCurrent);		strSQL = strSQL + strTemp;}
		if (strField3 != ""){ strTemp.Format(", %s = %ld",strField3, lDBTime);		strSQL = strSQL + strTemp;}
		if (strField4 != ""){ strTemp.Format(", %s = %f",strField4, fCapa);			strSQL = strSQL + strTemp;}
		strTemp.Format(" WHERE BarCode='%s'",strCell);
		strSQL = strSQL + strTemp;
	}
	try
	{
		TRACE("==> SQl : %s",strSQL);
//		rs.Open( AFX_DB_USE_DEFAULT_TYPE, strSQL, CRecordset::none);
//		db.BeginTrans();
		db.ExecuteSQL(strSQL);
//		db.CommitTrans();
		return TRUE;
	}
	catch (CException* e)
	{
		
		return FALSE;
	}
	catch (CMemoryException* e)
	{
		
	}
	
// 	//	COleVariant data;
// 	CRecordset rs(&db);
// 	try
// 	{
// 		rs.Open( AFX_DB_USE_DEFAULT_TYPE, strSQL, CRecordset::none);	
// 	}
// 	catch (CDaoException* e)
// 	{
// 		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
// 		e->Delete();
// 		db.Close();
// 		return 0;
// 	}	
// 	if(!rs.IsBOF() && !rs.IsEOF())
// 	{
// 		nRecordCnt = rs.GetRecordCount();
// 	}
//	rs.Close();
	return TRUE;
}

void CCTSMonDoc::OnInspectedResultRun()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	CString strProgramName;
	strProgramName.Format("%s\\CTSAnalyzer.exe", m_strCurFolder);
	
	ExecuteProgram(strProgramName, "", ANAL_CLASS_NAME, "", FALSE, TRUE);
}

void CCTSMonDoc::fnWriteTemperature( int nModuleID, BOOL bEndFlag )
{
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();

	int state = 0;
	int i=0;

	CFormModule *pModule = NULL;
	_MAPPING_DATA *pMapData;
	EP_GP_DATA gpData;

	bool bTitle = false;

	byte bFlag;
	CString strTemp = _T("");
	CString strState = _T("");
	CString strLog = _T("");
	CString strTitle = _T("STATE");
	CString strTempFileName = _T("");

	COleDateTime timeNow = COleDateTime::GetCurrentTime();

	int nTempratureSaveInterval = AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "TemperatureSaveInterval", 1);
	if( nTempratureSaveInterval == 0 )
	{
		return;
	}

	// 1. 선택된 모듈이 없을 경우 전체 확인
	// 2. 선택된 모듈이 있을 경우 현재 온도 저장
	if( nModuleID == 0)
	{
		for(nModuleID=1; nModuleID <= m_nInstalledModuleNum; nModuleID++ )
		{		
			pModule = GetModuleInfo(nModuleID);
			if( pModule == NULL )
			{
				continue;
			}

			WORD state = EPGetGroupState(nModuleID);
			if( state == EP_STATE_RUN )
			{
				if( pModule->GetResultFileName() != _T("") )
				{
					COleDateTime chTime;
					pModule->GetRunStartTime();

					COleDateTimeSpan timeDiff = timeNow - pModule->GetRunStartTime();

					int nMinute = timeDiff.GetMinutes();
					nMinute = nMinute + 1;

					if( nMinute > 0
						&& nMinute % nTempratureSaveInterval == 0 )
					{
						int nFileLength = pModule->GetResultFileName().GetLength();
						strTempFileName = pModule->GetResultFileName().Left(nFileLength-4);

						strLog.Format("%s", GetStateMsg(pModule->GetState(TRUE), bFlag) );

						gpData = EPGetGroupData(nModuleID, 0);

						for( int i=0; i<MAX_USE_TEMP; i++ )				// 총 Sensor값은 16개
						{							
							strTemp.Format(",T%d", i+1);
							strTitle += strTemp;

							strTemp.Format(",%.1f", (float)gpData.sensorData.sensorData1[i].lData/100.0f );
							strLog += strTemp;
						}

						GLog::log_TempSave(strLog, strTempFileName, NULL, TRUE, _T(""), strTitle);
					}
				}
			}
		}
	}
	else
	{
		pModule = GetModuleInfo(nModuleID);
		if( pModule != NULL )
		{
			if( pModule->GetResultFileName() != _T("") )
			{
				int nFileLength = pModule->GetResultFileName().GetLength();
				strTempFileName = pModule->GetResultFileName().Left(nFileLength-4);

				if( bEndFlag == TRUE )
				{
					strLog.Format("END");
				}
				else
				{
					strLog.Format("START");
				}	

				gpData = EPGetGroupData(nModuleID, 0);

				for( int i=0; i<MAX_USE_TEMP; i++ )				// 총 Sensor값은 16개
				{							
					strTemp.Format(",T%d", i+1);
					strTitle += strTemp;

					strTemp.Format(",%.1f", (float)gpData.sensorData.sensorData1[i].lData/100.0f );
					strLog += strTemp;
				}

				GLog::log_TempSave(strLog, strTempFileName, NULL, TRUE, _T(""), strTitle);
			}
		}
	}
}

void CCTSMonDoc::fnWriteTempFileSave( )
{
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();

	int state = 0;
	int i=0;

	CFormModule *pModule = NULL;
	_MAPPING_DATA *pMapData;
	EP_GP_DATA gpData;

	bool bTitle = false;

	byte bFlag;
	CString strTemp = _T("");
	CString strState = _T("");
	CString strLog = _T("");
	CString strTitle = _T("");
	CString strTempFileName = _T("");

	COleDateTime timeNow = COleDateTime::GetCurrentTime();

	int nTempratureSaveInterval = AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "TemperatureSaveInterval", 1);
	if( nTempratureSaveInterval == 0 )
	{
		return;
	}
	int nModuleID = 0;

	CString sFileName=GWin::win_CurrentTime(_T("filelog"));

	//			2014-02-10			bung			:			MES 관련 log 수정.
	CString dbFolderName;
	dbFolderName.Format(_T("%s"),sFileName.Left(7));

	CString strTempModuleId= _T("");
	CString strPath= _T("");
	// 1. 선택된 모듈이 없을 경우 전체 확인
	// 2. 선택된 모듈이 있을 경우 현재 온도 저장
	if( nModuleID == 0)
	{
		for(nModuleID=1; nModuleID <= m_nInstalledModuleNum; nModuleID++ )
		{		
			pModule = GetModuleInfo(nModuleID);
			if( pModule == NULL )
			{
				continue;
			}

			WORD state = EPGetGroupState(nModuleID);
			if( state != EP_STATE_LINE_OFF )
			{
				//strTempModuleId = GetModuleName(nModuleID);

				strPath.Format("%s\\TempFile\\%s", m_strCurFolder,GetModuleName(nModuleID));

				//strPath.Format("")
				gpData = EPGetGroupData(nModuleID, 0);

				for( int i=0; i<MAX_USE_TEMP; i++ )				// 총 Sensor값은 16개
				{							
					strTemp.Format("T%d,", i+1);
					strTitle += strTemp;

					strTemp.Format("%.1f,", (float)gpData.sensorData.sensorData1[i].lData/100.0f );
					strLog += strTemp;
				}

				GLog::log_TempFileSave(strLog, strPath, NULL, TRUE, _T(""), strTitle);
				strLog = _T("");
				strTitle = _T("");

			}
		}
	}
}