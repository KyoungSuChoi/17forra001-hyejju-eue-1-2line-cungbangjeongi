// FormResultFile.cpp: implementation of the CFormResultFile class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "FormResultFile.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

bool CFormResultFile::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CFormResultFile"), _T("TEXT_CFormResultFile_CNT"), _T("TEXT_CFormResultFile_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CFormResultFile_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CFormResultFile"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

CFormResultFile::CFormResultFile()
{
	LanguageinitMonConfig();

//	m_pConditionMain = NULL;
	m_bSumCapacity = FALSE;
	m_bLocalGrading = FALSE;
	ZeroMemory(&m_ResultCellSerial, sizeof(PNE_RESULT_CELL_SERIAL));
}

CFormResultFile::~CFormResultFile()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
	ClearConResult();
	ClearResult();
}
//////////////////////////////////////////////////////////
//														//
//					결과 파일 저장 구조					//
//														//		
//////////////////////////////////////////////////////////
//	1. EP_FILE_HEADER									//
//	2. RESULT_FILE_HEADER								//
//	3. EP_MD_SYSTEM_DATA								//
//	4. file version >= RESULT_FILE_VER3					//
//			STR_FILE_EXT_SPACE							//	
//	5. EP_TEST_HEADER									//
//	6. STR_CONDITION_HEADER								//
//	7. EP_PRETEST_PARAM									//
//	8. 각 Step 구조 * Step 수							//
//	9. 설정된 Grade 수 * Grade Step수					//
//////////////////////////////////////////////////////////			
//			10~13 항을 진행 Step 수 만큼 저장 한다.		//
//														//
//	10. file versin >= RESULT_FILE_VER4					//
//			EP_GP_DATA									//	
//		else  EP_GP_STATE_DATA							//	
//	11. file version >= RESULT_FILE_VER6				//
//			RESULT_FILE_HEADER							//
//			EP_COMMON_STEP								//
//	12. file version >= RESULT_FILE_VER4				//
//			EP_RESULT_TO_DATABASE*EP_RESULT_ITEM_NO		//
//	13. STR_SAVE_CH_DATA * Channel 수					//
//														//
//////////////////////////////////////////////////////////

// ContackChk 파일의 내용을 읽어 들인다.
int CFormResultFile::ReadConFile(CString strFileName)
{	
	ClearConResult();

	FILE *fp = fopen((LPCTSTR)strFileName, "rb");
	if(fp == NULL)				return -1;

	//File Header Read
	fread(&m_fileHaeder, sizeof(EP_FILE_HEADER), 1, fp);

	int nFileVersion = atol(m_fileHaeder.szFileVersion);	//File Version

	//Formation Result File Read
	if(strcmp(m_fileHaeder.szFileID, RESULT_FILE_ID) == 0)		
	{
		if( nFileVersion <= 0 || nFileVersion > atol(RESULT_FILE_VER3))
		{
			fclose(fp);
			fp = NULL;
			return -2;
		}
	}
	else if(strcmp(m_fileHaeder.szFileID, IROCV_FILE_ID) == 0)	//IROCV Result File Read
	{
// 		if( nFileVersion < 0 || nFileVersion > atol(IROCV_FILE_VER2))
// 		{
// 			fclose(fp);
// 			fp = NULL;
// 			return -3;
// 		}
	}
	else if(strcmp(m_fileHaeder.szFileID, PRODUCT_RESULT_FILE_ID) == 0)
	{

	}
	else	//알수 없는 파일 
	{
		fclose(fp);
		fp = NULL;
		return -4;
	}

	//Test 결과 Header를 읽는다.
	if(fread(&m_resultFileHeader, sizeof(RESULT_FILE_HEADER), 1, fp)<1)	//Read Test Result Header
	{
		fclose(fp);
		fp = NULL;
		return -5;	//File Header Read
	}

	// 시스템 Parameter를 읽는다.
	if(fread(&m_sysData, sizeof(EP_MD_SYSTEM_DATA), 1, fp)<1)			//Read System Param
	{
		TRACE("Test Group File header Read Fail..\n");
		fclose(fp);
		fp = NULL;
		return -6;	//File Header Read
	}

	if(fread(&m_extData, sizeof(STR_FILE_EXT_SPACE), 1, fp) < 1)
	{
		TRACE("File Data Read Fail..\n");
				
		fclose(fp);
		fp = NULL;
		return -7;
	}

	// Cell Serial 를 불러온다.
	if(strcmp(m_fileHaeder.szFileID, RESULT_FILE_ID) == 0 && nFileVersion >= atol(RESULT_FILE_VER3))
	{		
		if(fread(&m_ResultCellSerial, sizeof(PNE_RESULT_CELL_SERIAL), 1, fp) < 1)
		{
			TRACE("File Data Read Fail..\n");					
			fclose(fp);
			fp = NULL;
			return -21;	
		}
	}

	if(nFileVersion >= atol(RESULT_FILE_VER2))
	{
		SENSOR_MAPPING_SAVE_TABLE sensorMap;
		if(fread(&sensorMap, sizeof(SENSOR_MAPPING_SAVE_TABLE), 1, fp) <1)
		{
			fclose(fp);		fp = NULL;
			return -20;
		}
		memcpy(m_SensorMap1, sensorMap.mapData1, sizeof(_MAPPING_DATA)*EP_MAX_SENSOR_CH);
		memcpy(m_SensorMap2, sensorMap.mapData2, sizeof(_MAPPING_DATA)*EP_MAX_SENSOR_CH);
	}

	//Read test condition
	STR_CONDITION *pConditionMain = new STR_CONDITION;
	ASSERT(pConditionMain);
	if(ReadTestConditionFile(fp, pConditionMain, m_fileHaeder.szFileID, m_fileHaeder.szFileVersion) == FALSE)		//Read Tested Condition
	{
		RemoveCondition(pConditionMain); 
		fclose(fp);
		fp = NULL;
		return -8;
	}
	m_TestCondition.SetProcedure(pConditionMain);
	RemoveCondition(pConditionMain); 

	STR_SAVE_CH_DATA chReadChBuff, *lpReadChData;
	EP_GROUP_INFO	rdBuffer;
	STR_STEP_RESULT	*pStepResultData;
	EP_GP_DATA		rdBoardBuff;
	GROUP_STATE_SAVE gpStateSave;

	int nStepCount = 0;
	int nChCount = m_sysData.nTotalChNo;
	if(nFileVersion >= atol(RESULT_FILE_VER2))
	{
		nChCount = m_sysData.awChInTray[m_resultFileHeader.wJigIndex];
	}

	while(1)
	{
		if(nFileVersion >= atol(RESULT_FILE_VER2))
		{
			if(fread(&gpStateSave, sizeof(GROUP_STATE_SAVE), 1, fp) <1)		
			{
				break;	//Read Group State Result
			}
		}
		else
		{
			if(fread(&rdBoardBuff, sizeof(EP_GP_DATA), 1, fp) <1)		
			{
				break;	//Read Group State Result
			}
		}

		pStepResultData = new STR_STEP_RESULT;
		ASSERT(pStepResultData);

		// 1. ContackChk 데이터와 Formation 데이터 분류
		m_stepConData.Add(pStepResultData);
		
		if(nFileVersion >= atol(RESULT_FILE_VER2))
		{
			memcpy(&pStepResultData->gpStateSave, &gpStateSave, sizeof(GROUP_STATE_SAVE));
		}
		else
		{
			pStepResultData->gpStateSave.gpState.state = rdBoardBuff.gpState.state;
			pStepResultData->gpStateSave.gpState.failCode = rdBoardBuff.gpState.failCode;
		}

		if(fread(&pStepResultData->stepHead, sizeof(RESULT_STEP_HEADER), 1, fp) < 1)
		{
			fclose(fp);
			fp = NULL;
			return -12;
		}	
		
		if(fread(&pStepResultData->stepCondition, sizeof(STR_COMMON_STEP), 1, fp) < 1)
		{
			fclose(fp);
			fp = NULL;
			return -11;
		}
	
		EP_STEP_END_HEADER endHaeder;
		if(fread(&endHaeder, sizeof(EP_STEP_END_HEADER), 1, fp) < 1)
		{
			fclose(fp);
			fp = NULL;
			return -10;
		}

		pStepResultData->nNormalCount = 0;
		pStepResultData->nInputCount = 0;

		int i=0;

		for( i =0; i<nChCount ; i++ )		//0->를 각 구룹의 채널 수로 입력해야 한다.
		{
			if(fread(&chReadChBuff, sizeof(STR_SAVE_CH_DATA), 1, fp) <1)				//Read Channel Result 
			{
				fclose(fp);
				fp = NULL;
				return -9;
			}

			lpReadChData = new STR_SAVE_CH_DATA;
			memcpy(lpReadChData, &chReadChBuff, sizeof(STR_SAVE_CH_DATA));
			pStepResultData->aChData.Add(lpReadChData);

			if(::IsNormalCell(lpReadChData->channelCode))	
			{
				pStepResultData->averageData[EP_TIME_BIT_POS].AddData(lpReadChData->wChIndex, lpReadChData->fStepTime);
				pStepResultData->averageData[EP_VOLTAGE_BIT_POS].AddData(lpReadChData->wChIndex, lpReadChData->fVoltage);
				pStepResultData->averageData[EP_CURRENT_BIT_POS].AddData(lpReadChData->wChIndex, lpReadChData->fCurrent);
				pStepResultData->averageData[EP_CAPACITY_BIT_POS].AddData(lpReadChData->wChIndex, lpReadChData->fCapacity);
				pStepResultData->averageData[EP_WATT_BIT_POS].AddData(lpReadChData->wChIndex, lpReadChData->fWatt);
				pStepResultData->averageData[EP_WATT_HOUR_BIT_POS].AddData(lpReadChData->wChIndex, lpReadChData->fWattHour);
				pStepResultData->averageData[EP_IMPEDANCE_BIT_POS].AddData(lpReadChData->wChIndex, lpReadChData->fImpedance);
				pStepResultData->nNormalCount++;
			}

			if(::IsNonCell(lpReadChData->channelCode) == FALSE)
			{
				pStepResultData->nInputCount++;
			}
			
		}
		nStepCount++;
	}

	//파일 Close
	if(fp != NULL)
	{
		fclose(fp);
		fp = NULL;
	}

	return 1;
}

//주어진 파일을 읽어 들인다.
int CFormResultFile::ReadFile(CString strFileName, BOOL bSumCapa)
{	
	ClearResult();			//Clear Result Data Structure

	if(strFileName.IsEmpty())	return FALSE;
	m_strFileName = strFileName;
	m_bSumCapacity = bSumCapa;
		
	FILE *fp = fopen((LPCTSTR)m_strFileName, "rb");
	if(fp == NULL)				return -1;

	//File Header Read
	fread(&m_fileHaeder, sizeof(EP_FILE_HEADER), 1, fp);
	int nFileVersion = atol(m_fileHaeder.szFileVersion);	//File Version

	//Formation Result File Read
	if(strcmp(m_fileHaeder.szFileID, RESULT_FILE_ID) == 0)		
	{
		if( nFileVersion <= 0 || nFileVersion > atol(RESULT_FILE_VER3))
		{
			AfxMessageBox(TEXT_LANG[0]);//"지원하지 않는 Version의 파일입니다."
			fclose(fp);
			fp = NULL;
			return -2;
		}
	}
	else if(strcmp(m_fileHaeder.szFileID, IROCV_FILE_ID) == 0)	//IROCV Result File Read
	{
// 		if( nFileVersion < 0 || nFileVersion > atol(IROCV_FILE_VER2))
// 		{
// 			fclose(fp);
// 			fp = NULL;
// 			return -3;
// 		}
	}
	else if(strcmp(m_fileHaeder.szFileID, PRODUCT_RESULT_FILE_ID) == 0)
	{

	}
	else	//알수 없는 파일 
	{
//		AfxMessageBox("PNE Cell test system의 결과 파일이 아닙니다.");
		fclose(fp);
		fp = NULL;
		return -4;
	}

	//Test 결과 Header를 읽는다.
	if(fread(&m_resultFileHeader, sizeof(RESULT_FILE_HEADER), 1, fp)<1)	//Read Test Result Header
	{
		TRACE("Test Group File header Read Fail..\n");
		fclose(fp);
		fp = NULL;
		return -5;	//File Header Read
	}

	//시스템 Parameter를 읽는다.
//	if(nFileVersion >= atol(RESULT_FILE_VER2))
//	{
		if(fread(&m_sysData, sizeof(EP_MD_SYSTEM_DATA), 1, fp)<1)			//Read System Param
		{
			TRACE("Test Group File header Read Fail..\n");
			fclose(fp);
			fp = NULL;
			return -6;	//File Header Read
		}
/*	}
	else
	{
		EP_MD_SYSTEM_DATA_V0	sData;
		if(fread(&sData, sizeof(EP_MD_SYSTEM_DATA_V0), 1, fp)<1)			//Read System Param
		{
			TRACE("Test Group File header Read Fail..\n");
			fclose(fp);
			fp = NULL;
			return -6;	//File Header Read
		}

		m_sysData.nModuleID = sData.nModuleID;
		m_sysData.nVersion = sData.nVersion;						//2004/4/8 UINT	nVersion에서 변경 unsigned int :System Version 
		m_sysData.nControllerType = sData.nControllerType;			//SBC Type	0: HS-6637, 1: WEB-6580, 2:PCM-5864
		m_sysData.wInstalledBoard = sData.wInstalledBoard;			//unsigned short : Total Board Number in one Modlule
		m_sysData.wChannelPerBoard = sData.wChannelPerBoard;		//unsinged short : Total Channel Per Board
		m_sysData.nModuleGroupNo = 1;								//Group Number
		m_sysData.nTotalChNo = sData.awChInGroup[0];				//
		m_sysData.wTrayType = sData.wTrayType;						//Tray Type => LG 각형 256 : 2, LG 원통형 256: 1, SKC 128 : 3
		m_sysData.wTotalTrayNo = 1;									//Total insertable tray Number
		m_sysData.awChInTray[0] = sData.awChInTray[0];				//Channel in tray
	}
*/
	if(fread(&m_extData, sizeof(STR_FILE_EXT_SPACE), 1, fp) < 1)
	{
		TRACE("File Data Read Fail..\n");
				
		fclose(fp);
		fp = NULL;
		return -7;	//File Header Read
	}

	//Cell Serial 를 불러온다. kjh 20080714//////////////////////////////////////////////////////////////////////////
	if(strcmp(m_fileHaeder.szFileID, RESULT_FILE_ID) == 0 && nFileVersion >= atol(RESULT_FILE_VER3))
	{		
		if(fread(&m_ResultCellSerial, sizeof(PNE_RESULT_CELL_SERIAL), 1, fp) < 1)
		{
			TRACE("File Data Read Fail..\n");					
			fclose(fp);
			fp = NULL;
			return -21;	
		}
	}

	if(nFileVersion >= atol(RESULT_FILE_VER2))
	{
		SENSOR_MAPPING_SAVE_TABLE sensorMap;
		if(fread(&sensorMap, sizeof(SENSOR_MAPPING_SAVE_TABLE), 1, fp) <1)
		{
			fclose(fp);		fp = NULL;
			return -20;
		}
		memcpy(m_SensorMap1, sensorMap.mapData1, sizeof(_MAPPING_DATA)*EP_MAX_SENSOR_CH);
		memcpy(m_SensorMap2, sensorMap.mapData2, sizeof(_MAPPING_DATA)*EP_MAX_SENSOR_CH);
	}

	//Read test condition
	STR_CONDITION *pConditionMain = new STR_CONDITION;
	ASSERT(pConditionMain);
	if(ReadTestConditionFile(fp, pConditionMain, m_fileHaeder.szFileID, m_fileHaeder.szFileVersion) == FALSE)		//Read Tested Condition
	{
		RemoveCondition(pConditionMain); 
		fclose(fp);
		fp = NULL;
		return -8;
	}
	m_TestCondition.SetProcedure(pConditionMain);
	RemoveCondition(pConditionMain); 

	STR_SAVE_CH_DATA chReadChBuff, *lpReadChData;
	EP_GROUP_INFO	rdBuffer;
	STR_STEP_RESULT	*pStepResultData;
	EP_GP_DATA		rdBoardBuff;
//	EP_STEP_SUMMERY testSum;
	GROUP_STATE_SAVE gpStateSave;

	int nStepCount = 0;
	int nChCount = m_sysData.nTotalChNo;
	if(nFileVersion >= atol(RESULT_FILE_VER2))
	{
		nChCount = m_sysData.awChInTray[m_resultFileHeader.wJigIndex];
		//nChCount = m_extData.nCellCountInTray;
	}

	while(1)
	{
		if(nFileVersion >= atol(RESULT_FILE_VER2))
		{
			if(fread(&gpStateSave, sizeof(GROUP_STATE_SAVE), 1, fp) <1)		
			{
				break;	//Read Group State Result
			}
		}
		else
		{
			if(fread(&rdBoardBuff, sizeof(EP_GP_DATA), 1, fp) <1)		
			{
				break;	//Read Group State Result
			}
		}

		pStepResultData = new STR_STEP_RESULT;
		ASSERT(pStepResultData);
		m_stepData.Add(pStepResultData);
		
		if(nFileVersion >= atol(RESULT_FILE_VER2))
		{
			memcpy(&pStepResultData->gpStateSave, &gpStateSave, sizeof(GROUP_STATE_SAVE));
		}
		else
		{
			pStepResultData->gpStateSave.gpState.state = rdBoardBuff.gpState.state;
			pStepResultData->gpStateSave.gpState.failCode = rdBoardBuff.gpState.failCode;
		}

		if(fread(&pStepResultData->stepHead, sizeof(RESULT_STEP_HEADER), 1, fp) < 1)
		{
			fclose(fp);
			fp = NULL;
			return -12;
		}	
		
		if(fread(&pStepResultData->stepCondition, sizeof(STR_COMMON_STEP), 1, fp) < 1)
		{
			fclose(fp);
			fp = NULL;
			return -11;
		}
	
/*		if(nFileVersion < atol(RESULT_FILE_VER2))	//세방만 적용 
		{
			if(fread(&testSum, sizeof(EP_STEP_SUMMERY), 1, fp) <1)				//Read Channel Result 
			{
				fclose(fp);
				fp = NULL;
				return -10;
			}
		}
		else
		{
*/			EP_STEP_END_HEADER endHaeder;
			if(fread(&endHaeder, sizeof(EP_STEP_END_HEADER), 1, fp) < 1)
			{
				fclose(fp);
				fp = NULL;
				return -10;
			}
//		}

		pStepResultData->nNormalCount = 0;
		pStepResultData->nInputCount = 0;

		for(int i =0; i<nChCount ; i++)		//0->를 각 구룹의 채널 수로 입력해야 한다.
		{
			if(fread(&chReadChBuff, sizeof(STR_SAVE_CH_DATA), 1, fp) <1)				//Read Channel Result 
			{
				fclose(fp);
				fp = NULL;
				return -9;
			}

			lpReadChData = new STR_SAVE_CH_DATA;
			memcpy(lpReadChData, &chReadChBuff, sizeof(STR_SAVE_CH_DATA));
			pStepResultData->aChData.Add(lpReadChData);

			if(::IsNormalCell(lpReadChData->channelCode))	
			{
				pStepResultData->averageData[EP_TIME_BIT_POS].AddData(lpReadChData->wChIndex, lpReadChData->fStepTime);
				pStepResultData->averageData[EP_VOLTAGE_BIT_POS].AddData(lpReadChData->wChIndex, lpReadChData->fVoltage);
				pStepResultData->averageData[EP_CURRENT_BIT_POS].AddData(lpReadChData->wChIndex, lpReadChData->fCurrent);
				pStepResultData->averageData[EP_CAPACITY_BIT_POS].AddData(lpReadChData->wChIndex, lpReadChData->fCapacity);
				pStepResultData->averageData[EP_WATT_BIT_POS].AddData(lpReadChData->wChIndex, lpReadChData->fWatt);
				pStepResultData->averageData[EP_WATT_HOUR_BIT_POS].AddData(lpReadChData->wChIndex, lpReadChData->fWattHour);
				pStepResultData->averageData[EP_IMPEDANCE_BIT_POS].AddData(lpReadChData->wChIndex, lpReadChData->fImpedance);
				pStepResultData->nNormalCount++;
			}

			if(::IsNonCell(lpReadChData->channelCode) == FALSE)
			{
				pStepResultData->nInputCount++;
			}
			
		}
		nStepCount++;
	}
	
	//용량 합산 표기이면 용량 재계산 
	if(m_bSumCapacity)	CalculateCapa();

	if(m_bLocalGrading)	Regrading();

	//시험 조건을 재설정 
	if(strcmp(m_fileHaeder.szFileID, PRODUCT_RESULT_FILE_ID) == 0)
	{
		STR_CONDITION *pCondition = new STR_CONDITION;
		ASSERT(pCondition);

		memcpy(&pCondition->modelHeader, m_TestCondition.GetModelInfo(), sizeof(STR_CONDITION_HEADER));		
		memcpy(&pCondition->conditionHeader, m_TestCondition.GetTestInfo(), sizeof(STR_CONDITION_HEADER));	
		memcpy(&pCondition->checkParam, m_TestCondition.GetCheckParam(), sizeof(EP_PRETEST_PARAM));

		STR_COMMON_STEP *lpBuffer = NULL;
		pCondition->testHeader.totalStep = m_stepData.GetSize();
		
		//Result File의 File Ver이 RESULT_FILE_VER1인 것만 STEP_V1 structure로 되어 있다.
		for(int i =0; i<pCondition->testHeader.totalStep; i++)
		{
			pStepResultData = (STR_STEP_RESULT *)m_stepData.GetAt(i);
			lpBuffer = new STR_COMMON_STEP;
			pCondition->apStepList.Add(lpBuffer);
			memcpy(lpBuffer, &pStepResultData->stepCondition, sizeof(STR_COMMON_STEP));
		}
		
		m_TestCondition.SetProcedure(pCondition);
		RemoveCondition(pCondition); 
	}

	//파일 Close
	if(fp != NULL)
	{
		fclose(fp);
		fp = NULL;
	}

	return 1;
}

//Set Current file name
void CFormResultFile::SetFileName(CString strFileName)
{
	m_strFileName = strFileName;
}

//Return current File Name
CString CFormResultFile::GetFileName()
{
	return m_strFileName;
}

//Read condition Data From from file
int CFormResultFile::ReadTestConditionFile(FILE *fp, STR_CONDITION *pCondition, char *fileID, char *fileVer)
{
	if(fp == NULL || pCondition == NULL)	return FALSE;

	if(fread(&pCondition->modelHeader, sizeof(STR_CONDITION_HEADER), 1, fp) != 1)		return FALSE;
	if(fread(&pCondition->conditionHeader, sizeof(STR_CONDITION_HEADER), 1, fp) != 1)	return FALSE;
	if(fread(&pCondition->testHeader, sizeof(EP_TEST_HEADER), 1, fp) != 1)				return FALSE;
	if(fread(&pCondition->checkParam, sizeof(EP_PRETEST_PARAM), 1, fp) != 1)			return FALSE;

	STR_COMMON_STEP *lpBuffer = NULL;
	if(fileID != NULL && fileVer != NULL)	//공정 결과 파일인 경우 
	{
		//Result File의 File Ver이 RESULT_FILE_VER1인 것만 STEP_V1 structure로 되어 있다.
		for(int i =0; i<pCondition->testHeader.totalStep; i++)
		{
			lpBuffer = new STR_COMMON_STEP;
			if(fread((char *)lpBuffer, sizeof(STR_COMMON_STEP), 1, fp) != 1)
			{
				delete lpBuffer;
				return FALSE;
			}
			pCondition->apStepList.Add(lpBuffer);
		}	
	}
	else 
	{
		return FALSE;
	}

	return TRUE;
}

BOOL CFormResultFile::ClearConResult()
{	
	STR_SAVE_CH_DATA *lpChData;
	STR_STEP_RESULT * pStepData;

	for(int i = m_stepConData.GetSize()-1; i>= 0; i--)
	{
		if((pStepData = (STR_STEP_RESULT *)m_stepConData[i]) != NULL)
		{
			for(int j = pStepData->aChData.GetSize()-1; j>=0; j--)
			{
				if((lpChData = (STR_SAVE_CH_DATA *)pStepData->aChData[j]) != NULL)
				{
					delete lpChData;
					lpChData = NULL;
					pStepData->aChData.RemoveAt(j);
				}
			}
			pStepData->aChData.RemoveAll();
		}
		delete pStepData;
		pStepData = NULL;
		m_stepConData.RemoveAt(i);
	}	
	m_stepConData.RemoveAll();

	return TRUE;
}

BOOL CFormResultFile::ClearResult()
{	
//	for(int i = 0; i<EP_RESULT_ITEM_NO; i++)
//		m_calAverage[i].ResetData();

/*	if(m_pConditionMain != NULL)
	{
		if(RemoveCondition(m_pConditionMain))		//Free the Test Condition Memory
		{
			m_pConditionMain = NULL;
		}
	}
*/
	m_strFileName.Empty();

	m_TestCondition.ResetCondition();

	STR_SAVE_CH_DATA *lpChData;
	STR_STEP_RESULT * pStepData;

	for(int i = m_stepData.GetSize()-1; i>= 0; i--)
	{
		if((pStepData = (STR_STEP_RESULT *)m_stepData[i]) != NULL)
		{
			for(int j = pStepData->aChData.GetSize()-1; j>=0; j--)
			{
				if((lpChData = (STR_SAVE_CH_DATA *)pStepData->aChData[j]) != NULL)
				{
					delete lpChData;
					lpChData = NULL;
					pStepData->aChData.RemoveAt(j);
				}
			}
			pStepData->aChData.RemoveAll();
		}
		delete pStepData;
		pStepData = NULL;
		m_stepData.RemoveAt(i);
	}	
	m_stepData.RemoveAll();

	return TRUE;
}

//Remove Test Condition List
BOOL CFormResultFile::RemoveCondition(STR_CONDITION *pCondition)
{
	if(pCondition == NULL)	return TRUE;

	int nSize = pCondition->apStepList.GetSize();
	char *pObject; 

	if(nSize > 0)
	{
		for (int i = nSize-1 ; i>=0 ; i--)
		{
			if( (pObject = (char *)pCondition->apStepList[i]) != NULL )
			{
				delete[] pObject; // Delete the original element at 0.
				pObject = NULL;
				pCondition->apStepList.RemoveAt(i);  
			}
		}
		pCondition->apStepList.RemoveAll();
	}

	delete pCondition;
	pCondition = NULL;

	return TRUE;
}


EP_FILE_HEADER * CFormResultFile::GetFileHeader()
{
	ASSERT(!m_strFileName.IsEmpty());
	return &m_fileHaeder;
}

RESULT_FILE_HEADER * CFormResultFile::GetResultHeader()
{
	ASSERT(!m_strFileName.IsEmpty());
	return &m_resultFileHeader;
}

EP_MD_SYSTEM_DATA * CFormResultFile::GetMDSysData()
{
	ASSERT(!m_strFileName.IsEmpty());
	return &m_sysData;
}

STR_CONDITION_HEADER * CFormResultFile::GetModelData()
{
	ASSERT(!m_strFileName.IsEmpty());
	
	return m_TestCondition.GetModelInfo();
}

STR_STEP_RESULT * CFormResultFile::GetConStepData()
{
	if(m_stepConData.GetSize() < 1 )
	{
		return NULL;
	}

	STR_STEP_RESULT *pStepData;	
	pStepData = (STR_STEP_RESULT *)m_stepConData[0];
	return pStepData;
}

STR_STEP_RESULT * CFormResultFile::GetStepData(UINT nStepIndex)
{
	ASSERT(!m_strFileName.IsEmpty());

	if(m_stepData.GetSize() < 1 )
	{
		return NULL;
	}
	
	//Edited by KBH 2006/3/22
	//단순 Index 순서로 넘기는 것이 아니라
	//이전 step이 저장되어 있지 않을 수 있으므로 step index를 비교하여 일치하면 return

	//return (STR_STEP_RESULT *)m_stepData[nStepIndex];


	//20200229 KSJ (스탭중복저장시 가장 뒤에꺼 읽어오게 수정)
	STR_STEP_RESULT *pStepData = NULL;	
	for(int i=0; i<GetStepSize(); i++)
	{
		STR_STEP_RESULT *pTempData = (STR_STEP_RESULT *)m_stepData[i];
		if( pTempData->stepCondition.stepHeader.stepIndex == nStepIndex)	
		{
			pStepData = pTempData;
		}
	}

	return	pStepData;
}

STR_STEP_RESULT * CFormResultFile::GetStepIndexData(UINT nStepIndex)
{
	ASSERT(!m_strFileName.IsEmpty());

	if(m_stepData.GetSize() < 1 || nStepIndex >= m_stepData.GetSize())
		return NULL;
	
	STR_STEP_RESULT *pStepData;
	pStepData = (STR_STEP_RESULT *)m_stepData[nStepIndex];
	return pStepData;
}

int CFormResultFile::GetStepSize()
{
	return m_stepData.GetSize();
}

int CFormResultFile::GetFileVersion()
{
	ASSERT(!m_strFileName.IsEmpty());
	return atoi(m_fileHaeder.szFileVersion);
}

int CFormResultFile::GetModuleID()
{
	ASSERT(!m_strFileName.IsEmpty());
	return m_resultFileHeader.nModuleID;
}


int CFormResultFile::GetGroupIndex()
{
	ASSERT(!m_strFileName.IsEmpty());
	return m_resultFileHeader.wGroupIndex;
}


CString CFormResultFile::GetLotNo()
{
	ASSERT(!m_strFileName.IsEmpty());
	return m_resultFileHeader.szLotNo;
}

CString CFormResultFile::GetTestSerialNo()
{
	ASSERT(!m_strFileName.IsEmpty());
	return m_resultFileHeader.szTestSerialNo;
}

CString CFormResultFile::GetTrayNo()
{
	ASSERT(!m_strFileName.IsEmpty());
	return m_resultFileHeader.szTrayNo;

}

CString CFormResultFile::GetTypeSel()
{
	ASSERT(!m_strFileName.IsEmpty());
	return m_resultFileHeader.szTypeSel;

}

int CFormResultFile::GetTotalCh()
{
	ASSERT(!m_strFileName.IsEmpty());
	if(m_resultFileHeader.wGroupIndex < 0 ||  m_resultFileHeader.wGroupIndex >= EP_MAX_BD_PER_MD)	return 0;
	
	int nChCount = m_sysData.nTotalChNo;
	
	if(atol(m_fileHaeder.szFileVersion)  >= atol(RESULT_FILE_VER2))
	{
		nChCount = m_sysData.awChInTray[m_resultFileHeader.wJigIndex];
		//nChCount = m_extData.nCellCountInTray;
	}
	return nChCount;
}

STR_FILE_EXT_SPACE * CFormResultFile::GetExtraData()
{
	ASSERT(!m_strFileName.IsEmpty());
	return &m_extData;
}

CString CFormResultFile::GetTestName()
{
	ASSERT(!m_strFileName.IsEmpty());
	CString name(m_TestCondition.GetTestName());
	return name;
}

CString CFormResultFile::GetModelName()
{
	ASSERT(!m_strFileName.IsEmpty());
	CString name(m_TestCondition.GetModelInfo()->szName);
	return name;
}

/*
STR_CONDITION * CFormResultFile::GetConditionMain()
{
	return (STR_CONDITION *)m_pConditionMain;
}
*/

//파일을 Load 했는지 여부 확인
BOOL CFormResultFile::IsLoaded()
{
	if(m_strFileName.IsEmpty())		return FALSE;

	return TRUE;
}

STR_STEP_RESULT * CFormResultFile::GetFirstStepData()
{
	ASSERT(!m_strFileName.IsEmpty());
	if(m_stepData.GetSize() < 1)
		return NULL;
	
	return (STR_STEP_RESULT *)m_stepData[0];
}

//1. Step을 조사하여 충전이 연속되거나 방전이 연속되면 용량을 합산하여 표시 한다. Rest는 무시한다.
//2. 충전후 방전하게 되면 용량 효율을 계산한다.
BOOL CFormResultFile::CalculateCapa()
{
	if(IsLoaded() == FALSE)	return FALSE;
	
	STR_STEP_RESULT *lpStepData;
	STR_SAVE_CH_DATA *lpChData;

	float fSumCapa[512] = {0.0f,};
	float fChargeCapa[512] = {0.0f,};
	int nChCount;

	BYTE	preStepType = EP_TYPE_NONE;
	BOOL	bCycleChanged = FALSE;

	//Step을 조사하여 충전이 연속되거나 방전이 연속되면 용량을 합산하여 표시 한다.
	//Rest는 무시한다.
	for(int step = 0; step<GetStepSize(); step++)
	{
		lpStepData = (STR_STEP_RESULT *)m_stepData[step];
		nChCount = lpStepData->aChData.GetSize();
		
		switch(lpStepData->stepCondition.stepHeader.type)
		{
		case EP_TYPE_CHARGE:
			if(preStepType == EP_TYPE_CHARGE)	//이전 step이 충전 이였으면 
			{
				for(int ch =0; ch<nChCount; ch++)
				{
					lpChData = (STR_SAVE_CH_DATA *)lpStepData->aChData[ch];
					fSumCapa[ch] += lpChData->fCapacity;	//이전 Step 용량과 합산 
					lpChData->fCapacity = fSumCapa[ch];		//현재 Step 용량 Data를 이전 Step 합산 Data로 갱신 
					fChargeCapa[ch] = fSumCapa[ch];			//용량 효율 계산을 위해 충전 최종 용량 기록 
				}
			}
			else								//이전 step이 충전이 아니면 현재 용량만으로 초기화
			{
				for(int ch =0; ch<nChCount; ch++)
				{
					lpChData = (STR_SAVE_CH_DATA *)lpStepData->aChData[ch];
					fSumCapa[ch] = lpChData->fCapacity;
					fChargeCapa[ch] = fSumCapa[ch];
				}
			}
			bCycleChanged = FALSE;							//Cycle  Rest
			preStepType = EP_TYPE_CHARGE;					//이전 Step Type 갱신 
			break;

		case EP_TYPE_DISCHARGE:
			if(preStepType == EP_TYPE_DISCHARGE)
			{
				for(int ch =0; ch<nChCount; ch++)
				{
					lpChData = (STR_SAVE_CH_DATA *)lpStepData->aChData[ch];
					fSumCapa[ch] += lpChData->fCapacity;
					lpChData->fCapacity = fSumCapa[ch];
				}
			}
			else
			{
				for(int ch =0; ch<nChCount; ch++)
				{
					lpChData = (STR_SAVE_CH_DATA *)lpStepData->aChData[ch];
					fSumCapa[ch] = lpChData->fCapacity;
				}
			}
			
			//이전 Step에 충전이 있었으면 Cycle이 성립되고 Discharge가 연속 나와도 이전에 충전 
			//Step이 있었으면 성립한다고 봄 
			if(preStepType == EP_TYPE_CHARGE)	bCycleChanged = TRUE;		

			//Cycle이 성립되었으면 용량 효율 계산 
			if(bCycleChanged)
			{
				for(int ch = 0; ch<nChCount; ch++)
				if(fSumCapa[ch] > 0.0f)
				{
					lpChData = (STR_SAVE_CH_DATA *)lpStepData->aChData[ch];
					//방전의 경우 Channel의 total Time에 용량 효율을 계산하여 저장한다.(*100 된 Data)
					lpChData->fTotalTime = fSumCapa[ch]/fChargeCapa[ch]*10000.0f;
				}
			}
			else	//Cycle이 성립되지 않으면 0으로 Setting
			{
				for(int ch = 0; ch<nChCount; ch++)
				{	lpChData = (STR_SAVE_CH_DATA *)lpStepData->aChData[ch];
					lpChData->fTotalTime = 0;
				}
			}
			preStepType = EP_TYPE_DISCHARGE;
			break;

		case EP_TYPE_REST:		//Rest는 Step 사이에 있어도 무시 
			break;

		default:				//나머지 Step에 대해서는 연속적이지 않다고 봄 
			bCycleChanged = FALSE;
			preStepType = lpStepData->stepCondition.stepHeader.type;
			ZeroMemory(fSumCapa, sizeof(fSumCapa));
			break;
		}
	}
	return TRUE;
}

void CFormResultFile::SetCapacitySum(BOOL bSum)
{
	m_bSumCapacity = bSum;
}

//Tray배열 순성의 chIndex
WORD CFormResultFile::GetLastTrayChCellCode(int nChIndex)
{
	WORD code = EP_CODE_NORMAL;		//1개의 Step도 진행하지 않았을 경우
	if(nChIndex < 0)	code = EP_CODE_NONCELL;

	int size = m_stepData.GetSize();
	if(size > 0)
	{
		STR_STEP_RESULT *lpStepData = (STR_STEP_RESULT *)m_stepData[size-1];
		if(lpStepData)
		{
			if(lpStepData->aChData.GetSize() >0 && nChIndex >= 0 && nChIndex <lpStepData->aChData.GetSize())
			{
				STR_SAVE_CH_DATA *lpChData = (STR_SAVE_CH_DATA *)lpStepData->aChData[nChIndex];
				if(lpChData)
				{
					TRACE("Ch %d last code is %d\n", nChIndex+1, lpChData->channelCode);
					return lpChData->channelCode;
				}
			}
		}
	}
	return code;
}

//모듈의 channel 순서의 해당 Channel을 찾는다.
WORD CFormResultFile::GetLastModuleChCellCode(int nChIndex)
{
	WORD code = EP_CODE_NORMAL;		//1개의 Step도 진행하지 않았을 경우
	if(nChIndex < 1 || ConvertTrayCh2ModuleCh(nChIndex+1) < 1)		//현재  Channel이 모듈에 존재하지 않음 
	{
		code = EP_CODE_NONCELL;
	}

	int size = m_stepData.GetSize();
	if(size > 0)
	{
		//최종 Step을 정보를 구함 
		STR_STEP_RESULT *lpStepData = (STR_STEP_RESULT *)m_stepData[size-1];
		if(lpStepData)
		{
			STR_SAVE_CH_DATA *lpChData;
			for(int ch = 0; ch<lpStepData->aChData.GetSize(); ch++)
			{
				lpChData = (STR_SAVE_CH_DATA *)lpStepData->aChData[ch];
				if(lpChData->wModuleChIndex == nChIndex)
				{
					return lpChData->channelCode;
				}
			}
		}
	}	
	return code;
}

STR_STEP_RESULT * CFormResultFile::GetLastStepData()
{
	return GetStepData(GetStepSize()-1);
}

/*
MAPPING_DATA * CFormResultFile::GetSensorMap(int nSensorIndex)
{
	if(nSensorIndex == 0)
	{
		return m_SensorMap1;
	}
	return m_SensorMap2;
}
*/

//해당 Channel에 mapping된 Sensor channel을 찾는다.
BOOL CFormResultFile::FindMappingSensorCh(int nChTrayNo/*1Base*/, CWordArray &awMappingCh, int nSensorType, int nStepNo/*1 Base*/)
{
	int nModuleCh = ConvertTrayCh2ModuleCh(nChTrayNo);
	awMappingCh.RemoveAll();
	for(int s = 0; s<EP_MAX_SENSOR_CH; s++)
	{
		if(nSensorType == sensorType1)
		{
			if(	nModuleCh == m_SensorMap1[s].nChannelNo	
				|| m_SensorMap1[s].nChannelNo == 0)		// -1 : Not use, 0 : Module Use, 1~ : Module channel no
			{
				awMappingCh.Add(s+1);
			}
		}
		else if(nSensorType == sensorType2)
		{
			if(nModuleCh == m_SensorMap2[s].nChannelNo
				|| m_SensorMap2[s].nChannelNo == 0)
			{
				awMappingCh.Add(s+1);
			}
		}
	}
	return TRUE;
}

CString CFormResultFile::GetSensorName(int nSensorChIndex, int nSensorType)
{
	CString str;
	if(nSensorChIndex >=0 && nSensorChIndex<EP_MAX_SENSOR_CH)
	{
		if(nSensorType == sensorType1)
		{
			str = m_SensorMap1[nSensorChIndex].szName;
		}
		else if(nSensorType == sensorType2)
		{
			str = m_SensorMap2[nSensorChIndex].szName;
		}
	}
	return str;
}

//현재 결과 파일에서 가장 마지막 1Step Data를 Loading한다.
BOOL CFormResultFile::UpdateReadFile()
{
	if(m_strFileName.IsEmpty())		return FALSE;

	if(GetStepSize() < 1)
	{
		return ReadFile(m_strFileName);
	}

	//Step Size
	FILE *fp = fopen((LPCTSTR)m_strFileName, "rb");
	if(fp == NULL)				return -1;

	//File Header Read
	fread(&m_fileHaeder, sizeof(EP_FILE_HEADER), 1, fp);
	int nFileVersion = atol(m_fileHaeder.szFileVersion);	//File Version

	int nChCount = m_sysData.nTotalChNo;
	if(nFileVersion >= atol(RESULT_FILE_VER2))
	{
		nChCount = m_sysData.awChInTray[m_resultFileHeader.wJigIndex];
		//nChCount = m_extData.nCellCountInTray;
	}

	//Header
	int nPointSize = 0;		
	nPointSize += sizeof(EP_FILE_HEADER);
	nPointSize += sizeof(RESULT_FILE_HEADER);
	nPointSize += sizeof(EP_MD_SYSTEM_DATA);
	nPointSize += sizeof(STR_FILE_EXT_SPACE);
	if(nFileVersion >= atol(RESULT_FILE_VER3))
	{
		nPointSize += sizeof(PNE_RESULT_CELL_SERIAL);
	}
	nPointSize += sizeof(SENSOR_MAPPING_SAVE_TABLE);
	nPointSize += sizeof(STR_CONDITION_HEADER);
	nPointSize += sizeof(STR_CONDITION_HEADER);
	nPointSize += sizeof(EP_TEST_HEADER);
	nPointSize += sizeof(EP_PRETEST_PARAM);
	nPointSize += (sizeof(STR_COMMON_STEP)*m_TestCondition.GetTotalStepNo());


	int nStepSize = 0;
	int nStepHeadSize = 0;
	if(nFileVersion >= atol(RESULT_FILE_VER2))
	{
		nStepSize += sizeof(GROUP_STATE_SAVE);
	}
	else
	{
		nStepSize += sizeof(EP_GP_DATA);
	}

	nStepSize += sizeof(RESULT_STEP_HEADER);
	nStepHeadSize = nStepSize;

	nStepSize += sizeof(STR_COMMON_STEP);

/*	if(nFileVersion < atol(RESULT_FILE_VER2))	//세방만 적용 
	{
		nStepSize += sizeof(EP_STEP_SUMMERY);
	}
	else
	{
*/		nStepSize += sizeof(EP_STEP_END_HEADER);
//	}
	nStepSize = nStepSize + ( sizeof(STR_SAVE_CH_DATA) * nChCount );
	//////////////////////////////////////////////////////////////////////////
	
	STR_SAVE_CH_DATA chReadChBuff, *lpReadChData;
	EP_GROUP_INFO	rdBuffer;
	STR_STEP_RESULT	*pStepResultData;
	EP_GP_DATA		rdBoardBuff;
//	EP_STEP_SUMMERY testSum;
	GROUP_STATE_SAVE gpStateSave;

	int nTotStep = GetStepSize();
	int nOffset = nPointSize + (nStepSize * nTotStep);
	fseek(fp, nOffset, SEEK_SET);
	
	int nStepCount = 0;
	while(1)
	{				
		if(nFileVersion >= atol(RESULT_FILE_VER2))
		{
			if(fread(&gpStateSave, sizeof(GROUP_STATE_SAVE), 1, fp) <1)
			{
				break;	//Read Group State Result
				/*
				if( nStepCount == nTotStep )
				{
					break;	//Read Group State Result
				}
				else
				{
					nOffset += nStepCount * ( sizeof(GROUP_STATE_SAVE) + sizeof(RESULT_FILE_HEADER) + sizeof(STR_COMMON_STEP) + sizeof(EP_STEP_END_HEADER) + ( sizeof(STR_SAVE_CH_DATA) * nChCount ) );
					fseek(fp, nOffset, SEEK_SET);
					nStepCount++;
					continue;
				}
				*/
			}
		}
		else
		{
			if(fread(&rdBoardBuff, sizeof(EP_GP_DATA), 1, fp) <1)
			{
				break;	//Read Group State Result
			}
		}

		pStepResultData = new STR_STEP_RESULT;
		ASSERT(pStepResultData);
		m_stepData.Add(pStepResultData);
		
		if(nFileVersion >= atol(RESULT_FILE_VER2))
		{
			memcpy(&pStepResultData->gpStateSave, &gpStateSave, sizeof(GROUP_STATE_SAVE));
		}
		else
		{
			pStepResultData->gpStateSave.gpState.state = rdBoardBuff.gpState.state;
			pStepResultData->gpStateSave.gpState.failCode = rdBoardBuff.gpState.failCode;
		}

		if(fread(&pStepResultData->stepHead, sizeof(RESULT_STEP_HEADER), 1, fp) < 1)
		{
			fclose(fp);
			fp = NULL;
			return -12;
		}	
		
		if(fread(&pStepResultData->stepCondition, sizeof(STR_COMMON_STEP), 1, fp) < 1)
		{
			fclose(fp);
			fp = NULL;
			return -11;
		}
	
/*		if(nFileVersion < atol(RESULT_FILE_VER2))	//세방만 적용 
		{
			if(fread(&testSum, sizeof(EP_STEP_SUMMERY), 1, fp) <1)				//Read Channel Result 
			{
				fclose(fp);
				fp = NULL;
				return -10;
			}
		}
		else
		{
*/			EP_STEP_END_HEADER endHaeder;
			if(fread(&endHaeder, sizeof(EP_STEP_END_HEADER), 1, fp) < 1)
			{
				fclose(fp);
				fp = NULL;
				return -10;
			}
//		}

		pStepResultData->nNormalCount = 0;
		pStepResultData->nInputCount = 0;

		for(int i =0; i<nChCount ; i++)		//0->를 각 구룹의 채널 수로 입력해야 한다.
		{
			if(fread(&chReadChBuff, sizeof(STR_SAVE_CH_DATA), 1, fp) <1)				//Read Channel Result 
			{
				fclose(fp);
				fp = NULL;
				return -9;
			}

			lpReadChData = new STR_SAVE_CH_DATA;
			memcpy(lpReadChData, &chReadChBuff, sizeof(STR_SAVE_CH_DATA));
			pStepResultData->aChData.Add(lpReadChData);

			if(::IsNormalCell(lpReadChData->channelCode))	
			{
				pStepResultData->averageData[EP_TIME_BIT_POS].AddData(lpReadChData->wChIndex, lpReadChData->fStepTime);
				pStepResultData->averageData[EP_VOLTAGE_BIT_POS].AddData(lpReadChData->wChIndex, lpReadChData->fVoltage);
				pStepResultData->averageData[EP_CURRENT_BIT_POS].AddData(lpReadChData->wChIndex, lpReadChData->fCurrent);
				pStepResultData->averageData[EP_CAPACITY_BIT_POS].AddData(lpReadChData->wChIndex, lpReadChData->fCapacity);
				pStepResultData->averageData[EP_WATT_BIT_POS].AddData(lpReadChData->wChIndex, lpReadChData->fWatt);
				pStepResultData->averageData[EP_WATT_HOUR_BIT_POS].AddData(lpReadChData->wChIndex, lpReadChData->fWattHour);
				pStepResultData->averageData[EP_IMPEDANCE_BIT_POS].AddData(lpReadChData->wChIndex, lpReadChData->fImpedance);
				pStepResultData->nNormalCount++;
			}

			if(::IsNonCell(lpReadChData->channelCode) == FALSE)
			{
				pStepResultData->nInputCount++;
			}
			
		}
		nStepCount++;
		TRACE("Step end data added\n");
	}
	
	//용량 합산 표기이면 용량 재계산 
	if(m_bSumCapacity)	CalculateCapa();

	//파일 Close
	if(fp != NULL)
	{
		fclose(fp);
		fp = NULL;
	}

	return TRUE;
}

/*
//현재 결과 파일에서 빠진 Step Data를 Loading한다.
BOOL CFormResultFile::UpdateReadFile()
{
	if(m_strFileName.IsEmpty())		return FALSE;

	//Header
	int nPointSize = 0;		
	nPointSize += sizeof(EP_FILE_HEADER);
	nPointSize += sizeof(RESULT_FILE_HEADER);
	nPointSize += sizeof(EP_MD_SYSTEM_DATA);
	nPointSize += sizeof(STR_FILE_EXT_SPACE);
	nPointSize += sizeof(STR_CONDITION_HEADER);
	nPointSize += sizeof(STR_CONDITION_HEADER);
	nPointSize += sizeof(EP_TEST_HEADER);
	nPointSize += sizeof(EP_PRETEST_PARAM);
	nPointSize += (sizeof(STR_COMMON_STEP)*m_TestCondition.GetTotalStepNo());

	//Step Size
	FILE *fp = fopen((LPCTSTR)m_strFileName, "rb");
	if(fp == NULL)				return -1;

	//File Header Read
	fread(&m_fileHaeder, sizeof(EP_FILE_HEADER), 1, fp);
	int nFileVersion = atol(m_fileHaeder.szFileVersion);	//File Version

	int nChCount = m_sysData.nTotalChNo;
	if(nFileVersion >= atol(RESULT_FILE_VER2))
	{
		//nChCount = m_sysData.awChInTray[m_resultFileHeader.nGroupIndex];
		nChCount = m_extData.nCellCountInTray;
	}

	int nStepSize = 0;
	int nStepHeadSize = 0;
	if(nFileVersion >= atol(RESULT_FILE_VER2))
	{
		nStepSize += sizeof(GROUP_STATE_SAVE);
	}
	else
	{
		nStepSize += sizeof(EP_GP_DATA);
	}

	nStepSize += sizeof(RESULT_FILE_HEADER);
	nStepHeadSize = nStepSize;

	nStepSize += sizeof(STR_COMMON_STEP);

	if(nFileVersion < atol(RESULT_FILE_VER2))	//세방만 적용 
	{
		nStepSize += sizeof(EP_STEP_SUMMERY);
	}
	else
	{
		nStepSize += sizeof(EP_STEP_END_HEADER);
	}
	nStepSize = nStepSize + ( sizeof(STR_SAVE_CH_DATA) * nChCount );
	//////////////////////////////////////////////////////////////////////////
	
	STR_SAVE_CH_DATA chReadChBuff, *lpReadChData;
	EP_GROUP_INFO	rdBuffer;
	STR_STEP_RESULT	*pStepResultData;
	EP_GP_DATA		rdBoardBuff;
	EP_STEP_SUMMERY testSum;
	GROUP_STATE_SAVE gpStateSave;

	int nTotStep = GetStepSize();
	int nOffset = nPointSize + nStepSize*nTotStep;
	fseek(fp, nOffset, SEEK_SET);
	
	int nStepCount = 0;
	while(1)
	{			
		if(nFileVersion >= atol(RESULT_FILE_VER2))
		{
			if(fread(&gpStateSave, sizeof(GROUP_STATE_SAVE), 1, fp) <1)		break;	//Read Group State Result
		}
		else
		{
			if(fread(&rdBoardBuff, sizeof(EP_GP_DATA), 1, fp) <1)		break;	//Read Group State Result
		}

		pStepResultData = new STR_STEP_RESULT;
		ASSERT(pStepResultData);
		m_stepData.Add(pStepResultData);
		
		if(nFileVersion >= atol(RESULT_FILE_VER2))
		{
			memcpy(&pStepResultData->gpStateSave, &gpStateSave, sizeof(GROUP_STATE_SAVE));
		}
		else
		{
			pStepResultData->gpStateSave.gpState.state = rdBoardBuff.gpState.state;
			pStepResultData->gpStateSave.gpState.failCode = rdBoardBuff.gpState.failCode;
		}

		if(fread(&pStepResultData->stepHead, sizeof(RESULT_FILE_HEADER), 1, fp) < 1)
		{
			fclose(fp);
			fp = NULL;
			return -12;
		}	
		
		if(fread(&pStepResultData->stepCondition, sizeof(STR_COMMON_STEP), 1, fp) < 1)
		{
			fclose(fp);
			fp = NULL;
			return -11;
		}
	
		if(nFileVersion < atol(RESULT_FILE_VER2))	//세방만 적용 
		{
			if(fread(&testSum, sizeof(EP_STEP_SUMMERY), 1, fp) <1)				//Read Channel Result 
			{
				fclose(fp);
				fp = NULL;
				return -10;
			}
		}
		else
		{
			EP_STEP_END_HEADER endHaeder;
			if(fread(&endHaeder, sizeof(EP_STEP_END_HEADER), 1, fp) < 1)
			{
				fclose(fp);
				fp = NULL;
				return -10;
			}
		}

		pStepResultData->nNormalCount = 0;
		pStepResultData->nInputCount = 0;

		for(int i =0; i<nChCount ; i++)		//0->를 각 구룹의 채널 수로 입력해야 한다.
		{
			if(fread(&chReadChBuff, sizeof(STR_SAVE_CH_DATA), 1, fp) <1)				//Read Channel Result 
			{
				fclose(fp);
				fp = NULL;
				return -9;
			}

			lpReadChData = new STR_SAVE_CH_DATA;
			memcpy(lpReadChData, &chReadChBuff, sizeof(STR_SAVE_CH_DATA));
			pStepResultData->aChData.Add(lpReadChData);

			if(::IsNormalCell(lpReadChData->channelCode))	
			{
				pStepResultData->averageData[EP_TIME_BIT_POS].AddData(lpReadChData->wChIndex, lpReadChData->fStepTime);
				pStepResultData->averageData[EP_VOLTAGE_BIT_POS].AddData(lpReadChData->wChIndex, lpReadChData->fVoltage);
				pStepResultData->averageData[EP_CURRENT_BIT_POS].AddData(lpReadChData->wChIndex, lpReadChData->fCurrent);
				pStepResultData->averageData[EP_CAPACITY_BIT_POS].AddData(lpReadChData->wChIndex, lpReadChData->fCapacity);
				pStepResultData->averageData[EP_WATT_BIT_POS].AddData(lpReadChData->wChIndex, lpReadChData->fWatt);
				pStepResultData->averageData[EP_WATT_HOUR_BIT_POS].AddData(lpReadChData->wChIndex, lpReadChData->fWattHour);
				pStepResultData->averageData[EP_IMPEDANCE_BIT_POS].AddData(lpReadChData->wChIndex, lpReadChData->fImpedance);
				pStepResultData->nNormalCount++;
			}

			if(::IsNonCell(lpReadChData->channelCode) == FALSE)
			{
				pStepResultData->nInputCount++;
			}
			
		}
		nStepCount++;
		TRACE("Step end data added\n");
	}
	
	//용량 합산 표기이면 용량 재계산 
	if(m_bSumCapacity)	CalculateCapa();

	//파일 Close
	if(fp != NULL)
	{
		fclose(fp);
		fp = NULL;
	}

	return TRUE;
}
*/

//Module based channel no => Tray based channel no
int CFormResultFile::ConvertModuleCh2TrayCh(int nChNo)
{
	//Module system data에서 검색 (//첫전째 Step이 오지 않았을 경우)
	if(m_resultFileHeader.wJigIndex < 0 || m_resultFileHeader.wJigIndex>=16)	return 0;
	if(nChNo < 1)	return 0;

	int nCnt = 0;
	for(int i=0; i<m_resultFileHeader.wJigIndex; i++)
	{
		nCnt += m_sysData.awChInTray[i];
	}
	if(nChNo <= nCnt || nChNo > nCnt+m_sysData.awChInTray[m_resultFileHeader.wJigIndex])	return 0;
	return nChNo-nCnt;

/*	STR_STEP_RESULT * pStepData = GetLastStepData();
	if(pStepData)
	{
		STR_SAVE_CH_DATA * pStepResult;
		for(int i =0; i<pStepData->aChData.GetSize(); i++)
		{
			pStepResult = (STR_SAVE_CH_DATA *)pStepData->aChData[i];

			if(pStepResult->wModuleChIndex == nChNo-1)
			{
				return pStepResult->wChIndex+1;
			}
		}
	}
*/
	return 0;
}

//Tray based channel no => Module based channel no
int CFormResultFile::ConvertTrayCh2ModuleCh(int nChNo)
{
	//첫번째 Step 결과가 오지 않았을 경우
	
	if(m_resultFileHeader.wJigIndex < 0 || m_resultFileHeader.wJigIndex>=16)	return 0;
	if(nChNo < 1 || nChNo > m_sysData.awChInTray[m_resultFileHeader.wJigIndex])	return 0;

	int nCnt = 0;
	for(int i=0; i<m_resultFileHeader.wJigIndex; i++)
	{
		nCnt += m_sysData.awChInTray[i];
	}
	nCnt += nChNo;
	return nCnt;

/*	STR_STEP_RESULT * pStepData = GetLastStepData();
	if(pStepData)
	{
		STR_SAVE_CH_DATA * pStepResult;
		for(int i =0; i<pStepData->aChData.GetSize(); i++)
		{
			pStepResult = (STR_SAVE_CH_DATA *)pStepData->aChData[i];

			if(pStepResult->wChIndex == nChNo-1)
			{
				return pStepResult->wModuleChIndex+1;
			}
		}
	}
*/
}

CString CFormResultFile::GetModuleName()
{
	return CString(m_extData.szModuleName);
}

_MAPPING_DATA * CFormResultFile::GetSensorMap(int nNo, int nChIndex)
{
	if(nNo == sensorType1)
	{
		return &m_SensorMap1[nChIndex];
	}
	else if(nNo == sensorType2)
	{
		return &m_SensorMap2[nChIndex];
	}
	return NULL;
}

int CFormResultFile::GetLastStepNo()
{
	int nStepNo = 0;
	STR_STEP_RESULT *pStep = GetLastStepData();
	if(pStep)
	{
		nStepNo = pStep->stepCondition.stepHeader.stepIndex+1;
	}
	return nStepNo;
}

int CFormResultFile::GetLastIndexStepNo()
{
	int nStepNo = 0;
	int nStepSize = 0;
	nStepSize = GetStepSize();
	if( nStepSize > 0 )
	{
		STR_STEP_RESULT *pStep = GetStepIndexData(nStepSize-1);
		if(pStep)
		{
			nStepNo = pStep->stepCondition.stepHeader.stepIndex+1;
		}
	}
	
	return nStepNo;
}

BOOL CFormResultFile::GetLotTrayNo(CString strFile, CString &strLot, CString &strTray)
{
	CString strTemp;
	FILE *fp = NULL;
	RESULT_FILE_HEADER	 pResultFileHeader;
	EP_FILE_HEADER lpFileHeader;

	fp = fopen(strFile, "rb");
	if(fp)
	{
		fread(&lpFileHeader, sizeof(EP_FILE_HEADER), 1, fp);
		if(strcmp(lpFileHeader.szFileID, RESULT_FILE_ID) == 0 || 
		   strcmp(lpFileHeader.szFileID, IROCV_FILE_ID)  == 0 ||
		   strcmp(lpFileHeader.szFileID, PRODUCT_RESULT_FILE_ID) ==0)
		{
			fread(&pResultFileHeader, sizeof(RESULT_FILE_HEADER), 1, fp);				//File Header Read
			strLot = pResultFileHeader.szLotNo;
			strTray = pResultFileHeader.szTrayNo;
			fclose(fp);
			return TRUE;
		}
		fclose(fp);
	}
	return FALSE;	
}

BOOL CFormResultFile::UpdateTestInfo(CString strNewTray, CString strNewLot)
{
	if(IsLoaded() == FALSE)	return FALSE;

	FILE *fp = fopen(m_strFileName, "rb+");
	if(fp == NULL)		return FALSE; 
		
	sprintf(m_resultFileHeader.szTrayNo, "%s", strNewTray);
	sprintf(m_resultFileHeader.szLotNo, "%s", strNewLot);

	//File Header Read
	fseek(fp, sizeof(EP_FILE_HEADER), SEEK_SET);
	//Test 결과 Header를 읽는다.
	if(fwrite(&m_resultFileHeader, sizeof(RESULT_FILE_HEADER), 1, fp) < 1)	//Read Test Result Header
	{
		fclose(fp);
		fp = NULL;
		return FALSE;	//File Header Read
	}
	fclose(fp);
	return TRUE;
}

//PC에서 재 Grading  실시 한다.
BOOL CFormResultFile::Regrading()
{
	if(IsLoaded() == FALSE)	return FALSE;

	int size = m_stepData.GetSize();
	if(size <= 0)			return FALSE;

	//최종 Step을 정보를 구함 
	STR_STEP_RESULT *lpStepData;
	STR_SAVE_CH_DATA *lpChData;
	WORD nChCode = 0;
	WORD code = 0;
		
	for(int s=0; s<size; s++)
	{
		lpStepData = (STR_STEP_RESULT *)m_stepData[s];
		if(lpStepData)
		{
			for(int ch = 0; ch<lpStepData->aChData.GetSize(); ch++)
			{
				lpChData = (STR_SAVE_CH_DATA *)lpStepData->aChData[ch];
				nChCode = lpChData->channelCode;
				code = 0;
				if(lpStepData->stepCondition.stepHeader.gradeSize > 0)
				{
					//BYTE CFormModule::GetUserSelect(int nChCode, int nGradeCode)와 동일해야 함
					if(::IsNonCell(nChCode))
					{
						code = EP_SELECT_CODE_NONCELL;
					}
					else if(IsCellCheckFail(nChCode))
					{
						code = EP_SELECT_CODE_NONCELL;
					}
					else if(::IsFailCell(nChCode))
					{
						if(nChCode == EP_CODE_END_USER_STOP)
						{
							code = EP_SELECT_DEFAULT_NORMAL_CODE;
						}
						//Impedance code 불량
						else if(nChCode == EP_CODE_FAIL_IMPEDANCE_HIGH || nChCode == EP_CODE_FAIL_IMPEDANCE_LOW)
						{
							code = EP_SELECT_CODE_IMPEDANCE;
						}
						//Capacity code 불량
						else if(nChCode == EP_CODE_FAIL_CAP_HIGH || nChCode == EP_CODE_FAIL_CAP_LOW)
						{
							code = EP_SELECT_CODE_CAP_FAIL;
						}
						//Current code 불량 
						else if(nChCode == EP_CODE_FAIL_CRT_HIGH || nChCode == EP_CODE_FAIL_CRT_LOW) // =>system code 영역  || nChCode == EP_CODE_FAIL_I_HUNTING)
						{
							code = EP_SELECT_CODE_CRT_FAIL;
						}
						//Voltage code 불량 
						else if(nChCode == EP_CODE_FAIL_VTG_HIGH || nChCode == EP_CODE_FAIL_VTG_LOW) // =>system code 영역  || nChCode == EP_CODE_FAIL_V_HUNTING)
						{
							code = EP_SELECT_CODE_VTG_FAIL;
						}
						else
						{
							code = EP_SELECT_CODE_SAFETY_FAULT;
						}
					}
					else if(::IsNormalCell(nChCode))
					{
						//Grading 실시 
						for(int g=0; g<lpStepData->stepCondition.stepHeader.gradeSize; g++)
						{
							if(lpStepData->stepCondition.grade[g].gradeItem == EP_GRADE_VOLTAGE)
							{
								if(	lpStepData->stepCondition.grade[g].fMin <= lpChData->fVoltage &&
									lpChData->fVoltage < lpStepData->stepCondition.grade[g].fMax)
								{
									code = lpStepData->stepCondition.grade[g].gradeCode;
								}
							}
							else if(lpStepData->stepCondition.grade[g].gradeItem == EP_GRADE_CAPACITY)
							{
								if(	lpStepData->stepCondition.grade[g].fMin <= lpChData->fCapacity &&
									lpChData->fCapacity < lpStepData->stepCondition.grade[g].fMax)
								{
									code = lpStepData->stepCondition.grade[g].gradeCode;
								}
							}
							else if(lpStepData->stepCondition.grade[g].gradeItem == EP_GRADE_IMPEDANCE)
							{
								if(	lpStepData->stepCondition.grade[g].fMin <= lpChData->fImpedance &&
									lpChData->fImpedance < lpStepData->stepCondition.grade[g].fMax)
								{
									code = lpStepData->stepCondition.grade[g].gradeCode;
								}
							}
							else
							{
								code = EP_SELECT_CODE_NO_GRADE;
							}
						}
						
						if(code == 0)
						{
							code = EP_SELECT_CODE_NO_GRADE;
						}
					}
					else //if()
					{
						if(nChCode ==  EP_CODE_FAIL_V_HUNTING)
						{
							code = EP_SELECT_CODE_VTG_FAIL;
						}
						else if(nChCode == EP_CODE_FAIL_I_HUNTING)
						{
							code = EP_SELECT_CODE_CRT_FAIL;
						}
						else
						{
							code = EP_SELECT_CODE_SYS_ERROR;
						}
					}
				}
				else
				{
					//Grading을 취소 할수 있음 
					code = 0;
				}
				
				lpChData->grade = code;
			}
		}
	}	
	return TRUE;
}

//20090920 KBH
PNE_RESULT_CELL_SERIAL * CFormResultFile::GetResultCellSerial()
{
	return &m_ResultCellSerial;
}


CString CFormResultFile::GetCellSerial(int nChIndex)
{
	CString serial;
	if(nChIndex >= 0 && nChIndex < EP_MAX_CH_PER_MD) 
	{
		serial =  m_ResultCellSerial.szCellNo[nChIndex];
	}

	return serial;
}

