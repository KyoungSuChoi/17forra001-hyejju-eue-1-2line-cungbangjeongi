#if !defined(AFX_EDITORSETDLG_H__7E81EFEF_C4B4_4B72_8EA2_EC4D1CD2D1AC__INCLUDED_)
#define AFX_EDITORSETDLG_H__7E81EFEF_C4B4_4B72_8EA2_EC4D1CD2D1AC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EditorSetDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CEditorSetDlg dialog

class CEditorSetDlg : public CDialog
{
// Construction
public:
	void UnitUpdate();
	CEditorSetDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CEditorSetDlg)
	enum { IDD = IDD_EDITOR_SET_DLG };
	CComboBox	m_ctrlIUnit;
	CComboBox	m_ctrlCUnit;
	CComboBox	m_ctrlVUnit;
	BOOL	m_bVHigh;
	BOOL	m_bVLow;
	BOOL	m_bIHigh;
	BOOL	m_bILow;
	BOOL	m_bCHigh;
	BOOL	m_bCLow;
	float	m_fCLowVal;
	float	m_fCHighVal;
	float	m_fILowVal;
	float	m_fIHighVal;
	float	m_fVLowVal;
	float	m_fVHighVal;
	BOOL	m_bUseLogin;
	long	m_lVRefLow;
	long	m_lVRefHigh;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEditorSetDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
//	BOOL m_bEDLCType;
	// Generated message map functions
	//{{AFX_MSG(CEditorSetDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnOk();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EDITORSETDLG_H__7E81EFEF_C4B4_4B72_8EA2_EC4D1CD2D1AC__INCLUDED_)
