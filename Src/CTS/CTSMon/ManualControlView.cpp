// ManualControlView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "ManualControlView.h"
#include "MainFrm.h"
#include "StopperSettingDlg.h"

// CManualControlView

#define EDIT_FONT_SIZE		22

IMPLEMENT_DYNCREATE(CManualControlView, CFormView)

CManualControlView::CManualControlView()
	: CFormView(CManualControlView::IDD)
	, m_nAutoIntervalTime(0)	
{
	m_bOutMask = false;
	m_nCurModuleID = 0;
	m_bReadySetCmd = false;
	m_nChkInputData = 0;
	ZeroMemory( &m_SendIOData, sizeof(IO_Data));
	m_SendIOData.ch_2 = 1;	
	ZeroMemory( &m_RecvIOData, sizeof(IO_Data));
	m_nDisplayTimer = 0;

	m_pNormalSensorMapDlg = NULL;	

	LanguageinitMonConfig();
}

CManualControlView::~CManualControlView()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CManualControlView::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CManualControlView"), _T("TEXT_CManualControlView_CNT"), _T("TEXT_CManualControlView_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CManualControlView_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CManualControlView"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}


void CManualControlView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, ID_DLG_LABEL_NAME, m_LabelViewName);
	DDX_Control(pDX, IDC_STAGE_NAME, m_CmdTarget);
	DDX_Control(pDX, IDC_LABEL_WORKMODE, m_ctrlWorkMode);
	DDX_Control(pDX, IDC_OUTPUT1_STATIC, m_LabelOutput1);
	DDX_Control(pDX, IDC_OUTPUT2_STATIC, m_LabelOutput2);
	DDX_Control(pDX, IDC_OUTPUT3_STATIC, m_LabelOutput3);
	DDX_Control(pDX, IDC_OUTPUT4_STATIC, m_LabelOutput4);
	DDX_Control(pDX, IDC_OUTPUT5_STATIC, m_LabelOutput5);
	DDX_Control(pDX, IDC_OUTPUT6_STATIC, m_LabelOutput6);
	DDX_Control(pDX, IDC_INPUT1_STATIC, m_LabelInput1);
	DDX_Control(pDX, IDC_INPUT2_STATIC, m_LabelInput2);
	DDX_Control(pDX, IDC_INPUT3_STATIC, m_LabelInput3);
	DDX_Control(pDX, IDC_INPUT4_STATIC, m_LabelInput4);
	DDX_Control(pDX, IDC_INPUT5_STATIC, m_LabelInput5);
	DDX_Control(pDX, IDC_INPUT6_STATIC, m_LabelInput6);
	DDX_Control(pDX, IDC_INPUT7_STATIC, m_LabelInput7);
	DDX_Control(pDX, IDC_INPUT8_STATIC, m_LabelInput8);
	DDX_Control(pDX, IDC_INPUT9_STATIC, m_LabelInput9);
	DDX_Control(pDX, IDC_INPUT10_STATIC, m_LabelInput10);
	DDX_Control(pDX, IDC_RESTART_BTN, m_Btn_ReStart);
	DDX_Control(pDX, IDC_RESET_BTN, m_Btn_Reset);
	DDX_Control(pDX, IDC_LABEL1, m_Label1);
	DDX_Control(pDX, IDC_LABEL2, m_Label2);
	DDX_Control(pDX, IDC_LABEL3, m_Label3);
	DDX_Control(pDX, IDC_LABEL4, m_Label4);
	DDX_Control(pDX, IDC_LABEL6, m_Label6);
	DDX_Control(pDX, IDC_LABEL_TYPESEl_1, m_LabelTypeSel1);
	DDX_Control(pDX, IDC_LABEL_TYPESEl_2, m_LabelTypeSel2);
	DDX_Control(pDX, IDC_LABEL_TYPESEl_3, m_LabelTypeSel3);
	DDX_Control(pDX, IDC_LABEL_TYPESEl_4, m_LabelTypeSel4);
	DDX_Control(pDX, IDC_LABEL13, m_Label13);
	DDX_Control(pDX, IDC_LABEL14, m_Label14);
	DDX_Control(pDX, IDC_LABEL11, m_Label11);
	DDX_Control(pDX, IDC_LABEL12, m_Label12);
	DDX_Control(pDX, IDC_LABEL15, m_Label15);
	DDX_Control(pDX, IDC_LABEL16, m_Label16);
	DDX_Control(pDX, IDC_LABEL17, m_Label17);
	DDX_Control(pDX, IDC_LABEL18, m_Label18);
	DDX_Control(pDX, IDC_LABEL19, m_Label19);
	DDX_Control(pDX, IDC_LABEL20, m_Label20);
	DDX_Control(pDX, IDC_LABEL22, m_Label22);
	DDX_Control(pDX, IDC_LABEL23, m_Label23);
	DDX_Control(pDX, IDC_LABEL24, m_Label24);
	DDX_Control(pDX, IDC_LABEL25, m_Label25);
	DDX_Control(pDX, IDC_LABEL26, m_Label26);
	DDX_Control(pDX, IDC_LABEL21, m_Label21);
	DDX_Control(pDX, IDC_MAINTENANCE_MODE_BTN, m_Btn_MaintenanceMode);
	DDX_Control(pDX, IDC_LOCAL_MODE_BTN, m_Btn_LocalMode);
	DDX_Control(pDX, IDC_LABEL27, m_Label27);
	DDX_Control(pDX, IDC_BTN_LATCH_CLOSE, m_BtnLatchClose);
	DDX_Control(pDX, IDC_BTN_LATCH_OPEN, m_BtnLatchOpen);
	DDX_Control(pDX, IDC_BTN_CLIP_CLOSE, m_BtnClipClose);
	DDX_Control(pDX, IDC_BTN_CLIP_OPEN, m_BtnClipOpen);
	DDX_Control(pDX, IDC_BTN_STOPPER1, m_BtnStopper1);
	DDX_Control(pDX, IDC_BTN_STOPPER2, m_BtnStopper2);
	DDX_Control(pDX, IDC_BTN_STOPPER3, m_BtnStopper3);
	DDX_Control(pDX, IDC_BTN_STOPPER4, m_BtnStopper4);
	DDX_Control(pDX, IDC_BTN_JIG_PLATE_FORWARD, m_BtnJigPlateForward);
	DDX_Control(pDX, IDC_BTN_JIG_PLATE_BACK, m_BtnJigPlateback);
	DDX_Control(pDX, IDC_BTN_CLIP_PLATE_UP, m_BtnClipPlateUp);
	DDX_Control(pDX, IDC_BTN_CLIP_PLATE_DOWN, m_BtnClipPlateDown);
	DDX_Control(pDX, IDC_BTN_CLIP_PLATE_FORWARD, m_BtnClipPlateForward);
	DDX_Control(pDX, IDC_BTN_CLIP_PLATE_BACK, m_BtnClipPlateBack);
	DDX_Control(pDX, IDC_BTN_JIG_FAN_ON, m_BtnJigFanOn);
	DDX_Control(pDX, IDC_BTN_JIG_FAN_OFF, m_BtnJigFanOff);
	DDX_Control(pDX, IDC_LABEL_LEFT_STOPPER, m_LabelLeftStopper);
	DDX_Control(pDX, IDC_LABEL_RIGHT_STOPPER, m_LabelRightStopper);
	DDX_Control(pDX, IDC_BUZZER_OFF_BTN, m_Btn_BuzzerOff);
	DDX_Control(pDX, IDC_EMERGENCY_STOP_BTN, m_Btn_EmergencyStop);
	DDX_Control(pDX, IDC_FMS_RESET1_BTN, m_Btn_FmsReset1);
	DDX_Control(pDX, IDC_FMS_RESET2_BTN, m_Btn_FmsReset2);
	DDX_Control(pDX, IDC_FMS_END_CMD_SEND_BTN, m_Btn_FmsEndCommandSend);
	DDX_Control(pDX, IDC_LABEL28, m_LabelLog);
	DDX_Control(pDX, IDC_CONTINUE_BTN, m_Btn_Continue);
}

BEGIN_MESSAGE_MAP(CManualControlView, CFormView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_LOCAL_MODE_BTN, &CManualControlView::OnBnClickedLocalModeBtn)
	ON_BN_CLICKED(IDC_MAINTENANCE_MODE_BTN, &CManualControlView::OnBnClickedMaintenanceModeBtn)
	ON_BN_CLICKED(IDC_RESTART_BTN, &CManualControlView::OnBnClickedRestartBtn)
	ON_BN_CLICKED(IDC_RESET_BTN, &CManualControlView::OnBnClickedResetBtn)
//	ON_BN_CLICKED(IDC_KILL_PROCESS_BUTTON, &CManualControlView::OnBnClickedKillProcessButton)
//	ON_BN_CLICKED(IDC_REBOOT_BUTTON, &CManualControlView::OnBnClickedRebootButton)
//	ON_BN_CLICKED(IDC_HALT_BUTTON, &CManualControlView::OnBnClickedHaltButton)
	ON_BN_CLICKED(IDC_BTN_STOPPER1, &CManualControlView::OnBnClickedBtnStopper1)
	ON_BN_CLICKED(IDC_BTN_STOPPER2, &CManualControlView::OnBnClickedBtnStopper2)
	ON_BN_CLICKED(IDC_BTN_STOPPER3, &CManualControlView::OnBnClickedBtnStopper3)
	ON_BN_CLICKED(IDC_BTN_STOPPER4, &CManualControlView::OnBnClickedBtnStopper4)
	ON_BN_CLICKED(IDC_BTN_STOPPER_SETTING, &CManualControlView::OnBnClickedBtnStopperSetting)
	ON_BN_CLICKED(IDC_BTN_LATCH_CLOSE, &CManualControlView::OnBnClickedBtnLatchClose)
	ON_BN_CLICKED(IDC_BTN_LATCH_OPEN, &CManualControlView::OnBnClickedBtnLatchOpen)
	ON_BN_CLICKED(IDC_BTN_CLIP_CLOSE, &CManualControlView::OnBnClickedBtnClipClose)
	ON_BN_CLICKED(IDC_BTN_CLIP_OPEN, &CManualControlView::OnBnClickedBtnClipOpen)
	ON_BN_CLICKED(IDC_BTN_JIG_PLATE_FORWARD, &CManualControlView::OnBnClickedBtnJigPlateForward)
	ON_BN_CLICKED(IDC_BTN_JIG_PLATE_BACK, &CManualControlView::OnBnClickedBtnJigPlateBack)
	ON_BN_CLICKED(IDC_BTN_CLIP_PLATE_UP, &CManualControlView::OnBnClickedBtnClipPlateUp)
	ON_BN_CLICKED(IDC_BTN_CLIP_PLATE_DOWN, &CManualControlView::OnBnClickedBtnClipPlateDown)
	ON_BN_CLICKED(IDC_BTN_CLIP_PLATE_FORWARD, &CManualControlView::OnBnClickedBtnClipPlateForward)
	ON_BN_CLICKED(IDC_BTN_CLIP_PLATE_BACK, &CManualControlView::OnBnClickedBtnClipPlateBack)
	ON_BN_CLICKED(IDC_BTN_JIG_FAN_ON, &CManualControlView::OnBnClickedBtnJigFanOn)
	ON_BN_CLICKED(IDC_BTN_JIG_FAN_OFF, &CManualControlView::OnBnClickedBtnJigFanOff)
	ON_BN_CLICKED(IDC_BTN_NORMAL_SENSOR, &CManualControlView::OnBnClickedBtnNormalSensor)
	ON_BN_CLICKED(IDC_BUZZER_OFF_BTN, &CManualControlView::OnBnClickedBuzzerOffBtn)
	ON_BN_CLICKED(IDC_EMERGENCY_STOP_BTN, &CManualControlView::OnBnClickedEmergencyStopBtn)
	ON_BN_CLICKED(IDC_FMS_END_CMD_SEND_BTN, &CManualControlView::OnBnClickedFmsEndCmdSendBtn)
	ON_BN_CLICKED(IDC_FMS_RESET1_BTN, &CManualControlView::OnBnClickedFmsReset1Btn)
	ON_BN_CLICKED(IDC_CONTINUE_BTN, &CManualControlView::OnBnClickedContinueBtn)
	ON_BN_CLICKED(IDC_FMS_RESET2_BTN, &CManualControlView::OnBnClickedFmsReset2Btn)
END_MESSAGE_MAP()


// CManualControlView 진단입니다.

#ifdef _DEBUG
void CManualControlView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CManualControlView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif

CCTSMonDoc* CManualControlView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCTSMonDoc)));
	return (CCTSMonDoc*)m_pDocument;
}

#endif //_DEBUG


// CManualControlView 메시지 처리기입니다.
void CManualControlView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	m_nLineMode = EP_OPERATION_LOCAL;
	
	InitLabel();
	InitColorBtn();
	InitFont();
	
	if(LoadIoConfig() == FALSE)
	{
		AfxMessageBox("Can't find configuration File.(ioconfig.cfg)");
		return;
	}
	
	CString strTemp = _T("");

	strTemp.Format("#1 (%s)(%s)", AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Stopper1", "310"), AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "DESC_Stopper1", "310") );
	GetDlgItem(IDC_BTN_STOPPER1)->SetWindowText(strTemp);
	strTemp.Format("#2 (%s)(%s)", AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Stopper2", "250"), AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "DESC_Stopper2", "250") );
	GetDlgItem(IDC_BTN_STOPPER2)->SetWindowText(strTemp);
	strTemp.Format("#3 (%s)(%s)", AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Stopper3", "200"), AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "DESC_Stopper3", "200") );
	GetDlgItem(IDC_BTN_STOPPER3)->SetWindowText(strTemp);
	strTemp.Format("#4 (%s)(%s)", AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Stopper4", "180"), AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "DESC_Stopper4", "180") );
	GetDlgItem(IDC_BTN_STOPPER4)->SetWindowText(strTemp);		

	strTemp.Format("#P (%s)", AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "TypeSel1", "000") );
	m_LabelTypeSel1.SetText(strTemp);
	strTemp.Format("#T (%s)", AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "TypeSel2", "100") );
	m_LabelTypeSel2.SetText(strTemp);
	strTemp.Format("#D (%s)", AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "TypeSel3", "900") );
	m_LabelTypeSel3.SetText(strTemp);
	strTemp.Format("#R (%s)", AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "TypeSel4", "700") );
	m_LabelTypeSel4.SetText(strTemp);		

	int i=0;
	
	for( i =0; i<MAX_READ_ADDR_CNT; i++ )
	{
		// Read Addr Set
		m_stateButton[i].SubclassWindow(GetDlgItem(IDC_READ_CHECK1+i)->m_hWnd);
		m_stateButton[i].SetImage( IDB_RED_BUTTON, 15);
		m_stateButton[i].Depress(FALSE);
		m_stateButton[i].SetWindowText(m_strInputName[i]);
	}
	
	for( i =0; i<MAX_WRITE_ADDR_CNT; i++ )
	{
		// Write Addr Set		
		GetDlgItem(IDC_OUT_CHECK1+i)->SetWindowText(m_strOutputName[i]);
	}
	
	for( i=0; i<10; i++ )
	{
		strTemp.Format("Input %d(0x%02x)", i+1, m_nInputAddress[i]);
		GetDlgItem(IDC_INPUT1_STATIC+i)->SetWindowText(strTemp);	
	}
	
	for( i=0; i<6; i++ )
	{
		strTemp.Format("Output %d(0x%02x)", i+1, m_nOutputAddress[i]);
		GetDlgItem(IDC_OUTPUT1_STATIC+i)->SetWindowText(strTemp);	
	}

	CCTSMonDoc *pDoc =  GetDocument();	
}

void CManualControlView::InitLabel()
{
	m_LabelViewName.SetBkColor(RGB_MIDNIGHTBLUE);
	m_LabelViewName.SetTextColor(RGB_WHITE);
	m_LabelViewName.SetFontSize(24);
	m_LabelViewName.SetFontBold(TRUE);
	m_LabelViewName.SetText("Maintenance");
	
	m_ctrlWorkMode.SetFontSize(20)
		.SetTextColor(RGB_LABEL_FONT)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[0]);//"HY헤드라인M"

	m_CmdTarget.SetFontSize(20)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND_STAGENAME)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[0]);//"HY헤드라인M"
		
	// Addr 셋팅
	m_LabelOutput1.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND_STAGENAME)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[0]);//"HY헤드라인M"
	m_LabelOutput2.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND_STAGENAME)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[0]);//"HY헤드라인M"
	m_LabelOutput3.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND_STAGENAME)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[0]);//"HY헤드라인M"
	m_LabelOutput4.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND_STAGENAME)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[0]);//"HY헤드라인M"
	m_LabelOutput5.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND_STAGENAME)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[0]);//"HY헤드라인M"
	m_LabelOutput6.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND_STAGENAME)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[0]);//"HY헤드라인M"
		
	m_LabelInput1.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND_STAGENAME)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[0]);//"HY헤드라인M"
	m_LabelInput2.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND_STAGENAME)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[0]);//"HY헤드라인M"
	m_LabelInput3.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND_STAGENAME)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[0]);//"HY헤드라인M"
	m_LabelInput4.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND_STAGENAME)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[0]);//"HY헤드라인M"
	m_LabelInput5.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND_STAGENAME)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[0]);//"HY헤드라인M"
	m_LabelInput6.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND_STAGENAME)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[0]);//"HY헤드라인M"
	m_LabelInput7.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND_STAGENAME)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[0]);//"HY헤드라인M"
	m_LabelInput8.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND_STAGENAME)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[0]);//"HY헤드라인M"
	m_LabelInput9.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND_STAGENAME)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[0]);//"HY헤드라인M"
	m_LabelInput10.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND_STAGENAME)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[0]);//"HY헤드라인M"
		
	m_LabelLog.SetFontSize(14)
		.SetTextColor(RGB_YELLOW)
		.SetBkColor(RGB_BLACK)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[1])	//"맑은고딕"
		.SetText("-");
		
	
	m_Label2.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[1])	//"맑은고딕"
		.SetText("Contact Deepth Stopper");
		

	m_Label3.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE)
		.SetFontName("맑은고딕")	
		.SetText("Type Sel");


	m_Label4.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[1])	//"맑은고딕"
		.SetText(TEXT_LANG[2]);//"트레이 후면 고정 래치 작동"
		
	m_Label6.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[1])	//"맑은고딕"
		// .SetText(TEXT_LANG[3]);//"트레이 후면 고정 래치 작동 상태"
		.SetText("JIG FAN");//"트레이 후면 고정 래치 작동 상태"
		
	m_Label13.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[1])	//"맑은고딕"
		.SetText(TEXT_LANG[4]);//"왼쪽"
		
	m_Label14.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[1])	//"맑은고딕"
		.SetText(TEXT_LANG[5]);//"오른쪽"
		
	m_Label11.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[1])	//"맑은고딕"
		.SetText(TEXT_LANG[6]);//"클립 동작"
		
	m_Label12.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[1])	//"맑은고딕"
		.SetText(TEXT_LANG[7]);//"클립 동작 상태"
		
	m_Label15.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[1])	//"맑은고딕"
		.SetText(TEXT_LANG[8]);//"메인 지그 플레이트 동작"
		
	m_Label16.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[1])	//"맑은고딕"
		.SetText(TEXT_LANG[9]);//"메인 지그 플레이트 동작 상태"
		
	m_Label17.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[1])	//"맑은고딕"
		.SetText(TEXT_LANG[10]);//"클립 플레이트 상승 동작 상태"
		
	m_Label18.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[1])	//"맑은고딕"
		.SetText(TEXT_LANG[10]);//"클립 플레이트 상승 동작 상태"
		
	m_Label19.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[1])	//"맑은고딕"
		.SetText(TEXT_LANG[11]);//"클립 플레이트 전진 동작"
		
	m_Label20.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[1])	//"맑은고딕"
		.SetText(TEXT_LANG[12]);//"클립 플레이트 전진 동작 상태"
		
	m_Label21.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[1])	//"맑은고딕"
		.SetText(TEXT_LANG[13]);//"기구부 비상정지 알림 상태 확인"
		
	m_Label22.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[1])	//"맑은고딕"
		.SetText(TEXT_LANG[14]);//"지그 플레이트 온도 퓨즈 상태"
		
	m_Label23.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[1])	//"맑은고딕"
		.SetText(TEXT_LANG[15]);//"연기 감지 센서 상태"
		
	m_Label24.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[1])	//"맑은고딕"
		.SetText(TEXT_LANG[16]);//"트레이 감지 상태"
		
	m_Label25.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[1])	//"맑은고딕"
		.SetText(TEXT_LANG[17]);//"트레이 높이 감지 상태"
		
	m_Label26.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[1])	//"맑은고딕"
		.SetText(TEXT_LANG[18]);//"전원부 인버터 동작 상태"
		
	m_Label1.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[1])	//"맑은고딕"
		.SetText(TEXT_LANG[19]);//"지그부 팬 동작"
	
	m_Label27.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[1])	//"맑은고딕"
		.SetText(TEXT_LANG[20]);	//"Contact Deepth Stopper 상태"
		
	m_LabelLeftStopper.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_CORNFLOWERBLUE)
		.SetFontBold(TRUE)
		.SetText("ON - ON - ON - ON");		
		
	m_LabelRightStopper.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_CORNFLOWERBLUE)
		.SetFontBold(TRUE)
		.SetText("ON - ON - ON - ON");

	//20201211 ksj
	m_LabelTypeSel1.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_CORNFLOWERBLUE)
		.SetFontBold(TRUE)
		.SetFontName("맑은고딕");

	m_LabelTypeSel2.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_CORNFLOWERBLUE)
		.SetFontBold(TRUE)
		.SetFontName("맑은고딕");

	m_LabelTypeSel3.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_CORNFLOWERBLUE)
		.SetFontBold(TRUE)
		.SetFontName("맑은고딕");

	m_LabelTypeSel4.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_CORNFLOWERBLUE)
		.SetFontBold(TRUE)
		.SetFontName("맑은고딕");



}

void CManualControlView::InitColorBtn()
{	
	int nFontSize = 20;
	m_Btn_LocalMode.SetFontStyle(nFontSize, 1);
	m_Btn_LocalMode.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
	m_Btn_MaintenanceMode.SetFontStyle(nFontSize, 1);
	m_Btn_MaintenanceMode.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	m_Btn_AutoUpdateStop.SetFontStyle(nFontSize, 1);
	m_Btn_AutoUpdateStop.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	

	m_Btn_ReStart.SetFontStyle(nFontSize, 1);
	m_Btn_ReStart.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	m_Btn_Reset.SetFontStyle(nFontSize, 1);
	m_Btn_Reset.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);

	m_Btn_BuzzerOff.SetFontStyle(nFontSize, 1);
	m_Btn_BuzzerOff.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
	m_Btn_EmergencyStop.SetFontStyle(nFontSize, 1);
	m_Btn_EmergencyStop.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);

	m_Btn_FmsReset1.SetFontStyle(nFontSize, 1);
	m_Btn_FmsReset1.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
	m_Btn_FmsReset2.SetFontStyle(nFontSize, 1);
	m_Btn_FmsReset2.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
	m_Btn_FmsEndCommandSend.SetFontStyle(nFontSize, 1);
	m_Btn_FmsEndCommandSend.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);

	m_Btn_Continue.SetFontStyle(nFontSize, 1);
	m_Btn_Continue.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	

	m_BtnLatchClose.SetFontStyle(nFontSize, 1);
	m_BtnLatchClose.SetColor(RGB_BLACK, RGB_GRAY);	
	m_BtnLatchOpen.SetFontStyle(nFontSize, 1);
	m_BtnLatchOpen.SetColor(RGB_BLACK, RGB_GRAY);	
	m_BtnClipClose.SetFontStyle(nFontSize, 1);
	m_BtnClipClose.SetColor(RGB_BLACK, RGB_GRAY);
	m_BtnClipOpen.SetFontStyle(nFontSize, 1);
	m_BtnClipOpen.SetColor(RGB_BLACK, RGB_GRAY);	
	m_BtnStopper1.SetFontStyle(nFontSize, 1);
	m_BtnStopper1.SetColor(RGB_BLACK, RGB_GRAY);
	m_BtnStopper2.SetFontStyle(nFontSize, 1);
	m_BtnStopper2.SetColor(RGB_BLACK, RGB_GRAY);	
	m_BtnStopper3.SetFontStyle(nFontSize, 1);
	m_BtnStopper3.SetColor(RGB_BLACK, RGB_GRAY);
	m_BtnStopper4.SetFontStyle(nFontSize, 1);
	m_BtnStopper4.SetColor(RGB_BLACK, RGB_GRAY);	
	m_BtnJigPlateForward.SetFontStyle(nFontSize, 1);
	m_BtnJigPlateForward.SetColor(RGB_BLACK, RGB_GRAY);
	m_BtnJigPlateback.SetFontStyle(nFontSize, 1);
	m_BtnJigPlateback.SetColor(RGB_BLACK, RGB_GRAY);
	m_BtnClipPlateUp.SetFontStyle(nFontSize, 1);
	m_BtnClipPlateUp.SetColor(RGB_BLACK, RGB_GRAY);
	m_BtnClipPlateDown.SetFontStyle(nFontSize, 1);
	m_BtnClipPlateDown.SetColor(RGB_BLACK, RGB_GRAY);
	m_BtnJigFanOn.SetFontStyle(nFontSize, 1);
	m_BtnJigFanOn.SetColor(RGB_BLACK, RGB_GRAY);
	m_BtnJigFanOff.SetFontStyle(nFontSize, 1);
	m_BtnJigFanOff.SetColor(RGB_BLACK, RGB_GRAY);
	m_BtnClipPlateForward.SetFontStyle(nFontSize, 1);
	m_BtnClipPlateForward.SetColor(RGB_BLACK, RGB_GRAY);
	m_BtnClipPlateBack.SetFontStyle(nFontSize, 1);
	m_BtnClipPlateBack.SetColor(RGB_BLACK, RGB_GRAY);
}

void CManualControlView::InitFont()
{	
	LOGFONT			LogFont;

	GetDlgItem(IDC_STATIC_MANUAL_CONTROL)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight = 1000;
	LogFont.lfHeight = EDIT_FONT_SIZE;

	m_font.CreateFontIndirect( &LogFont );
	
	GetDlgItem(IDC_STATIC_MANUAL_CONTROL)->SetFont(&m_font);
	GetDlgItem(IDC_STATIC_SENSOR_STATUS)->SetFont(&m_font);	
	GetDlgItem(IDC_STATIC1)->SetFont(&m_font);	
}

bool CManualControlView::LoadIoConfig()
{
	CStdioFile	file;

	CFileException* e;
	e = new CFileException;

	if(!file.Open("ioconfig.cfg", CFile::modeRead|CFile::shareDenyNone))	// 오픈할 폴더를 지정한다.
	{
		return FALSE;
	}

	CString buff;
	BOOL	bEOF;

	TRY
	{
		while(TRUE)
		{
			bEOF = file.ReadString(buff);
			if(!bEOF)	break;

			if(!buff.IsEmpty())
			{
				ParsingConfig(buff);
 			}
		}

	}
	CATCH (CFileException, e)
	{
		AfxMessageBox(e->m_cause);
		file.Close();
		return FALSE;
	}
	END_CATCH
		file.Close();	
	return TRUE;
}

void CManualControlView::ModuleConnected(int nModuleID)
{
	if(nModuleID == m_nCurModuleID)							//현재 선택된 Module이면 Grid를 Setting 시키고
	{	
		UpdateGroupState(nModuleID);
		
		StartMonitoring();
	}
}

void CManualControlView::ModuleDisConnected(int nModuleID)
{
	if(nModuleID == m_nCurModuleID)							//현재 선택된 Module이면 Grid를 Setting 시키고
	{	
		UpdateGroupState(nModuleID);
		
		StopMonitoring();
	}
}

CString CManualControlView::GetTargetModuleName()
{
	CString temp;
	CString strMDName = GetModuleName(m_nCurModuleID);	
	temp.Format("Stage No. %s", strMDName);	
	return temp;
}

void CManualControlView::InitValue()
{
	int i = 0;
	m_bOutMask = false;	
	
	m_LabelLog.SetText("-");

	ZeroMemory(m_InputData, sizeof(m_InputData));	
	ZeroMemory(m_outputMask, sizeof(m_outputMask)); 
	
	ClearIoState();

	RollBackOutState(0);
}

void CManualControlView::SetCurrentModule(int nModuleID, int nGroupIndex)
{	
	m_nCurModuleID = nModuleID;
	m_CmdTarget.SetText(GetTargetModuleName());
	
	InitValue();
	
	UpdateGroupState(m_nCurModuleID);
	
	UpdateData(false);
}


void CManualControlView::StartMonitoring()
{		
	if( EPGetGroupState(m_nCurModuleID) != EP_STATE_LINE_OFF )
	{
		if( m_nDisplayTimer == 0 )
		{
			// 1. default 2s
			// 2. SBC에서 받은 설정된 값으로 화면 표시 및 적용
			EP_GP_DATA gpData = EPGetGroupData(m_nCurModuleID, 0);

			memcpy( &m_SendIOData, &gpData.gpState.ManualFunc,  sizeof(IO_Data));

			UpdateIoState( true );		

			m_nDisplayTimer = SetTimer(102, 2000, NULL);		
		}
	}
}

void CManualControlView::StopMonitoring()
{
	if( m_nDisplayTimer  != 0)
	{
		KillTimer(102);
		m_nDisplayTimer = 0;		
	}
	
	if( m_pNormalSensorMapDlg != NULL )
	{
		m_pNormalSensorMapDlg->DestroyWindow();
		delete m_pNormalSensorMapDlg;
		m_pNormalSensorMapDlg = NULL;
	}
}

void CManualControlView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	RECT rect;
	::GetClientRect(m_hWnd, &rect);

	if( GetDlgItem(ID_DLG_LABEL_NAME)->GetSafeHwnd() )
	{
		CRect rectGrid;
		CRect rectStageLabel;
		GetDlgItem(ID_DLG_LABEL_NAME)->GetWindowRect(rectGrid);
		ScreenToClient(&rectGrid);
		GetDlgItem(IDC_STAGE_NAME)->GetWindowRect(rectStageLabel);
		ScreenToClient(&rectStageLabel);
		GetDlgItem(ID_DLG_LABEL_NAME)->MoveWindow(rectGrid.left, rectGrid.top, rect.right-rectStageLabel.Width(), rectGrid.Height(), FALSE);
	}
	
	if(::IsWindow(this->GetSafeHwnd()))
	{
		CRect ctrlRect;
		RECT rect;
		::GetClientRect(m_hWnd, &rect);

		if( GetDlgItem(ID_DLG_LABEL_NAME)->GetSafeHwnd() )
		{
			CRect rectGrid;
			CRect rectStageLabel;
			GetDlgItem(ID_DLG_LABEL_NAME)->GetWindowRect(rectGrid);
			ScreenToClient(&rectGrid);
			GetDlgItem(IDC_STAGE_NAME)->GetWindowRect(rectStageLabel);
			ScreenToClient(&rectStageLabel);
			GetDlgItem(ID_DLG_LABEL_NAME)->MoveWindow(rectGrid.left, rectGrid.top, rect.right-rectStageLabel.Width(), rectGrid.Height(), FALSE);			
		}
	}	
}

int CManualControlView::Hex2Dec(CString hexData)
{
	int data = 0;

	int val = 0, val1=0;
	int count = 0;
	hexData.MakeLower();

	//remove pre and post space
	int i = 0;
	int nSize = hexData.GetLength();
	for(i = 0; i < nSize; i++)		//이전 공백 삭제 
	{
		if(hexData[0] == ' ')
		{
			hexData.Delete(0);
		}
		else
		{
			break;
		}
	}

	nSize = hexData.GetLength();
	for(i = nSize-1; i >= 0 ; i--)		//이후  공백 삭제 
	{
		if(hexData[i] == ' ')
		{
			hexData.Delete(i);
		}
		else
		{
			break;
		}
	}

	nSize = hexData.GetLength();
	for(i=nSize-1; i>=0; i--)
	{
		val = 0;
		if( hexData[i] >= '0' && hexData[i] <= '9')	//'0'~'1'
		{
			val = (hexData[i] - '0') << (4*count++);
		}
		else
		{
			val1 = hexData[i] - 'a';
			if(val1 >= 0 && val1 <= 5)	//'a'~'f', 'A'~'F'
			{
				val +=((10 + val1)<<(4*count++));
			}
			else	//	
			{
				return 0;
			}
		}
		data += val; 
	}
	return data;	
}


void CManualControlView::ParsingConfig(CString strConfig)
{
	if(strConfig.IsEmpty())		return;

	int a, b;
	a = strConfig.Find('[');
	b = strConfig.ReverseFind(']');

	if(b > 0 && a >= b)	return;

	CString strTitle, strData;
	strTitle = strConfig.Mid(a+1, b-a-1);

	a = strConfig.ReverseFind('=');

	strData = strConfig.Mid( a+1 );

	strData.TrimLeft(" ");
	strData.TrimRight(" ");

	if(strData.IsEmpty())	return;
	
	if(strTitle == "Input port 1")		{	if(Hex2Dec(strData) > 0)	m_nInputAddress[0] = Hex2Dec(strData);	}
	else if(strTitle == "Input port 2")	{	if(Hex2Dec(strData) > 0)	m_nInputAddress[1] = Hex2Dec(strData);	}
	else if(strTitle == "Input port 3")	{	if(Hex2Dec(strData) > 0)	m_nInputAddress[2] = Hex2Dec(strData);	}
	else if(strTitle == "Input port 4")	{	if(Hex2Dec(strData) > 0)	m_nInputAddress[3] = Hex2Dec(strData);	}
	else if(strTitle == "Input port 5")	{	if(Hex2Dec(strData) > 0)	m_nInputAddress[4] = Hex2Dec(strData);	}
	else if(strTitle == "Input port 6")	{	if(Hex2Dec(strData) > 0)	m_nInputAddress[5] = Hex2Dec(strData);	}
	else if(strTitle == "Input port 7")	{	if(Hex2Dec(strData) > 0)	m_nInputAddress[6] = Hex2Dec(strData);	}
	else if(strTitle == "Input port 8")	{	if(Hex2Dec(strData) > 0)	m_nInputAddress[7] = Hex2Dec(strData);	}
	else if(strTitle == "Input port 9")	{	if(Hex2Dec(strData) > 0)	m_nInputAddress[8] = Hex2Dec(strData);	}
	else if(strTitle == "Input port 10"){	if(Hex2Dec(strData) > 0)	m_nInputAddress[9] = Hex2Dec(strData);	}
	else if(strTitle == "Input port 11"){	if(Hex2Dec(strData) > 0)	m_nInputAddress[10] = Hex2Dec(strData);	}
	else if(strTitle == "Output port 1"){	if(Hex2Dec(strData) > 0)	m_nOutputAddress[0] = Hex2Dec(strData); }
	else if(strTitle == "Output port 2"){	if(Hex2Dec(strData) > 0)	m_nOutputAddress[1] = Hex2Dec(strData); }
	else if(strTitle == "Output port 3"){	if(Hex2Dec(strData) > 0)	m_nOutputAddress[2] = Hex2Dec(strData); }
	else if(strTitle == "Output port 4"){	if(Hex2Dec(strData) > 0)	m_nOutputAddress[3] = Hex2Dec(strData); }
	else if(strTitle == "Output port 5"){	if(Hex2Dec(strData) > 0)	m_nOutputAddress[4] = Hex2Dec(strData); }
	else if(strTitle == "Output port 6"){	if(Hex2Dec(strData) > 0)	m_nOutputAddress[5] = Hex2Dec(strData); }
	else if(strTitle == "Out Mask")		{   m_bOutMask = atol(strData);	}
	
	// ioconfig.cfg의 데이터를 받아 배열로 저장시킨다. 
	if(strTitle == "Project Name")	{   m_strProjectName = strData;		}
	else if(strTitle == "Input1_1 Name"){	m_strInputName[0] =  strData;	}
	else if(strTitle == "Input1_2 Name"){	m_strInputName[1] =  strData;	}
	else if(strTitle == "Input1_3 Name"){	m_strInputName[2] =  strData;	}
	else if(strTitle == "Input1_4 Name"){	m_strInputName[3] =  strData;	}
	else if(strTitle == "Input1_5 Name"){	m_strInputName[4] =  strData;	}
	else if(strTitle == "Input1_6 Name"){	m_strInputName[5] =  strData;	}
	else if(strTitle == "Input1_7 Name"){	m_strInputName[6] =  strData;	}
	else if(strTitle == "Input1_8 Name"){	m_strInputName[7] =  strData;	}
	else if(strTitle == "Input2_1 Name"){	m_strInputName[8] =  strData;	}
	else if(strTitle == "Input2_2 Name"){	m_strInputName[9] =  strData;	}
	else if(strTitle == "Input2_3 Name"){	m_strInputName[10] =  strData;	}
	else if(strTitle == "Input2_4 Name"){	m_strInputName[11] =  strData;	}
	else if(strTitle == "Input2_5 Name"){	m_strInputName[12] =  strData;	}
	else if(strTitle == "Input2_6 Name"){	m_strInputName[13] =  strData;	}
	else if(strTitle == "Input2_7 Name"){	m_strInputName[14] =  strData;	}
	else if(strTitle == "Input2_8 Name"){	m_strInputName[15] =  strData;	}
	else if(strTitle == "Input3_1 Name"){	m_strInputName[16] =  strData;	}
	else if(strTitle == "Input3_2 Name"){	m_strInputName[17] =  strData;	}
	else if(strTitle == "Input3_3 Name"){	m_strInputName[18] =  strData;	}
	else if(strTitle == "Input3_4 Name"){	m_strInputName[19] =  strData;	}
	else if(strTitle == "Input3_5 Name"){	m_strInputName[20] =  strData;	}
	else if(strTitle == "Input3_6 Name"){	m_strInputName[21] =  strData;	}
	else if(strTitle == "Input3_7 Name"){	m_strInputName[22] =  strData;	}
	else if(strTitle == "Input3_8 Name"){	m_strInputName[23] =  strData;	}
	else if(strTitle == "Input4_1 Name"){	m_strInputName[24] =  strData;	}
	else if(strTitle == "Input4_2 Name"){	m_strInputName[25] =  strData;	}
	else if(strTitle == "Input4_3 Name"){	m_strInputName[26] =  strData;	}
	else if(strTitle == "Input4_4 Name"){	m_strInputName[27] =  strData;	}
	else if(strTitle == "Input4_5 Name"){	m_strInputName[28] =  strData;	}
	else if(strTitle == "Input4_6 Name"){	m_strInputName[29] =  strData;	}
	else if(strTitle == "Input4_7 Name"){	m_strInputName[30] =  strData;	}
	else if(strTitle == "Input4_8 Name"){	m_strInputName[31] =  strData;	}
	else if(strTitle == "Input5_1 Name"){	m_strInputName[32] =  strData;	}
	else if(strTitle == "Input5_2 Name"){	m_strInputName[33] =  strData;	}
	else if(strTitle == "Input5_3 Name"){	m_strInputName[34] =  strData;	}
	else if(strTitle == "Input5_4 Name"){	m_strInputName[35] =  strData;	}
	else if(strTitle == "Input5_5 Name"){	m_strInputName[36] =  strData;	}
	else if(strTitle == "Input5_6 Name"){	m_strInputName[37] =  strData;	}
	else if(strTitle == "Input5_7 Name"){	m_strInputName[38] =  strData;	}
	else if(strTitle == "Input5_8 Name"){	m_strInputName[39] =  strData;	}
	else if(strTitle == "Input6_1 Name"){	m_strInputName[40] =  strData;	}
	else if(strTitle == "Input6_2 Name"){	m_strInputName[41] =  strData;	}
	else if(strTitle == "Input6_3 Name"){	m_strInputName[42] =  strData;	}
	else if(strTitle == "Input6_4 Name"){	m_strInputName[43] =  strData;	}
	else if(strTitle == "Input6_5 Name"){	m_strInputName[44] =  strData;	}
	else if(strTitle == "Input6_6 Name"){	m_strInputName[45] =  strData;	}
	else if(strTitle == "Input6_7 Name"){	m_strInputName[46] =  strData;	}
	else if(strTitle == "Input6_8 Name"){	m_strInputName[47] =  strData;	}
	else if(strTitle == "Input7_1 Name"){	m_strInputName[48] =  strData;	}
	else if(strTitle == "Input7_2 Name"){	m_strInputName[49] =  strData;	}
	else if(strTitle == "Input7_3 Name"){	m_strInputName[50] =  strData;	}
	else if(strTitle == "Input7_4 Name"){	m_strInputName[51] =  strData;	}
	else if(strTitle == "Input7_5 Name"){	m_strInputName[52] =  strData;	}
	else if(strTitle == "Input7_6 Name"){	m_strInputName[53] =  strData;	}
	else if(strTitle == "Input7_7 Name"){	m_strInputName[54] =  strData;	}
	else if(strTitle == "Input7_8 Name"){	m_strInputName[55] =  strData;	}
	else if(strTitle == "Input8_1 Name"){	m_strInputName[56] =  strData;	}
	else if(strTitle == "Input8_2 Name"){	m_strInputName[57] =  strData;	}
	else if(strTitle == "Input8_3 Name"){	m_strInputName[58] =  strData;	}
	else if(strTitle == "Input8_4 Name"){	m_strInputName[59] =  strData;	}
	else if(strTitle == "Input8_5 Name"){	m_strInputName[60] =  strData;	}
	else if(strTitle == "Input8_6 Name"){	m_strInputName[61] =  strData;	}
	else if(strTitle == "Input8_7 Name"){	m_strInputName[62] =  strData;	}
	else if(strTitle == "Input8_8 Name"){	m_strInputName[63] =  strData;	}
	else if(strTitle == "Input9_1 Name"){	m_strInputName[64] =  strData;	}
	else if(strTitle == "Input9_2 Name"){	m_strInputName[65] =  strData;	}
	else if(strTitle == "Input9_3 Name"){	m_strInputName[66] =  strData;	}
	else if(strTitle == "Input9_4 Name"){	m_strInputName[67] =  strData;	}
	else if(strTitle == "Input9_5 Name"){	m_strInputName[68] =  strData;	}
	else if(strTitle == "Input9_6 Name"){	m_strInputName[69] =  strData;	}
	else if(strTitle == "Input9_7 Name"){	m_strInputName[70] =  strData;	}
	else if(strTitle == "Input9_8 Name"){	m_strInputName[71] =  strData;	}
	else if(strTitle == "Input10_1 Name"){	m_strInputName[72] =  strData;	}
	else if(strTitle == "Input10_2 Name"){	m_strInputName[73] =  strData;	}
	else if(strTitle == "Input10_3 Name"){	m_strInputName[74] =  strData;	}
	else if(strTitle == "Input10_4 Name"){	m_strInputName[75] =  strData;	}
	else if(strTitle == "Input10_5 Name"){	m_strInputName[76] =  strData;	}
	else if(strTitle == "Input10_6 Name"){	m_strInputName[77] =  strData;	}
	else if(strTitle == "Input10_7 Name"){	m_strInputName[78] =  strData;	}
	else if(strTitle == "Input10_8 Name"){	m_strInputName[79] =  strData;	}	
	
	if(strTitle == "Output1_1 Name"){	m_strOutputName[0] =  strData;	}
	else if(strTitle == "Output1_2 Name"){	m_strOutputName[1] =  strData;	}
	else if(strTitle == "Output1_3 Name"){	m_strOutputName[2] =  strData;	}
	else if(strTitle == "Output1_4 Name"){	m_strOutputName[3] =  strData;	}
	else if(strTitle == "Output1_5 Name"){	m_strOutputName[4] =  strData;	}
	else if(strTitle == "Output1_6 Name"){	m_strOutputName[5] =  strData;	}
	else if(strTitle == "Output1_7 Name"){	m_strOutputName[6] =  strData;	}
	else if(strTitle == "Output1_8 Name"){	m_strOutputName[7] =  strData;	}
	else if(strTitle == "Output2_1 Name"){	m_strOutputName[8] =  strData;	}
	else if(strTitle == "Output2_2 Name"){	m_strOutputName[9] =  strData;	}
	else if(strTitle == "Output2_3 Name"){	m_strOutputName[10] =  strData;	}
	else if(strTitle == "Output2_4 Name"){	m_strOutputName[11] =  strData;	}
	else if(strTitle == "Output2_5 Name"){	m_strOutputName[12] =  strData;	}
	else if(strTitle == "Output2_6 Name"){	m_strOutputName[13] =  strData;	}
	else if(strTitle == "Output2_7 Name"){	m_strOutputName[14] =  strData;	}
	else if(strTitle == "Output2_8 Name"){	m_strOutputName[15] =  strData;	}
	else if(strTitle == "Output3_1 Name"){	m_strOutputName[16] =  strData;	}
	else if(strTitle == "Output3_2 Name"){	m_strOutputName[17] =  strData;	}
	else if(strTitle == "Output3_3 Name"){	m_strOutputName[18] =  strData;	}
	else if(strTitle == "Output3_4 Name"){	m_strOutputName[19] =  strData;	}
	else if(strTitle == "Output3_5 Name"){	m_strOutputName[20] =  strData;	}
	else if(strTitle == "Output3_6 Name"){	m_strOutputName[21] =  strData;	}
	else if(strTitle == "Output3_7 Name"){	m_strOutputName[22] =  strData;	}
	else if(strTitle == "Output3_8 Name"){	m_strOutputName[23] =  strData;	}
	else if(strTitle == "Output4_1 Name"){	m_strOutputName[24] =  strData;	}
	else if(strTitle == "Output4_2 Name"){	m_strOutputName[25] =  strData;	}
	else if(strTitle == "Output4_3 Name"){	m_strOutputName[26] =  strData;	}
	else if(strTitle == "Output4_4 Name"){	m_strOutputName[27] =  strData;	}
	else if(strTitle == "Output4_5 Name"){	m_strOutputName[28] =  strData;	}
	else if(strTitle == "Output4_6 Name"){	m_strOutputName[29] =  strData;	}
	else if(strTitle == "Output4_7 Name"){	m_strOutputName[30] =  strData;	}
	else if(strTitle == "Output4_8 Name"){	m_strOutputName[31] =  strData;	}
	else if(strTitle == "Output5_1 Name"){	m_strOutputName[32] =  strData;	}
	else if(strTitle == "Output5_2 Name"){	m_strOutputName[33] =  strData;	}
	else if(strTitle == "Output5_3 Name"){	m_strOutputName[34] =  strData;	}
	else if(strTitle == "Output5_4 Name"){	m_strOutputName[35] =  strData;	}
	else if(strTitle == "Output5_5 Name"){	m_strOutputName[36] =  strData;	}
	else if(strTitle == "Output5_6 Name"){	m_strOutputName[37] =  strData;	}
	else if(strTitle == "Output5_7 Name"){	m_strOutputName[38] =  strData;	}
	else if(strTitle == "Output5_8 Name"){	m_strOutputName[39] =  strData;	}
	else if(strTitle == "Output6_1 Name"){	m_strOutputName[40] =  strData;	}
	else if(strTitle == "Output6_2 Name"){	m_strOutputName[41] =  strData;	}
	else if(strTitle == "Output6_3 Name"){	m_strOutputName[42] =  strData;	}
	else if(strTitle == "Output6_4 Name"){	m_strOutputName[43] =  strData;	}
	else if(strTitle == "Output6_5 Name"){	m_strOutputName[44] =  strData;	}
	else if(strTitle == "Output6_6 Name"){	m_strOutputName[45] =  strData;	}
	else if(strTitle == "Output6_7 Name"){	m_strOutputName[46] =  strData;	}
	else if(strTitle == "Output6_8 Name"){	m_strOutputName[47] =  strData;	}
	
	else if(strTitle == "OutMask1_1"){ m_outputMask[0] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask1_2"){ m_outputMask[1] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask1_3"){ m_outputMask[2] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask1_4"){ m_outputMask[3] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask1_5"){ m_outputMask[4] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask1_6"){ m_outputMask[5] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask1_7"){ m_outputMask[6] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask1_8"){ m_outputMask[7] = (BYTE)atol(strData);	}

	else if(strTitle == "OutMask2_1"){ m_outputMask[8] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask2_2"){ m_outputMask[9] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask2_3"){ m_outputMask[10] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask2_4"){ m_outputMask[11] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask2_5"){ m_outputMask[12] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask2_6"){ m_outputMask[13] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask2_7"){ m_outputMask[14] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask2_8"){ m_outputMask[15] = (BYTE)atol(strData);	}

	else if(strTitle == "OutMask3_1"){ m_outputMask[16] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask3_2"){ m_outputMask[17] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask3_3"){ m_outputMask[18] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask3_4"){ m_outputMask[19] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask3_5"){ m_outputMask[20] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask3_6"){ m_outputMask[21] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask3_7"){ m_outputMask[22] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask3_8"){ m_outputMask[23] = (BYTE)atol(strData);	}

	else if(strTitle == "OutMask4_1"){ m_outputMask[24] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask4_2"){ m_outputMask[25] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask4_3"){ m_outputMask[26] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask4_4"){ m_outputMask[27] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask4_5"){ m_outputMask[28] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask4_6"){ m_outputMask[29] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask4_7"){ m_outputMask[30] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask4_8"){ m_outputMask[31] = (BYTE)atol(strData);	}

	else if(strTitle == "OutMask5_1"){ m_outputMask[32] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask5_2"){ m_outputMask[33] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask5_3"){ m_outputMask[34] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask5_4"){ m_outputMask[35] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask5_5"){ m_outputMask[36] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask5_6"){ m_outputMask[37] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask5_7"){ m_outputMask[38] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask5_8"){ m_outputMask[39] = (BYTE)atol(strData);	}

	else if(strTitle == "OutMask6_1"){ m_outputMask[40] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask6_2"){ m_outputMask[41] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask6_3"){ m_outputMask[42] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask6_4"){ m_outputMask[43] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask6_5"){ m_outputMask[44] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask6_6"){ m_outputMask[45] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask6_7"){ m_outputMask[46] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask6_8"){ m_outputMask[47] = (BYTE)atol(strData);	}
}

void CManualControlView::ClearIoState()
{
	int i = 0;
	m_BtnLatchOpen.SetColor(RGB_BLACK, RGB_GRAY);	
	m_BtnLatchClose.SetColor(RGB_BLACK, RGB_GRAY);	
	m_BtnClipOpen.SetColor(RGB_BLACK, RGB_GRAY);	
	m_BtnClipClose.SetColor(RGB_BLACK, RGB_GRAY);	
	m_BtnJigPlateback.SetColor(RGB_BLACK, RGB_GRAY);		
	m_BtnJigPlateForward.SetColor(RGB_BLACK, RGB_GRAY);	
	m_BtnClipPlateDown.SetColor(RGB_BLACK, RGB_GRAY);		
	m_BtnClipPlateUp.SetColor(RGB_BLACK, RGB_GRAY);	
	m_BtnClipPlateBack.SetColor(RGB_BLACK, RGB_GRAY);		
	m_BtnClipPlateForward.SetColor(RGB_BLACK, RGB_GRAY);
	m_BtnJigFanOff.SetColor(RGB_BLACK, RGB_GRAY);		
	m_BtnJigFanOn.SetColor(RGB_BLACK, RGB_GRAY);	

	m_BtnStopper1.SetColor(RGB_BLACK, RGB_GRAY);
	m_BtnStopper2.SetColor(RGB_BLACK, RGB_GRAY);	
	m_BtnStopper3.SetColor(RGB_BLACK, RGB_GRAY);	
	m_BtnStopper4.SetColor(RGB_BLACK, RGB_GRAY);
	
	for( i =0; i<MAX_READ_ADDR_CNT; i++ )
	{
		// Read Addr Set
		m_stateButton[i].Depress(FALSE);
	}
}

void CManualControlView::UpdateIoState( bool bForceUpdate )
{	
	if( m_nCurModuleID == 0 )
	{
		return;
	}

	EP_GP_DATA gpData = EPGetGroupData(m_nCurModuleID, 0);
	
	if( bForceUpdate == false )
	{
		if( m_nRecvIOData != gpData.gpState.ManualFunc )
		{
			ClearIoState();
			m_nRecvIOData = gpData.gpState.ManualFunc;			
			memcpy( &m_RecvIOData, &gpData.gpState.ManualFunc, sizeof(IO_Data) );	
		}
	}
	else
	{
		ClearIoState();
		m_nRecvIOData = gpData.gpState.ManualFunc;		
		memcpy( &m_RecvIOData, &gpData.gpState.ManualFunc, sizeof(IO_Data) );		
	}
		
	if( m_RecvIOData.ch_0 > 0 )
	{
		m_BtnLatchClose.SetColor(RGB_BLACK, RGB_LIGHTGREEN);		
	}
	else {		
		m_BtnLatchOpen.SetColor(RGB_BLACK, RGB_LIGHTGREEN);
	}
	
	if( m_RecvIOData.ch_1 > 0 )
	{
		m_BtnClipClose.SetColor(RGB_BLACK, RGB_LIGHTGREEN);		
	}
	else {	
		m_BtnClipOpen.SetColor(RGB_BLACK, RGB_LIGHTGREEN);
	}
	
	if( m_RecvIOData.ch_11 > 0 )
	{
		m_BtnJigPlateForward.SetColor(RGB_BLACK, RGB_LIGHTGREEN);		
	}
	else {	
		m_BtnJigPlateback.SetColor(RGB_BLACK, RGB_LIGHTGREEN);
	}
	
	if( m_RecvIOData.ch_7 > 0 )
	{
		m_BtnClipPlateUp.SetColor(RGB_BLACK, RGB_LIGHTGREEN);		
	}
	else {	
		m_BtnClipPlateDown.SetColor(RGB_BLACK, RGB_LIGHTGREEN);
	}
	
	if( m_RecvIOData.ch_8 > 0 )
	{
		m_BtnClipPlateForward.SetColor(RGB_BLACK, RGB_LIGHTGREEN);		
	}
	else
	{	
		m_BtnClipPlateBack.SetColor(RGB_BLACK, RGB_LIGHTGREEN);
	}
	
	if( m_RecvIOData.ch_9 > 0 )
	{
		m_BtnJigFanOn.SetColor(RGB_BLACK, RGB_LIGHTGREEN);
		
	}
	else {
		m_BtnJigFanOff.SetColor(RGB_BLACK, RGB_LIGHTGREEN);
	}
	
	if( m_RecvIOData.ch_2 > 0 )
	{
		m_LabelLeftStopper.SetText("OFF - OFF - ON - ON");
		m_LabelRightStopper.SetText("OFF - OFF - ON - ON");
		
		m_BtnStopper1.SetColor(RGB_BLACK, RGB_LIGHTGREEN);		
	}
	if( m_RecvIOData.ch_3 > 0 )
	{
		m_LabelLeftStopper.SetText("OFF - ON - ON - OFF");
		m_LabelRightStopper.SetText("OFF - ON - ON - OFF");
		
		m_BtnStopper2.SetColor(RGB_BLACK, RGB_LIGHTGREEN);		
	}
	if( m_RecvIOData.ch_4 > 0 )
	{
		m_LabelLeftStopper.SetText("ON - OFF - OFF - ON");
		m_LabelRightStopper.SetText("ON - OFF - OFF - ON");
		
		m_BtnStopper3.SetColor(RGB_BLACK, RGB_LIGHTGREEN);		
	}
	if( m_RecvIOData.ch_5 > 0 )
	{
		m_LabelLeftStopper.SetText("ON - ON - OFF - OFF");
		m_LabelRightStopper.SetText("ON - ON - OFF - OFF");
		
		m_BtnStopper4.SetColor(RGB_BLACK, RGB_LIGHTGREEN);		
	}

	int i=0, j=0;
	int nIndex = 0;

	CString strTemp;
	
	// for( i=0; i<16; i++ )
	for( i=0; i<10; i++ )
	{
		for(j =0; j< 8; j++)
		{
			nIndex = j + i * 8;
			m_stateButton[nIndex].Depress(gpData.gpState.IOSensorState[i] & (0x01<<j));			
		}		

		// strTemp.Format("====> [%d] : SENSOR_%d\n", i, gpData.gpState.IOSensorState[i]);
		// TRACE(strTemp);
	}
}

void CManualControlView::WriteData()
{
	UpdateData(TRUE);	

	int nRtn = 0;
	EP_CMD_IO_INFO IoInfo;
	ZeroMemory( &IoInfo, sizeof(EP_CMD_IO_INFO));
	
	memcpy( &IoInfo.manualFunc, &m_SendIOData, sizeof(IO_Data) );
	
	if((nRtn = EPSendCommand( m_nCurModuleID, 0, 0, EP_CMD_IO_SET_FUNC, &IoInfo, sizeof(EP_CMD_IO_INFO))) != EP_ACK)
	{
		AfxMessageBox(TEXT_LANG[22]);//"[WriteData] 명령 전송 실패!"
	}
}

void CManualControlView::RollBackOutState(int nPort)
{
	int i=0, j=0;
	if( nPort == 0 )
	{
		for( j=0; j<MAX_WRITE_PORT; j++ )
		{
			for( i=0; i<8; i++ )
			{
				((CButton *)GetDlgItem(IDC_OUT_CHECK1 + i + (j)*8))->SetCheck(0);				
			}
		}	
	}
	else
	{		
		for(i =0; i<8; i++)
		{
			((CButton *)GetDlgItem(IDC_OUT_CHECK1 + i + (nPort-1)*8))->SetCheck(0);
		}	
	}
}

bool CManualControlView::UpdateGroupState(int nModuleID, int /*nGroupIndex*/)
{
	if( nModuleID != m_nCurModuleID )
	{	
		// 1. 현재 실행중인 모듈이 아니기에 상태값을 변경하지 않는다.
		return false;
	}
	
	if( EPGetGroupState(nModuleID) == EP_STATE_LINE_OFF )
	{
		m_ctrlWorkMode.SetText("Line Off");
	}
	else
	{
		CCTSMonDoc *pDoc =  GetDocument();
		CFormModule *pModule = pDoc->GetModuleInfo(nModuleID);
		if(pModule != NULL)
		{
			m_nLineMode = pModule->GetOperationMode();

			switch( m_nLineMode )
			{
			case EP_OPERATION_LOCAL:
				{				
					ZeroMemory( &m_SendIOData, sizeof(IO_Data));
					m_SendIOData.ch_2 = 1;
					m_ctrlWorkMode.SetText("Local Mode");   
				}
				break;
			case EP_OPEARTION_MAINTENANCE:
				{
					m_ctrlWorkMode.SetText("Maintenance Mode"); 
				}
				break;
			case EP_OPERATION_AUTO:
				{
					m_ctrlWorkMode.SetText("Automatic Mode");
				}
				break;
			}	
		}
		else
		{
			m_ctrlWorkMode.SetText("Line Off");
			return false;
		}	
	}	
	
	return true;
}

void CManualControlView::UpdateSendBtnEnable( bool bEnable )
{
	GetDlgItem(IDC_BTN_LATCH_CLOSE)->EnableWindow(bEnable);
	GetDlgItem(IDC_BTN_LATCH_OPEN)->EnableWindow(bEnable);
	
}

void CManualControlView::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	UpdateIoState();
	
	if( m_bReadySetCmd )
	{
		m_bReadySetCmd = false;
		WriteData();
	}	
	
	CFormView::OnTimer(nIDEvent);
}

void CManualControlView::OnBnClickedLocalModeBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();
	CString strLog = _T("");
		
	WORD state;
	state = EPGetGroupState(m_nCurModuleID);
	if( state == EP_STATE_LINE_OFF )
	{
		strLog.Format(TEXT_LANG[23]);//"명령 전송을 실패 하였습니다.\n[ Stage Line off ]"
		pMainFrm->ShowInformation( m_nCurModuleID, strLog, INFO_TYPE_FAULT);
		return;
	}
	
	if( m_nLineMode == EP_OPERATION_AUTO )
	{
		strLog.Format(TEXT_LANG[24]);//"명령 전송을 실패 하였습니다.\n[ Automatic Mode 에서는 FMS에서 조작 가능합니다. ]"
		pMainFrm->ShowInformation( m_nCurModuleID, strLog, INFO_TYPE_FAULT);
		// AfxMessageBox("[Automatic Mode] 에서는 실행 할 수 없습니다.");
		return;
	}
	
	if( EPGetGroupState(m_nCurModuleID) == EP_STATE_LINE_OFF )
	{
		return;
	}	

	CCTSMonDoc *pDoc = GetDocument();
	CString strTemp = _T(""), strFailMD = _T("");

	strTemp.Format(TEXT_LANG[52], m_nCurModuleID );
	if(MessageBox(strTemp, "Mode Change", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;

	// 1. 전송할 Unit이 1개일 경우
	strTemp.Format("[%s] ", ::GetModuleName(m_nCurModuleID));
	if( pDoc->SetOperationMode(m_nCurModuleID, 0, EP_OPERATION_LOCAL) == false )
	{
		strFailMD += strTemp;
		strTemp.Format(TEXT_LANG[25],  ::GetModuleName(m_nCurModuleID));		//"%s는 local mode 변경에 실패 하였습니다."
		pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
	}
	
	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[26], strFailMD);//"%s 는 전송할 수 없는 상태이거나 전송을 실패 하였습니다."
		MessageBox(strTemp, ::GetStringTable(IDS_TEXT_SEND_FAIL), MB_OK|MB_ICONWARNING);		
	}
}

void CManualControlView::OnBnClickedMaintenanceModeBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();
	CCTSMonDoc *pDoc = GetDocument();
	CString strTemp = _T(""), strFailMD = _T("");
	
	WORD state;
	state = EPGetGroupState(m_nCurModuleID);
	if( state == EP_STATE_LINE_OFF )
	{
		strTemp.Format(TEXT_LANG[23]);//"명령 전송을 실패 하였습니다.\n[ Stage Line off ]"
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		return;		
	}
	
	if( m_nLineMode == EP_OPERATION_AUTO )
	{
		strTemp.Format(TEXT_LANG[24]);//"명령 전송을 실패 하였습니다.\n[ Automatic Mode 에서는 FMS에서 조작 가능합니다. ]"
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		// AfxMessageBox("[Automatic Mode] 에서는 실행 할 수 없습니다.");
		return;	
	}
	
	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_PERMISSION_ERROR) + " [Change to Maintenance]");
		return ;
	}
	
	strTemp.Format(TEXT_LANG[53], m_nCurModuleID);
	if(MessageBox(strTemp, "Mode Change", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;

	// 1. 전송할 Unit이 1개일 경우
	strTemp.Format("[%s] ", ::GetModuleName(m_nCurModuleID));
	
	if( pDoc->SetOperationMode(m_nCurModuleID, 0, EP_OPEARTION_MAINTENANCE) == false )
	{
		strFailMD += strTemp;
		strTemp.Format(TEXT_LANG[27], ::GetModuleName(m_nCurModuleID));		//"%s는 Maintenance mode 변경에 실패 하였습니다."
		pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
	}
	
	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[28], strFailMD);//"%s 에 명령 전송을 실패 하였습니다.[ Stage 상태정보 불일치 ]"
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		// strTemp.Format("%s 에 명령 전송을 실패 하였습니다.[ Stage 상태정보 불일치 ]", strFailMD);
		// MessageBox(strTemp, ::GetStringTable(IDS_TEXT_SEND_FAIL), MB_OK|MB_ICONWARNING);		
	}

	strTemp.Format(TEXT_LANG[29]);//"명령을 전송 하였습니다.\n잠시만 기다려주십시오."
	pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_NORMAL);
}

void CManualControlView::OnBnClickedRestartBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CCTSMonDoc *pDoc = GetDocument();
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();
	
	CString strTemp = _T(""), strFailMD = _T("");

	WORD state;
	state = EPGetGroupState(m_nCurModuleID);
	if( state == EP_STATE_LINE_OFF )
	{
		strTemp.Format(TEXT_LANG[23]);//"명령 전송을 실패 하였습니다.\n[ Stage Line off ]"
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		return;		
	}
	
	// 1. Contact Check 일 경우에만 공정을 진행한다.
	strTemp.Format("[Stage %s] ", ::GetModuleName(m_nCurModuleID));
	
	CFormModule *pModule = pDoc->GetModuleInfo(m_nCurModuleID);					
	if(pModule)
	{
		if( pModule->GetTrayInfo(0)->m_nResultFileType == EP_RESULTFILE_TYPE_CONTACT )
		{
			strTemp.Format(TEXT_LANG[54], ::GetModuleName(m_nCurModuleID));
			if(MessageBox(strTemp, "ReStart", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;					
		}
		else { 
			strFailMD +=  strTemp;
		}
	} 
	else { 
		strFailMD +=  strTemp; 
	}
	
	if( !strFailMD.IsEmpty() )
	{
		strTemp.Format(TEXT_LANG[30]);//"Contact Check Error 상태가 아닙니다."
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		// strTemp.Format("%s 는 Contact Check Error 상태가 아닙니다.", strFailMD );
		// MessageBox(strTemp, ::GetStringTable(IDS_TEXT_SEND_FAIL), MB_OK|MB_ICONWARNING);
		pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
		return;
	}	
	
	int nRtn = EP_NACK;

	state = EPGetGroupState(m_nCurModuleID);
	if(state == EP_STATE_PAUSE )	
	{		
		pModule->LoadResultData();

		((CMainFrame *)AfxGetMainWnd())->m_pTab->UpdateGroupState(m_nCurModuleID);
		
		if(( nRtn = EPSendCommand(m_nCurModuleID, 0, 0, EP_CMD_RESTART)) != EP_ACK)
		{
			strFailMD += strTemp;
			strTemp.Format("%s (%s-%s)",  ::GetStringTable(IDS_TEXT_CMD_SEND_FAIL),  ::GetModuleName(m_nCurModuleID), pDoc->CmdFailMsg(nRtn));		
			pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
		}
	}
	else if(state == EP_STATE_STANDBY || state == EP_STATE_READY)					
	{					
		EP_RUN_CMD_INFO	runInfo;					
		ZeroMemory(&runInfo, sizeof(EP_RUN_CMD_INFO));			

		runInfo.nTabDeepth = pModule->GetTrayInfo(0)->m_nTabDeepth;	
		runInfo.nTrayHeight = pModule->GetTrayInfo(0)->m_nTrayHeight;
		runInfo.nTrayType = pModule->GetTrayInfo(0)->m_nTrayType;	
		runInfo.nCurrentCellCnt = pModule->GetTrayInfo(0)->GetInputCellCnt();
		runInfo.nContactErrlimit = pModule->GetTrayInfo(0)->m_nContactErrlimit;
		runInfo.nCappErrlimit = pModule->GetTrayInfo(0)->m_nCapaErrlimit;
		runInfo.nChErrlimit = pModule->GetTrayInfo(0)->m_nChErrlimit;

		sprintf(runInfo.szTrayID, "%s", pModule->GetTrayInfo(0)->GetTrayNo() );

		//Module에 전송 정보 저장 (파일생성 이전에 )						
		pModule->UpdateStartTime();						
		pModule->UpdateTestLogTempFile();	//CheckEnableRun()에서 파일을 생성하므로 그 이전에 Update해야 한다.					
		
		//TestSerialNo 전송(실시간 저장 Data를 구별하는데 사용)
		//20061025 => 새로운 번호 생성(TestSerialNo는 Tray간의 고유번호로 사용되므로 다른 번호 생성)
		sprintf(runInfo.szTestSerialNo, pModule->GetTestSerialNo());

		runInfo.dwUseFlag = 1;
		runInfo.nTempGetPosition = atol(AfxGetApp()->GetProfileString(FORMSET_REG_SECTION, "Temp Measuring Position", "0"));

		CTray *pTray;						
		for(int nTrayIndex=0; nTrayIndex < INSTALL_TRAY_PER_MD; nTrayIndex++)						
		{						
			pTray = pModule->GetTrayInfo(nTrayIndex);						
			if(pTray)						
			{						
				runInfo.nInputCell[nTrayIndex] = pTray->lInputCellCount;						
			}						
		}

		if((nRtn = EPSendCommand(m_nCurModuleID, 1, 0, EP_CMD_RUN, &runInfo, sizeof(EP_RUN_CMD_INFO))) != EP_ACK)						
		{						
			strFailMD += strTemp;						
			strTemp.Format("%s (%s-%s)",  ::GetStringTable(IDS_TEXT_CMD_SEND_FAIL),  ::GetModuleName(m_nCurModuleID), pDoc->CmdFailMsg(nRtn));						
			pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);						
		}					
	}					
	else					
	{					
		strFailMD += strTemp;				
		strTemp.Format(TEXT_LANG[31],  ::GetModuleName(m_nCurModuleID));				//"%s 는 작업 재시작을 전송할 수 없는 상태이므로 전송하지 않았습니다."
		pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);				
	}

	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[32]);//"전송할 수 없는 상태이거나 전송을 실패 하였습니다."
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		// strTemp.Format("%s 는 전송할 수 없는 상태이거나 전송을 실패 하였습니다.", strFailMD);
		// MessageBox(strTemp, ::GetStringTable(IDS_TEXT_SEND_FAIL), MB_OK|MB_ICONWARNING);
		pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
	}
	else//FMS 초기화 [2013/9/10 cwm]
	{	
		pModule->GetTrayInfo(0)->m_nResultFileType = EP_RESULTFILE_TYPE_NORMAL;
		pModule->UpdateTestLogHeaderTempFile();

		pDoc->m_fmst.fnClearReset(m_nCurModuleID);

		strTemp.Format(TEXT_LANG[29]);//"명령을 전송 하였습니다.\n잠시만 기다려주십시오."
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_NORMAL);
	}
}

void CManualControlView::OnBnClickedResetBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CCTSMonDoc *pDoc = GetDocument();
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();
		
	CString strTemp = _T(""), strFailMD = _T("");

	pMainFrm->nModuleReadyToStart[m_nCurModuleID] = 0;

	WORD state;
	state = EPGetGroupState(m_nCurModuleID);
	if( state == EP_STATE_LINE_OFF )
	{
		strTemp.Format(TEXT_LANG[23]);//"명령 전송을 실패 하였습니다.\n[ Stage Line off ]"
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		return;		
	}
	
	strTemp.Format(GetStringTable(IDS_MSG_INIT_CONFIRM), ::GetModuleName(m_nCurModuleID));
	if(MessageBox(strTemp, "Reset", MB_ICONQUESTION|MB_YESNO) != IDYES)	 return;
	
	// 1. 전송할 Unit이 1개일 경우
	strTemp.Format("[%s] ", ::GetModuleName(m_nCurModuleID));
	state = EPGetGroupState(m_nCurModuleID);
		
	if(state != EP_STATE_RUN)	
	{
		if(pDoc->SendInitCommand(m_nCurModuleID, 0) == FALSE)
		{
			strFailMD += strTemp;			
		}
	}
	else
	{
		strFailMD += strTemp;		
	}	

	if(!strFailMD.IsEmpty())
	{	
		strTemp.Format(TEXT_LANG[33]);//"명령 전송을 실패 하였습니다.\n[ Stage 상태정보 불일치 ]"
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);		
		// strTemp.Format("%s 에 명령 전송을 실패 하였습니다.[ Stage 상태정보 불일치 ]", strFailMD);
		// MessageBox(strTemp, ::GetStringTable(IDS_TEXT_SEND_FAIL), MB_OK|MB_ICONWARNING);
	}
	
	strTemp.Format(TEXT_LANG[29]);//"명령을 전송 하였습니다.\n잠시만 기다려주십시오."
	pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_NORMAL);
}

void CManualControlView::OnBnClickedBtnStopper1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( m_nLineMode != EP_OPEARTION_MAINTENANCE )
	{
		m_LabelLog.SetText(TEXT_LANG[34]);//"[수동 조작] 기능은 Maintenance 상태에서만 실행할 수 있습니다."
		return;	
	}

	if( m_RecvIOData.ch_8 == 1 )
	{
		m_LabelLog.SetText(TEXT_LANG[35]);//"클립 플레이트 전진상태에서는 작동 시킬 수 없습니다."
		return;
	}

	m_SendIOData.ch_2 = 1;
	m_SendIOData.ch_3 = 0;
	m_SendIOData.ch_4 = 0;
	m_SendIOData.ch_5 = 0;
	
	m_bReadySetCmd = true;
}

void CManualControlView::OnBnClickedBtnStopper2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( m_nLineMode != EP_OPEARTION_MAINTENANCE )
	{
		m_LabelLog.SetText(TEXT_LANG[34]);//"[수동 조작] 기능은 Maintenance 상태에서만 실행할 수 있습니다."
		return;	
	}

	if( m_RecvIOData.ch_8 == 1 )
	{
		m_LabelLog.SetText(TEXT_LANG[35]);//"클립 플레이트 전진상태에서는 작동 시킬 수 없습니다."
		return;
	}

	m_SendIOData.ch_2 = 0;
	m_SendIOData.ch_3 = 1;
	m_SendIOData.ch_4 = 0;
	m_SendIOData.ch_5 = 0;
	
	m_bReadySetCmd = true;
}

void CManualControlView::OnBnClickedBtnStopper3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( m_nLineMode != EP_OPEARTION_MAINTENANCE )
	{
		m_LabelLog.SetText(TEXT_LANG[34]);//"[수동 조작] 기능은 Maintenance 상태에서만 실행할 수 있습니다."
		return;	
	}

	if( m_RecvIOData.ch_8 == 1 )
	{
		m_LabelLog.SetText(TEXT_LANG[35]);//"클립 플레이트 전진상태에서는 작동 시킬 수 없습니다."
		return;
	}

	m_SendIOData.ch_2 = 0;
	m_SendIOData.ch_3 = 0;
	m_SendIOData.ch_4 = 1;
	m_SendIOData.ch_5 = 0;
	
	m_bReadySetCmd = true;
}

void CManualControlView::OnBnClickedBtnStopper4()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( m_nLineMode != EP_OPEARTION_MAINTENANCE )
	{
		m_LabelLog.SetText(TEXT_LANG[34]);//"[수동 조작] 기능은 Maintenance 상태에서만 실행할 수 있습니다."
		return;	
	}

	if( m_RecvIOData.ch_8 == 1 )
	{
		m_LabelLog.SetText(TEXT_LANG[35]);//"클립 플레이트 전진상태에서는 작동 시킬 수 없습니다."
		return;
	}

	m_SendIOData.ch_2 = 0;
	m_SendIOData.ch_3 = 0;
	m_SendIOData.ch_4 = 0;
	m_SendIOData.ch_5 = 1;
	
	m_bReadySetCmd = true;
}

void CManualControlView::OnBnClickedBtnStopperSetting()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strTemp = _T("");
	CStopperSettingDlg dlg;
	dlg.m_Stopper1 = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Stopper1", "310");
	dlg.m_Stopper2 = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Stopper2", "250");
	dlg.m_Stopper3 = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Stopper3", "200");
	dlg.m_Stopper4 = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Stopper4", "180");


	dlg.m_strLabelName1 ="Stopper1";
	dlg.m_strLabelName2 ="Stopper2";
	dlg.m_strLabelName3 ="Stopper3";
	dlg.m_strLabelName4 ="Stopper4";
	if( dlg.DoModal() == IDOK )
	{	
		strTemp.Format("#1 (%s)", dlg.m_Stopper1);
		GetDlgItem(IDC_BTN_STOPPER1)->SetWindowText(strTemp);
		AfxGetApp()->WriteProfileString(FORMSET_REG_SECTION , "Stopper1", strTemp);	
		strTemp.Format("#2 (%s)", dlg.m_Stopper2);
		GetDlgItem(IDC_BTN_STOPPER2)->SetWindowText(strTemp);
		AfxGetApp()->WriteProfileString(FORMSET_REG_SECTION , "Stopper2", strTemp);	
		strTemp.Format("#3 (%s)", dlg.m_Stopper3);
		GetDlgItem(IDC_BTN_STOPPER3)->SetWindowText(strTemp);
		AfxGetApp()->WriteProfileString(FORMSET_REG_SECTION , "Stopper3", strTemp);	
		strTemp.Format("#4 (%s)", dlg.m_Stopper4);
		GetDlgItem(IDC_BTN_STOPPER4)->SetWindowText(strTemp);
		AfxGetApp()->WriteProfileString(FORMSET_REG_SECTION , "Stopper4", strTemp);


	}
}

void CManualControlView::OnBnClickedBtnLatchClose()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( m_nLineMode != EP_OPEARTION_MAINTENANCE )
	{
		m_LabelLog.SetText(TEXT_LANG[34]);//"[수동 조작] 기능은 Maintenance 상태에서만 실행할 수 있습니다."
		return;	
	}
	
	m_SendIOData.ch_0 = 1;
	m_LabelLog.SetText("-");

	m_bReadySetCmd = true;
}

void CManualControlView::OnBnClickedBtnLatchOpen()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( m_nLineMode != EP_OPEARTION_MAINTENANCE )
	{
		m_LabelLog.SetText(TEXT_LANG[34]);//"[수동 조작] 기능은 Maintenance 상태에서만 실행할 수 있습니다."
		return;	
	}
	
	m_SendIOData.ch_0 = 0;
	m_LabelLog.SetText("-");
	
	m_bReadySetCmd = true;
}

void CManualControlView::OnBnClickedBtnClipClose()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( m_nLineMode != EP_OPEARTION_MAINTENANCE )
	{
		m_LabelLog.SetText(TEXT_LANG[34]);//"[수동 조작] 기능은 Maintenance 상태에서만 실행할 수 있습니다."
		return;	
	}
	
	m_SendIOData.ch_1 = 1;
	m_LabelLog.SetText("-");
	
	m_bReadySetCmd = true;
}

void CManualControlView::OnBnClickedBtnClipOpen()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( m_nLineMode != EP_OPEARTION_MAINTENANCE )
	{
		m_LabelLog.SetText(TEXT_LANG[34]);//"[수동 조작] 기능은 Maintenance 상태에서만 실행할 수 있습니다."
		return;	
	}
	
	m_SendIOData.ch_1 = 0;
	m_LabelLog.SetText("-");
	
	m_bReadySetCmd = true;
}

void CManualControlView::OnBnClickedBtnJigPlateForward()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( m_nLineMode != EP_OPEARTION_MAINTENANCE )
	{
		m_LabelLog.SetText(TEXT_LANG[34]);//"[수동 조작] 기능은 Maintenance 상태에서만 실행할 수 있습니다."
		return;	
	}
	
	if( m_RecvIOData.ch_1 == 1 )
	{
		m_LabelLog.SetText(TEXT_LANG[36]);//"클립이 닫힌 상태에서는 작동 시킬 수 없습니다."
		return;	
	}

	CCTSMonDoc *pDoc = GetDocument();
	
	m_SendIOData.ch_11 = 1;
	m_LabelLog.SetText("-");
	
	m_bReadySetCmd = true;
}

void CManualControlView::OnBnClickedBtnJigPlateBack()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( m_nLineMode != EP_OPEARTION_MAINTENANCE )
	{
		m_LabelLog.SetText(TEXT_LANG[34]);//"[수동 조작] 기능은 Maintenance 상태에서만 실행할 수 있습니다."
		return;	
	}
	
	if( m_RecvIOData.ch_1 == 1 )
	{
		m_LabelLog.SetText(TEXT_LANG[36]);//"클립이 닫힌 상태에서는 작동 시킬 수 없습니다."
		return;	
	}
	
	m_SendIOData.ch_11 = 0;
	m_LabelLog.SetText("-");
	
	m_bReadySetCmd = true;
}

void CManualControlView::OnBnClickedBtnClipPlateUp()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( m_nLineMode != EP_OPEARTION_MAINTENANCE )
	{
		m_LabelLog.SetText(TEXT_LANG[34]);//"[수동 조작] 기능은 Maintenance 상태에서만 실행할 수 있습니다."
		return;	
	}
	
	if( m_RecvIOData.ch_1 == 1 )
	{
		m_LabelLog.SetText(TEXT_LANG[36]);//"클립이 닫힌 상태에서는 작동 시킬 수 없습니다."
		return;	
	}	

	m_SendIOData.ch_7 = 1;
	m_LabelLog.SetText("-");
	
	m_bReadySetCmd = true;
}

void CManualControlView::OnBnClickedBtnClipPlateDown()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( m_nLineMode != EP_OPEARTION_MAINTENANCE )
	{
		m_LabelLog.SetText(TEXT_LANG[34]);//"[수동 조작] 기능은 Maintenance 상태에서만 실행할 수 있습니다."
		return;	
	}
	
	if( m_RecvIOData.ch_1 == 1 )
	{
		m_LabelLog.SetText(TEXT_LANG[36]);//"클립이 닫힌 상태에서는 작동 시킬 수 없습니다."
		return;	
	}

	m_SendIOData.ch_7 = 0;
	m_LabelLog.SetText("-");

	m_bReadySetCmd = true;
}

void CManualControlView::OnBnClickedBtnClipPlateForward()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( m_nLineMode != EP_OPEARTION_MAINTENANCE )
	{
		m_LabelLog.SetText(TEXT_LANG[34]);//"[수동 조작] 기능은 Maintenance 상태에서만 실행할 수 있습니다."
		return;	
	}
	
	if( m_RecvIOData.ch_1 == 1 )
	{
		m_LabelLog.SetText(TEXT_LANG[36]);//"클립이 닫힌 상태에서는 작동 시킬 수 없습니다."
		return;	
	}

	m_SendIOData.ch_8 = 1;
	m_LabelLog.SetText("-");

	m_bReadySetCmd = true;
}

void CManualControlView::OnBnClickedBtnClipPlateBack()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( m_nLineMode != EP_OPEARTION_MAINTENANCE )
	{
		m_LabelLog.SetText(TEXT_LANG[34]);//"[수동 조작] 기능은 Maintenance 상태에서만 실행할 수 있습니다."
		return;	
	}
	
	if( m_RecvIOData.ch_1 == 1 )
	{
		m_LabelLog.SetText(TEXT_LANG[36]);//"클립이 닫힌 상태에서는 작동 시킬 수 없습니다."
		return;	
	}
	
	m_SendIOData.ch_8 = 0;
	m_LabelLog.SetText("-");

	m_bReadySetCmd = true;
}

void CManualControlView::OnBnClickedBtnJigFanOn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( m_nLineMode != EP_OPEARTION_MAINTENANCE )
	{
		m_LabelLog.SetText(TEXT_LANG[34]);//"[수동 조작] 기능은 Maintenance 상태에서만 실행할 수 있습니다."
		return;	
	}
	
	m_SendIOData.ch_9 = 1;
	m_LabelLog.SetText("-");

	m_bReadySetCmd = true;
}

void CManualControlView::OnBnClickedBtnJigFanOff()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( m_nLineMode != EP_OPEARTION_MAINTENANCE )
	{
		m_LabelLog.SetText(TEXT_LANG[34]);//"[수동 조작] 기능은 Maintenance 상태에서만 실행할 수 있습니다."
		return;	
	}

	m_SendIOData.ch_9 = 0;
	m_LabelLog.SetText("-");
	
	WriteData();
}

void CManualControlView::OnBnClickedBtnNormalSensor()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( m_pNormalSensorMapDlg == NULL )
	{	
		m_pNormalSensorMapDlg = new CNormalSensorMapDlg(GetDocument()->m_nDeviceID);
		m_pNormalSensorMapDlg->Create(IDD_NORMAL_SENSOR_PIC_DLG, this);
		m_pNormalSensorMapDlg->CenterWindow();
		m_pNormalSensorMapDlg->ShowWindow(SW_SHOW);	
	}
	else
	{
		m_pNormalSensorMapDlg->ShowWindow(SW_SHOW);
	}
}

void CManualControlView::OnBnClickedBuzzerOffBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CCTSMonDoc *pDoc = GetDocument();
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();
		
	CString strTemp = _T(""), strFailMD = _T("");

	WORD state;
	int nRtn = EP_NACK;

	state = EPGetGroupState(m_nCurModuleID);
	if( state != EP_STATE_LINE_OFF )
	{	
		if(( nRtn = EPSendCommand(m_nCurModuleID, 1, 0, EP_CMD_BUZZER_OFF)) != EP_ACK)
		{
			strFailMD += strTemp;
			strTemp.Format("%s (%s-%s)",  ::GetStringTable(IDS_TEXT_CMD_SEND_FAIL),  ::GetModuleName(m_nCurModuleID), pDoc->CmdFailMsg(nRtn));		
			pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
		}
	}
	else
	{
		strTemp.Format(TEXT_LANG[23]);		//"명령 전송을 실패 하였습니다.\n[ Stage Line off ]"
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		return;
		// strTemp.Format("[Stage %s] 는 'Buzzer Off' 명령을 전송할 수 없는 상태 입니다.[Line Off]",  ::GetModuleName(m_nCurModuleID));
		// MessageBox(strTemp, ::GetStringTable(IDS_TEXT_SEND_FAIL), MB_OK|MB_ICONWARNING);
	}

	if( !strFailMD.IsEmpty() )
	{
		strTemp.Format(TEXT_LANG[38]);//"명령 전송을 실패 하였습니다."
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp);
	}
	else
	{
		strTemp.Format(TEXT_LANG[29]);//"명령을 전송 하였습니다.\n잠시만 기다려주십시오."
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_NORMAL);
	}
}

void CManualControlView::OnBnClickedEmergencyStopBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CCTSMonDoc *pDoc = GetDocument();
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();
		
	CString strTemp = _T(""), strFailMD = _T("");

	WORD state;
	int nRtn = EP_NACK;

	state = EPGetGroupState(m_nCurModuleID);
	if( state != EP_STATE_LINE_OFF )
	{	
		if(( nRtn = EPSendCommand(m_nCurModuleID, 1, 0, EP_CMD_EMERGENCY_STOP)) != EP_ACK)
		{
			strFailMD += strTemp;
			strTemp.Format("%s (%s-%s)",  ::GetStringTable(IDS_TEXT_CMD_SEND_FAIL),  ::GetModuleName(m_nCurModuleID), pDoc->CmdFailMsg(nRtn));		
			pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
		}
		// EPSendCommand(m_nCurModuleID);
	}
	else
	{
		strTemp.Format(TEXT_LANG[23]);//"명령 전송을 실패 하였습니다.\n[ Stage Line off ]"
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);		
		return;
		// strTemp.Format("[Stage %s] 는 'Emergency Stop' 명령을 전송할 수 없는 상태 입니다.[Line Off]",  ::GetModuleName(m_nCurModuleID));
		// MessageBox(strTemp, ::GetStringTable(IDS_TEXT_SEND_FAIL), MB_OK|MB_ICONWARNING);
	}

	if( !strFailMD.IsEmpty() )
	{
		strTemp.Format(TEXT_LANG[38]);//"명령 전송을 실패 하였습니다."
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
	}
	else
	{
		strTemp.Format(TEXT_LANG[29]);//"명령을 전송 하였습니다.\n잠시만 기다려주십시오."
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_NORMAL);
	}
}

void CManualControlView::OnBnClickedContinueBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CCTSMonDoc *pDoc = GetDocument();
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();
		
	CString strTemp = _T(""), strFailMD = _T("");

	WORD state;
	state = EPGetGroupState(m_nCurModuleID);
	if( state == EP_STATE_LINE_OFF )
	{
		strTemp.Format(TEXT_LANG[23]);//"명령 전송을 실패 하였습니다.\n[ Stage Line off ]"
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);				
		return;		
	}

	// 1. 전송할 Unit이 1개일 경우
	strTemp.Format("[Stage %s] ", ::GetModuleName(m_nCurModuleID));
	state = EPGetGroupState(m_nCurModuleID);
	
	CFormModule *pModule = pDoc->GetModuleInfo(m_nCurModuleID);					
	if(pModule)
	{
		// 1. EMG 이거나 정전발생시 재시작이 가능하도록 수정
		if( pModule->GetTrayInfo(0)->m_nResultFileType == EP_RESULTFILE_TYPE_EMG 
		|| pModule->GetTrayInfo(0)->m_nResultFileType == EP_RESULTFILE_TYPE_RES
		|| state == EP_STATE_PAUSE )
		{
			strTemp.Format(GetStringTable(IDS_MSG_CONTINUE_CONFIRM), ::GetModuleName(m_nCurModuleID));
			if(MessageBox(strTemp, "Continue", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;				
		}
		else { 
			strFailMD +=  strTemp;
		}
	}
	else { 
		strFailMD +=  strTemp;
	}
	
	if( !strFailMD.IsEmpty() )
	{
		strTemp.Format(TEXT_LANG[39]);//"명령 전송을 실패 하였습니다.\n[ Stage 상태 불일치(Error) ]"
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		//strTemp.Format("%s 는 Error 상태가 아닙니다.", strFailMD );
		//MessageBox(strTemp, ::GetStringTable(IDS_TEXT_SEND_FAIL), MB_OK|MB_ICONWARNING);
		pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
		return;
	}
	
	int nRtn = EP_NACK;
	strTemp.Format("[Stage %s] ", ::GetModuleName(m_nCurModuleID));
	
	if( state == EP_STATE_PAUSE )
	{	
		CString strResultData = _T("");
		CString strTempResultFileName = _T("");
				
		char	drive[_MAX_PATH]; 
		char	dir[_MAX_PATH]; 
		char	fname[_MAX_PATH]; 
		char	ext[_MAX_PATH]; 
		
		strResultData = pModule->GetResultFileName(0);
		
		_splitpath((LPSTR)(LPCTSTR)strResultData, drive, dir, fname, ext);	

		strTempResultFileName.Format("%s\\%s\\%s_TEMP.fmt", drive, dir, fname);

		if( _access(strTempResultFileName, 0) != -1 )
		{
			if( _unlink(strResultData) != -1 )
			{
				CopyFile( strTempResultFileName, strResultData, false );			
			}
		}

		// Module 클래스의 공정 정보를 초기화 후 다시 불러온다.
		pModule->LoadResultData();

		((CMainFrame *)AfxGetMainWnd())->m_pTab->UpdateGroupState(m_nCurModuleID);			

		if(( nRtn = EPSendCommand(m_nCurModuleID, 0, 0, EP_CMD_CONTINUE)) != EP_ACK)
		{
			strFailMD += strTemp;
			strTemp.Format("%s (%s-%s)",  ::GetStringTable(IDS_TEXT_CMD_SEND_FAIL),  ::GetModuleName(m_nCurModuleID), pDoc->CmdFailMsg(nRtn));		
			pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
		}
	}
	else if(state == EP_STATE_STANDBY || state == EP_STATE_READY )					
	{						
		EP_RUN_CMD_INFO	runInfo;					
		ZeroMemory(&runInfo, sizeof(EP_RUN_CMD_INFO));			

		runInfo.nTabDeepth = pModule->GetTrayInfo(0)->m_nTabDeepth;	
		runInfo.nTrayHeight = pModule->GetTrayInfo(0)->m_nTrayHeight;
		runInfo.nTrayType = pModule->GetTrayInfo(0)->m_nTrayType;	
		runInfo.nCurrentCellCnt = pModule->GetTrayInfo(0)->GetInputCellCnt();
		runInfo.nContactErrlimit = pModule->GetTrayInfo(0)->m_nContactErrlimit;
		runInfo.nCappErrlimit = pModule->GetTrayInfo(0)->m_nCapaErrlimit;
		runInfo.nChErrlimit = pModule->GetTrayInfo(0)->m_nChErrlimit;
		sprintf(runInfo.szTrayID, "%s", pModule->GetTrayInfo(0)->GetTrayNo() );

		//Module에 전송 정보 저장 (파일생성 이전에 )						
		pModule->UpdateStartTime();						
		pModule->UpdateTestLogTempFile();	//CheckEnableRun()에서 파일을 생성하므로 그 이전에 Update해야 한다.					
		
		runInfo.dwUseFlag = 1;
		runInfo.nTempGetPosition = atol(AfxGetApp()->GetProfileString(FORMSET_REG_SECTION, "Temp Measuring Position", "0"));

		sprintf(runInfo.szTestSerialNo, pModule->GetTestSerialNo());
		CTray *pTray;						
		for(int nTrayIndex=0; nTrayIndex < INSTALL_TRAY_PER_MD; nTrayIndex++)						
		{						
			pTray = pModule->GetTrayInfo(nTrayIndex);						
			if(pTray)						
			{						
				runInfo.nInputCell[nTrayIndex] = pTray->lInputCellCount;						
			}						
		}

		if((nRtn = EPSendCommand(m_nCurModuleID, 1, 0, EP_CMD_RUN, &runInfo, sizeof(EP_RUN_CMD_INFO))) != EP_ACK)						
		{						
			strFailMD += strTemp;						
			strTemp.Format("%s (%s-%s)",  ::GetStringTable(IDS_TEXT_CMD_SEND_FAIL),  ::GetModuleName(m_nCurModuleID), pDoc->CmdFailMsg(nRtn));						
			pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);						
		}		
	}					
	else					
	{					
		strFailMD += strTemp;				
		strTemp.Format(TEXT_LANG[40],  ::GetModuleName(m_nCurModuleID));				//"%s 는 계속진행을 전송할 수 없는 상태이므로 전송하지 않았습니다."
		pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);				
	}

	if( !strFailMD.IsEmpty() )
	{
		strTemp.Format(TEXT_LANG[41]);//"명령 전송을 실패 하였습니다.\n[ 전송 실패 ]"
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
	}
	else
	{
		pModule->GetTrayInfo(0)->m_nResultFileType = EP_RESULTFILE_TYPE_NORMAL;	
		pModule->UpdateTestLogHeaderTempFile();

		pDoc->m_fmst.fnClearReset(m_nCurModuleID);

		strTemp.Format(TEXT_LANG[29]);//"명령을 전송 하였습니다.\n잠시만 기다려주십시오."
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_NORMAL);
	}
}

void CManualControlView::OnBnClickedFmsReset1Btn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CCTSMonDoc *pDoc = GetDocument();
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();
	
	CString strTemp = _T("");

	WORD state = EPGetGroupState(m_nCurModuleID);
	if( state == EP_STATE_READY )
	{
		strTemp.Format(TEXT_LANG[42],  ::GetModuleName(m_nCurModuleID));//"[Stage %s] 입고 예약을 취소합니다."
		if( MessageBox(strTemp, TEXT_LANG[43], MB_OKCANCEL|MB_ICONWARNING) != IDOK ) return;//"입고 예약 취소 확인"
				
		pDoc->SendInitCommand(m_nCurModuleID);

		strTemp.Format(TEXT_LANG[29]);//"명령을 전송 하였습니다.\n잠시만 기다려주십시오."
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_NORMAL);
	}
	else
	{	
		strTemp.Format(TEXT_LANG[44]);//"입고 예약을 받은 상태가 아닙니다."
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		// strTemp.Format(TEXT_LANG[45],  ::GetModuleName(m_nCurModuleID));//"[Stage %s] 는 예약 상태가 아닙니다."
		// MessageBox(strTemp, ::GetStringTable(IDS_TEXT_SEND_FAIL), MB_OK|MB_ICONWARNING);
	}
}

void CManualControlView::OnBnClickedFmsReset2Btn()
{
	CString strTemp = _T("");
	CCTSMonDoc *pDoc = GetDocument();
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();

	if(pDoc->m_fmst.fnTrayInStateReset(m_nCurModuleID) == FALSE )
	{
		strTemp.Format(TEXT_LANG[46]);//"입고 예약된 공정을 진행할 수 있는 상태가 아닙니다."
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		// strTemp.Format("[Stage %s] 는 예약된 공정을 진행할 수 있는 상태가 아닙니다.",  ::GetModuleName(m_nCurModuleID));
		// MessageBox(strTemp, ::GetStringTable(IDS_TEXT_SEND_FAIL), MB_OK|MB_ICONWARNING);
	}
	else
	{
		strTemp.Format(TEXT_LANG[47],  ::GetModuleName(m_nCurModuleID));//"[Stage %s] 입고 예약된 공정을 진행합니다."
		if( MessageBox(strTemp, TEXT_LANG[48], MB_OKCANCEL|MB_ICONWARNING) != IDOK ) return;//"공정 진행 확인"
		
		strTemp.Format(TEXT_LANG[29]);//"명령을 전송 하였습니다.\n잠시만 기다려주십시오."
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_NORMAL);
	}
	/*
	CString strTemp;
		
	strTemp.Format("[Stage %s] 오류 상태 색상을 초기화 합니다.",  ::GetModuleName(m_nCurModuleID));
	if( MessageBox(strTemp, "색상 초기화", MB_OKCANCEL|MB_ICONWARNING) != IDOK ) return;
	
	if(pDoc->m_fmst.fnClearColor(m_nCurModuleID))
	{

	}
	else
	{	
		strTemp.Format("[Stage %s] 는 시간 초과 상태가 아닙니다.",  ::GetModuleName(m_nCurModuleID));
		MessageBox(strTemp, ::GetStringTable(IDS_TEXT_SEND_FAIL), MB_OK|MB_ICONWARNING);
	}
	*/
}

void CManualControlView::OnBnClickedFmsEndCmdSendBtn()
{
	//end 처음부터 시작
	CCTSMonDoc *pDoc = GetDocument();
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();
	
	CString strTemp = _T("");
		
	if(pDoc->m_fmst.fnEndStateReset(m_nCurModuleID))
	{
		strTemp.Format(TEXT_LANG[49],  ::GetModuleName(m_nCurModuleID));//"[Stage %s] 검사 종료 통지 Command를 재전송합니다."
		if( MessageBox(strTemp, TEXT_LANG[50], MB_OKCANCEL|MB_ICONWARNING) != IDOK ) return;//"검사 종료 통지 Command 전송"
		
		strTemp.Format(TEXT_LANG[29]);//"명령을 전송 하였습니다.\n잠시만 기다려주십시오."
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_NORMAL);
	}
	else
	{	
		strTemp.Format(TEXT_LANG[51]);//"공정이 완료된 상태가 아닙니다."
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		// strTemp.Format("[Stage %s] 는 종료 상태가 아닙니다.",  ::GetModuleName(m_nCurModuleID));
		// MessageBox(strTemp, ::GetStringTable(IDS_TEXT_SEND_FAIL), MB_OK|MB_ICONWARNING);
	}
}
void CManualControlView::OnBnClickedBtnTypeSelSetting()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strTemp = _T("");
	CStopperSettingDlg dlg;
	dlg.m_Stopper1 = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "TypeSel1", "000");
	dlg.m_Stopper2 = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "TypeSel2", "100");
	dlg.m_Stopper3 = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "TypeSel3", "900");
	dlg.m_Stopper4 = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "TypeSel4", "700");

	dlg.m_strLabelName1 ="#P";
	dlg.m_strLabelName2 ="#T";
	dlg.m_strLabelName3 ="#D";
	dlg.m_strLabelName4 ="#R";
	if( dlg.DoModal() == IDOK )
	{	
		strTemp.Format("#P (%s)", dlg.m_Stopper1);
		m_LabelTypeSel1.SetText(strTemp);
		AfxGetApp()->WriteProfileString(FORMSET_REG_SECTION , "TypeSel1", strTemp);	
		strTemp.Format("#T (%s)", dlg.m_Stopper2);
		m_LabelTypeSel2.SetText(strTemp);
		AfxGetApp()->WriteProfileString(FORMSET_REG_SECTION , "TypeSel2", strTemp);	
		strTemp.Format("#D (%s)", dlg.m_Stopper3);
		m_LabelTypeSel3.SetText(strTemp);
		AfxGetApp()->WriteProfileString(FORMSET_REG_SECTION , "TypeSel3", strTemp);	
		strTemp.Format("#R (%s)", dlg.m_Stopper4);
		m_LabelTypeSel4.SetText(strTemp);	
		AfxGetApp()->WriteProfileString(FORMSET_REG_SECTION , "TypeSel4", strTemp);	
	}
}

