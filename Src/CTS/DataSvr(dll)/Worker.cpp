// Worker.cpp : implementation file
//

#include "stdafx.h"
#include "Worker.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

Worker::Worker() : m_nCurrThreads (0), m_nMaxThreads (0), m_nRequests(0), CWizRawSocketListener (_DATA_SVR_TCPIP_PORT)
{
}

BOOL Worker::TreatData(HANDLE hShutDownEvent, HANDLE hDataTakenEvent)
{
	{
		CWizCritSect wcs (cs);
		if (++m_nCurrThreads > m_nMaxThreads)
			m_nMaxThreads = m_nCurrThreads;
	}

	if (!CWizRawSocketListener::TreatData (hShutDownEvent, hDataTakenEvent))
		return FALSE;

	{
		CWizCritSect wcs (cs);
		--m_nCurrThreads;
		++m_nRequests;
	}
	return TRUE;
}

BOOL Worker::ReadWrite (CWizReadWriteSocket& /*socket*/)
{
	return TRUE;
}



