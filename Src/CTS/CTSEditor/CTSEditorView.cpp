// CTSEditorView.cpp : implementation of the CCTSEditorView class
//

#include "stdafx.h"
#include "CTSEditor.h"

#include "CTSEditorDoc.h"
#include "CTSEditorView.h"

#include "ProcDataRecordSet.h"
#include "TestTypeRecordset.h"

#include "EditorSetDlg.h"
//#include "ReportDataSelDlg.h"

#include "ModelNameDlg.h"
#include "TestNameDlg.h"

#include "PrntScreen.h"
#include "CommonInputDlg.h"
#include "UserAdminDlg.h"
#include "ChangeUserInfoDlg.h"
#include "ModelInfoDlg.h"
#include "SelectSimulDataDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCTSEditorView

IMPLEMENT_DYNCREATE(CCTSEditorView, CFormView)

BEGIN_MESSAGE_MAP(CCTSEditorView, CFormView)
	//{{AFX_MSG_MAP(CCTSEditorView)
	ON_WM_LBUTTONDOWN()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_LOAD_TEST, OnLoadTest)
	ON_BN_CLICKED(IDC_MODEL_NEW, OnModelNew)
	ON_BN_CLICKED(IDC_TEST_NEW, OnTestNew)
	ON_BN_CLICKED(IDC_MODEL_DELETE, OnModelDelete)
	ON_BN_CLICKED(IDC_TEST_DELETE, OnTestDelete)
	ON_BN_CLICKED(IDC_MODEL_SAVE, OnModelSave)
	ON_BN_CLICKED(IDC_TEST_SAVE, OnTestSave)
	ON_BN_CLICKED(IDC_STEP_SAVE, OnStepSave)
	ON_BN_CLICKED(IDC_STEP_DELETE, OnStepDelete)
	ON_BN_CLICKED(IDC_STEP_INSERT, OnStepInsert)
	ON_BN_CLICKED(IDC_GRADE_CHECK, OnGradeCheck)
	ON_BN_CLICKED(IDC_PRETEST_CHECK, OnPretestCheck)
	ON_EN_CHANGE(IDC_PRETEST_MAXV, OnChangePretestMaxv)
	ON_EN_CHANGE(IDC_PRETEST_DELTA_V, OnChangePretestDeltaV)
//	ON_EN_CHANGE(IDC_PRETEST_FAULT_NO, OnChangePretestFaultNo)
	ON_EN_CHANGE(IDC_PRETEST_MAXI, OnChangePretestMaxi)
	ON_EN_CHANGE(IDC_PRETEST_MINV, OnChangePretestMinv)
	ON_EN_CHANGE(IDC_PRETEST_OCV, OnChangePretestOcv)
	ON_EN_CHANGE(IDC_PRETEST_TRCKLE_I, OnChangePretestTrckleI)
	ON_EN_CHANGE(IDC_VTG_HIGH, OnChangeVtgHigh)
	ON_EN_CHANGE(IDC_VTG_LOW, OnChangeVtgLow)
	ON_EN_CHANGE(IDC_CRT_HIGH, OnChangeCrtHigh)
	ON_EN_CHANGE(IDC_CRT_LOW, OnChangeCrtLow)
	ON_EN_CHANGE(IDC_CAP_HIGH, OnChangeCapHigh)
	ON_EN_CHANGE(IDC_CAP_LOW, OnChangeCapLow)
	ON_EN_CHANGE(IDC_IMP_HIGH, OnChangeImpHigh)
	ON_EN_CHANGE(IDC_IMP_LOW, OnChangeImpLow)
	ON_EN_CHANGE(IDC_DELTA_V, OnChangeDeltaV)
	ON_EN_CHANGE(IDC_DELTA_I, OnChangeDeltaI)
	ON_EN_CHANGE(IDC_DELTA_TIME, OnChangeDeltaTime)
	ON_COMMAND(ID_OPTION, OnOption)
	ON_EN_CHANGE(IDC_COMP_V1, OnChangeCompV1)
	ON_EN_CHANGE(IDC_COMP_V2, OnChangeCompV2)
	ON_EN_CHANGE(IDC_COMP_V3, OnChangeCompV3)
	ON_EN_CHANGE(IDC_COMP_TIME1, OnChangeCompTime1)
	ON_EN_CHANGE(IDC_COMP_TIME2, OnChangeCompTime2)
	ON_EN_CHANGE(IDC_COMP_TIME3, OnChangeCompTime3)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, OnUpdateEditCopy)
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
	ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE, OnUpdateEditPaste)
	ON_COMMAND(ID_EDIT_CUT, OnEditCut)
	ON_UPDATE_COMMAND_UI(ID_EDIT_CUT, OnUpdateEditCut)
	ON_COMMAND(ID_EDIT_UNDO, OnEditUndo)
	ON_UPDATE_COMMAND_UI(ID_EDIT_UNDO, OnUpdateEditUndo)
	ON_BN_CLICKED(IDC_MODEL_COPY_BUTTON, OnModelCopyButton)
	ON_COMMAND(ID_EXCEL_SAVE, OnExcelSave)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE, OnUpdateFileSave)
	ON_UPDATE_COMMAND_UI(ID_FILE_OPEN, OnUpdateFileOpen)
	ON_COMMAND(ID_FILE_SAVE, OnFileSave)
	ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
	ON_UPDATE_COMMAND_UI(ID_EXCEL_SAVE, OnUpdateExcelSave)
	ON_NOTIFY(TCN_SELCHANGE, IDC_PARAM_TAB, OnSelchangeParamTab)
	ON_BN_CLICKED(IDC_EXT_OPTION_CHECK, OnExtOptionCheck)
	ON_BN_CLICKED(IDC_MODEL_EDIT, OnModelEdit)
	ON_BN_CLICKED(IDC_TEST_EDIT, OnTestEdit)
	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
	ON_COMMAND(ID_INSERT_STEP, OnInsertStep)
	ON_COMMAND(ID_DELETE_STEP, OnDeleteStep)
	ON_UPDATE_COMMAND_UI(ID_DELETE_STEP, OnUpdateDeleteStep)
	ON_UPDATE_COMMAND_UI(ID_INSERT_STEP, OnUpdateInsertStep)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_TEST_COPY_BUTTON, OnTestCopyButton)
	ON_COMMAND(ID_EDIT_INSERT, OnEditInsert)
	ON_UPDATE_COMMAND_UI(ID_EDIT_INSERT, OnUpdateEditInsert)
	ON_EN_CHANGE(IDC_REPORT_CURRENT, OnChangeReportCurrent)
	ON_EN_CHANGE(IDC_REPORT_TEMPERATURE, OnChangeReportTemperature)
	ON_EN_CHANGE(IDC_REPORT_VOLTAGE, OnChangeReportVoltage)
	ON_NOTIFY(DTN_DATETIMECHANGE, IDC_REPORT_TIME, OnDatetimechangeReportTime)
	ON_EN_CHANGE(IDC_CAP_VTG_LOW, OnChangeCapVtgLow)
	ON_EN_CHANGE(IDC_CAP_VTG_HIGH, OnChangeCapVtgHigh)
	ON_EN_CHANGE(IDC_COMP_I_TIME1, OnChangeCompITime1)
	ON_EN_CHANGE(IDC_COMP_I_TIME2, OnChangeCompITime2)
	ON_EN_CHANGE(IDC_COMP_I_TIME3, OnChangeCompITime3)
	ON_EN_CHANGE(IDC_COMP_I1, OnChangeCompI1)
	ON_EN_CHANGE(IDC_COMP_I2, OnChangeCompI2)
	ON_EN_CHANGE(IDC_COMP_I3, OnChangeCompI3)
	ON_EN_CHANGE(IDC_COMP_I4, OnChangeCompI4)
	ON_EN_CHANGE(IDC_COMP_I5, OnChangeCompI5)
	ON_EN_CHANGE(IDC_COMP_I6, OnChangeCompI6)
	ON_EN_CHANGE(IDC_COMP_V4, OnChangeCompV4)
	ON_EN_CHANGE(IDC_COMP_V5, OnChangeCompV5)
	ON_EN_CHANGE(IDC_COMP_V6, OnChangeCompV6)
	ON_NOTIFY(UDN_DELTAPOS, IDC_RPT_MSEC_SPIN, OnDeltaposRptMsecSpin)
	ON_EN_CHANGE(IDC_CELL_CHECK_TIME_EDIT, OnChangeCellCheckTimeEdit)
	ON_BN_CLICKED(IDC_ALL_STEP_BUTTON, OnAllStepButton)
	ON_BN_CLICKED(IDC_SAME_ALL_STEP_BUTTON, OnSameAllStepButton)
	ON_EN_CHANGE(IDC_TEMP_LOW, OnChangeTempLow)
	ON_EN_CHANGE(IDC_TEMP_HIGH, OnChangeTempHigh)
	ON_BN_CLICKED(IDC_BUTTON_MODEL_SEL, OnButtonModelSel)
	ON_COMMAND(ID_ADMINISTRATION, OnAdministration)
	ON_COMMAND(ID_USER_SETTING, OnUserSetting)
	ON_UPDATE_COMMAND_UI(ID_ADMINISTRATION, OnUpdateAdministration)
	ON_UPDATE_COMMAND_UI(ID_USER_SETTING, OnUpdateUserSetting)
	ON_COMMAND(ID_MODEL_REG, OnModelReg)
	ON_UPDATE_COMMAND_UI(ID_MODEL_REG, OnUpdateModelReg)
	ON_EN_CHANGE(IDC_REPORT_TIME_MILI, OnChangeReportTimeMili)
	ON_EN_KILLFOCUS(IDC_REPORT_TIME_MILI, OnKillFocusReportTimeMili)
	ON_WM_KEYUP()
	ON_WM_COPYDATA()
	ON_WM_CLOSE()	
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CFormView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CFormView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CFormView::OnFilePrintPreview)

	ON_MESSAGE(WM_GRID_CLICK, OnGridClicked)	
	ON_MESSAGE(WM_GRID_MOVECELL, OnMovedCurrentCell)
	ON_MESSAGE(WM_COMBO_SELECT, OnGridComboSelected)
	ON_MESSAGE(WM_COMBO_DROP_DOWN, OnGridComboDropDown)
	ON_MESSAGE(WM_GRID_RIGHT_CLICK, OnRButtonClickedRowCol)
	ON_MESSAGE(WM_GRID_ROW_DRAG_DROP ,OnSelDragRowsDrop)	
	ON_MESSAGE(WM_GRID_BEGINEDIT, OnStartEditing)
	ON_MESSAGE(WM_GRID_ENDEDIT, OnEndEditing)
	ON_MESSAGE(WM_GRID_DOUBLECLICK, OnGridDbClicked)
	ON_MESSAGE(WM_GRID_BTN_CLICKED, OnGridBtnClicked)

	ON_MESSAGE(WM_DATETIME_CHANGED, OnDateTimeChanged)
	

	ON_EN_CHANGE(IDC_VI_GET_TIME, &CCTSEditorView::OnEnChangeViGetTime)
	ON_EN_CHANGE(IDC_EDIT_REG_TEMP, &CCTSEditorView::OnEnChangeEditRegTemp)
	ON_EN_CHANGE(IDC_EDIT_RESISTANCE_RATE, &CCTSEditorView::OnEnChangeEditResistanceRate)
	ON_EN_CHANGE(IDC_CONTACK_RESISTANCE, &CCTSEditorView::OnEnChangeContackResistance)
	ON_EN_CHANGE(IDC_COMP_CHG_CV_DELTA_CRT, &CCTSEditorView::OnEnChangeCompChgCvDeltaCrt)
	ON_EN_CHANGE(IDC_COMP_CHG_CV_TIME, &CCTSEditorView::OnEnChangeCompChgCvTime)
	ON_EN_CHANGE(IDC_COMP_CHG_CV_CRT, &CCTSEditorView::OnEnChangeCompChgCvCrt)
	ON_EN_CHANGE(IDC_COMP_CHG_CC_DELTA_VTG, &CCTSEditorView::OnEnChangeCompChgCcDeltaVtg)
	ON_EN_CHANGE(IDC_COMP_CHG_CC_TIME, &CCTSEditorView::OnEnChangeCompChgCcTime)
	ON_EN_CHANGE(IDC_COMP_CHG_CC_VTG, &CCTSEditorView::OnEnChangeCompChgCcVtg)

	ON_EN_CHANGE(IDC_CONTACK_LIMIT_V, &CCTSEditorView::OnEnChangeContackLimitV)
	ON_EN_CHANGE(IDC_CONTACK_UPPER_V, &CCTSEditorView::OnEnChangeContackUpperV)
	ON_EN_CHANGE(IDC_CONTACK_LOWER_V, &CCTSEditorView::OnEnChangeContackLowerV)
	ON_EN_CHANGE(IDC_CONTACK_UPPER_I, &CCTSEditorView::OnEnChangeContackUpperI)
	ON_EN_CHANGE(IDC_CONTACK_LOWER_I, &CCTSEditorView::OnEnChangeContackLowerI)
	ON_EN_CHANGE(IDC_CONTACT_DELTA_LIMIT_V, &CCTSEditorView::OnEnChangeContactDeltaLimitV)
	END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCTSEditorView construction/destruction

CCTSEditorView::CCTSEditorView()
	: CFormView(CCTSEditorView::IDD)
	{
	//{{AFX_DATA_INIT(CCTSEditorView)
	m_bExtOption = FALSE;	
	m_nCheckTime = 0;
	//}}AFX_DATA_INIT
	// TODO: add construction code here
	m_lDisplayModelID = 0;
	m_lDisplayTestID = 0;
	m_nDisplayStep = 0;
	m_bStepDataSaved = TRUE;

	m_pTestTypeCombo = NULL;
	m_pProcTypeCombo = NULL;
	m_pStepTypeCombo = NULL;
	m_pStepModeCombo = NULL;
	m_bCopyed = FALSE;
	m_bPasteUndo = FALSE;

	m_bVHigh = FALSE;
	m_bVLow = FALSE;
	m_bIHigh = FALSE;
	m_bILow = FALSE;
	m_bCHigh = FALSE;
	m_bCLow = FALSE;
	m_fCLowVal = 0.0f;
	m_fCHighVal = 0.0f;
	m_fILowVal = 0.0f;
	m_fIHighVal = 0.0f;
	m_fVLowVal = 0.0f;
	m_fVHighVal = 0.0f;

	m_lLoadedBatteryModelID = 0;
	m_lLoadedTestID = 0;
	
	m_pTabImageList = NULL;
	
	m_nCurTabIndex = TAB_SAFT_VAL;
	m_nTimer =0;

	m_bShowEndDlgToggle = FALSE;

	m_pModelDlg = NULL;

	m_bUseModelInfo = FALSE;

	LanguageinitMonConfig();

}

CCTSEditorView::~CCTSEditorView()
{

	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}

	if(m_pTabImageList)
	{
		delete	m_pTabImageList;
	}

	if(m_pModelDlg)
	{
		m_pModelDlg->DestroyWindow();
		delete m_pModelDlg;
	}
}

bool CCTSEditorView::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCTSEditorView"), _T("TEXT_CCTSEditorView_CNT"), _T("TEXT_CCTSEditorView_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CCTSEditorView_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCTSEditorView"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Languge error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}


void CCTSEditorView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCTSEditorView)
	DDX_Control(pDX, IDC_DELTA_TIME, m_DeltaTime);
	DDX_Control(pDX, IDC_REPORT_TIME, m_ReportTime);
	DDX_Control(pDX, IDC_PARAM_TAB, m_ctrlParamTab);
	DDX_Control(pDX, IDC_TOT_STEP_COUNT, m_TotalStepCount);
	DDX_Control(pDX, IDC_TEST_LIST_LABEL, m_TestNameLabel);
	DDX_Control(pDX, IDC_TOT_TESTNUM, m_TotTestCount);
	DDX_Control(pDX, IDC_MODEL_TOTNUM, m_TotModelCount);
	DDX_Control(pDX, IDC_PRETEST_CHECK, m_PreTestCheck);
	DDX_Control(pDX, IDC_GRADE_CHECK, m_GradeCheck);
	DDX_Control(pDX, IDC_STEP_NUM, m_strStepNumber);
	DDX_Control(pDX, IDC_TEST_NAME, m_strTestName);
	DDX_Check(pDX, IDC_EXT_OPTION_CHECK, m_bExtOption);	
	DDX_Text(pDX, IDC_CELL_CHECK_TIME_EDIT, m_nCheckTime);
	DDX_Control(pDX, IDC_WARRING_STATIC, m_strWarning);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_LABEL1, m_Label1);
	DDX_Control(pDX, IDC_LABEL_SELECT_INFO, m_LabelSelectInfo);
}

BOOL CCTSEditorView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CCTSEditorView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

	m_strTestName.SetBkColor(RGB(255, 255, 230));
	m_strTestName.SetTextColor(RGB(255,0,0));

	m_TestNameLabel.SetBkColor(RGB(255, 255, 230));
	m_TestNameLabel.SetTextColor(RGB(255, 0, 0));

	m_strStepNumber.SetTextColor(RGB(255, 0 , 0)).SetFontBold(TRUE);
	
	m_btnModelNew.AttachButton(IDC_MODEL_NEW, stingray::foundation::SECBitmapButton::Al_Left, IDB_NEW, this);
	m_btnModelDelete.AttachButton(IDC_MODEL_DELETE, stingray::foundation::SECBitmapButton::Al_Left, IDB_DELETE, this);
	m_btnModelSave.AttachButton(IDC_MODEL_SAVE, stingray::foundation::SECBitmapButton::Al_Left, IDB_SAVE, this);
	m_btnModelCopy.AttachButton(IDC_MODEL_COPY_BUTTON, stingray::foundation::SECBitmapButton::Al_Left, IDB_COPY2, this);
	m_btnModelEdit.AttachButton(IDC_MODEL_EDIT, stingray::foundation::SECBitmapButton::Al_Left, IDB_EDIT, this);

	m_btnProcNew.AttachButton(IDC_TEST_NEW, stingray::foundation::SECBitmapButton::Al_Left, IDB_NEW, this);
	m_btnProcDelete.AttachButton(IDC_TEST_DELETE, stingray::foundation::SECBitmapButton::Al_Left, IDB_DELETE, this);
	m_btnProcSave.AttachButton(IDC_TEST_SAVE, stingray::foundation::SECBitmapButton::Al_Left, IDB_SAVE, this);
	m_btnProcEdit.AttachButton(IDC_TEST_EDIT, stingray::foundation::SECBitmapButton::Al_Left, IDB_EDIT, this);
	m_btnProcCopy.AttachButton(IDC_TEST_COPY_BUTTON, stingray::foundation::SECBitmapButton::Al_Left, IDB_COPY2, this);

	m_btnStepNew.AttachButton(IDC_STEP_INSERT, stingray::foundation::SECBitmapButton::Al_Left, IDB_INSERT, this);
	m_btnStepDelete.AttachButton(IDC_STEP_DELETE, stingray::foundation::SECBitmapButton::Al_Left, IDB_REMOVE, this);
	m_btnStepSave.AttachButton(IDC_STEP_SAVE, stingray::foundation::SECBitmapButton::Al_Left, IDB_SAVE, this);

	m_btnLoad.AttachButton(IDC_LOAD_TEST, stingray::foundation::SECBitmapButton::Al_Left, IDB_OPEN, this);
	m_btnModel.AttachButton(IDC_BUTTON_MODEL_SEL, stingray::foundation::SECBitmapButton::Al_Left, IDB_LOAD, this);	

	m_bVHigh = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Auto_V_High", FALSE);
	m_bVLow = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Auto_V_Low", FALSE);
	m_bIHigh = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Auto_I_High", FALSE);
	m_bILow = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Auto_I_Low", FALSE);
	m_bCHigh = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Auto_C_High", FALSE);
	m_bCLow = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Auto_C_Low", FALSE);
	m_fCLowVal = atof(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION , "Auto_C_Low_Val", "0"));
	m_fCHighVal = atof(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION , "Auto_C_High_Val", "0"));
	m_fILowVal = atof(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION , "Auto_I_Low_Val", "0"));
	m_fIHighVal = atof(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION , "Auto_I_High_Val", "0"));
	m_fVLowVal = atof(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION , "Auto_V_Low_Val", "0"));
	m_fVHighVal = atof(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION , "Auto_V_High_Val", "0"));
	m_bUseTemp = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "UseTemp", FALSE);

	//ljb 0:Formation -> CP Mode 없음	1 : 출력선별기 -> CP Mode 있음
	m_bEditType = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "EditType_CP_ADD", FALSE);

	//모델 목록이 있는지 검사후 없으며 등록하도록 한다.
	CString strSQL;
	CDaoDatabase  db;
	int nCnt = 0;

	strSQL = "SELECT Name FROM ModelInfo ORDER BY Name";
	try
	{
		db.Open(GetDataBaseName());
		CDaoRecordset rs(&db);
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);		

		while(!rs.IsEOF())
		{
			nCnt++;
			rs.MoveNext();
		}
		rs.Close();
		db.Close();

		if(nCnt < 1)
		{
			AfxMessageBox(TEXT_LANG[0]);//"사용할 모델이 등록되어 있지 않습니다. 공정 편집 이전에 사용할 모델을 등록하십시요."
			OnModelReg();
		}
		m_bUseModelInfo = TRUE;
	}
	catch (CDaoException* e)
	{
		//Old Version
		TRACE("Model information tabe not exist\n");
		e->Delete();
	}
#ifdef _DLG_MODEL_
	m_pModelDlg = new CModelSelDlg(this);
	m_pModelDlg->Create(IDD_MODEL_DIALOG, this);
	m_pModelDlg->ShowWindow(SW_SHOW);
#else
	InitBatteryModelGrid(IDC_BATTERY_MODEL, this, &m_wndBatteryModelGrid);
#endif

	InitTestListGrid();
	InitStepGrid();
	InitGradeGrid();
	InitEditCtrl();
	InitParamTab();
	InitCtrl();
	InitFont();
	
	InitToolTips();
	SetControlCellEdlc();

	for(int i =0; i < SCH_MAX_COMP_POINT; i++)
	{
		m_CompTimeV[i].EnableWindow(FALSE);
		m_CompTimeI[i].EnableWindow(FALSE);
	}

	ClearStepState();

	int nCurrentUnitMode = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "Crt Unit Mode", 0);

	CString strFormat;
	strFormat.Format(TEXT_LANG[1], GetDocument()->GetCrtUnit());//"전류 하한(%s)"
	GetDlgItem(IDC_I_STATIC)->SetWindowText(strFormat);
	strFormat.Format(TEXT_LANG[2], GetDocument()->GetCrtUnit());//"전류 상한(%s)"
	GetDlgItem(IDC_I_STATIC2)->SetWindowText(strFormat);	
	
	strFormat.Format(TEXT_LANG[3], GetDocument()->GetCrtUnit());//"전류변화(%s)"
	GetDlgItem(IDC_RPT_I_STATIC)->SetWindowText(strFormat);
	
	strFormat.Format(TEXT_LANG[4], GetDocument()->GetVtgUnit());//"전압 하한(%s)"
	GetDlgItem(IDC_V_STATIC)->SetWindowText(strFormat);
	strFormat.Format(TEXT_LANG[5], GetDocument()->GetVtgUnit());//"전압 상한(%s)"
	GetDlgItem(IDC_V_STATIC2)->SetWindowText(strFormat);
	
	strFormat.Format(TEXT_LANG[6], GetDocument()->GetVtgUnit());//"전압 취득 상한(%s)"
	GetDlgItem(IDC_V_STATIC4)->SetWindowText(strFormat);
	strFormat.Format(TEXT_LANG[7], GetDocument()->GetVtgUnit());//"전압 취득 하한(%s)"
	GetDlgItem(IDC_V_STATIC3)->SetWindowText(strFormat);
	
	strFormat.Format(TEXT_LANG[8], GetDocument()->GetVtgUnit());//"전압변화(%s)"
	GetDlgItem(IDC_RPT_V_STATIC)->SetWindowText(strFormat);
	
	strFormat.Format(TEXT_LANG[9], GetDocument()->GetCapUnit());//"용량 하한(%s)"
	GetDlgItem(IDC_CAP_STATIC)->SetWindowText(strFormat);
	strFormat.Format(TEXT_LANG[10], GetDocument()->GetCapUnit());//"용량 상한(%s)"
	GetDlgItem(IDC_CAP_STATIC2)->SetWindowText(strFormat);
	
	GetDlgItemText(IDC_STATIC_CONTACT_LIMIT_V, strFormat);
	strFormat.Format(strFormat+"(%s)", GetDocument()->GetVtgUnit());//
	GetDlgItem(IDC_STATIC_CONTACT_LIMIT_V)->SetWindowText(strFormat);

	GetDlgItemText(IDC_CONTACT_V_STATIC2, strFormat);
	strFormat.Format(strFormat+"(%s)", GetDocument()->GetVtgUnit());//
	GetDlgItem(IDC_CONTACT_V_STATIC2)->SetWindowText(strFormat);

	GetDlgItemText(IDC_CONTACT_V_STATIC, strFormat);
	strFormat.Format(strFormat+"(%s)", GetDocument()->GetVtgUnit());//
	GetDlgItem(IDC_CONTACT_V_STATIC)->SetWindowText(strFormat);

	GetDlgItemText(IDC_CONTACT_V_STATIC3, strFormat);
	strFormat.Format(strFormat+"(%s)", GetDocument()->GetCrtUnit());//
	GetDlgItem(IDC_CONTACT_V_STATIC3)->SetWindowText(strFormat);

	GetDlgItemText(IDC_CONTACT_V_STATIC4, strFormat);
	strFormat.Format(strFormat+"(%s)", GetDocument()->GetCrtUnit());//
	GetDlgItem(IDC_CONTACT_V_STATIC4)->SetWindowText(strFormat);

	GetDlgItemText(IDC_CONTACT_I_STATIC3, strFormat);
	strFormat.Format(strFormat+"(%s)", GetDocument()->GetVtgUnit());//
	GetDlgItem(IDC_CONTACT_I_STATIC3)->SetWindowText(strFormat);
	
	GetDlgItem(IDC_CHECK_MAXV_UNIT_STATIC)->SetWindowText(GetDocument()->GetVtgUnit());	
	GetDlgItem(IDC_CHECK_MINV_UNIT_STATIC)->SetWindowText(GetDocument()->GetVtgUnit());	

	GetDlgItem(IDC_CHECK_CRT_UNIT_STATIC)->SetWindowText(GetDocument()->GetCrtUnit());	
	GetDlgItem(IDC_CHECK_MAX_CRT_UNIT_STATIC)->SetWindowText(GetDocument()->GetCrtUnit());

	GetDlgItem(IDC_CHECK_MAX_C_UNIT_STATIC)->SetWindowText(GetDocument()->GetCapUnit());	
	GetDlgItem(IDC_REPORT_TIME_MILI)->SendMessage(EM_LIMITTEXT, 3, 0);
//	GetDlgItem(IDC_CHECK_OCV_MINV_UNIT_STATIC)->SetWindowText(GetDocument()->GetVtgUnit());	//★★ ljb 2009119  ★★★★★★★★

	strFormat.Format(TEXT_LANG[132], GetDocument()->GetVtgUnit());
	GetDlgItem(IDC_STATIC_COMP_CHG_CC_VTG)->SetWindowText(strFormat);
	strFormat.Format(TEXT_LANG[133]);
	GetDlgItem(IDC_STATIC_COMP_CHG_CC_TIME)->SetWindowText(strFormat);
	strFormat.Format(TEXT_LANG[134], GetDocument()->GetVtgUnit());
	GetDlgItem(IDC_STATIC_COMP_CHG_CC_DELTA_VTG)->SetWindowText(strFormat);	

	strFormat.Format(TEXT_LANG[135], GetDocument()->GetCrtUnit());
	GetDlgItem(IDC_STATIC_COMP_CHG_CV_CRT)->SetWindowText(strFormat);
	strFormat.Format(TEXT_LANG[136]);
	GetDlgItem(IDC_STATIC_COMP_CHG_CV_TIME)->SetWindowText(strFormat);
	strFormat.Format(TEXT_LANG[137], GetDocument()->GetCrtUnit());
	GetDlgItem(IDC_STATIC_COMP_CHG_CV_DELTA_CRT)->SetWindowText(strFormat);

// 	strFormat.Format("Low voltage(%s)", GetDocument()->GetVtgUnit());
// 	GetDlgItem(IDC_CONTACT_V_STATIC)->SetWindowText(strFormat);
// 	strFormat.Format("Upper voltage(%s)", GetDocument()->GetVtgUnit());
// 	GetDlgItem(IDC_CONTACT_V_STATIC2)->SetWindowText(strFormat);	

}

void CCTSEditorView::InitToolTips()
{
	m_tooltip.Create(this);
	m_tooltip.Activate(TRUE);

	m_tooltip.AddTool(GetDlgItem(IDC_PRETEST_CHECK), TEXT_LANG[12]);//"전지 접촉 검사"
	
	m_tooltip.AddTool(GetDlgItem(IDC_CELL_CHECK_TIME_EDIT), TEXT_LANG[13]);//"검사 전류 인가 시간"
	// m_tooltip.AddTool(GetDlgItem(IDC_PRETEST_FAULT_NO), "불량 채널수의 최대값");

	m_tooltip.AddTool(GetDlgItem(IDC_GRADE_CHECK), TEXT_LANG[15]);//"Step에 대한 Grade 여부 선택"
	m_tooltip.AddTool(GetDlgItem(IDC_VTG_HIGH), TEXT_LANG[16]);//"Step 진행중 설정값 이상으로 전압이 상승하면 불량 처리함"
	m_tooltip.AddTool(GetDlgItem(IDC_VTG_LOW), TEXT_LANG[17]);//"Step 진행중 설정값 이하로 전압이 하강하면 불량 처리함"
	m_tooltip.AddTool(GetDlgItem(IDC_CRT_HIGH), TEXT_LANG[18]);//"Step 진행중 설정값 이상 전류가 흐르면 불량 처리함"
	m_tooltip.AddTool(GetDlgItem(IDC_CRT_LOW), TEXT_LANG[19]);//"Step 진행중 설정값 이하 전류가 흐르면 불량 처리함"
	m_tooltip.AddTool(GetDlgItem(IDC_CAP_HIGH), TEXT_LANG[20]);//"Step 진행중 설정값 이상의 용량이 산출되면 불량 처리함"
	m_tooltip.AddTool(GetDlgItem(IDC_CAP_LOW), TEXT_LANG[21]);//"Step 완료 후 설정값 이하의 용량이 산출되면 불량 처리함"
	m_tooltip.AddTool(GetDlgItem(IDC_IMP_HIGH), TEXT_LANG[22]);//"Impedance 측정값이 설정값 이상이면 불량 처리함"
	m_tooltip.AddTool(GetDlgItem(IDC_IMP_LOW), TEXT_LANG[23]);//"Impedance 측정값이 설정값 이하이면 불량 처리함"
	m_tooltip.AddTool(GetDlgItem(IDC_TEMP_HIGH), TEXT_LANG[24]);//"온도 측정값이 설정값 이상이면 불량 처리함"
	m_tooltip.AddTool(GetDlgItem(IDC_TEMP_LOW), TEXT_LANG[25]);//"온도 측정값이 설정값 이하이면 불량 처리함"
	m_tooltip.AddTool(GetDlgItem(IDC_DELTA_V), TEXT_LANG[26]);//"전압 변화량이 검사 설정 시간내에 설정값 이상 변하면 불량 처리"
	m_tooltip.AddTool(GetDlgItem(IDC_DELTA_I), TEXT_LANG[27]);//"전류 변화량이 검사 설정 시간내에 설정값 이상 변하면 불량 처리"
	m_tooltip.AddTool(GetDlgItem(IDC_DELTA_TIME), TEXT_LANG[28]);//"전압 전류 변화 검사 간격"

	m_tooltip.AddTool(GetDlgItem(IDC_COMP_V1), TEXT_LANG[29]);//"CC 충방전에서 확인시간1에서 전압 하한값1"
	m_tooltip.AddTool(GetDlgItem(IDC_COMP_V4), TEXT_LANG[30]);//"CC 충방전에서 확인시간1에서 전압 상한값1"
	m_tooltip.AddTool(GetDlgItem(IDC_COMP_TIME1), TEXT_LANG[31]);//"전압도달확인시간1"
	// m_tooltip.AddTool(GetDlgItem(IDC_COMP_V2), "CC 충방전에서 확인시간2에서 전압 하한값2");
	// m_tooltip.AddTool(GetDlgItem(IDC_COMP_V5), "CC 충방전에서 확인시간2에서 전압 상한값2");
	// m_tooltip.AddTool(GetDlgItem(IDC_COMP_TIME2), "전압도달확인시간2");
	m_tooltip.AddTool(GetDlgItem(IDC_COMP_V3), TEXT_LANG[32]);//"CC 충방전에서 확인시간3에서 전압 하한값3"
	m_tooltip.AddTool(GetDlgItem(IDC_COMP_V6), TEXT_LANG[33]);//"CC 충방전에서 확인시간3에서 전압 상한값3"
	m_tooltip.AddTool(GetDlgItem(IDC_COMP_TIME3), TEXT_LANG[34]);//"전압도달확인시간3"

	m_tooltip.AddTool(GetDlgItem(IDC_COMP_I1), TEXT_LANG[35]);//"도달 전류 하한값1"
	m_tooltip.AddTool(GetDlgItem(IDC_COMP_I4), TEXT_LANG[36]);//"도달 전류 상한값1"
	m_tooltip.AddTool(GetDlgItem(IDC_COMP_I_TIME1), TEXT_LANG[37]);//"전류도달확인시간1"
	m_tooltip.AddTool(GetDlgItem(IDC_COMP_I2), TEXT_LANG[38]);//"도달 전류 하한값2"
	m_tooltip.AddTool(GetDlgItem(IDC_COMP_I5), TEXT_LANG[39]);//"도달 전류 상한값2"
	m_tooltip.AddTool(GetDlgItem(IDC_COMP_I_TIME2), TEXT_LANG[40]);//"전류도달확인시간2"
	m_tooltip.AddTool(GetDlgItem(IDC_COMP_I3), TEXT_LANG[41]);//"도달 전류 하한값3"
	m_tooltip.AddTool(GetDlgItem(IDC_COMP_I6), TEXT_LANG[42]);//"도달 전류 상한값3"
	m_tooltip.AddTool(GetDlgItem(IDC_COMP_I_TIME3), TEXT_LANG[43]);//"전류도달확인시간3"
	m_tooltip.AddTool(GetDlgItem(IDC_BUTTON_MODEL_SEL), TEXT_LANG[44]);		//"모델 목록을 표시합니다."
}


/////////////////////////////////////////////////////////////////////////////
// CCTSEditorView printing

BOOL CCTSEditorView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CCTSEditorView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CCTSEditorView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

void CCTSEditorView::OnPrint(CDC* pDC, CPrintInfo* /*pInfo*/)
{
	// TODO: add customized printing code here
}

/////////////////////////////////////////////////////////////////////////////
// CCTSEditorView diagnostics

#ifdef _DEBUG
void CCTSEditorView::AssertValid() const
{
	CFormView::AssertValid();
}

void CCTSEditorView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CCTSEditorDoc* CCTSEditorView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCTSEditorDoc)));
	return (CCTSEditorDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCTSEditorView message handlers

LRESULT CCTSEditorView::OnGridClicked(WPARAM wParam, LPARAM lParam)
{

//	TRACE("Mouse Clicked\n");
	
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;				//Get Grid	
	
	ROWCOL nRow, nCol;			
	nCol = LOWORD(wParam);		
	nRow = HIWORD(wParam);	//Get Current Row Col

	if(nRow < 1)	return 0;
	
	CString strTemp;		

	//Disable cell clicked (not occur movecell event)
	if(pGrid != (CMyGridWnd *)&m_wndStepGrid)	return 0;	//Step List Grid

	CGXStyle style;
	
	pGrid->GetStyleRowCol(nRow, nCol , style, gxCopy, 0);
	
/*	//Grid가 disable 상태이면 GirdMoveCellEven가 발생하지 않으므로 Click에서 처리
	if(!UpdateStepGrid(m_nDisplayStep))					//Update Previous Step Data
	{
		AfxMessageBox("Step Data Save Error");
	}
	
	if(m_nDisplayStep != nRow)
	{
		if(!DisplayStepGrid(nRow))							//Display Current Step Data
		{
			AfxMessageBox("Step Data Display Error");
		}
	}

	if(nCol != COL_STEP_END || !style.GetEnabled())		//if End Type Column is Disabled
	{
		HideEndDlg();									//Hide End Type Dlg
	}
	else												//End Type Column is Enabled	
	{
		ShowEndTypeDlg(nRow);							//Show End Type Dlg
	}
*/
	//Disable 상태이면 Event가 발생하지 않으므로 OnMovedCurrentCell()을 발생 시킴 
	if(style.GetIncludeEnabled())
	{
		if(!style.GetEnabled())
		{
			pGrid->SetCurrentCell(nRow, COL_STEP_PROCTYPE);
		}	
	}

	return 1;
}

LRESULT CCTSEditorView::OnGridDbClicked(WPARAM wParam, LPARAM lParam)
{
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;				//Get Grid	
	
	ROWCOL nRow, nCol;			
	nCol = LOWORD(wParam);		
	nRow = HIWORD(wParam);	//Get Current Row Col

	int nCount = m_wndStepGrid.GetRowCount();
	if(nCount > 0 && nRow == 0  )
	{
		if(nCol == COL_STEP_REF_I)
		{
			CCommonInputDlg dlg;
			dlg.m_strTitleString = TEXT_LANG[45];//"적용할 전류 설정값을 입력하십시요"
			if(dlg.DoModal() == IDOK)
			{
				STEP *pStep;
				for(int step = 0; step < GetDocument()->GetTotalStepNum(); step++)
				{
					pStep = GetDocument()->GetStepData(step+1);
					if(pStep == NULL)	break;
			
					if(dlg.m_nMode == 1)	//All Step
					{
						pStep->fIref = dlg.m_fValue;
						m_wndStepGrid.SelectRange(CGXRange(step+1, COL_STEP_REF_I));
					}
					else if(dlg.m_nMode == 2)	//Charge step
					{
						if(pStep->chType == PS_STEP_CHARGE)
						{
							pStep->fIref = dlg.m_fValue;
							m_wndStepGrid.SelectRange(CGXRange(step+1, COL_STEP_REF_I));
						}
					}
					else if(dlg.m_nMode == 3)	//DisCharge step
					{
						if(pStep->chType == PS_STEP_DISCHARGE)
						{
							pStep->fIref = dlg.m_fValue;
							m_wndStepGrid.SelectRange(CGXRange(step+1, COL_STEP_REF_I));
						}
					}
				}
				//Default 현재 선택된 Step으로 
				DisplayStepGrid();
			}
		}
		else if(nCol == COL_STEP_END)
		{
//			CEndConditionDlg dlg;
//			dlg.DoModal();
		}
		return 0;
	}

	if(nRow < 1)	return 0L;

	if(pGrid == (CMyGridWnd *)&m_wndTestListGrid)			//Test List Grid
	{
		OnLoadTest();
//		OnTestEdit() ;
	}
	else if(pGrid == m_pWndCurModelGrid)
	{
//		OnModelEdit() ;
	}
	return 0;
}

LRESULT CCTSEditorView::OnMovedCurrentCell(WPARAM wParam, LPARAM lParam)
{
	CString strTemp;
	ROWCOL nRow, nCol;			
	
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;				//Get Grid	
	nCol = LOWORD(wParam);		
	nRow = HIWORD(wParam);	//Get Current Row Col
	
	if( pGrid == NULL )
	{	
		m_lDisplayModelID = nCol;
		m_lDisplayTestID = nRow;
		
		strTemp.Format("Type %d  Process %d", m_lDisplayModelID, m_lDisplayTestID);
		m_LabelSelectInfo.SetText(strTemp);
		
		Fun_LoadProcessSetting( m_lDisplayTestID, m_lDisplayModelID );	
		
		return 1;	
	}
	else
	{
		if(nRow < 0)	
		{
			return 0;				//Row or Column Number Error
		}	
	}	
	
	if(pGrid == (CMyGridWnd *)&m_wndGradeGrid)			//Test List Grid
	{
		if(m_nDisplayStep < 0 || m_nDisplayStep >= GetDocument()->GetTotalStepNum())	return 0L;
		STEP *pStep;
		pStep = (STEP *)GetDocument()->GetStepData(m_nDisplayStep);
		
		if(!pStep)	return 0;
		
		switch((int)pStep->chType)
		{
		case PS_STEP_CHARGE:
		case PS_STEP_DISCHARGE:
//			AfxMessageBox("charge step");
// 			sprintf(str, TEXT_LANG[], TEXT_LANG[]);//"%s = 항목 단위"//"단위"
// 			GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(str);
			if (nCol == _COL_ITEM1_ || nCol == _COL_GRADE_MIN1_ || nCol == _COL_GRADE_MAX1_)
			{
				switch (atoi(m_wndGradeGrid.GetValueRowCol(nRow, _COL_ITEM1_)))
				{
				case PS_GRADE_VOLTAGE:
					strTemp.Format(TEXT_LANG[48],GetDocument()->GetVtgUnit());//"전압 (%s)"
					GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(strTemp);
					break;
				case PS_GRADE_CAPACITY:
					strTemp.Format(TEXT_LANG[49],GetDocument()->GetCapUnit());//"용량 (%s)"
					GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(strTemp);
					break;
				case PS_GRADE_CURRENT:
					strTemp.Format(TEXT_LANG[50],GetDocument()->GetCrtUnit());//"전류 (%s)"
					GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(strTemp);
					break;
				case PS_GRADE_TIME:
					strTemp = TEXT_LANG[51];//"시간 (sec)"
					GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(strTemp);
					break;
				default :
					strTemp = TEXT_LANG[52];//"단위 표시"
					GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(strTemp);
					break;
				}
			}
			else if (nCol == _COL_ITEM2_ || nCol == _COL_GRADE_MIN2_ || nCol == _COL_GRADE_MAX2_)
			{
				switch (atoi(m_wndGradeGrid.GetValueRowCol(nRow, _COL_ITEM2_)))
				{
				case PS_GRADE_VOLTAGE:
					strTemp.Format(TEXT_LANG[48],GetDocument()->GetVtgUnit());//"전압 (%s)"
					GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(strTemp);
					break;
				case PS_GRADE_CAPACITY:
					strTemp.Format(TEXT_LANG[49],GetDocument()->GetCapUnit());//"용량 (%s)"
					GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(strTemp);
					break;
				case PS_GRADE_CURRENT:
					strTemp.Format(TEXT_LANG[50],GetDocument()->GetCrtUnit());//"전류 (%s)"
					GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(strTemp);
					break;
				case PS_GRADE_TIME:
					strTemp = TEXT_LANG[51];//"시간 (sec)"
					GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(strTemp);
					break;
				default :
					strTemp = TEXT_LANG[52];//"단위 표시"
					GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(strTemp);
					break;
				}

			}
			break;
		case PS_STEP_IMPEDANCE:
			if (nCol == _COL_ITEM1_)
			{
				m_wndGradeGrid.SetStyleRange(CGXRange(nRow,_COL_ITEM1_),
					CGXStyle().SetValue((double)PS_GRADE_IMPEDANCE));
				m_wndGradeGrid.SetStyleRange(CGXRange().SetCells(nRow, nCol,1,1),
					CGXStyle().SetEnabled(FALSE));
			}
			break;
		case PS_STEP_OCV:
			if (nCol == _COL_ITEM1_)
			{
				m_wndGradeGrid.SetStyleRange(CGXRange(nRow,_COL_ITEM1_),
					CGXStyle().SetValue((double)PS_GRADE_VOLTAGE));
				m_wndGradeGrid.SetStyleRange(CGXRange().SetCells(nRow, nCol,1,1),
					CGXStyle().SetEnabled(FALSE));
			}
			break;
		}
		return 0;
	}
	
	if(pGrid == (CMyGridWnd *)&m_wndTestListGrid)			//Test List Grid
	{
		strTemp = m_wndTestListGrid.GetValueRowCol(nRow, COL_TEST_PRIMARY_KEY);	//Test ID
		if(!strTemp.IsEmpty())
		{
			GetDlgItem(IDC_LOAD_TEST)->EnableWindow(TRUE);
			UpdateDspTest(pGrid->GetValueRowCol(nRow, COL_TEST_NAME), atoi((LPCSTR)(LPCTSTR)strTemp));
		}
		else
		{
			GetDlgItem(IDC_LOAD_TEST)->EnableWindow(FALSE);
		}
	}

	if(pGrid == m_pWndCurModelGrid)		//Battery Model Grid
	{
		strTemp = m_pWndCurModelGrid->GetValueRowCol(nRow, COL_MODEL_PRIMARY_KEY);	//Battery Model ID
		if(strTemp.IsEmpty())
		{
			int nTotTestNum = m_wndTestListGrid.GetRowCount();
			if(nTotTestNum >0)	m_wndTestListGrid.RemoveRows( 1, nTotTestNum);
			
			GetDlgItem(IDC_TEST_NEW)->EnableWindow(FALSE);
			GetDlgItem(IDC_TEST_DELETE)->EnableWindow(FALSE);
			GetDlgItem(IDC_TEST_SAVE)->EnableWindow(FALSE);
			m_TestNameLabel.SetText(TEXT_LANG[53]);//"현재 선택된 Model은 저장되지 않은 Model입니다. 먼저 Model을 저장 하십시요."
		}
		else
		{
			UpdateDspModel(pGrid->GetValueRowCol(nRow, 1), atoi((LPCSTR)(LPCTSTR)strTemp));
			RequeryTestList(m_lDisplayModelID);

			GetDlgItem(IDC_TEST_NEW)->EnableWindow(TRUE);
			GetDlgItem(IDC_TEST_DELETE)->EnableWindow(TRUE);
			GetDlgItem(IDC_TEST_SAVE)->EnableWindow(FALSE);
		
//			strTemp.Format("%s[ID:%ld] 시험 List", m_strDspModelName, m_lDisplayModelID);
//			strTemp.Format("%s", m_strDspModelName);
//			m_TestNameLabel.SetText(strTemp);
		}
	}

//	strTemp.Format("  Model: %s[ID %ld], 공정명: %s [ID %ld]", m_strDspModelName, m_lDisplayModelID, m_strDspTestName, m_lDisplayTestID);
	if(m_strDspTestName.IsEmpty())
	{
		strTemp = TEXT_LANG[54];//"스케쥴 목록이 선택되지 않았습니다."
//		m_strTestName.SetTextColor(RGB(255, 100, 100));
	}
	else
	{
		strTemp.Format("%s", m_strDspTestName);
//		m_strTestName.SetTextColor(RGB(0, 0, 0));
	}
	m_strTestName.SetText(strTemp);
	
	if(pGrid != (CMyGridWnd *)&m_wndStepGrid)	return 0;	//Step List Grid

	int nTotStep = pGrid->GetRowCount();
	if(nRow > nTotStep )			return 0;				//Step is already Deleted
	
	if(m_nDisplayStep > nTotStep)	m_nDisplayStep = nTotStep;	//S

	if(!UpdateStepGrid(m_nDisplayStep))					//Save Left Step Data
	{
		AfxMessageBox("Update step data error");
	}
		
	if(m_nDisplayStep != nRow)
	{
		m_bShowEndDlgToggle = FALSE;
		if(!DisplayStepGrid(nRow))							//Display Current Step Data
		{
			AfxMessageBox("Step display error");
		}
	}

	return 1;
}

void CCTSEditorView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
//	DeleteEndDlg();
	CFormView::OnLButtonDown(nFlags, point);
}

//Resize Window
void CCTSEditorView::OnSize(UINT nType, int cx, int cy) 
{
	CFormView::OnSize(nType, cx, cy);
//	DeleteEndDlg();
	
	
/*	CRect rect;
	::GetClientRect(m_hWnd, &rect);
	if(rect.bottom<600 && rect.right<800)	return;

	CFormView::ShowScrollBar(SB_VERT,FALSE);
	CFormView::ShowScrollBar(SB_HORZ,FALSE);

	if( this->GetSafeHwnd())
	{
		::MoveWindow(m_wndBatteryModelGrid,	10, 55, rect.right*0.42, rect.bottom*0.27, FALSE);
		::MoveWindow(m_wndTestListGrid	  ,	rect.right*0.42+10+20, 55, rect.right*(1-0.42)-(10+20+10), rect.bottom*0.27, FALSE);

		if(GetDlgItem(IDC_STEP_RANGE)->GetSafeHwnd())
			GetDlgItem(IDC_STEP_RANGE)->MoveWindow( 10, rect.bottom*0.27+55+35, rect.right-20, rect.bottom*(1-0.27)-(55+40+10), FALSE);
		::MoveWindow(m_wndStepGrid, 20, rect.bottom*0.27+55+110, rect.right*0.52, rect.bottom*(1-0.27)-(55+110+20), FALSE);
		if(GetDlgItem(IDC_LOADED_TEST)->GetSafeHwnd())
			GetDlgItem(IDC_LOADED_TEST)->SetWindowPos(NULL, 20, rect.bottom*0.27+55+55, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
		if(GetDlgItem(IDC_STEP_INSERT)->GetSafeHwnd())
			GetDlgItem(IDC_STEP_INSERT)->SetWindowPos(NULL, 20, rect.bottom*0.27+55+85, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
		if(GetDlgItem(IDC_STEP_DELETE)->GetSafeHwnd())
			GetDlgItem(IDC_STEP_DELETE)->SetWindowPos(NULL, 110, rect.bottom*0.27+55+85, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
		if(GetDlgItem(IDC_STEP_SAVE)->GetSafeHwnd())
			GetDlgItem(IDC_STEP_SAVE)->SetWindowPos(NULL, 200, rect.bottom*0.27+55+85, 0, 0, SWP_NOSIZE | SWP_NOZORDER);

		if(GetDlgItem(IDC_SELECT_TEST_LABEL)->GetSafeHwnd())
			GetDlgItem(IDC_SELECT_TEST_LABEL)->SetWindowPos(NULL, 10, rect.bottom*0.27+55+10, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
		if(GetDlgItem(IDC_TEST_NAME)->GetSafeHwnd())
			GetDlgItem(IDC_TEST_NAME)->SetWindowPos(NULL, 120, rect.bottom*0.27+55+10, 0, 0, SWP_NOSIZE | SWP_NOZORDER);

		if(GetDlgItem(IDC_TEST_NEW)->GetSafeHwnd())
			GetDlgItem(IDC_TEST_NEW)->SetWindowPos(NULL, rect.right*0.42+10+20+10, 30, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
		if(GetDlgItem(IDC_TEST_DELETE)->GetSafeHwnd())
			GetDlgItem(IDC_TEST_DELETE)->SetWindowPos(NULL, rect.right*0.42+10+20+110, 30, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
		if(GetDlgItem(IDC_TEST_SAVE)->GetSafeHwnd())
			GetDlgItem(IDC_TEST_SAVE)->SetWindowPos(NULL, rect.right*0.42+10+20+210, 30, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
		if(GetDlgItem(IDC_TEST_LIST_LABEL)->GetSafeHwnd())
			GetDlgItem(IDC_TEST_LIST_LABEL)->SetWindowPos(NULL, rect.right*0.42+10+20+10, 10, 0, 0, SWP_NOSIZE | SWP_NOZORDER);

		if(GetDlgItem(IDC_MODEL_TOT_LABEL)->GetSafeHwnd())
			GetDlgItem(IDC_MODEL_TOT_LABEL)->SetWindowPos(NULL, rect.right*0.42-90, 30, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
		if(GetDlgItem(IDC_MODEL_TOTNUM)->GetSafeHwnd())
			GetDlgItem(IDC_MODEL_TOTNUM)->SetWindowPos(NULL, rect.right*0.42-50, 30, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
		if(GetDlgItem(IDC_TEST_TOT_LABEL)->GetSafeHwnd())
			GetDlgItem(IDC_TEST_TOT_LABEL)->SetWindowPos(NULL,  rect.right-10-90, 30, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
		if(GetDlgItem(IDC_TOT_TESTNUM)->GetSafeHwnd())
			GetDlgItem(IDC_TOT_TESTNUM)->SetWindowPos(NULL,  rect.right-10-50, 30, 0, 0, SWP_NOSIZE | SWP_NOZORDER);

		if(GetDlgItem(IDC_LOAD_TEST)->GetSafeHwnd())
			GetDlgItem(IDC_LOAD_TEST)->SetWindowPos(NULL, rect.right*0.52-150, rect.bottom*0.27+55+50, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
		if(GetDlgItem(IDC_STEP_TOT_LABEL)->GetSafeHwnd())
			GetDlgItem(IDC_STEP_TOT_LABEL)->SetWindowPos(NULL, rect.right*0.52-130, rect.bottom*0.27+55+85, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
		if(GetDlgItem(IDC_TOT_STEP_COUNT)->GetSafeHwnd())
			GetDlgItem(IDC_TOT_STEP_COUNT)->SetWindowPos(NULL, rect.right*0.52-50, rect.bottom*0.27+55+85, 0, 0, SWP_NOSIZE | SWP_NOZORDER);

		
		//		m_wndGradeGrid.MoveWindow( 5, rect.bottom/100*42, rect.right-10, rect.bottom-rect.bottom/100*42-5, FALSE);
	}
	SetModelGridColumnWidth();
	SetTestGridColumnWidth();
*/	// TODO: Add your message handler code here\	
}

//Init Test Grid
/*
BOOL CCTSEditorView::InitTestListGrid()
{
	m_wndTestListGrid.SubclassDlgItem(IDC_TESTLIST_GRID, this);
	m_wndTestListGrid.Initialize();
	m_wndTestListGrid.GetParam()->EnableUndo(FALSE);
	
	m_wndTestListGrid.SetColCount(8);
	SetTestGridColumnWidth();

	m_wndTestListGrid.SetStyleRange(CGXRange(0, COL_TEST_NO),	CGXStyle().SetValue("No"));
	m_wndTestListGrid.SetStyleRange(CGXRange(0, COL_TEST_PROC_TYPE),	CGXStyle().SetValue("Type"));
	m_wndTestListGrid.SetStyleRange(CGXRange(0, COL_TEST_NAME),	CGXStyle().SetValue("이름"));
	m_wndTestListGrid.SetStyleRange(CGXRange(0, COL_TEST_CREATOR),	CGXStyle().SetValue("작성자"));
	m_wndTestListGrid.SetStyleRange(CGXRange(0, COL_TEST_DESCRIPTION),	CGXStyle().SetValue("설명"));
	m_wndTestListGrid.SetStyleRange(CGXRange(0, COL_TEST_EDIT_TIME),	CGXStyle().SetValue("수정일"));
	m_wndTestListGrid.SetStyleRange(CGXRange(0, COL_TEST_RESET_PROC),	CGXStyle().SetValue("연계"));
	m_wndTestListGrid.SetStyleRange(CGXRange(0, COL_TEST_PRIMARY_KEY),	CGXStyle().SetValue("Primary Key"));
	m_wndTestListGrid.SetStyleRange(CGXRange(0, COL_TEST_EDIT_STATE),	CGXStyle().SetValue("Edit_State"));
	m_wndTestListGrid.SetStyleRange(CGXRange().SetCols(COL_TEST_PRIMARY_KEY),	CGXStyle().SetEnabled(FALSE).SetControl(GX_IDS_CTRL_STATIC));
	m_wndTestListGrid.SetStyleRange(CGXRange().SetCols(COL_TEST_EDIT_STATE),	CGXStyle().SetEnabled(FALSE).SetControl(GX_IDS_CTRL_STATIC));

	m_wndTestListGrid.SetStyleRange(CGXRange().SetTable(), CGXStyle().SetVertScrollBar(TRUE).SetWrapText(FALSE));

	m_pTestTypeCombo = new CGridComboBox(this, 
								&m_wndTestListGrid,
								GX_IDS_CTRL_ZEROBASED_EX5,
								GX_IDS_CTRL_ZEROBASED_EX5,
								GXCOMBO_ZEROBASED|GXCOMBO_DISPLAYCHOICE|GXCOMBO_TEXTFIT
								);
	m_wndTestListGrid.RegisterControl(GX_IDS_CTRL_ZEROBASED_EX5, m_pTestTypeCombo);

	CString strType, strMsg;
	CString strDBName = GetDataBaseName();
	int nCount = 0;
	if(!strDBName.IsEmpty())
	{
		CDaoDatabase  db;
		COleVariant data;

		CString strSQL("SELECT TestType, TestTypeName FROM TestType ORDER BY TestType");			
		try
		{
			db.Open(strDBName);
			CDaoRecordset rs(&db);
			rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
			if(!rs.IsBOF()&&!rs.IsEOF())
			{
				strType.Empty();
				while(!rs.IsEOF())
				{
					data = rs.GetFieldValue(0);
					m_pTestTypeCombo->SetItemData(nCount++, data.lVal);
					
					data = rs.GetFieldValue(1);
					strMsg = data.pbVal;
					strType += strMsg+CString("\n");
					rs.MoveNext();
				}
			}
			rs.Close();
			db.Close();
		}
		catch (CDaoException* e)
		{
			e->Delete();
		}
		
		//2006/9/29 
		//공정에서 실행 가능한 StepList 추가 (CODERIZ)
		//DataBase 호환을 위해 별도로 검색 한다.
		strSQL="SELECT ExeStep FROM TestType ORDER BY TestType";
		try
		{
			db.Open(strDBName);
			CDaoRecordset rs(&db);
			rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
			if(!rs.IsBOF()&&!rs.IsEOF())
			{
				while(!rs.IsEOF())
				{
					data = rs.GetFieldValue(0);
					m_strTypeMaskArray.Add(CString(data.pcVal));
					rs.MoveNext();
				}
			}
			rs.Close();
			db.Close();
		}
		catch (CDaoException* e)
		{
			TRACE("Type type table의 ExeStep Field가 없습니다.");
			e->Delete();
		}
	}

	//DataBase에서 TestType  정보를 찾을 수 없을 경우는 Defulat 정보 삽입 
	if(nCount == 0)
	{
		strType = " PreCharge\n OCV\n Formation\n OutCharge";
		m_pTestTypeCombo->SetItemData(0, 1);
		m_pTestTypeCombo->SetItemData(1, 2);
		m_pTestTypeCombo->SetItemData(2, 3);
		m_pTestTypeCombo->SetItemData(3, 4);
	}

	m_wndTestListGrid.SetStyleRange(CGXRange().SetCols(COL_TEST_PROC_TYPE),
		CGXStyle()
			.SetControl(GX_IDS_CTRL_ZEROBASED_EX5)
			.SetHorizontalAlignment(DT_LEFT)
			.SetChoiceList(strType)
	);


	m_wndTestListGrid.SetStyleRange(CGXRange().SetCols(COL_TEST_EDIT_TIME),
		CGXStyle()
			.SetControl(GX_IDS_CTRL_DATETIMENOCAL )
			.SetUserAttribute(GX_IDS_UA_DATEFORMATTYPE, GX_DTFORMAT_LONGDATE)
			.SetHorizontalAlignment(DT_LEFT)
	);

	m_wndTestListGrid.SetStyleRange(CGXRange().SetCols(COL_TEST_RESET_PROC),
		CGXStyle()
			.SetControl(GX_IDS_CTRL_ZEROBASED_EX)
			.SetHorizontalAlignment(DT_CENTER)
			.SetChoiceList(" N\n Y")
			.SetEnabled(FALSE)
	);

	m_wndTestListGrid.GetParam()->EnableUndo(TRUE);
	return TRUE;
}*/

BOOL CCTSEditorView::InitTestListGrid()
{	
	CString strType, strMsg, strSQL;
	CString strDBName = GetDataBaseName();
	int nCount = 0;
	if(!strDBName.IsEmpty())
	{
		CDaoDatabase  db;
		COleVariant data;
		
		//2006/9/29 
		//공정에서 실행 가능한 StepList 추가 (CODERIZ)
		//DataBase 호환을 위해 별도로 검색 한다.
		strSQL="SELECT ExeStep FROM TestType ORDER BY TestType";
		try
		{
			db.Open(strDBName);
			CDaoRecordset rs(&db);
			rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
			if(!rs.IsBOF()&&!rs.IsEOF())
			{
				while(!rs.IsEOF())
				{
					data = rs.GetFieldValue(0);
					m_strTypeMaskArray.Add(CString(data.pcVal));
					rs.MoveNext();
				}
			}
			rs.Close();
			db.Close();
		}
		catch (CDaoException* e)
		{
			TRACE(TEXT_LANG[55]);//"Type type table의 ExeStep Field가 없습니다."
			e->Delete();
		}
	}

	return TRUE;
}

//Init Step Grid
BOOL CCTSEditorView::InitStepGrid()
{
	CString strType;
	CRect rect, cellRect; 
	m_wndStepGrid.SubclassDlgItem(IDC_STEP_GRID, this);
	m_wndStepGrid.Initialize();

	// Sample setup for the grid
	m_wndStepGrid.GetParam()->EnableUndo(FALSE);
	m_wndStepGrid.GetParam()->EnableMoveRows(FALSE);		//Drag and Drop 지원

	m_wndStepGrid.SetColCount(COL_STEP_END_SOC);
	m_wndStepGrid.GetClientRect(&rect);
	
	m_wndStepGrid.SetDefaultRowHeight(25);
	m_wndStepGrid.SetDefaultColWidth(120);
	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(0, COL_STEP_END_SOC), CGXStyle().SetFont(CGXFont().SetSize(11).SetBold(TRUE)));
	
	m_wndStepGrid.SetRowHeight(0,0,20);	
		
	//EDLC나 Cycler용이면 Proc Type을 표시하지 않는다.
	m_wndStepGrid.SetColWidth(COL_STEP_CYCLE, COL_STEP_CYCLE, 0);
	m_wndStepGrid.SetColWidth(COL_STEP_NO, COL_STEP_NO, 35);
	m_wndStepGrid.SetColWidth(COL_STEP_MODE, COL_STEP_MODE, 90);
//#ifdef _AUTO_PROCESS_
	m_wndStepGrid.SetColWidth(COL_STEP_TYPE, COL_STEP_TYPE, 0);
	m_wndStepGrid.SetColWidth(COL_STEP_PROCTYPE, COL_STEP_PROCTYPE, 100);
	m_wndStepGrid.SetColWidth(COL_STEP_REF_V, COL_STEP_REF_I, 100);
	m_wndStepGrid.SetColWidth(COL_STEP_REF_T, COL_STEP_REF_T, 0);
	// [12/24/2009 kky]	
	m_wndStepGrid.SetColWidth(COL_STEP_REF_P, COL_STEP_REF_P, 0);
	m_wndStepGrid.SetColWidth(COL_STEP_END_DELTAVP, COL_STEP_END_DELTAVP, 0);
	m_wndStepGrid.SetColWidth(COL_STEP_END_DAY, COL_STEP_END_DAY, 0);	
	m_wndStepGrid.SetColWidth(COL_STEP_END_TIME, COL_STEP_END_CAP, 100);
	m_wndStepGrid.SetColWidth(COL_STEP_END_TEMP, COL_STEP_END_TEMP, 0);
	m_wndStepGrid.SetColWidth(COL_STEP_END_SOC, COL_STEP_END_SOC, 80);	
	m_wndStepGrid.SetColWidth(COL_STEP_PREIMARY_KEY, COL_STEP_INDEX, 0);	
	
	
	m_wndStepGrid.SetStyleRange(CGXRange(0, COL_STEP_NO),		CGXStyle().SetValue("No"));
	m_wndStepGrid.SetStyleRange(CGXRange(0, COL_STEP_CYCLE),	CGXStyle().SetValue("Cyc"));
	m_wndStepGrid.SetStyleRange(CGXRange(0, COL_STEP_PROCTYPE),	CGXStyle().SetValue("Step"));
	m_wndStepGrid.SetStyleRange(CGXRange(0, COL_STEP_TYPE),		CGXStyle().SetValue("Type"));
	m_wndStepGrid.SetStyleRange(CGXRange(0, COL_STEP_MODE),		CGXStyle().SetValue("Mode"));
	
	strType.Format(TEXT_LANG[56], GetDocument()->GetVtgUnit());//"전압(%s)"
	m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_REF_V),	strType);
	
	strType.Format(TEXT_LANG[57], GetDocument()->GetCrtUnit());//"전류(%s)"
	m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_REF_I),	strType);
	m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_REF_T),	TEXT_LANG[58]);//"온도(℃)"
	
	m_wndStepGrid.SetStyleRange(CGXRange(0, COL_STEP_END),		CGXStyle().SetValue(TEXT_LANG[59]));//"기타 조건(종료)"
	m_wndStepGrid.SetStyleRange(CGXRange(0, COL_STEP_PREIMARY_KEY),	CGXStyle().SetValue("Primary Key"));
	m_wndStepGrid.SetStyleRange(CGXRange(0, COL_STEP_INDEX),	CGXStyle().SetValue("Index"));
	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_PREIMARY_KEY, COL_STEP_INDEX), CGXStyle().SetEnabled(FALSE));
	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_END, COL_STEP_INDEX), CGXStyle().SetControl(GX_IDS_CTRL_STATIC));
	
	m_wndStepGrid.SetColWidth(COL_STEP_END, COL_STEP_INDEX, 0);

	strType.Format(TEXT_LANG[60], GetDocument()->GetWattUnit());//"종료파워(%s)"
	m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_REF_P),	strType);
	strType.Format(TEXT_LANG[61], GetDocument()->GetVtgUnit());//"종료전압(%s)"
	m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_END_V),	strType);
	strType.Format(TEXT_LANG[62], GetDocument()->GetCrtUnit());//"종료전류(%s)"
	m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_END_I),	strType);
	strType.Format("DeltaVp(%s)", GetDocument()->GetVtgUnit());
	m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_END_DELTAVP),	strType);
	
	m_wndStepGrid.SetStyleRange(CGXRange(0, COL_STEP_END_DAY),	CGXStyle().SetValue(TEXT_LANG[63]));//"날짜"
	m_wndStepGrid.SetStyleRange(CGXRange(0, COL_STEP_END_TIME),	CGXStyle().SetValue(TEXT_LANG[64]));//"종료시간"
	
	strType.Format(TEXT_LANG[65], GetDocument()->GetCapUnit());//"용량(%s)"
	m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_END_CAP),	strType);
	m_wndStepGrid.SetStyleRange(CGXRange(0, COL_STEP_END_TEMP),	CGXStyle().SetValue(TEXT_LANG[66]));//"종료온도(℃)"
	m_wndStepGrid.SetStyleRange(CGXRange(0, COL_STEP_END_SOC),	CGXStyle().SetValue("SOC"));

	m_wndStepGrid.SetStyleRange(CGXRange(0,COL_STEP_END_V,0, COL_STEP_END_SOC) , CGXStyle().SetInterior(RGB(128,255,128)));		// 종료조건 색상

	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_CYCLE),
		CGXStyle()
  			.SetHorizontalAlignment(DT_CENTER)
	);
	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_REF_V),
		CGXStyle()
			.SetControl(GX_IDS_CTRL_EDIT)
	);
	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_REF_I),
		CGXStyle()
			.SetControl(GX_IDS_CTRL_EDIT)
	);	

	m_wndStepGrid.EnableGridToolTips();
    m_wndStepGrid.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, 
         CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));
	
	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_REF_V, COL_STEP_END), CGXStyle().SetVertScrollBar(TRUE).SetWrapText(FALSE));

	m_pStepTypeCombo = new CGridComboBox(this, 
								&m_wndStepGrid,
								GX_IDS_CTRL_ZEROBASED_EX1,
								GX_IDS_CTRL_ZEROBASED_EX1,
								GXCOMBO_ZEROBASED|GXCOMBO_DISPLAYCHOICE
								);
	m_wndStepGrid.RegisterControl(GX_IDS_CTRL_ZEROBASED_EX1, m_pStepTypeCombo);

	m_pStepModeCombo = new CGridComboBox(this, 
								&m_wndStepGrid,
								GX_IDS_CTRL_ZEROBASED_EX2,
								GX_IDS_CTRL_ZEROBASED_EX2,
								GXCOMBO_ZEROBASED|GXCOMBO_DISPLAYCHOICE
								);
	m_wndStepGrid.RegisterControl(GX_IDS_CTRL_ZEROBASED_EX2, m_pStepModeCombo);

	m_pProcTypeCombo = new CGridComboBox(this, 
								&m_wndStepGrid,
								GX_IDS_CTRL_ZEROBASED_EX3,
								GX_IDS_CTRL_ZEROBASED_EX3,
								GXCOMBO_ZEROBASED|GXCOMBO_DISPLAYCHOICE
								);
	m_wndStepGrid.RegisterControl(GX_IDS_CTRL_ZEROBASED_EX3, m_pProcTypeCombo);

#ifdef _EDLC_CELL_
			strType = TEXT_LANG[67];//" Charge\n Discharge\n Rest\n Ocv\n ESR\n 완료\n"
#else
			// strType = " Charge\n Discharge\n Rest\n Ocv\n Impedance\n 완료\n";
			strType = TEXT_LANG[68];//" CHG\n DCHG\n Rest\n OCV\n DCIR\n 완료\n"
#endif

	m_pStepTypeCombo->SetItemData(0, PS_STEP_CHARGE);
	m_pStepTypeCombo->SetItemData(1, PS_STEP_DISCHARGE);
	m_pStepTypeCombo->SetItemData(2, PS_STEP_REST);
	m_pStepTypeCombo->SetItemData(3, PS_STEP_OCV);
	m_pStepTypeCombo->SetItemData(4, PS_STEP_IMPEDANCE);
	m_pStepTypeCombo->SetItemData(5, PS_STEP_END);	

	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_TYPE),
		CGXStyle()
			.SetControl(GX_IDS_CTRL_ZEROBASED_EX1)
			.SetHorizontalAlignment(DT_LEFT)
			.SetChoiceList(strType)
	);

	CString strMode(" CC-CV\n CC\n");
	CString strProcType;
	SCH_CODE_MSG *pMsg;

	CCTSEditorDoc *pDoc = GetDocument();
	for(int i = 0; i < GetDocument()->m_apProcType.GetSize(); i++)
	{
		pMsg = (SCH_CODE_MSG *)pDoc->m_apProcType[i];
		strMode.Format("%s\n", pMsg->szMessage);
		strProcType += strMode;
		m_pProcTypeCombo->SetItemData(i, pMsg->nCode);
	}
	
	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_PROCTYPE),
		CGXStyle()
			.SetControl(GX_IDS_CTRL_ZEROBASED_EX3)
			.SetHorizontalAlignment(DT_LEFT)
			.SetChoiceList(strProcType)
	);

	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_MODE),
		CGXStyle()
			.SetControl(GX_IDS_CTRL_ZEROBASED_EX2)
			.SetHorizontalAlignment(DT_LEFT)
//			.SetChoiceList(strMode)
	);

	m_wndStepGrid.RegisterControl(IDS_CTRL_REALEDIT, new CRealEditControl(&m_wndStepGrid, IDS_CTRL_REALEDIT));
	m_wndStepGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITFORMAT);
	m_wndStepGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITSIZE);
	m_wndStepGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITSIZE2);

	char str[12], str2[7], str3[5];
	//reference current
	sprintf(str3, "%d", GetDocument()->GetVtgUnitFloat());		//소숫점 이하
	sprintf(str, "%%.%dlf", GetDocument()->GetVtgUnitFloat());	//소숫점 이하
	if(pDoc->GetVtgUnit() == "V")
	{
		sprintf(str2, "%d", 4);		//정수부
	}
	else
	{
		sprintf(str2, "%d", 6);		//정수부
	}

	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_REF_V),
		CGXStyle()
			.SetHorizontalAlignment(DT_RIGHT)
			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
	);

	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_END_V),
		CGXStyle()
		.SetHorizontalAlignment(DT_RIGHT)
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
	);

	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_END_DELTAVP),
		CGXStyle()
		.SetHorizontalAlignment(DT_RIGHT)
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
	);

	sprintf(str3, "%d", GetDocument()->GetCrtUnitFloat());		//소숫점 이하
	sprintf(str, "%%.%dlf", GetDocument()->GetCrtUnitFloat());
	if(pDoc->GetCrtUnit() == "A")
	{
		sprintf(str2, "%d", 4);
	}
	else
	{
		sprintf(str2, "%d", 6);
	}

	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_REF_I),
		CGXStyle()
			.SetHorizontalAlignment(DT_RIGHT)
			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
	);

	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_END_I),
		CGXStyle()
		.SetHorizontalAlignment(DT_RIGHT)
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
	);

	sprintf(str3, "%d", 1);		//소숫점 이하
	sprintf(str, "%%.%dlf", 1);
	sprintf(str2, "%d", 3);

	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_REF_T),
		CGXStyle()
			.SetHorizontalAlignment(DT_RIGHT)
			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
	);

	sprintf(str, "%%.%dlf", 0);		
	sprintf(str3, "%d", 2);		//소숫점 이하
	sprintf(str2, "%d", 2);		//정수부
	
	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_END_DAY),
		CGXStyle()
		.SetHorizontalAlignment(DT_RIGHT)
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
	);	

	sprintf(str, "   HH:mm:ss" );	

	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_END_TIME),
		CGXStyle()
		.SetHorizontalAlignment(DT_RIGHT)
		.SetControl(GX_IDS_CTRL_DATETIMENOCAL)
		.SetUserAttribute(GX_IDS_UA_CUSTOMFORMAT, str)		
	);

	sprintf(str3, "%d", GetDocument()->GetCapUnitFloat());		//소숫점 이하
	sprintf(str, "%%.%dlf", GetDocument()->GetCapUnitFloat());
	if(pDoc->GetCapUnit() == "Ah")
	{
		sprintf(str2, "%d", 3);
	}
	else
	{
		sprintf(str2, "%d", 6);
	}
	
	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_END_CAP),
		CGXStyle()
		.SetHorizontalAlignment(DT_RIGHT)
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		);

	sprintf(str3, "%d", GetDocument()->GetWattUnitFloat());
	sprintf(str, "%%.%dlf", GetDocument()->GetWattUnitFloat());
	if( pDoc->GetWattUnit() == "W" )
	{
		sprintf(str2, "%d", 3);
	}
	else
	{
		sprintf(str2, "%d", 6);
	}

	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_REF_P),
		CGXStyle()
		.SetHorizontalAlignment(DT_RIGHT)
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		);
		
	sprintf(str, "%%.%dlf", 2);		
	sprintf(str3, "%d", 2);		//소숫점 이하
	sprintf(str2, "%d", 2);		//정수부
		
	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_END_SOC),
		CGXStyle()
		.SetHorizontalAlignment(DT_RIGHT)
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		);

	//현재 선택된 Step을 색상으로 표시한다.
	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(1, m_wndStepGrid.GetColCount()), CGXStyle().SetTextColor(RGB(165,165,165)));

	m_wndStepGrid.GetParam()->EnableUndo(TRUE);

	return TRUE;
}

//Init Grade Grid
BOOL CCTSEditorView::InitGradeGrid()
{
	CRect rect;
	m_wndGradeGrid.SubclassDlgItem(IDC_GRADE_GRID, this);
	m_wndGradeGrid.Initialize();

	// Sample setup for the grid
	m_wndGradeGrid.GetParam()->EnableUndo(FALSE);
	m_wndGradeGrid.SetRowCount(SCH_MAX_GRADE_STEP);
	m_wndGradeGrid.SetColCount(_COL_GRADE_CODE_);
	m_wndGradeGrid.GetClientRect(&rect);
	m_wndGradeGrid.SetColWidth(0, 0, 30);
	m_wndGradeGrid.SetColWidth(_COL_ITEM1_, _COL_ITEM1_, 65);
	m_wndGradeGrid.SetColWidth(_COL_GRADE_MIN1_, _COL_GRADE_MIN1_, 50);
	m_wndGradeGrid.SetColWidth(_COL_GRADE_MAX1_, _COL_GRADE_MAX1_, 50);
	m_wndGradeGrid.SetColWidth(_COL_GRADE_RELATION_, _COL_GRADE_RELATION_, 60);
	m_wndGradeGrid.SetColWidth(_COL_ITEM2_, _COL_ITEM2_, 65);
	m_wndGradeGrid.SetColWidth(_COL_GRADE_MIN2_, _COL_GRADE_MIN2_, 50);
	m_wndGradeGrid.SetColWidth(_COL_GRADE_MAX2_, _COL_GRADE_MAX2_, 50);
	m_wndGradeGrid.SetColWidth(_COL_GRADE_CODE_, _COL_GRADE_CODE_, 50);
// 	m_wndGradeGrid.SetCoveredCellsRowCol(0, 0, 1, 0);
// 	m_wndGradeGrid.SetCoveredCellsRowCol(0, 1, 0, 2);
// 	m_wndGradeGrid.SetCoveredCellsRowCol(0, 3, 0, 4);
// 	m_wndGradeGrid.SetCoveredCellsRowCol(0, 5, 1, 5);

	m_wndGradeGrid.SetStyleRange(CGXRange(0,0),	CGXStyle().SetValue("No"));
	m_wndGradeGrid.SetStyleRange(CGXRange(0,1),	CGXStyle().SetValue("ITEM1").SetHorizontalAlignment(DT_CENTER).SetBaseStyle(1));

//	m_wndGradeGrid.SetStyleRange(CGXRange(0,1),	CGXStyle().SetValue("용량"));
// #ifdef _EDLC_CELL_
// 	m_wndGradeGrid.SetStyleRange(CGXRange(0,3),	CGXStyle().SetValue("ESR"));
// #else
// 	m_wndGradeGrid.SetStyleRange(CGXRange(0,3),	CGXStyle().SetValue("임피던스"));
// #endif
	m_wndGradeGrid.SetStyleRange(CGXRange(0,2),	CGXStyle().SetValue(TEXT_LANG[69]).SetHorizontalAlignment(DT_CENTER).SetBaseStyle(1));//"하한값"
	m_wndGradeGrid.SetStyleRange(CGXRange(0,3),	CGXStyle().SetValue(TEXT_LANG[70]).SetHorizontalAlignment(DT_CENTER).SetBaseStyle(1));//"상한값"
	m_wndGradeGrid.SetStyleRange(CGXRange(0,4),	CGXStyle().SetValue(TEXT_LANG[71]).SetHorizontalAlignment(DT_CENTER).SetBaseStyle(1));//"조건"
	m_wndGradeGrid.SetStyleRange(CGXRange(0,5),	CGXStyle().SetValue("ITEM2").SetHorizontalAlignment(DT_CENTER).SetBaseStyle(1));
	m_wndGradeGrid.SetStyleRange(CGXRange(0,6),	CGXStyle().SetValue(TEXT_LANG[69]).SetHorizontalAlignment(DT_CENTER).SetBaseStyle(1));//"하한값"
	m_wndGradeGrid.SetStyleRange(CGXRange(0,7),	CGXStyle().SetValue(TEXT_LANG[70]).SetHorizontalAlignment(DT_CENTER).SetBaseStyle(1));//"상한값"
	m_wndGradeGrid.SetStyleRange(CGXRange(0,8),	CGXStyle().SetValue("Code"));
	m_wndGradeGrid.GetParam()->EnableUndo(TRUE);

//	m_wndGradeGrid.SetFrozenRows(1);

	m_wndGradeGrid.SetStyleRange(CGXRange().SetCols(_COL_GRADE_RELATION_),
		CGXStyle().SetHorizontalAlignment(DT_CENTER)
		);
	
	m_pGradeItem1Combo = new CGridComboBox(this, 
								&m_wndGradeGrid,
								GX_IDS_CTRL_ZEROBASED_EX1,
								GX_IDS_CTRL_ZEROBASED_EX1,
								GXCOMBO_ZEROBASED|GXCOMBO_DISPLAYCHOICE
								);
	m_wndGradeGrid.RegisterControl(GX_IDS_CTRL_ZEROBASED_EX1, m_pGradeItem1Combo);
	
	m_pGradeItem2Combo = new CGridComboBox(this, 
								&m_wndGradeGrid,
								GX_IDS_CTRL_ZEROBASED_EX2,
								GX_IDS_CTRL_ZEROBASED_EX2,
								GXCOMBO_ZEROBASED|GXCOMBO_DISPLAYCHOICE
								);
	m_wndGradeGrid.RegisterControl(GX_IDS_CTRL_ZEROBASED_EX2, m_pGradeItem2Combo);


	m_pGradeRelationTypeCombo = new CGridComboBox(this, 
								&m_wndGradeGrid,
								GX_IDS_CTRL_ZEROBASED_EX3,
								GX_IDS_CTRL_ZEROBASED_EX3,
								GXCOMBO_ZEROBASED|GXCOMBO_DISPLAYCHOICE
								);
	m_wndGradeGrid.RegisterControl(GX_IDS_CTRL_ZEROBASED_EX3, m_pGradeRelationTypeCombo);

// 	m_pStepTypeCombo->SetItemData(0, PS_STEP_CHARGE);
// 	m_pStepTypeCombo->SetItemData(1, PS_STEP_DISCHARGE);
// 	m_pStepTypeCombo->SetItemData(0, PS_STEP_CHARGE);
// 	m_pStepTypeCombo->SetItemData(1, PS_STEP_DISCHARGE);
	CString strMode, strGradeType;
	SCH_CODE_MSG *pMsg;
	
	CCTSEditorDoc *pDoc = GetDocument();
	for(int i = 0; i < GetDocument()->m_apGradeType.GetSize(); i++)
	{
		pMsg = (SCH_CODE_MSG *)pDoc->m_apGradeType[i];
		strMode.Format("%s\n", pMsg->szMessage);
		strGradeType += strMode;
		m_pGradeItem1Combo->SetItemData(i, pMsg->nCode);
		m_pGradeItem2Combo->SetItemData(i, pMsg->nCode);
	}
	
	
//	CString strType = " None\n 전압\n 용량\n 임피던스\n 전류\n 시간\n";
	m_wndGradeGrid.SetStyleRange(CGXRange().SetCols(_COL_ITEM1_),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_ZEROBASED_EX1)
		.SetHorizontalAlignment(DT_CENTER)
		.SetChoiceList(strGradeType)
		);
	
	m_wndGradeGrid.SetStyleRange(CGXRange().SetCols(_COL_ITEM2_),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_ZEROBASED_EX2)
		.SetHorizontalAlignment(DT_CENTER)
		.SetChoiceList(strGradeType)
		);

	strMode = "None\n AND\n OR\n";
	m_wndGradeGrid.SetStyleRange(CGXRange().SetCols(_COL_GRADE_RELATION_),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_ZEROBASED_EX3)
		.SetHorizontalAlignment(DT_CENTER)
		.SetChoiceList(strMode)
		.SetValue(0L)
		);
	//임시 색상변경
// 	m_wndGradeGrid.SetStyleRange(CGXRange().SetCols(_COL_GRADE_RELATION_),
// 		CGXStyle().SetInterior( CGXBrush().SetColor(RGB(255, 153, 0))));			

	m_wndGradeGrid.RegisterControl(IDS_CTRL_REALEDIT, new CRealEditControl(&m_wndGradeGrid, IDS_CTRL_REALEDIT));
	m_wndGradeGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITFORMAT);
	m_wndGradeGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITSIZE);
	m_wndGradeGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITSIZE2);

	char str[12], str2[7], str3[5];
	
	//Grade Value 
	sprintf(str, "%%.%dlf", 1);	//소숫점 이하
	sprintf(str2, "%d", 6);		//정수부
	sprintf(str3, "%d", 1);		//소숫점 이하

	m_wndGradeGrid.SetStyleRange(CGXRange().SetCells(1, _COL_GRADE_MIN1_,
		m_wndGradeGrid.GetRowCount(), _COL_GRADE_MAX1_),
		CGXStyle()
			.SetHorizontalAlignment(DT_RIGHT)
			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
	);
	m_wndGradeGrid.SetStyleRange(CGXRange().SetCells(1, _COL_GRADE_MIN2_,
		m_wndGradeGrid.GetRowCount(), _COL_GRADE_MAX2_),
		CGXStyle()
		.SetHorizontalAlignment(DT_RIGHT)
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		);

	//Mask control에 한글입력시 오류가 있음
	//2006/4/27
 /*	m_wndGradeGrid.SetStyleRange(CGXRange().SetCols(3),
		CGXStyle()
  			.SetHorizontalAlignment(DT_CENTER)
			.SetControl(GX_IDS_CTRL_MASKEDIT)
			.SetUserAttribute(GX_IDS_UA_INPUTMASK, _T("A"))
			.SetUserAttribute(GX_IDS_UA_INPUTPROMPT, _T("_"))
	);
*/

	m_wndGradeGrid.SetStyleRange(CGXRange().SetCols(_COL_GRADE_CODE_),
		CGXStyle()
  			.SetHorizontalAlignment(DT_CENTER)
			.SetControl(GX_IDS_CTRL_EDIT)
			.SetMaxLength(1)
	);
	
	return TRUE;
}

void CCTSEditorView::OnDestroy() 
{
	CFormView::OnDestroy();
	
	if(m_nTimer != 0)
	{
		KillTimer(m_nTimer);
		m_nTimer =0;
	}
	// TODO: Add your message handler code here
}


LRESULT CCTSEditorView::OnGridComboDropDown(WPARAM wParam, LPARAM lParam)
{
	CGridComboBox *pCombo = (CGridComboBox*) lParam;
	ROWCOL nRow, nCol;			
	nCol = LOWORD(wParam);		
	nRow = HIWORD(wParam);	//Get Current Row Col
	
	if(nRow < 1)	
	{
		return 0;
	}

	if(m_nDisplayStep < 0 || m_nDisplayStep >= GetDocument()->GetTotalStepNum())	return 0;
	STEP *pStep;
	pStep = (STEP *)GetDocument()->GetStepData(m_nDisplayStep);
	
	if(!pStep)
	{
		return 0;
	}
	
	switch((int)pStep->chType)
	{
	case PS_STEP_CHARGE:
	case PS_STEP_DISCHARGE:
		//			AfxMessageBox("charge step");
		break;
	case PS_STEP_IMPEDANCE:
		
		break;
	case PS_STEP_OCV:
		
		break;
	}

	return 1;
}

LRESULT CCTSEditorView::OnGridComboSelected(WPARAM wParam, LPARAM lParam)
{
	CGridComboBox *pCombo = (CGridComboBox*) lParam;
	int selID = (int) wParam;
	ROWCOL	nRow, nCol;

	int nTotRowCount;
	DWORD nComboData = 0;
	CCTSEditorDoc *pDoc = GetDocument();
	CString strTemp;
	
	int nID = 2;
	int a = 0;

// 2008-11 ljb 추가 Step이 Charge or DisCharge 이면 Grade grid에서 Impedance 선택 못함
	if(pCombo == m_pGradeItem1Combo || pCombo == m_pGradeItem2Combo)		
	{
		nTotRowCount = m_wndStepGrid.GetRowCount();
		if(!m_wndStepGrid.GetCurrentCell(&nRow, &nCol))		return 0;
		STEP *pStep = (STEP *)pDoc->GetStepData(nRow);	
		
		switch((int)pStep->chType)
		{
		case PS_STEP_CHARGE:
		case PS_STEP_DISCHARGE:
			nComboData = pCombo->GetItemData(selID);
			//AfxMessageBox("ff");
		}
		if (selID == PS_GRADE_IMPEDANCE)
		{
			AfxMessageBox(TEXT_LANG[72]);//"현재 모드 에서는 선택할 수 없습니다."
			if(!m_wndGradeGrid.GetCurrentCell(&nRow, &nCol))		return 0;
			m_wndGradeGrid.SetStyleRange(CGXRange(nRow,nCol),
				CGXStyle().SetValue(0L));
		}
	}
//2008-11 ljb 추가 끝------------------

	//Step Type or Step Procedure Type Select
	if(pCombo == m_pStepTypeCombo || pCombo == m_pProcTypeCombo)		
	{
		nTotRowCount = m_wndStepGrid.GetRowCount();
		if(!m_wndStepGrid.GetCurrentCell(&nRow, &nCol))		return 0;
		STEP *pStep = (STEP *)pDoc->GetStepData(nRow);	
		
		int nSelType;
		int i= 0;
		
		//선택한 Step Type을 구함
		nComboData = pCombo->GetItemData(selID);
		if(pCombo == m_pProcTypeCombo)
		{
			 nSelType= nComboData>>16;
			 if(pStep->nProcType == nComboData)		return 0;		//not changed
		}
		else
		{
			nSelType = nComboData;		
			if(pStep->chType == nComboData)			return 0;		//Not changed
		}

		TRACE("Step Type %d Selected\n", nSelType);

		if(nSelType < 0)	
		{
			return 0;
		}

		//선택한 Step Type이 End가 아니면 새로운 Step 자동 생성
		if(nSelType != PS_STEP_END && nTotRowCount == nRow)
		{
			AddStepNew(nRow); 
		}
		//선택한 Step Type이 End이면 밑에 있는 Step을 모두 지움
		if(nSelType == PS_STEP_END && nTotRowCount > nRow)
		{
//			m_wndStepGrid.SelectRange(CGXRange(nRow+1, 0, nTotRowCount, m_wndStepGrid.GetColCount()));
			CDWordArray awStep;
			for(i = nRow+1; i <= nTotRowCount; i++)
			{
				//m_wndStepGrid.SetCurrentCell(i, COL_STEP_TYPE);	//Move Current select step
				//OnStepDelete();
				awStep.Add(i);
			}
			if(MessageBox(TEXT_LANG[73], TEXT_LANG[74], MB_ICONQUESTION|MB_YESNO) == IDYES)//"현재 이후 Step을 삭제 하시겠습니까?"//"삭제"
			{
				DeleteStep(awStep);
			}
		}

		//Step selection changed to different type
		//Step이 추가 되거나 변경 되었으므로 다시 Step을 구한다.
		pStep = (STEP *)pDoc->GetStepData(nRow);	
		ZeroMemory(pStep, sizeof(STEP));	//Clear Inner Varible Data

		int nTotalStepNum = pDoc->GetTotalStepNum();
		int nFindGotoStepID = 1;
		BOOL nFind = TRUE;
		while(nFind)
		{
			nFind = FALSE;
			for(i = 1 ; i <= nTotalStepNum; i++)
			{
				STEP * pTempStep = pDoc->GetStepData(i);
				if(pTempStep == NULL) break;
				if(pTempStep->nGotoStepID == nFindGotoStepID)
				{
					nFindGotoStepID++;
					nFind = TRUE;
					break;
				}
				else
					nFind = FALSE;
			}
			
		}

		pStep->nGotoStepID = nFindGotoStepID;
		
		pStep->chType = nSelType;		//Update Current Step Type		

		// ljb 2009925 S	/////////////////
		/*
		if(nSelType == PS_STEP_NONE)
		{
			pStep->chType = PS_STEP_CHARGE;
		}
		else
		{
			
		}*/
		// ljb 2009925 E /////////////////

		if(pCombo == m_pProcTypeCombo)	//ProcStep이면 Procedure Type을 Update 한다.
		{
			pStep->nProcType =	nComboData;		
		}
				
		if(pStep->chType == PS_STEP_CHARGE || pStep->chType == PS_STEP_DISCHARGE)			
		{
			//최초 기본 저장 시간을 1분으로 한다.
			if(pStep->fIref == 0.0f)	//최초 
			{
				pStep->ulReportTime = 60.0f*100.0f;		//60sec
			}

			//Aging일 경우 방전 전류 고정
			if(pDoc->m_lLoadedProcType == PS_PGS_AGING)
			{
				pStep->fIref = pDoc->m_lMaxCurrent1;
			}

			if(pStep->chType == PS_STEP_CHARGE) 
			{
#ifdef _NIMH_CELL_
				//NIMH 전지는 CC로만 충전하므로 
				pStep->chMode = PS_MODE_CC;
#else
				pStep->chMode = PS_MODE_CCCV;	// 충전일 경우 기본 CCCV	
#endif
				pStep->fVref =  (float)(GetDocument()->m_lVRefHigh);			
			}

			if(pStep->chType == PS_STEP_DISCHARGE)
			{
				pStep->chMode = PS_MODE_CCCV;	//방전일 경우 기본 CC
				pStep->fVref =  (float)(GetDocument()->m_lVRefLow);			
			}

			if(m_bVHigh)	pStep->fVLimitHigh = m_fVHighVal;	//m_fVHighVal*V_UNIT_FACTOR);
			if(m_bVLow)		pStep->fVLimitLow = m_fVLowVal;	//m_fVLowVal*V_UNIT_FACTOR);
			if(m_bIHigh)	pStep->fILimitHigh = m_fIHighVal;	
			if(m_bILow)		pStep->fILimitLow = m_fILowVal;	
			if(m_bCHigh)	pStep->fCLimitHigh = m_fCHighVal;	
			if(m_bCLow)		pStep->fCLimitLow = m_fCLowVal;	
		}	
		else if(pStep->chType == PS_STEP_IMPEDANCE)	// Impedance 일 경우 기본 DC_Imp
		{
			if(IsExecutableStep(nID, PS_STEP_IMPEDANCE, PS_MODE_DCIMP))
			{
				pStep->ulReportTime = 1.1f*100.0f;		//1.1sec
				pStep->chMode = PS_MODE_DCIMP;
				if(m_bVHigh)	pStep->fVLimitHigh = m_fVHighVal;	
				if(m_bVLow)		pStep->fVLimitLow = m_fVLowVal;	//m_fVLowVal*V_UNIT_FACTOR);
				if(m_bIHigh)	pStep->fILimitHigh = m_fIHighVal;	//m_fIHighVal*I_UNIT_FACTOR);
				if(m_bILow)		pStep->fILimitLow = m_fILowVal;	//m_fILowVal*I_UNIT_FACTOR);
			}
			else
			{
				pStep->chMode = PS_MODE_ACIMP;
			}
		/*	if(IsExecutableStep(nID, PS_STEP_IMPEDANCE, PS_MODE_ACIMP))
			{
				pStep->chMode = PS_MODE_ACIMP;
			}
			else
			{
				pStep->ulReportTime = 1.1f*100.0f;		//1.1sec
				pStep->chMode = PS_MODE_DCIMP;
				if(m_bVHigh)	pStep->fVLimitHigh = m_fVHighVal;	
				if(m_bVLow)		pStep->fVLimitLow = m_fVLowVal;	//m_fVLowVal*V_UNIT_FACTOR);
				if(m_bIHigh)	pStep->fILimitHigh = m_fIHighVal;	//m_fIHighVal*I_UNIT_FACTOR);
				if(m_bILow)		pStep->fILimitLow = m_fILowVal;	//m_fILowVal*I_UNIT_FACTOR);
			}*/
		}
		else if(pStep->chType == PS_STEP_LOOP)
		{
			pStep->nLoopInfoCycle = 1;
			//현재 완료(End) Step이나 다음 cycle의 처음을 지칭한다.
			pStep->nLoopInfoGoto  = PS_GOTO_NEXT_CYC;//nRow+1;	// 0 is Next Cycle
		}
		else if(pStep->chType == PS_STEP_REST)
		{
			pStep->ulReportTime = 300.0f*100.0f;		//5분 
			if(m_bVHigh)	pStep->fVLimitHigh = m_fVHighVal;	
			if(m_bVLow)		pStep->fVLimitLow = m_fVLowVal;	//m_fVLowVal*V_UNIT_FACTOR);
		}
//		SettingStepWndType(nRow, pStep->chType);		//Clear Display Value
		DisplayStepGrid	(nRow);
		m_wndStepGrid.SetCurrentCell(nRow, COL_STEP_REF_V);
		
		//2006/9/20
		//적용가능한 Step인지 검사 	
//		int nID = -1;
//		for(int a=0; a<m_wndTestListGrid.GetRowCount(); a++)
//		{
//			if(m_lLoadedTestID == atoi(m_wndTestListGrid.GetValueRowCol(a+1, COL_TEST_PRIMARY_KEY)))
//			{
//				nID = atol(m_wndTestListGrid.GetValueRowCol(a+1, COL_TEST_PROC_TYPE));
				if(IsExecutableStep(nID, pStep->chType, pStep->chMode) == FALSE)
				{
					strTemp.Format(TEXT_LANG[75], m_wndTestListGrid.GetValueRowCol(a+1, COL_TEST_NAME));//"선택한 Type과 Mode는 [%s]공정에서 시행할수 없는 Type입니다.\nType과 Mode를 확인하십시요."
					MessageBox(strTemp, "Error", MB_OK|MB_ICONSTOP);
					return 0;
				}
//				break;
//			}
//		}
	}
	else if(pCombo == m_pStepModeCombo)		//Mode Combo Selected
	{
		nTotRowCount = m_wndStepGrid.GetRowCount();
		if(!m_wndStepGrid.GetCurrentCell(&nRow, &nCol))		return 0;

		STEP *pStep = (STEP *)pDoc->GetStepData(nRow);		//Clear Inner Varible Data
//		pStep->chMode = selID+1;
		if(pStep->chMode == m_pStepModeCombo->GetItemData(selID))		return 0;	//not changed
		
		pStep->chMode = m_pStepModeCombo->GetItemData(selID);

		if(pStep->chType == PS_STEP_IMPEDANCE)
		{
			if(pStep->chMode == PS_MODE_ACIMP)			//AC Imp Mode 선택시 관련 변수 초기화
			{
				pStep->ulEndTime = 0;
				pStep->fVref = 0.0f;
				pStep->fIref = 0.0f;
				pStep->ulReportTime = 0;
			}
		}
		else if(pStep->chType == PS_STEP_DISCHARGE || pStep->chType == PS_STEP_CHARGE)
		{
			m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_REF_I), TEXT_LANG[76]+GetDocument()->GetCrtUnit()+")");//"전류("
			if(pStep->chMode == PS_MODE_CC)				//CC 모든 선택시 END I 초기화 
			{
				pStep->fVref = 0.0f;
				pStep->fEndI = 0.0f;
				if(pStep->chType == PS_STEP_CHARGE)	
				{
					pStep->fVref = (float)pDoc->m_lMaxVoltage;
				}
				m_wndStepGrid.SetCurrentCell(nRow, COL_STEP_REF_I);
			}
			else if(pStep->chMode == PS_MODE_CCCV)		//CC/CV 변경 하면 End V값을 초기화 
			{
				pStep->fEndV = 0.0f;
				m_wndStepGrid.SetCurrentCell(nRow, COL_STEP_REF_V);
			}
			else if(pStep->chMode == PS_MODE_CP)
			{
				pStep->fIref = 0.0f;
				pStep->fVref = 0.0f;
				if(pStep->chType == PS_STEP_CHARGE)
				{
					pStep->fVref = (float)pDoc->m_lMaxVoltage;
				}
				m_wndStepGrid.SetCurrentCell(nRow, COL_STEP_REF_I);
				//전류 Ref 입력 Column에서 Ref Power를 받아 들인다.
				strTemp.Format(TEXT_LANG[77], GetDocument()->GetWattUnit());//"파워(%s)"
				m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_REF_I), strTemp);
			}
			else if(pStep->chMode == PS_MODE_CR)
			{
				pStep->fVref = 0.0f;
				pStep->fIref = 0.0f;
				if(pStep->chType == PS_STEP_CHARGE)
				{
					pStep->fVref = (float)pDoc->m_lMaxVoltage;
				}
				m_wndStepGrid.SetCurrentCell(nRow, COL_STEP_REF_I);
				//전류 Ref 입력 Column에서 Ref Resistor를  받아 들인다.
				
				strTemp = TEXT_LANG[78];//"저항(Ω)"
				m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_REF_I), strTemp);
			}
			else if(pStep->chMode == PS_MODE_CV)
			{
				pStep->fEndV = 0.0f;
				//pStep->fIref = (float)pDoc->m_lMaxCurrent;
				m_wndStepGrid.SetCurrentCell(nRow, COL_STEP_REF_V);
			}
		}
		DisplayStepGrid	(nRow);

		//2006/9/20
		//적용가능한 Step인지 검사 	
//		int nID = -1;
//		for(int a=0; a<m_wndTestListGrid.GetRowCount(); a++)
//		{
//			if(m_lLoadedTestID == atoi(m_wndTestListGrid.GetValueRowCol(a+1, COL_TEST_PRIMARY_KEY)))
//			{
//				nID = atol(m_wndTestListGrid.GetValueRowCol(a+1,COL_TEST_PROC_TYPE));
				if(IsExecutableStep(nID, pStep->chType, pStep->chMode) == FALSE)
				{
					strTemp.Format(TEXT_LANG[79], m_wndTestListGrid.GetValueRowCol(a+1, COL_TEST_NAME));//"선택한 Mode는 [%s]공정에서 시행할수 없는 Mode입니다."
					MessageBox(strTemp, "Error", MB_OK|MB_ICONSTOP);
					return 0;
				}
//				break;
//			}
//		}

	}
	else if(pCombo == m_pTestTypeCombo)
	{
		nTotRowCount = m_wndTestListGrid.GetRowCount();
		if(!m_wndTestListGrid.GetCurrentCell(&nRow, &nCol))		return 0;
		
		//Test명을 선택된 공정명 Type과 동일 하게 한다.(공정명을 사용자 임의 입력이 아니라 주어진 범위에서 선택하도록)
		CString strName;
		m_pTestTypeCombo->GetWindowText(strName);
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_NAME), strName);
		
		//Step을 검사하여 시행 불가능한 Step이 포함되어 있으면 경고 표시
		if(CheckExecutableStep(selID, atol(m_wndTestListGrid.GetValueRowCol(nRow, COL_TEST_PRIMARY_KEY)), m_lDisplayModelID)	 == FALSE)
		{
			strTemp.Format(TEXT_LANG[80],//"수정된 공정[%s]에서 실시할 수 없는 Step을 포함하고 있습니다. 수정된 공정에서 시행할 수 없는 Step은 삭제되거나 수정되어 합니다."
				strName);
			AfxMessageBox(strTemp);
		}
	}

	return 1;
}

//Row Drag
LRESULT CCTSEditorView::OnSelDragRowsDrop(WPARAM wParam, LPARAM lParam)
{
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;
	if(pGrid == &m_wndTestListGrid)
	{
		UpdateTestNo();
	}
	else if(pGrid == m_pWndCurModelGrid)
	{
		UpdateModelNo();
	}
	else if(pGrid == &m_wndStepGrid)
	{
		GX_DROP_TARGET *pTarget = (GX_DROP_TARGET *)wParam;


	}

	return 1;
}

//Show Context Menu
LRESULT CCTSEditorView::OnRButtonClickedRowCol(WPARAM wParam, LPARAM lParam)
{
	ROWCOL nRow, nCol;
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;
	nCol = wParam & 0x0000ffff;
	nRow = wParam >> 16;
	
	ASSERT(pGrid);
	if(nCol<0 || nRow < 1 || &m_wndStepGrid != pGrid)	return 0;
	
	CPoint point;
	::GetCursorPos(&point);
	
	if (point.x == -1 && point.y == -1)
	{
		//keystroke invocation
		CRect rect;
		GetClientRect(rect);
		ClientToScreen(rect);

		point = rect.TopLeft();
		point.Offset(5, 5);
	}

/*	if(pGrid == (CMyGridWnd *)&m_wndModuleGrid)
	{
		m_nCmdTarget = BOARD_CMD;
	}
	else if(pGrid == (CMyGridWnd *)&m_wndTopGrid)
	{
		m_nCmdTarget = CHANNEL_CMD;
	}
*/
	CMenu menu;
	VERIFY(menu.LoadMenu(IDR_MAINFRAME));

	CMenu* pPopup = menu.GetSubMenu(2);
	ASSERT(pPopup != NULL);
	CWnd* pWndPopupOwner = this;

	while (pWndPopupOwner->GetStyle() & WS_CHILD)
		pWndPopupOwner = pWndPopupOwner->GetParent();

	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, pWndPopupOwner);

	return 1;
}

void CCTSEditorView::InitFont()
{
	static CFont	font;
	LOGFONT			LogFont;

	GetDlgItem(IDC_STEP_RANGE2)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight = 1000;
	LogFont.lfHeight = 16;

	font.CreateFontIndirect( &LogFont );

	GetDlgItem(IDC_STEP_RANGE)->SetFont(&font);
	GetDlgItem(IDC_STEP_RANGE2)->SetFont(&font);
	GetDlgItem(IDC_STEP_RANGE3)->SetFont(&font);
	GetDlgItem(IDC_STEP_RANGE4)->SetFont(&font);
	GetDlgItem(IDC_STEP_RANGE5)->SetFont(&font);
	
	GetDlgItem(IDC_CONTACK_UPPER_V)->SetFont(&font);
	GetDlgItem(IDC_CONTACK_LOWER_V)->SetFont(&font);
	GetDlgItem(IDC_CONTACK_UPPER_I)->SetFont(&font);
	GetDlgItem(IDC_CONTACK_LOWER_I)->SetFont(&font);
	GetDlgItem(IDC_CONTACK_LIMIT_V)->SetFont(&font);
	GetDlgItem(IDC_CONTACT_DELTA_LIMIT_V)->SetFont(&font);

	GetDlgItem(IDC_CELL_CHECK_TIME_EDIT)->SetFont(&font);
	GetDlgItem(IDC_PRETEST_TRCKLE_I)->SetFont(&font);
//	GetDlgItem(IDC_PRETEST_FAULT_NO)->SetFont(&font);
	GetDlgItem(IDC_CONTACK_RESISTANCE)->SetFont(&font);
	GetDlgItem(IDC_VTG_HIGH)->SetFont(&font);
	GetDlgItem(IDC_VTG_LOW)->SetFont(&font);
	GetDlgItem(IDC_CRT_HIGH)->SetFont(&font);
	GetDlgItem(IDC_CRT_LOW)->SetFont(&font);
	GetDlgItem(IDC_CAP_HIGH)->SetFont(&font);
	GetDlgItem(IDC_CAP_LOW)->SetFont(&font);
	GetDlgItem(IDC_IMP_LOW)->SetFont(&font);
	GetDlgItem(IDC_IMP_HIGH)->SetFont(&font);
	GetDlgItem(IDC_REPORT_TIME)->SetFont(&font);
	GetDlgItem(IDC_VI_GET_TIME)->SetFont(&font);
	GetDlgItem(IDC_EDIT_REG_TEMP)->SetFont(&font);
	GetDlgItem(IDC_EDIT_RESISTANCE_RATE)->SetFont(&font);
	GetDlgItem(IDC_COMP_TIME1)->SetFont(&font);
	GetDlgItem(IDC_COMP_TIME2)->SetFont(&font);
	GetDlgItem(IDC_COMP_V2)->SetFont(&font);
	GetDlgItem(IDC_COMP_V5)->SetFont(&font);

	GetDlgItem(IDC_COMP_CHG_CC_VTG)->SetFont(&font);
	GetDlgItem(IDC_COMP_CHG_CC_TIME)->SetFont(&font);
	GetDlgItem(IDC_COMP_CHG_CC_DELTA_VTG)->SetFont(&font);
	GetDlgItem(IDC_COMP_CHG_CV_CRT)->SetFont(&font);
	GetDlgItem(IDC_COMP_CHG_CV_TIME)->SetFont(&font);
	GetDlgItem(IDC_COMP_CHG_CV_DELTA_CRT)->SetFont(&font);
}

void CCTSEditorView::InitCtrl()
{
	m_LabelSelectInfo.SetFontSize(20)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND_STAGENAME)
		.SetFontBold(TRUE)
		.SetFontName("HY헤드라인M")
		.SetText("-");

	m_Label1.SetBkColor(RGB_MIDNIGHTBLUE)
		.SetTextColor(RGB_WHITE)
		.SetFontSize(24)
		.SetFontBold(TRUE)
		.SetText("Process setting ( CHG/DCHG )");	
}

//Initialize Step Data Edit Ctrl
BOOL CCTSEditorView::InitEditCtrl()
{
	COleDateTime ole;
	SECCurrencyEdit::Format fmt(FALSE);
	fmt.SetMonetarySymbol(0);
	fmt.SetPositiveFormat(2);
	fmt.SetNegativeFormat(2);
	fmt.EnableLeadingZero(FALSE);
	fmt.SetFractionalDigits(1);
	fmt.SetThousandSeparator(0);
	fmt.SetDecimalDigits(0);
	
	int a = 3, b =3;
	a = GetDocument()->GetVtgUnitFloat();
	if(GetDocument()->GetVtgUnit() == "V")
	{
		b = 3;
	}
	else
	{
		b = 6;
	}

	fmt.SetFractionalDigits(a);	fmt.SetDecimalDigits(b);
	//안전조건 전압 상한값 
	VERIFY(m_PreMaxV.Initialize(this, IDC_PRETEST_MAXV));
	m_PreMaxV.SetFormat(fmt); m_PreMaxV.SetValue(0.0);
	
	//안전조건 전압 하한값 
	VERIFY(m_PreMinV.Initialize(this, IDC_PRETEST_MINV));
	m_PreMinV.SetFormat(fmt); m_PreMinV.SetValue(0.0);

	//end voltage비교
	VERIFY(m_DeltaV.Initialize(this, IDC_DELTA_V));				m_DeltaV.SetFormat(fmt);		m_DeltaV.SetValue(0.0);

	//전압 상승 비교 하한값
	VERIFY(m_CompV[0].Initialize(this, IDC_COMP_V1));			m_CompV[0].SetFormat(fmt);		m_CompV[0].SetValue(0.0);
	VERIFY(m_CompV[1].Initialize(this, IDC_COMP_V2));			m_CompV[1].SetFormat(fmt);		m_CompV[1].SetValue(0.0);
	VERIFY(m_CompV[2].Initialize(this, IDC_COMP_V3));			m_CompV[2].SetFormat(fmt);		m_CompV[2].SetValue(0.0);

	//전압 상승 비교 상한값
	VERIFY(m_CompV1[0].Initialize(this, IDC_COMP_V4));			m_CompV1[0].SetFormat(fmt);		m_CompV1[0].SetValue(0.0);
	VERIFY(m_CompV1[1].Initialize(this, IDC_COMP_V5));			m_CompV1[1].SetFormat(fmt);		m_CompV1[1].SetValue(0.0);
	VERIFY(m_CompV1[2].Initialize(this, IDC_COMP_V6));			m_CompV1[2].SetFormat(fmt);		m_CompV1[2].SetValue(0.0);

	//Report 조건 전압
	VERIFY(m_ReportV.Initialize(this, IDC_REPORT_VOLTAGE));		m_ReportV.SetFormat(fmt);		m_ReportV.SetValue(0.0);
	
//	VERIFY(m_PreReverseV.Initialize(this, IDC_PRETEST_FAULT_NO)); m_PreReverseV.SetFormat(fmt);		m_PreReverseV.SetValue(0.0);


	//20210223 KSJ
	VERIFY(m_PreContactLimitV.Initialize(this, IDC_CONTACK_LIMIT_V)); m_PreContactLimitV.SetFormat(fmt); m_PreContactLimitV.SetValue(0.0);
	VERIFY(m_PreContactUpperV.Initialize(this, IDC_CONTACK_UPPER_V)); m_PreContactUpperV.SetFormat(fmt); m_PreContactUpperV.SetValue(0.0);
	VERIFY(m_PreContactLowerV.Initialize(this, IDC_CONTACK_LOWER_V)); m_PreContactLowerV.SetFormat(fmt); m_PreContactLowerV.SetValue(0.0);
	VERIFY(m_PreContactDeltaLimitV.Initialize(this, IDC_CONTACT_DELTA_LIMIT_V)); m_PreContactDeltaLimitV.SetFormat(fmt); m_PreContactDeltaLimitV.SetValue(0.0);


	//EDLC Capa 측정
	VERIFY(m_CapVtgLow.Initialize(this, IDC_CAP_VTG_LOW));		m_CapVtgLow.SetFormat(fmt);		m_CapVtgLow.SetValue(0.0);
	VERIFY(m_CapVtgHigh.Initialize(this, IDC_CAP_VTG_HIGH));	m_CapVtgHigh.SetFormat(fmt);	m_CapVtgHigh.SetValue(0.0);

	VERIFY(m_VtgHigh.Initialize(this, IDC_VTG_HIGH));			m_VtgHigh.SetFormat(fmt);	m_VtgHigh.SetValue(0.0);	
	VERIFY(m_VtgLow.Initialize(this, IDC_VTG_LOW));				m_VtgLow.SetFormat(fmt);	m_VtgLow.SetValue(0.0);
	
	VERIFY(m_CompChgCcVtg.Initialize(this, IDC_COMP_CHG_CC_VTG));		m_CompChgCcVtg.SetFormat(fmt);	m_CompChgCcVtg.SetValue(0.0);	
	VERIFY(m_CompChgCcDeltaVtg.Initialize(this, IDC_COMP_CHG_CC_DELTA_VTG));	m_CompChgCcDeltaVtg.SetFormat(fmt);	m_CompChgCcDeltaVtg.SetValue(0.0);

	a = GetDocument()->GetCrtUnitFloat();
	if(GetDocument()->GetCrtUnit() == "A")
	{
		b = 3;
	}
	else
	{
		b = 6;
	}

	fmt.SetFractionalDigits(a);	fmt.SetDecimalDigits(b);
	//안전조건 전류 상한값 
	VERIFY(m_PreMaxI.Initialize(this, IDC_PRETEST_MAXI));
	m_PreMaxI.SetFormat(fmt); m_PreMaxI.SetValue(0.0);
	
	//안전조건 전류 하한값 
	VERIFY(m_TrickleI.Initialize(this, IDC_PRETEST_TRCKLE_I));
	m_TrickleI.SetFormat(fmt); m_TrickleI.SetValue(0.0);
	
	//안전조건 용량 상한값 
	VERIFY(m_PreOCV.Initialize(this, IDC_PRETEST_OCV));
	m_PreOCV.SetFormat(fmt); m_PreOCV.SetValue(0.0);	
	
/*	VERIFY(m_TrickleTime.AttachDateTimeCtrl(IDC_PRETEST_TRICKLE_TIME, this, 0));
	m_TrickleTime.SetFormat("HH:mm:ss");
	m_TrickleTime.SetTime(ole);
*/
/*	fmt.SetFractionalDigits(0);	fmt.SetDecimalDigits(3);
	VERIFY(m_FaultNo.Initialize(this, IDC_PRETEST_FAULT_NO));
	m_FaultNo.SetFormat(fmt); m_FaultNo.SetValue(0.0);
*/	


	//20210223 KSJ
	VERIFY(m_PreContactUpperI.Initialize(this, IDC_CONTACK_UPPER_I));	m_PreContactUpperI.SetFormat(fmt);	m_PreContactUpperI.SetValue(0.0);
	VERIFY(m_PreContactLowerI.Initialize(this, IDC_CONTACK_LOWER_I));	m_PreContactLowerI.SetFormat(fmt);	m_PreContactLowerI.SetValue(0.0);		


	//전류 설정
	VERIFY(m_CrtHigh.Initialize(this, IDC_CRT_HIGH));			m_CrtHigh.SetFormat(fmt);		m_CrtHigh.SetValue(0.0);
	VERIFY(m_CrtLow.Initialize(this, IDC_CRT_LOW));				m_CrtLow.SetFormat(fmt);		m_CrtLow.SetValue(0.0);		
	VERIFY(m_DeltaI.Initialize(this, IDC_DELTA_I));				m_DeltaI.SetFormat(fmt);		m_DeltaI.SetValue(0.0);

	//전류 상승 비교 하한값
	VERIFY(m_CompI[0].Initialize(this, IDC_COMP_I1));
	m_CompI[0].SetFormat(fmt); m_CompI[0].SetValue(0.0);

	VERIFY(m_CompI[1].Initialize(this, IDC_COMP_I2));
	m_CompI[1].SetFormat(fmt); m_CompI[1].SetValue(0.0);

	VERIFY(m_CompI[2].Initialize(this, IDC_COMP_I3));
	m_CompI[2].SetFormat(fmt); m_CompI[2].SetValue(0.0);

	//전류 상승 비교 상한값
	VERIFY(m_CompI1[0].Initialize(this, IDC_COMP_I4));
	m_CompI1[0].SetFormat(fmt); m_CompI1[0].SetValue(0.0);

	VERIFY(m_CompI1[1].Initialize(this, IDC_COMP_I5));
	m_CompI1[1].SetFormat(fmt); m_CompI1[1].SetValue(0.0);

	VERIFY(m_CompI1[2].Initialize(this, IDC_COMP_I6));
	m_CompI1[2].SetFormat(fmt); m_CompI1[2].SetValue(0.0);

	//Report 조건 전류
	VERIFY(m_ReportI.Initialize(this, IDC_REPORT_CURRENT));
	m_ReportI.SetFormat(fmt); m_ReportI.SetValue(0.0);

	VERIFY(m_CompChgCvCrt.Initialize(this, IDC_COMP_CHG_CV_CRT));				m_CompChgCvCrt.SetFormat(fmt);		m_CompChgCvCrt.SetValue(0.0);		
	VERIFY(m_CompChgCvDeltaCrt.Initialize(this, IDC_COMP_CHG_CV_DELTA_CRT));	m_CompChgCvDeltaCrt.SetFormat(fmt);	m_CompChgCvDeltaCrt.SetValue(0.0);
	
	a = GetDocument()->GetCapUnitFloat();
	if(GetDocument()->GetCapUnit() == "A" || GetDocument()->GetCapUnit() == "F")
	{
		b = 3;
	}
	else
	{
		b = 6;
	}
	fmt.SetFractionalDigits(a);	fmt.SetDecimalDigits(b);
	VERIFY(m_CapHigh.Initialize(this, IDC_CAP_HIGH));			m_CapHigh.SetFormat(fmt);		m_CapHigh.SetValue(0.0);
	VERIFY(m_CapLow.Initialize(this, IDC_CAP_LOW));				m_CapLow.SetFormat(fmt);		m_CapLow.SetValue(0.0);

	//Report 온도 조건 
	fmt.SetFractionalDigits(1);	fmt.SetDecimalDigits(3);
	VERIFY(m_ReportTemp.Initialize(this, IDC_REPORT_TEMPERATURE));
	m_ReportTemp.SetFormat(fmt); m_ReportTemp.SetValue(0.0);
	
	//안전조건 온도 상한값 
	VERIFY(m_PreDeltaV.Initialize(this, IDC_PRETEST_DELTA_V));
	m_PreDeltaV.SetFormat(fmt); m_PreDeltaV.SetValue(0.0);
	
	VERIFY(m_DCIR_RegTemp.Initialize(this, IDC_EDIT_REG_TEMP));
	m_DCIR_RegTemp.SetFormat(fmt); m_DCIR_RegTemp.SetValue(0.0);
	
	//Contact Check reverse cell voltage
		
	//온도 상하한값
	VERIFY(m_TempLow.Initialize(this, IDC_TEMP_LOW));
	m_TempLow.SetFormat(fmt); m_TempLow.SetValue(0.0);
	VERIFY(m_TempHigh.Initialize(this, IDC_TEMP_HIGH));
	m_TempHigh.SetFormat(fmt); m_TempHigh.SetValue(0.0);
	
	//Impedance
	VERIFY(m_ImpHigh.Initialize(this, IDC_IMP_HIGH));					m_ImpHigh.SetFormat(fmt);		m_ImpHigh.SetValue(0.00);
	VERIFY(m_ImpLow.Initialize(this, IDC_IMP_LOW));						m_ImpLow.SetFormat(fmt);		m_ImpLow.SetValue(0.00);
	
	//	VERIFY(m_DeltaTime.AttachDateTimeCtrl(IDC_DELTA_TIME, this, 0));
	m_DeltaTime.SetFormat("HH:mm:ss");
	m_DeltaTime.SetTime(DATE(0));

	VERIFY(m_CompTimeI[0].AttachDateTimeCtrl(IDC_COMP_I_TIME1, this, 0));
	m_CompTimeI[0].SetFormat("HH:mm:ss");
	m_CompTimeI[0].SetTime(ole);

	VERIFY(m_CompTimeI[1].AttachDateTimeCtrl(IDC_COMP_I_TIME2, this, 0));
	m_CompTimeI[1].SetFormat("HH:mm:ss");
	m_CompTimeI[1].SetTime(ole);

	VERIFY(m_CompTimeI[2].AttachDateTimeCtrl(IDC_COMP_I_TIME3, this, 0));
	m_CompTimeI[2].SetFormat("HH:mm:ss");
	m_CompTimeI[2].SetTime(ole);

	//Report 시간
//	VERIFY(m_ReportTime.AttachDateTimeCtrl(IDC_REPORT_TIME, this, 0));
	m_ReportTime.SetFormat("HH:mm:ss");
	m_ReportTime.SetTime(DATE(0));
	
	fmt.SetFractionalDigits(3);	fmt.SetDecimalDigits(2);
//	VERIFY(m_PreResistance.Initialize(this, IDC_CONTACK_RESISTANCE));	m_PreResistance.SetFormat(fmt); m_PreResistance.SetValue(0.000);	
	
	fmt.SetFractionalDigits(2);	fmt.SetDecimalDigits(2);
	VERIFY(m_DCIR_ResistanceRate.Initialize(this, IDC_EDIT_RESISTANCE_RATE));
	m_DCIR_ResistanceRate.SetFormat(fmt); m_DCIR_ResistanceRate.SetValue(0.00);
	
	fmt.EnableLeadingZero(TRUE);
	fmt.SetFractionalDigits(0);	
	fmt.SetDecimalDigits(5);
	// VERIFY(m_VIGetTime.Initialize(this, IDC_VI_GET_TIME));	m_VIGetTime.SetFormat(fmt); m_VIGetTime.SetValue(0);
	VERIFY(m_CompTimeV[0].Initialize(this, IDC_COMP_TIME1)); m_CompTimeV[0].SetFormat(fmt); m_CompTimeV[0].SetValue(0);
	VERIFY(m_CompTimeV[1].Initialize(this, IDC_COMP_TIME2)); m_CompTimeV[1].SetFormat(fmt); m_CompTimeV[1].SetValue(0);
	VERIFY(m_CompTimeV[2].Initialize(this, IDC_COMP_TIME3)); m_CompTimeV[2].SetFormat(fmt); m_CompTimeV[2].SetValue(0);
	
	VERIFY(m_CompChgCcTime.Initialize(this, IDC_COMP_CHG_CC_TIME));	m_CompChgCcTime.SetFormat(fmt); m_CompChgCcTime.SetValue(0);
	VERIFY(m_CompChgCvtTime.Initialize(this, IDC_COMP_CHG_CV_TIME));	m_CompChgCvtTime.SetFormat(fmt); m_CompChgCvtTime.SetValue(0);

	return TRUE;
}

//ReDraw Step Grid
BOOL CCTSEditorView::DisplayStepGrid(int nStepNum)
{
	CCTSEditorDoc *pDoc = (CCTSEditorDoc *)GetDocument();		//Get Document point
	int nTotStepNum = pDoc->GetTotalStepNum();					//Total Step Number
	if(nTotStepNum < 0)	return FALSE;

	CString strTemp;
	CString strTime;
	COleDateTime endTime;
	STEP *pStep;
	ROWCOL nRow = m_wndStepGrid.GetRowCount();					//Get Current Displayed Step Num

	long lSecond = 0;
	long mSec = 0;
	long nHour = 0;

	if(nStepNum > 0)											//UpDate 1 Step Grid
	{
		if(nStepNum > nTotStepNum || nStepNum > nRow)	return FALSE;

		pStep = (STEP *)pDoc->GetStepData(nStepNum);
		if(!pStep)	return FALSE;
		
		BOOL bLock = m_wndStepGrid.LockUpdate(TRUE);
		
		//Step Grid를 현재 Type에 맞게 Setting 한다.
		//Step Type/Mode 표시 한다.
		SettingStepWndType(nStepNum, (int)pStep->chType);

		//안전값 및 기타 옵션 표시
		DisplayStepOption(pStep);

		//Added by  KBH 2005///////////////////////////////////////////////////////
		//현재 선택된 Step을 색상으로 표시한다.
		//Row Header Style
		for(int k =0; k<nRow; k++)
		{
			m_wndStepGrid.SetStyleRange(CGXRange(k+1, 0), CGXStyle()
													.SetInterior(RGB(255,255,230))
													.SetFont(CGXFont().SetBold(FALSE))
										);
		}
		//Set current row
		m_wndStepGrid.SetStyleRange(CGXRange(nStepNum, 0), CGXStyle()
										.SetInterior(RGB(255,255,0))
										.SetFont(CGXFont().SetBold(TRUE))
									);
		//////////////////////////////////////////////////////////////////////////

		//all row Grid style
		m_wndStepGrid.SetStyleRange(CGXRange().SetRows(1, nRow), CGXStyle()
										.SetTextColor(RGB(165,165,165))
										.SetFont(CGXFont().SetBold(FALSE))
									);

		//Set current row
		m_wndStepGrid.SetStyleRange(CGXRange().SetRows(nStepNum), CGXStyle()
											.SetTextColor(RGB(0,0,0))
											.SetFont(CGXFont().SetBold(TRUE))
										);
		//////////////////////////////////////////////////////////////////////////


//		m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_PROCTYPE), (LONG)pDoc->GetProcIndex(pStep->nProcType));		//Step Type
//		m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_TYPE), (LONG)GetStepIndexFromType(pStep->chType));		//Step Type
		switch(int(pStep->chType))
		{
		case PS_STEP_CHARGE:
		case PS_STEP_DISCHARGE:
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_REF_V), pDoc->VUnitTrans(pStep->fVref, FALSE));//pStep->fVref/V_UNIT_FACTOR);
			if(pStep->chMode == PS_MODE_CP)
			{
				m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_REF_P), pDoc->WattUnitTrans(pStep->fIref, FALSE));//pStep->fIref/I_UNIT_FACTOR);
			}
			else if(pStep->chMode == PS_MODE_CR)
			{
				strTemp = TEXT_LANG[78];//"저항(Ω)"
				m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_REF_I), strTemp);
				//mOhm으로 저장된 data를 Ohm으로 표시 
				m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_REF_I), pStep->fIref/1000.0f);
			}
			else
			{
				strTemp.Format(TEXT_LANG[57], GetDocument()->GetCrtUnit());//"전류(%s)"
				m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_REF_I), strTemp);
				m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_REF_I), pDoc->IUnitTrans(pStep->fIref, FALSE));//pStep->fIref/I_UNIT_FACTOR);
			}
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_REF_T), pStep->fTref);
			
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_END_V), pDoc->VUnitTrans(pStep->fEndV, FALSE));
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_END_I), pDoc->IUnitTrans(pStep->fEndI, FALSE));
			
			lSecond = pStep->ulEndTime / 100;
			
			strTime.Format("%d", lSecond/86400);
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_END_DAY), strTime );
			nHour = lSecond % 86400;
			endTime.SetTime(nHour/3600, (nHour%3600)/60, (nHour%3600)%60);
			
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_END_TIME), endTime.Format() );
			
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_END_CAP), pDoc->CUnitTrans(pStep->fEndC, FALSE));
			// DeltaVp == fEndDV
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_END_DELTAVP), pDoc->VUnitTrans(pStep->fEndDV, FALSE));
			
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_END_SOC), pStep->fSocRate);
			
			break;
		
		case PS_STEP_REST:
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_REF_T), pStep->fTref);
			lSecond = pStep->ulEndTime / 100;
			
			strTime.Format("%d", lSecond/86400);
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_END_DAY), strTime );
			nHour = lSecond % 86400;
			endTime.SetTime(nHour/3600, (nHour%3600)/60, (nHour%3600)%60);
			
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_END_TIME), endTime.Format() );
			break;

		case PS_STEP_IMPEDANCE:
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_REF_T), pStep->fTref);
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_REF_V), pDoc->VUnitTrans(pStep->fVref, FALSE));	//pStep->fVref/V_UNIT_FACTOR);
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_REF_I), pDoc->IUnitTrans(pStep->fIref, FALSE));//pStep->fIref/I_UNIT_FACTOR);
			lSecond = pStep->ulEndTime / 100;
			
			strTime.Format("%d", lSecond/86400);
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_END_DAY), strTime );
			nHour = lSecond % 86400;
			endTime.SetTime(nHour/3600, (nHour%3600)/60, (nHour%3600)%60);
			
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_END_TIME), endTime.Format() );
			break;

		case PS_STEP_OCV:
			break;

		case PS_STEP_END:
		default:
			break;
		}
		m_wndStepGrid.SetStyleRange(CGXRange(nStepNum, COL_STEP_END_SOC), CGXStyle()
	         .SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(pDoc->MakeEndString(nStepNum))));
		m_nDisplayStep = nStepNum;
		m_wndStepGrid.LockUpdate(bLock);
		m_wndStepGrid.Redraw();
	}
	else				//Update All Step Data
	{
		BOOL bLock = m_wndStepGrid.LockUpdate(TRUE);
		
		int nDiffRow = nTotStepNum - nRow;
		if(nDiffRow > 0)
		{	
			m_wndStepGrid.InsertRows(nRow+1, nDiffRow);
		}
		else if(nDiffRow <0)
		{
			m_wndStepGrid.RemoveRows(nTotStepNum+1, nRow);
		}

		if(nTotStepNum == 0)
		{
			m_nDisplayStep = 0;
			DisplayStepOption(NULL);	//Reset Control Data

			for(int i = IDC_VTG_HIGH; i<= IDC_DELTA_TIME; i++)
			{
				GetDlgItem(i)->EnableWindow(FALSE);
			}
			SetCompCtrlEnable(FALSE);
		}
		else
		{
			if(m_nDisplayStep == 0 && nTotStepNum > 0)
			{
				m_nDisplayStep = 1;
			}

			for(int i = 0; i<nTotStepNum; i++)
			{
				pStep = (STEP *)pDoc->GetStepData(i+1);
				ASSERT(pStep != NULL);
							
				if((i+1) != (int)pStep->chStepNo)
				{
	//				AfxMessageBox("Step Number Error");
	//				return FALSE;
				}

				SettingStepWndType(i+1, (int)pStep->chType, FALSE);
	//			m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_TYPE), (LONG)(pStep->chType-1));
	//			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_TYPE), (LONG)GetStepIndexFromType(pStep->chType));		//Step Type

				switch(int(pStep->chType))
				{
				case PS_STEP_CHARGE:
				case PS_STEP_DISCHARGE:
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_REF_V), pDoc->VUnitTrans(pStep->fVref, FALSE));//pStep->fVref/V_UNIT_FACTOR);
					if(pStep->chMode == PS_MODE_CP)
					{
						m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_REF_P), pDoc->WattUnitTrans(pStep->fIref, FALSE));//pStep->fIref/I_UNIT_FACTOR);
					}
					else if(pStep->chMode == PS_MODE_CR)
					{
						strTemp = TEXT_LANG[78];//"저항(Ω)"
						m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_REF_I), strTemp);
						//mOhm으로 저장된 data를 Ohm으로 표시 
						m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_REF_I), pStep->fIref/1000.0f);
					}
					else
					{
						strTemp.Format(TEXT_LANG[57], GetDocument()->GetCrtUnit());//"전류(%s)"
						m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_REF_I), strTemp);
						m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_REF_I), pDoc->IUnitTrans(pStep->fIref, FALSE));//pStep->fIref/I_UNIT_FACTOR);
					}
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_REF_T), pStep->fTref);
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_END_V), pDoc->VUnitTrans(pStep->fEndV, FALSE));
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_END_I), pDoc->IUnitTrans(pStep->fEndI, FALSE));
					  
					lSecond = pStep->ulEndTime / 100;
					
					strTime.Format("%d", lSecond/86400);
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_END_DAY), strTime );
					nHour = lSecond % 86400;
					endTime.SetTime(nHour/3600, (nHour%3600)/60, (nHour%3600)%60);
					
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_END_TIME), endTime.Format() );

					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_END_CAP), pDoc->CUnitTrans(pStep->fEndC, FALSE));
					// DeltaVp == fEndDV
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_END_DELTAVP), pDoc->VUnitTrans(pStep->fEndDV, FALSE));
					
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_END_SOC), pStep->fSocRate);
					break;
				
				case PS_STEP_REST:
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_REF_T), pStep->fTref);
					lSecond = pStep->ulEndTime / 100;
					
					strTime.Format("%d", lSecond/86400);
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_END_DAY), strTime );
					nHour = lSecond % 86400;
					endTime.SetTime(nHour/3600, (nHour%3600)/60, (nHour%3600)%60);
					
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_END_TIME), endTime.Format() );
					break;
				case PS_STEP_IMPEDANCE:
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_REF_T), pStep->fTref);
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_REF_V), pDoc->VUnitTrans(pStep->fVref, FALSE));//pStep->fVref/V_UNIT_FACTOR);
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_REF_I), pDoc->IUnitTrans(pStep->fIref, FALSE));//pStep->fIref/I_UNIT_FACTOR);
					
					lSecond = pStep->ulEndTime / 100;
					
					strTime.Format("%d", lSecond/86400);
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_END_DAY), strTime );
					nHour = lSecond % 86400;
					endTime.SetTime(nHour/3600, (nHour%3600)/60, (nHour%3600)%60);
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_END_TIME), endTime.Format() );
					break;
				
				case PS_STEP_OCV:
				case PS_STEP_END:
				default:
					break;
				}
				
				//End Type ToolTips UpDate
				m_wndStepGrid.SetStyleRange(CGXRange(i+1, COL_STEP_END_SOC), CGXStyle()
					 .SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(pDoc->MakeEndString(i+1))));
				
				if(i+1 == m_nDisplayStep)
				{
					//Option Control들을 Step1내용으로 표시 한다.
					DisplayStepOption(pStep);
					m_wndStepGrid.SetCurrentCell(m_nDisplayStep, COL_STEP_PROCTYPE);
				}
			}
		}//End for		
		m_wndStepGrid.LockUpdate(bLock);
		m_wndStepGrid.Redraw();
	}

	strTemp.Format("Step %d", m_nDisplayStep);
	m_strStepNumber.SetText(strTemp);						//Display Step Number

	strTemp.Format("Total : %d", m_wndStepGrid.GetRowCount());
	m_TotalStepCount.SetText(strTemp);
	UpdateData(FALSE);
	m_wndStepGrid.Redraw();
	return TRUE;
}

//Load Test 
BOOL CCTSEditorView::RequeryTestList(LONG lModelID, CString strDefaultName)
{
	int nTotModelNum = 0;
	CTestListRecordSet	recordSet;

	recordSet.m_strFilter.Format("[ModelID] = %ld", lModelID);
	recordSet.m_strSort.Format("[TestNo]");

	try
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}

	ROWCOL nRow = m_wndTestListGrid.GetRowCount();

	if(recordSet.IsBOF())
	{
		nTotModelNum = 0;
	}
	else
	{
		recordSet.MoveLast();
		nTotModelNum = recordSet.GetRecordCount();
		recordSet.MoveFirst(); 
	}

	nTotModelNum -= nRow;

	if(nTotModelNum > 0)
	{	
		m_wndTestListGrid.InsertRows(nRow+1, nTotModelNum);
	}
	else if(nTotModelNum <0)
	{
		m_wndTestListGrid.RemoveRows(nRow+nTotModelNum+1, nRow);
	}

	nRow = 0;
	int nSelRow = 0;
	int nSelID  = 0;
	CString strSelName;

	while(!recordSet.IsEOF())
	{	
		nRow++;
		//if(strDefaultName == recordSet.m_TestName)
		if(strDefaultName == recordSet.m_Description)
		{
			nSelRow = nRow;
			nSelID  = recordSet.m_TestID;
			//strSelName = recordSet.m_TestName;
			strSelName = recordSet.m_Description;
		}
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_PROC_TYPE), (ROWCOL)GetTestIndexFromType(recordSet.m_ProcTypeID));	
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_NAME), recordSet.m_TestName);
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_CREATOR), recordSet.m_Creator);
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_DESCRIPTION), recordSet.m_Description);
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_EDIT_TIME), recordSet.m_ModifiedTime.Format());
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_PRIMARY_KEY), recordSet.m_TestID);
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_RESET_PROC), (long)recordSet.m_PreTestCheck);
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_EDIT_STATE), CS_SAVED);
		recordSet.MoveNext();
	}
	
	if(nSelRow == 0)
	{
		if(nRow >0)	//가장 마지막 Recoedset을 보여 준다.
		{
			recordSet.MovePrev();
			nSelID = recordSet.m_TestID;
			strSelName = recordSet.m_TestName;
			nSelRow = nRow;
		}
	}
	recordSet.Close();
	
	//default 
	UpdateDspTest(strSelName, nSelID);
	m_wndTestListGrid.SetCurrentCell(nSelRow, 1);
	
	CString strTemp;
	strTemp.Format("%d", nRow);
	m_TotTestCount.SetText(strTemp);
	
	return TRUE;
}

//Load Step Data
int CCTSEditorView::RequeryTestStep(LONG lTestID, LONG lModelID)
{
	int nTotStepNum = 0 ;
	CCTSEditorDoc *pDoc = (CCTSEditorDoc *)GetDocument();
	if(pDoc->RequeryPreTestParam(lTestID, lModelID)<0)	return -1;
	return nTotStepNum = pDoc->RequeryStepData(lTestID, lModelID);	//Load Step Data to Memory Array
}

void CCTSEditorView::Fun_LoadProcessSetting(LONG lTestID, LONG lModelID)
{
	// TODO: Add your control notification handler code here
	CString strTemp = _T("");

	CCTSEditorDoc *pDoc = GetDocument();
	pDoc->m_lLoadedProcType = PS_PGS_NOWORK;
	
	if(m_strTypeMaskArray.GetSize() > 0)
	{	
		if( m_strTypeMaskArray.GetSize() > 0 )
		{
			strTemp = m_strTypeMaskArray.GetAt(1);

			//Mask가 지정되어 있지 않으면 모두 실행 가능 
			if(!strTemp.IsEmpty())
			{
				strTemp.MakeUpper();
				//공정 Type(AG, PI, PC, FO, IR,  FC, SL, GD, OV)과 
				//시행가능한 Step Type (CODERIZ) C: Charge, D: Discharge, Z: AC Impedance, R: DC Impedance, O: OCV, I:Rest, E:End) NULL이면 모든 Step type 수행가능
				CString strID = strTemp.Left(2);
				if(strID == PS_PGS_AGING_CODE)		pDoc->m_lLoadedProcType =  PS_PGS_AGING;
				else if(strID == PS_PGS_PREIMPEDANCE_CODE)	pDoc->m_lLoadedProcType =  PS_PGS_PREIMPEDANCE;
				else if(strID == PS_PGS_PRECHARGE_CODE)	pDoc->m_lLoadedProcType =  PS_PGS_PRECHARGE;
				else if(strID == PS_PGS_FORMATION_CODE)	pDoc->m_lLoadedProcType =  PS_PGS_FORMATION;
				else if(strID == PS_PGS_IROCV_CODE)	pDoc->m_lLoadedProcType =  PS_PGS_IROCV;
				else if(strID == PS_PGS_FINALCHARGE_CODE)	pDoc->m_lLoadedProcType =  PS_PGS_FINALCHARGE;
				else if(strID == PS_PGS_SELECTOR_CODE)	pDoc->m_lLoadedProcType =  PS_PGS_SELECTOR;
				else if(strID == PS_PGS_GRADING_CODE)	pDoc->m_lLoadedProcType =  PS_PGS_GRADING;
				else if(strID == PS_PGS_OCV_CODE)	pDoc->m_lLoadedProcType =  PS_PGS_OCV;
			}
		}	
	}
	
	m_lLoadedBatteryModelID = lModelID;
	m_lLoadedTestID = lTestID;

	ReLoadStepData(m_lLoadedTestID, m_lLoadedBatteryModelID);
	GetDlgItem(IDC_STEP_INSERT)->EnableWindow(TRUE);
	GetDlgItem(IDC_STEP_DELETE)->EnableWindow(TRUE);
	//	GetDlgItem(IDC_STEP_SAVE)->EnableWindow(TRUE);

	if(pDoc->m_lLoadedProcType == PS_PGS_PRECHARGE || pDoc->m_lLoadedProcType == PS_PGS_FORMATION ||
		pDoc->m_lLoadedProcType == PS_PGS_FINALCHARGE)
	{
		GetDlgItem(IDC_PRETEST_CHECK)->EnableWindow(TRUE);
	}
	else
	{
		GetDlgItem(IDC_PRETEST_CHECK)->EnableWindow(FALSE);
	}

	m_bStepDataSaved = TRUE;

	//Undo List 삭제 
	GetDocument()->RemoveUndoStep();

	//Data Edit를 감시하는 Timer
	if(m_nTimer == 0)
	{
		m_nTimer = SetTimer(1000, 500, NULL);
	}
}

void CCTSEditorView::OnLoadTest() 
{
	// TODO: Add your control notification handler code here
	CString strTemp;

	CCTSEditorDoc *pDoc = GetDocument();
	if(pDoc->IsEdited() && pDoc->PermissionCheck(PMS_EDITOR_EDIT) != FALSE)
	{
		GetDlgItem(IDC_LOADED_TEST)->GetWindowText(strTemp);
		strTemp += TEXT_LANG[81];//" 시험 조건이 변경 되었습니다. 저장 하시겠습니까?"
		if(IDYES == MessageBox(strTemp, TEXT_LANG[82], MB_ICONQUESTION|MB_YESNO))//"저장"
		{
			if(!StepSave())	return;
		}
	}
	
	ROWCOL nRow, nCol;
	if(!m_wndTestListGrid.GetCurrentCell(nRow, nCol))
	{
		AfxMessageBox(TEXT_LANG[83]);//"시험 목록에서 먼저 선택 하십시요"
		return;
	}
	else
	{
		if(nRow >0)
		{
			m_lDisplayTestID = atoi(m_wndTestListGrid.GetValueRowCol(nRow, COL_TEST_PRIMARY_KEY));

			pDoc->m_lLoadedProcType = PS_PGS_NOWORK;
			if(m_strTypeMaskArray.GetSize() > 0)
			{
				int nID = atol(m_wndTestListGrid.GetValueRowCol(nRow, COL_TEST_PROC_TYPE));
				if(nID >= 0 && nID < m_strTypeMaskArray.GetSize())
				{
					strTemp = m_strTypeMaskArray.GetAt(nID);

					//Mask가 지정되어 있지 않으면 모두 실행 가능 
					if(!strTemp.IsEmpty())
					{
						strTemp.MakeUpper();
						//공정 Type(AG, PI, PC, FO, IR,  FC, SL, GD, OV)과 
						//시행가능한 Step Type (CODERIZ) C: Charge, D: Discharge, Z: AC Impedance, R: DC Impedance, O: OCV, I:Rest, E:End) NULL이면 모든 Step type 수행가능
						CString strID = strTemp.Left(2);
						if(strID == PS_PGS_AGING_CODE)		pDoc->m_lLoadedProcType =  PS_PGS_AGING;
						else if(strID == PS_PGS_PREIMPEDANCE_CODE)	pDoc->m_lLoadedProcType =  PS_PGS_PREIMPEDANCE;
						else if(strID == PS_PGS_PRECHARGE_CODE)	pDoc->m_lLoadedProcType =  PS_PGS_PRECHARGE;
						else if(strID == PS_PGS_FORMATION_CODE)	pDoc->m_lLoadedProcType =  PS_PGS_FORMATION;
						else if(strID == PS_PGS_IROCV_CODE)	pDoc->m_lLoadedProcType =  PS_PGS_IROCV;
						else if(strID == PS_PGS_FINALCHARGE_CODE)	pDoc->m_lLoadedProcType =  PS_PGS_FINALCHARGE;
						else if(strID == PS_PGS_SELECTOR_CODE)	pDoc->m_lLoadedProcType =  PS_PGS_SELECTOR;
						else if(strID == PS_PGS_GRADING_CODE)	pDoc->m_lLoadedProcType =  PS_PGS_GRADING;
						else if(strID == PS_PGS_OCV_CODE)	pDoc->m_lLoadedProcType =  PS_PGS_OCV;
					}
				}	
			}
		}	
		else
			return;
	}
	
	m_lLoadedBatteryModelID = m_lDisplayModelID;
	m_lLoadedTestID = m_lDisplayTestID;
	
	ReLoadStepData(m_lDisplayTestID, m_lLoadedBatteryModelID);
	GetDlgItem(IDC_STEP_INSERT)->EnableWindow(TRUE);
	GetDlgItem(IDC_STEP_DELETE)->EnableWindow(TRUE);
//	GetDlgItem(IDC_STEP_SAVE)->EnableWindow(TRUE);

	if(pDoc->m_lLoadedProcType == PS_PGS_PRECHARGE || pDoc->m_lLoadedProcType == PS_PGS_FORMATION ||
		pDoc->m_lLoadedProcType == PS_PGS_FINALCHARGE)
	{
		GetDlgItem(IDC_PRETEST_CHECK)->EnableWindow(TRUE);		//cycler에서는 Cell 접촉검사 조건을 입력 하지 못하도록 한다.
	}
	else
	{
		GetDlgItem(IDC_PRETEST_CHECK)->EnableWindow(FALSE);		//cycler에서는 Cell 접촉검사 조건을 입력 하지 못하도록 한다.
	}

	m_bStepDataSaved = TRUE;
	
	//Undo List 삭제 
	GetDocument()->RemoveUndoStep();

	//Data Edit를 감시하는 Timer
	if(m_nTimer == 0)
	{
		m_nTimer = SetTimer(1000, 500, NULL);
	}
	// [12/29/2009 kky]
	// 화면 전환
	SendMessage(WM_HSCROLL,MAKEWPARAM(SB_RIGHT,0),0L);
}

//Model 추가
void CCTSEditorView::OnModelNew() 
{
	NewModel();
}

//시험 목록 추가
void CCTSEditorView::OnTestNew() 
{
	if(GetDocument()->PermissionCheck(PMS_EDITOR_EDIT) ==  FALSE)
	{
		AfxMessageBox(TEXT_LANG[84]);//"수정 권한이 없습니다."
		return;
	}
	// TODO: Add your control notification handler code here

	COleDateTime curDate = COleDateTime::GetCurrentTime();

	/////////Dialog에서 새로운 이름을 받아 들이도록 수정
	CTestNameDlg	dlg(m_lDisplayModelID);
	dlg.m_strCreator = GetDocument()->m_LoginData.szLoginID;
	dlg.m_createdDate = curDate;
	if(dlg.DoModal() != IDOK)
		return;

	ROWCOL nRow = m_wndTestListGrid.GetRowCount()+1;
	m_wndTestListGrid.InsertRows(nRow, 1);
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_PROC_TYPE), (long)GetTestIndexFromType(dlg.m_lTestTypeID));	
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_CREATOR), dlg.m_strCreator);	
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_EDIT_TIME), dlg.m_createdDate.Format());
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_EDIT_STATE), CS_NEW);
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_NAME), dlg.m_strName);
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_DESCRIPTION), dlg.m_strDecription);
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_RESET_PROC), (long)dlg.m_bContinueCellCode);

	m_wndTestListGrid.SetCurrentCell(nRow, COL_TEST_PROC_TYPE);

	OnTestSave();
	
}

//Step 추가
void CCTSEditorView::AddStepNew(int nStepNum, BOOL bAuto, STEP *pCopyStep)
{
	// TODO: Add your control notification handler code here
	ROWCOL nRow = m_wndStepGrid.GetRowCount();
	CCTSEditorDoc *pDoc = (CCTSEditorDoc *)GetDocument();

	if(nRow >= SCH_MAX_STEP)
	{
		MessageBox(TEXT_LANG[85], "Step Error", MB_OK|MB_ICONSTOP);//"입력 가능한 Step 수를 초과 하였습니다."
		return;
	}

	if(nStepNum < 1)	nStepNum = 1;

	if(!pDoc->AddNewStep(nStepNum, pCopyStep))
	{
		MessageBox(TEXT_LANG[86], "Step Add Error", MB_OK|MB_ICONSTOP);//"Step을 추가 할수 없습니다."
		return;
	}
//	m_wndStepGrid.InsertRows(nStepNum, 1);
	
	if(bAuto)	m_wndStepGrid.InsertRows(nStepNum+1, 1);		//마자막 삽입일 경우는
	else	m_wndStepGrid.InsertRows(nStepNum, 1);

	m_wndStepGrid.SetCurrentCell(nStepNum, COL_STEP_PROCTYPE);
	m_wndStepGrid.SetStyleRange(CGXRange(nStepNum, COL_STEP_TYPE, nStepNum, COL_STEP_INDEX), CGXStyle().SetEnabled(FALSE));

	STEP *pStep;
	for(int i=0; i<pDoc->GetTotalStepNum(); i++)
	{
		pStep = pDoc->GetStepData(i+1);
		if(pStep == NULL)	break;
	}
}

//Battery Model 삭제
void CCTSEditorView::OnModelDelete() 
{
	// TODO: Add your control notification handler code here
	ModelDelete();

}

//시험 목록 삭제
void CCTSEditorView::OnTestDelete() 
{
	if(GetDocument()->PermissionCheck(PMS_EDITOR_EDIT) ==  FALSE)
	{
		AfxMessageBox(TEXT_LANG[84]);//"수정 권한이 없습니다."
		return;
	}
	// TODO: Add your control notification handler code here
	CString		strTemp;
	int i = 0;
	CTestListRecordSet	recordSet;
	ROWCOL nRow = m_wndTestListGrid.GetRowCount();
	if(nRow <1)		return;				//Test Condition is not exist

	CRowColArray	awRows;
	m_wndTestListGrid.GetSelectedRows(awRows);		//Get Selected Test Condition
	if(awRows.GetSize() <=0)	return;
	
	if(awRows.GetSize() == 1)
	{
		strTemp.Format(TEXT_LANG[87], m_wndTestListGrid.GetValueRowCol(awRows[0], COL_TEST_NAME) );//"%s를 조건을 삭제 하시겠습니까?"
	}
	else
	{
		strTemp = TEXT_LANG[88];//"선택한 조건들을 삭제 하시겠습니까?."
	}

	if(IDNO == AfxMessageBox(strTemp, MB_YESNO|MB_ICONQUESTION))	
		return;

	try									//DataBase Open
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return ;
	}

	for (i = awRows.GetSize()-1; i >=0 ; i--)
	{
		strTemp = m_wndTestListGrid.GetValueRowCol(awRows[i], COL_TEST_PRIMARY_KEY);	//Get TestID
		if(!strTemp.IsEmpty())										//Delete Test Condition Form DB	
		{
			strTemp = "TestID = " + strTemp;
			if(recordSet.Find(AFX_DAO_FIRST, strTemp))
			{
				LONG lDeleteTestID = recordSet.m_TestID;
				DeleteSimulationFileTestData(lDeleteTestID);

				recordSet.Delete();		
				
				if(lDeleteTestID == m_lLoadedTestID)
				{
					ReLoadStepData(lDeleteTestID, lDeleteTestID);
					ClearStepState();
				}
			}
		}
		m_wndTestListGrid.RemoveRows(awRows[i], awRows[i]);			//Delete Test Condition Row
	}

	//순서 저장 2002/10/02
	for( i = 0; i<m_wndTestListGrid.GetRowCount(); i++)
	{
		strTemp = m_wndTestListGrid.GetValueRowCol(i+1, COL_TEST_PRIMARY_KEY);	//Get TestID
		if(!strTemp.IsEmpty())										//Delete Test Condition Form DB	
		{
			strTemp = "TestID = " + strTemp;
			if(recordSet.Find(AFX_DAO_FIRST, strTemp))
			{
				recordSet.Edit();
				recordSet.m_TestNo = atol(m_wndTestListGrid.GetValueRowCol(i+1, COL_TEST_NO));
				recordSet.Update();
			}
		}
	}

	recordSet.Close();
	RequeryTestList(m_lDisplayModelID);			//Re Display Test Condition
	GetDocument()->m_bEditedFlag = TRUE;		//DataBase 변경 Flag (BackUp을 위해)
	
}

//Step Delete
void CCTSEditorView::OnStepDelete() 
{
	// TODO: Add your control notification handler code here
	CRowColArray	awRows;
	m_wndStepGrid.GetSelectedRows(awRows);
	int nSelNo = awRows.GetSize();

	if(nSelNo < 1)	return;

	AddUndoStep();
	DeleteStep(awRows);

// 	for(int i = 1 ; i <= GetDocument()->GetTotalStepNum(); i++)
// 	{
// 		UpdateStepGrid(i);		
// 	}

	DisplayStepEndStringAll();
}

//Battery Model Save
void CCTSEditorView::OnModelSave() 
{
	// TODO: Add your control notification handler code here
	ModelSave();
}

//시험 목록 저장
void CCTSEditorView::OnTestSave() 
{
	if(GetDocument()->PermissionCheck(PMS_EDITOR_EDIT) ==  FALSE)
	{
		AfxMessageBox(TEXT_LANG[84]);//"수정 권한이 없습니다."
		return;
	}
	// TODO: Add your control notification handler code here
	CString		strTemp, strState;
	CTestListRecordSet	recordSet;
	ROWCOL nCol, nRow = m_wndTestListGrid.GetRowCount();
	
	if(nRow <1)		
		return;


	ROWCOL i = 0;
	int nLastEditedModel =0;

	try
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return ;
	}

	for( i = 0; i< nRow; i++)
	{
		strState = m_wndTestListGrid.GetValueRowCol(i+1, COL_TEST_EDIT_STATE);
//		if(strState == CS_SAVED)	continue;					//Saved Test Name		//2002 10/2 순서 저장을 위해 모두 재저장 

		strTemp = m_wndTestListGrid.GetValueRowCol(i+1, COL_TEST_NAME);
		if(strTemp.IsEmpty())	//Test Name is Empty
		{
			strTemp.Format(TEXT_LANG[89], i+1);//"목록 %d번에 공정 조건명이 입력되지 않아서 저장되지 않습니다."
			MessageBox(strTemp, TEXT_LANG[90], MB_OK|MB_ICONSTOP);//"공정명 Error"
			continue;
		}

/*		strTemp = m_wndTestListGrid.GetValueRowCol(i+1, 5);
		if(strTemp.IsEmpty())
		{
			strTemp = "TestName = '" + strTemp +"'";
			if(recordSet.Find(AFX_DAO_FIRST, strTemp))	//Test Name Duplication
			{
				MessageBox("입력된 시험 조건명이 이미 존재 합니다.", "Test Name Error", MB_OK|MB_ICONSTOP);
				m_wndTestListGrid.SetCurrentCell(i+1, 1);
				recordSet.Close();
				return;
			}
*/		if(strState == CS_NEW)			
		{
			recordSet.AddNew();
		}
		else
		{
			strTemp = m_wndTestListGrid.GetValueRowCol(i+1, COL_TEST_PRIMARY_KEY);
			if(strTemp.IsEmpty())
			{
				recordSet.Close();
				TRACE("Test Primary Key Empty");
				return;
			}
			strTemp.Format("TestID = %ld", atol((LPCSTR)(LPCTSTR)strTemp));
			if(recordSet.Find(AFX_DAO_FIRST, strTemp))	//Find Model Name Data
			{
				recordSet.Edit();
			}
			else
			{
				recordSet.Close();
				TRACE("Test Search Error");
				return;
			}
		}

		recordSet.m_TestNo		= atol(m_wndTestListGrid.GetValueRowCol(i+1, COL_TEST_NO)); 
		recordSet.m_ModelID		= m_lDisplayModelID;		//////초기화 문제
		recordSet.m_ProcTypeID = m_pTestTypeCombo->GetItemData(atol(m_wndTestListGrid.GetValueRowCol(i+1, COL_TEST_PROC_TYPE)));
		recordSet.m_TestName	= m_wndTestListGrid.GetValueRowCol(i+1, COL_TEST_NAME);
		recordSet.m_Creator		= m_wndTestListGrid.GetValueRowCol(i+1, COL_TEST_CREATOR);
		recordSet.m_Description = m_wndTestListGrid.GetValueRowCol(i+1, COL_TEST_DESCRIPTION);
		recordSet.m_ModifiedTime.ParseDateTime(m_wndTestListGrid.GetValueRowCol(i+1, COL_TEST_EDIT_TIME));
		recordSet.m_PreTestCheck = atol(m_wndTestListGrid.GetValueRowCol(i+1, COL_TEST_RESET_PROC));
		recordSet.Update();
		nLastEditedModel = i+1;
	}
	

	//Update 이후 DB를 재검색하여 List를 Display 한다.

	recordSet.m_strFilter.Format("[ModelID] = %ld", m_lDisplayModelID);
	recordSet.m_strSort.Format("[TestNo]");

	i = 0;
	recordSet.Requery();
	if(!recordSet.IsBOF())
	{
		recordSet.MoveLast();
		i = recordSet.GetRecordCount();
		recordSet.MoveFirst();
	}
	nRow -= i;
	
	if(nRow>0)
	{
		m_wndTestListGrid.RemoveRows(i+1, i+nRow);		//Row가 RecordSet  보다 많으면 잉여 Row를 Delete
	}
	else if(nRow<0)
	{
		m_wndTestListGrid.InsertRows(i+1, -nRow);		//Row가 RescoedSet 보다 작으면 부족 Row Insert
	}
	
	nRow = 0;
	while(!recordSet.IsEOF())
	{	
		nRow++;
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_PROC_TYPE), (ROWCOL)GetTestIndexFromType(recordSet.m_ProcTypeID));
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_NAME), recordSet.m_TestName);
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_CREATOR), recordSet.m_Creator);
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_DESCRIPTION), recordSet.m_Description);
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_EDIT_TIME), recordSet.m_ModifiedTime.Format());
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_PRIMARY_KEY), recordSet.m_TestID);
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_EDIT_STATE), CS_SAVED);
	
		if(nRow == nLastEditedModel)				//Last Edited Battery Model is current selected Model
		{
			UpdateDspTest(recordSet.m_TestName,  recordSet.m_TestID);
			m_wndTestListGrid.SetCurrentCell(nRow, COL_TEST_PROC_TYPE);
		}
		recordSet.MoveNext();
	}
	
	recordSet.Close();
	strTemp.Format("%d", nRow);
	m_TotTestCount.SetText(strTemp);
	
	if(m_wndTestListGrid.GetCurrentCell(nRow, nCol))
	{
		if(nRow>0)
			GetDlgItem(IDC_LOAD_TEST)->EnableWindow(TRUE);
	}
	GetDlgItem(IDC_TEST_SAVE)->EnableWindow(FALSE);		
	GetDocument()->m_bEditedFlag = TRUE;					//DataBase 변경 Flag (BackUp을 위해)

}

//Step Save
void CCTSEditorView::OnStepSave() 
{
	// TODO: Add your control notification handler code here
	if(GetDocument()->PermissionCheck(PMS_EDITOR_EDIT) ==  FALSE)
	{
		AfxMessageBox(TEXT_LANG[84]);//"수정 권한이 없습니다."
		return;
	}

	StepSave();
	
	GetDocument()->m_bEditedFlag = TRUE;		//DataBase 변경 Flag (BackUp을 위해)

//	GetDlgItem(IDC_STEP_SAVE)->EnableWindow(FALSE);
}

void CCTSEditorView::OnStepInsert() 
{
	// TODO: Add your control notification handler code here
	if(GetDocument()->PermissionCheck(PMS_EDITOR_EDIT) ==  FALSE)
	{
		AfxMessageBox(TEXT_LANG[84]);//"수정 권한이 없습니다."
		return;
	}

	ROWCOL nRow =0, nCol;
	
	if(m_wndStepGrid.GetRowCount() >0)
	{
		if(!m_wndStepGrid.GetCurrentCell(nRow, nCol))
		{
			AfxMessageBox(TEXT_LANG[91]);//"추가할 Step을 먼저 지정 하십시요"
			return;
		}
	}
	
	AddUndoStep();
	
	AddStepNew(nRow, FALSE);	
	
	//reset Grid
	for ( int n = 1; n < m_wndGradeGrid.GetRowCount(); n++ )
	{
		m_wndGradeGrid.SetValueRange(CGXRange(n, _COL_ITEM1_), "");
		m_wndGradeGrid.SetValueRange(CGXRange(n, _COL_ITEM2_), "");
		m_wndGradeGrid.SetValueRange(CGXRange(n, _COL_GRADE_RELATION_), "");
		m_wndGradeGrid.SetValueRange(CGXRange(n, _COL_GRADE_MIN1_), "");
		m_wndGradeGrid.SetValueRange(CGXRange(n, _COL_GRADE_MAX1_), "");
		m_wndGradeGrid.SetValueRange(CGXRange(n, _COL_GRADE_MIN2_), "");
		m_wndGradeGrid.SetValueRange(CGXRange(n, _COL_GRADE_MAX2_), "");
		m_wndGradeGrid.SetValueRange(CGXRange(n, _COL_GRADE_CODE_), "");
	}

//	UpdateStepGrid();
// 	for(int i = 1 ; i <= GetDocument()->GetTotalStepNum(); i++)
// 	{
// 		UpdateStepGrid(i);		
// 	}

	DisplayStepEndStringAll();
	// TODO: Add your control notification handler code here
}

//Step Grid의 입력값을 받아 들인다. 
BOOL CCTSEditorView::UpdateStepGrid( int nStepNum )
{
//	static int nPrevStepNum = 0;
//	if(nPrevStepNum == nStepNum && nStepNum>0)	return TRUE;		//Update Display Does not Require

	//Display Step Data	
	CCTSEditorDoc *pDoc = (CCTSEditorDoc *)GetDocument();		//Get Document point
	int nTotStepNum = pDoc->GetTotalStepNum();						//Get Total Step No.
	ROWCOL nRow = m_wndStepGrid.GetRowCount();						//Get Inputed Step No.
	ASSERT(nRow == nTotStepNum);
	ASSERT(nStepNum <= nTotStepNum);
	CString strTemp;
	STEP *pStep;
//	char chTemp;
	UpdateData(TRUE);

	if(nStepNum > 0)	//UpDate 1 Step Grid
	{
		TRACE("UpDate 1 Step Grid\n");
		pStep = (STEP *)pDoc->GetStepData(nStepNum);
		ASSERT(pStep != NULL);
		double	dwValue =0.0;
		pStep->chStepNo = (char)atoi(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_NO));		// Step No.
		
		strTemp = m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_PROCTYPE);					//Step Type	
		if(!strTemp.IsEmpty())
//			pStep->nProcType = pDoc->GetProcID(atoi(strTemp));
			pStep->nProcType = m_pProcTypeCombo->GetItemData(atoi(strTemp));

		strTemp = m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_TYPE);						//Step Type	
		if(!strTemp.IsEmpty())
//			pStep->chType = GetStepTypeFromIndex((char)(atoi(strTemp)));
			pStep->chType = m_pStepTypeCombo->GetItemData(atoi(strTemp));
			
		strTemp = m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_MODE);
		if(!strTemp.IsEmpty() )//&& pStep->chType != PS_STEP_PATTERN)
			pStep->chMode = m_pStepModeCombo->GetItemData(atoi(strTemp));
		pStep->fVref = (float)(pDoc->VUnitTrans(atof(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_REF_V))));	//	*V_UNIT_FACTOR);
		
		if(pStep->chMode == PS_MODE_CC && pStep->fEndV)
		{
			if(pStep->chType == PS_STEP_CHARGE)
			{
				pStep->fVref = pStep->fEndV + GetDocument()->m_lMaxVoltage * 0.01;		//1% Margin
				if(pStep->fVref > GetDocument()->m_lMaxVoltage)	pStep->fVref = GetDocument()->m_lMaxVoltage;
			}
			else if(pStep->chType == PS_STEP_DISCHARGE)
			{
				if( pStep->fEndV > pDoc->m_nCCRefVoltage )
				{
					pStep->fVref = pStep->fEndV - pDoc->m_nCCRefVoltage;
					strTemp.Format("%f", pDoc->VUnitTrans(pStep->fVref, false));
					m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_REF_V), strTemp);
				}
				else
				{
					pStep->fVref = 0;
					strTemp.Format("%f", pDoc->VUnitTrans(pStep->fVref, false));
					m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_REF_V), strTemp);
				}
			}
		}
		/*
		if( pStep->chType == PS_STEP_DISCHARGE )
		{
			if( pStep->chMode == PS_MODE_CC )
			{
				if( pStep->fEndV > 200 )
				{
					pStep->fVref = pStep->fEndV - 200;
					strTemp.Format("%f", pDoc->VUnitTrans(pStep->fVref, false));
					m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_REF_V), strTemp);
				}
				else
				{
					pStep->fVref = 1;
					strTemp.Format("%f", pDoc->VUnitTrans(pStep->fVref, false));
					m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_REF_V), strTemp);
				}
			}
		}
		
		if( pStep->chType == PS_STEP_CHARGE )
		{
			if( pStep->chMode == PS_MODE_CC )
			{
				pStep->fVref = pStep->fEndV + 200;
				strTemp.Format("%f", pDoc->VUnitTrans(pStep->fVref, false));
				m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_REF_V), strTemp);				
			}			
		}
		*/
		
		if(pStep->chMode == PS_MODE_CR)
		{
			//Ohm단위로 입력 받아서 mOhm으로 저장한다.
			pStep->fIref = (float)(atof(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_REF_I))*1000.0f);	//	*I_UNIT_FACTOR);
		}
		else if(pStep->chMode == PS_MODE_CP)
		{
			pStep->fIref = (float)(pDoc->WattUnitTrans(atof(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_REF_I))));	//	*I_UNIT_FACTOR);
		}
		else
		{
			pStep->fIref = (float)(pDoc->IUnitTrans(atof(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_REF_I))));	//	*I_UNIT_FACTOR);
		}
		
		pStep->fTref = (float)atof(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_REF_T));	//	*I_UNIT_FACTOR);

		pStep->fEndV = pDoc->VUnitTrans(atof(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_END_V)));
		pStep->fEndI = pDoc->IUnitTrans(atof(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_END_I)));
		pStep->fEndC = pDoc->CUnitTrans(atof(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_END_CAP)));
		pStep->fEndDV = pDoc->VUnitTrans(atof(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_END_DELTAVP)));
		pStep->fEndTemp = (float)atof(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_END_TEMP));
		
		pStep->fSocRate = (float)atof(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_END_SOC));
		if( pStep->fSocRate < 0 )
		{
			pStep->fSocRate = 0;
			strTemp.Format("%.1f", pStep->fSocRate);
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_END_SOC), strTemp);
		}
		else if( pStep->fSocRate > 100 )
		{		
			pStep->fSocRate = 100;
			strTemp.Format("%.1f", pStep->fSocRate);
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_END_SOC), strTemp);
		}

				
		COleDateTime endTime;
		endTime.ParseDateTime( m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_END_TIME) );
		pStep->ulEndTime = (endTime.GetHour()*3600+endTime.GetMinute()*60+endTime.GetSecond() +
			atoi(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_END_DAY)) * 86400)*100;

		m_VtgHigh.GetValue(dwValue);	pStep->fVLimitHigh = (float)pDoc->VUnitTrans(dwValue);		//(dwValue*V_UNIT_FACTOR);
		m_VtgLow.GetValue(dwValue); 	pStep->fVLimitLow= (float)pDoc->VUnitTrans(dwValue);		//(dwValue*V_UNIT_FACTOR);
		m_CrtHigh.GetValue(dwValue);	pStep->fILimitHigh = (float)pDoc->IUnitTrans(dwValue);		//(dwValue*I_UNIT_FACTOR);
		m_CrtLow.GetValue(dwValue);		pStep->fILimitLow = (float)pDoc->IUnitTrans(dwValue);		//(dwValue*I_UNIT_FACTOR);
		m_CapHigh.GetValue(dwValue);	pStep->fCLimitHigh = (float)pDoc->CUnitTrans(dwValue);		//(dwValue*C_UNIT_FACTOR);
		m_CapLow.GetValue(dwValue);		pStep->fCLimitLow = (float)pDoc->CUnitTrans(dwValue);		//(dwValue*C_UNIT_FACTOR);
						
		m_DCIR_RegTemp.GetValue(dwValue);		pStep->fDCIR_RegTemp = dwValue * 10.0f;
		m_DCIR_ResistanceRate.GetValue(dwValue);	pStep->fDCIR_ResistanceRate = dwValue * 100.0f;
						
		m_ImpHigh.GetValue(dwValue);	pStep->fImpLimitHigh = (float)(dwValue*Z_UNIT_FACTOR);
//		m_ImpLow.GetValue(dwValue);		pStep->fImpLimitLow = (float)(dwValue*Z_UNIT_FACTOR);
		m_ImpLow.GetValue(dwValue);		pStep->fImpLimitLow = (float)(dwValue); //ljb 대기 휴지 for samsung
		m_DeltaI.GetValue(dwValue);		pStep->fDeltaI = (float)pDoc->IUnitTrans(dwValue);			//(dwValue*I_UNIT_FACTOR);
		m_DeltaV.GetValue(dwValue);		pStep->fDeltaV = (float)pDoc->VUnitTrans(dwValue);			//(dwValue*V_UNIT_FACTOR);
		m_TempHigh.GetValue(dwValue);	pStep->fTempHigh = (float)(dwValue);			
		m_TempLow.GetValue(dwValue);	pStep->fTempLow = (float)(dwValue);			

		//End 전압/전류 비교 
		m_VtgEndHigh.GetValue(dwValue);	pStep->fVEndHigh = (float)pDoc->VUnitTrans(dwValue);		//(dwValue*V_UNIT_FACTOR);
		m_VtgEndLow.GetValue(dwValue); 	pStep->fVEndLow = (float)pDoc->VUnitTrans(dwValue);			//(dwValue*V_UNIT_FACTOR);
		m_CrtEndHigh.GetValue(dwValue);	pStep->fIEndHigh = (float)pDoc->IUnitTrans(dwValue);		//(dwValue*I_UNIT_FACTOR);
		m_CrtEndLow.GetValue(dwValue);	pStep->fIEndLow = (float)pDoc->IUnitTrans(dwValue);			//(dwValue*I_UNIT_FACTOR);

		//EDLC 용량 측정 전압 V1, V2
		m_CapVtgLow.GetValue(dwValue);	pStep->fCapaVoltage1 = (float)pDoc->VUnitTrans(dwValue);	//(dwValue*V_UNIT_FACTOR);
		m_CapVtgHigh.GetValue(dwValue);	pStep->fCapaVoltage2 = (float)pDoc->VUnitTrans(dwValue);	//(dwValue*V_UNIT_FACTOR);

		m_ReportV.GetValue(dwValue);	pStep->fReportV = (float)pDoc->VUnitTrans(dwValue);			//(dwValue*V_UNIT_FACTOR);
		m_ReportI.GetValue(dwValue); 	pStep->fReportI= (float)pDoc->IUnitTrans(dwValue);			//(dwValue*I_UNIT_FACTOR);
		m_ReportTemp.GetValue(dwValue); 	pStep->fReportTemp= (float)(dwValue);
		
		COleDateTime time;
		m_ReportTime.GetTime(time);
		pStep->ulReportTime = (time.GetHour()*3600+time.GetMinute()*60+time.GetSecond())*100;		//저장 간격 설정 시간 
		GetDlgItem(IDC_REPORT_TIME_MILI)->GetWindowText(strTemp);
		pStep->ulReportTime += atol(strTemp)/10;
		
		m_VIGetTime.GetValue(dwValue); pStep->ulReportVIGetTime = dwValue * (60*100);
		
		m_DeltaTime.GetTime(time);
		pStep->lDeltaTime = (time.GetHour()*3600+time.GetMinute()*60+time.GetSecond())*100;

		m_CompChgCcVtg.GetValue(dwValue);	pStep->fCompChgCcVtg = (float)pDoc->VUnitTrans(dwValue);
		m_CompChgCcTime.GetValue(dwValue); pStep->ulCompChgCcTime = dwValue * 100;		
		m_CompChgCcDeltaVtg.GetValue(dwValue);	pStep->fCompChgCcDeltaVtg = (float)pDoc->VUnitTrans(dwValue);
		m_CompChgCvCrt.GetValue(dwValue);	pStep->fCompChgCvCrt = (float)pDoc->IUnitTrans(dwValue);
		m_CompChgCvtTime.GetValue(dwValue); pStep->ulCompChgCvtTime = dwValue * 100;
		m_CompChgCvDeltaCrt.GetValue(dwValue);	pStep->fCompChgCvDeltaCrt = (float)pDoc->IUnitTrans(dwValue);
		
		for(int i =0; i<SCH_MAX_COMP_POINT; i++)
		{
			m_CompV[i].GetValue(dwValue);
			pStep->fCompVLow[i] = (float)pDoc->VUnitTrans(dwValue);			//(dwValue*V_UNIT_FACTOR);		//전압 하한값 
			
			m_CompV1[i].GetValue(dwValue);		
			pStep->fCompVHigh[i] = (float)pDoc->VUnitTrans(dwValue);		//(dwValue*V_UNIT_FACTOR);		//전압 상한값
			
			m_CompI[i].GetValue(dwValue);
			pStep->fCompILow[i] = (float)pDoc->IUnitTrans(dwValue);			//(dwValue*I_UNIT_FACTOR);		//전류 하한값
			
			m_CompI1[i].GetValue(dwValue);
			pStep->fCompIHigh[i] = (float)pDoc->IUnitTrans(dwValue);		//(dwValue*I_UNIT_FACTOR);		//전류 상한값
			
			m_CompTimeV[i].GetValue(dwValue);
			pStep->ulCompTimeV[i] = dwValue * (60*100);
			
			time = m_CompTimeI[i].GetDateTime();
			pStep->ulCompTimeI[i] = (time.GetHour()*3600+time.GetMinute()*60+time.GetSecond())*100;	//전류 시간 

			//전압 비교 시간이 설정되어 있지 않으며 전압 입력 상하한 값을 무시한다.
			if(pStep->ulCompTimeV[i] == 0)
			{
				pStep->fCompVLow[i] = 0.0f;
				pStep->fCompVHigh[i] = 0.0f;
			}

			/*
			//전류 비교 시간이 설정되어 있지 않으며 전류 입력 상하한 값을 무시한다.
			if(pStep->ulCompTimeI[i] == 0)
			{
				pStep->fCompILow[i] = 0.0f;
				pStep->fCompIHigh[i] = 0.0f;
			}
			
			//전압상한 하한이 설정되지 않았을 경우 시간 입력을 무시한다.
			if(pStep->fCompVLow[i] == 0.0f && pStep->fCompVHigh[i] == 0.0f)
			{
				pStep->ulCompTimeV[i] = 0;
			}
			*/

			//전류상한 하한이 설정되지 않았을 경우 시간 입력을 무시한다.
			if(pStep->fCompILow[i] == 0.0f && pStep->fCompIHigh[i] == 0.0f)
			{
				pStep->ulCompTimeI[i] = 0;
			}
		}
		
		UpdateStepGrade(nStepNum);
	}
	else	//Update All Step Data
	{
		for(int i = 0; i<nTotStepNum; i++)
		{
			pStep = (STEP *)pDoc->GetStepData(i+1);
			if(!pStep)	return FALSE;
			
			TRACE("Update All Step Data\n");
			
			pStep->chStepNo = (char)atoi(m_wndStepGrid.GetValueRowCol(i+1, COL_STEP_NO));
			pStep->chType = (char)atoi(m_wndStepGrid.GetValueRowCol(i+1, COL_STEP_TYPE));
			pStep->chMode = (char)atoi(m_wndStepGrid.GetValueRowCol(i+1, COL_STEP_MODE));
			pStep->fVref = (float)atof(m_wndStepGrid.GetValueRowCol(i+1, COL_STEP_REF_V));
			
			if(pStep->chMode == PS_MODE_CR)
			{
				//Ohm단위로 입력 받아서 mOhm으로 저장한다.
				pStep->fIref = (float)(atof(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_REF_I))*1000.0f);	//	*I_UNIT_FACTOR);
			}
			else if(pStep->chMode == PS_MODE_CP)
			{
				pStep->fIref = (float)(pDoc->WattUnitTrans(atof(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_REF_P))));	//	*I_UNIT_FACTOR);
			}
			else
			{
				pStep->fIref = (float)(pDoc->IUnitTrans(atof(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_REF_I))));	//	*I_UNIT_FACTOR);
			}
			
			pStep->fEndV = pDoc->VUnitTrans(atof(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_END_V)));
			pStep->fEndI = pDoc->IUnitTrans(atof(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_END_I)));
			pStep->fEndC = pDoc->CUnitTrans(atof(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_END_CAP)));
			pStep->fEndDV = pDoc->VUnitTrans(atof(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_END_DELTAVP)));
			pStep->fEndTemp = (float)atof(m_wndStepGrid.GetValueRowCol(i+1, COL_STEP_END_TEMP));
			pStep->fSocRate = (float)atof(m_wndStepGrid.GetValueRowCol(i+1, COL_STEP_END_SOC));
			
			COleDateTime endTime;
			endTime.ParseDateTime( m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_END_TIME) );
			pStep->ulEndTime = (endTime.GetHour()*3600+endTime.GetMinute()*60+endTime.GetSecond() +
			atoi(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_END_DAY)) * 86400)*100;
		}
	}
	return TRUE;
}

//Step의 Grade 값을 Grade Grid에 Display 한다.
int CCTSEditorView::DisplayStepGrade(STEP *pStep)
{
	//reset Grid
	int n;
	for ( n = 1; n <= m_wndGradeGrid.GetRowCount(); n++ )
	{
 		m_wndGradeGrid.SetValueRange(CGXRange(n, _COL_ITEM1_), "");
 		m_wndGradeGrid.SetValueRange(CGXRange(n, _COL_ITEM2_), "");
 		m_wndGradeGrid.SetValueRange(CGXRange(n, _COL_GRADE_RELATION_), "");
		m_wndGradeGrid.SetValueRange(CGXRange(n, _COL_GRADE_MIN1_), "");
		m_wndGradeGrid.SetValueRange(CGXRange(n, _COL_GRADE_MAX1_), "");
		m_wndGradeGrid.SetValueRange(CGXRange(n, _COL_GRADE_MIN2_), "");
		m_wndGradeGrid.SetValueRange(CGXRange(n, _COL_GRADE_MAX2_), "");
		m_wndGradeGrid.SetValueRange(CGXRange(n, _COL_GRADE_CODE_), "");
	}

	if(pStep == NULL)	//0으로 표시
	{
		m_GradeCheck.SetCheck(FALSE);
		return 0;
	}

	//Grad에 보여 주는 단위 설정
	char str[32], str2[7], str3[5];

	GRADE sGrade;
	ZeroMemory(&sGrade,sizeof(GRADE));
	memcpy(&sGrade, &pStep->sGrading_Val, sizeof(GRADE));	
	float fVal1, fVal2;
	CString		strCode;
	int iGradCnt=0;
	if (sGrade.iGradeCount == 2)
		iGradCnt = (int)sGrade.chTotalGrade / 2;

	for(int i =0; i<(int)sGrade.chTotalGrade; i++)
	{
		strCode.Format("%C", sGrade.aszGradeCode[i]);
		if (i < iGradCnt)
		{
			//항목 표시
			m_wndGradeGrid.SetStyleRange(CGXRange(i+1,_COL_ITEM1_),
				CGXStyle().SetValue(sGrade.lGradeItem[i]));
			m_wndGradeGrid.SetValueRange(CGXRange(i+1, _COL_GRADE_CODE_), strCode);
			//항목 1
			switch((int)pStep->chType)
			{
			case PS_STEP_CHARGE:
			case PS_STEP_DISCHARGE:
				switch (int(sGrade.lGradeItem[i]))
				{
				case PS_GRADE_VOLTAGE:
					fVal1 = GetDocument()->VUnitTrans(sGrade.faValue1[i], FALSE);
					fVal2 = GetDocument()->VUnitTrans(sGrade.faValue2[i], FALSE);
					sprintf(str, "%%.%dlf", GetDocument()->GetVtgUnitFloat());	//소숫점 이하
					sprintf(str3, "%d", GetDocument()->GetVtgUnitFloat());		//소숫점 이하
					
					if(GetDocument()->GetVtgUnit() == "V")
						sprintf(str2, "%d", 3);		//정수부
					else
						sprintf(str2, "%d", 6);		//정수부
					break;
				case PS_GRADE_CAPACITY:
					fVal1 = GetDocument()->CUnitTrans(sGrade.faValue1[i], FALSE);
					fVal2 = GetDocument()->CUnitTrans(sGrade.faValue2[i], FALSE);
					sprintf(str, "%%.%dlf", GetDocument()->GetCapUnitFloat());	//소숫점 이하
					sprintf(str3, "%d", GetDocument()->GetCapUnitFloat());		//소숫점 이하
					
					if(GetDocument()->GetCapUnit() == "C")
						sprintf(str2, "%d", 3);		//정수부
					else
						sprintf(str2, "%d", 6);		//정수부
					break;
				case PS_GRADE_CURRENT:
					fVal1 = GetDocument()->IUnitTrans(sGrade.faValue1[i], FALSE);
					fVal2 = GetDocument()->IUnitTrans(sGrade.faValue2[i], FALSE);
					sprintf(str, "%%.%dlf", GetDocument()->GetCrtUnitFloat());	//소숫점 이하
					sprintf(str3, "%d", GetDocument()->GetCrtUnitFloat());		//소숫점 이하
					
					if(GetDocument()->GetCrtUnit() == "A")
						sprintf(str2, "%d", 3);		//정수부
					else
						sprintf(str2, "%d", 6);		//정수부
					break;
				case PS_GRADE_TIME:
					fVal1 = sGrade.faValue1[i]/T_UNIT_FACTOR;
					fVal2 = sGrade.faValue2[i]/T_UNIT_FACTOR;
					sprintf(str, "%%.%dlf", 1);	//소숫점 이하
					sprintf(str3, "%d", 1);		//소숫점 이하
					sprintf(str2, "%d", 6);		//정수부
					break;
				default:
					fVal1 = sGrade.faValue1[i];
					fVal2 = sGrade.faValue2[i];
					sprintf(str, "%%.%dlf", 3);	//소숫점 이하
					sprintf(str3, "%d", 3);		//소숫점 이하
					sprintf(str2, "%d", 3);		//정수부
					break;
				}
				break;
			case PS_STEP_IMPEDANCE:
				fVal1 = (double)sGrade.faValue1[i]/Z_UNIT_FACTOR;
				fVal2 = (double)sGrade.faValue2[i]/Z_UNIT_FACTOR;
				sprintf(str, "%%.%dlf", 3);	//소숫점 이하
				sprintf(str3, "%d", 3);		//소숫점 이하
				sprintf(str2, "%d", 3);		//정수부
				break;
			case PS_STEP_OCV:
				fVal1 = GetDocument()->VUnitTrans(sGrade.faValue1[i], FALSE);
				fVal2 = GetDocument()->VUnitTrans(sGrade.faValue2[i], FALSE);
				sprintf(str, "%%.%dlf", GetDocument()->GetVtgUnitFloat());	//소숫점 이하
				sprintf(str3, "%d", GetDocument()->GetVtgUnitFloat());		//소숫점 이하
				
				if(GetDocument()->GetVtgUnit() == "V")
					sprintf(str2, "%d", 3);		//정수부
				else
					sprintf(str2, "%d", 6);		//정수부
				break;
			case PS_STEP_REST:
			case PS_STEP_END:
			default:
				fVal1 = 0.0f;
				fVal2 = 0.0f;
				sprintf(str, "%%.%dlf", 3);	//소숫점 이하
				sprintf(str3, "%d", 3);		//소숫점 이하
				sprintf(str2, "%d", 3);		//정수부
				break;
			}
			m_wndGradeGrid.SetStyleRange(CGXRange().SetCells(i+1, _COL_GRADE_MIN1_,
				i+1, _COL_GRADE_MAX1_),
				CGXStyle()
				.SetControl(IDS_CTRL_REALEDIT)
				.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
				.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
				.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
			);
			m_wndGradeGrid.SetValueRange(CGXRange(i+1, _COL_GRADE_MIN1_), fVal1);
			m_wndGradeGrid.SetValueRange(CGXRange(i+1, _COL_GRADE_MAX1_), fVal2);
		}
		else
		{
			m_wndGradeGrid.SetStyleRange(CGXRange(i-iGradCnt+1,_COL_ITEM2_),
				CGXStyle().SetValue(sGrade.lGradeItem[i]));
			if (strCode == '0')
			{
				m_wndGradeGrid.SetStyleRange(CGXRange(i-iGradCnt+1,_COL_GRADE_RELATION_),
					CGXStyle().SetValue(0L));
			}
			if (strCode == '&')		// AND
				m_wndGradeGrid.SetStyleRange(CGXRange(i-iGradCnt+1,_COL_GRADE_RELATION_),
				CGXStyle().SetValue(1L));
			if (strCode == '+')		// OR
				m_wndGradeGrid.SetStyleRange(CGXRange(i-iGradCnt+1,_COL_GRADE_RELATION_),
				CGXStyle().SetValue(2L));
			switch((int)pStep->chType)
			{
			case PS_STEP_CHARGE:
			case PS_STEP_DISCHARGE:
				//항목2
				switch (int(sGrade.lGradeItem[i]))
				{
				case PS_GRADE_VOLTAGE:
					fVal1 = GetDocument()->VUnitTrans(sGrade.faValue1[i], FALSE);
					fVal2 = GetDocument()->VUnitTrans(sGrade.faValue2[i], FALSE);
					sprintf(str, "%%.%dlf", GetDocument()->GetVtgUnitFloat());	//소숫점 이하
					sprintf(str3, "%d", GetDocument()->GetVtgUnitFloat());		//소숫점 이하
					
					if(GetDocument()->GetVtgUnit() == "V")
						sprintf(str2, "%d", 3);		//정수부
					else
						sprintf(str2, "%d", 6);		//정수부
					break;
				case PS_GRADE_CAPACITY:
					fVal1 = GetDocument()->CUnitTrans(sGrade.faValue1[i], FALSE);
					fVal2 = GetDocument()->CUnitTrans(sGrade.faValue2[i], FALSE);
					sprintf(str, "%%.%dlf", GetDocument()->GetCapUnitFloat());	//소숫점 이하
					sprintf(str3, "%d", GetDocument()->GetCapUnitFloat());		//소숫점 이하
					
					if(GetDocument()->GetCapUnit() == "Ah")
						sprintf(str2, "%d", 3);		//정수부
					else
						sprintf(str2, "%d", 6);		//정수부
					break;
				case PS_GRADE_CURRENT:
					fVal1 = GetDocument()->IUnitTrans(sGrade.faValue1[i], FALSE);
					fVal2 = GetDocument()->IUnitTrans(sGrade.faValue2[i], FALSE);
					sprintf(str, "%%.%dlf", GetDocument()->GetCrtUnitFloat());	//소숫점 이하
					sprintf(str3, "%d", GetDocument()->GetCrtUnitFloat());		//소숫점 이하
					
					if(GetDocument()->GetCrtUnit() == "A")
						sprintf(str2, "%d", 3);		//정수부
					else
						sprintf(str2, "%d", 6);		//정수부
					break;
				case PS_GRADE_TIME:
					fVal1 = sGrade.faValue1[i];
					fVal2 = sGrade.faValue2[i];
					sprintf(str, "%%.%dlf", 3);	//소숫점 이하
					sprintf(str3, "%d", 3);		//소숫점 이하
					sprintf(str2, "%d", 3);		//정수부
					break;
				default:
					fVal1 = sGrade.faValue1[i];
					fVal2 = sGrade.faValue2[i];
					sprintf(str, "%%.%dlf", 3);	//소숫점 이하
					sprintf(str3, "%d", 3);		//소숫점 이하
					sprintf(str2, "%d", 3);		//정수부
					break;
				}
				break;
		case PS_STEP_IMPEDANCE:
		case PS_STEP_OCV:
		case PS_STEP_REST:
		case PS_STEP_END:
		default:
			fVal1 = 0.0f;
			fVal2 = 0.0f;
			sprintf(str, "%%.%dlf", 3);	//소숫점 이하
			sprintf(str3, "%d", 3);		//소숫점 이하
			sprintf(str2, "%d", 3);		//정수부
			break;
		}
		m_wndGradeGrid.SetStyleRange(CGXRange().SetCells(i-iGradCnt+1, _COL_GRADE_MIN2_,
			i+1, _COL_GRADE_MAX2_),
			CGXStyle()
			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
			);
		m_wndGradeGrid.SetStyleRange(CGXRange(i-iGradCnt+1,_COL_ITEM2_),
			CGXStyle().SetValue(sGrade.lGradeItem[i]));
		m_wndGradeGrid.SetValueRange(CGXRange(i-iGradCnt+1, _COL_GRADE_MIN2_), fVal1);
		m_wndGradeGrid.SetValueRange(CGXRange(i-iGradCnt+1, _COL_GRADE_MAX2_), fVal2);
		}
	}


	if((int)pStep->chType == PS_STEP_END || 
	   (int)pStep->chType == PS_STEP_REST ||
	   (int)pStep->chType == PS_STEP_LONG_REST ||
	   (int)pStep->chType == PS_STEP_LOOP ||
	   (int)pStep->chType == PS_STEP_ADV_CYCLE)
		//|| (int)pStep->chType == PS_STEP_SELECTING || (int)pStep->chType == PS_STEP_GRADING))
	{
//		m_GradeCheck.SetCheck(FALSE);
		m_GradeCheck.EnableWindow(FALSE);
		m_wndGradeGrid.SetStyleRange(CGXRange(1, 1, GetDocument()->m_nGradingStepLimit, _COL_GRADE_CODE_), CGXStyle().SetEnabled(FALSE));
	}
	else
	{
//		m_GradeCheck.EnableWindow(TRUE);
		m_GradeCheck.SetCheck(pStep->bGrade);
		m_wndGradeGrid.SetStyleRange(CGXRange(1, 1, GetDocument()->m_nGradingStepLimit, _COL_GRADE_CODE_), CGXStyle().SetEnabled(pStep->bGrade));
	}

	// 2008-11-20 ljb		Item1, Item2 비활성화
	//GRAY 설정
	if ( (int)pStep->chType == PS_STEP_OCV )
	{
		m_wndGradeGrid.SetStyleRange(CGXRange(1, _COL_ITEM1_,
			GetDocument()->m_nGradingStepLimit, _COL_ITEM1_), CGXStyle().SetValue((double)PS_GRADE_VOLTAGE));
		m_wndGradeGrid.SetStyleRange(CGXRange(1, _COL_ITEM1_,
			GetDocument()->m_nGradingStepLimit, _COL_ITEM1_), CGXStyle().SetEnabled(FALSE));
		m_wndGradeGrid.SetStyleRange(CGXRange(1, _COL_GRADE_RELATION_,
			GetDocument()->m_nGradingStepLimit, _COL_GRADE_MAX2_), CGXStyle().SetEnabled(FALSE));
	}
	else if ((int)pStep->chType == PS_STEP_IMPEDANCE)
	{
		m_wndGradeGrid.SetStyleRange(CGXRange(1, _COL_ITEM1_,
			GetDocument()->m_nGradingStepLimit, _COL_ITEM1_), CGXStyle().SetValue((double)PS_GRADE_IMPEDANCE));
		m_wndGradeGrid.SetStyleRange(CGXRange(1, _COL_ITEM1_,
			GetDocument()->m_nGradingStepLimit, _COL_ITEM1_), CGXStyle().SetEnabled(FALSE));
		m_wndGradeGrid.SetStyleRange(CGXRange(1, _COL_GRADE_RELATION_,
			GetDocument()->m_nGradingStepLimit, _COL_GRADE_MAX2_), CGXStyle().SetEnabled(FALSE));
	}
UpdateData(FALSE);	

	return 0;
}

//Grade Grid의 값을 주어진 Grade Data로 Update한다. 
int CCTSEditorView::UpdateStepGrade(int nStepNum)
{
	CCTSEditorDoc *pDoc = (CCTSEditorDoc *)GetDocument();		//Get Document point

	int nTotStepNum = pDoc->GetTotalStepNum();
	if(nTotStepNum <= 0)	return -1;
	if(nStepNum < 1 || nStepNum > nTotStepNum)		return -1;
	
	STEP *pStep;
		pStep = (STEP *)pDoc->GetStepData(nStepNum);
	if(!pStep)	return -1;
		
	CString	strItem1,strItem2;
	CString strTemp, strTemp1, strTemp2, strTemp3, strTemp4, strTemp5,strGrade;
	int iGradeCnt = 0;
	int iItem, nStepType, i;
	pStep->bGrade =  m_GradeCheck.GetCheck();
	if ( pStep->nProcType == 0 )
		nStepType = pStep->chType;
	else
		nStepType = HIWORD(pStep->nProcType);

	TRACE("%d",m_wndGradeGrid.GetRowCount());
	//1번째 항목
	for( i =0; i<m_wndGradeGrid.GetRowCount(); i++)
	{
		strTemp1 = strTemp2 = "";
		strTemp1 = m_wndGradeGrid.GetValueRowCol(i+1, _COL_GRADE_MIN1_);		//Min Val
		strTemp2 = m_wndGradeGrid.GetValueRowCol(i+1, _COL_GRADE_MAX1_);		//Max Val
		if (strTemp1.IsEmpty() || strTemp2.IsEmpty())
		{
			iGradeCnt=i;
			break;
		}
		pStep->sGrading_Val.faValue1[i] = (float)atof(strTemp1);
		pStep->sGrading_Val.faValue2[i] = (float)atof(strTemp2);
		strGrade = m_wndGradeGrid.GetValueRowCol(i+1, _COL_GRADE_CODE_);		//Grade Code
		if(strGrade.IsEmpty())
		{
			strGrade.Format("%c", 'A'+i);
		}
		pStep->sGrading_Val.aszGradeCode[i] =  strGrade.GetAt(0);

		switch (nStepType)
		{
		case PS_STEP_CHARGE :
		case PS_STEP_DISCHARGE :
			iItem = atoi(m_wndGradeGrid.GetValueRowCol(i+1, _COL_ITEM1_));
			pStep->sGrading_Val.lGradeItem[i] = iItem;
			switch (iItem)
			{
			case PS_GRADE_VOLTAGE:
				pStep->sGrading_Val.faValue1[i] = pDoc->VUnitTrans(pStep->sGrading_Val.faValue1[i]);	//*= V_UNIT_FACTOR;
				pStep->sGrading_Val.faValue2[i] = pDoc->VUnitTrans(pStep->sGrading_Val.faValue2[i]);	//*= V_UNIT_FACTOR;
				break;
			case PS_GRADE_CAPACITY:
				pStep->sGrading_Val.faValue1[i] = pDoc->CUnitTrans(pStep->sGrading_Val.faValue1[i]);	//*= V_UNIT_FACTOR;
				pStep->sGrading_Val.faValue2[i] = pDoc->CUnitTrans(pStep->sGrading_Val.faValue2[i]);	//*= V_UNIT_FACTOR;
				break;
			case PS_GRADE_CURRENT:
				pStep->sGrading_Val.faValue1[i] = pDoc->IUnitTrans(pStep->sGrading_Val.faValue1[i]);	//*= V_UNIT_FACTOR;
				pStep->sGrading_Val.faValue2[i] = pDoc->IUnitTrans(pStep->sGrading_Val.faValue2[i]);	//*= V_UNIT_FACTOR;
				break;
			case PS_GRADE_TIME:
				pStep->sGrading_Val.faValue1[i] *= T_UNIT_FACTOR;
				pStep->sGrading_Val.faValue2[i] *= T_UNIT_FACTOR;
				break;
			default:
				pStep->sGrading_Val.faValue1[i] *= Z_UNIT_FACTOR;
				pStep->sGrading_Val.faValue2[i] *= Z_UNIT_FACTOR;
				break;
			}
			break;
		case PS_STEP_IMPEDANCE :
			pStep->sGrading_Val.lGradeItem[i] = PS_GRADE_IMPEDANCE;
			pStep->sGrading_Val.faValue1[i] *= Z_UNIT_FACTOR;
			pStep->sGrading_Val.faValue2[i] *= Z_UNIT_FACTOR;
			break;
		case PS_STEP_OCV :
			pStep->sGrading_Val.lGradeItem[i] = PS_GRADE_VOLTAGE;
			pStep->sGrading_Val.faValue1[i] = pDoc->VUnitTrans(pStep->sGrading_Val.faValue1[i]);	//*= V_UNIT_FACTOR;
			pStep->sGrading_Val.faValue2[i] = pDoc->VUnitTrans(pStep->sGrading_Val.faValue2[i]);	//*= V_UNIT_FACTOR;
			break;
		default :
			pStep->sGrading_Val.faValue1[i]=0;
			pStep->sGrading_Val.faValue2[i]=0;
			pStep->sGrading_Val.lGradeItem[i] = 0;
			break;
		}
	}
	//2번째 항목
	for( i =0; i<iGradeCnt; i++)
	{
		strTemp1 = strTemp2 = "";
		strTemp1 = m_wndGradeGrid.GetValueRowCol(i+1, _COL_GRADE_MIN2_);		//Min Val
		strTemp2 = m_wndGradeGrid.GetValueRowCol(i+1, _COL_GRADE_MAX2_);		//Max Val
		pStep->sGrading_Val.faValue1[iGradeCnt+i] = (float)atof(strTemp1);
		pStep->sGrading_Val.faValue2[iGradeCnt+i] = (float)atof(strTemp2);

		switch (nStepType)
		{
		case PS_STEP_CHARGE :
		case PS_STEP_DISCHARGE :
			iItem = atoi(m_wndGradeGrid.GetValueRowCol(i+1, _COL_ITEM2_));
			pStep->sGrading_Val.lGradeItem[iGradeCnt+i] = iItem;
			switch (iItem)
			{
			case PS_GRADE_VOLTAGE:
				pStep->sGrading_Val.faValue1[iGradeCnt+i] = pDoc->VUnitTrans(pStep->sGrading_Val.faValue1[iGradeCnt+i]);	//*= V_UNIT_FACTOR;
				pStep->sGrading_Val.faValue2[iGradeCnt+i] = pDoc->VUnitTrans(pStep->sGrading_Val.faValue2[iGradeCnt+i]);	//*= V_UNIT_FACTOR;
				break;
			case PS_GRADE_CAPACITY:
				pStep->sGrading_Val.faValue1[iGradeCnt+i] = pDoc->CUnitTrans(pStep->sGrading_Val.faValue1[iGradeCnt+i]);	//*= V_UNIT_FACTOR;
				pStep->sGrading_Val.faValue2[iGradeCnt+i] = pDoc->CUnitTrans(pStep->sGrading_Val.faValue2[iGradeCnt+i]);	//*= V_UNIT_FACTOR;
				break;
			case PS_GRADE_CURRENT:
				pStep->sGrading_Val.faValue1[iGradeCnt+i] = pDoc->IUnitTrans(pStep->sGrading_Val.faValue1[iGradeCnt+i]);	//*= V_UNIT_FACTOR;
				pStep->sGrading_Val.faValue2[iGradeCnt+i] = pDoc->IUnitTrans(pStep->sGrading_Val.faValue2[iGradeCnt+i]);	//*= V_UNIT_FACTOR;
				break;
			case PS_GRADE_TIME:
				pStep->sGrading_Val.faValue1[iGradeCnt+i] *= Z_UNIT_FACTOR;
				pStep->sGrading_Val.faValue2[iGradeCnt+i] *= Z_UNIT_FACTOR;
				break;
			default:
				pStep->sGrading_Val.faValue1[iGradeCnt+i] *= Z_UNIT_FACTOR;
				pStep->sGrading_Val.faValue2[iGradeCnt+i] *= Z_UNIT_FACTOR;
				break;
			}
			break;
		case PS_STEP_IMPEDANCE :
			pStep->sGrading_Val.lGradeItem[iGradeCnt+i] = PS_GRADE_IMPEDANCE;
			pStep->sGrading_Val.faValue1[iGradeCnt+i] *= Z_UNIT_FACTOR;
			pStep->sGrading_Val.faValue2[iGradeCnt+i] *= Z_UNIT_FACTOR;
			break;
		case PS_STEP_OCV :
			pStep->sGrading_Val.lGradeItem[iGradeCnt+i] = PS_GRADE_VOLTAGE;
			pStep->sGrading_Val.faValue1[iGradeCnt+i] = pDoc->VUnitTrans(pStep->sGrading_Val.faValue1[i]);	//*= V_UNIT_FACTOR;
			pStep->sGrading_Val.faValue2[iGradeCnt+i] = pDoc->VUnitTrans(pStep->sGrading_Val.faValue2[i]);	//*= V_UNIT_FACTOR;
			break;
		default :
			pStep->sGrading_Val.faValue1[iGradeCnt+i]=0;
			pStep->sGrading_Val.faValue2[iGradeCnt+i]=0;
			pStep->sGrading_Val.lGradeItem[iGradeCnt+i] = 0;
			break;
		}
		strGrade="";
		strGrade = m_wndGradeGrid.GetValueRowCol(i+1, _COL_GRADE_RELATION_);		//Grade Code
		if(strGrade.IsEmpty())
		{
			strGrade.Format("%c", '0');
		}
		if(strGrade == "0")
		{
			strGrade.Format("%c", '0');
		}
		if(strGrade == "1")
		{
			strGrade.Format("%c", '&');
		}
		if(strGrade == "2")
		{
			strGrade.Format("%c", '+');
		}		
		pStep->sGrading_Val.aszGradeCode[iGradeCnt+i] =  strGrade.GetAt(0);
		pStep->sGrading_Val.iGradeCount=2;

	}

//		if(strTemp2.GetLength() >SCH_MAX_CODE_SIZE)	//Grade Code 길이 검사 
//		{
//			strTemp1 = strTemp2.Left(SCH_MAX_CODE_SIZE);
//			strTemp2 = strTemp1;
//		}
//		strcpy(pStep->sGrading_Val[0].aszGradeCode[nTotGrade], (LPCSTR)strTemp2);

	//Grading이 설정 되었으나 등급이 입력되어 있지 않은 경우 
	if(iGradeCnt < 1)
	{
		pStep->bGrade =  FALSE;
		m_GradeCheck.SetCheck(pStep->bGrade);
	}
	else
		pStep->sGrading_Val.chTotalGrade = iGradeCnt*2;

	return iGradeCnt*2;
}

//Grade Check 에 따라 Grade Grid의 상태 변경
void CCTSEditorView::OnGradeCheck() 
{
	// TODO: Add your control notification handler code here
//	CString strStepNum;
//	GetDlgItem(IDC_STEP_NUM)->GetWindowText(strStepNum);
//	if(strStepNum.IsEmpty())	return;
	
//	pStep = (STEP *)GetDocument()->GetStepData(atoi(strStepNum));
	if(m_nDisplayStep < 0 || m_nDisplayStep >= GetDocument()->GetTotalStepNum())	return;
	STEP *pStep;
	pStep = (STEP *)GetDocument()->GetStepData(m_nDisplayStep);
	
	if(!pStep)	return;
	pStep->bGrade =  m_GradeCheck.GetCheck();

	switch((int)pStep->chType)
	{
	case PS_STEP_CHARGE:
	case PS_STEP_DISCHARGE:
// 		if (pStep->bGrade)		// 2008-11-20	ljb		Item1,Item2 비활성화
// 		{
// 			m_wndGradeGrid.SetStyleRange(CGXRange(2, _COL_ITEM1_, GetDocument()->m_nGradingStepLimit, _COL_ITEM1_), CGXStyle().SetEnabled(FALSE));
// 			m_wndGradeGrid.SetStyleRange(CGXRange(2, _COL_ITEM2_, GetDocument()->m_nGradingStepLimit, _COL_ITEM2_), CGXStyle().SetEnabled(FALSE));
// 		}
		m_wndGradeGrid.SetStyleRange(CGXRange(1, 1, GetDocument()->m_nGradingStepLimit, _COL_GRADE_CODE_), CGXStyle().SetEnabled(pStep->bGrade));
		break;
	case PS_STEP_IMPEDANCE:
	case PS_STEP_OCV:
		m_wndGradeGrid.SetStyleRange(CGXRange(1, 1, GetDocument()->m_nGradingStepLimit, _COL_GRADE_CODE_), CGXStyle().SetEnabled(pStep->bGrade));
 		// 2008-11-20	ljb		_COL_GRADE_RELATION_ ~ _COL_GRADE_MAX2_ 비활성화
		m_wndGradeGrid.SetStyleRange(CGXRange(1,_COL_ITEM1_, GetDocument()->m_nGradingStepLimit, _COL_ITEM1_), CGXStyle().SetEnabled(FALSE));
		m_wndGradeGrid.SetStyleRange(CGXRange(1, _COL_GRADE_RELATION_, GetDocument()->m_nGradingStepLimit, _COL_GRADE_MAX2_), CGXStyle().SetEnabled(FALSE));
		break;
//	case PS_STEP_PATTERN:
	case PS_STEP_LOOP:
	case PS_STEP_ADV_CYCLE:
	case PS_STEP_REST:
	case PS_STEP_END:
	default:
		m_GradeCheck.SetCheck(FALSE);
		return;
	}
	

//  kjh 수정 시작 ---------
// 	if(bExGrade)
// 		m_wndGradeGrid.SetStyleRange(CGXRange(1, 1, GetDocument()->m_nGradingStepLimit+1, _COL_GRADE_CODE_), CGXStyle().SetEnabled(pStep->bGrade));
// 	else
// 	{
// 	{
// 		m_wndGradeGrid.SetStyleRange(CGXRange(1, 1, GetDocument()->m_nGradingStepLimit+1, _COL_GRADE_CODE_), CGXStyle().SetEnabled(FALSE));
// 		m_wndGradeGrid.SetStyleRange(CGXRange(1, 1, GetDocument()->m_nGradingStepLimit+1, _COL_GRADE_MAX1_), CGXStyle().SetEnabled(pStep->bGrade));
// 		m_wndGradeGrid.SetStyleRange(CGXRange(1, _COL_GRADE_CODE_, GetDocument()->m_nGradingStepLimit+1, _COL_GRADE_CODE_), CGXStyle().SetEnabled(pStep->bGrade));
// 	}
//	m_wndGradeGrid.SetStyleRange(CGXRange(1, 1, EP_MAX_GRADE_STEP, 3), CGXStyle().SetEnabled(pStep->bGrade));
//  kjh 수정 종료 ---------
	m_bStepDataSaved = FALSE;		//Step Data is Edited
}

//PreTest  Check에 따라 입력 Control의 Enable/Disable
void CCTSEditorView::OnPretestCheck() 
{
	// TODO: Add your control notification handler code here
	TEST_PARAM *pParam;
	pParam = (TEST_PARAM *)(&GetDocument()->m_sPreTestParam);

	pParam->bPreTest = m_PreTestCheck.GetCheck();
	if(pParam->bPreTest)
	{
		pParam->fOCVLimitVal = GetDocument()->m_lMaxVoltage;
	}
	else
	{
		pParam->fOCVLimitVal = 0;
	}

/*	for(int i = IDC_PRETEST_MAXV; i<= IDC_PRETEST_FAULT_NO; i++)
	{
		GetDlgItem(i)->EnableWindow(pParam->bPreTest);
	}
*/
	GetDlgItem(IDC_PRETEST_OCV)->EnableWindow(pParam->bPreTest);
	GetDlgItem(IDC_PRETEST_TRCKLE_I)->EnableWindow(pParam->bPreTest);
	GetDlgItem(IDC_CELL_CHECK_TIME_EDIT)->EnableWindow(pParam->bPreTest);	

	GetDlgItem(IDC_CONTACK_UPPER_V)->EnableWindow(pParam->bPreTest);
	GetDlgItem(IDC_CONTACK_LOWER_V)->EnableWindow(pParam->bPreTest);
	//20210223 KSJ
	GetDlgItem(IDC_CONTACK_UPPER_I)->EnableWindow(pParam->bPreTest);	
	GetDlgItem(IDC_CONTACK_LOWER_I)->EnableWindow(pParam->bPreTest);
	GetDlgItem(IDC_CONTACK_LIMIT_V)->EnableWindow(pParam->bPreTest);
	GetDlgItem(IDC_CONTACT_DELTA_LIMIT_V)->EnableWindow(pParam->bPreTest);

//	GetDlgItem(IDC_PRETEST_FAULT_NO)->EnableWindow(pParam->bPreTest);
	GetDlgItem(IDC_CONTACK_RESISTANCE)->EnableWindow(pParam->bPreTest);


	//CellCheck 방식 변경에 의해 이하 정보는 입력하지 않아도 됨 
	GetDlgItem(IDC_PRETEST_MAXV)->EnableWindow(FALSE);
	GetDlgItem(IDC_PRETEST_MINV)->EnableWindow(FALSE);
	GetDlgItem(IDC_PRETEST_DELTA_V)->EnableWindow(FALSE);	
	GetDlgItem(IDC_PRETEST_MAXI)->EnableWindow(FALSE);

	m_bStepDataSaved = FALSE;		//Step Data is Edited

	DisplayPreTestParam();
}

//PreTest Parameter의 값을 Display한다.
int CCTSEditorView::DisplayPreTestParam()
{
	TEST_PARAM *pParam;
	CCTSEditorDoc *pDoc = GetDocument();
	pParam = (TEST_PARAM *)(&pDoc->m_sPreTestParam);
	
	m_PreContactUpperV.SetValue(double(pDoc->VUnitTrans(pParam->fMaxVoltage, FALSE)));
	m_PreContactLowerV.SetValue(double(pDoc->VUnitTrans(pParam->fMinVoltage, FALSE)));
	//20210223 KSJ
	m_PreContactLimitV.SetValue(double(pDoc->VUnitTrans(pParam->fOCVLimitVal, FALSE)));
	m_PreContactUpperI.SetValue(double(pDoc->IUnitTrans(pParam->fDeltaVoltage, FALSE)));
	m_PreContactLowerI.SetValue(double(pDoc->IUnitTrans(pParam->fMaxFaultNo, FALSE)));
	m_PreContactDeltaLimitV.SetValue(double(pDoc->VUnitTrans(pParam->fDeltaVoltageLimit, FALSE)));

	// m_PreMaxV.SetValue(double(pDoc->VUnitTrans(pParam->fMaxVoltage, FALSE)));	//pParam->fMaxVoltage/V_UNIT_FACTOR));
	// m_PreMinV.SetValue(double(pDoc->VUnitTrans(pParam->fMinVoltage, FALSE)));	//pParam->fMinVoltage/V_UNIT_FACTOR));
	m_PreOCV.SetValue(double(pDoc->VUnitTrans(pParam->fOCVLimitVal, FALSE)));		//pParam->fOCVLimitVal/V_UNIT_FACTOR));		//V Ref
	// m_PreDeltaV.SetValue(double(pDoc->VUnitTrans(pParam->fDeltaVoltage, FALSE)));	//pParam->fDeltaVoltage/V_UNIT_FACTOR));
	m_PreResistance.SetValue(double(pParam->fDeltaVoltage/1000));	//pParam->fDeltaVoltage/V_UNIT_FACTOR));
	m_PreMaxI.SetValue(double(pDoc->IUnitTrans(pParam->fMaxCurrent, FALSE)));		//pParam->fMaxCurrent/I_UNIT_FACTOR));
	
	//I reference
	m_TrickleI.SetValue(double(pDoc->IUnitTrans(pParam->fTrickleCurrent, FALSE)));	//pParam->fTrickleCurrent/I_UNIT_FACTOR));
	m_PreReverseV.SetValue(double(pDoc->VUnitTrans(pParam->fMaxFaultNo, FALSE)));	//pParam->fDeltaVoltage/V_UNIT_FACTOR));

	CString strTemp;
	//Check time
	strTemp.Format("%d", pParam->lTrickleTime/100);
	GetDlgItem(IDC_CELL_CHECK_TIME_EDIT)->SetWindowText(strTemp);	
	
	m_PreTestCheck.SetCheck(pParam->bPreTest);

	//Safety capacity
	
	
	//Formation
#ifdef _FORMATION_
////////////
	GetDlgItem(IDC_CONTACK_UPPER_V)->EnableWindow(pParam->bPreTest);
	GetDlgItem(IDC_CONTACK_LOWER_V)->EnableWindow(pParam->bPreTest);
	//20210223 KSJ
	GetDlgItem(IDC_CONTACK_UPPER_I)->EnableWindow(pParam->bPreTest);	
	GetDlgItem(IDC_CONTACK_LOWER_I)->EnableWindow(pParam->bPreTest);
	GetDlgItem(IDC_CONTACK_LIMIT_V)->EnableWindow(pParam->bPreTest);
	GetDlgItem(IDC_CONTACT_DELTA_LIMIT_V)->EnableWindow(pParam->bPreTest);

	GetDlgItem(IDC_PRETEST_OCV)->EnableWindow(pParam->bPreTest);
	GetDlgItem(IDC_PRETEST_TRCKLE_I)->EnableWindow(pParam->bPreTest);
	GetDlgItem(IDC_CELL_CHECK_TIME_EDIT)->EnableWindow(pParam->bPreTest);
//	GetDlgItem(IDC_PRETEST_FAULT_NO)->EnableWindow(pParam->bPreTest);
	GetDlgItem(IDC_CONTACK_RESISTANCE)->EnableWindow(pParam->bPreTest);

	//CellCheck 방식 변경에 의해 이하 정보는 입력하지 않아도 됨 
	GetDlgItem(IDC_PRETEST_MAXV)->EnableWindow(FALSE);
	GetDlgItem(IDC_PRETEST_MINV)->EnableWindow(FALSE);
	GetDlgItem(IDC_PRETEST_DELTA_V)->EnableWindow(FALSE);	
	GetDlgItem(IDC_PRETEST_MAXI)->EnableWindow(FALSE);
/////////////////	
#else
	//Temperature limit
	GetDlgItem(IDC_PRETEST_DELTA_V)->EnableWindow(m_bUseTemp);
#endif
	return 0;
}

//PreTest Parameter의 값을 받아 들인다.
int CCTSEditorView::UpdatePreTestParam()
{
	TEST_PARAM *pParam;
	CCTSEditorDoc *pDoc = GetDocument();
	pParam = (TEST_PARAM*)(&pDoc->m_sPreTestParam);

	double	dwValue =0.0;
	//not use
	m_PreContactUpperV.GetValue(dwValue); pParam->fMaxVoltage = (float)pDoc->VUnitTrans(dwValue);
	m_PreContactLowerV.GetValue(dwValue); pParam->fMinVoltage = (float)pDoc->VUnitTrans(dwValue);
	//20210223 KSJ
	m_PreContactLimitV.GetValue(dwValue); pParam->fOCVLimitVal = (float)pDoc->VUnitTrans(dwValue);
	m_PreContactUpperI.GetValue(dwValue); pParam->fDeltaVoltage = (float)pDoc->IUnitTrans(dwValue);
	m_PreContactLowerI.GetValue(dwValue); pParam->fMaxFaultNo = (float)pDoc->IUnitTrans(dwValue);
	m_PreContactDeltaLimitV.GetValue(dwValue); pParam->fDeltaVoltageLimit = (float)pDoc->VUnitTrans(dwValue);

	// m_PreMaxV.GetValue(dwValue);		pParam->fMaxVoltage = (float)pDoc->VUnitTrans(dwValue);		// dwValue*V_UNIT_FACTOR);
	// m_PreMinV.GetValue(dwValue);		pParam->fMinVoltage = (float)pDoc->VUnitTrans(dwValue);		//(dwValue*V_UNIT_FACTOR);
	m_PreOCV.GetValue(dwValue);			pParam->fOCVLimitVal = (float)pDoc->VUnitTrans(dwValue);	//(dwValue*V_UNIT_FACTOR);
	// m_PreDeltaV.GetValue(dwValue);		pParam->fDeltaVoltage = (float)pDoc->VUnitTrans(dwValue);	//(dwValue*V_UNIT_FACTOR);
	m_PreResistance.GetValue(dwValue);		pParam->fDeltaVoltage = dwValue * 1000.0f;	//(dwValue*V_UNIT_FACTOR);
	m_PreMaxI.GetValue(dwValue);		pParam->fMaxCurrent = (float)pDoc->IUnitTrans(dwValue);		//(dwValue*I_UNIT_FACTOR);
	//I reference
	m_TrickleI.GetValue(dwValue);		pParam->fTrickleCurrent = (float)pDoc->IUnitTrans(dwValue);	//(dwValue*I_UNIT_FACTOR);	
	m_PreReverseV.GetValue(dwValue);		pParam->fMaxFaultNo = (float)pDoc->VUnitTrans(dwValue);	//(dwValue*V_UNIT_FACTOR);
		
	//Check time
	CString strTemp;
	GetDlgItem(IDC_CELL_CHECK_TIME_EDIT)->GetWindowText(strTemp);
	pParam->lTrickleTime = atoi(strTemp)*100;

	//not use
// 	GetDlgItem(IDC_PRETEST_FAULT_NO)->GetWindowText(strTemp);
// 	pParam->nMaxFaultNo = atoi(strTemp);
// 	pParam->bPreTest = m_PreTestCheck.GetCheck();
	return 0;
}

//Grid 입력 내용의 구별(Primary Key가 존재 하면 Edit, 존재 하지 않으면 New)
LRESULT CCTSEditorView::OnStartEditing(WPARAM wParam, LPARAM lParam)
{
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;				//Get Grid	
	ROWCOL nRow, nCol;			
	nCol = wParam & 0x0000ffff;		nRow = wParam >> 16;	//Get Current Row Col
	
	if(nRow < 1 || nCol < 1)	
	{
		return 0;
	}

	CString strTemp;

	if(pGrid == (CMyGridWnd *)m_pWndCurModelGrid)
	{
		strTemp  = pGrid->GetValueRowCol(nRow, 3);
		if(!strTemp.IsEmpty())	//if Primary Key is Empty -> New Model
		{						
			m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_EDIT_STATE), CS_EDIT);
		}
		GetDlgItem(IDC_MODEL_SAVE)->EnableWindow(TRUE);
	}
	if(pGrid == (CMyGridWnd *)&m_wndTestListGrid)
	{
		strTemp  = pGrid->GetValueRowCol(nRow, COL_TEST_PRIMARY_KEY);
		if(!strTemp.IsEmpty())	//if Primary Key is Empty -> New Model
		{						
			m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_EDIT_STATE), CS_EDIT);
		}
		GetDlgItem(IDC_TEST_SAVE)->EnableWindow(TRUE);
	}
	if(pGrid == (CMyGridWnd *)&m_wndStepGrid || pGrid == (CMyGridWnd *)&m_wndGradeGrid)
	{
		m_bStepDataSaved = FALSE;	//Step Data is Edited
	}

	return 1;
}

LRESULT CCTSEditorView::OnEndEditing(WPARAM wParam, LPARAM lParam)
{
/*	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;		//Get Grid	
	ROWCOL nRow, nCol;			
	nCol = LOWORD(wParam);  
	nRow = HIWORD(wParam);				//Get Current Row Col
	if(pGrid != &m_wndStepGrid)	return 0;
	if(nCol != COL_STEP_REF_V && nCol != COL_STEP_REF_I)	return 0;

	CCTSEditorDoc *pDoc = GetDocument();
	ASSERT(pDoc);

	STEP *pStep = pDoc->GetStepData(nRow);
	if(pStep == NULL)	return 0;

	if(pStep->chType == PS_STEP_CHARGE || pStep->chType == PS_STEP_DISCHARGE)
	{
		if(m_bVHigh)
		{w
			pStep->fVLimitHigh = pStep->fVref + m_nVHighVal;
//			m_VtgHigh.SetValue((double)(pStep->fVref + m_nVHighVal));
		}
		if(m_bVLow)
		{
			pStep->fVLimitLow = m_nVLowVal;
//			m_VtgLow.SetValue((double)m_nVLowVal);
		}
		if(m_bIHigh)
		{
			m_CrtHigh.SetValue((double)(pStep->fIref + m_nIHighVal));
		}
		if(m_bILow)
		{
			m_CrtLow.SetValue((double)m_nILowVal);
		}
		if(m_bCHigh)
		{
			m_CapHigh.SetValue((double)m_nCHighVal);
		}
		if(m_bCLow)
		{
			m_CapLow.SetValue((double)m_nCLowVal);
		}
	}
	else if(pStep->chType == PS_STEP_IMPEDANCE && pStep->chMode == EP_MODE_DC_IMP)
	{
		if(m_bVHigh)
		{
			m_VtgHigh.SetValue((double)(pStep->fVref + m_nVHighVal));
		}
		if(m_bVLow)
		{
			m_VtgLow.SetValue((double)m_nVLowVal);
		}
		if(m_bIHigh)
		{
			m_CrtHigh.SetValue((double)(pStep->fIref + m_nIHighVal));
		}
		if(m_bILow)
		{
			m_CrtLow.SetValue((double)m_nILowVal);
		}
	}
		 
*/	return 1;
}

//Step Data를 DataBase에서 Load해서 Display 한다.
BOOL CCTSEditorView::ReLoadStepData(LONG lTestID, LONG lModelID)
{
	int nStepNum;
	nStepNum = RequeryTestStep(m_lDisplayTestID, lModelID);

	if(nStepNum < 0)
	{
		AfxMessageBox(TEXT_LANG[92]);//"Step Data를 Load 하는데 실패 하였습니다."
		return FALSE;
	}
	else
	{

		if(!DisplayStepGrid())
		{
			AfxMessageBox("Step Data Display Error");
		}
		
		//기본적으로 1번 Step Data를 표기 한다.
		if(nStepNum > 0)
			DisplayStepGrid(1);
		
		if(DisplayPreTestParam()<0)
		{
			AfxMessageBox("PreTest Parameter Display Error");
		}
	}
	return TRUE;
}

//Step Type에 따라 Grade Gride의 Real Editor Control을 수정 등록 한다.
void CCTSEditorView::SettingGradeWndType(int nStepType, int nProcType)
{
	char str[32], str2[7], str3[5];
	switch(nStepType)
	{
	case PS_STEP_OCV:					//Voltage Grade	format *.***		
		if(nProcType <= 0)
		{
			sprintf(str, "OCV(%s) Grading", GetDocument()->GetVtgUnit());
		}
		else
		{
			sprintf(str, "OCV%d(%s) Grading", nProcType - (PS_STEP_OCV << 16)+1, GetDocument()->GetVtgUnit());
		}
		GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(str);
		
		sprintf(str, "%%.%dlf", GetDocument()->GetVtgUnitFloat());	//소숫점 이하
		sprintf(str3, "%d", GetDocument()->GetVtgUnitFloat());		//소숫점 이하
		if(GetDocument()->GetVtgUnit() == "V")
		{
			sprintf(str2, "%d", 3);		//정수부
		}
		else
		{
			sprintf(str2, "%d", 6);		//정수부
		}
		break;
	case PS_STEP_IMPEDANCE:
//	case PS_STEP_PATTERN:
	case PS_STEP_CHARGE:				//Capacity or Impedance Grade format ****.*	
	case PS_STEP_DISCHARGE:
		if(nStepType == PS_STEP_IMPEDANCE)
		{
#ifdef _EDLC_CELL_
			GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(TEXT_LANG[93]);//"ESR(mΩ) 등급"
#else
			GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(TEXT_LANG[94]);//"DCIR(mΩ) 등급"
#endif
		}
// 		else if(nStepType == PS_STEP_PATTERN)
// 		{
		// 			sprintf(str, TEXT_LANG[], GetDocument()->GetCapUnit());//"용량(%s)"
// 			GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(str);
// 		}
// 		else
// 		{
			sprintf(str, TEXT_LANG[52]);//"단위 표시"
			GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(str);
//		}
		sprintf(str, "%%.%dlf", GetDocument()->GetCapUnitFloat());	//소숫점 이하
		sprintf(str3, "%d", GetDocument()->GetCapUnitFloat());		//소숫점 이하
		
		if(GetDocument()->GetCrtUnit() == "A")
		{
			sprintf(str2, "%d", 4);		//정수부
			
		}
		else
		{
			sprintf(str2, "%d", 6);		//정수부
		}
		break;
	case PS_STEP_REST:
	case PS_STEP_END:
	default:
		m_wndGradeGrid.SetValueRange(CGXRange(1, 1, m_wndGradeGrid.GetRowCount(), _COL_GRADE_CODE_), "");
		break;
	}
	m_wndGradeGrid.SetStyleRange(CGXRange().SetCells(1, _COL_GRADE_MIN1_,
		m_wndGradeGrid.GetRowCount(), _COL_GRADE_MAX1_),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		);
	m_wndGradeGrid.SetStyleRange(CGXRange().SetCells(1, _COL_GRADE_MIN2_,
		m_wndGradeGrid.GetRowCount(), _COL_GRADE_MAX2_),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		);
}


void CCTSEditorView::SettingStepWndType(int nStep, int nStepType, BOOL bGradeWnd )
{
//	m_wndStepGrid.LockUpdate();

	m_wndStepGrid.SetValueRange(CGXRange(nStep, COL_STEP_CYCLE), "");	//Step is Selected Step
	m_wndStepGrid.SetValueRange(CGXRange(nStep, COL_STEP_TYPE), (LONG)GetStepIndexFromType(nStepType));	//Step is Selected Step
	m_wndStepGrid.SetValueRange(CGXRange(nStep, COL_STEP_MODE), (LONG)-1);				//Mode is Null
	m_wndStepGrid.SetValueRange(CGXRange(nStep, COL_STEP_REF_V, nStep, COL_STEP_END), "");			

	SetStepCycleColumn();

	//Step Grid만 Update하면은 기타 조건들은 손대지 않는다.
	if(bGradeWnd)
	{
		GetDlgItem(IDC_VTG_HIGH)->EnableWindow(FALSE);
		GetDlgItem(IDC_VTG_LOW)->EnableWindow(FALSE);
		GetDlgItem(IDC_CRT_HIGH)->EnableWindow(FALSE);
		GetDlgItem(IDC_CRT_LOW)->EnableWindow(FALSE);
		GetDlgItem(IDC_CAP_HIGH)->EnableWindow(FALSE);
		GetDlgItem(IDC_CAP_LOW)->EnableWindow(FALSE);
		GetDlgItem(IDC_IMP_HIGH)->EnableWindow(FALSE);
		GetDlgItem(IDC_IMP_LOW)->EnableWindow(FALSE);
		GetDlgItem(IDC_TEMP_LOW)->EnableWindow(FALSE);
		GetDlgItem(IDC_TEMP_HIGH)->EnableWindow(FALSE);
		GetDlgItem(IDC_DELTA_V)->EnableWindow(FALSE);
		GetDlgItem(IDC_DELTA_I)->EnableWindow(FALSE);
		GetDlgItem(IDC_DELTA_TIME)->EnableWindow(FALSE);
		GetDlgItem(IDC_COMP_V2)->EnableWindow(FALSE);
		GetDlgItem(IDC_COMP_V5)->EnableWindow(FALSE);
		GetDlgItem(IDC_COMP_TIME1)->EnableWindow(FALSE);
		GetDlgItem(IDC_COMP_TIME2)->EnableWindow(FALSE);
		
		GetDlgItem(IDC_EDIT_REG_TEMP)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_RESISTANCE_RATE)->EnableWindow(FALSE);

		GetDlgItem(IDC_REPORT_VOLTAGE)->EnableWindow(FALSE);
		GetDlgItem(IDC_REPORT_CURRENT)->EnableWindow(FALSE);
		GetDlgItem(IDC_REPORT_TIME)->EnableWindow(FALSE);
		GetDlgItem(IDC_REPORT_TIME_MILI)->EnableWindow(FALSE);
		// GetDlgItem(IDC_VI_GET_TIME)->EnableWindow(FALSE);
		GetDlgItem(IDC_RPT_MSEC_SPIN)->EnableWindow(FALSE);
		GetDlgItem(IDC_REPORT_TEMPERATURE)->EnableWindow(FALSE);

		GetDlgItem(IDC_CAP_VTG_LOW)->EnableWindow(FALSE);
		GetDlgItem(IDC_CAP_VTG_HIGH)->EnableWindow(FALSE);

		GetDlgItem(IDC_EXT_OPTION_CHECK)->EnableWindow(FALSE);

		GetDlgItem(IDC_COMP_CHG_CC_VTG)->EnableWindow(FALSE);
		GetDlgItem(IDC_COMP_CHG_CC_TIME)->EnableWindow(FALSE);
		GetDlgItem(IDC_COMP_CHG_CC_DELTA_VTG)->EnableWindow(FALSE);
		GetDlgItem(IDC_COMP_CHG_CV_CRT)->EnableWindow(FALSE);
		GetDlgItem(IDC_COMP_CHG_CV_TIME)->EnableWindow(FALSE);
		GetDlgItem(IDC_COMP_CHG_CV_DELTA_CRT)->EnableWindow(FALSE);
	}

	STEP *pStep = GetDocument()->GetStepData(nStep);
	m_wndStepGrid.SetValueRange(CGXRange(nStep, COL_STEP_PROCTYPE), (LONG)GetDocument()->GetProcIndex(pStep->nProcType));			//Procedure Type is Null
	m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_CYCLE), CGXStyle().SetEnabled(TRUE));
	CGXRange covered;

	if (m_wndStepGrid.GetCoveredCellsRowCol(nStep, COL_STEP_MODE, covered))
	{
		m_wndStepGrid.SetCoveredCellsRowCol(nStep, COL_STEP_MODE, nStep, COL_STEP_MODE);
	}
	
	m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_MODE),
			CGXStyle()
				.SetControl(GX_IDS_CTRL_ZEROBASED_EX2)
				.SetHorizontalAlignment(DT_LEFT)
		);

	switch(nStepType)
	{
	case PS_STEP_CHARGE:
	case PS_STEP_DISCHARGE:
		m_pStepModeCombo->ResetItemData();
		m_pStepModeCombo->SetItemData(0, PS_MODE_CCCV);
		m_pStepModeCombo->SetItemData(1, PS_MODE_CC);
		m_pStepModeCombo->SetItemData(2, PS_MODE_CV);
	
		if (m_bEditType)
		{
			m_pStepModeCombo->SetItemData(3, PS_MODE_CP);		//ljb CP 추가 for samsung
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_MODE),	CGXStyle().SetChoiceList(PS_CHARGE_MODE_STRING2));
			GetDlgItem(IDC_REPORT_TIME_MILI)->EnableWindow(TRUE);
		}
		else
		{
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_MODE),	CGXStyle().SetChoiceList(PS_CHARGE_MODE_STRING3));
			GetDlgItem(IDC_REPORT_TIME_MILI)->EnableWindow(FALSE);
			GetDlgItem(IDC_RPT_MSEC_SPIN)->EnableWindow(FALSE);
			GetDlgItem(IDC_RPT_TUNIT_STATIC)->EnableWindow(FALSE);
		}
		
		m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_MODE, nStep, COL_STEP_END_SOC), CGXStyle().SetEnabled(TRUE));
		m_wndStepGrid.SetValueRange(CGXRange(nStep, COL_STEP_MODE), (LONG)GetModeIndex(pStep->chMode));

		//Aging 공정일 경우 전압 전류 설정을 하지 못함
		if(GetDocument()->m_lLoadedProcType == PS_PGS_AGING)
		{
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_MODE), CGXStyle().SetEnabled(FALSE));
			if(nStepType == PS_STEP_DISCHARGE)
			{
				//방전에선 시간만 설정 가능 
//				m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_MODE), CGXStyle().SetEnabled(FALSE));
				m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_V), CGXStyle().SetEnabled(FALSE));
				m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_I), CGXStyle().SetEnabled(FALSE));
				m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_END_SOC), CGXStyle().SetEnabled(FALSE));
			}
		}	
		
		if(pStep->chMode == PS_MODE_CCCV)
		{
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_P), CGXStyle().SetEnabled(FALSE));		
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_END_V), CGXStyle().SetEnabled(FALSE));
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_END_TEMP), CGXStyle().SetEnabled(FALSE));			
		}
		else if(pStep->chMode == PS_MODE_CC)
		{
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_V), CGXStyle().SetEnabled(FALSE));
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_P), CGXStyle().SetEnabled(FALSE));
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_END_I), CGXStyle().SetEnabled(FALSE));
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_END_DAY), CGXStyle().SetEnabled(FALSE));	
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_END_TEMP), CGXStyle().SetEnabled(FALSE));			
		}
		else if(pStep->chMode == PS_MODE_CV)
		{
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_P), CGXStyle().SetEnabled(FALSE));
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_END_V), CGXStyle().SetEnabled(FALSE));
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_END_DAY), CGXStyle().SetEnabled(FALSE));	
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_END_TEMP), CGXStyle().SetEnabled(FALSE));			
		}
		else if(pStep->chMode == PS_MODE_CP)
		{
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_I), CGXStyle().SetEnabled(FALSE));
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_END_DAY), CGXStyle().SetEnabled(FALSE));	
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_END_TEMP), CGXStyle().SetEnabled(FALSE));	
		}
	
		if(bGradeWnd)
		{
			GetDlgItem(IDC_VTG_HIGH)->EnableWindow(TRUE);
			GetDlgItem(IDC_VTG_LOW)->EnableWindow(TRUE);

//			GetDlgItem(IDC_DELTA_V)->EnableWindow(TRUE);
//			GetDlgItem(IDC_DELTA_I)->EnableWindow(TRUE);
			
			GetDlgItem(IDC_DELTA_TIME)->EnableWindow(TRUE);
			
			// GetDlgItem(IDC_REPORT_VOLTAGE)->EnableWindow(TRUE);
			// GetDlgItem(IDC_REPORT_CURRENT)->EnableWindow(TRUE);
			GetDlgItem(IDC_REPORT_TIME)->EnableWindow(TRUE);
			GetDlgItem(IDC_REPORT_TIME_MILI)->EnableWindow(FALSE);
			// GetDlgItem(IDC_VI_GET_TIME)->EnableWindow(TRUE);
			GetDlgItem(IDC_RPT_MSEC_SPIN)->EnableWindow(TRUE);
			GetDlgItem(IDC_REPORT_TEMPERATURE)->EnableWindow(m_bUseTemp);	

			if(	GetDocument()->m_lLoadedProcType == PS_PGS_FORMATION 
				|| GetDocument()->m_lLoadedProcType == PS_PGS_FINALCHARGE 
				|| GetDocument()->m_lLoadedProcType == PS_PGS_PRECHARGE)
			{
				GetDlgItem(IDC_CRT_HIGH)->EnableWindow(TRUE);
				GetDlgItem(IDC_CRT_LOW)->EnableWindow(TRUE);

#ifndef _EDLC_TEST_SYSTEM
				GetDlgItem(IDC_CAP_HIGH)->EnableWindow(TRUE);
				GetDlgItem(IDC_CAP_LOW)->EnableWindow(TRUE);
#endif
				GetDlgItem(IDC_COMP_TIME1)->EnableWindow(TRUE);

				if( nStepType == PS_STEP_CHARGE)
				{	
					GetDlgItem(IDC_COMP_TIME2)->EnableWindow(TRUE);
					GetDlgItem(IDC_COMP_V2)->EnableWindow(TRUE);		
					GetDlgItem(IDC_COMP_V5)->EnableWindow(TRUE);	

					GetDlgItem(IDC_COMP_CHG_CC_VTG)->EnableWindow(TRUE);
					GetDlgItem(IDC_COMP_CHG_CC_TIME)->EnableWindow(TRUE);
					GetDlgItem(IDC_COMP_CHG_CC_DELTA_VTG)->EnableWindow(TRUE);
					GetDlgItem(IDC_COMP_CHG_CV_CRT)->EnableWindow(TRUE);
					GetDlgItem(IDC_COMP_CHG_CV_TIME)->EnableWindow(TRUE);
					GetDlgItem(IDC_COMP_CHG_CV_DELTA_CRT)->EnableWindow(TRUE);
				}
				
				GetDlgItem(IDC_CAP_VTG_LOW)->EnableWindow(TRUE);
				GetDlgItem(IDC_CAP_VTG_HIGH)->EnableWindow(TRUE);				

				m_GradeCheck.EnableWindow(TRUE);
				GetDlgItem(IDC_EXT_OPTION_CHECK)->EnableWindow(TRUE);
			}
			else
			{
				m_GradeCheck.EnableWindow(FALSE);
			}
			
			if(GetDocument()->m_lLoadedProcType == PS_PGS_AGING)
			{
				GetDlgItem(IDC_TEMP_LOW)->EnableWindow(TRUE);
				GetDlgItem(IDC_TEMP_HIGH)->EnableWindow(TRUE);
			}

			if(pStep->chMode == PS_MODE_CC)
			{
				SetCompCtrlEnable(TRUE, FALSE);								//전압비교만 Enable 시킴 
			}
			else if(pStep->chMode == PS_MODE_CV)
			{
				SetCompCtrlEnable(FALSE, FALSE);							//전류비교만 Enable 시킴 
			}
			else if(pStep->chMode == PS_MODE_CP)
			{
				SetCompCtrlEnable(TRUE, TRUE);								//전압/전류비교 모두 Enable 시킴 
			}
			else if(pStep->chMode == PS_MODE_CCCV)
			{
				SetCompCtrlEnable(TRUE, TRUE);								//전압/전류비교 모두 Enable 시킴 
			}
			else
			{
				SetCompCtrlEnable(FALSE, FALSE);							//모두 Disable 시킴 
			}
		}
		break;

	case PS_STEP_REST:
		m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_MODE, nStep, COL_STEP_END_SOC), CGXStyle().SetEnabled(FALSE));
		m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_T), CGXStyle().SetEnabled(TRUE));
		m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_END_DAY), CGXStyle().SetEnabled(TRUE));
		m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_END_TIME), CGXStyle().SetEnabled(TRUE));
				
		if(bGradeWnd)
		{
// 			GetDlgItem(IDC_VTG_HIGH)->EnableWindow(TRUE);
// 			GetDlgItem(IDC_VTG_LOW)->EnableWindow(TRUE);

			if(GetDocument()->m_lLoadedProcType == PS_PGS_AGING)
			{
				GetDlgItem(IDC_TEMP_LOW)->EnableWindow(TRUE);
				GetDlgItem(IDC_TEMP_HIGH)->EnableWindow(TRUE);
			}
			else
			{
				GetDlgItem(IDC_TEMP_LOW)->EnableWindow(FALSE);
				GetDlgItem(IDC_TEMP_HIGH)->EnableWindow(FALSE);
			}

			// GetDlgItem(IDC_REPORT_VOLTAGE)->EnableWindow(TRUE);
			// GetDlgItem(IDC_REPORT_CURRENT)->EnableWindow(TRUE);
			GetDlgItem(IDC_REPORT_TIME)->EnableWindow(TRUE);
			// GetDlgItem(IDC_VI_GET_TIME)->EnableWindow(TRUE);
			// GetDlgItem(IDC_REPORT_TIME_MILI)->EnableWindow(TRUE);
			GetDlgItem(IDC_REPORT_TEMPERATURE)->EnableWindow(m_bUseTemp);		
			SetCompCtrlEnable(FALSE);
			m_GradeCheck.EnableWindow(FALSE);
			
		}
		break;
		
	case PS_STEP_IMPEDANCE:
		m_pStepModeCombo->ResetItemData();
		m_pStepModeCombo->SetItemData(0, PS_MODE_DCIMP);
		// m_pStepModeCombo->SetItemData(1, PS_MODE_ACIMP);
		m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_MODE),	CGXStyle().SetChoiceList(PS_IMPEDANCE_MODE_STRING));
		m_wndStepGrid.SetValueRange(CGXRange(nStep, COL_STEP_MODE), (LONG)GetModeIndex(pStep->chMode));
		m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_MODE, nStep, COL_STEP_MODE), CGXStyle().SetEnabled(TRUE));
		
		m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_V, nStep, COL_STEP_END_SOC), CGXStyle().SetEnabled(FALSE));
		
		//EDLC Type 일 경우 DC만 지원 하므로 Mode변경을 못하도록 함
		if(pStep->chMode == PS_MODE_ACIMP)
		{
			//AC Imp 측정일 경우 제한치 입력 불가능 하도록 한다.
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_V, nStep, COL_STEP_END_SOC), CGXStyle().SetEnabled(FALSE));
		}
		else
		{
			//Vref를 사용 못하도록 한다.
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_V, nStep, COL_STEP_REF_V), CGXStyle().SetEnabled(FALSE));
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_I), CGXStyle().SetEnabled(TRUE));
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_END_TIME), CGXStyle().SetEnabled(TRUE));
		}

		m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_T), CGXStyle().SetEnabled(m_bUseTemp));

		if(bGradeWnd)
		{
			if(pStep->chMode == PS_MODE_DCIMP)
			{
				//DC Imp 측정일 경우 제한치 입력 가능 하도록 한다.
				GetDlgItem(IDC_VTG_HIGH)->EnableWindow(TRUE);
				GetDlgItem(IDC_VTG_LOW)->EnableWindow(TRUE);
				GetDlgItem(IDC_CRT_HIGH)->EnableWindow(TRUE);
				GetDlgItem(IDC_CRT_LOW)->EnableWindow(TRUE);
				GetDlgItem(IDC_TEMP_LOW)->EnableWindow(m_bUseTemp);
				GetDlgItem(IDC_TEMP_HIGH)->EnableWindow(m_bUseTemp);
				
				GetDlgItem(IDC_EDIT_REG_TEMP)->EnableWindow(TRUE);
				GetDlgItem(IDC_EDIT_RESISTANCE_RATE)->EnableWindow(TRUE);
				
				// GetDlgItem(IDC_REPORT_VOLTAGE)->EnableWindow(TRUE);
				// GetDlgItem(IDC_REPORT_CURRENT)->EnableWindow(TRUE);
				GetDlgItem(IDC_REPORT_TIME)->EnableWindow(TRUE);
				// GetDlgItem(IDC_VI_GET_TIME)->EnableWindow(TRUE);
				GetDlgItem(IDC_REPORT_TEMPERATURE)->EnableWindow(m_bUseTemp);		
				GetDlgItem(IDC_REPORT_TIME_MILI)->EnableWindow(FALSE);
				GetDlgItem(IDC_RPT_MSEC_SPIN)->EnableWindow(FALSE);
			}		
			GetDlgItem(IDC_IMP_HIGH)->EnableWindow(TRUE);	
			GetDlgItem(IDC_IMP_LOW)->EnableWindow(TRUE);
			m_GradeCheck.EnableWindow(TRUE);				

			SetCompCtrlEnable(FALSE);			
		}		
		break;

	case PS_STEP_OCV:
		m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_MODE, nStep, COL_STEP_END_SOC), CGXStyle().SetEnabled(FALSE));
		if(bGradeWnd)
		{
			GetDlgItem(IDC_VTG_HIGH)->EnableWindow(TRUE);		//Use Ocv and Impedance Step
			GetDlgItem(IDC_VTG_LOW)->EnableWindow(TRUE);
			m_GradeCheck.EnableWindow(TRUE);			//Use Ocv and Impedance Step

			SetCompCtrlEnable(FALSE);
		}
		break;

	case PS_STEP_END:
		m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_CYCLE), CGXStyle().SetEnabled(FALSE));
	case PS_STEP_ADV_CYCLE:
	default:
		if(bGradeWnd)
		{
			SetCompCtrlEnable(FALSE);
			m_GradeCheck.EnableWindow(FALSE);	
		}
		m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_TYPE, nStep, COL_STEP_END_SOC), CGXStyle().SetEnabled(FALSE));
		break;
	}

	SetApplyAllBtn(nStepType);
	
	//Step 공정 Type을 [관계없음]을 선택 하였을시 임의 Step 선택 가능하도록 한다. 
	if(pStep->nProcType == 0)
	{
		m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_TYPE), CGXStyle().SetEnabled(TRUE));
	}
	else
	{
		m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_TYPE), CGXStyle().SetEnabled(FALSE));
	}

// ljb 임시 삭제
//	AfxMessageBox("del");
	if(bGradeWnd)	SettingGradeWndType(nStepType, pStep->nProcType);		//Grade Wnd Setting

//	m_wndStepGrid.LockUpdate(FALSE);
//	m_wndStepGrid.Redraw();
}

void CCTSEditorView::UpdateDspModel(CString strName, LONG lID)
{
	m_lDisplayModelID = lID;
	m_strDspModelName = strName;
	
	//strTemp.Format("%s", m_strDspModelName);
	m_TestNameLabel.SetText(m_strDspModelName);
	
}

void CCTSEditorView::UpdateDspTest(CString strName, LONG lID)
{
	m_lDisplayTestID = lID;
	m_strDspTestName = strName;
	
	if(GetDlgItem(IDC_LOAD_TEST)->m_hWnd && m_tooltip.m_hWnd)
		m_tooltip.AddTool(GetDlgItem(IDC_LOAD_TEST), m_strDspTestName+TEXT_LANG[95]);//" 시험 조건을 Load 합니다."
}

//Battery Model Count에 따른 변경
void CCTSEditorView::UpdateBatteryCountState(int nCount)
{
	GetDlgItem(IDC_TEST_NEW)->EnableWindow(nCount);
	GetDlgItem(IDC_TEST_DELETE)->EnableWindow(nCount);
	GetDlgItem(IDC_TEST_SAVE)->EnableWindow(FALSE);
	
	CString strTemp;
	strTemp.Format("%d", nCount);
	m_TotModelCount.SetText(strTemp);
	
	if(nCount>0)
	{
		strTemp.Format("%s", m_strDspModelName, m_lDisplayModelID);
		m_TestNameLabel.SetText(strTemp);
	}
	else
	{
		m_TestNameLabel.SetText(TEXT_LANG[96]);//"선택된 Model이 없습니다. Model 목록에서 먼저 선택 하십시요."
	}
}

//remove End Type Dlg
void CCTSEditorView::HideEndDlg()
{
	
}

BOOL CCTSEditorView::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if( pMsg->message == WM_KEYDOWN )
	{
		if(GetFocus() == &m_wndStepGrid)
		{
			switch( pMsg->wParam )
			{
			case VK_INSERT:
				
				OnStepInsert();
				return TRUE;
				
			case VK_DELETE:
				OnStepDelete();
				return TRUE;
			}
		}
	}

	m_tooltip.RelayEvent(pMsg);
	return CFormView::PreTranslateMessage(pMsg);
}


void CCTSEditorView::ClearStepState()
{
	m_TotalStepCount.SetText("0");
	GetDlgItem(IDC_STEP_INSERT)->EnableWindow(FALSE);
	GetDlgItem(IDC_STEP_DELETE)->EnableWindow(FALSE);
	GetDlgItem(IDC_STEP_SAVE)->EnableWindow(FALSE);
	GetDlgItem(IDC_LOAD_TEST)->EnableWindow(FALSE);
	GetDlgItem(IDC_PRETEST_CHECK)->EnableWindow(FALSE);
}

void CCTSEditorView::ShowEndTypeDlg(int nStep)
{
	if(GetDocument()->GetStepData(nStep) == NULL)	return;

	CRect gridRect, cellRect,DlgRect;
	m_wndStepGrid.GetWindowRect(gridRect);						//Get Step Grid Window Position
	cellRect = m_wndStepGrid.CalcRectFromRowCol(0, 0, nStep, COL_STEP_END-1);

	int nTopPoint = gridRect.top+cellRect.Height()+3;
}

BOOL CCTSEditorView::StepSave()
{
	int nRtn;
	CString strTemp;

	CCTSEditorDoc *pDoc = (CCTSEditorDoc *)GetDocument();
	
	UpdatePreTestParam();

	if(!UpdateStepGrid(m_nDisplayStep))				//Update Current Display Step Data
	{
		AfxMessageBox("Step Data Update Fail...");
		return FALSE;
	}

	int nID = 2;	
	
	if(CheckExecutableStep(nID, m_lLoadedTestID, m_lDisplayModelID) == FALSE)
	{
		// strTemp.Format(TEXT_LANG[97], m_wndTestListGrid.GetValueRowCol(a+1, COL_TEST_NAME));//"[%s]공정에서 시행할수 없는 Step이 포함되어 있습니다."
		// MessageBox(strTemp, "Error", MB_OK|MB_ICONSTOP);
		return FALSE;
	}

	if((nRtn = pDoc->SaveStep(m_lLoadedTestID, m_lDisplayModelID))<0)
	{
		strTemp.Format(TEXT_LANG[98], GetDocument()->m_strLastErrorString);//"저장 실패. %s"
		MessageBox(strTemp, TEXT_LANG[99], MB_OK|MB_ICONSTOP);//"저장실패"
		return FALSE;
	}

	if(pDoc->SavePreTestParam(m_lLoadedTestID, m_lDisplayModelID)<0)
	{
		strTemp.Format(TEXT_LANG[100], GetDocument()->m_strLastErrorString);//"Cell check parameter 저장 실패. %s"
		AfxMessageBox(strTemp);
		return FALSE;
	}

	//Undo List 삭제 
	pDoc->RemoveUndoStep();
	m_bStepDataSaved = TRUE;
	return TRUE;
}

void CCTSEditorView::SetModelGridColumnWidth()
{
	if( m_pWndCurModelGrid->GetSafeHwnd())
	{
		CRect rect;
		m_pWndCurModelGrid->GetClientRect(&rect);
		m_pWndCurModelGrid->SetColWidth(COL_MODEL_NO, COL_MODEL_NO, rect.Width()*0.1f);
		
		if(GetDocument()->m_bUseLogin == FALSE)
		{
			m_pWndCurModelGrid->SetColWidth(COL_MODEL_NAME, COL_MODEL_NAME, rect.Width()*0.4f);
			m_pWndCurModelGrid->SetColWidth(COL_MODEL_CREATOR, COL_MODEL_CREATOR, rect.Width()*0.2f);
			m_pWndCurModelGrid->SetColWidth(COL_MODEL_DESCRIPTION, COL_MODEL_DESCRIPTION, rect.Width()*0.3f);
		}
		else
		{
			m_pWndCurModelGrid->SetColWidth(COL_MODEL_NAME, COL_MODEL_NAME, rect.Width()*0.5f);
			m_pWndCurModelGrid->SetColWidth(COL_MODEL_CREATOR, COL_MODEL_CREATOR, 0);
			m_pWndCurModelGrid->SetColWidth(COL_MODEL_DESCRIPTION, COL_MODEL_DESCRIPTION, rect.Width()*0.4f);
		}

		m_pWndCurModelGrid->SetColWidth(COL_MODEL_PRIMARY_KEY, COL_MODEL_PRIMARY_KEY, 0); 
		m_pWndCurModelGrid->SetColWidth(COL_MODEL_EDIT_STATE, COL_MODEL_EDIT_STATE, 0); 
	}
}

void CCTSEditorView::SetTestGridColumnWidth()
{
	if( m_pWndCurModelGrid->GetSafeHwnd())
	{
		CRect rect;
		m_wndTestListGrid.GetClientRect(&rect);
/*		m_wndTestListGrid.SetColWidth(COL_TEST_NO, COL_TEST_NO, rect.Width()*0.05f);
		m_wndTestListGrid.SetColWidth(COL_TEST_PROC_TYPE, COL_TEST_PROC_TYPE, rect.Width()*0.14f);
		m_wndTestListGrid.SetColWidth(COL_TEST_NAME, COL_TEST_NAME, rect.Width()*0.2f);
		m_wndTestListGrid.SetColWidth(COL_TEST_CREATOR, COL_TEST_CREATOR, rect.Width()*0.13f);
		m_wndTestListGrid.SetColWidth(COL_TEST_DESCRIPTION, COL_TEST_DESCRIPTION, rect.Width()*0.20f);
		m_wndTestListGrid.SetColWidth(COL_TEST_EDIT_TIME, COL_TEST_EDIT_TIME, rect.Width()*0.28f);
		m_wndTestListGrid.SetColWidth(COL_TEST_PRIMARY_KEY, COL_TEST_PRIMARY_KEY, 0);
		m_wndTestListGrid.SetColWidth(COL_TEST_EDIT_STATE, COL_TEST_EDIT_STATE, 0);
*/
		m_wndTestListGrid.SetColWidth(COL_TEST_NO, COL_TEST_NO, rect.Width()*0.1f);
		m_wndTestListGrid.SetColWidth(COL_TEST_PROC_TYPE, COL_TEST_PROC_TYPE, 0);
		m_wndTestListGrid.SetColWidth(COL_TEST_NAME, COL_TEST_NAME, rect.Width()*0.4f);

		m_wndTestListGrid.SetColWidth(COL_TEST_RESET_PROC, COL_TEST_RESET_PROC, 0);
		m_wndTestListGrid.SetColWidth(COL_TEST_DESCRIPTION, COL_TEST_DESCRIPTION, rect.Width()*0.5f);

		m_wndTestListGrid.SetColWidth(COL_TEST_CREATOR, COL_TEST_CREATOR, 0);
		m_wndTestListGrid.SetColWidth(COL_TEST_EDIT_TIME, COL_TEST_EDIT_TIME, 0);
		m_wndTestListGrid.SetColWidth(COL_TEST_PRIMARY_KEY, COL_TEST_PRIMARY_KEY, 0);
		m_wndTestListGrid.SetColWidth(COL_TEST_EDIT_STATE, COL_TEST_EDIT_STATE, 0);
	}
}

void CCTSEditorView::OnOption() 
{
	// TODO: Add your command handler code here
	CEditorSetDlg	*pDlg = new CEditorSetDlg(this);
	ASSERT(pDlg);

	if(pDlg->DoModal() == IDOK)
	{
		m_bVHigh  = pDlg->m_bVHigh;
		m_bVLow   = pDlg->m_bVLow ;
		m_bIHigh  = pDlg->m_bIHigh;
		m_bILow   = pDlg->m_bILow;
		m_bCHigh  = pDlg->m_bCHigh;
		m_bCLow   = pDlg->m_bCLow;

		m_fCLowVal = pDlg->m_fCLowVal;
		m_fCHighVal = pDlg->m_fCHighVal ;
		m_fILowVal = pDlg->m_fILowVal ;
		m_fIHighVal = pDlg->m_fIHighVal;
		m_fVLowVal = pDlg->m_fVLowVal;
		m_fVHighVal = pDlg->m_fVHighVal;
		GetDocument()->m_bUseLogin = pDlg->m_bUseLogin;
		GetDocument()->m_lVRefHigh = pDlg->m_lVRefHigh;
		GetDocument()->m_lVRefLow = pDlg->m_lVRefLow;
	}

	delete pDlg;
	pDlg = NULL;
}


//int CCTSEditorView::RequeryProcDataTypeID()
//{
//	BOOL	bRtn = TRUE;
//	CCTSEditorDoc *pDoc = GetDocument();
//	DATA_CODE *pData = NULL;
//	
//	CProcDataRecordSet	rs;
//	rs.m_strSort.Format("[ProgressID]");
//
//	try
//	{
//		if(rs.Open() == FALSE)	return FALSE;
//	}
//	catch (CDBException* e)
//	{
//		e->Delete();
//	}
//
//	if(rs.IsOpen())
//	{
//		while(!rs.IsEOF())
//		{
//			pData = new DATA_CODE;
//			pData->nCode = rs.m_ProgressID;
//			sprintf(pData->szMessage, "%s", rs.m_Name);
//			pData->nDataType = rs.m_DataType;
//			pDoc->m_apProcData.Add(pData);
//			rs.MoveNext();
//		}
//		rs.Close();
//		bRtn = TRUE;
//	}
//	else
//	{
//		pData =  new DATA_CODE;
//		ASSERT(pData);
//		pData->nCode = 0;
//		sprintf(pData->szMessage, "Unknown");
//		pDoc->m_apProcData.Add(pData);
//		bRtn = FALSE;
//	}
//	return bRtn;
//}

BOOL CCTSEditorView::UpdateTestNo()
{
	CTestListRecordSet	recordSet;
	recordSet.m_strSort.Format("[TestID]");

	try
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}

	ROWCOL nRow = m_wndTestListGrid.GetRowCount();
	CString key;

	for(int i=0; i<nRow; i++)
	{
		key.Format("TestID = %s", m_wndTestListGrid.GetValueRowCol(i+1, COL_TEST_PRIMARY_KEY));
		recordSet.Find(AFX_DAO_FIRST, key);
		recordSet.Edit();
		recordSet.m_TestNo = atol(m_wndTestListGrid.GetValueRowCol(i+1, COL_TEST_NO));
		recordSet.Update();
	}
	recordSet.Close();
	return TRUE;
}


BOOL CCTSEditorView::UpdateTypeComboList()
{
	CCTSEditorDoc *pDoc = (CCTSEditorDoc *)GetDocument();
	m_ptProcArray.RemoveAll();
	STEP *pStep;
	SCH_CODE_MSG procType;
	int i = 0, j=0;

	int nTotStep = pDoc->GetTotalStepNum();
	CString strProcType, strTemp;

	for(j = 0; j < pDoc->m_apProcType.GetSize(); j++)
	{
		memcpy(&procType, pDoc->m_apProcType[j], sizeof(SCH_CODE_MSG));
		for(i = 0 ; i<nTotStep; i++)
		{
			pStep = (STEP *)pDoc->GetStepData(i);
			if(pStep->nProcType == procType.nCode)
			{
				break;
			}
		}
		if( i >= nTotStep)
		{
			m_ptProcArray.Add(procType);
			strTemp.Format("%s\n", procType.szMessage);
			strProcType += strTemp;
		}
	}

	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_PROCTYPE),	CGXStyle().SetChoiceList(strProcType));
	return TRUE;
}

void CCTSEditorView::UpdateModelNo()
{
	CBatteryModelRecordSet	recordSet;
	recordSet.m_strSort.Format("[ModelID]");

	try
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return ;
	}

	ROWCOL nRow = m_pWndCurModelGrid->GetRowCount();
	CString key;

	for(int i=0; i<nRow; i++)
	{
		key.Format("[ModelID] = %s", m_pWndCurModelGrid->GetValueRowCol(i+1, COL_MODEL_PRIMARY_KEY));
		recordSet.Find(AFX_DAO_FIRST, key);
		recordSet.Edit();
		recordSet.Update();
	}
	recordSet.Close();
	return ;
}


//모델 목록 복사 
BOOL CCTSEditorView::CopyModel(ROWCOL nRow)
{
	if(nRow < 1)	return FALSE;

	CDaoDatabase  db;
	CCTSEditorDoc *pDoc =  GetDocument();

//	CString strDataBaseName;
//	strDataBaseName = pDoc->m_strDBFolder+ "\\" +FORM_SET_DATABASE_NAME;
	db.Open(g_strDataBaseName);

	int nCount = m_pWndCurModelGrid->GetRowCount() + 1;
	CString name = m_pWndCurModelGrid->GetValueRowCol(nRow, COL_MODEL_NAME);
	CString descript =  m_pWndCurModelGrid->GetValueRowCol(nRow, COL_MODEL_DESCRIPTION);
	long lModelID = atol(m_pWndCurModelGrid->GetValueRowCol(nRow, COL_MODEL_PRIMARY_KEY));

	//새로운 명을 Dialog 입력 받도록 수정 /////////////////
	CModelNameDlg	dlg;
	dlg.m_strName = name;
	dlg.m_strDescript = descript;
	dlg.m_strUserID = GetDocument()->m_LoginData.szLoginID;
	if(IDOK != dlg.DoModal())
	{
		return TRUE;
	}
	name = dlg.m_strName;
	descript = dlg.m_strDescript;
	//////////////////////////////////////////


	CString strSQL;

#ifdef _USE_MODEL_USER_
	strSQL.Format("INSERT INTO BatteryModel(No, ModelName, Description) VALUES (%d, '%s', '%s')", nCount, name, descript);
#else
	strSQL.Format("INSERT INTO BatteryModel(No, ModelName, Description, Creator) VALUES (%d, '%s', '%s', '%s')", nCount, name, descript, dlg.m_strUserID);
#endif

	db.Execute(strSQL);

	//Insert 된 Model의 ID를 구한다.
	long newModelID;
	strSQL = "SELECT MAX(ModelID) FROM BatteryModel";
	CDaoRecordset rs(&db);
	rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
	if(!rs.IsBOF())
	{
		COleVariant data = rs.GetFieldValue(0);
		newModelID = data.lVal;
	}
	rs.Close();
	db.Close();

	m_pWndCurModelGrid->InsertRows(nCount, 1);
	m_pWndCurModelGrid->SetValueRange(CGXRange(nCount, COL_MODEL_NAME), name);
	m_pWndCurModelGrid->SetValueRange(CGXRange(nCount, COL_MODEL_DESCRIPTION), descript);
	m_pWndCurModelGrid->SetValueRange(CGXRange(nCount, COL_MODEL_PRIMARY_KEY), newModelID);
	m_pWndCurModelGrid->SetValueRange(CGXRange(nCount, COL_MODEL_CREATOR), dlg.m_strUserID);
	m_pWndCurModelGrid->SetValueRange(CGXRange(nCount, COL_MODEL_EDIT_STATE), CS_SAVED);
	m_pWndCurModelGrid->SetCurrentCell(nCount, 1);

	CopyTestList(lModelID, newModelID );

	GetDocument()->m_bEditedFlag = TRUE;		//DataBase 변경 Flag (BackUp을 위해)
	
	return TRUE;
}

//TestName 목록 복사 
BOOL CCTSEditorView::CopyTestList(int lFromModelID, int lToModelID)
{
	CDaoDatabase  db;
	CCTSEditorDoc *pDoc =  GetDocument();

	db.Open(g_strDataBaseName);

	CString name, descript, creator;
	long check, typeID, testNo, lTestID, lFromTestID, lModelID;
	COleDateTime modtime;

	CString strSQL;
	strSQL.Format("SELECT TestID, ModelID, TestName, Description, Creator, PreTestCheck, ProcTypeID, TestNo, ModifiedTime FROM TestName WHERE ModelID = %d ORDER BY TestNo", lFromModelID);
	COleVariant data;
	CDaoRecordset rs(&db);
	rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
	while(!rs.IsEOF())
	{
		data = rs.GetFieldValue(0);		lFromTestID = data.lVal;
		data = rs.GetFieldValue(1);	    lModelID = data.lVal;
		data = rs.GetFieldValue(2);		name = data.pbVal;
		data = rs.GetFieldValue(3);		descript = data.pbVal;
		data = rs.GetFieldValue(4);		creator = data.pbVal;
		data = rs.GetFieldValue(5);		check = data.lVal;
		data = rs.GetFieldValue(6);		typeID = data.lVal;
		data = rs.GetFieldValue(7);		testNo = data.lVal;
//		data = rs.GetFieldValue(7);		modtime = data;
		modtime = COleDateTime::GetCurrentTime();
		rs.MoveNext();
		strSQL.Format("INSERT INTO TestName(ModelID, TestName, Description, Creator, PreTestCheck, ProcTypeID, TestNo, ModifiedTime) VALUES (%d, '%s', '%s', '%s', %d, %d, %d, '%s')",
			lToModelID, name, descript, creator, check, typeID, testNo, modtime.Format());

		db.Execute(strSQL);

		//Insert 된 test의 ID를 구한다.
		strSQL = "SELECT MAX(TestID) FROM TestName";
		CDaoRecordset rs1(&db);
		rs1.Open(dbOpenSnapshot, strSQL, dbReadOnly);
		if(!rs1.IsBOF())
		{
			lTestID = rs1.GetFieldValue(0).lVal;
		}

		CopyCheckParam(lFromTestID, lTestID, lModelID);
		CopyStepList(lFromTestID, lTestID, lModelID);
		
		rs1.Close();
	}
	rs.Close();

	db.Close();

	return TRUE;
}

BOOL CCTSEditorView::CopyStepList(long lFromTestID, long lToTestID,  long lModelID)
{	
	CDaoDatabase  db;
	db.Open(g_strDataBaseName);

	CString strSQL;
	CStepRecordSet rs, rs1;
	rs.m_strFilter.Format("[TestID] = %d AND [ModelID] = %ld", lFromTestID, lModelID);
	rs.m_strSort = "[StepNo]";

	rs.Open();
	rs1.Open();

	while(!rs.IsEOF())
	{
		rs1.AddNew();					
//		rs1.m_StepID = rs.m_StepID;				//1
		rs1.m_ModelID = rs.m_ModelID;
		rs1.m_TestID = lToTestID;
		rs1.m_StepNo = rs.m_StepNo;
		rs1.m_StepType = rs.m_StepType;
		rs1.m_StepMode = rs.m_StepMode;
		rs1.m_Vref = rs.m_Vref;
		rs1.m_Iref = rs.m_Iref;
		rs1.m_EndTime = rs.m_EndTime;
		rs1.m_EndV = rs.m_EndV;		//10
		rs1.m_EndI = rs.m_EndI;
		rs1.m_CycleCount = rs.m_CycleCount;
		rs1.m_EndCapacity = rs.m_EndCapacity;
		rs1.m_End_dV = rs.m_End_dV;
		rs1.m_End_dI = rs.m_End_dI;
		rs1.m_OverV = rs.m_OverV;
		rs1.m_LimitV = rs.m_LimitV;
		rs1.m_OverI = rs.m_OverI;
		rs1.m_LimitI = rs.m_LimitI;
		rs1.m_OverCapacity = rs.m_OverCapacity;	//20
		rs1.m_LimitCapacity = rs.m_LimitCapacity;
		rs1.m_OverImpedance = rs.m_OverImpedance;
		rs1.m_LimitImpedance = rs.m_LimitImpedance;
		rs1.m_DeltaTime = rs.m_DeltaTime;
		rs1.m_DeltaV = rs.m_DeltaV;
		rs1.m_DeltaTime1 = rs.m_DeltaTime1;
		rs1.m_DeltaI = rs.m_DeltaI;
		rs1.m_Grade = rs.m_Grade;
		rs1.m_CompTimeV1 = rs.m_CompTimeV1;
		rs1.m_CompTimeV2 = rs.m_CompTimeV2;		//30
		rs1.m_CompTimeV3 = rs.m_CompTimeV3;
		rs1.m_CompVLow1 = rs.m_CompVLow1;
		rs1.m_CompVLow2 = rs.m_CompVLow2;
		rs1.m_CompVLow3 = rs.m_CompVLow3;
		rs1.m_CompVHigh1 = rs.m_CompVHigh1;
		rs1.m_CompVHigh2 = rs.m_CompVHigh2;
		rs1.m_CompVHigh3 = rs.m_CompVHigh3;

		rs1.m_CompTimeI1 = rs.m_CompTimeI1;
		rs1.m_CompTimeI2 = rs.m_CompTimeI2;
		rs1.m_CompTimeI3 = rs.m_CompTimeI3;		//40
		rs1.m_CompILow1 = rs.m_CompILow1;
		rs1.m_CompILow2 = rs.m_CompILow2;
		rs1.m_CompILow3 = rs.m_CompILow3;
		rs1.m_CompIHigh1 = rs.m_CompIHigh1;
		rs1.m_CompIHigh2 = rs.m_CompIHigh2;
		rs1.m_CompIHigh3 = rs.m_CompIHigh3;

		rs1.m_CapVHigh		= rs.m_CapVHigh;	
		rs1.m_CapVLow		= rs.m_CapVLow	;
		
		rs1.m_RecordDeltaI	= rs.m_RecordDeltaI;
		rs1.m_RecordDeltaV	= rs.m_RecordDeltaV;		//50
		rs1.m_RecordTime	= rs.m_RecordTime;

		rs1.m_EndCheckVLow  = rs.m_EndCheckVLow ;
		rs1.m_EndCheckVHigh = rs.m_EndCheckVHigh ;
		rs1.m_EndCheckILow  = rs.m_EndCheckILow ;
		rs1.m_EndCheckIHigh	= rs.m_EndCheckIHigh;

		rs1.m_strGotoValue  = rs.m_strGotoValue;
		rs1.m_strReportTemp = rs.m_strReportTemp;
		rs1.m_strSocEnd  = rs.m_strSocEnd;
		rs1.m_strEndWatt  = rs.m_strEndWatt;
		rs1.m_strTempLimit  = rs.m_strTempLimit;		//60
		
// 		if(rs.m_StepType == PS_STEP_PATTERN)
// 			rs1.m_strSimulFile  = NewSimulationFile(rs.m_strSimulFile);
// 		else
			rs1.m_strSimulFile = rs.m_strSimulFile;

		rs1.m_strValueLimit = rs.m_strValueLimit;
		//rs1.m_Value7  = rs.m_Value7;
		rs1.m_strEndVGotoValue  = rs.m_strEndVGotoValue;
		rs1.m_strEndTGotoValue  = rs.m_strEndTGotoValue; //rs1.m_Value8  = rs.m_Value8;
		rs1.m_strEndCGotoValue  = rs.m_strEndCGotoValue; //rs1.m_Value9  = rs.m_Value9;
		
		rs1.m_StepProcType = rs.m_StepProcType;		//66
		
		rs1.m_RecordVIGetTime = rs.m_RecordVIGetTime;
		
		rs1.m_fDCIR_RegTemp = rs.m_fDCIR_RegTemp;
		rs1.m_fDCIR_ResistanceRate = rs.m_fDCIR_ResistanceRate;

		rs1.m_fCompChgCcVtg = rs.m_fCompChgCcVtg;
		rs1.m_ulCompChgCcTime = rs.m_ulCompChgCcTime;
		rs1.m_fCompChgCcDeltaVtg = rs.m_fCompChgCcDeltaVtg;
		rs1.m_fCompChgCvCrt = rs.m_fCompChgCvCrt;
		rs1.m_ulCompChgCvtTime = rs.m_ulCompChgCvtTime;
		rs1.m_fCompChgCvDeltaCrt = rs.m_fCompChgCvDeltaCrt;

		rs1.Update();
		
		if(rs.m_Grade)
		{
			strSQL = "SELECT MAX(StepID) FROM Step";
			CDaoRecordset rs2(&db);
			rs2.Open(dbOpenSnapshot, strSQL, dbReadOnly);
			if(!rs2.IsBOF())
			{
				CopyGradeStep(rs.m_StepID, rs2.GetFieldValue(0).lVal, lModelID);
			}
			rs2.Close();
		}
		rs.MoveNext();
	}
	rs.Close();
	rs1.Close();
	db.Close();

/*	CDaoDatabase  db;
	CCTSEditorDoc *pDoc =  GetDocument();

	db.Open(g_strDataBaseName);

	CString strSQL;
	strSQL.Format("SELECT TestID, TestName, Description, Creator, PreTestCheck, ProcTypeID, TestNo FROM TestName WHERE ModelID = %d ORDER BY TestNo", lFromTestID);
	COleVariant data;
	CDaoRecordset rs(&db);
	rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
	while(!rs.IsEOF())
	{
		data = rs.GetFieldValue(0);		lFromTestID = data.lVal;
		data = rs.GetFieldValue(1);		name = data.pbVal;
		data = rs.GetFieldValue(2);		descript = data.pbVal;
		data = rs.GetFieldValue(3);		creator = data.pbVal;
		data = rs.GetFieldValue(4);		check = data.lVal;
		data = rs.GetFieldValue(5);		typeID = data.lVal;
		data = rs.GetFieldValue(6);		testNo = data.lVal;
		strSQL.Format("INSERT INTO TestName(ModelID, TestName, Description, Creator, PreTestCheck, ProcTypeID, TestNo) VALUES (%d, '%s', '%s', '%s', %d, %d, %d)",
			lToModelID, name, descript, creator, check, typeID, testNo);

		db.Execute(strSQL);

		//Insert 된 test의 ID를 구한다.
		strSQL = "SELECT MAX(TestID) FROM TestName";
		CDaoRecordset rs1(&db);
		rs1.Open(dbOpenSnapshot, strSQL, dbReadOnly);
		if(!rs1.IsBOF())
		{
			lTestID = rs1.GetFieldValue(0).lVal;
		}
		rs1.Close();
		
		rs.MoveNext();
	}
	rs.Close();

	db.Close();

*/	return TRUE;
}

BOOL CCTSEditorView::CopyCheckParam(long lFromTestID, long lToTestID, long lModelID)
{
//	CCTSEditorDoc *pDoc =  GetDocument();

	CPreTestCheckRecordSet rs;
	rs.m_strFilter.Format("[TestID] = %d AND [ModelID] = %ld", lFromTestID, lModelID); 
	rs.m_strSort = "[CheckID]";

	rs.Open();

	if(!rs.IsEOF())
	{
		float maxV = rs.m_MaxV;
		float minV = rs.m_MinV;
		float crtRange = rs.m_CurrentRange;
		float ocvMax = rs.m_OCVLimit;
		float trickle_I = rs.m_TrickleCurrent;
		long trickle_time = rs.m_TrickleTime;
		float deltaV = rs.m_DeltaVoltage;
		int faultNo = rs.m_MaxFaultBattery;
		COleDateTime autoTime = rs.m_AutoTime;
		BOOL autoYN = rs.m_AutoProYN;
		BOOL check = rs.m_PreTestCheck;

		rs.AddNew();
		rs.m_TestID = lToTestID;
		rs.m_MaxV = maxV;
		rs.m_MinV = minV;
		rs.m_CurrentRange = crtRange;
		rs.m_OCVLimit = ocvMax;
		rs.m_TrickleCurrent = trickle_I;
		rs.m_TrickleTime = trickle_time;
		rs.m_DeltaVoltage = deltaV;
		rs.m_MaxFaultBattery = faultNo;
		rs.m_AutoTime = autoTime;
		TRACE("%s\n", rs.m_AutoTime.Format());
		rs.m_AutoProYN = autoYN;
		rs.m_PreTestCheck = check;
		rs.Update();
	}
	
	rs.Close();
	return TRUE;
}

BOOL CCTSEditorView::CopyGradeStep(long lFromStepID, long lToStepID, long lModelID)
{
	CGradeRecordSet rs, rs1;
	rs.m_strFilter.Format("[StepID] = %d AND [ModelID] = %ld", lFromStepID, lModelID);
	rs.m_strSort = "[GradeID]";

	rs.Open();
	rs1.Open();

	while(!rs.IsEOF())
	{
		rs1.AddNew();					
		rs1.m_StepID = lToStepID;
		rs1.m_GradeItem = rs.m_GradeItem;
		rs1.m_Value = rs.m_Value;
		rs1.m_Value1 = rs.m_Value1;
		rs1.m_GradeCode = rs.m_GradeCode;
		rs1.Update();
		rs.MoveNext();
	}
	rs.Close();
	rs1.Close();
	return TRUE;
}

void CCTSEditorView::OnEditCopy() 
{
	// TODO: Add your command handler code here
	CRowColArray	awRows;
	m_wndStepGrid.GetSelectedRows(awRows);
	int nSelNo = awRows.GetSize();

	if(nSelNo < 1)		return;

	if(BackUpStep(awRows))
	{
		m_bCopyed = TRUE;		//새로운 Step이 Copy 되었을시 
		m_bPasteUndo = FALSE;	//붙여넣기 취소 불가능 
	}
}

void CCTSEditorView::OnUpdateEditCopy(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	if(&m_wndStepGrid == (CMyGridWnd *)GetFocus())
	{
		pCmdUI->Enable(TRUE );
	}
	else
	{
		pCmdUI->Enable(FALSE );
	}
}

void CCTSEditorView::OnEditPaste() 
{
/*
	CPowerEditorDoc *pDoc = GetDocument();
	if(pDoc->m_apCopyStep.GetSize() < 1)	return;

	ROWCOL nRow, nCol;
	if(m_wndStepGrid.GetCurrentCell(nRow, nCol)== FALSE)	return;

	if(nRow <= 0)	nRow =1;
	pDoc->PasteStep(nRow);	

	DisplayStepGrid();		//전체 Step ReDraw

	DisplayStepGrid(nRow);	//현재 Display Step을 붙여 넣은 첫번째 Step을 표시 

	m_bPasteUndo = TRUE;	//붙여넣기 Undo 가능 
	m_bCutUndo = FALSE;		//잘라내기 Undo 불가능  
	m_bStepDataSaved = FALSE;
*/
	
	//덮어 쓰도록 수정 
	// TODO: Add your command handler code here
	CCTSEditorDoc *pDoc = GetDocument();
	int nCopySize = pDoc->m_apCopyStep.GetSize();
	if(nCopySize < 1)	return;

	ROWCOL nRow, nCol;
	if(m_wndStepGrid.GetCurrentCell(nRow, nCol)== FALSE)	return;

	CRowColArray	awRows;
	m_wndStepGrid.GetSelectedRows(awRows);
	int nSelNo = awRows.GetSize();

	nRow = awRows[0];

	if(nRow <= 0)	nRow = 1;

	STEP *pStep, *pCopyStep;
	int nStartRow = nRow;
	int nEndRow = nStartRow+nCopySize-1;
	
	int nDestRow = 0;
	int nOrgRowCount = m_wndStepGrid.GetRowCount();

	AddUndoStep();

	int i = 0;

	for(i = 0; i<nCopySize; i++)
	{
		nDestRow = nRow+i;
	
		//이미 존재하면 덮어쓴다.
		if(nDestRow <= nOrgRowCount)	
		{
			pStep = (STEP *)pDoc->GetStepData(nDestRow);
			pCopyStep = (STEP *)pDoc->m_apCopyStep[i];

// 			if(pCopyStep->chType == PS_STEP_PATTERN)
// 			{
// 				CString temp(pCopyStep->szSimulFile);
// 				
// 				sprintf(pCopyStep->szSimulFile, NewSimulationFile(temp));
// 			}

			if(pStep && pCopyStep)
			{
				memcpy(pStep, pCopyStep,sizeof(STEP));
				DisplayStepGrid(nDestRow);						//변경 결과를 반영하기 위해 반드시 필요함 
			}
		}
		else		//새로운 step을 삽입한다.
		{
			pCopyStep = (STEP *)pDoc->m_apCopyStep[i];

// 			if(pCopyStep->chType == PS_STEP_PATTERN)
// 				AfxMessageBox("Pattern2");

			if(pCopyStep)
			{
				AddStepNew(nDestRow, FALSE, pCopyStep);
				DisplayStepGrid(nDestRow);						//변경 결과를 반영하기 위해 반드시 필요함 
			}
		}
	}
	DisplayStepEndStringAll();	//EndV, EndT,EndC GotoStep 변경

	//가장 마지막이 End Step이 아니면 빈 step을 하나 넣는다.
	//2006/4/27
	if((nRow+nCopySize) >= nOrgRowCount)
	{
		pCopyStep = pDoc->GetLastStepData();
		if(pCopyStep)
		{
			if(pCopyStep->chType != PS_STEP_END)
			{
				AddStepNew(m_wndStepGrid.GetRowCount()+1);
			}
		}
	}

	m_wndStepGrid.SelectRange(CGXRange(nStartRow, 0, nEndRow, m_wndStepGrid.GetColCount()));

	m_bStepDataSaved = FALSE;

	for(i = 1 ; i <= GetDocument()->GetTotalStepNum(); i++)
	{
		UpdateStepGrid(i);		
	}

	DisplayStepEndStringAll();
}

void CCTSEditorView::OnUpdateEditPaste(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	if(&m_wndStepGrid == (CMyGridWnd *)GetFocus())
	{
		pCmdUI->Enable(m_bCopyed );
	}
	else
	{
		pCmdUI->Enable(FALSE );
	}
}

void CCTSEditorView::OnEditInsert() 
{
	// TODO: Add your command handler code here
/*
	CPowerEditorDoc *pDoc = GetDocument();
	if(pDoc->m_apCopyStep.GetSize() < 1)	return;

	ROWCOL nRow, nCol;
	if(m_wndStepGrid.GetCurrentCell(nRow, nCol)== FALSE)	return;

	if(nRow <= 0)	nRow =1;
	pDoc->PasteStep(nRow);	

	DisplayStepGrid();		//전체 Step ReDraw

	DisplayStepGrid(nRow);	//현재 Display Step을 붙여 넣은 첫번째 Step을 표시 

	m_bPasteUndo = TRUE;	//붙여넣기 Undo 가능 
	m_bCutUndo = FALSE;		//잘라내기 Undo 불가능  
	m_bStepDataSaved = FALSE;
*/
	
	//삽입한다.
	// TODO: Add your command handler code here
	CCTSEditorDoc *pDoc = GetDocument();
	int nCopySize = pDoc->m_apCopyStep.GetSize();
	if(nCopySize < 1)	return;

	ROWCOL nRow, nCol;
	if(m_wndStepGrid.GetCurrentCell(nRow, nCol)== FALSE)	return;
	if(nRow <= 0)	nRow = 1;
	
	AddUndoStep();

	STEP *pCopyStep;
	int nStartRow = nRow;
	int nEndRow = nStartRow+nCopySize-1;
	int nOrgRowCount = m_wndStepGrid.GetRowCount();
	//Undo 가능하도록 Backup을 해야 한다.
	int nDestRow;
	for(int i = 0; i<nCopySize; i++)
	{
		nDestRow = nStartRow + i;
		pCopyStep = (STEP *)pDoc->m_apCopyStep[i];
		AddStepNew(nDestRow, FALSE, pCopyStep);
		TRACE("Insert Step to %d\n", nDestRow);
		DisplayStepGrid(nDestRow);
	}

	m_wndStepGrid.SelectRange(CGXRange(nStartRow, 0, nEndRow, m_wndStepGrid.GetColCount()));
	//DisplayStepGrid(nDestRow);


	//가장 마지막이 End Step이 아니면 빈 step을 하나 넣는다.
	//2006/4/27
	if(nCopySize >= (nOrgRowCount - nStartRow))
	{
		pCopyStep = pDoc->GetLastStepData();
		if(pCopyStep)
		{
			if(pCopyStep->chType != PS_STEP_END)
			{
				AddStepNew(m_wndStepGrid.GetRowCount()+1);
			}
		}
	}
	//임시 삭제 (업데이트 하면 모든 스텝의 GRADE가 변경됩)
// 	for(i = 1 ; i <= GetDocument()->GetTotalStepNum(); i++)
// 	{
// 		UpdateStepGrid(i);		
// 	}
	DisplayStepEndStringAll();

	m_bStepDataSaved = FALSE;	
}

void CCTSEditorView::OnUpdateEditInsert(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	if(&m_wndStepGrid == (CMyGridWnd *)GetFocus())
	{
		pCmdUI->Enable(m_bCopyed );
	}
	else
	{
		pCmdUI->Enable(FALSE );
	}	
}


void CCTSEditorView::OnEditCut() 
{
	// TODO: Add your command handler code here
	CRowColArray	awRows;
	m_wndStepGrid.GetSelectedRows(awRows);
	int nSelNo = awRows.GetSize();

	if(nSelNo < 1)		return;

	if(BackUpStep(awRows, TRUE))
	{
		AddUndoStep();
		DeleteStep(awRows);
	}

	for(int i = 1 ; i <= GetDocument()->GetTotalStepNum(); i++)
	{
		UpdateStepGrid(i);		
	}

	DisplayStepEndStringAll();
}

void CCTSEditorView::OnUpdateEditCut(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	
	if(&m_wndStepGrid == (CMyGridWnd *)GetFocus())
	{
		pCmdUI->Enable(TRUE );
	}
	else
	{
		pCmdUI->Enable(FALSE );
	}
}

void CCTSEditorView::OnEditUndo() 
{
	// TODO: Add your command handler code here
	CCTSEditorDoc *pDoc = (CCTSEditorDoc *)GetDocument();
	if(m_bPasteUndo)
	{
		int nStart,nEnd;
		m_bPasteUndo = pDoc->UpdoStepEdit(nStart,nEnd);
		DisplayStepGrid();
		//변경된 부분을 선택된 상태로 한다.
	}
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnUpdateEditUndo(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	if(&m_wndStepGrid == (CMyGridWnd *)GetFocus())
	{
		pCmdUI->Enable(m_bPasteUndo);
	}
	else
	{
		pCmdUI->Enable(FALSE );
	}
}

BOOL CCTSEditorView::BackUpStep(CRowColArray &awRows, BOOL bEnableUndo)
{
/*	CRowColArray	awRows;
	m_wndStepGrid.GetSelectedRows(awRows);
*/	int nSelNo = awRows.GetSize();

	if(nSelNo < 1)
		return FALSE;

	CCTSEditorDoc *pDoc = GetDocument();
	pDoc->ClearCopyStep();
	for (int i = 0; i<nSelNo; i++)
	{
		if(awRows[i] > 0)
		{
			pDoc->AddCopyStep(awRows[i]);
		}
	}
	m_bCopyed = bEnableUndo;		//붙여 넣기 가능 
	m_bStepDataSaved = FALSE;
	return TRUE;
}


BOOL CCTSEditorView::DeleteStep(CDWordArray &awSteps)
{
	if(GetDocument()->PermissionCheck(PMS_EDITOR_EDIT) ==  FALSE)
	{
		AfxMessageBox(TEXT_LANG[84]);//"수정 권한이 없습니다."
		return FALSE;
	}
		
	CCTSEditorDoc *pDoc = (CCTSEditorDoc *)GetDocument();
	ROWCOL nRow = m_wndStepGrid.GetRowCount();
	if(nRow <1)		
	{
		return FALSE;
	}

//	CRowColArray	awSteps;		//Get Selected Step
//	m_wndStepGrid.GetSelectedRows(awSteps);

	if(awSteps.GetSize() < 1)	
	{
		return FALSE;
	}

	int i = 0 ;

	for (i = awSteps.GetSize()-1; i >=0 ; i--)
	{
		TRACE("SELECT Row is %d\n", awSteps[i]);
		STEP * pStep;
		pStep = pDoc->GetStepData(awSteps[i]);

		//Simulation 삭제시 연결된 파일도 삭제
// 		if(pStep->chType == PS_STEP_PATTERN)
// 		{
// 			CString temp(pStep->szSimulFile);
// 			DeleteSimulationFile(temp);
// 		}

		//삭제되는 Step를 EndV, EndT, EndC GotoStep으로 사용되는 연결 삭제
		for(int j = 1 ; j < pDoc->GetTotalStepNum(); j++)
		{
			STEP * pGotoStep = pDoc->GetStepData(j);
			if(pStep->nGotoStepID == pGotoStep->nLoopInfoEndVGoto)
				pGotoStep->nLoopInfoEndVGoto = 0;
			if(pStep->nGotoStepID == pGotoStep->nLoopInfoEndTGoto)
				pGotoStep->nLoopInfoEndTGoto = 0;
			if(pStep->nGotoStepID == pGotoStep->nLoopInfoEndCGoto)
				pGotoStep->nLoopInfoEndCGoto = 0;
		}
		

		if(!pDoc->ReMoveStepArray(awSteps[i]))
		{
			MessageBox(TEXT_LANG[101],"Delete Error", MB_OK|MB_ICONSTOP);	//"Step을 삭제 할수 없습니다."
			return FALSE;
		}
		m_wndStepGrid.RemoveRows(awSteps[i], awSteps[i]);
	}

	//Added by KBH 2005/12/22 
	//삭제된 이후 Step에 goto 정보가 있을 경우 수정된 Step으로 변경한다.
	int nStartRow = awSteps[0];		// nStep-1+1 가장 처음 Step의 이전 Step보다 1Step 다음 Step
	int nLastRow = awSteps[awSteps.GetSize()-1];
	STEP *pStep;
	for(i=0; i<pDoc->GetTotalStepNum(); i++)
	{
		pStep = pDoc->GetStepData(i+1);
		if(pStep == NULL)	break;

		if(pStep->chType == PS_STEP_LOOP)
		{
			//삭제 step이전이면서 삭제 Step이후를 참조하면 변경 실시(삭제 Step이전을 참조하면 변경하지 않음)
			if(i+1 < nStartRow && pStep->nLoopInfoGoto > nStartRow && pStep->nLoopInfoGoto != PS_GOTO_NEXT_CYC)		
			{
				pStep->nLoopInfoGoto -= awSteps.GetSize();
				m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_END), pDoc->MakeEndString(i+1));

			}
			
			//삭제 step이후 이면서 삭제 이후 Step을 참조하면 변경 실시(삭제된 Step이전을 참조하면 변경하지 않음)
			if(i+1 >= nStartRow && pStep->nLoopInfoGoto >= nStartRow && pStep->nLoopInfoGoto != PS_GOTO_NEXT_CYC)
			{
				pStep->nLoopInfoGoto -= awSteps.GetSize();	
				m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_END), pDoc->MakeEndString(i+1));
			}
		}

		
	}
	
	nRow = m_wndStepGrid.GetRowCount();

	int nCol;
#ifdef _CYCLER_
	nCol = COL_STEP_TYPE;
#else
	nCol = COL_STEP_PROCTYPE;
#endif

	if(nRow >= nStartRow)		//Last step deleted
	{
		m_wndStepGrid.SetCurrentCell(nStartRow, nCol);
		DisplayStepGrid(nStartRow);						
	}
	else 
	{
		m_wndStepGrid.SetCurrentCell(nStartRow-1, nCol);
		DisplayStepGrid(nStartRow-1);						
	}

/*	if(nLastRow > 0 && nLastRow <= nRow)		//삭제 이전 Step 선택 
	{
		m_wndStepGrid.SetCurrentCell(nLastRow, COL_STEP_TYPE);
		DisplayStepGrid(nLastRow);			
	}
	else
	{
		DisplayStepGrid(nRow);			
		
		if(nRow > 0)	//Step 1을 삭제 시켰을 시
		{
			m_wndStepGrid.SetCurrentCell(nRow, COL_STEP_TYPE);
		}

/*		else
		{
			//Step Size == 0;
			DisplayStepGrid();

			DisplayStepOption(NULL);	//Reset Control Data

			for(i = IDC_VTG_HIGH; i<= IDC_DELTA_TIME; i++)
			{
				GetDlgItem(i)->EnableWindow(FALSE);
			}

			SetCompCtrlEnable(FALSE);
		
		}
*///	}
	return TRUE;
}

//Step Type의 BomboIndex를 검색 한다.
int CCTSEditorView::GetStepIndexFromType(int nType)
{
	if(m_pStepTypeCombo == NULL)	return -1;
	
	for(int i=0; i<m_pStepTypeCombo->GetItemDataSize(); i++)
	{
		if(m_pStepTypeCombo->GetItemData(i) == nType)		return i;
	}
	return -1;
}

long CCTSEditorView::GetModeIndex(int nMode)
{
	if(m_pStepModeCombo == NULL)	return -1;
	
	for(int i=0; i<m_pStepModeCombo->GetItemDataSize(); i++)
	{
		if(m_pStepModeCombo->GetItemData(i) == nMode)		return i;
	}
	return -1;
}



//Test Name의 Combo Index를 검색 한다.
int CCTSEditorView::GetTestIndexFromType(int nType)
{
	if(m_pTestTypeCombo == NULL)	return -1;
	
	for(int i=0; i<m_pTestTypeCombo->GetItemDataSize(); i++)
	{
		if(m_pTestTypeCombo->GetItemData(i) == nType)		return i;
	}
	return -1;

}

/*

BOOL CCTSEditorView::LoadTestList(CTestListRecordSet *pRecord)
{
	if(pRecord == NULL)	return FALSE;
	
	int nTotModelNum = 0;
//	recordSet.m_strFilter.Format("[ModelID] = %ld", lModelID);
//	recordSet.m_strSort.Format("[TestNo]");

	try
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}

	ROWCOL nRow = pRecord->GetRowCount();

	if(pRecord->IsBOF())
	{
		nTotModelNum = 0;
	}
	else
	{
		pRecord->MoveLast();
		nTotModelNum = pRecord->GetRecordCount();
		pRecord->MoveFirst(); 
	}

	nTotModelNum -= nRow;

	if(nTotModelNum > 0)
	{	
		m_wndTestListGrid.InsertRows(nRow+1, nTotModelNum);
	}
	else if(nTotModelNum <0)
	{
		m_wndTestListGrid.RemoveRows(nRow+nTotModelNum+1, nRow);
	}

	nRow = 0;
	while(!pRecord->IsEOF())
	{	
		nRow++;
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_PROC_TYPE), (ROWCOL)GetTestIndexFromType(pRecord->m_ProcTypeID));	
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_NAME), pRecord->m_TestName);
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_CREATOR), pRecord->m_Creator);
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_DESCRIPTION), pRecord->m_Description);
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_EDIT_TIME), pRecord->m_ModifiedTime.Format());
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_PRIMARY_KEY), pRecord->m_TestID);
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_EDIT_STATE), CS_SAVED);
		pRecord->MoveNext();
	}
	
	if(nRow >0)	//가장 마지막 Recoedset을 보여 준다.
	{
		pRecord->MovePrev();
		UpdateDspTest(pRecord->m_TestName,  pRecord->m_TestID);
	}
	else
	{
		UpdateDspTest("", 0);
	}
	pRecord->Close();
	
	CString strTemp;
	strTemp.Format("%d", nRow);
	m_TotTestCount.SetText(strTemp);
	
	return TRUE;
}
*/

void CCTSEditorView::OnModelCopyButton() 
{
	// TODO: Add your control notification handler code here
	ModelCopy();
}

void CCTSEditorView::OnExcelSave() 
{
	// TODO: Add your command handler code here
	CScheduleData schData;
	if(schData.SetSchedule(GetDataBaseName(), m_lLoadedBatteryModelID, m_lLoadedTestID) == FALSE)
	{
		AfxMessageBox("Step data loading fail!!!");
		return;
	}

	CString strFileName;
	strFileName.Format("%s_%s.csv", schData.GetModelName(), schData.GetScheduleName());

	CFileDialog pDlg(FALSE, "csv", strFileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "csv File(*.csv)|*.csv|");
	if(IDOK == pDlg.DoModal())
	{
		strFileName = pDlg.GetPathName();
	}

	if(schData.SaveToExcelFile(strFileName, TRUE, TRUE) == FALSE)
	{
		AfxMessageBox(strFileName+ " save fail.");
		return;		
	}


/*	CString FileName, strTemp;
	ROWCOL nModelRow = 0, nTestRow = 0;
	//현재 Loading된 모델을 Model Grid에서 찾는다.
	for(int i=0; i<m_pWndCurModelGrid->GetRowCount(); i++)
	{
		if(m_lLoadedBatteryModelID == atol(m_pWndCurModelGrid->GetValueRowCol(i+1, COL_MODEL_PRIMARY_KEY)))
		{
			nModelRow =  i+1;
			break;
		}
	}
	if(nModelRow == 0)	
	{
		AfxMessageBox("모델 정보를 찾을 수 없습니다.", MB_OK);
		return;
	}

	//현재 Loading된 공정을 공정 Grid에서 찾는다.
	for(i=0; i<m_wndTestListGrid.GetRowCount(); i++)
	{
		if(m_lLoadedTestID == atol(m_wndTestListGrid.GetValueRowCol(i+1, COL_TEST_PRIMARY_KEY)))
		{
			nTestRow =  i+1;
			break;
		}
	}
	if(nTestRow == 0)	
	{
		strTemp.Format("모델 [%s]의 공정 정보를 찾을 수 없습니다.", m_pWndCurModelGrid->GetValueRowCol(nModelRow, 1));
		AfxMessageBox(strTemp, MB_OK);
		return;
	}


	CFileDialog pDlg(FALSE, "", m_strDspModelName+"_"+m_strDspTestName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "csv 파일(*.csv)|*.csv|");
	if(IDOK == pDlg.DoModal())
	{
		FileName = pDlg.GetPathName();
	}
	if(FileName.IsEmpty())
	{
		return;
	}

	CString strData;
	FILE *fp = fopen(FileName, "wt");
	if(fp == NULL)
	{
		strData.Format("%s_%s.csv 파일을 생성할 수 없습니다.", m_strDspModelName, m_strDspTestName);
		AfxMessageBox(strData, MB_OK);
		return;
	}
	fprintf(fp, "[모델명]\n");
	fprintf(fp, "%s,%s\n", "Model Name", m_pWndCurModelGrid->GetValueRowCol(nModelRow, COL_MODEL_NAME));
	fprintf(fp, "%s,%s\n", "설명", m_pWndCurModelGrid->GetValueRowCol(nModelRow, COL_MODEL_DESCRIPTION));

	fprintf(fp, "[공정명]\n");
	fprintf(fp, "%s,%s\n", "공정명", m_wndTestListGrid.GetValueRowCol(nTestRow, COL_TEST_NAME));
	fprintf(fp, "%s,%s\n", "작성자", m_wndTestListGrid.GetValueRowCol(nTestRow, COL_TEST_CREATOR));
	fprintf(fp, "%s,%s\n", "작성시간", m_wndTestListGrid.GetValueRowCol(nTestRow, COL_TEST_EDIT_TIME));
	fprintf(fp, "%s,%s\n\n", "설명", m_wndTestListGrid.GetValueRowCol(nTestRow, COL_TEST_DESCRIPTION));

	TEST_PARAM *pParam;
	pParam = (TEST_PARAM *)(&GetDocument()->m_sPreTestParam);

	fprintf(fp, "[검사 조건]\n");
	fprintf(fp, "%s,%.3f\n", "OCV 상한", pParam->fMaxVoltage);
	fprintf(fp, "%s,%.3f\n", "OCV 하한", pParam->fMinVoltage);
	fprintf(fp, "%s,%.3f\n", "검사전압", pParam->fOCVLimitVal);	//v ref
	fprintf(fp, "%s,%.1f\n", "검사전류", pParam->fTrickleCurrent);
	fprintf(fp, "%s,%s\n", "검사시간", pParam->OleTrickleTime.Format("%H:%M:%S"));
	fprintf(fp, "%s,%.3f\n", "전압변화", pParam->fDeltaVoltage);
	fprintf(fp, "%s,%d\n\n", "불량수", pParam->nMaxFaultNo);

	fprintf(fp, "[Step]\n");
//	fprintf(fp, "StepNo,Type,Mode,VRef,IRef,EndV,EndI,EndTime,EndC,");
	fprintf(fp, "StepNo,Type,Mode,VRef,IRef,End,");
	fprintf(fp, "V Limit,I Limit,C Limit, Imp Limit,");
	for(int a = 0; a<SCH_MAX_COMP_POINT; a++)
	{
		fprintf(fp, "(V%dlow<V<V%dhigh)/t%d,", a+1, a+1, a+1);
	}
	for(a = 0; a<SCH_MAX_COMP_POINT; a++)
	{
		fprintf(fp, "(I%dlow<I<I%dhigh)/t%d,", a+1, a+1, a+1);
	}
	fprintf(fp, "\n");
	
	CCTSEditorDoc *pDoc = (CCTSEditorDoc *)GetDocument();	//Get Document point
	int nTotStepNum = pDoc->GetTotalStepNum();					//Total Step Number
	STEP *pStep;
	for(int step = 0; step<nTotStepNum; step++)
	{
		pStep = (STEP *)pDoc->GetStepData(step+1);
		if(!pStep)
		{
			fclose(fp);
			return;
		}
		
		fprintf(fp, "%d,", pStep->chStepNo);			
		fprintf(fp, "%s,", StepTypeMsg(pStep->chType));
		fprintf(fp, "%s,", ModeTypeMsg(pStep->chType, pStep->chMode));
	
		switch(pStep->chType)
		{
		case PS_STEP_CHARGE:
		case PS_STEP_DISCHARGE:
			{
				fprintf(fp, "%.3f,", pStep->fVref);
				fprintf(fp, "%.1f,", pStep->fIref);
				fprintf(fp, "%s,", pDoc->MakeEndString(step+1));

				if(pStep->fVLimitLow != 0 && pStep->fVLimitHigh != 0)
				{
					strTemp.Format("%.3f≤V≤%.3f", pStep->fVLimitLow, pStep->fVLimitHigh);
				}
				else if(pStep->fVLimitLow != 0)
				{
					strTemp.Format("%.3f≤V", pStep->fVLimitLow);
				}
				else if(pStep->fVLimitHigh != 0)
				{
					strTemp.Format("V≤%.3f",  pStep->fVLimitHigh);
				}
				else 
					strTemp.Empty();
				fprintf(fp, "%s,", strTemp);

				if(pStep->fILimitLow != 0 && pStep->fILimitHigh != 0)
				{
					strTemp.Format("%.1f≤I≤%.1f", pStep->fILimitLow, pStep->fILimitHigh);
				}
				else if(pStep->fILimitLow != 0)
				{
					strTemp.Format("%.1f≤I", pStep->fILimitLow);
				}
				else if(pStep->fILimitHigh != 0)
				{
					strTemp.Format("I≤%.1f", pStep->fILimitHigh);
				}
				else 
					strTemp.Empty();
				fprintf(fp, "%s,", strTemp);

				if(pStep->fCLimitLow != 0 && pStep->fCLimitHigh != 0)
				{
					strTemp.Format("%.1f≤C≤%.1f", pStep->fCLimitLow, pStep->fCLimitHigh);
				}
				else if(pStep->fCLimitLow != 0)
				{
					strTemp.Format("%.1f≤C", pStep->fCLimitLow);
				}
				else if(pStep->fCLimitHigh != 0)
				{
					strTemp.Format("C≤%.1f", pStep->fCLimitHigh);
				}
				else 
					strTemp.Empty();
				fprintf(fp, "%s,", strTemp);
				fprintf(fp, ",");
				for(a = 0; a<SCH_MAX_COMP_POINT; a++)
				{
					fprintf(fp, "(%.3f<V<%.3f)/%d,", pStep->fCompVLow[a], pStep->fCompVHigh[a], pStep->ulCompTimeV[a]) ;
				}
				for(a = 0; a<SCH_MAX_COMP_POINT; a++)
				{
					fprintf(fp, "(%.1f/I/%.1f)/%d,", pStep->fCompILow[a], pStep->fCompIHigh[a], pStep->ulCompTimeV[a]) ;
				}
				fprintf(fp, "\n");
				break;
			}

		case PS_STEP_REST:
			{
				fprintf(fp, ",,");
				fprintf(fp, "%s,", pDoc->MakeEndString(step+1));
				fprintf(fp, "\n");
				break;
			}
		case PS_STEP_OCV:
			{
				fprintf(fp, ",,,");
				if(pStep->fVLimitLow != 0 && pStep->fVLimitHigh != 0)
				{
					strTemp.Format("%.3f≤V≤%.3f", pStep->fVLimitLow, pStep->fVLimitHigh);
				}
				else if(pStep->fVLimitLow != 0)
				{
					strTemp.Format("%.3f≤V", pStep->fVLimitLow);
				}
				else if(pStep->fVLimitHigh != 0)
				{
					strTemp.Format("V≤%.3f", pStep->fVLimitHigh);
				}
				else 
					strTemp.Empty();
				fprintf(fp, "%s,", strTemp);
				fprintf(fp, "\n");
				break;
			}
		case PS_STEP_IMPEDANCE:
			{
				fprintf(fp, ",,,,,,");
				if(pStep->fImpLimitLow != 0 && pStep->fImpLimitHigh != 0)
				{
					strTemp.Format("%s≤Z≤%s", pStep->fImpLimitLow, pStep->fImpLimitHigh);
				}
				else if(pStep->fImpLimitLow != 0)
				{
					strTemp.Format("%s≤Z", pStep->fImpLimitLow);
				}
				else if(pStep->fImpLimitHigh != 0)
				{
					strTemp.Format("Z≤%s", pStep->fImpLimitHigh);
				}
				else 
					strTemp.Empty();
				fprintf(fp, "\n");
				break;
			}
		case PS_STEP_LOOP:
			{
				fprintf(fp, "\n");
				break;
			}
		case PS_STEP_END:
		default:
			{
				fprintf(fp, "\n");
				break;
			}
		}
	}

	fclose(fp);	
*/
}

void CCTSEditorView::OnUpdateFileSave(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(m_lLoadedBatteryModelID);
}

void CCTSEditorView::OnUpdateFileOpen(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
//	pCmdUI->Enable(FALSE);	
}

BOOL CCTSEditorView::WriteFile()
{
	CScheduleData schData;
	if(schData.SetSchedule(GetDataBaseName(), m_lLoadedBatteryModelID, m_lLoadedTestID) == FALSE)
	{
		AfxMessageBox("Step data loading fail!!!");
		return FALSE;
	}

	CString FileName;
	FileName.Format("%s_%s.sch", schData.GetModelName(), schData.GetScheduleName());

	CFileDialog pDlg(FALSE, "csv", FileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "sch File(*.sch)|*.sch|");
	if(IDOK == pDlg.DoModal())
	{
		FileName = pDlg.GetPathName();
	}
	if(FileName.IsEmpty())
	{
		return FALSE;
	}

	if(schData.SaveToFile(FileName) == FALSE)
	{
		AfxMessageBox(FileName+TEXT_LANG[102]);//" 저장 실패!!!"
		return FALSE;
	}
	return TRUE;
	
	
/*	CString FileName, strTemp;
	ROWCOL nModelRow = 0, nTestRow = 0;
	//현재 Loading된 모델을 Model Grid에서 찾는다.
	for(int i=0; i<m_pWndCurModelGrid->GetRowCount(); i++)
	{
		if(m_lLoadedBatteryModelID == atol(m_pWndCurModelGrid->GetValueRowCol(i+1, COL_MODEL_PRIMARY_KEY)))
		{
			nModelRow =  i+1;
			break;
		}
	}
	if(nModelRow == 0)	
	{
		AfxMessageBox("모델 정보를 찾을 수 없습니다.", MB_OK);
		return FALSE;
	}

	//현재 Loading된 공정을 공정 Grid에서 찾는다.
	for(i=0; i<m_wndTestListGrid.GetRowCount(); i++)
	{
		if(m_lLoadedTestID == atol(m_wndTestListGrid.GetValueRowCol(i+1, COL_TEST_PRIMARY_KEY)))
		{
			nTestRow =  i+1;
			break;
		}
	}
	if(nTestRow == 0)	
	{
		strTemp.Format("모델 [%s]의 공정 정보를 찾을 수 없습니다.", m_pWndCurModelGrid->GetValueRowCol(nModelRow, 1));
		AfxMessageBox(strTemp, MB_OK);
		return FALSE;
	}


	FILE *fp = fopen(FileName, "wt");
	if(fp == NULL){
		AfxMessageBox(m_strDspModelName+"_"+m_strDspTestName+".sch 파일을 생성할 수 없습니다.", MB_OK);
		return FALSE;
	}

	//파일 Header 기록 
	PS_FILE_ID_HEADER	EpFileHeader;

	COleDateTime dateTime = COleDateTime::GetCurrentTime();
	sprintf(EpFileHeader.szCreateDateTime, "%s", dateTime.Format());	//기록 시간
	EpFileHeader.nFileID = SCHEDULE_FILE_ID;				//파일 ID
	EpFileHeader.nFileVersion = SCHEDULE_FILE_VER;			//파일 Version
	sprintf(EpFileHeader.szDescrition, "ADPOWER power supply scheule file.");	//파일 서명 
	fwrite(&EpFileHeader, sizeof(PS_FILE_ID_HEADER), 1, fp);				//기록 실시 
	
	//모델 정보 기록 
	SCH_TEST_INFORMATION modelData;
	modelData.lID = atol(m_pWndCurModelGrid->GetValueRowCol(nModelRow, COL_MODEL_PRIMARY_KEY));
	sprintf(modelData.szName, "%s", m_pWndCurModelGrid->GetValueRowCol(nModelRow, COL_MODEL_NAME));
	sprintf(modelData.szDescription, "%s", m_pWndCurModelGrid->GetValueRowCol(nModelRow, COL_MODEL_DESCRIPTION));
	//modelData.szCreator
	//modelData,.szModifiedTime
	fwrite(&modelData, sizeof(SCH_TEST_INFORMATION), 1, fp);				//기록 실시 

	//공정 정보 기록 
	SCH_TEST_INFORMATION testData;
	testData.lID = atol(m_wndTestListGrid.GetValueRowCol(nTestRow, COL_TEST_PROC_TYPE));		//공정 종류 
	sprintf(testData.szName, "%s", m_wndTestListGrid.GetValueRowCol(nTestRow, COL_TEST_NAME));
	sprintf(testData.szDescription, "%s", m_wndTestListGrid.GetValueRowCol(nTestRow, COL_TEST_DESCRIPTION));
	sprintf(testData.szCreator, "%s",  m_wndTestListGrid.GetValueRowCol(nTestRow, COL_TEST_CREATOR));
	sprintf(testData.szModifiedTime, "%s", m_wndTestListGrid.GetValueRowCol(nTestRow, COL_TEST_EDIT_TIME));
	fwrite(&testData, sizeof(SCH_TEST_INFORMATION), 1, fp);				//기록 실시 

	//cell check 조건 기록 
	TEST_PARAM *pParam;
	pParam = (TEST_PARAM *)(&GetDocument()->m_sPreTestParam);
	FILE_CELL_CHECK_PARAM cellCheck;
	if(ConvertCellCheckFormat(pParam, cellCheck) == FALSE)
	{
		fclose(fp);
		return FALSE;
	}
	fwrite(&cellCheck, sizeof(cellCheck), 1, fp);				//기록 실시 
	
	//step 정보 기록 
	CCTSEditorDoc *pDoc = (CCTSEditorDoc *)GetDocument();	//Get Document point
	int nTotStepNum = pDoc->GetTotalStepNum();					//Total Step Number
	STEP *pStep;
	FILE_STEP_PARAM fileStep;
	for(int step = 0; step<nTotStepNum; step++)
	{
		pStep = (STEP *)pDoc->GetStepData(step+1);
		if(!pStep)
		{
			fclose(fp);
			return FALSE;
		}
		ConvertStepFormat(pStep, fileStep);
		fwrite(&fileStep, sizeof(fileStep), 1, fp);				//기록 실시 
	}	
	
	fclose(fp);
	return TRUE;
*/
}

void CCTSEditorView::OnFileSave() 
{
	// TODO: Add your command handler code here
	WriteFile();
}

BOOL CCTSEditorView::ReadFile()
{
	CString FileName, strTemp;

	if((!m_bStepDataSaved || GetDocument()->IsEdited() )&& GetDocument()->PermissionCheck(PMS_EDITOR_EDIT) != FALSE)
	{
		GetDlgItem(IDC_LOADED_TEST)->GetWindowText(strTemp);
		strTemp += TEXT_LANG[103];//" 조건이 변경 되었습니다. 저장 하시겠습니까?"
		if(IDYES == MessageBox(strTemp, TEXT_LANG[82], MB_ICONQUESTION|MB_YESNO))//"저장"
		{
			if(!StepSave())	return FALSE;
		}
	}
	

	//File Open Dialog
	CFileDialog pDlg(TRUE, "", "", OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "sch 파일(*.sch)|*.sch|All Files (*.*)|*.*|");

	if(IDOK == pDlg.DoModal())
	{
		FileName = pDlg.GetPathName();
	}
	if(FileName.IsEmpty())
	{
		return FALSE;
	}
	
	CScheduleData schData;
	if(schData.SetSchedule(FileName) == FALSE)
	{
		AfxMessageBox(FileName + TEXT_LANG[104], MB_OK);//" 파일을 읽을 수 없습니다."
		return FALSE;
	}

/*
	//File Open
	FILE *fp = fopen(FileName, "rt");
	if(fp == NULL){
		AfxMessageBox(FileName + " 파일을 읽을 수 없습니다.", MB_OK);
		return FALSE;
	}

	//파일 Header 기록 
	PS_FILE_ID_HEADER	EpFileHeader;
	if(fread(&EpFileHeader, sizeof(PS_FILE_ID_HEADER), 1, fp) < 1)				//기록 실시 
	{
		fclose(fp);
		return FALSE;
	}
	//Header 정보 검사
	if(EpFileHeader.nFileID != SCHEDULE_FILE_ID)
	{
		AfxMessageBox("시험 조건 파일이 아닙니다.", MB_OK);
		fclose(fp);
		return FALSE;
	}

	//모델 정보 기록 
	SCH_TEST_INFORMATION modelData;
	if(fread(&modelData, sizeof(modelData), 1, fp) < 1)			//기록 실시 
	{
		fclose(fp);
		return FALSE;
	}
*/
	//새로운 모델 이름 지정 
	
/*	CModelNameDlg	dlg(TRUE, this);
	dlg.SetName(schData.GetModelName(), schData.GetModelDescript());
	if(dlg.DoModal() != IDOK)	
	{
		//저장 취소 
		return FALSE;
	}
*/

	CTestNameDlg	dlg1(m_lDisplayModelID, this);
	dlg1.m_strName = schData.GetScheduleName();
	dlg1.m_strDecription = schData.GetScheduleDescript();
	dlg1.m_strCreator = GetDocument()->m_LoginData.szLoginID;//schData.GetScheduleCreator();
	dlg1.m_createdDate = COleDateTime::GetCurrentTime();
	dlg1.m_lTestTypeID = schData.GetScheduleType();
	
	if(dlg1.DoModal() != IDOK)	
	{
		//저장 취소 
		return FALSE;
	}

	
	//이전 모델명에 같은 이름이 있을 경우 이후 공정으로 추가 여부 확인
	//같은 모델명이 없으면 새로운 모델로 추가 한다.
	ROWCOL nRow = m_pWndCurModelGrid->GetRowCount();
	ROWCOL modelRow = 0;
	for(int i =0; i<nRow; i++)
	{
//		if( m_pWndCurModelGrid->GetValueRowCol(i+1, COL_MODEL_NAME) == dlg.GetName())			//모델명 비교 
//			&& m_pWndCurModelGrid->GetValueRowCol(i+1, COL_MODEL_DESCRIPTION) == dlg.GetDescript())  //설명 비교 

/*
		if( atol(m_pWndCurModelGrid->GetValueRowCol(i+1, COL_MODEL_PRIMARY_KEY)) == dlg1.m_lModelID)			//모델명 비교 
		{
			modelRow = i+1;
			break;

		}
*/
	}

/*	if(modelRow == 0)	//같은 모델명 발견되지 않았거나 새로운 모델로 등록하길 원할 경우  
	{
		nRow++;
		//  Model Grid에 새로 추가 하고 ModelSave 실행 
		m_pWndCurModelGrid->InsertRows(nRow, 1);
		m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_EDIT_STATE), CS_NEW);
		m_pWndCurModelGrid->SetCurrentCell(nRow, COL_MODEL_NAME);
		//m_pWndCurModelGrid->GetValueRowCol(nRow, 0));
		m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_NAME), dlg.GetName());
		m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_DESCRIPTION), dlg.GetDescript());
		
		//DataBase에 모델 추가 
		OnModelSave();
	}
	else	//같은 모델명이 발견되어 추가 공정으로 원할 경우 
	{
		nRow = modelRow;
	}
*/
	nRow = modelRow;

	//추가 모델 정보로 Display 갱신 
	strTemp = m_pWndCurModelGrid->GetValueRowCol(nRow, COL_MODEL_PRIMARY_KEY);	//Battery Model ID
	UpdateDspModel(m_pWndCurModelGrid->GetValueRowCol(nRow, COL_MODEL_NAME), atoi((LPCSTR)(LPCTSTR)strTemp));
	m_pWndCurModelGrid->SetCurrentCell(nRow, COL_MODEL_NAME);
	
	RequeryTestList(m_lDisplayModelID);
		
//	strTemp.Format("%s", m_strDspModelName);
//	m_TestNameLabel.SetText(strTemp);
	
/*
	//공정 정보 기록 
	SCH_TEST_INFORMATION testData;
	if(fread(&testData, sizeof(SCH_TEST_INFORMATION), 1, fp)  < 1)			//기록 실시 
	{
		fclose(fp);
		return FALSE;
	}
*/

	//////////////////////////////////////////////////////////////////////////
	//Test 등록 시작 	
	//Grid에 추가 한다.
	nRow = m_wndTestListGrid.GetRowCount()+1;
	m_wndTestListGrid.InsertRows(nRow, 1);
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_CREATOR), GetDocument()->m_LoginData.szUserName);
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_EDIT_TIME), dlg1.m_createdDate);
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_EDIT_STATE), CS_NEW);
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_PROC_TYPE), (long)GetTestIndexFromType(dlg1.m_lTestTypeID));
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_NAME), dlg1.m_strName);
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_DESCRIPTION), dlg1.m_strDecription);
	
	//DataBase에 기록 
	OnTestSave();
	
	//Display 갱신 한다.
	strTemp = m_wndTestListGrid.GetValueRowCol(nRow, COL_TEST_PRIMARY_KEY);	//Test ID
	UpdateDspTest(m_wndTestListGrid.GetValueRowCol(nRow, COL_TEST_NAME), atoi((LPCSTR)(LPCTSTR)strTemp));

	//////////////////////////////////////////////////////////////////////////
	//Step 등록 시작 
	m_lLoadedBatteryModelID = m_lDisplayModelID;
	m_lLoadedTestID = m_lDisplayTestID;
	
	//cell check 조건 기록 
	TEST_PARAM *pParam;
	pParam = (TEST_PARAM *)(&GetDocument()->m_sPreTestParam);
	
/*	FILE_CELL_CHECK_PARAM cellCheck;
	if(fread(&cellCheck, sizeof(cellCheck), 1, fp)  < 1)					//기록 실시 
	{
		fclose(fp);
		return FALSE;
	}
	ConvertCellCheckData(cellCheck, pParam);
*/	
	CELL_CHECK_DATA * pCellCheck = schData.GetCellCheckParam();
	
	pParam->fMaxVoltage = pCellCheck->fMaxVoltage; 
	pParam->fMinVoltage = pCellCheck->fMinVoltage; 
	pParam->fMaxCurrent = pCellCheck->fMaxCurrent; 
	pParam->fOCVLimitVal = pCellCheck->fVrefVal;			//Vref
	pParam->fTrickleCurrent = pCellCheck->fIrefVal; 		//Iref
	pParam->fDeltaVoltage = pCellCheck->fDeltaVoltage; 
	pParam->lTrickleTime = pCellCheck->lTrickleTime;
	pParam->fMaxFaultNo = pCellCheck->fMaxFaultNo; 
	pParam->bPreTest = pCellCheck->bPreTest;
	
	//화면에 표시 
	DisplayPreTestParam();
		
	CCTSEditorDoc *pDoc = (CCTSEditorDoc *)GetDocument();	//Get Document point
	pDoc->ReMoveStepArray();

	//step 정보 기록 
	STEP *pStep;
	CStep *pSchStep;

	for(int step =0; step<schData.GetStepSize(); step++)
	{
//		if(fread(&stepData, sizeof(FILE_STEP_PARAM), 1, fp) < 1)
//			break;				//기록 실시 

		pSchStep = schData.GetStepData(step);
		if(pSchStep == NULL)	break;

		pStep = new STEP;
		ASSERT(pStep);
		ZeroMemory(pStep, sizeof(STEP));

		ConvertStepData(pSchStep, pStep);
		pDoc->m_apStep.Add((STEP *)pStep);
	}

	//현재 Step Data를 Grid에 표기
	DisplayStepGrid();

	//현재 Step정보를 DataBase 저장한다.
	
	m_nCheckTime = pParam->lTrickleTime/100;
	UpdateData(FALSE);
	
	UpdateData();
	OnStepSave();

	m_bStepDataSaved = TRUE;

//	fclose(fp);
	return TRUE;
}

void CCTSEditorView::OnFileOpen() 
{
	// TODO: Add your command handler code here
	ReadFile();
	
}
/*
BOOL CCTSEditorView::ConvertCellCheckFormat(TEST_PARAM *pParam, FILE_CELL_CHECK_PARAM &newParam)
{
	if(pParam == NULL)	return FALSE;

	newParam.fMaxVoltage = pParam->fMaxVoltage;
	newParam.fMinVoltage = pParam->fMinVoltage;
	newParam.fMaxCurrent = pParam->fMaxCurrent;
	newParam.fVrefVal = pParam->fOCVLimitVal;			//Vref
	newParam.fIrefVal = pParam->fTrickleCurrent;		//Iref
	newParam.fDeltaVoltage = pParam->fDeltaVoltage;
	newParam.lTrickleTime = pParam->OleTrickleTime.GetHour()*3600+pParam->OleTrickleTime.GetMinute()*60+pParam->OleTrickleTime.GetSecond();
	newParam.nMaxFaultNo = pParam->nMaxFaultNo;
	newParam.bPreTest = pParam->bPreTest;

	return TRUE;
}
*/
BOOL CCTSEditorView::ConvertStepFormat(STEP *pStep, FILE_STEP_PARAM &newStep)
{
	if(pStep == NULL)	return FALSE;

	newStep.chStepNo = pStep->chStepNo;
	newStep.chType = pStep->chType;
	newStep.chMode = pStep->chMode;
	newStep.fVref = pStep->fVref;
	newStep.fTref = pStep->fIref;
	newStep.bGrade = pStep->fTref;
	newStep.fEndTime = (pStep->ulEndTime)/100.0f;
	newStep.fEndV = pStep->fEndV;
	newStep.fEndI = pStep->fEndI;
	newStep.fEndC = pStep->fEndC;
	newStep.fEndDV = pStep->fEndDV;						//Step Charge/Discharge Flag
	newStep.fEndDI = pStep->fEndDI;						//Step 충방전시 2번째 I ref 
	newStep.fEndT = pStep->fEndTemp;
	newStep.fVLimitHigh = pStep->fVLimitHigh;
	newStep.fVLimitLow = pStep->fVLimitLow;
	newStep.fILimitHigh = pStep->fILimitHigh;
	newStep.fILimitLow = pStep->fILimitLow;
	newStep.fCLimitHigh = pStep->fCLimitHigh;
	newStep.fCLimitLow = pStep->fCLimitLow;
	newStep.fImpLimitHigh = pStep->fImpLimitHigh;
	newStep.fImpLimitLow = pStep->fImpLimitLow;
	newStep.fHighLimitTemp = pStep->fTempHigh;
	newStep.fLowLimitTemp = pStep->fTempLow;
	newStep.fDeltaTime = (pStep->lDeltaTime)/100.0f;
	newStep.fDeltaTime1 = (pStep->lDeltaTime1)/100.0f;			//Step 충방전시 2번째 End Cap으로 사용 
	newStep.fDeltaV = pStep->fDeltaV;					//Step 충방전시 2번째 End V
	newStep.fDeltaI = pStep->fDeltaI;					//Step 충방전시 2번째 End I
	newStep.bGrade = pStep->bGrade;
	memcpy(&newStep.sGrading_Val, &pStep->sGrading_Val, sizeof(newStep.sGrading_Val));

	for(int i=0; i<SCH_MAX_COMP_POINT && i<3; i++)
	{
		newStep.fCompVLow[i] =  pStep->fCompVLow[i];
		newStep.fCompVHigh[i] =  pStep->fCompVHigh[i];
		newStep.fCompTimeV[i] = (pStep->ulCompTimeV[i])/100.0f;
		newStep.fCompILow[i] = pStep->fCompILow[i];
		newStep.fCompIHigh[i] = pStep->fCompIHigh[i];
		newStep.fCompTimeI[i] = (pStep->ulCompTimeI[i])/100.0f;
	}

	//20071106 deleted
//	newStep.fIEndHigh = pStep->fIEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
//	newStep.fIEndLow = pStep->fIEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교
//	newStep.fVEndHigh = pStep->fVEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
//	newStep.fVEndLow = pStep->fVEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교
//	newStep.nProcType = pStep->nProcType;

	newStep.fReportV = pStep->fReportV;
	newStep.fReportI  = pStep->fReportI;
	newStep.fReportTime  = (pStep->ulReportTime)/100.0f;
	newStep.fReportTemp = pStep->fReportTemp;
	
	newStep.fCapaVoltage1 = pStep->fCapaVoltage1;		//EDLC 용량 검사시 전압 Point1
	newStep.fCapaVoltage2 = pStep->fCapaVoltage2;		//EDLC 용량 검사시 전압 Point2
	newStep.fDCRStartTime = (pStep->fDCRStartTime)/100.0f;		//EDLC DCR 검사시 Start Time
	newStep.fDCREndTime = (pStep->fDCREndTime)/100.0f;			//EDLC DCR 검사시 End Time
	newStep.fLCStartTime = (pStep->fLCStartTime)/100.0f;			//EDLC LC 측정 시작 시간
	newStep.fLCEndTime = (pStep->fLCEndTime)/100.0f;				//EDLC LC 측정 종료 시간
	newStep.lRange = pStep->lRange;						//Range 선택
	newStep.nLoopInfoGoto = pStep->nLoopInfoGoto;
	newStep.nLoopInfoEndVGoto = pStep->nLoopInfoEndVGoto;
	newStep.nLoopInfoEndTGoto = pStep->nLoopInfoEndTGoto;
	newStep.nLoopInfoEndCGoto = pStep->nLoopInfoEndCGoto;
	newStep.nLoopInfoCycle = pStep->nLoopInfoCycle;
	newStep.fPatternMinVal = pStep->fValueLimitLow;
	newStep.fPatternMaxVal = pStep->fValueLimitHigh;

	return TRUE;
}

/*
BOOL CCTSEditorView::ConvertCellCheckData(FILE_CELL_CHECK_PARAM &paramData, TEST_PARAM *pParam)
{
	if(pParam == NULL)	return FALSE;

	pParam->fMaxVoltage = paramData.fMaxVoltage; 
	pParam->fMinVoltage = paramData.fMinVoltage; 
	pParam->fMaxCurrent = paramData.fMaxCurrent; 
	pParam->fOCVLimitVal = paramData.fVrefVal;			//Vref
	pParam->fTrickleCurrent = paramData.fIrefVal; 		//Iref
	pParam->fDeltaVoltage = paramData.fDeltaVoltage; 
	pParam->OleTrickleTime.SetTime(paramData.lTrickleTime/3600, paramData.lTrickleTime/60, paramData.lTrickleTime%60);
	pParam->nMaxFaultNo = paramData.nMaxFaultNo; 
	pParam->bPreTest = paramData.bPreTest;

	return TRUE;
}
*/
BOOL CCTSEditorView::ConvertStepData(CStep *pStepData, STEP *pStep)
{
	if(pStep == NULL || pStepData == NULL)	
		return FALSE;

	int i = 0;

	//step header
	pStep->chStepNo = pStepData->m_StepIndex+1; 
	pStep->nProcType = pStepData->m_lProcType;
	pStep->chType = pStepData->m_type; 
	pStep->chMode = pStepData->m_mode; 
	pStep->fVref = pStepData->m_fVref;
	pStep->fIref = pStepData->m_fIref; 
	pStep->fTref = pStepData->m_fTref;
	
	//Range
	pStep->lRange = pStepData->m_lRange; 						//Range 선택

	//Cycle용
	pStep->nLoopInfoGoto = pStepData->m_nLoopInfoGoto; 
	pStep->nLoopInfoCycle = pStepData->m_nLoopInfoCycle; 
	pStep->nLoopInfoEndVGoto = pStepData->m_nLoopInfoEndVGoto;
	pStep->nLoopInfoEndTGoto = pStepData->m_nLoopInfoEndTGoto;
	pStep->nLoopInfoEndCGoto = pStepData->m_nLoopInfoEndCGoto;

	//end 조건
	pStep->ulEndTime = (pStepData->m_fEndTime)*100.0f;
	pStep->fEndV = pStepData->m_fEndV;
	pStep->fEndI = pStepData->m_fEndI; 
	pStep->fEndC = pStepData->m_fEndC; 
	pStep->fEndDV = pStepData->m_fEndDV; 						//Step Charge/Discharge Flag
	pStep->fEndDI = pStepData->m_fEndDI; 						//Step 충방전시 2번째 I ref 
	pStep->fEndTemp = pStepData->m_fEndT;

	pStep->bUseActualCapa  = pStepData->m_bUseActualCapa;					//현재 Step의 용량값을 기준 용량으로 사용 할지 여부 
	pStep->nUseDataStepNo  = pStepData->m_UseAutucalCapaStepNo;					//비교할 기준 용량이 속한 Step번호
	pStep->fSocRate	 = pStepData->m_fSocRate;						//비교 SOC Rate


	//안전조건
	pStep->fVLimitHigh = pStepData->m_fHighLimitV; 
	pStep->fVLimitLow = pStepData->m_fLowLimitV; 
	pStep->fILimitHigh = pStepData->m_fHighLimitI;
	pStep->fILimitLow = pStepData->m_fLowLimitI; 
	pStep->fCLimitHigh = pStepData->m_fHighLimitC; 
	pStep->fCLimitLow = pStepData->m_fLowLimitC ; 
	pStep->fImpLimitHigh = pStepData->m_fHighLimitImp; 
	pStep->fImpLimitLow = pStepData->m_fLowLimitImp ; 
	pStep->fTempHigh = pStepData->m_fHighLimitTemp; 
	pStep->fTempLow = pStepData->m_fLowLimitTemp ; 
	
	//Delta
	pStep->lDeltaTime = pStepData->m_fDeltaTimeV*100.0f; 
	pStep->lDeltaTime1 = pStepData->m_fDeltaTimeI*100.0f; 			//Step 충방전시 2번째 End Cap으로 사용 
	pStep->fDeltaV = pStepData->m_fDeltaV; 					//Step 충방전시 2번째 End V
	pStep->fDeltaI = pStepData->m_fDeltaI; 					//Step 충방전시 2번째 End I
	
	//등급조건
	pStep->bGrade = pStepData->m_bGrade; 
	GRADE_STEP gradeStep;
	//pStep->sGrading_Val[0].lGradeItem = pStepData->m_Grading.m_lGradingItem;
	//pStep->sGrading_Val[0].lGradeItem = pStepData->m_Grading.GetStepData(0).lGradeItem;
	pStep->sGrading_Val.chTotalGrade = pStepData->m_Grading.GetGradeStepSize();
	
	for(i=0; i<pStepData->m_Grading.GetGradeStepSize(); i++)
	{
		gradeStep = pStepData->m_Grading.GetStepData(i);
		pStep->sGrading_Val.lGradeItem[i] = gradeStep.lGradeItem;
		pStep->sGrading_Val.faValue1[i] = gradeStep.fMin;
		pStep->sGrading_Val.faValue2[i] = gradeStep.fMax;
//		pStep->sGrading_Val.aszGradeCode[i] = gradeStep.strCode[0];
		if(gradeStep.strCode.IsEmpty())
		{
			pStep->sGrading_Val.aszGradeCode[i] = 0;
		}
		else
		{
			pStep->sGrading_Val.aszGradeCode[i] = gradeStep.strCode[0];
		}
	}

	for(i =0; i<SCH_MAX_COMP_POINT && i<MAX_STEP_COMP_SIZE; i++)
	{
		//전압 상승비교

		pStep->fCompVLow[i] = pStepData->m_fCompLowV[i];
		pStep->fCompVHigh[i] =  pStepData->m_fCompHighV[i];
		pStep->ulCompTimeV[i] =  pStepData->m_fCompTimeV[i]*100.0f;

		//전류 상승비교
		pStep->fCompILow[i] =  pStepData->m_fCompLowI[i];
		pStep->fCompIHigh[i] =  pStepData->m_fCompHighI[i];
		pStep->ulCompTimeI[i] =  pStepData->m_fCompTimeI[i]*100.0f;
	}

	//종지값 확인 
	pStep->fIEndHigh = 0.0f;				//충전 CC/CV에서 종료후 전류 크기 비교
	pStep->fIEndLow  = 0.0f;				//충전 CC/CV에서 종료후 전류 크기 비교
	pStep->fVEndHigh = 0.0f;				//방전 CC에서 종료후 전압 크기 비교
	pStep->fVEndLow  = 0.0f;				//방전 CC에서 종료후 전압 크기 비교

	pStep->fValueLimitLow = pStepData->m_fLowLimitA;
	pStep->fValueLimitHigh = pStepData->m_fHighLimitA;

	//기록조건 
	pStep->fReportV	 = pStepData->m_fReportV ;
	pStep->fReportI  = pStepData->m_fReportI ;
	pStep->ulReportTime = pStepData->m_fReportTime*100.0f  ;
	pStep->fReportTemp = pStepData->m_fReportTemp;

	//ELDC용
	pStep->fCapaVoltage1 = pStepData->m_fCapaVoltage1; 		//EDLC 용량 검사시 전압 Point1
	pStep->fCapaVoltage2 = pStepData->m_fCapaVoltage2; 		//EDLC 용량 검사시 전압 Point2
	pStep->fDCRStartTime = pStepData->m_fDCRStartTime*100.0f; 		//EDLC DCR 검사시 Start Time
	pStep->fDCREndTime = pStepData->m_fDCREndTime*100.0f; 			//EDLC DCR 검사시 End Time
	pStep->fLCStartTime = pStepData->m_fLCStartTime*100.0f; 			//EDLC LC 측정 시작 시간
	pStep->fLCEndTime = pStepData->m_fLCEndTime*100.0f;				//EDLC LC 측정 종료 시간
	

	return TRUE;
}



void CCTSEditorView::OnUpdateExcelSave(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(m_lLoadedBatteryModelID);
	
}

void CCTSEditorView::InitParamTab()
{
	// 1. 화면에서 모두 HIDE 한 후 
	// 2. 필요한 항목을 화면에 표시한다.
	m_wndGradeGrid.ShowWindow(SW_HIDE);
	GetDlgItem(IDC_GRADE_CHECK)->ShowWindow(SW_HIDE);		
	GetDlgItem(IDC_V_STATIC)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_VTG_HIGH)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_VTG_LOW)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_I_STATIC)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CRT_HIGH)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CRT_LOW)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAP_STATIC)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAP_HIGH)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAP_LOW)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_IMP_STATIC)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_IMP_HIGH)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_IMP_LOW)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_TEMP_LOW)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_TEMP_HIGH)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_TEMP_STATIC)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_DELTA_V)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_DELTA_I)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_EXT_OPTION_CHECK)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAP_VTG_STATIC)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAP_VTG_LOW)->ShowWindow(SW_HIDE);	
	GetDlgItem(IDC_CAP_VTG_HIGH)->ShowWindow(SW_HIDE);	
	
	ShowExtOptionCtrl(SW_HIDE);
	
	//Tab 0	
	GetDlgItem(IDC_V_STATIC)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_VTG_HIGH)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_VTG_LOW)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_I_STATIC)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_CRT_HIGH)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_CRT_LOW)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_CAP_STATIC)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_CAP_HIGH)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_CAP_LOW)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_IMP_STATIC)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_COMP_TIME1)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_COMP_V5)->ShowWindow(SW_SHOW);		
	GetDlgItem(IDC_COMP_V2)->ShowWindow(SW_SHOW);		
	GetDlgItem(IDC_COMP_TIME2)->ShowWindow(SW_SHOW);			

	if(m_bUseTemp)
	{
		GetDlgItem(IDC_TEMP_STATIC)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_TEMP_LOW)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_TEMP_HIGH)->ShowWindow(SW_SHOW);
	}
	
	GetDlgItem(IDC_IMP_HIGH)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_IMP_LOW)->ShowWindow(SW_SHOW);

#ifdef _FORMATION_
	// GetDlgItem(IDC_EXT_OPTION_CHECK)->ShowWindow(SW_SHOW);	
#endif

#ifdef _EDLC_CELL_		
	GetDlgItem(IDC_CAP_VTG_STATIC)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_CAP_VTG_LOW)->ShowWindow(SW_SHOW);	
	GetDlgItem(IDC_CAP_VTG_HIGH)->ShowWindow(SW_SHOW);
#endif

	if(m_bExtOption)
	{
		ShowExtOptionCtrl(SW_SHOW);
	}
	
	GetDlgItem(IDC_RPT_TEMP_STATIC)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_RPT_I_STATIC)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_RPT_V_STATIC)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_RPT_TUNIT_STATIC)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_OR_STATIC)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_WARRING_STATIC)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_RPT_MSEC_SPIN)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_REPORT_TEMPERATURE)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_REPORT_TIME_MILI)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_REPORT_VOLTAGE)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_REPORT_CURRENT)->ShowWindow(SW_HIDE);
	
	GetDlgItem(IDC_RPT_TIME_STATIC)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_REPORT_TIME)->ShowWindow(SW_SHOW);
	// GetDlgItem(IDC_VI_GET_TIME)->ShowWindow(SW_SHOW);
	// GetDlgItem(IDC_RPT_VI_GET_TIME_STATIC)->ShowWindow(SW_SHOW);

	/*
	if (m_bEditType)
	{
		GetDlgItem(IDC_RPT_TUNIT_STATIC)->ShowWindow(SW_SHOW);
	}
	else
	{
		GetDlgItem(IDC_RPT_TUNIT_STATIC)->ShowWindow(SW_HIDE);
	}
	*/
	
	// GetDlgItem(IDC_RPT_I_STATIC)->ShowWindow(SW_SHOW);
	// GetDlgItem(IDC_RPT_V_STATIC)->ShowWindow(SW_SHOW);
	// GetDlgItem(IDC_REPORT_VOLTAGE)->ShowWindow(SW_SHOW);
	// GetDlgItem(IDC_REPORT_CURRENT)->ShowWindow(SW_SHOW);

#ifndef _FORMATION_		//Formation은 delta time만 적용가능 (Delta V, I등을 적용하기 위해서는 Download program을 수정해야함 )
	if(m_bUseTemp)
	{
		GetDlgItem(IDC_RPT_TEMP_STATIC)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_REPORT_TEMPERATURE)->ShowWindow(SW_SHOW);
	}
	
	GetDlgItem(IDC_OR_STATIC)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_WARRING_STATIC)->ShowWindow(SW_SHOW);
#endif
/*
	m_pTabImageList = new CImageList;

	m_pTabImageList->Create(IDB_TAB_BITMAP, 18, 3, RGB(255,255,255));
	m_ctrlParamTab.SetImageList(m_pTabImageList);

	TC_ITEM item;
	item.mask = TCIF_TEXT|TCIF_IMAGE;
	item.iImage = 0;
	item.pszText = _T("안전조건");
	m_ctrlParamTab.InsertItem(TAB_SAFT_VAL, &item);

	item.iImage = 1;
	item.pszText = "등급조건";
	m_ctrlParamTab.InsertItem(TAB_GRADE_VAL, &item);

	item.iImage = 2;
	item.pszText = "기록조건";
	m_ctrlParamTab.InsertItem(TAB_RECORD_VAL, &item);
*/
}


void CCTSEditorView::OnSelchangeParamTab(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	int nTabNum = m_ctrlParamTab.GetCurSel();

	TRACE("Tab %d Selected\n", nTabNum);
	GetDlgItem(IDC_V_STATIC)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_VTG_HIGH)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_VTG_LOW)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_I_STATIC)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CRT_HIGH)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CRT_LOW)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAP_STATIC)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAP_HIGH)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAP_LOW)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_IMP_STATIC)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_IMP_HIGH)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_IMP_LOW)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_TEMP_LOW)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_TEMP_HIGH)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_TEMP_STATIC)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_DELTA_V)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_DELTA_I)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_EXT_OPTION_CHECK)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAP_VTG_STATIC)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAP_VTG_LOW)->ShowWindow(SW_HIDE);	
	GetDlgItem(IDC_CAP_VTG_HIGH)->ShowWindow(SW_HIDE);	
	GetDlgItem(IDC_COMP_TIME1)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_COMP_TIME2)->ShowWindow(SW_HIDE);		
	GetDlgItem(IDC_COMP_V2)->ShowWindow(SW_HIDE);		
	GetDlgItem(IDC_COMP_V5)->ShowWindow(SW_HIDE);			

	ShowExtOptionCtrl(SW_HIDE);

	m_nCurTabIndex = nTabNum;
	switch(nTabNum)
	{

	case TAB_SAFT_VAL:		//안전 조건 

		//Tab 0		
		GetDlgItem(IDC_V_STATIC)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_VTG_HIGH)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_VTG_LOW)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_I_STATIC)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CRT_HIGH)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CRT_LOW)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CAP_STATIC)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CAP_HIGH)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CAP_LOW)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_IMP_STATIC)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COMP_TIME1)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COMP_V2)->ShowWindow(SW_SHOW);		
		GetDlgItem(IDC_COMP_V5)->ShowWindow(SW_SHOW);		
		GetDlgItem(IDC_COMP_TIME2)->ShowWindow(SW_SHOW);		
		
		if(m_bUseTemp)
		{
			GetDlgItem(IDC_TEMP_STATIC)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_TEMP_LOW)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_TEMP_HIGH)->ShowWindow(SW_SHOW);
		}
//		GetDlgItem(IDC_DELTA_V)->ShowWindow(SW_SHOW);
//		GetDlgItem(IDC_DELTA_I)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_IMP_HIGH)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_IMP_LOW)->ShowWindow(SW_SHOW);

#ifdef _FORMATION_
		// GetDlgItem(IDC_EXT_OPTION_CHECK)->ShowWindow(SW_SHOW);	
#endif

#ifdef _EDLC_CELL_		
		GetDlgItem(IDC_CAP_VTG_STATIC)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CAP_VTG_LOW)->ShowWindow(SW_SHOW);	
		GetDlgItem(IDC_CAP_VTG_HIGH)->ShowWindow(SW_SHOW);
#endif
		if(m_bExtOption)
		{
			ShowExtOptionCtrl(SW_SHOW);
		}

		//Tab 1
		m_wndGradeGrid.ShowWindow(SW_HIDE);
		GetDlgItem(IDC_GRADE_CHECK)->ShowWindow(SW_HIDE);

		//Tab 2
		GetDlgItem(IDC_RPT_TEMP_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RPT_I_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RPT_V_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RPT_TIME_STATIC)->ShowWindow(SW_HIDE);
		// GetDlgItem(IDC_RPT_VI_GET_TIME_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RPT_TUNIT_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_OR_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_WARRING_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RPT_MSEC_SPIN)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_REPORT_TEMPERATURE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_REPORT_TIME_MILI)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_REPORT_TIME)->ShowWindow(SW_HIDE);
		// GetDlgItem(IDC_VI_GET_TIME)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_REPORT_VOLTAGE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_REPORT_CURRENT)->ShowWindow(SW_HIDE);

		break;
	case TAB_GRADE_VAL:		//Grading조건
		
		//Tab 1
		m_wndGradeGrid.ShowWindow(SW_SHOW);
		GetDlgItem(IDC_GRADE_CHECK)->ShowWindow(SW_SHOW);

		//Tab 2
		GetDlgItem(IDC_RPT_TEMP_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RPT_I_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RPT_V_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RPT_TIME_STATIC)->ShowWindow(SW_HIDE);
		// GetDlgItem(IDC_RPT_VI_GET_TIME_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RPT_TUNIT_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_OR_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_WARRING_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RPT_MSEC_SPIN)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_REPORT_TEMPERATURE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_REPORT_TIME_MILI)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_REPORT_TIME)->ShowWindow(SW_HIDE);
		// GetDlgItem(IDC_VI_GET_TIME)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_REPORT_VOLTAGE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_REPORT_CURRENT)->ShowWindow(SW_HIDE);
		
		break;
	
	default:	//TAB_RECORD_VAL	//Reporting 조건

		//Tab 1
		m_wndGradeGrid.ShowWindow(SW_HIDE);
		GetDlgItem(IDC_GRADE_CHECK)->ShowWindow(SW_HIDE);

		//Tab 2
		GetDlgItem(IDC_RPT_TIME_STATIC)->ShowWindow(SW_SHOW);
		// GetDlgItem(IDC_RPT_VI_GET_TIME_STATIC)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_REPORT_TIME)->ShowWindow(SW_SHOW);
		// GetDlgItem(IDC_VI_GET_TIME)->ShowWindow(SW_SHOW);

		/*
		if (m_bEditType)
		{
			GetDlgItem(IDC_RPT_TUNIT_STATIC)->ShowWindow(SW_SHOW);
		}
		else
		{
			GetDlgItem(IDC_RPT_TUNIT_STATIC)->ShowWindow(SW_HIDE);
		}
		*/
		// GetDlgItem(IDC_RPT_I_STATIC)->ShowWindow(SW_SHOW);
		// GetDlgItem(IDC_RPT_V_STATIC)->ShowWindow(SW_SHOW);
		// GetDlgItem(IDC_REPORT_VOLTAGE)->ShowWindow(SW_SHOW);
		// GetDlgItem(IDC_REPORT_CURRENT)->ShowWindow(SW_SHOW);

#ifndef _FORMATION_		//Formation은 delta time만 적용가능 (Delta V, I등을 적용하기 위해서는 Download program을 수정해야함 )
		if(m_bUseTemp)
		{
			GetDlgItem(IDC_RPT_TEMP_STATIC)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_REPORT_TEMPERATURE)->ShowWindow(SW_SHOW);
		}
// 		GetDlgItem(IDC_RPT_I_STATIC)->ShowWindow(SW_SHOW);
// 		GetDlgItem(IDC_RPT_V_STATIC)->ShowWindow(SW_SHOW);
// 		GetDlgItem(IDC_REPORT_VOLTAGE)->ShowWindow(SW_SHOW);
// 		GetDlgItem(IDC_REPORT_CURRENT)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_OR_STATIC)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_WARRING_STATIC)->ShowWindow(SW_SHOW);
// 		GetDlgItem(IDC_RPT_TUNIT_STATIC)->ShowWindow(SW_SHOW);
// 		GetDlgItem(IDC_REPORT_TIME_MILI)->ShowWindow(SW_SHOW);
// 		GetDlgItem(IDC_RPT_MSEC_SPIN)->ShowWindow(SW_SHOW);
#endif
	}	

	STEP *pStep = (STEP *)GetDocument()->GetStepData(m_nDisplayStep);
	if(pStep)	SetApplyAllBtn(pStep->chType);

	if(!UpdateStepGrid(m_nDisplayStep))				//Update Current Display Step Data
	{
		//AfxMessageBox("Step Data Update Fail...");
	}

	*pResult = 0;
}

void CCTSEditorView::SetCompCtrlEnable(BOOL bEnable, BOOL bAll)
{

	if(bAll)
	{
	GetDlgItem(IDC_COMP_V1)->EnableWindow(bEnable);
	// GetDlgItem(IDC_COMP_V2)->EnableWindow(bEnable);
	GetDlgItem(IDC_COMP_V3)->EnableWindow(bEnable);
	GetDlgItem(IDC_COMP_V4)->EnableWindow(bEnable);
	// GetDlgItem(IDC_COMP_V5)->EnableWindow(bEnable);
	GetDlgItem(IDC_COMP_V6)->EnableWindow(bEnable);
	GetDlgItem(IDC_COMP_TIME1)->EnableWindow(bEnable);
	// GetDlgItem(IDC_COMP_TIME2)->EnableWindow(bEnable);
	GetDlgItem(IDC_COMP_TIME3)->EnableWindow(bEnable);

	GetDlgItem(IDC_COMP_I1)->EnableWindow(bEnable);
	GetDlgItem(IDC_COMP_I2)->EnableWindow(bEnable);
	GetDlgItem(IDC_COMP_I3)->EnableWindow(bEnable);
	GetDlgItem(IDC_COMP_I4)->EnableWindow(bEnable);
	GetDlgItem(IDC_COMP_I5)->EnableWindow(bEnable);
	GetDlgItem(IDC_COMP_I6)->EnableWindow(bEnable);
	GetDlgItem(IDC_COMP_I_TIME1)->EnableWindow(bEnable);
	GetDlgItem(IDC_COMP_I_TIME2)->EnableWindow(bEnable);
	GetDlgItem(IDC_COMP_I_TIME3)->EnableWindow(bEnable);
	}

	else
	{		
		GetDlgItem(IDC_COMP_V1)->EnableWindow(bEnable);
		// GetDlgItem(IDC_COMP_V2)->EnableWindow(bEnable);
		GetDlgItem(IDC_COMP_V3)->EnableWindow(bEnable);
		GetDlgItem(IDC_COMP_V4)->EnableWindow(bEnable);
		// GetDlgItem(IDC_COMP_V5)->EnableWindow(bEnable);
		GetDlgItem(IDC_COMP_V6)->EnableWindow(bEnable);
		GetDlgItem(IDC_COMP_TIME1)->EnableWindow(bEnable);
		// GetDlgItem(IDC_COMP_TIME2)->EnableWindow(bEnable);
		GetDlgItem(IDC_COMP_TIME3)->EnableWindow(bEnable);

		GetDlgItem(IDC_COMP_I1)->EnableWindow(!bEnable);
		GetDlgItem(IDC_COMP_I2)->EnableWindow(!bEnable);
		GetDlgItem(IDC_COMP_I3)->EnableWindow(!bEnable);
		GetDlgItem(IDC_COMP_I4)->EnableWindow(!bEnable);
		GetDlgItem(IDC_COMP_I5)->EnableWindow(!bEnable);
		GetDlgItem(IDC_COMP_I6)->EnableWindow(!bEnable);
		GetDlgItem(IDC_COMP_I_TIME1)->EnableWindow(!bEnable);
		GetDlgItem(IDC_COMP_I_TIME2)->EnableWindow(!bEnable);
		GetDlgItem(IDC_COMP_I_TIME3)->EnableWindow(!bEnable);
	}
}


void CCTSEditorView::OnExtOptionCheck() 
{
	// TODO: Add your control notification handler code here
	UpdateData();

	int nShow = SW_SHOW;
	if(m_bExtOption)
	{
		nShow = SW_SHOW;
	}
	else
	{
		nShow = SW_HIDE;

		//ext 값을 Reset 시킨다.
		m_VtgEndHigh.SetValue(0.0);
		m_VtgEndLow.SetValue(0.0);
		m_CrtEndHigh.SetValue(0.0);
		m_CrtEndLow.SetValue(0.0);

		//
		for(int count = 0; count < SCH_MAX_COMP_POINT; count++)
		{
			m_CompV[count].SetValue(0.0);			
			m_CompV1[count].SetValue(0.0);			
			m_CompI[count].SetValue(0.0);			
			m_CompI1[count].SetValue(0.0);
			m_CompTimeV[count].SetValue(0);
			m_CompTimeI[count].SetDateTime((DATE)0);
		}

	}
	ShowExtOptionCtrl(nShow);
}

void CCTSEditorView::ShowExtOptionCtrl(int nShow)
{
/*
	if(nShow == SW_SHOW)
	{
		GetDlgItem(IDC_EXT_OPTION_CHECK)->SetWindowText("추가 옵션 해제 <<<");
	}
	else
	{
		GetDlgItem(IDC_EXT_OPTION_CHECK)->SetWindowText("추가 옵션 설정 >>>");	
	}
*/	
	
	GetDlgItem(IDC_COMP1_STATIC)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP2_STATIC)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP3_STATIC)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP1_STATIC2)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP2_STATIC2)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP3_STATIC2)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP_V1)->ShowWindow(nShow);
	// GetDlgItem(IDC_COMP_V2)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP_V3)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP_V4)->ShowWindow(nShow);
	// GetDlgItem(IDC_COMP_V5)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP_V6)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP_I1)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP_I2)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP_I3)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP_I4)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP_I5)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP_I6)->ShowWindow(nShow);
	// GetDlgItem(IDC_COMP_TIME1)->ShowWindow(nShow);
	// GetDlgItem(IDC_COMP_TIME2)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP_TIME3)->ShowWindow(nShow);
	GetDlgItem(IDC_BAR_STATIC2)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP_I_TIME1)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP_I_TIME2)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP_I_TIME3)->ShowWindow(nShow);

//	GetDlgItem(IDC_END_VOLTAGE_LOW)->ShowWindow(nShow);
//	GetDlgItem(IDC_END_VOLTAGE_HIGH)->ShowWindow(nShow);
//	GetDlgItem(IDC_END_CURRENT_LOW)->ShowWindow(nShow);
//	GetDlgItem(IDC_END_CURRENT_HIGH)->ShowWindow(nShow);	
//	GetDlgItem(IDC_I_END_STATIC)->ShowWindow(nShow);
//	GetDlgItem(IDC_V_END_STATIC)->ShowWindow(nShow);
//	GetDlgItem(IDC_CV_END_STATIC)->ShowWindow(nShow);
//	GetDlgItem(IDC_CC_END_STATIC)->ShowWindow(nShow);	
//	GetDlgItem(IDC_INTER_STATIC1)->ShowWindow(nShow);	
//	GetDlgItem(IDC_INTER_STATIC2)->ShowWindow(nShow);

	GetDlgItem(IDC_LOW_VAL_STATIC)->ShowWindow(nShow);	
	GetDlgItem(IDC_HIGH_VAL_STATIC)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP_TIME_STATIC)->ShowWindow(nShow);
	
	
	GetDlgItem(IDC_INTER_STATIC3)->ShowWindow(nShow);	
	GetDlgItem(IDC_INTER_STATIC4)->ShowWindow(nShow);
	GetDlgItem(IDC_INTER_STATIC5)->ShowWindow(nShow);	
	GetDlgItem(IDC_INTER_STATIC6)->ShowWindow(nShow);	
	GetDlgItem(IDC_INTER_STATIC7)->ShowWindow(nShow);
	GetDlgItem(IDC_INTER_STATIC8)->ShowWindow(nShow);	
	GetDlgItem(IDC_BAR_STATIC3)->ShowWindow(nShow);
}

//EDLC나 Cell용에 따라 모양을 변경한다.
void CCTSEditorView::SetControlCellEdlc()
{
#ifdef _EDLC_CELL_
		//용량 단위를 F를 바꾼다.
		GetDlgItem(IDC_IMP_STATIC)->SetWindowText("ESR(mΩ)");
		GetDlgItem(IDC_IMP_STATIC)->SetWindowText("ESR(mΩ)");
		
#else
		GetDlgItem(IDC_CAP_VTG_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CAP_VTG_LOW)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CAP_VTG_HIGH)->ShowWindow(SW_HIDE);		
#endif

	//Time Control들은 Resource 속성에서 숨김을 하여도 숨겨지지 않으므로
	//Time Control들을 숨김
	GetDlgItem(IDC_DELTA_TIME)->EnableWindow(FALSE);
	
//	GetDlgItem(IDC_REPORT_TIME)->EnableWindow(FALSE);
//	GetDlgItem(IDC_REPORT_TIME_MILI)->EnableWindow(FALSE);
//	GetDlgItem(IDC_RPT_MSEC_SPIN)->EnableWindow(FALSE);

#ifndef _FORMATION_
	// GetDlgItem(IDC_EXT_OPTION_CHECK)->ShowWindow(SW_HIDE);	
#endif

	GetDlgItem(IDC_DELTA_TIME)->ShowWindow(SW_HIDE);
	
	// GetDlgItem(IDC_COMP_TIME1)->ShowWindow(SW_HIDE);
	// GetDlgItem(IDC_COMP_TIME2)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_COMP_TIME3)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_COMP_I_TIME1)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_COMP_I_TIME2)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_COMP_I_TIME3)->ShowWindow(SW_HIDE);	

	if(m_bUseTemp == FALSE)
	{
		GetDlgItem(IDC_TEMP_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_TEMP_LOW)->ShowWindow(SW_HIDE);	
		GetDlgItem(IDC_TEMP_HIGH)->ShowWindow(SW_HIDE);	
	}
//	GetDlgItem(IDC_REPORT_TIME)->ShowWindow(SW_HIDE);
//	GetDlgItem(IDC_REPORT_TIME_MILI)->ShowWindow(SW_HIDE);
//	GetDlgItem(IDC_REPORT_TIME_MILI)->ShowWindow(SW_HIDE);
}

//안전값을 표시한다.
void CCTSEditorView::DisplayStepOption( STEP *pStep)
{
	if(pStep == NULL)
	{
		m_VtgHigh.SetValue(0.0);
		m_VtgLow.SetValue(0.0);
		m_CrtHigh.SetValue(0.0);
		m_CrtLow.SetValue(0.0);
		m_CapHigh.SetValue(0.0);
		m_CapLow.SetValue(0.0);
		m_ImpHigh.SetValue(0.0);
		m_ImpLow.SetValue(0.0);
		m_TempHigh.SetValue(0.0);
		m_TempLow.SetValue(0.0);
		
		m_VIGetTime.SetValue(0);
		
		//
		m_DeltaV.SetValue(0.0);
		m_DeltaI.SetValue(0.0);
		m_DeltaTime.SetTime((DATE)0);

		//
		m_VtgEndHigh.SetValue(0.0);
		m_VtgEndLow.SetValue(0.0);
		m_CrtEndHigh.SetValue(0.0);
		m_CrtEndLow.SetValue(0.0);

		//
		m_ReportTemp.SetValue(0.0);
		m_ReportV.SetValue(0.0);
		m_ReportI.SetValue(0.0);
		m_ReportTime.SetTime((DATE)0);
		
		//
		m_CapVtgLow.SetValue(0.0);	
		m_CapVtgHigh.SetValue(0.0);	
		
		m_DCIR_RegTemp.SetValue(0.0);
		m_DCIR_ResistanceRate.SetValue(0.0);

		for(int count = 0; count < SCH_MAX_COMP_POINT; count++)
		{
			m_CompV[count].SetValue(0.0);			
			m_CompV1[count].SetValue(0.0);			
			m_CompI[count].SetValue(0.0);			
			m_CompI1[count].SetValue(0.0);		
			m_CompTimeV[count].SetValue(0);
			m_CompTimeI[count].SetDateTime((DATE)0);
		}

		m_CompChgCcVtg.SetValue(0.0);
		m_CompChgCcTime.SetValue(0);
		m_CompChgCcDeltaVtg.SetValue(0.0);
		m_CompChgCvCrt.SetValue(0.0);
		m_CompChgCvtTime.SetValue(0);
		m_CompChgCvDeltaCrt.SetValue(0.0);
	}
	else
	{
		m_bExtOption = FALSE;
		//
		m_VtgHigh.SetValue((double)GetDocument()->VUnitTrans(pStep->fVLimitHigh, FALSE));	//pStep->fVLimitHigh/V_UNIT_FACTOR);
		m_VtgLow.SetValue((double)GetDocument()->VUnitTrans(pStep->fVLimitLow, FALSE));	//pStep->fVLimitLow/V_UNIT_FACTOR);
		m_CrtHigh.SetValue((double)GetDocument()->IUnitTrans(pStep->fILimitHigh, FALSE));	//pStep->fILimitHigh/I_UNIT_FACTOR);
		m_CrtLow.SetValue((double)GetDocument()->IUnitTrans(pStep->fILimitLow, FALSE));	//pStep->fILimitLow/I_UNIT_FACTOR);
		m_CapHigh.SetValue((double)GetDocument()->CUnitTrans(pStep->fCLimitHigh, FALSE));	//pStep->fCLimitHigh/C_UNIT_FACTOR);
		m_CapLow.SetValue((double)GetDocument()->CUnitTrans(pStep->fCLimitLow, FALSE));	//pStep->fCLimitLow/C_UNIT_FACTOR);
		
		m_DCIR_RegTemp.SetValue((double)(pStep->fDCIR_RegTemp/10));
		m_DCIR_ResistanceRate.SetValue((double)(pStep->fDCIR_ResistanceRate/100));
				
		m_TempHigh.SetValue((double)pStep->fTempHigh);	//pStep->fCLimitHigh/C_UNIT_FACTOR);
		m_TempLow.SetValue((double)pStep->fTempLow);	//pStep->fCLimitLow/C_UNIT_FACTOR);
		
		m_ImpHigh.SetValue((double)pStep->fImpLimitHigh/Z_UNIT_FACTOR);
		m_ImpLow.SetValue((double)pStep->fImpLimitLow /Z_UNIT_FACTOR);
		
		//
		m_DeltaI.SetValue((double)GetDocument()->IUnitTrans(pStep->fDeltaI, FALSE));	//pStep->fDeltaI/I_UNIT_FACTOR);
		m_DeltaV.SetValue((double)GetDocument()->VUnitTrans(pStep->fDeltaV, FALSE));	//pStep->fDeltaV/V_UNIT_FACTOR);

		//
		m_ReportTemp.SetValue((double)pStep->fReportTemp);
		m_ReportV.SetValue((double)GetDocument()->VUnitTrans(pStep->fReportV, FALSE));	//pStep->fReportV/V_UNIT_FACTOR);
		m_ReportI.SetValue((double)GetDocument()->IUnitTrans(pStep->fReportI, FALSE));	//pStep->fReportI/I_UNIT_FACTOR);

		//
		m_CapVtgLow.SetValue((double)GetDocument()->VUnitTrans(pStep->fCapaVoltage1, FALSE));		//pStep->fCapaVoltage1/V_UNIT_FACTOR);
		m_CapVtgHigh.SetValue((double)GetDocument()->VUnitTrans(pStep->fCapaVoltage2, FALSE));	//pStep->fCapaVoltage2/V_UNIT_FACTOR);
		
		//Optin 영역
		m_VtgEndHigh.SetValue((double)GetDocument()->VUnitTrans(pStep->fVEndHigh, FALSE));	//pStep->fVEndHigh/V_UNIT_FACTOR);
		m_VtgEndLow.SetValue((double)GetDocument()->VUnitTrans(pStep->fVEndLow, FALSE));		//pStep->fVEndLow/V_UNIT_FACTOR);
		m_CrtEndHigh.SetValue((double)GetDocument()->IUnitTrans(pStep->fIEndHigh, FALSE));	//pStep->fIEndHigh/I_UNIT_FACTOR);
		m_CrtEndLow.SetValue((double)GetDocument()->IUnitTrans(pStep->fIEndLow, FALSE));		//pStep->fIEndLow/I_UNIT_FACTOR);
		if(pStep->fVEndHigh > 0 || pStep->fVEndLow > 0 || pStep->fIEndHigh> 0 || pStep->fIEndLow> 0)
		{
			m_bExtOption = TRUE;
		}
		
		COleDateTime time;
		div_t result;
		int hour;
		ULONG lSecond = pStep->lDeltaTime/100;
		result = div(lSecond, 3600);
		hour = result.quot;
		result = div(result.rem, 60) ;
		time.SetTime(hour, result.quot, result.rem);
		m_DeltaTime.SetTime(time);

		lSecond = pStep->ulReportTime/100;
		long lmiliSec = pStep->ulReportTime%100;
		result = div(lSecond, 3600);
		hour = result.quot;
		result = div(result.rem, 60) ;
		time.SetTime(hour, result.quot, result.rem);
		m_ReportTime.SetTime(time);
		
		m_VIGetTime.SetValue( pStep->ulReportVIGetTime/(60*100) );
				
		CString strTemp;
		strTemp.Format("%03d", lmiliSec*10);
		GetDlgItem(IDC_REPORT_TIME_MILI)->SetWindowText(strTemp);

		for(int k = 0; k < SCH_MAX_COMP_POINT; k++)
		{
			m_CompV[k].SetValue((double)GetDocument()->VUnitTrans(pStep->fCompVLow[k], FALSE));		//pStep->fCompVLow[k]/V_UNIT_FACTOR);
			m_CompV1[k].SetValue((double)GetDocument()->VUnitTrans(pStep->fCompVHigh[k], FALSE));	//pStep->fCompVHigh[k]/V_UNIT_FACTOR);
			m_CompI[k].SetValue((double)GetDocument()->IUnitTrans(pStep->fCompILow[k], FALSE));		//pStep->fCompILow[k]/I_UNIT_FACTOR);
			m_CompI1[k].SetValue((double)GetDocument()->IUnitTrans(pStep->fCompIHigh[k], FALSE));	//pStep->fCompIHigh[k]/I_UNIT_FACTOR);
			
			m_CompTimeV[k].SetValue( pStep->ulCompTimeV[k]/(60*100) );
			
			/* SKI 충전하한전압체크시간으로 변경
			lSecond = pStep->ulCompTimeV[k]/100;
			result = div(lSecond, 3600);
			hour = result.quot;
			result = div(result.rem, 60) ;
			time.SetTime(hour, result.quot, result.rem);
			m_CompTimeV[k].SetDateTime(time);
			*/
			
			lSecond = pStep->ulCompTimeI[k]/100;
			result = div(lSecond, 3600);
			hour = result.quot;
			result = div(result.rem, 60) ;
			time.SetTime(hour, result.quot, result.rem);
			m_CompTimeI[k].SetDateTime(time);

			if( pStep->fCompVLow[k] > 0 || pStep->fCompVHigh[k] > 0 || 
				pStep->fCompILow[k] > 0 || pStep->fCompIHigh[k] > 0 ||
				pStep->ulCompTimeV[k] > 0 || pStep->ulCompTimeI[k] > 0
				)
			{
				// SKI 충전하한전압체크시간으로 막음
				// m_bExtOption = TRUE;
			}
		}
				
		//Option에 설정된 값이 있으면 자동으로 표시한다.
		if(m_bExtOption && 	m_nCurTabIndex == TAB_SAFT_VAL)
		{
			ShowExtOptionCtrl(SW_SHOW);	
		}
		else
		{
			ShowExtOptionCtrl(SW_HIDE);	
		}

		m_CompChgCcVtg.SetValue((double)GetDocument()->VUnitTrans(pStep->fCompChgCcVtg, FALSE));
		m_CompChgCcTime.SetValue( pStep->ulCompChgCcTime/100 );
		m_CompChgCcDeltaVtg.SetValue((double)GetDocument()->VUnitTrans(pStep->fCompChgCcDeltaVtg, FALSE));

		m_CompChgCvCrt.SetValue((double)GetDocument()->IUnitTrans(pStep->fCompChgCvCrt, FALSE));
		m_CompChgCvtTime.SetValue( pStep->ulCompChgCvtTime/100 );
		m_CompChgCvDeltaCrt.SetValue((double)GetDocument()->IUnitTrans(pStep->fCompChgCvDeltaCrt, FALSE));

		UpdateData(FALSE);

	}

	DisplayStepGrade(pStep);
}

void CCTSEditorView::SetControlEnable(int nStepType)
{

}

void CCTSEditorView::OnModelEdit() 
{
	// TODO: Add your control notification handler code here
	ModelEdit();
}

void CCTSEditorView::OnFilePrint() 
{
	// TODO: Add your command handler code here
	 CPrntScreen * ScrCap;
     ScrCap = new CPrntScreen("Impossible to print!","Error!");
     ScrCap->DoPrntScreen(1,2,TRUE);
     delete ScrCap;
     ScrCap = NULL;	
}

void CCTSEditorView::SetDefultTest(CString strModel, CString strTest)
{
	if(!strModel.IsEmpty())
	{
		//현재 이름과 match되는 Model ID를 구한다.
		for(int row =0; row<m_pWndCurModelGrid->GetRowCount(); row++)
		{
			if(m_pWndCurModelGrid->GetValueRowCol(row+1, COL_MODEL_NAME) == strModel)
			{

				UpdateDspModel(strModel, atol(m_pWndCurModelGrid->GetValueRowCol(row+1, COL_MODEL_PRIMARY_KEY)));
				m_pWndCurModelGrid->SetCurrentCell(row+1, COL_MODEL_NAME);
				
				if(!strTest.IsEmpty())
				{
					if(RequeryTestList(m_lDisplayModelID, strTest))
					{
						OnLoadTest();
					}
				}
				break;
			}
		}

		AfxGetMainWnd()->ShowWindow(SW_SHOW);
		m_pModelDlg->ShowWindow(SW_HIDE);
	}
}

void CCTSEditorView::OnInsertStep() 
{
	// TODO: Add your command handler code here
	OnStepInsert();
}

void CCTSEditorView::OnDeleteStep() 
{
	// TODO: Add your command handler code here
	OnStepDelete();
}

void CCTSEditorView::OnUpdateInsertStep(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	if(&m_wndStepGrid == (CMyGridWnd *)GetFocus())
	{
		pCmdUI->Enable(TRUE );
	}
	else
	{
		pCmdUI->Enable(FALSE );
	}
	
}

void CCTSEditorView::OnUpdateDeleteStep(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	if(&m_wndStepGrid == (CMyGridWnd *)GetFocus())
	{
		pCmdUI->Enable(TRUE );
	}
	else
	{
		pCmdUI->Enable(FALSE );
	}
	
}

void CCTSEditorView::SetStepCycleColumn()
{
	int nTotStepNum = GetDocument()->GetTotalStepNum();
	STEP *pStep;

	int advCycleStep =1;
//	m_wndStepGrid.StoreCoveredCellsRowCol(1, COL_STEP_CYCLE, nTotStepNum,COL_STEP_CYCLE);
//	ROWCOL nRow, nCol;

	CGXRange covered;
	BOOL bLock = m_wndStepGrid.LockUpdate(TRUE);

//	if (m_wndStepGrid.GetCoveredCellsRowCol(1, COL_STEP_CYCLE, covered))
//	{
//	}

	DWORD CycleCount = 0;
	for(int i = 0; i< nTotStepNum; i++)		//
	{
		m_wndStepGrid.SetCoveredCellsRowCol(i+1, COL_STEP_CYCLE, i+1, COL_STEP_CYCLE);
		
		pStep = (STEP *)GetDocument()->GetStepData(i+1);
		ASSERT(pStep);

		if(pStep->chType < 0 )
		{
			break;
		}
		if( pStep->chType == PS_STEP_ADV_CYCLE)
		{
			advCycleStep = i+1;
//			CycleCount++;
		}
		if(pStep->chType == PS_STEP_LOOP)
		{
			CycleCount++;
		}

		if(pStep->chType != PS_STEP_END && pStep->chType != PS_STEP_ADV_CYCLE)
		{
			m_wndStepGrid.SetCoveredCellsRowCol(advCycleStep, COL_STEP_CYCLE, i+1, COL_STEP_CYCLE);
			m_wndStepGrid.SetValueRange(CGXRange(advCycleStep,COL_STEP_CYCLE), CycleCount);
		//	TRACE("MergeCell1 Row(%d, %d)\n", advCycleStep, i+1);
		}

		if(pStep->chType == PS_STEP_LOOP)
		{
			m_wndStepGrid.SetCoveredCellsRowCol(advCycleStep, COL_STEP_CYCLE, i+1, COL_STEP_CYCLE);
		//	TRACE("MergeCell2 Row(%d, %d)\n", advCycleStep, i+1);
			advCycleStep = i+2;
//			CycleCount++;
		}
		
	}
	m_wndStepGrid.LockUpdate(bLock);
	m_wndStepGrid.Redraw();

}

CString CCTSEditorView::StepTypeMsg(int type)
{
	CString msg("None");
	switch(type)
	{
	case PS_STEP_NONE		:		msg = "None";		break; 
	// case PS_STEP_CHARGE		:		msg = "Charge";		break; 
	case PS_STEP_CHARGE		:		msg = "CHG";		break;
	// case PS_STEP_DISCHARGE	:		msg = "Discharge";	break; 
	case PS_STEP_DISCHARGE	:		msg = "DCHG";	break; 
	case PS_STEP_REST		:		msg = "Rest";		break; 
	case PS_STEP_OCV		:		msg = "OCV";		break; 
	case PS_STEP_IMPEDANCE	:		msg = "DCIR";	break; 
	case PS_STEP_END		:		msg = TEXT_LANG[105];		break; //"완료"
	case PS_STEP_ADV_CYCLE	:		msg = "Cycle";		break; 
	case PS_STEP_LOOP		:		msg = "Loop";		break; 
//	case PS_STEP_PATTERN	:		msg = "Simuation";	break;
	default					:		msg = "None";		break; 
	}
	return msg;
}

CString CCTSEditorView::ModeTypeMsg(int type, int mode)
{
	CString msg;

	if(type == PS_STEP_CHARGE || type == PS_STEP_DISCHARGE)
	{
		switch(mode)
		{
		case PS_MODE_CCCV:		msg = "CC/CV";		break;	
		case PS_MODE_CC	:		msg = "CC";			break;	
		case PS_MODE_CV	:		msg = "CV";			break;	
		case PS_MODE_CP	:		msg = "CP";			break;	
		case PS_MODE_PULSE:		msg = "Pulse";		break;
		case PS_MODE_CR:		msg = "CR";			break;
		}
	}
	else if(type = PS_STEP_IMPEDANCE)
	{
		if(mode == PS_MODE_DCIMP)
		{
			msg = "DC";
		}
		else if(mode == PS_MODE_ACIMP)
		{
			msg = "AC";
		}
	}
	return msg;
}



void CCTSEditorView::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	
	if(m_bStepDataSaved == FALSE || GetDocument()->IsEdited())
	{
		GetDlgItem(IDC_STEP_SAVE)->EnableWindow(TRUE);

	}
	else
	{
		GetDlgItem(IDC_STEP_SAVE)->EnableWindow(FALSE);
	}
	
	CFormView::OnTimer(nIDEvent);
}

void CCTSEditorView::OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView) 
{
	// TODO: Add your specialized code here and/or call the base class
	HideEndDlg();	
	CFormView::OnActivateView(bActivate, pActivateView, pDeactiveView);
}

void CCTSEditorView::OnTestCopyButton() 
{
	// TODO: Add your control notification handler code here

	CRowColArray	awRows;
	m_wndTestListGrid.GetSelectedRows(awRows);
	int nSelNo = awRows.GetSize();

	if(nSelNo <= 0)
	{
		MessageBox(TEXT_LANG[106], TEXT_LANG[107], MB_OK|MB_ICONSTOP);//"선택된 목록이 없습니다."//"실패"
		return;
	}

	CString strMsg;
	CDaoDatabase  db;
	CCTSEditorDoc *pDoc =  GetDocument();
	db.Open(g_strDataBaseName);
	CString name, descript, creator;
	long check, typeID, testNo, lTestID, lFromTestID, lModelID;
	COleDateTime modtime;
	COleVariant data;

	CString strSQL;
	CTestNameDlg	dlg(m_lDisplayModelID, this);
	
	for (int i = 0; i<nSelNo; i++)
	{
		if(awRows[i] > 0)
		{
			if(m_wndTestListGrid.GetValueRowCol(awRows[i], COL_TEST_EDIT_STATE) !=  CS_NEW)
			{
				lFromTestID = atol(m_wndTestListGrid.GetValueRowCol(awRows[i], COL_TEST_PRIMARY_KEY));
				strSQL.Format("SELECT TestNo, ModelID, TestName, Description, Creator, PreTestCheck, ProcTypeID, ModifiedTime FROM TestName WHERE ModelID = %d AND TestID =%d", m_lDisplayModelID, lFromTestID);
				CDaoRecordset rs(&db);
				rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
				if( rs.IsBOF()|| rs.IsEOF())	
				{
					strMsg.Format(TEXT_LANG[108], m_wndTestListGrid.GetValueRowCol(awRows[i], COL_TEST_NAME));//"[%s] 정보를 DataBase에서 찾을 수 없습니다."
					MessageBox(strMsg, TEXT_LANG[107], MB_OK|MB_ICONSTOP);//"실패"
					rs.Close();
					continue;
				}
				data = rs.GetFieldValue(0);		testNo = data.lVal;
				data = rs.GetFieldValue(1);		lModelID = data.lVal;
				data = rs.GetFieldValue(2);		name = data.pbVal;
				data = rs.GetFieldValue(3);		descript = data.pbVal;
				data = rs.GetFieldValue(4);		creator = data.pbVal;
				data = rs.GetFieldValue(5);		check = data.lVal;
				data = rs.GetFieldValue(6);		typeID = data.lVal;
			//	data = rs.GetFieldValue(6);		modtime = data;
				modtime = COleDateTime::GetCurrentTime();
				rs.Close();

				dlg.m_strName = name;
				dlg.m_strDecription = descript;
				dlg.m_strCreator = creator;
				dlg.m_createdDate = modtime;				
				dlg.m_lTestTypeID = typeID;
				if(IDOK != dlg.DoModal())
				{
					continue;
				}

				//Type이 바뀌었으면 
				if(typeID != dlg.m_lTestTypeID)
				{
					//Step을 검사하여 시행 불가능한 Step이 포함되어 있으면 경고 표시
					if(CheckExecutableStep( GetTestIndexFromType(dlg.m_lTestTypeID), lFromTestID, lModelID) == FALSE)
					{
						strMsg.Format(TEXT_LANG[109],//"복사할 공정[%s]에서 실시할 수 없는 Step을 포함하고 있습니다.\n복사된 공정에서 시행할수 없는 Step은 삭제되거나 수정되어 합니다.\n\n공정을 복사 하시겠습니까?"
							dlg.m_strName);
						if(MessageBox(strMsg, "Error", MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2) == IDNO)
						{
							continue;
						}
					}
				}
				
				int nCount = m_wndTestListGrid.GetRowCount()+1;
/*
				strSQL.Format("INSERT INTO TestName(ModelID, TestName, Description, Creator, PreTestCheck, ProcTypeID, TestNo, ModifiedTime) VALUES (%d, '%s', '%s', '%s', %d, %d, %d, '%s')",
						dlg.m_lModelID, dlg.m_strName, dlg.m_strDecription, dlg.m_strCreator, check, typeID, nCount, modtime.Format());
*/
				db.Execute(strSQL);

					//Insert 된 test의 ID를 구한다.
				strSQL = "SELECT MAX(TestID) FROM TestName";
				CDaoRecordset rs1(&db);
				rs1.Open(dbOpenSnapshot, strSQL, dbReadOnly);
				if(!rs1.IsBOF())
				{
					lTestID = rs1.GetFieldValue(0).lVal;
				}
				rs1.Close();

				if(CopyCheckParam(lFromTestID, lTestID, lModelID) == FALSE)
				{
					AfxMessageBox(TEXT_LANG[110]);//"공정정보 복사간 오류가 발생하였습니다( ProtectSetting. 접촉검사 )"
				}
				if(CopyStepList(lFromTestID, lTestID, lModelID))
				{
				}
				
				/*
				if(dlg.m_lModelID == m_lDisplayModelID)
				{
					//복사 성공시 추가 
					m_wndTestListGrid.InsertRows(nCount, 1);
					m_wndTestListGrid.SetValueRange(CGXRange(nCount, COL_TEST_PROC_TYPE), (ROWCOL)GetTestIndexFromType(dlg.m_lTestTypeID));	
					m_wndTestListGrid.SetValueRange(CGXRange(nCount, COL_TEST_NAME), dlg.m_strName);
					m_wndTestListGrid.SetValueRange(CGXRange(nCount, COL_TEST_CREATOR), dlg.m_strCreator);
					m_wndTestListGrid.SetValueRange(CGXRange(nCount, COL_TEST_DESCRIPTION), dlg.m_strDecription);
					m_wndTestListGrid.SetValueRange(CGXRange(nCount, COL_TEST_EDIT_TIME), dlg.m_createdDate.Format());
					m_wndTestListGrid.SetValueRange(CGXRange(nCount, COL_TEST_PRIMARY_KEY), lTestID);
					m_wndTestListGrid.SetValueRange(CGXRange(nCount, COL_TEST_EDIT_STATE), CS_SAVED);
					m_wndTestListGrid.SetCurrentCell(nCount, 1);
				}
				*/
			}
		}
	}
	
	db.Close();
	GetDocument()->m_bEditedFlag = TRUE;		//DataBase 변경 Flag (BackUp을 위해)	
	
	return;	
}

//////////////////////////////////////////////////////////////////////////
//Record 조건 변경 검사 
void CCTSEditorView::OnChangeReportCurrent() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnChangeReportTemperature() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnChangeReportVoltage() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnDatetimechangeReportTime(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	m_bStepDataSaved = FALSE;
	*pResult = 0;
}
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
//Cell Check 조건 변경 검사 
void CCTSEditorView::OnChangePretestMaxv() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnChangePretestDeltaV() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnChangePretestFaultNo() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnChangePretestMaxi() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnChangePretestMinv() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnChangePretestOcv() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnChangePretestTrckleI() 
{
	m_bStepDataSaved = FALSE;
}

LRESULT CCTSEditorView::OnDateTimeChanged(WPARAM wParam, LPARAM lParam)
{
	m_bStepDataSaved = FALSE;
	return 1;
}
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
//안전 조건 
void CCTSEditorView::OnChangeVtgHigh() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnChangeVtgLow() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnChangeCrtHigh() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnChangeCrtLow() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnChangeCapHigh() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnChangeCapLow() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnChangeImpHigh() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnChangeImpLow() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnChangeDeltaV() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnChangeDeltaI() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnChangeDeltaTime() 
{
	m_bStepDataSaved = FALSE;
}
void CCTSEditorView::OnChangeCapVtgLow() 
{
	m_bStepDataSaved = FALSE;	
}

void CCTSEditorView::OnChangeCapVtgHigh() 
{
	m_bStepDataSaved = FALSE;	
}

void CCTSEditorView::OnChangeCompITime1() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnChangeCompITime2() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnChangeCompITime3() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnChangeCompI1() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnChangeCompI2() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnChangeCompI3() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnChangeCompI4() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnChangeCompI5() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnChangeCompI6() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnChangeCompV1() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnChangeCompV2() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnChangeCompV3() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnChangeCompTime1() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnChangeCompTime2() 
{
	m_bStepDataSaved = FALSE;	
}

void CCTSEditorView::OnChangeCompTime3() 
{
	m_bStepDataSaved = FALSE;
}
void CCTSEditorView::OnChangeCompV4() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnChangeCompV5() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnChangeCompV6() 
{
	m_bStepDataSaved = FALSE;	
}

void CCTSEditorView::AddUndoStep()
{
	GetDocument()->MakeUndoStep();
	m_bPasteUndo = TRUE;		//삭제 Undo 가능 하게 
}


void CCTSEditorView::OnDeltaposRptMsecSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	// TODO: Add your control notification handler code here
	
	CString strData;
	GetDlgItem(IDC_REPORT_TIME_MILI)->GetWindowText(strData);
	
	int mtime = atol(strData);
	if(pNMUpDown->iDelta > 0)
	{
		mtime -= MIN_TIME_INTERVAL;
		if(mtime < 0)	mtime = (1000-MIN_TIME_INTERVAL);
	}
	else
	{
		mtime += MIN_TIME_INTERVAL;
		if(mtime >= 1000)	mtime = 0;
	}
	strData.Format("%03d", mtime);
	GetDlgItem(IDC_REPORT_TIME_MILI)->SetWindowText(strData);	
	
	m_bStepDataSaved = FALSE;

	strData.Empty();
	if(mtime > 0)
	{
		strData.Format(TEXT_LANG[111], mtime*SCH_MAX_MSEC_DATA_POINT/1000, mtime);//"Step 시작부터 최대 %d초간 %dmsec 간격으로 기록 후 1초간격으로 자동 전환하여 저장"
	}
//	GetDlgItem(IDC_WARRING_STATIC)->SetWindowText(strData);
	m_strWarning.SetTextColor(RGB(255, 0 ,0));

	*pResult = 0;
}


void CCTSEditorView::OnChangeCellCheckTimeEdit() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	m_bStepDataSaved = FALSE;	
}

void CCTSEditorView::OnAllStepButton() 
{
	// TODO: Add your control notification handler code here
	
	//현재 설정된 조건을 모든 step에 적용 한다.
	UpdateStepGrid(m_nDisplayStep);

	ApplySelectStep();
	m_bStepDataSaved = FALSE;	

}

void CCTSEditorView::OnSameAllStepButton() 
{
	// TODO: Add your control notification handler code here
	
	//현재 설정된 조건을 모든 step에 적용 한다.
	UpdateStepGrid(m_nDisplayStep);

	STEP *pStep = GetDocument()->GetStepData(m_nDisplayStep);
	if(pStep)
	{
		ApplySelectStep(pStep->chType);
		m_bStepDataSaved = FALSE;	
	}
}

void CCTSEditorView::ApplySelectStep(int nStepType)
{
	STEP *pStep = NULL;
	STEP *pCurStep = GetDocument()->GetStepData(m_nDisplayStep);
	if(pCurStep == NULL)	return;

	int a;
	for(int step=0; step<GetDocument()->GetTotalStepNum(); step++)
	{
		pStep = GetDocument()->GetStepData(step+1);
		if(pStep == NULL)	break;

		//같은 step만 복사
		if(nStepType > 0 && nStepType != pStep->chType)		continue;
		
		pStep->fVLimitHigh = pCurStep->fVLimitHigh;
		pStep->fVLimitLow = pCurStep->fVLimitLow;
		pStep->fILimitHigh = pCurStep->fILimitHigh;
		pStep->fILimitLow = pCurStep->fILimitLow;
		pStep->fCLimitHigh = pCurStep->fCLimitHigh;
		pStep->fCLimitLow = pCurStep->fCLimitLow;
		
		for(a =0; a<SCH_MAX_COMP_POINT; a++)
		{
			pStep->fCompVHigh[a] = pCurStep->fCompVHigh[a];
			pStep->fCompVLow[a] = pCurStep->fCompVLow[a];
			pStep->ulCompTimeV[a] = pCurStep->ulCompTimeV[a];

			pStep->fCompIHigh[a] = pCurStep->fCompIHigh[a];
			pStep->fCompILow[a] = pCurStep->fCompILow[a];
			pStep->ulCompTimeI[a] = pCurStep->ulCompTimeI[a];
		}

		pStep->fCapaVoltage1 = pCurStep->fCapaVoltage1;			//EDLC 용량 검사시 전압 Point1
		pStep->fCapaVoltage2 = pCurStep->fCapaVoltage2;			//EDLC 용량 검사시 전압 Point2
		pStep->fLCStartTime = pCurStep->fLCStartTime;			//EDLC LC 측정 시작 시간
		pStep->fLCEndTime = pCurStep->fLCEndTime;				//EDLC LC 측정 종료 시간
		pStep->fTempHigh = pCurStep->fTempHigh;				
		pStep->fTempLow = pCurStep->fTempLow;
		
		pStep->fVLimitHigh = pCurStep->fVLimitHigh;
		pStep->fVLimitLow = pCurStep->fVLimitLow;
		
		pStep->fVLimitHigh = pCurStep->fVLimitHigh;
		pStep->fVLimitLow = pCurStep->fVLimitLow;
		pStep->fILimitHigh = pCurStep->fILimitHigh;
		pStep->fILimitLow = pCurStep->fILimitLow;
		pStep->fImpLimitHigh = pCurStep->fImpLimitHigh;
		pStep->fImpLimitLow = pCurStep->fImpLimitLow;
		pStep->fDCRStartTime = pCurStep->fDCRStartTime;			//EDLC DCR 검사시 Start Time
		pStep->fDCREndTime = pCurStep->fDCREndTime;			//EDLC DCR 검사시 End Time
		
		pStep->fReportV = pCurStep->fReportV;
		pStep->fReportI = pCurStep->fReportI;
		pStep->ulReportTime = pCurStep->ulReportTime;
		pStep->fReportTemp = pCurStep->fReportTemp;

		pStep->fCompChgCcVtg = pCurStep->fCompChgCcVtg;
		pStep->ulCompChgCcTime = pCurStep->ulCompChgCcTime;
		pStep->fCompChgCcDeltaVtg = pCurStep->fCompChgCcDeltaVtg;
		pStep->fCompChgCvCrt = pCurStep->fCompChgCvCrt;
		pStep->ulCompChgCvtTime = pCurStep->ulCompChgCvtTime;
		pStep->fCompChgCvDeltaCrt = pCurStep->fCompChgCvDeltaCrt;
		
		/*

		switch(m_nCurTabIndex)
		{
		case TAB_SAFT_VAL	:		//안전 조건 
			{
				switch(pStep->chType)
				{
				case PS_STEP_CHARGE:
				case PS_STEP_DISCHARGE:
//				case PS_STEP_PATTERN:
					pStep->fVLimitHigh = pCurStep->fVLimitHigh;
					pStep->fVLimitLow = pCurStep->fVLimitLow;
					pStep->fILimitHigh = pCurStep->fILimitHigh;
					pStep->fILimitLow = pCurStep->fILimitLow;
					pStep->fCLimitHigh = pCurStep->fCLimitHigh;
					pStep->fCLimitLow = pCurStep->fCLimitLow;
									
					for(a =0; a<SCH_MAX_COMP_POINT; a++)
					{
						pStep->fCompVHigh[a] = pCurStep->fCompVHigh[a];
						pStep->fCompVLow[a] = pCurStep->fCompVLow[a];
						pStep->ulCompTimeV[a] = pCurStep->ulCompTimeV[a];

						pStep->fCompIHigh[a] = pCurStep->fCompIHigh[a];
						pStep->fCompILow[a] = pCurStep->fCompILow[a];
						pStep->ulCompTimeI[a] = pCurStep->ulCompTimeI[a];
					}

					pStep->fCapaVoltage1 = pCurStep->fCapaVoltage1;			//EDLC 용량 검사시 전압 Point1
					pStep->fCapaVoltage2 = pCurStep->fCapaVoltage2;			//EDLC 용량 검사시 전압 Point2
					pStep->fLCStartTime = pCurStep->fLCStartTime;			//EDLC LC 측정 시작 시간
					pStep->fLCEndTime = pCurStep->fLCEndTime;				//EDLC LC 측정 종료 시간
					pStep->fTempHigh = pCurStep->fTempHigh;				
					pStep->fTempLow = pCurStep->fTempLow;				
					break;

				case PS_STEP_REST:		//Voltage
				case PS_STEP_OCV:
					pStep->fVLimitHigh = pCurStep->fVLimitHigh;
					pStep->fVLimitLow = pCurStep->fVLimitLow;
					break;
					
				case PS_STEP_IMPEDANCE:
					pStep->fVLimitHigh = pCurStep->fVLimitHigh;
					pStep->fVLimitLow = pCurStep->fVLimitLow;
					pStep->fILimitHigh = pCurStep->fILimitHigh;
					pStep->fILimitLow = pCurStep->fILimitLow;
					pStep->fImpLimitHigh = pCurStep->fImpLimitHigh;
					pStep->fImpLimitLow = pCurStep->fImpLimitLow;
					pStep->fDCRStartTime = pCurStep->fDCRStartTime;			//EDLC DCR 검사시 Start Time
					pStep->fDCREndTime = pCurStep->fDCREndTime;			//EDLC DCR 검사시 End Time
					
					break;

				case PS_STEP_LOOP:
				case PS_STEP_END:	
				case PS_STEP_ADV_CYCLE:
				default:
					break;
				}
				break;
			}

		case TAB_GRADE_VAL	:		//등급 조건 (같은 종류step만 지원)
			if(pStep->chType == nStepType)
			{
				switch(pStep->chType)
				{
				case PS_STEP_CHARGE:			//용량 Grading
				case PS_STEP_DISCHARGE:
				case PS_STEP_IMPEDANCE:
				case PS_STEP_OCV:
					//copy grade condition
					pStep->bGrade =  pCurStep->bGrade;
					for(a =0; a<SCH_MAX_GRADE_STEP; a++)
					{
						pStep->sGrading_Val.faValue1[a] = pCurStep->sGrading_Val.faValue1[a];
						pStep->sGrading_Val.faValue2[a] = pCurStep->sGrading_Val.faValue2[a];
						pStep->sGrading_Val.lGradeItem[a] = pCurStep->sGrading_Val.lGradeItem[a];
						pStep->sGrading_Val.aszGradeCode[a] =  pCurStep->sGrading_Val.aszGradeCode[a];
					}
					pStep->sGrading_Val.chTotalGrade = pCurStep->sGrading_Val.chTotalGrade;
					break;
		
				case PS_STEP_REST:
				case PS_STEP_LOOP:
				case PS_STEP_END:	
				case PS_STEP_ADV_CYCLE:
				default:
					break;
				}
				break;
			}

		case TAB_RECORD_VAL	:		//기록 조건 
			{
				switch(pStep->chType)
				{
				case PS_STEP_CHARGE:
				case PS_STEP_DISCHARGE:
				case PS_STEP_REST:
				case PS_STEP_IMPEDANCE:
// 				case PS_STEP_PATTERN:
					pStep->fReportV = pCurStep->fReportV;
					pStep->fReportI = pCurStep->fReportI;
					pStep->ulReportTime = pCurStep->ulReportTime;
					pStep->fReportTemp = pCurStep->fReportTemp;
					break;

				case PS_STEP_OCV:
				case PS_STEP_LOOP:
				case PS_STEP_END:	
				case PS_STEP_ADV_CYCLE:
				default:
					break;
				}
			}
			break;
		}
		*/
	}
}

void CCTSEditorView::SetApplyAllBtn(int nType)
{
	GetDlgItem(IDC_ALL_STEP_BUTTON)->EnableWindow(TRUE);
	GetDlgItem(IDC_SAME_ALL_STEP_BUTTON)->EnableWindow(TRUE);
	CString strTemp, strMsg;
	switch(m_nCurTabIndex)
	{
	case TAB_SAFT_VAL	:		//안전 조건 
		strTemp = TEXT_LANG[112];//"안전"
		GetDlgItem(IDC_ALL_STEP_BUTTON)->SetWindowText(TEXT_LANG[113]);//"현재 안전설정을 모든 Step에 적용"
		break;
	case TAB_GRADE_VAL	:		//등급 조건 
		strTemp = TEXT_LANG[114];//"등급"
		GetDlgItem(IDC_ALL_STEP_BUTTON)->EnableWindow(FALSE);
		GetDlgItem(IDC_ALL_STEP_BUTTON)->SetWindowText(TEXT_LANG[115]);//"현재 등급설정을 모든 Step에 적용"
		break;
	case TAB_RECORD_VAL	:		//기록 조건 
		strTemp = TEXT_LANG[116];//"기록"
		GetDlgItem(IDC_ALL_STEP_BUTTON)->SetWindowText(TEXT_LANG[117]);//"현재 기록설정을 모든 Step에 적용"
		break;
	}

	switch(nType)
	{
	case PS_STEP_CHARGE:
		strMsg.Format(TEXT_LANG[118],strTemp);//"현재 %s설정을 모든 [충전]에 적용"
		break;

	case PS_STEP_DISCHARGE:
		strMsg.Format(TEXT_LANG[119],strTemp);//"현재 %s설정을 모든 [방전]에 적용"
		break;

	case PS_STEP_REST:
		if(m_nCurTabIndex == TAB_GRADE_VAL)
		{
			GetDlgItem(IDC_SAME_ALL_STEP_BUTTON)->EnableWindow(FALSE);
		}
		strMsg.Format(TEXT_LANG[120],strTemp);//"현재 %s설정을 모든 [Rest]에 적용"
		break;
		
	case PS_STEP_IMPEDANCE:
		strMsg.Format(TEXT_LANG[121],strTemp);//"현재 %s설정을 모든 [Impedance]에 적용"
		break;

	case PS_STEP_OCV:
		strMsg.Format(TEXT_LANG[122],strTemp);//"현재 %s설정을 모든 [OCV]에 적용"
		break;

// 	case PS_STEP_PATTERN:
// 		strMsg.Format("현재 %s설정을 모든 [Pattern]에 적용", strTemp);
// 		break;

	case PS_STEP_LOOP:
	case PS_STEP_END:	
	case PS_STEP_ADV_CYCLE:
	default:
		strMsg = "...";
		GetDlgItem(IDC_SAME_ALL_STEP_BUTTON)->EnableWindow(FALSE);
		GetDlgItem(IDC_ALL_STEP_BUTTON)->EnableWindow(FALSE);
		break;
	}
	GetDlgItem(IDC_SAME_ALL_STEP_BUTTON)->SetWindowText(strMsg);

}


void CCTSEditorView::OnChangeTempLow() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	m_bStepDataSaved = FALSE;	
}

void CCTSEditorView::OnChangeTempHigh() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	m_bStepDataSaved = FALSE;	
}

//현재 공정에서 주어진 Type의 mode가 진행가능하진 검사 
BOOL CCTSEditorView::IsExecutableStep(int nTypeComboIndex, WORD type, WORD mode)
{
	//Mask 정보가 없거나 잘못 되었을 경우는 모두 실행 가능하도록 
	if(m_strTypeMaskArray.GetSize() < 0)	return TRUE;
	if(nTypeComboIndex < 0 || nTypeComboIndex >= m_strTypeMaskArray.GetSize())		return TRUE;
	
	CString strMsk;
	strMsk = m_strTypeMaskArray.GetAt(nTypeComboIndex);

	CString strTemp(strMsk);
	strMsk = strTemp.Mid(strTemp.Find('-')+1);

	//Mask가 지정되어 있지 않으면 모두 실행 가능 
	if(strMsk.IsEmpty())		return TRUE;
	
	strMsk.MakeUpper();
	//공정 Type(A, P, F, H, F, S, G)과 
	//시행가능한 Step Type (CODERIZ) C: Charge, D: Discharge, Z: AC Impedance, R: DC Impedance, O: OCV, I:Rest, E:End, S:Simulation ) NULL이면 모든 Step type 수행가능
	switch(type)
	{
	case PS_STEP_CHARGE:
		if(strMsk.Find('C') < 0)		return FALSE;
		break;

	case PS_STEP_DISCHARGE:
		if(strMsk.Find('D') < 0)		return FALSE;
		break;

	case PS_STEP_OCV:
		if(strMsk.Find('O') < 0)		return FALSE;
		break;

	case PS_STEP_IMPEDANCE:
		if(mode == PS_MODE_ACIMP)
		{
			if(strMsk.Find('Z') < 0)	return FALSE;
		}
		else if(mode == PS_MODE_DCIMP)
		{
			if(strMsk.Find('R') < 0)	return FALSE;
		}
		break;

	case PS_STEP_REST:
		if(strMsk.Find('I') < 0)		return FALSE;
		break;

// 	case PS_STEP_PATTERN:
// 		if(strMsk.Find('S') < 0)		return FALSE;
// 		break;

	//End는 무조건 가능 
	case PS_STEP_END:		
	default:	
		break;
	}

	return TRUE;
}

//주어진 시험에 가능한 Step만 포함되어 있는지 검사 
BOOL CCTSEditorView::CheckExecutableStep(int nTypeComboIndex, long lTestID, long lModelID)
{
	//현재 Loading되어 있으면 Loading된 Step과 검사하고 
	if(m_lLoadedTestID == lTestID)
	{
		CCTSEditorDoc *pDoc = (CCTSEditorDoc *)GetDocument();
		int nTotStepNum = pDoc->GetTotalStepNum();
		for(int i = 0; i<nTotStepNum; i++)
		{
			STEP *pStep = (STEP *)pDoc->GetStepData(i+1);		
			if(IsExecutableStep(nTypeComboIndex, pStep->chType, pStep->chMode) == FALSE)	
			{
				return FALSE;
			}
		}
	}
	//다를 경우는 DataBase에서 Loading하여 검사한다.
	else
	{
		CDaoDatabase  db;
		db.Open(GetDataBaseName());

		CString strSQL;
		CStepRecordSet rs;
		rs.m_strFilter.Format("[TestID] = %d AND [ModelID] = %ld", lTestID, lModelID);
		rs.m_strSort = "[StepNo]";
		rs.Open();
		while(!rs.IsEOF())
		{
			if(IsExecutableStep(nTypeComboIndex, rs.m_StepType, rs.m_StepMode) == FALSE)	
			{
				rs.Close();
				return FALSE;
			}
			rs.MoveNext();
		}
		rs.Close();
	}
	return TRUE;
}

void CCTSEditorView::NewModel()
{
	// TODO: Add your control notification handler code here
	if(GetDocument()->PermissionCheck(PMS_EDITOR_EDIT) ==  FALSE)
	{
		AfxMessageBox(TEXT_LANG[84]);//"수정 권한이 없습니다."
		return;
	}

	//Dialog 입력 받도록 수정 /////////////////
	CModelNameDlg	dlg;
	dlg.m_strUserID = GetDocument()->m_LoginData.szLoginID;
	if(IDOK != dlg.DoModal())
	{
		return;
	}

	//중복된 이름 사용 못하도록 한다.
	for(int i =0; i<m_pWndCurModelGrid->GetRowCount(); i++)
	{
		if( dlg.GetName() == m_pWndCurModelGrid->GetValueRowCol(i+1, COL_MODEL_NAME))
		{
			CString strtemp;
			strtemp.Format(TEXT_LANG[123], dlg.GetName());//"%s는 이미 등록되어 있습니다.\n"
			AfxMessageBox(strtemp);
			return;
		}
	}
	//////////////////////////////////////////

	ROWCOL nRow = m_pWndCurModelGrid->GetRowCount()+1;

	m_pWndCurModelGrid->InsertRows(nRow, 1);
	m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_EDIT_STATE), CS_NEW);
	m_pWndCurModelGrid->SetCurrentCell(nRow, 1);

	//dialog 입력 닫아서 자동으로 저장하도록 수정 /////
	m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_NAME), dlg.GetName());
	m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_DESCRIPTION), dlg.GetDescript());
	m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_CREATOR), dlg.m_strUserID);
	//DataBase에 모델 추가 
	OnModelSave();
	////////////////////////////////////////////////////
	
//	GetDlgItem(IDC_MODEL_COPY_BUTTON)->EnableWindow(FALSE);
}

void CCTSEditorView::ModelDelete()
{
	if(GetDocument()->PermissionCheck(PMS_EDITOR_EDIT) ==  FALSE)
	{
		AfxMessageBox(TEXT_LANG[84]);//"수정 권한이 없습니다."
		return;
	}

	CString		strTemp;
	CBatteryModelRecordSet	recordSet;
	ROWCOL nRow = m_pWndCurModelGrid->GetRowCount();
	if(nRow <1)		return;

	CRowColArray	awRows;
	m_pWndCurModelGrid->GetSelectedRows(awRows);
	int nSelSize = awRows.GetSize();
	if( nSelSize <= 0)	return;

	if(nSelSize == 1)
	{
		strTemp.Format(TEXT_LANG[124], m_pWndCurModelGrid->GetValueRowCol(awRows[0], COL_MODEL_NAME));//"[%s]을 삭제 하시겠습니까?"
	}
	else
	{
		strTemp.Format(TEXT_LANG[125], nSelSize);//"선택한 %d개의 공정목록 정보를 삭제 하시겠습니까?"
	}

	if(IDNO == MessageBox(strTemp, TEXT_LANG[126], MB_YESNO|MB_ICONQUESTION))	//"목록 삭제"
		return;
	
	try
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return ;
	}

	LONG lDeleteModelID;
	int i = 0;

	for (i = nSelSize-1; i >=0 ; i--)
	{
		strTemp = m_pWndCurModelGrid->GetValueRowCol(awRows[i], COL_MODEL_PRIMARY_KEY);
		if(!strTemp.IsEmpty())
		{
			strTemp = "ModelID = " + strTemp;
			if(recordSet.Find(AFX_DAO_FIRST, strTemp))
			{
				lDeleteModelID = recordSet.m_ModelID;

				// Simulation 파일이 존재하면 삭제한다.///////////////////////////
				DeleteSimulationFile(lDeleteModelID);
				//////////////////////////////////////////////////////////////////////////
				recordSet.Delete();
				
				if(m_lLoadedBatteryModelID == lDeleteModelID)
				{
					RequeryTestList(m_lLoadedBatteryModelID);
					ReLoadStepData(m_lLoadedTestID, m_lLoadedBatteryModelID);
					ClearStepState();
				}
			}
		}
		m_pWndCurModelGrid->RemoveRows(awRows[i], awRows[i]);
	}

	//순서 저장 2002/10/02
	for( i = 0; i<m_pWndCurModelGrid->GetRowCount(); i++)
	{
		strTemp = m_pWndCurModelGrid->GetValueRowCol(i+1, COL_MODEL_PRIMARY_KEY);		//Get TestID
		if(!strTemp.IsEmpty())										//Delete Test Condition Form DB	
		{
			strTemp = "ModelID = " + strTemp;
			if(recordSet.Find(AFX_DAO_FIRST, strTemp))
			{
				recordSet.Edit();
				recordSet.Update();
			}
		}
	}

	recordSet.Close();
	// RequeryBatteryModel();
	
	GetDlgItem(IDC_MODEL_COPY_BUTTON)->EnableWindow(TRUE);
	GetDocument()->m_bEditedFlag = TRUE;		//DataBase 변경 Flag (BackUp을 위해)
}

void CCTSEditorView::ModelEdit()
{
	ROWCOL nRow, nCol;

	if(m_pWndCurModelGrid->GetCurrentCell(nRow, nCol) == FALSE)
	{
		return;
	}
	if(nRow < 1)	
	{
		return;
	}

	CModelNameDlg	dlg;
	dlg.m_strName = m_pWndCurModelGrid->GetValueRowCol(nRow, COL_MODEL_NAME);
	dlg.m_strDescript = m_pWndCurModelGrid->GetValueRowCol(nRow, COL_MODEL_DESCRIPTION);
	dlg.m_strUserID = m_pWndCurModelGrid->GetValueRowCol(nRow, COL_MODEL_CREATOR);

	if(IDOK != dlg.DoModal())
	{
		return;
	}

	m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_NAME), dlg.GetName());
	m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_DESCRIPTION), dlg.GetDescript());
	m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_CREATOR), dlg.m_strUserID);
	m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_EDIT_STATE), CS_EDIT);

	OnModelSave();
}

void CCTSEditorView::OnTestEdit() 
{
	// TODO: Add your control notification handler code here
	/*
	ROWCOL nRow, nCol;

	if(m_wndTestListGrid.GetCurrentCell(nRow, nCol) == FALSE)
	{
		return;
	}
	if(nRow < 1)	
	{
		return;
	}


	CTestNameDlg	dlg(m_lDisplayModelID);
	dlg.m_strName = m_wndTestListGrid.GetValueRowCol(nRow, COL_TEST_NAME);
	dlg.m_strDecription = m_wndTestListGrid.GetValueRowCol(nRow, COL_TEST_DESCRIPTION);
	dlg.m_strCreator = m_wndTestListGrid.GetValueRowCol(nRow, COL_TEST_CREATOR);
	dlg.m_createdDate.ParseDateTime(m_wndTestListGrid.GetValueRowCol(nRow, COL_TEST_EDIT_TIME));
	dlg.m_lTestTypeID = m_pTestTypeCombo->GetItemData(atol(m_wndTestListGrid.GetValueRowCol(nRow, COL_TEST_PROC_TYPE)));
	dlg.m_bContinueCellCode = atol(m_wndTestListGrid.GetValueRowCol(nRow, COL_TEST_RESET_PROC));

	long nBefor = dlg.m_lTestTypeID;

	if(IDOK != dlg.DoModal())
	{
		return;
	}

	//Type이 바뀌었으면 
	if(nBefor != dlg.m_lTestTypeID)
	{
		//Step을 검사하여 시행 불가능한 Step이 포함되어 있으면 경고 표시
		if(CheckExecutableStep(GetTestIndexFromType(dlg.m_lTestTypeID), atol(m_wndTestListGrid.GetValueRowCol(nRow, COL_TEST_PRIMARY_KEY)))	 == FALSE)
		{
			CString strTemp;
			strTemp.Format("수정된 공정[%s]에서 실시할 수 없는 Step을 포함하고 있습니다.\n수정된 공정에서 시행할 수 없는 Step은 삭제되거나 수정되어 합니다.\n\n공정을 수정 하시겠습니까?",
				dlg.m_strName);
			if(MessageBox(strTemp, "Error", MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2) == IDNO)
			{
				return;
			}
		}
	}

	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_PROC_TYPE), (long)GetTestIndexFromType(dlg.m_lTestTypeID));
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_NAME), dlg.m_strName);
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_DESCRIPTION), dlg.m_strDecription);
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_CREATOR), dlg.m_strCreator);
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_RESET_PROC), (long)dlg.m_bContinueCellCode);
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_EDIT_STATE), CS_EDIT);

	OnTestSave();	
	*/

}

void CCTSEditorView::ModelCopy()
{
	CRowColArray	awRows;
	m_pWndCurModelGrid->GetSelectedRows(awRows);
	int nSelNo = awRows.GetSize();

	if(nSelNo <= 0)
	{
		MessageBox(TEXT_LANG[106], TEXT_LANG[107], MB_OK|MB_ICONSTOP);//"선택된 목록이 없습니다."//"실패"
		return;
	}

	for (int i = 0; i<nSelNo; i++)
	{
		if(awRows[i] > 0)
		{
			if(m_pWndCurModelGrid->GetValueRowCol(awRows[i], COL_MODEL_EDIT_STATE) !=  CS_NEW)
			{
				if(CopyModel(awRows[i]) == FALSE)
				{
					CString strMsg;
					strMsg.Format(TEXT_LANG[127], m_pWndCurModelGrid->GetValueRowCol(awRows[i], COL_MODEL_NAME));//"[%s]를 복사할 수 없습니다."
					MessageBox(strMsg, TEXT_LANG[107], MB_OK|MB_ICONSTOP);//"실패"
				}
			}
		}
	}
	
	GetDocument()->m_bEditedFlag = TRUE;		//DataBase 변경 Flag (BackUp을 위해)
}

void CCTSEditorView::ModelSave()
{
	if(GetDocument()->PermissionCheck(PMS_EDITOR_EDIT) ==  FALSE)
	{
		AfxMessageBox(TEXT_LANG[84]);//"수정 권한이 없습니다."
		return;
	}

	CString		strTemp, strState;
	CBatteryModelRecordSet	recordSet;
	ROWCOL nRow = m_pWndCurModelGrid->GetRowCount();
	if(nRow <1)		return;
	
	int nLastEditedModel = 0;
	ROWCOL i = 0;

	try
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return ;
	}

	for(i = 0; i< nRow; i++)
	{
		strState = m_pWndCurModelGrid->GetValueRowCol(i+1, COL_MODEL_EDIT_STATE);		//Get Model State

//		if(strState == CS_SAVED)	continue;							//Saved Model Name

		strTemp = m_pWndCurModelGrid->GetValueRowCol(i+1, COL_MODEL_NAME);
		if(strTemp.IsEmpty())	//Model Name is Empty
		{
			strTemp.Format(TEXT_LANG[128], i+1);//"%d 번째 Model명이 입력되지 않아서 저장 되지 않습니다."
			MessageBox(strTemp, "Battery Model Name Error", MB_OK|MB_ICONSTOP);
//			m_pWndCurModelGrid->SetCurrentCell(i+1, 1);
//			recordSet.Close();
//			return;
			continue;
		}

/*		strTemp = "ModelName = '" + strTemp + "'";
		if(recordSet.Find(AFX_DAO_FIRST, strTemp))	//Model Name Duplication
		{
			strTemp = "모델명 "+m_pWndCurModelGrid->GetValueRowCol(i+1, 1);
			strTemp += "이 이미 존재 합니다.";
			MessageBox(strTemp, "Battery Model Name Error", MB_OK|MB_ICONSTOP);
			m_pWndCurModelGrid->SetCurrentCell(i+1, 1);
			recordSet.Close();
			return;
		}
*/	
		if(strState == CS_NEW)
		{
			recordSet.AddNew();
		}
//		else if(strState == CS_EDIT)
		else
		{
			strTemp = m_pWndCurModelGrid->GetValueRowCol(i+1, COL_MODEL_PRIMARY_KEY);
			if(strTemp.IsEmpty())
			{
				recordSet.Close();
				TRACE("Model Primary Key Empty");
				return;
			}
			strTemp.Format("ModelID = %ld", atol((LPCSTR)(LPCTSTR)strTemp));
			if(recordSet.Find(AFX_DAO_FIRST, strTemp))	//Find Model Name Data
			{
				recordSet.Edit();
			}
			else
			{
				recordSet.Close();
				TRACE("Battery Model Search Error");
				return;
			}
		}
/*		else
		{
			recordSet.Close();
			TRACE("Cell State Error");
			return;
		}
*/		recordSet.m_ModelName = m_pWndCurModelGrid->GetValueRowCol(i+1, COL_MODEL_NAME);
		recordSet.m_Description = m_pWndCurModelGrid->GetValueRowCol(i+1, COL_MODEL_DESCRIPTION);
		recordSet.m_Creator = m_pWndCurModelGrid->GetValueRowCol(i+1, COL_MODEL_CREATOR);

		recordSet.Update();
		nLastEditedModel = i+1;

/*		GetDocument()->UpdateMainServer(atol(m_pWndCurModelGrid->GetValueRowCol(i+1, COL_MODEL_NO)),
			m_pWndCurModelGrid->GetValueRowCol(i+1, COL_MODEL_NAME), 
			m_pWndCurModelGrid->GetValueRowCol(i+1, COL_MODEL_CREATOR),
			strState);*/
	}

	i=0;

#ifdef _USE_MODEL_USER_
	CString strID(GetDocument()->m_LoginData.szLoginID);
	if(GetDocument()->m_bUseLogin && !strID.IsEmpty())
	{
		recordSet.m_strFilter.Format("[Creator] = '%s'", GetDocument()->m_LoginData.szLoginID);
	}
#endif
	recordSet.m_strSort.Format("[No]");
	recordSet.Requery();

	if(!recordSet.IsBOF())
	{
		recordSet.MoveLast();
		i = recordSet.GetRecordCount();
		recordSet.MoveFirst();
	}
	nRow -= i;
	
	if(nRow>0)
	{
		m_pWndCurModelGrid->RemoveRows(i+1, i+nRow);
	}
	else if(nRow<0)
	{
		m_pWndCurModelGrid->InsertRows(i+1, -nRow);
	}
	
	nRow = 0;
	while(!recordSet.IsEOF())
	{	
		nRow++;
		m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_NAME), recordSet.m_ModelName);
		m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_CREATOR), recordSet.m_Creator);
		m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_DESCRIPTION), recordSet.m_Description);
		m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_PRIMARY_KEY), recordSet.m_ModelID);
		m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_EDIT_STATE), CS_SAVED);
		
		if(nRow == nLastEditedModel)				//Last Edited Battery Model is current selected Model
		{
			UpdateDspModel(recordSet.m_ModelName, recordSet.m_ModelID);
			m_pWndCurModelGrid->SetCurrentCell(nRow, 1);
		}
		recordSet.MoveNext();
	}
	recordSet.Close();
	
	GetDlgItem(IDC_MODEL_SAVE)->EnableWindow(FALSE);
	GetDlgItem(IDC_MODEL_COPY_BUTTON)->EnableWindow(TRUE);

	UpdateBatteryCountState(nRow);
	GetDocument()->m_bEditedFlag = TRUE;		//DataBase 변경 Flag (BackUp을 위해)
}

void CCTSEditorView::OnButtonModelSel() 
{
	// TODO: Add your control notification handler code here
	if(m_bStepDataSaved == FALSE || GetDocument()->IsEdited() == TRUE)
	{
		CString strTemp;
		strTemp = TEXT_LANG[129];//"조건이 변경 되었습니다. 저장 하시겠습니까?"
		int nRtn = MessageBox(strTemp, TEXT_LANG[82], MB_ICONQUESTION|MB_YESNOCANCEL);//"저장"
		if(IDYES == nRtn)
		{
			if(StepSave() == FALSE)
			{
				if(MessageBox(TEXT_LANG[130], TEXT_LANG[131], MB_ICONQUESTION|MB_YESNO) != IDYES)//"저장을 실패 하였습니다. 저장을 취소 하시겠습니까?"//"취소"
				{
					return;
				}
			}
		}
		else if(nRtn == IDCANCEL)
		{
			return;
		}
	}

	m_pModelDlg->ShowWindow(SW_SHOW);
	AfxGetMainWnd()->ShowWindow(SW_HIDE);
}

void CCTSEditorView::OnAdministration() 
{
	// TODO: Add your command handler code here
	CUserAdminDlg dlg;
	dlg.m_pLoginData = &(GetDocument()->m_LoginData);
	dlg.DoModal();
}

void CCTSEditorView::OnUserSetting() 
{
	// TODO: Add your command handler code here
	CChangeUserInfoDlg *pDlg;
	pDlg = new CChangeUserInfoDlg(&GetDocument()->m_LoginData, &GetDocument()->m_LoginData, this);
	ASSERT(pDlg);
	pDlg->m_bNewUser = FALSE;
	if(IDOK == pDlg->DoModal())
	{
		memcpy(&GetDocument()->m_LoginData, pDlg->GetUserInfo(), sizeof(STR_LOGIN));
	}
	delete pDlg;
	pDlg = NULL;	
}

void CCTSEditorView::OnUpdateAdministration(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
//	if(GetDocument()->m_bUseLogin && (GetDocument()->m_LoginData.nPermission & PMS_USER_SETTING_CHANGE))
	if(GetDocument()->m_bUseLogin && strlen(GetDocument()->m_LoginData.szLoginID) > 0)
	{
		pCmdUI->Enable(TRUE);
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}
}

void CCTSEditorView::OnUpdateUserSetting(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	if(GetDocument()->m_bUseLogin && strlen(GetDocument()->m_LoginData.szLoginID) > 0)
	{
		pCmdUI->Enable(TRUE);
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}
	
}

void CCTSEditorView::OnModelReg() 
{
	// TODO: Add your command handler code here
	CModelInfoDlg dlg;
	dlg.m_pDoc = GetDocument();
	dlg.DoModal();
}


void CCTSEditorView::OnUpdateModelReg(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(m_bUseModelInfo);
}


LRESULT CCTSEditorView::OnGridBtnClicked(WPARAM wParam, LPARAM lParam)
{
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;				//Get Grid	
	
	ROWCOL nRow, nCol;			
	nCol = LOWORD(wParam);		
	nRow = HIWORD(wParam);	//Get Current Row Col

	if(nRow < 1 || nCol != COL_STEP_MODE)				return 0;

	STEP *pStep = (STEP *)GetDocument()->GetStepData(nRow);	
	if(pStep == NULL ) 
	{
		return 0; //|| pStep->chType != PS_STEP_PATTERN)	return;
	}
	
	UpdateStepGrid(nRow);

// 	CSelectSimulDataDlg * pDlg = new CSelectSimulDataDlg(this);
// 	pDlg->m_strSelFile = pStep->szSimulFile;
// 	pDlg->pStep = pStep;
// 
// 	if(pDlg->DoModal() == IDOK)
// 	{
// 		//파일명을 YYYYMMDDHHmmSS-파일명.csv
// 		
// 		sprintf(pStep->szSimulFile	, "%s", pDlg->m_strSelFile);
// 		pStep->fValueLimitHigh = pDlg->lLimitHigh;
// 		pStep->fValueLimitLow = pDlg->lLimitLow;
// 		pStep->chMode = pDlg->m_chMode;
// 		DisplayStepGrid(nRow);
// 	}
// 	delete pDlg;
	return 1;
}

//새로운 Simulation 파일을 생성한다.
CString CCTSEditorView::NewSimulationFile(CString strFile)
{
	CString strSelFile;
	CString strDataTitle;
	CString strDir(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION ,"DataBase"));

	CTime ct = CTime::GetCurrentTime();

	int nStart	= strFile.Find('-');
	int nEnd	= strFile.ReverseFind('.');
	
	if(nStart >= 0 && nEnd >= 0 && nStart < nEnd)
	{
		strDataTitle = strFile.Mid(nStart+1, nEnd-nStart-1);
	}	

	strSelFile.Format("%s\\%s-%s.csv", strDir, ct.Format("%Y%m%d%H%M%S"),strDataTitle);	

	//파일이 중복되는 것을 방지하기 위해//////////////////////////////////////////
	int nFile = 2;
	while(CopyFile(strFile, strSelFile, TRUE) == FALSE)
	{
		//AfxMessageBox(strSelFile+" Data를 지정할 수 없습니다.(중복 파일)");
		//return "";
		int nError = GetLastError();
		if(nError == ERROR_FILE_NOT_FOUND)
		{
			return strFile;
		}
		CString strFileNum;
		strFileNum.Format("(%d)", nFile++);
		strSelFile.Insert(strSelFile.ReverseFind('.'), strFileNum);
	}
	///////////////////////////////////////////////////////////////////////////////

	return strSelFile;
}

//파일이 존재하면 삭제한다.
void CCTSEditorView::DeleteSimulationFile(CString strFile)
{
	CFileFind aFind;
	BOOL bFind = aFind.FindFile(strFile);
	while(bFind)
	{
		bFind = aFind.FindNextFile();
		DeleteFile(aFind.GetFilePath());
	}
}

//Simulation 파일이 존재하면 해당 경로를 리턴한다.
void CCTSEditorView::DeleteSimulationFileTestData(LONG lTestID)
{
	CDaoDatabase  db;
	db.Open(GetDataBaseName());
	CString  strReturn;

	CString strSQL;
	CStepRecordSet rs;
	rs.m_strFilter.Format("[TestID] = %d", lTestID);
	rs.m_strSort = "[StepNo]";
	rs.Open();
	while(!rs.IsEOF())
	{

		if(rs.m_strSimulFile != "")
		{
			strReturn = rs.m_strSimulFile;
			DeleteSimulationFile(strReturn);
		}
		rs.MoveNext();
	}
	rs.Close();
}

//ModelID로 TestID를 얻는다.
void CCTSEditorView::DeleteSimulationFile(LONG lModelID)
{
	CTestListRecordSet	recordSet;
	CString strTemp;
	
	try									//DataBase Open
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return;
	}

	strTemp.Format("ModelID = %d", lModelID);
	recordSet.Find(AFX_DAO_FIRST, strTemp);
	while(!recordSet.IsEOF())
	{
		DeleteSimulationFileTestData(recordSet.m_TestID);
		recordSet.MoveNext();
	}

	recordSet.Close();
}

void CCTSEditorView::OnChangeReportTimeMili()
{
}

void CCTSEditorView::OnKillFocusReportTimeMili()
{
	CString strValue;
	GetDlgItem(IDC_REPORT_TIME_MILI)->GetWindowText(strValue);
	int nValue = atoi(strValue);

	int nTemp = nValue % 50;
	if(nTemp > 24)
		nValue += 50 - nTemp;
	else
		nValue -= nTemp;

	if(nValue >= 1000)
		nValue = 0;
	if(nValue < 0)
		nValue = 0;
	strValue.Format("%d", nValue);
	GetDlgItem(IDC_REPORT_TIME_MILI)->SetWindowText(strValue);

	nTemp = 0;
}


void CCTSEditorView::DisplayStepEndStringAll()
{
	CCTSEditorDoc * pDoc = GetDocument();

	for(int nStepNum = 1; nStepNum <= pDoc->GetTotalStepNum(); nStepNum++)
		m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_END), pDoc->MakeEndString(nStepNum));
}

void CCTSEditorView::OnChangePretestOcvMinv() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	m_bStepDataSaved = FALSE;	
}
void CCTSEditorView::OnEnChangeViGetTime()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CFormView::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnEnChangeEditRegTemp()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CFormView::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_bStepDataSaved = FALSE;	
}

void CCTSEditorView::OnEnChangeEditResistanceRate()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CFormView::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_bStepDataSaved = FALSE;	
}

void CCTSEditorView::OnEnChangeContackResistance()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CFormView::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_bStepDataSaved = FALSE;	
}

void CCTSEditorView::OnEnChangeCompChgCvDeltaCrt()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CFormView::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnEnChangeCompChgCvTime()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CFormView::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnEnChangeCompChgCvCrt()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CFormView::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnEnChangeCompChgCcDeltaVtg()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CFormView::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnEnChangeCompChgCcTime()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CFormView::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_bStepDataSaved = FALSE;
}

void CCTSEditorView::OnEnChangeCompChgCcVtg()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CFormView::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_bStepDataSaved = FALSE;
}
void CCTSEditorView::OnEnChangeContackUpperV()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CFormView::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_bStepDataSaved = FALSE;	
}

void CCTSEditorView::OnEnChangeContackLowerV()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CFormView::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_bStepDataSaved = FALSE;	
}


void CCTSEditorView::OnEnChangeContackLimitV()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CFormView::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_bStepDataSaved = FALSE;	
}

void CCTSEditorView::OnEnChangeContackUpperI()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CFormView::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_bStepDataSaved = FALSE;	
}

void CCTSEditorView::OnEnChangeContackLowerI()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CFormView::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_bStepDataSaved = FALSE;	
}

void CCTSEditorView::OnEnChangeContactDeltaLimitV()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CFormView::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_bStepDataSaved = FALSE;
}
