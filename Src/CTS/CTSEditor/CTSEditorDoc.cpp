// CTSEditorDoc.cpp : implementation of the CCTSEditorDoc class
//

#include "stdafx.h"
#include "CTSEditor.h"

#include "CTSEditorDoc.h"

#include "LoginDlg.h"

#include <afxdb.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define DBWM_UPDATE_LOT_TABLE		WM_USER+2107

/////////////////////////////////////////////////////////////////////////////
// CCTSEditorDoc

IMPLEMENT_DYNCREATE(CCTSEditorDoc, CDocument)

BEGIN_MESSAGE_MAP(CCTSEditorDoc, CDocument)
	//{{AFX_MSG_MAP(CCTSEditorDoc)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCTSEditorDoc construction/destruction

extern CString g_strDataBaseName;
extern CString GetDataBaseName();

CCTSEditorDoc::CCTSEditorDoc()
{
	// TODO: add one-time construction code here
	strcpy(m_LoginData.szLoginID, "");
	strcpy(m_LoginData.szPassword, "");
	m_LoginData.nPermission = PS_USER_SUPER;
	strcpy(m_LoginData.szRegistedDate, "");
	strcpy(m_LoginData.szUserName, "");
	strcpy(m_LoginData.szDescription, "");
	m_LoginData.bUseAutoLogOut = FALSE;	
	m_LoginData.lAutoLogOutTime = 0;

	m_nGradingStepLimit = 10;
	m_bEditedFlag = FALSE;

//	m_bEDLCType = FALSE;

//	m_bCycleType = TRUE;

	m_nMinRefI = 25;		//25mA이하 전류 Reference는 입력하지 못하도록 한다. 
	m_posUndoList = NULL;
	m_nCCRefVoltage = 1;
	LanguageinitMonConfig();

}

CCTSEditorDoc::~CCTSEditorDoc()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

BOOL CCTSEditorDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	m_bUseLogin = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "UseLogin", FALSE);
	m_lMaxCurrent = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Max Current", 5000);
	m_lMaxVoltage = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Max Voltage", 5000);
	m_lMaxCurrent1= AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Max Aging Current", 500);
//	m_nUnitType = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Unit Type", 1);
	m_nMinRefI = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Min Ref Current", 25);
	m_nCCRefVoltage = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "CCRefDiffVoltage", 50);
//	m_bEDLCType = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "EDLC", FALSE);

	//전압 전류 단위 Mode
//	m_VoltageUnitMode = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "Vtg Unit Mode", 0);
//	m_CurrentUnitMode = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "Crt Unit Mode", 0);

	m_lVRefLow = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "CCLowRef", 0);
	m_lVRefHigh = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "CCHighRef", m_lMaxVoltage);

	CString strTemp;
	char szBuff[64], szUnit[32], szDecimalPoint[32];
//	int nDeicmal;

	//Voltage Unit
	sprintf(szBuff, AfxGetApp()->GetProfileString(EDITOR_REG_SECTION, "V Unit", "V 3"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	strTemp = szUnit;
	m_VtgUnitInfo.nFloatPoint = atoi(szDecimalPoint);
	strcpy(m_VtgUnitInfo.szUnit, szUnit) ;
	if(strTemp == "V")
	{
		m_VtgUnitInfo.fTransfer = 1000.0f ;
	}
	else //if(strTemp == "mV")	//default V, uV 표시 
	{
		m_VtgUnitInfo.fTransfer = 1.0f ;
	}

	//Current Unit
	sprintf(szBuff, AfxGetApp()->GetProfileString(EDITOR_REG_SECTION, "I Unit", "mA 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	strTemp = szUnit;
	m_CrtUnitInfo.nFloatPoint = atoi(szDecimalPoint);
	strcpy(m_CrtUnitInfo.szUnit, szUnit) ;
	if(strTemp == "A")
	{
		m_CrtUnitInfo.fTransfer = 1000.0f ;
	}
	else //if(strTemp == "mA")	//default mA, uA 표시 
	{
		m_CrtUnitInfo.fTransfer = 1.0f ;
	}
	
	//Capacity Unit
	sprintf(szBuff, AfxGetApp()->GetProfileString(EDITOR_REG_SECTION, "C Unit", "mAh 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	strTemp = szUnit;
	m_CapUnitInfo.nFloatPoint = atoi(szDecimalPoint);
	strcpy(m_CapUnitInfo.szUnit, szUnit) ;
	if(strTemp == "Ah" || strTemp == "F")
	{
		m_CapUnitInfo.fTransfer = 1000.0f ;
	}
	else //if(strTemp == "mA")	//default mAh or uAh 표시 
	{
		m_CapUnitInfo.fTransfer = 1.0f ;
	}

	//Watt Unit
	sprintf(szBuff, AfxGetApp()->GetProfileString(EDITOR_REG_SECTION, "W Unit", "mW 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	strTemp = szUnit;
	m_WattUnitInfo.nFloatPoint = atoi(szDecimalPoint);
	strcpy(m_WattUnitInfo.szUnit, szUnit) ;
	if(strTemp == "W")
	{
		m_WattUnitInfo.fTransfer = 1000.0f ;
	}
	else //if(strTemp == "mW")	//default mW or uW 표시 
	{
		m_WattUnitInfo.fTransfer = 1.0f ;
	}

	//WattHour
	sprintf(szBuff, AfxGetApp()->GetProfileString(EDITOR_REG_SECTION, "Wh Unit", "mWh 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	strTemp = szUnit;
	m_WattHourUnitInfo.nFloatPoint = atoi(szDecimalPoint);
	strcpy(m_WattHourUnitInfo.szUnit, szUnit) ;
	if(strTemp == "Wh")
	{
		m_WattHourUnitInfo.fTransfer = 1000.0f ;
	}
	else //if(strTemp == "mWh")	//default mWh, uWh 표시 
	{
		m_WattHourUnitInfo.fTransfer = 1.0f ;
	}

/*	//Capa/Watt/WattHour는 전류 단위를 따라감 
	m_CapUnitInfo.nFloatPoint = m_CrtUnitInfo.nFloatPoint;
	m_WattUnitInfo.nFloatPoint = m_CrtUnitInfo.nFloatPoint;
	m_WattHourUnitInfo.nFloatPoint = m_CrtUnitInfo.nFloatPoint;
	if(GetCrtUnit() == "A")
	{
		m_CapUnitInfo.fTransfer = 1000.0f ;
		strcpy(m_CapUnitInfo.szUnit, "Ah") ;

		m_WattUnitInfo.fTransfer = 1000.0f ;
		m_WattHourUnitInfo.fTransfer = 1000.0f;
		strcpy(m_WattUnitInfo.szUnit, "W") ;
		strcpy(m_WattHourUnitInfo.szUnit, "Wh") ;
	}
	else //if(GetCrtUnit() == "mA")
	{
		m_CapUnitInfo.fTransfer = 1.0f ;
		strcpy(m_CapUnitInfo.szUnit, "mAh") ;
		m_WattUnitInfo.fTransfer = 1.0f ;
		m_WattHourUnitInfo.fTransfer = 1.0f;
		strcpy(m_WattUnitInfo.szUnit, "mW") ;
		strcpy(m_WattHourUnitInfo.szUnit, "mWh") ;
	}

*/
/*	//
	strTemp = AfxGetApp()->GetProfileString(EDITOR_REG_SECTION , "C Unit", "mAh");
	if(strTemp == "Ah")
	{
		m_CapUnitInfo.fTransfer = 1000.0f ;
		strcpy(m_CapUnitInfo.szUnit, "Ah") ;
	}
	else //if(strTemp == "mAh")	//default  mAh표시 
	{
		m_CapUnitInfo.fTransfer = 1.0f ;
		strcpy(m_CapUnitInfo.szUnit, "mAh") ;
	}

	strTemp = AfxGetApp()->GetProfileString(EDITOR_REG_SECTION , "W Unit", "mW");
	if(strTemp == "W")
	{
		m_WattUnitInfo.fTransfer = 1000.0f ;
		m_WattHourUnitInfo.fTransfer = 1000.0f;
		strcpy(m_WattUnitInfo.szUnit, "W") ;
		strcpy(m_WattHourUnitInfo.szUnit, "Wh") ;
	}
	else //if(strTemp == "mW")	//default mW 표시 
	{
		m_WattUnitInfo.fTransfer = 1.0f ;
		m_WattHourUnitInfo.fTransfer = 1.0f;
		strcpy(m_WattUnitInfo.szUnit, "mW") ;
		strcpy(m_WattHourUnitInfo.szUnit, "mWh") ;
	}
*/

	//commented by KBH	2006/09/18
	//Unit을 보고 판단하도록 수정
/*	if(m_CurrentUnitMode)
	{
		m_CrtUnitInfo.fTransfer = 1.0f;
		strcpy(m_CrtUnitInfo.szUnit, "uA") ;
	}
*/	
	if(m_bUseLogin)
	{
		CLoginDlg *pDlg;
		pDlg = new CLoginDlg;
		ASSERT(pDlg);

		if(pDlg->DoModal() != IDOK)
		{
			delete pDlg;
			pDlg = NULL;
			return FALSE;
		}
		
		m_LoginData = pDlg->m_LoginInfo; 
		delete pDlg;
		pDlg = NULL;
	
		if(PermissionCheck(PMS_EDITOR_START) == FALSE)
		{
			AfxMessageBox(TEXT_LANG[0]);//"시험조건 편집기를 실행할 권한이 없습니다."
			return FALSE;
		}
	}

	
	if(RequeryProcType() == FALSE)	return FALSE;
	if(RequeryGradeType() == FALSE)	return FALSE;
	

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CCTSEditorDoc serialization

void CCTSEditorDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CCTSEditorDoc diagnostics

#ifdef _DEBUG
void CCTSEditorDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CCTSEditorDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCTSEditorDoc commands

int CCTSEditorDoc::RequeryStepData(LONG lTestID, LONG lModelID)
{
	if(GetTotalStepNum()>0)
	{
		if(!ReMoveStepArray())	return -1;
	}

	RemoveInitStepArray();

	int nTotStepNum;
	CStepRecordSet	recordSet;

	recordSet.m_strFilter.Format("[TestID] = %ld AND [ModelID] = %ld", lTestID, lModelID);
	recordSet.m_strSort.Format("[StepNo]");

	try
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return -1;
	}


	if(recordSet.IsBOF())
	{
		nTotStepNum = 0;
	}
	else
	{
		recordSet.MoveLast();
		nTotStepNum = recordSet.GetRecordCount();
	}

	if(nTotStepNum <= 0 || nTotStepNum > SCH_MAX_STEP)
	{
		recordSet.Close();
		return 0;
	}

	recordSet.MoveFirst(); 
	
	char szTemp[128];
	STEP *pStep, *pIntStep;

	for(int i= 0;  i< nTotStepNum; i++)
	{
		pStep = new STEP;
		pIntStep = new STEP;

		ZeroMemory(pStep, sizeof(STEP));
		ZeroMemory(pIntStep, sizeof(STEP));

		//Step header
		pStep->chStepNo		= (char)recordSet.m_StepNo;
		pStep->chType		= (char)recordSet.m_StepType;
		pStep->nProcType = recordSet.m_StepProcType;
		pStep->chMode		= (char)recordSet.m_StepMode;
		pStep->fVref		= recordSet.m_Vref;
		pStep->fIref		= recordSet.m_Iref;
		
		//End 조건 
		pStep->ulEndTime	= recordSet.m_EndTime;
		pStep->fEndV		= recordSet.m_EndV;
		pStep->fEndI		= recordSet.m_EndI;

		//mF 단위 저장을 F단위로 표시 
		pStep->fEndC		= recordSet.m_EndCapacity;
		pStep->fEndDV		= recordSet.m_End_dV;
		pStep->fEndDI		= recordSet.m_End_dI;
		pStep->nLoopInfoCycle = recordSet.m_CycleCount;
		pStep->nLoopInfoGoto = atol(recordSet.m_strGotoValue);
		pStep->nLoopInfoEndVGoto	= atol(recordSet.m_strEndVGotoValue);
		pStep->nLoopInfoEndTGoto	= atol(recordSet.m_strEndTGotoValue);
		pStep->nLoopInfoEndCGoto	= atol(recordSet.m_strEndCGotoValue);
		
		//안전조건
		pStep->fVLimitHigh	= recordSet.m_OverV;
		pStep->fVLimitLow	= recordSet.m_LimitV;
		pStep->fILimitHigh	= recordSet.m_OverI;
		pStep->fILimitLow	= recordSet.m_LimitI;
		pStep->fCLimitHigh	= recordSet.m_OverCapacity;
		pStep->fCLimitLow	= recordSet.m_LimitCapacity;
		pStep->fImpLimitHigh = recordSet.m_OverImpedance;
		pStep->fImpLimitLow	= recordSet.m_LimitImpedance;
		pStep->fDeltaV		= recordSet.m_DeltaV;
		pStep->lDeltaTime = recordSet.m_DeltaTime ;
		pStep->fDeltaI		= recordSet.m_DeltaI;
		pStep->lDeltaTime1  = recordSet.m_DeltaTime1;
			
		// DCIR 셋팅 값
		pStep->fDCIR_RegTemp = recordSet.m_fDCIR_RegTemp;
		pStep->fDCIR_ResistanceRate = recordSet.m_fDCIR_ResistanceRate;
		
		//Grading
		pStep->bGrade		= recordSet.m_Grade;

		//전압 상승 비교
		pStep->ulCompTimeV[0] = recordSet.m_CompTimeV1;
		pStep->ulCompTimeV[1] = recordSet.m_CompTimeV2;
		pStep->ulCompTimeV[2] = recordSet.m_CompTimeV3;
		pStep->fCompVHigh[0] = recordSet.m_CompVHigh1;
		pStep->fCompVHigh[1] = recordSet.m_CompVHigh2;
		pStep->fCompVHigh[2] = recordSet.m_CompVHigh3;
		pStep->fCompVLow[0] = recordSet.m_CompVLow1;
		pStep->fCompVLow[1] = recordSet.m_CompVLow2;
		pStep->fCompVLow[2] = recordSet.m_CompVLow3;

		//전류 상승 비교
		pStep->ulCompTimeI[0] = recordSet.m_CompTimeI1;
		pStep->ulCompTimeI[1] = recordSet.m_CompTimeI2;
		pStep->ulCompTimeI[2] = recordSet.m_CompTimeI3;
		pStep->fCompIHigh[0] = recordSet.m_CompIHigh1;
		pStep->fCompIHigh[1] = recordSet.m_CompIHigh2;
		pStep->fCompIHigh[2] = recordSet.m_CompIHigh3;
		pStep->fCompILow[0] = recordSet.m_CompILow1;
		pStep->fCompILow[1] = recordSet.m_CompILow2;
		pStep->fCompILow[2] = recordSet.m_CompILow3;
		
		//EDLC 용량 계산 전압
		pStep->fCapaVoltage1 = recordSet.m_CapVLow;
		pStep->fCapaVoltage2 = recordSet.m_CapVHigh;
		
		//report조건
		pStep->fReportV		=	recordSet.m_RecordDeltaV;
		pStep->fReportI		=	recordSet.m_RecordDeltaI;
		pStep->ulReportTime =	recordSet.m_RecordTime;
		pStep->fReportTemp = atof(recordSet.m_strReportTemp);
		pStep->ulReportVIGetTime = recordSet.m_RecordVIGetTime;
		
		pStep->fDCIR_RegTemp = recordSet.m_fDCIR_RegTemp;
		pStep->fDCIR_ResistanceRate = recordSet.m_fDCIR_ResistanceRate;

		pStep->fCompChgCcVtg = recordSet.m_fCompChgCcVtg;
		pStep->ulCompChgCcTime = recordSet.m_ulCompChgCcTime;
		pStep->fCompChgCcDeltaVtg = recordSet.m_fCompChgCcDeltaVtg;
		pStep->fCompChgCvCrt = recordSet.m_fCompChgCvCrt;
		pStep->ulCompChgCvtTime = recordSet.m_ulCompChgCvtTime;
		pStep->fCompChgCvDeltaCrt = recordSet.m_fCompChgCvDeltaCrt;
		
		//End 전류 전압 검사
		pStep->fVEndHigh	=	recordSet.m_EndCheckVHigh;
		pStep->fVEndLow		=	recordSet.m_EndCheckVLow;
		pStep->fIEndHigh	=	recordSet.m_EndCheckIHigh;
		pStep->fIEndLow		=	recordSet.m_EndCheckILow;
			
		//SOC 종료 조건
		if(!recordSet.m_strSocEnd.IsEmpty())
		{
			sprintf(szTemp, "%s", recordSet.m_strSocEnd);
			sscanf(szTemp, "%d %d %f", &pStep->bUseActualCapa, &pStep->nUseDataStepNo, &pStep->fSocRate);
		}
		
		//End Watt/WattHour
		if(!recordSet.m_strEndWatt.IsEmpty())
		{
			sprintf(szTemp, "%s", recordSet.m_strEndWatt);
			sscanf(szTemp, "%f %f", &pStep->fEndW, &pStep->fEndWh);
		}

		//Temperature Limit
		if(!recordSet.m_strTempLimit.IsEmpty())
		{
			sprintf(szTemp, "%s", recordSet.m_strTempLimit);
			sscanf(szTemp, "%f %f %f %f %f", &pStep->fTempLow, &pStep->fTempHigh, &pStep->fEndTemp, &pStep->fTref, &pStep->fStartT);
		}
		sprintf(pStep->szSimulFile, "%s", recordSet.m_strSimulFile);

		//Patter data의 최대 최소값 
		if(!recordSet.m_strValueLimit.IsEmpty())
		{
			sprintf(szTemp, "%s", recordSet.m_strValueLimit);
			sscanf(szTemp, "%f %f", &pStep->fValueLimitLow, &pStep->fValueLimitHigh);
		}
		if(pStep->chType != PS_STEP_END && 
		   pStep->chType != PS_STEP_REST &&
		   pStep->chType != PS_STEP_LOOP )	
		{
			if(RequeryStepGrade(pStep, recordSet.m_StepID)<0)
			{
				TRACE("Grading Load Error\n");
			}
		}
		
		pStep->nGotoStepID = i+1;			//순차적으로 ID를 부여
		m_apStep.Add((STEP *)pStep);
		
		//초기 복사본을 만든다.
		memcpy(pIntStep, pStep, sizeof(STEP));
		m_apInitStep.Add(pIntStep);

		recordSet.MoveNext();
	}
	recordSet.Close();

	//EndVGoto, EndTGoto, EndCGoto가 GotoStepID를 참조하도록 변환
	for(int k = 0 ; k < nTotStepNum; k++)
	{
		if(((STEP*)m_apStep[k])->nLoopInfoEndVGoto > 0)
		{
			STEP * pGotoStep = GetStepData(((STEP*)m_apStep[k])->nLoopInfoEndVGoto);
			((STEP*)m_apStep[k])->nLoopInfoEndVGoto = pGotoStep->nGotoStepID;
		}

		if(((STEP*)m_apStep[k])->nLoopInfoEndTGoto > 0)
		{
			STEP * pGotoStep = GetStepData(((STEP*)m_apStep[k])->nLoopInfoEndTGoto);
			((STEP*)m_apStep[k])->nLoopInfoEndTGoto = pGotoStep->nGotoStepID;
		}

		if(((STEP*)m_apStep[k])->nLoopInfoEndCGoto > 0)
		{
			STEP * pGotoStep = GetStepData(((STEP*)m_apStep[k])->nLoopInfoEndCGoto);
			((STEP*)m_apStep[k])->nLoopInfoEndCGoto = pGotoStep->nGotoStepID;
		}
	}
	return nTotStepNum;
}

BOOL CCTSEditorDoc::ReMoveStepArray(int nStepNum)
{
	STEP *pStep;
	int nStepSize = GetTotalStepNum();

	if( nStepNum > 0)
	{
		if(nStepSize < nStepNum)
		{
			return FALSE;
		}
		else
		{
			if( ( pStep = (STEP *)m_apStep.GetAt(nStepNum-1) ) != NULL )
			{
				m_apStep.RemoveAt(nStepNum-1);  
			   delete pStep; // Delete the original element at 0.
			}
			/*pStep = (STEP *)m_apStep[nStepNum];
			delete pStep;
			return FALSE;
			*/
		}
	}
	else
	{
		for (int i = nStepSize-1 ; i>=0 ; i--)
		{
			if( ( pStep = (STEP *)m_apStep.GetAt(i) ) != NULL )
			{
				m_apStep.RemoveAt(i);  
			   delete pStep; // Delete the original element at 0.
			}
//			pStep = (STEP *)m_apStep[i];
//			delete pStep;
		}
		m_apStep.RemoveAll();
	}
	return TRUE;
}

void CCTSEditorDoc::RemoveInitStepArray()
{
	STEP *pStep;
	int nStepSize = m_apInitStep.GetSize();

	for (int i = nStepSize-1 ; i>=0 ; i--)
	{
		if( ( pStep = (STEP *)m_apInitStep.GetAt(i) ) != NULL )
		{
			delete pStep; // Delete the original element at 0.
			m_apInitStep.RemoveAt(i);  
		}
	}
	m_apInitStep.RemoveAll();
}

BOOL CCTSEditorDoc::ClearCopyStep()
{
	STEP *pStep;
	int nStepSize = m_apCopyStep.GetSize();

	if( nStepSize > 0)
	{
		for (int i = nStepSize-1 ; i>=0 ; i--)
		{
			if( ( pStep = (STEP *)m_apCopyStep.GetAt(i) ) != NULL )
			{
				m_apCopyStep.RemoveAt(i);  
			   delete pStep; // Delete the original element at 0.
			}
		}
		m_apCopyStep.RemoveAll();
	}
	return TRUE;
}

//설정된 종료 조건을 String으로 만든다.
CString CCTSEditorDoc::MakeEndString(int nStepNum)
{
	if(GetTotalStepNum()<nStepNum || nStepNum < 0)		return "End Loading Fail";

	ULONG lSecond, nHour;
	CString strFormat;
	STEP *pStep = GetStepData(nStepNum);
	if(pStep)
	{
		CString strTemp, strTemp1;
		switch(pStep->chType)
		{
		case PS_STEP_CHARGE:			//Charge
			if(pStep->fEndV > 0.0f)	
			{
				strFormat.Format("V > %%.%df", m_VtgUnitInfo.nFloatPoint);
				strTemp1.Format(strFormat, VUnitTrans(pStep->fEndV, FALSE));	//(double)pStep->fEndV/V_UNIT_FACTOR);
				//strTemp.Format(strFormat, VUnitTrans(pStep->fEndV, FALSE));	//(double)pStep->fEndV/V_UNIT_FACTOR);
				if(pStep->nLoopInfoEndVGoto > 0)
				{
					STEP * pGotoStep = GetStepDataGotoID(pStep->nLoopInfoEndVGoto);

					if(pGotoStep != NULL)
						strTemp.Format("%s ☞ Step%d", strTemp1, pGotoStep->chStepNo);
				}
				else
					strTemp = strTemp1;

			}
			if(pStep->fEndI > 0.0f)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strFormat.Format("I < %%.%df", m_CrtUnitInfo.nFloatPoint);
				strTemp1.Format(strFormat, IUnitTrans(pStep->fEndI, FALSE));	//(double)pStep->fEndI/I_UNIT_FACTOR);
				strTemp += strTemp1;
			}
			if(pStep->ulEndTime > 0)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				lSecond = pStep->ulEndTime / 100;
				long mSec = pStep->ulEndTime % 100;
				if(lSecond / 86400 > 0)	//day 표시 
				{
					nHour = lSecond % 86400;
					if(mSec > 0)
					{
						strTemp1.Format("t > %dD %d:%02d:%02d.%02d", lSecond/86400, nHour/3600, (nHour%3600)/60, (nHour%3600)%60, mSec);
					}
					else
					{
						strTemp1.Format("t > %dD %d:%02d:%02d", lSecond/86400, nHour/3600, (nHour%3600)/60, (nHour%3600)%60);
					}
				}
				else
				{
					if(mSec > 0)
					{
						strTemp1.Format("t > %d:%02d:%02d.%02d", lSecond/3600, (lSecond%3600)/60, (lSecond%3600)%60, mSec);
					}
					else
					{
						strTemp1.Format("t > %d:%02d:%02d", lSecond/3600, (lSecond%3600)/60, (lSecond%3600)%60);
					}
				}
				if(pStep->nLoopInfoEndTGoto > 0)
				{
					STEP * pGotoStep = GetStepDataGotoID(pStep->nLoopInfoEndTGoto);
					
					if(pGotoStep != NULL)
					{
						CString strGoto;
						strGoto.Format("%s ☞ Step%d", strTemp1, pGotoStep->chStepNo);
						strTemp1 = strGoto;
					}
					
				}
				strTemp += strTemp1;
			}
			if(pStep->fEndC > 0.0f)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}

				strFormat.Format("C > %%.%df", m_CapUnitInfo.nFloatPoint);
				strTemp1.Format(strFormat, CUnitTrans(pStep->fEndC, FALSE));	//(double)pStep->fEndC/C_UNIT_FACTOR);

				if(pStep->nLoopInfoEndCGoto > 0)
				{
					STEP * pGotoStep = GetStepDataGotoID(pStep->nLoopInfoEndCGoto);

					if(pGotoStep != NULL)
					{
						CString strGoto;
						strGoto.Format("%s ☞ Step%d", strTemp1, pStep->chStepNo);
						strTemp1 = strGoto;
					}

					
				}

				strTemp += strTemp1;
			}
			//2005.12 End Vp로 사용 
			if(pStep->fEndDV > 0.0f)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strFormat.Format("dVp > %%.%df", m_VtgUnitInfo.nFloatPoint);
				strTemp1.Format(strFormat, VUnitTrans(pStep->fEndDV, FALSE));	//(double)pStep->fEndDV/V_UNIT_FACTOR);
				strTemp += strTemp1;
			}
			if(pStep->fEndDI > 0.0f)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strFormat.Format("dI > %%.%df", m_CrtUnitInfo.nFloatPoint);
				strTemp1.Format(strFormat, IUnitTrans(pStep->fEndDI, FALSE));//(double)pStep->fEndDI/I_UNIT_FACTOR);
				strTemp += strTemp1;
			}
			//2006.5.1 End Watt로 사용
			if(pStep->fEndW > 0.0f)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strFormat.Format("W > %%.%df", m_WattUnitInfo.nFloatPoint);
				strTemp1.Format(strFormat, WattUnitTrans(pStep->fEndW, FALSE));//(double)pStep->fEndDI/I_UNIT_FACTOR);
				strTemp += strTemp1;
			}
			if(pStep->fEndWh > 0.0f)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strFormat.Format("Wh > %%.%df", m_WattHourUnitInfo.nFloatPoint);
				strTemp1.Format("Wh > %.1f", WattHourUnitTrans(pStep->fEndWh, FALSE));//(double)pStep->fEndDI/I_UNIT_FACTOR);
				strTemp += strTemp1;
			}
			
			if(pStep->nUseDataStepNo > 0 && pStep->fSocRate > 0.0f)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strTemp1.Format("Step %d SOC %.1f%%", pStep->nUseDataStepNo, pStep->fSocRate);
				strTemp += strTemp1;
			}
			return strTemp;
		
		case PS_STEP_DISCHARGE:		//Discharge
			if(pStep->fEndV > 0.0f)	
			{
				strFormat.Format("V < %%.%df", m_VtgUnitInfo.nFloatPoint);
				//strTemp.Format(strFormat, VUnitTrans(pStep->fEndV, FALSE));//(double)pStep->fEndV/V_UNIT_FACTOR);
				strTemp1.Format(strFormat, VUnitTrans(pStep->fEndV, FALSE));	//(double)pStep->fEndV/V_UNIT_FACTOR);
				if(pStep->nLoopInfoEndVGoto > 0)
				{
					STEP * pGotoStep = GetStepDataGotoID(pStep->nLoopInfoEndVGoto);

					if(pGotoStep != NULL)
						strTemp.Format("%s ☞ Step%d", strTemp1, pGotoStep->chStepNo);
				}
				else
					strTemp = strTemp1;
			}
			if(pStep->fEndI > 0.0f)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strFormat.Format("I < %%.%df", m_CrtUnitInfo.nFloatPoint);
				strTemp1.Format(strFormat, IUnitTrans(pStep->fEndI, FALSE));		//	(double)pStep->fEndI/I_UNIT_FACTOR);
				strTemp += strTemp1;
			}
			if(pStep->ulEndTime > 0)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				lSecond = pStep->ulEndTime / 100;
				long mSec = pStep->ulEndTime % 100;
				if(lSecond / 86400 > 0)	//day 표시 
				{
					nHour = lSecond % 86400;
					if(mSec > 0)
					{
						strTemp1.Format("t > %dD %d:%02d:%02d.%02d", lSecond/86400, nHour/3600, (nHour%3600)/60, (nHour%3600)%60, mSec);
					}
					else
					{
						strTemp1.Format("t > %dD %d:%02d:%02d", lSecond/86400, nHour/3600, (nHour%3600)/60, (nHour%3600)%60);
					}
				}
				else
				{
					if(mSec > 0)
					{
						strTemp1.Format("t > %d:%02d:%02d.%02d", lSecond/3600, (lSecond%3600)/60, (lSecond%3600)%60, mSec);
					}
					else
					{
						strTemp1.Format("t > %d:%02d:%02d", lSecond/3600, (lSecond%3600)/60, (lSecond%3600)%60);
					}
				}

				if(pStep->nLoopInfoEndTGoto > 0)
				{
					STEP * pGotoStep = GetStepDataGotoID(pStep->nLoopInfoEndTGoto);

					if(pGotoStep != NULL)
					{					
						CString strGoto;
						strGoto.Format("%s ☞ Step%d", strTemp1, pGotoStep->chStepNo);
						strTemp1 = strGoto;
					}
				}
				
				strTemp += strTemp1;
			}
			if(pStep->fEndC > 0.0f)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strFormat.Format("C > %%.%df", m_CapUnitInfo.nFloatPoint);
				strTemp1.Format(strFormat, CUnitTrans(pStep->fEndC, FALSE));	//(double)pStep->fEndC/C_UNIT_FACTOR);

				if(pStep->nLoopInfoEndCGoto > 0)
				{
					STEP * pGotoStep = GetStepDataGotoID(pStep->nLoopInfoEndCGoto);

					if(pGotoStep != NULL)
					{					
						CString strGoto;
						strGoto.Format("%s ☞ Step%d", strTemp1, pStep->chStepNo);
						strTemp1 = strGoto;
					}
				}
				strTemp += strTemp1;
			}
			//2005.12. End Vp로 사용
			if(pStep->fEndDV < 0.0f)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strFormat.Format("dVp > %%.%df", m_VtgUnitInfo.nFloatPoint);
				strTemp1.Format(strFormat, VUnitTrans(pStep->fEndDV, FALSE));//(double)pStep->fEndDV/V_UNIT_FACTOR);
				strTemp += strTemp1;
			}
			if(pStep->fEndDI > 0.0f)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strFormat.Format("dI > %%.%df", m_CrtUnitInfo.nFloatPoint);
				strTemp1.Format(strFormat, IUnitTrans(pStep->fEndI, FALSE));	//(double)pStep->fEndDI/I_UNIT_FACTOR);
				strTemp += strTemp1;
			}
			//2006.5.1 End Watt로 사용
			if(pStep->fEndW > 0.0f)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strFormat.Format("W > %%.%df", m_WattUnitInfo.nFloatPoint);
				strTemp1.Format(strFormat, WattUnitTrans(pStep->fEndW, FALSE));//(double)pStep->fEndDI/I_UNIT_FACTOR);
				strTemp += strTemp1;
			}
			if(pStep->fEndWh > 0.0f)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strFormat.Format("Wh > %%.%df", m_WattHourUnitInfo.nFloatPoint);
				strTemp1.Format(strFormat, WattHourUnitTrans(pStep->fEndWh, FALSE));//(double)pStep->fEndDI/I_UNIT_FACTOR);
				strTemp += strTemp1;
			}

			if(pStep->nUseDataStepNo > 0 && pStep->fSocRate > 0.0f)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strTemp1.Format("Step %d DOD %.1f%%", pStep->nUseDataStepNo, pStep->fSocRate);
				strTemp += strTemp1;
			}
			return strTemp;
		
		case PS_STEP_REST:			//Rest
		case PS_STEP_IMPEDANCE:		//Impedance
			{
				lSecond = pStep->ulEndTime / 100;
				long mSec = pStep->ulEndTime % 100;

				if(lSecond / 86400 > 0)	//day 표시 
				{
					nHour = lSecond % 86400;
					if(mSec > 0)
					{
						strTemp.Format("t > %dD %d:%02d:%02d.%02d", lSecond/86400, nHour/3600, (nHour%3600)/60, (nHour%3600)%60, mSec);
					}
					else
					{
						strTemp.Format("t > %dD %d:%02d:%02d", lSecond/86400, nHour/3600, (nHour%3600)/60, (nHour%3600)%60);
					}
				}
				else
				{
					if(mSec > 0)
					{
						strTemp.Format("t > %d:%02d:%02d.%02d", lSecond/3600, (lSecond%3600)/60, (lSecond%3600)%60, mSec);
					}
					else
					{
						strTemp.Format("t > %d:%02d:%02d", lSecond/3600, (lSecond%3600)/60, (lSecond%3600)%60);
					}
				}	
				
				if(pStep->nLoopInfoEndTGoto > 0)
				{
					STEP * pGotoStep = GetStepDataGotoID(pStep->nLoopInfoEndTGoto);
					
					if(pGotoStep != NULL)
					{					
						CString strGoto;
						strGoto.Format("%s ☞ Step%d", strTemp, pGotoStep->chStepNo);
						strTemp = strGoto;
					}
				}
			}
			return strTemp;
		
		case PS_STEP_OCV:				//Ocv
		case PS_STEP_END:				//End
		case PS_STEP_ADV_CYCLE:
		case -1:					//Not Seleted
			strTemp.Empty();
			return strTemp;
		case PS_STEP_LOOP:
			if(pStep->nLoopInfoGoto <= 0)
			{
				strTemp.Format(TEXT_LANG[1], pStep->nLoopInfoCycle, pStep->nLoopInfoGoto);//"반복 %d회 후 다음 Cycle로 이동"
			}
			else
			{
				strTemp.Format(TEXT_LANG[2], pStep->nLoopInfoCycle, pStep->nLoopInfoGoto);//"반복 %d회 후 Step %d로 이동"
			}
			return strTemp;
		default:
			strTemp = "Step Type Loading Fail";
			return strTemp;
		}
	}
	else
	{
		return "End Loading Fail";
	}
}

int CCTSEditorDoc::GetTotalStepNum()
{
	return m_apStep.GetSize();
}

STEP * CCTSEditorDoc::GetStepData(int nStepNum /*One Base*/)
{
	STEP *pStep = NULL;
	if(GetTotalStepNum() < nStepNum || nStepNum <1)
		return pStep;

	return (STEP *)m_apStep[nStepNum-1];
}

BOOL CCTSEditorDoc::RequeryPreTestParam(LONG lTestID, LONG lModelID)
{
	CPreTestCheckRecordSet	recordSet;
	recordSet.m_strFilter.Format("[TestID] = %ld AND [ModelID] = %ld", lTestID, lModelID);
//	recordSet.m_strSort.Format("[StepNo]");

	try
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}

	if(recordSet.IsBOF())	// Does not Exist
	{
		ZeroMemory(&m_sPreTestParam, sizeof(TEST_PARAM));
	}
	else
	{
		m_sPreTestParam.fDeltaVoltage = recordSet.m_DeltaVoltage;
		m_sPreTestParam.fMaxCurrent = recordSet.m_CurrentRange;
		m_sPreTestParam.fMaxVoltage = recordSet.m_MaxV;
		m_sPreTestParam.fMinVoltage = recordSet.m_MinV;
		m_sPreTestParam.fOCVLimitVal = recordSet.m_OCVLimit;
		m_sPreTestParam.fTrickleCurrent = recordSet.m_TrickleCurrent;
		m_sPreTestParam.fDeltaVoltage = recordSet.m_DeltaVoltage;
		m_sPreTestParam.fMaxFaultNo = recordSet.m_MaxFaultBattery ;
		m_sPreTestParam.lTrickleTime = recordSet.m_TrickleTime;
		m_sPreTestParam.bPreTest = recordSet.m_PreTestCheck;
		m_sPreTestParam.fDeltaVoltageLimit = recordSet.m_DeltaVoltageLimit;
	} 
	recordSet.Close();

	memcpy(&m_sInitPreTestParam, &m_sPreTestParam, sizeof(TEST_PARAM));

	return TRUE;
}

BOOL CCTSEditorDoc::AddNewStep(int nStepNum, STEP *pCopyStep)
{
	int nTotStepNum = GetTotalStepNum();
	if(nTotStepNum <0 || nStepNum < 0)	return FALSE;
	
	STEP *pStep;
	pStep = new STEP;
	ASSERT(pStep);
	if(pCopyStep)
	{
		memcpy(pStep, pCopyStep, sizeof(STEP));
	}
	else
	{
		ZeroMemory(pStep, sizeof(STEP));
	}

	pStep->chStepNo = nStepNum;

	int nFindGotoStepID = 1;
	BOOL nFind = TRUE;
	while(nFind)
	{
		nFind = FALSE;
		for(int i = 1 ; i <= GetTotalStepNum(); i++)
		{
			STEP * pTempStep = GetStepData(i);
			if(pTempStep == NULL) break;
			if(pTempStep->nGotoStepID == nFindGotoStepID)
			{
				nFindGotoStepID++;
				nFind = TRUE;
				break;
			}
			else
				nFind = FALSE;
		}
	}
	pStep->nGotoStepID = nFindGotoStepID;	

	
	
	if(nStepNum > nTotStepNum)
	{
//			pStep->chType = 0;
//			pStep->chMode =-1;		
		
		try
		{
			m_apStep.Add((STEP *)pStep);
		}
		catch (CMemoryException* e)
		{
			char szError[256];
			AfxMessageBox(e->GetErrorMessage(szError, 255));
			e->Delete();
			delete pStep;
			return FALSE;
		}
	}
	else 
	{
//			pStep->chType =-1;
//			pStep->chMode =-1;

		try
		{
			if((nStepNum - 1 ) < 0)	nStepNum = 1;
			m_apStep.InsertAt(nStepNum-1, pStep);
		}
		catch (CMemoryException* e)
		{
			char szError[256];
			AfxMessageBox(e->GetErrorMessage(szError, 255));
			e->Delete();
			delete pStep;
			return FALSE;
		}
	}
	return TRUE;
}

int CCTSEditorDoc::SaveStep(LONG lTestID, LONG lModelID)
{
	int nTotStepNum = GetTotalStepNum();
//	if(nTotStepNum <= 0)	return TRUE;	//0 Step Size
	
	int rtn = StepValidityCheck();
	if(rtn < 0)		return rtn;
	
	RemoveInitStepArray();
	
	CStepRecordSet	recordSet;
	recordSet.m_strFilter.Format("[TestID] = %ld AND [ModelId] = %ld", lTestID, lModelID);
	recordSet.m_strSort.Format("[StepNo]");

	try
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return -1;
	}

	while(!recordSet.IsEOF())	//Clear Step Data
	{
		try						//Delete All Step
		{
			recordSet.Delete();
		}
		catch (CDaoException* e)
		{
			AfxMessageBox(e->m_pErrorInfo->m_strDescription);
			e->Delete();
			recordSet.Close();
			return -1;
		}
		recordSet.MoveNext();
	}

	CString strTemp;						
	strTemp.Format("[TestID] = %ld AND [ModelID] = %ld", lTestID, lModelID);
	if(recordSet.Find(AFX_DAO_FIRST, strTemp))	//Clear Chcek
	{
		recordSet.Close();
		return -1;
	}	

	STEP* pStep, *pInitStep;
	int i = 0;
	for(i = 0; i< nTotStepNum; i++)		//Save All Step
	{
		pStep = (STEP*)m_apStep[i]; // Get element 0
		ASSERT(pStep);

		if((int)pStep->chType == 0)
			return -2;
			
		pInitStep = new STEP;
		memcpy(pInitStep, pStep, sizeof(STEP));
		m_apInitStep.Add(pInitStep);

		recordSet.AddNew();
		
		recordSet.m_TestID = lTestID;
		recordSet.m_ModelID = lModelID;
		//Step Header
		recordSet.m_StepNo = (long)(i+1);	//pStep->chStepNo;
		recordSet.m_StepType = (long)pStep->chType;
		recordSet.m_StepProcType = pStep->nProcType ;
		recordSet.m_StepMode = (long)pStep->chMode;
		recordSet.m_Vref = pStep->fVref;
		recordSet.m_Iref = pStep->fIref;

		//End 조건 
		recordSet.m_EndTime = pStep->ulEndTime;
		recordSet.m_EndV = pStep->fEndV;
		recordSet.m_EndI = pStep->fEndI;
		
		recordSet.m_EndCapacity = pStep->fEndC;

		recordSet.m_End_dV = pStep->fEndDV;
		recordSet.m_End_dI = pStep->fEndDI;
		recordSet.m_CycleCount = pStep->nLoopInfoCycle;
		recordSet.m_strGotoValue.Format("%d", pStep->nLoopInfoGoto);

		//EndVGoto, EndTGoto, EndCGoto의 Step ID가 가리키고 있는 Step No로 변경해서 저장
		int nGotoEndV = 0;
		int nGotoEndT = 0;
		int nGotoEndC = 0;
		if(pStep->nLoopInfoEndVGoto > 0)
		{
			STEP * pGotoStep = GetStepDataGotoID(pStep->nLoopInfoEndVGoto);
			nGotoEndV = pGotoStep->chStepNo;
		}

		if(pStep->nLoopInfoEndTGoto > 0)
		{
			STEP * pGotoStep = GetStepDataGotoID(pStep->nLoopInfoEndTGoto);
			nGotoEndT = pGotoStep->chStepNo;
		}

		if(pStep->nLoopInfoEndCGoto > 0)
		{
			STEP * pGotoStep = GetStepDataGotoID(pStep->nLoopInfoEndCGoto);
			nGotoEndC = pGotoStep->chStepNo;
		}
		
		recordSet.m_strEndVGotoValue.Format("%d", nGotoEndV);
		recordSet.m_strEndTGotoValue.Format("%d", nGotoEndT);
		recordSet.m_strEndCGotoValue.Format("%d", nGotoEndC);

		//안전 조건
		recordSet.m_OverV = pStep->fVLimitHigh;
		recordSet.m_LimitV = pStep->fVLimitLow;
		recordSet.m_OverI = pStep->fILimitHigh;
		recordSet.m_LimitI = pStep->fILimitLow;
		recordSet.m_OverCapacity = pStep->fCLimitHigh;
		recordSet.m_LimitCapacity = pStep->fCLimitLow;
		recordSet.m_OverImpedance = pStep->fImpLimitHigh;
		recordSet.m_LimitImpedance = pStep->fImpLimitLow;
		recordSet.m_DeltaV = pStep->fDeltaV;
		recordSet.m_DeltaTime = pStep->lDeltaTime;
		recordSet.m_DeltaI =pStep->fDeltaI;
		recordSet.m_DeltaTime1 = pStep->lDeltaTime1;
		
		//Grading
		recordSet.m_Grade = pStep->bGrade;

		//전압상승비교
		recordSet.m_CompTimeV1 = pStep->ulCompTimeV[0];
		recordSet.m_CompTimeV2 = pStep->ulCompTimeV[1]; 
		recordSet.m_CompTimeV3 = pStep->ulCompTimeV[2];
		recordSet.m_CompVLow1 = pStep->fCompVLow[0];
		recordSet.m_CompVLow2 = pStep->fCompVLow[1];
		recordSet.m_CompVLow3 = pStep->fCompVLow[2];
		recordSet.m_CompVHigh1 = pStep->fCompVHigh[0];
		recordSet.m_CompVHigh2 = pStep->fCompVHigh[1];
		recordSet.m_CompVHigh3 = pStep->fCompVHigh[2];

		//전류 상승 비교
		recordSet.m_CompTimeI1 = pStep->ulCompTimeI[0];
		recordSet.m_CompTimeI2 = pStep->ulCompTimeI[1]; 
		recordSet.m_CompTimeI3 = pStep->ulCompTimeI[2];
		recordSet.m_CompILow1 = pStep->fCompILow[0];
		recordSet.m_CompILow2 = pStep->fCompILow[1];
		recordSet.m_CompILow3 = pStep->fCompILow[2];
		recordSet.m_CompIHigh1 = pStep->fCompIHigh[0];
		recordSet.m_CompIHigh2 = pStep->fCompIHigh[1];
		recordSet.m_CompIHigh3 = pStep->fCompIHigh[2];

		//저장 조건
		recordSet.m_RecordDeltaV = pStep->fReportV;
		recordSet.m_RecordDeltaI = pStep->fReportI;
		recordSet.m_RecordTime = pStep->ulReportTime;
		recordSet.m_strReportTemp.Format("%.1f", pStep->fReportTemp);		//Value1에 온도 조건 저장 
		recordSet.m_RecordVIGetTime = pStep->ulReportVIGetTime;
		
		//EDLC 용량 산출
		recordSet.m_CapVHigh = pStep->fCapaVoltage2;
		recordSet.m_CapVLow = pStep->fCapaVoltage1;
		

		//End 전류 전압 검사
		recordSet.m_EndCheckVHigh = pStep->fVEndHigh;
		recordSet.m_EndCheckVLow  = pStep->fVEndLow	;	
		recordSet.m_EndCheckIHigh = pStep->fIEndHigh;
		recordSet.m_EndCheckILow  = pStep->fIEndLow	;	

		recordSet.m_strSocEnd.Format("%d %d %.2f", pStep->bUseActualCapa, pStep->nUseDataStepNo, pStep->fSocRate);
		TRACE("Save SOC STEP %d: %s\n", recordSet.m_StepNo,recordSet.m_strSocEnd);

		//End Watt/WattHour
		recordSet.m_strEndWatt.Format("%.3f %.3f", pStep->fEndW, pStep->fEndWh);

		//Temperature limit
		recordSet.m_strTempLimit.Format("%.1f %.1f %.1f %.1f %.1f", pStep->fTempLow, pStep->fTempHigh, pStep->fEndTemp, pStep->fTref, pStep->fStartT);

		//Simulation file name
		recordSet.m_strSimulFile.Format("%s", pStep->szSimulFile);

		recordSet.m_strValueLimit.Format("%.3f %.3f",  pStep->fValueLimitLow, pStep->fValueLimitHigh);
//		recordSet.m_ValueLimitHigh.Format("%ld",  pStep->lValueLimitHigh);
			
		// DCIR Setting
		recordSet.m_fDCIR_RegTemp = pStep->fDCIR_RegTemp;
		recordSet.m_fDCIR_ResistanceRate = pStep->fDCIR_ResistanceRate;

		recordSet.m_fCompChgCcVtg = pStep->fCompChgCcVtg;
		recordSet.m_ulCompChgCcTime = pStep->ulCompChgCcTime;
		recordSet.m_fCompChgCcDeltaVtg = pStep->fCompChgCcDeltaVtg;
		recordSet.m_fCompChgCvCrt = pStep->fCompChgCvCrt;
		recordSet.m_ulCompChgCvtTime = pStep->ulCompChgCvtTime;
		recordSet.m_fCompChgCvDeltaCrt = pStep->fCompChgCvDeltaCrt;

		recordSet.Update();
	}
	
	recordSet.Requery();		//Step Data Saved

	for(i = 0; i< nTotStepNum; i++)		//Save All Grade
	{
		pStep = (STEP*)m_apStep[i];		// Get element 0
		ASSERT(pStep);
		if(SaveStepGrade(pStep, recordSet.m_StepID, lModelID) < 0)	rtn = -3;
		
		recordSet.MoveNext();
	}
	recordSet.Close();
	return rtn;
}

int CCTSEditorDoc::StepValidityCheck()
{
	int nTotStepNum = GetTotalStepNum();
	STEP* pStep, *pTempStep;
	if(nTotStepNum <0 || nTotStepNum >  SCH_MAX_STEP)
	{
		m_strLastErrorString = TEXT_LANG[3];//"입력된 Step수가 범위를 벗어났습니다."
		return -1;
	}

	if(m_sPreTestParam.bPreTest)
	{
		//20070321 전류는 입력하지 않아도 됨
/*		if(m_sPreTestParam.fTrickleCurrent<m_nMinRefI)
		{
			m_strLastErrorString.Format("Cell Check Parameter의 전류 설정값이 %dmA 미만으로 설정되었습니다.", m_nMinRefI);
			return -5;
		}
*/
		if(m_sPreTestParam.lTrickleTime < 500 || m_sPreTestParam.lTrickleTime > 6000)
		{
			m_strLastErrorString = TEXT_LANG[4];//"Cell Check Parameter의 시간 설정값을 5초~60초 범위에서 입력하십시요."
			return -5;
		}

		//OCV 검사만도 지원함
/*		if(m_sPreTestParam.fMaxCurrent < 0.0f || m_sPreTestParam.fTrickleCurrent < 0.0f)
		{
			m_strLastErrorString = "검사 전류값은 0 보다 큰값을 입력하여야 합니다.";
			return -5;
		}
*/

//		if(m_sPreTestParam)
	}

	if(nTotStepNum < 1)		return 0;

	pStep = (STEP*)m_apStep[nTotStepNum-1]; // Get element End
	if(pStep->chType != PS_STEP_END)
	{
		m_strLastErrorString = TEXT_LANG[5];//"마지막 Step이 [완료] Step이 아닙니다."
		return -1;
	}
	
	int nAdvCycleCount = 0;
	int nNormalStepCount = 0;
	int nCurrentAdvCycleIndex = 0;
	int nMaxEndVGotoStepNo = 0;		//EndV goto를 가지고 있는 Step번호 
	int nMaxEndVGotoStepIndex = 0;	//EndV goto가 가리키고 있는 Step번호

	int nMaxEndTGotoStepNo = 0;		//EndT goto를 가지고 있는 Step번호 
	int nMaxEndTGotoStepIndex = 0;	//EndT goto가 가리키고 있는 Step번호

	int nMaxEndCGotoStepNo = 0;		//EndC goto를 가지고 있는 Step번호 
	int nMaxEndCGotoStepIndex = 0;	//EndC goto가 가리키고 있는 Step번호
	CString strTemp;

	for(int i = 0; i< nTotStepNum; i++)		//Save All Step
	{
		pStep = (STEP*)m_apStep[i]; // Get element 0
		ASSERT(pStep);


		if(pStep->chType < 0 )
		{
			m_strLastErrorString.Format(TEXT_LANG[6], i+1);//"Step %d의 Type이 설정되지 않았습니다."
			return -2;
		}
		
		//종료 시간이 설정되어 있고 Record time이 1Sec이하가 설정되어 있을 경우 
		if(pStep->ulEndTime > 0 && pStep->ulReportTime%100 > 0)
		{
			//경고 MessageBox
			if(pStep->ulEndTime > SCH_MAX_MSEC_DATA_POINT*(pStep->ulReportTime%100))
			{
				m_strLastErrorString.Format(TEXT_LANG[7],i+1, (SCH_MAX_MSEC_DATA_POINT*(pStep->ulReportTime%100))/100);//"Step %d의 저장 data는 %d초 이후는 1초 간격으로 저장됩니다.(Step 종료 시간이 최대 저장 시간 보다 깁니다.)"
				AfxMessageBox(m_strLastErrorString);
			}
		}

		
		//Grading이 설정되었지만 Step이 없을 경우 
		if(pStep->bGrade && pStep->sGrading_Val.chTotalGrade <= 0)
		{	
			pStep->bGrade = 0;
		}

		//DC Impedance 측정일 경우 설정값을 입력 하여야 한다.
		if(pStep->chType == PS_STEP_IMPEDANCE && pStep->chMode == PS_MODE_DCIMP)
		{
			if(pStep->fIref <= m_nMinRefI)
			{
				m_strLastErrorString.Format(TEXT_LANG[8], i+1);	//"Step %d의 전류값이 설정 되지 않았거나 범위를 벗어났습니다."
				return -1;
			}

			if( pStep->fIref > m_lMaxCurrent )
			{
				m_strLastErrorString.Format(TEXT_LANG[8], i+1);//"Step %d의 전류값이 설정 되지 않았거나 범위를 벗어났습니다."
				return -1;
			}

			if(pStep->ulEndTime == 0)
			{
				m_strLastErrorString.Format(TEXT_LANG[9], i+1);//"Step %d의 종료시간이 설정되지 않았습니다."
				return -1;
			}
			
#ifdef _EDLC_CELL_
			//Time is 10usec unit;
			if(pStep->ulEndTime < 200 || pStep->ulEndTime > 1000)
			{
				m_strLastErrorString.Format(TEXT_LANG[10], i+1);//"Step %d의 ESR 측정가능 종료 시간은 2~10sec 입니다."
				return -1;
			}
#endif

		} //Impedance 

// 		if(pStep->chType == PS_STEP_PATTERN)
// 		{
// 			if(strlen(pStep->szSimulFile) <= 0)
// 			{
// 				m_strLastErrorString.Format("Step %d의 Simulation data file이 지정되지 않았습니다.", i+1);
// 				return -1;
// 			}
// 			if(pStep->fValueLimitLow < 0)
// 			{
// 			//	CString strError;
// 			//	strError.Format("%d,%d / %d,%d",m_lMaxCurrent, m_nMinRefI,pStep->fValueLimitHigh,pStep->fValueLimitLow);
// 				m_strLastErrorString.Format("Step %d의 전류값이 설정 되지 않았거나 범위를 벗어났습니다.", i+1);								
// 				return -1;
// 			}
// 			//최소 한가지 이상의 종료조건이 설정 되어야 한다.
// 			if( pStep->fEndC <= 0 && pStep->fEndDI <= 0 &&
// 				pStep->fEndDV <=0 && pStep->fEndI <=0 && 
// 				pStep->fEndV <=0 && pStep->ulEndTime == 0 && pStep->nUseDataStepNo == 0)
// 			{
// 				m_strLastErrorString.Format("Step %d의 종료 조건이 설정되어 있지 않았습니다.", i+1);			
// 				return -1;
// 			}
// 
// 		}

		if(pStep->chType == PS_STEP_CHARGE || pStep->chType == PS_STEP_DISCHARGE)
		{
			//formation에서는 CC나 CV에 
			if(m_lLoadedProcType != PS_PGS_AGING)
			{
				if(pStep->chMode == PS_MODE_CC && pStep->fEndV <= 0.0f)
				{
					m_strLastErrorString.Format(TEXT_LANG[12], i+1);//"Step %d의 종료 전압이 설정되어 있지 않습니다."
					return -1;
				}
				if(pStep->chMode == PS_MODE_CV && pStep->fEndI <= 0.0f)
				{
					m_strLastErrorString.Format(TEXT_LANG[13], i+1);//"Step %d의 종료 전류가 설정되어 있지 않습니다."
					return -1;
				}

				if(pStep->chMode == PS_MODE_CCCV )
				{
					if(pStep->fVref > m_lMaxVoltage )
					{
						m_strLastErrorString.Format(TEXT_LANG[14], i+1, m_lMaxVoltage );//"Step %d의 전압값이 설정 전압값(%dmV) 보다 높습니다."
						return -1;
					}

					if( pStep->fIref > m_lMaxCurrent )
					{
						m_strLastErrorString.Format(TEXT_LANG[15], i+1, m_lMaxCurrent );//"Step %d의 전류값이 설정 전압값(%dmA) 보다 높습니다."
						return -1;
					}
				}
			}

			//전류 설정값은 반드시 입력해야 한다.
			if(pStep->chMode != PS_MODE_CP && pStep->chMode != PS_MODE_CR)
			{
				if(m_lLoadedProcType == PS_PGS_AGING )
				{
		//			if(pStep->fIref > m_lMaxCurrent1)
		//			{
		//				m_strLastErrorString.Format("Step %d의 설정전류가 최대 전류값을 초과하였습니다.(Aging 공정 최대 전류 %dmA)", i+1, m_lMaxCurrent1);			
		//				return -1;
		//			}
				}
				else
				{
					if( pStep->fIref < m_nMinRefI)
					{
						m_strLastErrorString.Format(TEXT_LANG[16], i+1, m_nMinRefI);//"Step %d의 전류값이 설정 되지 않았거나 범위를 벗어났습니다.(%dmA)"
						return -1;
					}

					if( pStep->fIref > m_lMaxCurrent )
					{
						m_strLastErrorString.Format(TEXT_LANG[15], i+1, m_lMaxCurrent );//"Step %d의 전류값이 설정 전압값(%dmA) 보다 높습니다."
						return -1;
					}
				}

				if(pStep->fIref <= pStep->fEndI || pStep->fIref <= pStep->fILimitLow )
				{
					m_strLastErrorString.Format(TEXT_LANG[17], i+1);//"Step %d의 설정 전류값이 종료 전류나 전류 하한값보다 작습니다."
					return -1;
				}
			}
			else	//fIRef에 CP, CR제어값이 들어 있다.
			{
//				if(PS_MODE_CP == pStep->chMode && (pStep->fIref > (float)m_lMaxCurrent/ 1000.0f * m_lMaxVoltage  || pStep->fIref <= 0.0f))
//				{
//					m_strLastErrorString.Format("Step %d의 CP 전력값 입력 범위가 벗어났습니다.(0 ~ %.1fW)", i+1,  (float)m_lMaxCurrent/1000.0f * m_lMaxVoltage );			
//					return -1;
//				}

				if(PS_MODE_CR == pStep->chMode && pStep->fIref <= 0.0f)
				{
					m_strLastErrorString.Format(TEXT_LANG[18], i+1);//"Step %d의 CR 저항값 입력 범위가 벗어났습니다.(CR > 0)"
					return -1;
				}
			}

			//최소 한가지 이상의 종료조건이 설정 되어야 한다.
			if( pStep->fEndC <= 0 && pStep->fEndDI <= 0 &&
				pStep->fEndDV <=0 && pStep->fEndI <=0 && 
				pStep->fEndV <=0 && pStep->ulEndTime == 0 && pStep->nUseDataStepNo == 0)
			{
				m_strLastErrorString.Format(TEXT_LANG[11], i+1);//"Step %d의 종료 조건이 설정되어 있지 않았습니다."
				return -1;
			}

/*			if(pStep->fEndDV > 0)		//2 Step Charge 
			{
				if(pStep->fEndDI <= 0.0f)
				{
					m_strLastErrorString.Format("Step %d의 2단계 전류값이 설정되지 않았습니다.", i+1);			
					return -1;
				}
				//End I, End V, End Time , End C
				if(pStep->fDeltaI <= 0.0f && pStep->fDeltaV <= 0.0f && pStep->lDeltaTime == 0 && pStep->lDeltaTime1 == 0)
				{
					m_strLastErrorString.Format("Step %d의 2단계 종료값이 설정되지 않았습니다.", i+1);			
					return -1;
				}
			}
*/

#ifdef _EDLC_CELL_
			//EDLC시 용량 검사 전압 Check
				if(pStep->fCapaVoltage1 > pStep->fCapaVoltage2)
				{
					m_strLastErrorString.Format(TEXT_LANG[19], i+1);//"Step %d 용량 측정 전압 상한값이 하한값보다 작습니다."
					return -1;
				}

				if(pStep->fCapaVoltage1 > 0.0f && pStep->fCapaVoltage2 <= 0.0f)
				{
					m_strLastErrorString.Format(TEXT_LANG[20], i+1);			//"Step %d 용량 측정 전압 상한값이 설정되어 있지 않습니다."
					return -1;
				}

				if(pStep->fCapaVoltage1 <= 0.0f && pStep->fCapaVoltage2 > 0.0f)
				{
					m_strLastErrorString.Format(TEXT_LANG[21], i+1);//"Step %d 용량 측정 전압 하한값이 설정되어 있지 않습니다."
					return -1;
				}

				if(m_lLoadedProcType == PS_PGS_FORMATION )
				{
					if(pStep->fCapaVoltage1 == 0.0f && pStep->fCapaVoltage2 == 0.0f)
					{
						m_strLastErrorString.Format(TEXT_LANG[22], i+1);		//"Step %d 용량 측정 전압값이 설정되어 있지 않습니다."
						strTemp.Format(TEXT_LANG[23], i+1);								//"Step %d 용량 측정 전압값이 설정되어 있지 않습니다. 전압값을 설정하지 않으며 용량이 측정되지 않습니다.\n\n용량측정을 하지 않겠습니까?"
						if(AfxMessageBox(strTemp, MB_ICONQUESTION|MB_YESNO) == IDNO)
						{
							return -1;
						}
					}
				}
#endif

			for(int k=0; k<SCH_MAX_COMP_POINT; k++)
			{
				//전압 상하한 크기 비교
				if(pStep->fCompVLow[k] > 0.0 && pStep->fCompVHigh[k] > 0.0)
				{
					if(pStep->fCompVLow[k] > pStep->fCompVHigh[k])
					{
						m_strLastErrorString.Format(TEXT_LANG[24], i+1, k+1 );			//"Step %d 전압 변화 비교 Point %d 전압상한값이 하한값보다 작습니다."
						return -1;
					}
				}

				//전류 상하한 크기 비교
				if( pStep->fCompILow[k] > 0.0 && pStep->fCompIHigh[k] > 0.0)
				{
					if( pStep->fCompILow[k] > pStep->fCompIHigh[k])
					{
						m_strLastErrorString.Format(TEXT_LANG[25], i+1, k+1 );			//"Step %d 전류 변화 비교 Point %d 전류상한값이 하한값보다 작습니다."
						return -1;
					}
				}
			}
			
			/*
			//Soc rate 설정
			if(pStep->fSocRate>0.0f)
			{
				STEP *pCapStep = GetStepData(pStep->nUseDataStepNo);
				if(pCapStep == NULL || pCapStep->bUseActualCapa == FALSE)
				{
					m_strLastErrorString.Format("Step %d의 기준용량 종료로 사용할 Step이 설정되지 않았거나 존재하지 않는 Step을 참조하고 있습니다.(참조 Step %d)", i+1, pStep->nUseDataStepNo);			
					return -1;
				}
			}
			*/

		}	//Charge Discharge

		
		if(pStep->chType == PS_STEP_CHARGE)
		{
			//End 전압을 정격전압보다 최소 10mV이하 낮게 설정하여야 한다.
			//End 전압은 CC모드 입력에서만 가능한데 CC동작을 위해서는 CV 전압이 더 높게 설정되어야 하므로 
//			if(pStep->fEndV >= m_lMaxVoltage-10)
//			{
//				m_strLastErrorString.Format("Step %d의 종료 전압이 정격 전압(%dmV)에 비해 높게 설정 되었습니다. 정격전압보다 최소 10mV이하 범위값으로 설정하십시오.", i+1, m_lMaxVoltage);			
//				return -1;				
//			}

			if(pStep->fVref <= 0.0f)
			{
				m_strLastErrorString.Format(TEXT_LANG[26], i+1);			//"Step %d의 전압값이 설정 되지 않았거나 범위를 벗어났습니다.	(범위:0mV)"
				return -1;
			}

			if(pStep->fVref > m_lMaxVoltage )
			{
				m_strLastErrorString.Format(TEXT_LANG[14], i+1, m_lMaxVoltage );//"Step %d의 전압값이 설정 전압값(%dmV) 보다 높습니다."
				return -1;
			}

			if(pStep->fVref <= pStep->fEndDV)	
			{
				m_strLastErrorString.Format(TEXT_LANG[27], i+1);			//"Step %d의 전압변화 설정값이 너무 큽니다."
				return -1;				
			}
			
			if(pStep->fVref <= pStep->fEndV)
			{
				m_strLastErrorString.Format(TEXT_LANG[28], i+1);			//"Step %d의 종료 전압이 설정 전압보다 높습니다."
				return -1;
			}
			if(pStep->fCLimitHigh <= 0)
			{
				m_strLastErrorString.Format("step %d Capacity upper limit is not set.", i+1);//"20200412엄륭 단계의 상한 용량값이 설정되지 않았습니다.		
				return -1;
			}
		} //charge
			

		//CC 방전일 경우 종료 전압은 Ref 전압 보다 커야 한다. 
		if(pStep->chType == PS_STEP_DISCHARGE)
		{
			if(pStep->fVref > 0 && pStep->chMode == PS_MODE_CC && pStep->fEndV > 0)		//Vref가 입력된 경우 
			{
				if(pStep->fVref >= pStep->fEndV)
				{
					m_strLastErrorString.Format(TEXT_LANG[29], i+1);//"Step %d의 전압 설정값이 종료 전압보다 높습니다."
					return -1;
				}
			}

			if(pStep->fEndV > m_lMaxVoltage )
			{
				m_strLastErrorString.Format(TEXT_LANG[30], i+1);//"Step %d의 종료 전압값이 설정 전압보다 보다 높습니다."
				return -1;
			}
		} //Discharge

		//Rest는 반드시 종료 시간이 입력 되어야 한다.
		if(pStep->chType == PS_STEP_REST)
		{
			if(pStep->ulEndTime == 0)
			{
				m_strLastErrorString.Format(TEXT_LANG[9], i+1);//"Step %d의 종료시간이 설정되지 않았습니다."
				return -1;
			}
		} //reset
	
		if(pStep->fTref == 0.0f && (pStep->fStartT !=0.0f || pStep->fEndTemp != 0.0f))
		{
			m_strLastErrorString.Format(TEXT_LANG[31], i+1);			//"Step %d의 온도 설정값을 입력하지 않고 시작온도나 종료온도가 설정되었습니다."
			return -1;
		}
			
		//전압 설정값 입력 범위 검사
		if(  pStep->fVref < 0.0f)
		{
			m_strLastErrorString.Format(TEXT_LANG[32], i+1);//"Step %d의 전압 설정값이 범위를 벗어 났습니다.(범위:0mV)"
			return -1;
		}
		//전압 종료값 입력 범위 검사
		if(pStep->fEndV < 0.0f)
		{
			m_strLastErrorString.Format(TEXT_LANG[33], i+1);//"Step %d의 종료 전압값이 입력 범위를 벗어났습니다.(범위:0mV)"
			return -7;
		}

		//전류 종료값 입력 범위 검사
		if(pStep->fEndI < 0.0f)
		{
			m_strLastErrorString.Format(TEXT_LANG[34], i+1);//"Step %d의 종료 전류값이 입력 범위를 벗어났습니다.(범위:0mA)"
			return -8;
		}

		//전압 제한값 입력 범위 검사
		if(pStep->chType == PS_STEP_OCV)
		{

		}
		else
		{
			if(pStep->fVLimitLow < 0)
			{
				m_strLastErrorString.Format(TEXT_LANG[35], i+1);//"Step %d의 안전 전압 상하한값이 입력 범위를 벗어났습니다.(범위:0mV)"
				return -1;
			}
		}		

		//전류 제한값 입력 검사 
		if(pStep->fILimitLow < 0)
		{
			m_strLastErrorString.Format(TEXT_LANG[36], i+1, 1);//"Step %d의 안전 전류 상하한값이 입력 범위를 벗어났습니다."
			return -1;
		}
		
		//전압 제한 상한값을 입력하였을 경우 전압 설정값보다 반드시 커야 한다.
		if(pStep->fVLimitHigh > 0)
		{
			if(pStep->fVLimitHigh <= pStep->fVref)
			{
				m_strLastErrorString.Format(TEXT_LANG[37], i+1);//"Step %d의 안전 전압 상한값이 설정 전압보다 작습니다."
				return -1;
			}
		}

		// 전류 제한 상한값을 입력하였을 경우 전류 설정값보다 반드시 커야 한다.
		/*
		if(pStep->fILimitHigh > 0)
		{
			if(pStep->fILimitHigh <= pStep->fIref || pStep->fILimitHigh <=pStep->fValueLimitHigh)
			{
				m_strLastErrorString.Format("Step %d의 안전 전류 상한값이 설정 전류보다 작습니다.", i+1);
				return -1;
			}
		}
		*/

		//전류 제한 상한값을 입력하였을 경우 전류 설정값보다 반드시 커야 한다.
		if(pStep->fILimitHigh > 0 )
		{
			if(pStep->fILimitHigh <= pStep->fIref || pStep->fILimitHigh <=pStep->fValueLimitHigh)
			{
				m_strLastErrorString.Format(TEXT_LANG[38], i+1);//"Step %d의 안전 전류 상한값이 설정 전류보다 작습니다."
				return -1;
			}
		}

		//전류 제한 상한값을 입력하였을 경우 전류 설정값보다 반드시 커야 한다.
		if(pStep->fILimitHigh > 0 )
		{
			if(pStep->fILimitHigh <=pStep->fValueLimitHigh)
			{
				m_strLastErrorString.Format(TEXT_LANG[38], i+1);//"Step %d의 안전 전류 상한값이 설정 전류보다 작습니다."
				return -1;
			}
		}

		//종료 전압값은 전압 제한 하한값보다 커야 한다.
		if(pStep->fVLimitLow > 0 && pStep->fEndV > 0)
		{
			if(pStep->fVLimitLow > pStep->fEndV)
			{
				m_strLastErrorString.Format(TEXT_LANG[39], i+1);//"Step %d의 종료 전압값이 안전 전압값 보다 작습니다."
				return -1;
			}
		}

		//용량 상한값이 하한값보다 커야 한다.
		if(pStep->fCLimitHigh > 0.0f && pStep->fCLimitLow> 0.0f && pStep->fCLimitHigh < pStep->fCLimitLow)
		{
			m_strLastErrorString.Format(TEXT_LANG[40], i+1);//"Step %d의 안전 용량 상한값이 하한값보다 작습니다."
			return -1;
		}

		//전압 상한 값이 하한값보다 커야 한다.
		if(pStep->chType == PS_STEP_OCV)
		{
			if( pStep->fVLimitHigh < pStep->fVLimitLow)
			{
				m_strLastErrorString.Format(TEXT_LANG[41], i+1);//"Step %d의 안전 전압 상한값이 하한값보다 작습니다."
				return -1;
			}
		}
		else
		{
			if(pStep->fVLimitHigh > 0.0f && pStep->fVLimitLow > 0.0f && pStep->fVLimitHigh < pStep->fVLimitLow)
			{
				m_strLastErrorString.Format(TEXT_LANG[41], i+1);//"Step %d의 안전 전압 상한값이 하한값보다 작습니다."
				return -1;
			}
		}

		//전류 상한 값이 하한값보다 커야 한다.
		if(pStep->fILimitHigh > 0 && pStep->fILimitLow > 0 && pStep->fILimitHigh < pStep->fILimitLow)
		{
			m_strLastErrorString.Format(TEXT_LANG[42], i+1);//"Step %d의 안전 전류 상한값이 하한값보다 작습니다."
			return -1;
		}

		//임피던스 상한 값이 하한값보다 커야 한다.
		if(pStep->fImpLimitHigh > 0 && pStep->fImpLimitLow > 0 && pStep->fImpLimitHigh < pStep->fImpLimitLow)
		{
			m_strLastErrorString.Format(TEXT_LANG[43], i+1);//"Step %d의 안전 저항 상한값이 하한값보다 작습니다."
			return -1;
		}

		//온도 상한 값이 하한값보다 커야 한다.
		if(pStep->fTempHigh > 0 && pStep->fTempLow > 0 && pStep->fTempHigh < pStep->fTempLow)
		{
			m_strLastErrorString.Format(TEXT_LANG[44], i+1);//"Step %d의 안전 온도 상한값이 하한값보다 작습니다."
			return -1;
		}
	
		//Grad check	2008-12 ljb add
		for (int k=0 ; (int)pStep->sGrading_Val.chTotalGrade; k++)
		{

			TRACE("%d",(int)pStep->sGrading_Val.chTotalGrade);
			if (pStep->sGrading_Val.aszGradeCode[k] == '0') break;
			if (pStep->sGrading_Val.faValue1[k] >= pStep->sGrading_Val.faValue2[k])
			{
				m_strLastErrorString.Format(TEXT_LANG[45], i+1);//"입력값 확인\nStep %d의 Grad 하한값이 상한값과 같거나, 큽니다."
				AfxMessageBox(m_strLastErrorString);
				return -1;
			}
		}

	}
	return 0;
}

void CCTSEditorDoc::OnCloseDocument() 
{
	// TODO: Add your specialized code here and/or call the base class
	if(m_bEditedFlag)
		BackUpDataBase();

	RemoveInitStepArray();
	ReMoveStepArray();
	ClearCopyStep();

	RemoveUndoStep();


	DATA_CODE *pData = NULL;
//	for(int i =0; i<m_apProcData.GetSize(); i++)
//	{
//		pData = (DATA_CODE *)m_apProcData[i];
//		if(pData)
//		{
//			delete pData;
//			pData = NULL;
//		}
//	}
//	m_apProcData.RemoveAll();

	SCH_CODE_MSG *pObject;
	int i;
	for( i = 0; i<m_apProcType.GetSize(); i++)
	{
		pObject = (SCH_CODE_MSG *)m_apProcType[i];
		if(pObject)
		{
			delete pObject;
			pObject = NULL;
		}
	}
	m_apProcType.RemoveAll();

	for( i = 0; i<m_apGradeType.GetSize(); i++)
	{
		pObject = (SCH_CODE_MSG *)m_apGradeType[i];
		if(pObject)
		{
			delete pObject;
			pObject = NULL;
		}
	}
	m_apGradeType.RemoveAll();
	CDocument::OnCloseDocument();
}

int CCTSEditorDoc::SaveStepGrade(STEP *pStep, LONG lStepID, LONG lModelID)
{
	if(pStep == NULL)	return -1;
	if(pStep->chType == PS_STEP_END || 
	   pStep->chType == PS_STEP_REST ||
	   pStep->chType == PS_STEP_LOOP)		
	   return 0;;

	//	if((int)pStep->sGrading_Val.chTotalGrade == 0)	return 0;
	
	CGradeRecordSet	recordSet;
	recordSet.m_strFilter.Format("[StepID] = %ld AND [ModelID] = %ld", lStepID, lModelID);
	recordSet.m_strSort.Format("[Value]");

	try
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return -1;
	}

	while(!recordSet.IsEOF())	
	{
		try						//Delete All Step Grade
		{
			recordSet.Delete();
		}
		catch (CDaoException* e)
		{
			AfxMessageBox(e->m_pErrorInfo->m_strDescription);
			e->Delete();
			recordSet.Close();
			return -1;
		}
		recordSet.MoveNext();
	}

	CString strTemp;
	strTemp.Format("[StepID] = %ld AND [ModelID] = %ld", lStepID, lModelID);

	if(recordSet.Find(AFX_DAO_FIRST, strTemp))	//Delete Error
	{
		recordSet.Close();
		return -1;
	}

	for(int i = 0; i<(int)pStep->sGrading_Val.chTotalGrade ; i++)		//Save All Step
	{
		recordSet.AddNew();
		recordSet.m_StepID = lStepID;
		recordSet.m_ModelID = lModelID;
		recordSet.m_GradeItem = pStep->sGrading_Val.lGradeItem[i];
		recordSet.m_Value = pStep->sGrading_Val.faValue1[i];
		recordSet.m_Value1 = pStep->sGrading_Val.faValue2[i];

		recordSet.m_GradeCode.Format("%c", pStep->sGrading_Val.aszGradeCode[i]);
		recordSet.m_GradeIndex = i+1;
		recordSet.Update();
	}
	recordSet.Close();
	return 0;

}

int CCTSEditorDoc::SavePreTestParam(LONG lTestID, LONG lModelID)
{

	CPreTestCheckRecordSet	recordSet;
	recordSet.m_strFilter.Format("[TestID] = %ld AND [ModelID] = %ld", lTestID, lModelID);
	recordSet.m_strSort.Format("[CheckID]");

	try
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return -1;
	}

	if(recordSet.IsBOF())	// Add New
	{
		recordSet.AddNew();
		recordSet.m_TestID = lTestID;
		recordSet.m_ModelID = lModelID;
		recordSet.m_AutoTime = (DATE)0;
		recordSet.m_AutoProYN = FALSE;
	}	
	else					//Update
	{
		recordSet.Edit();
	}
	
	recordSet.m_DeltaVoltage = m_sPreTestParam.fDeltaVoltage;
	recordSet.m_CurrentRange = m_sPreTestParam.fMaxCurrent;
	recordSet.m_MaxV = m_sPreTestParam.fMaxVoltage;
	recordSet.m_MinV = m_sPreTestParam.fMinVoltage;
	recordSet.m_OCVLimit = m_sPreTestParam.fOCVLimitVal;
	recordSet.m_TrickleCurrent = m_sPreTestParam.fTrickleCurrent;	//OCV 하한 값으로 사용 //★★ ljb 2009119  ★★★★★★★★
	recordSet.m_MaxFaultBattery	= m_sPreTestParam.fMaxFaultNo;
	recordSet.m_TrickleTime	= m_sPreTestParam.lTrickleTime;
	recordSet.m_PreTestCheck =	m_sPreTestParam.bPreTest;
	recordSet.m_DeltaVoltageLimit = m_sPreTestParam.fDeltaVoltageLimit;

	recordSet.Update();
	recordSet.Close();

	memcpy(&m_sInitPreTestParam, &m_sPreTestParam, sizeof(TEST_PARAM));

	return 0;
}

int CCTSEditorDoc::RequeryStepGrade(STEP *pStep, LONG lStepID)
{
	if(pStep == NULL)	return -1;
	
	CGradeRecordSet	recordSet;
	recordSet.m_strFilter.Format("[StepID] = %ld", lStepID);
	recordSet.m_strSort.Format("[GradeID]");

	try
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return -1;
	}

	if(recordSet.IsBOF())
	{
		recordSet.Close();
		return -1;		//Zero Grading Step
	}

	//2008-12 ljb Grade 레코드는 등급레코드(count) 다음 조건레코드(count)읽어들이는 코드로 변경
	pStep->sGrading_Val.chTotalGrade = recordSet.GetRecordCount();
	recordSet.MoveFirst();
	pStep->sGrading_Val.iGradeCount=1;
	for(int i = 0; i<recordSet.GetRecordCount() ; i++)		//Save All Step
	{
		pStep->sGrading_Val.lGradeItem[i] = recordSet.m_GradeItem; 
		pStep->sGrading_Val.faValue1[i] = recordSet.m_Value; 
		pStep->sGrading_Val.faValue2[i] = recordSet.m_Value1; 	
		pStep->sGrading_Val.aszGradeCode[i] = recordSet.m_GradeCode.GetAt(0);
		if (recordSet.m_GradeCode == "0" || recordSet.m_GradeCode == "&" || recordSet.m_GradeCode == "+")
			pStep->sGrading_Val.iGradeCount=2;
			
		recordSet.MoveNext();
		continue;
	}

//////////////////////////////////////////////////////////////
/*
//20081007 kjh Ah + ESR or F + ESR을 위해 변경
	recordSet.MoveLast();
	pStep->sGrading_Val.chTotalGrade = recordSet.m_GradeIndex;
	recordSet.MoveFirst();

	int nGradeType = 0;
	for(int i = 0; i<recordSet.GetRecordCount() ; i++)		//Save All Step
	{
		switch(pStep->chType)
		{
		case PS_STEP_OCV:
		case PS_STEP_IMPEDANCE:
		case PS_STEP_CHARGE:
		case PS_STEP_DISCHARGE:
			if(recordSet.m_GradeItem != PS_GRADE_CAPACITY)
				nGradeType = 1;
			else
				nGradeType = 0;
			break;
		default:
			nGradeType = 0;
			break;
		}
		pStep->sGrading_Val[nGradeType].lGradeItem = recordSet.m_GradeItem; 
		pStep->sGrading_Val[nGradeType].faValue1[recordSet.m_GradeIndex-1] = recordSet.m_Value; 
		pStep->sGrading_Val[nGradeType].faValue2[recordSet.m_GradeIndex-1] = recordSet.m_Value1; 	
		pStep->sGrading_Val[nGradeType].aszGradeCode[recordSet.m_GradeIndex-1] = recordSet.m_GradeCode.GetAt(0);
		recordSet.MoveNext();
	}
*/
	
//////////////////////////////////////////////////////////////

/* //20081007 kjh Ah + ESR or F + ESR 적용 전
	recordSet.MoveLast();
	pStep->sGrading_Val.chTotalGrade = (char)recordSet.GetRecordCount();
	recordSet.MoveFirst();

	for(int i = 0; i<(int)pStep->sGrading_Val.chTotalGrade ; i++)		//Save All Step
	{
		pStep->sGrading_Val.lGradeItem = recordSet.m_GradeItem;
		pStep->sGrading_Val.faValue1[i] = recordSet.m_Value; 
		pStep->sGrading_Val.faValue2[i] = recordSet.m_Value1; 	

		pStep->sGrading_Val.aszGradeCode[i] = recordSet.m_GradeCode.GetAt(0);
		recordSet.MoveNext();
	}*/
	recordSet.Close();
	return 0;
}

int CCTSEditorDoc::PermissionCheck(int nAction)
{
	return (nAction & m_LoginData.nPermission);
}
BOOL CCTSEditorDoc::RequeryGradeType()
{
	CString strType, strMsg;
	CString strDBName = GetDataBaseName();
	int nCount = 0;
	
	CDaoDatabase  db;
	try
	{
		db.Open(strDBName);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);	
		e->Delete();
		return FALSE;
	}

	COleVariant data;
	CDaoRecordset rs(&db);;	

	CString strSQL("SELECT ItemID, ItemName FROM GradeItem ORDER BY ItemID");			
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);	
		e->Delete();
		return FALSE;
	}

	if(rs.IsBOF())
	{
		rs.Close();
		return FALSE;
	}

	SCH_CODE_MSG	*pObject;
	pObject = new SCH_CODE_MSG;
	pObject->nCode = 0;
	pObject->nData = 0;
	// sprintf(pObject->szMessage, "임의공정");
	sprintf(pObject->szMessage, "None");
	m_apProcType.Add(pObject);
	
	while(!rs.IsEOF())
	{
		pObject = new SCH_CODE_MSG;
		ASSERT(pObject);
		ZeroMemory(pObject, sizeof(SCH_CODE_MSG));
		data = rs.GetFieldValue(0);
		pObject->nCode = (int)data.lVal;
		pObject->nData = 0;
		data = rs.GetFieldValue(1);
		strMsg = data.pbVal;
		sprintf(pObject->szMessage, "%s", strMsg);
		m_apGradeType.Add(pObject);
		
		TRACE("Grade Type Id %d : %s\n", pObject->nCode, strMsg);
		rs.MoveNext();
	}
	rs.Close();

	return TRUE;
}
BOOL CCTSEditorDoc::RequeryProcType()
{
	CProcTypeRecordSet	rs;
	rs.m_strFilter.Format("[ProcType] >= %d", (PS_STEP_CHARGE << 16));
	rs.m_strSort.Format("[ProcType]");

	try
	{
		rs.Open();
	}
	catch (CDaoException *e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);	
		e->Delete();
		return FALSE;
	}
	
	if(rs.IsBOF())
	{
		rs.Close();
		return FALSE;
	}

	SCH_CODE_MSG	*pObject;
	/* 20131105 
	pObject = new SCH_CODE_MSG;
	pObject->nCode = 0;
	pObject->nData = 0;
	sprintf(pObject->szMessage, "None");
	m_apProcType.Add(pObject);
	*/

	while(!rs.IsEOF())
	{
		pObject = new SCH_CODE_MSG;
		ASSERT(pObject);
		ZeroMemory(pObject, sizeof(SCH_CODE_MSG));
		pObject->nCode = rs.m_ProcType;
		pObject->nData = rs.m_ProcID;
		m_apProcType.Add(pObject);
		sprintf(pObject->szMessage, "%s", rs.m_Description);
		TRACE("Proc Type Id %d : %s\n", rs.m_ProcType, rs.m_Description);
		rs.MoveNext();
	}
	rs.Close();
	return TRUE;
}

int CCTSEditorDoc::GetProcID(int nIndex)
{
	if(nIndex < 0 || nIndex >=m_apProcType.GetSize())	return 0;

	SCH_CODE_MSG *pData = (SCH_CODE_MSG *)m_apProcType[nIndex];
	return pData->nCode;

}

int CCTSEditorDoc::GetProcIndex(int nID)
{
	SCH_CODE_MSG *pData;

	for(int i =0; i<m_apProcType.GetSize(); i++)
	{
		pData = (SCH_CODE_MSG *)m_apProcType[i];
		if(pData->nCode == nID)
		{
			return i;
		}
	}

	return 0;
}

BOOL CCTSEditorDoc::AddCopyStep(int nStepNo)
{
	STEP *pStep, *pSourceStep;
	pStep = new STEP;
	ASSERT(pStep);
	ZeroMemory(pStep, sizeof(STEP));

	pSourceStep = GetStepData(nStepNo);

	if(pSourceStep == NULL)		return FALSE;

	pStep->nEditState = COPY_DATA;
	memcpy(pStep, pSourceStep, sizeof(STEP));
	m_apCopyStep.Add(pStep);
	return TRUE;
}

BOOL CCTSEditorDoc::PasteStep(int nStepNo)
{
	if(nStepNo < 0)		return FALSE;
	
	int nTotStepCount = GetTotalStepNum();

	STEP *pStep, *pSourceStep;
	int i = 0;
	
	for(i = 0; i<m_apCopyStep.GetSize(); i++)
	{
		pSourceStep = (STEP *)m_apCopyStep[i];
		if(nTotStepCount >= nStepNo)	
		{
			pStep = new STEP;
			memcpy(pStep, pSourceStep, sizeof(STEP));
			pStep->chStepNo = nStepNo + i;
			m_apStep.InsertAt(nStepNo-1 + i , pStep);

			pSourceStep->chStepNo = pStep->chStepNo;		//Undo 동작을 위해 
		}
		else
		{
			pStep = new STEP;
			memcpy(pStep, pSourceStep, sizeof(STEP));
			
			pStep->chStepNo = nTotStepCount + i + 1;
			m_apStep.Add(pStep);
			pSourceStep->chStepNo = pStep->chStepNo;		//Undo 동작을 위해
		}
	}

	nTotStepCount = GetTotalStepNum();
	for( i =0; i<nTotStepCount; i++)
	{
		pStep = (STEP *)m_apStep[i];
		TRACE("Step No %d\n", pStep->chStepNo);
		pStep->chStepNo = i+1;
	}

#if _DEBUG
	for(i = 0; i<m_apCopyStep.GetSize(); i++)
	{
		pSourceStep = (STEP *)m_apCopyStep[i];
		
		TRACE("Copyed Step %d\n", pSourceStep->chStepNo);
	}
#endif

	return TRUE;
}

BOOL CCTSEditorDoc::InsertStep(int nStepIndex, STEP *pStep)
{
	if(pStep == NULL)	return FALSE;

	int nTotStepCount = GetTotalStepNum();
	if(nStepIndex < 0 || nStepIndex > nTotStepCount)		return FALSE;
	
	STEP *pInsertStep = NULL;
	pInsertStep = new STEP;
	memcpy(pInsertStep, pStep, sizeof(STEP));
	
	if(nTotStepCount > nStepIndex)	
	{
		m_apStep.InsertAt(nStepIndex , pInsertStep);
	}
	else
	{
		m_apStep.Add(pInsertStep);
	}

	nTotStepCount = GetTotalStepNum();
	for(int  i =0; i<nTotStepCount; i++)
	{
		pInsertStep = (STEP *)m_apStep[i];
		pInsertStep->chStepNo = i+1;
	}

	return TRUE;
}

BOOL CCTSEditorDoc::BackUpDataBase()
{

//	CString strCurFolder = AfxGetApp()->GetProfileString("Path" ,"CTSEditor");	//Get Current Folder(PowerFormation Folde)
//	if(strCurFolder.IsEmpty())
//	{
//		char szBuff[256];
//		if(GetCurrentDirectory(511, szBuff) < 1)	//Get Current Direcotry Name
//		{
//			return FALSE;	
//		}
//		strCurFolder = szBuff;
//	}

	CString strCurFolder;
	CString strDBName = GetDataBaseName();
	if(strDBName.IsEmpty())	return FALSE;
	
	char szFrom[256], szTo[256];
	ZeroMemory(szFrom, 256);
	ZeroMemory(szTo, 256);
	
	strCurFolder = strDBName.Left(strDBName.ReverseFind('\\'));
	if(strCurFolder.IsEmpty())	return FALSE;

	
	sprintf(szTo, "%s\\BackUp", strCurFolder);

	CFileStatus status;
	if( CFile::GetStatus(szTo, status)) 
	{
	} 
	else 
	{
		CreateDirectory(szTo, NULL);
	}

	sprintf(szFrom, "%s", strDBName);
	sprintf(szTo, "%s\\BackUp\\%s", strCurFolder, PS_SCHEDULE_DATABASE_NAME);

	LPSHFILEOPSTRUCT lpFileOp = new SHFILEOPSTRUCT;
	ZeroMemory(lpFileOp, sizeof(SHFILEOPSTRUCT));
    lpFileOp->hwnd = NULL; 
    lpFileOp->wFunc = FO_COPY; 
    lpFileOp->pFrom = szFrom; 
    lpFileOp->pTo = szTo; 
    lpFileOp->fFlags = FOF_NOCONFIRMMKDIR|FOF_FILESONLY|FOF_NOCONFIRMATION ;//FOF_NOCONFIRMATION | FOF_NOERRORUI | FOF_FILESONLY ; 
    lpFileOp->fAnyOperationsAborted = FALSE; 
    lpFileOp->hNameMappings = NULL; 
    lpFileOp->lpszProgressTitle = NULL; 
	
	SHFileOperation(lpFileOp);

	delete lpFileOp;
	lpFileOp = NULL;
	
	return TRUE;
}

//초기 Load한 것과 비교해서 변경 되었는지 확인 
BOOL CCTSEditorDoc::IsEdited()
{
	if(m_apStep.GetSize() < 1 && m_apInitStep.GetSize() < 1)	return FALSE;

	if(memcmp(&m_sInitPreTestParam, &m_sPreTestParam, sizeof(TEST_PARAM)) != 0)	return TRUE;

	if(m_apInitStep.GetSize() != m_apStep.GetSize() )	return TRUE;

	STEP *pInitStep, *pStep;
	for(int i=0; i<m_apStep.GetSize(); i++)
	{
		pInitStep = (STEP *)m_apInitStep.GetAt(i);
		pStep = (STEP *)m_apStep.GetAt(i);

		if(memcmp(pInitStep, pStep, sizeof(STEP)) != 0)	
		{
//			memcpy(pInitStep, pStep, sizeof(STEP)); //for debugging
			return TRUE;
		}
		
	}
	return FALSE;
}

//Undo 할수 있도록 현재 step의 Data를 Backup한다.
void CCTSEditorDoc::MakeUndoStep()
{
/*	RemoveUndoStep();

	//copy current data
	STEP *pStep, *pStep2;
	int nStepSize = m_apStep.GetSize();
	for (int i = 0 ; i<nStepSize ; i++)
	{
		if( ( pStep = (STEP *)m_apStep.GetAt(i) ) != NULL )
		{
			pStep2 =  new STEP;
			memcpy(pStep2, pStep, sizeof(STEP));
			m_apUpdoStep.Add(pStep2);
		}
	}
	*/

	STEP *pStep, *pStep2;
	CPtrArray *pPtrArray;
	POSITION pos;

	//이전에 Undo를 몇번했으면 이후 목록은 삭제한다.
	if(m_posUndoList != m_aPtrArrayList.GetTailPosition())
	{
		pos = m_posUndoList;
		if(pos)
		{
			m_aPtrArrayList.GetNext(pos);
			while(pos)	
			{
				RemoveUndoList(pos);
				m_aPtrArrayList.RemoveAt(pos);
				pos = m_posUndoList;
				m_aPtrArrayList.GetNext(pos);
				TRACE("Delete undo list %d\n", m_aPtrArrayList.GetCount());
			}
		}
		else
		{
			RemoveUndoStep();	//remove all undo step list
		}
	}

	//최대 개수를 넘으면 가장 앞쪽것을 삭제 한다.
	if(m_aPtrArrayList.GetCount() >= MAX_UNDO_COUNT)
	{
		pos = m_aPtrArrayList.GetHeadPosition();
		RemoveUndoList(pos);
		m_aPtrArrayList.RemoveHead();
		
		TRACE("Delete undo list head\n");
	}
	
	//현재 목록을 제일 뒤에 추가한다.
	pPtrArray = new CPtrArray;
	int nStepSize = m_apStep.GetSize();
	for (int i = 0 ; i<nStepSize ; i++)
	{
		if( ( pStep = (STEP *)m_apStep.GetAt(i) ) != NULL )
		{
			pStep2 =  new STEP;
			memcpy(pStep2, pStep, sizeof(STEP));
			pPtrArray->Add(pStep2);
		}
	}

	m_aPtrArrayList.AddTail(pPtrArray);
	m_posUndoList = m_aPtrArrayList.GetTailPosition();
	TRACE("Add undo list at tail %d\n", m_aPtrArrayList.GetCount());

	return;
}

BOOL CCTSEditorDoc::UpdoStepEdit(int &nStartStep, int &nEndStep)
{
	STEP *pStep, *pStep2;
	
/*	ReMoveStepArray();

	//copy current data
	int nStepSize = m_apUpdoStep.GetSize();
	for (int i = 0 ; i<nStepSize ; i++)
	{
		if(( pStep = (STEP *)m_apUpdoStep.GetAt(i) ) != NULL )
		{
			pStep2 =  new STEP;
			memcpy(pStep2, pStep, sizeof(STEP));
			m_apStep.Add(pStep2);
		}
	}

	//1개의 Undo만 지원하므로 return FALSE;
	return FALSE;
*/
	
	if(m_posUndoList == NULL)	return FALSE;				//Undo 할 이전 내용이 없음 
	
	CPtrArray *pPtrArray = (CPtrArray *)m_aPtrArrayList.GetPrev(m_posUndoList);
	if(pPtrArray)
	{
		ReMoveStepArray();

		//copy current data
		int nStepSize = pPtrArray->GetSize();
		for (int i = 0 ; i<nStepSize ; i++)
		{
			if(( pStep = (STEP *)pPtrArray->GetAt(i) ) != NULL )
			{
				pStep2 =  new STEP;
				memcpy(pStep2, pStep, sizeof(STEP));
				m_apStep.Add(pStep2);
			}
		}
	}
	else
	{
		return FALSE;
	}
	TRACE("Undo list %d\n", m_aPtrArrayList.GetCount());
	return TRUE;
}

BOOL CCTSEditorDoc::RedoStepEdit()
{
	STEP *pStep, *pStep2;

	if(m_posUndoList == NULL)
	{
		m_posUndoList = m_aPtrArrayList.GetHeadPosition();
		if(m_posUndoList == NULL)	return FALSE;				//Undo 할 이전 내용이 없음 
	}
	else
	{
		if( m_posUndoList == m_aPtrArrayList.GetTailPosition())	
		{
			return FALSE;
		}
		else
		{
			m_aPtrArrayList.GetNext(m_posUndoList);
			if(m_posUndoList == NULL)
			{
				return FALSE;
			}
		}
	}

	CPtrArray *pPtrArray = (CPtrArray *)m_aPtrArrayList.GetNext(m_posUndoList);
	if(pPtrArray)
	{
		ReMoveStepArray();

		//copy current data
		int nStepSize = pPtrArray->GetSize();
		for (int i = 0 ; i<nStepSize ; i++)
		{
			if(( pStep = (STEP *)pPtrArray->GetAt(i) ) != NULL )
			{
				pStep2 =  new STEP;
				memcpy(pStep2, pStep, sizeof(STEP));
				m_apStep.Add(pStep2);
			}
		}
	}
	return TRUE;
}

//Remove All Undo Step
void CCTSEditorDoc::RemoveUndoStep()
{
	m_posUndoList = m_aPtrArrayList.GetHeadPosition();
	while(m_posUndoList)
	{
		RemoveUndoList(m_posUndoList);
		m_aPtrArrayList.RemoveHead();
		m_posUndoList = m_aPtrArrayList.GetHeadPosition();
	}
}



BOOL CCTSEditorDoc::RemoveUndoList(POSITION pos)
{
	if(pos == NULL)		return FALSE;

	STEP *pStep;
	CPtrArray *pPtrArray = (CPtrArray *)m_aPtrArrayList.GetAt(pos);
	int nStepSize = pPtrArray->GetSize();
	for (int i = 0 ; i<nStepSize ; i++)
	{
		if( ( pStep = (STEP *)pPtrArray->GetAt(0) ) != NULL )
		{
			pPtrArray->RemoveAt(0);  
		   delete pStep;			// Delete the original element at 0.
		   pStep = NULL;
		}
	}
	pPtrArray->RemoveAll();
	delete pPtrArray;
	pPtrArray = NULL;

	return TRUE;
}

float CCTSEditorDoc::VUnitTrans(float fData, BOOL bSave)
{
	//Display => Save(m단위)
	if(bSave)
	{
		return fData*m_VtgUnitInfo.fTransfer;
	}
	else
	{
		return fData/m_VtgUnitInfo.fTransfer;
	}

}

float CCTSEditorDoc::IUnitTrans(float fData, BOOL bSave)
{
	//Display => Save(m단위)
	if(bSave)
	{
		return fData*m_CrtUnitInfo.fTransfer;
	}
	else
	{
		return fData/m_CrtUnitInfo.fTransfer;
	}
}
float CCTSEditorDoc::CUnitTrans(float fData, BOOL bSave)
{
	//Display => Save(m단위)
	if(bSave)
	{
		return fData*m_CapUnitInfo.fTransfer;
	}
	else
	{
		return fData/m_CapUnitInfo.fTransfer;
	}
}
float CCTSEditorDoc::WattUnitTrans(float fData, BOOL bSave)
{
	//Display => Save(m단위)
	if(bSave)
	{
		return fData*m_WattUnitInfo.fTransfer;
	}
	else
	{
		return fData/m_WattUnitInfo.fTransfer;
	}
}

float CCTSEditorDoc::WattHourUnitTrans(float fData, BOOL bSave)
{
	//Display => Save(m단위)
	if(bSave)
	{
		return fData*m_WattHourUnitInfo.fTransfer;
	}
	else
	{
		return fData/m_WattHourUnitInfo.fTransfer;
	}
}


STEP * CCTSEditorDoc::GetLastStepData()
{
	int a = m_apStep.GetSize();
	if(a < 1)	return NULL;

	return (STEP *)m_apStep[a-1];
}

//현재 Step 보다 작은  Step에서 Capa ref step을 찾는다.
int CCTSEditorDoc::GetLastRefCapStepNo(int nCurStepNo)
{
	if(nCurStepNo > GetTotalStepNum() || nCurStepNo < 1)	return 0;

	int nCapRefStep = 0;
	STEP *pStep;
	for(int s=0; s<nCurStepNo; s++)
	{
		pStep = GetStepData(s+1);
		
		if(pStep)
		{
			if(pStep->bUseActualCapa)
			{
				nCapRefStep = s+1;
			}
		}
	}

	return nCapRefStep;
}

CString CCTSEditorDoc::GetSimulationTitle(CString strFileName)
{
	//파일명을 YYYYMMDDHHmmSS-제목.csv
	CString strFile(strFileName);
	int nStart = strFile.Find('-');
	int nEnd = strFile.ReverseFind('.');

	if(nStart < 0 || nEnd < 0 || nStart >= nEnd)		return strFile;

	return strFile.Mid(nStart+1, nEnd-nStart-1);

}

STEP * CCTSEditorDoc::GetStepDataGotoID(int nGotoID)
{
	int nTotalStepNum = GetTotalStepNum();
	
	for(int i = 0; i < nTotalStepNum; i++ )
	{
		STEP * pStep = (STEP *)m_apStep[i];
		if(pStep->nGotoStepID == nGotoID)
			return (STEP *)m_apStep[i];
	}

	return NULL;
}

BOOL CCTSEditorDoc::UpdateMainServer(long lModelNo, CString strModelName, CString strCreator,  CString strState)
{
	int nValSize = 0;
	TCHAR szValue[256];
	ZeroMemory(szValue, sizeof(TCHAR)*256);

	CString strSQL = "";
	CString strQuery = "";
	CString strWhere = "";
	CDatabase db;

	CString strError;

	if(strState =="2")
	{
		strSQL.Format("INSERT INTO [BProject].[dbo].[LotInfo] ([LotNo],[LotName],[LotCreator])");
		strQuery.Format("VALUES(%d, '%s', '%s')", lModelNo, strModelName, strCreator);
	}
	else if(strState == "3")
	{
		strSQL.Format("UPDATE [BProject].[dbo].[LotInfo]");
		strQuery.Format("SET  [LotNo] = %d ,[LotName] = '%s', [LotCreator]='%s'", lModelNo, strModelName, strCreator);
		strWhere.Format("WHERE [LotNo] = %d", lModelNo);
	}
	

	if(strQuery != "")
	{
		if(db.Open(_T("BProject"), FALSE, FALSE, _T("ODBC;UID=sa;PWD=pnesolution")) == FALSE)
		{
			strError = TEXT_LANG[46];//"DataBase Server 연결에 실패하였습니다."
			return FALSE;
		}

		try
		{
			db.BeginTrans();
			
		//	strSQL.Format("INSERT INTO [BProject].[dbo].[LotInfo] ([LotName])");
		//	strQuery.Format("VALUES('abc')");
			db.ExecuteSQL(strSQL+strQuery+strWhere);		
			db.CommitTrans();
		}
		catch (CDBException* e)
		{
			strError = e->m_strError;
			AfxMessageBox(strError);
    		e->Delete();     //DataBase Open Fail
			db.Rollback();
			
			return FALSE;
		}

		db.Close();
	}
		
	return TRUE;

/*	db.BindParameters(db.m_hdbc);

	SQLPrepare(db.m_hdbc)

	TCHAR szTemp[512];	

	_stprintf(szTemp, strProc);
	
	// 먼저 SQL 문을 바인딩한다
	if(SQLPrepare((_TUCHAR *)szTemp) != SQL_SUCCESS){
		::MessageBox(NULL, _T("Error"), _T("Error"), 0);					
		return FALSE;
	}	
	
	// 프로시져 호출
	ret = SQLExecute(m_hStmt);
	if((ret != SQL_SUCCESS)  &&  (ret != SQL_SUCCESS_WITH_INFO)  &&  (ret != SQL_NO_DATA)) {
		ErrorHandling(m_hStmt);
		return FALSE;
	}
	*/		
	

}

bool CCTSEditorDoc::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCTSEditorDoc"), _T("TEXT_CCTSEditorDoc_CNT"), _T("TEXT_CCTSEditorDoc_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CCTSEditorDoc_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCTSEditorDoc"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}