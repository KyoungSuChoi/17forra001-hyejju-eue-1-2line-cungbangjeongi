This describes how to use PEload and PEstore with VB and Delphi to save/restore the state of a
PE object to file.

-----------------------------------------------------------------------
VB3/VB4-16 example code
-----------------------------------------------------------------------

DECLARATIONS NEEDED
Declare Function PEload Lib "PEGRAPHS.DLL" (ByVal hObject%, lphGlbl As Any) As Integer
Declare Function PEstore Lib "PEGRAPHS.DLL" (ByVal hObject%, lphGlbl As Any, lpdwSize As Any) As Integer
Declare Function GlobalLock Lib "Kernel" (ByVal hGlobal%) As Long
Declare Function GlobalUnlock Lib "Kernel" (ByVal hGlobal%) As Integer
Declare Function GlobalAlloc Lib "Kernel" (ByVal nHowTo%, ByVal dwSize As Long) As Integer
Declare Function GlobalFree Lib "Kernel" (ByVal hGlobal%) As Integer
Declare Function hwrite Lib "Kernel" Alias "_hwrite" (ByVal hGlobal%, lpData As Any, ByVal dwSize As Long) As Long
Declare Function hread Lib "Kernel" Alias "_hread" (ByVal hGlobal%, lpData As Any, ByVal dwSize As Long) As Long
Declare Function OpenFile Lib "Kernel" (ByVal lpszFilename$, lpOFstruct As Any, ByVal nAccess%) As Integer
Declare Function lclose Lib "Kernel" Alias "_lclose" (ByVal hFile%) As Integer
Type OFstruct
    Bytes As String * 1
    FixedDisk As String * 1
    ErrCode As Integer
    reserved As Long
    szPathName As String * 128
End Type

TO STORE THE STATE
Dim hGlobal As Integer
Dim lpdata As Long
Dim dwSize As Long
Dim dwWritten As Long
Dim MyOF As OFstruct
Dim hFile As Integer
Dim test as Integer
hGlobal = 0
dwSize = 0
test = PEstore(PEGraph1, hGlobal, dwSize)
lpdata = GlobalLock(hGlobal)
hFile = OpenFile("Test.dat", MyOF, 4098) '4098=CREATE+READWRITE, 0=READ, 1=WRITE
dwWritten = hwrite(hFile, dwSize, 4) 'save size so we know how much to allocate later
dwWritten = hwrite(hFile, ByVal lpdata, dwSize) 'save actual data
test = lclose(hFile)
test = GlobalFree(hGlobal)

TO LOAD THE STATE
Dim hGlobal As Integer
Dim lpdata As Long
Dim dwSize As Long
Dim dwRead As Long
Dim MyOF As OFstruct
Dim hFile As Integer
Dim test As Integer
hFile = OpenFile("Test.dat", MyOF, 0) '0=READ, 1=WRITE, 2=READWRITE
dwRead = hread(hFile, dwSize, 4) 'read size to allocate correct amount
hGlobal = GlobalAlloc(2, dwSize) 
lpdata = GlobalLock(hGlobal)
dwRead = hread(hFile, ByVal lpdata, dwSize) 'read in actual data
test = GlobalUnlock(hGlobal)
test = lclose(hFile)
test = PEload(PEGraph1, hGlobal)
test = GlobalFree(hGlobal)
PEGraph1.PEactions = 0 'reinitialize after PEload


-----------------------------------------------------------------------
VB4-32 example code
-----------------------------------------------------------------------
DECLARATIONS NEEDED
Declare Function PEload Lib "PEGRAP32.DLL" (ByVal hObject&, lphGlbl As Any) As Long
Declare Function PEstore Lib "PEGRAP32.DLL" (ByVal hObject&, lphGlbl As Any, lpdwSize As Any) As Long
Declare Function GlobalLock Lib "Kernel32" (ByVal hGlobal&) As Long
Declare Function GlobalUnlock Lib "Kernel32" (ByVal hGlobal&) As Long
Declare Function GlobalAlloc Lib "Kernel32" (ByVal nHowTo&, ByVal dwSize As Long) As Long
Declare Function GlobalFree Lib "Kernel32" (ByVal hGlobal&) As Long
Declare Function hwrite Lib "Kernel32" Alias "_hwrite" (ByVal hGlobal&, lpdata As Any, ByVal dwSize As Long) As Long
Declare Function hread Lib "Kernel32" Alias "_hread" (ByVal hGlobal&, lpdata As Any, ByVal dwSize As Long) As Long
Declare Function OpenFile Lib "Kernel32" (ByVal lpszFilename$, lpOFstruct As Any, ByVal nAccess&) As Long
Declare Function lclose Lib "Kernel32" Alias "_lclose" (ByVal hFile&) As Long
Type OFstruct
    Bytes As String * 1
    FixedDisk As String * 1
    ErrCode As Integer
    reserved As Long
    szPathName As String * 128
End Type

TO STORE THE STATE
Dim hGlobal As Long
Dim lpdata As Long
Dim dwSize As Long
Dim dwWritten As Long
Dim MyOF As OFstruct
Dim hFile As Long
Dim test As Long
hGlobal = 0
dwSize = 0
test = PEstore(PEGraph1, hGlobal, dwSize)
lpdata = GlobalLock(hGlobal)
hFile = OpenFile("Test.dat", MyOF, 4098) '4098=CREATE+READWRITE, 0=READ, 1=WRITE
dwWritten = hwrite(hFile, dwSize, 4) 'save size so we know how much to allocate later
dwWritten = hwrite(hFile, ByVal lpdata, dwSize) 'save actual data
test = lclose(hFile)
test = GlobalFree(hGlobal)

TO LOAD THE STATE
Dim hGlobal As Long
Dim lpdata As Long
Dim dwSize As Long
Dim dwRead As Long
Dim MyOF As OFstruct
Dim hFile As Long
Dim test As Long
hFile = OpenFile("Test.dat", MyOF, 0) '0=READ, 1=WRITE, 2=READWRITE
dwRead = hread(hFile, dwSize, 4) 'read size to allocate correct amount
hGlobal = GlobalAlloc(2, dwSize)
lpdata = GlobalLock(hGlobal)
dwRead = hread(hFile, ByVal lpdata, dwSize) 'read in actual data
test = GlobalUnlock(hGlobal)
test = lclose(hFile)
test = PEload(PEGraph1, hGlobal)
test = GlobalFree(hGlobal)
PEGraph1.PEactions = 0 'reinitialize after PEload


-----------------------------------------------------------------------
Delphi1 16 bit example code
-----------------------------------------------------------------------
DECLARATIONS NEEDED
Add the Pegrpapi unit to your uses clause.  This unit holds declarations.

FUNCTION TO SAVE OBJECT TO DISK
procedure TForm1.Button1Click(Sender: TObject);
var
hGlobal: Integer;
lpdata: Pointer;
dwSize: Longint;
dwWritten: Longint;
MyOF: TOFStruct;
hFile: Integer;
begin
hGlobal := 0;
dwSize := 0;
if (PEstore(PEGraph1.hObject, @hGlobal, @dwSize) <> 0) then
begin
     lpdata := GlobalLock(hGlobal);
     hFile := OpenFile('c:\Test.dat', MyOF, OF_CREATE+OF_READWRITE);
     dwWritten := _hwrite(hFile, @dwSize, 4);
     dwWritten := _hwrite(hFile, lpdata, dwSize);
     _lclose(hFile);
     GlobalUnlock(hGlobal);
     GlobalFree(hGlobal);
end;
end;

FUNCTION TO LOAD OBJECT FROM DISK
procedure TForm1.Button2Click(Sender: TObject);
var
hGlobal: Integer;
lpdata: Pointer;
dwSize: Longint;
dwRead: Longint;
MyOF: TOFStruct;
hFile: Integer;
begin
hFile := OpenFile('c:\Test.dat', MyOF, 0);
dwRead := _lread(hFile, @dwSize, 4);
hGlobal := GlobalAlloc(GMEM_MOVEABLE, dwSize);
lpdata := GlobalLock(hGlobal);
dwRead := _lread(hFile, lpdata, dwSize);
GlobalUnlock(hGlobal);
_lclose(hFile);
PEload(PEGraph1.hObject, @hGlobal);
GlobalFree(hGlobal);
PEGraph1.PEactions := gReinitAndReset;
end;

-----------------------------------------------------------------------
Delphi2 32 bit example code
-----------------------------------------------------------------------
DECLARATIONS NEEDED
Add the Pegrpapi unit to your uses clause.  This unit holds declarations.

FUNCTION TO SAVE OBJECT TO DISK
procedure TForm1.Button1Click(Sender: TObject);
var
hGlobal: Longint;
lpdata: Pointer;
dwSize: Longint;
dwWritten: Longint;
MyOF: TOFStruct;
hFile: Longint;
begin
hGlobal := 0;
dwSize := 0;
if (PEstore(PEGraph1.hObject, @hGlobal, @dwSize) <> 0) then
begin
     lpdata := GlobalLock(hGlobal);
     hFile := OpenFile('c:\Test.dat', MyOF, OF_CREATE+OF_READWRITE);
     dwWritten := _hwrite(hFile, @dwSize, 4);
     dwWritten := _hwrite(hFile, lpdata, dwSize);
     _lclose(hFile);
     GlobalUnlock(hGlobal);
     GlobalFree(hGlobal);
end;
end;

FUNCTION TO LOAD OBJECT FROM DISK
procedure TForm1.Button2Click(Sender: TObject);
var
hGlobal: Longint;
lpdata: Pointer;
dwSize: Longint;
dwRead: Longint;
MyOF: TOFStruct;
hFile: Longint;
begin
hFile := OpenFile('c:\Test.dat', MyOF, 0);
dwRead := _lread(hFile, @dwSize, 4);
hGlobal := GlobalAlloc(GMEM_MOVEABLE, dwSize);
lpdata := GlobalLock(hGlobal);
dwRead := _lread(hFile, lpdata, dwSize);
GlobalUnlock(hGlobal);
_lclose(hFile);
PEload(PEGraph1.hObject, @hGlobal);
GlobalFree(hGlobal);
PEGraph1.PEactions := gReinitAndReset;
end;


------------------------------------------------------
-- Builder Example -----------------------------------
------------------------------------------------------

Using both Windows file handling and TFileStream

void __fastcall TForm3::Button1Click(TObject *Sender)
{
// Saving to Disk //
HGLOBAL hGlobal;
LPVOID lpdata;
unsigned long dwSize;
unsigned long dwWritten;
OFSTRUCT MyOF;
HANDLE hFile;
hGlobal = 0;
dwSize = 0;
PEstore((HWND) PESGraph1->hObject, &hGlobal, &dwSize);

lpdata = GlobalLock(hGlobal);

TFileStream* tfs;
tfs = new TFileStream("c:\\test.dat", fmCreate);
tfs->WriteBuffer(&dwSize, 4);
tfs->WriteBuffer(lpdata, dwSize);
delete tfs;

/*hFile = (HANDLE) OpenFile("c:\Test.dat", &MyOF, 4098);  // '4098=CREATE+READWRITE, 0=READ, 1=WRITE
WriteFile(hFile, &dwSize, 4, &dwWritten, NULL);         // 'save size so we know how much to allocate later
WriteFile(hFile, lpdata, dwSize, &dwWritten, NULL);     // 'save actual data
_lclose((HFILE)hFile);*/

GlobalFree(hGlobal);

}

//---------------------------------------------------------------------------
void __fastcall TForm3::Button2Click(TObject *Sender)
{
// Loading From Disk //
HGLOBAL hGlobal;
LPVOID lpdata;
unsigned long dwSize;
unsigned long dwRead;
OFSTRUCT MyOF;
HANDLE hFile;
hGlobal = 0;
dwSize = 0;

TFileStream* tfs;
tfs = new TFileStream("c:\\test.dat", fmOpenRead);
tfs->ReadBuffer(&dwSize, 4);
//hFile = (HANDLE) OpenFile("c:\Test.dat", &MyOF, 0); // '0=READ, 1=WRITE, 2=READWRITE
//ReadFile(hFile, &dwSize, 4, &dwRead, NULL);         // 'read size to allocate correct amount

hGlobal = GlobalAlloc(2, dwSize);
lpdata = GlobalLock(hGlobal);

tfs->ReadBuffer(lpdata, dwSize);
//ReadFile(hFile, lpdata, dwSize, &dwRead, NULL);     // 'read in actual data

GlobalUnlock(hGlobal);

delete tfs;
//_lclose((HFILE)hFile);

PEload((HWND) PESGraph1->hObject, &hGlobal);
GlobalFree(hGlobal);
PESGraph1->PEactions = sgReinitAndReset;            // 'reinitialize after PEload

}

