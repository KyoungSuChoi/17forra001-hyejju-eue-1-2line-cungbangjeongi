#if !defined(AFX_CPKVIEWDLG_H__190DBAAB_5AE1_4D1C_9EF7_880B0343C8B4__INCLUDED_)
#define AFX_CPKVIEWDLG_H__190DBAAB_5AE1_4D1C_9EF7_880B0343C8B4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CpkViewDlg.h : header file
//
class CCTSAnalView;
/////////////////////////////////////////////////////////////////////////////
// CCpkViewDlg dialog
//Class  "CTSAnalView.h"

#include "DeviationGraphWnd.h"

class CCpkViewDlg : public CDialog
{
// Construction
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	BOOL SetRowData(double *pData, int nCount, double dDivision);
	void SetLowLimit(BOOL bUse, double dLowLimit);
	void SetHighLimit(BOOL bUse, double dHighLimit);
	double m_dDivision;
	double *m_pData;
	int m_nCellCount;
	double m_dCp;
	double m_dCpk;
	BOOL	m_bHighLimit;
	BOOL	m_bLowLimit;
	double m_dHighLimitVal;
	double m_dLowLimitVal;
	CString	m_strTitle;
	CCpkViewDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CCpkViewDlg();

	CString m_strXAxisLable;

// Dialog Data
	//{{AFX_DATA(CCpkViewDlg)
	enum { IDD = IDD_CPKVIEW_DLG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCpkViewDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CDeviationGraphWnd	m_Graph;

	// Generated message map functions
	//{{AFX_MSG(CCpkViewDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSaveImgFile();
	afx_msg void OnPrintImg();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CPKVIEWDLG_H__190DBAAB_5AE1_4D1C_9EF7_880B0343C8B4__INCLUDED_)
