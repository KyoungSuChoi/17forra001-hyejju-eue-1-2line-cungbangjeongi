// CTSAnalDoc.cpp : implementation of the CCTSAnalDoc class
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "mainfrm.h"

#include "CTSAnalDoc.h"
#include "ChCodeRecordSet.h"
#include "ReportDataListRecordSet.h"
#include "DataFieldSetDlg.h"

#include "CpkSetDlg.h"
#include "CpkViewDlg.h"
#include "SerarchDataDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCTSAnalDoc

static STR_Y_AXIS_SET g_Default_Setting[MAX_MUTI_AXIS] = 
{
	{ RGB(230,   0,   0), 0, 1, TRUE,      0, 5, "V(V)"	   },
	{ RGB(  0,   0, 200), 0, 1,	TRUE,  -2000, 2000, "I(㎃)"	   },
	{ RGB(200, 200,   0), 0, 1, FALSE,     0, 2000, "C(㎃h)"   },
	{ RGB(  0, 255, 255), 0, 0, FALSE,     0, 5000, "R(mΩ)"   },
	{ RGB(255,   0, 255), 0, 0, FALSE,     0, 200, "T(℃)"	   },
	{ RGB(255, 100,  30), 0, 0, FALSE,     0, 100, "P(㎏f/㎠)" },
};

IMPLEMENT_DYNCREATE(CCTSAnalDoc, CDocument)

BEGIN_MESSAGE_MAP(CCTSAnalDoc, CDocument)
	//{{AFX_MSG_MAP(CCTSAnalDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCTSAnalDoc construction/destruction


bool CCTSAnalDoc::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCTSAnalDoc"), _T("TEXT_CCTSAnalDoc_CNT"), _T("TEXT_CCTSAnalDoc_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CCTSAnalDoc_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCTSAnalDoc"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

CCTSAnalDoc::CCTSAnalDoc()
{
	LanguageinitMonConfig();

	// TODO: add one-time construction code here
	m_strModuleName = "Stage";
	m_strGroupName  = "Group";
	m_bUseGroupSet  = FALSE;
	m_bProgressInit = FALSE;
	m_pProgressWnd  = NULL; 
	m_TotCol        = 0;
	for(int i =0; i<FIELDNO; i++)
	{
		m_field[i].No = 0;
		m_field[i].ID = 0;
		m_field[i].FieldName = "";
		m_field[i].DataType = 0;
		m_field[i].bDisplay = 0;
	}
	m_bLoadCodeDB = FALSE;
	m_bLoadField = FALSE;


	//ADD
	m_nMaxDataPoint = MAX_XAXIS_POINT;
	m_fSecPerPoint = 10.0f;						//Every Data Point Interval is 10 Second
	m_nLoadedFileNum = 0;
	memcpy(m_YSet, g_Default_Setting, sizeof(STR_Y_AXIS_SET)*MAX_MUTI_AXIS);
	m_pFileInfo = NULL;
	m_pData = NULL;
	m_pParsingData = NULL;

	//Channel Color
	m_ChColor[0] = RGB(230,  0,  0);
	m_ChColor[1] = RGB(  0,200,  0);
	m_ChColor[2] = RGB(  0,  0,255);
	m_ChColor[3] = RGB(255,255,  0);
	m_ChColor[4] = RGB(  0,255,255);
	m_ChColor[5] = RGB(255,  0,255);
	m_ChColor[6] = RGB(255,100, 30);
	m_ChColor[7] = RGB( 0,0, 50);
	
	m_GraphLineSet.bAdded = FALSE;
	m_GraphLineSet.bAutoGrid = TRUE;
	m_GraphLineSet.bUserAddGrid = FALSE;
	m_GraphLineSet.colorLine = RGB(192,192,192);
	m_GraphLineSet.devided = 0;
	m_GraphLineSet.fVal[0] = 0.0f;
	m_GraphLineSet.fVal[1] = 5.0f;
	m_GraphLineSet.fVal[2] = 0.1f;
	m_GraphLineSet.lineStyle = 0;
	m_GraphLineSet.nLineNo =0;
	m_GraphLineSet.select = 0;

	m_nXItem = X_TIME;
	m_nXMaxReadPoint = 0;

	m_nModulePerRack = 3;
	m_bUseRackIndex = TRUE;

	m_nTrayColSize = EP_DEFAULT_TRAY_COL_COUNT;
}

CCTSAnalDoc::~CCTSAnalDoc()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}

	if(m_pProgressWnd) 
	{
		delete m_pProgressWnd;
		m_pProgressWnd = NULL;
	}

	STR_MSG_DATA	*pObject;
	for(int i=0; i<m_apProcType.GetSize(); i++)
	{
		pObject = (STR_MSG_DATA	*)m_apProcType.GetAt(i);
		delete pObject;
		pObject = NULL;
	}
	m_apProcType.RemoveAll();
}

/*
STR_TOP_CONFIG GetDefaultTopConfigSet()
{
	STR_TOP_CONFIG strTopConfig;
	memset(&strTopConfig, -1, sizeof(STR_TOP_CONFIG));
	FILE *fp = fopen(FORM_SETTING_SAVE_FILE, "rb");
	if(fp != NULL)
	{
		fread(&strTopConfig, sizeof(STR_TOP_CONFIG), 1, fp);
		fclose(fp);
	}
	return strTopConfig;
}

STR_TOP_CONFIG	GetTopConfig()
{
	STR_TOP_CONFIG strTopConfig;
	memset(&strTopConfig, 0, sizeof(STR_TOP_CONFIG));
	FILE *fp = fopen(FORM_SETTING_SAVE_FILE, "rb");
	if(fp != NULL)
	{
		fseek(fp, sizeof(STR_TOP_CONFIG), SEEK_SET);
		fread(&strTopConfig, sizeof(STR_TOP_CONFIG), 1, fp);
		fclose(fp);
	}
	return strTopConfig;
}

BOOL	WriteTopConfig(STR_TOP_CONFIG topConfig)
{
	FILE *fp = fopen(FORM_SETTING_SAVE_FILE, "rb+");
	if(fp != NULL)
	{
		fseek(fp, sizeof(STR_TOP_CONFIG), SEEK_SET);
		fwrite(&topConfig, sizeof(STR_TOP_CONFIG), 1, fp);
		fclose(fp);
	}
	else
	{
		return FALSE;
	}
	return TRUE;
}
*/

BOOL CCTSAnalDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	return LoadSetting();
}



/////////////////////////////////////////////////////////////////////////////
// CCTSAnalDoc serialization

void CCTSAnalDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CCTSAnalDoc diagnostics

#ifdef _DEBUG
void CCTSAnalDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CCTSAnalDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCTSAnalDoc commands

STR_TOP_CONFIG * CCTSAnalDoc::GetTopConfig()
{

	return &m_TopConfig;	
	
}

int CCTSAnalDoc::ReadTestConditionFile(FILE *fp, STR_CONDITION *pCondition, char *fileID, char *fileVer)
{
	if(fp == NULL || pCondition == NULL)	return FALSE;

	if(fread(&pCondition->modelHeader, sizeof(STR_CONDITION_HEADER), 1, fp) != 1)			return FALSE;
	if(fread(&pCondition->conditionHeader, sizeof(STR_CONDITION_HEADER), 1, fp) != 1)		return FALSE;
	if(fread(&pCondition->testHeader, sizeof(EP_TEST_HEADER), 1, fp) != 1)					return FALSE;
	if(fread(&pCondition->checkParam, sizeof(STR_CHECK_PARAM), 1, fp) != 1)				return FALSE;

	STR_COMMON_STEP *lpBuffer = NULL;
	if(fileID != NULL && fileVer != NULL && strcmp(fileID, RESULT_FILE_ID) == 0)	//공정 결과 파일인 경우 
	{
		//호환을 위해 Version에 맞게 read
		//Result File의 File Ver이 RESULT_FILE_VER1인 것만 STEP_V1 structure로 되어 있다.
		for(int i =0; i<pCondition->testHeader.totalStep; i++)
		{
			if(strcmp(fileVer, RESULT_FILE_VER1) == 0)
			{
				lpBuffer = new STR_COMMON_STEP;
				if(fread((char *)lpBuffer, sizeof(STR_COMMON_STEP), 1, fp) != 1)		return FALSE;
				pCondition->apStepList.Add(lpBuffer);
			}
		}	
	}
	else	//공정 결과 파일이 아닌 경우 
	{
		for(int i =0; i<pCondition->testHeader.totalStep; i++)
		{
			lpBuffer = new STR_COMMON_STEP;
			if(fread((char *)lpBuffer, sizeof(STR_COMMON_STEP), 1, fp) != 1)		return FALSE;
			pCondition->apStepList.Add(lpBuffer);
		}
	}
	return TRUE;
}

CString CCTSAnalDoc::ModuleName(int nModuleID, int nGroupIndex)
{
/*	CString strName;
	if(m_bUseGroupSet && nGroupIndex >= 0)
	{
		strName.Format("%s %d-%d, %s %d", m_strModuleName, (nModuleID-1)/3+1, (nModuleID-1)%3+1, m_strGroupName, nGroupIndex+1);
	}
	else
	{
		strName.Format("%s %d-%d", m_strModuleName, (nModuleID-1)/3+1, (nModuleID-1)%3+1);
	}

	return strName;
*/
	CString strName;
	
	if(m_bUseRackIndex)
	{
		div_t div_result;
		div_result = div( nModuleID-1, m_nModulePerRack );

		if(m_bUseGroupSet && nGroupIndex >= 0)
		{
			strName.Format("%s %d-%d, %s %d", m_strModuleName, div_result.quot+1, div_result.rem+1, m_strGroupName, nGroupIndex+1);
		}
		else
		{
			strName.Format("%s %d-%d", m_strModuleName, div_result.quot+1, div_result.rem+1);
		}
	}
	else
	{
		if(m_bUseGroupSet && nGroupIndex >= 0)
		{
			strName.Format("%s %d, %s %d", m_strModuleName, nModuleID,  m_strGroupName, nGroupIndex+1);
		}
		else
		{
			strName.Format("%s %d", m_strModuleName, nModuleID);
		}
	}
	return strName;
}

//Remove Test Condition List
BOOL CCTSAnalDoc::RemoveCondition(STR_CONDITION *pCondition)
{
	if(pCondition == NULL)	return TRUE;

	int nSize = pCondition->apStepList.GetSize();
	LPVOID pObject; 

	if(nSize > 0)
	{
		for (int i = nSize-1 ; i>=0 ; i--)
		{
			if( (pObject = pCondition->apStepList[i]) != NULL )
			{
				delete pObject; // Delete the original element at 0.
				pObject = NULL;
				pCondition->apStepList.RemoveAt(i);  
			}
		}
		pCondition->apStepList.RemoveAll();
	}

/*	nSize = pCondition->apGradeList.GetSize();
	char *pObject1; 

	if(nSize > 0)
	{
		for (int i = nSize-1 ; i>=0 ; i--)
		{
			if( (pObject1 = (char *)pCondition->apGradeList[i]) != NULL )
			{
				delete [] pObject1; // Delete the original element at 0.
				pObject = NULL;
				pCondition->apGradeList.RemoveAt(i);  
			}
		}
		pCondition->apGradeList.RemoveAll();
	}
*/
	delete pCondition;
	pCondition = NULL;

	return TRUE;
}

CString CCTSAnalDoc::StepTypeMsg(int nType)
{
	CString strMsg;
	switch (nType)
	{
		case EP_TYPE_CHARGE:		strMsg = TEXT_LANG[0];		break;//Charge
		case EP_TYPE_DISCHARGE:		strMsg = TEXT_LANG[1];	break;//DisCharge
		case EP_TYPE_REST:			strMsg = TEXT_LANG[2];		break;//Rest
		case EP_TYPE_OCV:			strMsg = TEXT_LANG[3];			break;//OCV
		case EP_TYPE_IMPEDANCE:		strMsg = TEXT_LANG[4];	break;//DCIR
		case EP_TYPE_END:			strMsg = TEXT_LANG[5];			break;	//End
		default:					strMsg = "";		break;
	}
	return strMsg;
}

CString CCTSAnalDoc::ModeTypeMsg(int nMode)
{
	CString strMsg;
	switch (nMode)
	{
		case EP_MODE_CCCV:		strMsg = TEXT_LANG[6];	break;//CC/CV
		case EP_MODE_CC:		strMsg = TEXT_LANG[7];		break;//CC
		case EP_MODE_CV:		strMsg = TEXT_LANG[8];		break;//CV
		default:				strMsg = "";	break;
	}
	return strMsg;
}

/*
CString CCTSAnalDoc::StepEndString(LPVOID lpStep, int nType)
{
	ASSERT(lpStep);

	CString strTemp, strTemp1;
	char szTemp[128];
	switch(nType)
	{
	case EP_TYPE_CHARGE:			//Charge
		{
			EP_CHARGE_STEP *pStep = (EP_CHARGE_STEP *)lpStep;
			if(pStep->lEndV > 0L)	
			{
				strTemp.Format("V > %.3f", VTG_PRECISION(pStep->lEndV));
			}
	
			if(pStep->lEndI > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strTemp1.Format("I < %.1f", CRT_PRECISION(pStep->lEndI));
				strTemp += strTemp1;
			}
			
			if(pStep->ulEndTime >0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				ConvertTime(szTemp, pStep->ulEndTime);
				strTemp1.Format("t > %s", szTemp);
				strTemp += strTemp1;
			}
			if(pStep->lEndC > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strTemp1.Format("C > %.1f", ETC_PRECISION(pStep->lEndC));
				strTemp += strTemp1;
			}
			if(pStep->lEndDV > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strTemp1.Format("dV > %.3f", VTG_PRECISION(pStep->lEndDV));
				strTemp += strTemp1;
			}
			if(pStep->lEndDI > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strTemp1.Format("dI > %.1f", CRT_PRECISION(pStep->lEndDI));
				strTemp += strTemp1;
			}
			break;
		}

	case EP_TYPE_DISCHARGE:		//Discharge
		{
			EP_DISCHARGE_STEP *pStep = (EP_DISCHARGE_STEP *)lpStep;
			if(pStep->lEndV > 0L)	
			{
				strTemp.Format("V < %.3f", VTG_PRECISION(pStep->lEndV));
			}
			if(pStep->lEndI < 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strTemp1.Format("I > %.1f", CRT_PRECISION(pStep->lEndI));
				strTemp += strTemp1;
			}
			if(pStep->ulEndTime > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				ConvertTime(szTemp, pStep->ulEndTime);
				strTemp1.Format("t > %s", szTemp);
				strTemp += strTemp1;
			}
			if(pStep->lEndC > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strTemp1.Format("C > %.1f", ETC_PRECISION(pStep->lEndC));
				strTemp += strTemp1;
			}
			if(pStep->lEndDV < 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strTemp1.Format("dV < %.3f", VTG_PRECISION(pStep->lEndDV));
				strTemp += strTemp1;
			}
			if(pStep->lEndDI > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strTemp1.Format("dI > %.1f", CRT_PRECISION(pStep->lEndDI));
				strTemp += strTemp1;
			}

			break;
		}

	case EP_TYPE_REST:				//Rest
		{
			EP_REST_STEP *pStep = (EP_REST_STEP *)lpStep;
			ConvertTime(szTemp, pStep->ulEndTime);
			strTemp.Format("t > %s", szTemp); 
			break;
		}
	case EP_TYPE_OCV:				//Ocv
	case EP_TYPE_IMPEDANCE:		//Impedance
	case EP_TYPE_END:				//End
	case -1:					//Not Seleted
		strTemp.Empty();
		break;
	default:
		strTemp = "Step Type Loading Fail";
		break;
	}
	return strTemp;
}
*/

CString CCTSAnalDoc::GetStateMsg(WORD State, BYTE &colorFlag)
{
	CString strMsg;
	switch (State)
	{
		case EP_STATE_IDLE		:		strMsg = TEXT_LANG[9];		colorFlag = 0;	break;//Vacancy
		case EP_STATE_SELF_TEST	:		strMsg = TEXT_LANG[10];		colorFlag = 3;	break;//Test
		case EP_STATE_STANDBY	:		strMsg = TEXT_LANG[11];		colorFlag = 2;	break;//Standby
		case EP_STATE_RUN		:		strMsg = TEXT_LANG[12];			colorFlag = 4;	break;//Running
		case EP_STATE_PAUSE		:		strMsg = TEXT_LANG[13];		colorFlag = 6;	break;//Pause
		case EP_STATE_FAIL		:		strMsg = TEXT_LANG[14];		colorFlag = 5;	break;//Error
		case EP_STATE_MAINTENANCE:		strMsg = TEXT_LANG[15];		colorFlag = 8;	break;//Maintenance
		case EP_STATE_OCV		:		strMsg = TEXT_LANG[3];			colorFlag = 10;	break;//OCV
		case EP_STATE_CHARGE	:		strMsg = TEXT_LANG[0];		colorFlag = 11;	break;//Charge
		case EP_STATE_DISCHARGE	:		strMsg = TEXT_LANG[1];	colorFlag = 12;	break;//Discharge
		case EP_STATE_REST		:		strMsg = TEXT_LANG[2];		colorFlag = 13;	break;//Rest
		case EP_STATE_IMPEDANCE	:		strMsg = TEXT_LANG[4];	colorFlag = 14;	break;//DCIR
		case EP_STATE_CHECK		:		strMsg = TEXT_LANG[16];		colorFlag = 3;	break;//Check
		case EP_STATE_STOP		:		strMsg = TEXT_LANG[17];		colorFlag = 1;	break;//Stop
		case EP_STATE_END		:		strMsg = TEXT_LANG[5];			colorFlag = 7;	break;//End
		case EP_STATE_FAULT		:		strMsg = TEXT_LANG[18];		colorFlag = 5;	break;//Fault
		case EP_STATE_READY		:		strMsg = TEXT_LANG[19];		colorFlag = 0;	break;//Ready

		case EP_STATE_LINE_OFF	:		strMsg = TEXT_LANG[20];	colorFlag = 9;	break;	//Line Off
		case EP_STATE_LINE_ON	:		strMsg = TEXT_LANG[21];		colorFlag = 0;	break;//Line On
		case EP_STATE_EMERGENCY	:		strMsg = TEXT_LANG[14];	colorFlag = 9;	break;//Error
		case EP_STATE_NONCELL   :		strMsg = TEXT_LANG[22];		colorFlag = 5;	break;//NonCell
		
		case EP_STATE_COMMON	:		
		case EP_STATE_ERROR		:		
		default:						strMsg = TEXT_LANG[14];		colorFlag = 9;	break;//Error
	}
	return strMsg;
}

CString CCTSAnalDoc::GetTypeMsg(WORD State, BYTE &colorFlag)
{
	CString strMsg;
	switch (State)
	{
		case EP_TYPE_NONE		:		strMsg = TEXT_LANG[23];		colorFlag = 0;	break;//None
		case EP_TYPE_CHARGE		:		strMsg = TEXT_LANG[0];		colorFlag = 11;	break;//Charge
		case EP_TYPE_DISCHARGE	:		strMsg = TEXT_LANG[1];	colorFlag = 12;	break;//Discharge
		case EP_TYPE_REST		:		strMsg = TEXT_LANG[2];		colorFlag = 13;	break;//Rest
		case EP_TYPE_OCV		:		strMsg = TEXT_LANG[3];			colorFlag = 10;	break;//OCV
		case EP_TYPE_IMPEDANCE	:		strMsg = TEXT_LANG[4];	colorFlag = 14;	break;//DCIR
		case EP_TYPE_END		:		strMsg = TEXT_LANG[5];			colorFlag = 7;	break;//End
		case EP_TYPE_LOOP		:		strMsg = TEXT_LANG[24];		colorFlag = 0;	break;//Loop
		case EP_TYPE_AGING		:		strMsg = TEXT_LANG[25];		colorFlag = 0;	break;//Aging
		case EP_TYPE_GRADING	:		strMsg = TEXT_LANG[26];		colorFlag = 0;	break;//Grading
		case EP_TYPE_SELECTING	:		strMsg = TEXT_LANG[27];	colorFlag = 0;	break;//Selecting
		default					:		strMsg = TEXT_LANG[14];		colorFlag = 0;	break;//Error
	}
	return strMsg;
}

CString CCTSAnalDoc::ChCodeMsg(WORD code, BOOL bDetail)
{
	STR_MSG_DATA *pCode;
	CString strMsg;
	strMsg.Format("%d", code);
	
	if(m_bLoadCodeDB == FALSE)	return strMsg;

	for(INDEX i=0; i<m_apChCodeMsg.GetSize(); i++)
	{
		pCode = (STR_MSG_DATA *)m_apChCodeMsg[i];
		ASSERT(pCode);
		if(pCode->nCode == (int)code)
		{
#if _DEBUG
			if(bDetail)
			{
				strMsg.Format("%s(%d):%s", pCode->szMessage, code, pCode->szDescript);
			}
			else
			{
				strMsg.Format("%s(%d)", pCode->szMessage, code);
			}
#else
			if(bDetail)
			{
				strMsg.Format("%s:%s", pCode->szMessage, pCode->szDescript);
			}
			else
			{
				strMsg.Format("%s", pCode->szMessage);
			}
#endif
			return strMsg;
		} 
	}
	return strMsg;
}

void CCTSAnalDoc::OnCloseDocument() 
{
	// TODO: Add your specialized code here and/or call the base class
	AfxGetApp()->WriteProfileString(FORM_PATH_REG_SECTION ,TEXT_LANG[28], m_strDataFolder);		//Get Data Folder  //TEXT_LANG[28]="Data"
	
	STR_MSG_DATA *pCode;
	for( int i = 0; i< m_apChCodeMsg.GetSize(); i++)
	{
		pCode = (STR_MSG_DATA *)m_apChCodeMsg[i];
		if(pCode)
		{
			delete pCode;
			pCode = NULL;
		}
	}
	m_apChCodeMsg.RemoveAll();
//------------------------------------------------------------------------------------//
/*	if(!m_ReportDataList.IsOpen())
		m_ReportDataList.Open();

	for(i = 0 ; i < m_TotCol-1; i++)
	{
		m_ReportDataList.Edit();
		m_ReportDataList.m_No         = m_field[i].No;
		m_ReportDataList.m_ProgressID = m_field[i].ID;
		m_ReportDataList.m_Name       = m_field[i].FieldName;
		m_ReportDataList.m_DataType   = m_field[i].DataType;
		m_ReportDataList.m_Display    = m_field[i].bDisplay;
    	m_ReportDataList.Update();
		m_ReportDataList.MoveNext();
	}
	m_ReportDataList.Close();
*/	
	SaveGraphSetting();
	RemoveFileData(m_nLoadedFileNum);
//------------------------------------------------------------------------------------//
	CDocument::OnCloseDocument();
}

BOOL CCTSAnalDoc::LoadCodeTable()
{
	CDaoDatabase  db;
	
	if(m_strDataBaseName.IsEmpty())		return FALSE;	

	CString strSQL;

	if( g_nLanguage == LANGUAGE_ENG )
	{
		strSQL.Format("SELECT Code, Messge, Description FROM ChannelCode_ENG");
	}
	else if( g_nLanguage == LANGUAGE_CHI )
	{
		strSQL.Format("SELECT Code, Messge, Description FROM ChannelCode_CHI");
	}
	else
	{
		strSQL.Format("SELECT Code, Messge, Description FROM ChannelCode");
	}

	try
	{
		db.Open(m_strDataBaseName);

		CDaoRecordset rs(&db);

		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
		
		COleVariant data;
		STR_MSG_DATA *pCode;

		if(!rs.IsBOF() && !rs.IsEOF())
		{	
			while(!rs.IsEOF())
			{ 
				pCode = new STR_MSG_DATA;
				ASSERT(pCode);
				ZeroMemory(pCode, sizeof(STR_MSG_DATA));
				
				data = rs.GetFieldValue(0);
				pCode->nCode = data.lVal;
				data = rs.GetFieldValue(1);
				sprintf(pCode->szMessage, "%s", data.pbVal);
				data = rs.GetFieldValue(2);
				pCode->nData = data.lVal;
				sprintf(pCode->szDescript, "%s", data.pbVal);
				
				m_apChCodeMsg.Add(pCode);
				
				rs.MoveNext();
			}
		}

		rs.Close();
		db.Close();

		return TRUE;
	}
	catch (CException* e)
	{
		TCHAR sz[255];
		e->GetErrorMessage(sz,255);
		AfxMessageBox(sz);
		e->Delete();		
		return FALSE;
	}

	return FALSE;
}

void CCTSAnalDoc::SetProgressWnd(UINT nMin, UINT nMax, CString m_strTitle)
{
	if(!m_bProgressInit)
		InitProgressWnd();
	m_pProgressWnd->Clear();
	m_pProgressWnd->SetRange(nMin, nMax);
	m_pProgressWnd->SetText(m_strTitle);
	m_pProgressWnd->PeekAndPump(FALSE);
	m_pProgressWnd->SetPos(nMin);
	m_pProgressWnd->Show();
}

void CCTSAnalDoc::SetProgressPos(int nPos)
{
	m_pProgressWnd->SetPos(nPos);
	m_pProgressWnd->PeekAndPump(FALSE);
}

void CCTSAnalDoc::HideProgressWnd()
{
	m_pProgressWnd->Hide();
}

void CCTSAnalDoc::InitProgressWnd()
{
	if(m_bProgressInit == FALSE)
	{
		m_pProgressWnd = new CProgressWnd;
		m_pProgressWnd->Create(AfxGetApp()->m_pMainWnd, "Progress", TRUE);
		m_pProgressWnd->Hide();
//		m_pProgressWnd->SetColor(RGB(0,0,192));
		m_bProgressInit = TRUE;
	}
}

BOOL CCTSAnalDoc::LoadFieldList()
{	
	if(m_strLogDataBaseName.IsEmpty())		return FALSE;

	CDaoDatabase  db;
	
	CString strSQL;
	strSQL.Format("SELECT No,ProgressID,Name,DataType,Display FROM ReportDataList WHERE ProgressID >= %d ORDER BY No", EP_REPORT_GRADING);
	
	COleVariant data;
	
	try
	{
		db.Open(m_strLogDataBaseName);	
		CDaoRecordset rs(&db);

		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
		
		if(!rs.IsBOF() && !rs.IsEOF())
		{
			m_TotCol = 0;
			
			while(!rs.IsEOF())
			{				
				data = rs.GetFieldValue(0);
				m_field[m_TotCol].No = data.lVal;
				data = rs.GetFieldValue(1);
				m_field[m_TotCol].ID        = data.lVal;
				data = rs.GetFieldValue(2);
				m_field[m_TotCol].FieldName.Format("%s",data.pbVal);
				data = rs.GetFieldValue(3);
				m_field[m_TotCol].DataType  = data.lVal;
				data = rs.GetFieldValue(4);
				m_field[m_TotCol].bDisplay  = data.bVal;
				
				rs.MoveNext();
				m_TotCol++;
			}		
		}

		rs.Close();
		db.Close();
	}
	catch (CException* e)
	{
		TCHAR sz[255];
		e->GetErrorMessage(sz,255);
		AfxMessageBox(sz);
		e->Delete();		
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////
//ADD

int CCTSAnalDoc::OnNewFileOpen(int nIndex)
{
	CFile srcFile;
	if ( !srcFile.Open(m_pFileInfo[nIndex].strFileName, CFile::modeRead) )
	{
		AfxMessageBox(TEXT_LANG[29]);//"파일을 찾을 수 없습니다"
		srcFile.Close();
		return -1;
	}
	int nFileSize = (int)srcFile.GetLength();

	char* lpLineBuffer = new char[nFileSize];
	
	if ( srcFile.Read(lpLineBuffer, nFileSize) != (UINT)nFileSize )
	{
		AfxMessageBox(TEXT_LANG[30]);//"파일을 읽는데 실패했습니다"
		srcFile.Close();
		return -1;
	}
	int nCnt = 0;	// 첫번째 라인까지의 포인터 인덱스

	// 첫번째 라인을 읽어들여 아이템의 이름을 구분한다. (아이템의 개수 파악)
	//nCnt = ParsingItem(lpLineBuffer);
	
	// 아이템의 개수는 무조건 4개이다..
	m_pParsingData[nIndex].m_nFieldNum = 4;

	TRACE("%d\n", m_pParsingData[nIndex].m_nFieldNum);
	for ( int j = 0; j < 4; j++ )
	{
		m_pParsingData[nIndex].m_nFieldID[j] = j+1;
	}

	// 두번째 라인부터 유효한 데이터의 개수를 파악한다.
	int nDataNum = 0;	// 총 데이터의 개수
	for( int i = 0; i < nFileSize; i++ )
	{
		if(lpLineBuffer[i] == '\n' && lpLineBuffer[i-1] != '\n')
			nDataNum++;
	}
	
	// 각 데이터 필드에 메모리를 할당
	m_pParsingData[nIndex].InitField(nDataNum);

	// 두번째 라인부터 각각의 아이템에 데이터를 삽입한다.
	//char *lpBuffer = lpLineBuffer + nCnt + 1;
	//int nDataSize = nFileSize - nCnt - 2;
	
	char *lpBuffer = lpLineBuffer;
	int nDataSize = nFileSize ;
	int nOffset = 0;

	while(true)
	{
		nCnt = ParsingData(lpBuffer + nOffset, nIndex);
		nOffset += nCnt + 1;
		if ( nDataSize <= nOffset)
			break;
	}	

	// 할당한 메모리들을 해제한다.
	{
		delete [] lpLineBuffer;
		srcFile.Close();
	}
	return 0;
}

int CCTSAnalDoc::ParsingItem(char *pDest)
{
/*
	char lpTmpBuffer [1024];
	int i = 0, nCnt = 0;

	while(true)
	{
		if ( pDest[i] == '\n' )
		{
			lpTmpBuffer[i] = '\0';
			nCnt = i;
			break;
		}
		lpTmpBuffer[i] = pDest[i++];
	}
	char lpTmpName[1024];
	int index = 0;
	int nItemNum = 0;
	for ( i = 0; i < nCnt; i++ )
	{
		if ( lpTmpBuffer[i] == ',' )
		{
			if ( lpTmpBuffer[i-1] == ' ' )
				index--;
			if ( lpTmpBuffer[i+1] == ' ' )
				i++;
			lpTmpName[index] = '\0';
			index = 0;
			char *pTmp = new char [index+1] ;
			sprintf(pTmp, lpTmpName);
			m_pParsingData->m_aFieldName.Add(pTmp);
			//m_pParsingData->m_nFieldID[nItemNum++] = GetFieldAttribute(lpTmpName);
			m_pParsingData->m_nFieldID[nItemNum++] = nItemNum+1;
			memset(lpTmpName, 0, 1024);
		}
		else
			lpTmpName[index++] = lpTmpBuffer[i];
	}
	// 열의 마지막에 ','를 만나지 못했다면 강제로 저장시킨다.
	if ( index )
	{
		if ( lpTmpBuffer[i-1] == ' ' )
				index--;
		lpTmpName[index] = '\0';
		index = 0;
		char *pTmp = new char [index+1] ;
		m_pParsingData->m_aFieldName.Add(pTmp);
		m_pParsingData->m_nFieldID[nItemNum++] = GetFieldAttribute(lpTmpName);
		memset(lpTmpName, 0, 1024);
	}
	m_pParsingData->m_nFieldNum = m_pParsingData->m_aFieldName.GetSize();
	return nCnt;
*/
	return 0;
}

int CCTSAnalDoc::ParsingData(char *pDest, int nIndex)
{
	char lpTmpBuffer [1024];
	int i = 0, nCnt = 0;
	int nItemIndex = 0;

	while(true)
	{
		if ( pDest[i] == '\n' )
		{
			lpTmpBuffer[i] = '\0';
			nCnt = i;
			break;
		}
		lpTmpBuffer[i] = pDest[i++];
	}
	
	if (!nCnt)
		return 0;
	
	int nDataIndex = m_pParsingData[nIndex].m_nDataNum++;

	char lpTmpName[1024];
	int index = 0;
	for ( i = 0; i < nCnt; i++ )
	{
		if ( lpTmpBuffer[i] == ',' )
		{
			if ( lpTmpBuffer[i-1] == ' ' )
				index--;
			if ( lpTmpBuffer[i+1] == ' ' )
				i++;
			lpTmpName[index] = '\0';
			index = 0;
			SetFieldData(nIndex, lpTmpName, nItemIndex++, nDataIndex);
			memset(lpTmpName, 0, 1024);
		}
		else
			lpTmpName[index++] = lpTmpBuffer[i];
	}
	// 열의 마지막에 ','를 만나지 못했다면 강제로 저장시킨다.
	if ( index )
	{
		if ( lpTmpBuffer[i-1] == ' ' )
				index--;
		lpTmpName[index] = '\0';
		index = 0;
		SetFieldData(nIndex, lpTmpName, nItemIndex, nDataIndex);
		memset(lpTmpName, 0, 1024);
	}

	return nCnt;
}

inline INT CCTSAnalDoc::GetFieldAttribute(char *lpStr)
{
	if(!strcmp(lpStr, STR_STEP_TIME))			return STEP_TIME ;
	else if(!strcmp(lpStr, STR_CH_NO))			return CHANNEL_NO ;
	else if(!strcmp(lpStr, STR_STEP_NO))		return STEP_NO ;
	else if(!strcmp(lpStr, STR_STEP_TYPE))		return STEP_TYPE ;
	else if(!strcmp(lpStr, STR_VOLTAGE))		return VOLTAGE ;
	else if(!strcmp(lpStr, STR_CURRENT))		return CURRENT ;
	else if(!strcmp(lpStr, STR_CAPACITANCE))	return CAPACITANCE ;
	else	return PARSING_ERROR ;
}

inline int CCTSAnalDoc::GetStepTypeID(char *lpStr)
{
	if(!strcmp(lpStr, TEXT_LANG[0]))			return CHARGE ;//"Charge"
	else if(!strcmp(lpStr, TEXT_LANG[1]))	return DISCHARGE ;//"Discharge"
	else if(!strcmp(lpStr, TEXT_LANG[2]))			return REST ;//"Rest"
	else if(!strcmp(lpStr, TEXT_LANG[5]))			return ENDSTEP ;//"End"
	else									return PARSING_ERROR ;
}

void CCTSAnalDoc::SetFieldData(int nIndex, char *lpStr, int nItemIndex, int nItemLine)
{
	switch ( m_pParsingData[nIndex].m_nFieldID[nItemIndex] )
	{
		case STEP_TIME :
			m_pParsingData[nIndex].m_nTime[nItemLine] = atoi(lpStr);
			break;
		case VOLTAGE :
			m_pParsingData[nIndex].m_fVoltage[nItemLine] = (float)atof(lpStr) / 10000.0f;
			break;
		case CURRENT :
			m_pParsingData[nIndex].m_fCurrent[nItemLine] = (float)atof(lpStr) / 100.0f;
			break;
		case CAPACITANCE :
			m_pParsingData[nIndex].m_fCapacity[nItemLine] = (float)atof(lpStr) / 100.0f;
			break;
		default:
			break;
	}
}

CString CCTSAnalDoc::GetFieldData(int nItemNum, int nFieldNum)
{
	CString strData;
/*	switch ( m_pParsingData->m_nFieldID[nFieldNum] )
	{
		case STEP_NO :
			strData.Format("%d", m_pParsingData->m_fldData.m_nStepNum[nItemNum]);
			break;
		case STEP_TYPE :
			strData = GetStepType(m_pParsingData->m_fldData.m_nStepType[nItemNum]);
			break;
		case STEP_TIME :
			strData.Format("%d", m_pParsingData->m_fldData.m_nTime[nItemNum]);
			break;
		case CHANNEL_NO :
			strData.Format("%d", m_pParsingData->m_fldData.m_nChNum[nItemNum]);
			break;
		case VOLTAGE :
			strData.Format("%2.3f", m_pParsingData->m_fldData.m_fVoltage[nItemNum]);
			break;
		case CURRENT :
			strData.Format("%2.3f", m_pParsingData->m_fldData.m_fCurrent[nItemNum]);
			break;
		case CAPACITANCE :
			strData.Format("%2.3f", m_pParsingData->m_fldData.m_fCapacity[nItemNum]);
			break;
	}
*/	return strData;
}

inline CString CCTSAnalDoc::GetStepType(int nID)
{
	switch ( nID )
	{
		case CHARGE :		return TEXT_LANG[0] ;//"Charge"
		case DISCHARGE :	return TEXT_LANG[1] ;//"Discharge" 
		case REST :			return TEXT_LANG[2] ;//"Rest"
		case ENDSTEP :		return TEXT_LANG[5] ;//"End"
		default :			return "";
	}
}

//주어진 파일에서 주어진 시간 간격으로 nDataNo개 Data를 Load 한다.
int CCTSAnalDoc::GetGeneralResultData(
										 int nIndex,		// File Index 
										 float fInterVal,	// Load Time InterVal
										 int nDataNo)		// Max Read Point
{
	ASSERT(m_pData);

//	size_t	nReadSize = sizeof(STR_GEN_DATA);
	int nCount =0, nDataIndex =0;
	int nPrevIndex = -1, nPointIndex, nTemp;

#ifdef _DEBUG
	int nReadData = 0;
#endif

	if(m_pFileInfo[nIndex].pnDataIndexArray == NULL)
	{
		m_pFileInfo[nIndex].pnDataIndexArray = new int[nDataNo];
		ASSERT(m_pFileInfo[nIndex].pnDataIndexArray);
		memset(m_pFileInfo[nIndex].pnDataIndexArray, -1, sizeof(int)*nDataNo);
	}

	for( nCount = 0; nCount <= (int)m_pParsingData[nIndex].m_nDataNum; nCount++ ) 
		//&& nCount< m_pFileInfo[nIndex].nDataSize)
	{
		if ( nCount < m_pFileInfo[nIndex].nDataSize )
		{
			//Make Time Index;
			//nPointIndex = (int)((float)m_pParsingData[nIndex].m_nTime[nCount]/fInterVal);	
			nPointIndex  = nCount;

			//JSH Add Here
			//nPointIndex--;

			if(nPointIndex < 0 || nPointIndex >= nDataNo)
			{
				TRACE("%d :: Wrong Input Data index %d\n", nCount, nPointIndex);
				continue;
			}

			if(nPrevIndex != nPointIndex)		//최초 Data
			{
				nPrevIndex = nPointIndex;

				if(m_pFileInfo[nIndex].pnDataIndexArray)
					m_pFileInfo[nIndex].pnDataIndexArray[nPointIndex] = nCount;

				nDataIndex = 0;
		/*
		 *	nIndex : 현재 파일의 번호 (0, 1, 2....)
		 *	nDataNo : Max Read Point
		 */
//				int nAddOn = GetAxisCount() * nIndex * nDataNo;
				int nFinalIndex = 0;
				if(m_YSet[0].bItemSel)	
				{
					nFinalIndex = (nIndex + (nDataIndex++)*m_nLoadedFileNum)*nDataNo + nPointIndex;

					m_pData[nFinalIndex] = (float)m_pParsingData[nIndex].m_fVoltage[nCount];

#ifdef	_DEBUG	//////////////////////////////////////////////////////////////////////////
					fprintf(f, "Item %06d / Index : %06d\t\t", nCount, nFinalIndex);	//
#endif//_DEBUG	//////////////////////////////////////////////////////////////////////////

				}
				if(m_YSet[1].bItemSel)	
				{
					nFinalIndex = (nIndex + (nDataIndex++)*m_nLoadedFileNum)*nDataNo + nPointIndex;
					m_pData[nFinalIndex] = (float)m_pParsingData[nIndex].m_fCurrent[nCount];

#ifdef	_DEBUG	//////////////////////////////////////////////////////////////////////////
					fprintf(f, "%06d\t\t", nFinalIndex);	//
#endif//_DEBUG	//////////////////////////////////////////////////////////////////////////

				}
				if(m_YSet[2].bItemSel)	
				{
					nFinalIndex = (nIndex + (nDataIndex++)*m_nLoadedFileNum)*nDataNo + nPointIndex;
					m_pData[nFinalIndex] = (float)m_pParsingData[nIndex].m_fCapacity[nCount];

#ifdef	_DEBUG	//////////////////////////////////////////////////////////////////////////
					fprintf(f, "%06d\n", nFinalIndex);	//
#endif//_DEBUG	//////////////////////////////////////////////////////////////////////////

				}

	#ifdef _DEBUG
				nReadData++;
	#endif
			}
			else								//중복 Data->평균 값을 구한다.
			{
				nDataIndex = 0 ;
				if(m_YSet[0].bItemSel)	
				{
					nTemp = (nIndex+nDataIndex*m_nLoadedFileNum)*nDataNo+nPointIndex;
					m_pData[nTemp] = (m_pData[nTemp]+(float)m_pParsingData[nIndex].m_fVoltage[nCount])/2.0f;
					nDataIndex++;
				}
				if(m_YSet[1].bItemSel)	
				{
					nTemp = (nIndex+nDataIndex*m_nLoadedFileNum)*nDataNo+nPointIndex;
					m_pData[nTemp] = (m_pData[nTemp]+(float)m_pParsingData[nIndex].m_fCurrent[nCount])/2.0f;		//Current	
					nDataIndex++;
				}
				if(m_YSet[2].bItemSel)	
				{
					nTemp = (nIndex+nDataIndex*m_nLoadedFileNum)*nDataNo+nPointIndex;
					m_pData[nTemp] = (m_pData[nTemp]+(float)m_pParsingData[nIndex].m_fCapacity[nCount])/2.0f;
					nDataIndex++;
				}
			}
		}
	}
	
///////////////////////////////////////////////////////////////////
// Debugging : Transfer to Files
#ifdef	_DEBUG
	static int nFNum = 0;
	CString strFileName;
	strFileName.Format("Result-%02d.txt", nFNum++);
	int nDataNum = m_nXMaxReadPoint*GetAxisCount()*m_nLoadedFileNum;
	FILE* fp = fopen(strFileName, "w");
	if(!fp)
		return 0;
	for(int i = 0; i < nDataNum; i++)
		fprintf(fp, "%05d\t%03.3f\n", i, m_pData[i]);
	fclose(fp);
#endif	//_DEBUG
///////////////////////////////////////////////////////////////////

	return 0;
}

int	CCTSAnalDoc::GetCapacityResultData(int nIndex,int nDataNo)
{
	ASSERT( nIndex>=0 && nIndex < m_nLoadedFileNum && nDataNo>0);
	ASSERT(m_pData);
	if(m_pFileInfo[nIndex].fp == NULL)		return -1;				//file Point Check

	int nPointIndex =0, nDataIndex =0;
	STR_CAP_DATA stReadBuff;
	

	fseek(m_pFileInfo[nIndex].fp, sizeof(STR_RESULT_DATA), SEEK_SET);		//Find First Data Point
	
/*	if(m_pFileInfo[nIndex].pDataHeader == NULL)								//처음으로 읽으면 Data Point Information을 읽는다.
	{
		m_pFileInfo[nIndex].pDataHeader = new STR_READ_DATA[nDataNo];
		ASSERT(m_pFileInfo[nIndex].pDataHeader);
		memset(m_pFileInfo[nIndex].pDataHeader, 0, sizeof(STR_READ_DATA)*nDataNo);
	}
*/
	while((fread(&stReadBuff, sizeof(STR_CAP_DATA), 1, m_pFileInfo[nIndex].fp) == 1) && nPointIndex< m_pFileInfo[nIndex].nDataSize)
	{
/*		ASSERT(m_pFileInfo[nIndex].pDataHeader);

		m_pFileInfo[nIndex].pDataHeader[nPointIndex].index = nPointIndex;
		m_pFileInfo[nIndex].pDataHeader[nPointIndex].state = (BYTE)stReadBuff.state;
		m_pFileInfo[nIndex].pDataHeader[nPointIndex].type = (BYTE)stReadBuff.type;
		m_pFileInfo[nIndex].pDataHeader[nPointIndex].mode = (BYTE)stReadBuff.mode;
		m_pFileInfo[nIndex].pDataHeader[nPointIndex].stepNo = (BYTE)stReadBuff.stepNo;
		m_pFileInfo[nIndex].pDataHeader[nPointIndex].totalTime = stReadBuff.totalTime;
		m_pFileInfo[nIndex].pDataHeader[nPointIndex].stepTime = stReadBuff.stepTime;
*/	
		nDataIndex =0;
		if(m_YSet[0].bItemSel)	
			m_pData[(nIndex+(nDataIndex++)*m_nLoadedFileNum)*nDataNo+nPointIndex] = (float)stReadBuff.Voltage;		//Voltage
		if(m_YSet[1].bItemSel)	
			m_pData[(nIndex+(nDataIndex++)*m_nLoadedFileNum)*nDataNo+nPointIndex] = 0.0f;		//Current	
		if(m_YSet[2].bItemSel)	
			m_pData[(nIndex+(nDataIndex++)*m_nLoadedFileNum)*nDataNo+nPointIndex] = (float)stReadBuff.Capacity;
		if(m_YSet[3].bItemSel)	
			m_pData[(nIndex+(nDataIndex++)*m_nLoadedFileNum)*nDataNo+nPointIndex] = 0.0f;
		if(m_YSet[4].bItemSel)	
			m_pData[(nIndex+(nDataIndex++)*m_nLoadedFileNum)*nDataNo+nPointIndex] = 0.0f;
		if(m_YSet[5].bItemSel)	
			m_pData[(nIndex+nDataIndex*m_nLoadedFileNum)*nDataNo+nPointIndex] = 0.0f;

		nPointIndex++;

	}
	return 0;
}

//Memory 해제
void CCTSAnalDoc::RemoveFileData(int nFileCount)
{
	if(m_pFileInfo == NULL)		return;
	if(nFileCount <= 0)   nFileCount = m_nLoadedFileNum;

	for(char index =0; index< nFileCount; index++)
	{
/*		if(m_pFileInfo[index].pDataHeader)
		{
			delete [] m_pFileInfo[index].pDataHeader;
			m_pFileInfo[index].pDataHeader = NULL;
		}
*/		if(m_pFileInfo[index].pnDataIndexArray)
		{
			delete m_pFileInfo[index].pnDataIndexArray;
			m_pFileInfo[index].pnDataIndexArray = NULL;
		}
		if(m_pFileInfo[index].fp)
		{
			fclose(m_pFileInfo[index].fp);
			m_pFileInfo[index].fp = NULL;
		}
	}
	
	delete [] m_pFileInfo;
	m_pFileInfo = NULL;

	if(m_pData)
	{
		delete[] m_pData;
		m_pData = NULL;				
	}
	if(m_pParsingData)
	{
		delete [] m_pParsingData;
		m_pParsingData = NULL;
	}
	m_nXMaxReadPoint = 0;
}


//Load Graph Confilg File 
BOOL CCTSAnalDoc::LoadGraphSetting()
{
	char szTemp[512];
	if(GetCurrentDirectory(511, szTemp) <= 0)	return FALSE;
	CString strInstallPath = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"CTSAnalyzer", szTemp);		//Get Data Folder
	
	m_strConfigFileName = strInstallPath;
	m_strConfigFileName+= "\\BtsGraph.cfg";

	FILE *fp = fopen(m_strConfigFileName, "rb");
	if(fp == NULL)		return FALSE;

	fread(&m_nXItem, sizeof(int), 1, fp);
	fread(&m_nMaxDataPoint, sizeof(int), 1, fp);

	fread(m_YSet, sizeof(STR_Y_AXIS_SET)*MAX_MUTI_AXIS, 1, fp);
	fread(&m_GraphLineSet, sizeof(STR_GRID_SET), 1, fp);
	fread(m_ChColor, sizeof(COLORREF) * MAX_CHANNEL_NUM, 1, fp);
	fclose(fp);
	return TRUE;
}

//Save Graph Config file
BOOL CCTSAnalDoc::SaveGraphSetting()
{
	if(m_strConfigFileName.IsEmpty())	return FALSE;
	
	FILE *fp = fopen(m_strConfigFileName, "wb");
	if(fp == NULL)		return FALSE;
	
	fwrite(&m_nXItem, sizeof(int), 1, fp);
	fwrite(&m_nMaxDataPoint, sizeof(int), 1, fp);

	fwrite(m_YSet, sizeof(STR_Y_AXIS_SET)*MAX_MUTI_AXIS, 1, fp);
	fwrite(&m_GraphLineSet, sizeof(STR_GRID_SET), 1, fp);
	fwrite(m_ChColor, sizeof(COLORREF) * MAX_CHANNEL_NUM, 1, fp);
	fclose(fp);
	return TRUE;
}

/*
 *		CSV File Loading..
 */
int CCTSAnalDoc::LoadFileData(BOOL bLoadFile, CString* strFileName, int nLoadNum)
{
	static int nMaxDataSize = 0;
	
	if(bLoadFile)													//File을 처음 읽는다.
	{
		if(m_pFileInfo)												//Remove Previous Data structure
		{
			RemoveFileData(m_nLoadedFileNum);
		}

		m_nLoadedFileNum = nLoadNum;
		m_pFileInfo = new STR_FILE_INFO_A[m_nLoadedFileNum];		//Make File Information Structure
		ASSERT(m_pFileInfo);
		
		//if ( m_pParsingData )
		//	delete [] m_pParsingData;
		m_pParsingData = new CDataFormat [m_nLoadedFileNum];
		ASSERT(m_pParsingData);

		for ( int i = 0; i < m_nLoadedFileNum; i++ )
		{
			m_pFileInfo[i].fp = NULL;								
//			m_pFileInfo[i].pDataHeader = NULL;
			m_pFileInfo[i].pnDataIndexArray = NULL;
			m_pFileInfo[i].strFileName = strFileName[i];

			if(GetFileInfoDataA(i) < 0)	return -1;		//Read File Infomation And Data		

			//m_nXMaxReadPoint = m_pFileInfo[i].nSubSetSize;
			//m_fSecPerPoint = m_pFileInfo[i].fSecPerData;
			//nMaxDataSize = m_pFileInfo[i].nDataSize;
			//m_nMaxDataFileIndex = 0;
			if(m_nXMaxReadPoint < (UINT)m_pFileInfo[i].nSubSetSize)			//Get Max. Subset Size File
			{
				m_nXMaxReadPoint = m_pFileInfo[i].nSubSetSize;
				m_nMaxDataFileIndex = i;
			}
			if(m_fSecPerPoint < m_pFileInfo[i].fSecPerData)					//Get Max. Data Interval File.
				m_fSecPerPoint = m_pFileInfo[i].fSecPerData;
			if(nMaxDataSize < m_pFileInfo[i].nDataSize)						//Get Max. Data Length File
				nMaxDataSize = m_pFileInfo[i].nDataSize;
		}
	}
	else		//Data만 다시 읽는다.
	{
		if(m_pFileInfo == NULL)	return 0;
		if(m_pData)		delete[] m_pData;
	}
	
	if(m_nXMaxReadPoint <= 0)	return -1;
	
	//JSH Modify Here..
	//int nSize = (m_nXMaxReadPoint)*GetAxisCount();		//Get Memory Size
	int nReadPoint = m_nXMaxReadPoint ;
	int nSize = nReadPoint*m_nLoadedFileNum*GetAxisCount();		//Get Memory Size
	ASSERT(nSize>0);
	m_pData = new float[nSize];								//Memory allocation
	ASSERT(m_pData);
	memset(m_pData, 0, sizeof(float)*nSize);				//Reset Memory

	TRACE("Memory Allocated %d Byte\n", nSize);

#ifdef _DEBUG
	f = fopen("ResultTotal.txt", "w");
	fprintf(f, "Data's Read Point : %06d\n", nReadPoint);
	fprintf(f, "Data's Total Number : %06d\n\n", nSize);
#endif _DEBUG

	for(int i =0; i<m_nLoadedFileNum; i++)				//Read Data From File
	{
		GetGeneralResultData(i, m_fSecPerPoint, nReadPoint);
	}

#ifdef _DEBUG
	fclose(f);
#endif _DEBUG

	return 0;
}

int CCTSAnalDoc::LoadFileDataA(BOOL bLoadFile, CString strFileName)
{
	bLoadFile = false;
/*	static int nMaxDataSize = 0;
	
	if ( m_pProgressWnd )
	{
		m_pProgressWnd->SetText("파일을 읽고 있습니다.");
		m_pProgressWnd->SetPos(30);
		m_pProgressWnd->PeekAndPump(FALSE);	
	}

	if(bLoadFile)													//File을 처음 읽는다.
	{
		if(m_pFileInfo)												//Remove Previous Data structure
		{
			RemoveFileData(m_nLoadedFileNum);
		}

		m_pFileInfo = new STR_FILE_INFO_A;			//Make File Information Structure
		ASSERT(m_pFileInfo);

		m_pFileInfo->fp = NULL;								
//			m_pFileInfo[i].pDataHeader = NULL;
		m_pFileInfo->pnDataIndexArray = NULL;
		m_pFileInfo->strFileName = strFileName;

		if(GetFileInfoDataA() < 0)	return -1;		//Read File Infomation And Data		
		m_nXMaxReadPoint = m_pFileInfo->nSubSetSize;
		m_fSecPerPoint = m_pFileInfo->fSecPerData;
		nMaxDataSize = m_pFileInfo->nDataSize;
		m_nMaxDataFileIndex = 0;
	}
	else		//Data만 다시 읽는다.
	{
		if(m_pFileInfo == NULL)	return 0;
		if(m_pData)		delete[] m_pData;
	}
	
	if(m_nXMaxReadPoint <= 0)	return -1;
	
	//JSH Modify Here..
	//int nSize = (m_nXMaxReadPoint)*GetAxisCount();			//Get Memory Size
	int nSize = (m_nXMaxReadPoint+1)*GetAxisCount();			//Get Memory Size
	ASSERT(nSize>0);
	m_pData = new float[nSize];								//Memory allocation
	ASSERT(m_pData);
	memset(m_pData, 0, sizeof(float)*nSize);				//Reset Memory

	TRACE("Memory Allocated %d Byte\n", nSize);

//for(int i =0; i<m_nLoadedFileNum; i++)					//Read Data From File
//	{
//		GetGeneralResultData(i, m_fSecPerPoint, m_nXMaxReadPoint);
//	}
	if ( m_pProgressWnd )
	{
		m_pProgressWnd->SetText("파일에서 데이터를 축출하고 있습니다..");
		m_pProgressWnd->SetPos(200);
		m_pProgressWnd->PeekAndPump(FALSE);	
	}

	GetGeneralResultDataA(m_fSecPerPoint, m_nXMaxReadPoint+1);
	
	if ( m_pProgressWnd )
	{
		m_pProgressWnd->SetText("변환한 데이터를 화면에 출력중입니다..");
		m_pProgressWnd->SetPos(470);
		m_pProgressWnd->PeekAndPump(FALSE);	
	}*/
	return 0;
}


int CCTSAnalDoc::GetGeneralResultDataA(float /*fInterVal*/, int nDataNo)
{
	ASSERT(m_pData);

//	size_t	nReadSize = sizeof(STR_GEN_DATA);
	int nCount =0, nDataIndex =0;
	int nPrevIndex = -1, nPointIndex, nTemp;

#ifdef _DEBUG
	int nReadData = 0;
#endif

	if(m_pFileInfo->pnDataIndexArray == NULL)
	{
		m_pFileInfo->pnDataIndexArray = new int[nDataNo];
		ASSERT(m_pFileInfo->pnDataIndexArray);
		memset(m_pFileInfo->pnDataIndexArray, -1, sizeof(int)*nDataNo);
	}
		
	for( nCount = 0; nCount <= (int)m_pParsingData->m_nDataNum; nCount++ ) 
		//&& nCount< m_pFileInfo[nIndex].nDataSize)
	{
		

		//Make Time Index;
		//nPointIndex = (int)((float)m_pParsingData->m_fldData.m_nTime[nCount]/fInterVal);	
		nPointIndex  = nCount;

		//JSH Add Here
		//nPointIndex--;

		if(nPointIndex < 0 || nPointIndex >= nDataNo)
		{
			TRACE("%d :: Wrong Input Data index %d\n", nCount, nPointIndex);
			continue;
		}

		if(nPrevIndex != nPointIndex)		//최초 Data
		{
			nPrevIndex = nPointIndex;

			if(m_pFileInfo->pnDataIndexArray)
				m_pFileInfo->pnDataIndexArray[nPointIndex] = nCount;

			nDataIndex = 0;

			if(m_YSet[0].bItemSel)	
				m_pData[((nDataIndex++)*m_nLoadedFileNum)*nDataNo+nPointIndex] = (float)m_pParsingData->m_fVoltage[nCount];
			if(m_YSet[1].bItemSel)	
				m_pData[((nDataIndex++)*m_nLoadedFileNum)*nDataNo+nPointIndex] = (float)m_pParsingData->m_fCurrent[nCount];
			if(m_YSet[2].bItemSel)	
				m_pData[((nDataIndex++)*m_nLoadedFileNum)*nDataNo+nPointIndex] = (float)m_pParsingData->m_fCapacity[nCount];

#ifdef _DEBUG
			nReadData++;
#endif
		}
		else								//중복 Data->평균 값을 구한다.
		{
			nDataIndex = 0 ;
			if(m_YSet[0].bItemSel)	
			{
				nTemp = (nDataIndex*m_nLoadedFileNum)*nDataNo+nPointIndex;
				m_pData[nTemp] = (m_pData[nTemp]+(float)m_pParsingData->m_fVoltage[nCount])/2.0f;
				nDataIndex++;
			}
			if(m_YSet[1].bItemSel)	
			{
				nTemp = (nDataIndex*m_nLoadedFileNum)*nDataNo+nPointIndex;
				m_pData[nTemp] = (m_pData[nTemp]+(float)m_pParsingData->m_fCurrent[nCount])/2.0f;		//Current	
				nDataIndex++;
			}
			if(m_YSet[2].bItemSel)	
			{
				nTemp = (nDataIndex*m_nLoadedFileNum)*nDataNo+nPointIndex;
				m_pData[nTemp] = (m_pData[nTemp]+(float)m_pParsingData->m_fCapacity[nCount])/2.0f;
				nDataIndex++;
			}
		}
	}
	

// DEBUG
#ifdef _DEBUG
	int nDataNum = (m_nXMaxReadPoint+2)*GetAxisCount();
	FILE* fp = fopen("result.txt", "w");
	for(int i = 0; i < nDataNum; i++)
		fprintf(fp, "%05d\t%03.3f\n", i, m_pData[i]);
	fclose(fp);
#endif
// DEBUG

	return 0;
}

int CCTSAnalDoc::GetFileInfoDataA(int nIndex)
{
	if(m_pFileInfo[nIndex].strFileName.IsEmpty())	return -1;
	if(m_pFileInfo[nIndex].fp != NULL)											//Previous File close
	{
		fclose(m_pFileInfo[nIndex].fp);
		m_pFileInfo[nIndex].fp = NULL;
	}
	//m_pFileInfo->fp = fopen(m_pFileInfo->strFileName, "rb");		//File Open
	//if(m_pFileInfo->fp == NULL)	return -1;								//Open Fail

	if ( OnNewFileOpen(nIndex) < 0 )
		return -1;
	
//	int nTemp = 0;
//	m_nLoadedFileNum = 1;

	m_pFileInfo[nIndex].nDataSize = m_pParsingData[nIndex].m_nDataNum;
	int nTotalTime = m_pParsingData[nIndex].m_nTime[m_pFileInfo[nIndex].nDataSize-1];
	if( nTotalTime == 0 )
	{
		nTotalTime = m_pParsingData[nIndex].m_nTime[m_pFileInfo[nIndex].nDataSize-2];
	}
	m_pFileInfo[nIndex].nTotalTime = nTotalTime;
	
//	int nAxisCnt = GetAxisCount();
	m_pFileInfo[nIndex].nSubSetSize = m_nMaxDataPoint/GetAxisCount();	//Get Data Point Per Subset
	//m_pFileInfo->fSecPerData = (float)nTotalTime/(float)m_pFileInfo->nSubSetSize;	//Get Second Per Data Point	
	m_pFileInfo[nIndex].fSecPerData = (float)20.0;
	
	if(m_pFileInfo[nIndex].nDataSize > m_nMaxDataPoint )	//Data를 1초로 보여 주기 충분 할때
	{
		AfxMessageBox(TEXT_LANG[31]);//"Data 수가 너무 많습니다"
		return -1;
	}
	m_pFileInfo[nIndex].nSubSetSize = m_pFileInfo[nIndex].nDataSize;
	/*
	if(m_pFileInfo->fSecPerData <10.0f)	//Data를 1초로 보여 주기 충분 할때
	{
		m_pFileInfo->fSecPerData = 10.0f;					//1Sec Interval View
		m_pFileInfo->nSubSetSize = (int)((float)nTotalTime/m_pFileInfo->fSecPerData);	//All Data View
	}*/
	
	return 0;
}


int CCTSAnalDoc::GetAxisCount()
{
	int nCount =0;
	for(char i=0; i<MAX_MUTI_AXIS; i++)
	{
		if(m_YSet[i].bItemSel)	nCount++;
	}
	return nCount;//MAX_MUTI_AXIS;
}

STR_Y_AXIS_SET * 
CCTSAnalDoc::GetAxisYSetting()
{
	int nCount =0;
	for(char i=0; i<MAX_MUTI_AXIS; i++)
	{
		if(m_YSet[i].bItemSel)
		{
			m_SelectedYSet[nCount++] = m_YSet[i];
		}
	}
	
	return m_SelectedYSet;
}

CString CCTSAnalDoc::ValueString(double dData, int item, BOOL bUnit)
{
	CString strMsg, strTemp;
	double dTemp;
	BYTE color;
	char szTemp[8];

	dTemp = dData;
	switch(item)
	{
	case EP_STATE:		strMsg = GetStateMsg((WORD)dTemp, color);	
		break;
		
//	case EP_OCV:
	case EP_VOLTAGE:		//voltage
		//mV단위로 변경 
		if(m_strVUnit == "V")
		{
			dTemp = dTemp / 1000.0f;	//LONG2FLOAT(Value);
		}
		//else //if(m_strVUnit == "mV")
		//{
		//}
		
		if(m_nVDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nVDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			dTemp = dTemp + 0.5f;	// 소수점 아래 반올림 처리
			strMsg.Format("%d", (LONG)dTemp);
		}		
		if(bUnit) strMsg+= (" "+m_strVUnit);
		
		break;

//		CUnit vtgUnit(DSP_UNIT_FLOAT);
//		strMsg = vtgUnit.GetData(Value);

	case EP_CURRENT:		//current
		if(dTemp < 0)	dTemp = -dTemp;			//양수 표기
		
		//current
		if(m_strIUnit == "A")
		{
			dTemp = dTemp/1000.0f;
		}  
		else if(m_strIUnit == "uA")
		{
			dTemp = dTemp * 1000.0f;		// LONG2FLOAT(Value)/1000.0f;
		}
		//else if(m_strIUnit == "mA")	//mA
		//{
		//}

		if(m_nIDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nIDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			dTemp = dTemp + 0.5f;	// 소수점 아래 반올림 처리
			strMsg.Format("%d", (LONG)dTemp);
		}
		if(bUnit) strMsg+= (" "+m_strIUnit);

		break;
			
	case EP_WATT	:
		if(dTemp < 0)	dTemp = -dTemp;			//양수 표기

		strTemp = " mW";
		if(m_strCUnit == "Ah" || m_strCUnit == "F")
		{
			dTemp = dTemp/1000.0f;
			strTemp = " W";
		}
		//else //if(m_strCUnit == "mAh" || m_strCUnit == "mF")
		//{
		//}

		if(m_nCDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nCDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			dTemp = dTemp + 0.5f;	// 소수점 아래 반올림 처리
			strMsg.Format("%d", (LONG)dTemp);
		}
		if(bUnit) strMsg+= strTemp;
		break;	
	case EP_WATT_HOUR:			
		if(dTemp < 0)	dTemp = -dTemp;			//양수 표기

		strTemp = " mWh";
		if(m_strCUnit == "Ah" || m_strCUnit == "F")
		{
			dTemp = dTemp/1000.0f;
			strTemp = " Wh";
		}
		//else //if(m_strCUnit == "mAh" || m_strCUnit == "mF")
		//{
		//}

		if(m_nCDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nCDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			dTemp = dTemp + 0.5f;	// 소수점 아래 반올림 처리
			strMsg.Format("%d", (LONG)dTemp);
		}
		if(bUnit) strMsg+= strTemp;
		
		break;
		
	case EP_CAPACITY:		//capacity
		if(dTemp < 0)	dTemp = -dTemp;			//양수 표기

		if(m_strCUnit == "Ah" || m_strCUnit == "F")
		{
			dTemp = dTemp/1000.0f;
		}
		//else //if(m_strCUnit == "mAh" || m_strCUnit == "mF")
		//{
		//}

		if(m_nCDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nCDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			dTemp = dTemp + 0.5f;	// 소수점 아래 반올림 처리
			strMsg.Format("%d", (LONG)dTemp);
		}
		if(bUnit) strMsg+= (" "+m_strCUnit);
		break;

	case EP_IMPEDANCE:	
		strMsg.Format("%.3f", dTemp);
		if(bUnit) strMsg+= " mOhm";
		break;

	case EP_CH_CODE:	//failureCode
		strMsg = ChCodeMsg((unsigned short)dTemp);
		break;

	case EP_TOT_TIME:
	case EP_STEP_TIME:

		//sbc에서 100msec단위 전송되어 온다.
		if(m_nTimeUnit == 1)	//sec 표시
		{
			strMsg.Format("%.1f", dTemp);
			if(bUnit) strMsg+= " sec";
		}
		else if(m_nTimeUnit == 2)	//Minute 표시
		{
			strMsg.Format("%.2f", dTemp/60.0f);
			if(bUnit) strMsg+= " Min";
		}
		else
		{
			CTimeSpan timeSpan((ULONG)dTemp);
			if(timeSpan.GetDays() > 0)
			{
				strMsg =  timeSpan.Format("%Dd %H:%M:%S");
			}
			else
			{
				strMsg = timeSpan.Format("%H:%M:%S");
			}
		}

/*		{
			char szTemp[32];
			ConvertTime(szTemp, Value);
			strMsg = szTemp;
		}
*/		break;

	case EP_GRADE_CODE:	
		strMsg = ::GetSelectCodeName((BYTE)dTemp);
		break;
	
	case EP_TEMPER:
		if(0xFFFFFFFF == (float)dTemp)
		{
			strMsg = TEXT_LANG[32];//"미사용"
		}
		else
		{
			strMsg.Format("%.1f", dTemp);
			if(bUnit) strMsg+= " ℃";
		}
		
		break;

	case EP_STEP_NO:
	default:
		strMsg.Format("%d", (int)dTemp);
		break;
	}
	
	return strMsg;
}

BOOL CCTSAnalDoc::LoadSetting()
{
	char szBuff[512];

	m_strCurFolder = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"CTSAnalyzer");

	m_strDataFolder = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"Data");		//Get Data Folder
	if(m_strDataFolder.IsEmpty())
	{
		m_strDataFolder.Format("%s\\Data", m_strCurFolder);
	}

	CString strInstallPath = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"CTSAnalyzer");		//Get Data Folder
	sprintf(szBuff, "%s\\%s", strInstallPath, FORM_SETTING_SAVE_FILE);
	::configFileName(szBuff);
//	AfxMessageBox(szBuff);

	m_TopConfig = ::GetTopConfig();

	if(m_TopConfig.m_ChannelInterval == -1)
	{
		m_TopConfig = GetDefaultTopConfigSet();
//		WritePrivateProfileStruct("CONFIG", "TOPCONFIG", 
//						&m_TopConfig, sizeof(STR_TOP_CONFIG), INI_FILE);
	}
	m_strModuleName = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Module Name", "Rack");	//Get Current Folder(PowerFormation Folde)
	m_strGroupName = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Group Name", "Group");	//Get Current Folder(PowerFormation Folde)
	m_nModulePerRack = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION , "Module Per Rack", 3);
	m_bUseRackIndex = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION , "Use Rack Index", TRUE);
	m_bUseGroupSet = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION , "Use Group", FALSE);

	m_nTrayColSize = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "TrayColSize", EP_DEFAULT_TRAY_COL_COUNT);


	CString strDBFolder = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");		//Get DataBase Folder
	if(strDBFolder.IsEmpty())
	{
		strDBFolder.Format("%s\\Database", m_strCurFolder);
		if(AfxGetApp()->WriteProfileString(FORM_PATH_REG_SECTION, "DataBase", strDBFolder) == 0)	return FALSE;
	}

	m_strDataBaseName = strDBFolder+"\\"+FORM_SET_DATABASE_NAME;
	
	m_strLogDataBaseName = strDBFolder+"\\"+LOG_DATABASE_NAME;

	UpdateUnitSetting();

	m_bLoadCodeDB = LoadCodeTable();
	
	m_bLoadField = LoadFieldList();
	
	RequeryProcType();	

	m_gradeColorConfig = ::GetGradeColorConfig();

	return TRUE;
}

//=============================================================================================//

CString CCTSAnalDoc::StepEndString(STR_COMMON_STEP *pStep) 
{
	CString strTemp, strTemp1;
	switch(pStep->stepHeader.type)
	{
	case EP_TYPE_CHARGE:			//Charge
		{
			if(pStep->fEndV > 0L)	
			{
				strTemp.Format("V >%s", ValueString(pStep->fEndV, EP_VOLTAGE));
			}
	
			if(pStep->fEndI > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strTemp1.Format("I < %s", ValueString(pStep->fEndI, EP_CURRENT));
				strTemp += strTemp1;
			}
			
			if(pStep->fEndTime >0)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
//				ConvertTime(szTemp, pStep->ulEndTime);
				strTemp1.Format("t > %s", ValueString(pStep->fEndTime, EP_STEP_TIME));
				strTemp += strTemp1;
			}
			if(pStep->fEndC > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strTemp1.Format("C > %s", ValueString(pStep->fEndC, EP_CAPACITY));
				strTemp += strTemp1;
			}
			if(pStep->fEndDV > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strTemp1.Format("dV > %s", ValueString(pStep->fEndDV, EP_VOLTAGE));
				strTemp += strTemp1;
			}
			if(pStep->fEndDI > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strTemp1.Format("dI > %s", ValueString(pStep->fEndDI, EP_CURRENT));
				strTemp += strTemp1;
			}
			break;
		}

	case EP_TYPE_DISCHARGE:		//Discharge
		{
			if(pStep->fEndV > 0L)	
			{
				strTemp.Format("V < %s", ValueString(pStep->fEndV, EP_VOLTAGE));
			}
			if(pStep->fEndI < 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strTemp1.Format("I > %s", ValueString(pStep->fEndI, EP_CURRENT));
				strTemp += strTemp1;
			}
			if(pStep->fEndTime > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
//				ConvertTime(szTemp, pStep->ulEndTime);
				strTemp1.Format("t > %s", ValueString(pStep->fEndTime, EP_STEP_TIME));
				strTemp += strTemp1;
			}
			if(pStep->fEndC > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strTemp1.Format("C > %s", ValueString(pStep->fEndC, EP_CAPACITY));
				strTemp += strTemp1;
			}
			if(pStep->fEndDV < 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strTemp1.Format("dV < %s", ValueString(pStep->fEndDV, EP_VOLTAGE));
				strTemp += strTemp1;
			}
			if(pStep->fEndDI > 0L)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strTemp1.Format("dI > %s", ValueString(pStep->fEndDI, EP_CURRENT));
				strTemp += strTemp1;
			}

			break;
		}

	case EP_TYPE_REST:				//Rest
		{
//			ConvertTime(szTemp, pStep->ulEndTime);
			strTemp.Format("t > %s", ValueString(pStep->fEndTime, EP_STEP_TIME)); 
			break;
		}
	case EP_TYPE_OCV:				//Ocv
	case EP_TYPE_IMPEDANCE:		//Impedance
	case EP_TYPE_END:				//End
	case -1:					//Not Seleted
		strTemp.Empty();
		break;

	case EP_TYPE_LOOP:
		{
			strTemp.Format("Goto(%d) Cycle(%d)", pStep->fVref+1, pStep->fIref); 
			break;
		}
		
	default:
		strTemp = TEXT_LANG[33];//"Step Type Loading Fail"
		break;
	
	}
	return strTemp;
}

CString CCTSAnalDoc::GetProcTypeName(int nType, int nStepType)
{
	STR_MSG_DATA *pMsg;
	CString strTemp(TEXT_LANG[34]);//"Unknown"

	if(nType == EP_PROC_TYPE_NONE)
		return strTemp = ::StepTypeMsg(nStepType);


	for(int i =0; i<m_apProcType.GetSize(); i++)
	{
		pMsg = (STR_MSG_DATA *)m_apProcType[i];
		if(nType == pMsg->nCode)
		{
			strTemp.Format("%s", pMsg->szMessage);
			return strTemp;
		}
	}
	return strTemp;
}

BOOL CCTSAnalDoc::RequeryProcType()
{
	ASSERT(m_apProcType.GetSize() == 0);

	if(m_strDataBaseName.IsEmpty())		return FALSE;
	
	CString strSQL;
	strSQL.Format("SELECT ProcType, ProcID, Description FROM ProcType WHERE ProcType >= %d ORDER BY ProcType", EP_PROC_TYPE_CHARGE_LOW);
	
	COleVariant data;
	CDaoDatabase  db;

	try
	{
		db.Open(m_strDataBaseName);
		CDaoRecordset rs(&db);
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
		
		if(!rs.IsBOF() && !rs.IsEOF())
		{
			COleVariant data;
			STR_MSG_DATA	*pObject;
			pObject = new STR_MSG_DATA;
			pObject->nCode = 0;
			pObject->nData = 0;
			sprintf(pObject->szMessage, TEXT_LANG[35]);//"관계없음"
			m_apProcType.Add(pObject);
			
			while(!rs.IsEOF())
			{
				pObject = new STR_MSG_DATA;
				ASSERT(pObject);
				ZeroMemory(pObject, sizeof(STR_MSG_DATA));
				
				data = rs.GetFieldValue(0);
				pObject->nCode = data.lVal;
				data = rs.GetFieldValue(1);
				pObject->nData = data.lVal;
				data = rs.GetFieldValue(2);
				sprintf(pObject->szMessage, "%s", data.pbVal);
				
				m_apProcType.Add(pObject);
				rs.MoveNext();
			}	
		}

		rs.Close();
		db.Close();
	}
	catch (CException* e)
	{
		TCHAR sz[255];
		e->GetErrorMessage(sz,255);
		AfxMessageBox(sz);
		e->Delete();
	}

	return TRUE;
}

BOOL CCTSAnalDoc::DownLoadProfileData(CString strFileName)
{
	CString strProgramName;
	strProgramName.Format("%s\\DataDown.exe", m_strCurFolder);
	if(ExecuteProgram(strProgramName, "", "", DATA_DOWN_TITLE_NAME, FALSE, TRUE))
	{
		//Message를 전송한다.
		HWND pWnd = ::FindWindow(NULL, DATA_DOWN_TITLE_NAME);
		if(pWnd)
		{
			CString strData;
			strData = strFileName;
			if(strData.IsEmpty())
			{
				return FALSE;
			}
			int nSize = strData.GetLength()+1;
			char *pData = new char[nSize];
			ZeroMemory(pData, nSize);
			sprintf(pData, "%s", strData);
			pData[nSize-1] = '\0';

			COPYDATASTRUCT CpStructData;
			CpStructData.dwData = 2;		//CTS Program 구별 Index
			CpStructData.cbData = nSize;
			CpStructData.lpData = pData;
			
			::SendMessage(pWnd, WM_COPYDATA, 0, (LPARAM)&CpStructData);
			
			delete [] pData;
		}
		else
		{
			return FALSE;
		}
	}
	else
	{
		return FALSE;
	}
	return TRUE;
}

void CCTSAnalDoc::UpdateUnitSetting()
{
	char szBuff[32], szUnit[16], szDecimalPoint[16];

	sprintf(szBuff, AfxGetApp()->GetProfileString(FORMSET_REG_SECTION, "V Unit", "V 0"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strVUnit = szUnit;
	m_nVDecimal = atoi(szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(FORMSET_REG_SECTION, "I Unit", "mA 0"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strIUnit = szUnit;
	m_nIDecimal = atoi(szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(FORMSET_REG_SECTION, "C Unit", "mAh 0"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strCUnit = szUnit;
	m_nCDecimal = atoi(szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(FORMSET_REG_SECTION, "Time Unit", "0"));
	m_nTimeUnit = atol(szBuff);

	m_bHideNonCellData = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "HideNonCellData", FALSE);
}

BOOL CCTSAnalDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
	if (!CDocument::OnOpenDocument(lpszPathName))
		return FALSE;
	
	// TODO: Add your specialized creation code here
	if(LoadSetting() == FALSE)
	{
		AfxMessageBox("Config data open fail...");
		return FALSE;
	}
	return TRUE;
}

BOOL CCTSAnalDoc::SetDataField()
{
	// TODO: Add your control notification handler code here

	CDataFieldSetDlg	*pDlg = new CDataFieldSetDlg();
	if(pDlg == NULL)	return FALSE;
	
	pDlg->m_nFieldCount = m_TotCol;
	for(int i =0; i<m_TotCol; i++)
	{
		pDlg->m_Field[i] = m_field[i];
	}

	if(pDlg->DoModal() == IDOK)
	{
		m_TotCol = pDlg->m_nFieldCount;
		for(int i =0; i<pDlg->m_nFieldCount; i++)
		{
			m_field[i] = pDlg->m_Field[i];
		}
		delete pDlg;
		pDlg = NULL;
		return TRUE;
	}

	delete pDlg;
	pDlg = NULL;
	return FALSE;
}

COLORREF CCTSAnalDoc::GetGradeColor(CString strCode)
{
	if(strCode.IsEmpty() == FALSE)
	{
		for(int i=0; i<MAX_GRADE_COLOR; i++)
		{
			if(m_gradeColorConfig.colorData[i].bUse)
			{
				if(CString(m_gradeColorConfig.colorData[i].szCode) == strCode)
				{
					return m_gradeColorConfig.colorData[i].dwColor;
				}
			}
		}
	}

	return RGB(255,255,255);
}

BOOL CCTSAnalDoc::ExecuteProgram(CString strProgram, CString strArgument/* = ""*/, CString strClassName /*= ""*/, CString strWindowName/*= ""*/, BOOL bNewExe/* = FALSE*/, 	BOOL bWait/* = FALSE*/)
{
	if(strProgram.IsEmpty())	return FALSE;

//	TCHAR szBuff[_MAX_PATH];
//	::GetModuleFileName(AfxGetApp()->m_hInstance,szBuff,_MAX_PATH);
//	CString path = CString(szBuff).Mid(0, CString(szBuff).ReverseFind('\\'));

	//새롭게 실행 하지 않으면 주어진 이름으로 검색한다.
	if(!bNewExe)
	{
		CWnd *pWndPrev = NULL, *pWndChild = NULL;

		if(!strWindowName.IsEmpty())
		{
			pWndPrev = CWnd::FindWindow(NULL, strWindowName);
		}

		if(!strClassName.IsEmpty())
		{
			pWndPrev = CWnd::FindWindow(strClassName, NULL);
		}

		// Determine if a window with the class name exists...
		if(pWndPrev != NULL)
		{
			// If so, does it have any popups?
			pWndChild = pWndPrev->GetLastActivePopup();

			// If iconic, restore the main window.
			if (pWndPrev->IsIconic())
				pWndPrev->ShowWindow(SW_RESTORE);

			// Bring the main window or its popup to the foreground
			pWndChild->SetForegroundWindow();

			// and you are done activating the other application.
			return TRUE;
		}
		/*
		HWND FirsthWnd = NULL;
		if(!strWindowName.IsEmpty())
		{
			FirsthWnd = ::FindWindow(NULL, strWindowName);
		}
		if(!strClassName.IsEmpty())
		{
			FirsthWnd = ::FindWindow(strClassName, NULL);
		}
		if (FirsthWnd)		//실행 되어 있으면 
		{
//			::SetActiveWindow(FirsthWnd);	
			::SetForegroundWindow(FirsthWnd);
			return TRUE;
		}
		//else	//실행된게 없으면 새롭게 실행 
		*/
	}

	CString strTemp;

	CString strFullName;
	if(!strArgument.IsEmpty())
	{
		strFullName.Format("%s \"%s\"", strProgram, strArgument);
	}
	else
	{
		strFullName.Format("%s", strProgram);
	}

	STARTUPINFO	stStartUpInfo;
	PROCESS_INFORMATION	pi;
	ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
	ZeroMemory(&pi, sizeof(PROCESS_INFORMATION));
	
	stStartUpInfo.cb = sizeof(STARTUPINFO);
	stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
	stStartUpInfo.wShowWindow = SW_SHOWNORMAL;

		
	BOOL bFlag = ::CreateProcess(	NULL, 
									(LPSTR)(LPCTSTR)strFullName, 
									NULL, 
									NULL, 
									FALSE, 
									NORMAL_PRIORITY_CLASS, 
									NULL, 
									NULL, 
									&stStartUpInfo, 
									&pi
								);
	if(bFlag == FALSE)
	{
		strTemp.Format(TEXT_LANG[36], strFullName);
		AfxMessageBox(strTemp, MB_OK|MB_ICONSTOP);
		return FALSE;
	}
    
	if(bWait)
	{
		DWORD dwRet = WaitForInputIdle(pi.hProcess, 15000);
		if (WAIT_TIMEOUT != dwRet)  return true;
	}

	CloseHandle( pi.hProcess );
	CloseHandle( pi.hThread );
	
	return TRUE;
}

BOOL CCTSAnalDoc::ShowCpCpkData(CString strLot, CStringArray &aFile)
{
	// TODO: Add your control notification handler code here
	double         dMax = -10E+100, dMin = 10E+100;
	CString        str;
	double			dData1;

	static double dDivision = 1;
	static double dLowLimit = 0;
	static double dHighLimit = 0;
	static int nCpItem = 0;

//////Cp, Cpk Set Dlg
	CCpkSetDlg     dlgCpkSet;
	dlgCpkSet.m_strHighRange.Format("%.4f", dHighLimit);
	dlgCpkSet.m_strLowRange.Format("%.4f", dLowLimit);
	dlgCpkSet.m_strGraphGap.Format("%.4f", dDivision);
	dlgCpkSet.m_nItem = nCpItem;
	if(strLot.IsEmpty() == FALSE)
	{
		dlgCpkSet.m_strTitle.Format(TEXT_LANG[37], strLot);//"Lot [%s] 분포도 보기"
	}
   	
	dlgCpkSet.m_nCount = m_TotCol;
	for(int i =0; i<m_TotCol; i++)
	{
		dlgCpkSet.m_field[i].No = m_field[i].No;
		dlgCpkSet.m_field[i].FieldName = m_field[i].FieldName;
		dlgCpkSet.m_field[i].ID = m_field[i].ID;
   		dlgCpkSet.m_field[i].DataType = m_field[i].DataType;
		dlgCpkSet.m_field[i].bDisplay = m_field[i].bDisplay;
	}
	
	if(dlgCpkSet.DoModal() != IDOK)
	{
		return FALSE;
	}
	
	dHighLimit = atof(dlgCpkSet.m_strHighRange);
	dLowLimit  = atof(dlgCpkSet.m_strLowRange);
	dDivision  = atof(dlgCpkSet.m_strGraphGap);
	nCpItem = dlgCpkSet.m_nItem;
//////
	int procType = m_field[nCpItem].ID;
	int dataType = m_field[nCpItem].DataType;

	CList<double, double&> dDataBuffer;   
	CString strRltFile, strTemp;
	STR_STEP_RESULT *lpStepDataInfo;
	STR_SAVE_CH_DATA *lpChannelData;
	CFormResultFile *pRltFile = new CFormResultFile;
	
	for(int row=0; row<aFile.GetSize(); row++)
	{
		strRltFile = aFile.GetAt(row);
		if(pRltFile->ReadFile(strRltFile))
		{
			for(int step = 0; step<pRltFile->GetStepSize(); step++)
			{
				lpStepDataInfo = pRltFile->GetStepData(step);
				if(lpStepDataInfo)
				{
					//find matting field
//					for(int f = 0; f<m_pDoc->m_TotCol; f++)
//					{
						if(lpStepDataInfo->stepCondition.stepHeader.nProcType == procType)// || pDoc->m_field[f].ID == RPT_CH_CODE)
						{
							for(int c = 0; c<lpStepDataInfo->aChData.GetSize(); c++)
							{
								lpChannelData = (STR_SAVE_CH_DATA *)lpStepDataInfo->aChData[c];
								if(!IsNormalCell(lpChannelData->channelCode))	 continue;	//정상이 아니면 계산 않함

								//find matting data type
								switch(dataType)
								{
								case EP_VOLTAGE:
									dData1 = atof(ValueString(lpChannelData->fVoltage, dataType));
									if(dMin > dData1)  dMin = dData1;	//Min
									if(dMax < dData1)  dMax = dData1;	//Max
									dDataBuffer.AddTail(dData1);
									break;
								case EP_CURRENT:
									dData1 = atof(ValueString(lpChannelData->fCurrent, dataType));
									if(dMin > dData1)  dMin = dData1;	//Min
									if(dMax < dData1)  dMax = dData1;	//Max
									dDataBuffer.AddTail(dData1);
									break;
								case EP_CAPACITY:
									dData1 = atof(ValueString(lpChannelData->fCapacity, dataType));
									if(dMin > dData1)  dMin = dData1;	//Min
									if(dMax < dData1)  dMax = dData1;	//Max
									dDataBuffer.AddTail(dData1);
									break;
								case EP_STEP_TIME:
									dData1 = atof(ValueString(lpChannelData->fStepTime, dataType));
									if(dMin > dData1)  dMin = dData1;	//Min
									if(dMax < dData1)  dMax = dData1;	//Max
									dDataBuffer.AddTail(dData1);
									break;
								case EP_IMPEDANCE:
									dData1 = atof(ValueString(lpChannelData->fImpedance, dataType));
									if(dMin > dData1)  dMin = dData1;	//Min
									if(dMax < dData1)  dMax = dData1;	//Max
									dDataBuffer.AddTail(dData1);
									break;
								}
							}
						}
//					}			
				}
			}
		}
	}
	delete pRltFile;
	pRltFile = NULL;

	long lCount = dDataBuffer.GetCount();
	if(lCount <= 0)
	{
		AfxMessageBox(TEXT_LANG[38], MB_ICONSTOP|MB_OK);//"범위내의 해당하는 Cell이 없습니다."
		return FALSE;
	}

	//Memory 할당 받는다. (양품수로 해서 => 최대 가능 범위)
	double *pData = new double[lCount];
	ASSERT(pData);
	ZeroMemory(pData, sizeof(double)*lCount);	
	lCount = 0;

	POSITION pos = dDataBuffer.GetHeadPosition();
	while(pos)
	{
		dData1 = dDataBuffer.GetNext(pos);
		if(dlgCpkSet.m_bRange == TRUE)
		{
   			if(dlgCpkSet.m_bHighRange == TRUE && dlgCpkSet.m_bLowRange == TRUE) //상하한 설정시  
			{
   				if(dLowLimit <= dData1 && dData1 <= dHighLimit)
				{
					pData[lCount++] = dData1;
				}		    					
			}
			else if(dlgCpkSet.m_bHighRange == TRUE && dlgCpkSet.m_bLowRange == FALSE)//상한만
			{
				if(dData1 <= dHighLimit)
				{
					pData[lCount++] = dData1;
				}
			}
			else if(dlgCpkSet.m_bHighRange == TRUE && dlgCpkSet.m_bLowRange == FALSE)//하한만
			{
	  			if(dLowLimit <= dData1)
				{
					pData[lCount++] = dData1;
				}
			}
		}
		else   // m_pCpkSetDlg.m_bRange = FALSE
		{
			pData[lCount++] = dData1;
		}
	}
		
	//ASSERT(lCount > 0);	
	if(lCount <= 0)
	{
		AfxMessageBox(TEXT_LANG[38], MB_ICONSTOP|MB_OK);//"범위내의 해당하는 Cell이 없습니다."
		return FALSE;
	}
	//Cp, Cpk Graph  Display
	CCpkViewDlg* pCpkViewDlg = new CCpkViewDlg();
	pCpkViewDlg->SetRowData(pData, lCount, dDivision);
	pCpkViewDlg->SetHighLimit(dlgCpkSet.m_bHighRange, dHighLimit);
	pCpkViewDlg->SetLowLimit(dlgCpkSet.m_bLowRange, dLowLimit);
	pCpkViewDlg->m_strXAxisLable =m_field[nCpItem].FieldName; 
	if(strLot.IsEmpty() == FALSE)
	{
		pCpkViewDlg->m_strTitle.Format(TEXT_LANG[39], strLot, m_field[nCpItem].FieldName, lCount);
		//"%s %s 분포도(%d개)"
	}
	else
	{
		pCpkViewDlg->m_strTitle.Format(TEXT_LANG[40], m_field[nCpItem].FieldName, lCount);
		//"%s 분포도(%d개)"
	}
	pCpkViewDlg->DoModal();
	delete pCpkViewDlg;

	delete [] pData;
	pData = NULL;	

	return TRUE;
}

BOOL CCTSAnalDoc::ShowLotAllData(CString strLot, CStringArray &aFile)
{
	CSerarchDataDlg dlg(this);
	dlg.SetFileName(&aFile);
	CString str;
	if(strLot.IsEmpty())
	{
		str = TEXT_LANG[41];//"검색 Tray data 보기"
	}
	else
	{
		str.Format(TEXT_LANG[42], strLot);//"Lot %s 검색 Tray data 보기"

	}
	dlg.SetTitle(str);
	dlg.DoModal();

	return TRUE;
}

BOOL CCTSAnalDoc::ExecuteGraphAnalyzer(CString strFileName)
{
	CString strProgramName;
	strProgramName.Format("%s\\CTSGraphAnalyzer.exe", m_strCurFolder);
	if(ExecuteProgram(strProgramName, "", "", "CTSGraphAnalyzer", FALSE, TRUE))
	{
		//Message를 전송한다.
		HWND pWnd = ::FindWindow(NULL, "CTSGraphAnalyzer");
		if(pWnd)
		{
			CString strData;
			strData = strFileName;
			if(strData.IsEmpty())
			{
				return FALSE;
			}
			int nSize = strData.GetLength()+1;
			char *pData = new char[nSize];
			ZeroMemory(pData, nSize);
			sprintf(pData, "%s", strData);
			pData[nSize-1] = '\0';

			COPYDATASTRUCT CpStructData;
			CpStructData.dwData = 5;		//CTS Program 구별 Index
			CpStructData.cbData = nSize;
			CpStructData.lpData = pData;
			
			::SendMessage(pWnd, WM_COPYDATA, 0, (LPARAM)&CpStructData);
			
			delete [] pData;
		}
		else
		{
			return FALSE;
		}
	}
	else
	{
		return FALSE;
	}
	return TRUE;
}
