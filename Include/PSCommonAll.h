///////////////////////////////////////
//
//		ADPOWER Battery Charge/Discharge System
//		Inculde file of PSCommon.dll
//		BFGraphCom.dll Class include file
//
///////////////////////////////////////

#ifndef _ADPOWER_BFGRAPHCOMMON_DLL_INCLUDE_H_
#define _ADPOWER_BFGRAPHCOMMON_DLL_INCLUDE_H_

#include "PSCommon.h"

#include "../BFGraphCom/Grading.h"
#include "../BFGraphCom/Step.h"
#include "../BFGraphCom/ScheduleData.h"

#include "../BFGraphCom/GridCtrl_src/GridCtrl.h"

#include "../BFGraphCom/Label.h"

#include "../BFGraphCom/ChData.h"
#include "../BFGraphCom/Table.h"
#include "../BFGraphCom/ProgressWnd.h"

#include "../BFGraphCom/UnitTrans.h"

#include "../BFGraphCom/Label.h"


#endif	//_ADPOWER_BFGRAPHCOMMON_DLL_INCLUDE_H_


