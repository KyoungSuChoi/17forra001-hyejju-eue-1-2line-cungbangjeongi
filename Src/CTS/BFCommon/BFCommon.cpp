// BFCommon.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include <afxdllx.h>
#include <Winsock2.h>
#include "resource.h"       // main symbols


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static AFX_EXTENSION_MODULE BFCommonDLL = { NULL, NULL };

extern "C" int APIENTRY
DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		TRACE0("BFCommon.DLL Initializing!\n");
		
		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(BFCommonDLL, hInstance))
			return 0;

		// Insert this DLL into the resource chain
		// NOTE: If this Extension DLL is being implicitly linked to by
		//  an MFC Regular DLL (such as an ActiveX Control)
		//  instead of an MFC application, then you will want to
		//  remove this line from DllMain and put it in a separate
		//  function exported from this Extension DLL.  The Regular DLL
		//  that uses this Extension DLL should then explicitly call that
		//  function to initialize this Extension DLL.  Otherwise,
		//  the CDynLinkLibrary object will not be attached to the
		//  Regular DLL's resource chain, and serious problems will
		//  result.

		new CDynLinkLibrary(BFCommonDLL);
	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		TRACE0("BFCommon.DLL Terminating!\n");
		// Terminate the library before destructors are called
		AfxTermExtensionModule(BFCommonDLL);
	}
	return 1;   // ok
}

char szSettingFileName[128] = "C:CommConfig.cfg";
char szTempConfigFileName[128] = "TempConfig.cfg";
CHAR szTempScaleConfigFileName[128] = "TempScaleConfig.cfg";

EPDLL_API long ExpFloattoLong(float fvalue, int exponential)
{
	int decimal, sign;
	char *buffer;
   	buffer = _fcvt( fvalue, exponential, &decimal, &sign );

	if(sign == 1)
		return (-1)*atol(buffer);
	else return atol(buffer);
}

EPDLL_API float ExpLongToFloat(long ldata, int exponential)
{
	float fResult = 1.0f;

	for(int i= 0; i<exponential; i++)
		fResult *= 10.0f;
	
	if(exponential<0)
	{
		fResult = (float)ldata*fResult;
	}
	else if(exponential>0)
	{
		 fResult = (float)ldata/fResult;
	}
	else
	{
		fResult =  (float)ldata;
	}
	return fResult;
}

EPDLL_API int WriteLogFile(char *szFileName, char *szData, int nMode)
{
	if(szData == NULL || szFileName == NULL)	return -1;
	
	FILE *fp;
//	time_t aclock;
//	struct tm *newtime;
	
	if(nMode == FILE_CREATE)
	{
		fp = fopen(szFileName, "wb");
	}
	else if(nMode == FILE_APPEND)
	{
		fp = fopen(szFileName, "ab+");
	}
	
	if(fp)
	{
//		time( &aclock );					/* Get time in seconds */
//	    newtime = localtime( &aclock );		/* Convert time to struct tm form */
//		fprintf(fp, "%s :: %s", asctime( newtime ), szData );	/* Print local time as a string */
//		CTimeSpan newtime(aclock);
//		CString s = newtime.Format( "Total days: %D, hours: %H, mins: %M, secs: %S" );
//		fprintf(fp, "%s :: %s\r\n", ctime(&aclock), szData);

		COleDateTime nowtime(COleDateTime::GetCurrentTime());
		fprintf(fp, "%s :: %s\r\n", nowtime.Format(), szData);
		fclose(fp);
		return 0;
	}
	return -1;
}

EPDLL_API void ConvertTime(char *szTime, float fTime)
{
	if(szTime == NULL)	return;
	time_t		tmptime = (time_t)(fTime);
	CTimeSpan 	stepTime(tmptime);
	int modtime = int((fTime-(UINT)fTime)*10);

	if(stepTime.GetDays() > 0)
	{
		if(modtime > 0)
		{
			sprintf(szTime, "%dD %02d:%02d.%d", stepTime.GetDays(), stepTime.GetHours(), stepTime.GetMinutes(), stepTime.GetSeconds(), modtime);
		}
		else
		{
			sprintf(szTime, "%dD %02d:%02d", stepTime.GetDays(), stepTime.GetHours(), stepTime.GetMinutes(), stepTime.GetSeconds());
		}
	}
	else
	{
		if(modtime > 0)
		{
			sprintf(szTime, "%d:%02d:%02d.%d", stepTime.GetHours(), stepTime.GetMinutes(), stepTime.GetSeconds(), modtime);
		}
		else
		{
			sprintf(szTime, "%d:%02d:%02d", stepTime.GetHours(), stepTime.GetMinutes(), stepTime.GetSeconds());
		}
	}
	return;
}

EPDLL_API char *WSALastErrorString(int nLastError)
{
	switch(nLastError)
	{
	case WSAENAMETOOLONG	:   return "Winsock Error :: Name too long";
    case WSANOTINITIALISED	:	return "Winsock Error :: Not initialized";
    case WSASYSNOTREADY		:   return "Winsock Error :: System not ready";
    case WSAVERNOTSUPPORTED :	return "Winsock Error :: Version is not supported";
    case WSAESHUTDOWN		:	return "Winsock Error :: Can't send after socket shutdown";
    case WSAEINTR			:   return "Winsock Error :: Interrupted system call";
    case WSAHOST_NOT_FOUND	:	return "Winsock Error :: Host not found";
    case WSATRY_AGAIN		:   return "Winsock Error :: Try again";
    case WSANO_RECOVERY		:	return "Winsock Error :: Non-recoverable error";
    case WSANO_DATA			:   return "Winsock Error :: No data record available";
    case WSAEBADF			:   return "Winsock Error :: Bad file number";
    case WSAEWOULDBLOCK		:   return "Winsock Error :: Operation would block";
    case WSAEINPROGRESS		:   return "Winsock Error :: Operation now in progress";
    case WSAEALREADY		:   return "Winsock Error :: Operation already in progress";
    case WSAEFAULT			:   return "Winsock Error :: Bad address";
    case WSAEDESTADDRREQ	:   return "Winsock Error :: Destination address required";
    case WSAEMSGSIZE		:   return "Winsock Error :: Message too long";
    case WSAEPFNOSUPPORT	:	return "Winsock Error :: Protocol family not supported";
    case WSAENOTEMPTY		:   return "Winsock Error :: Directory not empty";
    case WSAEPROCLIM		:   return "Winsock Error :: EPROCLIM returned";
    case WSAEUSERS			:   return "Winsock Error :: EUSERS returned";
    case WSAEDQUOT			:   return "Winsock Error :: Disk quota exceeded";
    case WSAESTALE			:   return "Winsock Error :: ESTALE returned";
    case WSAEINVAL			:   return "Winsock Error :: Invalid argument";
    case WSAEMFILE			:   return "Winsock Error :: Too many open files";
    case WSAEACCES			:   return "Winsock Error :: Access denied";
    case WSAELOOP			:   return "Winsock Error :: Too many levels of symbolic links";
    case WSAEREMOTE			:   return "Winsock Error :: The object is remote";
    case WSAENOTSOCK		:   return "Winsock Error :: Socket operation on non-socket";
    case WSAEADDRNOTAVAIL	:   return "Winsock Error :: Can't assign requested address";
    case WSAEADDRINUSE		:   return "Winsock Error :: Address already in use";
    case WSAEAFNOSUPPORT	:   return "Winsock Error :: Address family not supported by protocol family";
    case WSAESOCKTNOSUPPORT :   return "Winsock Error :: Socket type not supported";
    case WSAEPROTONOSUPPORT :   return "Winsock Error :: Protocol not supported";
    case WSAENOBUFS			:   return "Winsock Error :: No buffer space is supported";
    case WSAETIMEDOUT		:   return "Winsock Error :: Connection timed out";
    case WSAEISCONN			:   return "Winsock Error :: Socket is already connected";
    case WSAENOTCONN		:   return "Winsock Error :: Socket is not connected";
    case WSAENOPROTOOPT		:   return "Winsock Error :: Bad protocol option";
    case WSAECONNRESET		:   return "Winsock Error :: Connection reset by peer";
    case WSAECONNABORTED	:   return "Winsock Error :: Software caused connection abort";
    case WSAENETDOWN		:   return "Winsock Error :: Network is down";
    case WSAENETRESET		:   return "Winsock Error :: Network was reset";
    case WSAECONNREFUSED	:   return "Winsock Error :: Connection refused";
    case WSAEHOSTDOWN		:   return "Winsock Error :: Host is down";
    case WSAEHOSTUNREACH	:   return "Winsock Error :: Host is unreachable";
    case WSAEPROTOTYPE		:   return "Winsock Error :: Protocol is wrong type for socket";
    case WSAEOPNOTSUPP		:   return "Winsock Error :: Operation not supported on socket";
    case WSAENETUNREACH		:   return "Winsock Error :: ICMP network unreachable";
    case WSAETOOMANYREFS	:   return "Winsock Error :: Too many references";
    default					:   return "Winsock Error :: Unknown";
    }
}

EPDLL_API void FileSize(char *szString, unsigned long nSize)
{
	if(szString == NULL || nSize <0 )	return;
	if(nSize < 1000)	
	{
		sprintf(szString, "%dB", nSize);
	}
	else 
	{
		int nTeraSize =0, nMegaSize =0, nKilloSize =0;
		
		nKilloSize = nSize/1000;
		nMegaSize = nKilloSize/1000;
		nTeraSize = nMegaSize/1000;
		if(nTeraSize > 0)
		{
			nMegaSize = nMegaSize%1000;
			nKilloSize = nKilloSize%1000;
			sprintf(szString, "%d,%d,%dKB",  nTeraSize, nMegaSize, nKilloSize);
		}
		else if(nMegaSize >0)
		{
			nKilloSize = nKilloSize%1000;
			sprintf(szString, "%d,%dKB",  nMegaSize, nKilloSize);
		}
		else 
		{
			sprintf(szString, "%dKB", nKilloSize);
		}
	}
	return;
}

EPDLL_API void myGetFileName(char *szFullName)
{
	CString strFullName(szFullName);
	strFullName = strFullName.Mid(strFullName.ReverseFind('\\')+1);
	strcpy(szFullName, (LPSTR)(LPCTSTR)strFullName);
}

EPDLL_API void myGetPathName(char *szFullName)
{
	CString strFullName(szFullName);
	strFullName = strFullName.Left(strFullName.ReverseFind('\\'));
	strcpy(szFullName, (LPSTR)(LPCTSTR)strFullName);
}

EPDLL_API BOOL configFileName(char *szSetFileName)
{
	if(szSetFileName == NULL)	
		return FALSE;
	
	strcpy(szSettingFileName, szSetFileName);
	return TRUE;
}

EPDLL_API char* getconfigFileName()
{
	return szSettingFileName;
}

EPDLL_API STR_TOP_CONFIG GetDefaultTopConfigSet()
{
	STR_TOP_CONFIG strTopConfig;
	memset(&strTopConfig, -1, sizeof(STR_TOP_CONFIG));

	if(szSettingFileName == NULL)	return strTopConfig;

	FILE *fp = fopen(szSettingFileName, "rb");
	if(fp != NULL)
	{
		fread(&strTopConfig, sizeof(STR_TOP_CONFIG), 1, fp);		//get default setting
		fclose(fp);
	}
	return strTopConfig;
}

EPDLL_API STR_TOP_CONFIG  GetTopConfig()
{
	STR_TOP_CONFIG strTopConfig;
	memset(&strTopConfig, -1, sizeof(STR_TOP_CONFIG));

	if(szSettingFileName == NULL)
	{ 
		return strTopConfig;
	}
	
	FILE *fp = fopen(szSettingFileName, "rb");
	if(fp != NULL)
	{
		fseek(fp, sizeof(STR_TOP_CONFIG), SEEK_SET);				//Skip default config
		fread(&strTopConfig, sizeof(STR_TOP_CONFIG), 1, fp);		//Current Config
		fclose(fp);
	}
	return strTopConfig;
}

EPDLL_API BOOL	WriteTopConfig(STR_TOP_CONFIG topConfig)
{
	if(szSettingFileName == NULL)	return FALSE;

	FILE *fp = fopen(szSettingFileName, "wb+");

	STR_TOP_CONFIG tempConfig;
		
	if(fp != NULL)
	{
		fwrite(&topConfig, sizeof(STR_TOP_CONFIG), 1, fp);			// save current setting	
		// fseek(fp, sizeof(STR_TOP_CONFIG), SEEK_SET);				// skip default config
		fwrite(&topConfig, sizeof(STR_TOP_CONFIG), 1, fp);			// save current setting		
		fclose(fp);
	}
	else
	{
		return FALSE;
	}
	return TRUE;
}

EPDLL_API BOOL WriteTempConfig( STR_TEMP_CONFIG tempConfig )
{
	if( szTempConfigFileName == NULL )
		return FALSE;

	FILE *fp = fopen(szTempConfigFileName, "wb+");
	if( fp != NULL )
	{
		// fseek(fp, sizeof(STR_TEMP_CONFIG), SEEK_SET);
		fwrite(&tempConfig, sizeof(STR_TEMP_CONFIG), 1, fp);
		fclose(fp);
	}
	else
	{
		return FALSE;
	}
	return TRUE;
}

EPDLL_API BOOL WriteTempScaleConfig( STR_TEMPSCALE_CONFIG tempConfig )
{
	if( szTempConfigFileName == NULL )
		return FALSE;
	
	FILE *fp = fopen(szTempScaleConfigFileName, "wb+");
	if( fp != NULL )
	{
		fseek(fp, sizeof(STR_TEMPSCALE_CONFIG), SEEK_SET);
		fwrite(&tempConfig, sizeof(STR_TEMPSCALE_CONFIG), 1, fp);
		fclose(fp);
	}
	else
	{
		return FALSE;
	}
	return TRUE;
}

EPDLL_API STR_TEMP_CONFIG GetTempConfig()
{
	STR_TEMP_CONFIG strTempConfig;
	memset(&strTempConfig, 0, sizeof(STR_TEMP_CONFIG));
	if( szTempConfigFileName == NULL )	return strTempConfig;
	FILE *fp = fopen(szTempConfigFileName, "rb");
	if( fp != NULL )
	{
		fseek(fp, sizeof(STR_TEMP_CONFIG), SEEK_SET );
		fread(&strTempConfig, sizeof(STR_TEMP_CONFIG), 1, fp);
		fclose(fp);
	}
	return strTempConfig;
}

EPDLL_API STR_TEMPSCALE_CONFIG GetTempScaleConfig()
{
	STR_TEMPSCALE_CONFIG strTempConfig;
	memset(&strTempConfig, 0, sizeof(STR_TEMPSCALE_CONFIG));
	if( szTempScaleConfigFileName == NULL )	return strTempConfig;
	FILE *fp = fopen(szTempScaleConfigFileName, "rb");
	if( fp != NULL )
	{
		fseek(fp, sizeof(STR_TEMPSCALE_CONFIG), SEEK_SET );
		fread(&strTempConfig, sizeof(STR_TEMPSCALE_CONFIG), 1, fp);
		fclose(fp);
	}
	return strTempConfig;
}

EPDLL_API STR_GRADE_COLOR_CONFIG  GetGradeColorConfig()
{
	STR_GRADE_COLOR_CONFIG gradeConfig;
	memset(&gradeConfig, 0, sizeof(STR_GRADE_COLOR_CONFIG));

	if(szSettingFileName == NULL)	return gradeConfig;
	
	FILE *fp = fopen(szSettingFileName, "rb");
	if(fp != NULL)
	{
		fseek(fp, sizeof(STR_TOP_CONFIG)*2+sizeof(STR_LOGIN), SEEK_SET);				//Skip default config
		fread(&gradeConfig, sizeof(STR_GRADE_COLOR_CONFIG), 1, fp);		//Current Config
		fclose(fp);
	}
	return gradeConfig;
}

EPDLL_API BOOL	WriteGradeColorConfig(STR_GRADE_COLOR_CONFIG gradeConfig)
{
	if(szSettingFileName == NULL)	return FALSE;

	FILE *fp = fopen(szSettingFileName, "rb+");
	if(fp != NULL)
	{
		fseek(fp, sizeof(STR_TOP_CONFIG)*2+sizeof(STR_LOGIN), SEEK_SET);	//Skip default config
		fwrite(&gradeConfig, sizeof(STR_GRADE_COLOR_CONFIG), 1, fp);				//save current setting
		fclose(fp);
	}
	else
	{
		return FALSE;
	}
	return TRUE;
}


EPDLL_API STR_LOGIN ReadLastLogID()
{
	STR_LOGIN logInData;
	memset(&logInData, 0, sizeof(STR_LOGIN));
	
	if(szSettingFileName == NULL)	return logInData;

	unsigned int i;

	FILE *fp = fopen(szSettingFileName, "rb");
	if(fp != NULL)
	{
		fseek(fp, sizeof(STR_TOP_CONFIG)*2, SEEK_SET);
		fread(&logInData, sizeof(STR_LOGIN), 1, fp);
		
		for(i =0; i<strlen(logInData.szLoginID); i++)
		{
			logInData.szLoginID[i] += PASSWORD_OFFSET;
		}
		for(i =0; i<strlen(logInData.szPassword); i++)
		{
			logInData.szPassword[i] += PASSWORD_OFFSET;
		}
		for(i =0; i<strlen(logInData.szUserName); i++)
		{
			logInData.szUserName[i] += PASSWORD_OFFSET;
		}
		for(i =0; i<strlen(logInData.szDescription); i++)
		{
			logInData.szDescription[i] += PASSWORD_OFFSET;
		}
		for(i =0; i<strlen(logInData.szRegistedDate); i++)
		{
			logInData.szRegistedDate[i] += PASSWORD_OFFSET;
		}
		fclose(fp);
	}
	return logInData;
}

EPDLL_API BOOL WriteLastLogID(STR_LOGIN logData)
{
	if(szSettingFileName == NULL)	return FALSE;

	FILE *fp = fopen(szSettingFileName, "rb+");
	unsigned int i = 0;
	if(fp != NULL)
	{
		fseek(fp, sizeof(STR_TOP_CONFIG)*2, SEEK_SET);
			
		for(i =0; i<strlen(logData.szLoginID); i++)
		{
			logData.szLoginID[i] -= PASSWORD_OFFSET;
		}
		for(i =0; i<strlen(logData.szPassword); i++)
		{
			logData.szPassword[i] -= PASSWORD_OFFSET;
		}
		for(i =0; i<strlen(logData.szUserName); i++)
		{
			logData.szUserName[i] -= PASSWORD_OFFSET;
		}
		for(i =0; i<strlen(logData.szDescription); i++)
		{
			logData.szDescription[i] -= PASSWORD_OFFSET;
		}
		for(i =0; i<strlen(logData.szRegistedDate); i++)
		{
			logData.szRegistedDate[i] -= PASSWORD_OFFSET;
		}

		if(fwrite(&logData, sizeof(logData), 1, fp) < 1)
		{
			fclose(fp);
			return FALSE;
		}
		fclose(fp);
		return TRUE;
	}
	return FALSE;
}

//0x00, 0x40~5F
EPDLL_API BOOL IsNormalCell(WORD code)
{
	if(code == EP_CODE_NORMAL || ( code >= EP_NORMAL_CODE_LOW && code <= EP_NORMAL_CODE_HIGH))	return TRUE;
	
	return FALSE;
}

//0x04, 0x60~7f
EPDLL_API BOOL IsCellCheckFail(WORD code)
{
	if(code == EP_CODE_CELL_CHECK_FAIL || ( code >= EP_NONCELL_CODE_LOW && code <= EP_NONCELL_CODE_HIGH))	return TRUE;
	
	return FALSE;
}

//0x03, 0x80~CF
EPDLL_API BOOL IsFailCell(WORD code)
{
	if(code == EP_CODE_CELL_FAIL || ( code >= EP_CELL_FAIL_CODE_LOW && code <= EP_CELL_FAIL_CODE_HIGH))	return TRUE;
	
	return FALSE;
}

//0x01
EPDLL_API BOOL IsNonCell(WORD code)
{
	return code == EP_CODE_NONCELL ? TRUE : FALSE;
}

//0x02, 0xD0~0xff
EPDLL_API BOOL IsSysFail(WORD code)
{
	if(code == EP_CODE_SYS_ERROR || ( code >= EP_SYS_FAIL_CODE_LOW && code <= EP_SYS_FAIL_CODE_HIGH))	return TRUE;
	
	return FALSE;

}

EPDLL_API BOOL IsSelectNormalCell (WORD code)
{
	if(code >= EP_SELECT_CODE_LOW && code <= EP_SELECT_CODE_HIGH)
	{
		return FALSE;
	}
	return TRUE;
}

EPDLL_API BOOL IsSelectBadCell (WORD code)
{
	if(code >= EP_SELECT_CODE_LOW && code <= EP_SELECT_CODE_HIGH)		//NonCell을 포함 한다.
	{
		return TRUE;
	}

	return FALSE;
}

EPDLL_API CString GetSelectCodeName(WORD code)
{
	CString codeString;
	switch(code)
	{
//	case EP_SELECT_CODE_NORMAL			: codeString = "";			break;
	case EP_SELECT_CODE_NONCELL			: codeString = "[X]";		break;
	case EP_SELECT_CODE_SYS_ERROR		: codeString = "[S]";		break;
	case EP_SELECT_CODE_SAFETY_FAULT	: codeString = "[N]";		break;
	case EP_SELECT_CODE_CELL_CHECK_FAIL	: codeString = "[K]";		break;
	case EP_SELECT_CODE_DELTA_OCV1		: codeString = "[DO]";		break;
	case EP_SELECT_CODE_DELTA_OCV2		: codeString = "[DO]";		break;
	case EP_SELECT_CODE_OCV_FAIL1		: codeString = "[O1]";		break;
	case EP_SELECT_CODE_OCV_FAIL4		: codeString = "[O2]";		break;
	case EP_SELECT_CODE_USER_CODE		: codeString = "[U]";		break;
	case EP_SELECT_CODE_VTG_FAIL		: codeString = "[V]";		break;
	case EP_SELECT_CODE_CRT_FAIL		: codeString = "[I]";		break;
	case EP_SELECT_CODE_CAP_FAIL		: codeString = "[C]";		break;
	case EP_SELECT_CODE_IMPEDANCE		: codeString = "[Z]";		break;
	case EP_SELECT_CODE_NO_GRADE		: codeString = " ";			break;
	default:
		codeString.Format("%c", code);
		break;
	}
	return codeString;
}
//
EPDLL_API CString EmgMsg(int nCode)
{
	CString strMsg;
	switch(nCode)
	{
	case EP_EMG_NORMAL:				strMsg = "정상";				break;
	case EP_EMG_UPS_POWER:			strMsg = "정전 발생";			break;
	case EP_EMG_UPS_BATTERY_FAIL:	strMsg = "UPS Battery Fail";	break;
	case EP_EMG_MAIN_EMG_SWITCH	:	strMsg = "비상 정지 Switch 동작";	break;
	case EP_EMG_SUB_EMG_SWITCH:		strMsg = "보조 비상 정지 Switch 동작";	break;
	case EP_EMG_TEMP_LIMIT:			strMsg = "설정값 보다 높은 온도 감지";		break;
	case EP_EMG_TEMP_DIFF_LIMIT:	strMsg = "온도 분포가 설정값 보다 크게 발생";	break;
	case EP_EMG_SMOKE_LEAK:			strMsg = "가스 탐지";		break;
	case EP_EMG_NETWORK_ERROR:		strMsg = "통신 장애";		break;
	case EP_EMG_POWER_SWITCH:		strMsg = "종료";			break;
	case EP_EMG_DVM_COMM_ERROR:		strMsg = "계측기 통신장애";			break;
	case EP_EMG_CALIBRATOR_COMM_ERROR:	strMsg = " 캘리브레이션 B/D 통신장애";		break;
	case EP_EMG_CPU_WATCHDOG:			strMsg = "제어 컴퓨터 이상";		break;
	case EP_EMG_MODULE_WATCHDOG:		strMsg = "Module 상태 이상";		break;
	case EP_EMG_MAINCYL_UP_TIMEOUT:		strMsg = "Jig를 올릴 수 없습니다.(공기압 점검)";		break;
	case EP_EMG_GRIPCYL_OPEN_TIMEOUT:	strMsg = "핀을 Open 시킬수 없습니다.";		break;
	case EP_EMG_GRIPCYL_CLOSE_TIMEOUT:	strMsg = "핀을 Close 시킬수 없습니다.";		break;
	case EP_EMG_MAINCYL_DOWN_TIMEOUT:	strMsg = "Jig를 내릴 수 없습니다.";			break;
	case EP_EMG_GRIPCYL_RO_SENS	:		strMsg = "핀 Open 오른쪽 Sensor 이상";		break;
	case EP_EMG_GRIPCYL_RC_SENS:		strMsg = "핀 Close 오른쪽 Sensor 이상";		break;
	case EP_EMG_GRIPCYL_LO_SENS	:		strMsg = "핀 Open 왼쪽 Sensor 이상";		break;
	case EP_EMG_GRIPCYL_LC_SENS:		strMsg = "핀 Close 왼쪽 Sensor 이상";		break;
	case EP_EMG_MAINCYL_U_SENS:			strMsg = "Jig Up Sensor 이상";		break;
	case EP_EMG_MAINCYL_L_SENS:			strMsg = "Jig Down Sensor 이상";	break;
	case EP_EMG_AIR_PRESS:				strMsg = "공기압이 낮음";			break;
	case EP_EMG_NVRAM_COMM_ERROR:		strMsg = "Tray 정보를 인식할 수 없음";		break;
	case EP_EMG_DOOR_OPEN:				strMsg = "Door Open";				break;
	case EP_EMG_MAX_FAULT:				strMsg = "최대 오류수 초과 ";		break;
	case EP_EMG_UPPER_TEMP:				strMsg = "온도 상한";				break;
	case EP_EMG_LOWER_TEMP:				strMsg = "온도 하한";				break;
	case EP_EMG_ADC_READ_FAIL:			strMsg = "전류 Read(ADC) 오류";		break;
		
	default:	strMsg.Format("Code %d", nCode);	break;
	}
	return strMsg;
}

//For LG
//DB로 이동

EPDLL_API CString ModuleFailMsg(int nCode)
{
	CString strMsg;
	
	switch(nCode)
	{
	case EP_FAIL_TERMINAL_QUIT	: strMsg = "모듈 단말기에서 멈춤";			break;//0x01
	case EP_FAIL_TERMINAL_HALT	: strMsg = "모듈 단말기에서 종료 시킴";		break;//0x02
	case EP_FAIL_NET_CMD_EXIT	: strMsg = "종료 명령 수신";				break;//0x03
	case EP_FAIL_POWER_SWITCH	: strMsg = "Power Switch 눌림";				break;//0x04
	case EP_FAIL_AC_POWER1		: strMsg = "입력 AC 전원 이상1";			break;//0x05
	case EP_FAIL_AC_POWER2		: strMsg = "입력 AC 전원 이상2";			break;//0x06
	case EP_FAIL_UPS_BATTERY	: strMsg = "UPS Battery 이상";				break;//0x07
	case EP_FAIL_MAIN_EMG		: strMsg = "비상 정지 Switch 동작";			break;//0x08
	case EP_FAIL_SUB_EMG		: strMsg = "보조 비상 정지 Switch 동작";	break;//0x09
	case EP_FAIL_CPU_WATCHDOG	: strMsg = "제어 컴퓨터 이상";				break;//0x0A
	case EP_FAIL_MAIN_BD_ADC	: strMsg = "Main Board Sensing Read(ADC) 오류";		break;//0x0B
	case EP_FAIL_MAIN_BD_HW		: strMsg = "Main Board 오류 발생";			break;//0x0C
	case EP_FAIL_MAIN_BD_TEMP_UPPER	: strMsg = "Board 온도 상한 감지";		break;//0x0D
	case EP_FAIL_NETWORK_ERROR	: strMsg = "통신 이상";						break;//0x0E
	case EP_FAIL_DVM_COMM_ERROR	: strMsg = "계측기 통신장애";				break;//0x0F
	case EP_FAIL_CAL_COMM_ERROR	: strMsg = "캘리브레이션 B/D 통신장애";		break;//0x10
	case EP_FAIL_OVP			: strMsg = "과전압 안전 발생";				break;//0x11
	case EP_FAIL_OCP			: strMsg = "과전류 안전 발생";				break;//0x12
	case EP_FAIL_UPPER_VOLTAGE	: strMsg = "과전압 감지";					break;//0x14
	case EP_FAIL_RUN_TIME_OVER	: strMsg = "동작 시간 경과";				break;//0x15
	
	case EP_FAIL_MAX_FAULT		: strMsg = "최대 불량수 초과";				break;//0x20

	case EP_FAIL_MAINCYL_UP_TIME_OVER	: strMsg = "Jig 상승 시간 초과";	break;//0x40
	case EP_FAIL_MAINCYL_DWON_TIME_OVER	: strMsg = "Jig 하강 시간 초과";	break;//0x41
	case EP_FAIL_MAINCYL_UP_SENS	: strMsg = "Jig Up 감지 이상";			break;//0x42
	case EP_FAIL_MAINCYL_DOWN_SENS	: strMsg = "Jig Down 감지 이상";		break;//0x43
	case EP_FAIL_MAINCYL_RU_SENS	: strMsg = "Jig 오른쪽 Up 감지 이상";	break;//0x44
	case EP_FAIL_MAINCYL_LU_SENS	: strMsg = "Jig 왼쪽 Up 감지 이상";		break;//0x45
	case EP_FAIL_MAINCYL_RD_SENS	: strMsg = "Jig 오른쪽 Down 감지 이상";	break;//0x46
	case EP_FAIL_MAINCYL_LD_SENS	: strMsg = "Jig 왼쪽 Down 감지 이상";	break;//0x47
	case EP_FAIL_LATCHCYL_OPEN_TIME_OVER	: strMsg = "Latch Open 시간 초과";	break;//0x48
	case EP_FAIL_LATCHCYL_CLOSE_TIME_OVER	: strMsg = "Latch Close 기간 초과";	break;//0x49
	case EP_FAIL_LATCHCYL_RC_SENS	: strMsg = "Latch 오른쪽 Close 감지 이상";	break;//0x4A
	case EP_FAIL_LATCHCYL_RO_SENS	: strMsg = "Latch 오른쪽 Open 감지 이상";	break;//0x4B
	case EP_FAIL_LATCHCYL_LC_SENS	: strMsg = "Latch 왼쪽 Close 감지 이상";	break;//0x4C
	case EP_FAIL_LATCHCYL_LO_SENS	: strMsg = "Latch 왼쪽 Open 감지 이상";		break;//0x4D
	case EP_FAIL_GRIPCYL_OPEN_TIME_OVER		: strMsg = "Gripper Open 시간 초과";	break;//0x4E
	case EP_FAIL_GRIPCYL_CLOSE_TIME_OVER	: strMsg = "Gripper Close 시간 초과";	break;//0x4F
	case EP_FAIL_GRIPCYL_RO_SENS	: strMsg = "Gripper 오른쪽 Open 감지 이상";		break;//0x50
	case EP_FAIL_GRIPCYL_RC_SENS	: strMsg = "Gripper 오른쪽 Close 감지 이상";	break;//0x51
	case EP_FAIL_GRIPCYL_LC_SENS	: strMsg = "Gripper 왼쪽 Open 감지 이상";		break;//0x52
	case EP_FAIL_GRIPCYL_LO_SENS	: strMsg = "Gripper 왼쪽 Close 감지 이상";		break;//0x53
	case EP_FAIL_TRAY_SENS			: strMsg = "Tray 감지 이상";				break;//0x54
	case EP_FAIL_TRAY_DIR_SENS		: strMsg = "Tray 방향 감지 이상";			break;//0x55
	case EP_FAIL_DOOR_OPEN			: strMsg = "전면 도어 열림";				break;//0x56
	case EP_FAIL_SMOKE_LEAK			: strMsg = "연기 감지";						break;//0x57
	case EP_FAIL_AIR_PRESS			: strMsg = "공기압 이상";					break;//0x58
	case EP_FAIL_TEMP_UPPER			: strMsg = "내부 온도 상한 감지";			break;//0x59
	case EP_FAIL_DIFF_TEMP_UPPER	: strMsg = "내부 온도 분포 이상";			break;//0x5A
	case EP_FAIL_NVRAM_COMM_ERROR	: strMsg = "NVRAM 통신 장애";				break;//0x5B
	case EP_FAIL_CRAIN_ERROR		: strMsg = "Crain 이상";					break;//0x5C
	default:   strMsg.Format("Code 0x%x", nCode);								break;
	}
	return strMsg;
}

EPDLL_API CString DoorStateMsg(WORD State)
{
	CString strMsg;
	switch (State)
	{
	case EP_DOOR_NONE:		strMsg.LoadString(IDS_TEXT_NONE);		break;
	case EP_DOOR_OPEN:		strMsg.LoadString(IDS_TEXT_OPEN);		break;
	case EP_DOOR_MOVE:		strMsg.LoadString(IDS_TEXT_RUN);		break;
	case EP_DOOR_CLOSE:		strMsg.LoadString(IDS_TEXT_CLOSE);		break;
	case EP_DOOR_ERROR:
	default:				strMsg.LoadString(IDS_TEXT_ERROR);		break;
	}
	return strMsg;
}

EPDLL_API CString TrayStateMsg(WORD State)
{
	CString strMsg;
	switch (State)
	{
		case EP_TRAY_NONE:		strMsg.LoadString(IDS_TEXT_NONE);		break;
		case EP_TRAY_UNLOAD:	strMsg.LoadString(IDS_TEXT_UNLOAD);		break;
		case EP_TRAY_MOVE:		strMsg.LoadString(IDS_TEXT_MOVING);			break;
		case EP_TRAY_LOAD:		strMsg.LoadString(IDS_TEXT_LOAD);			break;
		case EP_TRAY_ERROR:		
		default:				strMsg.LoadString(IDS_TEXT_ERROR);		break;
	}
	return strMsg;
}

EPDLL_API CString JigStateMsg(WORD state)
{
	CString strMsg;
	switch (state)
	{
		case EP_JIG_NONE:		strMsg.LoadString(IDS_TEXT_NONE);		break;
		case EP_JIG_UP:			strMsg.LoadString(IDS_TEXT_UP);			break;
		case EP_JIG_MOVE_DOWN:	strMsg.LoadString(IDS_TEXT_GOING_DOWN);	break;
		case EP_JIG_DOWN:		strMsg.LoadString(IDS_TEXT_DOWN);			break;
		case EP_JIG_MOVE_UP:	strMsg.LoadString(IDS_TEXT_GOING_UP);	break;
		case EP_JIG_ERROR:
		default:				strMsg.LoadString(IDS_TEXT_ERROR);		break;
	}
	return strMsg;
}

EPDLL_API CString StepTypeMsg(int nType)
{
	CString strMsg;
	switch (nType)
	{
		case EP_TYPE_CHARGE:		strMsg.LoadString(IDS_TEXT_CHARGE);		break;
		case EP_TYPE_DISCHARGE:		strMsg.LoadString(IDS_TEXT_DISCHARGE);		break;
		case EP_TYPE_REST:			strMsg.LoadString(IDS_TEXT_REST);		break;
		case EP_TYPE_OCV:			strMsg.LoadString(IDS_TEXT_OCV);		break;
		case EP_TYPE_IMPEDANCE:		strMsg.LoadString(IDS_TEXT_IMPEDANCE);		break;
		case EP_TYPE_END:			strMsg.LoadString(IDS_TEXT_END);		break;	
		case EP_TYPE_LOOP:			strMsg.LoadString(IDS_TEXT_LOOP);		break;
		default:					strMsg.Format("%d", nType);		break;
	}
	return strMsg;
}

EPDLL_API CString ModeTypeMsg( int nType, int nMode)
{
	CString strMsg;
	if(nType == EP_TYPE_CHARGE || nType == EP_TYPE_DISCHARGE)
	{
		switch (nMode)
		{
			case EP_MODE_CCCV:		strMsg = "CC/CV";	break;
			case EP_MODE_CC:		strMsg = "CC";		break;
			case EP_MODE_CV:		strMsg = "CV";		break;
			case EP_MODE_DC_IMP	:	strMsg = "DC";		break;
			case EP_MODE_AC_IMP	:	strMsg = "AC";		break;
			case EP_MODE_CP		:	strMsg = "CP";		break;
			case EP_MODE_PULSE	:	strMsg = "Pulse";	break;
			case EP_MODE_CR		:	strMsg = "CR";		break;
			default:				strMsg.Format("%d", nMode);		break;
		}
	}
	else if(nType == EP_TYPE_IMPEDANCE)
	{
		switch (nMode)
		{
			case EP_MODE_DC_IMP:		strMsg = "DC IMP";	break;
			case EP_MODE_AC_IMP:		strMsg = "AC IMP";	break;
			default:					strMsg.Format("%d", nMode);		break;
		}

	}
	return strMsg;
}
