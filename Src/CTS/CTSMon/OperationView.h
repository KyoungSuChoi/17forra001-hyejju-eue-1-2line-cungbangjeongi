#pragma once
#include "afxwin.h"
#include "afxcmn.h"

#include "CTSMonDoc.h"
#include "ColorButton2.h"
// COperationView 폼 뷰입니다.

class COperationView : public CFormView
{
	DECLARE_DYNCREATE(COperationView)
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

protected:
	COperationView();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~COperationView();

public:
	enum { IDD = IDD_OPERATION_VIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

public:
	CCTSMonDoc* GetDocument();
	void InitLabel();
	void InitFont();
	void InitCombo();
	void InitColorBtn();
	void ModuleConnected(int nModuleID);
	void ModuleDisConnected(int nModuleID);
	void StartMonitoring();
	void DrawProcessInfo();
	void StopMonitoring();
	void InitStepList();
	void DrawStepList();
	void SetCurrentModule(int nModuleID, int nGroupIndex=0);
	CString GetTargetModuleName();
	void DrawChannel();	
	void UpdateGroupState(int nModuleID, int nGroupIndex=0 );
	void UpdateBtnEnable( bool bShow );		// Line on, Local 일 경우에만 버튼 활성화


public:
	CFont	m_Font;
	int		m_nPrevStep;
	int		m_nCurModuleID;
	STR_TOP_CONFIG		*m_pConfig;
	CImageList *m_pChStsSmallImage;
	UINT	m_nDisplayTimer;
		
	CString m_strPreTrayId;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	CLabel m_CmdTarget;
	CLabel m_LabelViewName;
	CListCtrl m_ctrlStepList;
	afx_msg void OnBnClickedRunBtn();
	afx_msg void OnBnClickedStopBtn();
	afx_msg void OnBnClickedContinueBtn();
//	afx_msg void OnBnClickedNextstepBtn();
	afx_msg void OnBnClickedPauseBtn();
	afx_msg void OnBnClickedStepoverBtn();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedAutoOperationBtn();
	afx_msg void OnBnClickedLocalOperationBtn();
	CLabel m_ctrlLineMode;
	afx_msg void OnNMDblclkStepList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnGetdispinfoStepList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedSendProcessBtn();
	afx_msg void OnBnClickedResultdataDispBtn();
	afx_msg void OnBnClickedContactdataDispBtn();
				 
	CLabel m_LabelProcessModel;
	CLabel m_LabelTypeSel;	//20201209 KSJ
	CLabel m_LabelProcessTest;	
	CLabel m_LabelBatch;
	CLabel m_LabelTrayId;	
	CColorButton2 m_Btn_Run;
	CColorButton2 m_Btn_Stop;
	CColorButton2 m_Btn_Continue;
	CColorButton2 m_Btn_StepOver;
	CColorButton2 m_Btn_Pause;
	CColorButton2 m_Btn_SendProcess;
	CColorButton2 m_Btn_ResultDataDisplay;
	CColorButton2 m_Btn_ContactResultDataDisplay;
	CColorButton2 m_Btn_OperationAuto;
	CColorButton2 m_Btn_OperationLocal;	
	long m_lModelId;
	long m_lTypeSel;	//20201209 ksj
	long m_lProcess;
	CString m_strBatch;
	CString m_strTrayId;
	CLabel m_Label1;
	CLabel m_Label2;
	CLabel m_Label3;
	CComboBox m_ctrlTypeSelCombo;
	CComboBox m_ctrlTabDeepthCombo;
	CComboBox m_ctrlTrayHeightCombo;
	CComboBox m_ctrlTrayTypeCombo;
	int m_nCurrentChCnt;
	CLabel m_Label4;
	afx_msg void OnEnChangeEditTrayid();
};

#ifndef _DEBUG  // debug version in TopView.cpp
inline CCTSMonDoc* COperationView::GetDocument()
{ return (CCTSMonDoc*)m_pDocument; }
#endif
