// TestListRecordSet.cpp : implementation file
//

#include "stdafx.h"
#include "TestListRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTestListRecordSet

IMPLEMENT_DYNAMIC(CTestListRecordSet, CDaoRecordset)

extern CString GetDataBaseName();


CTestListRecordSet::CTestListRecordSet(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CTestListRecordSet)
	m_TestID = 0;
	m_ModelID = 0;
	m_TestName = _T("");
	m_Description = _T("");
	m_Creator = _T("");
	m_ModifiedTime = (DATE)0;
	m_PreTestCheck = FALSE;
	m_ProcTypeID = 0;
	m_TestNo = 0;
	m_nFields = 9;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}

CString CTestListRecordSet::GetDefaultDBName()
{
	return _T(GetDataBaseName());
//	return _T("C:\\My Documents\\Condition.mdb");
}


CString CTestListRecordSet::GetDefaultSQL()
{
	return _T("[TestName]");
}

void CTestListRecordSet::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CTestListRecordSet)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Long(pFX, _T("[TestID]"), m_TestID);
	DFX_Long(pFX, _T("[ModelID]"), m_ModelID);
	DFX_Text(pFX, _T("[TestName]"), m_TestName);
	DFX_Text(pFX, _T("[Description]"), m_Description);
	DFX_Text(pFX, _T("[Creator]"), m_Creator);
	DFX_DateTime(pFX, _T("[ModifiedTime]"), m_ModifiedTime);
	DFX_Bool(pFX, _T("[PreTestCheck]"), m_PreTestCheck);
	DFX_Long(pFX, _T("[ProcTypeID]"), m_ProcTypeID);
	DFX_Long(pFX, _T("[TestNo]"), m_TestNo);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CTestListRecordSet diagnostics

#ifdef _DEBUG
void CTestListRecordSet::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CTestListRecordSet::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
