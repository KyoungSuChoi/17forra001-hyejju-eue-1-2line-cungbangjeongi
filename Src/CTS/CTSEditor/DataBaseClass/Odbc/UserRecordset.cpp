// UserRecordset.cpp : implementation file
//

#include "stdafx.h"
#include "UserRecordset.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUserRecordset

IMPLEMENT_DYNAMIC(CUserRecordset, CRecordset)

CUserRecordset::CUserRecordset(CDatabase* pdb)
	: CRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CUserRecordset)
	m_Index = 0;
	m_UserID = _T("");
	m_Password = _T("");
	m_Name = _T("");
	m_Authority = 0;
	m_Description = _T("");
	m_AutoLogOut = FALSE;
	m_AutoLogOutTime = 0;
	m_RegistedDate = CTime::GetCurrentTime();
	m_nFields = 9;
	//}}AFX_FIELD_INIT
	m_nDefaultType = snapshot;
}


CString CUserRecordset::GetDefaultConnect()
{
	return _T("ODBC;DSN=TestCondition");
}

CString CUserRecordset::GetDefaultSQL()
{
	return _T("[User]");
}

void CUserRecordset::DoFieldExchange(CFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CUserRecordset)
	pFX->SetFieldType(CFieldExchange::outputColumn);
	RFX_Long(pFX, _T("[Index]"), m_Index);
	RFX_Text(pFX, _T("[UserID]"), m_UserID);
	RFX_Text(pFX, _T("[Password]"), m_Password);
	RFX_Text(pFX, _T("[Name]"), m_Name);
	RFX_Long(pFX, _T("[Authority]"), m_Authority);
	RFX_Date(pFX, _T("[RegistedDate]"), m_RegistedDate);
	RFX_Text(pFX, _T("[Description]"), m_Description);
	RFX_Bool(pFX, _T("[AutoLogOut]"), m_AutoLogOut);
	RFX_Long(pFX, _T("[AutoLogOutTime]"), m_AutoLogOutTime);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CUserRecordset diagnostics

#ifdef _DEBUG
void CUserRecordset::AssertValid() const
{
	CRecordset::AssertValid();
}

void CUserRecordset::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG
