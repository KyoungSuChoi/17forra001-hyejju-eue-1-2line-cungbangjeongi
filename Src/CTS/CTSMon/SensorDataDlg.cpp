// SensorDataDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSMon.h"
#include "SensorDataDlg.h"

#include "SensorMapDlg.h"
#include "MainFrm.h"
#include "PrntScreen.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#define TEMP_PROGRESS_COLOR	RGB(100, 100, 200)
#define GAS_PROGRESS_COLOR	RGB(240, 100, 0)

/////////////////////////////////////////////////////////////////////////////
// CSensorDataDlg dialog


CSensorDataDlg::CSensorDataDlg(CCTSMonDoc *pDoc, CWnd* pParent /*=NULL*/)
	: CDialog(CSensorDataDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSensorDataDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_nModuleID = 0;
	m_pDoc = pDoc;
	ASSERT(m_pDoc);
	m_bLockUpdate = FALSE;	
	LanguageinitMonConfig();
}


CSensorDataDlg::~CSensorDataDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CSensorDataDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CSensorDataDlg"), _T("TEXT_CSensorDataDlg_CNT"), _T("TEXT_CSensorDataDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CSensorDataDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CSensorDataDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}



void CSensorDataDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSensorDataDlg)
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDC_EXCEL_SAVE_BUTTON, m_btnExcel);
	DDX_Control(pDX, IDC_PRINT_BUTTON, m_btnPrint);
	DDX_Control(pDX, IDC_MAP_BUTTON, m_btnMap);
	DDX_Control(pDX, IDC_MODULE_SEL_COMBO, m_ctrlMDSelCombo);
	DDX_Control(pDX, IDC_GAS_MIN, m_ctrlGasMin);
	DDX_Control(pDX, IDC_GAS_MAX, m_ctrlGasMax);
	DDX_Control(pDX, IDC_TEMP_MIN, m_ctrlTempMin);
	DDX_Control(pDX, IDC_TEMP_MAX, m_ctrlTempMax);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSensorDataDlg, CDialog)
	//{{AFX_MSG_MAP(CSensorDataDlg)
	ON_WM_TIMER()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_MAP_BUTTON, OnMapButton)
	ON_CBN_SELCHANGE(IDC_MODULE_SEL_COMBO, OnSelchangeModuleSelCombo)
	ON_BN_CLICKED(IDC_PRINT_BUTTON, OnPrintButton)
	ON_BN_CLICKED(IDC_EXCEL_SAVE_BUTTON, OnExcelSaveButton)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSensorDataDlg message handlers

BOOL CSensorDataDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_strModuleName = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Module Name", "Rack");	//Get Current Folder(CTSMon Folde)

	CString strTemp;
	int nModuleID;
	
	m_ctrlMDSelCombo.AddString("== All ==");
	m_ctrlMDSelCombo.SetItemData(0, 0);

	int nDefaultIndex = 0;
	int nCount = 1;
	for(int i=0; i<m_nInstallModuleNum; i++)
	{
		nModuleID = EPGetModuleID(i);
		if(nModuleID == m_nModuleID)
		{
			nDefaultIndex = nCount;
		}
		m_ctrlMDSelCombo.AddString(::GetModuleName(nModuleID));
		m_ctrlMDSelCombo.SetItemData(nCount++, nModuleID);
	}
	m_ctrlMDSelCombo.SetCurSel(nDefaultIndex);
	
	InitTempGrid();

	if(m_nModuleID > 0)
	{
		GetDlgItem(IDC_MAP_BUTTON)->EnableWindow(TRUE);
	}
	else
	{
		GetDlgItem(IDC_MAP_BUTTON)->EnableWindow(FALSE);
	}
	UpdateGridData();
	SetTimer(500, 5000, NULL);		//Value Update Timer
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSensorDataDlg::InitTempGrid()
{
	m_wndTempGrid.SubclassDlgItem(IDC_TEMP_GRID, this);
	m_wndTempGrid.m_bSameColSize  = FALSE;
	m_wndTempGrid.m_bSameRowSize  = FALSE;
	m_wndTempGrid.m_bCustomWidth  = TRUE;

	m_wndTempGrid.Initialize();
	BOOL bLock = m_wndTempGrid.LockUpdate();
	m_wndTempGrid.SetColCount(9);
	m_wndTempGrid.m_BackColor	= RGB(255,255,255);

	m_wndTempGrid.SetValueRange(CGXRange(0,1), m_strModuleName);
	m_wndTempGrid.SetValueRange(CGXRange(0,2), "Sensor");
	m_wndTempGrid.SetValueRange(CGXRange(0,3), ::GetStringTable(IDS_LABEL_CUR_VAL));
	m_wndTempGrid.SetValueRange(CGXRange(0,4), ::GetStringTable(IDS_LABEL_MAX));
	m_wndTempGrid.SetValueRange(CGXRange(0,5), ::GetStringTable(IDS_LABEL_MIN));
	m_wndTempGrid.SetValueRange(CGXRange(0,6), ::GetStringTable(IDS_LABEL_USE_WARRING));
	m_wndTempGrid.SetValueRange(CGXRange(0,7), ::GetStringTable(IDS_LABLE_WARRING_VAL));
	m_wndTempGrid.SetValueRange(CGXRange(0,8), "SernsorNo");
	m_wndTempGrid.SetValueRange(CGXRange(0,9), TEXT_LANG[0]);//"Sensor종류"

	CRect rectGrid;
	float width;
	m_wndTempGrid.GetClientRect(rectGrid);
	width = (float)rectGrid.Width()/100.0f;
				
	m_wndTempGrid.m_nWidth[1]	= int(width* 15.0f);
	m_wndTempGrid.m_nWidth[2]	= int(width* 10.0f);
	m_wndTempGrid.m_nWidth[3]	= int(width* 25.0f);
	m_wndTempGrid.m_nWidth[4]	= int(width* 25.0f);
	m_wndTempGrid.m_nWidth[5]	= int(width* 25.0f);
	m_wndTempGrid.m_nWidth[6]	= 0;//int(width* 10.0f);
	m_wndTempGrid.m_nWidth[7]	= 0;//int(width* 15.0f);
	m_wndTempGrid.m_nWidth[8]	= 0;//int(width* 15.0f);
	m_wndTempGrid.m_nWidth[9]	= 0;//int(width* 15.0f);

	m_wndTempGrid.SetDefaultRowHeight(20);
	m_wndTempGrid.GetParam()->GetProperties()->SetDisplayHorzLines(TRUE);
	m_wndTempGrid.GetParam()->GetProperties()->SetDisplayVertLines(TRUE);
	m_wndTempGrid.SetStyleRange(CGXRange().SetTable(), CGXStyle().SetDraw3dFrame(gxFrameNormal));
	m_wndTempGrid.SetStyleRange(CGXRange().SetCols(6), 
			CGXStyle()
			.SetControl(GX_IDS_CTRL_CHECKBOX3D)
		  );

	ReDrawGrid();

	m_wndTempGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	m_wndTempGrid.SetScrollBarMode(SB_HORZ, gxnDisabled);
	m_wndTempGrid.EnableCellTips();
	
	m_wndTempGrid.LockUpdate(bLock);
	m_wndTempGrid.Redraw();
}

void CSensorDataDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	UpdateGridData();
	CDialog::OnTimer(nIDEvent);
}

void CSensorDataDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	if(::IsWindow(this->GetSafeHwnd()))
	{
		RECT rect;
		::GetClientRect(m_hWnd, &rect);

		CRect rectGrid;
		float width;
		if(m_wndTempGrid.GetSafeHwnd())
		{
			m_wndTempGrid.GetWindowRect(rectGrid);	//Step Grid Window Position
			ScreenToClient(&rectGrid);
			m_wndTempGrid.MoveWindow(5, rectGrid.top, rect.right-10, rect.bottom-rectGrid.top-5, FALSE);
			
			m_wndTempGrid.GetClientRect(rectGrid);
			width = (float)rectGrid.Width()/100.0f;
				
			m_wndTempGrid.m_nWidth[1]	= int(width* 15.0f);
			m_wndTempGrid.m_nWidth[2]	= int(width* 10.0f);
			m_wndTempGrid.m_nWidth[3]	= int(width* 25.0f);
			m_wndTempGrid.m_nWidth[4]	= int(width* 25.0f);
			m_wndTempGrid.m_nWidth[5]	= int(width* 25.0f);
			m_wndTempGrid.m_nWidth[6]	= 0;//int(width* 10.0f);
			m_wndTempGrid.m_nWidth[7]	= 0;//int(width* 15.0f);
			m_wndTempGrid.m_nWidth[8]	= 0;//int(width* 15.0f);
			m_wndTempGrid.m_nWidth[9]	= 0;//int(width* 15.0f);
			Invalidate();
		}
	}
}	

void CSensorDataDlg::UpdateGridData()
{
	if(m_bLockUpdate)	return;

	EP_GP_DATA gpData;
	CString strTemp;
	long moduleID;

	long lTempMax = -10e8, lTempMin = 10e8, lTempMaxModuleID = 0, lTempMinModuleID = 0;
	long lGasMax = -10e8, lGasMin = 10e8, lGasMaxModuleID = 0, lGasMinModuleID = 0;

	int nCount = 0, nCout1 = 0;
	int nRow;
	int nRowCount = m_wndTempGrid.GetRowCount();
	int nSenorNo, nSensorItem;
	CFormModule *pModule;

	int i = 0;

	//최대 최소값을 구한다.
	for(i =0; i<nRowCount; i++)
	{
		moduleID = atol(m_wndTempGrid.GetValueRowCol(i+1, 0));
		nSenorNo = atol(m_wndTempGrid.GetValueRowCol(i+1, 8));
		nSensorItem = atol(m_wndTempGrid.GetValueRowCol(i+1, 9))-1;
		gpData = EPGetGroupData(moduleID, 0);
	
		if(nSenorNo<=0 || nSenorNo > EP_MAX_SENSOR_CH)	continue;
		int j = nSenorNo-1;
		if(gpData.gpState.state != EP_STATE_LINE_OFF)
		{
			if(nSensorItem == CFormModule::sensorType1)
			{
				if(lTempMax < gpData.sensorData.sensorData1[j].lData)	
				{
					lTempMaxModuleID =  moduleID;
					lTempMax = gpData.sensorData.sensorData1[j].lData;
				}

				if(lTempMin > gpData.sensorData.sensorData1[j].lData)
				{
					lTempMinModuleID = moduleID;
					lTempMin = gpData.sensorData.sensorData1[j].lData;
				}
				nCount++;
			}
			else if(nSensorItem == CFormModule::sensorType2)
			{
				if(lGasMax < gpData.sensorData.sensorData2[j].lData)
				{
					lGasMaxModuleID = moduleID;
					lGasMax = gpData.sensorData.sensorData2[j].lData;
				}

				if(lGasMin > gpData.sensorData.sensorData2[j].lData)
				{
					lGasMinModuleID = moduleID;
					lGasMin = gpData.sensorData.sensorData2[j].lData;
				}
				nCout1++;
			}
		}
	}

	//표시할 data가 없을 경우 
	if(nCount <= 0)
	{
		lTempMax = 0; lTempMin = 0;
	}
	
	if(nCout1 <= 0)
	{
		lGasMax = 0; lGasMin = 0;
	}

	//Progress Control의 범위 설정을 위해 
	CString strMin, strMax;
	long nRngeMin, nRangeMax;

	if(lTempMin > 0)
	{
		nRngeMin = 0;		//0~Max Range
	}
	else
	{
		nRngeMin = lTempMin;	// - 온도 표시 
	}

	nRangeMax = ((lTempMax/5000)+1)*5000;	//50도 단위로 표시 

	strMin.Format("%d", int(ETC_PRECISION(nRngeMin)));
	strMax.Format("%d", int(ETC_PRECISION(nRangeMax)));	

	strTemp.Format("%s[%s'C~%s'C]", ::GetStringTable(IDS_LABEL_CUR_VAL), strMin, strMax);
	m_wndTempGrid.SetValueRange(CGXRange(0,3), strTemp);
	
	strTemp.Format("%s    %.1f 'C", ::GetModuleName(lTempMaxModuleID), ETC_PRECISION(lTempMax));
	m_ctrlTempMax.SetText(strTemp);
	strTemp.Format("%s    %.1f 'C", ::GetModuleName(lTempMinModuleID), ETC_PRECISION(lTempMin));
	m_ctrlTempMin.SetText(strTemp);
	strTemp.Format("%s    %s", ::GetModuleName(lGasMaxModuleID), m_pDoc->ValueString(lGasMax, EP_VOLTAGE, TRUE));
	m_ctrlGasMax.SetText(strTemp);
	strTemp.Format("%s    %s", ::GetModuleName(lGasMinModuleID), m_pDoc->ValueString(lGasMin, EP_VOLTAGE, TRUE));
	m_ctrlGasMin.SetText(strTemp);
	

	for(i =0; i<nRowCount; i++)
	{
		nRow = i+1;
		moduleID = atol(m_wndTempGrid.GetValueRowCol(nRow, 0));
		pModule = m_pDoc->GetModuleInfo(moduleID);
		nSenorNo = atol(m_wndTempGrid.GetValueRowCol(nRow, 8));
		nSensorItem = atol(m_wndTempGrid.GetValueRowCol(nRow, 9));
		gpData = EPGetGroupData(moduleID, 0);
		
		if(pModule == NULL)		continue;
		if(nSenorNo<=0 || nSenorNo > EP_MAX_SENSOR_CH)	continue;
		
		int j = nSenorNo-1;
		if(nSensorItem-1 == CFormModule::sensorType1)
		{
			strTemp.Format("%.2f", ETC_PRECISION(gpData.sensorData.sensorData1[j].lData));
			
			//Progess 구간을 현재 표시 data의 Min/Max로 조정한다.
			m_wndTempGrid.SetStyleRange(CGXRange(nRow, 3), 
					CGXStyle()
//					.SetControl(GX_IDS_CTRL_PROGRESS)
					.SetValue(ETC_PRECISION(gpData.sensorData.sensorData1[j].lData))
					.SetUserAttribute(GX_IDS_UA_PROGRESS_MIN, _T(strMin))
					.SetUserAttribute(GX_IDS_UA_PROGRESS_MAX, _T(strMax))
//					.SetUserAttribute(GX_IDS_UA_PROGRESS_CAPTIONMASK, _T("%ld 'C"))		// sprintf formatting for value
//					.SetTextColor(RGB(100, 100, 200))									// blue progress bar
//					.SetInterior(RGB(255, 255, 255))									// on white background
			  );

			strTemp.Format("%.1f 'C/ %s", ETC_PRECISION(gpData.sensorMinMax.sensorData1[j].lMax), gpData.sensorMinMax.sensorData1[j].szMaxDateTime);
			m_wndTempGrid.SetStyleRange(CGXRange(nRow, 4), 
				CGXStyle()
					.SetValue(strTemp)
					.SetTextColor(RGB(0, 0, 255))      // blue text
					.SetInterior(RGB(255, 255, 255))   // on white b
					);

			strTemp.Format("%.1f 'C/ %s", ETC_PRECISION(gpData.sensorMinMax.sensorData1[j].lMin), gpData.sensorMinMax.sensorData1[j].szMinDateTime);
			m_wndTempGrid.SetStyleRange(CGXRange(nRow, 5), 
				CGXStyle()
					.SetValue(strTemp)
					.SetTextColor(RGB(0, 0, 255))      // blue text
					.SetInterior(RGB(255, 255, 255))   // on white b
					);
		}
		else if(nSensorItem-1 == CFormModule::sensorType2)
		{
//			strTemp.Format("%s", m_pDoc->ValueString(gpData.sensorData.sensorData2[j].lData, EP_VOLTAGE));
			strTemp.Format("%d", (long)VTG_PRECISION(gpData.sensorData.sensorData2[j].lData));
			m_wndTempGrid.SetStyleRange(CGXRange(nRow, 3), 
				CGXStyle()
					.SetValue(strTemp)
		//			.SetTextColor(RGB(255, 0, 0))      // blue progress bar
		//			.SetInterior(RGB(255, 255, 255))   // on white b
					);

			//strTemp.Format("%s / %s", m_pDoc->ValueString(gpData.sensorMinMax.sensorData2[j].lMax, EP_VOLTAGE), gpData.sensorMinMax.sensorData2[j].szMaxDateTime);
			strTemp.Format("%d / %s", (long)VTG_PRECISION(gpData.sensorMinMax.sensorData2[j].lMax), gpData.sensorMinMax.sensorData2[j].szMaxDateTime);
			m_wndTempGrid.SetStyleRange(CGXRange(nRow, 4), 
				CGXStyle()
					.SetValue(strTemp)
					.SetTextColor(RGB(255, 0, 0))      // red text
					.SetInterior(RGB(255, 255, 255))   // on white b
					);

			//strTemp.Format("%s / %s", m_pDoc->ValueString(gpData.sensorMinMax.sensorData2[j].lMin, EP_VOLTAGE), gpData.sensorMinMax.sensorData2[j].szMinDateTime);
			strTemp.Format("%d / %s", (long)VTG_PRECISION(gpData.sensorMinMax.sensorData2[j].lMin), gpData.sensorMinMax.sensorData2[j].szMinDateTime);
			m_wndTempGrid.SetStyleRange(CGXRange(nRow, 5), 
				CGXStyle()
					.SetValue(strTemp)
					.SetTextColor(RGB(255, 0, 0))      // red text
					.SetInterior(RGB(255, 255, 255))   // on white b
					);
		}
	}
}

void CSensorDataDlg::OnOK() 
{
	// TODO: Add extra validation here				
	((CMainFrame *)AfxGetMainWnd())->m_bSensorData = FALSE;	
	CDialog::OnOK();
}

void CSensorDataDlg::OnMapButton() 
{
	// TODO: Add your control notification handler code here
	if(m_nModuleID < 1)	return;

	WORD state = EPGetGroupState(m_nModuleID,  0);
//ljb 임시
// 	if(state != EP_STATE_IDLE && state != EP_STATE_STANDBY && state != EP_STATE_READY && state != EP_STATE_END)
// 	{
// 		CString str;
// 		str.Format("%s는 현재 작업중이거나 설정을 변경할 수 없는 상태입니다.", ::GetModuleName(m_nModuleID));
// 		AfxMessageBox(str);
// 		SetForegroundWindow();
// 		return;
// 	}
	CSensorMapDlg dlg(m_pDoc, this);
	dlg.m_nModuleID = m_nModuleID;
	if(dlg.DoModal()==IDOK)
	{
		ReDrawGrid();
		UpdateGridData();
	}
	return;
}

void CSensorDataDlg::OnSelchangeModuleSelCombo() 
{
	// TODO: Add your control notification handler code here
	int nCurSel = m_ctrlMDSelCombo.GetCurSel();
	if(nCurSel == LB_ERR)	return;

	m_nModuleID = m_ctrlMDSelCombo.GetItemData(nCurSel);

	if(m_nModuleID > 0)
	{
		GetDlgItem(IDC_MAP_BUTTON)->EnableWindow(TRUE);
	}
	else
	{
		GetDlgItem(IDC_MAP_BUTTON)->EnableWindow(FALSE);
	}

	m_bLockUpdate = TRUE;
	ReDrawGrid();
	m_bLockUpdate = FALSE;

	UpdateGridData();
}

/*
void CSensorDataDlg::ReDrawGrid()
{
	int nDispModuleCount  = 1;
	if(m_nModuleID < 1)	//Display All Module
	{
		nDispModuleCount = m_nInstallModuleNum;
	}
	BOOL bLock = m_wndTempGrid.LockUpdate();

	if(m_wndTempGrid.GetRowCount() > 0)
		m_wndTempGrid.RemoveRows(1, m_wndTempGrid.GetRowCount());
	
	m_wndTempGrid.SetRowCount(nDispModuleCount*m_nDispSensorDataNo*DISPLAY_SENSOR_NO);

	CString strTemp;
	EP_SYSTEM_PARAM	*lpSysParam;
	int nModuleID =0;
	int nRow, nRow1 =0;

	for(int i=0; i<nDispModuleCount; i++)
	{
		if(m_nModuleID < 1)		
			nModuleID = EPGetModuleID(i);
		else					
			nModuleID = m_nModuleID;

		lpSysParam = EPGetSysParam(nModuleID);
		if(lpSysParam == NULL)	continue;

		for(int j =0; j<DISPLAY_SENSOR_NO; j++)
		{
			nRow = m_nDispSensorDataNo*j+i*(DISPLAY_SENSOR_NO*m_nDispSensorDataNo) +1;
			nRow1 = m_nDispSensorDataNo*(j+1)+i*(DISPLAY_SENSOR_NO*m_nDispSensorDataNo);

//			TRACE("Display Data nRow %d~%d\n", nRow, nRow1);

			m_wndTempGrid.SetCoveredCellsRowCol(nRow, 1, nRow1, 1);
			strTemp.Format("%s C%03d", ::GetModuleName(nModuleID), j+1);
			m_wndTempGrid.SetValueRange(CGXRange(nRow,1), strTemp);
			
			//Sensor Item1
			m_wndTempGrid.SetStyleRange(CGXRange(nRow, 2), 
					CGXStyle()
					.SetControl(GX_IDS_CTRL_PROGRESS)
					.SetValue(0L)
					.SetUserAttribute(GX_IDS_UA_PROGRESS_MIN, _T("0"))
					.SetUserAttribute(GX_IDS_UA_PROGRESS_MAX, _T("100"))
					.SetUserAttribute(GX_IDS_UA_PROGRESS_CAPTIONMASK, _T("%ld 'C"))		// sprintf formatting for value
					.SetTextColor(TEMP_PROGRESS_COLOR)									// blue progress bar
					.SetInterior(RGB(255, 255, 255))									// on white background
			  );


			m_wndTempGrid.SetStyleRange(CGXRange(nRow, 6), 
					CGXStyle()
					.SetControl(GX_IDS_CTRL_SPINEDIT)
					.SetUserAttribute(GX_IDS_UA_SPINBOUND_MIN, _T("0"))
					.SetUserAttribute(GX_IDS_UA_SPINBOUND_MAX, _T("100"))
					.SetUserAttribute(GX_IDS_UA_VALID_MIN, _T("0"))
					.SetUserAttribute(GX_IDS_UA_VALID_MAX, _T("100"))
			  );

			m_wndTempGrid.SetValueRange(CGXRange(nRow, 5), (long)lpSysParam->bUseTempLimit);
			strTemp.Format("%.1f 'C", ETC_PRECISION(lpSysParam->lMaxTemp));
			m_wndTempGrid.SetValueRange(CGXRange(nRow, 6), strTemp);

			//Sensor Item2
			if(m_nDispSensorDataNo > 1)
			{
				m_wndTempGrid.SetStyleRange(CGXRange(nRow+1, 2), 
						CGXStyle()
						.SetControl(GX_IDS_CTRL_PROGRESS)
						.SetValue(0L)
						.SetUserAttribute(GX_IDS_UA_PROGRESS_MIN, _T("0"))
						.SetUserAttribute(GX_IDS_UA_PROGRESS_MAX, _T("5000"))
						.SetUserAttribute(GX_IDS_UA_PROGRESS_CAPTIONMASK, _T("%ld"))  // sprintf formatting for value
						.SetTextColor(GAS_PROGRESS_COLOR)      // Red progress bar
						.SetInterior(RGB(255, 255, 255))   // on white background
				  );


				m_wndTempGrid.SetStyleRange(CGXRange(nRow+1, 6), 
						CGXStyle()
						.SetControl(GX_IDS_CTRL_SPINEDIT)
						.SetUserAttribute(GX_IDS_UA_SPINBOUND_MIN, _T("0"))
						.SetUserAttribute(GX_IDS_UA_SPINBOUND_MAX, _T("5000"))
						.SetUserAttribute(GX_IDS_UA_VALID_MIN, _T("0"))
						.SetUserAttribute(GX_IDS_UA_VALID_MAX, _T("5000"))
				  );

				m_wndTempGrid.SetValueRange(CGXRange(nRow+1, 5), (long)lpSysParam->bUseGasLimit);
				strTemp.Format("%d", (long)ETC_PRECISION(lpSysParam->lMaxGas));
				m_wndTempGrid.SetValueRange(CGXRange(nRow+1, 6), strTemp);

			}

			//Sensor Item3
			if(m_nDispSensorDataNo > 2)
			{
				//nRow+2
			}	
			//	:
			//	:
		}
		
		m_wndTempGrid.SetStyleRange(CGXRange(nRow1, 1, nRow1, 6), CGXStyle().SetBorders(gxBorderBottom, CGXPen().SetWidth(3).SetColor(RGB(0,0,0))));
	}

	m_wndTempGrid.LockUpdate(bLock);
	m_wndTempGrid.Redraw();

}
*/
void CSensorDataDlg::ReDrawGrid()
{
	int nDispModuleCount  = 1;
	CString strTemp;
	EP_SYSTEM_PARAM	*lpSysParam;
	int nModuleID =0;
	CFormModule *pModule;

	if(m_nModuleID < 1)	//Display All Module
	{
		nDispModuleCount = m_nInstallModuleNum;
	}

	BOOL bLock = m_wndTempGrid.LockUpdate();
	if(m_wndTempGrid.GetRowCount() > 0)
		m_wndTempGrid.RemoveRows(1, m_wndTempGrid.GetRowCount());
	
	_MAPPING_DATA *pMapData;
	int nRow = 0;
	for(int i=0; i<nDispModuleCount; i++)
	{
		if(m_nModuleID < 1)		
			nModuleID = EPGetModuleID(i);
		else					
			nModuleID = m_nModuleID;

		lpSysParam = EPGetSysParam(nModuleID);
		if(lpSysParam == NULL)	continue;

		pModule = m_pDoc->GetModuleInfo(nModuleID);
		for(int s =0; s<2; s++)
		{
			for(int j =0; j<EP_MAX_SENSOR_CH; j++)
			{
				pMapData = pModule->GetSensorMap(s, j);
				if(pMapData->nChannelNo >= 0)
				{
					m_wndTempGrid.InsertRows(++nRow, 1);					
					m_wndTempGrid.SetValueRange(CGXRange(nRow,0), (long)nModuleID);
					strTemp.Format("%s S%d-%02d", ::GetModuleName(nModuleID), s+1, j+1);
					m_wndTempGrid.SetValueRange(CGXRange(nRow,1), strTemp);
					m_wndTempGrid.SetValueRange(CGXRange(nRow,2), pMapData->szName);
					strTemp.Format("%d", j+1);
					m_wndTempGrid.SetValueRange(CGXRange(nRow,8), strTemp);
					strTemp.Format("%d", s+1);
					m_wndTempGrid.SetValueRange(CGXRange(nRow,9), strTemp);
				
					if(s == CFormModule::sensorType1)
					{
						//Sensor Item1
						m_wndTempGrid.SetStyleRange(CGXRange(nRow, 3), 
								CGXStyle()
								.SetControl(GX_IDS_CTRL_PROGRESS)
								.SetValue(0L)
								.SetUserAttribute(GX_IDS_UA_PROGRESS_MIN, _T("0"))
								.SetUserAttribute(GX_IDS_UA_PROGRESS_MAX, _T("100"))
								.SetUserAttribute(GX_IDS_UA_PROGRESS_CAPTIONMASK, _T("%ld 'C"))		// sprintf formatting for value
								.SetTextColor(TEMP_PROGRESS_COLOR)									// blue progress bar
								.SetInterior(RGB(255, 255, 255))									// on white background
						  );

						m_wndTempGrid.SetStyleRange(CGXRange(nRow, 7), 
								CGXStyle()
								.SetControl(GX_IDS_CTRL_SPINEDIT)
								.SetUserAttribute(GX_IDS_UA_SPINBOUND_MIN, _T("0"))
								.SetUserAttribute(GX_IDS_UA_SPINBOUND_MAX, _T("100"))
								.SetUserAttribute(GX_IDS_UA_VALID_MIN, _T("0"))
								.SetUserAttribute(GX_IDS_UA_VALID_MAX, _T("100"))
							 );
						
						m_wndTempGrid.SetValueRange(CGXRange(nRow, 6), (long)lpSysParam->bUseTempLimit);
						strTemp.Format("%.1f 'C", ETC_PRECISION(lpSysParam->sWanningTemp));
						m_wndTempGrid.SetValueRange(CGXRange(nRow, 7), strTemp);
					}
					else if(s == CFormModule::sensorType2)
					{
						//Sensor Item2
						m_wndTempGrid.SetStyleRange(CGXRange(nRow, 3), 
								CGXStyle()
								.SetControl(GX_IDS_CTRL_PROGRESS)
								.SetValue(0L)
								.SetUserAttribute(GX_IDS_UA_PROGRESS_MIN, _T("0"))
								.SetUserAttribute(GX_IDS_UA_PROGRESS_MAX, _T("5000"))
								.SetUserAttribute(GX_IDS_UA_PROGRESS_CAPTIONMASK, _T("%ld mV"))  // sprintf formatting for value
								.SetTextColor(GAS_PROGRESS_COLOR)      // Red progress bar
								.SetInterior(RGB(255, 255, 255))   // on white background
							);


						m_wndTempGrid.SetStyleRange(CGXRange(nRow, 7), 
								CGXStyle()
								.SetControl(GX_IDS_CTRL_SPINEDIT)
								.SetUserAttribute(GX_IDS_UA_SPINBOUND_MIN, _T("0"))
								.SetUserAttribute(GX_IDS_UA_SPINBOUND_MAX, _T("5000"))
								.SetUserAttribute(GX_IDS_UA_VALID_MIN, _T("0"))
								.SetUserAttribute(GX_IDS_UA_VALID_MAX, _T("5000"))
							);
						// m_wndTempGrid.SetValueRange(CGXRange(nRow, 6), (long)lpSysParam->bUseGasLimit);
						// strTemp.Format("%d", (long)ETC_PRECISION(lpSysParam->lMaxGas));
						// m_wndTempGrid.SetValueRange(CGXRange(nRow, 7), strTemp);
					}
				}	
			}
		}
		m_wndTempGrid.SetStyleRange(CGXRange(nRow, 1, nRow, 8), CGXStyle().SetBorders(gxBorderBottom, CGXPen().SetWidth(3).SetColor(RGB(0,0,0))));
	}

	m_wndTempGrid.LockUpdate(bLock);
	m_wndTempGrid.Redraw();

}

void CSensorDataDlg::OnPrintButton() 
{
	// TODO: Add your control notification handler code here
	 CPrntScreen * ScrCap;
     ScrCap = new CPrntScreen("Impossible to print!","Error!");
     ScrCap->DoPrntScreen(1,2,TRUE);
     delete ScrCap;
     ScrCap = NULL;		
}

void CSensorDataDlg::OnExcelSaveButton() 
{
	// TODO: Add your control notification handler code here
	CString strFileName;
	strFileName = "temperature.csv";

	CFileDialog pDlg(FALSE, "", strFileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, TEXT_LANG[1]);//"csv 파일(*.csv)|*.csv|"
	if(IDOK == pDlg.DoModal())
	{
		strFileName = pDlg.GetPathName();
		if(strFileName.IsEmpty())	return ;

		FILE *fp = fopen(strFileName, "wt");
		if(fp == NULL)	return ;

		BeginWaitCursor();
			
		for(int i=0; i< m_wndTempGrid.GetRowCount()+1; i++)
		{
			for(int j=1; j< m_wndTempGrid.GetColCount()+1; j++)
			{
				if( j > 1)
					fprintf(fp, ",");

				fprintf(fp, "%s", m_wndTempGrid.GetValueRowCol(i, j));
			}
			fprintf(fp, "\n");
		}

		EndWaitCursor();
		
		fclose(fp);
	}		
}
