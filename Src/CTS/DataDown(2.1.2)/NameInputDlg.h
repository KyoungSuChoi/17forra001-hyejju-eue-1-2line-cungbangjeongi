#if !defined(AFX_NAMEINPUTDLG_H__2C3C8218_3B5F_403B_A650_C8E91AD7432B__INCLUDED_)
#define AFX_NAMEINPUTDLG_H__2C3C8218_3B5F_403B_A650_C8E91AD7432B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NameInputDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CNameInputDlg dialog

class CNameInputDlg : public CDialog
{
// Construction
public:
	CString *TEXT_LANG;					
	bool LanguageinitMonConfig();

	CString m_strTitle;
	CNameInputDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CNameInputDlg();

// Dialog Data
	//{{AFX_DATA(CNameInputDlg)
	enum { IDD = IDD_NAME_DIALOG };
	CString	m_strName;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNameInputDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CNameInputDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NAMEINPUTDLG_H__2C3C8218_3B5F_403B_A650_C8E91AD7432B__INCLUDED_)
