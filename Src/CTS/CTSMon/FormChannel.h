// FormChannel.h: interface for the CFormChannel class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FORMCHANNEL_H__9D6E1988_DDF3_4E83_BD9C_80E879B48FED__INCLUDED_)
#define AFX_FORMCHANNEL_H__9D6E1988_DDF3_4E83_BD9C_80E879B48FED__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CFormChannel  
{
public:
	CFormChannel();
	virtual ~CFormChannel();

};

#endif // !defined(AFX_FORMCHANNEL_H__9D6E1988_DDF3_4E83_BD9C_80E879B48FED__INCLUDED_)
