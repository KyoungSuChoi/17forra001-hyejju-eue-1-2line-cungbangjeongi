/////////////////////////////////////////////////////////////////////////////
//
// SocketServerDlg.h : header file
//
/////////////////////////////////////////////////////////////////////////////

#ifndef Worker___H___
#define Worker___H___

//#include "ThreadDispatcher.h"
#include "critsect.h"
#include "RawSocketServerWorker.h"

class Worker : public CWizRawSocketListener
{
public:
	Worker();
	virtual BOOL TreatData   (HANDLE hShutDownEvent, HANDLE hDataTakenEvent);
	virtual BOOL ReadWrite   (CWizReadWriteSocket& socket);

	int 	m_nCurrThreads;
	int 	m_nMaxThreads;
	int		m_nRequests;

	CWizCriticalSection cs;
};

/////////////////////////////////////////////////////////////////////////////
#endif // Worker___H___

