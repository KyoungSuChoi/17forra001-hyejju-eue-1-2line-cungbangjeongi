#if !defined(AFX_SENSORDATADLG_H__069BD939_3C03_412C_9854_2C115217F0EA__INCLUDED_)
#define AFX_SENSORDATADLG_H__069BD939_3C03_412C_9854_2C115217F0EA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SensorDataDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSensorDataDlg dialog
#include "MyGridWnd.h"
#include "CTSMonDoc.h"

class CSensorDataDlg : public CDialog
{
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();
	virtual ~CSensorDataDlg();

// Construction
public:
	BOOL m_bLockUpdate;
	void ReDrawGrid();
	int m_nModuleID;
	void UpdateGridData();
	CString m_strModuleName;
	int m_nInstallModuleNum;
	CMyGridWnd m_wndTempGrid;
	CSensorDataDlg(CCTSMonDoc *pDoc, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSensorDataDlg)
	enum { IDD = IDD_SENSOR_DATA_DLG };
	CXPButton	m_btnOK;
	CXPButton	m_btnExcel;
	CXPButton	m_btnPrint;
	CXPButton	m_btnMap;
	CComboBox	m_ctrlMDSelCombo;
	CLabel	m_ctrlGasMin;
	CLabel	m_ctrlGasMax;
	CLabel	m_ctrlTempMin;
	CLabel	m_ctrlTempMax;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSensorDataDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void InitTempGrid();
	CCTSMonDoc *m_pDoc;

	// Generated message map functions
	//{{AFX_MSG(CSensorDataDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual void OnOK();
	afx_msg void OnMapButton();
	afx_msg void OnSelchangeModuleSelCombo();
	afx_msg void OnPrintButton();
	afx_msg void OnExcelSaveButton();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SENSORDATADLG_H__069BD939_3C03_412C_9854_2C115217F0EA__INCLUDED_)
