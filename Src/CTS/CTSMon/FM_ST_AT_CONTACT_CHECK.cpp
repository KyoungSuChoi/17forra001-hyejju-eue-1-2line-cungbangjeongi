#include "stdafx.h"

#include "CTSMon.h"

#include "FMS.h"
#include "FM_Unit.h"

#include "FM_STATE.h"

CFM_ST_AT_CONTACT_CHECK::CFM_ST_AT_CONTACT_CHECK(FM_STATUS_ID _Eqstid, FM_STATUS_ID _stid, CFM_Unit* _unit)
:CFM_ST_AUTO(_Eqstid, _stid, _unit)
{

}


CFM_ST_AT_CONTACT_CHECK::~CFM_ST_AT_CONTACT_CHECK(void)
{
}

VOID CFM_ST_AT_CONTACT_CHECK::fnEnter()
{
	fnSetFMSStateCode(FMS_ST_CONTACT_CHECK);
	//트레이 검사

	//공정 진행

	TRACE("CFM_ST_AT_CONTACT_CHECK::fnEnter %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_AT_CONTACT_CHECK::fnProc()
{
	//공정진행 중

	TRACE("CFM_ST_AT_CONTACT_CHECK::fnProc %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_AT_CONTACT_CHECK::fnExit()
{
	//공정 완료

	TRACE("CFM_ST_AT_CONTACT_CHECK::fnExit %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_AT_CONTACT_CHECK::fnSBCPorcess(WORD _state)
{
	CFM_ST_AUTO::fnSBCPorcess(_state);

	TRACE("%s \n", g_strSBCState[m_Unit->fnGetModule()->GetState()]);
	
	if(m_Unit->fnGetFMS()->fnGetError())
	{
		CHANGE_STATE(AUTO_ST_ERROR);
	}
        
	switch(m_Unit->fnGetModule()->GetState())
	{
	case EP_STATE_IDLE:
		if(m_Unit->fnGetModuleTrayState())
		{
			CHANGE_STATE(AUTO_ST_TRAY_IN);
		}
		else
		{
			CHANGE_STATE(AUTO_ST_VACANCY);
		}
		break;
	case EP_STATE_STANDBY:
		{
			CHANGE_STATE(AUTO_ST_TRAY_IN);
		}
		break;
	case EP_STATE_PAUSE:
		{
			CHANGE_STATE(AUTO_ST_ERROR);
		}
		break;
	case EP_STATE_RUN:
		{
			if(m_Unit->fnGetModuleCheckState())
			{
				//CHANGE_STATE(AUTO_ST_CONTACT_CHECK);
			}
			else
			{
				CHANGE_STATE(AUTO_ST_RUN);
			}
		}
		break;
	case EP_STATE_READY:
		{
			CHANGE_STATE(AUTO_ST_READY);
		}
		break;
	case EP_STATE_END:
		{
			CHANGE_STATE(AUTO_ST_END);
		}
		break;
	case EP_STATE_ERROR:
		{
			CHANGE_STATE(AUTO_ST_ERROR);
		}
		break;
	}
	TRACE("CFM_ST_AT_CONTACT_CHECK::fnSBCPorcess %d \n", m_Unit->fnGetModuleIdx());
}

FMS_ERRORCODE CFM_ST_AT_CONTACT_CHECK::fnFMSPorcess(FMS_COMMAND _msgId, st_FMS_PACKET* _recvData)
{
	FMS_ERRORCODE _error = ER_NO_Process;

	//입고완료
	switch(_msgId)
	{
	default:
		{
		}
		break;
	}
	TRACE("CFM_ST_AT_CONTACT_CHECK::fnFMSPorcess %d \n", m_Unit->fnGetModuleIdx());

	return _error;

}