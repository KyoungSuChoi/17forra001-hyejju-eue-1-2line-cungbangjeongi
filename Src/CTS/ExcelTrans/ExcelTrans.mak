# Microsoft Developer Studio Generated NMAKE File, Based on ExcelTrans.dsp
!IF "$(CFG)" == ""
CFG=ExcelTrans - Win32 Debug
!MESSAGE No configuration specified. Defaulting to ExcelTrans - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "ExcelTrans - Win32 Release" && "$(CFG)" != "ExcelTrans - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "ExcelTrans.mak" CFG="ExcelTrans - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "ExcelTrans - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "ExcelTrans - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "ExcelTrans - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

ALL : "$(OUTDIR)\ExcelTrans.exe" "$(OUTDIR)\ExcelTrans.bsc"


CLEAN :
	-@erase "$(INTDIR)\ExcelTrans.obj"
	-@erase "$(INTDIR)\ExcelTrans.pch"
	-@erase "$(INTDIR)\ExcelTrans.res"
	-@erase "$(INTDIR)\ExcelTrans.sbr"
	-@erase "$(INTDIR)\ExcelTransDlg.obj"
	-@erase "$(INTDIR)\ExcelTransDlg.sbr"
	-@erase "$(INTDIR)\FolderDialog.obj"
	-@erase "$(INTDIR)\FolderDialog.sbr"
	-@erase "$(INTDIR)\FormResultFile.obj"
	-@erase "$(INTDIR)\FormResultFile.sbr"
	-@erase "$(INTDIR)\Grading.obj"
	-@erase "$(INTDIR)\Grading.sbr"
	-@erase "$(INTDIR)\Procedure.obj"
	-@erase "$(INTDIR)\Procedure.sbr"
	-@erase "$(INTDIR)\SaveItemSetDlg.obj"
	-@erase "$(INTDIR)\SaveItemSetDlg.sbr"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\StdAfx.sbr"
	-@erase "$(INTDIR)\Step.obj"
	-@erase "$(INTDIR)\Step.sbr"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\ExcelTrans.bsc"
	-@erase "$(OUTDIR)\ExcelTrans.exe"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\ExcelTrans.pch" /Yu"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
RSC=rc.exe
RSC_PROJ=/l 0x412 /fo"$(INTDIR)\ExcelTrans.res" /d "NDEBUG" /d "_AFXDLL" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\ExcelTrans.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\ExcelTrans.sbr" \
	"$(INTDIR)\ExcelTransDlg.sbr" \
	"$(INTDIR)\FolderDialog.sbr" \
	"$(INTDIR)\FormResultFile.sbr" \
	"$(INTDIR)\Grading.sbr" \
	"$(INTDIR)\Procedure.sbr" \
	"$(INTDIR)\SaveItemSetDlg.sbr" \
	"$(INTDIR)\StdAfx.sbr" \
	"$(INTDIR)\Step.sbr"

"$(OUTDIR)\ExcelTrans.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
LINK32_FLAGS=PowerForm.Lib /nologo /subsystem:windows /incremental:no /pdb:"$(OUTDIR)\ExcelTrans.pdb" /machine:I386 /out:"$(OUTDIR)\ExcelTrans.exe" 
LINK32_OBJS= \
	"$(INTDIR)\ExcelTrans.obj" \
	"$(INTDIR)\ExcelTransDlg.obj" \
	"$(INTDIR)\FolderDialog.obj" \
	"$(INTDIR)\FormResultFile.obj" \
	"$(INTDIR)\Grading.obj" \
	"$(INTDIR)\Procedure.obj" \
	"$(INTDIR)\SaveItemSetDlg.obj" \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\Step.obj" \
	"$(INTDIR)\ExcelTrans.res"

"$(OUTDIR)\ExcelTrans.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "ExcelTrans - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

ALL : "$(OUTDIR)\ExcelTrans.exe" "$(OUTDIR)\ExcelTrans.bsc"


CLEAN :
	-@erase "$(INTDIR)\ExcelTrans.obj"
	-@erase "$(INTDIR)\ExcelTrans.pch"
	-@erase "$(INTDIR)\ExcelTrans.res"
	-@erase "$(INTDIR)\ExcelTrans.sbr"
	-@erase "$(INTDIR)\ExcelTransDlg.obj"
	-@erase "$(INTDIR)\ExcelTransDlg.sbr"
	-@erase "$(INTDIR)\FolderDialog.obj"
	-@erase "$(INTDIR)\FolderDialog.sbr"
	-@erase "$(INTDIR)\FormResultFile.obj"
	-@erase "$(INTDIR)\FormResultFile.sbr"
	-@erase "$(INTDIR)\Grading.obj"
	-@erase "$(INTDIR)\Grading.sbr"
	-@erase "$(INTDIR)\Procedure.obj"
	-@erase "$(INTDIR)\Procedure.sbr"
	-@erase "$(INTDIR)\SaveItemSetDlg.obj"
	-@erase "$(INTDIR)\SaveItemSetDlg.sbr"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\StdAfx.sbr"
	-@erase "$(INTDIR)\Step.obj"
	-@erase "$(INTDIR)\Step.sbr"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\ExcelTrans.bsc"
	-@erase "$(OUTDIR)\ExcelTrans.exe"
	-@erase "$(OUTDIR)\ExcelTrans.ilk"
	-@erase "$(OUTDIR)\ExcelTrans.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\ExcelTrans.pch" /Yu"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
RSC=rc.exe
RSC_PROJ=/l 0x412 /fo"$(INTDIR)\ExcelTrans.res" /d "_DEBUG" /d "_AFXDLL" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\ExcelTrans.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\ExcelTrans.sbr" \
	"$(INTDIR)\ExcelTransDlg.sbr" \
	"$(INTDIR)\FolderDialog.sbr" \
	"$(INTDIR)\FormResultFile.sbr" \
	"$(INTDIR)\Grading.sbr" \
	"$(INTDIR)\Procedure.sbr" \
	"$(INTDIR)\SaveItemSetDlg.sbr" \
	"$(INTDIR)\StdAfx.sbr" \
	"$(INTDIR)\Step.sbr"

"$(OUTDIR)\ExcelTrans.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
LINK32_FLAGS=PowerFormD.Lib /nologo /subsystem:windows /incremental:yes /pdb:"$(OUTDIR)\ExcelTrans.pdb" /debug /machine:I386 /out:"$(OUTDIR)\ExcelTrans.exe" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\ExcelTrans.obj" \
	"$(INTDIR)\ExcelTransDlg.obj" \
	"$(INTDIR)\FolderDialog.obj" \
	"$(INTDIR)\FormResultFile.obj" \
	"$(INTDIR)\Grading.obj" \
	"$(INTDIR)\Procedure.obj" \
	"$(INTDIR)\SaveItemSetDlg.obj" \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\Step.obj" \
	"$(INTDIR)\ExcelTrans.res"

"$(OUTDIR)\ExcelTrans.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("ExcelTrans.dep")
!INCLUDE "ExcelTrans.dep"
!ELSE 
!MESSAGE Warning: cannot find "ExcelTrans.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "ExcelTrans - Win32 Release" || "$(CFG)" == "ExcelTrans - Win32 Debug"
SOURCE=.\ExcelTrans.cpp

"$(INTDIR)\ExcelTrans.obj"	"$(INTDIR)\ExcelTrans.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\ExcelTrans.pch"


SOURCE=.\ExcelTrans.rc

"$(INTDIR)\ExcelTrans.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) $(RSC_PROJ) $(SOURCE)


SOURCE=.\ExcelTransDlg.cpp

"$(INTDIR)\ExcelTransDlg.obj"	"$(INTDIR)\ExcelTransDlg.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\ExcelTrans.pch"


SOURCE=..\MYDll\src\Utility\FolderDialog.cpp

"$(INTDIR)\FolderDialog.obj"	"$(INTDIR)\FolderDialog.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\ExcelTrans.pch"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\MYDll\src\Utility\FormResultFile.cpp

"$(INTDIR)\FormResultFile.obj"	"$(INTDIR)\FormResultFile.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\ExcelTrans.pch"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\MYDll\src\Utility\Grading.cpp

"$(INTDIR)\Grading.obj"	"$(INTDIR)\Grading.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\ExcelTrans.pch"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\MYDll\src\Utility\Procedure.cpp

"$(INTDIR)\Procedure.obj"	"$(INTDIR)\Procedure.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\ExcelTrans.pch"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\SaveItemSetDlg.cpp

"$(INTDIR)\SaveItemSetDlg.obj"	"$(INTDIR)\SaveItemSetDlg.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\ExcelTrans.pch"


SOURCE=.\StdAfx.cpp

!IF  "$(CFG)" == "ExcelTrans - Win32 Release"

CPP_SWITCHES=/nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\ExcelTrans.pch" /Yc"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

"$(INTDIR)\StdAfx.obj"	"$(INTDIR)\StdAfx.sbr"	"$(INTDIR)\ExcelTrans.pch" : $(SOURCE) "$(INTDIR)"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ELSEIF  "$(CFG)" == "ExcelTrans - Win32 Debug"

CPP_SWITCHES=/nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\ExcelTrans.pch" /Yc"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 

"$(INTDIR)\StdAfx.obj"	"$(INTDIR)\StdAfx.sbr"	"$(INTDIR)\ExcelTrans.pch" : $(SOURCE) "$(INTDIR)"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ENDIF 

SOURCE=..\MYDll\src\Utility\Step.cpp

"$(INTDIR)\Step.obj"	"$(INTDIR)\Step.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\ExcelTrans.pch"
	$(CPP) $(CPP_PROJ) $(SOURCE)



!ENDIF 

