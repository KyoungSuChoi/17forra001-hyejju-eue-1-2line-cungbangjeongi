// ResultView.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "CTSAnalDoc.h"
#include "ResultFileLoadDlg.h"

#include "ResultView.h"
#include "MainFrm.h"

//#include "Mainfrm.h"
#include "DetailTestConditionDlg.h"
#include "DetailAutoTestConditionDlg.h"
#include "ChResultViewDlg.h"
#include "CellGradeListDlg.h"
#include "ShowDetailResultDlg.h"
#include "SetDataFolderDlg.h"

#include "CpkSetDlg.h"
#include "SerarchDataDlg.h"
#include "SensorDataDlg.h"
#include "GradeColorDlg.h"
#include "CellInfoDlg.h"

#include "ModuleStepEndData.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CResultView

//volatile BOOL m_gBeLoding = FALSE;

IMPLEMENT_DYNCREATE(CResultView, CFormView)

bool CResultView::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CResultView"), _T("TEXT_CResultView_CNT"), _T("TEXT_CResultView_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CResultView_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CResultView"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

CResultView::CResultView()
	: CFormView(CResultView::IDD)
{
	LanguageinitMonConfig();

	//{{AFX_DATA_INIT(CResultView)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
//	m_pConditionMain = NULL;
	m_pConfig = NULL;
//	ZeroMemory(&m_extData, sizeof(STR_FILE_EXT_SPACE));
	m_nDisplayMode = TYPE_CHANNEL_LIST;
	m_pCurrentGrid = & m_wndCellListGrid;
//	m_pLoadDlg = new CResultFileLoadDlg(this);

	m_nCurrentStepIndex = 0;
//	m_nInputCellCount = 0;
//	m_nFailCount = 0;
//	m_nNormalCount = 0;

	m_bDispCapSum = FALSE;
	m_bUseTempSensor = FALSE;
	m_pImageList = NULL;
	m_bDblclkDataTree = false;
	m_DeviceMode = DEVICE_FORMATION;

	LanguageinitMonConfig();
}

CResultView::~CResultView()
{
/*	if(m_pLoadDlg)
	{
		delete m_pLoadDlg;
		m_pLoadDlg = NULL;
	}
*/
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}

	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "LastDisplayType", m_nDisplayMode);

	if(m_pImageList)
	{
		delete m_pImageList;
		m_pImageList = NULL;
	}
}

void CResultView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CResultView)
	DDX_Control(pDX, IDC_RESTORE_RESULTDATA, m_btnRestoreResultData);
	DDX_Control(pDX, IDC_MES_UPDATE_BUTTON, m_btnMesUpdate);
	DDX_Control(pDX, IDC_TEST_CONDITION_VIEW, m_btnTestCon);
	DDX_Control(pDX, IDC_FORMAT_SETTING, m_btnExcelSave);
	DDX_Control(pDX, IDC_SYSPARAM_SHOW, m_btnProfile);
	DDX_Control(pDX, IDC_RELOAD, m_btnReload);
	DDX_Control(pDX, IDC_BUTTON_SEARCH, m_btnSearch);
	DDX_Control(pDX, IDC_CP_BUTTON, m_btnCp);
	DDX_Control(pDX, IDC_BUTTON_LOT_ALL, m_btnLotAll);
	DDX_Control(pDX, IDC_DISPLAY_MODE, m_ctrlDisplayMode);
	DDX_Control(pDX, IDC_RATE, m_ctrlRate);
	DDX_Control(pDX, IDC_ERROR_RATE, m_ctrlErrorRate);
	DDX_Control(pDX, IDC_NORMAL_ERROR_RATE, m_ctrlNormalRate);
	DDX_Control(pDX, IDC_OPERATOR, m_ctrlOperator);
	DDX_Control(pDX, IDC_TYPE_SEL, m_ctrlTypeSel);
	DDX_Control(pDX, IDC_TESTED_MODULE, m_ctrlTestedModule);
	DDX_Control(pDX, IDC_TEST_NAME, m_ctrlTestName);
	DDX_Control(pDX, IDC_TESTED_DATETIME, m_ctrlTestedDateTime);
	DDX_Control(pDX, IDC_TEST_LOTNO, m_ctrlTestLotNo);
	DDX_Control(pDX, IDC_FILE_NAME, m_ctrlFileName);
	DDX_Control(pDX,  IDC_STEP_SEL_COMBO, m_ctrlStepSelCombo);
	DDX_Control(pDX, IDC_DATA_TREE, m_wndDataTree);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CResultView, CFormView)
	//{{AFX_MSG_MAP(CResultView)
	ON_BN_CLICKED(IDC_TEST_CONDITION_VIEW, OnTestConditonView)
	ON_WM_DESTROY()
	ON_CBN_SELCHANGE(IDC_STEP_SEL_COMBO, OnSelchangeCombo1)
	ON_WM_SIZE()
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE_AS, OnUpdateFileSaveAs)
	ON_COMMAND(ID_FILE_SAVE_AS, OnFileSaveAs)
	ON_BN_CLICKED(IDC_RELOAD, OnReload)
	ON_BN_CLICKED(IDC_FORMAT_SETTING, OnFormatSetting)
	ON_CBN_SELCHANGE(IDC_DISPLAY_MODE, OnSelchangeDisplayMode)
	ON_COMMAND(ID_FILE_SAVE, OnFileSave)
	ON_COMMAND(ID_DATA_SORT, OnDataSort)
	ON_BN_CLICKED(IDC_SYSPARAM_SHOW, OnSysparamShow)
	ON_UPDATE_COMMAND_UI(ID_DATA_SORT, OnUpdateDataSort)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_COMMAND(ID_SELECT_ALL, OnSelectAll)
	ON_NOTIFY(NM_DBLCLK, IDC_DATA_TREE, OnDblclkDataTree)
	ON_COMMAND(ID_DATA_PATH, OnDataPath)
	ON_COMMAND(ID_FIELD_SET, OnFieldSet)
	ON_WM_CONTEXTMENU()
	ON_BN_CLICKED(IDC_CP_BUTTON, OnCpButton)
	ON_BN_CLICKED(IDC_BUTTON_SEARCH, OnButtonSearch)
	ON_BN_CLICKED(IDC_BUTTON_LOT_ALL, OnButtonLotAll)
	ON_BN_CLICKED(IDC_SENSOR_BUTTON, OnSensorButton)
	ON_COMMAND(ID_GRADE_COLOR_SET, OnGradeColorSet)
	ON_BN_CLICKED(IDC_MES_UPDATE_BUTTON, OnMesUpdateButton)
	ON_COMMAND(ID_FILE_RAW_VIEW, OnFileRawView)
	ON_UPDATE_COMMAND_UI(ID_FILE_RAW_VIEW, OnUpdateFileRawView)
	ON_COMMAND(ID_RELOAD, OnReload)
	ON_BN_CLICKED(IDC_RESTORE_RESULTDATA, OnRestoreResultdata)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_GRID_DOUBLECLICK, OnGridDoubleClick)
	ON_MESSAGE(WM_GRID_MOVECELL, OnMovedCurrentCell)
	ON_MESSAGE(WM_GRID_RIGHT_CLICK, OnRButtonClickedRowCol)

	ON_COMMAND_RANGE(IDM_FIRST_SHELLMENUID, IDM_LAST_SHELLMENUID, OnShellCommand)
	ON_COMMAND(ID_FILE_PRINT, 			CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, 	CView::OnFilePrintPreview)
	ON_BN_CLICKED(IDC_VIEW_SELECT1_BTN, &CResultView::OnBnClickedViewSelect1Btn)
	ON_BN_CLICKED(IDC_VIEW_SELECT2_BTN, &CResultView::OnBnClickedViewSelect2Btn)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CResultView diagnostics

#ifdef _DEBUG
void CResultView::AssertValid() const
{
	CFormView::AssertValid();
}

void CResultView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CCTSAnalDoc* CResultView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCTSAnalDoc)));
	return (CCTSAnalDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CResultView message handlers

void CResultView::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();
	
	// TODO: Add your specialized code here and/or call the base class
	CCTSAnalDoc *pDoc = (CCTSAnalDoc *)GetDocument();
	m_pConfig = pDoc->GetTopConfig();
	ASSERT(m_pConfig);
	m_ctrlErrorRate.SetTextColor(RGB(255,0,0)).SetBorder(TRUE).SetBorder(TRUE);
	m_ctrlNormalRate.SetTextColor(RGB(0, 0, 255)).SetBorder(TRUE).SetBorder(TRUE);
	m_ctrlRate.SetTextColor(RGB(255, 0, 0)).SetBorder(TRUE).SetBorder(TRUE);

	InitStepListGrid();
	InitChannelListGrid();
	InitMinMaxGrid();
	InitCellListGrid();
	InitTrayTypeGrid();

	UpdateDataUnit();

	// m_ctrlDisplayMode.AddString("Tray 전공정 Data");
	m_ctrlDisplayMode.AddString(TEXT_LANG[0]);//"Step별 Data"
	// m_ctrlDisplayMode.AddString("Tray 배열형태");
	m_nDisplayMode = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "LastDisplayType", TYPE_CHANNEL_LIST);
	m_ctrlDisplayMode.SetCurSel(m_nDisplayMode);

	m_pImageList = new CImageList;
	//상태별 표시할 이미지 로딩
	m_pImageList->Create(IDB_CELL_STATE_ICON,19,24,RGB(255,255,255));
	m_ctrlStepSelCombo.SetImageList(m_pImageList);


/*	if(m_strSelFolder.IsEmpty())
		m_pLoadDlg->m_strFileFolder = GetDocument()->m_strDataFolder;
	else
		m_pLoadDlg->m_strFileFolder = m_strSelFolder;
	m_pLoadDlg->m_pDoc =  GetDocument();
	m_pLoadDlg->Create(IDD_RESULT_FILE_LOAD_DLG, this);
*/
	m_strSelFolder = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION, "LastSelDir", GetDocument()->m_strDataFolder);
	
	SetDisplayMode(m_nDisplayMode);

	LoadDataTree();

#ifdef _EDLC_TEST_SYSTEM
	HWND hWnd = ::FindWindow(NULL, CTSDATA_SERVER);
	if(hWnd == NULL)
	{
		GetDlgItem(IDC_MES_UPDATE_BUTTON)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_MES_UPDATE_BUTTON)->EnableWindow(TRUE);
	}
#endif

}

bool CResultView::OnResultFileOpen() 
{
//	if(m_gBeLoding)					
//		return;

/*	((CMainFrame *)AfxGetMainWnd())->m_tabWnd.ActivateTab(2);
	m_strSelFolder = GetDocument()->m_pLoadDlg->m_strFileFolder;
	m_strSelFileName = GetDocument()->m_pLoadDlg->GetSelFileName();
*/		
	m_wndChannelListGrid.SetRowCount(0);

	if(m_strSelFileName.IsEmpty())	
		return false;

//	m_gBeLoding = TRUE;

	int nRtn;
	if((nRtn = ReadGroupFile((LPSTR)(LPCTSTR)m_strSelFileName)) <0)
	{
		CString strTemp;
		strTemp.Format("%s File Open Fail. (Code %d)", m_strSelFileName, nRtn);
		AfxMessageBox(strTemp);
//		m_gBeLoding = FALSE;
		return false;
	}

//	if(m_resultFile.GetStepSize() < 1)	return;
	DisplayData();
	
	DisplayTestCondition();

//	m_ctrlStepSelCombo.SetCurSel(m_stepData.GetSize()-1);
//	m_gBeLoding = FALSE;

	return true;
}

int CResultView::ReadGroupFile(char *szFileName)
{	
	if(m_resultFile.ReadFile(szFileName) < 0)		return -1;

	//Display Test Information
	CString strTemp;
	RESULT_FILE_HEADER *pHeader =  m_resultFile.GetResultHeader();
//	EP_MD_SYSTEM_DATA *pSysData = m_resultFile.GetMDSysData();

	m_ctrlFileName.SetText(pHeader->szTrayNo);
	m_ctrlTestLotNo.SetText(pHeader->szLotNo);
	m_ctrlTypeSel.SetText(pHeader->szTypeSel);
	// m_ctrlTestName.SetText(m_resultFile.GetTestName());
	strTemp.Format("%ld", m_resultFile.GetTestCondition()->GetTestInfo()->lID);
	m_ctrlTestName.SetText(strTemp);
	m_ctrlTestedDateTime.SetText(pHeader->szDateTime);
	strTemp = m_resultFile.GetModuleName();
	if(strTemp.IsEmpty())
	{
		strTemp.Format("%s (Total %d Ch)", 
				GetDocument()->ModuleName(pHeader->nModuleID, pHeader->wGroupIndex),  
				m_resultFile.GetTotalCh()
				);
	}
	m_ctrlTestedModule.SetText(strTemp);
	// m_ctrlOperator.SetText(m_resultFile.GetModelName());		//==>Type num
	strTemp.Format("%ld", m_resultFile.GetTestCondition()->GetModelID());
	m_ctrlOperator.SetText(strTemp);		//==>Type num

//	m_ctrlStepSelCombo.ResetContent();
	Display_Data_List dataList;
	m_dataList.RemoveAll();

	STR_STEP_RESULT *pStep;
	int nStepIndex = 0;

	// for(int i = 0; i<m_resultFile.GetStepSize(); i++)	
	for(int i = 0; i<m_resultFile.GetLastIndexStepNo(); i++)
	{
		pStep = m_resultFile.GetStepData(i);
		if(pStep == NULL)
		{
			continue;			
		}

		strTemp.Format("[%d] ", pStep->stepCondition.stepHeader.stepIndex+1);
		strTemp = strTemp + GetDocument()->GetProcTypeName(pStep->stepCondition.stepHeader.nProcType, pStep->stepCondition.stepHeader.type);
	
		dataList.stepIndex = pStep->stepCondition.stepHeader.stepIndex;
		dataList.nType = pStep->stepCondition.stepHeader.type;
		switch(pStep->stepCondition.stepHeader.type)
		{
		case EP_TYPE_CHARGE:	
		case EP_TYPE_DISCHARGE:	
			dataList.nDataType = EP_VOLTAGE;
			dataList.strName = strTemp+TEXT_LANG[1];//"_전압(mV)"
			m_dataList.Add(dataList);

			dataList.nDataType = EP_CURRENT;
			dataList.strName = strTemp+TEXT_LANG[2];//"_전류(mA)"
			m_dataList.Add(dataList);

			dataList.nDataType = EP_CAPACITY;
			dataList.strName = strTemp+TEXT_LANG[3];//"_용량(mAh)"
			m_dataList.Add(dataList);

			dataList.nDataType = EP_STEP_TIME;
			dataList.strName = strTemp+TEXT_LANG[4];//"_시간(sec)"
			m_dataList.Add(dataList);

			if(m_bUseTempSensor)
			{
				dataList.nDataType = EP_TEMPER;
				dataList.strName = strTemp+TEXT_LANG[5];//"_온도(℃)"
				m_dataList.Add(dataList);
			}

			dataList.nDataType = EP_GRADE_CODE;
			dataList.strName =  strTemp+TEXT_LANG[6];//"_등급"
			m_dataList.Add(dataList);

			break;

		case EP_TYPE_REST:
			dataList.nDataType = EP_VOLTAGE;
			dataList.strName = strTemp+TEXT_LANG[1];//"_전압(mV)"
			m_dataList.Add(dataList);
			break;
			
		case EP_TYPE_OCV:			//전압 표시 
			dataList.nDataType = EP_VOLTAGE;
			dataList.strName = strTemp+TEXT_LANG[1];//"_전압(mV)"
			m_dataList.Add(dataList);
			
			dataList.nDataType = EP_GRADE_CODE;
			dataList.strName =  strTemp+TEXT_LANG[6];//"_등급"
			m_dataList.Add(dataList);
			break;

		case EP_TYPE_IMPEDANCE:							//임피던스 표시
			dataList.nDataType = EP_IMPEDANCE;
			dataList.strName = strTemp+TEXT_LANG[7];//"_저항(mΩ)"
			m_dataList.Add(dataList);

			dataList.nDataType = EP_GRADE_CODE;
			dataList.strName =  strTemp+TEXT_LANG[6];//"_등급"
			m_dataList.Add(dataList);

			break;
		}
			
//		if(pStep->stepCondition.stepHeader.gradeSize > 0)
//		{
//			dataList.nDataType = EP_GRADE_CODE;
//			dataList.strName =  strTemp+"_Grade";
//			m_dataList.Add(dataList);
//		}
	
/*		strTemp.Format("%d :: %s", i+1, GetDocument()->GetProcTypeName(pStep->stepHead.nGroupIndex, pStep->stepCondition.stepHeader.type));

		m_ctrlStepSelCombo.AddString(strTemp);	
		m_ctrlStepSelCombo.SetItemIcon(i, 2);
*/
		nStepIndex++;
	}

	dataList.nDataType = EP_CH_CODE;
	// dataList.stepIndex = m_resultFile.GetStepSize()-1;	
	dataList.stepIndex = nStepIndex;
	dataList.strName = TEXT_LANG[8];//"완료채널코드"
	// if(dataList.stepIndex >= 0)
	if( nStepIndex >= 0 )
	{
		m_dataList.InsertAt(0, dataList);
	}
	return 0;
}


/*
int CResultView::ReadGroupFile(char *szFileName)
{
	ASSERT(strlen(szFileName) > 0);
	FILE *fp = fopen(szFileName, "rb");
	CCTSAnalDoc *pDoc = GetDocument();
	if(fp == NULL)	return -1;

	fread(&m_fileHaeder, sizeof(EP_FILE_HEADER), 1, fp);
	int nFileVersion = atol(m_fileHaeder.szFileVersion);

	if(strcmp(m_fileHaeder.szFileID, RESULT_FILE_ID) == 0)		//Formation Result File Read
	{
		if( nFileVersion < 0 || nFileVersion > atol(RESULT_FILE_VER6))
		{
			fclose(fp);
			fp = NULL;
			return -2;
		}
	}
	else if(strcmp(m_fileHaeder.szFileID, IROCV_FILE_ID) == 0)	//IROCV Result File Read
	{
		if( nFileVersion < 0 || nFileVersion > atol(IROCV_FILE_VER6))
		{
			fclose(fp);
			fp = NULL;
			return -3;
		}
	}
	else
	{
		fclose(fp);
		fp = NULL;
		return -4;
	}

	if(fread(&m_resultFileHeader, sizeof(RESULT_FILE_HEADER), 1, fp)<1)	//Read Test Result Header
	{
		TRACE("Test Group File header Read Fail..\n");
		fclose(fp);
		fp = NULL;
		return -5;	//File Header Read
	}

	if(fread(&m_sysData, sizeof(EP_MD_SYSTEM_DATA), 1, fp)<1)			//Read System Param
	{
		TRACE("Test Group File header Read Fail..\n");
		fclose(fp);
		fp = NULL;
		return -6;	//File Header Read
	}

	if(nFileVersion >= atol(RESULT_FILE_VER3))
	{
		if(fread(&m_extData, sizeof(STR_FILE_EXT_SPACE), 1, fp) < 1)
		{
			TRACE("File Data Read Fail..\n");
					
			fclose(fp);
			fp = NULL;
			return -7;	//File Header Read
		}
	}

	ClearResult();			//Clear Result Data Structure
	ASSERT(m_pConditionMain == NULL);
	m_pConditionMain = new STR_CONDITION;
	ASSERT(m_pConditionMain);

	if(pDoc->ReadTestConditionFile(fp, m_pConditionMain, m_fileHaeder.szFileID, m_fileHaeder.szFileVersion) == FALSE)		//Read Tested Condition
	{
		fclose(fp);
		fp = NULL;
		return -8;
	}

	//Display Test Information
	CString strTemp;
	RESULT_FILE_HEADER *pHeader =  m_resultFile.GetResultHeader();

	m_ctrlFileName.SetText(pHeader->szTrayNo);
	m_ctrlTestLotNo.SetText(pHeader->szLotNo);
	m_ctrlTestName.SetText(m_pConditionMain->conditionHeader.szName);
	m_ctrlTestedDateTime.SetText(pHeader->szDateTime);
	if(pHeader->nGroupIndex >= 0 && pHeader->nGroupIndex < EP_MAX_BD_PER_MD)
	{
		strTemp.Format("%s (Total %d Ch)", pDoc->ModuleName(pHeader->nModuleID, pHeader->nGroupIndex),  m_sysData.awChInGroup[pHeader->nGroupIndex]);
		m_ctrlTestedModule.SetText(strTemp);
	}
	else
	{
		m_ctrlTestedModule.SetText("Board Index error");
	}
//	m_ctrlOperator.SetText(m_resultFileHeader.szOperatorID);	
	m_ctrlOperator.SetText(m_resultFile.GetModelData()->szName);		//==>Model Name
//	int nTestedStepSize = m_pConditionMain->apStepList.GetSize();

	EP_GROUP_INFO	rdBuffer;
	STR_STEP_RESULT	*pStepResultData;
	EP_GP_DATA		rdBoardBuff;
	EP_CH_DATA		rdChnnelBuff, *lpChnnelData;
	EP_RESULT_TO_DATABASE	testResult[EP_RESULT_ITEM_NO];
	
	m_ctrlStepSelCombo.ResetContent();
	int nCount = 0;
	Display_Data_List dataList;
	m_dataList.RemoveAll();

	while(1)
	{
//		pDoc->SetProgressPos(1000/nTestedStepSize*nCount);
		ZeroMemory(&rdBoardBuff, sizeof(EP_GP_DATA));
		if(nFileVersion >= atol(RESULT_FILE_VER4))
		{
			if(fread(&rdBoardBuff, sizeof(EP_GP_DATA), 1, fp) <1)		break;	//Read Group State Result
		}
		else
		{
			if(fread(&rdBoardBuff.gpState, sizeof(EP_GP_STATE_DATA), 1, fp) <1)		break;	//Read Group State Result
		}
		
		pStepResultData = new STR_STEP_RESULT;
		ASSERT(pStepResultData);
		
		memcpy(&pStepResultData->stepData.gpData, &rdBoardBuff, sizeof(EP_GP_DATA));
		m_stepData.Add(pStepResultData);

		//Version 6이전 파일은 STR_CONDITION 구조의 시험 조건을 EP_COMM_STEP을 변형 시킨다.
		if(nFileVersion < atol(RESULT_FILE_VER6))
		{
			ZeroMemory(&pStepResultData->stepHead, sizeof(RESULT_FILE_HEADER));
			pStepResultData->stepCondition = ConvertToCommStep(m_pConditionMain, nCount);
		}
		else if(nFileVersion >= atol(RESULT_FILE_VER6))			//LG File Format
		{
			if(fread(&pStepResultData->stepHead, sizeof(RESULT_FILE_HEADER), 1, fp) < 1)
			{
				fclose(fp);
				fp = NULL;
				return -12;
			}	
		
			if(fread(&pStepResultData->stepCondition, sizeof(EP_COMMON_STEP), 1, fp) < 1)
			{
				fclose(fp);
				fp = NULL;
				return -11;
			}
		}
		strTemp.Format("%d:", nCount+1);
		strTemp = strTemp + pDoc->GetProcTypeName(pStepResultData->stepHead.nGroupIndex, pStepResultData->stepCondition.stepHeader.type);
	
		dataList.stepIndex = nCount;
		switch(pStepResultData->stepCondition.stepHeader.type)
		{
		case EP_TYPE_CHARGE:	
		case EP_TYPE_DISCHARGE:	
			dataList.nDataType = EP_VOLTAGE;
			dataList.strName = strTemp+"_V(mV)";
			m_dataList.Add(dataList);

			dataList.nDataType = EP_CURRENT;
			dataList.strName = strTemp+"_I(mA)";
			m_dataList.Add(dataList);

			dataList.nDataType = EP_CAPACITY;
			dataList.strName = strTemp+"_C(mAh)";
			m_dataList.Add(dataList);
			break;

		case EP_TYPE_OCV:			//전압 표시 
			dataList.nDataType = EP_VOLTAGE;
			dataList.strName = strTemp+"(mV)";
			m_dataList.Add(dataList);
			break;

		case EP_TYPE_IMPEDANCE:							//임피던스 표시
			dataList.nDataType = EP_IMPEDANCE;
			dataList.strName = strTemp+"(mΩ)";
			m_dataList.Add(dataList);
			break;
		}
		nCount++;
	
		strTemp.Format("%d :: %s", nCount, pDoc->GetProcTypeName(pStepResultData->stepHead.nGroupIndex, pStepResultData->stepCondition.stepHeader.type));

		m_ctrlStepSelCombo.AddString(strTemp);

		if(nFileVersion >= atol(RESULT_FILE_VER4))
		{
			if(fread(&testResult, sizeof(EP_RESULT_TO_DATABASE)*EP_RESULT_ITEM_NO, 1, fp) <1)				//Read Channel Result 
			{
				fclose(fp);
				fp = NULL;
				return -10;
			}
		}

//		TRACE("%d Step Data Read\n", m_stepResult.GetSize());

		for(int i =0; i<m_sysData.awChInGroup[m_resultFileHeader.nGroupIndex] ; i++)		//0->를 각 구릅의 채널 수로 입력해야 한다.
		{
			if(fread(&rdChnnelBuff, sizeof(EP_CH_DATA), 1, fp) <1)				//Read Channel Result 
			{
				fclose(fp);
				fp = NULL;
				return -9;
			}

			lpChnnelData = new EP_CH_DATA;
			memcpy(lpChnnelData, &rdChnnelBuff, sizeof(EP_CH_DATA));
			pStepResultData->aChData.Add(lpChnnelData);
			
//			TRACE("%d Step Channel %d Data Read\n", m_stepResult.GetSize(), pStepData->chData.GetSize());
		}
	}

	dataList.nDataType = EP_GRADE_CODE;
	dataList.stepIndex = m_stepData.GetSize()-1;
	dataList.strName = "Grade";
	if(dataList.stepIndex >= 0)
	{
		m_dataList.InsertAt(0, dataList);
	}

	if(fp != NULL)
	{
		fclose(fp);
		fp = NULL;
	}
	return 0;
}
*/

void CResultView::InitStepListGrid()
{
	m_wndStepListGrid.SubclassDlgItem(IDC_BOARD_VIEW_GRID, this);
//	m_wndStepListGrid.m_bRowSelection = TRUE;
	m_wndStepListGrid.m_bSameColSize  = FALSE;
	m_wndStepListGrid.m_bSameRowSize  = FALSE;
	m_wndStepListGrid.m_bCustomWidth  = TRUE;

	m_wndStepListGrid.Initialize();
//	m_wndStepListGrid.SetRowCount();
	m_wndStepListGrid.SetColCount(10);
	m_wndStepListGrid.SetDefaultRowHeight(24);
//	m_wndStepListGrid.GetParam()->SetSortRowsOnDblClk(TRUE);
	m_wndStepListGrid.m_BackColor	= RGB(255,255,255);
//	m_wndStepListGrid.m_SelColor	= RGB(225,255,225);
	m_wndStepListGrid.EnableCellTips();
	
	m_wndStepListGrid.SetStyleRange(CGXRange().SetCols(1), CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(255,255,255)));
	m_wndStepListGrid.SetStyleRange(CGXRange().SetTable(), CGXStyle().SetFont(CGXFont().SetSize(11).SetBold(TRUE)).SetWrapText(FALSE));

//	m_wndStepListGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
}

void CResultView::InitCellListGrid()
{
	m_wndCellListGrid.SubclassDlgItem(IDC_RESULT_CELL_LIST_GRID, this);
//	m_wndCellListGrid.m_bRowSelection = TRUE;
	m_wndCellListGrid.m_bSameColSize  = FALSE;
	m_wndCellListGrid.m_bSameRowSize  = FALSE;
	m_wndCellListGrid.m_bCustomWidth  = FALSE;


/*	CRect rect;
	m_wndStepListGrid.GetWindowRect(rect);	//Step Grid Window Position
	ScreenToClient(&rect);

	int width = rect.Width()/100;
	m_wndStepListGrid.m_nWidth[1]	= width* 100;
	m_wndStepListGrid.m_nWidth[2]	= width* 10;
	m_wndStepListGrid.m_nWidth[3]	= width* 150;
	m_wndStepListGrid.m_nWidth[4]	= width* 20;
	m_wndStepListGrid.m_nWidth[5]	= width* 20;
*/
	m_wndCellListGrid.Initialize();
	m_wndCellListGrid.SetRowCount(EP_MAX_CH_PER_MD);
//	m_wndCellListGrid.SetColCount(10);
	m_wndCellListGrid.SetDefaultRowHeight(40);
	m_wndCellListGrid.SetDefaultColWidth(100);
	m_wndCellListGrid.GetParam()->SetSortRowsOnDblClk(TRUE);
	m_wndCellListGrid.m_BackColor	= RGB(255,255,255);
//	m_wndStepListGrid.m_SelColor	= RGB(225,255,225);

	m_wndCellListGrid.EnableGridToolTips();
    m_wndCellListGrid.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, 
         CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));

	m_wndCellListGrid.SetColWidth(0, 0, 40);
	m_wndCellListGrid.SetRowHeight(0, 0, 32);
	
//	m_wndCellListGrid.SetStyleRange(CGXRange().SetCols(1), CGXStyle().SetEnabled(FALSE));
	
/*	m_wndCellListGrid.SetValueRange(CGXRange(0,1),  "Step");
	m_wndCellListGrid.SetValueRange(CGXRange(0,2),  "Type");
	m_wndCellListGrid.SetValueRange(CGXRange(0,3),  "Mode");
	m_wndCellListGrid.SetValueRange(CGXRange(0,4),  "V_Ref");
	m_wndCellListGrid.SetValueRange(CGXRange(0,5),  "I_Ref");
	m_wndCellListGrid.SetValueRange(CGXRange(0,6),  "End");
	m_wndCellListGrid.SetValueRange(CGXRange(0,7),  "");
	m_wndCellListGrid.SetValueRange(CGXRange(0,8),  "");
	m_wndCellListGrid.SetValueRange(CGXRange(0,9),  "");
	m_wndCellListGrid.SetValueRange(CGXRange(0,10),  "");
*/
}
void CResultView::InitChannelListGrid()
{
	m_wndChannelListGrid.SubclassDlgItem(IDC_CHANNEL_VIEW_GRID, this);
//	m_wndChannelListGrid.m_bRowSelection = TRUE;
	m_wndChannelListGrid.m_bSameColSize  = FALSE;
	m_wndChannelListGrid.m_bSameRowSize  = FALSE;
	m_wndChannelListGrid.m_bCustomWidth  = TRUE;

/*	CRect rect;
	m_wndChannelListGrid.GetWindowRect(rect);	//Step Grid Window Position
	ScreenToClient(&rect);

	int width = rect.Width()/100;
	m_wndChannelListGrid.m_nWidth[1]	= width* 6;
	m_wndChannelListGrid.m_nWidth[2]	= width* 6;
	m_wndChannelListGrid.m_nWidth[3]	= width* 8;
	m_wndChannelListGrid.m_nWidth[4]	= width* 10;
	m_wndChannelListGrid.m_nWidth[5]	= width* 9;
	m_wndChannelListGrid.m_nWidth[6]	= width* 9;
	m_wndChannelListGrid.m_nWidth[7]	= width* 9;
	m_wndChannelListGrid.m_nWidth[8]	= width* 9;
	m_wndChannelListGrid.m_nWidth[9]	= width* 9;
	m_wndChannelListGrid.m_nWidth[10]	= width* 16;
	m_wndChannelListGrid.m_nWidth[11]	= width* 10;
*/	
	m_wndChannelListGrid.Initialize();
	m_wndChannelListGrid.SetRowCount(128);
	m_wndChannelListGrid.SetColCount(25);
	m_wndChannelListGrid.SetDefaultRowHeight(30);
	m_wndChannelListGrid.SetRowHeight(0, 0, 32);
	m_wndChannelListGrid.GetParam()->SetSortRowsOnDblClk(TRUE);
	m_wndChannelListGrid.m_BackColor	= RGB(255,255,255);
//	m_wndChannelListGrid.m_SelColor	= RGB(225,255,225);
//	m_wndChannelListGrid.HideCols(5, 5);

	m_wndChannelListGrid.m_bCustomColor 	= FALSE;
//	m_wndChannelListGrid.m_CustomColorRange	= CGXRange(1, 2, TOT_CH_PER_MODULE, 2);
//	memset(m_DetalChColorFlag, 15, sizeof(m_DetalChColorFlag));		//Default color Setting Array[15]
//	m_wndChannelListGrid.m_pCustomColorFlag = (char *)m_DetalChColorFlag;

/*	m_wndChannelListGrid.SetValueRange(CGXRange(0,1),  "Ch No");
	m_wndChannelListGrid.SetValueRange(CGXRange(0,2),  "Cell 번호");
	m_wndChannelListGrid.SetValueRange(CGXRange(0,3),  "상태");
	m_wndChannelListGrid.SetValueRange(CGXRange(0,4),  "Code");
	m_wndChannelListGrid.SetValueRange(CGXRange(0,5),  "Grade");
	m_wndChannelListGrid.SetValueRange(CGXRange(0,6),  "시간(s)");
	m_wndChannelListGrid.SetValueRange(CGXRange(0,7),  "전압(mV)");
	m_wndChannelListGrid.SetValueRange(CGXRange(0,8),  "전류(㎃)");
	m_wndChannelListGrid.SetValueRange(CGXRange(0,9), "용량(㎃h)");
	m_wndChannelListGrid.SetValueRange(CGXRange(0,10), "임피던스(mΩ)");
	m_wndChannelListGrid.SetValueRange(CGXRange(0,11),  "Watt(㎽)");
	m_wndChannelListGrid.SetValueRange(CGXRange(0,12),  "Watt Hour(mWh)");
*/
	m_wndChannelListGrid.SetStyleRange(CGXRange().SetTable(), CGXStyle().SetFont(CGXFont().SetSize(11).SetBold(TRUE)).SetWrapText(FALSE));

	m_wndChannelListGrid.EnableGridToolTips();
    m_wndChannelListGrid.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, 
         CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));

//	m_wndChannelListGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);

//	CGXProgressCtrl* pProgressCtrl = new CGXProgressCtrl(&m_wndChannelListGrid);
//	m_wndChannelListGrid.RegisterControl(GX_IDS_CTRL_PROGRESS, pProgressCtrl);

/*	m_wndChannelListGrid.SetStyleRange(CGXRange(1, 2, 100, 2),
			CGXStyle()
				.SetControl(GX_IDS_CTRL_PROGRESS)
				.SetValue(50L)
				.SetUserAttribute(GX_IDS_UA_PROGRESS_MIN, _T("0"))
				.SetUserAttribute(GX_IDS_UA_PROGRESS_MAX, _T("100"))
				.SetUserAttribute(GX_IDS_UA_PROGRESS_CAPTIONMASK, _T("%ld %%"))  // sprintf formatting for value
				.SetTextColor(RGB(0, 0, 255))       // blue progress bar
				.SetInterior(RGB(255, 255, 255))    // on white background
	);
*/
}

void CResultView::OnTestConditonView() 
{
	// TODO: Add your control notification handler code here
//	if(m_resultFile.GetConditionMain() == NULL)	return;

	CDetailAutoTestConditionDlg *pDlg;
	pDlg = new CDetailAutoTestConditionDlg(m_resultFile.GetTestCondition());
	pDlg->m_pDoc = (CCTSAnalDoc *)GetDocument();
	pDlg->DoModal();
	
	delete pDlg;
	pDlg = NULL;
	
	/* kky
	CDetailTestConditionDlg *pDlg;
	pDlg = new CDetailTestConditionDlg(m_resultFile.GetTestCondition());

//	pDlg->m_Condition = m_resultFile.GetConditionMain();
	pDlg->m_pDoc = (CCTSAnalDoc *)GetDocument();
	pDlg->DoModal();
	delete pDlg;
	pDlg = NULL;
	*/


/*	FILE *fp = fopen("1.txt", "r");
	float data;
	CString strQuery, tmp, tem2;
	strQuery = "Insert into step_result (step_serial_no, channel_no, voltage, grade_code, fail_code) values ";
	for(int j =0; j<256; j++)
	{
		fscanf(fp, "%f", &data);
		tmp.Format(" (4854,  %d,  %.3f, 'A', 68)", j, data);
		tem2 = strQuery+tmp;
//		GetDocument()->ExecuteSQL(tem2);
		TRACE("%s;\n", tem2);
	}
	fclose(fp);
*/
}
/*
BOOL CResultView::ClearResult()
{

	CCTSAnalDoc *pDoc = (CCTSAnalDoc *)GetDocument();
	if(m_pConditionMain != NULL)
	{
		if(pDoc->RemoveCondition(m_pConditionMain))		//Free the Test Condition Memory
		{
			m_pConditionMain = NULL;
		}
	}
*/
//	LPEP_GROUP_INFO lpGroupInfo;
/*	LPEP_CH_DATA	lpChData;
	STR_STEP_RESULT * pStepData;

	for(int i = m_resultFile.GetStepSize()-1; i>= 0; i--)
	{
		if((pStepData = m_resultFile.GetStepData(i)) != NULL)
		{
			for(int j = pStepData->aChData.GetSize()-1; j>=0; j--)
			{
				if((lpChData = (EP_CH_DATA *)pStepData->aChData[j]) != NULL)
				{
					delete lpChData;
					lpChData = NULL;
					pStepData->aChData.RemoveAt(j);
				}
			}
			pStepData->aChData.RemoveAll();
		}
		delete pStepData;
		pStepData = NULL;
		m_stepData.RemoveAt(i);
	}	
	m_stepData.RemoveAll();

	return TRUE;
}
*/
void CResultView::OnDestroy() 
{
	CFormView::OnDestroy();
	
	// TODO: Add your message handler code here
//	ClearResult();
}

void CResultView::DisplayTestCondition()
{
	CCTSAnalDoc *pDoc = GetDocument();
	int nRow = m_wndStepListGrid.GetRowCount();
	if(nRow >0)
		m_wndStepListGrid.RemoveRows(1, nRow);
	
	m_wndStepListGrid.LockUpdate();
	int stepRow;
	CString strTemp;
	STR_TOP_CONFIG *topConfig = pDoc->GetTopConfig();
	ASSERT(topConfig);

//	if(m_resultFile.GetFileVersion() == atoi(RESULT_FILE_VER6))
//	{
		STR_STEP_RESULT *pStepData;
		// stepRow = m_resultFile.GetStepSize();
		// if(stepRow <= 0)	return;

		stepRow = m_resultFile.GetLastIndexStepNo();
		if(stepRow <= 0)	return;


		m_wndStepListGrid.InsertRows(1, stepRow);
		CString str;
		COleDateTime time;
		for(nRow =0; nRow<stepRow; nRow++)
		{
			pStepData = m_resultFile.GetStepData(nRow);
			if(pStepData == NULL)	continue;
			m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 1), (LONG)( pStepData->stepCondition.stepHeader.stepIndex+1));
//kbh		m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 1), (LONG)(nRow+1));
			m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 2), pDoc->GetProcTypeName(pStepData->stepCondition.stepHeader.nProcType, pStepData->stepCondition.stepHeader.type));
			time.ParseDateTime(pStepData->stepHead.szStartDateTime);
			if(time.GetStatus() == COleDateTime::valid)
				m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 7), time.Format("%m/%d %H:%M:%S"));
			time.ParseDateTime(pStepData->stepHead.szEndDateTime);
			if(time.GetStatus() == COleDateTime::valid)
				m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 8), time.Format("%m/%d %H:%M:%S"));
			m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 9), (LONG)pStepData->stepHead.nModuleID);

			BYTE colorFlag;
			pDoc->GetTypeMsg(pStepData->stepCondition.stepHeader.type, colorFlag);

			switch(pStepData->stepCondition.stepHeader.type)
			{
			case EP_TYPE_CHARGE:
				{
					m_wndStepListGrid.SetStyleRange(CGXRange(nRow+1, 2),
							CGXStyle().SetTextColor(topConfig->m_TStateColor[colorFlag]).SetInterior(topConfig->m_BStateColor[colorFlag]));
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 3), ModeTypeMsg( EP_TYPE_CHARGE, pStepData->stepCondition.stepHeader.mode));
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 4), pDoc->ValueString(pStepData->stepCondition.fVref, EP_VOLTAGE));
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 5), pDoc->ValueString(pStepData->stepCondition.fIref, EP_CURRENT));
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 6), pDoc->StepEndString(&pStepData->stepCondition));
					break;
				}
			case EP_TYPE_DISCHARGE:
				{
					m_wndStepListGrid.SetStyleRange(CGXRange(nRow+1, 2),
							CGXStyle().SetTextColor(topConfig->m_TStateColor[colorFlag]).SetInterior(topConfig->m_BStateColor[colorFlag]));
//					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 2), pDoc->StepTypeMsg(pStep->stepHeader.type));
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 3), ModeTypeMsg(EP_TYPE_DISCHARGE, pStepData->stepCondition.stepHeader.mode));
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 4), pDoc->ValueString(pStepData->stepCondition.fVref, EP_VOLTAGE));
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 5), pDoc->ValueString(pStepData->stepCondition.fIref, EP_CURRENT));
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 6), pDoc->StepEndString(&pStepData->stepCondition));
					break;
				}
			case EP_TYPE_REST:
				{
					m_wndStepListGrid.SetStyleRange(CGXRange(nRow+1, 2),
							CGXStyle().SetTextColor(topConfig->m_TStateColor[colorFlag]).SetInterior(topConfig->m_BStateColor[colorFlag]));
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 6), pDoc->StepEndString(&pStepData->stepCondition));
					break;
				}
			case EP_TYPE_OCV:
				{
					m_wndStepListGrid.SetStyleRange(CGXRange(nRow+1, 2),
							CGXStyle().SetTextColor(topConfig->m_TStateColor[colorFlag]).SetInterior(topConfig->m_BStateColor[colorFlag]));
					break;
				}
			case EP_TYPE_IMPEDANCE:
				{
					m_wndStepListGrid.SetStyleRange(CGXRange(nRow+1, 2),
							CGXStyle().SetTextColor(topConfig->m_TStateColor[colorFlag]).SetInterior(topConfig->m_BStateColor[colorFlag]));
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 3), ModeTypeMsg(EP_TYPE_IMPEDANCE, pStepData->stepCondition.stepHeader.mode));
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 4), pDoc->ValueString(pStepData->stepCondition.fVref, EP_VOLTAGE));
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 5), pDoc->ValueString(pStepData->stepCondition.fIref, EP_CURRENT));
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 6), pDoc->StepEndString(&pStepData->stepCondition));
					break;
				}
			case EP_TYPE_END:
				{
//					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 2), pDoc->StepTypeMsg(pStep->stepHeader.type));
					m_wndStepListGrid.SetStyleRange(CGXRange(nRow+1, 2),
							CGXStyle().SetTextColor(topConfig->m_TStateColor[colorFlag]).SetInterior(topConfig->m_BStateColor[colorFlag]));
					break;
				}
			case EP_TYPE_LOOP:
				{
					m_wndStepListGrid.SetStyleRange(CGXRange(nRow+1, 2),
							CGXStyle().SetTextColor(topConfig->m_TStateColor[colorFlag]).SetInterior(topConfig->m_BStateColor[colorFlag]));

					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 6), pDoc->StepEndString(&pStepData->stepCondition));
					break;
				}
			default:
				{
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 1), "Error Step");
					break;
				}
			}
		}
/*	}
	else
	{
		STR_CONDITION *pConditionMain = m_resultFile.GetConditionMain();
		if(pConditionMain == NULL)	return;

		stepRow = pConditionMain->apStepList.GetSize();
		if(stepRow <= 0)	return;
		m_wndStepListGrid.InsertRows(1, stepRow);

		LPEP_STEP_HEADER	lpStepHeader;

		for(nRow =0 ; nRow < stepRow; nRow++)
		{
			lpStepHeader = (EP_STEP_HEADER *)pConditionMain->apStepList[nRow];
			if(lpStepHeader == NULL) break;	

			switch(lpStepHeader->type)
			{
			case EP_TYPE_CHARGE:
				{
					EP_CHARGE_STEP *pStep = (EP_CHARGE_STEP *)pConditionMain->apStepList[nRow];
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 1), (LONG)(pStep->stepHeader.stepIndex+1));
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 2), StepTypeMsg(pStep->stepHeader.type));
					m_wndStepListGrid.SetStyleRange(CGXRange(nRow+1, 2),
							CGXStyle().SetTextColor(topConfig->m_TStateColor[11]).SetInterior(topConfig->m_BStateColor[11]));

					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 3), ModeTypeMsg(EP_TYPE_CHARGE, pStep->stepHeader.mode));
	//				strTemp.Format("%.3f", VTG_PRECISION(pStep->lVref));
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 4), pDoc->ValueString(pStep->lVref, EP_VOLTAGE));
	//				strTemp.Format("%.2f", CRT_PRECISION(pStep->lIref));
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 5), pDoc->ValueString(pStep->lIref, EP_CURRENT));
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 6), pDoc->StepEndString(pConditionMain->apStepList[nRow], lpStepHeader->type));
					break;
				}
			case EP_TYPE_DISCHARGE:
				{
					EP_DISCHARGE_STEP *pStep = (EP_DISCHARGE_STEP *)pConditionMain->apStepList[nRow];
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 1), (LONG)(pStep->stepHeader.stepIndex+1));
					m_wndStepListGrid.SetStyleRange(CGXRange(nRow+1, 2),
							CGXStyle().SetTextColor(topConfig->m_TStateColor[12]).SetInterior(topConfig->m_BStateColor[12]));
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 2), StepTypeMsg(pStep->stepHeader.type));
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 3), ModeTypeMsg(EP_TYPE_DISCHARGE, pStep->stepHeader.mode));
	//				strTemp.Format("%.3f", VTG_PRECISION(pStep->lVref));
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 4), pDoc->ValueString(pStep->lVref, EP_VOLTAGE));
	//				strTemp.Format("%.2f", CRT_PRECISION(pStep->lIref));
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 5), pDoc->ValueString(pStep->lIref, EP_CURRENT));
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 6), pDoc->StepEndString(pConditionMain->apStepList[nRow], lpStepHeader->type));
					break;
				}
			case EP_TYPE_REST:
				{
					EP_REST_STEP *pStep = (EP_REST_STEP *)pConditionMain->apStepList[nRow];
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 1), (LONG)(pStep->stepHeader.stepIndex+1));
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 2), StepTypeMsg(pStep->stepHeader.type));
					m_wndStepListGrid.SetStyleRange(CGXRange(nRow+1, 2),
							CGXStyle().SetTextColor(topConfig->m_TStateColor[13]).SetInterior(topConfig->m_BStateColor[13]));
	//				m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 3), (LONG)pStep->stepHeader.mode);
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 6), pDoc->StepEndString(pConditionMain->apStepList[nRow], lpStepHeader->type));
					break;
				}
			case EP_TYPE_OCV:
				{
					EP_OCV_STEP *pStep = (EP_OCV_STEP *)pConditionMain->apStepList[nRow];
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 1), (LONG)(pStep->stepHeader.stepIndex+1));
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 2), StepTypeMsg(pStep->stepHeader.type));
					m_wndStepListGrid.SetStyleRange(CGXRange(nRow+1, 2),
							CGXStyle().SetTextColor(topConfig->m_TStateColor[10]).SetInterior(topConfig->m_BStateColor[10]));
	//				m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 3), (LONG)pStep->stepHeader.mode);
	//				m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 6), "MakeEndString(nRow)");
					break;
				}
			case EP_TYPE_IMPEDANCE:
				{
					EP_IMPEDANCE_STEP *pStep = (EP_IMPEDANCE_STEP *)pConditionMain->apStepList[nRow];
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 1), (LONG)(pStep->stepHeader.stepIndex+1));
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 2), StepTypeMsg(pStep->stepHeader.type));
					m_wndStepListGrid.SetStyleRange(CGXRange(nRow+1, 2),
							CGXStyle().SetTextColor(topConfig->m_TStateColor[14]).SetInterior(topConfig->m_BStateColor[14]));
	//				m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 3), (LONG)pStep->stepHeader.mode);
				//	strTemp.Format("%.3f", VTG_PRECISION(pStep->lVref));
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 4), pDoc->ValueString(pStep->lVref, EP_VOLTAGE));
				//	strTemp.Format("%.2f", CRT_PRECISION(pStep->lIref));
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 5), pDoc->ValueString(pStep->lIref, EP_CURRENT));
	//				m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 6), "MakeEndString(nRow)");
					break;
				}
			case EP_TYPE_END:
				{
					EP_END_STEP *pStep = (EP_END_STEP *)pConditionMain->apStepList[nRow];
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 1), (LONG)(pStep->stepHeader.stepIndex+1));
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 2), StepTypeMsg(pStep->stepHeader.type));
					m_wndStepListGrid.SetStyleRange(CGXRange(nRow+1, 2),
							CGXStyle().SetTextColor(topConfig->m_TStateColor[7]).SetInterior(topConfig->m_BStateColor[7]));
	//				m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 3), (LONG)pStep->stepHeader.mode);
	//				m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 6), "MakeEndString(nRow)");
					break;
				}
			default:
				{
					m_wndStepListGrid.SetValueRange(CGXRange(nRow+1, 1), "Error Step");
					break;
				}
			}
		}
	}
*/
	m_wndStepListGrid.LockUpdate(FALSE);
	m_wndStepListGrid.Redraw();
}

BOOL CResultView::DisplayChannelData(int nStepIndex)
{
	LPSTR_SAVE_CH_DATA	lpChnnelData;
	STR_STEP_RESULT		*lpStepData;

	/*
	int size = m_resultFile.GetStepSize();
	if(size <= 0)	return FALSE;

	if( size <= nStepIndex)
	{
		AfxMessageBox("선택한 Step은 완료 되지 않았습니다.");
		return FALSE;
	}
	*/
	int size = m_resultFile.GetLastIndexStepNo();
	if( size <= 0 )
	{
		return FALSE;
	}

	if( size <= nStepIndex)
	{
		AfxMessageBox(TEXT_LANG[9]);//"선택한 Step은 완료 되지 않았습니다."
		return FALSE;
	}	

	CCTSAnalDoc *pDoc = GetDocument();
	ASSERT(nStepIndex >= 0 && nStepIndex < size);
	lpStepData = m_resultFile.GetStepData(nStepIndex);//(STR_STEP_RESULT *)m_stepData[nStepIndex];	
	if(lpStepData == NULL)	return FALSE;
	
	if( lpStepData->stepCondition.stepHeader.type == EP_TYPE_IMPEDANCE 
		&& lpStepData->stepCondition.stepHeader.nProcType == 0 )
	{
		if( m_DeviceMode != DEVICE_DCIR )
		{
			m_DeviceMode = DEVICE_DCIR;
			SendMessage(WM_SIZE, (WPARAM)SIZE_RESTORED, NULL); 				
		}
	}
	else
	{
		if( m_DeviceMode != DEVICE_FORMATION )
		{
			m_DeviceMode = DEVICE_FORMATION;
			SendMessage(WM_SIZE, (WPARAM)SIZE_RESTORED, NULL); 								
		}	
	}

	size = lpStepData->aChData.GetSize();

	int rowsize;
	int nTotCol = pDoc->m_nTrayColSize;

	rowsize = size/nTotCol;
	if(size%nTotCol > 0)	rowsize++;

	BYTE colorFlag;
	char szTemp[256];

	sprintf(szTemp, TEXT_LANG[10], m_strSelFileName.Mid(m_strSelFileName.ReverseFind('\\')+1));//"결과 파일 %s를 읽고 있습니다."
	pDoc->SetProgressWnd(0, 1000, szTemp);	

	m_ctrlStepSelCombo.SetCurSel(nStepIndex);

	if(m_resultFile.GetTotalCh() != size)	
	{
		sprintf(szTemp, TEXT_LANG[11], m_strSelFileName);//"%s을 읽는 중 오류가 발생 하였습니다.(채널 수 오류)"
		AfxMessageBox(szTemp);
		return FALSE;
	}
	
/////////////	File을 읽은 후 추가 할 필요성이 있다.
	int nCount = m_wndChannelListGrid.GetRowCount();
	if(nCount < size)
	{
		m_wndChannelListGrid.InsertRows(nCount+1, size - nCount);
	}
	else if(nCount > size)
	{
		m_wndChannelListGrid.RemoveRows(size+1, nCount);
	}
///////////////
	m_nCurrentStepIndex = nStepIndex;

	//OCV 일경우 용량 Data에 전압 보정값이 들어 있다.
/*	if(lpStepData->stepCondition.stepHeader.type == EP_TYPE_OCV)
	{
		m_wndChannelListGrid.SetValueRange(CGXRange(0,10), "보정전압(mV)");
		m_wndMinMaxGrid.SetValueRange(CGXRange(0,8), "보정전압(mV)");
	}
	else
	{
		m_wndChannelListGrid.SetValueRange(CGXRange(0,10), "임피던스(mΩ)");
		m_wndMinMaxGrid.SetValueRange(CGXRange(0,8), "Imp(mΩ)");
	}
*/
	BOOL bContinueChNo = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "Continue ChNo", 0);

	CString strTemp;
	for(int i =0; i<size ; i++)		
	{
		pDoc->SetProgressPos(int(1000.0f/(float)size*(float)i));
		nCount = i+1;
		lpChnnelData = (STR_SAVE_CH_DATA *)lpStepData->aChData.GetAt(i);
		ASSERT(lpChnnelData);

#ifdef DSP_TYPE_VERTICAL
		sprintf(szTemp, "%03d (%c-%2d)", lpChnnelData->wChIndex+1, 'A'+lpChnnelData->wChIndex/rowsize, lpChnnelData->wChIndex%rowsize+1);
#else if
		// sprintf(szTemp, "%03d (%c-%2d)", lpChnnelData->wChIndex+1, 'A'+lpChnnelData->wChIndex/nTotCol, lpChnnelData->wChIndex%nTotCol+1);
		sprintf(szTemp, "%02d", lpChnnelData->wChIndex+1 );
#endif
		m_wndChannelListGrid.SetValueRange(CGXRange(nCount, 0), (ROWCOL) (lpChnnelData->wChIndex+1));	//Module and Group Number
		m_wndChannelListGrid.SetValueRange(CGXRange(nCount, 1), szTemp);	//Module and Group Number

		//2007/2/15 NonCell은 Data를 표기 하지 않음 
		if(pDoc->m_bHideNonCellData && (lpChnnelData->channelCode == EP_CODE_CELL_NONE || 
			lpChnnelData->channelCode == EP_CODE_NONCELL 
			//|| lpChnnelData->channelCode == EP_CODE_NONCELL_CONTACT //20210306KSJ
			))
		{
			m_wndChannelListGrid.SetValueRange(CGXRange(nCount, 2, nCount, m_wndChannelListGrid.GetColCount()), "");
			m_wndChannelListGrid.SetStyleRange(CGXRange(nCount, 3), CGXStyle().SetTextColor(RGB(0,0,0)).SetInterior(RGB(255,255,255)));
			m_wndChannelListGrid.SetStyleRange(CGXRange(nCount, 4), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(pDoc->ChCodeMsg(lpChnnelData->channelCode, TRUE))));
		}
		else
		{
			sprintf(szTemp, "");
			strTemp = m_resultFile.GetCellSerial(lpChnnelData->wChIndex);
			if(strTemp.GetLength() > 3)
			{
				sprintf(szTemp, "%s", strTemp);
				//if(bContinueChNo)	sprintf(szTemp, "%d :: %s", m_resultFile.GetExtraData()->nCellNo+i, strTemp);
			}
			else
			{
				// sprintf(szTemp, "%d", m_resultFile.GetExtraData()->nCellNo+i);
				// if(bContinueChNo)	sprintf(szTemp, "%d", m_resultFile.GetExtraData()->nCellNo+i);
			}
			m_wndChannelListGrid.SetValueRange(CGXRange(nCount, 2), szTemp);

#ifdef _DEBUG
			sprintf(szTemp, "%s(%d)", pDoc->GetStateMsg(lpChnnelData->state, colorFlag), lpChnnelData->state);
			m_wndChannelListGrid.SetValueRange(CGXRange(nCount, 3),  szTemp);	
#else
			m_wndChannelListGrid.SetValueRange(CGXRange(nCount, 3),  pDoc->GetStateMsg(lpChnnelData->state, colorFlag));
#endif
	//		pDoc->GetStateMsg(lpChnnelData->state, colorFlag);		//Get Color Flag
			m_wndChannelListGrid.SetStyleRange(CGXRange(nCount, 3), 
									CGXStyle().SetTextColor(m_pConfig->m_TStateColor[colorFlag])
											  .SetInterior(m_pConfig->m_BStateColor[colorFlag]));
			
			if(IsNormalCell(lpChnnelData->channelCode))	
			{	//정상 cell
				m_wndChannelListGrid.SetStyleRange(CGXRange(nCount, 4), 
										CGXStyle().SetTextColor(0).SetInterior(RGB(255,255,255)));
			}
			else	//불량 Cell
			{
				m_wndChannelListGrid.SetStyleRange(CGXRange(nCount, 4), 
										CGXStyle().SetTextColor(m_pConfig->m_TStateColor[colorFlag])
												  .SetInterior(m_pConfig->m_BStateColor[colorFlag]));
			}
			
			m_wndChannelListGrid.SetValueRange(CGXRange(nCount, 4), pDoc->ChCodeMsg(lpChnnelData->channelCode));
			m_wndChannelListGrid.SetStyleRange(CGXRange(nCount, 4), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(pDoc->ChCodeMsg(lpChnnelData->channelCode, TRUE))));
			
			TRACE("CH %d:%d\n", i+1, lpChnnelData->grade);
			m_wndChannelListGrid.SetValueRange(CGXRange(nCount, 5), pDoc->ValueString(lpChnnelData->grade, EP_GRADE_CODE));
			m_wndChannelListGrid.SetStyleRange(CGXRange(nCount, 5), CGXStyle().SetTextColor(RGB(0,0,0)).SetInterior(pDoc->GetGradeColor(pDoc->ValueString(lpChnnelData->grade, EP_GRADE_CODE))));

			m_wndChannelListGrid.SetValueRange(CGXRange(nCount, 6),  pDoc->ValueString(lpChnnelData->fStepTime, EP_STEP_TIME));
			m_wndChannelListGrid.SetValueRange(CGXRange(nCount, 7),  pDoc->ValueString(lpChnnelData->fVoltage, EP_VOLTAGE));
			m_wndChannelListGrid.SetValueRange(CGXRange(nCount, 8),  pDoc->ValueString(lpChnnelData->fCurrent, EP_CURRENT));
			m_wndChannelListGrid.SetValueRange(CGXRange(nCount, 9),  pDoc->ValueString(lpChnnelData->fCapacity, EP_CAPACITY));
			m_wndChannelListGrid.SetValueRange(CGXRange(nCount, 10), pDoc->ValueString(lpChnnelData->fImpedance, EP_IMPEDANCE));
			m_wndChannelListGrid.SetValueRange(CGXRange(nCount, 11),  pDoc->ValueString(lpChnnelData->fWatt, EP_WATT));
			m_wndChannelListGrid.SetValueRange(CGXRange(nCount, 12),  pDoc->ValueString(lpChnnelData->fWattHour, EP_WATT_HOUR));
			m_wndChannelListGrid.SetValueRange(CGXRange(nCount, 13),  pDoc->ValueString(lpChnnelData->fAuxData[0], EP_TEMPER));
			m_wndChannelListGrid.SetValueRange(CGXRange(nCount, 14),  pDoc->ValueString(lpChnnelData->fTimeGetChargeVoltage, EP_VOLTAGE));
			
			m_wndChannelListGrid.SetValueRange(CGXRange(nCount, 15),  pDoc->ValueString(lpChnnelData->fComDCIR, EP_IMPEDANCE));
			m_wndChannelListGrid.SetValueRange(CGXRange(nCount, 16),  pDoc->ValueString(lpChnnelData->fComCapacity, EP_CAPACITY));
			m_wndChannelListGrid.SetValueRange(CGXRange(nCount, 17),  pDoc->ValueString(lpChnnelData->fCcRunTime, EP_STEP_TIME));
			m_wndChannelListGrid.SetValueRange(CGXRange(nCount, 18),  pDoc->ValueString(lpChnnelData->fImpedance, EP_IMPEDANCE));
			m_wndChannelListGrid.SetValueRange(CGXRange(nCount, 19),  pDoc->ValueString(lpChnnelData->fDCIR_V1, EP_VOLTAGE));
			m_wndChannelListGrid.SetValueRange(CGXRange(nCount, 20),  pDoc->ValueString(lpChnnelData->fDCIR_V2, EP_VOLTAGE));
			m_wndChannelListGrid.SetValueRange(CGXRange(nCount, 21),  pDoc->ValueString(lpChnnelData->fDCIR_AvgTemp, EP_TEMPER));
			m_wndChannelListGrid.SetValueRange(CGXRange(nCount, 22),  pDoc->ValueString(lpChnnelData->fDCIR_AvgCurrent, EP_CURRENT));

			//20210223 ksj
			m_wndChannelListGrid.SetValueRange(CGXRange(nCount, 23),  pDoc->ValueString(lpChnnelData->fDeltaOCV, EP_VOLTAGE));
			m_wndChannelListGrid.SetValueRange(CGXRange(nCount, 24),  pDoc->ValueString(lpChnnelData->fStartVoltage, EP_VOLTAGE));
			m_wndChannelListGrid.SetValueRange(CGXRange(nCount, 25),  pDoc->ValueString(lpChnnelData->fDeltaCapacity, EP_DELTA_CAPACITY));
		}	
	}

	m_wndChannelListGrid.Redraw();

	DisplayMinMaxGrid(nStepIndex);
	pDoc->HideProgressWnd();
	return TRUE;
}

/*
STR_STEP_RESULT *CResultView::GetStepData(UINT nStepIndex)
{
	STR_STEP_RESULT *pStep = NULL;
	int nStepSize = m_stepData.GetSize();
	if(nStepIndex >= nStepSize)	return pStep;
	return (STR_STEP_RESULT *)m_stepData[nStepIndex];
}
*/
void CResultView::OnSelchangeCombo1() 
{
	// TODO: Add your control notification handler code here
	if(m_nDisplayMode == TYPE_CHANNEL_LIST)
		DisplayChannelData(m_ctrlStepSelCombo.GetCurSel());
	else if(m_nDisplayMode == TYPE_TRAY_LIST)
		DisplayTrayChData(m_ctrlStepSelCombo.GetCurSel());
}

void CResultView::OnSize(UINT nType, int cx, int cy) 
{
	CFormView::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	if(::IsWindow(this->GetSafeHwnd()))
	{
		CFormView::ShowScrollBar(SB_HORZ, FALSE);
		CFormView::ShowScrollBar(SB_VERT, FALSE);
		RECT rect;
		::GetClientRect(m_hWnd, &rect);

		CRect rectGrid;
		float width;
		if(m_wndStepListGrid.GetSafeHwnd())
		{
			m_wndStepListGrid.GetWindowRect(rectGrid);	//Step Grid Window Position
			ScreenToClient(&rectGrid);
			m_wndStepListGrid.MoveWindow(rectGrid.left, rectGrid.top, rect.right-rectGrid.left-1, rectGrid.Height(), FALSE);

			m_wndStepListGrid.GetClientRect(rectGrid);
			width = (float)rectGrid.Width()/100.0f;
			m_wndStepListGrid.m_nWidth[1]	= int(width* 8.0f);
			m_wndStepListGrid.m_nWidth[2]	= int(width* 15.0f);
			m_wndStepListGrid.m_nWidth[3]	= 0;//int(width* 18.0f);
			m_wndStepListGrid.m_nWidth[4]	= int(width* 13.0f);
			m_wndStepListGrid.m_nWidth[5]	= int(width* 13.0f);	
			m_wndStepListGrid.m_nWidth[6]	= int(width* 20.0f);
			m_wndStepListGrid.m_nWidth[7]	= int(width* 16.0f);	
			m_wndStepListGrid.m_nWidth[8]	= int(width* 16.0f);
			m_wndStepListGrid.m_nWidth[9]	= 0;//int(width* 8.0f);	
			m_wndStepListGrid.m_nWidth[10]	= 0;//int(width* 18.0f);	

			m_wndStepListGrid.Redraw();
		}

		if(m_wndChannelListGrid.GetSafeHwnd())
		{
			m_wndChannelListGrid.GetWindowRect(rectGrid);	//Step Grid Window Position
			ScreenToClient(&rectGrid);
			m_wndChannelListGrid.MoveWindow(rectGrid.left, rectGrid.top, rect.right-rectGrid.left, rect.bottom-rectGrid.top-5, FALSE);
			
			m_wndChannelListGrid.GetClientRect(rectGrid);
			width = (float)rectGrid.Width()/100.0f;
			
			TRACE("====> %f\n", width);
			
			if( m_DeviceMode == DEVICE_FORMATION )
			{
				m_wndChannelListGrid.m_nWidth[1]	= int(width* 7.0f);			//Ch No 8
				m_wndChannelListGrid.m_nWidth[2]	= int(width* 11.0f);		//Cell No 12
				m_wndChannelListGrid.m_nWidth[3]	= int(width* 7.0f);			//State 8
				m_wndChannelListGrid.m_nWidth[4]	= int(width* 8.0f);		//Code 10
				m_wndChannelListGrid.m_nWidth[5]	= 0; //int(width* 5.0f);	//Grade
				m_wndChannelListGrid.m_nWidth[6]	= int(width* 9.0f);			//Step Time(s) 12
				m_wndChannelListGrid.m_nWidth[7]	= int(width* 8.0f);			//Voltage(V) 10
				m_wndChannelListGrid.m_nWidth[8]	= int(width* 8.0f);			//Current(㎃) 10
				m_wndChannelListGrid.m_nWidth[9]	= int(width* 8.0f);			//Capacity(㎃h) 10
				m_wndChannelListGrid.m_nWidth[10]	= 0; //int(width* 7.0f);	//Imp(mΩ)
				m_wndChannelListGrid.m_nWidth[11]	= 0; //int(width* 7.0f);	//Watt(㎽)
				m_wndChannelListGrid.m_nWidth[12]	= 0; //int(width* 7.0f);	//Watt Hour(mWh)
				m_wndChannelListGrid.m_nWidth[13]	= 0; //int(width* 7.0f);	//온도		
				m_wndChannelListGrid.m_nWidth[14]	= int(width* 8.0f);	//5분전류			
				m_wndChannelListGrid.m_nWidth[15]	= 0; //int(width* 8.0f);	//전압(T)			
				m_wndChannelListGrid.m_nWidth[16]	= 0; //int(width* 8.0f);	//전류(T)			
				m_wndChannelListGrid.m_nWidth[17]	= int(width* 9.0f);	//CC시간			
				m_wndChannelListGrid.m_nWidth[18]	= 0; //int(width* 7.0f);	//DCIR			
				m_wndChannelListGrid.m_nWidth[19]	= 0; //int(width* 7.0f);	//V1			
				m_wndChannelListGrid.m_nWidth[20]	= 0; //int(width* 7.0f);	//V2			
				m_wndChannelListGrid.m_nWidth[21]	= 0; //int(width* 7.0f);	//Temp			
				m_wndChannelListGrid.m_nWidth[22]	= 0; //int(width* 7.0f);	//DCIR 평균전류		
				//20210225 ksj					
				m_wndChannelListGrid.m_nWidth[23]	= int(width* 9.0f);	        // Delta OCV
				m_wndChannelListGrid.m_nWidth[24]	= int(width* 9.0f);	        // fStartVoltage
				m_wndChannelListGrid.m_nWidth[25]	= 0; //int(width* 7.0f);	//Delta Cpacity	
			}
			else if( m_DeviceMode == DEVICE_DCIR )
			{
				m_wndChannelListGrid.m_nWidth[1]	= int(width* 8.0f);			//Ch No
				m_wndChannelListGrid.m_nWidth[2]	= int(width* 11.0f);		//Cell No
				m_wndChannelListGrid.m_nWidth[3]	= int(width* 8.0f);			//State
				m_wndChannelListGrid.m_nWidth[4]	= int(width* 8.0f);			//Code
				m_wndChannelListGrid.m_nWidth[5]	= 0; //int(width* 5.0f);	//Grade
				m_wndChannelListGrid.m_nWidth[6]	= int(width* 10.0f);		//Step Time(s)
				m_wndChannelListGrid.m_nWidth[7]	= 0; //int(width* 8.0f);	//Voltage(V)
				m_wndChannelListGrid.m_nWidth[8]	= 0; //int(width* 8.0f);	//Current(㎃)
				m_wndChannelListGrid.m_nWidth[9]	= 0; //int(width* 8.0f);	//Capacity(㎃h)
				m_wndChannelListGrid.m_nWidth[10]	= 0; //int(width* 7.0f);	//Imp(mΩ)
				m_wndChannelListGrid.m_nWidth[11]	= 0; //int(width* 7.0f);	//Watt(㎽)
				m_wndChannelListGrid.m_nWidth[12]	= 0; //int(width* 7.0f);	//Watt Hour(mWh)
				m_wndChannelListGrid.m_nWidth[13]	= 0; //int(width* 7.0f);	//온도
				m_wndChannelListGrid.m_nWidth[14]	= 0; //int(width* 8.0f);	//5분전류			
				m_wndChannelListGrid.m_nWidth[15]	= 0; //int(width* 10.0f); //int(width* 8.0f);	//Com.DCIR			
				m_wndChannelListGrid.m_nWidth[16]	= 0; //int(width* 8.0f);	//Com.Capacity			
				m_wndChannelListGrid.m_nWidth[17]	= 0; //int(width* 9.0f);	//CC시간			
				m_wndChannelListGrid.m_nWidth[18]	= int(width* 9.0f); //int(width* 7.0f);	//DCIR			
				m_wndChannelListGrid.m_nWidth[19]	= int(width* 9.0f); //int(width* 7.0f);	//V1			
				m_wndChannelListGrid.m_nWidth[20]	= int(width* 9.0f); //int(width* 7.0f);	//V2			
				m_wndChannelListGrid.m_nWidth[21]	= int(width* 9.0f); //int(width* 7.0f);	//Temp			
				m_wndChannelListGrid.m_nWidth[22]	= int(width* 12.0f); //int(width* 7.0f);	//DCIR 평균전류				
				//20210225 ksj
			}	
			m_wndChannelListGrid.Redraw();																
		}

		if(m_wndCellListGrid.GetSafeHwnd())
		{
			m_wndCellListGrid.GetWindowRect(rectGrid);	//Step Grid Window Position
			ScreenToClient(&rectGrid);
			m_wndCellListGrid.MoveWindow(rectGrid.left, rectGrid.top, rect.right-rectGrid.left-5, rect.bottom-rectGrid.top-5, FALSE);																	
		}
		
		if(m_wndTrayChGrid.GetSafeHwnd())
		{
			m_wndTrayChGrid.GetWindowRect(rectGrid);	//Step Grid Window Position
			ScreenToClient(&rectGrid);
			m_wndTrayChGrid.MoveWindow(rectGrid.left, rectGrid.top, rect.right-rectGrid.left-5, rect.bottom-rectGrid.top-5, FALSE);																	

			m_wndTrayChGrid.GetClientRect(rectGrid);
			int nTotCol = m_wndTrayChGrid.GetColCount()/2;
			if(nTotCol > 0)
			{
				width = (float)rectGrid.Width()/nTotCol;
				for(int c = 0; c<nTotCol&&c<32; c++)
				{
					m_wndTrayChGrid.m_nWidth[c*2+1]	= int(width* 0.4f);
					m_wndTrayChGrid.m_nWidth[(c+1)*2]	= int(width* 0.6f);
				}
			}
		}

		if(m_wndMinMaxGrid.GetSafeHwnd())
		{
			m_wndMinMaxGrid.GetWindowRect(rectGrid);	//Step Grid Window Position
			ScreenToClient(&rectGrid);
			m_wndMinMaxGrid.MoveWindow(rectGrid.left, rectGrid.top, rect.right-rectGrid.left-10, rectGrid.Height(), FALSE);

			m_wndMinMaxGrid.GetClientRect(rectGrid);
			width = (float)rectGrid.Width()/100.0f;
			m_wndMinMaxGrid.m_nWidth[1]	= int(width* 12.0f);
			m_wndMinMaxGrid.m_nWidth[2]	= int(width* 28.0f);
			m_wndMinMaxGrid.m_nWidth[3]	= int(width* 15.0f);
			m_wndMinMaxGrid.m_nWidth[4]	= int(width* 15.0f);
			m_wndMinMaxGrid.m_nWidth[5]	= int(width* 15.0f);
			m_wndMinMaxGrid.m_nWidth[6]	= 0;//int(width* 12.0f);
			m_wndMinMaxGrid.m_nWidth[7]	= 0;//int(width* 15.0f);
			m_wndMinMaxGrid.m_nWidth[8]	= int(width* 15.0f);
		}

		if(m_wndDataTree.GetSafeHwnd())
		{
			m_wndDataTree.GetWindowRect(rectGrid);	//Step Grid Window Position
			ScreenToClient(&rectGrid);
			m_wndDataTree.MoveWindow(rectGrid.left, rectGrid.top, rectGrid.Width(), rect.bottom-rectGrid.top-10, FALSE);
		}
	}
}

LRESULT CResultView::OnMovedCurrentCell(WPARAM wParam, LPARAM lParam)
{
	ROWCOL nRow, nCol;
	
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;
	nCol = LOWORD(wParam);
	nRow = HIWORD(wParam);
	if(nRow<1 || nCol <1)		return 0;
	if(pGrid == &m_wndStepListGrid)
	{
		if(m_nDisplayMode == TYPE_CHANNEL_LIST)
			DisplayChannelData(nRow-1);
	}
	return 1;
}

LRESULT CResultView::OnGridDoubleClick(WPARAM wParam, LPARAM lParam)
{
	ROWCOL nRow, nCol;
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;
	nCol = LOWORD(wParam);
	nRow = HIWORD(wParam);
	if(nRow<1 || nCol <1)		return 0;
	if(m_strSelFileName.IsEmpty())	return 0;

/* 20130826 SKI 에서는 사용안함
	if(pGrid == &m_wndChannelListGrid)
	{
		int nGroupIndex = m_resultFile.GetGroupIndex();
		CChResultViewDlg	*pDlg;
		pDlg = new CChResultViewDlg;
		pDlg->m_pResultFile = &m_resultFile;
		pDlg->m_strModule = m_resultFile.GetModelName();
		pDlg->m_pDoc = (CCTSAnalDoc *)GetDocument();
		pDlg->m_pConfig = m_pConfig;

		int nChannelID = atol(pGrid->GetValueRowCol(nRow, 0));
		if(nChannelID > 0 && nChannelID <= m_resultFile.GetTotalCh())
		{
			pDlg->m_nChannelIndex = nChannelID-1;
			pDlg->DoModal();
		}
		delete pDlg;
		pDlg = NULL;
	}
*/
	if(pGrid == &m_wndCellListGrid || pGrid == &m_wndTrayChGrid)
	{
		STR_STEP_RESULT *lpStepData = m_resultFile.GetLastStepData();
		if(lpStepData)
		{
			CCellGradeListDlg dlg(GetDocument(), this);
			dlg.m_nChannelSize = lpStepData->aChData.GetSize();
			dlg.m_nTrayType = m_resultFile.GetMDSysData()->wTrayType;
			dlg.m_strLotNo = m_resultFile.GetLotNo();
			dlg.m_strTrayNo = m_resultFile.GetTrayNo();
			STR_SAVE_CH_DATA	*lpChnnelData;
			for(int i = 0; i<lpStepData->aChData.GetSize(); i++)
			{
				lpChnnelData = (STR_SAVE_CH_DATA *)lpStepData->aChData[i];
				if(lpChnnelData == NULL)	return 0; 
				dlg.cellState[i] = lpChnnelData->grade;
			}
			dlg.DoModal();
		}
	}

	if(pGrid == &m_wndMinMaxGrid)
	{
		if(nRow == 1 || nRow == 2)
		{
			CString strTemp, strTemp2;
			strTemp = pGrid->GetValueRowCol(nRow, nCol);
			if(!strTemp.IsEmpty())
			{
				strTemp = strTemp.Mid(strTemp.ReverseFind('(')+4); 
				strTemp = strTemp.Left(strTemp.GetLength()-1);
				for(int i = 0; i<m_wndChannelListGrid.GetRowCount(); i++)
				{
					strTemp2 =  m_wndChannelListGrid.GetValueRowCol(i+1, 2);
					if(strTemp == m_wndChannelListGrid.GetValueRowCol(i+1, 2))
					{
						m_wndChannelListGrid.SetCurrentCell(i+1, 2);
						break;
					}
				}
			}
		}
	}
	return 1;
}

//Contol Context Menu를 보여 준다.
LRESULT CResultView::OnRButtonClickedRowCol(WPARAM wParam, LPARAM lParam)
{
/*	ROWCOL nRow, nCol;
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;
	nCol = LOWORD(wParam);
	nRow = HIWORD(wParam);
	
	ASSERT(pGrid);
	if(nCol<2 || nRow < 1)	return 0;
	
	CPoint point;
	::GetCursorPos(&point);
	
	if (point.x == -1 && point.y == -1)
	{
		//keystroke invocation
		CRect rect;
		pGrid->GetClientRect(rect);
		pGrid->ClientToScreen(rect);

		point = rect.TopLeft();
		point.Offset(5, 5);
	}

	CMenu menu, *pPopup;

/*	if(pGrid == (CMyGridWnd *)&m_wndCellListGrid)
	{
		m_nCmdTarget = EP_GROUP_CMD;
		VERIFY(menu.LoadMenu(IDR_CONTEXT1));
	}
	else if(pGrid == (CMyGridWnd *)&m_wndTopGrid)
	{
		VERIFY(menu.LoadMenu(IDR_CONTEXT2));
	}
*/
	
/*	VERIFY(menu.LoadMenu(IDR_CONTEXT2));
	pPopup = menu.GetSubMenu(0);
	ASSERT(pPopup != NULL);
	
	CWnd* pWndPopupOwner = this;
	while (pWndPopupOwner->GetStyle() & WS_CHILD)
		pWndPopupOwner = pWndPopupOwner->GetParent();

	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, pWndPopupOwner);
*/	
	CMenu menu, *popmenu; 
	VERIFY(menu.LoadMenu(IDR_MAINFRAME)); 
	popmenu = menu.GetSubMenu(1); 
	CPoint ptMouse; 
	GetCursorPos(&ptMouse); 
	popmenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, ptMouse.x, ptMouse.y, AfxGetMainWnd()); 				
	
	return 1;
}


void CResultView::OnRltFileOpen() 
{
//	if(m_gBeLoding)		return;

//	GetDocument()->ResultFileOpen();
//	return;
	CResultFileLoadDlg *pDlg;
	pDlg = new CResultFileLoadDlg(this);
	ASSERT(pDlg);
	CString strFileName;

	static CString strLotFilter("");
	static CString strTrayFilter("");
	static CString strSelFile("");
	
	if(m_strSelFolder.IsEmpty())
		pDlg->m_strFileFolder = GetDocument()->m_strDataFolder;
	else
		pDlg->m_strFileFolder = m_strSelFolder;	

	pDlg->m_strSelFileName = strSelFile;
	pDlg->m_strLotFilter = strLotFilter;
	pDlg->m_strTrayFilter = strTrayFilter;
	pDlg->m_pDoc =  GetDocument();

	if(pDlg->DoModal() != IDOK)
	{
		m_strSelFolder = pDlg->m_strFileFolder;
		delete pDlg;
		pDlg = NULL;
		return;
	}

	m_strSelFolder = pDlg->m_strFileFolder;
	m_strSelFileName = pDlg->GetSelFileName();

	strLotFilter = pDlg->m_strLotFilter;
	strTrayFilter = pDlg->m_strTrayFilter;
	strSelFile =  pDlg->m_strSelFileName;

	delete pDlg;
	pDlg = NULL;
	
	if(m_strSelFileName.IsEmpty())
	{
		AfxMessageBox(TEXT_LANG[12]);//"선택한 파일이 없습니다."
		return;
	}

	OnResultFileOpen();
}

void CResultView::OnUpdateFileSaveAs(CCmdUI* /*pCmdUI*/) 
{
	// TODO: Add your command update UI handler code here
/*	if(m_pTestResult)
		pCmdUI->Enable(TRUE);
	else
		pCmdUI->Enable(FALSE);
*/
}

void CResultView::OnFileSaveAs() 
{
	// TODO: Add your command handler code here
	OnFormatSetting();
}

void CResultView::OnReload() 
{
	// TODO: Add your control notification handler code here
	LoadDataTree();
//	OnResultFileOpen();
}

void CResultView::OnFormatSetting() 
{	
	// TODO: Add your control notification handler code here
	if(!m_resultFile.IsLoaded())
	{
		AfxMessageBox(TEXT_LANG[13]);//"Load된 파일이 없습니다."
		return;
	}

	//2004/2/20 CoverFile.exe를 호출하여 처리//////////////////////
	CString strTemp;

	STARTUPINFO	stStartUpInfo;
	PROCESS_INFORMATION	ProcessInfo;
	ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
	ZeroMemory(&ProcessInfo, sizeof(PROCESS_INFORMATION));
	
	stStartUpInfo.cb = sizeof(STARTUPINFO);
	stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
	stStartUpInfo.wShowWindow = SW_NORMAL;

	char buff[512];
	GetShortPathName((LPSTR)(LPCTSTR)m_resultFile.GetFileName(), buff, 511);

	CString strDirTemp;
	{
		TCHAR szCurDir[MAX_PATH];
		::GetModuleFileName(AfxGetApp()->m_hInstance, szCurDir, MAX_PATH);
		strDirTemp = CString(szCurDir).Mid(0, CString(szCurDir).ReverseFind('\\'));
	}
	strTemp.Format("%s\\ExcelTrans.exe %s", strDirTemp, buff);	
	//strTemp.Format("%s\\ConvertFile.exe %s", GetDocument()->m_strCurFolder, buff);	
	BOOL bFlag = CreateProcess(NULL, (LPSTR)(LPCTSTR)strTemp, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &stStartUpInfo, &ProcessInfo);
	if(bFlag == FALSE)
	{
/*		LPVOID lpMsgBuf;
		FormatMessage( 
			FORMAT_MESSAGE_ALLOCATE_BUFFER | 
			FORMAT_MESSAGE_FROM_SYSTEM | 
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			GetLastError(),
			0, // Default language
			(LPTSTR) &lpMsgBuf,
			0,
			NULL 
		);
		// Process any inserts in lpMsgBuf.
		// ...
		// Display the string.
		MessageBox( (LPCTSTR)lpMsgBuf, "Error", MB_OK | MB_ICONINFORMATION );
		// Free the buffer.
		LocalFree( lpMsgBuf );
*/
		strTemp.Format(TEXT_LANG[14], GetDocument()->m_strCurFolder);	//IP를 인자로 넘김 //"%s\\ExcelTrans.exe를 찾을 수 없습니다."
		MessageBox(strTemp, TEXT_LANG[15], MB_OK|MB_ICONSTOP);//"실행오류"
	}
	return;
	////////////////////////////
	

/*	if(m_nDisplayMode == TYPE_CHANNEL_LIST)
	{
		CFileSaveDlg	*pDlg = NULL;
		pDlg = new CFileSaveDlg(this);
		pDlg->m_strOrgFileName = m_strSelFileName;

		if(pDlg->DoModal() == IDOK)
		{
			SaveCapEfficencyFile(pDlg);
			SaveChFileSave(pDlg);
			SaveStepFileSave(pDlg);
		}

		if(pDlg)
		{
			delete pDlg;
			pDlg = NULL;
		}
	}
	else 
	{
		CString strFileName(m_strSelFileName);
		
		strFileName.Format("%s.csv", m_strSelFileName.Left(m_strSelFileName.GetLength()-4));
		CFileDialog pDlg(FALSE, "", strFileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "csv 파일(*.csv)|*.csv|");
		if(IDOK == pDlg.DoModal())
		{
			strFileName = pDlg.GetPathName();
			SaveExelFile(strFileName);
		}
	}
*/
}

BOOL CResultView::SaveCapEfficencyFile(CFileSaveDlg *pDlg)
{

	if(pDlg == NULL)					return FALSE;
	if(pDlg->m_bCycleSave == FALSE)		return TRUE;

	ASSERT(!m_strSelFileName.IsEmpty());
	if(m_resultFile.GetStepSize() <= 0)		return FALSE;

	FILE	*fp = fopen(pDlg->m_strSaveFileName, "wt");

	if (fp == FALSE)					return FALSE;
	
	RESULT_FILE_HEADER *pHeader = m_resultFile.GetResultHeader();
	fprintf(fp, TEXT_LANG[16]+":, %s\n", (char *)(LPCTSTR)m_strSelFileName);	//"파일명"
	fprintf(fp, TEXT_LANG[17]+":,%s\n",  m_resultFile.GetModelData());			//"모델명"
	fprintf(fp, TEXT_LANG[18]+":,%s\n",  m_resultFile.GetTestName());		//"공정_조건명"
	fprintf(fp, TEXT_LANG[19]+":, %s\n",  pHeader->szTrayNo);			//"Tray ID"
	fprintf(fp, TEXT_LANG[20]+":, %s\n",  pHeader->szLotNo);			// Lot No//"Batch"
	fprintf(fp, TEXT_LANG[21]+"No:, "+TEXT_LANG[21]+" %d-%d\n",  pHeader->nModuleID, pHeader->wGroupIndex+1);// Module name //"모듈"
	fprintf(fp, TEXT_LANG[22]+":, %s\n",  pHeader->szDateTime);		//"시작_시각"
	fprintf(fp, TEXT_LANG[23]+":, %s\n\n",  pHeader->szOperatorID);//"운영자"
//	fprintf(fp, "설명:, %s\n",  m_pConditionMain->conditionHeader.szDescription);		// Battery No
	fprintf(fp, "Cycle No, Cha Cap(mAh), Cha Time, Dis Cap(mAh), Dis Time, Efficiency(%)\n");

/*	fprintf(fp, "파일명:, %s\n", (char *)(LPCTSTR)m_strSelFileName);	// FileName
	fprintf(fp, "모델명:,%s\n",  m_extData.modelData.szName);			// 모델명 	
	fprintf(fp, "공정_조건명:,%s\n",  m_pConditionMain->conditionHeader.szName);		// 시험조건명
	fprintf(fp, "Tray_No:, %s\n",  m_resultFileHeader.szTrayNo);			// Tray ID
	fprintf(fp, "Lot_No:, %s\n",  m_resultFileHeader.szLotNo);			// Lot No
	fprintf(fp, "모듈No:, 모듈 %d-%d\n",  m_resultFileHeader.nModuleID, m_resultFileHeader.nGroupIndex+1);					// Module name 
	fprintf(fp, "시작_시각:, %s\n",  m_resultFileHeader.szDateTime);		// 시작시간
	fprintf(fp, "운영자:, %s\n\n",  m_resultFileHeader.szOperatorID);					// 총시간
//	fprintf(fp, "설명:, %s\n",  m_pConditionMain->conditionHeader.szDescription);		// Battery No
	fprintf(fp, "Cycle No, Cha Cap(mAh), Cha Time, Dis Cap(mAh), Dis Time, Efficiency(%)\n");
*/



//	LPEP_GROUP_INFO	lpStep;
	LPSTR_SAVE_CH_DATA	lpChannel;
	STR_STEP_RESULT *lpStepData;
	lpStepData = m_resultFile.GetStepData(0);//(STR_STEP_RESULT *)GetStepData(0);
	ASSERT(lpStepData);
	int chSize = lpStepData->aChData.GetSize();
	int nStepSize = m_resultFile.GetStepSize();
	int nChIndex;
	int nCycleNumber;

	int nBeforState = EP_STATE_IDLE; 
	float fChargeCap =0.0f, fEffiency = 0.0f;

	char szTemp[512];
	for(int ch =0; ch < chSize; ch++)
	{
		nChIndex = -1;
		nCycleNumber = 0;
		for(int step =0; step<nStepSize; step++)
		{
			lpStepData = m_resultFile.GetStepData(step);
			ASSERT(lpStepData);
			if(ch >= lpStepData->aChData.GetSize())		break;	//각 Step의 Channel 수가 다를 경우

			lpChannel = (STR_SAVE_CH_DATA *)lpStepData->aChData[ch];
			ASSERT(lpChannel);
			if(nChIndex == -1)
			{
				nChIndex = lpChannel->wChIndex;
				fprintf(fp, "Channel %d\n", nChIndex);
			}
			ASSERT(lpChannel->wChIndex == nChIndex);					//각 Step의 channel 저장 순서가 똑 같은지 검사

			if(lpChannel->state == EP_STATE_CHARGE)
			{
				nCycleNumber++;
				if(nBeforState == EP_STATE_CHARGE)			//Charge->charge
				{
					fprintf(fp, "0.0, 0d 00:00:00.0, 0.0\n");
				}
				nBeforState = EP_STATE_CHARGE;
				fChargeCap =  lpChannel->fCapacity;
				ConvertTime(szTemp, lpChannel->fStepTime);
				fprintf(fp, "%d, %.1f, %s,", nCycleNumber, fChargeCap, szTemp);		// Step Time
			}
			
			if(lpChannel->state == EP_STATE_DISCHARGE)
			{
				ConvertTime(szTemp, lpChannel->fStepTime);
				if(nBeforState ==EP_STATE_CHARGE)			//Charge -> Discharge
				{
					if(fChargeCap <= 0.0f)
					{
						fEffiency = 0.0f;
					}
					else
					{
						fEffiency = (lpChannel->fCapacity)/fChargeCap*100.0f;	//Efficiency 
					}
					fprintf(fp, "%f, %s, %f\n", lpChannel->fCapacity, szTemp, fEffiency);	// Step Time
				}
				else										//Discharge->Discharge
				{
					nCycleNumber++;
					fprintf(fp, "%d, 0.0, 0d 00:00:00.0, %f, %s, 0.0,", nCycleNumber, lpChannel->fCapacity, szTemp);					// Step Time
				}
				nBeforState = EP_STATE_DISCHARGE;
			}
		}
	}
	fclose(fp);
	fp = NULL;
	return TRUE;
}

/*

BOOL CResultView::SaveChFileSave(CFileSaveDlg *pDlg)
{
	if(pDlg == NULL)	return FALSE;
	if(pDlg->m_bCannelSave == FALSE)	return TRUE;

	CCTSAnalDoc *pDoc = GetDocument();
	ASSERT(!m_strSelFileName.IsEmpty());
	if(m_stepResult.GetSize() <= 0)		return FALSE;

	FILE	*fp = fopen(pDlg->m_strChFileName, "wt");
	if (fp == FALSE)	return FALSE;

	CString temp;
	char szTemp[512];
	
	BeginWaitCursor();
	fprintf(fp, "파일명: %s\n", (char *)(LPCTSTR)m_strSelFileName);	// FileName
	fprintf(fp, "모듈No: 모듈 %d-%d\n",  m_resultFileHeader.nModuleID, m_resultFileHeader.nGroupIndex+1);					// Module name 
	fprintf(fp, "공정_조건명: %s\n",  m_pConditionMain->conditionHeader.szTestName);		// 시험조건명
	fprintf(fp, "설명: %s\n",  m_pConditionMain->conditionHeader.szDescription);		// Battery No
	fprintf(fp, "Batch: %s\n",  m_resultFileHeader.szLotNo);			// Lot No
	fprintf(fp, "시작_시각: %s\n",  m_resultFileHeader.szDateTime);		// 시작시간
	fprintf(fp, "운영자: %s\n\n",  m_resultFileHeader.szOperatorID);					// 총시간


	temp = "Ch";		fprintf(fp, "%4s", temp);
	temp = "Step";		fprintf(fp, "%4s", temp);	
	temp = "Type";		fprintf(fp, "%10s", temp);
	temp = "State";		if(pDlg->m_bChState)		fprintf(fp, "%10s", temp);	// 상태
	temp = "Time";		if(pDlg->m_bChTime)			fprintf(fp, "%15s", temp);	// Step Time
	temp = "Vtg";		if(pDlg->m_bChVoltage)		fprintf(fp, "%8s", temp);	// Voltage
	temp = "Crt";		if(pDlg->m_bChCurrent)		fprintf(fp, "%8s", temp);	// Current
	temp = "Cap";		if(pDlg->m_bChCapacity) 	fprintf(fp, "%8s", temp);	// Capacity
	temp = "Watt";		if(pDlg->m_bChWatt)			fprintf(fp, "%8s", temp);	// WattHour
	temp = "Imp";		if(pDlg->m_bChImpedance)	fprintf(fp, "%8s", temp);	// Capacity
	temp = "G";			if(pDlg->m_bChGrade)		fprintf(fp, "%4s", temp);
	temp = "Code";		if(pDlg->m_bChFailure)		fprintf(fp, " %s",	temp);	// Failure Code

	fprintf(fp, "\r\n");	// Failure Code

	LPEP_CH_DATA	lpChannel;
	LPEP_GROUP_INFO	lpStep;
	lpStep = (LPEP_GROUP_INFO)m_stepResult[0];
	ASSERT(lpStep);

	int chSize = lpStep->chData.GetSize();
	int nStepSize = m_stepResult.GetSize();
	BYTE color;
	int nTestStep = m_pConditionMain->apStepList.GetSize();
	LPEP_STEP_HEADER lpStepHeader;

	for(int ch = 0; ch < chSize; ch++)
	{
		for (int step = 0; step < nStepSize ; step++)
		{
			lpStep = (LPEP_GROUP_INFO)m_stepResult[step];
			ASSERT(lpStep);
			if(ch >= lpStep->chData.GetSize())		break;	//각 Step의 Channel 수가 다를 경우
			lpChannel = (LPEP_CH_DATA )lpStep->chData[ch];
			ASSERT(lpChannel);
			
			fprintf(fp, "%4d", lpChannel->wChIndex+1);
			fprintf(fp, "%4d", lpChannel->nStepNo+1);	
			if(step < nTestStep)
			{
				lpStepHeader = (LPEP_STEP_HEADER)m_pConditionMain->apStepList[step];
				fprintf(fp, "%10s", pDoc->StepTypeMsg(lpStepHeader->type));
			}
			else
			{
				fprintf(fp, "%10s", "Error");
			}
			
			if(pDlg->m_bChState)		fprintf(fp, "%10s", pDoc->GetStateMsg(lpChannel->state, color));	// 상태
			ConvertTime(szTemp, lpChannel->ulStepTime);
			if(pDlg->m_bChTime)			fprintf(fp, "%15s", szTemp);	// Step Time
			if(pDlg->m_bChVoltage)		fprintf(fp, "%8.3f", VTG_PRECISION(lpChannel->lVoltage));	// Voltage
			if(pDlg->m_bChCurrent)		fprintf(fp, "%8.1f", CRT_PRECISION(lpChannel->lCurrent));	// Current
			if(pDlg->m_bChCapacity) 	fprintf(fp, "%8.1f", ETC_PRECISION(lpChannel->lCapacity));	// Capacity
			if(pDlg->m_bChWatt)			fprintf(fp, "%8.1f", ETC_PRECISION(lpChannel->lWatt));	// WattHour
			if(pDlg->m_bChImpedance)	fprintf(fp, "%8.1f", ETC_PRECISION(lpChannel->lImpedance));	// Capacity
			if(pDlg->m_bChGrade)		fprintf(fp, "%4d", lpChannel->grade);
			if(pDlg->m_bChFailure)		fprintf(fp, " %s", pDoc->ChCodeMsg(lpChannel->channelCode));	// Failure Code
			fprintf(fp, "\r\n");	 
		}
		fprintf(fp, "\r\n");	
	}
	fclose(fp);
	fp = NULL;
	return TRUE;
}

BOOL CResultView::SaveStepFileSave(CFileSaveDlg *pDlg)
{
	if(pDlg == NULL)	return FALSE;
	if(pDlg->m_bProcSave == FALSE)	return TRUE;

	CCTSAnalDoc *pDoc = GetDocument();
	ASSERT(!m_strSelFileName.IsEmpty());
	if(m_stepResult.GetSize() <= 0)		return FALSE;

	FILE	*fp = fopen(pDlg->m_strStepFileName, "wt");
	if (fp == FALSE)	return FALSE;

	CString temp;
	char szTemp[512];
	
	BeginWaitCursor();
	fprintf(fp, "파일명: %s\n", (char *)(LPCTSTR)m_strSelFileName);				// FileName
	fprintf(fp, "모듈No: 모듈 %d-%d\n",  m_resultFileHeader.nModuleID, m_resultFileHeader.nGroupIndex+1);					// Module name 
	fprintf(fp, "공정조건명: %s\n",  m_pConditionMain->conditionHeader.szTestName);		// 시험조건명
	fprintf(fp, "설명: %s\n",  m_pConditionMain->conditionHeader.szDescription);		// Battery No
	fprintf(fp, "Batch: %s\n",  m_resultFileHeader.szLotNo);					// Lot No
	fprintf(fp, "시작_시각: %s\n",  m_resultFileHeader.szDateTime);				// 시작시간
	fprintf(fp, "운영자: %s\n\n",  m_resultFileHeader.szOperatorID);			// 총시간


	temp = "Ch";		fprintf(fp, "%4s", temp);
	temp = "Step";		fprintf(fp, "%4s", temp);	
	temp = "State";		if(pDlg->m_bChState)		fprintf(fp, "%10s", temp);	// 상태
	temp = "Time";		if(pDlg->m_bChTime)			fprintf(fp, "%15s", temp);	// Step Time
	temp = "Vtg";		if(pDlg->m_bChVoltage)		fprintf(fp, "%8s", temp);	// Voltage
	temp = "Crt";		if(pDlg->m_bChCurrent)		fprintf(fp, "%8s", temp);	// Current
	temp = "Cap";		if(pDlg->m_bChCapacity) 	fprintf(fp, "%8s", temp);	// Capacity
	temp = "Watt";		if(pDlg->m_bChWatt)			fprintf(fp, "%8s", temp);	// WattHour
	temp = "Imp";		if(pDlg->m_bChImpedance)	fprintf(fp, "%8s", temp);	// Capacity
	temp = "G";			if(pDlg->m_bChGrade)		fprintf(fp, "%4s", temp);
	temp = "Code";		if(pDlg->m_bChFailure)		fprintf(fp, " %s",	temp);	// Failure Code

	fprintf(fp, "\r\n");	// Failure Code

	LPEP_GROUP_INFO	lpStep;
	LPEP_CH_DATA	lpChannel;
	int nStepSize = m_stepResult.GetSize();
	int chSize;
	BYTE color;
	int nTestStep = m_pConditionMain->apStepList.GetSize();
	LPEP_STEP_HEADER lpStepHeader;

	for (int step = 0; step < nStepSize ; step++)
	{
		lpStep = (LPEP_GROUP_INFO)m_stepResult[step];
		ASSERT(lpStep);
		chSize = lpStep->chData.GetSize();

		if(step < nTestStep)
		{
			lpStepHeader = (LPEP_STEP_HEADER)m_pConditionMain->apStepList[step];
			fprintf(fp, "\nStep Type :%s\n", pDoc->StepTypeMsg(lpStepHeader->type));
		}
		else
		{
			fprintf(fp, "%s\n", "Error");
		}

		for(int ch =0; ch < chSize; ch++)
		{
			lpChannel = (EP_CH_DATA *)lpStep->chData[ch];
			ASSERT(lpChannel);
			
			fprintf(fp, "%4d", lpChannel->wChIndex+1);
			fprintf(fp, "%4d", lpChannel->nStepNo+1);
			if(pDlg->m_bChState)		fprintf(fp, "%10s", pDoc->GetStateMsg(lpChannel->state, color));	// 상태
			ConvertTime(szTemp, lpChannel->ulStepTime);
			if(pDlg->m_bChTime)			fprintf(fp, "%15s", szTemp);	// Step Time
			if(pDlg->m_bChVoltage)		fprintf(fp, "%8.3f", VTG_PRECISION(lpChannel->lVoltage));	// Voltage
			if(pDlg->m_bChCurrent)		fprintf(fp, "%8.1f", CRT_PRECISION(lpChannel->lCurrent));	// Current
			if(pDlg->m_bChCapacity) 	fprintf(fp, "%8.1f", ETC_PRECISION(lpChannel->lCapacity));	// Capacity
			if(pDlg->m_bChWatt)			fprintf(fp, "%8.1f", ETC_PRECISION(lpChannel->lWatt));	// WattHour
			if(pDlg->m_bChImpedance)	fprintf(fp, "%8.1f", ETC_PRECISION(lpChannel->lImpedance));	// Capacity
			if(pDlg->m_bChGrade)		fprintf(fp, "%4d", lpChannel->grade);
			if(pDlg->m_bChFailure)		fprintf(fp, " %s", pDoc->ChCodeMsg(lpChannel->channelCode));	// Failure Code
			fprintf(fp, "\r\n");	 
		}
		fprintf(fp, "\r\n");	
	}
	fclose(fp);
	fp = NULL;
	return TRUE;
}
*/
BOOL CResultView::SaveChFileSave(CFileSaveDlg *pDlg)
{
	if(pDlg == NULL)	return FALSE;
	if(pDlg->m_bCannelSave == FALSE)	return TRUE;

	CCTSAnalDoc *pDoc = GetDocument();
	ASSERT(!m_strSelFileName.IsEmpty());
	if(m_resultFile.GetStepSize() <= 0)		return FALSE;

	FILE	*fp = fopen(pDlg->m_strChFileName, "wt");
	if (fp == FALSE)	return FALSE;

	CString temp;
//	char szTemp[512];
	
	BeginWaitCursor();

	RESULT_FILE_HEADER *pHeader = m_resultFile.GetResultHeader();
	fprintf(fp, TEXT_LANG[16]+":, %s\n", (char *)(LPCTSTR)m_strSelFileName);	//"파일명"
	fprintf(fp, TEXT_LANG[17]+":,%s\n",  m_resultFile.GetModelName());			//"모델명"
	fprintf(fp, TEXT_LANG[18]+":,%s\n",  m_resultFile.GetTestName());		//"공정_조건명"
	fprintf(fp, TEXT_LANG[24]+":, %s\n",  pHeader->szTrayNo);		//"Tray_No"
	fprintf(fp, TEXT_LANG[20]+":, %s\n",  pHeader->szLotNo);			//"Batch"
	fprintf(fp, TEXT_LANG[21]+"No:, "+TEXT_LANG[21]+" %d-%d\n",  pHeader->nModuleID, pHeader->wGroupIndex+1);	//"모듈"
	fprintf(fp, TEXT_LANG[22]+":, %s\n",  pHeader->szDateTime); //"시작_시각"
	fprintf(fp, TEXT_LANG[23]+":, %s\n\n",  pHeader->szOperatorID);				//"운영자"
//	fprintf(fp, "설명:, %s\n",  m_pConditionMain->conditionHeader.szDescription);		// Battery No


	
/*	fprintf(fp, "파일명:, %s\n", (char *)(LPCTSTR)m_strSelFileName);	// FileName
	fprintf(fp, "모델명:,%s\n",  m_extData.modelData.szName);			// 모델명 	
	fprintf(fp, "공정_조건명:,%s\n",  m_pConditionMain->conditionHeader.szName);		// 시험조건명
	fprintf(fp, "Tray_No:, %s\n",  m_resultFileHeader.szTrayNo);		// Tray No
	fprintf(fp, "Lot_No:, %s\n",  m_resultFileHeader.szLotNo);			// Lot No
	fprintf(fp, "모듈No:, 모듈 %d-%d\n",  m_resultFileHeader.nModuleID, m_resultFileHeader.nGroupIndex+1);					// Module name 
	fprintf(fp, "시작_시각:, %s\n",  m_resultFileHeader.szDateTime);	// 시작시간
	fprintf(fp, "운영자:, %s\n\n",  m_resultFileHeader.szOperatorID);					// 총시간
//	fprintf(fp, "설명:, %s\n",  m_pConditionMain->conditionHeader.szDescription);		// Battery No
*/

	temp = "Ch,";		fprintf(fp, "%4s,", temp);
	temp = "Step,";		fprintf(fp, "%4s,", temp);	
	temp = "Type,";		fprintf(fp, "%10s,", temp);
	temp = "State,";		if(pDlg->m_bChState)		fprintf(fp, "%10s,", temp);	// 상태
	temp = "Time,";		if(pDlg->m_bChTime)			fprintf(fp, "%15s,", temp);	// Step Time
	temp = "Vtg,";		if(pDlg->m_bChVoltage)		fprintf(fp, "%8s,", temp);	// Voltage
	temp = "Crt,";		if(pDlg->m_bChCurrent)		fprintf(fp, "%8s,", temp);	// Current
	temp = "Cap,";		if(pDlg->m_bChCapacity) 	fprintf(fp, "%8s,", temp);	// Capacity
	temp = "Watt,";		if(pDlg->m_bChWatt)			fprintf(fp, "%8s,", temp);	// WattHour
	temp = "Imp,";		if(pDlg->m_bChImpedance)	fprintf(fp, "%8s,", temp);	// Capacity
	temp = "G,";			if(pDlg->m_bChGrade)		fprintf(fp, "%4s,", temp);
	temp = "Code,";		if(pDlg->m_bChFailure)		fprintf(fp, " %s,",	temp);	// Failure Code

	fprintf(fp, "\n");	// Failure Code

	LPSTR_SAVE_CH_DATA	lpChannel;
//	LPEP_GROUP_INFO	lpStep;
	STR_STEP_RESULT	*lpStepData;
	lpStepData = m_resultFile.GetStepData(0);//STR_STEP_RESULT *)GetStepData(0);
	ASSERT(lpStepData);

	int chSize = lpStepData->aChData.GetSize();
	int nStepSize = m_resultFile.GetStepSize();
	BYTE color;
//	int nTestStep = m_pConditionMain->apStepList.GetSize();
//	LPEP_STEP_HEADER lpStepHeader;

	for(int ch = 0; ch < chSize; ch++)
	{
		for (int step = 0; step < nStepSize ; step++)
		{
			lpStepData = m_resultFile.GetStepData(step);
			ASSERT(lpStepData);
			if(ch >= lpStepData->aChData.GetSize())		break;	//각 Step의 Channel 수가 다를 경우
			lpChannel = (LPSTR_SAVE_CH_DATA )lpStepData->aChData[ch];
			ASSERT(lpChannel);
			
			fprintf(fp, "%4d,", lpChannel->wChIndex+1);
			fprintf(fp, "%4d,", lpChannel->nStepNo+1);	
//			if(step < nTestStep)
//			{
//				lpStepHeader = (LPEP_STEP_HEADER)m_pConditionMain->apStepList[step];
//				fprintf(fp, "%10s,", StepTypeMsg(lpStepHeader->type));
				fprintf(fp, "%10s,", StepTypeMsg(lpStepData->stepCondition.stepHeader.type));
			
//			}
//			else
//			{
//				fprintf(fp, "%10s,", "Error");
//			}
			
			if(pDlg->m_bChState)		fprintf(fp, "%10s,", pDoc->GetStateMsg(lpChannel->state, color));	// 상태
//			ConvertTime(szTemp, lpChannel->ulStepTime);
			if(pDlg->m_bChTime)			fprintf(fp, "%15s,",pDoc->ValueString(lpChannel->fStepTime, EP_STEP_TIME));	// Step Time
			if(pDlg->m_bChVoltage)		fprintf(fp, "%8s,", pDoc->ValueString(lpChannel->fVoltage, EP_VOLTAGE));	// Voltage
			if(pDlg->m_bChCurrent)		fprintf(fp, "%8s,", pDoc->ValueString(lpChannel->fCurrent, EP_CURRENT));	// Current
			if(pDlg->m_bChCapacity) 	fprintf(fp, "%8s,", pDoc->ValueString(lpChannel->fCapacity, EP_CAPACITY));	// Capacity
			if(pDlg->m_bChWatt)			fprintf(fp, "%8s,", pDoc->ValueString(lpChannel->fWattHour, EP_WATT_HOUR));			// WattHour
			if(pDlg->m_bChImpedance)	fprintf(fp, "%8s,", pDoc->ValueString(lpChannel->fImpedance, EP_IMPEDANCE));	// Capacity
			if(pDlg->m_bChGrade)		fprintf(fp, "%4s,", pDoc->ValueString(lpChannel->grade, EP_GRADE_CODE));
			if(pDlg->m_bChFailure)		fprintf(fp, "%s,", pDoc->ChCodeMsg(lpChannel->channelCode));				// Failure Code
			fprintf(fp, "\n");	 
		}
		fprintf(fp, "\n");	
	}
	fclose(fp);
	fp = NULL;
	EndWaitCursor();
	return TRUE;
}

BOOL CResultView::SaveStepFileSave(CFileSaveDlg *pDlg)
{
	if(pDlg == NULL)	return FALSE;
	if(pDlg->m_bProcSave == FALSE)	return TRUE;

	CCTSAnalDoc *pDoc = GetDocument();
	ASSERT(!m_strSelFileName.IsEmpty());
	if(m_resultFile.GetStepSize() <= 0)		return FALSE;

	if(pDlg->m_strStepFileName.IsEmpty())	return FALSE;

	FILE	*fp = fopen(pDlg->m_strStepFileName, "wt");
	if (fp == FALSE)	return FALSE;

	CString temp;
//	char szTemp[512];
	
	BeginWaitCursor();
	RESULT_FILE_HEADER *pHeader = m_resultFile.GetResultHeader();
	fprintf(fp, TEXT_LANG[16]+":, %s\n", (char *)(LPCTSTR)m_strSelFileName);	//"파일명"
	fprintf(fp, TEXT_LANG[17]+":,%s\n",  m_resultFile.GetModelName());			//"모델명"
	fprintf(fp, TEXT_LANG[18]+":,%s\n",  m_resultFile.GetTestName());		//"공정_조건명"
	fprintf(fp, TEXT_LANG[19]+":, %s\n",  pHeader->szTrayNo);		//"Tray ID"
	fprintf(fp, TEXT_LANG[20]+":, %s\n",  pHeader->szLotNo);			// Lot No//"Batch"
	fprintf(fp, TEXT_LANG[21]+"No:, "+TEXT_LANG[21]+" %d-%d\n",  pHeader->nModuleID, pHeader->wGroupIndex+1);//"모듈"
	fprintf(fp, TEXT_LANG[22]+":, %s\n",  pHeader->szDateTime);	// "시작_시각"
	fprintf(fp, TEXT_LANG[23]+":, %s\n\n",  pHeader->szOperatorID);					//"운영자"
//	fprintf(fp, "설명:, %s\n",  m_pConditionMain->conditionHeader.szDescription);		// Battery No

/*
	fprintf(fp, "파일명:, %s\n", (char *)(LPCTSTR)m_strSelFileName);	// FileName
	fprintf(fp, "모델명:,%s\n",  m_extData.modelData.szName);			// 모델명 	
	fprintf(fp, "공정_조건명:,%s\n",  m_pConditionMain->conditionHeader.szName);		// 시험조건명
	fprintf(fp, "Tray_No:, %s\n",  m_resultFileHeader.szTrayNo);			// Tray No
	fprintf(fp, "Batch:, %s\n",  m_resultFileHeader.szLotNo);			// Lot No
	fprintf(fp, "모듈No:, 모듈 %d-%d\n",  m_resultFileHeader.nModuleID, m_resultFileHeader.nGroupIndex+1);					// Module name 
	fprintf(fp, "시작_시각:, %s\n",  m_resultFileHeader.szDateTime);		// 시작시간
	fprintf(fp, "운영자:, %s\n\n",  m_resultFileHeader.szOperatorID);					// 총시간
//	fprintf(fp, "설명:, %s\n",  m_pConditionMain->conditionHeader.szDescription);		// Battery No
*/


	temp = "Ch";		fprintf(fp, "%4s,", temp);
	temp = "Step";		fprintf(fp, "%4s,", temp);	
	temp = "State";		fprintf(fp, "%10s,", temp);	// 상태
	temp = "Time";		if(pDlg->m_bStepTime)			fprintf(fp, "%15s,", temp);	// Step Time
	temp = "Vtg";		if(pDlg->m_bStepVoltage)		fprintf(fp, "%8s,", temp);	// Voltage
	temp = "Crt";		if(pDlg->m_bStepCurrent)		fprintf(fp, "%8s,", temp);	// Current
	temp = "Cap";		if(pDlg->m_bStepCapacity) 		fprintf(fp, "%8s,", temp);	// Capacity
	temp = "Watt";		if(pDlg->m_bStepWatt)			fprintf(fp, "%8s,", temp);	// WattHour
	temp = "Imp";		if(pDlg->m_bStepImpedance)		fprintf(fp, "%8s,", temp);	// Capacity
	temp = "G";			if(pDlg->m_bStepGrade)		fprintf(fp, "%4s,", temp);
	temp = "Code";		if(pDlg->m_bStepFailure)		fprintf(fp, " %s,",	temp);	// Failure Code

	fprintf(fp, "\n");	// Failure Code

//	LPEP_GROUP_INFO	lpStep;
	LPSTR_SAVE_CH_DATA	lpChannel;
	STR_STEP_RESULT *lpStepData;
	int nStepSize = m_resultFile.GetStepSize();
	int chSize;
	BYTE color;
//	int nTestStep = m_pConditionMain->apStepList.GetSize();
//	LPEP_STEP_HEADER lpStepHeader;

	for (int step = 0; step < nStepSize ; step++)
	{
		lpStepData = m_resultFile.GetStepData(0);//(STR_STEP_RESULT *)GetStepData(step);
		ASSERT(lpStepData);
		chSize = lpStepData->aChData.GetSize();

//		if(step < nTestStep)
//		{
//			lpStepHeader = (LPEP_STEP_HEADER)m_pConditionMain->apStepList[step];
			fprintf(fp, "\nStep Type :%s\n", StepTypeMsg(lpStepData->stepCondition.stepHeader.type));
//			fprintf(fp, "\nStep Type :%s\n", StepTypeMsg(lpStepHeader->type));
//		}
//		else
//		{
//			fprintf(fp, "%s\n", "Error");
//		}

		for(int ch =0; ch < chSize; ch++)
		{
			lpChannel = (STR_SAVE_CH_DATA *)lpStepData->aChData[ch];
			ASSERT(lpChannel);
			
			fprintf(fp, "%4d,", lpChannel->wChIndex+1);
			fprintf(fp, "%4d,", lpChannel->nStepNo+1);
			if(pDlg->m_bChState)		fprintf(fp, "%10s,", pDoc->GetStateMsg(lpChannel->state, color));	// 상태
			if(pDlg->m_bStepTime)		fprintf(fp, "%15s,", pDoc->ValueString(lpChannel->fStepTime, EP_STEP_TIME));	// Step Time
			if(pDlg->m_bStepVoltage)	fprintf(fp, "%8s,", pDoc->ValueString(lpChannel->fVoltage, EP_VOLTAGE));	// Voltage
			if(pDlg->m_bStepCurrent)	fprintf(fp, "%8s,", pDoc->ValueString(lpChannel->fCurrent, EP_CURRENT));	// Current
			if(pDlg->m_bStepCapacity) 	fprintf(fp, "%8s,", pDoc->ValueString(lpChannel->fCapacity, EP_CAPACITY));	// Capacity
			if(pDlg->m_bStepWatt)		fprintf(fp, "%8s,", pDoc->ValueString(lpChannel->fWatt, EP_WATT_HOUR));	// WattHour
			if(pDlg->m_bStepImpedance)	fprintf(fp, "%8s,", pDoc->ValueString(lpChannel->fImpedance, EP_IMPEDANCE));	// Capacity
			if(pDlg->m_bStepGrade)		fprintf(fp, "%4s,", pDoc->ValueString(lpChannel->grade, EP_GRADE_CODE));
			if(pDlg->m_bStepFailure)	fprintf(fp, "%s,", pDoc->ChCodeMsg(lpChannel->channelCode));	// Failure Code
			fprintf(fp, "\n");	 
		}
		fprintf(fp, "\n");	
	}
	fclose(fp);
	fp = NULL;

	EndWaitCursor();
	return TRUE;
}

void CResultView::InitMinMaxGrid()
{
	m_wndMinMaxGrid.SubclassDlgItem(IDC_MINMAX_GRID, this);
//	m_wndMinMaxGrid.m_bRowSelection = TRUE;
	m_wndMinMaxGrid.m_bSameColSize  = FALSE;
	m_wndMinMaxGrid.m_bSameRowSize  = TRUE;
	m_wndMinMaxGrid.m_bCustomWidth  = TRUE;

	m_wndMinMaxGrid.Initialize();
	m_wndMinMaxGrid.LockUpdate();
	m_wndMinMaxGrid.SetRowCount(4);
	m_wndMinMaxGrid.SetColCount(8);
//	m_wndMinMaxGrid.SetDefaultRowHeight(18);
//	m_wndMinMaxGrid.GetParam()->SetSortRowsOnDblClk(TRUE);
	m_wndMinMaxGrid.m_BackColor	= RGB(255,255,255);
//	m_wndMinMaxGrid.m_SelColor	= RGB(225,255,225);

	m_wndMinMaxGrid.SetValueRange(CGXRange(1,1),  TEXT_LANG[25]);//"최대"
	m_wndMinMaxGrid.SetValueRange(CGXRange(2,1),  TEXT_LANG[26]);//"최소"
	m_wndMinMaxGrid.SetValueRange(CGXRange(3,1),  TEXT_LANG[27]);//"평균"
	m_wndMinMaxGrid.SetValueRange(CGXRange(4,1),  TEXT_LANG[28]);//"표준편차"
//	m_wndMinMaxGrid.SetValueRange(CGXRange(5,1), "");
//	m_wndMinMaxGrid.SetValueRange(CGXRange(6,1),  "");

	m_wndMinMaxGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	m_wndMinMaxGrid.SetScrollBarMode(SB_HORZ, gxnDisabled);
	m_wndMinMaxGrid.EnableCellTips();
	
	m_wndMinMaxGrid.LockUpdate(FALSE);
	m_wndMinMaxGrid.Redraw();
}


void CResultView::OnPrint(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	CString strTmp, strLine;
	strLine = _T("   ");
	for(int i =0; i<90; i++)
		strLine += _T("━");
	strLine += _T("   ");

	CGXData& Header	= m_pCurrentGrid->GetParam()->GetProperties()->GetDataHeader();
	CGXData& Footer	= m_pCurrentGrid->GetParam()->GetProperties()->GetDataFooter();
	if(m_resultFile.IsLoaded())
	{

		Header.StoreStyleRowCol(1, 1, CGXStyle().SetValue(""), gxOverride);
		Header.StoreStyleRowCol(1, 2, 
				CGXStyle().SetValue(m_strSelFileName.Mid(m_strSelFileName.ReverseFind('\\')+1)).SetFont(CGXFont().SetSize(11).SetBold(TRUE)), gxOverride);
		Header.StoreStyleRowCol(2, 1, CGXStyle().SetValue(strLine), gxOverride);
		Header.StoreStyleRowCol(2, 2, CGXStyle().SetValue(""), gxOverride);

		strTmp.Format("File Name: %s", m_strSelFileName); 	
		Header.StoreStyleRowCol(3, 1, CGXStyle().SetValue(strTmp).SetFont(CGXFont().SetSize(10).SetBold(TRUE)),gxOverride);

		strTmp.Format(TEXT_LANG[29]+":%s", m_resultFile.GetTestName()); 	//"공정 Name"
		Header.StoreStyleRowCol(4, 1, CGXStyle().SetValue(strTmp).SetFont(CGXFont().SetSize(10).SetBold(TRUE)),gxOverride);

		strTmp.Format("Model Name : %s", m_resultFile.GetModelName());		//==>Model Name
		Header.StoreStyleRowCol(5, 1, CGXStyle().SetValue(strTmp).SetFont(CGXFont().SetSize(10).SetBold(TRUE)),gxOverride);

		strTmp.Format("Batch: %s", m_resultFile.GetLotNo()); 	
		Header.StoreStyleRowCol(6, 1, CGXStyle().SetValue(strTmp).SetFont(CGXFont().SetSize(10).SetBold(TRUE)),gxOverride);

		strTmp.Format("Date Time : %s", m_resultFile.GetResultHeader()->szDateTime);
		Header.StoreStyleRowCol(7, 1, CGXStyle().SetValue(strTmp).SetFont(CGXFont().SetSize(10).SetBold(TRUE)),gxOverride);
		
		strTmp.Format("%s : %s (Total %d Ch)", GetDocument()->m_strModuleName, 
				m_resultFile.GetModelName(), m_resultFile.GetTotalCh());
		Header.StoreStyleRowCol(8, 1, CGXStyle().SetValue(strTmp).SetFont(CGXFont().SetSize(10).SetBold(TRUE)),gxOverride);

		strTmp.Format("Tray ID : %s", m_resultFile.GetTrayNo());
		Header.StoreStyleRowCol(9, 1, CGXStyle().SetValue(strTmp).SetFont(CGXFont().SetSize(10).SetBold(TRUE)),gxOverride);

		if(m_nDisplayMode == TYPE_CHANNEL_LIST)
		{
			strTmp.Empty();
			CString strData;
			for(int col = 1; col<= m_wndStepListGrid.GetColCount(); col++)
			{
				strLine = m_wndStepListGrid.GetValueRowCol(0, col);
				strData = m_wndStepListGrid.GetValueRowCol(m_nCurrentStepIndex+1, col);
				if(!strData.IsEmpty())
				{
					strLine += ( "=" + strData + " / ");
					strTmp += strLine;
				}
				Header.StoreStyleRowCol(10, 1, CGXStyle().SetValue(strTmp).SetFont(CGXFont().SetSize(10).SetBold(TRUE)),gxOverride);
			}
		}
		else if(m_nDisplayMode == TYPE_TRAY_LIST)
		{
			GetDlgItem(IDC_STEP_SEL_COMBO)->GetWindowText(strTmp);
			strTmp =  TEXT_LANG[30]+" : Step "+strTmp;//"표시 Data"
			Header.StoreStyleRowCol(10, 1, CGXStyle().SetValue(strTmp).SetFont(CGXFont().SetSize(10).SetBold(TRUE)),gxOverride);
		}
	}
	
	Footer.StoreStyleRowCol(1, 1, CGXStyle().SetValue(""),gxOverride);
	Footer.StoreStyleRowCol(1, 2, CGXStyle().SetValue("$P/$N"),gxOverride);
	Footer.StoreStyleRowCol(1, 3, CGXStyle().SetValue("$d{%Y/%m/%d %H:%M:%S}"),gxOverride);
	
	m_pCurrentGrid->OnGridPrint(pDC, pInfo);
//	CFormView::OnPrint(pDC, pInfo);
}

void CResultView::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	m_pCurrentGrid->OnGridEndPrinting(pDC, pInfo);
	m_pCurrentGrid->SetStyleRange(CGXRange().SetTable(),CGXStyle().SetFont(CGXFont().SetSize(9)));
	//	CFormView::OnEndPrinting(pDC, pInfo);
}

void CResultView::OnPrepareDC(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	m_pCurrentGrid->OnGridPrepareDC(pDC, pInfo);
	//CFormView::OnPrepareDC(pDC, pInfo);
}

BOOL CResultView::OnPreparePrinting(CPrintInfo* pInfo) 
{
	// TODO: call DoPreparePrinting to invoke the Print dialog box
	pInfo->SetMaxPage(0xffff);

	pInfo->m_pPD->m_pd.Flags &= ~PD_NOSELECTION;
	pInfo->m_pPD->m_pd.hInstance = AfxGetInstanceHandle();

	// default preparation
	return DoPreparePrinting(pInfo);

//	return CFormView::OnPreparePrinting(pInfo);
}

void CResultView::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class

	m_pCurrentGrid->GetParam()->GetProperties()->SetBlackWhite(FALSE);
	m_pCurrentGrid->GetParam()->GetProperties()->SetMargins(180,5,50,5);
	m_pCurrentGrid->SetStyleRange(CGXRange().SetTable(), CGXStyle().SetFont(CGXFont().SetSize(8)));

	if(m_nDisplayMode == TYPE_CHANNEL_LIST)
	{
		int nAWndGrid[] = {65, 40, 70, 80, 40, 85, 60, 60, 60, 60, 0, 0};
		m_wndChannelListGrid.SetColWidth(1, 12, 0, nAWndGrid, GX_UPDATENOW);
	}
	else if(m_nDisplayMode == TYPE_TRAY_LIST)
	{
		for(int i=0; i<m_wndTrayChGrid.GetColCount(); i++)
		{
			if(i%2)
			{
				m_wndTrayChGrid.SetColWidth(i+1, i+1, 60);
			}
			else
			{
				m_wndTrayChGrid.SetColWidth(i+1, i+1, 30);
			}
		}
		m_wndTrayChGrid.SetRowHeight(1, m_wndTrayChGrid.GetRowCount(), 28);
	}
	else //if(m_nDisplayMode == TYPE_CELL_LIST)
	{
//		m_wndCellListGrid.SetColWidth(1, m_wndCellListGrid.GetColCount(), 43);
	}
	m_pCurrentGrid->OnGridBeginPrinting(pDC, pInfo);

//	CFormView::OnBeginPrinting(pDC, pInfo);
}


//Tray의 진행 공정 data를 모두 표기한다.
//같은 폴더내에서 Lot와 Tray번호가 같은 data를 표기한다.
BOOL CResultView::DisplayCellListAll()
{
	CCTSAnalDoc *pDoc = (CCTSAnalDoc *)GetDocument();

	if(m_wndCellListGrid.GetColCount() != pDoc->m_TotCol)
	{
		m_wndCellListGrid.SetColCount(0);
		m_wndCellListGrid.SetColCount(pDoc->m_TotCol);
		m_wndCellListGrid.SetColWidth(1, pDoc->m_TotCol, 55);
	}
	m_wndCellListGrid.SetRowCount(0);

	for(int i=0; i<pDoc->m_TotCol; i++)
	{
		m_wndCellListGrid.SetValueRange(CGXRange(0, i+1), pDoc->m_field[i].FieldName);
	}

	CString strRltFile, strTemp, strSrcFile, strCode, strCode2, strTemp2;
	strSrcFile = m_resultFile.GetFileName();
	strRltFile = strSrcFile.Left(strSrcFile.ReverseFind('\\'));
	
	//정렬 순서??
	CFileFind affind;
	BOOL bFind = affind.FindFile(strRltFile+"\\*."+BF_RESULT_FILE_EXT);

	STR_STEP_RESULT *lpStepDataInfo;
	STR_SAVE_CH_DATA *lpChannelData;
	CFormResultFile *pRltFile = new CFormResultFile;
	BYTE colorFlag = 0;
	ROWCOL nRow, nCol;
	while(bFind)
	{
		bFind = affind.FindNextFile();
		strRltFile = affind.GetFilePath();
		if(pRltFile->ReadFile(strRltFile))
		{
			if(m_resultFile.GetLotNo() == pRltFile->GetLotNo() && m_resultFile.GetTrayNo() == pRltFile->GetTrayNo())
			{
				for(int step = 0; step<pRltFile->GetStepSize(); step++)
				{
					lpStepDataInfo = pRltFile->GetStepData(step);
					if(lpStepDataInfo)
					{
						//find matting field
						for(int f = 0; f<pDoc->m_TotCol; f++)
						{
							nCol = f+1;
							//Grading code와 Cell code는 기본적으로 표시한다.
							if( lpStepDataInfo->stepCondition.stepHeader.nProcType == pDoc->m_field[f].ID || 
								pDoc->m_field[f].ID == EP_REPORT_GRADING || pDoc->m_field[f].ID == EP_REPORT_CH_CODE)
							{
								//check row count
								if(m_wndCellListGrid.GetRowCount() < lpStepDataInfo->aChData.GetSize())
								{
									m_wndCellListGrid.SetRowCount(lpStepDataInfo->aChData.GetSize());
								}

								int n = lpStepDataInfo->aChData.GetSize();
								for(int c = 0; c<n; c++)
								{
									nRow = c+1;
									lpChannelData = (STR_SAVE_CH_DATA *)lpStepDataInfo->aChData[c];
									//find matting data type
									switch(pDoc->m_field[f].DataType)
									{
									case EP_VOLTAGE:
										strTemp.Format("%s", pDoc->ValueString(lpChannelData->fVoltage, pDoc->m_field[f].DataType));
										break;
									case EP_CURRENT:
										strTemp.Format("%s", pDoc->ValueString(lpChannelData->fCurrent, pDoc->m_field[f].DataType));
										break;
									case EP_CAPACITY:
										strTemp.Format("%s", pDoc->ValueString(lpChannelData->fCapacity, pDoc->m_field[f].DataType));
										break;
									case EP_STEP_TIME:
										strTemp.Format("%s", pDoc->ValueString(lpChannelData->fStepTime, pDoc->m_field[f].DataType));
										break;
									case EP_IMPEDANCE:
										strTemp.Format("%s", pDoc->ValueString(lpChannelData->fImpedance, pDoc->m_field[f].DataType));
										break;
									case EP_CH_CODE:
										//strCode = m_wndCellListGrid.GetValueRowCol(nRow, nCol);
										//if(strCode.IsEmpty())
										//{
											strTemp.Format("%s", pDoc->ValueString(lpChannelData->channelCode, pDoc->m_field[f].DataType));
										//}
										//else
										//{
										//	strTemp.Format("%s-%s", strCode, pDoc->ValueString(lpChannelData->channelCode, pDoc->m_field[f].DataType));
										//}
										break;
									case EP_GRADE_CODE:
										strCode = m_wndCellListGrid.GetValueRowCol(nRow, nCol);
										strCode2 = pDoc->ValueString(lpChannelData->grade, pDoc->m_field[f].DataType);
										if(strCode.IsEmpty())
										{
											strTemp.Format("%s", strCode2);
										}
										else
										{
											if(strCode2.IsEmpty())
											{
												strTemp.Format("%s", strCode);
											}
											else
											{
												strTemp.Format("%s-%s", strCode, strCode2);
											}
										}
										break;
									}

									pDoc->GetStateMsg(lpChannelData->state, colorFlag);	//Get color
									if(pDoc->m_bHideNonCellData && (lpChannelData->channelCode == EP_CODE_CELL_NONE || lpChannelData->channelCode == EP_CODE_NONCELL))
									{
										strTemp.Empty();
										m_wndCellListGrid.SetStyleRange(CGXRange(nRow, nCol), 
																		CGXStyle()
																		.SetValue(strTemp)
																		.SetTextColor(0)
																		.SetInterior(RGB(255,255,255)));

									}
									else
									{
										if(::IsNormalCell(lpChannelData->channelCode) == FALSE)
										{
											m_wndCellListGrid.SetStyleRange(CGXRange(nRow, nCol),					
																CGXStyle()
																.SetValue(strTemp)
																.SetTextColor(m_pConfig->m_TStateColor[colorFlag])
																.SetInterior(m_pConfig->m_BStateColor[colorFlag]));
										}
										else
										{
											m_wndCellListGrid.SetStyleRange(CGXRange(nRow, nCol), 
																CGXStyle()
																.SetValue(strTemp)
																.SetTextColor(0)
																.SetInterior(RGB(255,255,255)));
										}
									}
								}
							}

							if(pDoc->m_field[f].ID == EP_REPORT_CH_CODE)
							{
								m_wndCellListGrid.SetColWidth(nCol, nCol, 100);
							}
						}
					}
				}
			}
		}
	}
	delete pRltFile;
	pRltFile = NULL;
	return TRUE;
}


BOOL CResultView::DisplayCellList()
{
	int size = m_resultFile.GetStepSize();	
	if(size <= 0)	return FALSE;

	CCTSAnalDoc *pDoc = (CCTSAnalDoc *)GetDocument();

	int chsize;
	int rowsize;
	int nTotCol = pDoc->m_nTrayColSize;

	STR_STEP_RESULT *lpStepData = NULL;
	if(m_resultFile.GetStepSize() <= 0)	return	FALSE;
	
	lpStepData = m_resultFile.GetStepData(0); //STR_STEP_RESULT *)m_stepData[0];
	if(lpStepData == NULL)	return FALSE;
	chsize = lpStepData->aChData.GetSize();
	
	rowsize = chsize/nTotCol;
	if(chsize%nTotCol > 0)	rowsize++;

	CString strTemp, strTemp1;;

	strTemp.Format(TEXT_LANG[10], m_strSelFileName.Mid(m_strSelFileName.ReverseFind('\\')+1));//"결과 파일 %s를 읽고 있습니다."
	pDoc->SetProgressWnd(0, 1000, strTemp);
	
	int nChannelCout = 0;
	int nColCount = 0;
	LPSTR_SAVE_CH_DATA		lpChannelData;
	BYTE colorFlag, prevStepType = EP_TYPE_NONE;
	m_wndCellListGrid.SetColCount(nColCount);
	m_wndCellListGrid.SetRowCount(0);
	int nInputCellCount  = m_resultFile.GetExtraData()->nInputCellNo;
	int nNormalCount = 0;
	int anWidth[5] = { 70, 100, 50, 70, 50 };
	
	for(int nStepIndex =0; nStepIndex<size; nStepIndex++)
	{
//		nInputCellCount  = 0;
		nNormalCount = 0;
		pDoc->SetProgressPos(int(1000.0f/(float)size*(float)nStepIndex));

		m_wndCellListGrid.LockUpdate();
		
		//Channel No, Cell No, CellCode, Grading Code Column 삽입 
		if(nStepIndex == 0)
		{
			ASSERT(size > 0);
			lpStepData = m_resultFile.GetStepData(size-1);
			
			if(lpStepData == NULL)
			{
				m_wndCellListGrid.LockUpdate(FALSE);
				m_wndCellListGrid.Redraw();
				pDoc->HideProgressWnd();
				return FALSE;
			}
			m_wndCellListGrid.SetRowCount(lpStepData->aChData.GetSize());
			nColCount = m_wndCellListGrid.GetColCount();
			m_wndCellListGrid.InsertCols(nColCount+1, 3, anWidth);
		
			//
			for(int i=0; i<lpStepData->aChData.GetSize(); i++)
			{
				lpChannelData = (STR_SAVE_CH_DATA *)lpStepData->aChData[i];
				m_wndCellListGrid.SetValueRange(CGXRange(0, 0), TEXT_LANG[31]);//"ChannelNo"
				m_wndCellListGrid.SetValueRange(CGXRange(i+1, 0), (ROWCOL) (lpChannelData->wChIndex+1));	//Module and Group Number
#ifdef DSP_TYPE_VERTICAL
				strTemp.Format("%c-%2d (%03d)", 'A'+lpChannelData->wChIndex/rowsize, lpChannelData->wChIndex%rowsize+1, lpChannelData->wChIndex+1);
#else
				strTemp.Format("%c-%2d (%03d)", 'A'+lpChannelData->wChIndex/nTotCol, lpChannelData->wChIndex%nTotCol+1, lpChannelData->wChIndex+1);
#endif
				m_wndCellListGrid.SetValueRange(CGXRange(0, nColCount+1), TEXT_LANG[32]);//"Cell No"
				m_wndCellListGrid.SetValueRange(CGXRange(i+1, nColCount+1), strTemp);	//Module and Group Number

				m_wndCellListGrid.SetValueRange(CGXRange(0, nColCount+2), TEXT_LANG[33]);//"Code"
				strTemp = pDoc->GetStateMsg(lpChannelData->state, colorFlag);
				strTemp = pDoc->ValueString(lpChannelData->channelCode, EP_CH_CODE);
				m_wndCellListGrid.SetValueRange(CGXRange(i+1, nColCount+2), strTemp);
				m_wndCellListGrid.SetStyleRange(CGXRange(i+1, nColCount+2), 
										CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(pDoc->ChCodeMsg(lpChannelData->channelCode, TRUE))));

				m_wndCellListGrid.SetValueRange(CGXRange(0, nColCount+3), TEXT_LANG[34]);//"종합등급"
				m_wndCellListGrid.SetValueRange(CGXRange(i+1, nColCount+3), pDoc->ValueString(lpChannelData->grade, EP_GRADE_CODE));
				if(!IsNormalCell(lpChannelData->channelCode))
				{
					m_wndCellListGrid.SetStyleRange(CGXRange(i+1, nColCount+2), 
										CGXStyle().SetTextColor(m_pConfig->m_TStateColor[colorFlag])
												.SetInterior(m_pConfig->m_BStateColor[colorFlag]));
				}
				else
				{
					m_wndCellListGrid.SetStyleRange(CGXRange(i+1, nColCount+2), 
										CGXStyle().SetTextColor(0)
												.SetInterior(RGB(255,255,255)));
					nNormalCount++;
				}
				//if(!IsNonCell(lpChannelData->channelCode))	nInputCellCount++;
			}
		}
		anWidth[0] = 60;	//voltage
		anWidth[1] = 60;	//current
		anWidth[2] = 60;	//capacity
		anWidth[3] = 80;	//time
		
		lpStepData = m_resultFile.GetStepData(nStepIndex);
		if(lpStepData == NULL)
		{
			continue;
		}

		nColCount = m_wndCellListGrid.GetColCount();	
		strTemp.Format("%d:", nStepIndex+1);
		strTemp = strTemp + pDoc->GetProcTypeName(lpStepData->stepCondition.stepHeader.nProcType, lpStepData->stepCondition.stepHeader.type);
		
		
		int increase;

		switch(lpStepData->stepCondition.stepHeader.type)
		{

		case EP_TYPE_CHARGE:
			m_wndCellListGrid.InsertCols(nColCount+1, 4, anWidth);
			m_wndCellListGrid.SetStyleRange(CGXRange().SetCols(nColCount+1, nColCount+4), 
											CGXStyle().SetInterior(RGB(255,240,240)));
			strTemp1.Format("%s(%s)", strTemp, pDoc->m_strVUnit);
			m_wndCellListGrid.SetValueRange(CGXRange(0, nColCount+1), strTemp1);
			strTemp1.Format("%s(%s)", strTemp, pDoc->m_strIUnit);
			m_wndCellListGrid.SetValueRange(CGXRange(0, nColCount+2), strTemp1);
			strTemp1.Format("%s(%s)", strTemp, pDoc->m_strCUnit);
			m_wndCellListGrid.SetValueRange(CGXRange(0, nColCount+3), strTemp1);
			m_wndCellListGrid.SetValueRange(CGXRange(0, nColCount+4), strTemp+"_t");			
			if(lpStepData->stepCondition.stepHeader.gradeSize > 0)
			{
				m_wndCellListGrid.InsertCols(nColCount+5, 1, anWidth);
				m_wndCellListGrid.SetStyleRange(CGXRange().SetCols(nColCount+5),
												CGXStyle().SetInterior(RGB(255,240,240)));
				m_wndCellListGrid.SetValueRange(CGXRange(0, nColCount+5), strTemp+"_G");
			}
			break;

		case EP_TYPE_DISCHARGE:	
			m_wndCellListGrid.InsertCols(nColCount+1, 4, anWidth);
			m_wndCellListGrid.SetStyleRange(CGXRange().SetCols(nColCount+1, nColCount+4), 
											CGXStyle().SetInterior(RGB(240,240,255)));
			strTemp1.Format("%s(%s)", strTemp, pDoc->m_strVUnit);
			m_wndCellListGrid.SetValueRange(CGXRange(0, nColCount+1), strTemp1);
			strTemp1.Format("%s(%s)", strTemp, pDoc->m_strIUnit);
			m_wndCellListGrid.SetValueRange(CGXRange(0, nColCount+2), strTemp1);
			strTemp1.Format("%s(%s)", strTemp, pDoc->m_strCUnit);
			m_wndCellListGrid.SetValueRange(CGXRange(0, nColCount+3), strTemp1);
			m_wndCellListGrid.SetValueRange(CGXRange(0, nColCount+4), strTemp+"_t");
			
			increase = 5;
			if(m_bDispCapSum)
			{
				m_wndCellListGrid.InsertCols(nColCount+4, 1, anWidth);			//Capacity 와 Sec 사이에 효율 표시 
				m_wndCellListGrid.SetStyleRange(CGXRange().SetCols(nColCount+4),
												CGXStyle().SetInterior(RGB(240,240,255)));
				m_wndCellListGrid.SetValueRange(CGXRange(0, nColCount+4), strTemp+TEXT_LANG[35]);//"_효율(%)"
				increase++;
			}
			if(lpStepData->stepCondition.stepHeader.gradeSize > 0 )
			{
				m_wndCellListGrid.InsertCols(nColCount+increase, 1, anWidth);
				m_wndCellListGrid.SetStyleRange(CGXRange().SetCols(nColCount+increase),
												CGXStyle().SetInterior(RGB(240,240,255)));
				m_wndCellListGrid.SetValueRange(CGXRange(0, nColCount+increase), strTemp+"_G");
			}
			
			break;

		case EP_TYPE_OCV:			//전압 표시 
			
			m_wndCellListGrid.InsertCols(nColCount+1, 1, anWidth);
			strTemp1.Format("%s(%s)", strTemp, pDoc->m_strVUnit);
			m_wndCellListGrid.SetValueRange(CGXRange(0, nColCount+1), strTemp1);
			if(lpStepData->stepCondition.stepHeader.gradeSize > 0)
			{
				m_wndCellListGrid.InsertCols(nColCount+2, 1, anWidth);
				m_wndCellListGrid.SetValueRange(CGXRange(0, nColCount+2), strTemp+"_G");
			}
			break;

		case EP_TYPE_IMPEDANCE:							//임피던스 표시
			m_wndCellListGrid.InsertCols(nColCount+1, 1, anWidth);
				m_wndCellListGrid.SetStyleRange(CGXRange().SetCols(nColCount+1),
												CGXStyle().SetInterior(RGB(255,255,190)));
			m_wndCellListGrid.SetValueRange(CGXRange(0, nColCount+1), strTemp+"(mΩ)");
			if(lpStepData->stepCondition.stepHeader.gradeSize > 0)
			{
				m_wndCellListGrid.InsertCols(nColCount+2, 1, anWidth);
				m_wndCellListGrid.SetStyleRange(CGXRange().SetCols(nColCount+2),
												CGXStyle().SetInterior(RGB(255,255,190)));
				m_wndCellListGrid.SetValueRange(CGXRange(0, nColCount+2), strTemp+"_G");
			}
			break;
			
		default:
			m_wndCellListGrid.LockUpdate(FALSE);
			m_wndCellListGrid.Redraw();

			continue;
		}
		
		nChannelCout = lpStepData->aChData.GetSize();
		for(int i =0; i<nChannelCout ; i++)		
		{
			increase = nColCount+1;
			lpChannelData = (STR_SAVE_CH_DATA *)lpStepData->aChData[i];
			ASSERT(lpChannelData);
			switch(lpStepData->stepCondition.stepHeader.type)
			{
			case EP_TYPE_CHARGE:	
				m_wndCellListGrid.SetValueRange(CGXRange(i+1, nColCount+1),  pDoc->ValueString(lpChannelData->fVoltage, EP_VOLTAGE));
				m_wndCellListGrid.SetValueRange(CGXRange(i+1, nColCount+2),  pDoc->ValueString(lpChannelData->fCurrent, EP_CURRENT));
				m_wndCellListGrid.SetValueRange(CGXRange(i+1, nColCount+3),  pDoc->ValueString(lpChannelData->fCapacity, EP_CAPACITY));
				m_wndCellListGrid.SetValueRange(CGXRange(i+1, nColCount+4),  ULONG(lpChannelData->fStepTime));	
				if(lpStepData->stepCondition.stepHeader.gradeSize > 0)
				{
					m_wndCellListGrid.SetValueRange(CGXRange(i+1, nColCount+5),  pDoc->ValueString(lpChannelData->grade, EP_GRADE_CODE));
				}
				break;
			
			case EP_TYPE_DISCHARGE:	
				m_wndCellListGrid.SetValueRange(CGXRange(i+1, increase++),  pDoc->ValueString(lpChannelData->fVoltage, EP_VOLTAGE));
				m_wndCellListGrid.SetValueRange(CGXRange(i+1, increase++),  pDoc->ValueString(lpChannelData->fCurrent, EP_CURRENT));
				m_wndCellListGrid.SetValueRange(CGXRange(i+1, increase++),  pDoc->ValueString(lpChannelData->fCapacity, EP_CAPACITY));
				//방전 효율 표기 여부 
				if(m_bDispCapSum)
					m_wndCellListGrid.SetValueRange(CGXRange(i+1, increase++),  pDoc->ValueString(lpChannelData->fTotalTime, EP_CAPACITY));
				
				m_wndCellListGrid.SetValueRange(CGXRange(i+1, increase++),  ULONG(lpChannelData->fStepTime));		//방전 효율이 저장 되어 있음 	
				
				if(lpStepData->stepCondition.stepHeader.gradeSize > 0)
				{
					m_wndCellListGrid.SetValueRange(CGXRange(i+1, increase++),  pDoc->ValueString(lpChannelData->grade, EP_GRADE_CODE));
				}
				break;

			case EP_TYPE_OCV:
				m_wndCellListGrid.SetValueRange(CGXRange(i+1, nColCount+1),  pDoc->ValueString(lpChannelData->fVoltage, EP_VOLTAGE));
				if(lpStepData->stepCondition.stepHeader.gradeSize > 0)
				{
					m_wndCellListGrid.SetValueRange(CGXRange(i+1, nColCount+2),  pDoc->ValueString(lpChannelData->grade, EP_GRADE_CODE));
				}
			break;

			case EP_TYPE_IMPEDANCE:								//임피던스 표시
				m_wndCellListGrid.SetValueRange(CGXRange(i+1, nColCount+1), pDoc->ValueString(lpChannelData->fImpedance, EP_IMPEDANCE));
				if(lpStepData->stepCondition.stepHeader.gradeSize > 0)
				{
					m_wndCellListGrid.SetValueRange(CGXRange(i+1, nColCount+2),  pDoc->ValueString(lpChannelData->grade, EP_GRADE_CODE));
				}
				break;
			
			default :	//Rest//Loop
				break;
			}

			if(lpStepData->stepCondition.stepHeader.type != EP_TYPE_REST)
				prevStepType = lpStepData->stepCondition.stepHeader.type;
		}
		
		m_wndCellListGrid.LockUpdate(FALSE);
		m_wndCellListGrid.Redraw();

	}

	strTemp.Format("%3d 개", nNormalCount);	//정상 수량  
	m_ctrlNormalRate.SetText(strTemp);				
	strTemp.Format("%3d 개", nInputCellCount - nNormalCount);		
	m_ctrlErrorRate.SetText(strTemp);			//불량 수량 
	if(nInputCellCount == 0)
	{
		strTemp = "0 %%";
	}
	else
	{
		strTemp.Format("%.1f %%", (float)(nInputCellCount - nNormalCount)/(float)nInputCellCount * 100.0f);
	}
	m_ctrlRate.SetText(strTemp);

	pDoc->SetProgressPos(1000);
	pDoc->HideProgressWnd();

	return TRUE;
}

void CResultView::InitTrayTypeGrid()
{
	m_wndTrayChGrid.SubclassDlgItem(IDC_RESULT_GRID, this);
	m_wndTrayChGrid.m_bSameRowSize = TRUE;
	m_wndTrayChGrid.m_bSameColSize = FALSE;
//---------------------------------------------------------------------//
	m_wndTrayChGrid.m_bCustomWidth = TRUE;
	m_wndTrayChGrid.m_bCustomColor = FALSE;

	m_wndTrayChGrid.Initialize();
	m_wndTrayChGrid.LockUpdate();
	m_wndTrayChGrid.SetDefaultRowHeight(20);

	m_wndTrayChGrid.EnableGridToolTips();
    m_wndTrayChGrid.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, 
         CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));

//	m_wndTrayChGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	//Row Header Setting
//	m_wndTrayChGrid.SetStyleRange(CGXRange().SetCols(1),
//			CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(255,255,255)).SetInterior(RGB(192,192,192)));
	m_wndTrayChGrid.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(9).SetFaceName("굴림")));
	
	m_wndTrayChGrid.HideRows(0,0);

	m_wndTrayChGrid.LockUpdate(FALSE);
	m_wndTrayChGrid.Redraw();

}

BOOL CResultView::DisplayTrayChData(int nItem)
{
	int size = m_resultFile.GetStepSize();
	if(size <= 0)	return FALSE;

	if(nItem < 0 || nItem >= m_dataList.GetSize())	return FALSE;
	m_ctrlStepSelCombo.SetCurSel(nItem);

	Display_Data_List &dataList = m_dataList.ElementAt(nItem);
	if(dataList.stepIndex < 0 || dataList.stepIndex >= size)	return FALSE;

	STR_STEP_RESULT *lpStepData = m_resultFile.GetStepData(dataList.stepIndex);

	m_wndTrayChGrid.SetRowCount(0);
	m_wndTrayChGrid.SetColCount(0);

	if(lpStepData == NULL)	return FALSE;
	
	CCTSAnalDoc *pDoc = GetDocument();
	int nTotRow, nTotCol;
	nTotCol = pDoc->m_nTrayColSize;

	int nChsize = lpStepData->aChData.GetSize();
	nTotRow = nChsize/nTotCol;
	if(nChsize%pDoc->m_nTrayColSize > 0)	nTotRow++;

//	BOOL bLockUpdate = m_wndTrayChGrid.LockUpdate();
	m_wndTrayChGrid.SetRowCount(nTotRow);
	m_wndTrayChGrid.SetColCount(nTotCol*2);

	CRect rectGrid;
	m_wndTrayChGrid.GetClientRect(rectGrid);
	if(nTotCol > 0)
	{
		float fWidth = (float)rectGrid.Width()/(float)nTotCol;
		for(int c = 0; c<nTotCol&&c<32; c++)
		{
			m_wndTrayChGrid.m_nWidth[c*2+1]	= int(fWidth* 0.4f);
			m_wndTrayChGrid.m_nWidth[(c+1)*2]	= int(fWidth* 0.6f);
		}
	}

	
	CString strTemp;
	strTemp.Format(TEXT_LANG[10], m_strSelFileName.Mid(m_strSelFileName.ReverseFind('\\')+1));//"결과 파일 %s를 읽고 있습니다."
	pDoc->SetProgressWnd(0, 100, strTemp);

	STR_SAVE_CH_DATA	*lpChannelData;
	int nChNo = 0;
	BYTE colorFlag;
	int nDRow, nDCol;

#ifdef DSP_TYPE_VERTICAL
	for(int col = 0; col<nTotCol; col++)
	{
		pDoc->SetProgressPos(100/nTotRow*col);
#else
	for(int row = 0; row <nTotRow ; row++)
	{
		pDoc->SetProgressPos(100/nTotCol*row);
#endif

#ifdef DSP_TYPE_VERTICAL
		for(int row = 0; row <nTotRow ; row++)
#else
		for(int col = 0; col<nTotCol; col++)
#endif
		{
//////////////////////////////////////////////////////////////////////////
			if(nChNo >= lpStepData->aChData.GetSize())	break;
			lpChannelData = (STR_SAVE_CH_DATA *)lpStepData->aChData[nChNo];

			nDRow = row+1;
			nDCol = (col+1)*2;

			strTemp.Format("%d", nChNo+1);
			//m_wndTrayChGrid.SetStyleRange(CGXRange(nDRow, col*2+1), CGXStyle().SetEnabled(FALSE).SetValue(strTemp).SetDraw3dFrame(gxFrameRaised));
			m_wndTrayChGrid.SetStyleRange(CGXRange(nDRow, col*2+1), CGXStyle().SetValue(strTemp).SetInterior(GetSysColor(COLOR_3DFACE)).SetFont(CGXFont().SetBold(TRUE)));
			m_wndTrayChGrid.SetStyleRange(CGXRange(nDRow, col*2+1), CGXStyle().SetBorders(gxBorderLeft, CGXPen().SetStyle(PS_SOLID).SetColor(RGB(128, 128 , 128))));
			m_wndTrayChGrid.SetStyleRange(CGXRange(nDRow, col*2+1), CGXStyle().SetBorders(gxBorderTop, CGXPen().SetStyle(PS_SOLID).SetColor(RGB(128, 128 , 128))));
			m_wndTrayChGrid.SetStyleRange(CGXRange(nDRow, col*2+1), CGXStyle().SetBorders(gxBorderBottom, CGXPen().SetStyle(PS_SOLID).SetColor(RGB(128, 128 , 128))));
			m_wndTrayChGrid.SetStyleRange(CGXRange(nDRow, (col+1)*2), CGXStyle().SetBorders(gxBorderRight, CGXPen().SetStyle(PS_SOLID).SetColor(RGB(128, 128 , 128))));
			m_wndTrayChGrid.SetStyleRange(CGXRange(nDRow, (col+1)*2), CGXStyle().SetBorders(gxBorderTop, CGXPen().SetStyle(PS_SOLID).SetColor(RGB(128, 128 , 128))));
			m_wndTrayChGrid.SetStyleRange(CGXRange(nDRow, (col+1)*2), CGXStyle().SetBorders(gxBorderBottom, CGXPen().SetStyle(PS_SOLID).SetColor(RGB(128, 128 , 128))));
//////////////////////////////////////////////////////////////////////////


			switch(dataList.nDataType)
			{
			case EP_VOLTAGE:
				strTemp = pDoc->ValueString(lpChannelData->fVoltage, EP_VOLTAGE);
				break;
			case EP_CURRENT:
				strTemp = pDoc->ValueString(lpChannelData->fCurrent, EP_CURRENT);
				break;
			case EP_CAPACITY:
				strTemp = pDoc->ValueString(lpChannelData->fCapacity, EP_CAPACITY);
				break;
			case EP_IMPEDANCE:
				strTemp = pDoc->ValueString(lpChannelData->fImpedance, EP_IMPEDANCE);
				break;
//			case EP_DELTA_OCV:
//				strTemp.Format("%.1f", lpChannelData->fWatt);
//				break;
			case EP_GRADE_CODE:
				strTemp = pDoc->ValueString(lpChannelData->grade, EP_GRADE_CODE);
				break;
			case EP_CH_CODE:
				strTemp = pDoc->ValueString(lpChannelData->channelCode, EP_CH_CODE);
				break;
			case EP_STATE:
				strTemp = pDoc->ValueString(lpChannelData->state, EP_STATE);
				break;
			case EP_STEP_TIME:
				strTemp = pDoc->ValueString(lpChannelData->fStepTime, EP_STEP_TIME);
				break;
			case EP_WATT:
				strTemp = pDoc->ValueString(lpChannelData->fWatt, EP_WATT);
				break;
			case EP_WATT_HOUR:
				strTemp = pDoc->ValueString(lpChannelData->fWattHour, EP_WATT_HOUR);
				break;
			case EP_TEMPER:
				strTemp = pDoc->ValueString(lpChannelData->fAuxData[0], EP_TEMPER);
				break;
			default:
				strTemp.Empty();
				break;
			}
			
			if(IsNormalCell(lpChannelData->channelCode))
			{
				//정상 Cell은 Grading 설정으로 색상 표시한다.
				m_wndTrayChGrid.SetStyleRange(CGXRange(nDRow, nDCol), 
									CGXStyle().SetTextColor(0)
											  .SetInterior(pDoc->GetGradeColor(pDoc->ValueString(lpChannelData->grade, EP_GRADE_CODE)))
											  .SetValue(strTemp)
											  .SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(pDoc->ChCodeMsg(lpChannelData->channelCode, TRUE)))
											  );

			}
			else
			{
				//등급 표시 이외에서 NonCell은 표기하지 않음 
				if((pDoc->m_bHideNonCellData && (lpChannelData->channelCode == EP_CODE_CELL_NONE || lpChannelData->channelCode == EP_CODE_NONCELL )) && dataList.nDataType != EP_GRADE_CODE )
				{
					strTemp.Empty();
					m_wndTrayChGrid.SetStyleRange(CGXRange(nDRow, nDCol), 
										CGXStyle().SetTextColor(0)
												  .SetInterior(RGB(255, 255, 255))
												  .SetValue(strTemp)
												  .SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(pDoc->ChCodeMsg(lpChannelData->channelCode, TRUE)))
												  );
				}
				else
				{
					if(dataList.nDataType == EP_GRADE_CODE)
					{
						//pDoc->GetStateMsg(lpChannelData->state, colorFlag);
						m_wndTrayChGrid.SetStyleRange(CGXRange(nDRow, nDCol), 
											CGXStyle().SetTextColor(0)
													  .SetInterior(pDoc->GetGradeColor(pDoc->ValueString(lpChannelData->grade, EP_GRADE_CODE)))
													  .SetValue(strTemp)
													  .SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(pDoc->ChCodeMsg(lpChannelData->channelCode, TRUE)))
													  );

					}
					else
					{
						pDoc->GetStateMsg(lpChannelData->state, colorFlag);
						m_wndTrayChGrid.SetStyleRange(CGXRange(nDRow, nDCol), 
											CGXStyle().SetTextColor(m_pConfig->m_TStateColor[colorFlag])
													  .SetInterior(m_pConfig->m_BStateColor[colorFlag])
													  .SetValue(strTemp)
													  .SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(pDoc->ChCodeMsg(lpChannelData->channelCode, TRUE)))
													  );
					}
				}
			}

			nChNo++;
		}
	}

	DisplayMinMaxGrid(dataList.stepIndex);
	pDoc->SetProgressPos(100);
	pDoc->HideProgressWnd();
//	m_wndTrayChGrid.LockUpdate(bLockUpdate);
//	m_wndTrayChGrid.Redraw();

	return TRUE;	
}

void CResultView::SetDisplayMode(BYTE mode)
{
	CString strTemp;
	m_nDisplayMode = mode;
	m_ctrlDisplayMode.SetCurSel(m_nDisplayMode);

	STR_STEP_RESULT *lpStepData;
	/* SKI 사용안함
	if(m_nDisplayMode == TYPE_CELL_LIST)
	{
		m_ctrlStepSelCombo.EnableWindow(FALSE);
		m_wndChannelListGrid.ShowWindow(SW_HIDE);
		m_wndTrayChGrid.ShowWindow(SW_HIDE);
		m_wndCellListGrid.ShowWindow(SW_SHOW);
		m_pCurrentGrid = &m_wndCellListGrid;
	}
	else */if(m_nDisplayMode == TYPE_TRAY_LIST)
	{
		m_ctrlStepSelCombo.EnableWindow(TRUE);
		m_ctrlStepSelCombo.ResetContent();
		for(int i =0; i<m_dataList.GetSize(); i++)
		{
			Display_Data_List &dataList = m_dataList.ElementAt(i);
			m_ctrlStepSelCombo.AddString(dataList.strName);
			m_ctrlStepSelCombo.SetItemIcon(i, GetStepTypeImageIndex( dataList.nType));
		}
		m_wndChannelListGrid.ShowWindow(SW_HIDE);
		m_wndTrayChGrid.ShowWindow(SW_SHOW);
		m_wndCellListGrid.ShowWindow(SW_HIDE);
		m_pCurrentGrid = &m_wndTrayChGrid;
	}
	else	//Channel List
	{
		m_ctrlStepSelCombo.EnableWindow(TRUE);
		m_ctrlStepSelCombo.ResetContent();
		for(int i = 0; i<m_resultFile.GetStepSize(); i++)
		{
			lpStepData = m_resultFile.GetStepData(i);
			if(lpStepData)
			{
				strTemp.Format("[%d] %s", i+1, GetDocument()->GetProcTypeName(lpStepData->stepCondition.stepHeader.nProcType, lpStepData->stepCondition.stepHeader.type));
				m_ctrlStepSelCombo.AddString(strTemp);
				m_ctrlStepSelCombo.SetItemIcon(i, GetStepTypeImageIndex( lpStepData->stepCondition.stepHeader.type));
			}
			else
			{
				strTemp.Format("[%d] ", i+1);
				m_ctrlStepSelCombo.AddString(strTemp);
			}
		}

		m_wndChannelListGrid.ShowWindow(SW_SHOW);
		m_wndTrayChGrid.ShowWindow(SW_HIDE);
		m_wndCellListGrid.ShowWindow(SW_HIDE);
		m_pCurrentGrid = &m_wndChannelListGrid;
	}
}

BOOL CResultView::SaveExelFile(CString strFileName)
{
	if(strFileName.IsEmpty())	return FALSE;

	FILE *fp = fopen(strFileName, "wt");
	if(fp == NULL)	return FALSE;

	BeginWaitCursor();

	RESULT_FILE_HEADER *pHeader = m_resultFile.GetResultHeader();
	fprintf(fp, TEXT_LANG[16]+", %s\n", (char *)(LPCTSTR)m_strSelFileName);	//"파일명:"
	fprintf(fp, TEXT_LANG[17]+":,%s\n",  m_resultFile.GetModelName());			//"모델명"
	fprintf(fp, TEXT_LANG[18]+":,%s\n",  m_resultFile.GetTestName());	//"공정_조건명"
	fprintf(fp, TEXT_LANG[19]+":, %s\n",  pHeader->szTrayNo);			//"Tray ID"
	fprintf(fp, TEXT_LANG[20]+":, %s\n",  pHeader->szLotNo);			//"Batch"
	fprintf(fp, "TypeSel:, %s\n",  pHeader->szTypeSel);			// 20201211 ksj
	fprintf(fp, TEXT_LANG[21]+"No:, "+TEXT_LANG[21]+" %d-%d\n",  pHeader->nModuleID, pHeader->wGroupIndex+1);	//"모듈"
	fprintf(fp, TEXT_LANG[22]+":, %s\n",  pHeader->szDateTime);		//"시작_시각"
	fprintf(fp, TEXT_LANG[23]+":, %s\n\n",  pHeader->szOperatorID);//"운영자"
//	fprintf(fp, "설명:, %s\n",  m_pConditionMain->conditionHeader.szDescription);		// Battery No

	for(int row = 0; row<m_pCurrentGrid->GetRowCount()+1; row++)
	{
		for(int col = 1; col<m_pCurrentGrid->GetColCount()+1; col++)
		{
			fprintf(fp, "%s,", m_pCurrentGrid->GetValueRowCol(row, col));
		}
		fprintf(fp, "\n");
	}
	fclose(fp);
	EndWaitCursor();
	return TRUE;
}

/*
int CResultView::GetStepSize()
{
	return m_stepData.GetSize();
}
*/

void CResultView::OnSelchangeDisplayMode() 
{
	// TODO: Add your control notification handler code here
//	SetDisplayMode(m_ctrlDisplayMode.GetCurSel());
	DisplayData();
}

BOOL CResultView::DisplayData()
{
	SetDisplayMode((BYTE)m_ctrlDisplayMode.GetCurSel());

/* 20130826 kky SKI 사용안함.
	if(m_nDisplayMode == TYPE_CELL_LIST)
	{
		DisplayCellListAll();
//		DisplayCellList();
//		DisplayTrayChData();
	}
*/
	if(m_nDisplayMode == TYPE_CHANNEL_LIST)
	{
		DisplayChannelData();//m_stepData.GetSize()-1);
//		DisplayTrayChData();
	}
	else
	{
		DisplayTrayChData();
	}
	return TRUE;
}

void CResultView::OnFileSave() 
{
	// TODO: Add your command handler code here
	OnFileSaveAs();
}

//작업이 완료된 파일을 각 달별로 폴더를 만들어 이동 시킨다.
void CResultView::OnDataSort() 
{
	// TODO: Add your command handler code here
/*	CCTSAnalDoc *pDoc = GetDocument();
	char szFrom[256], szTo[256];


	CString lastTime; 
	lastTime = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION, "SortDate");
	COleDateTime currentTime;

	LPSHFILEOPSTRUCT lpFileOp = new SHFILEOPSTRUCT;
	ZeroMemory(lpFileOp, sizeof(SHFILEOPSTRUCT));
    lpFileOp->hwnd = NULL; 
    lpFileOp->wFunc = FO_MOVE ; 
    lpFileOp->pFrom = szFrom; 
    lpFileOp->pTo = szTo; 
    lpFileOp->fFlags = FOF_FILESONLY|FOF_NOCONFIRMATION ;//FOF_NOCONFIRMATION | FOF_NOERRORUI | FOF_FILESONLY ; 
    lpFileOp->fAnyOperationsAborted = FALSE; 
    lpFileOp->hNameMappings = NULL; 
    lpFileOp->lpszProgressTitle = NULL; 

	RESULT_FILE_HEADER *pResultFileHeader = new RESULT_FILE_HEADER;
	FILE *fp;
	COleDateTime workTime;
	CString strQuery, strFileName, strSocurFile;

	strQuery.Format("SELECT b.result_filename from test_log2 a, procedure2 b WHERE a.save_done = 'Y' AND a.test_serial_no = b.test_serial_no");	
	if(!lastTime.IsEmpty())
	{
		currentTime.ParseDateTime(lastTime);
		if(currentTime.GetStatus() == COleDateTime::valid)
		{
			strSocurFile.Format(" AND a.date_time >= '%s'", currentTime.Format("%Y/%m/%d"));
			strQuery = strQuery + strSocurFile;				
		}
	}
	currentTime = COleDateTime::GetCurrentTime();		//Get Update Time

	CRecordset rs(&pDoc->m_dbServer);
	rs.Open(CRecordset::forwardOnly, strQuery);
	while(!rs.IsEOF())
	{
		rs.GetFieldValue((short)0, strSocurFile);

		if(!strSocurFile.IsEmpty())
		{
			fp = fopen(strSocurFile, "rb");
			if(fp != NULL)
			{
/*				fread(&lpFileHeader, sizeof(EP_FILE_HEADER), 1, fp);
				if(strcmp(lpFileHeader.szFileID, RESULT_FILE_ID) != 0 && 
				   strcmp(lpFileHeader.szFileID, IROCV_FILE_ID))
				{
					fclose(fp);
					fp = NULL;
					TRACE("Not Support Result File\n");
					continue;
				}
*/
/*				fseek(fp, sizeof(EP_FILE_HEADER), SEEK_SET);
				fread(pResultFileHeader, sizeof(RESULT_FILE_HEADER), 1, fp);				//File Header Read
				workTime.ParseDateTime(pResultFileHeader->szDateTime);			//작업 일자를 구한다.
				fclose(fp);
		
				ZeroMemory(szFrom, 256);
				ZeroMemory(szTo, 256);
				sprintf(szFrom, "%s", strSocurFile);								//Source Location
				strFileName = strSocurFile.Mid(strSocurFile.ReverseFind('\\')+1);	//Get File Name
				sprintf(szTo, "%s\\%s\\%s", pDoc->m_strDataFolder, workTime.Format("%Y%m"), strFileName);		//달별고 정리 destinatin Location
				
				SHFileOperation(lpFileOp);		//Move File
			}
		}
		rs.MoveNext();
	}
	rs.Close();

	delete pResultFileHeader;
	pResultFileHeader = NULL;
	delete lpFileOp;
	lpFileOp = NULL;

	AfxGetApp()->WriteProfileString(FORMSET_REG_SECTION, "SortDate", (LPSTR)(LPCTSTR)currentTime.Format());
*/
//	AfxMessageBox("Error");
}

void CResultView::OnSysparamShow() 
{
	// TODO: Add your control notification handler code here
	//DownLoadProfile();
	CString strTemp = _T("");

	int nRtn = RunGraphAnalyzer();	

	switch( nRtn )
	{
	case -1:
		{
			AfxMessageBox(TEXT_LANG[37], MB_OK|MB_ICONINFORMATION);//"선택한 결과데이터를 찾을 수 없습니다."
		}
		break;
	case -2:
		{
			strTemp.Format(TEXT_LANG[38], m_strSelFileName);//"[%s]\n\n 결과데이터 Profile data를 찾고 있습니다. 잠시 기다려 주십시요."
			AfxMessageBox(strTemp, MB_OK|MB_ICONINFORMATION);
		}
	default:
		break;
	}
}

void CResultView::DisplayMinMaxGrid(int nStepIndex)
{
	CCTSAnalDoc *pDoc = GetDocument();
	STR_STEP_RESULT *lpStepData = m_resultFile.GetStepData(nStepIndex);//(STR_STEP_RESULT *)m_stepData[nStepIndex];
	
	if(lpStepData == NULL)	return;

	//Time
	char szTemp[128];
	CString		strTemp;
	strTemp.Format("%s (Ch %d)", pDoc->ValueString(lpStepData->averageData[EP_TIME_BIT_POS].GetMaxValue(), EP_STEP_TIME), lpStepData->averageData[EP_TIME_BIT_POS].GetMaxIndex()+1);
	m_wndMinMaxGrid.SetValueRange(CGXRange(1, 2), strTemp);
	strTemp.Format("%s (Ch %d)", pDoc->ValueString(lpStepData->averageData[EP_TIME_BIT_POS].GetMinValue(), EP_STEP_TIME), lpStepData->averageData[EP_TIME_BIT_POS].GetMinIndex()+1);
	m_wndMinMaxGrid.SetValueRange(CGXRange(2, 2), strTemp);
	m_wndMinMaxGrid.SetValueRange(CGXRange(3, 2),  pDoc->ValueString(lpStepData->averageData[EP_TIME_BIT_POS].GetAvg(), EP_STEP_TIME));
	m_wndMinMaxGrid.SetValueRange(CGXRange(4, 2),  pDoc->ValueString(lpStepData->averageData[EP_TIME_BIT_POS].GetSTDD(), EP_STEP_TIME));

	//Voltage
	sprintf(szTemp, "%s (Ch %d)", pDoc->ValueString(lpStepData->averageData[EP_VOLTAGE_BIT_POS].GetMaxValue(), EP_VOLTAGE), lpStepData->averageData[EP_VOLTAGE_BIT_POS].GetMaxIndex()+1);
	m_wndMinMaxGrid.SetValueRange(CGXRange(1, 3), szTemp);
	sprintf(szTemp, "%s (Ch %d)", pDoc->ValueString(lpStepData->averageData[EP_VOLTAGE_BIT_POS].GetMinValue(), EP_VOLTAGE), lpStepData->averageData[EP_VOLTAGE_BIT_POS].GetMinIndex()+1);
	m_wndMinMaxGrid.SetValueRange(CGXRange(2, 3), szTemp);
	sprintf(szTemp, "%s", pDoc->ValueString(lpStepData->averageData[EP_VOLTAGE_BIT_POS].GetAvg(), EP_VOLTAGE));
	m_wndMinMaxGrid.SetValueRange(CGXRange(3, 3), szTemp);
	sprintf(szTemp, "%s", pDoc->ValueString(lpStepData->averageData[EP_VOLTAGE_BIT_POS].GetSTDD(), EP_VOLTAGE));
	m_wndMinMaxGrid.SetValueRange(CGXRange(4, 3), szTemp);

	sprintf(szTemp, "%s (Ch %d)", pDoc->ValueString(lpStepData->averageData[EP_CURRENT_BIT_POS].GetMaxValue(), EP_CURRENT), lpStepData->averageData[EP_CURRENT_BIT_POS].GetMaxIndex()+1);
	m_wndMinMaxGrid.SetValueRange(CGXRange(1, 4), szTemp);
	sprintf(szTemp, "%s (Ch %d)", pDoc->ValueString(lpStepData->averageData[EP_CURRENT_BIT_POS].GetMinValue(), EP_CURRENT), lpStepData->averageData[EP_CURRENT_BIT_POS].GetMinIndex()+1);
	m_wndMinMaxGrid.SetValueRange(CGXRange(2, 4), szTemp);
	sprintf(szTemp, "%s", pDoc->ValueString(lpStepData->averageData[EP_CURRENT_BIT_POS].GetAvg(), EP_CURRENT));
	m_wndMinMaxGrid.SetValueRange(CGXRange(3, 4), szTemp);
	sprintf(szTemp, "%s", pDoc->ValueString(lpStepData->averageData[EP_CURRENT_BIT_POS].GetSTDD(), EP_CURRENT));
	m_wndMinMaxGrid.SetValueRange(CGXRange(4, 4), szTemp);

	for(int i = 3;	i< 6; i++)
	{
		sprintf(szTemp, "%s (Ch %d)", pDoc->ValueString(lpStepData->averageData[i].GetMaxValue(), EP_CAPACITY), lpStepData->averageData[i].GetMaxIndex()+1);
		m_wndMinMaxGrid.SetValueRange(CGXRange(1, i+2), szTemp);
		sprintf(szTemp, "%s (Ch %d)", pDoc->ValueString(lpStepData->averageData[i].GetMinValue(), EP_CAPACITY), lpStepData->averageData[i].GetMinIndex()+1);
		m_wndMinMaxGrid.SetValueRange(CGXRange(2, i+2), szTemp);
		sprintf(szTemp, "%s", pDoc->ValueString(lpStepData->averageData[i].GetAvg(), EP_CAPACITY));
		m_wndMinMaxGrid.SetValueRange(CGXRange(3, i+2), szTemp);
		sprintf(szTemp, "%s", pDoc->ValueString(lpStepData->averageData[i].GetSTDD(), EP_CAPACITY));
		m_wndMinMaxGrid.SetValueRange(CGXRange(4, i+2), szTemp);
	}
	
	sprintf(szTemp, "%s (Ch %d)", pDoc->ValueString(lpStepData->averageData[EP_IMPEDANCE_BIT_POS].GetMaxValue(), EP_IMPEDANCE), lpStepData->averageData[EP_IMPEDANCE_BIT_POS].GetMaxIndex()+1);
	m_wndMinMaxGrid.SetValueRange(CGXRange(1, 8), szTemp);
	sprintf(szTemp, "%s (Ch %d)", pDoc->ValueString(lpStepData->averageData[EP_IMPEDANCE_BIT_POS].GetMinValue(), EP_IMPEDANCE), lpStepData->averageData[EP_IMPEDANCE_BIT_POS].GetMinIndex()+1);
	m_wndMinMaxGrid.SetValueRange(CGXRange(2, 8), szTemp);
	sprintf(szTemp, "%s", pDoc->ValueString(lpStepData->averageData[EP_IMPEDANCE_BIT_POS].GetAvg(), EP_IMPEDANCE));
	m_wndMinMaxGrid.SetValueRange(CGXRange(3, 8), szTemp);
	sprintf(szTemp, "%s", pDoc->ValueString(lpStepData->averageData[EP_IMPEDANCE_BIT_POS].GetSTDD(), EP_IMPEDANCE));
	m_wndMinMaxGrid.SetValueRange(CGXRange(4, 8), szTemp);

	sprintf(szTemp, "%d 개", lpStepData->nNormalCount);	//정상 수량  
	
	m_ctrlNormalRate.SetText(szTemp);				
	//sprintf(szTemp, "%d 개", lpStepData->nInputCount - lpStepData->nNormalCount);		
	int nInputCellCount = m_resultFile.GetExtraData()->nInputCellNo;
	sprintf(szTemp, "%d 개", nInputCellCount - lpStepData->nNormalCount);		
	m_ctrlErrorRate.SetText(szTemp);			//불량 수량 
	if(lpStepData->nInputCount == 0)
	{
		sprintf(szTemp, "0 %%");
	}
	else
	{
		sprintf(szTemp, "%.1f %%", (float)(nInputCellCount - lpStepData->nNormalCount)/(float)nInputCellCount * 100.0f);
	}
	m_ctrlRate.SetText(szTemp);

	strTemp.Format("Step %d: Min Max Data", nStepIndex+1);
	GetDlgItem(IDC_MIN_MAX_STATIC)->SetWindowText(strTemp);

}



void CResultView::OnUpdateDataSort(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);
}

BOOL CResultView::DisplayFile(CString strFile)
{
	if(strFile.IsEmpty())	return FALSE;
	m_strSelFileName = strFile;
	OnResultFileOpen();
	return TRUE;
}

void CResultView::OnEditCopy() 
{
	// TODO: Add your command handler code here
/*	if(m_nDisplayMode == TYPE_CELL_LIST)
	{
		m_wndCellListGrid.Copy();
	}
	else if(m_nDisplayMode == TYPE_CHANNEL_LIST)
	{
		m_wndChannelListGrid.Copy();
	}
	else if(m_nDisplayMode == TYPE_TRAY_LIST)
	{
		m_wndTrayChGrid.Copy();
	}
*/
	CWnd *pWnd= GetFocus();
	if(&m_wndCellListGrid== (CMyGridWnd *)pWnd)
	{
		m_wndCellListGrid.Copy();
	}
	else if(&m_wndChannelListGrid == (CMyGridWnd *)pWnd)
	{
		m_wndChannelListGrid.Copy();
	}
	else if(&m_wndTrayChGrid == (CMyGridWnd *)pWnd)
	{
		m_wndTrayChGrid.Copy();	
	}
	else if(&m_wndMinMaxGrid == (CMyGridWnd *)pWnd)
	{
		m_wndMinMaxGrid.Copy();	
	}
	else if(&m_wndStepListGrid == (CMyGridWnd *)pWnd)
	{
		m_wndStepListGrid.Copy();	
	}
}

void CResultView::OnSelectAll() 
{
	// TODO: Add your command handler code here
/*	if(m_nDisplayMode == TYPE_CELL_LIST)
	{
		m_wndCellListGrid.SelectRange(CGXRange().SetTable());
	}
	else if(m_nDisplayMode == TYPE_CHANNEL_LIST)
	{
		m_wndChannelListGrid.SelectRange(CGXRange().SetTable());
	}
	else if(m_nDisplayMode == TYPE_TRAY_LIST)
	{
		m_wndTrayChGrid.SelectRange(CGXRange().SetTable());
	}
*/
	CWnd *pWnd= GetFocus();
	if(&m_wndCellListGrid== (CMyGridWnd *)pWnd)
	{
		m_wndCellListGrid.SelectRange(CGXRange().SetTable());
	}
	else if(&m_wndChannelListGrid == (CMyGridWnd *)pWnd)
	{
		m_wndChannelListGrid.SelectRange(CGXRange().SetTable());
	}
	else if(&m_wndTrayChGrid == (CMyGridWnd *)pWnd)
	{
		m_wndTrayChGrid.SelectRange(CGXRange().SetTable());		
	}
	else if(&m_wndMinMaxGrid == (CMyGridWnd *)pWnd)
	{
		m_wndMinMaxGrid.SelectRange(CGXRange().SetTable());		
	}
	else if(&m_wndStepListGrid == (CMyGridWnd *)pWnd)
	{
		m_wndStepListGrid.SelectRange(CGXRange().SetTable());		
	}
}

// 표시순서 : 
// CH	Cell Barcode	상태	CODE	시간	전압	전류	용량	충전취득전압  전압(T)	전류(T)	CC시간	DCIR	V1	V2	Temp DCIR평균전류
void CResultView::UpdateDataUnit()
{
	CString strVUnit;
	CString strIUnit;
	CString strCUnit;

	CString strTemp;	

	m_wndChannelListGrid.SetValueRange(CGXRange(0,1),  TEXT_LANG[41]);//"Ch No"
	m_wndChannelListGrid.SetValueRange(CGXRange(0,2),  TEXT_LANG[42]);//"Cell 번호"
	m_wndChannelListGrid.SetValueRange(CGXRange(0,3),  TEXT_LANG[43]);//"상태"
	m_wndChannelListGrid.SetValueRange(CGXRange(0,4),  TEXT_LANG[33]);//"Code"
	m_wndChannelListGrid.SetValueRange(CGXRange(0,5),  TEXT_LANG[44]);//"Grade"
	m_wndChannelListGrid.SetValueRange(CGXRange(0,6),  TEXT_LANG[45]);//"시간\n(s)"
	
	strVUnit.Format(TEXT_LANG[46], GetDocument()->m_strVUnit);//"전압(%s)"
	strIUnit.Format(TEXT_LANG[47], GetDocument()->m_strIUnit);//"전류(%s)"
	strCUnit.Format(TEXT_LANG[48], GetDocument()->m_strCUnit);	//"용량(%s)"
	m_wndChannelListGrid.SetValueRange(CGXRange(0,7),	strVUnit);
	m_wndChannelListGrid.SetValueRange(CGXRange(0,8),	strIUnit);
	m_wndChannelListGrid.SetValueRange(CGXRange(0,9),	strCUnit);

	m_wndChannelListGrid.SetValueRange(CGXRange(0,10), "Imp(mOhm)");

	if(GetDocument()->m_strCUnit == "Ah")
	{
		m_wndChannelListGrid.SetValueRange(CGXRange(0,11), "Watt(W)");
		m_wndChannelListGrid.SetValueRange(CGXRange(0,12), "Watt Hour(Wh)");
	}
	else
	{
		m_wndChannelListGrid.SetValueRange(CGXRange(0,11), "Watt(mW)");
		m_wndChannelListGrid.SetValueRange(CGXRange(0,12), "Watt Hour(mWh)");
	}
	m_wndChannelListGrid.SetValueRange(CGXRange(0,13), TEXT_LANG[49]);//"온도(℃)"
	
	// strIUnit.Format("5분전류(%s)", GetDocument()->m_strIUnit);
	strVUnit.Format(TEXT_LANG[50], GetDocument()->m_strVUnit);//"충전취득전압(%s)"
	m_wndChannelListGrid.SetValueRange(CGXRange(0,14), strVUnit);
	
	m_wndChannelListGrid.SetValueRange(CGXRange(0,15), "Com.DC-IR(mOhm)");
	
	strCUnit.Format("Com.Capacity(%s)", GetDocument()->m_strCUnit);//"전류T(%s)"
	m_wndChannelListGrid.SetValueRange(CGXRange(0,16), strCUnit);
	
	m_wndChannelListGrid.SetValueRange(CGXRange(0,17),  TEXT_LANG[53]);//"CC시간(s)"
	
	m_wndChannelListGrid.SetValueRange(CGXRange(0,18), "DC-IR(mOhm)");
	
	strVUnit.Format("V1(%s)", GetDocument()->m_strVUnit);
	m_wndChannelListGrid.SetValueRange(CGXRange(0,19), strVUnit);
	
	strVUnit.Format("V2(%s)", GetDocument()->m_strVUnit);
	m_wndChannelListGrid.SetValueRange(CGXRange(0,20), strVUnit);
	
	m_wndChannelListGrid.SetValueRange(CGXRange(0,21), TEXT_LANG[49]);//"온도(℃)"
	
	strIUnit.Format(TEXT_LANG[54], GetDocument()->m_strIUnit);//"평균전류(%s)"
	m_wndChannelListGrid.SetValueRange(CGXRange(0,22), strIUnit);	

	strVUnit.Format("DeltaOCV(%s)", GetDocument()->m_strVUnit); //"DeltaOCV(%s)"
	m_wndChannelListGrid.SetValueRange(CGXRange(0,23),	strVUnit);

	strVUnit.Format("Start Voltage(%s)", GetDocument()->m_strVUnit); //"시작전압(%s)"
	m_wndChannelListGrid.SetValueRange(CGXRange(0,24),	strVUnit);
	m_wndChannelListGrid.SetValueRange(CGXRange(0,25),	"DeltaCapacity(%)");

	m_wndMinMaxGrid.SetValueRange(CGXRange(0,1),	TEXT_LANG[55]);//"항목"
	m_wndMinMaxGrid.SetValueRange(CGXRange(0,2),	TEXT_LANG[56]);//"경과 시간(s)"
	m_wndMinMaxGrid.SetValueRange(CGXRange(0,3),	strVUnit);
	m_wndMinMaxGrid.SetValueRange(CGXRange(0,4),	strIUnit);
	m_wndMinMaxGrid.SetValueRange(CGXRange(0,5),	strCUnit);
	if(GetDocument()->m_strCUnit == "Ah")
	{
		m_wndMinMaxGrid.SetValueRange(CGXRange(0,6),	"Watt(W)");
		m_wndMinMaxGrid.SetValueRange(CGXRange(0,7),	"WattHr(Wh)");
	}
	else
	{
		m_wndMinMaxGrid.SetValueRange(CGXRange(0,6),	"Watt(mW)");
		m_wndMinMaxGrid.SetValueRange(CGXRange(0,7),	"WattHr(mWh)");
	}
	m_wndMinMaxGrid.SetValueRange(CGXRange(0,8),	"Imp(mOhm)");

	strVUnit.Format(TEXT_LANG[46], GetDocument()->m_strVUnit);//"전압(%s)"
	strIUnit.Format(TEXT_LANG[47], GetDocument()->m_strIUnit);//"전류(%s)"
	
	m_wndStepListGrid.SetValueRange(CGXRange(0,1),  TEXT_LANG[57]);//"Step"
	m_wndStepListGrid.SetValueRange(CGXRange(0,2),  TEXT_LANG[58]);//"Type"
	m_wndStepListGrid.SetValueRange(CGXRange(0,3),  TEXT_LANG[59]);//"Mode"
	m_wndStepListGrid.SetValueRange(CGXRange(0,4),  strVUnit);
	m_wndStepListGrid.SetValueRange(CGXRange(0,5),  strIUnit);
	m_wndStepListGrid.SetValueRange(CGXRange(0,6),  TEXT_LANG[60]);//"End"
	m_wndStepListGrid.SetValueRange(CGXRange(0,7),  TEXT_LANG[61]);//"시작시간"
	m_wndStepListGrid.SetValueRange(CGXRange(0,8),  TEXT_LANG[62]);//"종료시간"
	m_wndStepListGrid.SetValueRange(CGXRange(0,9),  TEXT_LANG[63]);//"Module"
	m_wndStepListGrid.SetValueRange(CGXRange(0,10), "");

	m_bDispCapSum = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "Capacity Sum", FALSE);
	GetDocument()->m_nTrayColSize = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "TrayColSize", EP_DEFAULT_TRAY_COL_COUNT);
	
	OnResultFileOpen();

//	DisplayData();	
//	DisplayTestCondition();
}

int CResultView::GetStepTypeImageIndex(int nType)
{
	switch(nType)
	{

	case EP_TYPE_CHARGE		:	return	2;
	case EP_TYPE_DISCHARGE	:	return	1;
	case EP_TYPE_REST		:	return	11;
	case EP_TYPE_OCV		:	return	7;
	case EP_TYPE_IMPEDANCE	:	return	10;
	case EP_TYPE_END		:	return	4;

	case EP_TYPE_LOOP		:
	case EP_TYPE_NONE		:
	case EP_TYPE_AGING		:
	case EP_TYPE_GRADING	:
	case EP_TYPE_SELECTING	:
	default:	return -1;
	}
}

void CResultView::OnAllFileData() 
{
	// TODO: Add your command handler code here
	if(!m_resultFile.IsLoaded())	return;

	CShowDetailResultDlg	*pDlg = NULL;
	pDlg = new CShowDetailResultDlg(&m_resultFile, this);
	pDlg->DoModal();

	delete pDlg;
	pDlg = NULL;
	
}

void CResultView::OnDblclkDataTree(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	if( m_bDblclkDataTree == false )
	{
		m_bDblclkDataTree = true;
		HTREEITEM hItem = m_wndDataTree.GetSelectedItem();
		if (hItem != NULL)
		{
			//TVITEMDATA* pData = (TVITEMDATA* )m_wndDataTree.GetItemData(hItem);
			//if (pData != NULL)
			//{
			//	ASSERT(pData->IsValid());
			
			//	DWORD dwAttributes = SFGAO_FOLDER;
			//	pData->pParentFolder->GetAttributesOf(1, &pidl, &dwAttributes);
			//	m_strSelFileName = pData->pidlAbs.GetPath();
			
			CShellPidl pidl = m_wndDataTree.GetItemIDList(hItem);
			CString str = pidl.GetPath();
			CString str1 = str.Mid(str.ReverseFind('.')+1);
			
			str1.MakeLower();
			if(str1 == BF_RESULT_FILE_EXT)
			{
				m_strSelFileName = str;
				OnResultFileOpen();
			}
			else if(str1 == "csv" || str1 == "xls")	//EXCEL Open
			{
				//Excel open
				TRACE("Excel open\n");
				
				CString strMsg = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"Excel Path", "");
				GetDocument()->ExecuteProgram(strMsg, str);
			}
			//}
		}
		*pResult = 0;
		m_bDblclkDataTree = false;
	}
}

void CResultView::OnDataPath() 
{
	// TODO: Add your command handler code here
	CSetDataFolderDlg dlg;
	if(dlg.DoModal() == IDOK)
	{
		LoadDataTree();
	}
}

bool CResultView::LoadDataTree()
{
	m_wndDataTree.DeleteAllItems();
	
	CString strTmp, str;
	for(int i = 0; i<10; i++)
	{
		strTmp.Format("Data%d", i);
		str = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION , strTmp);
		if(str.IsEmpty() == FALSE)
		{
			CShellPidl pidl(str, m_wndDataTree.m_hWnd);
			m_wndDataTree.SetRedraw(FALSE);
			m_wndDataTree.SetRedraw(TRUE);
			
			if(pidl)
			{
				m_wndDataTree.AddRootFolderContent(pidl, STCF_INCLUDEALL);
			}
		}
	}
	return true;
}

void CResultView::OnFieldSet() 
{
	// TODO: Add your command handler code here
	GetDocument()->SetDataField();
	/* SKI 사용안함.
	if(m_nDisplayMode == TYPE_CELL_LIST)
	{
		DisplayCellListAll();
	}
	*/
}

void CResultView::OnContextMenu(CWnd* pWnd, CPoint point) 
{
	// TODO: Add your message handler code here
	if (GetFocus()->m_hWnd != m_wndDataTree.m_hWnd)
	{
		Default();
		return;
	}

	// get clicked item
	UINT nFlags = 0;
	m_wndDataTree.ScreenToClient(&point);

	HTREEITEM hItem = m_wndDataTree.HitTest(point, &nFlags);

	TRACE("H:0x%x, %d, %d\n", hItem, point.x, point.y);

	if (!(nFlags & TVHT_ONITEM))
		return;

	// load menu
	CMenu menu;
	menu.LoadMenu(IDR_SHELL_MENU);
	CMenu *pPopupMenu = menu.GetSubMenu(0);

	// Ctrl key down, make a submenu
	BOOL bPopup = (GetAsyncKeyState(VK_CONTROL) & 0x8000);

	// search insert position
	UINT nInsertAt = 0;
	CMenu *pMenu = FindMenuItemByText(pPopupMenu, _T("&Shell"), nInsertAt);
	if (pMenu == NULL)
	{
		pMenu = pPopupMenu;
		nInsertAt = 0;
	}
	else if (bPopup)
	{
		// add a separator
		pMenu->InsertMenu(nInsertAt+1, MF_BYPOSITION|MF_SEPARATOR, 0);

		pMenu = pMenu->GetSubMenu(nInsertAt);
		nInsertAt = 0;
	}
	pMenu->DeleteMenu(nInsertAt, MF_BYPOSITION);

	m_wndDataTree.GetItemContextMenu(hItem, m_shellMenu);
	if (!m_shellMenu.FillMenu(pMenu, nInsertAt, IDM_FIRST_SHELLMENUID,
		IDM_LAST_SHELLMENUID, CMF_NODEFAULT|CMF_EXPLORE))
	{
		// no items added
		if (bPopup)
			pMenu->AppendMenu(MF_GRAYED, ID_SHELLMENU, _T("(empty)"));
	}

	// display menu
	m_shellMenu.SetOwner(this);
	m_wndDataTree.ClientToScreen(&point);
	pPopupMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON,
		point.x, point.y, this);


//	HTREEITEM hPItem  = m_wndDataTree.GetParentItem(hItem);
//	m_wndDataTree.ReDrawItem(hItem);

	m_shellMenu.SetOwner(NULL);
	
}

CMenu* CResultView::FindMenuItemByText(CMenu *pMenu, LPCTSTR pszText, UINT &nIndex, BOOL bRecursive)
{
	if (!::IsMenu(pMenu->GetSafeHmenu()))
		return NULL;

	CString text;
	UINT count = pMenu->GetMenuItemCount();
	for (UINT id=0; id < count; id++)
	{
		if (pMenu->GetMenuString(id, text, MF_BYPOSITION) > 0
			&& text == pszText)
		{
			nIndex = id;
			return pMenu;
		}
		// search recursively in sub-menus
		if (bRecursive && pMenu->GetMenuState(id, MF_BYPOSITION) & MF_POPUP)
		{
			CMenu *pSubMenu = FindMenuItemByText(pMenu->GetSubMenu(id),
				pszText, nIndex, bRecursive);
			if (pSubMenu != NULL)
				return pSubMenu;
		}
	}
	return NULL;
}

void CResultView::OnShellCommand(UINT nID) 
{
	// shell command
	BOOL bRtn = m_shellMenu.InvokeCommand(nID);
	
	if(bRtn)
	{
		if(nID == IDM_FIRST_SHELLMENUID+16)	//make short cut
		{
			//AfxMessageBox("Make short cut");
		}

		//Redraw current item
		if(nID == IDM_FIRST_SHELLMENUID+17)
		{
			//LoadDataTree();
			//AfxMessageBox("Delete item");
		}

		//open command
		if(nID == IDM_FIRST_SHELLMENUID+100)
		{
		}

		HTREEITEM hItem;
		hItem = m_wndDataTree.GetSelectedItem();
		m_wndDataTree.ReDrawItem(hItem);
	}
}

void CResultView::ViewCpCpk()
{
	// TODO: Add your control notification handler code here
	CStringArray aStrFile;
	SearchSameLotFile(aStrFile);
	GetDocument()->ShowCpCpkData(m_resultFile.GetLotNo(), aStrFile);

/*	double         dMax = -10E+100, dMin = 10E+100;
	CString        str;
	double			dData1;

	static double m_dDivision = 1;
	static double m_dLowLimit = 0;
	static double m_dHighLimit = 0;
	static int m_nCpItem = 0;

//////Cp, Cpk Set Dlg
	CCpkSetDlg     dlgCpkSet;
	dlgCpkSet.m_strHighRange.Format("%.4f", m_dHighLimit);
	dlgCpkSet.m_strLowRange.Format("%.4f", m_dLowLimit);
	dlgCpkSet.m_strGraphGap.Format("%.4f", m_dDivision);
	dlgCpkSet.m_nItem = m_nCpItem;
	dlgCpkSet.m_strTitle.Format("Lot [%s] 분포도 보기", m_resultFile.GetLotNo());
   	
	CCTSAnalDoc *pDoc= (CCTSAnalDoc *)GetDocument();
	dlgCpkSet.m_nCount = pDoc->m_TotCol;
	for(int i =0; i<pDoc->m_TotCol; i++)
	{
		dlgCpkSet.m_field[i].No = pDoc->m_field[i].No;
		dlgCpkSet.m_field[i].FieldName = pDoc->m_field[i].FieldName;
		dlgCpkSet.m_field[i].ID = pDoc->m_field[i].ID;
   		dlgCpkSet.m_field[i].DataType = pDoc->m_field[i].DataType;
		dlgCpkSet.m_field[i].bDisplay = pDoc->m_field[i].bDisplay;
	}
	
	if(dlgCpkSet.DoModal() != IDOK)	return;
	
	m_dHighLimit = atof(dlgCpkSet.m_strHighRange);
	m_dLowLimit  = atof(dlgCpkSet.m_strLowRange);
	m_dDivision  = atof(dlgCpkSet.m_strGraphGap);
	m_nCpItem = dlgCpkSet.m_nItem;
//////
	int procType = pDoc->m_field[m_nCpItem].ID;
	int dataType = pDoc->m_field[m_nCpItem].DataType;

	CList<double, double&> dDataBuffer;   
	CString strRltFile, strTemp, strSrcFile;
	strSrcFile = m_resultFile.GetFileName();
	strRltFile = strSrcFile.Left(strSrcFile.ReverseFind('\\'));
//	CFileFind affind;
//	BOOL bFind = affind.FindFile(strRltFile+"\\*."+BF_RESULT_FILE_EXT);

	CStringArray aStrFile;
	SearchSameLotFile(aStrFile);

	STR_STEP_RESULT *lpStepDataInfo;
	STR_SAVE_CH_DATA *lpChannelData;
	CFormResultFile *pRltFile = new CFormResultFile;
	for(int n=0; n<aStrFile.GetSize(); n++)
	{
		strRltFile = aStrFile.GetAt(n);
		if(pRltFile->ReadFile(strRltFile))
		{
			if(m_resultFile.GetLotNo() == pRltFile->GetLotNo())// && m_resultFile.GetTrayNo() == pRltFile->GetTrayNo())
			{
				for(int step = 0; step<pRltFile->GetStepSize(); step++)
				{
					lpStepDataInfo = pRltFile->GetStepData(step);
					if(lpStepDataInfo)
					{
						if(lpStepDataInfo->stepCondition.stepHeader.nProcType == procType)// || pDoc->m_field[f].ID == RPT_CH_CODE)
						{
							for(int c = 0; c<lpStepDataInfo->aChData.GetSize(); c++)
							{
								lpChannelData = (STR_SAVE_CH_DATA *)lpStepDataInfo->aChData[c];
								if(!IsNormalCell((BYTE)lpChannelData->channelCode))	 continue;	//정상이 아니면 계산 않함

								//find matting data type
								switch(dataType)
								{
								case EP_VOLTAGE:
									dData1 = atof(pDoc->ValueString(lpChannelData->fVoltage, dataType));
									if(dMin > dData1)  dMin = dData1;	//Min
									if(dMax < dData1)  dMax = dData1;	//Max
									dDataBuffer.AddTail(dData1);
									break;
								case EP_CURRENT:
									dData1 = atof(pDoc->ValueString(lpChannelData->fCurrent, dataType));
									if(dMin > dData1)  dMin = dData1;	//Min
									if(dMax < dData1)  dMax = dData1;	//Max
									dDataBuffer.AddTail(dData1);
									TRACE("Add s%d, %d, %d, %s\n", c+1, procType, dataType, strRltFile);
									break;
								case EP_CAPACITY:
									dData1 = atof(pDoc->ValueString(lpChannelData->fCapacity, dataType));
									if(dMin > dData1)  dMin = dData1;	//Min
									if(dMax < dData1)  dMax = dData1;	//Max
									dDataBuffer.AddTail(dData1);
									break;
								case EP_STEP_TIME:
									dData1 = atof(pDoc->ValueString(lpChannelData->fStepTime, dataType));
									if(dMin > dData1)  dMin = dData1;	//Min
									if(dMax < dData1)  dMax = dData1;	//Max
									dDataBuffer.AddTail(dData1);
									break;
								case EP_IMPEDANCE:
									dData1 = atof(pDoc->ValueString(lpChannelData->fImpedance, dataType));
									if(dMin > dData1)  dMin = dData1;	//Min
									if(dMax < dData1)  dMax = dData1;	//Max
									dDataBuffer.AddTail(dData1);
									break;
								}
							}
						}
					}
				}
			}
		}
	}
	delete pRltFile;
	pRltFile = NULL;

	long lCount = dDataBuffer.GetCount();
	if(lCount <= 0)
	{
		MessageBox("범위내의 해당하는 Cell이 없습니다.", "Data 오류", MB_ICONSTOP|MB_OK);
		return;
	}

	//Memory 할당 받는다. (양품수로 해서 => 최대 가능 범위)
	double *pData = new double[lCount];
	ASSERT(pData);
	ZeroMemory(pData, sizeof(double)*lCount);	
	lCount = 0;

	POSITION pos = dDataBuffer.GetHeadPosition();
	while(pos)
	{
		dData1 = dDataBuffer.GetNext(pos);
		if(dlgCpkSet.m_bRange == TRUE)
		{
   			if(dlgCpkSet.m_bHighRange == TRUE && dlgCpkSet.m_bLowRange == TRUE) //상하한 설정시  
			{
   				if(m_dLowLimit <= dData1 && dData1 <= m_dHighLimit)
				{
					pData[lCount++] = dData1;
				}		    					
			}
			else if(dlgCpkSet.m_bHighRange == TRUE && dlgCpkSet.m_bLowRange == FALSE)//상한만
			{
				if(dData1 <= m_dHighLimit)
				{
					pData[lCount++] = dData1;
				}
			}
			else if(dlgCpkSet.m_bHighRange == TRUE && dlgCpkSet.m_bLowRange == FALSE)//하한만
			{
	  			if(m_dLowLimit <= dData1)
				{
					pData[lCount++] = dData1;
				}
			}
		}
		else   // m_pCpkSetDlg.m_bRange = FALSE
		{
			pData[lCount++] = dData1;
		}
	}
		
	ASSERT(lCount > 0);	
	//Cp, Cpk Graph  Display
	CCpkViewDlg*	pCpkViewDlg = new CCpkViewDlg(this);
	pCpkViewDlg->SetRowData(pData, lCount, m_dDivision);
	pCpkViewDlg->SetHighLimit(dlgCpkSet.m_bHighRange, m_dHighLimit);
	pCpkViewDlg->SetLowLimit(dlgCpkSet.m_bLowRange, m_dLowLimit);
	pCpkViewDlg->m_strTitle.Format("%s %s 분포도(%d개)", m_resultFile.GetLotNo(), pDoc->m_field[m_nCpItem].FieldName, lCount);
	pCpkViewDlg->DoModal();
	delete pCpkViewDlg;

	delete [] pData;
	pData = NULL;
*/		
}

void CResultView::OnCpButton() 
{
	// TODO: Add your control notification handler code here
	if(m_resultFile.IsLoaded() == FALSE)	return;

	ViewCpCpk();
}

void CResultView::OnButtonSearch() 
{
	// TODO: Add your control notification handler code here
	OnRltFileOpen();
}

void CResultView::OnButtonLotAll() 
{
	// TODO: Add your control notification handler code here
	if(m_resultFile.IsLoaded() == FALSE)	return;

	CStringArray aStrFile;
	SearchSameLotFile(aStrFile);

	//Find same lot file list
	//////////////////////////////////////////////////////////////////////////
/*	CString strRltFile, strSrcFile;
	strSrcFile = m_resultFile.GetFileName();
	strRltFile = strSrcFile.Left(strSrcFile.ReverseFind('\\'));
	CFileFind affind;
	BOOL bFind = affind.FindFile(strRltFile+"\\*."+BF_RESULT_FILE_EXT);
	CFormResultFile *pRltFile = new CFormResultFile;
	while(bFind)
	{
		bFind = affind.FindNextFile();
		strRltFile = affind.GetFilePath();
		if(pRltFile->ReadFile(strRltFile))
		{
			if(m_resultFile.GetLotNo() == pRltFile->GetLotNo())// && m_resultFile.GetTrayNo() == pRltFile->GetTrayNo())
			{
				aStrFile.Add(strRltFile);
			}
		}
	}
	delete pRltFile;
	pRltFile = NULL;
	//////////////////////////////////////////////////////////////////////////
	
	CSerarchDataDlg dlg(GetDocument(), this);
	dlg.SetFileName(&aStrFile);
	CString str;
	str.Format("Lot %s 검색 Tray data 보기", m_resultFile.GetLotNo());
	dlg.SetTitle(str);
	dlg.DoModal();	
*/
	GetDocument()->ShowLotAllData(m_resultFile.GetLotNo(), aStrFile);
}

BOOL CResultView::DownLoadProfile()
{
	if(!m_resultFile.IsLoaded())	return FALSE;

	GetDocument()->DownLoadProfileData(m_strSelFileName);
	
	return TRUE;
}

void CResultView::OnSensorButton() 
{
	// TODO: Add your control notification handler code here
	if(!m_resultFile.IsLoaded())	return;

	CSensorDataDlg dlg(&m_resultFile, this);
	dlg.SetDocumnet(GetDocument());
	dlg.DoModal();
}

void CResultView::OnGradeColorSet() 
{
	// TODO: Add your command handler code here

	STR_GRADE_COLOR_CONFIG gradeConfig = GetDocument()->m_gradeColorConfig;
	CGradeColorDlg dlg;
	dlg.m_GradeColor = gradeConfig;
	if(dlg.DoModal() == IDOK)
	{
		gradeConfig = dlg.m_GradeColor;
		GetDocument()->m_gradeColorConfig = gradeConfig;
		::WriteGradeColorConfig(gradeConfig);
	}
}

void CResultView::OnMesUpdateButton() 
{
	if(m_resultFile.IsLoaded() == FALSE)	return;

	CString strTemp;
	strTemp.Format(TEXT_LANG[64], //"Tray [%s]의 [%s] 결과 data를 MES Server로 전송합니다. Tray 번호와 Lot 번호를 입력하십시요."
		m_resultFile.GetTrayNo(), m_resultFile.GetTestName());

	//TrayNo, Cell serial no update
	CCellInfoDlg dlg;
	dlg.m_strTitle = strTemp;
	dlg.m_strTrayNo = m_resultFile.GetTrayNo();
	dlg.m_strCellSerial = m_resultFile.GetLotNo();
	
	if(dlg.DoModal() != IDOK)	return;
	
	//Tray 정보나 Cell Serial이 수정된 경우 결과 Data를 Update 함
	if(dlg.m_strTrayNo != m_resultFile.GetTrayNo() || dlg.m_strCellSerial != m_resultFile.GetLotNo())
	{
		if(m_resultFile.UpdateTestInfo(dlg.m_strTrayNo, dlg.m_strCellSerial) == FALSE)
		{
			strTemp.Format(TEXT_LANG[65]);//"결과 정보를 Update 할 수 없습니다."
			AfxMessageBox(strTemp);
			return;
		}
		m_ctrlFileName.SetText(m_resultFile.GetTrayNo());
		m_ctrlTestLotNo.SetText(m_resultFile.GetLotNo());
	}
	
	CWaitCursor wait;

	//Find same lot file list
	//////////////////////////////////////////////////////////////////////////
	CString strSrcFile;
	strSrcFile = m_resultFile.GetFileName();
	HWND hWnd = ::FindWindow(NULL, CTSDATA_SERVER);
	if(hWnd)
	{
		char szData[MAX_PATH];
		ZeroMemory(szData, MAX_PATH);
		sprintf(szData, "%s", strSrcFile);
		COPYDATASTRUCT CpStructData;
		CpStructData.cbData = strlen(szData);
		CpStructData.lpData = szData;
		CpStructData.dwData = 1;
		
		if(::SendMessage(hWnd, WM_COPYDATA, 0, (LPARAM)&CpStructData))
		{
			strSrcFile.Format(TEXT_LANG[66], m_resultFile.GetTrayNo());//"TRAY [%s] data를 Server로 전송 완료 하였습니다."
			AfxMessageBox(strSrcFile);
		}
		else
		{
			strSrcFile.Format(TEXT_LANG[67], m_resultFile.GetTrayNo());//"TRAY [%s] data 전송을 실패 하였습니다."
			AfxMessageBox(strSrcFile, MB_OK|MB_ICONSTOP);
		}
	}
	else
	{
		AfxMessageBox(TEXT_LANG[68]);//"CTSData Server를 찾을 수 없습니다."
	}			
}

BOOL CResultView::SearchSameLotFile(CStringArray &aFileList)
{
	CWaitCursor wati;
	CString strTemp, strFolder;
	DWORD start = GetTickCount();
	for(int i =0; i<10; i++)
	{
		strTemp.Format("Data%d", i);
		strFolder = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION, strTemp);		//Get Data Folder
		if(strFolder.IsEmpty())	continue;


		FindSameLotFile(strFolder, m_resultFile.GetLotNo(), aFileList);

	}		
	TRACE("Elapsed time is %dmsec\n", GetTickCount()-start);
	
	return TRUE;
}

//strFolder에서 strFilterLot에 해당하는 파일 리스트를 검색한다.
BOOL CResultView::FindSameLotFile(CString strFolder, CString strFilerLot, CStringArray &aFile, BOOL bIncludeSubFolder)
{
	CString strLot, strTray;
	CStringArray	aTray;
	CFileFind FileFinder;
	BOOL bFind = FileFinder.FindFile(strFolder+"\\*");
	while(bFind)
	{
		bFind = FileFinder.FindNextFile();
		
		if(FileFinder.IsDots())
		{
			continue;
		}
		else if(FileFinder.IsDirectory())
		{
			if(bIncludeSubFolder)
			{
				FindSameLotFile(FileFinder.GetFilePath(), strFilerLot, aFile);
			}
		}
		else	
		{
			//*.fmt file
			CString strFileName(FileFinder.GetFilePath());
			CString strExt(strFileName.Mid(strFileName.ReverseFind('.')+1));
			strExt.MakeLower();
			if(strExt == BF_RESULT_FILE_EXT)
			{
				if(CFormResultFile::GetLotTrayNo(strFileName, strLot, strTray))
				{
					if(strLot == strFilerLot)
					{
						aFile.Add(strFileName);
						aTray.Add(strTray);
					}
				}

/*				CFormResultFile rltFile;
				if(rltFile.ReadFile(FileFinder.GetFilePath()))
				{
					if(strFilerLot == rltFile.GetLotNo())
					{
						aFile.Add(strFileName);
					}
				}
*/
			}
		}
	}

	return TRUE;
}

void CResultView::OnFileRawView() 
{
	// TODO: Add your command handler code here
	OnAllFileData();	
}

void CResultView::OnUpdateFileRawView(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(m_resultFile.IsLoaded());
}

// Rtn ====> -1 : 결과데이터를 찾을 수 없습니다.
// Rtn ====> -2 : Raw 데이터를 찾고 있는 중.
int CResultView::RunGraphAnalyzer()
{
	if(!m_resultFile.IsLoaded())	
		return -1;

	CString str = m_strSelFileName.Left(m_strSelFileName.ReverseFind('.'));

	CFileFind ff;
	if(ff.FindFile(str))
	{
		ff.FindNextFile();
		//20090920 KBH
		if(ff.IsDirectory())
		{
			STR_STEP_RESULT *lpStepData = m_resultFile.GetLastStepData();
			if(lpStepData)
			{
				if(lpStepData->stepCondition.stepHeader.type == EP_TYPE_END)
				{
					GetDocument()->ExecuteGraphAnalyzer(str);
					return TRUE;
				}
			}
		}
	}

	DownLoadProfile();
	
	return -2;
}

//////////////////////////////////////////////////////////
//														//
//					결과 파일 저장 구조					//
//														//		
//////////////////////////////////////////////////////////
//	1. EP_FILE_HEADER									//
//	2. RESULT_FILE_HEADER								//
//	3. EP_MD_SYSTEM_DATA								//
//	4. STR_FILE_EXT_SPACE								//	
//	5. EP_TEST_HEADER									//
//	6. STR_CONDITION_HEADER								//
//	7. EP_PRETEST_PARAM									//
//	8. 각 Step 구조 * Step 수							//
//////////////////////////////////////////////////////////			
//	9~13 항을 진행 Step 수 만큼 저장 한다.				//
//	9. GROUP_STATE_SAVE									//	
//	10. RESULT_FILE_HEADER								//
//	11	STR_COMMON_STEP									//
//	12. EP_STEP_END_HEADER								//
//	13. STR_SAVE_CH_DATA * Channel 수					//
//														//
//////////////////////////////////////////////////////////
int CResultView::Fun_RestoreResultdata()
{
	// TODO: Add your control notification handler code here
	// 1. *.fmt 파일에서 결과데이터 정보를 읽어오고
	// 2. TestSerial 번호를 이용해서 SBC에 StepEnd 결과데이터가 존재하는 지 확인 후
	// 3. "Schedule.mdb" 파일에서 IP정보를 읽어온다.	
	// 4. SBC 에서 TestSerial 번호로 데이터를 찾은 후 임시폴더에 다운로드 한다.
	// 5. 기존의 결과데이터의 확장자는 bak로 바꾼다.
	// 6. 데이터를 파싱한 후 결과데이터를 생성한다.
	bool bRtn = false;	
	CString strTemp = _T("");
	CString address = _T("");
	CString strSyncCode = _T("");
	CString srcFileName = _T("");
	CString destFileName = _T("");
	CString strSelFileName = _T("");
	int nGroupNo = 0;
	
	if(m_strSelFileName.IsEmpty())	
	{	
		return -1;
	}

	if(m_resultFile.ReadFile(m_strSelFileName) < 0)		
	{		
		return -2;
	}	

	// 1. UNIT 의 IP를 DataBase에서 확인
	CDaoDatabase  db;
	CString strDataBaseName = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION, "DataBase");
	if(strDataBaseName.IsEmpty())	
	{
		return -3;
	}

	strDataBaseName = strDataBaseName+ "\\" + FORM_SET_DATABASE_NAME;
	
	db.Open(strDataBaseName);
		
	CString strSQL;
	strSQL.Format("SELECT IP_Address, Data5 FROM SystemConfig WHERE ModuleID = %d", m_resultFile.GetModuleID());
		
	CDaoRecordset rs(&db);

	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
		if(!rs.IsBOF())
		{
			COleVariant data = rs.GetFieldValue(0);
			address = data.pbVal;

			data = rs.GetFieldValue(1);
			nGroupNo = data.intVal;
		}
	}
	catch (CException* e)
	{
		TCHAR sz[255];
		e->GetErrorMessage(sz,255);
		AfxMessageBox(sz);
		e->Delete();
	}		
		
	rs.Close();
	db.Close();

	if( address.IsEmpty() == true )
	{
		return -4;
	}

	strTemp.Format(TEXT_LANG[69], m_strSelFileName);//"[%s]\n\n 결과데이터 복구를 실행하시겠습니까?"
	if( MessageBox(strTemp, TEXT_LANG[70], MB_ICONQUESTION|MB_YESNO) != IDYES )//"결과데이터 복구"
	{
		return -9;
	}

	char szBuff[_MAX_PATH];
	::GetTempPath(_MAX_PATH, szBuff);		//Get Windows temp. folder
	
	CString strStepEndFile = _T("");
	CString strRemoteFileName = _T("");
	CString strWinTempFolder(szBuff);

	RESULT_FILE_HEADER *pResultHeader =  m_resultFile.GetResultHeader();

	CString strRemoteLocation = GetRemoteLocation( nGroupNo, m_resultFile.GetTestSerialNo(), address);
	if(strRemoteLocation.IsEmpty())
	{
		return -5;
	}
	
	char	drive[_MAX_PATH]; 
	char	dir[_MAX_PATH]; 
	char	fname[_MAX_PATH]; 
	char	ext[_MAX_PATH]; 
	_splitpath((LPSTR)(LPCTSTR)m_strSelFileName, drive, dir, fname, ext);	
	
	srcFileName =  m_strSelFileName;								// 복구하고자 하는 파일
	destFileName.Format("%s\\%s\\%s.bak", drive, dir, fname);		// 복구해서 임시로 생성하는 파일
	_unlink(destFileName);											// 동일명으로 backup file이 있으면 삭제한다.
	rename(m_strSelFileName, destFileName);							// 현재 파일을 확장자 .bak으로 해서 이름을 바꾼다.
	
	//DownLoad Channel result file from remote...
	CFtpDownLoad *pDownLoad = new CFtpDownLoad;
	BOOL bConnect = pDownLoad->ConnectFtpSession(address);
	if(bConnect == FALSE)
	{
		delete pDownLoad;
		return -6;
	}

	int nChTotNo = m_resultFile.GetTotalCh();
	for(int nChIndex = 0; nChIndex <nChTotNo ; nChIndex++)
	{
		//1. Step 종료값을 DownLoad한다.
		strRemoteFileName.Format("%s/ch%03d_SaveData.csv", strRemoteLocation, nChIndex+1);
		strStepEndFile.Format("%s\\%03d_stepEndData_ch%03d.csv", strWinTempFolder, m_resultFile.GetModuleID(), nChIndex+1);
		if(pDownLoad->DownLoadFile(strStepEndFile, strRemoteFileName) < 1)
		{
			delete pDownLoad;
			return -7;			
		}
	}
	delete pDownLoad;

	//Module에서 Download한 파일을 Parsing한다.
	int nMaxStepCount = 0, nTemp;
	CModuleStepEndData *lpChData = new CModuleStepEndData[nChTotNo];
	// ZeroMemory(lpChData, sizeof(CModuleStepEndData) * nChTotNo);

	for(int ch = 0; ch <nChTotNo; ch++)
	{
		strStepEndFile.Format("%s\\%03d_stepEndData_ch%03d.csv", strWinTempFolder, m_resultFile.GetModuleID(), ch+1);
		nTemp = lpChData[ch].LoadData(ch, strStepEndFile);
	
		if(nTemp  > nMaxStepCount)
		{
			nMaxStepCount = nTemp;
		}
	}	

	//저장된 Step data가 없는 채널이 있음 
	/*
	if(nMaxStepCount < 2)
	{
		delete[] lpChData;
		return -7;
	}
	else
	{
		// 마지막 Step 종료 메세지는 무시
		nMaxStepCount = nMaxStepCount - 1;
	}
	*/
	if(nMaxStepCount < 1)
	{
		delete[] lpChData;
		return -7;
	}

	// 1. COM 계산을 위한 공식을 가져온다.
	CString strCapacityFormula;
	CString strDCRFormula;

	HKEY hKey;
	long rtn1;
	DWORD size;
	DWORD type;

	//20200220 KSJ
 	STR_STEP_RESULT* pStepResult = m_resultFile.GetStepData(0);
	if( pStepResult != NULL )
	{
		if(pStepResult->stepCondition.stepHeader.nProcType == EP_PROC_TYPE_END1)
		{
			nMaxStepCount = 1;
		}
	}

// 	rtn1 = RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\CTSMon\\SystemSetting", 0, KEY_READ, &hKey);
// 	if ( rtn1 == ERROR_SUCCESS )
// 	{		
// 		BYTE buf[512];
// 
// 		RegQueryValueEx(hKey, "CapacityFormula", NULL, &type, buf, &size);
// 		strCapacityFormula.Format("%s", buf );
// 
// 		if( strCapacityFormula.IsEmpty() )
// 		{
// 			strCapacityFormula = "C+(0.003341-0.03567*(T-25)+0.003342*((T-25)^2)-0.00011*((T-25)^3))";
// 		}
// 
// 		RegQueryValueEx(hKey, "DCRFormula", NULL, &type, buf, &size);
// 		strDCRFormula.Format("%s", buf );
// 
// 		if( strDCRFormula.IsEmpty() )
// 		{
// 			strDCRFormula = "D/(0.000140218657265343*(2.71^(2626.15545994168/(273+T))))";
// 		}
// 	}
// 
// 	RegCloseKey(hKey);

	FILE *fp = NULL;

	//Create Result File=> 파일 존재 여부 검사	
	// m_strSelFileName.Replace("\\", "\\\\");
	fp = fopen(m_strSelFileName, "wb+");	
	
	EP_FILE_HEADER *pFileHeader = m_resultFile.GetFileHeader();
	if(fwrite(pFileHeader, sizeof(EP_FILE_HEADER), 1, fp) < 1)
	{
		fclose(fp);
		return -8;
	}
	
	RESULT_FILE_HEADER *pHeader = m_resultFile.GetResultHeader();
	if(fwrite(pHeader, sizeof(RESULT_FILE_HEADER), 1, fp) < 1)
	{
		fclose(fp);
		return -8;
	}

	EP_MD_SYSTEM_DATA *lpGroupSysData = m_resultFile.GetMDSysData();
	if(fwrite(lpGroupSysData, sizeof(EP_MD_SYSTEM_DATA), 1, fp) < 1)
	{
		fclose(fp);
		return -8;
	}

	STR_FILE_EXT_SPACE *pExtData =  m_resultFile.GetExtraData();
	if(fwrite(pExtData, sizeof(STR_FILE_EXT_SPACE), 1, fp) < 1)	
	{
		fclose(fp);
		return -8;
	}

	PNE_RESULT_CELL_SERIAL *lpResultCellSerial= m_resultFile.GetResultCellSerial();
	if(fwrite(lpResultCellSerial, sizeof(PNE_RESULT_CELL_SERIAL), 1, fp) < 1)
	{
		fclose(fp);
		return -8;
	}	

	SENSOR_MAPPING_SAVE_TABLE sensorMap;	
	sensorMap.nModuleID = m_resultFile.GetModuleID();
	memcpy(&sensorMap.mapData1, m_resultFile.GetSensorMap(CFormResultFile::sensorType1), sizeof(_MAPPING_DATA)*EP_MAX_SENSOR_CH);
	memcpy(&sensorMap.mapData2, m_resultFile.GetSensorMap(CFormResultFile::sensorType2), sizeof(_MAPPING_DATA)*EP_MAX_SENSOR_CH);
		
	if(fwrite(&sensorMap, sizeof(SENSOR_MAPPING_SAVE_TABLE), 1, fp) < 1)
	{
		fclose(fp);
		return -8;
	}

	CTestCondition *pCon = m_resultFile.GetTestCondition();	
	if(fwrite(pCon->GetModelInfo(), sizeof(STR_CONDITION_HEADER), 1, fp) != 1)
	{
		fclose(fp);
		return -8;
	}

	if(fwrite(pCon->GetTestInfo(), sizeof(STR_CONDITION_HEADER), 1, fp) != 1)
	{
		fclose(fp);
		return -8;
	}

	EP_TEST_HEADER testHeader;
	ZeroMemory(&testHeader, sizeof(EP_TEST_HEADER));	
	testHeader.totalStep = (byte)pCon->GetTotalStepNo();
	testHeader.totalGrade = (byte)pCon->GetCountIsGradeStep();

	if(fwrite(&testHeader, sizeof(EP_TEST_HEADER), 1, fp) != 1)			
	{
		fclose(fp);
		return -8;
	}
	
	if(fwrite(pCon->GetCheckParam(), sizeof(STR_CHECK_PARAM), 1, fp) != 1)			
	{
		fclose(fp);
		return -8;
	}

	int step = 0;
	for(step = 0; step < pCon->GetTotalStepNo(); step++)
	{
		if(fwrite(&pCon->GetStep(step)->GetStepData(), sizeof(STR_COMMON_STEP), 1, fp) != 1)
		{
			fclose(fp);			
			return -8;
		}
	}	

	// STR_STEP_RESULT *pStepResult;
	EP_STEP_END_HEADER endHeader;
	STR_STEP_RESULT StepResult;
	_MAPPING_DATA *pMapData;

				
	// for( step = 0; step < m_resultFile.GetLastIndexStepNo(); step++ )
	 for( step = 0; step < pCon->GetTotalStepNo(); step++ )
	// 1. SBC 에 저장된 Step 데이터만큼 복구를 진행
//	for( step = 0; step < nMaxStepCount; step++ )
	{
		//////////////////////////////////////////////////////////////////////////
		// 1. Step에서 결과데이터가 존재하면 기존의 데이터를 사용하고
		// 2. Step에서 손실된 데이터가 있으면 SBC에서 저장한 데이터를 복구한다. 
		//////////////////////////////////////////////////////////////////////////
		pStepResult = m_resultFile.GetStepData(step);
		if( pStepResult != NULL )
		{
			if(fwrite(&pStepResult->gpStateSave, sizeof(GROUP_STATE_SAVE), 1, fp) != 1)
			{
				fclose(fp);			
				return -8;
			}
			
			if(fwrite(&pStepResult->stepHead, sizeof(RESULT_STEP_HEADER), 1, fp) != 1)
			{
				fclose(fp);			
				return -8;
			}
			
			if(fwrite(&pStepResult->stepCondition, sizeof(STR_COMMON_STEP), 1, fp) != 1)
			{
				fclose(fp);			
				return -8;
			}
			
			endHeader.wStep = step;
			if(fwrite(&endHeader, sizeof(EP_STEP_END_HEADER), 1, fp) != 1)			
			{
				fclose(fp);
				return -8;
			}
			
			for(int ch =0; ch<m_resultFile.GetTotalCh(); ch++)	
			{
				STR_SAVE_CH_DATA* lpChannel = (STR_SAVE_CH_DATA *)pStepResult->aChData[ch];

				if(fwrite(lpChannel, sizeof(STR_SAVE_CH_DATA), 1, fp) != 1)
				{
					fclose(fp);			
					return -8;
				}			
			}
		}
		else
		{
			//////////////////////////////////////////////////////////////////////////
			// 1. GROUP_STATE_SAVE
			//////////////////////////////////////////////////////////////////////////

			ZeroMemory(&StepResult.gpStateSave, sizeof(GROUP_STATE_SAVE));
			/*
			for(int s=0; s<EP_MAX_SENSOR_CH; s++)
			{
				StepResult.gpStateSave.sensorChData1[s].fData = ETC_PRECISION(gpData.sensorData.sensorData1[s].lData);
				pMapData = m_resultFile.GetSensorMap(CFormResultFile::sensorType1, s);
				if(pMapData)
				{
					StepResult.gpStateSave.sensorChData1[s].lReserved = pMapData->nChannelNo;
				}
				memcpy(&StepResult.gpStateSave.sensorChData1[s].minmaxData, &gpData.sensorMinMax.sensorData1[s], sizeof(_MINMAX_DATA));
				
				StepResult.gpStateSave.sensorChData2[s].fData = VTG_PRECISION(gpData.sensorData.sensorData2[s].lData);
				pMapData = m_resultFile.GetSensorMap(CFormResultFile::sensorType2, s);
				if(pMapData)
				{
					StepResult.gpStateSave.sensorChData2[s].lReserved = pMapData->nChannelNo;
				}
				memcpy(&StepResult.gpStateSave.sensorChData2[s].minmaxData, &gpData.sensorMinMax.sensorData2[s], sizeof(_MINMAX_DATA));
			}
			*/			
			if(fwrite(&StepResult.gpStateSave, sizeof(GROUP_STATE_SAVE), 1, fp) < 1)
			{
				fclose(fp);			
				return -8;
			}		

			//////////////////////////////////////////////////////////////////////////
			// 2. RESULT_STEP_HEADER
			//////////////////////////////////////////////////////////////////////////
			
			COleDateTime curTime = COleDateTime::GetCurrentTime();		//완료 시각
			
			StepResult.stepHead.nModuleID = m_resultFile.GetModuleID();
			StepResult.stepHead.wGroupIndex = nGroupNo-1;
			StepResult.stepHead.wJigIndex = 0;							
			sprintf(StepResult.stepHead.szEndDateTime, "%s", curTime.Format("%Y-%m-%d %H:%M:%S"));
			// sprintf(StepResult.stepHead.szModuleIP,	"%s", pModule->GetIPAddress());
			// sprintf(StepResult.stepHead.szOperatorID,	"%s", g_LoginData.szLoginID);	
			curTime = COleDateTime::GetCurrentTime();		// pModule->GetStepStartTime();
			sprintf(StepResult.stepHead.szStartDateTime, "%s", curTime.Format("%Y-%m-%d %H:%M:%S"));
			sprintf(StepResult.stepHead.szTestSerialNo, "%s", m_resultFile.GetTestSerialNo());
			sprintf(StepResult.stepHead.szTrayNo,		"%s", m_resultFile.GetTrayNo());
			sprintf(StepResult.stepHead.szTraySerialNo, "%s", m_resultFile.GetTrayNo());
			
			if(fwrite(&StepResult.stepHead, sizeof(RESULT_STEP_HEADER), 1, fp) < 1)
			{
				fclose(fp);			
				return -8;
			}
			
			//////////////////////////////////////////////////////////////////////////
			// 3. STR_COMMON_STEP - 현재 Step의  조건을 기록 한다.
			//////////////////////////////////////////////////////////////////////////
			
			ZeroMemory(&StepResult.stepCondition, sizeof(STR_COMMON_STEP));
			CTestCondition *pCondition = m_resultFile.GetTestCondition();
			CStep *pStep = pCondition->GetStep(step);
			if(pStep) 
			{
				StepResult.stepCondition = pStep->GetStepData();
			}
			
			if(fwrite(&StepResult.stepCondition, sizeof(STR_COMMON_STEP), 1, fp) < 1)
			{
				fclose(fp);			
				return -8;
			}		
			
			//////////////////////////////////////////////////////////////////////////
			// 4. EP_STEP_END_HEADER - 전체널 Step 결과에 대한 종합값 사용 안함 2006/2/16 (의미 없는 data)
			//////////////////////////////////////////////////////////////////////////
			
			EP_STEP_END_HEADER endHaeder;
			endHaeder.wStep = StepResult.stepCondition.stepHeader.stepIndex+1;
			if(fwrite(&endHaeder, sizeof(EP_STEP_END_HEADER), 1, fp) < 1)
			{
				fclose(fp);
				return -8;
			}

					
			//////////////////////////////////////////////////////////////////////////
			// 5. STR_SAVE_CH_DATA - Channel data structure type changed by KBH 2005/12/27
			//////////////////////////////////////////////////////////////////////////
			
			STR_SAVE_CH_DATA CChannerl;	
			for(int ch =0; ch<m_resultFile.GetTotalCh(); ch++)	
			{
				ZeroMemory(&CChannerl, sizeof(STR_SAVE_CH_DATA));
				
				CChannerl = ConvertChSaveData( m_resultFile.GetModuleID(), ch, lpChData[ch].GetStepData(step+1), strCapacityFormula, strDCRFormula );
				
				if(fwrite(&CChannerl, sizeof(STR_SAVE_CH_DATA), 1, fp) < 1)
				{
					fclose(fp);
					return -8;
				}
			}
		}
	}

	fclose(fp);
	return 1;
}

void CResultView::OnRestoreResultdata() 
{
	int nRtn = 1;
	nRtn = Fun_RestoreResultdata();
	
	switch( nRtn )
	{
	case 1:
		{
			AfxMessageBox(TEXT_LANG[71], MB_ICONINFORMATION);			//"데이터 복구가 완료 되었습니다."
			OnResultFileOpen();
		}
		break;
	case -1:
		{
			AfxMessageBox(TEXT_LANG[72], MB_ICONEXCLAMATION);//"데이터 복구를 위한 결과데이터 정보가 없습니다."
		}
		break;
	case -2:
		{
			AfxMessageBox(TEXT_LANG[73], MB_ICONEXCLAMATION);//"결과데이터의 정보가 손상되었습니다."
		}
		break;
	case -3:
		{
			AfxMessageBox(TEXT_LANG[74], MB_ICONEXCLAMATION);//"레지스트리의 DataBase 경로 설정이 잘못되었습니다."
		}
		break;
	case -4:
		{
			AfxMessageBox(TEXT_LANG[75], MB_ICONEXCLAMATION);//"[ Schedule.mdb ]에서 UNIT의 IP주소값 읽어오기를 실패했습니다."
		}
		break;
	case -5:
		{
			AfxMessageBox(TEXT_LANG[76], MB_ICONEXCLAMATION);//"복구용 data를 찾을 수 없습니다"
		}
		break;
	case -6:
		{
			AfxMessageBox(TEXT_LANG[77], MB_ICONEXCLAMATION);//"충전기 내부 SBC 접속을 실패했습니다."
		}
		break;
	case -7:
		{
			AfxMessageBox(TEXT_LANG[78], MB_ICONEXCLAMATION);//"충전기 내부 SBC 에서 채널정보 찾기를 실패했습니다."
		}
		break;
	case -8:
		{
			AfxMessageBox(TEXT_LANG[79], MB_ICONEXCLAMATION);//"결과파일 생성에 실패했습니다. 해당경로에서 *.bak 확장자를 *.fmt 로 변경해주십시오."
		}
		break;
	default:
		{
		}
	}
	
	return;
}

STR_SAVE_CH_DATA CResultView::ConvertChSaveData( int nModuleID, int nChIndex/*Tray Base Index*/, LPEP_CH_DATA pChData, CString strCapacityFormula, CString strDCRFormula )
{
	//1 Tray Index를 구함 
	STR_SAVE_CH_DATA ChSaveData;
	ZeroMemory( &ChSaveData, sizeof(STR_SAVE_CH_DATA));

	//모든 단위를 m단위로 변환하여 저장한다.
    // pChSaveData->wModuleChIndex	= GetStartChIndex(nTrayIndex)+nChIndex;
	ChSaveData.wModuleChIndex	= nChIndex;
	ChSaveData.wChIndex	= nChIndex;
	ChSaveData.state	= pChData->state;
	//사용자 Grading 실시 
	
	CTestCondition *pCon = m_resultFile.GetTestCondition();	

	CStep *pStep = pCon->GetStep(pChData->nStepNo-1);
	if(pStep && pStep->m_Grading.GetGradeStepSize() > 0)
	{
		//Grading
		ChSaveData.grade = GetUserSelect(pChData->channelCode, pChData->grade);
	}
	else
	{
		ChSaveData.grade = pChData->grade;
	}
	
	ChSaveData.channelCode = pChData->channelCode;
	ChSaveData.nStepNo	= pChData->nStepNo;
    ChSaveData.fStepTime =  MD2PCTIME(pChData->ulStepTime);				//sec 단위로 변환  
    ChSaveData.fTotalTime = MD2PCTIME(pChData->ulTotalTime);			//sec 단위로 변환
    ChSaveData.fVoltage =	 VTG_PRECISION(pChData->lVoltage);
    ChSaveData.fCurrent = CRT_PRECISION(pChData->lCurrent);
    ChSaveData.fWatt	= ETC_PRECISION(pChData->lWatt);
    ChSaveData.fWattHour = ETC_PRECISION(pChData->lWattHour);
    ChSaveData.fCapacity = ETC_PRECISION(pChData->lCapacity);
    ChSaveData.fImpedance = ETC_PRECISION(pChData->lImpedance);	
    
	ChSaveData.fCcCapacity = CRT_PRECISION(pChData->lCcCapacity);
	ChSaveData.fCvCapacity = CRT_PRECISION(pChData->lCvCapacity);
	ChSaveData.fCcRunTime = MD2PCTIME(pChData->ulCcRunTime);
	ChSaveData.fCvRunTime = MD2PCTIME(pChData->ulCvRunTime);
	ChSaveData.fDeltaOCV = VTG_PRECISION(pChData->lDeltaOCV);  //KSH 20191107
	ChSaveData.fStartVoltage = VTG_PRECISION(pChData->lStartVoltage);  //KSH 20191107
	ChSaveData.fDeltaCapacity = ETC_PRECISION(pChData->lDeltaCapacity);  //KSH 20191107

	ChSaveData.fDCIR_V1 = VTG_PRECISION(pChData->lDCIR_V1);
	ChSaveData.fDCIR_V2 = VTG_PRECISION(pChData->lDCIR_V2);
	ChSaveData.fDCIR_AvgCurrent = CRT_PRECISION(pChData->lDCIR_AvgCurrent);
	ChSaveData.fDCIR_AvgTemp = ETC_PRECISION(pChData->lDCIR_AvgTemp);
	ChSaveData.fTimeGetChargeVoltage = VTG_PRECISION(pChData->lTimeGetChargeVoltage);
	
	ChSaveData.fComDCIR = ETC_PRECISION(pChData->lComDCIR);
	ChSaveData.fComCapacity = ETC_PRECISION(pChData->lComCapacity);
	ChSaveData.currentFaultLevel1Cnt = pChData->currentFaultLevel1Cnt;
	ChSaveData.currentFaultLevel2Cnt = pChData->currentFaultLevel2Cnt;
	ChSaveData.currentFaultLevel3Cnt = pChData->currentFaultLevel3Cnt;
	ChSaveData.voltageFaultLevelV1Cnt = pChData->voltageFaultLevelV1Cnt;
	ChSaveData.voltageFaultLevelV2Cnt = pChData->voltageFaultLevelV2Cnt;
	ChSaveData.voltageFaultLevelV3Cnt = pChData->voltageFaultLevelV3Cnt;
	ChSaveData.ulTotalSamplingVoltageCnt = pChData->ulTotalSamplingVoltageCnt;	
	ChSaveData.ulTotalSamplingCurrentCnt = pChData->ulTotalSamplingCurrentCnt;

	int i=0;
	for(i=0; i<6; i++ )
	{
		ChSaveData.fJigTemp[i] = ETC_PRECISION(pChData->ulJigTemp[i]);
	}

	ChSaveData.fTempMax = ETC_PRECISION(pChData->ulTempMax);
	ChSaveData.fTempMin = ETC_PRECISION(pChData->ulTempMin);
	ChSaveData.fTempAvg = ETC_PRECISION(pChData->ulTempAvg);  

	ChSaveData.fAuxData[0] = ETC_PRECISION(pChData->ulTempAvg);

	if( ChSaveData.state == EP_STATE_CHARGE || ChSaveData.state == EP_STATE_DISCHARGE || ChSaveData.state == EP_STATE_IMPEDANCE )
	{
		if( ChSaveData.fTempAvg < 0.0f || ChSaveData.fTempAvg > 120.0f
			|| strCapacityFormula.IsEmpty() || strDCRFormula.IsEmpty() )
		{
			return ChSaveData;
		}

		double dResult;
		CString strValue;
		CString strTemp;

		CString strCCCapacityT;
		CString strCVCapacityT;
		CString strCapacityT;
		CString strDCRT;

		strCapacityT = strCapacityFormula;		

		strValue.Format("%.1f", ChSaveData.fTempAvg);
		strCapacityT = GStr::str_Replace(strCapacityT, strValue, 'T' );

		strCCCapacityT = strCapacityT;
		strCVCapacityT = strCapacityT;

		// 1. 용량
		strValue.Format("%.4f", ChSaveData.fCapacity/1000.0f);
		strCapacityT = GStr::str_Replace(strCapacityT, strValue, 'C' );	

		char *postfix;
		postfix = GMath::infix_to_postfix((LPSTR)(LPCTSTR)strCapacityT);
		dResult = GMath::evalPostfix(postfix);
		delete postfix;

		strTemp.Format("%f", dResult);
		ChSaveData.fComCapacity = dResult*1000.0f;

		// 2. CC용량
		strValue.Format("%.4f", ChSaveData.fCcCapacity/1000.0f);
		strCCCapacityT = GStr::str_Replace(strCCCapacityT, strValue, 'C' );	

		postfix = GMath::infix_to_postfix((LPSTR)(LPCTSTR)strCCCapacityT);
		dResult = GMath::evalPostfix(postfix);
		delete postfix;

		strTemp.Format("%f", dResult);		
		ChSaveData.fAuxData[1] = dResult*1000.0f;

		// 3. CV용량
		strValue.Format("%.4f", ChSaveData.fCvCapacity/1000.0f);
		strCVCapacityT = GStr::str_Replace(strCVCapacityT, strValue, 'C' );	

		postfix = GMath::infix_to_postfix((LPSTR)(LPCTSTR)strCVCapacityT);
		dResult = GMath::evalPostfix(postfix);
		delete postfix;

		strTemp.Format("%f", dResult);
		ChSaveData.fAuxData[2] = dResult*1000.0f;

		// 4. DCIR
		strDCRT = strDCRFormula;		
		strValue.Format("%.1f", ChSaveData.fTempAvg);
		strDCRT = GStr::str_Replace(strDCRT, strValue, 'T' );

		strValue.Format("%.2f", ChSaveData.fImpedance);
		strDCRT = GStr::str_Replace(strDCRT, strValue, 'D' );	

		postfix = GMath::infix_to_postfix((LPSTR)(LPCTSTR)strDCRT);
		dResult = GMath::evalPostfix(postfix);
		delete postfix;

		strTemp.Format("%f", dResult);
		ChSaveData.fComDCIR = dResult;
	}

	return ChSaveData;	
}

BYTE CResultView::GetUserSelect(int nChCode, int nGradeCode)
{
	BYTE code = 0;
	//사용자 Grading 실시 
	if(::IsNonCell(nChCode))
	{
		code = EP_SELECT_CODE_NONCELL;
	}
	else if(IsCellCheckFail(nChCode))
	{
		//code = EP_SELECT_CODE_CELL_CHECK_FAIL;
		code = EP_SELECT_CODE_NONCELL;
	}
	else if(::IsFailCell(nChCode))
	{
		if(nChCode == EP_CODE_END_USER_STOP)
		{
			code = EP_SELECT_DEFAULT_NORMAL_CODE;
		}
		else if(nChCode == EP_CODE_FAIL_IMPEDANCE_HIGH || nChCode == EP_CODE_FAIL_IMPEDANCE_LOW)
		{
			code = EP_SELECT_CODE_IMPEDANCE;
		}
		else if(nChCode == EP_CODE_FAIL_CAP_HIGH || nChCode == EP_CODE_FAIL_CAP_LOW)
		{
			code = EP_SELECT_CODE_CAP_FAIL;
		}
		else if(nChCode == EP_CODE_FAIL_CRT_HIGH || nChCode == EP_CODE_FAIL_CRT_LOW)// || nChCode == EP_CODE_FAIL_I_HUNTING) =>system code 영역 
		{
			code = EP_SELECT_CODE_CRT_FAIL;
		}
		else if(nChCode == EP_CODE_FAIL_VTG_HIGH || nChCode == EP_CODE_FAIL_VTG_LOW)// || nChCode == EP_CODE_FAIL_V_HUNTING) =>  =>system code 영역 
		{
			code = EP_SELECT_CODE_VTG_FAIL;
		}
		else
		{
			code = EP_SELECT_CODE_SAFETY_FAULT;
		}
	}
	else if(::IsNormalCell(nChCode))
	{
		if(nGradeCode == 0)
		{
			//정상 Cell인데 code를 부여 받지 못한 경우 
			code = EP_SELECT_CODE_NO_GRADE;
		}
		else
		{
			code = nGradeCode;
		}
	}
	else //if()
	{
		if(nChCode ==  EP_CODE_FAIL_V_HUNTING)
		{
			code = EP_SELECT_CODE_VTG_FAIL;
		}
		else if(nChCode == EP_CODE_FAIL_I_HUNTING)
		{
			code = EP_SELECT_CODE_CRT_FAIL;
		}
		else
		{
			code = EP_SELECT_CODE_SYS_ERROR;
		}
	}
	return code;
}

CString CResultView::GetRemoteLocation(int nGroupNo, CString strTestSerial, CString strIp)
{
	CString strTemp;

	if(strIp.IsEmpty() || strTestSerial.IsEmpty())	
	{
		strTemp.Format("Parameter error!!!(%s, %s)", strIp, strTestSerial);
		AfxMessageBox(strTemp);
		return "";
	}
	
	CString strRemoteLocation;

	CFtpDownLoad *pDownLoad = new CFtpDownLoad;
	BOOL bConnect = pDownLoad->ConnectFtpSession(strIp);
	if(bConnect == FALSE)
	{
		strTemp.Format(TEXT_LANG[80], strIp);//"원격 위치 %s에 접속할 수 없습니다."
		AfxMessageBox(strTemp);
		delete pDownLoad;
		return "";
	}

	CString strPathName = "formation_data/resultData";

	char szBuff[_MAX_PATH];
	::GetTempPath(_MAX_PATH, szBuff);		//Get Windows temp. folder
	CString strRemoteFileName, strLocalFileName;

	//1. 목록 Index 파일을 Download한 후 주어진 test serialno의 폴더를 찾는다.
	strLocalFileName.Format("%s\\downtemp.csv", szBuff);
	strRemoteFileName.Format("%s/group%d/savingFileIndex_start.csv", strPathName, nGroupNo);

 	if(pDownLoad->DownLoadFile(strLocalFileName, strRemoteFileName) < 1)
	{
		delete pDownLoad;
		strTemp.Format(TEXT_LANG[81], strIp);//"원격 위치 %s에서 Index 파일을 찾을 수 없습니다."
		AfxMessageBox(strTemp);
		return "";
	}
	delete pDownLoad;
	pDownLoad = NULL;
	
	//Download list file open
	CStdioFile sourceFile;			
	CFileException ex;	
	CString saveFilePath;
	CString strReadData;
	CString strData;
	CString strIndex;
	BOOL bFind = FALSE;
	int nIndex = 0;
	int nSize = -1;

	if( !sourceFile.Open( strLocalFileName, CFile::modeRead, &ex ))
	{
		CString strMsg;
		ex.GetErrorMessage((LPSTR)(LPCSTR)strMsg,1024);
		AfxMessageBox(strMsg);	
		return "";
	}

	// 저장 Format : 
	// mointorIndex 1, open_year 6, open_month 2, open_day 2, test_serial,
	while(TRUE)	
	{			
		sourceFile.ReadString(strReadData);    // 한 문자열씩 읽는다.	
		nSize = strReadData.Find(',');
		
		if( nSize > 0 )
		{
			AfxExtractSubString(strTemp, strReadData, 3,',');
			AfxExtractSubString(strData, strTemp, 2,' ');
			if( strData  == strTestSerial )
			{
				bFind = TRUE;
				break;
			}			
		}			
		else
		{
			break;				
		}
	}			  
	sourceFile.Close();
	_unlink(strLocalFileName);

	if(bFind == FALSE)
	{
		strTemp.Format(TEXT_LANG[82], strTestSerial);//"Index 파일에서 작업 번호 %s 정보를 찾을 수 없습니다."
		AfxMessageBox(strTemp);
		return "";
	}

	AfxExtractSubString(strTemp, strReadData, 0,',');
	AfxExtractSubString(strData, strTemp, 1,' ');
	strRemoteLocation.Format("%s/group%d/data%d", strPathName, nGroupNo, atoi(strData));

	return strRemoteLocation;	
}
void CResultView::OnBnClickedViewSelect1Btn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( m_DeviceMode != DEVICE_FORMATION )
	{
		m_DeviceMode = DEVICE_FORMATION;
		SendMessage(WM_SIZE, (WPARAM)SIZE_RESTORED, NULL); 								
	}
}

void CResultView::OnBnClickedViewSelect2Btn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( m_DeviceMode != DEVICE_DCIR )
	{
		m_DeviceMode = DEVICE_DCIR;
		SendMessage(WM_SIZE, (WPARAM)SIZE_RESTORED, NULL); 								
	}
}