// Step.h: interface for the CStep class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_STEP_H__B1C39ECF_2E5D_4089_85F8_D596E36E94A0__INCLUDED_)
#define AFX_STEP_H__B1C39ECF_2E5D_4089_85F8_D596E36E94A0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Grading.h"

#define MAX_STEP_COMP_SIZE	3

class AFX_EXT_CLASS CStep  
{
public:
	FILE_STEP_PARAM		GetFileStep();
	void	SetStepData(FILE_STEP_PARAM *pStepData);
	
	BOOL ClearStepData();
	CStep(int nStepType /*=0*/);
	CStep();
	virtual ~CStep();
	void operator = (CStep &step); 

	//DB PK
	long	m_StepID;

	/*Step Header */
	BYTE	m_StepIndex;	//StepNo
	long	m_lProcType;				
	BYTE	m_type;			//Step Type
	BYTE	m_mode;
	ULONG	m_lRange;			
	float	m_fVref;
    float	m_fIref;
	float	m_fEndT;		//종료온도
	float	m_fStartT;		//시작온도
	float	m_fTref;		//Referance Temperature
	float	m_fHref;		//Referance humidity
	float	m_fTrate;		//온도변화율
	float	m_fHrate;		//습도변화율
    
	/* Step End Value */
	float	m_fEndTime;
    float	m_fEndV;
    float	m_fEndI;
    float	m_fEndC;
    float	m_fEndDV;
    float	m_fEndDI;
	float	m_fEndW;
	float	m_fEndWh;
	float	m_fCVTime;
	//float	m_fCVTime;	//20080119 Only LSCable kjh

	/* Soc End Condition */
	BYTE	m_bUseActualCapa;					//현재 Step의 용량값을 기준 용량으로 사용 할지 여부 
	BYTE	m_UseAutucalCapaStepNo;				//비교할 기준 용량이 속한 Step번호
	float	m_fSocRate;							//비교 SOC Rate


	/*Step Fail Value */
	float	m_fHighLimitV;
    float	m_fLowLimitV;
    float	m_fHighLimitI;
    float	m_fLowLimitI;
    float	m_fHighLimitC;
    float	m_fLowLimitC;
	float 	m_fHighLimitImp;
    float	m_fLowLimitImp;
	float	m_fHighLimitTemp;
	float	m_fLowLimitTemp;
	float	m_fHighLimitA;
	float	m_fLowLimitA;
	
	/*Grading*/
	BYTE	m_bGrade;
	CGrading	m_Grading;
	CGrading	m_AdGrading;		//20081007 kjh Ah + ESR or F + ESR

	//Cycle Check
	long	m_nAccLoopInfoCycle;
	long	m_nAccLoopGroupID;
	long	m_nLoopInfoGoto;
	long	m_nLoopInfoGotoCnt;
	long	m_nLoopInfoEndTGoto;
	long	m_nLoopInfoEndVGoto;
	long	m_nLoopInfoEndIGoto;
	long	m_nLoopInfoEndCGoto;
	long	m_nLoopInfoCycle;	
	long	m_nGotoStepID;		//EndV, EndT, EndC용 Step ID -> 이동스텝은 Step ID를 참조한다.

	//V/I Ramp check
	float	m_fCompTimeV[MAX_STEP_COMP_SIZE];			
	float	m_fCompHighV[MAX_STEP_COMP_SIZE];
 	float	m_fCompLowV[MAX_STEP_COMP_SIZE];
	float	m_fCompTimeI[MAX_STEP_COMP_SIZE];			
	float	m_fCompHighI[MAX_STEP_COMP_SIZE];
	float	m_fCompLowI[MAX_STEP_COMP_SIZE];

	//Delta Check
	float	m_fDeltaTimeV;
    float	m_fDeltaV;
    float	m_fDeltaTimeI;
    float	m_fDeltaI;

	//Recording 
	float	m_fReportTemp;
	float	m_fReportV;
	float	m_fReportI;
	float	m_fReportTime;
	float	m_fReportVIGetTime;

	//EDLC
	float	m_fCapaVoltage1;		
	float	m_fCapaVoltage2;	
	float	m_fDCRStartTime;		
	float	m_fDCREndTime;
	float	m_fLCStartTime;		
	float	m_fLCEndTime;	
	
	//User Pattern
	CString	m_strPatternFileName;
	float	m_fValueMax;		//Pattern data 중 최대값
	float	m_fValueMin;		//Pattern data 중 최소값 
};

#endif // !defined(AFX_STEP_H__B1C39ECF_2E5D_4089_85F8_D596E36E94A0__INCLUDED_)
