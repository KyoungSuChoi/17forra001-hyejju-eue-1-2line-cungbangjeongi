#if !defined(AFX_GRAPHWND_H__7EA2A462_3364_11D2_80A6_0020741517CE__INCLUDED_)
#define AFX_GRAPHWND_H__7EA2A462_3364_11D2_80A6_0020741517CE__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// GraphWnd.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CGraphWnd window
#include "PEGRPAPI.h"
#include "pemessag.h"

class CGraphWnd : public CStatic
{
// Construction
public:
	CGraphWnd();
	virtual ~CGraphWnd();

// Attributes
public:
	CString *TEXT_LANG;//TEXT_LANG[@]
	bool LanguageinitMonConfig();

	HWND	m_hWndPE;
	int		m_SetsetNum;
	int		m_TotalPoint;
	int		m_ScreenPoint;
	int		m_Delay;
	COLORREF	m_YColor[8];
	CString		m_MainTitle;
	CString		m_SubTitle;
	CString		m_SubsetName[6];
	CString		m_SubsetYName[6];
	double		m_Min[6], m_Max[6];
	unsigned long *		m_TimerPtr;
	unsigned long	m_OldTime;

	int			m_TimerID;
	float *		m_pData[6];
	BOOL		m_bShowArray[8];

	time_t		m_StartTime;
	int			m_Count;
	CString		m_XName;

// Operations
public:
	void SetN(UINT type, INT value);
	void SetV(UINT type, VOID FAR * lpData, UINT nItems);
	void SetSZ(UINT type, CHAR FAR *str);
	void SetVCell(UINT type, UINT nCell, VOID FAR * lpData);

	void InitGraph();
	void InitSubset(int subID, double min, double max, LPCTSTR title, LPCTSTR yname);
	void ShowSubset(int id, BOOL bShow);
	void SetSplit(BOOL bSplit);
	void Start();
	void Stop();
	void SetDataPtr(int id, float *data);
	void ClearGraph();
	void Print();
	void SetData(int subset, int pos, LPCTSTR xData, double yData);
	void SetMaximize();

	virtual BOOL OnCommand( WPARAM wParam, LPARAM lParam);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGraphWnd)
	protected:
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

// Implementation
public:
	

	// Generated message map functions
protected:
	//{{AFX_MSG(CGraphWnd)
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnDestroy();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRAPHWND_H__7EA2A462_3364_11D2_80A6_0020741517CE__INCLUDED_)
