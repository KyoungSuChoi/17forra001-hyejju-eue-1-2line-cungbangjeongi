#if !defined(AFX_DETAILTESTCONDITIONDLG_H__EE792208_31C5_11D2_80A6_0020741517CE__INCLUDED_)
#define AFX_DETAILTESTCONDITIONDLG_H__EE792208_31C5_11D2_80A6_0020741517CE__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// DetailTestConditionDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDetailTestConditionDlg dialog
//class CPowerBTSDoc;

#include "MyGridWnd.h"
#include "CTSAnalDoc.h"

class CDetailTestConditionDlg : public CDialog
{
// Construction
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	void DisplayCondition();
	CDetailTestConditionDlg(CTestCondition *pTestCondition, CWnd* pParent = NULL);   // standard constructor
	virtual ~CDetailTestConditionDlg();

// Dialog Data
	//{{AFX_DATA(CDetailTestConditionDlg)
	enum { IDD = IDD_DETAIL_TESTCONDITION_DLG };
	CXPButton	m_btnExcel;
	CXPButton	m_btnOK;
	CXPButton	m_btnPrint;
	CLabel	m_Static1;
	//}}AFX_DATA

	CMyGridWnd m_SafetyGrid;
	CMyGridWnd m_StepGrid;
	CMyGridWnd m_FailGrid;
	CMyGridWnd m_GradeGrid;

	CTestCondition *m_pTestCondition;
//	STR_CONDITION *m_Condition;

//	int m_StepRow;
	CCTSAnalDoc *m_pDoc;
	void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDetailTestConditionDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
//	void InitGrid(CMyGridWnd *pGrid, int id, int rowNum, int colNum, CStringArray *aTitle);
	void InitSafetyGrid();
	void InitStepGrid();
	void InitFailGrid();
	void InitGradeGrid();

	void DiaplayStepGrid();
	void DisplaySafetyGrid();
	void DisplayFailGrid(int nStepIndex = 0);
	void DisplayGradeGrid(int nStepIndex = 0);

protected:
	void PrintText(CString text, int nLine, CDC *pDC);

	// Generated message map functions
	//{{AFX_MSG(CDetailTestConditionDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnExcelSaveButton();
	afx_msg void OnPrintButton();
	//}}AFX_MSG
//	afx_msg LONG OnGridDoubleClick(WPARAM wParam, LPARAM lParam);
	afx_msg LONG OnMovedCurrentCell(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnEnChangeDetailDate();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DETAILTESTCONDITIONDLG_H__EE792208_31C5_11D2_80A6_0020741517CE__INCLUDED_)
