#if !defined(AFX_ROUTINGRECORDSET_H__5FE20187_CB45_4893_98B0_2925B600106A__INCLUDED_)
#define AFX_ROUTINGRECORDSET_H__5FE20187_CB45_4893_98B0_2925B600106A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RoutingRecordSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CRoutingRecordSet DAO recordset

class CRoutingRecordSet : public CDaoRecordset
{
public:
	CRoutingRecordSet(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CRoutingRecordSet)

// Field/Param Data
	//{{AFX_FIELD(CRoutingRecordSet, CDaoRecordset)
	long	m_RoutingID;
	long	m_StepID;
	long	m_ProcID;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRoutingRecordSet)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ROUTINGRECORDSET_H__5FE20187_CB45_4893_98B0_2925B600106A__INCLUDED_)
