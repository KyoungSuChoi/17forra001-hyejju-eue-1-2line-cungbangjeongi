#if !defined(AFX_SHOWDETAILRESULTDLG_H__4B261863_9EBC_4484_9844_49E8BE071C94__INCLUDED_)
#define AFX_SHOWDETAILRESULTDLG_H__4B261863_9EBC_4484_9844_49E8BE071C94__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ShowDetailResultDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CShowDetailResultDlg dialog

class CShowDetailResultDlg : public CDialog
{
// Construction
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	CShowDetailResultDlg(CFormResultFile * pFile, CWnd* pParent = NULL);   // standard constructor
	virtual ~CShowDetailResultDlg();

	CFormResultFile * m_pResultFile;
	

// Dialog Data
	//{{AFX_DATA(CShowDetailResultDlg)
	enum { IDD = IDD_SYS_PARAM_DLG };
	CTreeCtrl	m_fileTree;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CShowDetailResultDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CShowDetailResultDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SHOWDETAILRESULTDLG_H__4B261863_9EBC_4484_9844_49E8BE071C94__INCLUDED_)
