#include "stdafx.h"

#include "CTSMon.h"

#include "FMS.h"
#include "FM_Unit.h"

#include "FM_STATE.h"

CFM_ST_MA_MAINT::CFM_ST_MA_MAINT(FM_STATUS_ID _Eqstid, FM_STATUS_ID _stid, CFM_Unit* _unit)
:CFM_ST_MAINT(_Eqstid, _stid, _unit)
{
}


CFM_ST_MA_MAINT::~CFM_ST_MA_MAINT(void)
{
}

VOID CFM_ST_MA_MAINT::fnEnter()
{
	//TRACE("%s \n", g_strSBCState[m_Unit->fnGetModule()->GetState()]);
	switch(m_Unit->fnGetModule()->GetState())
	{
	case EP_STATE_IDLE:
		if(m_Unit->fnGetModuleTrayState())
		{
			fnSetFMSStateCode(FMS_ST_TRAY_IN);
		}
		else
		{
			fnSetFMSStateCode(FMS_ST_VACANCY);
		}
		break;
	case EP_STATE_STANDBY:
		{
			fnSetFMSStateCode(FMS_ST_TRAY_IN);
		}
		break;
	case EP_STATE_PAUSE:
		{
			fnSetFMSStateCode(FMS_ST_ERROR);
		}
		break;
	case EP_STATE_RUN:
		{
			if(m_Unit->fnGetModuleCheckState())
			{
				fnSetFMSStateCode(FMS_ST_CONTACT_CHECK);
			}
			else
			{
				fnSetFMSStateCode(FMS_ST_RUNNING);
			}
		}
		break;
	case EP_STATE_READY:
		{
			fnSetFMSStateCode(FMS_ST_READY);
		}
		break;
	case EP_STATE_END:
		{
			fnSetFMSStateCode(FMS_ST_END);
		}
		break;
	case EP_STATE_ERROR:
		{
			fnSetFMSStateCode(FMS_ST_ERROR);
		}
		break;
	}

	if(m_Unit->fnGetFMS()->fnGetError() == FMS_ST_ERROR)
	{
		CHANGE_STATE(LOCAL_ST_ERROR);
	}
}

VOID CFM_ST_MA_MAINT::fnProc()
{
}

VOID CFM_ST_MA_MAINT::fnExit()
{
}

VOID CFM_ST_MA_MAINT::fnSBCPorcess(WORD _state)
{
	CFM_ST_MAINT::fnSBCPorcess(_state);
}

FMS_ERRORCODE CFM_ST_MA_MAINT::fnFMSPorcess(FMS_COMMAND _msgId, st_FMS_PACKET* _recvData)
{
	FMS_ERRORCODE _error = ER_NO_Process;

	switch(_msgId)
	{
	default:
		{
		}
		break;
	}

	return _error;

}