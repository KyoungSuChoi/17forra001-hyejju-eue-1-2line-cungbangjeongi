// ContactCheckResultDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "ContactCheckResultDlg.h"

// ContactCheckResultDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(ContactCheckResultDlg, CDialog)

ContactCheckResultDlg::ContactCheckResultDlg(CCTSMonDoc *pDoc, CWnd* pParent /*=NULL*/ )
	: CDialog(ContactCheckResultDlg::IDD, pParent)
	, m_strResultMsg(_T(""))
{
	m_pDoc = pDoc;
	m_nRnt = 0;
	LanguageinitMonConfig();
}

ContactCheckResultDlg::~ContactCheckResultDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

bool ContactCheckResultDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_ContactCheckResultDlg"), _T("TEXT_ContactCheckResultDlg_CNT"), _T("TEXT_ContactCheckResultDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt];

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_ContactCheckResultDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_ContactCheckResultDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

void ContactCheckResultDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_STATIC_INFO, m_strResultMsg);
	DDX_Control(pDX, IDC_LABEL1, m_Label1);
}

BEGIN_MESSAGE_MAP(ContactCheckResultDlg, CDialog)
	ON_BN_CLICKED(IDC_RESTART_BTN, &ContactCheckResultDlg::OnBnClickedRestartBtn)
	ON_BN_CLICKED(IDOK, &ContactCheckResultDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_CONTINUE_BTN, &ContactCheckResultDlg::OnBnClickedContinueBtn)
	ON_BN_CLICKED(IDC_STOP_BTN, &ContactCheckResultDlg::OnBnClickedStopBtn)
	ON_BN_CLICKED(IDCANCEL, &ContactCheckResultDlg::OnBnClickedCancel)
END_MESSAGE_MAP()

// ContactCheckResultDlg 메시지 처리기입니다.

void ContactCheckResultDlg::OnBnClickedRestartBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.	
	if( EPJigState(m_nModuleID) != EP_JIG_UP )
	{
		m_Label1.SetText(TEXT_LANG[1]);		
		return;	
	}

	CString strTemp;
	if(m_pDoc->SendReStartCommand(m_nModuleID, m_nGroupIndex) == FALSE)
	{
		strTemp.Format("%s [%s] %s",  ::GetModuleName(m_nModuleID), ::GetStringTable(IDS_TEXT_CONTINE_WORK), ::GetStringTable(IDS_TEXT_CMD_SEND_FAIL));
		m_Label1.SetText(strTemp);		
	}
	else
	{
		OnOK();	
	}
}

void ContactCheckResultDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.	
	OnOK();
}

void ContactCheckResultDlg::OnBnClickedContinueBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( EPJigState(m_nModuleID) != EP_JIG_UP )
	{
		m_Label1.SetText(TEXT_LANG[1]);
		return;	
	}
	
	CString strTemp;
	if(m_pDoc->SendContinueCommand(m_nModuleID, m_nGroupIndex) == FALSE)
	{
		strTemp.Format("%s [%s] %s",  ::GetModuleName(m_nModuleID), ::GetStringTable(IDS_TEXT_CONTINE_WORK), ::GetStringTable(IDS_TEXT_CMD_SEND_FAIL));
		m_Label1.SetText(strTemp);
	}
	else
	{
		OnOK();
	}
}

void ContactCheckResultDlg::OnBnClickedStopBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( EPJigState(m_nModuleID) != EP_JIG_UP )
	{
		m_Label1.SetText(TEXT_LANG[1]);		
		return;	
	}
	
	CString strTemp;
	if(m_pDoc->SendStopCmd(m_nModuleID, m_nGroupIndex) == FALSE)
	{
		strTemp.Format("%s [%s] %s",  ::GetModuleName(m_nModuleID), ::GetStringTable(IDS_TEXT_STOP), ::GetStringTable(IDS_TEXT_CMD_SEND_FAIL));
		m_Label1.SetText(strTemp);		
	}
	else
	{
		OnOK();	
	}
}

void ContactCheckResultDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	// 1. Buzzer Off 전송 후 창을 닫는다.	
	EPSendCommand(m_nModuleID, 0, 0, EP_CMD_BUZZER_OFF);

	OnCancel();
}

void ContactCheckResultDlg::InitFont()
{
	LOGFONT	LogFont;

	GetDlgItem(IDC_STATIC_INFO)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight = 1000;
	LogFont.lfHeight = EDIT_FONT_SIZE;

	font.CreateFontIndirect( &LogFont );

	GetDlgItem(IDC_STATIC_INFO)->SetFont(&font);
}

void ContactCheckResultDlg::InitLabel()
{
	m_Label1.SetBkColor(RGB_MIDNIGHTBLUE)
		.SetTextColor(RGB_WHITE)
		.SetFontSize(18)
		.SetFontBold(TRUE);
}

void ContactCheckResultDlg::SetModuleID(int nModuleID)
{
	m_nModuleID = nModuleID;
}

int ContactCheckResultDlg::GetModuleID()
{
	return m_nModuleID;
}

BOOL ContactCheckResultDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	UpdateData(false);
	
	InitFont();	
	InitLabel();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void ContactCheckResultDlg::SetResultMsg( CString strMsg )
{
	CenterWindow();
	m_Label1.SetText("-");
	m_strResultMsg = strMsg;
	
	UpdateData(false);
}