#if !defined(AFX_UNITCOMLOGDLG_H__8A983040_E552_448F_A48F_E1976BE61C83__INCLUDED_)
#define AFX_UNITCOMLOGDLG_H__8A983040_E552_448F_A48F_E1976BE61C83__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UnitComLogDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CUnitComLogDlg dialog

class CUnitComLogDlg : public CDialog
{
// Construction
public:
	CString m_strIpAddress;
	CUnitComLogDlg(CString strIpAddress, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CUnitComLogDlg)
	enum { IDD = IDD_SEL_UNIT_LOG_DIALOG };
	CListCtrl	m_wndSelList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUnitComLogDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CUnitComLogDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDblclkSelList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnButtonSave();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCancel();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UNITCOMLOGDLG_H__8A983040_E552_448F_A48F_E1976BE61C83__INCLUDED_)
