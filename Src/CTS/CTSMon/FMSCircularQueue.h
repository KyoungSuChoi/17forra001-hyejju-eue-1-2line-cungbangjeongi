#pragma once

#define MAX_QUEUE_LENGTH	1000

template <class T>
class CFMSCircularQueue : public CFMSSyncParent<CFMSCircularQueue<T>>
{
public:
	CFMSCircularQueue(VOID)
	{
		ZeroMemory(m_atDatas, sizeof(m_atDatas));
		m_dwHead = m_dwTail = 0;
		count = 0;
	}
	~CFMSCircularQueue(VOID){}

	UINT count;

private:
	T		m_atDatas[MAX_QUEUE_LENGTH];
	DWORD	m_dwHead;
	DWORD	m_dwTail;

public:
	BOOL Push(T tInputData)
	{
		CFMSSyncObj FMSSync;

		DWORD dwTempTail = (m_dwTail + 1) % MAX_QUEUE_LENGTH;

		if (dwTempTail == m_dwHead)
			return FALSE;

		CopyMemory(&m_atDatas[dwTempTail], &tInputData, sizeof(T));
		m_dwTail = dwTempTail;
		count++;
		return TRUE;
	}

	BOOL Pop(T& rtOutputData)
	{
		CFMSSyncObj FMSSync;

		if (m_dwHead == m_dwTail)
			return FALSE;

		DWORD dwTempHead = (m_dwHead + 1) % MAX_QUEUE_LENGTH;
		CopyMemory(&rtOutputData, &m_atDatas[dwTempHead], sizeof(T));
		m_dwHead = dwTempHead;
		count--;
		return TRUE;
	}

	BOOL GetIsEmpty(VOID)
	{
		CFMSSyncObj FMSSync;

		if (m_dwHead == m_dwTail)
			return TRUE;

		return FALSE;
	}
};