// ShowDetailResultDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Datadown.h"
#include "ShowDetailResultDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CShowDetailResultDlg dialog


CShowDetailResultDlg::CShowDetailResultDlg(CFormResultFile * pFile, CWnd* pParent /*=NULL*/)
	: CDialog(CShowDetailResultDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CShowDetailResultDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_pResultFile = pFile;
	ASSERT(m_pResultFile);	
}


void CShowDetailResultDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CShowDetailResultDlg)
	DDX_Control(pDX, IDC_FILE_TREE, m_fileTree);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CShowDetailResultDlg, CDialog)
	//{{AFX_MSG_MAP(CShowDetailResultDlg)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CShowDetailResultDlg message handlers

BOOL CShowDetailResultDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CString str, strTemp;

	GetDlgItem(IDC_EXT_DATA)->SetWindowText(m_pResultFile->GetFileName());
	HTREEITEM rghItem, subItem1, subItem2, subItem3, subItem4, subItem5, subItem6;

	//////////////////////////////////////////////////////////////////////////
	EP_FILE_HEADER *pHeader = m_pResultFile->GetFileHeader();
	rghItem = m_fileTree.InsertItem("File Info.");
	str.Format("File ID : %s", pHeader->szFileID); //1000152
	m_fileTree.InsertItem(str, rghItem);
	str.Format("File Ver : %s", pHeader->szFileVersion);
	m_fileTree.InsertItem(str, rghItem);
	str.Format("Date Time : %s", pHeader->szCreateDateTime);
	m_fileTree.InsertItem(str, rghItem);
	str.Format("Description : %s", pHeader->szDescrition);
	m_fileTree.InsertItem(str, rghItem);
	str.Format("Extra : %s", pHeader->szReserved);
	m_fileTree.InsertItem(str, rghItem);
	
	//////////////////////////////////////////////////////////////////////////
	rghItem = m_fileTree.InsertItem("Work Info.");

	RESULT_FILE_HEADER *pRltHeader = m_pResultFile->GetResultHeader();
	subItem1 = m_fileTree.InsertItem("Procedure", rghItem);
	str.Format("ModuleID : %d", pRltHeader->nModuleID);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Group : %d", pRltHeader->wGroupIndex+1);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Jig : %d", pRltHeader->wJigIndex+1);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Time : %s", pRltHeader->szDateTime);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("IP : %s", pRltHeader->szModuleIP);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Tray No : %s", pRltHeader->szTrayNo);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Lot No : %s", pRltHeader->szLotNo);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Worker ID : %s", pRltHeader->szOperatorID);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("TestSerial : %s", pRltHeader->szTestSerialNo);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("TraySerial : %s", pRltHeader->szTraySerialNo);
	m_fileTree.InsertItem(str, subItem1);

	//////////////////////////////////////////////////////////////////////////
	EP_MD_SYSTEM_DATA *pSysData = m_pResultFile->GetMDSysData();
	subItem1 = m_fileTree.InsertItem("Unit Info.", rghItem);
	str.Format("ModuleID : %d", pSysData->nModuleID);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Protocol Ver : 0x%x", pSysData->nVersion);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Controller Type : %d", pSysData->nControllerType);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Board Cnt. : %d", pSysData->wInstalledBoard);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Ch per BD : %d", pSysData->wChannelPerBoard);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Module GroupNo : %d", pSysData->nModuleGroupNo);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Channel Cnt. : %d", pSysData->nTotalChNo);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Install Jig : %d", pSysData->wTotalTrayNo);
	m_fileTree.InsertItem(str, subItem1);
	str = "Ch per Jig : ";
	for(int a = 0; a<pSysData->wTotalTrayNo; a++)
	{
		strTemp.Format("%d/", pSysData->awChInTray[a]);
		str += strTemp;
	}
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Tray Type : %d", pSysData->wTrayType);
	m_fileTree.InsertItem(str, subItem1);

	//////////////////////////////////////////////////////////////////////////
	subItem1 = m_fileTree.InsertItem("ETC Info.", rghItem);
	STR_FILE_EXT_SPACE *pExtSpace = m_pResultFile->GetExtraData();
	str.Format("File Name : %s", pExtSpace->szResultFileName);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Cell No : %d", pExtSpace->nCellNo);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Input Cnt. : %d", pExtSpace->nInputCellNo);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Tray Cell Cnt.: %d", pExtSpace->nReserved);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("System ID : %d", pExtSpace->nSystemID);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("System Type : %d", pExtSpace->nSystemType);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Module Name : %s", pExtSpace->szModuleName);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Extra : %s", pExtSpace->szReserved);
	m_fileTree.InsertItem(str, subItem1);

	//////////////////////////////////////////////////////////////////////////
	subItem1 = m_fileTree.InsertItem("Sensor Mapping", rghItem); //100186
	subItem2 = m_fileTree.InsertItem("Sensor 1", subItem1);
	_MAPPING_DATA *pSensorMap = m_pResultFile->GetSensorMap(CFormResultFile::sensorType1);
	for(int s=0; s<EP_MAX_SENSOR_CH; s++)
	{
		if(pSensorMap[s].nChannelNo < 0)
		{
			str.Format("Ch %02d: Not Use", s+1);
		}
		else if(pSensorMap[s].nChannelNo == 0)
		{
			str.Format("Ch %02d: Universal", s+1);
		}
		else
		{
			str.Format("Ch %02d: %s => Unit CH %d", s+1, pSensorMap[s].szName, pSensorMap[s].nChannelNo);
		}
		m_fileTree.InsertItem(str, subItem2);

	}
	subItem2 = m_fileTree.InsertItem("Sensor 2", subItem1);
	pSensorMap = m_pResultFile->GetSensorMap(CFormResultFile::sensorType2);
	for( s=0; s<EP_MAX_SENSOR_CH; s++)
	{
		if(pSensorMap[s].nChannelNo < 0)
		{
			str.Format("Ch %02d: Not Use", s+1);
		}
		else if(pSensorMap[s].nChannelNo == 0)
		{
			str.Format("Ch %02d: Universal", s+1);
		}
		else
		{
			str.Format("Ch %02d: %s => Unit CH %d", s+1, pSensorMap[s].szName, pSensorMap[s].nChannelNo);
		}
		m_fileTree.InsertItem(str, subItem2);
	}

	//////////////////////////////////////////////////////////////////////////
	rghItem = m_fileTree.InsertItem("Schedule Info.");
	subItem1 = m_fileTree.InsertItem("Schedule", rghItem);
	//STR_CONDITION *pCon = m_pResultFile->GetConditionMain();
	CTestCondition *pCon = m_pResultFile->GetTestCondition();
	str.Format("DB PK : %d", pCon->GetModelInfo()->lID);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("No : %d", pCon->GetModelInfo()->lNo);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Type : %d", pCon->GetModelInfo()->lType);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Name : %s", pCon->GetModelInfo()->szName);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Descript. : %s", pCon->GetModelInfo()->szDescription);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Author : %s", pCon->GetModelInfo()->szCreator);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Edit date : %s", pCon->GetModelInfo()->szModifiedTime);
	m_fileTree.InsertItem(str, subItem1);

	subItem1 = m_fileTree.InsertItem("Procedure", rghItem);
	str.Format("DB PK : %d", pCon->GetTestInfo()->lID);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("No : %d", pCon->GetTestInfo()->lNo);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Type : %d", pCon->GetTestInfo()->lType);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Name : %s", pCon->GetTestInfo()->szName);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Decript. : %s", pCon->GetTestInfo()->szDescription);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Author : %s", pCon->GetTestInfo()->szCreator);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Edit date : %s", pCon->GetTestInfo()->szModifiedTime);
	m_fileTree.InsertItem(str, subItem1);

	subItem1 = m_fileTree.InsertItem("Step Info.", rghItem);
	str.Format("Total Step : %d", pCon->GetTotalStepNo());
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Total Grading Step : %d", 0);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Extra 1 : %s", "");
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Extra 2 : %s", "");
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Extra 3 : %d", "");
	m_fileTree.InsertItem(str, subItem1);

	subItem1 = m_fileTree.InsertItem("Cell Check", rghItem);
	str.Format("V High : %f", pCon->GetCheckParam()->fOCVUpperValue);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("V Low : %f", pCon->GetCheckParam()->fOCVLowerValue);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("V ref : %f", pCon->GetCheckParam()->fVRef);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("I ref : %f", pCon->GetCheckParam()->fIRef);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Time : %f", pCon->GetCheckParam()->fTime);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Delta V : %f", pCon->GetCheckParam()->fDeltaVoltage);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Reverse V : %f", pCon->GetCheckParam()->fMaxFaultBattery);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Use Flag : %d", pCon->GetCheckParam()->compFlag);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Auto Process : %d", pCon->GetCheckParam()->autoProcessingYN);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Extra : %d", pCon->GetCheckParam()->reserved);
	m_fileTree.InsertItem(str, subItem1);

	subItem1 = m_fileTree.InsertItem("Step Condition", rghItem);
	STR_COMMON_STEP *pStep;
	for(a=0; a < pCon->GetTotalStepNo(); a++)
	{
		str.Format("Step %d", a+1);
		subItem2 = m_fileTree.InsertItem(str, subItem1);
		pStep = (STR_COMMON_STEP *)pCon->GetStep(a);
		
		subItem3 = m_fileTree.InsertItem("Step Header", subItem2);
		str.Format("Step No : %d", pStep->stepHeader.stepIndex);
		m_fileTree.InsertItem(str, subItem3);
		str.Format("Type : %d", pStep->stepHeader.type);
		m_fileTree.InsertItem(str, subItem3);
		str.Format("Mode : %d", pStep->stepHeader.mode);
		m_fileTree.InsertItem(str, subItem3);
		str.Format("Grade Step Cnt. : %d", pStep->stepHeader.gradeSize);
		m_fileTree.InsertItem(str, subItem3);
		str.Format("Proc. Type : 0x%x", pStep->stepHeader.nProcType);
		m_fileTree.InsertItem(str, subItem3);
		
		str.Format("V Ref. : %f", pStep->fVref);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("I Ref. : %f", pStep->fIref);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("End Time : %f", pStep->fEndTime);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("End Voltage : %f", pStep->fEndV);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("End Current : %f", pStep->fEndI);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("End Capacity : %f", pStep->fEndC);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("End Delta V : %f", pStep->fEndDV);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("End Delta I : %f", pStep->fEndDI);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("Capa. Flag : %d", pStep->bUseActucalCap);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("Extra1 : %d", pStep->bReserved);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("End SOC : %f", pStep->fSocRate);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("V High Limit : %f", pStep->fOverV);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("V Low Limit : %f", pStep->fLimitV);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("I High Limit : %f", pStep->fOverI);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("I Low Limit : %f", pStep->fLimitI);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("C High Limit : %f", pStep->fOverC);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("C Low Limit : %f", pStep->fLimitC);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("Z High Limit : %f", pStep->fOverImp);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("Z Low Limit : %f", pStep->fLimitImp);
		m_fileTree.InsertItem(str, subItem2);
		
		subItem3 = m_fileTree.InsertItem("Rising Check V", subItem2);
		for(int b = 0; b<EP_COMP_POINT; b++)
		{
			str.Format("Check V %d : %f< %f< %f", b+1, pStep->fCompVLow[b],  pStep->fCompTimeV[b],  pStep->fCompVHigh[b]);
			m_fileTree.InsertItem(str, subItem3);
		}
		subItem3 = m_fileTree.InsertItem("Rising Check I", subItem2);
		for(b = 0; b<EP_COMP_POINT; b++)
		{
			str.Format("Check I %d : %f< %f< %f", b+1, pStep->fCompILow[b],  pStep->fCompTimeI[b],  pStep->fCompIHigh[b]);
			m_fileTree.InsertItem(str, subItem3);
		}
		subItem3 = m_fileTree.InsertItem("Delta Check", subItem2);
		str.Format("Delta V Limit: %f/%f", pStep->fDeltaV, pStep->fDeltaTimeV);
		m_fileTree.InsertItem(str, subItem3);
		str.Format("Delta I Limit: %f/%f", pStep->fDeltaI, pStep->fDeltaTimeI);
		m_fileTree.InsertItem(str, subItem3);
		
		str.Format("Move Step : %d", pStep->nLoopInfoGoto);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("Loop Cnt. : %d", pStep->nLoopInfoCycle);
		m_fileTree.InsertItem(str, subItem2);
	
		subItem3 = m_fileTree.InsertItem("Grading Condition", subItem2);
		for(b = 0; b<EP_MAX_GRADE_STEP; b++)
		{
			str.Format("Grade Step %d : No %d, Item %d, %f <Value <%f =>%c", b+1, 
						pStep->grade[b].index+1, pStep->grade[b].gradeItem, pStep->grade[b].fMin, pStep->grade[b].fMax, pStep->grade[b].gradeCode);
			m_fileTree.InsertItem(str, subItem3);
		}

		//float		fReserved[16];
	}

	rghItem = m_fileTree.InsertItem("Step Result");

	STR_STEP_RESULT *pResult;
	int nStepSize = m_pResultFile->GetStepSize();
	for( a = 0; a<nStepSize; a++)
	{
		pResult = m_pResultFile->GetStepData(a);
		if(pResult == NULL)	break;

		str.Format("Step %d", a+1);
		subItem1 = m_fileTree.InsertItem(str, rghItem);

		//
		subItem2 = m_fileTree.InsertItem("Status", subItem1);
		subItem3 = m_fileTree.InsertItem("Jig", subItem2);
		str.Format("Box Status : %d", pResult->gpStateSave.gpState.state);
		m_fileTree.InsertItem(str, subItem3);
		str.Format("Fail Code : %d", pResult->gpStateSave.gpState.failCode);
		m_fileTree.InsertItem(str, subItem3);
		str.Format("Sensor1 : 0x%x", pResult->gpStateSave.gpState.sensorState1);
		m_fileTree.InsertItem(str, subItem3);
		str.Format("Sensor2 : 0x%x", pResult->gpStateSave.gpState.sensorState2);
		m_fileTree.InsertItem(str, subItem3);
		str.Format("Sensor3 : 0x%x", pResult->gpStateSave.gpState.sensorState3);
		m_fileTree.InsertItem(str, subItem3);
		str.Format("Sensor4 : 0x%x", pResult->gpStateSave.gpState.sensorState4);
		m_fileTree.InsertItem(str, subItem3);
		str.Format("Sensor5 : 0x%x", pResult->gpStateSave.gpState.sensorState5);
		m_fileTree.InsertItem(str, subItem3);
		str.Format("Sensor6 : 0x%x", pResult->gpStateSave.gpState.sensorState6);
		m_fileTree.InsertItem(str, subItem3);
		str.Format("Sensor7 : 0x%x", pResult->gpStateSave.gpState.sensorState7);
		m_fileTree.InsertItem(str, subItem3);
		str.Format("Sensor8 : 0x%x", pResult->gpStateSave.gpState.sensorState8);
		m_fileTree.InsertItem(str, subItem3);
		str.Format("�ΰ����� : %d", pResult->gpStateSave.gpState.lReserved[0]);
		m_fileTree.InsertItem(str, subItem3);

		subItem3 = m_fileTree.InsertItem("Sensor", subItem2);
		subItem4 = m_fileTree.InsertItem("Sensor 1", subItem3);
		for(int s=0; s<EP_MAX_SENSOR_CH; s++)
		{
			str.Format("Ch %d", s+1);
			subItem5 = m_fileTree.InsertItem(str, subItem4);

			str.Format("Sensor Data : %f", pResult->gpStateSave.sensorChData1[s].fData);
			m_fileTree.InsertItem(str, subItem5);
			str.Format("Reserved : %d", pResult->gpStateSave.sensorChData1[s].lReserved);
			m_fileTree.InsertItem(str, subItem5);
			subItem6 = m_fileTree.InsertItem("MinMax", subItem5);
			str.Format("Max Val. : %d", pResult->gpStateSave.sensorChData1[s].minmaxData.lMax);
			m_fileTree.InsertItem(str, subItem6);
			str.Format("Min val. : %d", pResult->gpStateSave.sensorChData1[s].minmaxData.lMin);
			m_fileTree.InsertItem(str, subItem6);
			str.Format("Average : %d", pResult->gpStateSave.sensorChData1[s].minmaxData.lAvg);
			m_fileTree.InsertItem(str, subItem6);
			str.Format("Max Index : %d", pResult->gpStateSave.sensorChData1[s].minmaxData.wMaxIndex);
			m_fileTree.InsertItem(str, subItem6);
			str.Format("Min Index : %d", pResult->gpStateSave.sensorChData1[s].minmaxData.wMinIndex);
			m_fileTree.InsertItem(str, subItem6);
			str.Format("Max Time : %s", pResult->gpStateSave.sensorChData1[s].minmaxData.szMaxDateTime);
			m_fileTree.InsertItem(str, subItem6);
			str.Format("Min Time: %s", pResult->gpStateSave.sensorChData1[s].minmaxData.szMinDateTime);
			m_fileTree.InsertItem(str, subItem6);
		}
	
		subItem4 = m_fileTree.InsertItem("Sensor 2", subItem3);
		for( s=0; s<EP_MAX_SENSOR_CH; s++)
		{
			str.Format("Ch %d", s+1);
			subItem5 = m_fileTree.InsertItem(str, subItem4);

			str.Format("Sensor Data : %f", pResult->gpStateSave.sensorChData2[s].fData);
			m_fileTree.InsertItem(str, subItem5);
			str.Format("Reserved : %d", pResult->gpStateSave.sensorChData2[s].lReserved);
			m_fileTree.InsertItem(str, subItem5);
			subItem6 = m_fileTree.InsertItem("MinMax", subItem5);
			str.Format("Max Val. : %d", pResult->gpStateSave.sensorChData1[s].minmaxData.lMax);
			m_fileTree.InsertItem(str, subItem6);
			str.Format("Min Val. : %d", pResult->gpStateSave.sensorChData1[s].minmaxData.lMin);
			m_fileTree.InsertItem(str, subItem6);
			str.Format("Average : %d", pResult->gpStateSave.sensorChData1[s].minmaxData.lAvg);
			m_fileTree.InsertItem(str, subItem6);
			str.Format("Max Index : %d", pResult->gpStateSave.sensorChData1[s].minmaxData.wMaxIndex);
			m_fileTree.InsertItem(str, subItem6);
			str.Format("Min Index : %d", pResult->gpStateSave.sensorChData1[s].minmaxData.wMinIndex);
			m_fileTree.InsertItem(str, subItem6);
			str.Format("Max Time : %s", pResult->gpStateSave.sensorChData1[s].minmaxData.szMaxDateTime);
			m_fileTree.InsertItem(str, subItem6);
			str.Format("Min Time : %s", pResult->gpStateSave.sensorChData1[s].minmaxData.szMinDateTime);
			m_fileTree.InsertItem(str, subItem6);
		}

		subItem2 = m_fileTree.InsertItem("Step Header", subItem1);
		str.Format("ModuleID : %d", pResult->stepHead.nModuleID);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("Group : %d", pResult->stepHead.wGroupIndex+1);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("Jig : %d", pResult->stepHead.wJigIndex+1);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("Start Time : %s", pResult->stepHead.szStartDateTime);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("End Time : %s", pResult->stepHead.szEndDateTime);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("IP : %s", pResult->stepHead.szModuleIP);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("Tray No : %s", pResult->stepHead.szTrayNo);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("Worker ID : %s", pResult->stepHead.szOperatorID);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("TestSerial : %s", pResult->stepHead.szTestSerialNo);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("TraySerial : %s", pResult->stepHead.szTraySerialNo);
		m_fileTree.InsertItem(str, subItem2);

		//
		subItem2 = m_fileTree.InsertItem("Step Condition", subItem1);	
		STR_COMMON_STEP *pStep = &pResult->stepCondition;
		subItem3 = m_fileTree.InsertItem("Step Header", subItem2);
		str.Format("Step No : %d", pStep->stepHeader.stepIndex);
		m_fileTree.InsertItem(str, subItem3);
		str.Format("Type : %d", pStep->stepHeader.type);
		m_fileTree.InsertItem(str, subItem3);
		str.Format("Mode : %d", pStep->stepHeader.mode);
		m_fileTree.InsertItem(str, subItem3);
		str.Format("Grade Step Cnt. : %d", pStep->stepHeader.gradeSize);
		m_fileTree.InsertItem(str, subItem3);
		str.Format("Proc. Type : 0x%x", pStep->stepHeader.nProcType);
		m_fileTree.InsertItem(str, subItem3);
		
		str.Format("V Ref. : %f", pStep->fVref);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("I Ref. : %f", pStep->fIref);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("End Time : %f", pStep->fEndTime);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("End Voltage : %f", pStep->fEndV);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("End Current : %f", pStep->fEndI);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("End Capacity : %f", pStep->fEndC);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("End Delta V : %f", pStep->fEndDV);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("End Delta I : %f", pStep->fEndDI);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("Capa. Flag : %d", pStep->bUseActucalCap);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("Extra1 : %d", pStep->bReserved);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("End SOC : %f", pStep->fSocRate);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("V High Limit : %f", pStep->fOverV);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("V Low Limit : %f", pStep->fLimitV);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("I High Limit : %f", pStep->fOverI);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("I Low Limit : %f", pStep->fLimitI);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("C High Limit : %f", pStep->fOverC);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("C Low Limit : %f", pStep->fLimitC);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("Z High Limit : %f", pStep->fOverImp);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("Z Low Limit : %f", pStep->fLimitImp);
		m_fileTree.InsertItem(str, subItem2);
		
		subItem3 = m_fileTree.InsertItem("Rising Check V", subItem2);
		for(int b = 0; b<EP_COMP_POINT; b++)
		{
			str.Format("Check V %d : %f< %f< %f", b+1, pStep->fCompVLow[b],  pStep->fCompTimeV[b],  pStep->fCompVHigh[b]);
			m_fileTree.InsertItem(str, subItem3);
		}
		subItem3 = m_fileTree.InsertItem("Rising Check I", subItem2);
		for(b = 0; b<EP_COMP_POINT; b++)
		{
			str.Format("Check I %d : %f< %f< %f", b+1, pStep->fCompILow[b],  pStep->fCompTimeI[b],  pStep->fCompIHigh[b]);
			m_fileTree.InsertItem(str, subItem3);
		}
		subItem3 = m_fileTree.InsertItem("Delta Check", subItem2);
		str.Format("Delta V Limit: %f/%f", pStep->fDeltaV, pStep->fDeltaTimeV);
		m_fileTree.InsertItem(str, subItem3);
		str.Format("Delta I Limit: %f/%f", pStep->fDeltaI, pStep->fDeltaTimeI);
		m_fileTree.InsertItem(str, subItem3);
		
		str.Format("Move Step : %d", pStep->nLoopInfoGoto);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("Loop Cnt. : %d", pStep->nLoopInfoCycle);
		m_fileTree.InsertItem(str, subItem2);
	
		subItem3 = m_fileTree.InsertItem("Grading Condition", subItem2);
		for(b = 0; b<EP_MAX_GRADE_STEP; b++)
		{
			str.Format("Grade Step %d : No %d, Item %d, %f <Value <%f =>%c", b+1, 
						pStep->grade[b].index+1, pStep->grade[b].gradeItem, pStep->grade[b].fMin, pStep->grade[b].fMax, pStep->grade[b].gradeCode);
			m_fileTree.InsertItem(str, subItem3);
		}

		//
		subItem2 = m_fileTree.InsertItem("Summery", subItem1);
		for( b =0; b<EP_RESULT_ITEM_NO; b++)
		{
			str.Format("Item %d", b+1);
			subItem3 = m_fileTree.InsertItem(str, subItem2);
			str.Format("Average : %f",pResult->averageData[b].GetAvg());
			m_fileTree.InsertItem(str, subItem3);
			str.Format("Min : %f",pResult->averageData[b].GetMinValue());
			m_fileTree.InsertItem(str, subItem3);
			str.Format("Min Ch : %d",pResult->averageData[b].GetMinIndex()+1);
			m_fileTree.InsertItem(str, subItem3);
			str.Format("Max : %f",pResult->averageData[b].GetMaxValue());
			m_fileTree.InsertItem(str, subItem3);
			str.Format("Max Ch : %d",pResult->averageData[b].GetMaxIndex()+1);
			m_fileTree.InsertItem(str, subItem3);	
			str.Format("STDEV : %f",pResult->averageData[b].GetSTDD());
			m_fileTree.InsertItem(str, subItem3);	
			str.Format("GOOD Cnt. : %d",pResult->averageData[b].GetDataCount());
			m_fileTree.InsertItem(str, subItem3);	
		}
		
		//
		subItem2 = m_fileTree.InsertItem("Channel", subItem1);
		STR_SAVE_CH_DATA *pChData;
		for(b =0; b<pResult->aChData.GetSize(); b++)
		{
			str.Format("Ch %d", b+1);
			subItem3 = m_fileTree.InsertItem(str, subItem2);

			pChData = (STR_SAVE_CH_DATA *)pResult->aChData[b]; 
			str.Format("Tray CH Number : %d", pChData->wChIndex+1);
			m_fileTree.InsertItem(str, subItem3);
			str.Format("Status : %d", pChData->state);
			m_fileTree.InsertItem(str, subItem3);
			str.Format("Grade code : %c", pChData->grade);
			m_fileTree.InsertItem(str, subItem3);
			str.Format("CH Code : %d", pChData->channelCode);
			m_fileTree.InsertItem(str, subItem3);
			str.Format("Step No: %d", pChData->nStepNo);
			m_fileTree.InsertItem(str, subItem3);
			str.Format("Step Time : %f", pChData->fStepTime);
			m_fileTree.InsertItem(str, subItem3);
			str.Format("Total Run Time : %f", pChData->fTotalTime);
			m_fileTree.InsertItem(str, subItem3);
			str.Format("Voltage : %f", pChData->fVoltage);
			m_fileTree.InsertItem(str, subItem3);
			str.Format("Current : %f", pChData->fCurrent);
			m_fileTree.InsertItem(str, subItem3);
			str.Format("Watt : %f", pChData->fWatt);
			m_fileTree.InsertItem(str, subItem3);
			str.Format("WattHour : %f", pChData->fWattHour);
			m_fileTree.InsertItem(str, subItem3);
			str.Format("Capacity : %f", pChData->fCapacity);
			m_fileTree.InsertItem(str, subItem3);
			str.Format("Impedeance : %f", pChData->fImpedance);
			m_fileTree.InsertItem(str, subItem3);
			str.Format("Temperature : %f", pChData->fAuxData[0]);
			m_fileTree.InsertItem(str, subItem3);
			str.Format("Unit CH Number : %d", pChData->wModuleChIndex+1);
			m_fileTree.InsertItem(str, subItem3);
			str.Format("Extra 1 : %d", pChData->wReserved);
			m_fileTree.InsertItem(str, subItem3);
			
			//str.Format("Ch %d", pChData->fReserved[0]);
			//subItem4 = m_fileTree.InsertItem(str, subItem3);

		}
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CShowDetailResultDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	if(::IsWindow(this->GetSafeHwnd()))
	{
		RECT rect;
		::GetClientRect(m_hWnd, &rect);

		CRect rectGrid;
		if(m_fileTree.GetSafeHwnd())
		{
			m_fileTree.GetWindowRect(rectGrid);	//Step Grid Window Position
			ScreenToClient(&rectGrid);
			m_fileTree.MoveWindow(5, rectGrid.top, rect.right-10, rect.bottom-rectGrid.top-5, FALSE);
			Invalidate();
		}
	}	
}
