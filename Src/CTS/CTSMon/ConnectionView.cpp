// ConnectionView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "ConnectionView.h"
#include "MainFrm.h"

// CConnectionView

IMPLEMENT_DYNCREATE(CConnectionView, CFormView)

CConnectionView::CConnectionView()
	: CFormView(CConnectionView::IDD)
{
	m_pConfig = NULL;
	m_pRowCol2GpCh = NULL;

	m_nCurModuleID = 0;
	m_nDisplayTimer = 0;
	m_nCurGroup = 0;
	m_nCurChannel = 1;
	m_nCurTrayIndex = 0;
	m_nTopGridColCount = 1;
	LanguageinitMonConfig();
}

CConnectionView::~CConnectionView()
{
	if(m_pRowCol2GpCh)
	{
		delete[] m_pRowCol2GpCh;
		m_pRowCol2GpCh = NULL;
	}
}

bool CConnectionView::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CConnectionView"), _T("TEXT_CConnectionView_CNT"), _T("TEXT_CConnectionView_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CConnectionView_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CConnectionView"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

void CConnectionView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, ID_DLG_LABEL_NAME, m_LabelViewName);
	DDX_Control(pDX, IDC_STAGE_NAME, m_CmdTarget);
	DDX_Control(pDX, IDC_LIST_ERROR_CODE, m_ctrlChErrorList);
}

BEGIN_MESSAGE_MAP(CConnectionView, CFormView)
	ON_WM_SIZE()
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CConnectionView 진단입니다.

#ifdef _DEBUG
void CConnectionView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CConnectionView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif

CCTSMonDoc* CConnectionView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCTSMonDoc)));
	return (CCTSMonDoc*)m_pDocument;
}

#endif //_DEBUG


// CConnectionView 메시지 처리기입니다.
void CConnectionView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetDocument();
	m_pConfig = pDoc->GetTopConfig();						//Top Config

	m_nTopGridColCount = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "TopGridColCout", 1);
	m_nTopGridRowCount = EP_MAX_CH_PER_MD/m_nTopGridColCount;
	if(EP_MAX_CH_PER_MD % m_nTopGridColCount) 
		m_nTopGridRowCount++;

	InitLabel();
	
	InitChannelCodeList();
	LoadChannelCode();

	InitSmallChGrid();
	SetTopGridFont();
}

void CConnectionView::InitChannelCodeList()
{
	CRect rect;
	GetDlgItem(IDC_LIST_ERROR_CODE)->GetWindowRect(rect);

	DWORD style = 	m_ctrlChErrorList.GetExtendedStyle();
	// style |= LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_HEADERDRAGDROP|LVS_EX_SUBITEMIMAGES;
	style |= LVS_EX_GRIDLINES;
	m_ctrlChErrorList.SetExtendedStyle(style );

	// Column 삽입
	m_ctrlChErrorList.InsertColumn(0, "Code", LVCFMT_CENTER, 50);	//center 정렬을 0 column을 사용하지 않음 
	m_ctrlChErrorList.InsertColumn(1, "Desc", LVCFMT_CENTER, rect.Width()-52);
}

bool CConnectionView::LoadChannelCode()
{
	CFile file;
	CFileException* e;
	e = new CFileException;

	int nI = 0;
	LVITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.mask = LVIF_TEXT;
	
	if(!file.Open("ChannelErrorCode.cfg", CFile::modeRead|CFile::typeBinary ))
	{
		return FALSE;
	}

	WCHAR *buffer;
	buffer = new WCHAR[file.GetLength()];

	WCHAR buf[1];

	int i = 0;
	int nRength = 0;

	CString strData;
	CString strTemp;

	file.Read(buf, sizeof(WCHAR));

	while (1)
	{
		file.Read(buf, sizeof(WCHAR));

		if( buf[0] == 0x0d )
		{
			buffer[i] = 0x00;

			int nLen = WideCharToMultiByte(CP_ACP, 0, buffer, -1, NULL, 0, NULL, NULL);			

			char* pMultibyte  = new char[nLen];
			memset(pMultibyte, 0x00, (nLen)*sizeof(char));

			WideCharToMultiByte(CP_ACP, 0, buffer, -1, pMultibyte, nLen, NULL, NULL);

			ZeroMemory( buffer, sizeof(WCHAR) * file.GetLength());			

			strData = pMultibyte;

			delete pMultibyte;

			i = 0;

			if(!strData.IsEmpty())
			{
				lvItem.iItem = nI;
				lvItem.iSubItem = 0;
				m_ctrlChErrorList.InsertItem(&lvItem);

				AfxExtractSubString(strTemp, strData, 0,';');
				m_ctrlChErrorList.SetItemText(nI, 0, strTemp);

				AfxExtractSubString(strTemp, strData, g_nLanguage+1,';');
				m_ctrlChErrorList.SetItemText(nI, 1, strTemp);
				nI++;
			}			
		}
		else
		{
			buffer[i] = buf[0];		

			i++;
			nRength++;
		}
		
		if( nRength > file.GetLength()-2 )
		{
			break;
		}
	}

	file.Close();
	delete buffer;
	
	return true;
}

void CConnectionView::ModuleConnected(int nModuleID)
{
	if(nModuleID == m_nCurModuleID)							//현재 선택된 Module이면 Grid를 Setting 시키고
	{	
		StartMonitoring();	
	}	
}

void CConnectionView::ModuleDisConnected(int nModuleID)
{
	if(nModuleID == m_nCurModuleID)							//현재 선택된 Module이면 Grid를 Setting 시키고
	{	
		StopMonitoring();	
	}
}

void CConnectionView::UpdateGroupState(int nModuleID, int /*nGroupIndex*/)
{
	// 1. 상태 변경시 작성할 부분
}

void CConnectionView::GroupSetChanged(int nColCount)
{
	//Calculate Grid Info
	if(nColCount >0 && nColCount<= 64)
	{
		m_nTopGridColCount = nColCount;						//Update Top Grid Column Count
	}

	WORD nCount = 0, wChinGroup;

	CFormModule *pModule = GetDocument()->GetModuleInfo(m_nCurModuleID);
	if(pModule == NULL)	
	{
		return;
	}

	//Tray 단위 보기 
	wChinGroup = pModule->GetChInJig(m_nCurTrayIndex);

	//Module 단위 보기 
	//	wChinGroup = pModule->GetTotalInstallCh();
	nCount = (wChinGroup / m_nTopGridColCount);
	if(wChinGroup%m_nTopGridColCount>0)
	{
		nCount++;
	}

	m_nTopGridRowCount = (int)nCount;					//Update Top Gird Row Count

	//Make Group And Channel Index Table
	if(m_pRowCol2GpCh)
	{
		delete[] m_pRowCol2GpCh;
		m_pRowCol2GpCh = NULL;
	}

	nCount= m_nTopGridRowCount*m_nTopGridColCount;
	//	ASSERT(nCount);
	m_pRowCol2GpCh = new SHORT[nCount];
	memset(m_pRowCol2GpCh, -1, nCount);

	nCount = 0;
	WORD nRowIndex = 0, nCellCout = 0;

	//Grid의 특정 위치에 Group 번호와 Channel 번호를 쉽게 찾기 위해 Mapping Table을 생성한다. 

	WORD nTrayType = GetDocument()->m_nViewTrayType;
	WORD j = 0;
	switch (nTrayType)
	{
	case TRAY_TYPE_CUSTOMER:
	case TRAY_TYPE_LG_ROW_COL:	//세로 번호 체계
	case TRAY_TYPE_PB5_ROW_COL:	//세로 번호 체계(각형)
		for( j=0; j<wChinGroup; j++)
		{
			nCellCout = nRowIndex*m_nTopGridColCount+nCount;
			m_pRowCol2GpCh[nCellCout] = j;
			nRowIndex++;

			if(nRowIndex >= m_nTopGridRowCount)
			{
				nCount++;
				nRowIndex = 0;
			}
		}
		break;
	case TRAY_TYPE_PB5_COL_ROW:	//가로 번호 체계(각형)
		for( j=0; j<wChinGroup; j++)
		{
			nCellCout = nRowIndex*m_nTopGridColCount+nCount;
			m_pRowCol2GpCh[nCellCout] = j;
			nCount++;

			if(nCount >= m_nTopGridColCount)
			{
				nCount=0;
				nRowIndex++;
			}
		}
		break;
	}	

	//모둘에 할당된 지그(Tray) 숫자 가져오기
	int nCnt = pModule->GetTotalJig();

	//////////////////////////////////////////////////////////////////////////
	//Draw small channel display grid
	BOOL bLock = m_wndSmallChGrid.LockUpdate();
	m_wndSmallChGrid.SetRowCount(1);
	m_wndSmallChGrid.SetColCount(1);		//Delete Grid Attribute

	UINT nRow = 0, nCol = 1;
	int n = 0;
	for(n = 0; n<nCnt; n++)
	{
		nRow += pModule->GetChInJig(n);
	}

	nCol = 2 * m_nTopGridColCount;

	if(m_wndSmallChGrid.GetRowCount() != nRow+1 || m_wndSmallChGrid.GetColCount() != nCol+1)
	{
		m_wndSmallChGrid.SetRowCount(m_nTopGridRowCount);
		m_wndSmallChGrid.SetColCount(nCol);
		m_wndSmallChGrid.SetCoveredCellsRowCol(0, 1, 0, m_nTopGridColCount);
		m_wndSmallChGrid.SetCoveredCellsRowCol(0, 1+m_nTopGridColCount, 0, m_nTopGridColCount*2);

		CString strTemp;		
		for( n=0; n<pModule->GetTotalJig(); n++)
		{			
			strTemp.Format("TRAY %d", n+1);
			m_wndSmallChGrid.SetStyleRange(CGXRange(0, m_nTopGridColCount+1), 
				CGXStyle().SetValue(strTemp).SetFont(CGXFont().SetBold(TRUE))
				);

			COLORREF boldColor = RGB(255, 192, 0);

			m_wndSmallChGrid.SetStyleRange(CGXRange(1, 1+m_nTopGridColCount, 1, 2*m_nTopGridColCount ), CGXStyle().SetBorders(gxBorderTop, CGXPen().SetWidth(2).SetColor(boldColor)));			
			m_wndSmallChGrid.SetStyleRange(CGXRange(m_nTopGridRowCount, 1+m_nTopGridColCount, m_nTopGridRowCount, 2*m_nTopGridColCount), CGXStyle().SetBorders(gxBorderBottom, CGXPen().SetWidth(2).SetColor(boldColor)));

			m_wndSmallChGrid.SetStyleRange(CGXRange(1, 1+m_nTopGridColCount, m_nTopGridRowCount, 1+m_nTopGridColCount), CGXStyle().SetBorders(gxBorderLeft, CGXPen().SetWidth(2).SetColor(boldColor)));
			m_wndSmallChGrid.SetStyleRange(CGXRange(1, 2*m_nTopGridColCount, m_nTopGridRowCount, 2*m_nTopGridColCount), CGXStyle().SetBorders(gxBorderRight, CGXPen().SetWidth(2).SetColor(boldColor)));
		}

		m_wndSmallChGrid.SetStyleRange(CGXRange(0, 1), CGXStyle().SetValue("CH(/)").SetFont(CGXFont().SetBold(TRUE)));
		
		int a = 1;
		int b = 1;
		int nChNum = 0;

		for( a=0; a<m_nTopGridColCount; a++ )
		{
			for ( b=1; b<=m_nTopGridRowCount; b++)
			{
				nChNum = b + m_nTopGridRowCount*a;
				m_wndSmallChGrid.SetStyleRange(CGXRange(b, a+1), CGXStyle().SetValue(ROWCOL(nChNum)).SetDraw3dFrame(gxFrameRaised).SetEnabled(FALSE));	
			}
		}
	}
	m_wndSmallChGrid.LockUpdate(bLock);
	m_wndSmallChGrid.Redraw();
}

void CConnectionView::DrawConnectionCode()
{
	CCTSMonDoc *pDoc = GetDocument();
	CString strSelFileName = _T("");

	CFormModule *pModule;
	pModule =  pDoc->GetModuleInfo(m_nCurModuleID);
	if(pModule == NULL)
	{
		return;
	}

	strSelFileName = pModule->GetResultFileName();
	if( strSelFileName.IsEmpty() )
	{
		return;
	}

	CFormResultFile formFile;
	if(formFile.ReadFile(strSelFileName) < 0)
	{
		return;
	}

	BYTE colorFlag = 0;
	CString  strMsg, strToolTip;

	ROWCOL nRow = 0, nCol = 0;
	int nChannelNo = 0;
	int nTotalGroup = EPGetGroupCount(m_nCurModuleID);

	STR_STEP_RESULT *lpStepData;
	STR_SAVE_CH_DATA *lpChannel;

	lpStepData = formFile.GetStepData(formFile.GetStepSize()-1);
	if( lpStepData == NULL )
	{
		return;
	}

	bool bLock = m_wndSmallChGrid.LockUpdate();
	for (INDEX nGroupIndex= 0; nGroupIndex < pModule->GetTotalJig(); nGroupIndex++)	// Group 수 만큼 
	{
		nChannelNo = formFile.GetTotalCh();

		for (INDEX ch = 0; ch < nChannelNo; ch++)					//Display Channel Ch Per Group
		{	
			if(GetChRowCol(nGroupIndex, ch, nRow, nCol) == FALSE)		
			{
				break;
			}

			lpChannel = (STR_SAVE_CH_DATA *)lpStepData->aChData[ch];
			if( lpChannel != NULL )
			{
				strMsg = pDoc->ChCodeMsg(lpChannel->channelCode);

				if( lpChannel->state == EP_STATE_FAULT )
				{
					pDoc->GetStateMsg(lpChannel->state, colorFlag);
				}
				else
				{
					colorFlag = 0;
				}

				m_wndSmallChGrid.SetStyleRange(CGXRange(nRow, nCol), 
					CGXStyle().SetValue(strMsg).SetTextColor(m_pConfig->m_TStateColor[colorFlag]).SetInterior(m_pConfig->m_BStateColor[colorFlag]));
			}

			strToolTip.Format("CH : %d", ch+1 );

			m_wndSmallChGrid.SetStyleRange(CGXRange(nRow, nCol), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(strToolTip)));		
		}

		strMsg = pModule->GetTrayNo(nGroupIndex);
		if(strMsg.IsEmpty())
		{
			strMsg.Format("TRAY %d", nGroupIndex+1);
		}

		m_wndSmallChGrid.SetValueRange(CGXRange(0, 2), strMsg);	
	}

	m_wndSmallChGrid.LockUpdate(bLock);
	m_wndSmallChGrid.Redraw();	
}

void CConnectionView::DrawChannel()
{
	CCTSMonDoc *pDoc = GetDocument();
	BYTE colorFlag = 0;
	CString  strMsg, strToolTip;

	float	fTemp = 0;

	CStep *pStep = NULL;

	ROWCOL nRow = 0, nCol = 0;
	int nChannelNo = 0;
	int nTotalGroup = EPGetGroupCount(m_nCurModuleID);

	CFormModule *pModule;
	pModule =  pDoc->GetModuleInfo(m_nCurModuleID);
	if(pModule == NULL)		
	{
		return;
	}

	STR_SAVE_CH_DATA chSaveData;		

	bool bLock = m_wndSmallChGrid.LockUpdate();
	for (INDEX nGroupIndex= 0; nGroupIndex < pModule->GetTotalJig(); nGroupIndex++)	// Group 수 만큼 
	{
		nChannelNo = pModule->GetChInJig(nGroupIndex);
		
		for (INDEX nChannelIndex = 0; nChannelIndex < nChannelNo; nChannelIndex++)					//Display Channel Ch Per Group
		{	
			if(GetChRowCol(nGroupIndex, nChannelIndex, nRow, nCol) == FALSE)		
			{
				break;
			}
			
			pModule->GetChannelStepData(chSaveData, nChannelIndex, nGroupIndex);

			strToolTip.Format("CH : %d", nChannelIndex+1 );

			m_wndSmallChGrid.SetStyleRange(CGXRange(nRow, nCol), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(strToolTip)));
			
			strMsg = pDoc->ChCodeMsg(chSaveData.channelCode);

			if( chSaveData.state == EP_STATE_FAULT )
			{
				pDoc->GetStateMsg(chSaveData.state, colorFlag);
			}
			else
			{
				colorFlag = 0;
			}

			m_wndSmallChGrid.SetStyleRange(CGXRange(nRow, nCol), 
				CGXStyle().SetValue(strMsg).SetTextColor(m_pConfig->m_TStateColor[colorFlag]).SetInterior(m_pConfig->m_BStateColor[colorFlag]));
		}

		//1열 배열 표기시 Column Heaer에 Tray 번호 표시 
		strMsg = pModule->GetTrayNo(nGroupIndex);
		if(strMsg.IsEmpty())
		{
			strMsg.Format("TRAY %d", nGroupIndex+1);
		}

		m_wndSmallChGrid.SetValueRange(CGXRange(0, 2), strMsg);	
	}

	m_wndSmallChGrid.LockUpdate(bLock);
	m_wndSmallChGrid.Redraw();	
}

void CConnectionView::InitLabel()
{
	m_CmdTarget.SetFontSize(20)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND_STAGENAME)
		.SetFontBold(TRUE)
		.SetFontName(TEXT_LANG[0]);

	m_LabelViewName.SetBkColor(RGB_MIDNIGHTBLUE);
	m_LabelViewName.SetTextColor(RGB_WHITE);
	m_LabelViewName.SetFontSize(24);
	m_LabelViewName.SetFontBold(TRUE);
	m_LabelViewName.SetText("Code");
}

void CConnectionView::InitSmallChGrid()
{
	m_wndSmallChGrid.SubclassDlgItem(IDC_LARGE_GRID, this);
	m_wndSmallChGrid.m_bSameRowSize = TRUE;
	m_wndSmallChGrid.m_bSameColSize = TRUE;
	m_wndSmallChGrid.m_bCustomWidth = FALSE;
	m_wndSmallChGrid.m_bCustomColor = FALSE;

	m_wndSmallChGrid.Initialize();
	m_wndSmallChGrid.SetRowCount(1);
	m_wndSmallChGrid.SetColCount(1);
	m_wndSmallChGrid.SetDefaultRowHeight(26);
	m_wndSmallChGrid.SetFrozenRows(1);

	//Enable Multi Channel Select
	m_wndSmallChGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	//Enable Tooltips
	m_wndSmallChGrid.EnableGridToolTips();
	m_wndSmallChGrid.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, 
		CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));
	//Use MemDc
	m_wndSmallChGrid.SetDrawingTechnique(gxDrawUsingMemDC);

	//Row Header Setting
	m_wndSmallChGrid.SetStyleRange(CGXRange().SetCols(1),
		CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(255,255,255)).SetInterior(RGB(192,192,192)));
}

BOOL CConnectionView::GetChRowCol(int nItemIndex, int nChindex, ROWCOL &nRow, ROWCOL &nCol)
{
	nRow = nChindex % m_nTopGridRowCount + 1;
	nCol = nChindex / m_nTopGridRowCount + m_nTopGridColCount+1;
	nCol = nCol + (nItemIndex * m_nTopGridColCount);	

	if(nCol < 0 || nCol > 6 * m_nTopGridColCount+ m_nTopGridColCount+1)	return FALSE;
	if(nRow < 0 || nRow > m_nTopGridRowCount+2)	return FALSE;

	return TRUE;
}

void CConnectionView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	if(::IsWindow(this->GetSafeHwnd()))
	{
		CFormView::ShowScrollBar(SB_HORZ,FALSE);
		CRect ctrlRect;
		RECT rect;
		::GetClientRect(m_hWnd, &rect);

		if(m_wndSmallChGrid.GetSafeHwnd())
		{
			m_wndSmallChGrid.GetWindowRect(&ctrlRect);
			ScreenToClient(&ctrlRect);
			m_wndSmallChGrid.MoveWindow(ctrlRect.left, ctrlRect.top, rect.right-ctrlRect.left-5, rect.bottom-ctrlRect.top-5, FALSE);
		}

		if( GetDlgItem(ID_DLG_LABEL_NAME)->GetSafeHwnd() )
		{
			CRect rectGrid;
			CRect rectStageLabel;
			GetDlgItem(ID_DLG_LABEL_NAME)->GetWindowRect(rectGrid);
			ScreenToClient(&rectGrid);
			GetDlgItem(IDC_STAGE_NAME)->GetWindowRect(rectStageLabel);
			ScreenToClient(&rectStageLabel);
			GetDlgItem(ID_DLG_LABEL_NAME)->MoveWindow(rectGrid.left, rectGrid.top, rect.right-rectStageLabel.Width(), rectGrid.Height(), FALSE);
		}
		
		if(::IsWindow(GetDlgItem(IDC_LIST_ERROR_CODE)->GetSafeHwnd()))
		{
			GetDlgItem(IDC_LIST_ERROR_CODE)->GetWindowRect(&ctrlRect);
			ScreenToClient(&ctrlRect);
			GetDlgItem(IDC_LIST_ERROR_CODE)->MoveWindow(ctrlRect.left, ctrlRect.top, ctrlRect.Width(), rect.bottom - ctrlRect.top-5 , FALSE);
		}
	}	
}

void CConnectionView::SetTopGridFont()
{
	//Font를 Registry에서 Laod
	UINT nSize;
	LPVOID* pData;
	LOGFONT lfGridFont;
	BOOL nRtn = AfxGetApp()->GetProfileBinary(FORMSET_REG_SECTION, "GridFont", (LPBYTE *)&pData, &nSize);
	if(nRtn && nSize > 0)
	{
		memcpy(&lfGridFont, pData, nSize);
	}
	else
	{
		CFont *pFont = GetFont();
		pFont->GetLogFont(&lfGridFont);
		AfxGetApp()->WriteProfileBinary(FORMSET_REG_SECTION, "GridFont",(LPBYTE)&lfGridFont, sizeof(LOGFONT));
	}
	delete [] pData;

	m_wndSmallChGrid.SetStyleRange(CGXRange().SetTable(), CGXStyle().SetFont(CGXFont(lfGridFont)));
}

void CConnectionView::SetCurrentModule(int nModuleID, int nGroupIndex)
{	
	m_nCurModuleID = nModuleID;
	
	////2003/5/12 Added By KBH////
	m_nCurGroup = 0;
	m_CmdTarget.SetText(GetTargetModuleName());
	
	// StartMonitoring();		//현재 모듈 모니터링 시작 
}

CString CConnectionView::GetTargetModuleName()
{
	CString temp;
	CString strMDName = GetModuleName(m_nCurModuleID);	
	temp.Format("Stage No. %s", strMDName);	
	return temp;
}


void CConnectionView::StopMonitoring()
{
	if(m_nDisplayTimer == 0)	
	{
		return;
	}

	KillTimer(m_nDisplayTimer);
	m_nDisplayTimer = 0;
}

void CConnectionView::StartMonitoring()
{
	// 현재 모듈이 OffLine 상태이면 사용 안함
	if( m_nCurModuleID != 0 )
	{
		if(m_nDisplayTimer)	
		{
			return;
		}

		GroupSetChanged();		//현재 모듈의 Group Setting으로 바꾼 다음 	

		int nTimer = 2000;
		if(m_pConfig)
		{
			nTimer = m_pConfig->m_ChannelInterval*1000;
		}	
		m_nDisplayTimer = SetTimer(102, nTimer, NULL);
	}
}


void CConnectionView::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	if(nIDEvent == 102 )
	{	
		WORD gpState = EPGetGroupState( m_nCurModuleID );
		if( gpState == EP_STATE_END || gpState == EP_STATE_IDLE )
		{
			DrawConnectionCode();

			KillTimer(m_nDisplayTimer);
			m_nDisplayTimer = 0;			
		}
		else
		{
			DrawChannel();
		}
	}

	CFormView::OnTimer(nIDEvent);
}
