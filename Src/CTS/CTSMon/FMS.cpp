#include "stdafx.h"
#include "CTSMon.h"
#include "FMS.h"

CFMS::CFMS(void)
: m_totStep(0)
, m_pStep(NULL)
, m_WorkCode(0)
, m_resultfilePathNName("")
{
	ZeroMemory(&m_inreserv, sizeof(st_CHARGER_INRESEVE));
	ZeroMemory(&m_Packet, sizeof(st_FMS_PACKET));

	m_Mode = EQUIP_ST_OFF;
	m_FMSStateCode = FMS_ST_OFF;

	m_MisSendState = 0;
	m_SendMisIdx = 0;

	m_LastState = 0;

	m_pModule = NULL;

	m_Result_FIle_Type = '0';

	memset(&m_ErrorData, 0x20, sizeof(st_EQ_ERROR));
	
	m_bError = FALSE;

	LanguageinitMonConfig();
}

CFMS::~CFMS(void)
{
	for (UINT i = 0; i < m_vSendMis.size(); i++)
	{
		st_SEND_MIS_INFO* pMisInfo = m_vSendMis[i];
		delete pMisInfo;
	}
	m_vSendMis.clear();

	m_TestCondition.RemoveStep();
	m_TestCondition.ResetCondition();

	if(m_pModule)
		m_pModule->GetCondition()->ResetCondition();

	fnResetChannelCode();

	if(m_inreserv.Step_Info)
	{
		delete [] m_inreserv.Step_Info;
	}
	if(m_pStep)
	{
		delete [] m_pStep;
	}

	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

bool CFMS::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CFMS"), _T("TEXT_CFMS_CNT"), _T("TEXT_CFMS_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CFMS_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CFMS"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}

VOID CFMS::fnInit(CFormModule* _Module)
{
	m_pModule = _Module;	
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	INT idx = 0;
	INT lenidx = 0;

	memset(&m_Packet.head, 0x00, sizeof(st_FMSMSGHEAD));

	m_Packet.head.MsgSTR[0] = '@';//1

	idx++;
		
	memcpy(m_Packet.head.Line
		, AfxGetApp()->GetProfileString(FMS_REG, "Line", "")
		, g_iHead_Map[idx] < strlen(AfxGetApp()->GetProfileString(FMS_REG, "Line", ""))
		? g_iHead_Map[idx] : strlen(AfxGetApp()->GetProfileString(FMS_REG, "Line", "")));//4

	idx++;
	
	lenidx += g_iHead_Map[idx];
	memcpy(m_Packet.head.Sender
		, AfxGetApp()->GetProfileString(FMS_REG, "Sender", "")
		, g_iHead_Map[idx] < strlen(AfxGetApp()->GetProfileString(FMS_REG, "Sender", ""))
		? g_iHead_Map[idx] : strlen(AfxGetApp()->GetProfileString(FMS_REG, "Sender", "")));//8

	idx++;
	
	lenidx += g_iHead_Map[idx];
	memcpy(m_Packet.head.Addressee
		, AfxGetApp()->GetProfileString(FMS_REG, "Addressee", "")
		, g_iHead_Map[idx] < strlen(AfxGetApp()->GetProfileString(FMS_REG, "Addressee", ""))
		? g_iHead_Map[idx] : strlen(AfxGetApp()->GetProfileString(FMS_REG, "Addressee", "")));//8

	m_Packet.head.HeadEnd[0] = ':';
	
	//////////////////////////////////////////////////////////////////////////
	if( _Module == NULL )
	{
		return;
	}

	//////////////////////////////////////////////////////////////////////////
	//Get Data Folder
	m_strDataFolder = theApp.GetProfileString(FORM_PATH_REG_SECTION ,"Data");		
	if(m_strDataFolder.IsEmpty())
	{
		m_strDataFolder.Format("%s\\Data", theApp.m_strCurFolder);
		theApp.WriteProfileString(FORM_PATH_REG_SECTION, "Data", m_strDataFolder);
	}

	m_bFolderModuleName = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "ModuleFolder", FALSE);
	m_bFolderTime = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "TimeFolder", FALSE);
	m_nTimeFolderInterval = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "FolderTimeInterval", 30);
	m_bFolderLot = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "LotFolder", FALSE);
	m_bFolderTray = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "TrayFolder", FALSE);
	//////////////////////////////////////////////////////////////////////////
	fnLoadChannelCode();

	m_nModuleID = _Module->GetModuleID();

	CString strTemp;
	strTemp.Format("StageLastState_%02d", m_nModuleID);
	m_LastState = AfxGetApp()->GetProfileInt(FMS_REG, strTemp, 0);
	if(m_LastState == FMS_ST_ERROR)
	{
		m_bError = TRUE;
	}
}
VOID CFMS::fnLoadTypeSel()
{
	m_strTypeSel[0] = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "TypeSel1", "000");
	m_strTypeSel[1] = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "TypeSel2", "100");
	m_strTypeSel[2] = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "TypeSel3", "900");
	m_strTypeSel[3] = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "TypeSel4", "700");
}

BOOL CFMS::fnLoadChannelCode()
{
	CDaoDatabase  db;

	CString dbpath = theApp.GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");
	CString m_strDataBaseName = _T("");

	m_strDataBaseName = dbpath +"\\"+FORM_SET_DATABASE_NAME;
	
	try
	{
		db.Open(m_strDataBaseName);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	

	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		CString strSQL;
		
		if( g_nLanguage == LANGUAGE_ENG )
		{
			strSQL.Format("SELECT Code, Messge, Description FROM ChannelCode_ENG");
		}
		else if( g_nLanguage == LANGUAGE_CHI )
		{
			strSQL.Format("SELECT Code, Messge, Description FROM ChannelCode_CHI");
		}
		else
		{
			strSQL.Format("SELECT Code, Messge, Description FROM ChannelCode");
		}

		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	

	if(!rs.IsBOF() && !rs.IsEOF())
	{
		COleVariant data;
		st_FMS_ChannelCode *pCode;

		while(!rs.IsEOF())
		{
			data = rs.GetFieldValue(0);		//code
			INT code = data.lVal;

			data = rs.GetFieldValue(1);		//message
			if(VT_NULL != data.vt)
			{
				if(strlen((CHAR*)data.pbVal) == 2)
				{
					pCode = new st_FMS_ChannelCode;
					pCode->nCode = code;
					sprintf(pCode->szMessage, "%s", data.pbVal);

					m_vChannelCode.push_back(pCode);
				}
			}
			
			rs.MoveNext();
		}
	}
	rs.Close();
	db.Close();
	return TRUE;
}
CHAR* CFMS::fnGetChannelCode(INT _code)
{
	for (UINT i = 0; i < m_vChannelCode.size(); i++)
	{
		if(m_vChannelCode[i]->nCode == _code)
		{
			return m_vChannelCode[i]->szMessage;
		}
	}

	return "XX";
}
VOID CFMS::fnResetChannelCode()
{
	for (UINT i = 0; i < m_vChannelCode.size(); i++)
	{
		st_FMS_ChannelCode* pCode = m_vChannelCode[i];

		delete pCode;
	}

	m_vChannelCode.clear();
}

INT CFMS::strLinker(CHAR *msg, VOID * _dest, INT* mesmap)
{
	CHAR *stTemp = (CHAR *)_dest;

	UINT totlink = 0;
	UINT totidx = 0;
	UINT i = 0;
	while(mesmap[i])
	{
		INT iLink = mesmap[i++];
		memcpy(msg + totlink, stTemp + totidx, iLink); 

		totidx += iLink + 1;
		totlink += iLink;
	}

	return totlink;
}

INT CFMS::strCutter(CHAR *msg, VOID * _dest, INT* mesmap)
{
	CHAR *stTemp = (CHAR *)_dest;

	UINT totcut = 0;
	UINT totidx = 0;
	UINT szEndIdx = 0;
	UINT i = 0;
	while(mesmap[i])
	{
		INT iCut = mesmap[i++];
		memcpy(stTemp + totcut, msg + totidx, iCut);

		totidx += iCut;
		totcut += iCut;
		stTemp[totcut] = 0;
		totcut++;
	}

	return totidx;
}
FMS_ERRORCODE CFMS::fnMakeHead(st_FMSMSGHEAD& _head, CHAR* _cmd, UINT _len, CHAR _result)
{
	strcpy_s(_head.Command, _cmd);

	COleDateTime dateTime = COleDateTime::GetCurrentTime();
	sprintf(_head.SendTime, "%s", dateTime.Format("%Y%m%d%H%M%S"));

	sprintf(_head.DataLength, "%06d", _len);

	memset(_head.ResultcCode, 0x20, 5);
	_head.ResultcCode[3] = _result;

	_head.HeadEnd[0] = ':';

	return FMS_ER_NONE;
}

BOOL CFMS::fnSendResponse(CHAR* _cmd, CHAR _resultcode)
{
	fnMakeHead(m_Packet.head, _cmd
		, 0
		, _resultcode);
	memset(m_Packet.data, 0x20, sizeof(m_Packet.data));
	m_Packet.dataLen = 0;
	return theApp.m_FMS_SendQ.Push(m_Packet);	
}

FMS_ERRORCODE CFMS::fnReserveProc(INT64 _code)
{
	FMS_ERRORCODE _error = FMS_ER_NONE;
	m_WorkCode = _code;
	
	m_pModule->ResetGroupData();

	if(fnLoadWorkInfo(m_WorkCode) == FALSE)
	{
		return _error = ER_fnLoadWorkInfo;
	}
	if(fnTransFMSWorkInfoToCTSWorkInfo() == FALSE)
	{
		if(m_OverCCheck == TRUE)
		{
			m_OverCCheck = FALSE;
			return _error = ER_fnRecipeOverC_Error;  //20200411 엄륭 용량 상한 관련 에러 처리
		}
		else
		{
			return _error = ER_fnTransFMSWorkInfoToCTSWorkInfo;
		}
		return _error = ER_fnTransFMSWorkInfoToCTSWorkInfo;
	}
	if(fnSendConditionToModule() == FALSE)
	{
		return _error = ER_fnSendConditionToModule;
	}
	if(fnMakeTrayInfo() == FALSE)
	{
		return _error = ER_fnMakeTrayInfo;
	}
	
	return _error;
}

FMS_ERRORCODE CFMS::fnReStartSet(INT64 _code)
{
	FMS_ERRORCODE _error = FMS_ER_NONE;
	m_WorkCode = _code;
	if(fnLoadWorkInfo(m_WorkCode) == FALSE)
	{
		return _error = ER_fnLoadWorkInfo;
	}
	if(fnTransFMSWorkInfoToCTSWorkInfo() == FALSE)
	{
		return _error = ER_fnTransFMSWorkInfoToCTSWorkInfo;
	}	
	/* kky
	if(fnMakeTrayInfo(FALSE) == FALSE)
	{
		return _error = ER_fnMakeTrayInfo;
	}
	*/
	return _error;
}

BOOL CFMS::fnLoadWorkInfo(INT64 _code)
{
	if( m_pModule == NULL )
	{
		CFMSLog::WriteErrorLog("fnLoadWorkInfo : m_pModule == NULL");
	}

#ifdef _DCIR

	INT loadOK = 0;
	INT sqlerr = 0;

	CString strSqliteDBPathNName = theApp.GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");

	CString dbname;
	dbname.Format("\\FMS_DB\\CTSMonDB_%03d.db3", fnGetLineNo());
	strSqliteDBPathNName += dbname;

	sqlite3* mSqlite;

	sqlerr = sqlite3_open(strSqliteDBPathNName, &mSqlite);

	sqlite3_stmt * stmt = 0;

	CString strQuery;
	if(_code)
	{
		strQuery.Format("select idx\
						, moduleID\
						, F_Info \
						, StepInfo \
						, B_Info \
						from FMSWorkInfo where moduleID = %d AND idx = %I64d"
						, fnGetLineNo()
						, _code);
	}
	else
	{
		strQuery.Format("select MAX(idx)\
						, moduleID\
						, F_Info \
						, StepInfo \
						, B_Info \
						from FMSWorkInfo where moduleID = %d"
						, fnGetLineNo());
	}

	sqlerr = sqlite3_prepare( mSqlite,  strQuery, -1, &stmt, 0 );

	if(m_inreserv.Step_Info)
	{
		delete [] m_inreserv.Step_Info;
		m_inreserv.Step_Info = NULL;
	}

	while((sqlerr = sqlite3_step( stmt )) == SQLITE_ROW)
	{
		INT c = 0;
		m_WorkCode = sqlite3_column_int64(stmt, c++);
		UINT64 idx = m_WorkCode;
		INT year = idx / 10000000000;
		idx -= year * 10000000000;
		INT month = idx / 100000000;
		idx -= month * 100000000;
		INT day = idx / 1000000;
		idx -= day * 1000000;

		INT hour = idx / 10000;
		idx -= hour * 10000;
		INT mint = idx / 100;
		idx -= mint * 100;
		INT sec = idx;
		m_OleRunStartTime.SetDateTime(year
			, month
			, day
			, hour
			, mint
			, sec);

		INT id = sqlite3_column_int(stmt, c++);

		INT blobsize = 0;
		blobsize = sqlite3_column_bytes(stmt, c);
		if(blobsize == sizeof(st_DCIR_INRESEVE_F))
		{
			ZeroMemory(&m_inreserv.F_Value, sizeof(st_DCIR_INRESEVE_F));
			memcpy(&m_inreserv.F_Value, sqlite3_column_blob(stmt, c), sizeof(st_DCIR_INRESEVE_F));
		}
		else loadOK++;
		c++;

		if(m_inreserv.Step_Info)
		{
			delete [] m_inreserv.Step_Info;
			m_inreserv.Step_Info = NULL;
			loadOK++;
			break;
		}

		INT totstep = atoi(m_inreserv.F_Value.Total_Step);
		blobsize = sqlite3_column_bytes(stmt, c);
		if(blobsize == sizeof(st_DCIR_Step_Set) * totstep)
		{
			st_DCIR_Step_Set* pStepInfo = new st_DCIR_Step_Set[totstep];
			ZeroMemory(pStepInfo, sizeof(st_DCIR_Step_Set) * totstep);
			memcpy(pStepInfo, sqlite3_column_blob(stmt, c), sizeof(st_DCIR_Step_Set) * totstep);
			m_inreserv.Step_Info = pStepInfo;
		}
		else loadOK++;
		c++;

		blobsize = sqlite3_column_bytes(stmt, c);
		if(blobsize == sizeof(st_DCIR_INRESEVE_B))
		{
			ZeroMemory(&m_inreserv.B_Value, sizeof(st_DCIR_INRESEVE_B));
			memcpy(&m_inreserv.B_Value, sqlite3_column_blob(stmt, c), sizeof(st_DCIR_INRESEVE_B));
		}
		else loadOK++;
	}

	if(SQLITE_DONE != sqlerr)
	{
		LPSTR szError = (LPSTR)sqlite3_errmsg(mSqlite);
		CFMSLog::WriteLog(szError);

		int nRowsChanged = sqlite3_changes(mSqlite);

		sqlerr = sqlite3_reset(stmt);

		if (sqlerr != SQLITE_OK)
		{
			szError = (LPSTR)sqlite3_errmsg(mSqlite);
			CFMSLog::WriteLog(szError);
		}
		else loadOK++;
	}

	sqlite3_finalize(stmt);
	sqlite3_close(mSqlite);

	if(loadOK)
		return FALSE;
	else
		return TRUE;
	
#else

	INT loadOK = 0;
	INT sqlerr = 0;

	CString strSqliteDBPathNName = theApp.GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");

	CString dbname;
	dbname.Format("\\FMS_DB\\CTSMonDB_%03d.db3", fnGetLineNo());
	strSqliteDBPathNName += dbname;

	sqlite3* mSqlite;
	sqlerr = sqlite3_open(strSqliteDBPathNName, &mSqlite);
 
	sqlite3_stmt * stmt = 0;
	
	CString strQuery;
	if(_code)
	{
		strQuery.Format("select idx\
			  , moduleID\
			  , F_Info \
			  , StepInfo \
			  , B_Info \
			  from FMSWorkInfo where moduleID = %d AND idx = %I64d"
			  , fnGetLineNo()
			  , _code);
	}
	else
	{
		strQuery.Format("select MAX(idx)\
				   , moduleID\
				   , F_Info \
				   , StepInfo \
				   , B_Info \
				   , Result_File_Name \
				   , Send_Complet \
				   from FMSWorkInfo where moduleID = %d"
				    , fnGetLineNo());
	}
	
	sqlerr = sqlite3_prepare( mSqlite,  strQuery, -1, &stmt, 0 );

	if(m_inreserv.Step_Info)
	{
		delete [] m_inreserv.Step_Info;
		m_inreserv.Step_Info = NULL;
	}

	while((sqlerr = sqlite3_step( stmt )) == SQLITE_ROW)
	{
		INT c = 0;
		m_WorkCode = sqlite3_column_int64(stmt, c++);
		UINT64 idx = m_WorkCode;
		INT year = idx / 10000000000;
		idx -= year * 10000000000;
		INT month = idx / 100000000;
		idx -= month * 100000000;
		INT day = idx / 1000000;
		idx -= day * 1000000;

		INT hour = idx / 10000;
		idx -= hour * 10000;
		INT mint = idx / 100;
		idx -= mint * 100;
		INT sec = idx;
		m_OleRunStartTime.SetDateTime(year
			, month
			, day
			, hour
			, mint
			, sec);

		INT id = sqlite3_column_int(stmt, c++);

		INT blobsize = 0;
		blobsize = sqlite3_column_bytes(stmt, c);
		if(blobsize == sizeof(st_CHARGER_INRESEVE_F))
		{
			ZeroMemory(&m_inreserv.F_Value, sizeof(st_CHARGER_INRESEVE_F));
			memcpy(&m_inreserv.F_Value, sqlite3_column_blob(stmt, c), sizeof(st_CHARGER_INRESEVE_F));
		}
		else 
		{
			loadOK++;
		}
		
		c++;

		if(m_inreserv.Step_Info)
		{
			delete [] m_inreserv.Step_Info;
			m_inreserv.Step_Info = NULL;
			loadOK++;
			break;
		}

		INT totstep = atoi(m_inreserv.F_Value.Total_Step);
		blobsize = sqlite3_column_bytes(stmt, c);
		if(blobsize == sizeof(st_Step_Set) * totstep)
		{
			st_Step_Set* pStepInfo = new st_Step_Set[totstep];
			ZeroMemory(pStepInfo, sizeof(st_Step_Set) * totstep);
			memcpy(pStepInfo, sqlite3_column_blob(stmt, c), sizeof(st_Step_Set) * totstep);
			m_inreserv.Step_Info = pStepInfo;
		}
		else 
		{
			loadOK++;		
		}
		
		c++;

		blobsize = sqlite3_column_bytes(stmt, c);
		if(blobsize == sizeof(st_CHARGER_INRESEVE_B))
		{
			ZeroMemory(&m_inreserv.B_Value, sizeof(st_CHARGER_INRESEVE_B));
			memcpy(&m_inreserv.B_Value, sqlite3_column_blob(stmt, c), sizeof(st_CHARGER_INRESEVE_B));
		}
		else loadOK++;

		if(_code)
		{

		}
		else
		{
			c++;
			m_resultfilePathNName = (CHAR*)sqlite3_column_text(stmt, c++);
			m_PreState = sqlite3_column_int(stmt, c++);
		}
	}

	if(SQLITE_DONE != sqlerr)
	{
		LPSTR szError = (LPSTR)sqlite3_errmsg(mSqlite);
		CFMSLog::WriteLog(szError);

		int nRowsChanged = sqlite3_changes(mSqlite);

		sqlerr = sqlite3_reset(stmt);

		if (sqlerr != SQLITE_OK)
		{
			szError = (LPSTR)sqlite3_errmsg(mSqlite);
			CFMSLog::WriteLog(szError);
		}
		else loadOK++;
	}

	sqlite3_finalize(stmt);
	sqlite3_close(mSqlite);

	if(loadOK)
		return FALSE;
	else
		return TRUE;
#endif
}

BOOL CFMS::fnRangeCheck(STEP_VALUE_RANGE _range, CHAR* _szValue, FLOAT& _fValue, FMS_Step_Type fmsStepType)
{
	FLOAT _result = atof(_szValue);
	
	switch(_range)
	{
	case Range_S_I:		//반드시 사이값 입력
		{
			if(_MIN_CC_I <= _result && _MAX_CC_I >= _result)
			{			
				_fValue = _result;

				if( fmsStepType != 0 )
				{
					CFMSLog::WriteErrorLog("[Module %d] Step Value Error : Item %d, szValue %s, Value %f, Step %d", m_pModule->GetModuleID(),  _range, _szValue, _result, fmsStepType);
				}
				
				return FALSE;
			}
		}
		break;
	case Range_S_V:		//반드시 사이값 입력
		{
			if(_MIN_CV_V <= _result && _MAX_CV_V >= _result)
			{
				_fValue = _result;
				
				if( fmsStepType != 0 )
				{
					CFMSLog::WriteErrorLog("[Module %d] Step Value Error : Item %d, szValue %s, Value %f, Step %d", m_pModule->GetModuleID(),  _range, _szValue, _result, fmsStepType);
				}
				
				return FALSE;
			}
		}
		break;
	case Range_I:		//0입력 허용(선택입력)
		{
			if(_result == 0.0f || (_MIN_CC_I <= _result && _MAX_CC_I >= _result))
			{
				_fValue = _result;
				
				if( fmsStepType != 0 )
				{
					CFMSLog::WriteErrorLog("[Module %d] Step Value Error : Item %d, szValue %s, Value %f, Step %d", m_pModule->GetModuleID(),  _range, _szValue, _result, fmsStepType);
				}
				
				return FALSE;
			}
		}
		break;
	case Range_V:		//0입력 허용(선택입력)
		{
			if(0.0f == _result || (_MIN_CV_V <= _result && _MAX_CV_V >= _result))
			{
				_fValue = _result;
				
				if( fmsStepType != 0 )
				{
					CFMSLog::WriteErrorLog("[Module %d] Step Value Error : Item %d, szValue %s, Value %f, Step %d", m_pModule->GetModuleID(),  _range, _szValue, _result, fmsStepType);
				}
				
				return FALSE;
			}
		}
		break;
	case Range_SOC:		//0입력 허용(선택입력)
		{
			if(0 <= _result && 100 >= _result)
			{
				_fValue = _result;

				if( fmsStepType != 0 )
				{				
					CFMSLog::WriteErrorLog("[Module %d] Step Value Error : Item %d, szValue %s, Value %f, Step %d", m_pModule->GetModuleID(),  _range, _szValue, _result, fmsStepType);
				}
				
				return FALSE;
			}
		}
		break;
	case Range_S_Min_Time:	//반드시 0 이상값 입력
		{
			if(0 < _result)
			{
				_fValue = _result * 60.0f;
				
				if( fmsStepType != 0 )
				{
					CFMSLog::WriteErrorLog("[Module %d] Step Value Error : Item %d, szValue %s, Value %f, Step %d", m_pModule->GetModuleID(),  _range, _szValue, _result, fmsStepType);
				}
				
				return FALSE;
			}
		}
		break;
    case Range_Min_Time:	//0입력 허용(선택입력)
        {
            if(0 <= _result)
            {
                _fValue = _result * 60.0f;
                
				if( fmsStepType != 0 )
				{
					CFMSLog::WriteErrorLog("[Module %d] Step Value Error : Item %d, szValue %s, Value %f, Step %d", m_pModule->GetModuleID(),  _range, _szValue, _result, fmsStepType);
				}
                
                return FALSE;
            }
        }
        break;		
    case Range_S_Sec_Time:    //반드시 0 이상값 입력
        {
            if(0 < _result)
            {
                _fValue = _result;
                
				if( fmsStepType != 0 )
				{
					CFMSLog::WriteErrorLog("[Module %d] Step Value Error : Item %d, szValue %s, Value %f, Step %d", m_pModule->GetModuleID(),  _range, _szValue, _result, fmsStepType);
				}
                
                return FALSE;
            }
        }
        break;		
	case Range_Capa:		//0입력 허용(선택입력)
	case Range_Sec_Time:
		{
			if(0 <= _result)
			{
				_fValue = _result;
				
				if( fmsStepType != 0 )
				{
					CFMSLog::WriteErrorLog("[Module %d] Step Value Error : Item %d, szValue %s, Value %f, Step %d", m_pModule->GetModuleID(),  _range, _szValue, _result, fmsStepType);
				}
				
				return FALSE;
			}
		}
		break;
	}
	
	return TRUE;
}

// 1. 충전기 동작 Spec 범위 확인
BOOL CFMS::fnTransFMSWorkInfoToCTSWorkInfo()
{
	if( m_pModule == NULL )
	{
		CFMSLog::WriteErrorLog("fnTransFMSWorkInfoToCTSWorkInfo : m_pModule == NULL");
	}

#ifdef _CHARGER

	CString strtemp;

	char nMachineTraytype = 0;
	nMachineTraytype = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "MachineTraytype", 0);
	nMachineTraytype += 0x30;

	if( m_inreserv.F_Value.Tray_Type[0] != nMachineTraytype )
	{
		CFMSLog::WriteErrorLog("[Module %d] Tray_Type Code ERROR(0 - 1) : Recv[%c] Charger[%c]", m_pModule->GetModuleID(), m_inreserv.F_Value.Tray_Type[0], nMachineTraytype );
		return FALSE;
	}

//     if(m_inreserv.F_Value.Tray_Type[0] < 0x30 || m_inreserv.F_Value.Tray_Type[0] > 0x31 )
//     {
//         CFMSLog::WriteErrorLog("[Module %d] Tray_Type Code ERROR(0 - 1) : [%c]", m_pModule->GetModuleID(), m_inreserv.F_Value.Tray_Type[0] );
//         return FALSE;
//     }

	// 20201215 ksj
	CString strTypeSel;
	strTypeSel.Format("%s", m_inreserv.B_Value.Type_Sel);
	if( m_pModule->GetSettingTypeSel() == 1)	// 재화성 Disable 인데
	{
		if(strTypeSel == m_strTypeSel[3])		// 재화성 TypeSel이 들어올떄 
		{
			CFMSLog::WriteErrorLog("[Module %d] TypeSell Code ERROR(input : %s  / setting : %s)", m_pModule->GetModuleID(), m_inreserv.B_Value.Type_Sel, m_strTypeSel[3]);
			return FALSE;
		}
	}

	UINT InputCellCount = 0;
	INT i = 0;
	for (i = 0; i < MAX_CELL_COUNT; i++)
	{
		if(m_inreserv.B_Value.arCellInfo[i].Cell_Info[0] == '0')//OK
		{
		}
		else if(m_inreserv.B_Value.arCellInfo[i].Cell_Info[0] == '1')//이전 공정 불량	
		{
		}
		else if(m_inreserv.B_Value.arCellInfo[i].Cell_Info[0] == '2')//No Cell
		{
		}
		else if(m_inreserv.B_Value.arCellInfo[i].Cell_Info[0] == '3')//Skip
		{
		}
		else
		{
			CFMSLog::WriteErrorLog("[Module %d, Ch %d] CellInfo Error(0 - 3) : [%c]", m_pModule->GetModuleID(), i+1, m_inreserv.B_Value.arCellInfo[i].Cell_Info[0]);
		}
	}

	STR_CONDITION Condition;
	ZeroMemory(Condition.szTestSerialNo, EP_TEST_SERIAL_LENGTH+1);
	ZeroMemory(&Condition.modelHeader, sizeof(STR_CONDITION_HEADER));
	ZeroMemory(&Condition.conditionHeader, sizeof(STR_CONDITION_HEADER));
	ZeroMemory(&Condition.testHeader, sizeof(EP_TEST_HEADER));
	ZeroMemory(&Condition.checkParam, sizeof(STR_CHECK_PARAM));	

	Condition.conditionHeader.lID = atoi(m_inreserv.F_Value.Process_No);
	Condition.conditionHeader.lNo = atoi(m_inreserv.F_Value.Process_No);

	Condition.modelHeader.lID = atoi(m_inreserv.F_Value.Type);
	Condition.modelHeader.lNo = atoi(m_inreserv.F_Value.Type);
	
	strtemp = m_inreserv.F_Value.Batch_No;
	// 문자열 끝 널처리 전 뒤 쓰레기 제거용 [2013/9/24 cwm]
	strtemp = strtemp.Trim();
	strcpy_s(Condition.conditionHeader.szName, strtemp.GetBuffer());
	FLOAT ftemp = 0;
	Condition.checkParam.fOCVUpperValue = 0;
	Condition.checkParam.fOCVLowerValue = 0;
	Condition.checkParam.fVRef = 0;

	if(fnRangeCheck(Range_I
		, m_inreserv.B_Value.Contact.Charge_Cur
		, Condition.checkParam.fIRef))
		return FALSE;

	if(fnRangeCheck(Range_Sec_Time
		, m_inreserv.B_Value.Contact.Charge_Time
		, Condition.checkParam.fTime))
		return FALSE;

//20210303 ksj
	Condition.checkParam.fVRef = atof(m_inreserv.B_Value.Contact.Inverse_Vol);
	Condition.checkParam.fOCVUpperValue = atof(m_inreserv.B_Value.Contact.Upper_Vol_Check);
	Condition.checkParam.fOCVLowerValue = atof(m_inreserv.B_Value.Contact.Lower_Vol_Check);
	Condition.checkParam.fDeltaVoltage = atof(m_inreserv.B_Value.Contact.Upper_Cur_Check);
	Condition.checkParam.fMaxFaultBattery = atof(m_inreserv.B_Value.Contact.Lower_Cur_Check);
	Condition.checkParam.fDeltaVoltageLimit = atof(m_inreserv.B_Value.Contact.DeltaVLimit);

	if(Condition.checkParam.fDeltaVoltageLimit <= 0)
	{
		CFMSLog::WriteErrorLog("fDeltaVoltageLimit <= 0");
		return FALSE;
	}

	if(Condition.checkParam.fTime > 0)
	{
		Condition.checkParam.compFlag = 1;
	}

	Condition.checkParam.autoProcessingYN = 0;
	Condition.checkParam.reserved;

	st_Step_Set *pFMSStep = m_inreserv.Step_Info;

	INT totStep = atoi(m_inreserv.F_Value.Total_Step);

    if(totStep < 1 || totStep > 100)
    {
		CFMSLog::WriteErrorLog(TEXT_LANG[0].GetBuffer(TEXT_LANG[0].GetLength()), m_pModule->GetModuleID(), totStep);//"[Module %d] 적용할 수 없는 Step수입니다.(1-100) [%d]"
        return FALSE;
    }

	float fRecordTime = AfxGetApp()->GetProfileInt("FMS", "IMSRecordConditionTime", 1);
    
	for(i = 0; i < totStep; i++)
	{
		STR_COMMON_STEP _step;
		ZeroMemory(&_step, sizeof(STR_COMMON_STEP));

		_step.stepHeader.stepIndex = i;//atoi(pFMSStep[i].Step_ID) - 1;

		FMS_Step_Type fmsStepType = (FMS_Step_Type)atoi(pFMSStep[i].Step_Type);
		_step.fmsType = pFMSStep[i].Step_Type[0];
		//EP_TYPE_CHARGE
		//EP_MODE_CCCV

		_step.fRecDeltaTime = 10.0f;		//5sec

		switch(fmsStepType)
		{
		case FMS_CHG:
			{
				_step.stepHeader.type
					= EP_TYPE_CHARGE;
				_step.stepHeader.mode
					= EP_MODE_CCCV;

				if(fnRangeCheck(Range_S_V	//정전압 반드시 입력
					, pFMSStep[i].Volt
					, _step.fVref))
					return FALSE;

				if(fnRangeCheck(Range_S_I	//정전류 반드시 입력
					, pFMSStep[i].Current
					, _step.fIref))
					return FALSE;

				if(fnRangeCheck(Range_S_Min_Time	//종지 시간 반드시 입력
					, pFMSStep[i].Time
					, _step.fEndTime))
					return FALSE;

				if(fnRangeCheck(Range_S_I		//종지 전류 반드시 입력
					, pFMSStep[i].CutOff
					, _step.fEndI))
					return FALSE;
					
				fnRangeCheck(Range_SOC			//SOC 선택입력
					, pFMSStep[i].CutOff_SOC
					, _step.fSocRate);

				fnRangeCheck(Range_Capa			//CAPA 선택입력
					, pFMSStep[i].CutOff_A
					, _step.fEndC);

                fnRangeCheck(Range_Min_Time
					, pFMSStep[i].GetTime_Setting
					, _step.fRecVIGetTime); 

				fnRangeCheck(Range_Min_Time
					, pFMSStep[i].Step_LimitChkTime
					, _step.fCompTimeV[0]); 
					
				_step.fOverV = atof(m_inreserv.B_Value.Protect.Charge_H_Vol);
				_step.fLimitV = atof(m_inreserv.B_Value.Protect.Charge_L_Vol);
				_step.fOverI = atof(m_inreserv.B_Value.Protect.Charge_H_Cur);
				_step.fLimitI = atof(m_inreserv.B_Value.Protect.Charge_L_Cur);
				_step.fOverC = atof(m_inreserv.B_Value.Protect.Charge_H_Cap);
				if(_step.fOverC == 0) //20200411 엄륭 용량 상한 관련
				{
					m_OverCCheck = TRUE;
					return FALSE;
				}
				_step.fLimitC = atof(m_inreserv.B_Value.Protect.Charge_L_Cap);
															 
				if(_step.fCompTimeV[0] < 0.0f)
					_step.fCompTimeV[0] = 0.0f;
				else
					_step.fCompVLow[0] =  atof(m_inreserv.B_Value.Protect.Charge_L_Vol);
				
				fnRangeCheck(Range_Min_Time
					, m_inreserv.B_Value.Protect.Charge_Vol_Get_Time
					, _step.fCompTimeV[1]);
					
				_step.fCompVHigh[1] = atof(m_inreserv.B_Value.Protect.Charge_H_Time_Vol);
				_step.fCompVLow[1] = atof(m_inreserv.B_Value.Protect.Charge_L_Time_Vol);
				
				_step.fCompChgCcVtg = atof(m_inreserv.B_Value.Protect.Charge_Comp_CC_Vol);
				
				fnRangeCheck(Range_Sec_Time
					, m_inreserv.B_Value.Protect.Charge_Comp_CC_Time
					, _step.fCompChgCcTime );

				_step.fCompChgCcDeltaVtg = atof(m_inreserv.B_Value.Protect.Charge_Comp_CC_DeltaVol);

				_step.fCompChgCvCrt = atof(m_inreserv.B_Value.Protect.Charge_Comp_CV_Cur);
				fnRangeCheck(Range_Sec_Time
					, m_inreserv.B_Value.Protect.Charge_Comp_CV_Time
					, _step.fCompChgCvTime );
				_step.fCompChgCvDeltaCrt = atof(m_inreserv.B_Value.Protect.Charge_Comp_Cv_DeltaCur);

				// _step.fRecDeltaTime = 1.0f;		//5sec
				_step.fRecDeltaTime = fRecordTime;
			}
			break;

		case FMS_DCHG:
			{
				_step.stepHeader.type
					= EP_TYPE_DISCHARGE;
				_step.stepHeader.mode
					= EP_MODE_CC;

				if(fnRangeCheck(Range_S_I		//정전류 Ref 반드시 입력
					, pFMSStep[i].Current
					, _step.fIref))
					return FALSE;

				if(fnRangeCheck(Range_V			//정전압 Ref 선택 입력 (미입력시 SBC default 값 적용)
					, pFMSStep[i].Volt
					, _step.fVref))
					return FALSE;

				if(fnRangeCheck(Range_S_Min_Time	//End Time 필수입력
					, pFMSStep[i].Time
					, _step.fEndTime))
					return FALSE;

				if(fnRangeCheck(Range_S_V		//종지 전압 필수입력
					, pFMSStep[i].CutOff
					, _step.fEndV))
					return FALSE;

				if(fnRangeCheck(Range_SOC		//Soc 선택입력
					, pFMSStep[i].CutOff_SOC
					, _step.fSocRate))
					return FALSE;

				if(fnRangeCheck(Range_Capa		//Capa 선택입력
					, pFMSStep[i].CutOff_A
					, _step.fEndC))
					return FALSE;

				if(_step.fVref <= 0.0f)
					_step.fVref = _step.fEndV - 50.0f;	//default Ref 
				if(_step.fVref <= 0.0f)
					_step.fVref = 0.0f;					//default Ref

                fnRangeCheck(Range_Min_Time, pFMSStep[i].GetTime_Setting, _step.fRecVIGetTime); 
				fnRangeCheck(Range_Min_Time, pFMSStep[i].Step_LimitChkTime, _step.fCompTimeV[0]); 

				_step.fOverV = atof(m_inreserv.B_Value.Protect.DisCharge_H_Vol);
				_step.fLimitV = atof(m_inreserv.B_Value.Protect.DisCharge_L_Vol);
				_step.fOverI = atof(m_inreserv.B_Value.Protect.DisCharge_H_Cur);
				_step.fLimitI = atof(m_inreserv.B_Value.Protect.DisCharge_L_Cur);
				_step.fOverC = atof(m_inreserv.B_Value.Protect.DisCharge_H_Cap);
				if(_step.fOverC == 0) //20200411 엄륭 용량 상한 관련
				{
					m_OverCCheck = TRUE;
					return FALSE;
				}

				_step.fLimitC = atof(m_inreserv.B_Value.Protect.DisCharge_L_Cap);

				// _step.fRecDeltaTime = 1.0f;		//5sec
				_step.fRecDeltaTime = fRecordTime;
			}
			break;
		case FMS_REST:
			{
				_step.stepHeader.type
					= EP_TYPE_REST;
				_step.stepHeader.mode
					= 0;

				_step.fRecDeltaTime = 10.0f;

				if(fnRangeCheck(Range_S_Min_Time		//필수 입력
					, pFMSStep[i].Time
					, _step.fEndTime))
					return FALSE;

                fnRangeCheck(Range_Min_Time, pFMSStep[i].GetTime_Setting, _step.fRecVIGetTime); 
			}
			break;
		case FMS_OCV:
			{
				if( i==0 )
				{
					_step.fOverV = atof(m_inreserv.B_Value.Protect.OCV_H_Vol);
					_step.fLimitV = atof(m_inreserv.B_Value.Protect.OCV_L_Vol);
				}

				_step.stepHeader.type
					= EP_TYPE_OCV;
				_step.stepHeader.mode
					= 0;

				_step.fRecDeltaTime = 10.0f;
			}
			break;
		case FMS_CC_CHARGE_FANOFF:
			{
				// 팬 동작을 멈추는 스텝
				_step.m_nFanOffFlag = 1;
			}
		case FMS_CC_CHARGE:		
		case FMS_OG_CHARGE:
			{
				_step.stepHeader.type
					= EP_TYPE_CHARGE;
				_step.stepHeader.mode
					= EP_MODE_CC;

				if(fnRangeCheck(Range_S_I		//CC 전류 필수
					, pFMSStep[i].Current
					, _step.fIref))
					return FALSE;

				if(fnRangeCheck(Range_V			//CV 전압 선택 입력(미입력시 SBC Default)
					, pFMSStep[i].Volt
					, _step.fVref))
					return FALSE;

				if(fnRangeCheck(Range_S_V		//종지 전압 필수입력
					, pFMSStep[i].CutOff
					, _step.fEndV))
					return FALSE;


				if(fnRangeCheck(Range_S_Min_Time	//종료 시간 필수
					, pFMSStep[i].Time
					, _step.fEndTime))
					return FALSE;


				if(fnRangeCheck(Range_SOC		//Soc 선택입력
					, pFMSStep[i].CutOff_SOC
					, _step.fSocRate))
					return FALSE;

				if(fnRangeCheck(Range_Capa		//Capa 선택입력
					, pFMSStep[i].CutOff_A
					, _step.fEndC))
					return FALSE;

				if(_step.fVref <= 0.0f)
					_step.fVref = _step.fEndV + 50.0f;	//default Ref

				if(_step.fVref >= _OVP_V)
					_step.fVref = _OVP_V-10.0f;				//default Ref 

				fnRangeCheck(Range_Min_Time, pFMSStep[i].GetTime_Setting, _step.fRecVIGetTime); 
				fnRangeCheck(Range_Min_Time, pFMSStep[i].Step_LimitChkTime, _step.fCompTimeV[0]);

				_step.fOverV = atof(m_inreserv.B_Value.Protect.Charge_H_Vol);
				_step.fLimitV = atof(m_inreserv.B_Value.Protect.Charge_L_Vol);		//사용 용도가 다름 => 20131211 충전하한전압
				_step.fOverI = atof(m_inreserv.B_Value.Protect.Charge_H_Cur);
				_step.fLimitI = atof(m_inreserv.B_Value.Protect.Charge_L_Cur);
				_step.fOverC = atof(m_inreserv.B_Value.Protect.Charge_H_Cap);
				if(_step.fOverC == 0) //20200411 엄륭 용량 상한 관련
				{
					m_OverCCheck = TRUE;
					return FALSE;
				}
				_step.fLimitC = atof(m_inreserv.B_Value.Protect.Charge_L_Cap);

				/*
                fnRangeCheck(Range_Min_Time, m_inreserv.B_Value.Protect.Charge_L_Vol_Check_Time, _step.fCompTimeV[0]); 
				if(_step.fCompTimeV[0] < 0.0f)
					_step.fCompTimeV[0] = 0.0f;
				else
					_step.fCompVLow[0] =  atof(m_inreserv.B_Value.Protect.Charge_L_Vol);
					*/
					
				// 1. 충전 취득 전압 관련 추가 20131210
				fnRangeCheck(Range_Min_Time
					, m_inreserv.B_Value.Protect.Charge_Vol_Get_Time
					, _step.fCompTimeV[1]);

				_step.fCompVHigh[1] = atof(m_inreserv.B_Value.Protect.Charge_H_Time_Vol);
				_step.fCompVLow[1] = atof(m_inreserv.B_Value.Protect.Charge_L_Time_Vol);

				_step.fCompChgCcVtg = atof(m_inreserv.B_Value.Protect.Charge_Comp_CC_Vol);

				fnRangeCheck(Range_Sec_Time
					, m_inreserv.B_Value.Protect.Charge_Comp_CC_Time
					, _step.fCompChgCcTime );

				_step.fCompChgCcDeltaVtg = atof(m_inreserv.B_Value.Protect.Charge_Comp_CC_DeltaVol);

 				_step.fCompChgCvCrt = atof(m_inreserv.B_Value.Protect.Charge_Comp_CV_Cur);
 				fnRangeCheck(Range_Sec_Time
 					, m_inreserv.B_Value.Protect.Charge_Comp_CV_Time
 					, _step.fCompChgCvTime );
 				_step.fCompChgCvDeltaCrt = atof(m_inreserv.B_Value.Protect.Charge_Comp_Cv_DeltaCur);

				// _step.fRecDeltaTime = 1.0f;		//5sec
				_step.fRecDeltaTime = fRecordTime;
			}
			break;
		case FMS_DCIR:
				_step.stepHeader.type
					= EP_TYPE_IMPEDANCE;
				_step.stepHeader.mode
					= EP_MODE_DC_IMP;

				if(fnRangeCheck(Range_S_I		//정전류 Ref 반드시 입력
					, pFMSStep[i].Current
					, _step.fIref))
					return FALSE;

				if(fnRangeCheck(Range_V			//정전압 Ref 선택 입력 (미입력시 SBC default 값 적용)
					, pFMSStep[i].Volt
					, _step.fVref))
					return FALSE;

				if(fnRangeCheck(Range_S_Min_Time	//End Time 필수입력
					, pFMSStep[i].Time
					, _step.fEndTime))
					return FALSE;

				if(fnRangeCheck(Range_V		//종지 전압 선택입력
					, pFMSStep[i].CutOff
					, _step.fEndV))
					return FALSE;


				//default V Ref.
				if(_step.fEndV <= 0.0f)		//endV 미설정시
				{
					_step.fVref = _MIN_CV_V - 50.0f;
				}
				else						//End V 설정시
				{
					_step.fVref = _step.fEndV-50.0f;	//default Ref 
					if(_step.fVref <= 0.0f)
						_step.fVref = 0.0f;				//default Ref 
				}
				
                fnRangeCheck(Range_Min_Time, pFMSStep[i].GetTime_Setting, _step.fRecVIGetTime); 

				_step.fOverV = atof(m_inreserv.B_Value.Protect.DisCharge_H_Vol);
				_step.fLimitV = atof(m_inreserv.B_Value.Protect.DisCharge_L_Vol);
				_step.fOverI = atof(m_inreserv.B_Value.Protect.DisCharge_H_Cur);
				_step.fLimitI = atof(m_inreserv.B_Value.Protect.DisCharge_L_Cur);
				_step.fOverC = atof(m_inreserv.B_Value.Protect.DisCharge_H_Cap);
				_step.fLimitC = atof(m_inreserv.B_Value.Protect.DisCharge_L_Cap);

				_step.fRecDeltaTime = 1.0f;		//5sec
			break;
		}
		
		_step.stepHeader.gradeSize = 0;
		_step.stepHeader.nProcType;

		_step.fEndDV;
		_step.fEndDI;

		_step.bUseActucalCap;
		_step.bReserved;

		_step.fSocRate;

		_step.fOverV;
		_step.fLimitV;
		_step.fOverI;
		_step.fLimitI;
		_step.fOverC;
		_step.fLimitC;
		_step.fOverImp;
		_step.fLimitImp;

		//_step.fCompTimeV;
		//_step.fCompVLow;
		//_step.fCompVHigh;

		//_step.fCompTimeI;
		//_step.fCompILow;
		//_step.fCompIHigh;

		_step.fDeltaTimeV;
		_step.fDeltaV;
		_step.fDeltaTimeI;
		_step.fDeltaI;

		_step.grade;

		// _step.fRecDeltaTime = 5.0f;		//5sec
		_step.fRecDeltaV;
		_step.fRecDeltaI;
		_step.fRecDeltaT;

		_step.fParam1;
		_step.fParam2;

		_step.UseStepContinue;
		//_step.fmsType;
		_step.Reserved2;

		_step.fReserved;

		STR_COMMON_STEP* pStep = new STR_COMMON_STEP;
		CopyMemory(pStep, &_step, sizeof(STR_COMMON_STEP));
		Condition.apStepList.Add(pStep);
	}

	//end step 추가
	STR_COMMON_STEP* pstepEnd = new STR_COMMON_STEP;
	ZeroMemory(pstepEnd, sizeof(STR_COMMON_STEP));
	pstepEnd->stepHeader.type = EP_TYPE_END;//nType;
	pstepEnd->stepHeader.stepIndex = i;//stepIndex; 0x00
	pstepEnd->stepHeader.mode = 0; //StepMode 1 : CC-CV, 2: CC, 3: CV, 4: OCV

	Condition.apStepList.Add(pstepEnd);

	Condition.testHeader.totalStep = totStep + 1;

	m_TestCondition.RemoveStep();
	m_TestCondition.ResetCondition();
	if(m_TestCondition.SetProcedure(&Condition) == FALSE)
	{
		return FALSE;
	}

	m_pModule->GetCondition()->ResetCondition();
	m_pModule->GetCondition()->SetTestConditon(&m_TestCondition);

	//if(fnStepValidityCheck() == 0)
	//{
	//	TRACE("****** Condition 정상 ");
	//}

	//return fnStepValidityCheck() < 0 ? FALSE : TRUE;
	return TRUE;

#elif _PRECHARGER

	CString strtemp;

	char nMachineTraytype = 0;
	nMachineTraytype = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "MachineTraytype", 0);
	nMachineTraytype += 0x30;

	if( m_inreserv.F_Value.Tray_Type[0] != nMachineTraytype )
	{
		CFMSLog::WriteErrorLog("[Module %d] Tray_Type Code ERROR(0 - 1) : Recv[%c] Charger[%c]", m_pModule->GetModuleID(), m_inreserv.F_Value.Tray_Type[0], nMachineTraytype );
		return FALSE;
	}
	/*
	if(m_inreserv.F_Value.Tray_Type[0] < 0x30 || m_inreserv.F_Value.Tray_Type[0] > 0x31 )
    {
        CFMSLog::WriteErrorLog("[Module %d] Tray_Type Code ERROR(0 - 1) : [%c]", m_pModule->GetModuleID(), m_inreserv.F_Value.Tray_Type[0] );
        return FALSE;
    }
	*/

	UINT InputCellCount = 0;
	INT i = 0;
	for (i = 0; i < MAX_CELL_COUNT; i++)
	{
		if(m_inreserv.B_Value.arCellInfo[i].Cell_Info[0] == '0')//OK
		{
		}
		else if(m_inreserv.B_Value.arCellInfo[i].Cell_Info[0] == '1')//이전 공정 불량	
		{
		}
		else if(m_inreserv.B_Value.arCellInfo[i].Cell_Info[0] == '2')//No Cell
		{
		}
		else if(m_inreserv.B_Value.arCellInfo[i].Cell_Info[0] == '3')//Skip
		{
		}
		else
		{
			CFMSLog::WriteErrorLog("[Module %d, Ch %d] CellInfo Error(0 - 3) : [%c]", m_pModule->GetModuleID(), i+1, m_inreserv.B_Value.arCellInfo[i].Cell_Info[0]);
		}
	}

	STR_CONDITION Condition;
	ZeroMemory(Condition.szTestSerialNo, EP_TEST_SERIAL_LENGTH+1);
	ZeroMemory(&Condition.modelHeader, sizeof(STR_CONDITION_HEADER));
	ZeroMemory(&Condition.conditionHeader, sizeof(STR_CONDITION_HEADER));
	ZeroMemory(&Condition.testHeader, sizeof(EP_TEST_HEADER));
	ZeroMemory(&Condition.checkParam, sizeof(STR_CHECK_PARAM));	

	Condition.conditionHeader.lID = atoi(m_inreserv.F_Value.Process_No);
	Condition.conditionHeader.lNo = atoi(m_inreserv.F_Value.Process_No);

	Condition.modelHeader.lID = atoi(m_inreserv.F_Value.Type);
	Condition.modelHeader.lNo = atoi(m_inreserv.F_Value.Type);
	
	strtemp = m_inreserv.F_Value.Batch_No;	
	strtemp = strtemp.Trim();
	strcpy_s(Condition.conditionHeader.szName, strtemp.GetBuffer());
	FLOAT ftemp = 0;
	Condition.checkParam.fOCVUpperValue = 0;
	Condition.checkParam.fOCVLowerValue = 0;
	Condition.checkParam.fVRef = 0;

	if(fnRangeCheck(Range_I
		, m_inreserv.B_Value.Contact.Charge_Cur
		, Condition.checkParam.fIRef))
		return FALSE;

	if(fnRangeCheck(Range_Sec_Time
		, m_inreserv.B_Value.Contact.Charge_Time
		, Condition.checkParam.fTime))
		return FALSE;


	Condition.checkParam.fDeltaVoltage = atof(m_inreserv.B_Value.Contact.Contact_Reg);
	Condition.checkParam.fMaxFaultBattery = atof(m_inreserv.B_Value.Contact.Inverse_Vol);

	Condition.checkParam.fOCVUpperValue = atof(m_inreserv.B_Value.Contact.Upper_Vol_Check);
	Condition.checkParam.fOCVLowerValue = atof(m_inreserv.B_Value.Contact.Lower_Vol_Check);

	if(Condition.checkParam.fTime > 0)
	{
		Condition.checkParam.compFlag = 1;
	}

	Condition.checkParam.autoProcessingYN = 0;
	Condition.checkParam.reserved;

	st_Step_Set *pFMSStep = m_inreserv.Step_Info;

	INT totStep = atoi(m_inreserv.F_Value.Total_Step);

    if(totStep < 1 || totStep > 100)
    {
		CFMSLog::WriteErrorLog(TEXT_LANG[0].GetBuffer(TEXT_LANG[0].GetLength()), m_pModule->GetModuleID(), totStep);//"[Module %d] 적용할 수 없는 Step수입니다.(1-100) [%d]"
        return FALSE;
    }

	float fRecordTime = AfxGetApp()->GetProfileInt("FMS", "IMSRecordConditionTime", 1);
    
	for(i = 0; i < totStep; i++)
	{
		STR_COMMON_STEP _step;
		ZeroMemory(&_step, sizeof(STR_COMMON_STEP));

		_step.stepHeader.stepIndex = i;//atoi(pFMSStep[i].Step_ID) - 1;

		FMS_Step_Type fmsStepType = (FMS_Step_Type)atoi(pFMSStep[i].Step_Type);
		_step.fmsType = pFMSStep[i].Step_Type[0];
		//EP_TYPE_CHARGE
		//EP_MODE_CCCV

		_step.fRecDeltaTime = 10.0f;		//5sec

		switch(fmsStepType)
		{
		case FMS_CHG:
			{
				_step.stepHeader.type
					= EP_TYPE_CHARGE;
				_step.stepHeader.mode
					= EP_MODE_CCCV;

				if(fnRangeCheck(Range_S_V	//정전압 반드시 입력
					, pFMSStep[i].Volt
					, _step.fVref))
					return FALSE;

				if(fnRangeCheck(Range_S_I	//정전류 반드시 입력
					, pFMSStep[i].Current
					, _step.fIref))
					return FALSE;

				if(fnRangeCheck(Range_S_Min_Time	//종지 시간 반드시 입력
					, pFMSStep[i].Time
					, _step.fEndTime))
					return FALSE;

				if(fnRangeCheck(Range_S_I		//종지 전류 반드시 입력
					, pFMSStep[i].CutOff
					, _step.fEndI))
					return FALSE;
					
				fnRangeCheck(Range_SOC			//SOC 선택입력
					, pFMSStep[i].CutOff_SOC
					, _step.fSocRate);

				fnRangeCheck(Range_Capa			//CAPA 선택입력
					, pFMSStep[i].CutOff_A
					, _step.fEndC);

				fnRangeCheck(Range_Min_Time, pFMSStep[i].GetTime_Setting, _step.fRecVIGetTime); 
				fnRangeCheck(Range_Min_Time, pFMSStep[i].Step_LimitChkTime, _step.fCompTimeV[0]);  
					
				_step.fOverV = atof(m_inreserv.B_Value.Protect.Charge_H_Vol);
				_step.fLimitV = atof(m_inreserv.B_Value.Protect.Charge_L_Vol);
				_step.fOverI = atof(m_inreserv.B_Value.Protect.Charge_H_Cur);
				_step.fLimitI = atof(m_inreserv.B_Value.Protect.Charge_L_Cur);
				_step.fOverC = atof(m_inreserv.B_Value.Protect.Charge_H_Cap);
				_step.fLimitC = atof(m_inreserv.B_Value.Protect.Charge_L_Cap);
								
				if(_step.fCompTimeV[0] < 0.0f)
					_step.fCompTimeV[0] = 0.0f;
				else
					_step.fCompVLow[0] =  atof(m_inreserv.B_Value.Protect.Charge_L_Vol);
				
				// 1. 충전 취득 전압 관련 추가 20131210
				fnRangeCheck(Range_Min_Time
					, m_inreserv.B_Value.Protect.Charge_Vol_Get_Time
					, _step.fCompTimeV[1]);
					
				_step.fCompVHigh[1] = atof(m_inreserv.B_Value.Protect.Charge_H_Time_Vol);
				_step.fCompVLow[1] = atof(m_inreserv.B_Value.Protect.Charge_L_Time_Vol);

				_step.fCompChgCcVtg = atof(m_inreserv.B_Value.Protect.Charge_Comp_CC_Vol);
				
				fnRangeCheck(Range_Sec_Time
					, m_inreserv.B_Value.Protect.Charge_Comp_CC_Time
					, _step.fCompChgCcTime );

				_step.fCompChgCcDeltaVtg = atof(m_inreserv.B_Value.Protect.Charge_Comp_CC_DeltaVol);

				_step.fCompChgCvCrt = atof(m_inreserv.B_Value.Protect.Charge_Comp_CV_Cur);
				fnRangeCheck(Range_Sec_Time
					, m_inreserv.B_Value.Protect.Charge_Comp_CV_Time
					, _step.fCompChgCvTime );
				_step.fCompChgCvDeltaCrt = atof(m_inreserv.B_Value.Protect.Charge_Comp_Cv_DeltaCur);

				// _step.fRecDeltaTime = 1.0f;		//5sec
				_step.fRecDeltaTime = fRecordTime;
			}
			break;
		case FMS_DCHG:
			{
				CFMSLog::WriteErrorLog("[Module %d] Pre-Charge could not supported DCHG step.", m_pModule->GetModuleID() );
				return FALSE;				
			}
			break;
		case FMS_REST:
			{
				_step.stepHeader.type
					= EP_TYPE_REST;
				_step.stepHeader.mode
					= 0;

				if(fnRangeCheck(Range_S_Min_Time		//필수 입력
					, pFMSStep[i].Time
					, _step.fEndTime))
					return FALSE;

				_step.fRecDeltaTime = 10.0f;

                fnRangeCheck(Range_Min_Time, pFMSStep[i].GetTime_Setting, _step.fRecVIGetTime); 
			}
			break;
		case FMS_OCV:
			{
				if( i==0 )
				{
					_step.fOverV = atof(m_inreserv.B_Value.Protect.OCV_H_Vol);
					_step.fLimitV = atof(m_inreserv.B_Value.Protect.OCV_L_Vol);
				}

				_step.fRecDeltaTime = 10.0f;

				_step.stepHeader.type
					= EP_TYPE_OCV;
				_step.stepHeader.mode
					= 0;
			}
			break;
		case FMS_CC_CHARGE_FANOFF:
			{
				// 팬 동작을 멈추는 스텝
				_step.m_nFanOffFlag = 1;
			}
		case FMS_CC_CHARGE:		
		case FMS_OG_CHARGE:
			{
				_step.stepHeader.type
					= EP_TYPE_CHARGE;
				_step.stepHeader.mode
					= EP_MODE_CC;

				if(fnRangeCheck(Range_S_I		//CC 전류 필수
					, pFMSStep[i].Current
					, _step.fIref))
					return FALSE;

				if(fnRangeCheck(Range_V			//CV 전압 선택 입력(미입력시 SBC Default)
					, pFMSStep[i].Volt
					, _step.fVref))
					return FALSE;

				if(fnRangeCheck(Range_S_V		//종지 전압 필수입력
					, pFMSStep[i].CutOff
					, _step.fEndV))
					return FALSE;


				if(fnRangeCheck(Range_S_Min_Time	//종료 시간 필수
					, pFMSStep[i].Time
					, _step.fEndTime))
					return FALSE;


				if(fnRangeCheck(Range_SOC		//Soc 선택입력
					, pFMSStep[i].CutOff_SOC
					, _step.fSocRate))
					return FALSE;

				if(fnRangeCheck(Range_Capa		//Capa 선택입력
					, pFMSStep[i].CutOff_A
					, _step.fEndC))
					return FALSE;

				if(_step.fVref <= 0.0f)
					_step.fVref = _step.fEndV + 50.0f;	//default Ref 
				if(_step.fVref >= _OVP_V)
					_step.fVref = _OVP_V-10.0f;				//default Ref 

				fnRangeCheck(Range_Min_Time, pFMSStep[i].GetTime_Setting, _step.fRecVIGetTime); 
				fnRangeCheck(Range_Min_Time, pFMSStep[i].Step_LimitChkTime, _step.fCompTimeV[0]); 

				_step.fOverV = atof(m_inreserv.B_Value.Protect.Charge_H_Vol);
				_step.fLimitV = atof(m_inreserv.B_Value.Protect.Charge_L_Vol);		//사용 용도가 다름 => 20131211 충전하한전압
				_step.fOverI = atof(m_inreserv.B_Value.Protect.Charge_H_Cur);
				_step.fLimitI = atof(m_inreserv.B_Value.Protect.Charge_L_Cur);
				_step.fOverC = atof(m_inreserv.B_Value.Protect.Charge_H_Cap);
				_step.fLimitC = atof(m_inreserv.B_Value.Protect.Charge_L_Cap);

				// 1. 충전 취득 전압 관련 추가 20131210
				fnRangeCheck(Range_Min_Time
					, m_inreserv.B_Value.Protect.Charge_Vol_Get_Time
					, _step.fCompTimeV[1]);

				_step.fCompVHigh[1] = atof(m_inreserv.B_Value.Protect.Charge_H_Time_Vol);
				_step.fCompVLow[1] = atof(m_inreserv.B_Value.Protect.Charge_L_Time_Vol);

				_step.fCompChgCcVtg = atof(m_inreserv.B_Value.Protect.Charge_Comp_CC_Vol);

				fnRangeCheck(Range_Sec_Time
					, m_inreserv.B_Value.Protect.Charge_Comp_CC_Time
					, _step.fCompChgCcTime );

				_step.fCompChgCcDeltaVtg = atof(m_inreserv.B_Value.Protect.Charge_Comp_CC_DeltaVol);

				_step.fCompChgCvCrt = atof(m_inreserv.B_Value.Protect.Charge_Comp_CV_Cur);
				fnRangeCheck(Range_Sec_Time
					, m_inreserv.B_Value.Protect.Charge_Comp_CV_Time
					, _step.fCompChgCvTime );
				_step.fCompChgCvDeltaCrt = atof(m_inreserv.B_Value.Protect.Charge_Comp_Cv_DeltaCur);

				// _step.fRecDeltaTime = 1.0f;		//5sec
				_step.fRecDeltaTime = fRecordTime;

			}
			break;

		case FMS_DCIR:
				_step.stepHeader.type
					= EP_TYPE_IMPEDANCE;
				_step.stepHeader.mode
					= EP_MODE_DC_IMP;

				if(fnRangeCheck(Range_S_I		//정전류 Ref 반드시 입력
					, pFMSStep[i].Current
					, _step.fIref))
					return FALSE;

				if(fnRangeCheck(Range_V			//정전압 Ref 선택 입력 (미입력시 SBC default 값 적용)
					, pFMSStep[i].Volt
					, _step.fVref))
					return FALSE;

				if(fnRangeCheck(Range_S_Min_Time	//End Time 필수입력
					, pFMSStep[i].Time
					, _step.fEndTime))
					return FALSE;

				if(fnRangeCheck(Range_V		//종지 전압 선택입력
					, pFMSStep[i].CutOff
					, _step.fEndV))
					return FALSE;


				//default V Ref.
				if(_step.fEndV <= 0.0f)		//endV 미설정시
				{
					_step.fVref = _MIN_CV_V - 50.0f;
				}
				else						//End V 설정시
				{
					_step.fVref = _step.fEndV-50.0f;	//default Ref 
					if(_step.fVref <= 0.0f)
						_step.fVref = 0.0f;				//default Ref 
				}
				
                fnRangeCheck(Range_Min_Time, pFMSStep[i].GetTime_Setting, _step.fRecVIGetTime); 

				_step.fOverV = atof(m_inreserv.B_Value.Protect.DisCharge_H_Vol);
				_step.fLimitV = atof(m_inreserv.B_Value.Protect.DisCharge_L_Vol);
				_step.fOverI = atof(m_inreserv.B_Value.Protect.DisCharge_H_Cur);
				_step.fLimitI = atof(m_inreserv.B_Value.Protect.DisCharge_L_Cur);
				_step.fOverC = atof(m_inreserv.B_Value.Protect.DisCharge_H_Cap);
				_step.fLimitC = atof(m_inreserv.B_Value.Protect.DisCharge_L_Cap);

				_step.fRecDeltaTime = 1.0f;		//5sec
			break;
		}
		
		_step.stepHeader.gradeSize = 0;
		_step.stepHeader.nProcType;

		_step.fEndDV;
		_step.fEndDI;

		_step.bUseActucalCap;
		_step.bReserved;

		_step.fSocRate;

		_step.fOverV;
		_step.fLimitV;
		_step.fOverI;
		_step.fLimitI;
		_step.fOverC;
		_step.fLimitC;
		_step.fOverImp;
		_step.fLimitImp;

		//_step.fCompTimeV;
		//_step.fCompVLow;
		//_step.fCompVHigh;

		//_step.fCompTimeI;
		//_step.fCompILow;
		//_step.fCompIHigh;

		_step.fDeltaTimeV;
		_step.fDeltaV;
		_step.fDeltaTimeI;
		_step.fDeltaI;

		_step.grade;

		// _step.fRecDeltaTime = 5.0f;		//5sec
		_step.fRecDeltaV;
		_step.fRecDeltaI;
		_step.fRecDeltaT;

		_step.fParam1;
		_step.fParam2;

		_step.UseStepContinue;
		//_step.fmsType;
		_step.Reserved2;

		_step.fReserved;

		STR_COMMON_STEP* pStep = new STR_COMMON_STEP;
		CopyMemory(pStep, &_step, sizeof(STR_COMMON_STEP));
		Condition.apStepList.Add(pStep);
	}

	//end step 추가
	STR_COMMON_STEP* pstepEnd = new STR_COMMON_STEP;
	ZeroMemory(pstepEnd, sizeof(STR_COMMON_STEP));
	pstepEnd->stepHeader.type = EP_TYPE_END;//nType;
	pstepEnd->stepHeader.stepIndex = i;//stepIndex; 0x00
	pstepEnd->stepHeader.mode = 0; //StepMode 1 : CC-CV, 2: CC, 3: CV, 4: OCV

	Condition.apStepList.Add(pstepEnd);

	Condition.testHeader.totalStep = totStep + 1;

	m_TestCondition.RemoveStep();
	m_TestCondition.ResetCondition();
	if(m_TestCondition.SetProcedure(&Condition) == FALSE)
	{
		return FALSE;
	}

	m_pModule->GetCondition()->ResetCondition();
	m_pModule->GetCondition()->SetTestConditon(&m_TestCondition);

	//if(fnStepValidityCheck() == 0)
	//{
	//	TRACE("****** Condition 정상 ");
	//}

	//return fnStepValidityCheck() < 0 ? FALSE : TRUE;
	return TRUE;

#elif _DCIR

	CString strtemp;
	
	char nMachineTraytype = 0;
	nMachineTraytype = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "MachineTraytype", 0);
	nMachineTraytype += 0x30;

	if( m_inreserv.F_Value.Tray_Type[0] != nMachineTraytype )
	{
		CFMSLog::WriteErrorLog("[Module %d] Tray_Type Code ERROR(0 - 1) : Recv[%c] Charger[%c]", m_pModule->GetModuleID(), m_inreserv.F_Value.Tray_Type[0], nMachineTraytype );
		return FALSE;
	}

// 	if(m_inreserv.F_Value.Tray_Type[0] < 0x30 || m_inreserv.F_Value.Tray_Type[0] > 0x31)
// 	{
// 		CFMSLog::WriteErrorLog("Tray_Type code ERROR(0 - 1) [%c]", m_inreserv.F_Value.Tray_Type[0]);
// 		return FALSE;
// 	}

	UINT InputCellCount = 0;
	INT i = 0;
	for (i = 0; i < MAX_CELL_COUNT; i++)
	{
		if(m_inreserv.B_Value.arCellInfo[i].Cell_Info[0] == '0')//OK
		{
		}
		else if(m_inreserv.B_Value.arCellInfo[i].Cell_Info[0] == '1')//이전 공정 불량	
		{
		}
		else if(m_inreserv.B_Value.arCellInfo[i].Cell_Info[0] == '2')//No Cell
		{
		}
		else if(m_inreserv.B_Value.arCellInfo[i].Cell_Info[0] == '3')//Skip
		{
		}
		else
		{
			CFMSLog::WriteErrorLog("CellInfo code Error(0 - 3) [%c] ", m_inreserv.B_Value.arCellInfo[i].Cell_Info[0]);
			return FALSE;
		}
	}

	STR_CONDITION Condition;
	ZeroMemory(Condition.szTestSerialNo, EP_TEST_SERIAL_LENGTH+1);
	ZeroMemory(&Condition.modelHeader, sizeof(STR_CONDITION_HEADER));
	ZeroMemory(&Condition.conditionHeader, sizeof(STR_CONDITION_HEADER));
	ZeroMemory(&Condition.testHeader, sizeof(EP_TEST_HEADER));
	ZeroMemory(&Condition.checkParam, sizeof(STR_CHECK_PARAM));

	Condition.conditionHeader.lID = atoi(m_inreserv.F_Value.Process_No);
	Condition.conditionHeader.lNo = atoi(m_inreserv.F_Value.Process_No);

	Condition.modelHeader.lID = atoi(m_inreserv.F_Value.Type);
	Condition.modelHeader.lNo = atoi(m_inreserv.F_Value.Type);

	strtemp = m_inreserv.F_Value.Batch_No;
	strtemp = strtemp.Trim();
	strcpy_s(Condition.modelHeader.szName, strtemp.GetBuffer());
	FLOAT ftemp = 0;
	Condition.checkParam.fOCVUpperValue = 0;
	Condition.checkParam.fOCVLowerValue = 0;
	Condition.checkParam.fVRef = 0;

	if(fnRangeCheck(Range_I
	   , m_inreserv.B_Value.Contact.Charge_Cur
	   , Condition.checkParam.fIRef))
	   return FALSE;

	if(fnRangeCheck(Range_Sec_Time
	   , m_inreserv.B_Value.Contact.Charge_Time
	   , Condition.checkParam.fTime))
	   return FALSE;


	Condition.checkParam.fDeltaVoltage = atof(m_inreserv.B_Value.Contact.Contact_Reg);
	Condition.checkParam.fMaxFaultBattery = atof(m_inreserv.B_Value.Contact.Inverse_Vol);

	Condition.checkParam.fOCVUpperValue = atof(m_inreserv.B_Value.Contact.Upper_Vol_Check);
	Condition.checkParam.fOCVLowerValue = atof(m_inreserv.B_Value.Contact.Lower_Vol_Check);

	if(Condition.checkParam.fTime > 0)
	{
		Condition.checkParam.compFlag = 1;
	}

	Condition.checkParam.autoProcessingYN = 0;
	Condition.checkParam.reserved;

	st_DCIR_Step_Set *pFMSStep = m_inreserv.Step_Info;

	INT totStep = atoi(m_inreserv.F_Value.Total_Step);

	if(totStep < 1 || totStep > 100)
	{
		CFMSLog::WriteErrorLog(TEXT_LANG[4].GetBuffer(TEXT_LANG[4].GetLength()),totStep);//"적용할 수 없는 Step수입니다.(1-100) [%d], "
		return FALSE;
	}

	float fRecordTime = AfxGetApp()->GetProfileInt("FMS", "IMSRecordConditionTime", 1);

	for(i = 0; i < totStep; i++)
	{
		STR_COMMON_STEP _step;
		ZeroMemory(&_step, sizeof(STR_COMMON_STEP));

		_step.stepHeader.stepIndex = i;

		FMS_Step_Type fmsStepType = (FMS_Step_Type)atoi(pFMSStep[i].Step_Type);
		_step.fmsType = pFMSStep[i].Step_Type[0];
		
		_step.fRecDeltaTime = 10.0f;		//10sec

		switch(fmsStepType)
		{
		case FMS_CHG:
			{
				_step.stepHeader.type
					= EP_TYPE_CHARGE;
				_step.stepHeader.mode
					= EP_MODE_CCCV;

				if(fnRangeCheck(Range_S_V	//정전압 반드시 입력
					, pFMSStep[i].Volt
					, _step.fVref))
					return FALSE;

				if(fnRangeCheck(Range_S_I	//정전류 반드시 입력
					, pFMSStep[i].Current
					, _step.fIref))
					return FALSE;

				if(fnRangeCheck(Range_S_Sec_Time	//종지 시간 반드시 입력
					, pFMSStep[i].Time
					, _step.fEndTime))
					return FALSE;

				if(fnRangeCheck(Range_S_I		//종지 전류 반드시 입력
					, pFMSStep[i].CutOff
					, _step.fEndI))
					return FALSE;

				fnRangeCheck(Range_SOC			//SOC 선택입력
					, pFMSStep[i].CutOff_SOC
					, _step.fSocRate);

				fnRangeCheck(Range_Capa			//CAPA 선택입력
					, pFMSStep[i].CutOff_A
					, _step.fEndC);

				fnRangeCheck(Range_Min_Time, pFMSStep[i].GetTime_Setting, _step.fRecVIGetTime); 
				fnRangeCheck(Range_Min_Time, pFMSStep[i].Step_LimitChkTime, _step.fCompTimeV[0]); 

				_step.fOverV = atof(m_inreserv.B_Value.Protect.Charge_H_Vol);
				_step.fLimitV = atof(m_inreserv.B_Value.Protect.Charge_L_Vol);	// 사용용도가 다름 // 20131211 충전하한전압 추가
				_step.fOverI = atof(m_inreserv.B_Value.Protect.Charge_H_Cur);
				_step.fLimitI = atof(m_inreserv.B_Value.Protect.Charge_L_Cur);
				_step.fOverC = atof(m_inreserv.B_Value.Protect.Charge_H_Cap);
				_step.fLimitC = atof(m_inreserv.B_Value.Protect.Charge_L_Cap);

				// 1. 충전 취득 전압 관련 추가 20131210
				/*
				fnRangeCheck(Range_Min_Time
					, m_inreserv.B_Value.Protect.Charge_Vol_Get_Time
					, _step.fCompTimeV[1]);

				_step.fCompVHigh[1] = atof(m_inreserv.B_Value.Protect.Charge_H_Time_Vol);
				_step.fCompVLow[1] = atof(m_inreserv.B_Value.Protect.Charge_L_Time_Vol);

				_step.fCompChgCcVtg = atof(m_inreserv.B_Value.Protect.Charge_Comp_CC_Vol);
				fnRangeCheck(Range_Sec_Time
					, m_inreserv.B_Value.Protect.Charge_Comp_CC_Time
					, _step.fCompChgCcTime );

				_step.fCompChgCcDeltaVtg = atof(m_inreserv.B_Value.Protect.Charge_Comp_CC_DeltaVol);

				_step.fCompChgCvCrt = atof(m_inreserv.B_Value.Protect.Charge_Comp_CV_Cur);
				fnRangeCheck(Range_Sec_Time
					, m_inreserv.B_Value.Protect.Charge_Comp_CV_Time
					, _step.fCompChgCvTime );
				_step.fCompChgCvDeltaCrt = atof(m_inreserv.B_Value.Protect.Charge_Comp_Cv_DeltaCur);
				*/

				// _step.fRecDeltaTime = 1.0f;
				_step.fRecDeltaTime = fRecordTime;
			}
			break;

		case FMS_DCHG:
			{
				_step.stepHeader.type
					= EP_TYPE_DISCHARGE;
				_step.stepHeader.mode
					= EP_MODE_CC;

				if(fnRangeCheck(Range_S_I		//정전류 Ref 반드시 입력
					, pFMSStep[i].Current
					, _step.fIref))
					return FALSE;

				if(fnRangeCheck(Range_V			//정전압 Ref 선택 입력 (미입력시 SBC default 값 적용)
					, pFMSStep[i].Volt
					, _step.fVref))
					return FALSE;

				if(fnRangeCheck(Range_S_Sec_Time	//End Time 필수입력
					, pFMSStep[i].Time
					, _step.fEndTime))
					return FALSE;

				if(fnRangeCheck(Range_S_V		//종지 전압 필수입력
					, pFMSStep[i].CutOff
					, _step.fEndV))
					return FALSE;

				if(fnRangeCheck(Range_SOC		//Soc 선택입력
					, pFMSStep[i].CutOff_SOC
					, _step.fSocRate))
					return FALSE;

				if(fnRangeCheck(Range_Capa		//Capa 선택입력
					, pFMSStep[i].CutOff_A
					, _step.fEndC))
					return FALSE;

				if(_step.fVref <= 0.0f)
					_step.fVref = _step.fEndV - 50.0f;	//default Ref 
				if(_step.fVref <= 0.0f)
					_step.fVref = 0.0f;					//default Ref

				fnRangeCheck(Range_Min_Time, pFMSStep[i].GetTime_Setting, _step.fRecVIGetTime);
				fnRangeCheck(Range_Min_Time, pFMSStep[i].Step_LimitChkTime, _step.fCompTimeV[0]); 

				_step.fOverV = atof(m_inreserv.B_Value.Protect.DisCharge_H_Vol);
				_step.fLimitV = atof(m_inreserv.B_Value.Protect.DisCharge_L_Vol);
				_step.fOverI = atof(m_inreserv.B_Value.Protect.DisCharge_H_Cur);
				_step.fLimitI = atof(m_inreserv.B_Value.Protect.DisCharge_L_Cur);
				_step.fOverC = atof(m_inreserv.B_Value.Protect.DisCharge_H_Cap);
				_step.fLimitC = atof(m_inreserv.B_Value.Protect.DisCharge_L_Cap);

				// _step.fRecDeltaTime = 1.0f;
				_step.fRecDeltaTime = fRecordTime;
			}
			break;
		case FMS_REST:
			{
				_step.stepHeader.type
					= EP_TYPE_REST;
				_step.stepHeader.mode
					= 0;

				_step.fRecDeltaTime = 10.0f;

				if(fnRangeCheck(Range_S_Sec_Time		//필수 입력
					, pFMSStep[i].Time
					, _step.fEndTime))
					return FALSE;

				_step.fRecVIGetTime = atof(pFMSStep[i].GetTime_Setting) * 60.0f;
			}
			break;
		case FMS_OCV:
			{
				_step.stepHeader.type
					= EP_TYPE_OCV;
				_step.stepHeader.mode
					= 0;
				_step.fRecDeltaTime = 10.0f;
			}
			break;
		case FMS_CC_CHARGE_FANOFF:
			{
				// 팬 동작을 멈추는 스텝
				_step.m_nFanOffFlag = 1;
			}			
		case FMS_CC_CHARGE:
		case FMS_OG_CHARGE:
			{
				_step.stepHeader.type
					= EP_TYPE_CHARGE;
				_step.stepHeader.mode
					= EP_MODE_CC;

				if(fnRangeCheck(Range_S_I		//CC 전류 필수
					, pFMSStep[i].Current
					, _step.fIref))
					return FALSE;

				if(fnRangeCheck(Range_V			//CV 전압 선택 입력(미입력시 SBC Default)
					, pFMSStep[i].Volt
					, _step.fVref))
					return FALSE;

				if(fnRangeCheck(Range_S_V		//종지 전압 필수입력
					, pFMSStep[i].CutOff
					, _step.fEndV))
					return FALSE;


				if(fnRangeCheck(Range_S_Sec_Time	//종료 시간 필수
					, pFMSStep[i].Time
					, _step.fEndTime))
					return FALSE;


				if(fnRangeCheck(Range_SOC		//Soc 선택입력
					, pFMSStep[i].CutOff_SOC
					, _step.fSocRate))
					return FALSE;

				if(fnRangeCheck(Range_Capa		//Capa 선택입력
					, pFMSStep[i].CutOff_A
					, _step.fEndC))
					return FALSE;

				if(_step.fVref <= 0.0f)
					_step.fVref = _step.fEndV + 50.0f;	//default Ref 
				if(_step.fVref >= _OVP_V)
					_step.fVref = _OVP_V-10.0f;				//default Ref 

				fnRangeCheck(Range_Min_Time, pFMSStep[i].GetTime_Setting, _step.fRecVIGetTime);
				fnRangeCheck(Range_Min_Time, pFMSStep[i].Step_LimitChkTime, _step.fCompTimeV[0]); 

				_step.fOverV = atof(m_inreserv.B_Value.Protect.Charge_H_Vol);
				_step.fLimitV = atof(m_inreserv.B_Value.Protect.Charge_L_Vol);		//사용 용도가 다름 20131211 충전하한전압
				_step.fOverI = atof(m_inreserv.B_Value.Protect.Charge_H_Cur);
				_step.fLimitI = atof(m_inreserv.B_Value.Protect.Charge_L_Cur);
				_step.fOverC = atof(m_inreserv.B_Value.Protect.Charge_H_Cap);
				_step.fLimitC = atof(m_inreserv.B_Value.Protect.Charge_L_Cap);

				// _step.fRecDeltaTime = 1.0f;		//5sec
				_step.fRecDeltaTime = fRecordTime;

				// 1. 충전 취득 전압 관련 추가 20131210
				fnRangeCheck(Range_Min_Time
					, m_inreserv.B_Value.Protect.Charge_Vol_Get_Time
					, _step.fCompTimeV[1]);

				_step.fCompVHigh[1] = atof(m_inreserv.B_Value.Protect.Charge_H_Time_Vol);
				_step.fCompVLow[1] = atof(m_inreserv.B_Value.Protect.Charge_L_Time_Vol);
			}
			break;

		case FMS_DCIR:
			_step.stepHeader.type
				= EP_TYPE_IMPEDANCE;
			_step.stepHeader.mode
				= EP_MODE_DC_IMP;

			if(fnRangeCheck(Range_S_I		//정전류 Ref 반드시 입력
				, pFMSStep[i].Current
				, _step.fIref))
				return FALSE;

			if(fnRangeCheck(Range_V			//정전압 Ref 선택 입력 (미입력시 SBC default 값 적용)
				, pFMSStep[i].Volt
				, _step.fVref))
				return FALSE;

			if(fnRangeCheck(Range_S_Sec_Time	//End Time 필수입력
				, pFMSStep[i].Time
				, _step.fEndTime))
				return FALSE;

			if(fnRangeCheck(Range_V		//종지 전압 선택입력
				, pFMSStep[i].CutOff
				, _step.fEndV))
				return FALSE;

			//default V Ref.
			if(_step.fEndV <= 0.0f)		//endV 미설정시
			{
				_step.fVref = _MIN_CV_V-50.0f;
			}
			else						//End V 설정시
			{
				_step.fVref = _step.fEndV-50.0f;	//default Ref 
				if(_step.fVref <= 0.0f)
					_step.fVref = 0.0f;				//default Ref 
			}

			fnRangeCheck(Range_Min_Time, pFMSStep[i].GetTime_Setting, _step.fRecVIGetTime); 
			_step.fOverV = atof(m_inreserv.B_Value.Protect.DisCharge_H_Vol);
			_step.fLimitV = atof(m_inreserv.B_Value.Protect.DisCharge_L_Vol);
			_step.fOverI = atof(m_inreserv.B_Value.Protect.DisCharge_H_Cur);
			_step.fLimitI = atof(m_inreserv.B_Value.Protect.DisCharge_L_Cur);
			_step.fOverC = atof(m_inreserv.B_Value.Protect.DisCharge_H_Cap);
			_step.fLimitC = atof(m_inreserv.B_Value.Protect.DisCharge_L_Cap);

			_step.fRecDeltaTime = 1.0f;		//100msec

			_step.lDCIR_RegTemp = atof(m_inreserv.B_Value.Setting.Reference_Temp)*10.0f;
			_step.lDCIR_ResistanceRate = atof(m_inreserv.B_Value.Setting.Resistance_Change)*100.0f;  

			break;
		}

		_step.stepHeader.gradeSize = 0;
		_step.stepHeader.nProcType;


		//_step.fEndV = atof(pFMSStep->CutOff);
		//_step.fEndI = atof(pFMSStep->CutOff);
		_step.fEndDV;
		_step.fEndDI;

		_step.bUseActucalCap;
		_step.bReserved;

		_step.fSocRate;

		_step.fOverV;
		_step.fLimitV;
		_step.fOverI;
		_step.fLimitI;
		_step.fOverC;
		_step.fLimitC;
		_step.fOverImp;
		_step.fLimitImp;

		//_step.fCompTimeV;
		//_step.fCompVLow;
		//_step.fCompVHigh;

		//_step.fCompTimeI;
		//_step.fCompILow;
		//_step.fCompIHigh;

		_step.fDeltaTimeV;
		_step.fDeltaV;
		_step.fDeltaTimeI;
		_step.fDeltaI;

		_step.grade;

		_step.fRecDeltaV;
		_step.fRecDeltaI;
		_step.fRecDeltaT;

		_step.fParam1;
		_step.fParam2;

		_step.UseStepContinue;
		//_step.fmsType;
		_step.Reserved2;

		_step.fReserved;

		STR_COMMON_STEP* pStep = new STR_COMMON_STEP;
		CopyMemory(pStep, &_step, sizeof(STR_COMMON_STEP));
		Condition.apStepList.Add(pStep);

	}

	//end step 추가
	STR_COMMON_STEP* pstepEnd = new STR_COMMON_STEP;
	ZeroMemory(pstepEnd, sizeof(STR_COMMON_STEP));
	pstepEnd->stepHeader.type = EP_TYPE_END;//nType;
	pstepEnd->stepHeader.stepIndex = i;//stepIndex; 0x00
	pstepEnd->stepHeader.mode = 0; //StepMode 1 : CC-CV, 2: CC, 3: CV, 4: OCV

	Condition.apStepList.Add(pstepEnd);

	Condition.testHeader.totalStep = totStep + 1;

	m_TestCondition.RemoveStep();
	m_TestCondition.ResetCondition();
	if(m_TestCondition.SetProcedure(&Condition) == FALSE)
	{
		return FALSE;
	}

	m_pModule->GetCondition()->ResetCondition();
	m_pModule->GetCondition()->SetTestConditon(&m_TestCondition);

	//if(fnStepValidityCheck() == 0)
	//{
	//	TRACE(TEXT_LANG[5]);//"****** Condition 정상 "
	//}

	//return fnStepValidityCheck() < 0 ? FALSE : TRUE;
	return TRUE;

#endif
}

BOOL CFMS::fnSendConditionToModule()
{
	if( m_pModule == NULL )
	{
		CFMSLog::WriteErrorLog("fnSendConditionToModule : m_pModule == NULL");
		return FALSE;
	}
		
	CStep* pstep = (CStep*)m_TestCondition.GetStep(0);
	pstep = (CStep*)m_TestCondition.GetStep(1);

	CString strTemp;
	int nRtn = EP_NACK;
	int nDataSize = 0;
	m_bChgChkF = 0;		//20201215 ksj

	INT nModuleID = m_pModule->GetModuleID();
	INT nGroupIndex = 0;
	CTestCondition *lpProc = m_pModule->GetCondition();
		
	int nModuleIndex = EPGetModuleIndex(nModuleID);

	EP_TEST_HEADER testHeader;
	ZeroMemory(&testHeader, sizeof(EP_TEST_HEADER));
	testHeader.totalStep = (BYTE)lpProc->GetTotalStepNo();
	testHeader.totalGrade = 0;

	if((nRtn = EPSendCommand(nModuleID, nGroupIndex, 0, EP_CMD_TEST_HEADERDATA, &testHeader, sizeof(EP_TEST_HEADER)))!= EP_ACK)	
	{
		CFMSLog::WriteErrorLog("[Stage %s] EPSendCommand EP_CMD_TEST_HEADERDATA(0x2005) != EP_ACK", m_pModule->GetModuleName());
		return FALSE;
	}
	
	CFMSLog::WriteLog("[Stage %s] Send EP_CMD_TEST_HEADERDATA(0x2005)", m_pModule->GetModuleName());

	EP_PRETEST_PARAM checkParam;
	ZeroMemory( &checkParam, sizeof( EP_PRETEST_PARAM));

	checkParam = lpProc->GetNetCheckParam();

	// Event 초기화 delay 추측
	Sleep(100);

	if((nRtn = EPSendCommand(nModuleID, nGroupIndex, 0, EP_CMD_CHECK_PARAM, &checkParam, sizeof(EP_PRETEST_PARAM))) != EP_ACK)
	{
		CFMSLog::WriteErrorLog("[Stage %s] EPSendCommand EP_CMD_CHECK_PARAM(0x2006) != EP_ACK", m_pModule->GetModuleName());		
		return FALSE;
	}

	CFMSLog::WriteLog("[Stage %s] Send EP_CMD_CHECK_PARAM(0x2006)", m_pModule->GetModuleName());
	
	//20201215 ksj
	if(m_inreserv.B_Value.Type_Sel == m_strTypeSel[0])
	{
		m_ProcessNo = 1;
	}
	else
	{
		m_ProcessNo = 0;
	}

	CStep *pStep;
	LPVOID lpData = NULL;
	
	int j = 0;

	for(j=0; j< testHeader.totalStep; j++)
	{
		pStep = lpProc->GetStep(j);

		if(!pStep) return EP_NACK;

		if(pStep->m_type == 3 && m_bChgChkF == 0)  // 20200807 엄륭
		{
			m_bChgChkF = 1;
		}
		else if(pStep->m_type == 3 && m_bChgChkF != 0)
		{
			m_ProcessNo = 0;
		}

		if(pStep->GetNetStepData(lpData, nDataSize, m_ProcessNo) == FALSE)	//ljb Step Send
		{
			return FALSE;
		}

		if((nRtn = EPSendCommand(nModuleID, nGroupIndex, 0, EP_CMD_STEPDATA, lpData, nDataSize)) != EP_ACK)	
		{
			if(lpData != NULL)
			{
				delete lpData;	//20210223 ksj
				lpData = NULL;
			}
			CFMSLog::WriteErrorLog("[Stage %s] EPSendCommand EP_CMD_STEPDATA(0x2003) != EP_ACK", m_pModule->GetModuleName());			
			return FALSE;
		}
		else
		{
			CFMSLog::WriteLog("[Stage %s] EPSendCommand EP_CMD_STEPDATA(0x2003_%02d)", m_pModule->GetModuleName(), j+1);
		}
		
		delete lpData;
		lpData = NULL;
	}

	m_bChgChkF = 0 ; //20201215 ksj

	for(j=0; j< testHeader.totalStep; j++)
	{
		pStep = lpProc->GetStep(j);
		if(pStep->GetNetStepGradeData(lpData, nDataSize))
		{
			if((nRtn = EPSendCommand(nModuleID, nGroupIndex, 0, EP_CMD_GRADEDATA, lpData, nDataSize)) != EP_ACK)	
			{
				if(lpData != NULL)
				{
					delete lpData;
					lpData = NULL;
				}
				CFMSLog::WriteErrorLog("[Stage %s] EPSendCommand EP_CMD_GRADEDATA(0x2004) != EP_ACK", m_pModule->GetModuleName());
				return FALSE;
			}			
			
			if(lpData != NULL)
			{
				delete lpData;
				lpData = NULL;
			}
		}	
	}	

	if(m_pModule->WriteTestLogTempFile(lpProc) == FALSE)	//전송 공정 조건 Log 기록 파일 
	{
		CFMSLog::WriteErrorLog(TEXT_LANG[6].GetBuffer(TEXT_LANG[6].GetLength()), m_pModule->GetModuleName());//"[Stage %s] WriteTestLogTempFile( 공정 조건 log 파일 생성 )"
		return FALSE;
	}

	return TRUE;
}

BOOL CFMS::fnCheckProcAndSystemType(long lProcTypeID)
{
	//장비 종류만 확인 (ex.Formation인데 Aging으로 진입)
	//TestType Tabel에서 검색

	int nSysType = ((CCTSMonApp*)AfxGetApp())->GetSystemType();

	if(nSysType == EP_ID_ALL_SYSTEM)	return TRUE;

	CString str;
	str.Format("SELECT TestTypeName, ExeStep FROM TestType WHERE TestType = %d", lProcTypeID);	
	CDaoDatabase  db;
	try
	{
		db.Open(::GetDataBaseName());
		CDaoRecordset rs(&db);
		rs.Open(dbOpenSnapshot, str, dbReadOnly);	

		str.Empty();
		if(!rs.IsBOF() && !rs.IsEOF())
		{
			COleVariant data;
			data = rs.GetFieldValue(0);
			data = rs.GetFieldValue(1);
			str = data.pcVal;
		}
		rs.Close();
		db.Close();
	}
	catch (CDaoException* e)
	{
		e->Delete();
	}

	if(str.GetLength() > 2)
	{
		if(str.Left(2) == EP_PGS_AGING_CODE)	//Aging 장비 
		{
			if(nSysType != EP_ID_AGIGN_SYSTEM)
			{
				CFMSLog::WriteErrorLog(TEXT_LANG[7].GetBuffer(TEXT_LANG[7].GetLength()), str.Left(2), nSysType);//"Aging System에서 진행할 수 없습니다.[Code : %s, System : %d]"
				return FALSE;
			}
		}
		else if(str.Left(2) == EP_PGS_PREIMPEDANCE_CODE || str.Left(2) == EP_PGS_IROCV_CODE || str.Left(2) == EP_PGS_OCV_CODE )			//IROCV 측정기 
		{
			if(nSysType != EP_ID_IROCV_SYSTEM)
			{
				CFMSLog::WriteErrorLog(TEXT_LANG[8].GetBuffer(TEXT_LANG[8].GetLength()), str.Left(2), nSysType);//"IR/OCV System에서 진행할 수 없습니다.[Code : %s, System : %d]"
				return FALSE;
			}
		}
		else if(str.Left(2) == EP_PGS_GRADING_CODE || str.Left(2) == EP_PGS_SELECTOR_CODE)								//Selecting 설비 
		{
			if(nSysType != EP_ID_GRADING_SYSTEM)
			{
				CFMSLog::WriteErrorLog(TEXT_LANG[9].GetBuffer(TEXT_LANG[9].GetLength()), str.Left(2), nSysType);//"Grading/Selecting System에서 진행할 수 없습니다.[Code : %s, System : %d]"
				return FALSE;
			}
		}
		else //if(strProcess == "PC" || strProcess == "FO" || strProcess == "FC" )	//충방전기 설비 
		{
			if(nSysType != EP_ID_FORM_OP_SYSTEM)
			{
				CFMSLog::WriteErrorLog(TEXT_LANG[10].GetBuffer(TEXT_LANG[10].GetLength()), str.Left(2), nSysType);//"Formation System에서 진행할 수 없습니다.[Code : %s, System : %d]"
				return FALSE;
			}
		}
	}
	return TRUE;
}


INT CFMS::fnStepValidityCheck()
{
	int nTotStepNum = m_TestCondition.GetTotalStepNo();
	CStep* pStep, *pTempStep;
	if(nTotStepNum <0 || nTotStepNum >  EP_MAX_STEP)
	{
		CFMSLog::WriteErrorLog(TEXT_LANG[11].GetBuffer(TEXT_LANG[11].GetLength()));//"입력된 Step수가 범위를 벗어났습니다."
		return -1;
	}
	
	STR_CHECK_PARAM* pcheck = m_TestCondition.GetCheckParam();
	if(pcheck->compFlag)
	{
		//if(m_sPreTestParam.lTrickleTime < 500 || m_sPreTestParam.lTrickleTime > 6000)
		if(pcheck->fTime < 5 || pcheck->fTime > 60)
		{
			CFMSLog::WriteErrorLog(TEXT_LANG[12].GetBuffer(TEXT_LANG[12].GetLength()));//"Cell Check Parameter의 시간 설정값을 5초~60초 범위에서 입력하십시요."
			return -5;
		}
	}

	if(nTotStepNum < 1)
	{
		CFMSLog::WriteErrorLog("nTotStepNum < 0 .");
		return 0;
	}

	pStep = (CStep*)m_TestCondition.GetStep(nTotStepNum - 1); // Get element End
	if(pStep->m_type != EP_TYPE_END)
	{
		CFMSLog::WriteErrorLog(TEXT_LANG[13].GetBuffer(TEXT_LANG[13].GetLength()));//"마지막 Step이 [완료] Step이 아닙니다."
		return -1;
	}
	
	int nAdvCycleCount = 0;
	int nNormalStepCount = 0;
	int nCurrentAdvCycleIndex = 0;
	int nMaxEndVGotoStepNo = 0;		//EndV goto를 가지고 있는 Step번호 
	int nMaxEndVGotoStepIndex = 0;	//EndV goto가 가리키고 있는 Step번호

	int nMaxEndTGotoStepNo = 0;		//EndT goto를 가지고 있는 Step번호 
	int nMaxEndTGotoStepIndex = 0;	//EndT goto가 가리키고 있는 Step번호

	int nMaxEndCGotoStepNo = 0;		//EndC goto를 가지고 있는 Step번호 
	int nMaxEndCGotoStepIndex = 0;	//EndC goto가 가리키고 있는 Step번호
	CString strTemp;

	for(int i = 0; i< nTotStepNum; i++)		//Save All Step
	{
		pStep = (CStep*)m_TestCondition.GetStep(i); // Get element 0
		ASSERT(pStep);

		if(pStep->m_type < 0 )
		{
			CFMSLog::WriteErrorLog(TEXT_LANG[14].GetBuffer(TEXT_LANG[14].GetLength()), i+1);//"Step %d의 Type이 설정되지 않았습니다."
			return -2;
		}
		
		if(pStep->m_type == EP_TYPE_IMPEDANCE && pStep->m_mode == EP_MODE_DC_IMP)
		{
			if(pStep->m_fIref <= _MIN_CC_I)
			{
				CFMSLog::WriteErrorLog(TEXT_LANG[15].GetBuffer(TEXT_LANG[15].GetLength()), i+1);								//"Step %d의 전류값이 설정 되지 않았거나 범위를 벗어났습니다."
				return -1;
			}

			if(pStep->m_fIref > _MAX_CC_I)
			{
				CFMSLog::WriteErrorLog(TEXT_LANG[15].GetBuffer(TEXT_LANG[15].GetLength()), i+1);//"Step %d의 전류값이 설정 되지 않았거나 범위를 벗어났습니다."
				return -1;
			}

			if(pStep->m_fEndTime == 0)
			{
				CFMSLog::WriteErrorLog(TEXT_LANG[16].GetBuffer(TEXT_LANG[16].GetLength()), i+1);//"Step %d의 종료시간이 설정되지 않았습니다."
				return -1;
			}

		} //Impedance 

		if(pStep->m_type == EP_TYPE_CHARGE || pStep->m_type == EP_TYPE_DISCHARGE)
		{
			//formation에서는 CC나 CV에 
			//if(m_lLoadedProcType != PS_PGS_AGING)
			{
				if(pStep->m_mode == EP_MODE_CC && pStep->m_fEndV <= 0.0f)
				{
					CFMSLog::WriteErrorLog(TEXT_LANG[17].GetBuffer(TEXT_LANG[17].GetLength()), i+1);//"Step %d의 종료 전압이 설정되어 있지 않습니다."
					return -1;
				}
				if(pStep->m_mode == EP_MODE_CV && pStep->m_fEndI <= 0.0f)
				{
					CFMSLog::WriteErrorLog(TEXT_LANG[18].GetBuffer(TEXT_LANG[18].GetLength()), i+1);//"Step %d의 종료 전류가 설정되어 있지 않습니다."
					return -1;
				}

				if(pStep->m_mode == EP_MODE_CCCV )
				{
					if(pStep->m_fVref > _MAX_CV_V )
					{
						CFMSLog::WriteErrorLog(TEXT_LANG[19].GetBuffer(TEXT_LANG[19].GetLength())//"Step %d의 전압값이 최대 설정 전압값(%dmV) 보다 높습니다."
							, i+1, _MAX_CV_V );
						return -1;
					}

					if( pStep->m_fIref > _MAX_CC_I )
					{
						CFMSLog::WriteErrorLog(TEXT_LANG[20].GetBuffer(TEXT_LANG[20].GetLength())//"Step %d의 전류값이 최대 설정 전압값(%dmA) 보다 높습니다."
							, i+1, _MAX_CC_I );
						return -1;
					}
				}
			}

			//전류 설정값은 반드시 입력해야 한다.
			if(pStep->m_mode != EP_MODE_CP && pStep->m_mode != EP_MODE_CR)
			{
				if( pStep->m_fIref < _MIN_CC_I)
				{
					CFMSLog::WriteErrorLog(TEXT_LANG[21].GetBuffer(TEXT_LANG[21].GetLength())//"Step %d의 전류값이 설정 되지 않았거나 범위를 벗어났습니다.(%dmA)"
						, i+1, _MIN_CC_I);			
					return -1;
				}

				if( pStep->m_fIref > _MAX_CC_I )
				{
					CFMSLog::WriteErrorLog(TEXT_LANG[22].GetBuffer(TEXT_LANG[22].GetLength())//"Step %d의 전류값이 최대 설정 전류값(%dmA) 보다 높습니다."
						, i+1, _MAX_CC_I );
					return -1;
				}
			
				if(pStep->m_fIref <= pStep->m_fEndI || pStep->m_fIref <= pStep->m_fLowLimitI )
				{
					CFMSLog::WriteErrorLog(TEXT_LANG[23].GetBuffer(TEXT_LANG[23].GetLength()), i+1);			//"Step %d의 설정 전류값이 종료 전류나 전류 하한값보다 작습니다."
					return -1;
				}
			}
			else	//fIRef에 CP, CR제어값이 들어 있다.
			{
				if(EP_MODE_CR == pStep->m_mode && pStep->m_fIref <= 0.0f)
				{
					CFMSLog::WriteErrorLog(TEXT_LANG[24].GetBuffer(TEXT_LANG[24].GetLength()), i+1);			//"Step %d의 CR 저항값 입력 범위가 벗어났습니다.(CR > 0)"
					return -1;
				}
			}

			//최소 한가지 이상의 종료조건이 설정 되어야 한다.
			if( pStep->m_fEndC <= 0 && pStep->m_fEndDI <= 0 &&
				pStep->m_fEndDV <=0 && pStep->m_fEndI <=0 && 
				pStep->m_fEndV <=0 && pStep->m_fEndTime == 0
				//cwm//&& pStep->nUseDataStepNo == 0
				)
			{
				CFMSLog::WriteErrorLog(TEXT_LANG[25].GetBuffer(TEXT_LANG[25].GetLength()), i+1);			//"Step %d의 종료 조건이 설정되어 있지 않았습니다."
				return -1;
			}

			
			int k = 0;
			for( k=0; k<3; k++ )
			{
				//전압 상하한 크기 비교
				if(pStep->m_fCompVLow[k] > 0.0 && pStep->m_fCompVHigh[k] > 0.0)
				{
					if(pStep->m_fCompVLow[k] > pStep->m_fCompVHigh[k])
					{
						CFMSLog::WriteErrorLog(TEXT_LANG[26].GetBuffer(TEXT_LANG[26].GetLength()), i+1, k+1 );			//"Step %d 전압 변화 비교 Point %d 전압상한값이 하한값보다 작습니다."
						return -1;
					}
				}

				//전류 상하한 크기 비교
				if( pStep->m_fCompILow[k] > 0.0 && pStep->m_fCompIHigh[k] > 0.0)
				{
					if( pStep->m_fCompILow[k] > pStep->m_fCompIHigh[k])
					{
						CFMSLog::WriteErrorLog(TEXT_LANG[27].GetBuffer(TEXT_LANG[27].GetLength()), i+1, k+1 );			//"Step %d 전류 변화 비교 Point %d 전류상한값이 하한값보다 작습니다."
						return -1;
					}
				}
			}
			
			/*
			//Soc rate 설정
			if(pStep->m_fSocRate>0.0f)
			{
				STEP *pCapStep = GetStepData(pStep->nUseDataStepNo);
				if(pCapStep == NULL || pCapStep->bUseActualCapa == FALSE)
				{
					CFMSLog::WriteErrorLog("Step %d의 기준용량 종료로 사용할 Step이 설정되지 않았거나 존재하지 않는 Step을 참조하고 있습니다.(참조 Step %d)", i+1, pStep->nUseDataStepNo);			
					return -1;
				}
			}
			*/

		}	//Charge Discharge


		if(pStep->m_type == EP_TYPE_CHARGE)
		{
			//End 전압을 정격전압보다 최소 10mV이하 낮게 설정하여야 한다.
			//End 전압은 CC모드 입력에서만 가능한데 CC동작을 위해서는 CV 전압이 더 높게 설정되어야 하므로 
//			if(pStep->m_fEndV >= m_lMaxVoltage-10)
//			{
//				CFMSLog::WriteErrorLog("Step %d의 종료 전압이 정격 전압(%dmV)에 비해 높게 설정 되었습니다. 정격전압보다 최소 10mV이하 범위값으로 설정하십시오.", i+1, m_lMaxVoltage);			
//				return -1;				
//			}

			if(pStep->m_fVref > 0 && pStep->m_mode == EP_MODE_CC && pStep->m_fEndV > 0)		//Vref가 입력된 경우 
			{
				if(pStep->m_fVref <= pStep->m_fEndV)
				{
					CFMSLog::WriteErrorLog(TEXT_LANG[28].GetBuffer(TEXT_LANG[28].GetLength()), i+1);//"Step %d의 전압 설정값이 종료 전압보다 낮습니다."
					return -1;
				}
			}


			if(pStep->m_fVref <= 0.0f)
			{
				CFMSLog::WriteErrorLog(TEXT_LANG[29].GetBuffer(TEXT_LANG[29].GetLength()), i+1);			//"Step %d의 전압값이 설정 되지 않았거나 범위를 벗어났습니다.	(범위:0mV)"
				return -1;
			}

			if(pStep->m_fVref > _OVP_V )
			{
				CFMSLog::WriteErrorLog(TEXT_LANG[30].GetBuffer(TEXT_LANG[30].GetLength()), i+1, _OVP_V );//"Step %d의 전압값이 설정 전압값(%dmV) 보다 높습니다."
				return -1;
			}

			if(pStep->m_fVref <= pStep->m_fEndDV)	
			{
				CFMSLog::WriteErrorLog(TEXT_LANG[31].GetBuffer(TEXT_LANG[31].GetLength()), i+1);			//"Step %d의 전압변화 설정값이 너무 큽니다."
				return -1;				
			}
			
			if(pStep->m_fVref <= pStep->m_fEndV)
			{
				CFMSLog::WriteErrorLog(TEXT_LANG[32].GetBuffer(TEXT_LANG[32].GetLength()), i+1);			//"Step %d의 종료 전압이 설정 전압보다 높습니다."
				return -1;
			}
		} //charge
			

		//CC 방전일 경우 종료 전압은 Ref 전압 보다 커야 한다. 
		if(pStep->m_type == EP_TYPE_DISCHARGE)
		{
			if(pStep->m_fVref > 0 && pStep->m_mode == EP_MODE_CC && pStep->m_fEndV > 0)		//Vref가 입력된 경우 
			{
				if(pStep->m_fVref >= pStep->m_fEndV)
				{
					CFMSLog::WriteErrorLog(TEXT_LANG[33].GetBuffer(TEXT_LANG[33].GetLength()), i+1);//"Step %d의 전압 설정값이 종료 전압보다 높습니다."
					return -1;
				}
			}

			if(pStep->m_fEndV > _MAX_CV_V )
			{
				CFMSLog::WriteErrorLog(TEXT_LANG[34].GetBuffer(TEXT_LANG[34].GetLength()), i+1);//"Step %d의 종료 전압값이 설정 전압보다 보다 높습니다."
				return -1;
			}
		} //Discharge

		//Rest는 반드시 종료 시간이 입력 되어야 한다.
		if(pStep->m_type == EP_TYPE_REST)
		{
			if(pStep->m_fEndTime == 0)
			{
				CFMSLog::WriteErrorLog(TEXT_LANG[16].GetBuffer(TEXT_LANG[16].GetLength()), i+1);//"Step %d의 종료시간이 설정되지 않았습니다."
				return -1;
			}
		} //reset
	
		//cwm//if(pStep->m_fTref == 0.0f && (pStep->m_fStartT !=0.0f || pStep->m_fEndTemp != 0.0f))
		//cwm//{
		//cwm//	CFMSLog::WriteErrorLog("Step %d의 온도 설정값을 입력하지 않고 시작온도나 종료온도가 설정되었습니다.", i+1);			
		//cwm//	return -1;
		//cwm//}
			
		//전압 설정값 입력 범위 검사
		if(  pStep->m_fVref < 0.0f)
		{
			CFMSLog::WriteErrorLog(TEXT_LANG[52].GetBuffer(TEXT_LANG[52].GetLength()), i+1);//"Step %d의 전압 설정값이 범위를 벗어 났습니다.(범위:0mV)"
			return -1;
		}
		//전압 종료값 입력 범위 검사
		if(pStep->m_fEndV < 0.0f)
		{
			CFMSLog::WriteErrorLog(TEXT_LANG[35].GetBuffer(TEXT_LANG[35].GetLength()), i+1);//"Step %d의 종료 전압값이 입력 범위를 벗어났습니다.(범위:0mV)"
			return -7;
		}
		//전류 종료값 입력 범위 검사
		if(pStep->m_fEndI < 0.0f)
		{
			CFMSLog::WriteErrorLog(TEXT_LANG[36].GetBuffer(TEXT_LANG[36].GetLength()), i+1);//"Step %d의 종료 전류값이 입력 범위를 벗어났습니다.(범위:0mA)"
			return -8;
		}

		//전압 제한값 입력 범위 검사
		if(pStep->m_fLowLimitV < 0)
		{
			CFMSLog::WriteErrorLog(TEXT_LANG[37].GetBuffer(TEXT_LANG[37].GetLength()), i+1);//"Step %d의 안전 전압 상하한값이 입력 범위를 벗어났습니다.(범위:0mV)"
			return -1;
		}

		//전류 제한값 입력 검사 
		if(pStep->m_fLowLimitI < 0)
		{
			CFMSLog::WriteErrorLog(TEXT_LANG[38].GetBuffer(TEXT_LANG[38].GetLength()), i+1);//"Step %d의 안전 전류 상하한값이 입력 범위를 벗어났습니다."
			return -1;
		}
		
		//전압 제한 상한값을 입력하였을 경우 전압 설정값보다 반드시 커야 한다.
		if(pStep->m_fHighLimitV > 0)
		{
			if(pStep->m_fHighLimitV <= pStep->m_fVref)
			{
				CFMSLog::WriteErrorLog(TEXT_LANG[39].GetBuffer(TEXT_LANG[39].GetLength()), i+1);//"Step %d의 안전 전압 상한값이 설정 전압보다 작습니다."
				return -1;
			}
		}

		//전류 제한 상한값을 입력하였을 경우 전류 설정값보다 반드시 커야 한다.
		if(pStep->m_fHighLimitI > 0 )
		{
			if(pStep->m_fHighLimitI <= pStep->m_fIref)
			{
				CFMSLog::WriteErrorLog(TEXT_LANG[40].GetBuffer(TEXT_LANG[40].GetLength()), i+1);//"Step %d의 안전 전류 상한값이 설정 전류보다 작습니다."
				return -1;
			}
		}

		//종료 전압값은 전압 제한 하한값보다 커야 한다.
		if(pStep->m_fLowLimitV > 0 && pStep->m_fEndV > 0)
		{
			if(pStep->m_fLowLimitV > pStep->m_fEndV)
			{
				CFMSLog::WriteErrorLog(TEXT_LANG[41].GetBuffer(TEXT_LANG[41].GetLength()), i+1);//"Step %d의 종료 전압값이 안전 전압값 보다 작습니다."
				return -1;
			}
		}

		//용량 상한값이 하한값보다 커야 한다.
		if(pStep->m_fHighLimitC > 0.0f && pStep->m_fLowLimitC > 0.0f && pStep->m_fHighLimitC < pStep->m_fLowLimitC)
		{
			CFMSLog::WriteErrorLog(TEXT_LANG[53].GetBuffer(TEXT_LANG[53].GetLength()), i+1);//"Step %d의 안전 용량 상한값이 하한값보다 작습니다."
			return -1;
		}

		//전압 상한 값이 하한값보다 커야 한다.
		if(pStep->m_fHighLimitV > 0.0f && pStep->m_fLowLimitV > 0.0f && pStep->m_fHighLimitV < pStep->m_fLowLimitV)
		{
			CFMSLog::WriteErrorLog(TEXT_LANG[42].GetBuffer(TEXT_LANG[42].GetLength()), i+1);//"Step %d의 안전 전압 상한값이 하한값보다 작습니다."
			return -1;
		}

		//전류 상한 값이 하한값보다 커야 한다.
		if(pStep->m_fHighLimitI > 0 && pStep->m_fLowLimitI > 0 && pStep->m_fHighLimitI < pStep->m_fLowLimitI)
		{
			CFMSLog::WriteErrorLog(TEXT_LANG[43].GetBuffer(TEXT_LANG[43].GetLength()), i+1);//"Step %d의 안전 전류 상한값이 하한값보다 작습니다."
			return -1;
		}

		//임피던스 상한 값이 하한값보다 커야 한다.
		if(pStep->m_fHighLimitImp > 0 && pStep->m_fLowLimitImp > 0 && pStep->m_fHighLimitImp < pStep->m_fLowLimitImp)
		{
			CFMSLog::WriteErrorLog(TEXT_LANG[44].GetBuffer(TEXT_LANG[44].GetLength()), i+1);//"Step %d의 안전 저항 상한값이 하한값보다 작습니다."
			return -1;
		}
	}
	return 1;
}
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
BOOL CFMS::fnMakeTrayInfo( bool bNew )
{
	//fnLoadWorkInfo(0);

	CFormResultFile* pResultFile = &m_pModule->m_aResultData[0];

	CTray* pTray = m_pModule->GetTrayInfo(0);

	if( bNew == TRUE )
	{
		pTray->InitData(TRUE);
	}
	else
	{
		return TRUE;
	}
	
	pTray->strLotNo = m_inreserv.F_Value.Batch_No;	
	pTray->strLotNo = pTray->strLotNo.Trim();

	CString TrayNo = m_inreserv.F_Value.Tray_ID;	
	TrayNo = TrayNo.Trim();
	pTray->SetTrayNo(TrayNo);

	CString strTestSerialNo = _T("");
	COleDateTime curTime = COleDateTime::GetCurrentTime();
	strTestSerialNo.Format("%s%02d", curTime.Format("%Y%m%d%H%M%S"), 1);
	pTray->strTestSerialNo = strTestSerialNo;
	pTray->m_nTrayType = atoi(m_inreserv.F_Value.Tray_Type);

	if(AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "AutoCellCheck", FALSE) == TRUE )
	{
		pTray->m_nContactErrlimit = MAX_CELL_COUNT;
	}
	else
	{
		pTray->m_nContactErrlimit = atoi(m_inreserv.B_Value.Contact_Error_Setting);
	}
	
	pTray->m_nChErrlimit = AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "ChErrorlimit", 1);

	//////////////////////////////////////////////////////////////////////////
	st_CELL_INFO arcellInfo[MAX_CELL_COUNT];
	memcpy(arcellInfo, m_inreserv.B_Value.arCellInfo, sizeof(st_CELL_INFO) * MAX_CELL_COUNT);
	UINT InputCellCount = 0;
	INT i = 0;
	for (i = 0; i < MAX_CELL_COUNT; i++)
	{
		if(arcellInfo[i].Cell_Info[0] == '0')//OK
		{
			InputCellCount++;
			pTray->cellCode[i] = EP_CODE_NORMAL;
		}
		else if(arcellInfo[i].Cell_Info[0] == '1') //이전 공정 불량	
		{
			pTray->cellCode[i] = EP_CODE_CELL_FAIL;
		}
		else if(arcellInfo[i].Cell_Info[0] == '2') //No Cell
		{
			pTray->cellCode[i] = EP_CODE_NONCELL;
		}
		else
		{
			pTray->cellCode[i] = EP_CODE_NONCELL;
		}
	}	
	//////////////////////////////////////////////////////////////////////////
	pTray->lInputCellCount = InputCellCount;
	for (i = 0; i < MAX_CELL_COUNT; i++)
	{
		pTray->SetCellSerial(i, m_inreserv.F_Value.arCell_ID[i].Cell_ID);
	}

	//////////////////////////////////////////////////////////////////////////
	ZeroMemory(&m_runInfo, sizeof(EP_RUN_CMD_INFO));
	//단방향 에러 처리
	//높이 1 에러 처리
	// m_runInfo.nTabDeepth = atoi(m_inreserv.F_Value.Tap_Deepth);
	m_runInfo.nTrayType = atoi(m_inreserv.F_Value.Tray_Type);
	// m_runInfo.nTrayType = atoi(m_inreserv.F_Value.Tray_Type);	
	m_runInfo.nCurrentCellCnt = InputCellCount;					
	m_runInfo.nContactErrlimit = atoi(m_inreserv.B_Value.Contact_Error_Setting);

	CFMSLog::WriteLog("[Stage %s] nTrayType %d, nCurrentCellCnt %d, nContactErrorSetting %d, nCappErrlimit %d "
		, m_pModule->GetModuleName()		
		, m_runInfo.nTrayType
		, m_runInfo.nCurrentCellCnt
		, m_runInfo.nContactErrlimit
		, m_runInfo.nCappErrlimit
		);

	sprintf(m_runInfo.szTestSerialNo, m_pModule->GetTestSerialNo());

	for(INT nTrayIndex=0; nTrayIndex < INSTALL_TRAY_PER_MD; nTrayIndex++)
	{
		pTray = m_pModule->GetTrayInfo(nTrayIndex);
		if(pTray)
		{
			m_runInfo.nInputCell[nTrayIndex] = pTray->lInputCellCount;
		}
	}

	if(SendLastTrayStateData(m_pModule->GetModuleID(), 0) == FALSE)	//현재 Tray의 최종 상태를 Module로 전송
	{
		return FALSE;
	}

	// 1. 입고 예약 정보를 저장하기 위해서..
	if(m_pModule->UpdateTestLogTempFile())
	{

	}

	return TRUE;
}
BOOL CFMS::FileNameCheck(char *szfileName)
{
	//현재 파일명이 사용 가능한 파일명인지 확인(특수 문자등의 입력으로 파일생성 안되는것을 방지) 
	CString strTemp;
	FILE *fp = NULL;
	if(strlen(szfileName) <= 0)	return  FALSE;

	if((fp = fopen(szfileName, "wb")) != NULL)
	{
		fclose(fp);
		fp = NULL;
		_unlink(szfileName);	//확인 됐으면 삭제(불 필요한 파일 증가 막기 위해) 
		return TRUE;
	}
	else
	{
		CFMSLog::WriteErrorLog("%S, %S", GetStringTable(IDS_MSG_FILE_NAME_INVALID), szfileName);
	}
	return FALSE;
}
BOOL CFMS::CheckFileNameValidate(CString strFileName, CString &strNewFileName, int nModuleID, int nGroupIndex )
{
	//다른 Module에서 사용중인 파일이지 검사
	CString strTemp;
	//int ID;
	//for(int i =0; i<GetInstalledModuleNum(); i++)
	//{
	//	ID = EPGetModuleID(i);
	//	for(int j=0; j<EPGetGroupCount(ID); j++)
	//	{
	//		if(nModuleID  != ID || nGroupIndex != j)	//자기 자신은 제외
	//		{
	//			if(GetResultFileName(ID, j) == strFileName && !strFileName.IsEmpty())
	//			{
	//				m_strLastErrorString.Format(GetStringTable(IDS_MSG_FILE_NAME_USED), strFileName, ::GetModuleName(ID, j));
	//				AfxMessageBox(m_strLastErrorString);
	//				return FALSE;
	//			}
	//		}
	//	}
	//}

	//파일이 중복되는지 검사 
	CFileFind fileFinder;
	if(fileFinder.FindFile(strFileName))			//Find Board Test Result File
	{
		if(AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "NoOverWrite", FALSE))
		{
			//Over write 방지 (for LSC)
			CFMSLog::WriteErrorLog(TEXT_LANG[45].GetBuffer(TEXT_LANG[45].GetLength()), strFileName);//"%s는 중복된 이름입니다. 다른 이름을 입력하거나 이전 결과를 삭제 후 시작하십시요."
			//AfxMessageBox(strTemp, MB_ICONSTOP|MB_OK);
			return FALSE;
		}
		else
		{
			//사용자에게 Overwite여부를 물음
			//strTemp.Format(GetStringTable(IDS_MSG_FILE_OVER_WIRTE_CONFIRM), strFileName);
			//if(AfxMessageBox(strTemp, MB_ICONQUESTION|MB_YESNO) == IDNO)
			{
				// 1. 결과파일이 있을경우 파일 이름을 변경해서 저장한다.
				CString strTemp = _T("");
				CString strNewFileName = _T("");

				AfxExtractSubString(strTemp, strFileName, 1,'.');

				if( strTemp == "fmt" )
				{					
					COleDateTime curTime = COleDateTime::GetCurrentTime();
					AfxExtractSubString(strTemp, strFileName, 0,'.');					
					strFileName.Format("%s_%s", strTemp, curTime.Format("%H%M%S"));					
				}
				else
				{
					return false;
				}
			}
		}		
	}

	m_resultfilePathNName = strNewFileName = strFileName;
	//파일명으로 유효 여부 검사 
	return FileNameCheck((LPSTR)(LPCTSTR)strNewFileName);
}

CString CFMS::CreateResultFilePath(int nModuleID, CString strDataFolder, CString strLot, CString strTray)
{
	//모듈별로 분리해서 저장 	
	CFileFind aDirChecker;
	CString strTempFolderName(strDataFolder);

	//1. Lot 구별 폴더 
	if(m_bFolderLot && !strLot.IsEmpty())
	{
		strTempFolderName =  strTempFolderName + "\\" + strLot;
		if(aDirChecker.FindFile(strTempFolderName) == FALSE)
		{
			if(::CreateDirectory(strTempFolderName, NULL) == FALSE)
			{
				return "";
			}
		}
		aDirChecker.Close();
	}

	//2. Tray별 구별 폴더 
	if(m_bFolderTray && !strTray.IsEmpty())
	{
		strTempFolderName =  strTempFolderName + "\\" + strTray;
		if(aDirChecker.FindFile(strTempFolderName) == FALSE)
		{
			if(::CreateDirectory(strTempFolderName, NULL) == FALSE)
			{
				return "";
			}
		}
		aDirChecker.Close();
	}

	//3. 폴더 구별
	if(m_bFolderModuleName)
	{
		strTempFolderName =  strTempFolderName + "\\" + m_pModule->GetModuleName();
		if(aDirChecker.FindFile(strTempFolderName) == FALSE)
		{
			if(::CreateDirectory(strTempFolderName, NULL) == FALSE)
			{
				return "";
			}
		}
		aDirChecker.Close();
	}

	//4. 시간 별로 폴더 분리
	if(m_bFolderTime)
	{
		COleDateTime oleTime = COleDateTime::GetCurrentTime();
		if(m_nTimeFolderInterval <= 1)			//1일 단위 생성 
		{
			strTempFolderName += oleTime.Format("\\%Y%m%d");
		}
		else if(m_nTimeFolderInterval <= 10)	//10일 단위 생성
		{
			if(oleTime.GetDay() <= 10)
			{
				strTempFolderName += oleTime.Format("\\%Y%m01");
			}
			else if(oleTime.GetDay() <= 20)
			{
				strTempFolderName += oleTime.Format("\\%Y%m11");
			}
			else
			{
				strTempFolderName += oleTime.Format("\\%Y%m21");				
			}
		}
		else if(m_nTimeFolderInterval <= 20)		//15일 간격 생성 
		{
			if(oleTime.GetDay() <= 15)
			{
				strTempFolderName += oleTime.Format("\\%Y%m01");
			}
			else
			{
				strTempFolderName += oleTime.Format("\\%Y%m16");				
			}
		}
		else
		{
			strTempFolderName += oleTime.Format("\\%Y%m");				
		}

		if(aDirChecker.FindFile(strTempFolderName) == FALSE)
		{
			if(::CreateDirectory(strTempFolderName, NULL) == FALSE)
			{
				return "";
			}
		}
		aDirChecker.Close();
	}

	return strTempFolderName;
}
INT CFMS::ShowRunInformation(int nModuleID, int nGroupIndex, BOOL bNewFileName, long lProcPK = 0)
{
	CString strTemp;
	CFormModule *pModule = m_pModule;
	if(pModule == FALSE)	
		return 0;
	
	CTray *pTray;
	BOOL bAuto = EPGetAutoProcess(nModuleID);

	///폴더와 기본 파일명 생성 ///
	CString strDataPath;
	if(bNewFileName)
	{
		for(int j=0; j<pModule->GetTotalJig(); j++)
		{
			pTray = pModule->GetTrayInfo();
			if(pTray && bAuto == FALSE)	//수동 공정이면 모두 처음 공정처럼 처리 	
			{
				pTray->SetFirstProcess(TRUE);
			}
			
			COleDateTime nowtime(COleDateTime::GetCurrentTime());

			strDataPath = CreateResultFilePath(nModuleID, m_strDataFolder, pModule->GetLotNo(j), pModule->GetTrayNo(j));			
			
			// SKI kky 결과파일 형식 변경( ProcessNo(00)_TrayId(000000)_일시(130826134333).fmt
			strTemp.Format("%s\\%02d_%s_%s.fmt", 
				strDataPath,
				pModule->GetCondition()->GetModelID(),
				pModule->GetTrayNo(j),
				nowtime.Format("%y%m%d%H%M%S")				
				);
			/*
			strTemp.Format("%s\\%d_%s_%s_%s_T%02d.fmt", 
							strDataPath,
							pModule->GetTestSequenceNo(),
							pModule->GetTestName(),
							pModule->GetLotNo(j),
							pModule->GetTrayNo(j),
							j+1
							);	
			*/
			
			pModule->SetResultFileName(strTemp, j);
			CFMSLog::WriteLog("[Stage %s] CreateResultFileName => %s", pModule->GetModuleName(), m_pModule->GetResultFileName() );
		}
	}

	//////////////////////////////////////////////////////////////////////////	
	//자동 진행에서 자동 시작 설정시는 새로운 Tray가 아니고 Group이 Standby 상태 이면 바로 진행 
	//모두 자동 진행 설정시에는 공정 조건이 자동으로 보내지고 Module이 Stanby 상태가 되기전에
	//Start Command가 전송되는 경우가 있어서 상태를 검사 한다.
	//BOOL bAutoRun = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "AutoRun", 0);
	
	//STR_CONDITION_HEADER testInfo = GetPrevTest(lProcPK);
	//BOOL bNewTray = testInfo.lID <=0 ? TRUE: FALSE;	//pTray->IsNewProcedureTray();

	// if((bAutoRun && bAuto && !bNewTray && EPGetGroupState(nModuleID, nGroupIndex) == EP_STATE_STANDBY)) 
	if(EPGetGroupState(nModuleID, nGroupIndex) == EP_STATE_STANDBY
		|| EPGetGroupState(nModuleID, nGroupIndex) == EP_STATE_END
		|| EPGetGroupState(nModuleID, nGroupIndex) == EP_STATE_READY)
	{
		BOOL bAllOk = TRUE;
		for(int j=0; j<pModule->GetTotalJig(); j++)
		{
			CString strTemp;
			if(CheckFileNameValidate(pModule->GetResultFileName(j), strTemp, nModuleID) == FALSE)
			{
				bAllOk = FALSE;
			}
		}
		if(bAllOk)	return 1;	//파일명이 모두 정상이면 자동 실행하고 
	}

	CFMSLog::WriteErrorLog(TEXT_LANG[46].GetBuffer(TEXT_LANG[46].GetLength()), pModule->GetModuleName() );//"[Stage %s] 파일명에 문제가 발생함."
	return 0;
}

BOOL CFMS::SendLastTrayStateData(int nModuleID, int nGroupIndex)
{
	CFormModule *pModule = m_pModule;
	
	if(pModule == NULL)	return FALSE;

	CTray* pTray = pModule->GetTrayInfo(0);
	
	ZeroMemory(&m_runInfo, sizeof(EP_RUN_CMD_INFO));
	
	int nTotalHWCh = EPGetChInGroup(nModuleID, nGroupIndex);	//전송해야할 채널수 
	int nTraychSum = 0;
	for(int t=0; t<pModule->GetTotalJig(); t++)
	{
		nTraychSum += pModule->GetChInJig(t);
	}

	CString strTemp;	
	EP_TRAY_STATE	trayState;
	ZeroMemory(&trayState, sizeof(trayState));	

	int nInputCell = 0;
	for (INT i = 0; i < nTotalHWCh; i++)
	{	
		trayState.cellCode[i] = pTray->cellCode[i];
		if( trayState.cellCode[i] == EP_CODE_NORMAL )
		{
			nInputCell++;
		}
	}

	sprintf(m_runInfo.szTestSerialNo, "%s",  pTray->strTestSerialNo );
	sprintf(m_runInfo.szTrayID, "%s", pTray->GetTrayNo() );
	m_runInfo.nTabDeepth = pTray->m_nTabDeepth;
	m_runInfo.nTrayHeight = pTray->m_nTrayHeight;
	m_runInfo.nTrayType = pTray->m_nTrayType;
	m_runInfo.nCurrentCellCnt = pTray->lInputCellCount;
	m_runInfo.nContactErrlimit = pTray->m_nContactErrlimit;
	m_runInfo.nCappErrlimit = pTray->m_nCapaErrlimit;
	m_runInfo.nChErrlimit = pTray->m_nChErrlimit;
	m_runInfo.nInputCell[0] = pTray->lInputCellCount;

	int nRtn;
	if((nRtn = EPSendCommand(nModuleID, nGroupIndex+1, 0, EP_CMD_TRAY_DATA, &trayState, sizeof(trayState))) != EP_ACK)
	{
		CFMSLog::WriteErrorLog("%s :: Grade Data Send Fail");
		return FALSE;
	}
	
	return TRUE;
}

DWORD CFMS::CheckEnableRun(int nModuleID)
{
	CString strTemp;
	DWORD bStateFlag = 0;

	//TrayLoad Check & Crate file 
	CFormModule *pModule = m_pModule;
	if(pModule == NULL)		return 0;

	CTray *pTrayInfo;
	for(int t =0; t<pModule->GetTotalJig(); t++)
	{
		//준비된 Tray
		pTrayInfo = pModule->GetTrayInfo(t);
		if(pTrayInfo->IsWorkingTray())
		{
			////Tray 감지 Sensor Check;
			// 무조건 Run 보내기 - Tray 감지 제외 [2013/9/23 cwm]
			//if(EPTrayState(nModuleID, t) != EP_TRAY_LOAD)
			//{
			//	CFMSLog::WriteErrorLog("%s의 Jig %d번에 Tray[%s]가 감지되지 않아 시작할 수 없습니다.", 
			//		m_pModule->GetModuleName(), t+1, pTrayInfo->GetTrayNo());
			//	return 0;
			//}
			//else
			//{

			//}

			//Create result file
			//20060523
			//기존에 최소 1개의 step이 종료되면 결과 파일이 생성되던 방식을 시험 시작시 결과 파일을 생성하도록 수정
			//실시간 DataDown시 Step1번의 진행 data 경과를 보기 위해  			
			if(pModule->CreateTrayResultFile(t) == FALSE)
			{
				CFMSLog::WriteErrorLog("%s %s", pModule->GetResultFileName(), " file create fail!!!");
				return 0;
			}
		
			bStateFlag |= (0x01<<t);
		}
	}	

	if(bStateFlag == 0)
	{
		CFMSLog::WriteErrorLog(TEXT_LANG[47].GetBuffer(TEXT_LANG[47].GetLength())//"%s에 작업가능한 Tray번호나 수량이 입력되지 않았습니다.\n작업할 Tray를 삽입한 후 Tray번호와 수량을 정확히 입력 후 재시도 하십시요."
			,	m_pModule->GetModuleName());
	}
	return bStateFlag;
}

BOOL CFMS::fnRun()
{
	INT nModuleID = m_pModule->GetModuleID();
	//이전 정보를 Reset하고 보여줄지 여부 
	if(ShowRunInformation(nModuleID, 0, TRUE) < 1)
	{
		return FALSE;
	}

	//Module에 전송 정보 저장 (파일생성 이전에 )
	m_pModule->UpdateStartTime();

	m_pModule->UpdateTestLogTempFile();	//CheckEnableRun()에서 파일을 생성하므로 그 이전에 Update해야 한다.	

	//작업 가능상태인지 검사 
	//결과 파일도 생성한다.
	DWORD dwJigFlag = CheckEnableRun(nModuleID);
	if(dwJigFlag == 0)
	{
		return FALSE;
	}

	//최종 상태 검사
	// 무조건 Run 보내기 - 상태 검사 제외 [2013/9/23 cwm]
	//BYTE byTemp;
	//WORD state = EPGetGroupState(nModuleID, 0);
	//if(state != EP_STATE_STANDBY && state != EP_STATE_READY && state != EP_STATE_END && state != EP_STATE_IDLE)
	//{
	//	CFMSLog::WriteErrorLog("%s :: [작업시작] 명령 전송가능 상태가 아닙니다.(state : %s)", m_pModule->GetModuleName(), g_strSBCState[state]);
	//	return FALSE;
	//}

	sprintf(m_runInfo.szTestSerialNo, m_pModule->GetTestSerialNo());
	m_runInfo.dwUseFlag = dwJigFlag;
	m_runInfo.nTempGetPosition = atol(AfxGetApp()->GetProfileString(FORMSET_REG_SECTION, "Temp Measuring Position", 0));

	int nRtn = EP_NACK;
	//ljb Send Run Command
	if((nRtn = EPSendCommand(nModuleID, 0, 0, EP_CMD_RUN, &m_runInfo, sizeof(EP_RUN_CMD_INFO))) != EP_ACK)
	{
		if( nRtn == EP_TIMEOUT )
		{
			// 20131205 시작 신호 전송을 실패 했을 경우는 그냥 무시한다.
			//
		}
		else
		{
			CFMSLog::WriteErrorLog("[%s] :: %s", m_pModule->GetModuleName(), ::GetStringTable(IDS_TEXT_CMD_SEND_FAIL));
			return FALSE;
		}
	}

	//2006/03/22 KBH
	//완료된 Step의 결과 data를 표시할 수 있도록 최종 data file reset 한다.
	m_pModule->LoadResultData();

	fnResultDataStateUpdate(m_resultfilePathNName.GetBuffer(), RS_RUN);

	return TRUE;
}

// 1. 상태가 변경되면 CHARGER_STATE 메세지를 전송
VOID CFMS::fnSetFMSStateCode(UINT mode, FMS_STATE_CODE _code)
{
	if(m_Mode != mode || m_FMSStateCode != _code)
	{
		m_Mode = mode;
		m_FMSStateCode = _code;
		//////////////////////////////////////////////////////////////////////////
		m_LastState = _code;
		CString strTemp;
		strTemp.Format("StageLastState_%02d", m_nModuleID);
		AfxGetApp()->WriteProfileInt(FMS_REG, strTemp, m_LastState);
		//////////////////////////////////////////////////////////////////////////
		st_CHARGER_STATE StateData;
		memset(&StateData, 0x00, sizeof(st_CHARGER_STATE));
		// sprintf(StateData.Equipment_Num, "%03d", (m_pModule->GetModuleID()) + m_LineNo);
		sprintf(StateData.Equipment_Num, "%03d", fnGetLineNo() );

		switch(m_Mode)
		{
		case EQUIP_ST_OFF:
		case EQUIP_ST_LOCAL:
		case EQUIP_ST_MAINT:
			if(m_FMSStateCode <= 9)
				StateData.Equipment_State[0] = m_FMSStateCode + 0x30;
			else
				StateData.Equipment_State[0] = m_FMSStateCode - 10 + 0x41;
			StateData.Equipment_State[1] = '1';
			break;
		case EQUIP_ST_AUTO:
			if(m_FMSStateCode <= 9)
				StateData.Equipment_State[0] = m_FMSStateCode + 0x30;
			else
				StateData.Equipment_State[0] = m_FMSStateCode - 10 + 0x41;
			StateData.Equipment_State[1] = '0';
			break;
		}
		st_FMS_PACKET* pPack = fnGetPack();
		memset(pPack->data, 0x20, sizeof(pPack->data));
		UINT dataLen = strLinker(pPack->data, &StateData, g_iCHARGER_STATE_Map);
		pPack->dataLen = dataLen;
		fnMakeHead(pPack->head, CHARGER_STATE, dataLen, RESULT_CODE_OK);

		theApp.m_FMS_SendQ.Push(*pPack);
	}

}
FMS_STATE_CODE CFMS::fnGetFMSStateCode()
{
	return m_FMSStateCode;
}
VOID CFMS::fnGetFMSImpossbleCode(st_IMPOSSBLE_STATE& _iState)
{
	switch(m_Mode)
	{
	case EQUIP_ST_OFF:
	case EQUIP_ST_LOCAL:
	case EQUIP_ST_MAINT:
		if(m_FMSStateCode <= 9)
			_iState.imState[0] = m_FMSStateCode + 0x30;
		else
			_iState.imState[0] = m_FMSStateCode - 10 + 0x41;
		_iState.imState[1] = '1';
		break;
	case EQUIP_ST_AUTO:
		if(m_FMSStateCode <= 9)
			_iState.imState[0] = m_FMSStateCode + 0x30;
		else
			_iState.imState[0] = m_FMSStateCode - 10 + 0x41;
		_iState.imState[1] = '0';
		break;
	}
}
VOID CFMS::fnGetFMSDCIRImpossbleCode(st_DCIR_IMPOSSBLE_STATE& _iState)
{
	switch(m_Mode)
	{
	case EQUIP_ST_OFF:
	case EQUIP_ST_LOCAL:
	case EQUIP_ST_MAINT:
		if(m_FMSStateCode <= 9)
			_iState.imState[0] = m_FMSStateCode + 0x30;
		else
			_iState.imState[0] = m_FMSStateCode - 10 + 0x41;
		_iState.imState[1] = '1';
		break;
	case EQUIP_ST_AUTO:
		if(m_FMSStateCode <= 9)
			_iState.imState[0] = m_FMSStateCode + 0x30;
		else
			_iState.imState[0] = m_FMSStateCode - 10 + 0x41;
		_iState.imState[1] = '0';
		break;
	}
}
INT CFMS::fnGetLineNo()
{
	return atoi(m_pModule->GetModuleName().Right(3));
	// return (m_pModule->G etModuleID()) + m_LineNo;
}

CHAR* CFMS::fnGetTrayID()
{
	if(strlen(m_inreserv.F_Value.Tray_ID) == 0)
		strcpy_s(m_inreserv.F_Value.Tray_ID, "       ");

	return m_inreserv.F_Value.Tray_ID;
}

CString CFMS::Time_Conversion(CString str_Time)
{
	CString Mon[7], str_Mon_Time;	
		
	str_Time.Replace(" ","-");
	str_Time.Replace(":","-");

	for(int i = 0; i < 7; i++)
	{
		AfxExtractSubString(Mon[i], str_Time, i, '-');
		Mon[i].Trim();
	}

	str_Time = "";
	str_Time += Mon[0];
	str_Time += Mon[1];
	str_Time += Mon[2];
	str_Time += Mon[3];
	str_Time += Mon[4];
	str_Time += Mon[5];
	str_Time += Mon[6];

	return str_Time;
}

// ok				0
// file error		1
// Contact Error	2
// Capa Error		3
// Over Charge		4
// FMS 결과데이터 작성
//[2013/9/5 cwm]
FMS_ERRORCODE CFMS::fnMakeResult()
{	
 	FMS_ERRORCODE _error = FMS_ER_NONE;

	CFormModule *pModule = m_pModule;
	if(pModule != NULL)
	{
		m_resultfilePathNName = pModule->GetResultFileName();		
	}
	else
	{
		return ER_File_Not_Found;
	}

	CString strFile = m_resultfilePathNName;
	CString strConFile = strFile.Left( strFile.ReverseFind('.'));
	strConFile = strConFile + "_CON.Fmt";

	CFMSLog::WriteLog("[Stage %s] fnMakeResult Start For FMS => FileName:%s, ConFileName:%s", m_pModule->GetModuleName(), strFile, strConFile);
	
	if(pModule->m_resultFile.ReadFile(strFile, 0) != 1)
	{
		return ER_File_Not_Found;
	}
	
    CString strTmp;
	EP_FILE_HEADER		*lpFileHeader = NULL;
    STR_SAVE_CH_DATA	*lpChannel1 = NULL;
    STR_STEP_RESULT		*lpStepData1 = NULL;
    RESULT_FILE_HEADER	*lpFileData = NULL;
    STR_CONDITION_HEADER *lpModelData = NULL;
	
	// int nStepSize = pModule->m_resultFile.GetStepSize();
	// if(nStepSize <= 0) return ER_File_Open_error;
	int nStepCnt = pModule->m_resultFile.GetTestCondition()->GetTotalStepNo();

	INT nStepIndex = 0;
	BOOL bCheckfileloss = FALSE;
	for( nStepIndex=0; nStepIndex < nStepCnt; nStepIndex++)
	{
		lpStepData1 = pModule->m_resultFile.GetStepData(nStepIndex);
		if( lpStepData1 == NULL )
		{
			bCheckfileloss = TRUE;
			break;
		}
	}

	if( bCheckfileloss == TRUE )
	{
		// 1. 손실 복구 실행
		// 2. 결과 파일 다시 열기
		if( AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "AutoRestoreFmtData", 0) == TRUE )
		{
			if( pModule->RestoreLostData_new() == TRUE )
			{	
				if(pModule->m_resultFile.ReadFile(strFile, 0) != 1)
				{
					return ER_File_Not_Found;
				}

				pModule->LoadResultData();
			}
			else
			{
				return ER_File_Not_Found;
			}
		}
	}
    
	lpFileHeader = pModule->m_resultFile.GetFileHeader();
	if(lpFileHeader == NULL) return ER_File_Open_error;
    
    lpFileData = pModule->m_resultFile.GetResultHeader();
    if(lpFileData == NULL) return ER_File_Open_error;	

#ifdef _DCIR

	BOOL bImpStep = FALSE;

	st_DCIR_INRESEVE* pReserveInfo = &m_inreserv;
	memset(&m_Result_Data_L, 0x00, sizeof(st_DCIR_RESULT_FILE_RESPONSE_L));
	
	strTmp = pModule->m_resultFile.GetModuleName(); 
    memset(m_Result_Data_L.Stage_No, 0x20, sizeof(m_Result_Data_L.Stage_No));
    memcpy(m_Result_Data_L.Stage_No, strTmp.GetBuffer(), min(3, strTmp.GetLength()));
    m_Result_Data_L.Stage_No[3] = 0x00;
   
    strcpy_s(m_Result_Data_L.Type, pReserveInfo->F_Value.Type);
	strcpy_s(m_Result_Data_L.Process_ID, pReserveInfo->F_Value.Process_No);
	strcpy_s(m_Result_Data_L.Batch_No, pReserveInfo->F_Value.Batch_No);
	
    memset(m_Result_Data_L.Tray_ID, 0x20, sizeof(m_Result_Data_L.Tray_ID));
    memcpy(m_Result_Data_L.Tray_ID, lpFileData->szTrayNo, min(7, strlen(lpFileData->szTrayNo)));
    m_Result_Data_L.Tray_ID[7] = 0x00;
	 
	// m_Result_Data_L.DC_IR[
    //TrayID
    strTmp = lpFileData->szTrayNo; 
    strTmp.Trim();
    
    CString strFMSTrayID = fnGetTrayID();
    strFMSTrayID.Trim();
    
    //요청 TrayID와 비교
 	if(strcmp(strFMSTrayID, strTmp) != 0)
	{
		CFMSLog::WriteErrorLog("MakeResult TrayID Error");
		return ER_File_Open_error;
	}

	CString Str_Step;

    INT stepCnt = 0;
    for(int a =0; a < nStepCnt; a++)
    {
        lpStepData1 = pModule->m_resultFile.GetStepData(a);
        
		if( lpStepData1->stepCondition.stepHeader.type == EP_TYPE_END )
		{
			break;
		}

		stepCnt++;

		memset(&m_total_Step[a], 0x00, sizeof(st_Result_Step_Set));

		int j = 0;		
		Str_Step.Format("%3d", a+1);
		memcpy(m_total_Step[a].R_Step_No, Str_Step.GetBuffer(), 3);

		if( lpStepData1 == NULL )
		{
			// 1. 중간에 빠진 결과 데이터가 존재할 경우
			continue;
		}       

        if(lpStepData1->stepCondition.fmsType != 0)                     //action
		{
            m_total_Step[a].R_Action[0] = lpStepData1->stepCondition.fmsType;
		}
        else
		{
            m_total_Step[a].R_Action[0] = ' ';
		}

        m_total_Step[a].R_Action[1] = 0x00;

        //Channel Data
		for(int i = 0; i < MAX_CELL_COUNT; i++)
		{
            if(i < lpStepData1->aChData.GetSize())
            {
				lpChannel1 = (STR_SAVE_CH_DATA *)lpStepData1->aChData.GetAt(i);
				
                CHAR* pconCode = fnGetChannelCode(lpChannel1->channelCode);     //FMS Cell Code
				CString strCode;
				strCode.Format("%2s", pconCode);

				if( i == 0 )
				{
					Str_Step.Format("%4.1f", lpChannel1->fJigTemp[0]);
					memcpy(m_total_Step[a].R_JigTemp1, Str_Step.GetBuffer(), 4);
					Str_Step.Format("%4.1f", lpChannel1->fJigTemp[1]);
					memcpy(m_total_Step[a].R_JigTemp2, Str_Step.GetBuffer(), 4);
					Str_Step.Format("%4.1f", lpChannel1->fJigTemp[2]);
					memcpy(m_total_Step[a].R_JigTemp3, Str_Step.GetBuffer(), 4);
					Str_Step.Format("%4.1f", lpChannel1->fJigTemp[3]);
					memcpy(m_total_Step[a].R_JigTemp4, Str_Step.GetBuffer(), 4);
					Str_Step.Format("%4.1f", lpChannel1->fJigTemp[4]);
					memcpy(m_total_Step[a].R_JigTemp5, Str_Step.GetBuffer(), 4);
					Str_Step.Format("%4.1f", lpChannel1->fJigTemp[5]);
					memcpy(m_total_Step[a].R_JigTemp6, Str_Step.GetBuffer(), 4);
					Str_Step.Format("%4.1f", lpChannel1->fTempMax);
					memcpy(m_total_Step[a].R_JigTempMax, Str_Step.GetBuffer(), 4);
					Str_Step.Format("%4.1f", lpChannel1->fTempMin);
					memcpy(m_total_Step[a].R_JigTempMin, Str_Step.GetBuffer(), 4);
					Str_Step.Format("%4.1f", lpChannel1->fTempAvg);
					memcpy(m_total_Step[a].R_JigTempAvg, Str_Step.GetBuffer(), 4);					
				}

				// if(	strcmp(pconCode, "02") == 0 || strcmp(pconCode, "01") == 0 )
				if(	strcmp(strCode, "02") == 0 || strcmp(strCode, "01") == 0 )
				{
					if(lpStepData1->stepCondition.stepHeader.type == EP_TYPE_IMPEDANCE)
					{
						bImpStep = TRUE;

						Str_Step.Format("%6s", "     0");
						memcpy(m_Result_Data_L.DC_IR[i].DC_IR, Str_Step.GetBuffer(), 6);
						Str_Step.Format("%5s", "    0");
						memcpy(m_Result_Data_L.V1[i].V1, Str_Step.GetBuffer(), 5);
						Str_Step.Format("%5s", "    0");
						memcpy(m_Result_Data_L.V2[i].V2, Str_Step.GetBuffer(), 5);
						Str_Step.Format("%5s", "    0");
						memcpy(m_Result_Data_L.Discharge_Time[i].Discharge_Time, Str_Step.GetBuffer(), 5);
						Str_Step.Format("%6s", "     0");
						memcpy(m_Result_Data_L.CutOff[i].CutOff, Str_Step.GetBuffer(), 6);
						Str_Step.Format("%5s", "    0");
						memcpy(m_Result_Data_L.Age_temp[i].Age_temp, Str_Step.GetBuffer(), 5);   
					}

					Str_Step.Format("%5s", "    0");
					memcpy(m_total_Step[a].R_Voltage_Worth[i].Voltage_Worth, Str_Step.GetBuffer(), 5);
					Str_Step.Format("%6s", "     0");
					memcpy(m_total_Step[a].R_Capacity_Worth[i].Capacity_Worth, Str_Step.GetBuffer(), 6);
					Str_Step.Format("%6s", "     0");
					memcpy(m_total_Step[a].R_Discharge_Current_Worth[i].Discharge_Current_Worth, Str_Step.GetBuffer(), 6);
					Str_Step.Format("%5s", "    0");
					memcpy(m_total_Step[a].R_Step_End_Time[i].Step_End_Time, Str_Step.GetBuffer(), 5);		
				}
				else
				{
					if(lpStepData1->stepCondition.stepHeader.type == EP_TYPE_IMPEDANCE)
					{
						bImpStep = TRUE;

						Str_Step.Format("%6.3f", lpChannel1->fImpedance);
						memcpy(m_Result_Data_L.DC_IR[i].DC_IR, Str_Step.GetBuffer(), 6);

						Str_Step.Format("%5.f", lpChannel1->fDCIR_V1);
						memcpy(m_Result_Data_L.V1[i].V1, Str_Step.GetBuffer(), 5);

						Str_Step.Format("%5.f", lpChannel1->fDCIR_V2);
						memcpy(m_Result_Data_L.V2[i].V2, Str_Step.GetBuffer(), 5);

						Str_Step.Format("%5.f", lpChannel1->fStepTime);
						memcpy(m_Result_Data_L.Discharge_Time[i].Discharge_Time, Str_Step.GetBuffer(), 5);

						Str_Step.Format("%6.f", fabs(lpChannel1->fDCIR_AvgCurrent));
						memcpy(m_Result_Data_L.CutOff[i].CutOff, Str_Step.GetBuffer(), 6);

						if(strcmp(pconCode, "01") == 0)        //이전공정 불량이면 전류값 0표시
						{
							Str_Step.Format("%6s", "     0"); 
							memcpy(m_Result_Data_L.CutOff[i].CutOff, Str_Step.GetBuffer(), 6);
						}
						Str_Step.Format("%5.1f", lpChannel1->fDCIR_AvgTemp);
						memcpy(m_Result_Data_L.Age_temp[i].Age_temp, Str_Step.GetBuffer(), 5);
					}   

					Str_Step.Format("%5.f", lpChannel1->fVoltage);
					memcpy(m_total_Step[a].R_Voltage_Worth[i].Voltage_Worth, Str_Step.GetBuffer(), 5);

					Str_Step.Format("%6.f", lpChannel1->fCapacity);
					memcpy(m_total_Step[a].R_Capacity_Worth[i].Capacity_Worth, Str_Step.GetBuffer(), 6);

					Str_Step.Format("%6.f", fabs(lpChannel1->fCurrent));
					memcpy(m_total_Step[a].R_Discharge_Current_Worth[i].Discharge_Current_Worth, Str_Step.GetBuffer(), 6);

					//이전공정 불량과 REST,OCV는 Current 저장 안함
					if(strcmp(pconCode, "01") == 0 || m_total_Step[a].R_Action[0] == '3' || m_total_Step[a].R_Action[0] == '4')
					{
						Str_Step.Format("%6s", "     0");
						memcpy(m_total_Step[a].R_Discharge_Current_Worth[i].Discharge_Current_Worth, Str_Step.GetBuffer(), 6);
					}

					Str_Step.Format("%5.f", lpChannel1->fStepTime);
					memcpy(m_total_Step[a].R_Step_End_Time[i].Step_End_Time, Str_Step.GetBuffer(), 5);
				}
			}
			else        //사용하지 않는 Channel
			{
                if(lpStepData1->stepCondition.stepHeader.type == EP_TYPE_IMPEDANCE)
                {
					bImpStep = TRUE;

                    Str_Step.Format("%6s", "     0");
                    memcpy(m_Result_Data_L.DC_IR[i].DC_IR, Str_Step.GetBuffer(), 6);
                    Str_Step.Format("%5s", "    0");
                    memcpy(m_Result_Data_L.V1[i].V1, Str_Step.GetBuffer(), 5);
                    Str_Step.Format("%5s", "    0");
                    memcpy(m_Result_Data_L.V2[i].V2, Str_Step.GetBuffer(), 5);
                    Str_Step.Format("%5s", "    0");
                    memcpy(m_Result_Data_L.Discharge_Time[i].Discharge_Time, Str_Step.GetBuffer(), 5);
                    Str_Step.Format("%6s", "     0");
                    memcpy(m_Result_Data_L.CutOff[i].CutOff, Str_Step.GetBuffer(), 6);
                    Str_Step.Format("%5s", "    0");
                    memcpy(m_Result_Data_L.Age_temp[i].Age_temp, Str_Step.GetBuffer(), 5);   
                }
                
                Str_Step.Format("%5s", "    0");
                memcpy(m_total_Step[a].R_Voltage_Worth[i].Voltage_Worth, Str_Step.GetBuffer(), 5);
                Str_Step.Format("%6s", "     0");
                memcpy(m_total_Step[a].R_Capacity_Worth[i].Capacity_Worth, Str_Step.GetBuffer(), 6);
                Str_Step.Format("%6s", "     0");
                memcpy(m_total_Step[a].R_Discharge_Current_Worth[i].Discharge_Current_Worth, Str_Step.GetBuffer(), 6);
                Str_Step.Format("%5s", "    0");
                memcpy(m_total_Step[a].R_Step_End_Time[i].Step_End_Time, Str_Step.GetBuffer(), 5);					
			}
		}		
	}

	if( bImpStep == FALSE )
	{
		for(int i = 0; i < MAX_CELL_COUNT; i++)
		{
			Str_Step.Format("%6s", "     0");
			memcpy(m_Result_Data_L.DC_IR[i].DC_IR, Str_Step.GetBuffer(), 6);
			Str_Step.Format("%5s", "    0");
			memcpy(m_Result_Data_L.V1[i].V1, Str_Step.GetBuffer(), 5);
			Str_Step.Format("%5s", "    0");
			memcpy(m_Result_Data_L.V2[i].V2, Str_Step.GetBuffer(), 5);
			Str_Step.Format("%5s", "    0");
			memcpy(m_Result_Data_L.Discharge_Time[i].Discharge_Time, Str_Step.GetBuffer(), 5);
			Str_Step.Format("%6s", "     0");
			memcpy(m_Result_Data_L.CutOff[i].CutOff, Str_Step.GetBuffer(), 6);
			Str_Step.Format("%5s", "    0");
			memcpy(m_Result_Data_L.Age_temp[i].Age_temp, Str_Step.GetBuffer(), 5);   
		}
	}

    sprintf_s(m_Result_Data_L.Total_Step, "%03d", stepCnt);

    memset(&m_Result_Data_R, 0x00, sizeof(st_DCIR_RESULT_FILE_RESPONSE_R));

    CString Str_Result_Data_R;
    Str_Result_Data_R.Format("%3d",stepCnt);
    memcpy(m_Result_Data_R.Now_Step_No, Str_Result_Data_R.GetBuffer(), 3);

    //저장한 스텝의 제일 마지막(End Step or 마지막 Step)
    lpStepData1 = pModule->m_resultFile.GetStepData(pModule->m_resultFile.GetStepSize() - 1);

    if(lpStepData1 != NULL)
    {
        INT ContactErrorCnt = 0;
        for(int h = 0; h < MAX_CELL_COUNT; h++)
        {
            CHAR szTemp[3] = {0};

            if(h < lpStepData1->aChData.GetSize())
            {
                lpChannel1 = (STR_SAVE_CH_DATA *)lpStepData1->aChData.GetAt(h);

                sprintf(szTemp, "%2s", fnGetChannelCode(lpChannel1->channelCode));
            }
            else
            {
                sprintf(szTemp, "%2s", "  ");
            }
            memcpy(m_Result_Data_R.arCellInfo[h].Cell_Info, szTemp, 3);     //?
        }

        CString StartTime, EndTime;
        EndTime.Format("%s", lpStepData1->stepHead.szEndDateTime);
        EndTime = Time_Conversion(EndTime);
        Str_Result_Data_R.Format("%14s",EndTime);
        memcpy(m_Result_Data_R.Test_Finish_Date, Str_Result_Data_R.GetBuffer(), 14);

        lpStepData1 = pModule->m_resultFile.GetFirstStepData();
        StartTime.Format("%s", lpStepData1->stepHead.szStartDateTime);
        StartTime = Time_Conversion(StartTime);
        Str_Result_Data_R.Format("%14s",StartTime);
        memcpy(m_Result_Data_R.Test_Begin_Date, Str_Result_Data_R.GetBuffer(), 14);
    }	
//////
	
	char* msg1;
	msg1 = new CHAR[4096];
	memset(msg1, 0x00, 4096);
	CString str_Result_Data_L_String;

	strLinker(msg1, &m_Result_Data_L, g_iDCIR_Result_File_Response_L_Map);
	TRACE("%s ",msg1);
	str_Result_Data_L_String.Format("%s",msg1);
	memset(msg1, 0x00, 4096);

	char* msg;
	msg = new CHAR[4096];
	memset(msg, 0x00, 4096);
	CString str_Step, str_Step_String;

	for(int i = 0; i < stepCnt; i++)
	{
		strLinker(msg, m_total_Step[i].R_Step_No, g_iDCIR_Result_R_Step_No_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strLinker(msg, m_total_Step[i].R_Action, g_iDCIR_Result_R_Action_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strLinker(msg, m_total_Step[i].R_JigTemp1, g_iResult_R_Temp_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strLinker(msg, m_total_Step[i].R_JigTemp2, g_iResult_R_Temp_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strLinker(msg, m_total_Step[i].R_JigTemp3, g_iResult_R_Temp_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strLinker(msg, m_total_Step[i].R_JigTemp4, g_iResult_R_Temp_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strLinker(msg, m_total_Step[i].R_JigTemp5, g_iResult_R_Temp_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strLinker(msg, m_total_Step[i].R_JigTemp6, g_iResult_R_Temp_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strLinker(msg, m_total_Step[i].R_JigTempMax, g_iResult_R_Temp_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strLinker(msg, m_total_Step[i].R_JigTempMin, g_iResult_R_Temp_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strLinker(msg, m_total_Step[i].R_JigTempAvg, g_iResult_R_Temp_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strLinker(msg, m_total_Step[i].R_Voltage_Worth, g_iDCIR_Voltage_Worth_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strLinker(msg, m_total_Step[i].R_Capacity_Worth, g_iDCIR_Capacity_Worth_Map);
		TRACE("%s ",msg);
		str_Step_String.Format("%s",msg);
		str_Step = str_Step + str_Step_String;
		memset(msg, 0x00, 4096);

		strLinker(msg, m_total_Step[i].R_Discharge_Current_Worth, g_iDCIR_Discharge_Current_Worth_Map);
		TRACE("%s ",msg);
		str_Step_String.Format("%s",msg);
		str_Step = str_Step + str_Step_String;
		memset(msg, 0x00, 4096);

		strLinker(msg, m_total_Step[i].R_Step_End_Time, g_iDCIR_Step_End_Time_Map);
		TRACE("%s ",msg);
		str_Step_String.Format("%s",msg);
		str_Step = str_Step + str_Step_String;
		memset(msg, 0x00, 4096);
	}

	char* msg2;
	msg2 = new CHAR[4096];
	memset(msg2, 0x00, 4096);
	CString str_Result_Data_R_String, str_Result_Data;

	strLinker(msg2, &m_Result_Data_R, g_iDCIR_Result_File_Response_R_Map);
	TRACE("%s ",msg2);
	str_Result_Data_R_String.Format("%s",msg2);
	memset(msg2, 0x00, 4096);

	m_str_Result_Data = str_Result_Data_L_String + str_Step + str_Result_Data_R_String;
	
	delete [] msg;
	delete [] msg1;
	delete [] msg2;
	
	CFMSLog::WriteLog("[Stage %s] fnMakeResult(%d) End For FMS", m_pModule->GetModuleName(), m_str_Result_Data.GetLength() );

    return _error;

#else

	st_CHARGER_INRESEVE* pReserveInfo = &m_inreserv;
	memset(&m_Result_Data_L, 0x00, sizeof(st_RESULT_FILE_RESPONSE_L));
    
	strTmp = pModule->m_resultFile.GetModuleName();
    memset(m_Result_Data_L.Stage_No, 0x20, sizeof(m_Result_Data_L.Stage_No));
    memcpy(m_Result_Data_L.Stage_No, strTmp.GetBuffer(), min(3, strTmp.GetLength()));
    m_Result_Data_L.Stage_No[3] = 0x00;	

	strcpy_s(m_Result_Data_L.Type, pReserveInfo->F_Value.Type);

	strcpy_s(m_Result_Data_L.Process_ID, pReserveInfo->F_Value.Process_No);

	strcpy_s(m_Result_Data_L.Batch_No, pReserveInfo->F_Value.Batch_No);

	memset(m_Result_Data_L.Tray_ID, 0x20, sizeof(m_Result_Data_L.Tray_ID));
	memcpy(m_Result_Data_L.Tray_ID, lpFileData->szTrayNo, min(7, strlen(lpFileData->szTrayNo)));
	m_Result_Data_L.Tray_ID[7] = 0x00;


	//TrayID
	strTmp = lpFileData->szTrayNo; 
	strTmp.Trim();

	CString strFMSTrayID = fnGetTrayID();
	strFMSTrayID.Trim();

	//요청 TrayID와 비교
	if(strcmp(strFMSTrayID, strTmp) != 0)
	{
		CFMSLog::WriteErrorLog("MakeResult TrayID Error");
		return ER_File_Open_error;
	}

	CString Str_Step;

	INT a = 0;
	INT stepCnt = 0;
	for(a=0; a < nStepCnt; a++)
	{
		lpStepData1 = pModule->m_resultFile.GetStepData(a);

		if( lpStepData1->stepCondition.stepHeader.type == EP_TYPE_END )
		{
			break;
		}

		stepCnt++;

		memset(&m_total_Step[a], 0x00, sizeof(st_Result_Step_Set));

		int j = 0;		
		Str_Step.Format("%3d", a+1);
		memcpy(m_total_Step[a].R_Step_No, Str_Step.GetBuffer(), 3);

		if( lpStepData1 == NULL )
		{
			// 1. 중간에 빠진 결과 데이터가 존재할 경우
			continue;
		}

		//Str_Step.Format("%1s", m_inreserv.Step_Info[a].Step_Type);
		//Str_Step.Format("%1s", lpStepData1->stepCondition.fmsType);
		//memcpy(m_total_Step[a].R_Action, Str_Step.GetBuffer(), 1);
		if(lpStepData1->stepCondition.fmsType != 0)
			m_total_Step[a].R_Action[0] = lpStepData1->stepCondition.fmsType;
		else
			m_total_Step[a].R_Action[0] = 0x20;
		m_total_Step[a].R_Action[1] = 0x00;

		for(int i = 0; i < MAX_CELL_COUNT; i++)
		{
			if(i < lpStepData1->aChData.GetSize())
			{
				//변환된 채널코드가 00 정상저장
				//변환된 채널코드가 01 전압저장
				//변환되 채널코드가 02 저장안함
				lpChannel1 = (STR_SAVE_CH_DATA *)lpStepData1->aChData.GetAt(i);

				if( i == 0 )
				{
					Str_Step.Format("%4.1f", lpChannel1->fJigTemp[0]);
					memcpy(m_total_Step[a].R_JigTemp1, Str_Step.GetBuffer(), 4);
					Str_Step.Format("%4.1f", lpChannel1->fJigTemp[1]);
					memcpy(m_total_Step[a].R_JigTemp2, Str_Step.GetBuffer(), 4);
					Str_Step.Format("%4.1f", lpChannel1->fJigTemp[2]);
					memcpy(m_total_Step[a].R_JigTemp3, Str_Step.GetBuffer(), 4);
					Str_Step.Format("%4.1f", lpChannel1->fJigTemp[3]);
					memcpy(m_total_Step[a].R_JigTemp4, Str_Step.GetBuffer(), 4);
					Str_Step.Format("%4.1f", lpChannel1->fJigTemp[4]);
					memcpy(m_total_Step[a].R_JigTemp5, Str_Step.GetBuffer(), 4);
					Str_Step.Format("%4.1f", lpChannel1->fJigTemp[5]);
					memcpy(m_total_Step[a].R_JigTemp6, Str_Step.GetBuffer(), 4);
					Str_Step.Format("%4.1f", lpChannel1->fTempMax);
					memcpy(m_total_Step[a].R_JigTempMax, Str_Step.GetBuffer(), 4);
					Str_Step.Format("%4.1f", lpChannel1->fTempMin);
					memcpy(m_total_Step[a].R_JigTempMin, Str_Step.GetBuffer(), 4);
					Str_Step.Format("%4.1f", lpChannel1->fTempAvg);
					memcpy(m_total_Step[a].R_JigTempAvg, Str_Step.GetBuffer(), 4);					
				}

				CHAR* pconCode = fnGetChannelCode(lpChannel1->channelCode);
				CString strCode;
				strCode.Format("%2s", pconCode);

				if( strcmp(strCode, "02") == 0 || strcmp(strCode, "01") == 0 )
				{
					Str_Step.Format("%5s", "    0");
					memcpy(m_total_Step[a].R_Voltage_Worth[i].Voltage_Worth, Str_Step.GetBuffer(), 5);

					Str_Step.Format("%6s", "     0");
					memcpy(m_total_Step[a].R_Capacity_Worth[i].Capacity_Worth, Str_Step.GetBuffer(), 6);

// 					Str_Step.Format("%6s", "     0");
// 					memcpy(m_total_Step[a].R_Current_Worth_Hour[i].Current_Worth_Hour, Str_Step.GetBuffer(), 6);

					Str_Step.Format("%6s", "     0");
					memcpy(m_total_Step[a].R_Discharge_Current_Worth[i].Discharge_Current_Worth, Str_Step.GetBuffer(), 6);

					Str_Step.Format("%6s", "     0");
					memcpy(m_total_Step[a].R_Step_End_Time[i].Step_End_Time, Str_Step.GetBuffer(), 6);

					Str_Step.Format("%5s", "    0");
					memcpy(m_total_Step[a].R_Step_Voltage_Time[i].Step_Voltage_Time, Str_Step.GetBuffer(), 5);

					Str_Step.Format("%6s", "     0");
					memcpy(m_total_Step[a].R_Step_Current_Time[i].Step_Current_Time, Str_Step.GetBuffer(), 6);

					//////////////////////////////////////////////////////////////////////////

					Str_Step.Format("%6s", "     0");
					memcpy(m_total_Step[a].R_Step_Current_Capacity_CC[i].Step_Current_Capacity_CC, Str_Step.GetBuffer(), 6);

					Str_Step.Format("%6s", "     0");
					memcpy(m_total_Step[a].R_Step_CV_Time[i].Step_CV_Time, Str_Step.GetBuffer(), 6);

					Str_Step.Format("%6s", "     0");
					memcpy(m_total_Step[a].R_Step_CV_Capacity[i].Step_CV_Capacity, Str_Step.GetBuffer(), 6);					
					Str_Step.Format("%6d", (int)lpChannel1->fDeltaOCV); //20210223 ksj
					memcpy(m_total_Step[a].R_Step_DeltaVoltage[i].Step_DeltaVoltage, Str_Step.GetBuffer(), 7);	

				}
				else
				{
					Str_Step.Format("%5d", (int)lpChannel1->fVoltage);
					memcpy(m_total_Step[a].R_Voltage_Worth[i].Voltage_Worth, Str_Step.GetBuffer(), 5);	//20210223 ksj

					// 					Str_Step.Format("%6.f", lpChannel1->fTimeGetChargeVoltage);
					// 					memcpy(m_total_Step[a].R_Current_Worth_Five_Min[i].Current_Worth_Five_Min, Str_Step.GetBuffer(), 6);
					// 
					// 					//이전공정 불량과 REST,OCV는 Current 저장 안함
					// 					if(strcmp(pconCode, "01") == 0 || m_total_Step[a].R_Action[0] == '3' || m_total_Step[a].R_Action[0] == '4')
					// 					{
					// 						Str_Step.Format("%6s", "     0");
					// 						memcpy(m_total_Step[a].R_Current_Worth_Five_Min[i].Current_Worth_Five_Min, Str_Step.GetBuffer(), 6);
					// 					}

					Str_Step.Format("%6d", (int)lpChannel1->fCapacity);
					memcpy(m_total_Step[a].R_Capacity_Worth[i].Capacity_Worth, Str_Step.GetBuffer(), 6);

// 					//수집시간 전류
// 					Str_Step.Format("%6s", "     0");
// 					memcpy(m_total_Step[a].R_Current_Worth_Hour[i].Current_Worth_Hour, Str_Step.GetBuffer(), 6);

					//종지전류값
					Str_Step.Format("%6d", fabs(lpChannel1->fCurrent));
					memcpy(m_total_Step[a].R_Discharge_Current_Worth[i].Discharge_Current_Worth, Str_Step.GetBuffer(), 6);

					//이전공정 불량과 REST,OCV는 Current 저장 안함
					if(strcmp(pconCode, "01") == 0 || m_total_Step[a].R_Action[0] == '3' || m_total_Step[a].R_Action[0] == '4')
					{
// 						Str_Step.Format("%6s", "     0");
// 						memcpy(m_total_Step[a].R_Current_Worth_Hour[i].Current_Worth_Hour, Str_Step.GetBuffer(), 6);

						Str_Step.Format("%6s", "     0");
						memcpy(m_total_Step[a].R_Discharge_Current_Worth[i].Discharge_Current_Worth, Str_Step.GetBuffer(), 6);
					}

					//Step종료시간
					Str_Step.Format("%6d", (int)lpChannel1->fStepTime);
					memcpy(m_total_Step[a].R_Step_End_Time[i].Step_End_Time, Str_Step.GetBuffer(), 6);

					Str_Step.Format("%5d", (int)lpChannel1->fTimeGetChargeVoltage);
					memcpy(m_total_Step[a].R_Step_Voltage_Time[i].Step_Voltage_Time, Str_Step.GetBuffer(), 5);

					//CC Time
					Str_Step.Format("%6d", (int)lpChannel1->fCcRunTime);
					memcpy(m_total_Step[a].R_Step_Current_Time[i].Step_Current_Time, Str_Step.GetBuffer(), 6);

					//////////////////////////////////////////////////////////////////////////
					Str_Step.Format("%6d", (int)lpChannel1->fCcCapacity);
					memcpy(m_total_Step[a].R_Step_Current_Capacity_CC[i].Step_Current_Capacity_CC, Str_Step.GetBuffer(), 6);

					Str_Step.Format("%6d", (int)lpChannel1->fCvRunTime);
					memcpy(m_total_Step[a].R_Step_CV_Time[i].Step_CV_Time, Str_Step.GetBuffer(), 6);

					Str_Step.Format("%6d", (int)lpChannel1->fCvCapacity);
					memcpy(m_total_Step[a].R_Step_CV_Capacity[i].Step_CV_Capacity, Str_Step.GetBuffer(), 6);

					Str_Step.Format("%6d", (int)lpChannel1->fDeltaOCV); //20210223 ksj
					memcpy(m_total_Step[a].R_Step_DeltaVoltage[i].Step_DeltaVoltage, Str_Step.GetBuffer(), 7);	

				}
			}
			else
			{
				Str_Step.Format("%5s", "    0");
				memcpy(m_total_Step[a].R_Voltage_Worth[i].Voltage_Worth, Str_Step.GetBuffer(), 5);

// 				Str_Step.Format("%6s", "     0");
// 				memcpy(m_total_Step[a].R_Current_Worth_Five_Min[i].Current_Worth_Five_Min, Str_Step.GetBuffer(), 6);

				Str_Step.Format("%6s", "     0");
				memcpy(m_total_Step[a].R_Capacity_Worth[i].Capacity_Worth, Str_Step.GetBuffer(), 6);

// 				Str_Step.Format("%6s", "     0");
// 				memcpy(m_total_Step[a].R_Current_Worth_Hour[i].Current_Worth_Hour, Str_Step.GetBuffer(), 6);

				Str_Step.Format("%6s", "     0");
				memcpy(m_total_Step[a].R_Discharge_Current_Worth[i].Discharge_Current_Worth, Str_Step.GetBuffer(), 6);

				Str_Step.Format("%6s", "     0");
				memcpy(m_total_Step[a].R_Step_End_Time[i].Step_End_Time, Str_Step.GetBuffer(), 6);

				Str_Step.Format("%5s", "    0");
				memcpy(m_total_Step[a].R_Step_Voltage_Time[i].Step_Voltage_Time, Str_Step.GetBuffer(), 5);

				Str_Step.Format("%6s", "     0");
				memcpy(m_total_Step[a].R_Step_Current_Time[i].Step_Current_Time, Str_Step.GetBuffer(), 6);

				//////////////////////////////////////////////////////////////////////////
				Str_Step.Format("%6s", "     0");
				memcpy(m_total_Step[a].R_Step_Current_Capacity_CC[i].Step_Current_Capacity_CC, Str_Step.GetBuffer(), 6);

				Str_Step.Format("%6s", "     0");
				memcpy(m_total_Step[a].R_Step_CV_Time[i].Step_CV_Time, Str_Step.GetBuffer(), 6);

				Str_Step.Format("%6s", "     0");
				memcpy(m_total_Step[a].R_Step_CV_Capacity[i].Step_CV_Capacity, Str_Step.GetBuffer(), 6);

				Str_Step.Format("%6d", (int)lpChannel1->fDeltaOCV); //20210223 ksj
				memcpy(m_total_Step[a].R_Step_DeltaVoltage[i].Step_DeltaVoltage, Str_Step.GetBuffer(), 7);	
				//////////////////////////////////////////////////////////////////////////				
			}
		}
	}

	sprintf_s(m_Result_Data_L.Total_Step, "%03d", stepCnt);

	memset(&m_Result_Data_R, 0x00, sizeof(st_RESULT_FILE_RESPONSE_R));

	CString Str_Result_Data_R;
	Str_Result_Data_R.Format("%3d",stepCnt);
	memcpy(m_Result_Data_R.Now_Step_No, Str_Result_Data_R.GetBuffer(), 3);

	//저장한 스텝의 제일 마지막
	// lpStepData1 = pModule->m_resultFile.GetStepData(pModule->m_resultFile.GetStepSize() - 1);
	lpStepData1 = pModule->m_resultFile.GetStepData(stepCnt);

	if(lpStepData1 != NULL)
	{
		INT ContactErrorCnt = 0;
		for(int h = 0; h < MAX_CELL_COUNT; h++)
		{
			CHAR szTemp[3] = {0};

			if(h < lpStepData1->aChData.GetSize())
			{
				lpChannel1 = (STR_SAVE_CH_DATA *)lpStepData1->aChData.GetAt(h);

				sprintf(szTemp, "%2s", fnGetChannelCode(lpChannel1->channelCode));
			}
			else
			{
                sprintf(szTemp, "%2s", "  ");
			}
            memcpy(m_Result_Data_R.arCellInfo[h].Cell_Info, szTemp, 3);
		}

		CString StartTime, EndTime;
		EndTime.Format("%s", lpStepData1->stepHead.szEndDateTime);
		EndTime = Time_Conversion(EndTime);
		Str_Result_Data_R.Format("%14s",EndTime);
		memcpy(m_Result_Data_R.Test_Finish_Date, Str_Result_Data_R.GetBuffer(), 14);

		lpStepData1 = pModule->m_resultFile.GetFirstStepData();
		StartTime.Format("%s", lpStepData1->stepHead.szStartDateTime);
		StartTime = Time_Conversion(StartTime);
		Str_Result_Data_R.Format("%14s",StartTime);
		memcpy(m_Result_Data_R.Test_Begin_Date, Str_Result_Data_R.GetBuffer(), 14);
	}	
	//////////////////////////////////////////////////////////////////////////
	//if(ContactErrorCnt > atoi(pReserveInfo->B_Value.Contact_Error_Setting))
	//{
	//	_error = ER_Contact_Error;
	//}

	char* msg1;
	msg1 = new CHAR[4096];
	memset(msg1, 0x00, 4096);
	CString str_m_Result_Data_L_String;

	strLinker(msg1, &m_Result_Data_L, g_iResult_File_Response_L_Map);
	TRACE("%s ",msg1);
	str_m_Result_Data_L_String.Format("%s",msg1);
	memset(msg1, 0x00, 4096);

	char* msg;
	msg = new CHAR[4096];
	memset(msg, 0x00, 4096);
	CString str_Step, str_Step_String;
	INT strtotLen = 0;
	for(int i = 0; i < stepCnt; i++)
	{
		strtotLen += strLinker(msg, m_total_Step[i].R_Step_No, g_iResult_R_Step_No_Map);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strtotLen += strLinker(msg, m_total_Step[i].R_Action, g_iResult_R_Action_Map);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strtotLen += strLinker(msg, m_total_Step[i].R_JigTemp1, g_iResult_R_Temp_Map);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strtotLen += strLinker(msg, m_total_Step[i].R_JigTemp2, g_iResult_R_Temp_Map);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strtotLen += strLinker(msg, m_total_Step[i].R_JigTemp3, g_iResult_R_Temp_Map);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strtotLen += strLinker(msg, m_total_Step[i].R_JigTemp4, g_iResult_R_Temp_Map);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strtotLen += strLinker(msg, m_total_Step[i].R_JigTemp5, g_iResult_R_Temp_Map);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strtotLen += strLinker(msg, m_total_Step[i].R_JigTemp6, g_iResult_R_Temp_Map);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strtotLen += strLinker(msg, m_total_Step[i].R_JigTempMax, g_iResult_R_Temp_Map);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strtotLen += strLinker(msg, m_total_Step[i].R_JigTempMin, g_iResult_R_Temp_Map);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strtotLen += strLinker(msg, m_total_Step[i].R_JigTempAvg, g_iResult_R_Temp_Map);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strtotLen += strLinker(msg, m_total_Step[i].R_Voltage_Worth, g_iVoltage_Worth_Map);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strtotLen += strLinker(msg, m_total_Step[i].R_Capacity_Worth, g_iCapacity_Worth_Map);
		str_Step_String.Format("%s",msg);
		str_Step = str_Step + str_Step_String;
		memset(msg, 0x00, 4096);

// 		strtotLen += strLinker(msg, m_total_Step[i].R_Current_Worth_Hour, g_iCurrent_Worth_Hour_Map);
// 		str_Step_String.Format("%s",msg);
// 		str_Step = str_Step + str_Step_String;
// 		memset(msg, 0x00, 4096);

		strtotLen += strLinker(msg, m_total_Step[i].R_Discharge_Current_Worth, g_iDischarge_Current_Worth_Map);
		str_Step_String.Format("%s",msg);
		str_Step = str_Step + str_Step_String;
		memset(msg, 0x00, 4096);

		strtotLen += strLinker(msg, m_total_Step[i].R_Step_End_Time, g_iStep_End_Time_Map);
		str_Step_String.Format("%s",msg);
		str_Step = str_Step + str_Step_String;
		memset(msg, 0x00, 4096);

		strtotLen += strLinker(msg, m_total_Step[i].R_Step_Voltage_Time, g_iStep_Voltage_Time_Map);
		str_Step_String.Format("%s",msg);
		str_Step = str_Step + str_Step_String;
		memset(msg, 0x00, 4096);

		strtotLen += strLinker(msg, m_total_Step[i].R_Step_Current_Time, g_iStep_Current_Time_Map);
		str_Step_String.Format("%s",msg);
		str_Step = str_Step + str_Step_String;
		memset(msg, 0x00, 4096);

		//////////////////////////////////////////////////////////////////////////

		strtotLen += strLinker(msg, m_total_Step[i].R_Step_Current_Capacity_CC, g_iStep_Current_Capacity_CC_Map);
		str_Step_String.Format("%s",msg);
		str_Step = str_Step + str_Step_String;
		memset(msg, 0x00, 4096);

		strtotLen += strLinker(msg, m_total_Step[i].R_Step_CV_Time, g_iStep_CV_Time_Map);
		str_Step_String.Format("%s",msg);
		str_Step = str_Step + str_Step_String;
		memset(msg, 0x00, 4096);

		strtotLen += strLinker(msg, m_total_Step[i].R_Step_CV_Capacity, g_iStep_CV_Capacity_Map);
		str_Step_String.Format("%s",msg);
		str_Step = str_Step + str_Step_String;
		memset(msg, 0x00, 4096);	

		strtotLen += strLinker(msg, m_total_Step[i].R_Step_DeltaVoltage, g_iStep_DeltaVoltage_Map);
		str_Step_String.Format("%s",msg);
		str_Step = str_Step + str_Step_String;
		memset(msg, 0x00, 4096);

		//////////////////////////////////////////////////////////////////////////
		/*
		strtotLen += strLinker(msg, m_total_Step[i].R_Step_Capacity_T, g_iStep_Capacity_T_Map);
		str_Step_String.Format("%s",msg);
		str_Step = str_Step + str_Step_String;
		memset(msg, 0x00, 4096);

		strtotLen += strLinker(msg, m_total_Step[i].R_Step_CC_Capacity_T, g_iStep_CC_Capacity_T_Map);
		str_Step_String.Format("%s",msg);
		str_Step = str_Step + str_Step_String;
		memset(msg, 0x00, 4096);

		strtotLen += strLinker(msg, m_total_Step[i].R_Step_CV_Capacity_T, g_iStep_CV_Capacity_T_Map);
		str_Step_String.Format("%s",msg);
		str_Step = str_Step + str_Step_String;
		memset(msg, 0x00, 4096);
		*/
	}

	char* msg2;
	msg2 = new CHAR[4096];
	memset(msg2, 0x00, 4096);
	CString str_Result_Data_R_String;

	INT len = strLinker(msg2, &m_Result_Data_R, g_iResult_File_Response_R_Map);
	TRACE("%s ",msg2);
	str_Result_Data_R_String.Format("%s",msg2);
	memset(msg2, 0x00, 4096);

	m_str_Result_Data = str_m_Result_Data_L_String + str_Step + str_Result_Data_R_String;

	delete [] msg;
	delete [] msg1;
	delete [] msg2;

	CFMSLog::WriteLog("[Stage %s] fnMakeResult(%d) End For FMS", m_pModule->GetModuleName(), m_str_Result_Data.GetLength() );

    return _error;

#endif
}

//0 정상변경
//1 변경실패
//2 작업 없음
FMS_ERRORCODE CFMS::fnSetOperationMode(CHAR _code)
{
	INT nOperationMode  = 0;

	if(m_pModule->GetOperationMode() != _code)
	{
		switch(_code)
		{
		case '0':
			nOperationMode = EP_OPERATION_AUTO;
			if(EPSetLineMode(m_pModule->GetModuleID(), 0+1, EP_OPERATION_MODE, nOperationMode) == FALSE)
			{
				CFMSLog::WriteLog("[Stage %s] Control Mode Setting Fail.", m_pModule->GetModuleName());		
				return ER_Stage_Setting_Error;
			}
			break;
		case '1':
			nOperationMode = EP_OPERATION_LOCAL;
			if(EPSetLineMode(m_pModule->GetModuleID(), 0+1, EP_OPERATION_MODE, nOperationMode) == FALSE)
			{
				CFMSLog::WriteLog("[Stage %s] Control Mode Setting Fail.", m_pModule->GetModuleName());		
				return ER_Stage_Setting_Error;
			}
			break;
		case '2':
			break;
		case '3':
			break;
		default:
			return ER_Settiing_Data_error;
		}
	}


	return FMS_ER_NONE;
}

FMS_ERRORCODE CFMS::SendSBCInitCommand()
{
	CFormModule *pModule = m_pModule;
	if(pModule == NULL)
	{
		//return ER_Stage_Setting_Error;
		return ER_Status_Error;
	}

	WORD state = EPGetGroupState(pModule->GetModuleID());
	BYTE byTemp;
	//	if(state != EP_STATE_STANDBY && state != EP_STATE_READY && state != EP_STATE_END)
	if(state == EP_STATE_RUN)	
	{
		//CFMSLog::WriteErrorLog("%s :: [초기화] 명령 전송가능 상태가 아닙니다.(state : %s)", ::GetModuleName(nModuleID, nGroupIndex), GetStateMsg(state, byTemp));
		//return ER_Stage_Setting_Error;
		return ER_Status_Error;
	}

	int nRtn;
	if((nRtn = EPSendCommand(pModule->GetModuleID(), 0+1, 0, EP_CMD_CLEAR)) != EP_ACK)
	{
		//m_strLastErrorString.Format(GetStringTable(IDS_MSG_COMMAND_SEND_FAILE), ::GetModuleName(nModuleID, nGroupIndex));
		//m_strLastErrorString = m_strLastErrorString + " ("+ CmdFailMsg(nRtn) +")";
		//AfxMessageBox(m_strLastErrorString);
		//return ER_Stage_Setting_Error;
	}

	// Sleep(100);

	//예약된 공정을 Reset 시킨다.
	// 종료후 정보 유지 [2013/9/23 cwm]
	//pModule->ResetGroupData();

	return FMS_ER_NONE;
}

FMS_ERRORCODE CFMS::SendGUIInitCommand()
{
	m_pModule->ResetGroupData();

	return FMS_ER_NONE;
}

FMS_ERRORCODE CFMS::fnSend_E_HEAET_BEAT_P(FMS_ERRORCODE _rec)
{
	FMS_ERRORCODE _error = FMS_ER_NONE;

	return _error;
}

FMS_ERRORCODE CFMS::fnSend_E_TIME_SET_RESPONSE(FMS_ERRORCODE _rec)
{

	FMS_ERRORCODE _error = FMS_ER_NONE;


	return _error;
}
FMS_ERRORCODE CFMS::fnSend_E_ERROR_NOTICE_RESPONSE(FMS_ERRORCODE _rec)
{

	FMS_ERRORCODE _error = FMS_ER_NONE;


	return _error;
}
VOID CFMS::fnSet_E_EQ_ERROR_Dtat(CString _errCode)
{
	INT type = atoi(_errCode);
	switch(type)
	{
	case 1:
		m_Result_FIle_Type = '2';
		break;
	default:
		m_Result_FIle_Type = '1';
		break;
	}
	memset(&m_ErrorData, 0x20, sizeof(st_EQ_ERROR));

	sprintf(m_ErrorData.Equipment_Num, "%03d", fnGetLineNo());

	memcpy(m_ErrorData.Error_Code, _errCode.GetBuffer(), min(2, _errCode.GetLength()));
	
	m_bError = TRUE;
}

FMS_ERRORCODE CFMS::fnSend_E_EQ_ERROR(FMS_ERRORCODE _rec)
{
	FMS_ERRORCODE _error = FMS_ER_NONE;

	if(m_ErrorData.Equipment_Num[0] != 0x20)
	{
		st_FMS_PACKET* pPack = fnGetPack();
		memset(pPack->data, 0x20, sizeof(pPack->data));
		UINT dataLen = strLinker(pPack->data
			, &m_ErrorData, g_iEQ_ERROR_Map);

		pPack->dataLen = dataLen;

		fnMakeHead(pPack->head
			, EQ_ERROR, dataLen, RESULT_CODE_OK);

		theApp.m_FMS_SendQ.Push(*pPack);
		memset(&m_ErrorData, 0x20, sizeof(st_EQ_ERROR));
	}

	return _error;
}

FMS_ERRORCODE CFMS::fnSend_E_CHARGER_IMPOSSBLE_RESPONSE(FMS_ERRORCODE _rec)
{
	FMS_ERRORCODE _error = FMS_ER_NONE;

	return _error;
}

FMS_ERRORCODE CFMS::fnSend_E_CHARGER_INRESEVE_RESPONSE(FMS_ERRORCODE _rec)
{
	FMS_ERRORCODE _error = FMS_ER_NONE;

	st_CHARGER_INRESEVE_RESPONSE data;
	memset(&data, 0x20, sizeof(st_CHARGER_INRESEVE_RESPONSE));

	sprintf(data.Equipment_Num, "%03d", fnGetLineNo());

	st_FMS_PACKET* pPack = fnGetPack();
	memset(pPack->data, 0x20, sizeof(pPack->data));
	UINT dataLen = strLinker(pPack->data
		, &data, g_iCHARGER_INRESEVE_RESPONSE_Map);
	pPack->dataLen = dataLen;

	CHAR _result = RESULT_UNKNOW_ERROR;
	switch(_rec)
	{
	case FMS_ER_NONE:
		_result = RESULT_CODE_OK;
		break;
		//case ER_Already_Reserve:
		//	_result = RESULT_CODE_ALREADY_RESERVED;
		//	break;
	case ER_fnLoadWorkInfo:
		_result = RESULT_CODE_INSPECTION;
		break;
	case ER_fnTransFMSWorkInfoToCTSWorkInfo:
		_result = RESULT_CODE_INSPECTION;
		break;
	case ER_fnMakeTrayInfo:
		_result = RESULT_CODE_INSPECTION;
		break;
	case ER_fnSendConditionToModule:
		//_result = RESULT_CODE_STEP_SEND;
		_result = RESULT_CODE_OK;
		//에러 발생
		//fnSetLastState(FMS_ST_ERROR);
		//fnSet_E_EQ_ERROR_Dtat("26");
		//fnSetError();
		break;
	case ER_NO_Process:
		_result = RESULT_CODE_STATUS_ERROR;
		break;
	case ER_FMS_Recv_Error_State:
		//_result = RESULT_CODE_ERROR_STATE;
		_result = RESULT_CODE_STATUS_ERROR;
		break;

	}

	if(_rec)
	{
		CFMSLog::WriteErrorLog("[%03d][%s]%s"
			, fnGetLineNo()
			, g_str_FMS_State[m_FMSStateCode]
		, g_str_FMS_ERROR[_rec]);
	}
	fnMakeHead(pPack->head, CHARGER_INRESEVE_RESPONSE , dataLen, _result);

	theApp.m_FMS_SendQ.Push(*pPack);

	return _error;
}

FMS_ERRORCODE CFMS::fnSend_E_CHARGER_IN_RESPONSE(FMS_ERRORCODE _rec)
{
	FMS_ERRORCODE _error = FMS_ER_NONE;

	st_CHARGER_IN_RESPONSE data;
	memset(&data, 0x20, sizeof(st_CHARGER_IN_RESPONSE));

	sprintf(data.Equipment_Num
		, "%03d", fnGetLineNo());

	st_FMS_PACKET* pPack = fnGetPack();
	memset(pPack->data, 0x20, sizeof(pPack->data));
	UINT dataLen = strLinker(pPack->data
		, &data, g_iCHARGER_IN_RESPONSE_Map);
	pPack->dataLen = dataLen;

	CHAR _result = RESULT_UNKNOW_ERROR;
	switch(_rec)
	{
	case FMS_ER_NONE:
		_result = RESULT_CODE_OK;
		break;
	case ER_TrayID:
		_result = RESULT_CODE_DATA_ERROR;
		break;
		//case ER_Already_Reserve:
		//	_result = RESULT_CODE_ALREADY_RESERVED;
		//	break;
		//case ER_fnLoadWorkInfo:
		//	_result = RESULT_CODE_INSPECTION;
		//	break;
		//case ER_fnTransFMSWorkInfoToCTSWorkInfo:
		//	_result = RESULT_CODE_INSPECTION;
		//	break;
		//case ER_fnMakeTrayInfo:
		//	_result = RESULT_CODE_INSPECTION;
		//	break;
	case ER_Run_Fail:
		//_result = RESULT_CODE_STEP_SEND;
		_result = RESULT_CODE_OK;
		//	//에러 발생
		//	//fnSetLastState(FMS_ST_ERROR);
		//	fnSet_E_EQ_ERROR_Dtat("24");
		break;
	case ER_NO_Process:
		_result = RESULT_CODE_STATUS_ERROR;
		break;
	}

	if(_rec)
	{
		CFMSLog::WriteErrorLog("[%03d][%s]%s"
			, fnGetLineNo()
			, g_str_FMS_State[m_FMSStateCode]
		, g_str_FMS_ERROR[_rec]);
	}

	fnMakeHead(pPack->head, CHARGER_IN_RESPONSE, dataLen, _result);

	theApp.m_FMS_SendQ.Push(*pPack);

	return _error;
}

FMS_ERRORCODE CFMS::fnSend_E_CHARGER_WORKEND(FMS_ERRORCODE _rec)
{
	FMS_ERRORCODE _error = FMS_ER_NONE;

	//종료 메시지
	st_CHARGER_WORKEND data;
	memset(&data, 0x20, sizeof(st_CHARGER_WORKEND));

	sprintf(data.Equipment_Num, "%03d", fnGetLineNo());
	strcpy_s(data.Tray_ID, fnGetTrayID());

	CHAR _result = RESULT_UNKNOW_ERROR;

	// 1. 결과 생성
	_rec = fnMakeResult();

	switch(_rec)
	{
	case FMS_ER_NONE:
		_result = RESULT_CODE_OK;
		break;
	default:
		{
			_result = RESULT_CODE_OK;
		}
		break;
	//case ER_File_Not_Found:
	//	_result = RESULT_CODE_FILE_NOT_FOUND;
	//	break;
	//case ER_File_Open_error:
	//	_result = RESULT_CODE_FILE_OPEN_ERROR;
	//	break;
	//case ER_Contact_Error:
	//	_result = RESULT_CODE_CONTACT_ERROR;
	//	break;
	//case ER_Capa_Error:
	//	_result = RESULT_CODE_CAPA_ERROR;
	//	break;
	//case ER_Over_Charge:
	//	_result = RESULT_CODE_OVER_CHARGE;
	//	break;
	}

	if(_rec)
	{
		CFMSLog::WriteErrorLog("[%03d][%s]%s"
			, fnGetLineNo()
			, g_str_FMS_State[m_FMSStateCode]
		, g_str_FMS_ERROR[_rec]);
	}

	st_FMS_PACKET* pPack = fnGetPack();
	memset(pPack->data, 0x20, sizeof(pPack->data));
	UINT dataLen = strLinker(pPack->data
		, &data, g_iCHARGER_WORKEND_Map);
	pPack->dataLen = dataLen;

	fnMakeHead(pPack->head, CHARGER_WORKEND, dataLen, _result);

	theApp.m_FMS_SendQ.Push(*pPack);

	return _error;
}

FMS_ERRORCODE CFMS::fnSend_E_CHARGER_RESULT_RESPONSE(FMS_ERRORCODE _rec)
{
	FMS_ERRORCODE _error = FMS_ER_NONE;

	CHAR _result = RESULT_UNKNOW_ERROR;

	INT dataLen = 0;

	switch(_rec)
	{
	case ER_TrayID:
		_result = RESULT_CODE_TRAY_ID_ERROR;
		//_result = RESULT_CODE_DATA_ERROR;
		break;
	case ER_NO_Process:
		_result = RESULT_CODE_FILE_OPEN_ERROR;
		break;
	default:
		dataLen = fnGetResultData().GetLength();
		if(dataLen <= 0)
		{
			_result = RESULT_CODE_FILE_OPEN_ERROR;
			CFMSLog::WriteErrorLog("Result DataLen error[%03d][%s]%s"
				, fnGetLineNo()
				, g_str_FMS_State[m_FMSStateCode]
			, g_str_FMS_ERROR[_rec]);
		}
		else
		{
			_result = RESULT_CODE_OK;
		}
		break;	
	}

	if(_rec)
	{
		CFMSLog::WriteErrorLog("[%03d][%s]%s"
			, fnGetLineNo()
			, g_str_FMS_State[m_FMSStateCode]
		, g_str_FMS_ERROR[_rec]);
	}

	st_FMS_PACKET* pPack = fnGetPack();
	fnMakeHead(pPack->head, CHARGER_RESULT_RESPONSE, dataLen, _result);

	CHAR head[51];
	memset(&head, 0x20, 50);
	head[50] = 0;
	strLinker(head, &pPack->head, g_iHead_Map);

	CString strResult;
	if(_result == RESULT_CODE_OK)
	{		
		strResult = head + fnGetResultData() + "*;";
		const CHAR *temp = strResult.GetBuffer();
		theApp.m_FMSserver.fnSendResult(temp);
	}
	else
	{
		CHAR dumy[134 + 1] = {0};
		memset(dumy, 0x20, 134);
		CString	strDumy = dumy;
		strResult = head + strDumy + "*;";
		const CHAR *temp = strResult.GetBuffer();
		theApp.m_FMSserver.fnSendResult(temp);
	}
	
	CFMSLog::SendFMSLog(strResult.GetBuffer(), E_CHARGER_RESULT_RESPONSE);		// CHARGER_RESULT_RESPONSE 메세지를 로그로 저장
	
	return _error;
}

FMS_ERRORCODE CFMS::fnSend_E_CHARGER_RESULT_RECV_RESPONSE(FMS_ERRORCODE _rec)
{
	FMS_ERRORCODE _error = FMS_ER_NONE;

	st_CHARGER_RESULT_RECV_RESPONSE data;
	memset(&data, 0x20, sizeof(st_CHARGER_RESULT_RECV_RESPONSE));

	sprintf(data.Equipment_Num, "%03d", fnGetLineNo());
	strcpy_s(data.Tray_ID, fnGetTrayID());

	CHAR _result = RESULT_UNKNOW_ERROR;
	switch(_rec)
	{
	case FMS_ER_NONE:
		_result = RESULT_CODE_OK;
		break;
	case ER_TrayID:
		//_result = RESULT_CODE_TRAY_ID_ERROR;
		_result = RESULT_CODE_DATA_ERROR;
		break;
	case ER_NO_Process:
		//_result = RESULT_CODE_STATUS_ERROR;
		_result = RESULT_CODE_DATA_ERROR;
		break;
	}

	if(_rec)
	{
		CFMSLog::WriteErrorLog("[%03d][%s]%s"
			, fnGetLineNo()
			, g_str_FMS_State[m_FMSStateCode]
		, g_str_FMS_ERROR[_rec]);
	}

	st_FMS_PACKET* pPack = fnGetPack();
	memset(pPack->data, 0x20, sizeof(pPack->data));
	UINT dataLen = strLinker(pPack->data
		, &data, g_iCHARGER_RESULT_RECV_RESPONSE_Map);

	pPack->dataLen = dataLen;
	fnMakeHead(pPack->head
		, CHARGER_RESULT_RECV_RESPONSE
		, dataLen, _result);

	theApp.m_FMS_SendQ.Push(*pPack);

	return _error;
}

FMS_ERRORCODE CFMS::fnSend_E_CHARGER_OUT_RESPONSE(FMS_ERRORCODE _rec)
{
	FMS_ERRORCODE _error = FMS_ER_NONE;

	SendSBCInitCommand();

	st_CHARGER_OUT_RESPONSE data;
	memset(&data, 0x20, sizeof(st_CHARGER_OUT_RESPONSE));

	sprintf(data.Equipment_Num, "%03d", fnGetLineNo());

	CHAR _result = RESULT_UNKNOW_ERROR;
	switch(_rec)
	{
	case FMS_ER_NONE:
		_result = RESULT_CODE_OK;
		break;
	//case ER_Data_Error:
	//	_result = RESULT_CODE_DATA_ERROR;
	//	break;un
	case ER_NO_Process:
		//_result = RESULT_CODE_STATUS_ERROR;
		_result = RESULT_CODE_OK;
		break;
	//case ER_Tray_In:
	//	_result = RESULT_CODE_TRAY_IN_ERROR;
	//	break;
	}

	st_FMS_PACKET* pPack = fnGetPack();
	memset(pPack->data, 0x20, sizeof(pPack->data));
	UINT dataLen = strLinker(pPack->data
		, &data, g_iCHARGER_OUT_RESPONSE_Map);
	pPack->dataLen = dataLen;

	fnMakeHead(pPack->head
		, CHARGER_OUT_RESPONSE, dataLen, _result);

	theApp.m_FMS_SendQ.Push(*pPack);

	return _error;
}

FMS_ERRORCODE CFMS::fnSend_E_CHARGER_MODE_CHANGE_RESPONSE(FMS_ERRORCODE _rec)
{
	FMS_ERRORCODE _error = FMS_ER_NONE;

	st_CHARGER_MODE_CHANGE_RESPONSE data;
	memset(&data, 0x20, sizeof(st_CHARGER_MODE_CHANGE_RESPONSE));

	sprintf(data.Equipment_Num, "%03d", fnGetLineNo());

	st_FMS_PACKET* pPack = fnGetPack();
	memset(pPack->data, 0x20, sizeof(pPack->data));
	UINT dataLen = strLinker(pPack->data
		, &data, g_iCHARGER_MODE_CHANGE_RESPONSE_Map);

	pPack->dataLen = dataLen;


	CHAR _result = RESULT_UNKNOW_ERROR;
	switch(_rec)
	{
	case FMS_ER_NONE:
		_result = RESULT_CODE_OK;
		break;
	case ER_Data_Error:
		_result = RESULT_CODE_DATA_ERROR;
		break;
	case ER_Status_Error:
		_result = RESULT_CODE_STATUS_ERROR;
		break;
	case ER_Settiing_Data_error:
		//_result = RESULT_CODE_SETTING_DATA_ERROR;
		_result = RESULT_CODE_DATA_ERROR;
		break;
	case ER_Stage_Setting_Error:
		//_result = RESULT_CODE_STAGE_SETTING_ERROR;
		_result = RESULT_CODE_OK;
		break;
	}

	fnMakeHead(pPack->head
		, CHARGER_MODE_CHANGE_RESPONSE, dataLen, _result);

	theApp.m_FMS_SendQ.Push(*pPack);


	return _error;
}

FMS_ERRORCODE CFMS::fnSend_E_CHARGER_STATE(FMS_ERRORCODE _rec)
{
	FMS_ERRORCODE _error = FMS_ER_NONE;
	return _error;
}

BOOL CFMS::fnResultDataStateUpdate(CHAR* _resultFileName, RESULTDATA_STATE _rState)
{
	INT sqlerr = 0;

	CString strSqliteDBPathNName = theApp.GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");
	CString dbname;
	dbname.Format("\\FMS_DB\\CTSMonDB_%03d.db3", fnGetLineNo());
	strSqliteDBPathNName += dbname;

	sqlite3* mSqlite;

	sqlerr = sqlite3_open(strSqliteDBPathNName, &mSqlite);

	sqlite3_stmt * stmt = 0;

	CString strQuery;

	if(_resultFileName == NULL)
	{
		strQuery.Format("Update FMSWorkInfo \
						Set \
						Send_Complet = %d \
						where moduleID = %d AND idx = %I64d;"
						, _rState
						, m_pModule->GetModuleID()
						, fnGetWorkCode());
	}
	else
	{
		strQuery.Format("Update FMSWorkInfo \
						Set \
						Result_File_Name = '%s' \
						, Send_Complet = %d \
						where moduleID = %d AND idx = %I64d;"
						, _resultFileName
						, _rState
						, m_pModule->GetModuleID()
						, fnGetWorkCode());
	}

	CHAR *errorMsg;
	sqlerr = sqlite3_exec(mSqlite, strQuery, NULL, NULL, &errorMsg);
	
	if(SQLITE_OK != sqlerr)
	{
		LPSTR szError = (LPSTR)sqlite3_errmsg(mSqlite);
		CFMSLog::WriteLog(szError);

		int nRowsChanged = sqlite3_changes(mSqlite);

		sqlerr = sqlite3_reset(stmt);

		if (sqlerr != SQLITE_OK)
		{
			szError = (LPSTR)sqlite3_errmsg(mSqlite);
			CFMSLog::WriteLog(szError);
		}
	}

	sqlite3_finalize(stmt);
	sqlite3_close(mSqlite);

	return TRUE;
}

//미전송 파일
//Send_Complet
//0 사용한적 없음 - reset
//1 정상 시작 - 공정 정상 시작되면 업데이트
//2 결과데이터 전송 - 결과확인 메시지 수신시 업데이트
#ifdef _DCIR
BOOL CFMS::fnLoadWorkInfo_DCIR(INT64 _code, st_DCIR_INRESEVE& _reserveInfo)
{
	INT loadOK = 0;
	INT sqlerr = 0;

	CString strSqliteDBPathNName = theApp.GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");

	CString dbname;
	dbname.Format("\\FMS_DB\\CTSMonDB_%03d.db3", fnGetLineNo());
	strSqliteDBPathNName += dbname;

	sqlite3* mSqlite;

	sqlerr = sqlite3_open(strSqliteDBPathNName, &mSqlite);

	sqlite3_stmt * stmt = 0;

	CString strQuery;
	strQuery.Format("select idx\
					, moduleID\
					, F_Info \
					, StepInfo \
					, B_Info \
					from FMSWorkInfo where idx = %I64d"
					, _code);

	sqlerr = sqlite3_prepare( mSqlite,  strQuery, -1, &stmt, 0 );

	if(m_inreserv.Step_Info)
	{
		delete [] m_inreserv.Step_Info;
		m_inreserv.Step_Info = NULL;
	}

	while((sqlerr = sqlite3_step( stmt )) == SQLITE_ROW)
	{
		INT c = 0;
		m_WorkCode = sqlite3_column_int64(stmt, c++);
		UINT64 idx = m_WorkCode;
		INT year = idx / 10000000000;
		idx -= year * 10000000000;
		INT month = idx / 100000000;
		idx -= month * 100000000;
		INT day = idx / 1000000;
		idx -= day * 1000000;

		INT hour = idx / 10000;
		idx -= hour * 10000;
		INT mint = idx / 100;
		idx -= mint * 100;
		INT sec = idx;
		m_OleRunStartTime.SetDateTime(year
			, month
			, day
			, hour
			, mint
			, sec);

		INT id = sqlite3_column_int(stmt, c++);

		INT blobsize = 0;
		blobsize = sqlite3_column_bytes(stmt, c);
		if(blobsize == sizeof(st_DCIR_INRESEVE_F))
		{
			ZeroMemory(&m_inreserv.F_Value, sizeof(st_DCIR_INRESEVE_F));
			memcpy(&m_inreserv.F_Value, sqlite3_column_blob(stmt, c), sizeof(st_DCIR_INRESEVE_F));
		}
		else loadOK++;
		c++;

		if(m_inreserv.Step_Info)
		{
			delete [] m_inreserv.Step_Info;
			m_inreserv.Step_Info = NULL;
			loadOK++;
			break;
		}

		INT totstep = atoi(m_inreserv.F_Value.Total_Step);
		blobsize = sqlite3_column_bytes(stmt, c);
		if(blobsize == sizeof(st_DCIR_Step_Set) * totstep)
		{
			st_DCIR_Step_Set* pStepInfo = new st_DCIR_Step_Set[totstep];
			ZeroMemory(pStepInfo, sizeof(st_DCIR_Step_Set) * totstep);
			memcpy(pStepInfo, sqlite3_column_blob(stmt, c), sizeof(st_DCIR_Step_Set) * totstep);
			m_inreserv.Step_Info = pStepInfo;
		}
		else loadOK++;
		c++;

		blobsize = sqlite3_column_bytes(stmt, c);
		if(blobsize == sizeof(st_DCIR_INRESEVE_B))
		{
			ZeroMemory(&m_inreserv.B_Value, sizeof(st_DCIR_INRESEVE_B));
			memcpy(&m_inreserv.B_Value, sqlite3_column_blob(stmt, c), sizeof(st_DCIR_INRESEVE_B));
		}
		else loadOK++;
	}

	if(SQLITE_DONE != sqlerr)
	{
		LPSTR szError = (LPSTR)sqlite3_errmsg(mSqlite);
		CFMSLog::WriteLog(szError);

		int nRowsChanged = sqlite3_changes(mSqlite);

		sqlerr = sqlite3_reset(stmt);

		if (sqlerr != SQLITE_OK)
		{
			szError = (LPSTR)sqlite3_errmsg(mSqlite);
			CFMSLog::WriteLog(szError);
		}
		else loadOK++;
	}

	sqlite3_finalize(stmt);
	sqlite3_close(mSqlite);

	if(loadOK)
		return FALSE;
	else
		return TRUE;
}
#else
BOOL CFMS::fnLoadWorkInfo_CHARGER(INT64 _code, st_CHARGER_INRESEVE& _reserveInfo)
{
	INT loadOK = 0;
	INT sqlerr = 0;

	CString strSqliteDBPathNName = theApp.GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");

	CString dbname;
	dbname.Format("\\FMS_DB\\CTSMonDB_%03d.db3", fnGetLineNo());
	strSqliteDBPathNName += dbname;

	sqlite3* mSqlite;

	sqlerr = sqlite3_open(strSqliteDBPathNName, &mSqlite);

	sqlite3_stmt * stmt = 0;

	CString strQuery;

	strQuery.Format("select idx\
					, moduleID\
					, F_Info \
					, StepInfo \
					, B_Info \
					from FMSWorkInfo where idx = %I64d"
					, _code);

	sqlerr = sqlite3_prepare( mSqlite,  strQuery, -1, &stmt, 0 );

	if(_reserveInfo.Step_Info)
	{
		delete [] _reserveInfo.Step_Info;
		_reserveInfo.Step_Info = NULL;
	}

	while((sqlerr = sqlite3_step( stmt )) == SQLITE_ROW)
	{
		INT c = 0;
		m_WorkCode = sqlite3_column_int64(stmt, c++);
		UINT64 idx = m_WorkCode;
		INT year = idx / 10000000000;
		idx -= year * 10000000000;
		INT month = idx / 100000000;
		idx -= month * 100000000;
		INT day = idx / 1000000;
		idx -= day * 1000000;

		INT hour = idx / 10000;
		idx -= hour * 10000;
		INT mint = idx / 100;
		idx -= mint * 100;
		INT sec = idx;
		m_OleRunStartTime.SetDateTime(year
			, month
			, day
			, hour
			, mint
			, sec);

		INT id = sqlite3_column_int(stmt, c++);

		INT blobsize = 0;
		blobsize = sqlite3_column_bytes(stmt, c);
		if(blobsize == sizeof(st_CHARGER_INRESEVE_F))
		{
			ZeroMemory(&_reserveInfo.F_Value, sizeof(st_CHARGER_INRESEVE_F));
			memcpy(&_reserveInfo.F_Value, sqlite3_column_blob(stmt, c), sizeof(st_CHARGER_INRESEVE_F));
		}
		else loadOK++;
		c++;

		if(_reserveInfo.Step_Info)
		{
			delete [] _reserveInfo.Step_Info;
			_reserveInfo.Step_Info = NULL;
			loadOK++;
			break;
		}

		INT totstep = atoi(_reserveInfo.F_Value.Total_Step);
		blobsize = sqlite3_column_bytes(stmt, c);
		if(blobsize == sizeof(st_Step_Set) * totstep)
		{
			st_Step_Set* pStepInfo = new st_Step_Set[totstep];
			ZeroMemory(pStepInfo, sizeof(st_Step_Set) * totstep);
			memcpy(pStepInfo, sqlite3_column_blob(stmt, c), sizeof(st_Step_Set) * totstep);
			_reserveInfo.Step_Info = pStepInfo;
		}
		else loadOK++;
		c++;

		blobsize = sqlite3_column_bytes(stmt, c);
		if(blobsize == sizeof(st_CHARGER_INRESEVE_B))
		{
			ZeroMemory(&_reserveInfo.B_Value, sizeof(st_CHARGER_INRESEVE_B));
			memcpy(&_reserveInfo.B_Value, sqlite3_column_blob(stmt, c), sizeof(st_CHARGER_INRESEVE_B));
		}
		else loadOK++;
	}

	if(SQLITE_DONE != sqlerr)
	{
		LPSTR szError = (LPSTR)sqlite3_errmsg(mSqlite);
		CFMSLog::WriteLog(szError);

		int nRowsChanged = sqlite3_changes(mSqlite);

		sqlerr = sqlite3_reset(stmt);

		if (sqlerr != SQLITE_OK)
		{
			szError = (LPSTR)sqlite3_errmsg(mSqlite);
			CFMSLog::WriteLog(szError);
		}
		else loadOK++;
	}

	sqlite3_finalize(stmt);
	sqlite3_close(mSqlite);

	if(loadOK)
		return FALSE;
	else
		return TRUE;
}
#endif

INT CFMS::fnLoadSendMisVector()
{
	INT sqlerr = 0;

	CString strSqliteDBPathNName = theApp.GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");

	CString dbname;
	dbname.Format("\\FMS_DB\\CTSMonDB_%03d.db3", fnGetLineNo());
	strSqliteDBPathNName += dbname;

	sqlite3* mSqlite;

	sqlerr = sqlite3_open(strSqliteDBPathNName, &mSqlite);

	sqlite3_stmt * stmt = 0;

	CString strQuery;

	COleDateTime OleCurrentTime = COleDateTime::GetCurrentTime();
	COleDateTimeSpan savePeriod;
	savePeriod.SetDateTimeSpan( -30, 0, 0, 0 );		// 저장기간은 30일
	OleCurrentTime += savePeriod;

	INT64 idx = OleCurrentTime.GetYear()	* 10000000000;
	idx += OleCurrentTime.GetMonth()		* 100000000;
	idx += OleCurrentTime.GetDay()			* 1000000;
	idx += OleCurrentTime.GetHour()			* 10000;
	idx += OleCurrentTime.GetMinute()		* 100;
	idx += OleCurrentTime.GetSecond();

	strQuery.Format("delete \
					from FMSWorkInfo where idx < %I64d \
					AND moduleID = %d"
					, idx, m_pModule->GetModuleID());	

	CHAR *errorMsg;
	sqlerr = sqlite3_exec(mSqlite, strQuery, NULL, NULL, &errorMsg);

	strQuery.Format("select idx, Result_File_Name\
					from FMSWorkInfo where Send_Complet = 2 \
					AND moduleID = %d ORDER BY idx desc limit 1"
					, m_pModule->GetModuleID());

	sqlerr = sqlite3_prepare( mSqlite,  strQuery, -1, &stmt, 0 );

	m_SendMisIdx = 0;
	m_SendMisCnt = 0;

	fnResetSendMisVector();

	while((sqlerr = sqlite3_step( stmt )) == SQLITE_ROW)
	{
		INT c = 0;
		st_SEND_MIS_INFO* pMisInfo = new st_SEND_MIS_INFO;

		pMisInfo->idxKey = sqlite3_column_int64(stmt, c++);

		CHAR* ans = (CHAR*)sqlite3_column_text(stmt, c++);

		if( ans != NULL )
		{
			// strcpy_s(pMisInfo->szFMTFilePath, (CHAR*)sqlite3_column_text(stmt, c++));		
			strcpy_s(pMisInfo->szFMTFilePath, ans);		
			m_vSendMis.push_back(pMisInfo);

			m_SendMisCnt++;

			if(m_SendMisCnt > 10) break;
		}
	}

	if(SQLITE_DONE != sqlerr)
	{
		LPSTR szError = (LPSTR)sqlite3_errmsg(mSqlite);
		CFMSLog::WriteLog(szError);

		int nRowsChanged = sqlite3_changes(mSqlite);

		sqlerr = sqlite3_reset(stmt);

		if (sqlerr != SQLITE_OK)
		{
			szError = (LPSTR)sqlite3_errmsg(mSqlite);
			CFMSLog::WriteLog(szError);
		}
	}

	sqlite3_finalize(stmt);
	sqlite3_close(mSqlite);

	return m_SendMisCnt;
}

VOID CFMS::fnResetSendMisVector()
{
	m_MisSendState = 0;

	for (UINT i = 0; i < m_vSendMis.size(); i++)
	{
		st_SEND_MIS_INFO* pMisInfo = m_vSendMis[i];
		delete pMisInfo;
	}
	m_vSendMis.clear();
}

BOOL CFMS::fnSendMisProc()
{
	BOOL bRes = FALSE;
	switch( m_MisSendState )
	{
	case 1://최초시작 검사종료 통지
		{
			if(m_SendMisIdx < m_vSendMis.size())
			{
#ifdef _DCIR

				st_DCIR_INRESEVE _reserve;
				memset(&_reserve, 0x00, sizeof(st_DCIR_INRESEVE));
				fnLoadWorkInfo_DCIR(m_vSendMis[m_SendMisIdx]->idxKey, _reserve);
				fnSend_E_CHARGER_WORKEND_MisSend_DCIR(m_vSendMis[m_SendMisIdx]->szFMTFilePath, _reserve);

#else
				st_CHARGER_INRESEVE _reserve;
				memset(&_reserve, 0x00, sizeof(st_CHARGER_INRESEVE));
				fnLoadWorkInfo_CHARGER(m_vSendMis[m_SendMisIdx]->idxKey, _reserve);
				fnSend_E_CHARGER_WORKEND_MisSend_CHARGER(m_vSendMis[m_SendMisIdx]->szFMTFilePath, _reserve);
#endif
				
				bRes = TRUE;
				m_MisSendState = 2;				
			}
			else
			{
				fnResetSendMisVector();
			}
		}
		break;
	case 2://감사 종료 통지 응답 대기
		{

		}
		break;
	case 3://검사 종료 통지 응답-> 결과 응답
		{
			if(fnSend_E_CHARGER_RESULT_RESPONSE_MisSend() == FMS_ER_NONE)
			{
				m_MisSendState = 4;
			}			
		}
		break;
	case 4:
		{
		
		}
		break;
	}

	return bRes;
}

#ifdef _DCIR
FMS_ERRORCODE CFMS::fnSend_E_CHARGER_WORKEND_MisSend_DCIR(CHAR* path, st_DCIR_INRESEVE& _reserve)
{

	FMS_ERRORCODE _error = FMS_ER_NONE;

	//종료 메시지
	st_CHARGER_WORKEND data;
	memset(&data, 0x20, sizeof(st_CHARGER_WORKEND));

	sprintf(data.Equipment_Num, "%03d", fnGetLineNo());
	strcpy_s(data.Tray_ID, _reserve.F_Value.Tray_ID);

	CHAR _result = '2';
	//결과 생성
	FMS_ERRORCODE _rec = fnMisSendMakeResult_DCIR(path, _reserve);

	if(_rec)
	{
		CFMSLog::WriteErrorLog("[%03d][%s]%s"
			, fnGetLineNo()
			, g_str_FMS_State[m_FMSStateCode]
		, g_str_FMS_ERROR[_rec]);
	}

	st_FMS_PACKET* pPack = fnGetPack();
	memset(pPack->data, 0x20, sizeof(pPack->data));
	UINT dataLen = strLinker(pPack->data
		, &data, g_iCHARGER_WORKEND_Map);
	pPack->dataLen = dataLen;

	fnMakeHead(pPack->head, CHARGER_WORKEND, dataLen, _result);

	theApp.m_FMS_SendQ.Push(*pPack);

	return _error;
}

#else
FMS_ERRORCODE CFMS::fnSend_E_CHARGER_WORKEND_MisSend_CHARGER(CHAR* path, st_CHARGER_INRESEVE& _reserve)
{
	FMS_ERRORCODE _error = FMS_ER_NONE;

	//종료 메시지
	st_CHARGER_WORKEND data;
	memset(&data, 0x20, sizeof(st_CHARGER_WORKEND));

	sprintf(data.Equipment_Num, "%03d", fnGetLineNo());
	strcpy_s(data.Tray_ID, _reserve.F_Value.Tray_ID);

	CHAR _result = '2';
	//결과 생성
	FMS_ERRORCODE _rec = fnMisSendMakeResult_CHARGE(path, _reserve);

	if(_rec)
	{
		CFMSLog::WriteErrorLog("[%03d][%s]%s"
			, fnGetLineNo()
			, g_str_FMS_State[m_FMSStateCode]
		, g_str_FMS_ERROR[_rec]);
	}

	st_FMS_PACKET* pPack = fnGetPack();
	memset(pPack->data, 0x20, sizeof(pPack->data));
	UINT dataLen = strLinker(pPack->data
		, &data, g_iCHARGER_WORKEND_Map);
	pPack->dataLen = dataLen;

	fnMakeHead(pPack->head, CHARGER_WORKEND, dataLen, _result);

	theApp.m_FMS_SendQ.Push(*pPack);

	return _error;
}
#endif

#ifdef _DCIR
FMS_ERRORCODE CFMS::fnMisSendMakeResult_DCIR(CString strFile, st_DCIR_INRESEVE& _reserve)
{
	FMS_ERRORCODE _error = FMS_ER_NONE;

	if(strFile.IsEmpty())
		return ER_File_Not_Found;

	m_str_MisSendResult_Data.Empty();

	CFormResultFile formFile;
	if(formFile.ReadFile(strFile, 0) < 0)
	{
		return ER_File_Not_Found;
	}

	CString strTmp;
	EP_FILE_HEADER		*lpFileHeader = NULL;
	STR_SAVE_CH_DATA	*lpChannel1 = NULL;
	STR_STEP_RESULT		*lpStepData1 = NULL;
	RESULT_FILE_HEADER	*lpFileData = NULL;
	STR_CONDITION_HEADER *lpModelData = NULL;

	lpFileHeader = formFile.GetFileHeader();
	if(lpFileHeader == NULL) return ER_File_Open_error;

	int nStepSize = formFile.GetStepSize();
	if(nStepSize <= 0) return ER_File_Open_error;

	lpStepData1 = formFile.GetFirstStepData();//시작시간 가져오기

	lpFileData = formFile.GetResultHeader();
	if(lpFileData == NULL) return ER_File_Open_error;

	BOOL bImpStep = FALSE;

	st_DCIR_INRESEVE* pReserveInfo = &_reserve;
	memset(&m_MisSend_Result_Data_L, 0x00, sizeof(st_DCIR_RESULT_FILE_RESPONSE_L));


	strTmp =formFile.GetModuleName(); 
	memset(m_MisSend_Result_Data_L.Stage_No, 0x20, sizeof(m_MisSend_Result_Data_L.Stage_No));
	memcpy(m_MisSend_Result_Data_L.Stage_No, strTmp.GetBuffer(), min(3, strTmp.GetLength()));
	m_MisSend_Result_Data_L.Stage_No[3] = 0x00;

	strcpy_s(m_MisSend_Result_Data_L.Type, pReserveInfo->F_Value.Type);
	strcpy_s(m_MisSend_Result_Data_L.Process_ID, pReserveInfo->F_Value.Process_No);
	strcpy_s(m_MisSend_Result_Data_L.Batch_No, pReserveInfo->F_Value.Batch_No);

	memset(m_MisSend_Result_Data_L.Tray_ID, 0x20, sizeof(m_MisSend_Result_Data_L.Tray_ID));
	memcpy(m_MisSend_Result_Data_L.Tray_ID, lpFileData->szTrayNo, min(7, strlen(lpFileData->szTrayNo)));
	m_MisSend_Result_Data_L.Tray_ID[7] = 0x00;

	INT stepCnt = 0;
	CString Str_Step;

	for(int a =0; a < nStepSize; a++)
	{
		lpStepData1 = formFile.GetStepData(a);

		//step data 손실 or end step
		if(lpStepData1 == NULL || lpStepData1->stepCondition.stepHeader.type == EP_TYPE_END)
			break;

		stepCnt++;	

		//step data 갱신
		memset(&m_MisSend_total_Step[a], 0x00, sizeof(st_DCIR_Result_Step_Set));

		int j = 0;		
		Str_Step.Format("%3d", a+1);
		memcpy(m_MisSend_total_Step[a].R_Step_No, Str_Step.GetBuffer(), 3);     //step no

		if(lpStepData1->stepCondition.fmsType != 0)                     //action
			m_MisSend_total_Step[a].R_Action[0] = lpStepData1->stepCondition.fmsType;
		else
			m_MisSend_total_Step[a].R_Action[0] = ' ';

		m_MisSend_total_Step[a].R_Action[1] = 0x00;		

		//Channel Data
		for(int i = 0; i < MAX_CELL_COUNT; i++)
		{
			if(i < lpStepData1->aChData.GetSize())
			{
				lpChannel1 = (STR_SAVE_CH_DATA *)lpStepData1->aChData.GetAt(i);

				CHAR* pconCode = fnGetChannelCode(lpChannel1->channelCode);     //FMS Cell Code
				CString strCode;
				strCode.Format("%2s", pconCode);

				if( i == 0 )
				{
					Str_Step.Format("%4.1f", lpChannel1->fJigTemp[0]);
					memcpy(m_MisSend_total_Step[a].R_JigTemp1, Str_Step.GetBuffer(), 4);
					Str_Step.Format("%4.1f", lpChannel1->fJigTemp[1]);
					memcpy(m_MisSend_total_Step[a].R_JigTemp2, Str_Step.GetBuffer(), 4);
					Str_Step.Format("%4.1f", lpChannel1->fJigTemp[2]);
					memcpy(m_MisSend_total_Step[a].R_JigTemp3, Str_Step.GetBuffer(), 4);
					Str_Step.Format("%4.1f", lpChannel1->fJigTemp[3]);
					memcpy(m_MisSend_total_Step[a].R_JigTemp4, Str_Step.GetBuffer(), 4);
					Str_Step.Format("%4.1f", lpChannel1->fJigTemp[4]);
					memcpy(m_MisSend_total_Step[a].R_JigTemp5, Str_Step.GetBuffer(), 4);
					Str_Step.Format("%4.1f", lpChannel1->fJigTemp[5]);
					memcpy(m_MisSend_total_Step[a].R_JigTemp6, Str_Step.GetBuffer(), 4);
					Str_Step.Format("%4.1f", lpChannel1->fTempMax);
					memcpy(m_MisSend_total_Step[a].R_JigTempMax, Str_Step.GetBuffer(), 4);
					Str_Step.Format("%4.1f", lpChannel1->fTempMin);
					memcpy(m_MisSend_total_Step[a].R_JigTempMin, Str_Step.GetBuffer(), 4);
					Str_Step.Format("%4.1f", lpChannel1->fTempAvg);
					memcpy(m_MisSend_total_Step[a].R_JigTempAvg, Str_Step.GetBuffer(), 4);					
				}

				if( strcmp(strCode, "02") == 0 || strcmp(strCode, "01") == 0 )
				{
					if(lpStepData1->stepCondition.stepHeader.type == EP_TYPE_IMPEDANCE)
					{
						bImpStep = TRUE;

						Str_Step.Format("%6s", "     0");
						memcpy(m_MisSend_Result_Data_L.DC_IR[i].DC_IR, Str_Step.GetBuffer(), 6);
						Str_Step.Format("%5s", "    0");
						memcpy(m_MisSend_Result_Data_L.V1[i].V1, Str_Step.GetBuffer(), 5);
						Str_Step.Format("%5s", "    0");
						memcpy(m_MisSend_Result_Data_L.V2[i].V2, Str_Step.GetBuffer(), 5);
						Str_Step.Format("%5s", "    0");
						memcpy(m_MisSend_Result_Data_L.Discharge_Time[i].Discharge_Time, Str_Step.GetBuffer(), 5);
						Str_Step.Format("%6s", "     0");
						memcpy(m_MisSend_Result_Data_L.CutOff[i].CutOff, Str_Step.GetBuffer(), 6);
						Str_Step.Format("%5s", "    0");
						memcpy(m_MisSend_Result_Data_L.Age_temp[i].Age_temp, Str_Step.GetBuffer(), 5);   
					}

					Str_Step.Format("%5s", "    0");
					memcpy(m_MisSend_total_Step[a].R_Voltage_Worth[i].Voltage_Worth, Str_Step.GetBuffer(), 5);
					Str_Step.Format("%6s", "     0");
					memcpy(m_MisSend_total_Step[a].R_Capacity_Worth[i].Capacity_Worth, Str_Step.GetBuffer(), 6);
					Str_Step.Format("%6s", "     0");
					memcpy(m_MisSend_total_Step[a].R_Discharge_Current_Worth[i].Discharge_Current_Worth, Str_Step.GetBuffer(), 6);
					Str_Step.Format("%5s", "    0");
					memcpy(m_MisSend_total_Step[a].R_Step_End_Time[i].Step_End_Time, Str_Step.GetBuffer(), 5);
				}
				else
				{
					if(lpStepData1->stepCondition.stepHeader.type == EP_TYPE_IMPEDANCE)
					{
						bImpStep = TRUE;

						Str_Step.Format("%6.3f", lpChannel1->fImpedance);
						memcpy(m_MisSend_Result_Data_L.DC_IR[i].DC_IR, Str_Step.GetBuffer(), 6);

						Str_Step.Format("%5.f", lpChannel1->fDCIR_V1);
						memcpy(m_MisSend_Result_Data_L.V1[i].V1, Str_Step.GetBuffer(), 5);

						Str_Step.Format("%5.f", lpChannel1->fDCIR_V2);
						memcpy(m_MisSend_Result_Data_L.V2[i].V2, Str_Step.GetBuffer(), 5);

						Str_Step.Format("%5.f", lpChannel1->fStepTime);
						memcpy(m_MisSend_Result_Data_L.Discharge_Time[i].Discharge_Time, Str_Step.GetBuffer(), 5);

						Str_Step.Format("%6.f", fabs(lpChannel1->fDCIR_AvgCurrent));
						memcpy(m_MisSend_Result_Data_L.CutOff[i].CutOff, Str_Step.GetBuffer(), 6);

						if(strcmp(pconCode, "01") == 0)        //이전공정 불량이면 전류값 0표시
						{
							Str_Step.Format("%6s", "     0"); 
							memcpy(m_MisSend_Result_Data_L.CutOff[i].CutOff, Str_Step.GetBuffer(), 6);
						}
						Str_Step.Format("%5.1f", lpChannel1->fDCIR_AvgTemp);
						memcpy(m_MisSend_Result_Data_L.Age_temp[i].Age_temp, Str_Step.GetBuffer(), 5);
					}   

					Str_Step.Format("%5.f", lpChannel1->fVoltage);
					memcpy(m_MisSend_total_Step[a].R_Voltage_Worth[i].Voltage_Worth, Str_Step.GetBuffer(), 5);

					Str_Step.Format("%6.f", lpChannel1->fCapacity);
					memcpy(m_MisSend_total_Step[a].R_Capacity_Worth[i].Capacity_Worth, Str_Step.GetBuffer(), 6);

					Str_Step.Format("%6.f", fabs(lpChannel1->fCurrent));
					memcpy(m_MisSend_total_Step[a].R_Discharge_Current_Worth[i].Discharge_Current_Worth, Str_Step.GetBuffer(), 6);

					//이전공정 불량과 REST,OCV는 Current 저장 안함
					if(strcmp(pconCode, "01") == 0 || m_total_Step[a].R_Action[0] == '3' || m_total_Step[a].R_Action[0] == '4')
					{
						Str_Step.Format("%6s", "     0");
						memcpy(m_MisSend_total_Step[a].R_Discharge_Current_Worth[i].Discharge_Current_Worth, Str_Step.GetBuffer(), 6);
					}

					Str_Step.Format("%5.f", lpChannel1->fStepTime);
					memcpy(m_MisSend_total_Step[a].R_Step_End_Time[i].Step_End_Time, Str_Step.GetBuffer(), 5);
				}
			}
			else        //사용하지 않는 Channel
			{
				if(lpStepData1->stepCondition.stepHeader.type == EP_TYPE_IMPEDANCE)
				{
					Str_Step.Format("%6s", "     0");
					memcpy(m_MisSend_Result_Data_L.DC_IR[i].DC_IR, Str_Step.GetBuffer(), 6);
					Str_Step.Format("%5s", "    0");
					memcpy(m_MisSend_Result_Data_L.V1[i].V1, Str_Step.GetBuffer(), 5);
					Str_Step.Format("%5s", "    0");
					memcpy(m_MisSend_Result_Data_L.V2[i].V2, Str_Step.GetBuffer(), 5);
					Str_Step.Format("%5s", "    0");
					memcpy(m_MisSend_Result_Data_L.Discharge_Time[i].Discharge_Time, Str_Step.GetBuffer(), 5);
					Str_Step.Format("%6s", "     0");
					memcpy(m_MisSend_Result_Data_L.CutOff[i].CutOff, Str_Step.GetBuffer(), 6);
					Str_Step.Format("%5s", "    0");
					memcpy(m_MisSend_Result_Data_L.Age_temp[i].Age_temp, Str_Step.GetBuffer(), 5);   
				}

				Str_Step.Format("%5s", "    0");
				memcpy(m_MisSend_total_Step[a].R_Voltage_Worth[i].Voltage_Worth, Str_Step.GetBuffer(), 5);
				Str_Step.Format("%6s", "     0");
				memcpy(m_MisSend_total_Step[a].R_Capacity_Worth[i].Capacity_Worth, Str_Step.GetBuffer(), 6);
				Str_Step.Format("%6s", "     0");
				memcpy(m_MisSend_total_Step[a].R_Discharge_Current_Worth[i].Discharge_Current_Worth, Str_Step.GetBuffer(), 6);
				Str_Step.Format("%5s", "    0");
				memcpy(m_MisSend_total_Step[a].R_Step_End_Time[i].Step_End_Time, Str_Step.GetBuffer(), 5);					
			}
		}
	}

	if( bImpStep == FALSE )
	{
		for(int i = 0; i < MAX_CELL_COUNT; i++)
		{
			Str_Step.Format("%6s", "     0");
			memcpy(m_MisSend_Result_Data_L.DC_IR[i].DC_IR, Str_Step.GetBuffer(), 6);
			Str_Step.Format("%5s", "    0");
			memcpy(m_MisSend_Result_Data_L.V1[i].V1, Str_Step.GetBuffer(), 5);
			Str_Step.Format("%5s", "    0");
			memcpy(m_MisSend_Result_Data_L.V2[i].V2, Str_Step.GetBuffer(), 5);
			Str_Step.Format("%5s", "    0");
			memcpy(m_MisSend_Result_Data_L.Discharge_Time[i].Discharge_Time, Str_Step.GetBuffer(), 5);
			Str_Step.Format("%6s", "     0");
			memcpy(m_MisSend_Result_Data_L.CutOff[i].CutOff, Str_Step.GetBuffer(), 6);
			Str_Step.Format("%5s", "    0");
			memcpy(m_MisSend_Result_Data_L.Age_temp[i].Age_temp, Str_Step.GetBuffer(), 5);   
		}
	}

	//////
	sprintf_s(m_MisSend_Result_Data_L.Total_Step, "%03d", stepCnt);
	memset(&m_MisSend_Result_Data_R, 0x00, sizeof(st_DCIR_RESULT_FILE_RESPONSE_R));

	CString Str_Result_Data_R;
	Str_Result_Data_R.Format("%3d",stepCnt);
	memcpy(m_MisSend_Result_Data_R.Now_Step_No, Str_Result_Data_R.GetBuffer(), 3);

	//저장한 스텝의 제일 마지막(End Step or 마지막 Step)
	lpStepData1 = formFile.GetStepData(formFile.GetStepSize() - 1);

	if(lpStepData1 != NULL)
	{
		INT ContactErrorCnt = 0;
		for(int h = 0; h < MAX_CELL_COUNT; h++)
		{
			CHAR szTemp[3] = {0};

			if(h < lpStepData1->aChData.GetSize())
			{
				lpChannel1 = (STR_SAVE_CH_DATA *)lpStepData1->aChData.GetAt(h);

				sprintf(szTemp, "%2s", fnGetChannelCode(lpChannel1->channelCode));
			}
			else
			{
				sprintf(szTemp, "%2s", "  ");
			}
			memcpy(m_MisSend_Result_Data_R.arCellInfo[h].Cell_Info, szTemp, 3);     //?
		}

		CString StartTime, EndTime;
		EndTime.Format("%s", lpStepData1->stepHead.szEndDateTime);
		EndTime = Time_Conversion(EndTime);
		Str_Result_Data_R.Format("%14s",EndTime);
		memcpy(m_MisSend_Result_Data_R.Test_Finish_Date, Str_Result_Data_R.GetBuffer(), 14);

		lpStepData1 = formFile.GetFirstStepData();
		StartTime.Format("%s", lpStepData1->stepHead.szStartDateTime);
		StartTime = Time_Conversion(StartTime);
		Str_Result_Data_R.Format("%14s",StartTime);
		memcpy(m_MisSend_Result_Data_R.Test_Begin_Date, Str_Result_Data_R.GetBuffer(), 14);
	}	
	//////

	char* msg1;
	msg1 = new CHAR[4096];
	memset(msg1, 0x00, 4096);
	CString str_Result_Data_L_String;


	strLinker(msg1, &m_MisSend_Result_Data_L, g_iDCIR_Result_File_Response_L_Map);
	TRACE("%s ",msg1);
	str_Result_Data_L_String.Format("%s",msg1);
	memset(msg1, 0x00, 4096);

	char* msg;
	msg = new CHAR[4096];
	memset(msg, 0x00, 4096);
	CString str_Step, str_Step_String;

	for(int i = 0; i < stepCnt; i++)
	{
		strLinker(msg, m_MisSend_total_Step[i].R_Step_No, g_iDCIR_Result_R_Step_No_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strLinker(msg, m_MisSend_total_Step[i].R_Action, g_iDCIR_Result_R_Action_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strLinker(msg, m_MisSend_total_Step[i].R_JigTemp1, g_iDCIR_Result_R_Temp_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strLinker(msg, m_MisSend_total_Step[i].R_JigTemp2, g_iDCIR_Result_R_Temp_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strLinker(msg, m_MisSend_total_Step[i].R_JigTemp3, g_iDCIR_Result_R_Temp_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strLinker(msg, m_MisSend_total_Step[i].R_JigTemp4, g_iDCIR_Result_R_Temp_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strLinker(msg, m_MisSend_total_Step[i].R_JigTemp5, g_iDCIR_Result_R_Temp_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strLinker(msg, m_MisSend_total_Step[i].R_JigTemp6, g_iDCIR_Result_R_Temp_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strLinker(msg, m_MisSend_total_Step[i].R_JigTempMax, g_iDCIR_Result_R_Temp_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strLinker(msg, m_MisSend_total_Step[i].R_JigTempMin, g_iDCIR_Result_R_Temp_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strLinker(msg, m_MisSend_total_Step[i].R_JigTempAvg, g_iDCIR_Result_R_Temp_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strLinker(msg, m_MisSend_total_Step[i].R_Voltage_Worth, g_iDCIR_Voltage_Worth_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strLinker(msg, m_MisSend_total_Step[i].R_Capacity_Worth, g_iDCIR_Capacity_Worth_Map);
		TRACE("%s ",msg);
		str_Step_String.Format("%s",msg);
		str_Step = str_Step + str_Step_String;
		memset(msg, 0x00, 4096);

		strLinker(msg, m_MisSend_total_Step[i].R_Discharge_Current_Worth, g_iDCIR_Discharge_Current_Worth_Map);
		TRACE("%s ",msg);
		str_Step_String.Format("%s",msg);
		str_Step = str_Step + str_Step_String;
		memset(msg, 0x00, 4096);

		strLinker(msg, m_MisSend_total_Step[i].R_Step_End_Time, g_iDCIR_Step_End_Time_Map);
		TRACE("%s ",msg);
		str_Step_String.Format("%s",msg);
		str_Step = str_Step + str_Step_String;
		memset(msg, 0x00, 4096);
	}


	char* msg2;
	msg2 = new CHAR[4096];
	memset(msg2, 0x00, 4096);
	CString str_Result_Data_R_String, str_Result_Data;

	strLinker(msg2, &m_MisSend_Result_Data_R, g_iDCIR_Result_File_Response_R_Map);
	TRACE("%s ",msg2);
	str_Result_Data_R_String.Format("%s",msg2);
	memset(msg2, 0x00, 4096);

	m_str_Result_Data = str_Result_Data_L_String + str_Step + str_Result_Data_R_String;

	int test = str_Result_Data_L_String.GetLength();
	int test1 = str_Step.GetLength();
	int test2 = str_Result_Data_R_String.GetLength();
	int test3 = str_Result_Data.GetLength();

	TRACE(TEXT_LANG[51], test, test1, test2, test3, str_Result_Data);//"Step 길이 = [%d] - [%d] - [%d] = [%d] %s"

	delete [] msg;
	delete [] msg1;
	delete [] msg2;

	memset(&m_MisSend_Result_Data_L, 0x00, sizeof(st_DCIR_RESULT_FILE_RESPONSE_L));
	memset(&m_MisSend_total_Step, 0x00, sizeof(st_DCIR_Result_Step_Set));
	memset(&m_MisSend_Result_Data_R, 0x00, sizeof(st_DCIR_RESULT_FILE_RESPONSE_R));

	return _error;
}

#else

FMS_ERRORCODE CFMS::fnMisSendMakeResult_CHARGE(CString strFile, st_CHARGER_INRESEVE& _reserve)
{
	FMS_ERRORCODE _error = FMS_ER_NONE;

	if(strFile.IsEmpty())
		return ER_File_Not_Found;

	m_str_MisSendResult_Data.Empty();

	CFormResultFile formFile;
	if(formFile.ReadFile(strFile, 0) < 0)
	{
		return ER_File_Not_Found;
	}

	CString strTmp;
	EP_FILE_HEADER		*lpFileHeader = NULL;
	STR_SAVE_CH_DATA	*lpChannel1 = NULL;
	STR_STEP_RESULT		*lpStepData1 = NULL;
	RESULT_FILE_HEADER	*lpFileData = NULL;
	STR_CONDITION_HEADER *lpModelData = NULL;

	lpFileHeader = formFile.GetFileHeader();
	if(lpFileHeader == NULL) return ER_File_Open_error;

	int nStepSize = formFile.GetStepSize();
	if(nStepSize <= 0) return ER_File_Open_error;	

	lpStepData1 = formFile.GetFirstStepData(); //시작 시간 가져오기
	if(lpStepData1 == NULL) return ER_File_Open_error;

	lpFileData = formFile.GetResultHeader();
	if(lpFileData == NULL) return ER_File_Open_error;

	st_CHARGER_INRESEVE* pReserveInfo = &_reserve;
	memset(&m_MisSend_Result_Data_L, 0x00, sizeof(st_RESULT_FILE_RESPONSE_L));

	strTmp =formFile.GetModuleName();
	memset(m_MisSend_Result_Data_L.Stage_No, 0x20, sizeof(m_MisSend_Result_Data_L.Stage_No));
	memcpy(m_MisSend_Result_Data_L.Stage_No, strTmp.GetBuffer(), min(3, strTmp.GetLength()));
	m_MisSend_Result_Data_L.Stage_No[3] = 0x00;	

	//Str_m_MisSend_Result_Data_L.Format("%3d", 995);
	strcpy_s(m_MisSend_Result_Data_L.Type, pReserveInfo->F_Value.Type);

	//Str_m_MisSend_Result_Data_L.Format("%2s","02");
	strcpy_s(m_MisSend_Result_Data_L.Process_ID, pReserveInfo->F_Value.Process_No);

	//Str_m_MisSend_Result_Data_L.Format("%20s","                    ");
	strcpy_s(m_MisSend_Result_Data_L.Batch_No, pReserveInfo->F_Value.Batch_No);

	memset(m_MisSend_Result_Data_L.Tray_ID, 0x20, sizeof(m_MisSend_Result_Data_L.Tray_ID));
	memcpy(m_MisSend_Result_Data_L.Tray_ID, lpFileData->szTrayNo, min(7, strlen(lpFileData->szTrayNo)));
	m_MisSend_Result_Data_L.Tray_ID[7] = 0x00;

	CString Str_Step;

	INT a = 0;
	INT stepCnt = 0;
	for(; a < nStepSize; a++)
	{
		lpStepData1 = formFile.GetStepData(a);

		if(lpStepData1 == NULL || lpStepData1->stepCondition.stepHeader.type == EP_TYPE_END)
			break;

		stepCnt++;

		memset(&m_MisSend_total_Step[a], 0x00, sizeof(st_Result_Step_Set));

		int j = 0;
		
		Str_Step.Format("%3d", a+1);
		memcpy(m_MisSend_total_Step[a].R_Step_No, Str_Step.GetBuffer(), 3);

		//Str_Step.Format("%1s", m_inreserv.Step_Info[a].Step_Type);
		//Str_Step.Format("%1s", lpStepData1->stepCondition.fmsType);
		//memcpy(m_MisSend_total_Step[a].R_Action, Str_Step.GetBuffer(), 1);
		if(lpStepData1->stepCondition.fmsType != 0)
			m_MisSend_total_Step[a].R_Action[0] = lpStepData1->stepCondition.fmsType;
		else
			m_MisSend_total_Step[a].R_Action[0] = 0x20;
		m_MisSend_total_Step[a].R_Action[1] = 0x00;

		for(int i = 0; i < MAX_CELL_COUNT; i++)
		{
			if(i < lpStepData1->aChData.GetSize())
			{
				//변환된 채널코드가 00 정상저장
				//변환된 채널코드가 01 전압저장
				//변환되 채널코드가 02 저장안함
				lpChannel1 = (STR_SAVE_CH_DATA *)lpStepData1->aChData.GetAt(i);

				if( i == 0 )
				{
					Str_Step.Format("%4.1f", lpChannel1->fJigTemp[0]);
					memcpy(m_total_Step[a].R_JigTemp1, Str_Step.GetBuffer(), 4);
					Str_Step.Format("%4.1f", lpChannel1->fJigTemp[1]);
					memcpy(m_total_Step[a].R_JigTemp2, Str_Step.GetBuffer(), 4);
					Str_Step.Format("%4.1f", lpChannel1->fJigTemp[2]);
					memcpy(m_total_Step[a].R_JigTemp3, Str_Step.GetBuffer(), 4);
					Str_Step.Format("%4.1f", lpChannel1->fJigTemp[3]);
					memcpy(m_total_Step[a].R_JigTemp4, Str_Step.GetBuffer(), 4);
					Str_Step.Format("%4.1f", lpChannel1->fJigTemp[4]);
					memcpy(m_total_Step[a].R_JigTemp5, Str_Step.GetBuffer(), 4);
					Str_Step.Format("%4.1f", lpChannel1->fJigTemp[5]);
					memcpy(m_total_Step[a].R_JigTemp6, Str_Step.GetBuffer(), 4);
					Str_Step.Format("%4.1f", lpChannel1->fTempMax);
					memcpy(m_total_Step[a].R_JigTempMax, Str_Step.GetBuffer(), 4);
					Str_Step.Format("%4.1f", lpChannel1->fTempMin);
					memcpy(m_total_Step[a].R_JigTempMin, Str_Step.GetBuffer(), 4);
					Str_Step.Format("%4.1f", lpChannel1->fTempAvg);
					memcpy(m_total_Step[a].R_JigTempAvg, Str_Step.GetBuffer(), 4);					
				}

				CHAR* pconCode = fnGetChannelCode(lpChannel1->channelCode);
				CString strCode;
				strCode.Format("%2s", pconCode);

				if( strcmp(strCode, "02") == 0 || strcmp(strCode, "01") == 0 )
				{
					Str_Step.Format("%5s", "    0");
					memcpy(m_MisSend_total_Step[a].R_Voltage_Worth[i].Voltage_Worth, Str_Step.GetBuffer(), 5);

					// 					Str_Step.Format("%6s", "     0");
					// 					memcpy(m_MisSend_total_Step[a].R_Current_Worth_Five_Min[i].Current_Worth_Five_Min, Str_Step.GetBuffer(), 6);

					Str_Step.Format("%6s", "     0");
					memcpy(m_MisSend_total_Step[a].R_Capacity_Worth[i].Capacity_Worth, Str_Step.GetBuffer(), 6);

// 					Str_Step.Format("%6s", "     0");
// 					memcpy(m_MisSend_total_Step[a].R_Current_Worth_Hour[i].Current_Worth_Hour, Str_Step.GetBuffer(), 6);

					Str_Step.Format("%6s", "     0");
					memcpy(m_MisSend_total_Step[a].R_Discharge_Current_Worth[i].Discharge_Current_Worth, Str_Step.GetBuffer(), 6);

					Str_Step.Format("%6s", "     0");
					memcpy(m_MisSend_total_Step[a].R_Step_End_Time[i].Step_End_Time, Str_Step.GetBuffer(), 6);

					Str_Step.Format("%5s", "    0");
					memcpy(m_MisSend_total_Step[a].R_Step_Voltage_Time[i].Step_Voltage_Time, Str_Step.GetBuffer(), 5);

					Str_Step.Format("%6s", "     0");
					memcpy(m_MisSend_total_Step[a].R_Step_Current_Time[i].Step_Current_Time, Str_Step.GetBuffer(), 6);

					//////////////////////////////////////////////////////////////////////////

					Str_Step.Format("%6s", "     0");
					memcpy(m_MisSend_total_Step[a].R_Step_Current_Capacity_CC[i].Step_Current_Capacity_CC, Str_Step.GetBuffer(), 6);

					Str_Step.Format("%6s", "     0");
					memcpy(m_MisSend_total_Step[a].R_Step_CV_Time[i].Step_CV_Time, Str_Step.GetBuffer(), 6);

					Str_Step.Format("%6s", "     0");
					memcpy(m_MisSend_total_Step[a].R_Step_CV_Capacity[i].Step_CV_Capacity, Str_Step.GetBuffer(), 6);

					//20210203 ksj
					Str_Step.Format("%6s", "      0");
					memcpy(m_MisSend_total_Step[a].R_Step_DeltaVoltage[i].Step_DeltaVoltage, Str_Step.GetBuffer(), 6);

					//////////////////////////////////////////////////////////////////////////					
				}
				else
				{
					Str_Step.Format("%5.f", lpChannel1->fVoltage);
					memcpy(m_MisSend_total_Step[a].R_Voltage_Worth[i].Voltage_Worth, Str_Step.GetBuffer(), 5);

					Str_Step.Format("%6.f", lpChannel1->fCapacity);
					memcpy(m_MisSend_total_Step[a].R_Capacity_Worth[i].Capacity_Worth, Str_Step.GetBuffer(), 6);

// 					Str_Step.Format("%6s", "     0");
// 					memcpy(m_MisSend_total_Step[a].R_Current_Worth_Hour[i].Current_Worth_Hour, Str_Step.GetBuffer(), 6);

					Str_Step.Format("%6.f", fabs(lpChannel1->fCurrent));
					memcpy(m_MisSend_total_Step[a].R_Discharge_Current_Worth[i].Discharge_Current_Worth, Str_Step.GetBuffer(), 6);

					//이전공정 불량과 REST,OCV는 Current 저장 안함
					if(strcmp(pconCode, "01") == 0 || m_MisSend_total_Step[a].R_Action[0] == '3' || m_MisSend_total_Step[a].R_Action[0] == '4')
					{
// 						Str_Step.Format("%6s", "     0");
// 						memcpy(m_MisSend_total_Step[a].R_Current_Worth_Hour[i].Current_Worth_Hour, Str_Step.GetBuffer(), 6);

						Str_Step.Format("%6s", "     0");
						memcpy(m_MisSend_total_Step[a].R_Discharge_Current_Worth[i].Discharge_Current_Worth, Str_Step.GetBuffer(), 6);
					}

					Str_Step.Format("%6.f", lpChannel1->fStepTime);
					memcpy(m_MisSend_total_Step[a].R_Step_End_Time[i].Step_End_Time, Str_Step.GetBuffer(), 6);

					Str_Step.Format("%5.f", lpChannel1->fTimeGetChargeVoltage);
					memcpy(m_MisSend_total_Step[a].R_Step_Voltage_Time[i].Step_Voltage_Time, Str_Step.GetBuffer(), 5);

					Str_Step.Format("%6.f", lpChannel1->fCcRunTime);
					memcpy(m_MisSend_total_Step[a].R_Step_Current_Time[i].Step_Current_Time, Str_Step.GetBuffer(), 6);

					//////////////////////////////////////////////////////////////////////////

					Str_Step.Format("%6.f", lpChannel1->fCcCapacity);
					memcpy(m_MisSend_total_Step[a].R_Step_Current_Capacity_CC[i].Step_Current_Capacity_CC, Str_Step.GetBuffer(), 6);

					Str_Step.Format("%6.f", lpChannel1->fCvRunTime);
					memcpy(m_MisSend_total_Step[a].R_Step_CV_Time[i].Step_CV_Time, Str_Step.GetBuffer(), 6);

					Str_Step.Format("%6.f", fabs(lpChannel1->fCvCapacity));
					memcpy(m_MisSend_total_Step[a].R_Step_CV_Capacity[i].Step_CV_Capacity, Str_Step.GetBuffer(), 6);
					//20210203 ksj
					Str_Step.Format("%6s", lpChannel1->fDeltaOCV);
					memcpy(m_MisSend_total_Step[a].R_Step_DeltaVoltage[i].Step_DeltaVoltage, Str_Step.GetBuffer(), 6);

					//////////////////////////////////////////////////////////////////////////
				}
			}
			else
			{
				Str_Step.Format("%5s", "    0");
				memcpy(m_MisSend_total_Step[a].R_Voltage_Worth[i].Voltage_Worth, Str_Step.GetBuffer(), 5);

// 				Str_Step.Format("%6s", "     0");
// 				memcpy(m_MisSend_total_Step[a].R_Current_Worth_Five_Min[i].Current_Worth_Five_Min, Str_Step.GetBuffer(), 6);

				Str_Step.Format("%6s", "     0");
				memcpy(m_MisSend_total_Step[a].R_Capacity_Worth[i].Capacity_Worth, Str_Step.GetBuffer(), 6);

// 				Str_Step.Format("%6s", "     0");
// 				memcpy(m_MisSend_total_Step[a].R_Current_Worth_Hour[i].Current_Worth_Hour, Str_Step.GetBuffer(), 6);

				Str_Step.Format("%6s", "     0");
				memcpy(m_MisSend_total_Step[a].R_Discharge_Current_Worth[i].Discharge_Current_Worth, Str_Step.GetBuffer(), 6);

				Str_Step.Format("%6s", "     0");
				memcpy(m_MisSend_total_Step[a].R_Step_End_Time[i].Step_End_Time, Str_Step.GetBuffer(), 6);

				Str_Step.Format("%5s", "    0");
				memcpy(m_MisSend_total_Step[a].R_Step_Voltage_Time[i].Step_Voltage_Time, Str_Step.GetBuffer(), 5);

				Str_Step.Format("%6s", "     0");
				memcpy(m_MisSend_total_Step[a].R_Step_Current_Time[i].Step_Current_Time, Str_Step.GetBuffer(), 6);

				//////////////////////////////////////////////////////////////////////////

				Str_Step.Format("%6s", "     0");
				memcpy(m_MisSend_total_Step[a].R_Step_Current_Capacity_CC[i].Step_Current_Capacity_CC, Str_Step.GetBuffer(), 6);

				Str_Step.Format("%6s", "     0");
				memcpy(m_MisSend_total_Step[a].R_Step_CV_Time[i].Step_CV_Time, Str_Step.GetBuffer(), 6);

				Str_Step.Format("%6s", "     0");
				memcpy(m_MisSend_total_Step[a].R_Step_CV_Capacity[i].Step_CV_Capacity, Str_Step.GetBuffer(), 6);

				//20210203 ksj
				Str_Step.Format("%6s", "      0");
				memcpy(m_MisSend_total_Step[a].R_Step_DeltaVoltage[i].Step_DeltaVoltage, Str_Step.GetBuffer(), 6);
				//////////////////////////////////////////////////////////////////////////
			}
		}
	}

	sprintf_s(m_MisSend_Result_Data_L.Total_Step, "%03d", stepCnt);

	memset(&m_MisSend_Result_Data_R, 0x00, sizeof(st_RESULT_FILE_RESPONSE_R));

	CString Str_Result_Data_R;
	Str_Result_Data_R.Format("%3d",nStepSize);
	memcpy(m_MisSend_Result_Data_R.Now_Step_No, Str_Result_Data_R.GetBuffer(), 3);

	//저장한 스텝의 제일 마지막
	lpStepData1 = formFile.GetStepData(formFile.GetStepSize() - 1);

	if(lpStepData1 != NULL)
	{
		INT ContactErrorCnt = 0;
		for(int h = 0; h < MAX_CELL_COUNT; h++)
		{
			CHAR szTemp[3] = {0};

			if(h < lpStepData1->aChData.GetSize())
			{
				lpChannel1 = (STR_SAVE_CH_DATA *)lpStepData1->aChData.GetAt(h);

				sprintf(szTemp, "%2s", fnGetChannelCode(lpChannel1->channelCode));
			}
			else
			{
				sprintf(szTemp, "%2s", "  ");
			}
			memcpy(m_MisSend_Result_Data_R.arCellInfo[h].Cell_Info, szTemp, 3);
		}

		CString StartTime, EndTime;
		EndTime.Format("%s", lpStepData1->stepHead.szEndDateTime);
		EndTime = Time_Conversion(EndTime);
		Str_Result_Data_R.Format("%14s",EndTime);
		memcpy(m_MisSend_Result_Data_R.Test_Finish_Date, Str_Result_Data_R.GetBuffer(), 14);

		lpStepData1 = formFile.GetFirstStepData();
		StartTime.Format("%s", lpStepData1->stepHead.szStartDateTime);
		StartTime = Time_Conversion(StartTime);
		Str_Result_Data_R.Format("%14s",StartTime);
		memcpy(m_MisSend_Result_Data_R.Test_Begin_Date, Str_Result_Data_R.GetBuffer(), 14);
	}	
	//////////////////////////////////////////////////////////////////////////
	//if(ContactErrorCnt > atoi(pReserveInfo->B_Value.Contact_Error_Setting))
	//{
	//	_error = ER_Contact_Error;
	//}

	char* msg1;
	msg1 = new CHAR[4096];
	memset(msg1, 0x00, 4096);
	CString str_m_MisSend_Result_Data_L_String;

	strLinker(msg1, &m_MisSend_Result_Data_L, g_iResult_File_Response_L_Map);
	TRACE("%s ",msg1);
	str_m_MisSend_Result_Data_L_String.Format("%s",msg1);
	memset(msg1, 0x00, 4096);

	char* msg;
	msg = new CHAR[4096];
	memset(msg, 0x00, 4096);
	CString str_Step, str_Step_String;
	INT strtotLen = 0;
	for(int i = 0; i < stepCnt; i++)
	{
		strtotLen += strLinker(msg, m_MisSend_total_Step[i].R_Step_No, g_iResult_R_Step_No_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strtotLen += strLinker(msg, m_MisSend_total_Step[i].R_Action, g_iResult_R_Action_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strtotLen += strLinker(msg, m_MisSend_total_Step[i].R_JigTemp1, g_iResult_R_Temp_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strtotLen += strLinker(msg, m_MisSend_total_Step[i].R_JigTemp2, g_iResult_R_Temp_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strtotLen += strLinker(msg, m_MisSend_total_Step[i].R_JigTemp3, g_iResult_R_Temp_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strtotLen += strLinker(msg, m_MisSend_total_Step[i].R_JigTemp4, g_iResult_R_Temp_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strtotLen += strLinker(msg, m_MisSend_total_Step[i].R_JigTemp5, g_iResult_R_Temp_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strtotLen += strLinker(msg, m_MisSend_total_Step[i].R_JigTemp6, g_iResult_R_Temp_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strtotLen += strLinker(msg, m_MisSend_total_Step[i].R_JigTempMax, g_iResult_R_Temp_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strtotLen += strLinker(msg, m_MisSend_total_Step[i].R_JigTempMin, g_iResult_R_Temp_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strtotLen += strLinker(msg, m_MisSend_total_Step[i].R_JigTempAvg, g_iResult_R_Temp_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

		strtotLen += strLinker(msg, m_MisSend_total_Step[i].R_Voltage_Worth, g_iVoltage_Worth_Map);
		TRACE("%d = %s ",i+1,msg);
		str_Step_String.Format("%s",msg);
		memset(msg, 0x00, 4096);
		str_Step = str_Step + str_Step_String;

// 		strtotLen += strLinker(msg, m_MisSend_total_Step[i].R_Current_Worth_Five_Min, g_iCurrent_Worth_Five_Min_Map);
// 		TRACE("%s ",msg);
// 		str_Step_String.Format("%s",msg);
// 		str_Step = str_Step + str_Step_String;
// 		memset(msg, 0x00, 4096);

		strtotLen += strLinker(msg, m_MisSend_total_Step[i].R_Capacity_Worth, g_iCapacity_Worth_Map);
		TRACE("%s ",msg);
		str_Step_String.Format("%s",msg);
		str_Step = str_Step + str_Step_String;
		memset(msg, 0x00, 4096);

// 		strtotLen += strLinker(msg, m_MisSend_total_Step[i].R_Current_Worth_Hour, g_iCurrent_Worth_Hour_Map);
// 		TRACE("%s ",msg);
// 		str_Step_String.Format("%s",msg);
// 		str_Step = str_Step + str_Step_String;
// 		memset(msg, 0x00, 4096);

		strtotLen += strLinker(msg, m_MisSend_total_Step[i].R_Discharge_Current_Worth, g_iDischarge_Current_Worth_Map);
		TRACE("%s ",msg);
		str_Step_String.Format("%s",msg);
		str_Step = str_Step + str_Step_String;
		memset(msg, 0x00, 4096);

		strtotLen += strLinker(msg, m_MisSend_total_Step[i].R_Step_End_Time, g_iStep_End_Time_Map);
		TRACE("%s ",msg);
		str_Step_String.Format("%s",msg);
		str_Step = str_Step + str_Step_String;
		memset(msg, 0x00, 4096);

		strtotLen += strLinker(msg, m_MisSend_total_Step[i].R_Step_Voltage_Time, g_iStep_Voltage_Time_Map);
		TRACE("%s ",msg);
		str_Step_String.Format("%s",msg);
		str_Step = str_Step + str_Step_String;
		memset(msg, 0x00, 4096);

		strtotLen += strLinker(msg, m_MisSend_total_Step[i].R_Step_Current_Time, g_iStep_Current_Time_Map);
		TRACE("%s ",msg);
		str_Step_String.Format("%s",msg);
		str_Step = str_Step + str_Step_String;
		memset(msg, 0x00, 4096);

		//////////////////////////////////////////////////////////////////////////

		strtotLen += strLinker(msg, m_MisSend_total_Step[i].R_Step_Current_Capacity_CC, g_iStep_Current_Capacity_CC_Map);
		TRACE("%s ",msg);
		str_Step_String.Format("%s",msg);
		str_Step = str_Step + str_Step_String;
		memset(msg, 0x00, 4096);

		strtotLen += strLinker(msg, m_MisSend_total_Step[i].R_Step_CV_Time, g_iStep_CV_Time_Map);
		TRACE("%s ",msg);
		str_Step_String.Format("%s",msg);
		str_Step = str_Step + str_Step_String;
		memset(msg, 0x00, 4096);

		strtotLen += strLinker(msg, m_MisSend_total_Step[i].R_Step_CV_Capacity, g_iStep_CV_Capacity_Map);
		TRACE("%s ",msg);
		str_Step_String.Format("%s",msg);
		str_Step = str_Step + str_Step_String;
		memset(msg, 0x00, 4096);

		//20210203 ksj
		strtotLen += strLinker(msg, m_MisSend_total_Step[i].R_Step_DeltaVoltage, g_iStep_DeltaVoltage_Map);
		TRACE("%s ",msg);
		str_Step_String.Format("%s",msg);
		str_Step = str_Step + str_Step_String;
		memset(msg, 0x00, 4096);

		//////////////////////////////////////////////////////////////////////////
	}

	char* msg2;
	msg2 = new CHAR[4096];
	memset(msg2, 0x00, 4096);
	CString str_Result_Data_R_String;

	INT len = strLinker(msg2, &m_MisSend_Result_Data_R, g_iResult_File_Response_R_Map);
	TRACE("%s ",msg2);
	str_Result_Data_R_String.Format("%s",msg2);
	memset(msg2, 0x00, 4096);

	m_str_MisSendResult_Data = str_m_MisSend_Result_Data_L_String + str_Step + str_Result_Data_R_String;

	delete [] msg;
	delete [] msg1;
	delete [] msg2;

	memset(&m_MisSend_Result_Data_L, 0x00, sizeof(st_RESULT_FILE_RESPONSE_L));
	memset(&m_MisSend_total_Step, 0x00, sizeof(st_Result_Step_Set));
	memset(&m_MisSend_Result_Data_R, 0x00, sizeof(st_RESULT_FILE_RESPONSE_R));

	return _error;
}
#endif

FMS_ERRORCODE CFMS::fnSend_E_CHARGER_RESULT_RESPONSE_MisSend()
{
	FMS_ERRORCODE _error = FMS_ER_NONE;

	CHAR _result = RESULT_CODE_FILE_OPEN_ERROR;
	INT dataLen = 0;
	if(m_MisSendState != 3)
	{
		CFMSLog::WriteErrorLog("No MisSend Sate %d", m_nModuleID);
		fnMisSendResultDataStateUpdate2();
		fnSendMisNext();
		m_MisSendState = 1;
		return ER_Data_Error;
	}
	else
	{
		dataLen = m_str_MisSendResult_Data.GetLength();
		if(dataLen <= 0)
		{
			CFMSLog::WriteErrorLog("Mis Send File Open error[%03d][%s]"
				, fnGetLineNo()
				, g_str_FMS_State[m_FMSStateCode]);

			fnMisSendResultDataStateUpdate2();
			fnSendMisNext();
			m_MisSendState = 1;
			return ER_File_Open_error;
		}
		else
		{
			_result = RESULT_CODE_MIS_SEND;
		}
	}

	st_FMS_PACKET* pPack = fnGetPack();
	fnMakeHead(pPack->head, CHARGER_RESULT_RESPONSE, dataLen, _result);

	CHAR head[51];
	memset(&head, 0x20, 50);
	head[50] = 0;
	strLinker(head, &pPack->head, g_iHead_Map);

	CString strResult = head + m_str_MisSendResult_Data += "*;";
	const CHAR *temp = strResult.GetBuffer();
	theApp.m_FMSserver.fnSendResult(temp);
	
	CFMSLog::SendFMSLog(strResult.GetBuffer(), E_CHARGER_RESULT_RESPONSE);		// CHARGER_RESULT_RESPONSE 메세지를 로그로 저장

	return _error;
}

FMS_ERRORCODE CFMS::fnSend_E_CHARGER_RESULT_RECV_RESPONSE_MisSend(FMS_ERRORCODE _rec)
{
	FMS_ERRORCODE _error = FMS_ER_NONE;

	st_CHARGER_RESULT_RECV_RESPONSE data;
	memset(&data, 0x20, sizeof(st_CHARGER_RESULT_RECV_RESPONSE));

	sprintf(data.Equipment_Num, "%03d", fnGetLineNo());
	strcpy_s(data.Tray_ID, fnGetTrayID());

	CHAR _result = RESULT_UNKNOW_ERROR;
	switch(_rec)
	{
	case FMS_ER_NONE:
		_result = RESULT_CODE_OK;
		break;
	case ER_TrayID:
		//_result = RESULT_CODE_TRAY_ID_ERROR;
		_result = RESULT_CODE_DATA_ERROR;
		break;
	case ER_NO_Process:
		//_result = RESULT_CODE_STATUS_ERROR;
		_result = RESULT_CODE_DATA_ERROR;
		break;
	}

	if(_rec)
	{
		CFMSLog::WriteErrorLog("[%03d][%s]%s"
			, fnGetLineNo()
			, g_str_FMS_State[m_FMSStateCode]
		, g_str_FMS_ERROR[_rec]);
	}

	st_FMS_PACKET* pPack = fnGetPack();
	memset(pPack->data, 0x20, sizeof(pPack->data));
	UINT dataLen = strLinker(pPack->data
		, &data, g_iCHARGER_RESULT_RECV_RESPONSE_Map);

	pPack->dataLen = dataLen;
	fnMakeHead(pPack->head
		, CHARGER_RESULT_RECV_RESPONSE
		, dataLen, _result);

	theApp.m_FMS_SendQ.Push(*pPack);

	return _error;
}

// 1. 미전송 파일 업데이트 완료 후 Database에서 정보 삭제
BOOL CFMS::fnMisSendResultDataStateUpdate()
{
	if(m_vSendMis.size() <= 0)
		return FALSE;

	INT sqlerr = 0;

	CString strSqliteDBPathNName = theApp.GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");
	CString dbname;
	dbname.Format("\\FMS_DB\\CTSMonDB_%03d.db3", fnGetLineNo());
	strSqliteDBPathNName += dbname;

	sqlite3* mSqlite;

	sqlerr = sqlite3_open(strSqliteDBPathNName, &mSqlite);

	sqlite3_stmt * stmt = 0;

	CString strQuery;

	strQuery.Format("Update FMSWorkInfo \
					Set \
					Send_Complet = %d \
					where idx = %I64d;"
					, RS_REPORT
					, m_vSendMis[m_SendMisIdx]->idxKey);

	CHAR *errorMsg;
	sqlerr = sqlite3_exec(mSqlite, strQuery, NULL, NULL, &errorMsg);
	
	if(SQLITE_OK != sqlerr)
	{
		LPSTR szError = (LPSTR)sqlite3_errmsg(mSqlite);
		CFMSLog::WriteLog(szError);

		int nRowsChanged = sqlite3_changes(mSqlite);

		sqlerr = sqlite3_reset(stmt);

		if (sqlerr != SQLITE_OK)
		{
			szError = (LPSTR)sqlite3_errmsg(mSqlite);
			CFMSLog::WriteLog(szError);
		}
	}

	sqlite3_finalize(stmt);
	sqlite3_close(mSqlite);

	return TRUE;
}

BOOL CFMS::fnMisSendResultDataStateUpdate2()
{
	if(m_vSendMis.size() <= 0)
		return FALSE;

	INT sqlerr = 0;

	CString strSqliteDBPathNName = theApp.GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");
	CString dbname;
	dbname.Format("\\FMS_DB\\CTSMonDB_%03d.db3", fnGetLineNo());
	strSqliteDBPathNName += dbname;

	sqlite3* mSqlite;

	sqlerr = sqlite3_open(strSqliteDBPathNName, &mSqlite);

	sqlite3_stmt * stmt = 0;

	CString strQuery;

	strQuery.Format("Update FMSWorkInfo \
					Set \
					Send_Complet = %d \
					where idx = %I64d;"
					, RS_ERROR
					, m_vSendMis[m_SendMisIdx]->idxKey);


	CHAR *errorMsg;
	sqlerr = sqlite3_exec(mSqlite, strQuery, NULL, NULL, &errorMsg);
	
	if(SQLITE_OK != sqlerr)
	{
		LPSTR szError = (LPSTR)sqlite3_errmsg(mSqlite);
		CFMSLog::WriteLog(szError);

		int nRowsChanged = sqlite3_changes(mSqlite);

		sqlerr = sqlite3_reset(stmt);

		if (sqlerr != SQLITE_OK)
		{
			szError = (LPSTR)sqlite3_errmsg(mSqlite);
			CFMSLog::WriteLog(szError);
		}
	}

	sqlite3_finalize(stmt);
	sqlite3_close(mSqlite);

	return TRUE;
}

VOID CFMS::fnWriteTemperature()
{
	if( m_pModule == NULL )
	{
		return;
	}

	CString strTemp = _T("");
	CString strTempFileName = _T("");
	CString strState = _T("");
	CString strLog = _T("");
	CString strTitle = _T("STATE");

	if( m_pModule->GetResultFileName() != _T("") )
	{		
		int nFileLength = m_pModule->GetResultFileName().GetLength();
		strTempFileName = m_pModule->GetResultFileName().Left(nFileLength-4);

		strLog.Format("START");

		EP_GP_DATA gpData = EPGetGroupData(m_nModuleID, 0);

		for( int i=0; i<MAX_USE_TEMP; i++ )				// 총 Sensor값은 16개
		{							
			strTemp.Format(",T%d", i+1);
			strTitle += strTemp;

			strTemp.Format(",%.1f", (float)gpData.sensorData.sensorData1[i].lData/100.0f );
			strLog += strTemp;
		}

		GLog::log_TempSave(strLog, strTempFileName, NULL, TRUE, _T(""), strTitle);
	}
}

/* 1. 추후에 FMS 로그 데이터가 필요한 경우 활용
VOID CFMS::Result_Data_file_Save()
{
#ifdef _CHARGER

	COleDateTime dateTime = COleDateTime::GetCurrentTime();
	CString strCurTime = dateTime.Format("%Y%m%d_%H%M%S");

	CString strTemp;

	strTemp += m_Result_Data_L.Stage_No;
	strTemp += "\r";
	strTemp += m_Result_Data_L.File_Type;
	strTemp += "\r";
	strTemp += m_Result_Data_L.Type;
	strTemp += "\r";
	strTemp += m_Result_Data_L.Process_ID;
	strTemp += "\r";
	strTemp += m_Result_Data_L.Batch_No;
	strTemp += "\r";
	strTemp += m_Result_Data_L.Tray_ID;
	strTemp += "\r";
	strTemp += m_Result_Data_L.Total_Step;
	strTemp += "\r";

	//strTemp += "--------------------------------Step 시작--------------------------------";
	strTemp += "\r";

	for(int i = 0; i < atoi(m_Result_Data_L.Total_Step); i++)
	{
		CString Step_num;		
		//Step_num.Format("--------------------------------Step %d---------------------------------",i+1);
		strTemp += Step_num;
		strTemp += "\r";
		strTemp += m_total_Step[i].R_Step_No;
		strTemp += "\r";
		strTemp += m_total_Step[i].R_Action;
		strTemp += "\r";
		for(int y = 0; y < 32; y++)
		{
			strTemp += m_total_Step[i].R_Voltage_Worth[y].Voltage_Worth;
			strTemp += ", ";
		}
		strTemp += "\r";
		for(int y = 0; y < 32; y++)
		{
			strTemp += m_total_Step[i].R_Current_Worth_Five_Min[y].Current_Worth_Five_Min;
			strTemp += ", ";
		}
		strTemp += "\r";
		for(int y = 0; y < 32; y++)
		{
			strTemp += m_total_Step[i].R_Capacity_Worth[y].Capacity_Worth;
			strTemp += ", ";
		}
		strTemp += "\r";
		for(int y = 0; y < 32; y++)
		{
			strTemp += m_total_Step[i].R_Current_Worth_Hour[y].Current_Worth_Hour;
			strTemp += ", ";
		}
		strTemp += "\r";
		for(int y = 0; y < 32; y++)
		{
			strTemp += m_total_Step[i].R_Discharge_Current_Worth[y].Discharge_Current_Worth;
			strTemp += ", ";
		}
		strTemp += "\r";
		for(int y = 0; y < 32; y++)
		{
			strTemp += m_total_Step[i].R_Step_End_Time[y].Step_End_Time;
			strTemp += ", ";
		}
		strTemp += "\r";
		for(int y = 0; y < 32; y++)
		{
			strTemp += m_total_Step[i].R_Step_Voltage_Time[y].Step_Voltage_Time;
			strTemp += ", ";
		}
		strTemp += "\r";
		for(int y = 0; y < 32; y++)
		{
			strTemp += m_total_Step[i].R_Step_Current_Time[y].Step_Current_Time;
			strTemp += ", ";

		}
		strTemp += "\r";
	}

	//strTemp += "--------------------------------Step 끝--------------------------------";
	strTemp += "\r";

	strTemp += m_Result_Data_R.Now_Step_No;
	strTemp += "\r";

	for(int y = 0; y < 32; y++)
	{
		strTemp += m_Result_Data_R.arCellInfo[y].Cell_Info;
		strTemp += ", ";
	}
	strTemp += "\r";

	strTemp += m_Result_Data_R.Test_Begin_Date;
	strTemp += "\r";

	strTemp += m_Result_Data_R.Test_Finish_Date;

	CString N_num, strFile_N;
	CFile file_G;
	TCHAR  *szTemp = NULL;
	strFile_N.Format(_T("C:\\Program Files (x86)\\PNE CTS\\Result_Data\\Result_Data_%s.txt"), strCurTime);

	if (file_G.Open(strFile_N, CFile::modeCreate | CFile::modeWrite))
	{		
		TCHAR *szPath = new TCHAR[strTemp.GetLength()+1];
		_tcscpy(szPath, strTemp +"\r");
		int nLen = _tcslen(szPath);
		file_G.Write(szPath, nLen*sizeof(TCHAR));

		file_G.Close();

		if(szPath != NULL)
		{
			szPath = NULL;
			delete [] szPath;
		}

		memset(&m_Result_Data_L, 0x00, sizeof(st_RESULT_FILE_RESPONSE_L));
		memset(&m_total_Step, 0x00, sizeof(st_Result_Step_Set));
		memset(&m_Result_Data_R, 0x00, sizeof(st_RESULT_FILE_RESPONSE_R));
	}

#else ifdef _DCIR

	COleDateTime dateTime = COleDateTime::GetCurrentTime();
	CString strCurTime = dateTime.Format("%Y%m%d_%H%M%S");

	CString strTemp;

	strTemp += m_Result_Data_L.Stage_No;
	strTemp += "\r";
	strTemp += m_Result_Data_L.Type;
	strTemp += "\r";
	strTemp += m_Result_Data_L.Process_ID;
	strTemp += "\r";
	strTemp += m_Result_Data_L.Batch_No;
	strTemp += "\r";
	strTemp += m_Result_Data_L.Tray_ID;
	strTemp += "\r";	

	for(int y = 0; y < 32; y++)
	{
		strTemp += m_Result_Data_L.DC_IR[y].DC_IR;
		strTemp += "\r";
	}

	for(int y = 0; y < 32; y++)
	{
		strTemp += m_Result_Data_L.V1[y].V1;
		strTemp += "\r";
	}

	for(int y = 0; y < 32; y++)
	{
		strTemp += m_Result_Data_L.V2[y].V2;
		strTemp += "\r";
	}

	for(int y = 0; y < 32; y++)
	{
		strTemp += m_Result_Data_L.Discharge_Time[y].Discharge_Time;
		strTemp += "\r";
	}

	for(int y = 0; y < 32; y++)
	{
		strTemp += m_Result_Data_L.CutOff[y].CutOff;
		strTemp += "\r";
	}

	for(int y = 0; y < 32; y++)
	{
		strTemp += m_Result_Data_L.Age_temp[y].Age_temp;
		strTemp += "\r";
	}

	strTemp += m_Result_Data_L.Total_Step;
	strTemp += "\r";

	//strTemp += "--------------------------------Step 시작--------------------------------";
	strTemp += "\r";
	CString num;
	num.Format("%s", m_Result_Data_L.Total_Step);

	char* msg;
	msg = new CHAR[4096];
	memset(msg, 0x00, 4096);

	for(int i = 0; i < atoi(num); i++)
	{
		CString Step_num;		
		//Step_num.Format("--------------------------------Step %d---------------------------------",i+1);
		strTemp += Step_num;
		strTemp += "\r";
		strTemp += m_total_Step[i].R_Step_No;
		strTemp += "\r";
		strTemp += m_total_Step[i].R_Action;
		strTemp += "\r";
		for(int y = 0; y < 32; y++)
		{
			strTemp += m_total_Step[i].R_Voltage_Worth[y].Voltage_Worth;
			strTemp += "\r";
		}		
		for(int y = 0; y < 32; y++)
		{
			strTemp += m_total_Step[i].R_Capacity_Worth[y].Capacity_Worth;
			strTemp += "\r";
		}		
		for(int y = 0; y < 32; y++)
		{
			strTemp += m_total_Step[i].R_Discharge_Current_Worth[y].Discharge_Current_Worth;
			strTemp += "\r";
		}
		for(int y = 0; y < 32; y++)
		{
			strTemp += m_total_Step[i].R_Step_End_Time[y].Step_End_Time;
			strTemp += "\r";
		}
	}

	//strTemp += "--------------------------------Step 끝--------------------------------";
	strTemp += "\r";

	strTemp += m_Result_Data_R.Now_Step_No;
	strTemp += "\r";

	for(int y = 0; y < 32; y++)
	{
		strTemp += m_Result_Data_R.arCellInfo[y].Cell_Info;
		strTemp += "\r";
	}

	strTemp += m_Result_Data_R.Test_Begin_Date;
	strTemp += "\r";

	strTemp += m_Result_Data_R.Test_Finish_Date;

	CString N_num, strFile_N;
	CFile file_G;
	TCHAR  *szTemp = NULL;
	strFile_N.Format(_T("C:\\Program Files (x86)\\PNE CTS\\Result_Data\\Result_Data_%s.txt"), strCurTime);

	if (file_G.Open(strFile_N, CFile::modeCreate | CFile::modeWrite))
	{		
		TCHAR *szPath = new TCHAR[strTemp.GetLength()+1];
		_tcscpy(szPath, strTemp +"\r");
		int nLen = _tcslen(szPath);
		file_G.Write(szPath, nLen*sizeof(TCHAR));

		file_G.Close();

		if(szPath != NULL)
		{
			szPath = NULL;
			delete [] szPath;
		}

		memset(&m_Result_Data_L, 0x00, sizeof(st_DCIR_RESULT_FILE_RESPONSE_L));
		memset(&m_total_Step, 0x00, sizeof(st_DCIR_Result_Step_Set));
		memset(&m_Result_Data_R, 0x00, sizeof(st_DCIR_RESULT_FILE_RESPONSE_R));
	}

#endif
}
*/