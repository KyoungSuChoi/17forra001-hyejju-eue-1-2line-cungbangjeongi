// UserSetDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSGraphAnal.h"
#include "UserSetDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUserSetDlg dialog


CUserSetDlg::CUserSetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUserSetDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CUserSetDlg)
	m_nTrayColSize = 0;
	m_bDispCapSum = FALSE;
	m_fTime1 = 0.0f;
	m_fTime2 = 0.0f;
	m_bUserImpTime = FALSE;
	m_nPrevIncludeCount = 0;
	m_nPrevExcludeCount = 0;
	m_nAfterExcludeCount = 0;
	m_nAfterIncludeCount = 0;
	m_bUseFitLine = FALSE;
	//}}AFX_DATA_INIT
}


void CUserSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CUserSetDlg)
	DDX_Control(pDX, IDC_ITEM_LIST, m_itemList);
	DDX_Control(pDX, IDC_WH_PRESI_COMBO, m_ctrlWhDecimal);
	DDX_Control(pDX, IDC_W_PRESI_COMBO, m_ctrlWDecimal);
	DDX_Control(pDX, IDC_WH_UNIT_COMBO, m_ctrlWhUnit);
	DDX_Control(pDX, IDC_W_UNIT_COMBO, m_ctrlWUnit);
	DDX_Control(pDX, IDC_FONT_NAME, m_ctrFontResult);
	DDX_Control(pDX, IDC_TIME_UNIT_COMBO, m_ctrlTimeUnit);
	DDX_Control(pDX, IDC_C_PRESI_COMBO, m_ctrlCDecimal);
	DDX_Control(pDX, IDC_I_PRESI_COMBO, m_ctrlIDecimal);
	DDX_Control(pDX, IDC_V_PRESI_COMBO, m_ctrlVDecimal);
	DDX_Control(pDX, IDC_C_UNIT_COMBO, m_ctrlCUnit);
	DDX_Control(pDX, IDC_I_UNIT_COMBO, m_ctrlIUnit);
	DDX_Control(pDX, IDC_V_UNIT_COMBO, m_ctrlVUnit);
	DDX_Text(pDX, 1078, m_nTrayColSize);
	DDX_Check(pDX, IDC_CAP_SUM_CHECK, m_bDispCapSum);
	DDX_Text(pDX, IDC_EDIT_TIME1, m_fTime1);
	DDX_Text(pDX, IDC_EDIT_TIME2, m_fTime2);
	DDX_Check(pDX, IDC_CHECK1, m_bUserImpTime);
	DDX_Text(pDX, IDC_EDIT4, m_nPrevIncludeCount);
	DDX_Text(pDX, IDC_EDIT3, m_nPrevExcludeCount);
	DDX_Text(pDX, IDC_EDIT5, m_nAfterExcludeCount);
	DDX_Text(pDX, IDC_EDIT6, m_nAfterIncludeCount);
	DDX_Check(pDX, IDC_CHECK_FIT_LINE, m_bUseFitLine);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CUserSetDlg, CDialog)
	//{{AFX_MSG_MAP(CUserSetDlg)
	ON_BN_CLICKED(IDC_FONT_SET_BTN, OnFontSetBtn)
	ON_BN_CLICKED(IDC_UP_BUTTON, OnUpButton)
	ON_BN_CLICKED(IDC_DOWN_BUTTON, OnDownButton)
	ON_BN_CLICKED(IDC_CHECK1, OnCheck1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUserSetDlg message handlers

BOOL CUserSetDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	//////////////////////////////////////////////////////////////////////////

	m_nTrayColSize = AfxGetApp()->GetProfileInt(REG_CONFIG_SECTION, "TrayColSize", 8);

	UINT nSize;
	LPVOID* pData= NULL;
	BOOL nRtn = AfxGetApp()->GetProfileBinary(REG_CONFIG_SECTION, "GridFont", (LPBYTE *)&pData, &nSize);
	if(nSize > 0 && nRtn == TRUE)	
	{
		memcpy(&m_afont, pData, min(sizeof(m_afont), nSize));
	}
	else
	{
		CFont *pFont = GetFont();
		pFont->GetLogFont(&m_afont);
	}
	if(pData) delete [] pData;

	char szBuff[32], szUnit[16], szDecimalPoint[16];
	sprintf(szBuff, AfxGetApp()->GetProfileString(REG_CONFIG_SECTION, "V Unit", "V 3"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_ctrlVUnit.SelectString(0, szUnit);
	m_ctrlVDecimal.SelectString(0,szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(REG_CONFIG_SECTION, "I Unit", "mA 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_ctrlIUnit.SelectString(0,szUnit);
	m_ctrlIDecimal.SelectString(0,szDecimalPoint);

#ifdef _EDLC_TEST_SYSTEM
	m_ctrlCUnit.ResetContent();
	m_ctrlCUnit.AddString("F");
	m_ctrlCUnit.AddString("mF");
	m_ctrlCUnit.AddString("uF");
#endif

	sprintf(szBuff, AfxGetApp()->GetProfileString(REG_CONFIG_SECTION, "C Unit", "mAh 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_ctrlCUnit.SelectString(0,szUnit);
	m_ctrlCDecimal.SelectString(0,szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(REG_CONFIG_SECTION, "W Unit", "mW 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_ctrlWUnit.SelectString(0,szUnit);
	m_ctrlWDecimal.SelectString(0,szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(REG_CONFIG_SECTION, "Wh Unit", "mWh 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_ctrlWhUnit.SelectString(0,szUnit);
	m_ctrlWhDecimal.SelectString(0,szDecimalPoint);

	
	sprintf(szBuff, AfxGetApp()->GetProfileString(REG_CONFIG_SECTION, "Time Unit", "0"));
	m_ctrlTimeUnit.SetCurSel(atol(szBuff));
	//////////////////////////////////////////////////////////////////////////
	
	m_bDispCapSum = AfxGetApp()->GetProfileInt(REG_CONFIG_SECTION, "Capacity Sum", FALSE);

	m_fTime1 = atof(AfxGetApp()->GetProfileString(REG_CONFIG_SECTION, "ImpTime1", "0.0"));
	m_fTime2 = atof(AfxGetApp()->GetProfileString(REG_CONFIG_SECTION, "ImpTime2", "0.01"));
	m_bUserImpTime = AfxGetApp()->GetProfileInt(REG_CONFIG_SECTION, "UserImp", FALSE);
	GetDlgItem(IDC_EDIT_TIME1)->EnableWindow(m_bUserImpTime);
	GetDlgItem(IDC_EDIT_TIME2)->EnableWindow(m_bUserImpTime);

	//Save Item
	CString strData;
	DWORD dwItem[PS_MAX_ITEM_NUM] = {0xffff,};

	pData = NULL;
	nRtn = AfxGetApp()->GetProfileBinary(REG_CONFIG_SECTION, "SaveItem", (LPBYTE *)&pData, &nSize);
	if(nSize > 0 && nRtn == TRUE)	
	{
		memcpy(dwItem, pData, min(sizeof(dwItem), nSize));
	}
	else
	{
		//default list
		for( int nI = 0; nI < PS_MAX_ITEM_NUM; nI++ )
		{
			strData = ::PSGetItemName(nI);	
			if(strData.IsEmpty())
			{
				dwItem[nI] = MAKELONG(nI , FALSE);
			}
			else
			{
				dwItem[nI] = MAKELONG(nI , TRUE);
			}
		}
	}
	if(pData) delete [] pData;

	//Display selected data
	for( int nI = 0; nI < PS_MAX_ITEM_NUM; nI++ )
	{
		strData = ::PSGetItemName(LOWORD(dwItem[nI]));	
		if(!strData.IsEmpty())
		{
			m_itemList.AddString(strData);
			
			m_itemList.SetCheck(nI, HIWORD(dwItem[nI]));
			m_itemList.SetItemData(nI, LOWORD(dwItem[nI]));
		}
	}		


	m_bUseFitLine = AfxGetApp()->GetProfileInt("Settings", "UseFitLine", FALSE);
	m_nPrevExcludeCount = AfxGetApp()->GetProfileInt("Settings", "PrevExCount", 0);
	m_nPrevIncludeCount = AfxGetApp()->GetProfileInt("Settings", "PrevInCount", 100);
	m_nAfterExcludeCount = AfxGetApp()->GetProfileInt("Settings", "AfterExCount", 0);
	m_nAfterIncludeCount = AfxGetApp()->GetProfileInt("Settings", "AfterInCount", 100);

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CUserSetDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
	
	AfxGetApp()->WriteProfileInt(REG_CONFIG_SECTION, "TrayColSize", m_nTrayColSize);

	AfxGetApp()->WriteProfileBinary(REG_CONFIG_SECTION, "GridFont",(LPBYTE)&m_afont, sizeof(LOGFONT));
	
	char szBuff[32], szUnit[16], szDecimalPoint[16];
	m_ctrlVUnit.GetLBText(m_ctrlVUnit.GetCurSel(), szUnit);
	m_ctrlVDecimal.GetLBText(m_ctrlVDecimal.GetCurSel(), szDecimalPoint);
	sprintf(szBuff, "%s %s", szUnit, szDecimalPoint);
	AfxGetApp()->WriteProfileString(REG_CONFIG_SECTION, "V Unit", szBuff);

	m_ctrlIUnit.GetLBText(m_ctrlIUnit.GetCurSel(), szUnit);
	m_ctrlIDecimal.GetLBText(m_ctrlIDecimal.GetCurSel(), szDecimalPoint);
	sprintf(szBuff, "%s %s", szUnit, szDecimalPoint);
	AfxGetApp()->WriteProfileString(REG_CONFIG_SECTION, "I Unit", szBuff);

	m_ctrlCUnit.GetLBText(m_ctrlCUnit.GetCurSel(), szUnit);
	m_ctrlCDecimal.GetLBText(m_ctrlCDecimal.GetCurSel(), szDecimalPoint);
	sprintf(szBuff, "%s %s", szUnit, szDecimalPoint);
	AfxGetApp()->WriteProfileString(REG_CONFIG_SECTION, "C Unit", szBuff);

	m_ctrlWUnit.GetLBText(m_ctrlWUnit.GetCurSel(), szUnit);
	m_ctrlWDecimal.GetLBText(m_ctrlWDecimal.GetCurSel(), szDecimalPoint);
	sprintf(szBuff, "%s %s", szUnit, szDecimalPoint);
	AfxGetApp()->WriteProfileString(REG_CONFIG_SECTION, "W Unit", szBuff);

	m_ctrlWhUnit.GetLBText(m_ctrlWhUnit.GetCurSel(), szUnit);
	m_ctrlWhDecimal.GetLBText(m_ctrlWhDecimal.GetCurSel(), szDecimalPoint);
	sprintf(szBuff, "%s %s", szUnit, szDecimalPoint);
	AfxGetApp()->WriteProfileString(REG_CONFIG_SECTION, "Wh Unit", szBuff);

	sprintf(szBuff, "%d", m_ctrlTimeUnit.GetCurSel());
	AfxGetApp()->WriteProfileString(REG_CONFIG_SECTION, "Time Unit", szBuff);
	
	AfxGetApp()->WriteProfileInt(REG_CONFIG_SECTION, "Capacity Sum", m_bDispCapSum);

	//Save item 
	int nTot = m_itemList.GetCount();
	DWORD dwItem[PS_MAX_ITEM_NUM];
	memset(dwItem, PS_MAX_ITEM_NUM+1, sizeof(dwItem));	//존재하지 않는 숫자로 Setting한다.

	for(int a=0; a<nTot && a<PS_MAX_ITEM_NUM; a++)
	{
		dwItem[a] = MAKELONG(m_itemList.GetItemData(a), m_itemList.GetCheck(a));
	}

	AfxGetApp()->WriteProfileBinary(REG_CONFIG_SECTION, "SaveItem",(LPBYTE)dwItem, sizeof(dwItem));

	sprintf(szBuff, "%f", m_fTime1);
	AfxGetApp()->WriteProfileString(REG_CONFIG_SECTION, "ImpTime1", szBuff);
	sprintf(szBuff, "%f", m_fTime2);
	AfxGetApp()->WriteProfileString(REG_CONFIG_SECTION, "ImpTime2", szBuff);
	AfxGetApp()->WriteProfileInt(REG_CONFIG_SECTION, "UserImp", m_bUserImpTime);

	AfxGetApp()->WriteProfileInt("Settings", "UseFitLine", m_bUseFitLine);
	AfxGetApp()->WriteProfileInt("Settings", "PrevExCount", m_nPrevExcludeCount);
	AfxGetApp()->WriteProfileInt("Settings", "PrevInCount", m_nPrevIncludeCount);
	AfxGetApp()->WriteProfileInt("Settings", "AfterExCount", m_nAfterExcludeCount);
	AfxGetApp()->WriteProfileInt("Settings", "AfterInCount", m_nAfterIncludeCount);

	CDialog::OnOK();
}

void CUserSetDlg::OnFontSetBtn() 
{
	// TODO: Add your control notification handler code here
	CFontDialog aDlg(&m_afont);
	if(aDlg.DoModal()==IDOK)
	{
		aDlg.GetCurrentFont(&m_afont);
		m_ctrFontResult.SetUserFont(&m_afont);
	}	
}

void CUserSetDlg::OnUpButton() 
{
	// TODO: Add your control notification handler code here
	int nSel = m_itemList.GetCurSel();
	if(nSel > 0)
	{
		DWORD itemData = m_itemList.GetItemData(nSel);
		BOOL bCheck = m_itemList.GetCheck(nSel);
		CString strItem;
		m_itemList.GetText(nSel, strItem);

		m_itemList.InsertString(nSel-1, strItem);
		m_itemList.SetItemData(nSel-1, itemData);
		m_itemList.SetCheck(nSel-1, bCheck);
		
		m_itemList.DeleteString(nSel+1);
		m_itemList.SetCurSel(nSel-1);
	}
}

void CUserSetDlg::OnDownButton() 
{
	// TODO: Add your control notification handler code here
	int nSel = m_itemList.GetCurSel();
	if(nSel >= 0 && nSel < m_itemList.GetCount()-1)
	{
		DWORD itemData = m_itemList.GetItemData(nSel);
		BOOL bCheck = m_itemList.GetCheck(nSel);
		CString strItem;
		m_itemList.GetText(nSel, strItem);

		m_itemList.InsertString(nSel+2, strItem);
		m_itemList.SetItemData(nSel+2, itemData);
		m_itemList.SetCheck(nSel+2, bCheck);
		
		m_itemList.DeleteString(nSel);
		m_itemList.SetCurSel(nSel+1);
	}	
}

void CUserSetDlg::OnCheck1() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	GetDlgItem(IDC_EDIT_TIME1)->EnableWindow(m_bUserImpTime);
	GetDlgItem(IDC_EDIT_TIME2)->EnableWindow(m_bUserImpTime);
}
