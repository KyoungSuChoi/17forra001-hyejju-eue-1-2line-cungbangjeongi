// ExcelTransDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ExcelTrans.h"
#include "ExcelTransDlg.h"
#include "SaveItemSetDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CExcelTransDlg dialog

CExcelTransDlg::CExcelTransDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CExcelTransDlg::IDD, pParent)
	, m_bExcelOpenFlag(false)
{
	//{{AFX_DATA_INIT(CExcelTransDlg)
	m_strSelFileName = _T("");
	m_bFileAppend = FALSE;
	m_strSaveFileName = _T("");
	m_strSaveFolderName = _T("");
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_nConvertType = 0;
	
	m_bState = TRUE;
	m_bTime = TRUE;
	m_bVoltage = TRUE;
	m_bCurrent = TRUE;
	m_bCapacity = TRUE;
	m_bImpedance = TRUE;
	m_bWatt = TRUE;
	m_bWattHour = TRUE;
	m_bGradeCode = TRUE;
	m_bCellCode = TRUE;
	m_bType = TRUE;
	m_bCapEff = TRUE;	
	m_bSumCap = FALSE;

	m_bTimeGetChargeVoltage = FALSE;
	m_bComCapacity = FALSE;
	m_bCCTime = FALSE;
	m_bComDcir = FALSE;
	m_bDcir = FALSE;
	m_bV1 = FALSE;
	m_bV2 = FALSE;
	m_bAvgJigTemp = FALSE;
	m_bAvgCurrent = FALSE;
	
	m_nColCount = 8;

	m_strSaveFileName = "";

	ZeroMemory(m_aChMsg, sizeof(m_aChMsg));

	m_nTimeUnit = 0;
	m_strVUnit = "V";
	m_nVDecimal  = 3;
	m_strIUnit = "mA";
	m_nIDecimal = 1;
	m_strCUnit = "mAh";
	m_nCDecimal = 1;

	LanguageinitMonConfig();
}

CExcelTransDlg::~CExcelTransDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

void CExcelTransDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CExcelTransDlg)
	DDX_Text(pDX, IDC_SEL_FILE_EDIT, m_strSelFileName);
	DDX_Check(pDX, IDC_FILE_ADD_CHECK, m_bFileAppend);
	DDX_Text(pDX, IDC_SAVE_FILE_EDIT, m_strSaveFileName);
	DDX_Text(pDX, IDC_EDIT1, m_strSaveFolderName);
	DDX_Check(pDX, IDC_EXCEL_FLAG, m_bExcelOpenFlag);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CExcelTransDlg, CDialog)
	//{{AFX_MSG_MAP(CExcelTransDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_FILE_SEL_BUTTON, OnFileSelButton)
	ON_BN_CLICKED(IDC_SEL_SAVE_FILE, OnSelSaveFile)
	ON_BN_CLICKED(IDC_CHANNEL_LIST_RADIO, OnChannelListRadio)
	ON_BN_CLICKED(IDC_STEP_LIST_RADIO, OnStepListRadio)
	ON_BN_CLICKED(IDC_CHANNEL_PROCESS_RADIO, OnChannelProcessRadio)
	ON_BN_CLICKED(IDC_TRAY_FORM_RADIO, OnTrayFormRadio)
	ON_BN_CLICKED(IDC_CYCLE_RADIO, OnCycleRadio)
	ON_BN_CLICKED(IDC_CONVERT_RUN, OnConvertRun)
	ON_BN_CLICKED(IDC_SAVE_ITEM_BUTTON, OnSaveItemButton)
	ON_BN_CLICKED(IDC_FILE_ADD_CHECK, OnFileAddCheck)
	ON_BN_CLICKED(IDC_SAVE_FOLDER_BUTTON, OnSaveFolderButton)
	ON_BN_CLICKED(IDC_SAVE_UNIT_BUTTON, OnSaveUnitButton)
	ON_WM_COPYDATA()
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_AUTO_FILE_TRANS, OnAutoFileTrans)
	ON_WM_DESTROY()	
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CExcelTransDlg message handlers

BOOL CExcelTransDlg::OnInitDialog()
{
	//CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here

	m_nConvertType = AfxGetApp()->GetProfileInt("Settings", "ConvertType", 0);

	//초기값 표시
	CString strTemp;
	int nIndex;
	if(m_aSelFileName.GetSize() > 0)
	{
		for(int i=0; i<m_aSelFileName.GetSize(); i++)
		{
			strTemp = m_aSelFileName.GetAt(i);
			nIndex = strTemp.ReverseFind('\\');
			if(nIndex > 0)
			{
				m_strSelFileName = m_strSelFileName + "\"" + strTemp.Mid(nIndex+1) + "\" ";	//파일명만 표기
			}
		}
/*		//기본 저장 폴더 위치를 선택 파일 위치로 한다. 
		if(nIndex > 0)
		{
			m_strSaveFolderName = strTemp.Left(nIndex);	//Folder 표기 
		}
*/
	}


	strTemp.Format("%d File Selected", m_aSelFileName.GetSize());
	GetDlgItem(IDC_SEL_FILE_COUNT_STATIC)->SetWindowText(strTemp);
	
	//Channel Code Load
	LoadCodeTable();
	
	long rtn;
	HKEY hKey = 0;
	rtn = ::RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\CTSAnalyzer\\FormSetting", 0, KEY_READ, &hKey);
	
	//Tray Column Size를 Registry에서 읽어옴 
	if(ERROR_SUCCESS == rtn)
	{
		BYTE buf[64], buf2[64];
		DWORD size = 64;
		DWORD type;
		char szUnit[32], szDecimalPoint[32];
		
		//Tray Column Size 경로를 읽어온다. 
		rtn = ::RegQueryValueEx(hKey, "TrayColSize", NULL, &type, buf, &size);		
		//Column 수 읽어 들임 
		if(ERROR_SUCCESS == rtn && type == REG_DWORD)
		{
			sprintf((char *)buf2, "%ld", *(long *)buf);
			m_nColCount = atol((LPCTSTR)buf2);
		}

		size = 64;
		rtn = ::RegQueryValueEx(hKey, "V Unit", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && type == REG_SZ)
		{
			sscanf((char *)buf, "%s %s", szUnit, szDecimalPoint);
			m_strVUnit = szUnit;
			m_nVDecimal = atoi(szDecimalPoint);
		}

		size = 64;
		rtn = ::RegQueryValueEx(hKey, "I Unit", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && type == REG_SZ)
		{
			sscanf((char *)buf, "%s %s", szUnit, szDecimalPoint);
			m_strIUnit = szUnit;
			m_nIDecimal = atoi(szDecimalPoint);
		}

		size = 64;
		rtn = ::RegQueryValueEx(hKey, "C Unit", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && type == REG_SZ)
		{
			sscanf((char *)buf, "%s %s", szUnit, szDecimalPoint);
			m_strCUnit = szUnit;
			m_nCDecimal = atoi(szDecimalPoint);
		}

		size = 64;
		rtn = ::RegQueryValueEx(hKey, "Time Unit", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && type == REG_SZ)
		{
			m_nTimeUnit = atol((char *)buf);
		}

		::RegCloseKey(hKey);
	}

	m_bState = AfxGetApp()->GetProfileInt(REG_SECTION, "State", TRUE);
	m_bTime = AfxGetApp()->GetProfileInt(REG_SECTION, "Time", TRUE);
	m_bVoltage = AfxGetApp()->GetProfileInt(REG_SECTION, "Voltage", TRUE);
	m_bCurrent = AfxGetApp()->GetProfileInt(REG_SECTION, "Current", TRUE);
	m_bCapacity = AfxGetApp()->GetProfileInt(REG_SECTION, "Capacity", TRUE);
	m_bImpedance = AfxGetApp()->GetProfileInt(REG_SECTION, "Impedance", TRUE);
	m_bWatt = AfxGetApp()->GetProfileInt(REG_SECTION, "Watt", TRUE);
	m_bWattHour = AfxGetApp()->GetProfileInt(REG_SECTION, "WattHour", TRUE);
	m_bGradeCode = AfxGetApp()->GetProfileInt(REG_SECTION, "GradeCode", TRUE);
	m_bCellCode = AfxGetApp()->GetProfileInt(REG_SECTION, "CellCode", TRUE);
	m_bType = AfxGetApp()->GetProfileInt(REG_SECTION, "Type", TRUE);

	m_bTimeGetChargeVoltage = AfxGetApp()->GetProfileInt(REG_SECTION, "TimeGetChargeVoltage", TRUE);
	m_bComCapacity = AfxGetApp()->GetProfileInt(REG_SECTION, "ComCapacity", TRUE);
	m_bCCTime = AfxGetApp()->GetProfileInt(REG_SECTION, "CCTime", TRUE);
	m_bComDcir = AfxGetApp()->GetProfileInt(REG_SECTION, "ComDcir", TRUE);
	m_bDcir = AfxGetApp()->GetProfileInt(REG_SECTION, "Dcir", TRUE);
	m_bV1 = AfxGetApp()->GetProfileInt(REG_SECTION, "V1", TRUE);
	m_bV2 = AfxGetApp()->GetProfileInt(REG_SECTION, "V2", TRUE);
	m_bAvgJigTemp = AfxGetApp()->GetProfileInt(REG_SECTION, "AvgJigTemp", TRUE);
	m_bAvgCurrent = AfxGetApp()->GetProfileInt(REG_SECTION, "AvgCurrent", TRUE);
	 
	// m_bCapEff = AfxGetApp()->GetProfileInt(REG_SECTION, "CapEff", TRUE);
	// m_bSumCap = AfxGetApp()->GetProfileInt(REG_SECTION, "CapaSum", FALSE);
	
	m_strSaveFolderName= AfxGetApp()->GetProfileString(REG_SECTION, "LastPath", "");

	//파일을 선택하였고 저장 위치를 설정 하였으면 변환 버튼 Enable 시킴 
	if(m_aSelFileName.GetSize() > 0 && !m_strSaveFolderName.IsEmpty())
	{
		GetDlgItem(IDC_CONVERT_RUN)->EnableWindow(TRUE);
	}

	//Default Convert Type
	switch(m_nConvertType)
	{
	case 0:	OnChannelListRadio();	break;
	case 1:	OnStepListRadio()	;	break;
	case 2:	OnChannelProcessRadio();	break;
	case 3:	OnTrayFormRadio();		break;
	case 4:	OnCycleRadio()	;		break;
	}


	UpdateData(FALSE);
	
	return TRUE;  // return TRUE  unless you set the focus to a control

}

void CExcelTransDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CExcelTransDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CExcelTransDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CExcelTransDlg::OnFileSelButton() 
{
	// TODO: Add your control notification handler code here
	CString selFileName;
	if(m_aSelFileName.GetSize() > 0)
		selFileName = m_aSelFileName.GetAt(0);

//	AfxMessageBox(selFileName);

	CFileDialog pDlg(TRUE, "fmt", selFileName, OFN_ALLOWMULTISELECT|OFN_HIDEREADONLY|OFN_READONLY|OFN_FILEMUSTEXIST|OFN_PATHMUSTEXIST, "fmt file(*.fmt)|*.fmt|");

	char buff[8192] = {NULL,};
	pDlg.m_ofn.lpstrFile = buff; 
	pDlg.m_ofn.nMaxFile = 8192;

	int nRtn = pDlg.DoModal();
	if(IDOK == nRtn)
	{
		if(m_aSelFileName.GetSize() > 0)
			m_aSelFileName.RemoveAll();
		POSITION pos = pDlg.GetStartPosition();
		m_strSelFileName.Empty();

		int nIndex;
		while(pos != NULL)
		{
			selFileName = pDlg.GetNextPathName(pos);
			TRACE("FILE %s Selected\n", selFileName);
			nIndex = selFileName.ReverseFind('\\');
			//파일명만 Display
			if(nIndex > 0)
			{
				m_strSelFileName = m_strSelFileName+ "\"" +selFileName.Mid(nIndex+1) + "\" ";	//파일명만 표기
			}
			m_aSelFileName.Add(selFileName);	//path 저장 
		}

		//파일을 선택하였고 저장 위치를 설정 하였으면 변환 버튼 Enable 시킴 
		if(m_aSelFileName.GetSize() > 0 && !m_strSaveFolderName.IsEmpty())
		{
			GetDlgItem(IDC_CONVERT_RUN)->EnableWindow(TRUE);
		}
		UpdateData(FALSE);
	}
	
	selFileName.Format(TEXT_LANG[0], m_aSelFileName.GetSize());
	GetDlgItem(IDC_SEL_FILE_COUNT_STATIC)->SetWindowText(selFileName);
}

void CExcelTransDlg::OnSelSaveFile() 
{
	// TODO: Add your control notification handler code here
	CFileDialog pDlg(FALSE, "csv", m_strSaveFileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "csv file(*.csv)|*.csv|");

	if(IDOK == pDlg.DoModal())
	{
		m_strSaveFileName = pDlg.GetPathName();
		UpdateData(FALSE);
	}
}

void CExcelTransDlg::OnChannelListRadio() 
{
	// TODO: Add your control notification handler code here
	CString strPreView;
	strPreView = "[Ch1]\n[StepNo]  [Volt] [Curr] [Capa] ...\nStep1 :\nStep2 :\n   :\n\n[Ch2]\nStep1 :\nStep2 :\n   :";
	GetDlgItem(IDC_PREVIEW_STATIC)->SetWindowText(strPreView);
	m_nConvertType = 0;
	((CButton *)(GetDlgItem(IDC_CHANNEL_LIST_RADIO)))->SetCheck(TRUE);
}

void CExcelTransDlg::OnStepListRadio() 
{
	// TODO: Add your control notification handler code here
	CString strPreView;
	strPreView = "[Step1]\n[Ch No]  [Volt] [Curr] [Capa] ...\nCh001 :\nCh002 :\n   :\n\n[Step2]\nCh001 :\nCh002 :\n   :";
	GetDlgItem(IDC_PREVIEW_STATIC)->SetWindowText(strPreView);	
	m_nConvertType = 1;
	((CButton *)(GetDlgItem(IDC_STEP_LIST_RADIO)))->SetCheck(TRUE);
}

void CExcelTransDlg::OnChannelProcessRadio() 
{
	// TODO: Add your control notification handler code here
	CString strPreView;
	strPreView = "[Ch No]  [Step1 V] [Step1 I] [Step1 C] [Step2 V]...\nCh001 :\nCh002 :\nCh003 :\n   :\n";
	GetDlgItem(IDC_PREVIEW_STATIC)->SetWindowText(strPreView);		
	m_nConvertType = 2;
	((CButton *)(GetDlgItem(IDC_CHANNEL_PROCESS_RADIO)))->SetCheck(TRUE);
}

void CExcelTransDlg::OnTrayFormRadio() 
{
	// TODO: Add your control notification handler code here
	CString strPreView;
	strPreView = "[Voltage]\n[ / ] [ A ][ B ] [ C ] [ D ] ...\n[ 1 ]\n[ 2 ]\n  :\n[Current]\n[ / ] [ A ][ B ] [ C ] [ D ] ...\n[ 1 ]\n[ 2 ]\n  :";
	GetDlgItem(IDC_PREVIEW_STATIC)->SetWindowText(strPreView);			
	m_nConvertType = 3;
	((CButton *)(GetDlgItem(IDC_TRAY_FORM_RADIO)))->SetCheck(TRUE);
}

void CExcelTransDlg::OnCycleRadio() 
{
	// TODO: Add your control notification handler code here
	CString strPreView;
	strPreView = "[ Ch ][CycNo][Cha C][Cha Time][Dis C][Dis T][Eff(%)]\n[  1  ]\n[  2  ]\n[  3  ]\n   :\n";
//	strPreView  += "[  Ch2  ]\n[    1    ]\n[    2    ]\n[    3    ]\n      :\n";

	GetDlgItem(IDC_PREVIEW_STATIC)->SetWindowText(strPreView);				
	m_nConvertType = 4;
	((CButton *)(GetDlgItem(IDC_CYCLE_RADIO)))->SetCheck(TRUE);
}


void CExcelTransDlg::OnConvertRun() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

	CString strFile, strform;

	if(m_bFileAppend && m_strSaveFileName.IsEmpty())
	{
		AfxMessageBox("File name is empty", MB_ICONINFORMATION);
		GetDlgItem(IDC_SAVE_FILE_EDIT)->SetFocus();
		return;
	}	

	if(ConvertData() == FALSE)
	{
		strform.Format("%s failed conversion.", strFile);
		MessageBox(strform, "Conversion Error", MB_OK|MB_ICONSTOP);
		return;
	}
	
	// Excel로 Open 여부를 묻는다.	
	if( m_bExcelOpenFlag == false )
	{
		return;
	}

	if(!m_bFileAppend)	//1개의 파일로 통합시 
	{
		if(m_aSelFileName.GetSize() > 5)
		{
			strFile.Format(TEXT_LANG[1]);
			if(MessageBox(strFile, "File Open", MB_ICONQUESTION|MB_YESNO) == IDNO)
				return;
		}
	}

	//  Excel로 실행
/*		HWND FirsthWnd;
		FirsthWnd = ::FindWindow(NULL, "Excel");
		if (FirsthWnd)
		{
			::SetActiveWindow(FirsthWnd);	
			::SetForegroundWindow(FirsthWnd);
			return;
		}
*/

	STARTUPINFO	stStartUpInfo;
	PROCESS_INFORMATION	ProcessInfo;
	ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
	ZeroMemory(&ProcessInfo, sizeof(PROCESS_INFORMATION));
		
	stStartUpInfo.cb = sizeof(STARTUPINFO);
	stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
	stStartUpInfo.wShowWindow = SW_NORMAL;

	CString strExcelPath = AfxGetApp()->GetProfileString(REG_SECTION, "Excel Path");
	CFileFind finder;
	if(finder.FindFile(strExcelPath) == FALSE)
	{
		CFileDialog pDlg(TRUE, "exe", strExcelPath, OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, "exe file(*.exe)|*.exe|All Files(*.*)|*.*|");
		pDlg.m_ofn.lpstrTitle = TEXT_LANG[2];
		if(IDOK != pDlg.DoModal())
		{
			return;
		}
		strExcelPath = pDlg.GetPathName();
		AfxGetApp()->WriteProfileString(REG_SECTION,"Excel Path", strExcelPath);
	}

	CString strTemp;
	char buff1[512];
	char buff2[512];

	GetShortPathName((LPSTR)(LPCTSTR)strExcelPath, buff2, 511);

	if(m_bFileAppend)	//1개의 파일로 통합시 
	{
		GetShortPathName((LPSTR)(LPCTSTR)m_strSaveFileName, buff1, 511);
		strTemp.Format("%s %s", buff2, buff1);
		BOOL bFlag = ::CreateProcess(NULL, (LPSTR)(LPCTSTR)strTemp, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &stStartUpInfo, &ProcessInfo);
		if(bFlag == FALSE)
		{
			strform.LoadString(IDS_TEXT_NOT_EXECUTE);
			MessageBox(strform, "Error", MB_OK|MB_ICONSTOP);
		}
	}
	else
	{
		for(int i=0; i<m_aSelFileName.GetSize(); i++)
		{
			strFile = GetSaveFileName(m_aSelFileName.GetAt(i));
			GetShortPathName((LPSTR)(LPCTSTR)strFile, buff1, 511);

			strTemp.Format("%s %s", buff2, buff1);
			BOOL bFlag = ::CreateProcess(NULL, (LPSTR)(LPCTSTR)strTemp, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &stStartUpInfo, &ProcessInfo);
			if(bFlag == FALSE)
			{
				strform.LoadString(IDS_TEXT_NOT_EXECUTE);
				MessageBox(strform, "Error", MB_OK|MB_ICONSTOP);
				break;
			}
		}
	}
}

void CExcelTransDlg::OnSaveItemButton() 
{
	// TODO: Add your control notification handler code here
	CSaveItemSetDlg dlg;
/*	dlg.m_bState = m_bState;
	dlg.m_bTime = m_bTime;
	dlg.m_bVoltage = m_bVoltage;
	dlg.m_bCurrent = m_bCurrent;
	dlg.m_bCapacity = m_bCapacity;
	dlg.m_bCapEff = m_bCapEff;
	dlg.m_bImpedance = m_bImpedance;
	dlg.m_bWatt = m_bWatt;
	dlg.m_bWattHour = m_bWattHour;
	dlg.m_bGradeCode = m_bGradeCode;
	dlg.m_bCellCode = m_bCellCode;
	dlg.m_bType = m_bType;
	dlg.m_bSumCap = m_bSumCap;
*/	if(dlg.DoModal() == IDOK)
	{
		m_bState = dlg.m_bState;
		m_bTime = dlg.m_bTime;
		m_bVoltage = dlg.m_bVoltage;
		m_bCurrent = dlg.m_bCurrent;
		m_bCapacity = dlg.m_bCapacity;
		m_bCapEff = dlg.m_bCapEff;
		m_bImpedance = dlg.m_bImpedance;
		m_bWatt = dlg.m_bWatt;
		m_bWattHour = dlg.m_bWattHour;
		m_bGradeCode = dlg.m_bGradeCode;
		m_bCellCode = dlg.m_bCellCode;
		m_bType = dlg.m_bType;			
		m_bSumCap = dlg.m_bSumCap;

		m_bTimeGetChargeVoltage = dlg.m_bTimeGetChargeVoltage;
		m_bComCapacity = dlg.m_bComCapacity;
		m_bCCTime = dlg.m_bCCTime;
		m_bComDcir = dlg.m_bComDcir;
		m_bDcir = dlg.m_bDcir;
		m_bV1 = dlg.m_bV1;
		m_bV2 = dlg.m_bV2;
		m_bAvgJigTemp = dlg.m_bAvgJigTemp;
		m_bAvgCurrent = dlg.m_bAvgCurrent;
	}
}

void CExcelTransDlg::OnFileAddCheck() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	GetDlgItem(IDC_SAVE_FILE_EDIT)->EnableWindow(m_bFileAppend);
	GetDlgItem(IDC_SEL_SAVE_FILE)->EnableWindow(m_bFileAppend);
}

BOOL CExcelTransDlg::SaveChStepFile(CString strFile)
{
	CFormResultFile formFile;
	if(formFile.ReadFile(strFile, m_bSumCap) < 0)
	{
		CString strform;
		strform.LoadString(IDS_TEXT_NOT_READ);
		strFile += strform;
		AfxMessageBox(strFile);
		return FALSE;
	}
	
	//기존 저장 대상 파일명의 확장자를 csv로 바꿈
	CString strSaveFileName;
	char szMode[8];
	if(m_bFileAppend)	//1개의 파일로 통합시 
	{
		strSaveFileName = m_strSaveFileName;
		sprintf(szMode, "at+");
	}
	else				//각각의 파일로 저장 
	{
		strSaveFileName= GetSaveFileName(strFile);	
		sprintf(szMode, "wt");
	}

	//Open Save File
	FILE	*fp = fopen(strSaveFileName, szMode);
	if (fp == NULL)	return FALSE;

	//Header 기록 
	if(WriteFileHeader(fp, &formFile) == FALSE)
	{
		fclose(fp);
		return FALSE;
	}

	BeginWaitCursor();

	fprintf(fp, "Ch,");
	fprintf(fp, "CellSerial,");
	fprintf(fp, "Step,");
	if(m_bType)		fprintf(fp, "Type,");
	if(m_bState)	fprintf(fp, "State,");
	if(m_bTime)		fprintf(fp, "Time,");
	if(m_bVoltage)	fprintf(fp, "Voltage(%s),", m_strVUnit);
	if(m_bCurrent)	fprintf(fp, "Current(%s),", m_strIUnit);
	if(m_bCapacity)	fprintf(fp, "Capacity(%s),", m_strCUnit);
	if(m_bImpedance)fprintf(fp, "Impedance(mOhm),");
	if(m_strIUnit == "A")
	{
		if(m_bWatt)		fprintf(fp, "Watt(W),");
		if(m_bWattHour)	fprintf(fp, "WattHour(Wh),");
	}
	else
	{
		if(m_bWatt)		fprintf(fp, "Watt(mW),");
		if(m_bWattHour)	fprintf(fp, "WattHour(mWh),");
	}
	if(m_bGradeCode)fprintf(fp, "Grade,");
	if(m_bCellCode)	fprintf(fp, "Code,");

	if(m_bTimeGetChargeVoltage) fprintf(fp, "TimeGetChargeVoltage(%s),", m_strVUnit);
	if(m_bComCapacity) fprintf(fp, "ComCapacity(%s),", m_strCUnit);
	if(m_bCCTime) fprintf(fp, "CCTime,");
	if(m_bComDcir) fprintf(fp, "ComDcir(mOhm),");
	if(m_bDcir) fprintf(fp, "Dcir(mOhm),");
	if(m_bComCapacity) fprintf(fp, "ComCapacity(%s),", m_strCUnit);
	if(m_bV1) fprintf(fp, "V1(%s),", m_strVUnit);
	if(m_bV2) fprintf(fp, "V2(%s),", m_strVUnit);
	if(m_bAvgJigTemp) fprintf(fp, "AvgJigTemp,");
	if(m_bAvgCurrent)	fprintf(fp, "AvgCurrent(%s),", m_strIUnit);

	fprintf(fp, "\n");	

	STR_SAVE_CH_DATA	*lpChannel;
	STR_STEP_RESULT	*lpStepData;
	lpStepData = formFile.GetFirstStepData();
	if(lpStepData == NULL )								//저장된 Step Data가 없을 경우
	{
		fclose(fp);
		return FALSE;
	}

	int chSize = lpStepData->aChData.GetSize();		// 저장된 Channel 수
	int nStepSize = formFile.GetStepSize();			//저장된 결과 Step 수

	for(int ch = 0; ch < chSize; ch++)
	{
		for (int step = 0; step < nStepSize ; step++)
		{
			lpStepData = formFile.GetStepData(step);
			ASSERT(lpStepData);
			if(ch >= lpStepData->aChData.GetSize())		break;	//각 Step의 Channel 수가 다를 경우
			lpChannel = (STR_SAVE_CH_DATA *)lpStepData->aChData[ch];
			ASSERT(lpChannel);
			
			fprintf(fp, "%d,", lpChannel->wChIndex+1);
			fprintf(fp, "%s,", formFile.GetCellSerial(lpChannel->wChIndex));	
			fprintf(fp, "%d,", lpChannel->nStepNo);	
			if(m_bType)			fprintf(fp, "%s,", StepTypeMsg(lpStepData->stepCondition.stepHeader.type));		
			if(m_bState)		fprintf(fp, "%s,", ValueString(lpChannel->state, EP_STATE));	// 상태
			if(m_bTime)			fprintf(fp, "%s,", ValueString(lpChannel->fStepTime, EP_STEP_TIME));	// Step Time
			if(m_bVoltage)		fprintf(fp, "%s,", ValueString(lpChannel->fVoltage, EP_VOLTAGE));	// Voltage
			if(m_bCurrent)		fprintf(fp, "%s,", ValueString(lpChannel->fCurrent, EP_CURRENT));	// Current
			if(m_bCapacity)		fprintf(fp, "%s,", ValueString(lpChannel->fCapacity, EP_CAPACITY));	// Capacity
			if(m_bImpedance)	fprintf(fp, "%s,", ValueString(lpChannel->fImpedance, EP_IMPEDANCE));	
			if(m_bWatt)			fprintf(fp, "%s,", ValueString(lpChannel->fWatt, EP_WATT));			
			if(m_bWattHour)	    fprintf(fp, "%s,", ValueString(lpChannel->fWattHour, EP_WATT_HOUR));			// WattHour
			if(m_bGradeCode)	fprintf(fp, "%s,", ValueString(lpChannel->grade, EP_GRADE_CODE));
			if(m_bCellCode)		fprintf(fp, "%s,", ValueString(lpChannel->channelCode, EP_CH_CODE));				// Failure Code

			if(m_bTimeGetChargeVoltage)		fprintf(fp, "%s,", ValueString(lpChannel->fTimeGetChargeVoltage, EP_VOLTAGE));
			if(m_bComCapacity)				fprintf(fp, "%s,", ValueString(lpChannel->fCapacity, EP_CAPACITY));
			if(m_bCCTime)					fprintf(fp, "%s,", ValueString(lpChannel->fCcRunTime, EP_STEP_TIME));
			if(m_bComDcir)					fprintf(fp, "%s,", ValueString(lpChannel->fComDCIR, EP_IMPEDANCE));	
			if(m_bDcir)						fprintf(fp, "%s,", ValueString(lpChannel->fImpedance, EP_IMPEDANCE));	
			if(m_bComCapacity)				fprintf(fp, "%s,", ValueString(lpChannel->fComCapacity, EP_CAPACITY));
			if(m_bV1)						fprintf(fp, "%s,", ValueString(lpChannel->fDCIR_V1, EP_VOLTAGE));
			if(m_bV2)						fprintf(fp, "%s,", ValueString(lpChannel->fDCIR_V2, EP_VOLTAGE));
			if(m_bAvgJigTemp)				fprintf(fp, "%s,", ValueString(lpChannel->fDCIR_AvgTemp, EP_TEMPER));
			if(m_bAvgCurrent)				fprintf(fp, "%s,", ValueString(lpChannel->fDCIR_AvgCurrent, EP_CURRENT));
		
			fprintf(fp, "\n");	 
		}
		fprintf(fp, "\n");	
	}

	fclose(fp);
	fp = NULL;
	EndWaitCursor();
	return TRUE;
}

BOOL CExcelTransDlg::SaveStepChListFile(CString strFile)
{
	CFormResultFile formFile;
	if(formFile.ReadFile(strFile, m_bSumCap) < 0)
	{
		CString strform;
		strform.LoadString(IDS_TEXT_NOT_READ);
		strFile += strform;
		AfxMessageBox(strFile);
		return FALSE;
	}
	
	//기존 저장 대상 파일명의 확장자를 csv로 바꿈
	CString strSaveFileName;
	char szMode[8];
	if(m_bFileAppend)	//1개의 파일로 통합시 
	{
		strSaveFileName = m_strSaveFileName;
		sprintf(szMode, "at+");
	}
	else				//각각의 파일로 저장 
	{
		strSaveFileName= GetSaveFileName(strFile);	
		sprintf(szMode, "wt");
	}

	//Open Save File
	FILE	*fp = fopen(strSaveFileName, szMode);
	if (fp == FALSE)	return FALSE;

	//Header 기록 
	if(WriteFileHeader(fp, &formFile) == FALSE)
	{
		fclose(fp);
		return FALSE;
	}

	BeginWaitCursor();

	fprintf(fp, "Ch,");
	fprintf(fp, "CellSerial,");
	fprintf(fp, "Step,");
	if(m_bType)		fprintf(fp, "Type,");
	if(m_bState)	fprintf(fp, "State,");
	if(m_bTime)		fprintf(fp, "Time,");
	if(m_bVoltage)	fprintf(fp, "Voltage(%s),", m_strVUnit);
	if(m_bCurrent)	fprintf(fp, "Current(%s),", m_strIUnit);
	if(m_bCapacity)	fprintf(fp, "Capacity(%s),", m_strCUnit);
	if(m_bImpedance)fprintf(fp, "Impedance(mOhm),");
	if(m_strIUnit == "A")
	{
		if(m_bWatt)		fprintf(fp, "Watt(W),");
		if(m_bWattHour)	fprintf(fp, "WattHour(Wh),");
	}
	else
	{
		if(m_bWatt)		fprintf(fp, "Watt(mW),");
		if(m_bWattHour)	fprintf(fp, "WattHour(mWh),");
	}
	if(m_bGradeCode)fprintf(fp, "Grade,");
	if(m_bCellCode)	fprintf(fp, "Code,");

	if(m_bTimeGetChargeVoltage) fprintf(fp, "TimeGetChargeVoltage(%s),", m_strVUnit);
	if(m_bComCapacity) fprintf(fp, "ComCapacity(%s),", m_strCUnit);
	if(m_bCCTime) fprintf(fp, "CCTime,");
	if(m_bComDcir) fprintf(fp, "ComDcir(mOhm),");
	if(m_bDcir) fprintf(fp, "Dcir(mOhm),");
	if(m_bComCapacity) fprintf(fp, "ComCapacity(%s),", m_strCUnit);
	if(m_bV1) fprintf(fp, "V1(%s),", m_strVUnit);
	if(m_bV2) fprintf(fp, "V2(%s),", m_strVUnit);
	if(m_bAvgJigTemp) fprintf(fp, "AvgJigTemp,");
	if(m_bAvgCurrent)	fprintf(fp, "AvgCurrent(%s),", m_strIUnit);


	fprintf(fp, "\n");	

	STR_SAVE_CH_DATA	*lpChannel;
	STR_STEP_RESULT *lpStepData;
	int nStepSize = formFile.GetStepSize();
	int chSize;
//	BYTE color;

	for (int step = 0; step < nStepSize ; step++)
	{
		lpStepData = formFile.GetStepData(step);
		if(lpStepData == NULL)	break;
		chSize = lpStepData->aChData.GetSize();

		fprintf(fp, "\nStep:%d, Type:%s\n", step+1, StepTypeMsg(lpStepData->stepCondition.stepHeader.type));

		for(int ch =0; ch < chSize; ch++)
		{
			lpChannel = (STR_SAVE_CH_DATA *)lpStepData->aChData[ch];
			if(lpChannel == NULL)	break;
			
			fprintf(fp, "%d,", lpChannel->wChIndex+1);
			fprintf(fp, "%s,", formFile.GetCellSerial(lpChannel->wChIndex));	
			fprintf(fp, "%d,", lpChannel->nStepNo);	

			if(m_bType)			fprintf(fp, "%s,", StepTypeMsg(lpStepData->stepCondition.stepHeader.type));			
			if(m_bState)		fprintf(fp, "%s,", ValueString(lpChannel->state, EP_STATE));	// 상태
			if(m_bTime)			fprintf(fp, "%s,", ValueString(lpChannel->fStepTime, EP_STEP_TIME));	// Step Time
			if(m_bVoltage)		fprintf(fp, "%s,", ValueString(lpChannel->fVoltage, EP_VOLTAGE));	// Voltage
			if(m_bCurrent)		fprintf(fp, "%s,", ValueString(lpChannel->fCurrent, EP_CURRENT));	// Current
			if(m_bCapacity)		fprintf(fp, "%s,", ValueString(lpChannel->fCapacity, EP_CAPACITY));	// Capacity
			if(m_bImpedance)	fprintf(fp, "%s,", ValueString(lpChannel->fImpedance, EP_IMPEDANCE));	
			if(m_bWatt)			fprintf(fp, "%s,", ValueString(lpChannel->fWatt, EP_WATT));			
			if(m_bWattHour)	    fprintf(fp, "%s,", ValueString(lpChannel->fWattHour, EP_WATT_HOUR));			// WattHour
			if(m_bGradeCode)	fprintf(fp, "%s,", ValueString(lpChannel->grade, EP_GRADE_CODE));
			if(m_bCellCode)		fprintf(fp, "%s,", ValueString(lpChannel->channelCode, EP_CH_CODE));				// Failure Code

			if(m_bTimeGetChargeVoltage)		fprintf(fp, "%s,", ValueString(lpChannel->fTimeGetChargeVoltage, EP_VOLTAGE));
			if(m_bComCapacity)				fprintf(fp, "%s,", ValueString(lpChannel->fCapacity, EP_CAPACITY));
			if(m_bCCTime)					fprintf(fp, "%s,", ValueString(lpChannel->fCcRunTime, EP_STEP_TIME));
			if(m_bComDcir)					fprintf(fp, "%s,", ValueString(lpChannel->fComDCIR, EP_IMPEDANCE));	
			if(m_bDcir)						fprintf(fp, "%s,", ValueString(lpChannel->fImpedance, EP_IMPEDANCE));	
			if(m_bComCapacity)				fprintf(fp, "%s,", ValueString(lpChannel->fComCapacity, EP_CAPACITY));
			if(m_bV1)						fprintf(fp, "%s,", ValueString(lpChannel->fDCIR_V1, EP_VOLTAGE));
			if(m_bV2)						fprintf(fp, "%s,", ValueString(lpChannel->fDCIR_V2, EP_VOLTAGE));
			if(m_bAvgJigTemp)				fprintf(fp, "%s,", ValueString(lpChannel->fDCIR_AvgTemp, EP_TEMPER));
			if(m_bAvgCurrent)				fprintf(fp, "%s,", ValueString(lpChannel->fDCIR_AvgCurrent, EP_CURRENT));

			fprintf(fp, "\n");	 
		}
		fprintf(fp, "\n");	
	}
	
	fclose(fp);
	fp = NULL;
	EndWaitCursor();
	return TRUE;	
}
/*
BOOL CExcelTransDlg::SaveChCellListFile(CString strFile)
{
	CFormResultFile formFile;
	if(formFile.ReadFile(strFile, m_bSumCap) < 0)
	{
		CString strform;
		strform.LoadString(IDS_TEXT_NOT_READ);
		strFile += strform;
		AfxMessageBox(strFile);
		return FALSE;
	}

	//기존 저장 대상 파일명의 확장자를 csv로 바꿈
	CString strSaveFileName;
	char szMode[8];
	if(m_bFileAppend)	//1개의 파일로 통합시 
	{
		strSaveFileName = m_strSaveFileName;
		sprintf(szMode, "at+");
	}
	else				//각각의 파일로 저장 
	{
		strSaveFileName= GetSaveFileName(strFile);	
		sprintf(szMode, "wt");
	}

	//Open Save File
	FILE	*fp = fopen(strSaveFileName, szMode);
	if (fp == FALSE)	return FALSE;

	//Header 기록 
	if(WriteFileHeader(fp, &formFile) == FALSE)
	{
		fclose(fp);
		return FALSE;
	}

	//필요한 Column 수를 검사하고 Column Header를 기록 
	int nColumnCount = 2;		//기본적으로 ChannelNo, CellCode Column은 제공

	STR_SAVE_CH_DATA	*lpChannel;
	STR_STEP_RESULT	*lpStepData;
	lpStepData = formFile.GetFirstStepData();
	if(lpStepData == NULL )								//저장된 Step Data가 없을 경우
	{
		fclose(fp);
		return FALSE;
	}
	int chSize = lpStepData->stepData.chData.GetSize();	//저장된 Channel 수
	int nStepSize = formFile.GetStepSize();				//저장된 결과 Step 수

	BeginWaitCursor();
	fprintf(fp, "Ch,");
	
	for (int step = 0; step < nStepSize ; step++)
	{
		lpStepData = formFile.GetStepData(step);
		if(lpStepData == NULL)	break;

		//StepType에 따라 필요한 Column수 Count
		switch(lpStepData->stepCondition.stepHeader.type)
		{
		case EP_TYPE_CHARGE:
			if(m_bVoltage)	{ nColumnCount++;	fprintf(fp, "%d:Char_Vtg,", step+1);	}
			if(m_bCurrent)	{ nColumnCount++;	fprintf(fp, "%d:Char_Crt,", step+1);	}
			if(m_bCapacity)	{ nColumnCount++;	fprintf(fp, "%d:Char_Cap,", step+1);	}
			if(m_bTime)		{ nColumnCount++;	fprintf(fp, "%d:Char_Time,", step+1);	}
			if(m_bWatt)		{ nColumnCount++;	fprintf(fp, "%d:Char_Watt,", step+1);	}
			if(m_bWattHour)	{ nColumnCount++;	fprintf(fp, "%d:Char_Wh,", step+1);		}
			break;
		case EP_TYPE_DISCHARGE:
			if(m_bVoltage)	{ nColumnCount++;	fprintf(fp, "%d:Disch_Vtg,", step+1);	}
			if(m_bCurrent)	{ nColumnCount++;	fprintf(fp, "%d:Disch_Crt,", step+1);	}
			if(m_bCapacity)	{ nColumnCount++;	fprintf(fp, "%d:Disch_Cap,", step+1);	}
			if(m_bCapEff)	{ nColumnCount++;	fprintf(fp, "%d:Cap_Eff(%%),", step+1);	}
			if(m_bTime)		{ nColumnCount++;	fprintf(fp, "%d:Disch_Time,", step+1);	}
			if(m_bWatt)		{ nColumnCount++;	fprintf(fp, "%d:Disch_Watt,", step+1);	}
			if(m_bWattHour)	{ nColumnCount++;	fprintf(fp, "%d:Disch_Wh,", step+1);	}
			break;
		case EP_TYPE_OCV:
			if(m_bVoltage)	{ nColumnCount++;	fprintf(fp, "%d:OCV,", step+1);	}
			break;
		case EP_TYPE_IMPEDANCE:
			if(m_bImpedance){ nColumnCount++;	fprintf(fp, "%d:Imp,", step+1);	}
			break;
		default:
			break;
		}
	}
	fprintf(fp, "\n");

	//Excel 최대 Column 수 검사
	if(nColumnCount > 256)
	{
		EndWaitCursor();
		fclose(fp);
		_unlink(strSaveFileName);	//생성된 파일 삭제 
		AfxMessageBox(IDS_TEXT_OVER_PARAMETER, MB_OK|MB_ICONSTOP);	
		return FALSE;
	}

	for(int ch =0; ch < chSize; ch++)
	{
		fprintf(fp, "%d,", ch+1);
		
		for (step = 0; step < nStepSize ; step++)
		{
			lpStepData = formFile.GetStepData(step);
			if(lpStepData == NULL)	break;

			lpChannel = (STR_SAVE_CH_DATA *)lpStepData->stepData.chData[ch];
			if(lpChannel == NULL)	break;

			switch(lpStepData->stepCondition.stepHeader.type)
			{
			case EP_TYPE_CHARGE:
				if(m_bVoltage)	{ fprintf(fp, "%s,", ValueString(lpChannel->lVoltage, EP_VOLTAGE));	}
				if(m_bCurrent)	{ fprintf(fp, "%s,", ValueString(lpChannel->lCurrent, EP_CURRENT));	}
				if(m_bCapacity)	{ fprintf(fp, "%s,", ValueString(lpChannel->lCapacity, EP_CAPACITY));	}
				if(m_bTime)		{ fprintf(fp, "%s,", ValueString(lpChannel->ulStepTime, EP_STEP_TIME));	}
				if(m_bWatt)		{ fprintf(fp, "%s,", ValueString(lpChannel->lWatt, EP_WATT));		}
				if(m_bWattHour)	{ fprintf(fp, "%s,", ValueString(lpChannel->lWattHour, EP_WATT_HOUR));}
				break;
			case EP_TYPE_DISCHARGE:
				if(m_bVoltage)	{ fprintf(fp, "%s,", ValueString(lpChannel->lVoltage, EP_VOLTAGE));	}
				if(m_bCurrent)	{ fprintf(fp, "%s,", ValueString(lpChannel->lCurrent, EP_CURRENT));	}
				if(m_bCapacity)	{ fprintf(fp, "%s,", ValueString(lpChannel->lCapacity, EP_CAPACITY)); }
				//방전의 경우 totalTime에 방전효율이 *100 형태로 저장 되어 있음 
				if(m_bCapEff)	{ fprintf(fp, "%s,", ValueString(lpChannel->ulTotalTime, EP_CAPACITY)); }
				if(m_bTime)		{ fprintf(fp, "%s,", ValueString(lpChannel->ulStepTime, EP_STEP_TIME));	}
				if(m_bWatt)		{ fprintf(fp, "%s,", ValueString(lpChannel->lWatt, EP_WATT)); }
				if(m_bWattHour)	{ fprintf(fp, "%s,", ValueString(lpChannel->lWattHour, EP_WATT_HOUR));}
				break;
			case EP_TYPE_OCV:
				if(m_bVoltage)	{ fprintf(fp, "%s,", ValueString(lpChannel->lVoltage, EP_VOLTAGE));	}
				break;
			case EP_TYPE_IMPEDANCE:
				if(m_bImpedance){ fprintf(fp, "%s,", ValueString(lpChannel->lImpedance, EP_IMPEDANCE));	}
				break;
			default:
				break;
			}
		}
		fprintf(fp, "\n");
	}

	fclose(fp);
	fp = NULL;
	EndWaitCursor();
	return TRUE;	
}
*/

BOOL CExcelTransDlg::SaveChCellListFile(CString strFile)
{
	CFormResultFile formFile;
	if(formFile.ReadFile(strFile, m_bSumCap) < 0)
	{
		CString strform;
		strform.LoadString(IDS_TEXT_NOT_READ);
		strFile += strform;
		AfxMessageBox(strFile);
		return FALSE;
	}

	//기존 저장 대상 파일명의 확장자를 csv로 바꿈
	CString strSaveFileName;
	char szMode[8];
	if(m_bFileAppend)	//1개의 파일로 통합시 
	{
		strSaveFileName = m_strSaveFileName;
		sprintf(szMode, "at+");
	}
	else				//각각의 파일로 저장 
	{
		strSaveFileName= GetSaveFileName(strFile);	
		sprintf(szMode, "wt");
	}

	int nColumnCount = 0;		
	int nColumnCountInFile = 0;
	int nFileCount = 0;
	int nStartStep = 0;
	STR_SAVE_CH_DATA	*lpChannel;
	STR_STEP_RESULT	*lpStepData;
	int nFileStepArray[128];			//Step이 길어 여러파일에 분할 저장할 경우 파일에 저장할수 있는 Step 번호 저장

	//2004/9/9 KBH
	//최대 Column수가 256을 넘으면 파일을 분할 하여 저장 하도록 함 

	//전체 필요 Column 수를 계산

	BeginWaitCursor();

	nColumnCountInFile = 1;		//기본 채널번호 저장 필요
	nColumnCount = 1;
	nFileCount = 0;
	ZeroMemory(nFileStepArray, sizeof(nFileStepArray));

	lpStepData = formFile.GetFirstStepData();
	if(lpStepData == NULL )								//저장된 Step Data가 없을 경우
	{
		return FALSE;
	}
	int chSize = lpStepData->aChData.GetSize();	//저장된 Channel 수
	int nStepSize = formFile.GetStepSize();				//저장된 결과 Step 수

	for (int step = 0; step < nStepSize ; step++)
	{
		lpStepData = formFile.GetStepData(step);
		if(lpStepData == NULL)	break;
		
		int nStepCol = GetColumnCount(lpStepData->stepCondition.stepHeader.type);
		TRACE("Step %d : %d Column\n", step+1, nStepCol);
		if((nColumnCountInFile+nStepCol) > MAX_EXCEL_COLUMN)		//현재 Step을 추가시 over하게 되면 	
		{
			nFileStepArray[nFileCount++] = step;	//step 번호 저장
			nColumnCountInFile = nStepCol+1;		//채널 번호 Column 포함 
			nColumnCount++;
		}
		else										//현재 파일에 저장 가능한 step
		{
			nColumnCountInFile += nStepCol;
		}
		nColumnCount += nStepCol;				//전체 Column 수 
	}

	//Excel 최대 Column 수 검사
	if(nColumnCount > MAX_EXCEL_COLUMN)
	{
		CString strTemp;
		strTemp.LoadString(IDS_TEXT_OVER_COLUMN);
		if(MessageBox(strTemp, "", MB_YESNO|MB_ICONQUESTION) != IDYES)
		{
			EndWaitCursor();
			return FALSE;
		}
		nFileStepArray[nFileCount++] = nStepSize;
	}
	else
	{
		ASSERT(nColumnCount == nColumnCountInFile);
		
		nFileStepArray[0] = nStepSize;
		nFileCount = 1;
	}
	
	CString newFilename;
	for(int name = 0; name < nFileCount; name++)
	{
		//Make Save File Name
		if(name > 0)						//연속된 새로운 이름 생성
			newFilename.Format("%s_%d.csv", strSaveFileName.Left(strSaveFileName.ReverseFind('.')), name);
		else 
			newFilename = strSaveFileName;	//1개이면 파일이름 그대로 사용
			
		FILE	*fp = fopen(newFilename, szMode);
		if (fp == FALSE)	
			break;

		//Header 기록 
		if(WriteFileHeader(fp, &formFile) == FALSE)
		{
			fclose(fp);
			return FALSE;
		}

		fprintf(fp, "Ch,CellSerial,");
		
		if(name <=0 )		nStartStep = 0;
		else				nStartStep = nFileStepArray[name-1];

		for (int step = nStartStep; step < nFileStepArray[name] ; step++)
		{
			lpStepData = formFile.GetStepData(step);
			if(lpStepData == NULL)	break;

			//StepType에 따라 필요한 Column수 Count
			switch(lpStepData->stepCondition.stepHeader.type)
			{
			case EP_TYPE_CHARGE:
				if(m_bVoltage)	{ 	fprintf(fp, "%d:Char_Vtg,", step+1);	}
				if(m_bCurrent)	{ 	fprintf(fp, "%d:Char_Crt,", step+1);	}
				if(m_bCapacity)	{ 	fprintf(fp, "%d:Char_Cap,", step+1);	}
				if(m_bTime)		{ 	fprintf(fp, "%d:Char_Time,", step+1);	}
				if(m_bWatt)		{ 	fprintf(fp, "%d:Char_Watt,", step+1);	}
				if(m_bWattHour)	{ 	fprintf(fp, "%d:Char_Wh,", step+1);		}
				break;
			case EP_TYPE_DISCHARGE:
				if(m_bVoltage)	{ 	fprintf(fp, "%d:Disch_Vtg,", step+1);	}
				if(m_bCurrent)	{ 	fprintf(fp, "%d:Disch_Crt,", step+1);	}
				if(m_bCapacity)	{ 	fprintf(fp, "%d:Disch_Cap,", step+1);	}
				if(m_bCapEff)	{ 	fprintf(fp, "%d:Cap_Eff(%%),", step+1);	}
				if(m_bTime)		{ 	fprintf(fp, "%d:Disch_Time,", step+1);	}
				if(m_bWatt)		{ 	fprintf(fp, "%d:Disch_Watt,", step+1);	}
				if(m_bWattHour)	{ 	fprintf(fp, "%d:Disch_Wh,", step+1);	}
				break;
			case EP_TYPE_OCV:
				if(m_bVoltage)	{ 	fprintf(fp, "%d:OCV,", step+1);	}
				break;
			case EP_TYPE_IMPEDANCE:
				if(m_bImpedance){ 	fprintf(fp, "%d:Imp,", step+1);	}
				break;
			default:
				break;
			}
		}
		fprintf(fp, "\n");


		for(int ch =0; ch < chSize; ch++)
		{
			fprintf(fp, "%d,%s,", ch+1, formFile.GetCellSerial(ch));
			int step;
			for (step = nStartStep; step < nFileStepArray[name] ; step++)
			{
				lpStepData = formFile.GetStepData(step);
				if(lpStepData == NULL)	break;

				lpChannel = (STR_SAVE_CH_DATA *)lpStepData->aChData[ch];
				if(lpChannel == NULL)	break;

				switch(lpStepData->stepCondition.stepHeader.type)
				{
				case EP_TYPE_CHARGE:
					if(m_bVoltage)	{ fprintf(fp, "%s,", ValueString(lpChannel->fVoltage, EP_VOLTAGE));	}
					if(m_bCurrent)	{ fprintf(fp, "%s,", ValueString(lpChannel->fCurrent, EP_CURRENT));	}
					if(m_bCapacity)	{ fprintf(fp, "%s,", ValueString(lpChannel->fCapacity, EP_CAPACITY));	}
					if(m_bTime)		{ fprintf(fp, "%s,", ValueString(lpChannel->fStepTime, EP_STEP_TIME));	}
					if(m_bWatt)		{ fprintf(fp, "%s,", ValueString(lpChannel->fWatt, EP_WATT));		}
					if(m_bWattHour)	{ fprintf(fp, "%s,", ValueString(lpChannel->fWattHour, EP_WATT_HOUR));}
					break;
				case EP_TYPE_DISCHARGE:
					if(m_bVoltage)	{ fprintf(fp, "%s,", ValueString(lpChannel->fVoltage, EP_VOLTAGE));	}
					if(m_bCurrent)	{ fprintf(fp, "%s,", ValueString(lpChannel->fCurrent, EP_CURRENT));	}
					if(m_bCapacity)	{ fprintf(fp, "%s,", ValueString(lpChannel->fCapacity, EP_CAPACITY)); }
					//방전의 경우 totalTime에 방전효율이 *100 형태로 저장 되어 있음 
					if(m_bCapEff)	{ fprintf(fp, "%s,", ValueString(lpChannel->fTotalTime, EP_CAPACITY)); }
					if(m_bTime)		{ fprintf(fp, "%s,", ValueString(lpChannel->fStepTime, EP_STEP_TIME));	}
					if(m_bWatt)		{ fprintf(fp, "%s,", ValueString(lpChannel->fWatt, EP_WATT)); }
					if(m_bWattHour)	{ fprintf(fp, "%s,", ValueString(lpChannel->fWattHour, EP_WATT_HOUR));}
					break;
				case EP_TYPE_OCV:
					if(m_bVoltage)	{ fprintf(fp, "%s,", ValueString(lpChannel->fVoltage, EP_VOLTAGE));	}
					break;
				case EP_TYPE_IMPEDANCE:
					if(m_bImpedance){ fprintf(fp, "%s,", ValueString(lpChannel->fImpedance, EP_IMPEDANCE));	}
					break;
				default:
					break;
				}
			}
			fprintf(fp, "\n");
		}

		fclose(fp);
		fp = NULL;
	}
	
	EndWaitCursor();
	return TRUE;	
}


BOOL CExcelTransDlg::SaveTrayFormFile(CString strFile)
{
	CFormResultFile formFile;
	if(formFile.ReadFile(strFile, m_bSumCap) < 0)
	{
		CString strform;
		strform.LoadString(IDS_TEXT_NOT_READ);
		strFile += strform;
		AfxMessageBox(strFile);
		return FALSE;
	}
	
	//기존 저장 대상 파일명의 확장자를 csv로 바꿈
	CString strSaveFileName;
	char szMode[8];
	if(m_bFileAppend)	//1개의 파일로 통합시 
	{
		strSaveFileName = m_strSaveFileName;
		sprintf(szMode, "at+");
	}
	else				//각각의 파일로 저장 
	{
		strSaveFileName= GetSaveFileName(strFile);	
		sprintf(szMode, "wt");
	}

	//Open Save File
	FILE	*fp = fopen(strSaveFileName, szMode);
	if (fp == FALSE)	return FALSE;

	//Header 기록 
	if(WriteFileHeader(fp, &formFile) == FALSE)
	{
		fclose(fp);
		return FALSE;
	}

	BeginWaitCursor();

	STR_SAVE_CH_DATA	*lpChannel;
	STR_STEP_RESULT *lpStepData;
	int nStepSize = formFile.GetStepSize();
	int chSize;
//	BYTE color;

	//Column Header 생성  
	CString strColHeader, strBuff;
	strColHeader = "/,";

#if CUSTOMER_TYPE == LG_CHEMICAL || CUSTOMER_TYPE == LGC_PB5
	for(int i=0; i<m_nColCount; i++)
	{
		strBuff.Format("%c,", 'A'+i);
		strColHeader += strBuff;
	}
#else
	for(int i=0; i<m_nColCount; i++)
	{
		strBuff.Format("%d,", 1+i);
		strColHeader += strBuff;
	}
#endif
	strColHeader += "\n";

	for (int step = 0; step < nStepSize ; step++)
	{
		lpStepData = formFile.GetStepData(step);
		if(lpStepData == NULL)	break;
		chSize = lpStepData->aChData.GetSize();
		
		int nTotRow =  chSize / m_nColCount;
		if(nTotRow%m_nColCount > 0)	nTotRow++;

		int nRow, nCol;
//		int nMapCh;
		switch(lpStepData->stepCondition.stepHeader.type)
		{
		case EP_TYPE_CHARGE:
		case EP_TYPE_DISCHARGE:
			//전압 기록 
			if(m_bVoltage)	
			{
				//Data 종류 기록
				fprintf(fp, "Step:%d, Type:%s, Voltage\n", step+1, StepTypeMsg(lpStepData->stepCondition.stepHeader.type));
				
				//Column Header 기록 
				fprintf(fp, "%s", strColHeader);
				
				nCol = 0;
				nRow = 0;
				for(int ch =0; ch < chSize; ch++)
				{
					//Get Channel Data
					lpChannel = (STR_SAVE_CH_DATA *)lpStepData->aChData[ch];
					if(lpChannel == NULL)	break;

					//Row Header 기록 
					if(nCol == 0)
					{
#if CUSTOMER_TYPE == LG_CHEMICAL || CUSTOMER_TYPE == LGC_PB5
						fprintf(fp, "%d,", ++nRow);
#else
						fprintf(fp, "%c,", 'A'+nRow++);
#endif
					}
					//전압 기록 
					fprintf(fp, "%s,", ValueString(lpChannel->fVoltage, EP_VOLTAGE));	
					
					//줄바꿈 여부 확인 
					if(++nCol >= m_nColCount)
					{
						fprintf(fp, "\n");
						nCol = 0;
					}
				}
			}

			fprintf(fp, "\n");

			if(m_bCurrent)	
			{ 
				//Data 종류 기록
				fprintf(fp, "Step:%d, Type:%s, Current\n", step+1, StepTypeMsg(lpStepData->stepCondition.stepHeader.type));
				
				//Column Header 기록 
				fprintf(fp, "%s", strColHeader);
				
				nCol = 0;
				nRow = 0;
				for(int ch =0; ch < chSize; ch++)
				{
					//Get Channel Data
					lpChannel = (STR_SAVE_CH_DATA *)lpStepData->aChData[ch];
					if(lpChannel == NULL)	break;

					//Row Header 기록 
					if(nCol == 0)
					{
#if CUSTOMER_TYPE == LG_CHEMICAL || CUSTOMER_TYPE == LGC_PB5
						fprintf(fp, "%d,", ++nRow);
#else
						fprintf(fp, "%c,", 'A'+nRow++);
#endif
					}
					//전류 기록 
					fprintf(fp, "%s,", ValueString(lpChannel->fCurrent, EP_CURRENT));	
					
					//줄바꿈 여부 확인 
					if(++nCol >= m_nColCount)
					{
						fprintf(fp, "\n");
						nCol = 0;
					}
				}					
			}

			fprintf(fp, "\n");

			if(m_bCapacity)	
			{ 
				//Data 종류 기록
				fprintf(fp, "Step:%d, Type:%s, Capacity\n", step+1, StepTypeMsg(lpStepData->stepCondition.stepHeader.type));
				
				//Column Header 기록 
				fprintf(fp, "%s", strColHeader);
				
				nCol = 0;
				nRow = 0;
				for(int ch =0; ch < chSize; ch++)
				{
					//Get Channel Data
					lpChannel = (STR_SAVE_CH_DATA *)lpStepData->aChData[ch];
					if(lpChannel == NULL)	break;

					//Row Header 기록 
					if(nCol == 0)
					{
#if CUSTOMER_TYPE == LG_CHEMICAL || CUSTOMER_TYPE == LGC_PB5
						fprintf(fp, "%d,", ++nRow);
#else
						fprintf(fp, "%c,", 'A'+nRow++);
#endif
					}
					//용량 기록 
					fprintf(fp, "%s,", ValueString(lpChannel->fCapacity, EP_CAPACITY));		
					
					//줄바꿈 여부 확인 
					if(++nCol >= m_nColCount)
					{
						fprintf(fp, "\n");
						nCol = 0;
					}
				}					
			}

			fprintf(fp, "\n");

			if(m_bTime)		
			{ 
				//Data 종류 기록
				fprintf(fp, "Step:%d, Type:%s, Time\n", step+1, StepTypeMsg(lpStepData->stepCondition.stepHeader.type));
				
				//Column Header 기록 
				fprintf(fp, "%s", strColHeader);
				
				nCol = 0;
				nRow = 0;
				for(int ch =0; ch < chSize; ch++)
				{
					//Get Channel Data
					lpChannel = (STR_SAVE_CH_DATA *)lpStepData->aChData[ch];
					if(lpChannel == NULL)	break;

					//Row Header 기록 
					if(nCol == 0)
					{
#if CUSTOMER_TYPE == LG_CHEMICAL || CUSTOMER_TYPE == LGC_PB5
						fprintf(fp, "%d,", ++nRow);
#else
						fprintf(fp, "%c,", 'A'+nRow++);
#endif
					}
					//용량 기록 
					fprintf(fp, "%s,", ValueString(lpChannel->fStepTime, EP_STEP_TIME));		
					
					//줄바꿈 여부 확인 
					if(++nCol >= m_nColCount)
					{
						fprintf(fp, "\n");
						nCol = 0;
					}
				}								
			}

			fprintf(fp, "\n");

			if(m_bWatt)		
			{ 
				//Data 종류 기록
				fprintf(fp, "Step:%d, Type:%s, Watt\n", step+1, StepTypeMsg(lpStepData->stepCondition.stepHeader.type));
				
				//Column Header 기록 
				fprintf(fp, "%s", strColHeader);
				
				nCol = 0;
				nRow = 0;
				for(int ch =0; ch < chSize; ch++)
				{
					//Get Channel Data
					lpChannel = (STR_SAVE_CH_DATA *)lpStepData->aChData[ch];
					if(lpChannel == NULL)	break;

					//Row Header 기록 
					if(nCol == 0)
					{
#if CUSTOMER_TYPE == LG_CHEMICAL || CUSTOMER_TYPE == LGC_PB5
						fprintf(fp, "%d,", ++nRow);
#else
						fprintf(fp, "%c,", 'A'+nRow++);
#endif
					}
					//Watt 기록 
					fprintf(fp, "%s,", ValueString(lpChannel->fWatt, EP_WATT));	
					
					//줄바꿈 여부 확인 
					if(++nCol >= m_nColCount)
					{
						fprintf(fp, "\n");
						nCol = 0;
					}
				}					
			}

			fprintf(fp, "\n");

			if(m_bWattHour)	
			{ 
				//Data 종류 기록
				fprintf(fp, "Step:%d, Type:%s, WattHour\n", step+1, StepTypeMsg(lpStepData->stepCondition.stepHeader.type));
				
				//Column Header 기록 
				fprintf(fp, "%s", strColHeader);
				
				nCol = 0;
				nRow = 0;
				for(int ch =0; ch < chSize; ch++)
				{
					//Get Channel Data
					lpChannel = (STR_SAVE_CH_DATA *)lpStepData->aChData[ch];
					if(lpChannel == NULL)	break;

					//Row Header 기록 
					if(nCol == 0)
					{
#if CUSTOMER_TYPE == LG_CHEMICAL || CUSTOMER_TYPE == LGC_PB5
						fprintf(fp, "%d,", ++nRow);
#else
						fprintf(fp, "%c,", 'A'+nRow++);
#endif
					}
					//WattHour 기록 
					fprintf(fp, "%s,", ValueString(lpChannel->fWattHour, EP_WATT_HOUR));
					
					//줄바꿈 여부 확인 
					if(++nCol >= m_nColCount)
					{
						fprintf(fp, "\n");
						nCol = 0;
					}
				}			
			}
			break;

		case EP_TYPE_OCV:
			//전압 기록 
			if(m_bVoltage)	
			{
				//Data 종류 기록
				fprintf(fp, "Step:%d, Type:%s, OCV\n", step+1, StepTypeMsg(lpStepData->stepCondition.stepHeader.type));
				
				//Column Header 기록 
				fprintf(fp, "%s", strColHeader);
				
				nCol = 0;
				nRow = 0;
				for(int ch =0; ch < chSize; ch++)
				{
					//Get Channel Data
					lpChannel = (STR_SAVE_CH_DATA *)lpStepData->aChData[ch];
					if(lpChannel == NULL)	break;

					//Row Header 기록 
					if(nCol == 0)
					{
#if CUSTOMER_TYPE == LG_CHEMICAL || CUSTOMER_TYPE == LGC_PB5
						fprintf(fp, "%d,", ++nRow);
#else
						fprintf(fp, "%c,", 'A'+nRow++);
#endif
					}
					//전압 기록 
					fprintf(fp, "%s,", ValueString(lpChannel->fVoltage, EP_VOLTAGE));	
					
					//줄바꿈 여부 확인 
					if(++nCol >= m_nColCount)
					{
						fprintf(fp, "\n");
						nCol = 0;
					}
				}
			}
			break;
		case EP_TYPE_IMPEDANCE:
			if(m_bImpedance)
			{
				//Data 종류 기록
				fprintf(fp, "Step:%d, Type:%s, Impedance\n", step+1, StepTypeMsg(lpStepData->stepCondition.stepHeader.type));
				
				//Column Header 기록 
				fprintf(fp, "%s", strColHeader);
				
				nCol = 0;
				nRow = 0;
				for(int ch =0; ch < chSize; ch++)
				{
					//Get Channel Data
					lpChannel = (STR_SAVE_CH_DATA *)lpStepData->aChData[ch];
					if(lpChannel == NULL)	break;

					//Row Header 기록 
					if(nCol == 0)
					{
#if CUSTOMER_TYPE == LG_CHEMICAL || CUSTOMER_TYPE == LGC_PB5
						fprintf(fp, "%d,", ++nRow);
#else
						fprintf(fp, "%c,", 'A'+nRow++);
#endif
					}
					//전압 기록 
					fprintf(fp, "%s,", ValueString(lpChannel->fImpedance, EP_IMPEDANCE));	
					
					//줄바꿈 여부 확인 
					if(++nCol >= m_nColCount)
					{
						fprintf(fp, "\n");
						nCol = 0;
					}
				}
			}
			break;
		default:
			break;
		}
		fprintf(fp, "\n");
	}
	
	fclose(fp);
	fp = NULL;
	EndWaitCursor();
	return TRUE;	
}

BOOL CExcelTransDlg::SaveCellEfficiencyFile(CString strFile)
{
	CFormResultFile formFile;
	if(formFile.ReadFile(strFile, m_bSumCap) < 0)
	{
		CString strform;
		strform.LoadString(IDS_TEXT_NOT_READ);
		strFile += strform;
		AfxMessageBox(strFile);
		return FALSE;
	}
	
	//기존 저장 대상 파일명의 확장자를 csv로 바꿈
	CString strSaveFileName;
	char szMode[8];
	if(m_bFileAppend)	//1개의 파일로 통합시 
	{
		strSaveFileName = m_strSaveFileName;
		sprintf(szMode, "at+");
	}
	else				//각각의 파일로 저장 
	{
		strSaveFileName= GetSaveFileName(strFile);	
		sprintf(szMode, "wt");
	}

	//Open Save File
	FILE	*fp = fopen(strSaveFileName, szMode);
	if (fp == FALSE)	return FALSE;

	//Header 기록 
	if(WriteFileHeader(fp, &formFile) == FALSE)
	{
		fclose(fp);
		return FALSE;
	}

	BeginWaitCursor();

	struct CycleData {
		float fChargeCap;
		float fChargeTime;
		float fDischargeCap;
		float fDischargeTime;
	};

	struct CycleData cycleData = {0.0f, 0.0f, 0.0f, 0.0f};

	fprintf(fp, "Channel No, Cycle No,Cha Cap,Cha Time,Dis Cap,Dis Time, Efficiency(%%)\n");

	STR_SAVE_CH_DATA	*lpChannel;
	STR_STEP_RESULT	*lpStepData;
	lpStepData = formFile.GetFirstStepData();
	if(lpStepData == NULL )								//저장된 Step Data가 없을 경우
	{
		fclose(fp);
		return FALSE;
	}

	int chSize = lpStepData->aChData.GetSize();	//저장된 Channel 수
	int nStepSize = formFile.GetStepSize();				//저장된 결과 Step 수

	int nCycleCount = 0;
	for(int ch = 0; ch < chSize; ch++)
	{
		nCycleCount = 0;
		
		for (int step = 0; step < nStepSize ; step++)
		{
			lpStepData = formFile.GetStepData(step);
			ASSERT(lpStepData);
			if(ch >= lpStepData->aChData.GetSize())		break;	//각 Step의 Channel 수가 다를 경우
			lpChannel = (STR_SAVE_CH_DATA *)lpStepData->aChData[ch];
			ASSERT(lpChannel);

			//1Cycle 완료
			if(lpStepData->stepCondition.stepHeader.type == EP_TYPE_DISCHARGE)
			{
				cycleData.fDischargeCap = lpChannel->fCapacity;
				cycleData.fDischargeTime = lpChannel->fStepTime;
				
				fprintf(fp, "%d,%d,%s,%s,%s,%s,", 
						ch+1,
						++nCycleCount, 
						ValueString(cycleData.fChargeCap, EP_CAPACITY),
						ValueString(cycleData.fChargeTime, EP_STEP_TIME),
						ValueString(cycleData.fDischargeCap, EP_CAPACITY),
						ValueString(cycleData.fDischargeTime, EP_STEP_TIME)
						);	// 상태
				
				if(cycleData.fChargeCap > 0)
				{
					fprintf(fp, "%.1f", cycleData.fDischargeCap/cycleData.fChargeCap*100.0f);	
				}
				fprintf(fp, "\n");	
				ZeroMemory(&cycleData, sizeof(cycleData));
				
			}
			else if(lpStepData->stepCondition.stepHeader.type == EP_TYPE_CHARGE)
			{
				cycleData.fChargeCap += lpChannel->fCapacity;
				cycleData.fChargeTime = lpChannel->fStepTime;
			}

		}
		fprintf(fp, "\n");	
	}
	fclose(fp);
	fp = NULL;
	EndWaitCursor();
	return TRUE;
}

BOOL CExcelTransDlg::WriteFileHeader(FILE *fp, CFormResultFile *pResultFile)
{
	ASSERT(fp);
	ASSERT(pResultFile);

	if(pResultFile->IsLoaded() == FALSE)	return FALSE;

	RESULT_FILE_HEADER *pHeader = pResultFile->GetResultHeader();
	fprintf(fp, "File name:, %s\n", (char *)(LPCTSTR)pResultFile->GetFileName());	// FileName
	fprintf(fp, "Model:,%s\n",  pResultFile->GetModelName());			// 모델명 	
	fprintf(fp, TEXT_LANG[5],  pResultFile->GetTestName());		// 시험조건명
	fprintf(fp, TEXT_LANG[6],  pHeader->szTrayNo);		// Tray No
	fprintf(fp, TEXT_LANG[7],  pHeader->szLotNo);			// Lot No
	fprintf(fp, TEXT_LANG[8],  pHeader->nModuleID, pHeader->wGroupIndex+1);					// Module name 
	fprintf(fp, TEXT_LANG[9],  pHeader->szDateTime);	// 시작시간
	fprintf(fp, TEXT_LANG[10],  pHeader->szOperatorID);					// 총시간

	return TRUE;
}

void CExcelTransDlg::OnSaveFolderButton() 
{
	// TODO: Add your control notification handler code here
	//저장 Folder 설정
	CFolderDialog *pDlg = new CFolderDialog(m_strSaveFolderName, BIF_RETURNONLYFSDIRS, this);
	pDlg->SetTitle(TEXT_LANG[11]);
	pDlg->m_Title = TEXT_LANG[12];

	if(pDlg->DoModal() == IDOK)
	{
		m_strSaveFolderName = pDlg->GetPathName();
		UpdateData(FALSE);
		AfxGetApp()->WriteProfileString(REG_SECTION, "LastPath", m_strSaveFolderName);
	}
	delete pDlg;
	pDlg = NULL;

	if(m_aSelFileName.GetSize() > 0 && !m_strSaveFolderName.IsEmpty())
		GetDlgItem(IDC_CONVERT_RUN)->EnableWindow(TRUE);

}

void CExcelTransDlg::SetFileName(const CStringArray &aSelFileName)
{
	m_aSelFileName.Copy( aSelFileName);
}

CString CExcelTransDlg::GetSaveFileName(CString strFileName)
{
	//Make Save File Name
	int index = strFileName.ReverseFind('.');
	if(index <= 0)	return "";
		
	int index2 = strFileName.ReverseFind('\\');	
	if(index2 < 0)	return "";

	CString name;
	name = strFileName.Mid(index2+1, index - index2);

	//기존 저장 대상 파일명의 확장자를 csv로 바꿈
	return m_strSaveFolderName + "\\" + name + "csv";	
}

CString CExcelTransDlg::ValueString(double dData, int item, BOOL bUnit)
{
	CString strMsg, strTemp;
	double dTemp;
	char szTemp[8];

	dTemp = dData;
	switch(item)
	{
	case EP_STATE:		strMsg = GetStateMsg((WORD)dTemp);	
		break;
		
	case EP_VOLTAGE:		//voltage
		//mV단위로 변경 
		if(m_strVUnit == "V")
		{
			dTemp = dTemp / 1000.0f;	//LONG2FLOAT(Value);
		}
		
		if(m_nVDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nVDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}		
		if(bUnit) strMsg+= (" "+m_strVUnit);
		
		break;


	case EP_CURRENT:		//current
		if(dTemp < 0)	dTemp = -dTemp;			//양수 표기
		
		//current
		if(m_strIUnit == "A")
		{
			dTemp = dTemp/1000.0f;
		}  
		else if(m_strIUnit == "uA")
		{
			dTemp = dTemp * 1000.0f;		// LONG2FLOAT(Value)/1000.0f;
		}

		if(m_nIDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nIDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}
		if(bUnit) strMsg+= (" "+m_strIUnit);

		break;
			
	case EP_WATT	:
		if(dTemp < 0)	dTemp = -dTemp;			//양수 표기

		strTemp = " mW";
		if(m_strCUnit == "Ah" || m_strCUnit == "F")
		{
			dTemp = dTemp/1000.0f;
			strTemp = " W";
		}

		if(m_nCDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nCDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}
		if(bUnit) strMsg+= strTemp;
		break;	
	case EP_WATT_HOUR:			
		if(dTemp < 0)	dTemp = -dTemp;			//양수 표기

		strTemp = " mWh";
		if(m_strCUnit == "Ah" || m_strCUnit == "F")
		{
			dTemp = dTemp/1000.0f;
			strTemp = " Wh";
		}

		if(m_nCDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nCDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}
		if(bUnit) strMsg+= strTemp;
		
		break;
		
	case EP_CAPACITY:		//capacity

		if(dTemp < 0)	dTemp = -dTemp;			//양수 표기

		if(m_strCUnit == "Ah" || m_strCUnit == "F")
		{
			dTemp = dTemp/1000.0f;
		}

		if(m_nCDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nCDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}
		if(bUnit) strMsg+= (" "+m_strCUnit);
		break;

	case EP_IMPEDANCE:	
		strMsg.Format("%.2f", dTemp);
		if(bUnit) strMsg+= " mOhm";
		break;

	case EP_CH_CODE:	//failureCode
/*
		strMsg = ChCodeMsg((BYTE)dTemp);
		break;
*/
		strMsg = m_aChMsg[(BYTE)dTemp];
		if(strMsg.IsEmpty())
			strMsg.Format("%d", (BYTE)dTemp);
		break;

	case EP_TOT_TIME:
	case EP_STEP_TIME:

		//sbc에서 100msec단위 전송되어 온다.
		if(m_nTimeUnit == 1)	//sec 표시
		{
			strMsg.Format("%.1f", dTemp);
			if(bUnit) strMsg+= " sec";
		}
		else if(m_nTimeUnit == 2)	//Minute 표시
		{
			strMsg.Format("%.2f", dTemp/60.0f);
		}
		else
		{
			CTimeSpan timeSpan((ULONG)dTemp);
			if(timeSpan.GetDays() > 0)
			{
				strMsg =  timeSpan.Format("%Dd %H:%M:%S");
			}
			else
			{
				strMsg = timeSpan.Format("%H:%M:%S");
			}
		}

		break;

	case EP_GRADE_CODE:	
		strMsg = ::GetSelectCodeName((BYTE)dTemp);
		break;
	
	case EP_TEMPER:
		if(0xFFFFFFFF == (float)dTemp)
		{
			strMsg = TEXT_LANG[31];
		}
		else
		{
			strMsg.Format("%.1f", dTemp);
			if(bUnit) strMsg+= " 'C";
		}
		
		break;

	case EP_STEP_NO:
	default:
		strMsg.Format("%d", (int)dTemp);
		break;
	}
	
	return strMsg;
}

CString CExcelTransDlg::GetStateMsg(WORD State)
{
	CString strMsg;
	switch (State)
	{
		case EP_STATE_IDLE		:		strMsg = TEXT_LANG[13];		break;
		case EP_STATE_SELF_TEST	:		strMsg = TEXT_LANG[14];		break;
		case EP_STATE_STANDBY	:		strMsg = TEXT_LANG[15];	break;
		case EP_STATE_RUN		:		strMsg = TEXT_LANG[16];			break;
		case EP_STATE_PAUSE		:		strMsg = TEXT_LANG[17];		break;
		case EP_STATE_FAIL		:		strMsg = TEXT_LANG[18];		break;
		case EP_STATE_MAINTENANCE:		strMsg = TEXT_LANG[19];	break;
		case EP_STATE_OCV		:		strMsg = TEXT_LANG[20];			break;
		case EP_STATE_CHARGE	:		strMsg = TEXT_LANG[21];		break;
		case EP_STATE_DISCHARGE	:		strMsg = TEXT_LANG[22];	break;
		case EP_STATE_REST		:		strMsg = TEXT_LANG[23];		break;
		case EP_STATE_IMPEDANCE	:		strMsg = TEXT_LANG[24];	break;
		case EP_STATE_CHECK		:		strMsg = TEXT_LANG[25];		break;
		case EP_STATE_STOP		:		strMsg = TEXT_LANG[26]	;		break;
		case EP_STATE_END		:		strMsg = TEXT_LANG[27];	break;
		case EP_STATE_FAULT		:		strMsg = TEXT_LANG[28];		break;
		case EP_STATE_READY		:		strMsg = TEXT_LANG[29];		break;

		case EP_STATE_LINE_OFF	:		strMsg = TEXT_LANG[30];	break;	
		case EP_STATE_LINE_ON	:		strMsg = TEXT_LANG[31];		break;
		case EP_STATE_EMERGENCY	:		strMsg = TEXT_LANG[32];	break;
		case EP_STATE_NONCELL   :		strMsg = TEXT_LANG[33];		break;
		
		case EP_STATE_COMMON	:		
		case EP_STATE_ERROR		:		
		default:						strMsg = TEXT_LANG[34];		break;
	}
	return strMsg;
}

BOOL CExcelTransDlg::LoadCodeTable()
{
	//Registry에서 PowerFormation DataBase 경로를 검색 

	long rtn;
	HKEY hKey = 0;
	rtn = ::RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\CTSMon\\Path", 0, KEY_READ, &hKey);
	
	//찾을 수 없음 
	if(ERROR_SUCCESS != rtn)	return FALSE;

	BYTE buf[512];
	DWORD size = 512;
	DWORD type;
	
	//PowerFormation DataBase 경로를 읽어온다. 
	rtn = ::RegQueryValueEx(hKey, "DataBase", NULL, &type, buf, &size);
	::RegCloseKey(hKey);
	
	//
	if(ERROR_SUCCESS != rtn || (type != REG_SZ && type != REG_EXPAND_SZ))	return FALSE;

	CString strDataBaseName(buf);
	//DataBase 경로를 찾을 수 없을 경우 
	if(strDataBaseName.IsEmpty())	return  FALSE;

	//DataBase 검색 
	CDaoDatabase  db;
	
	strDataBaseName = strDataBaseName+ "\\"+FORM_SET_DATABASE_NAME;
	
	CString strSQL;

	if( g_nLanguage == LANGUAGE_ENG )
	{
		strSQL.Format("SELECT Code, Messge, Description FROM ChannelCode_ENG ORDER BY Code ASC");
	}
	else if( g_nLanguage == LANGUAGE_CHI )
	{
		strSQL.Format("SELECT Code, Messge, Description FROM ChannelCode_CHI ORDER BY Code ASC");
	}
	else
	{
		strSQL.Format("SELECT Code, Messge, Description FROM ChannelCode ORDER BY Code ASC");
	}

	db.Open(strDataBaseName);

	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
	}
	catch (CDaoException *e)
	{
		TCHAR szMessage[128];
		e->GetErrorMessage(szMessage, 128);
		e->Delete();
		AfxMessageBox(szMessage);
		return FALSE;
	}
	
	CString message;
	COleVariant data;
	while(!rs.IsEOF())
	{
		data = rs.GetFieldValue(0);
		long  code = data.lVal;
		if(code > 0 && code < 256)
		{
			data = rs.GetFieldValue(1);
			sprintf(m_aChMsg[code], "%s", data.pbVal);
		}
		rs.MoveNext();
	}

	rs.Close();
	db.Close();

	return TRUE;
}

void CExcelTransDlg::OnSaveUnitButton() 
{
	// TODO: Add your control notification handler code here
	
}


int CExcelTransDlg::GetColumnCount(int nStepType)
{
		int nColumnCount = 0;
		//StepType에 따라 필요한 Column수 Count
		switch(nStepType)
		{
		case EP_TYPE_CHARGE:
			if(m_bVoltage)	 nColumnCount++;
			if(m_bCurrent)	 nColumnCount++;
			if(m_bCapacity)	 nColumnCount++;
			if(m_bTime)		 nColumnCount++;
			if(m_bWatt)		 nColumnCount++;
			if(m_bWattHour)	 nColumnCount++;
			break;
		case EP_TYPE_DISCHARGE:
			if(m_bVoltage)	 nColumnCount++;
			if(m_bCurrent)	 nColumnCount++;
			if(m_bCapacity)	 nColumnCount++;
			if(m_bCapEff)	 nColumnCount++;
			if(m_bTime)		 nColumnCount++;
			if(m_bWatt)		 nColumnCount++;
			if(m_bWattHour)	 nColumnCount++;
			break;
		case EP_TYPE_OCV:
			if(m_bVoltage)	 nColumnCount++;
			break;
		case EP_TYPE_IMPEDANCE:
			if(m_bImpedance) nColumnCount++;
			break;
		default:
			break;
		}
		return nColumnCount;
}

LRESULT CExcelTransDlg::OnAutoFileTrans(WPARAM wParam, LPARAM lParam)
{
	CString strTemp;
	{
		TCHAR szCurDir[MAX_PATH];
		::GetModuleFileName(AfxGetApp()->m_hInstance, szCurDir, MAX_PATH);
		strTemp = CString(szCurDir).Mid(0, CString(szCurDir).ReverseFind('\\')) + "\\Temp\\AutoFileName.tmp";
	}

	CStdioFile file;
	CString strFileName;
	if(file.Open(strTemp, CFile::modeRead))
	{
		file.ReadString(strFileName);
		file.Close();
	}
	
	if(strFileName.IsEmpty() == FALSE)
	{
		m_aSelFileName.RemoveAll();
		m_aSelFileName.Add(strFileName);
		UpdateFileList();

		ConvertData();
	}
	return TRUE;
}

void CExcelTransDlg::UpdateFileList()
{
	CString selFileName;
	m_strSelFileName.Empty();

	for(int i=0; i<m_aSelFileName.GetSize(); i++)
	{
		selFileName = m_aSelFileName.GetAt(i);
		TRACE("FILE %s Selected\n", selFileName);
		int nIndex = selFileName.ReverseFind('\\');
		//파일명만 Display
		if(nIndex > 0)
		{
			m_strSelFileName = m_strSelFileName+ "\"" +selFileName.Mid(nIndex+1) + "\" ";	//파일명만 표기
		}
	}

	//파일을 선택하였고 저장 위치를 설정 하였으면 변환 버튼 Enable 시킴 
	if(m_aSelFileName.GetSize() > 0 && !m_strSaveFolderName.IsEmpty())
	{
		GetDlgItem(IDC_CONVERT_RUN)->EnableWindow(TRUE);
	}
	UpdateData(FALSE);
	
	selFileName.Format("%d File Selected", m_aSelFileName.GetSize());
	GetDlgItem(IDC_SEL_FILE_COUNT_STATIC)->SetWindowText(selFileName);
}

BOOL CExcelTransDlg::ConvertData()
{
	CString strFile;

	int nRtn;
	for(int i=0; i<m_aSelFileName.GetSize(); i++)
	{
		strFile = m_aSelFileName.GetAt(i);
		int index = strFile.ReverseFind('\\');	
		if(index > 0)
		{
			GetDlgItem(IDC_MESSAGE_STATIC)->SetWindowText(strFile.Mid(index+1) + "Converting..");
		}

		switch(m_nConvertType)
		{
		case 0:
			nRtn = SaveChStepFile(strFile);
			break;
		case 1:
			nRtn = SaveStepChListFile(strFile);
			break;
		case 2:
			nRtn = SaveChCellListFile(strFile);
			break;
		case 3:
			nRtn = SaveTrayFormFile(strFile);
			break;
		case 4:
			nRtn = SaveCellEfficiencyFile(strFile);
			break;
		}
		
		if(nRtn == FALSE)
		{
			return FALSE;
		}
	}

	strFile.Format("%d convert complited...", m_aSelFileName.GetSize());
	GetDlgItem(IDC_MESSAGE_STATIC)->SetWindowText(strFile);

	return TRUE;
}

void CExcelTransDlg::OnOK() 
{
	// TODO: Add extra validation here
	
	AfxGetApp()->WriteProfileInt("Settings", "ConvertType", m_nConvertType);

	CDialog::OnOK();
}

BOOL CExcelTransDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CExcelTransDlg"), _T("TEXT_CExcelTransDlg_CNT"), _T("TEXT_CExcelTransDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt];

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CExcelTransDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CExcelTransDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Languge error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}

void CExcelTransDlg::OnDestroy()
{
	CDialog::OnDestroy();

	delete[] TEXT_LANG;
	TEXT_LANG = NULL;
}