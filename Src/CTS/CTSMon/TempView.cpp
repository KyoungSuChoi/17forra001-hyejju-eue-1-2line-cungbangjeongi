// TempView.cpp : implementation file
//

#include "stdafx.h"
#include "ctsmon.h"
#include "TempView.h"
#include "MainFrm.h"
// #include "TempColorConfig.h"
#include "TempColorScaleConfig.h"
#include "SensorMapDlg.h"
#include "WarnningDlg.h"
#include "DetailTempCondition.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTempView

IMPLEMENT_DYNCREATE(CTempView, CFormView)

typedef struct
{
	int r;
	int g;
	int b;
}COLRGB;

COLRGB TempColScl[7] = {
	{0,0,255},
	{0,128,255},
	{0,255,255}, 
	{0,255,0},
	{255,255,0},
	{255,128,0},
	{255,0,0}
};

CTempView::CTempView()
	: CFormView(CTempView::IDD)
{
	//{{AFX_DATA_INIT(CTempView)
	//}}AFX_DATA_INIT
	m_nDisplayTimer = 0;
	m_bRackIndexUp = FALSE;	
	m_bWarnningWindow = FALSE;
	m_pImagelist = NULL;

	m_nTempGridRowCount = 7;
	m_nTempGridColCount = 10;
	m_bRackIndexUp = FALSE;
	LanguageinitMonConfig();
}

CTempView::~CTempView()
{
	if( m_pImagelist )
		delete m_pImagelist;
	m_pImagelist = NULL;

	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}

}

bool CTempView::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CTempView"), _T("TEXT_CTempView_CNT"), _T("TEXT_CTempView_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CTempView_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CTempView"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}


void CTempView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTempView)	
	DDX_Control(pDX, IDC_COL_UNITSCALE, m_UnitScale);
	DDX_Control(pDX, IDC_COL_JIGSCALE, m_JigScale);
	DDX_Control(pDX, IDC_UNIT_LEVEL8, m_UnitLevel8);
	DDX_Control(pDX, IDC_UNIT_LEVEL7, m_UnitLevel7);
	DDX_Control(pDX, IDC_UNIT_LEVEL6, m_UnitLevel6);
	DDX_Control(pDX, IDC_UNIT_LEVEL5, m_UnitLevel5);
	DDX_Control(pDX, IDC_UNIT_LEVEL4, m_UnitLevel4);
	DDX_Control(pDX, IDC_UNIT_LEVEL3, m_UnitLevel3);
	DDX_Control(pDX, IDC_UNIT_LEVEL2, m_UnitLevel2);
	DDX_Control(pDX, IDC_UNIT_LEVEL1, m_UnitLevel1);
	DDX_Control(pDX, IDC_JIG_LEVEL8, m_JigLevel8);
	DDX_Control(pDX, IDC_JIG_LEVEL7, m_JigLevel7);
	DDX_Control(pDX, IDC_JIG_LEVEL6, m_JigLevel6);
	DDX_Control(pDX, IDC_JIG_LEVEL5, m_JigLevel5);
	DDX_Control(pDX, IDC_JIG_LEVEL3, m_JigLevel3);
	DDX_Control(pDX, IDC_JIG_LEVEL2, m_JigLevel2);
	DDX_Control(pDX, IDC_JIG_LEVEL4, m_JigLevel4);
	DDX_Control(pDX, IDC_JIG_LEVEL1, m_JigLevel1);
	DDX_Control(pDX, IDC_CHNAME_COMBO, m_ctrlChNameCombo);
	DDX_Control(pDX, IDC_TEMP_COMBO, m_ctrlDisplayCombo);
	DDX_Control(pDX, IDC_MIN_TEMPINDEX, m_MinTempIndex);
	DDX_Control(pDX, IDC_MAX_TEMPINDEX, m_MaxTempIndex);
	DDX_Control(pDX, IDC_CHANNEL_INDEX, m_CmdTarget);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CTempView, CFormView)
	//{{AFX_MSG_MAP(CTempView)
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_CBN_SELCHANGE(IDC_TEMP_COMBO, OnSelchangeTempCombo)
	ON_CBN_SELCHANGE(IDC_CHNAME_COMBO, OnSelchangeChnameCombo)	
	ON_BN_CLICKED(IDC_BTN_TEMPSETTING, OnBtnTempsetting)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_GRID_DOUBLECLICK, OnGridDoubleClick)
	ON_MESSAGE(WM_GRID_MOVECELL, OnMovedCurrentCell)
	ON_MESSAGE(WM_GRID_RIGHT_CLICK, OnGridRightClick)
//	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTempView diagnostics

#ifdef _DEBUG
void CTempView::AssertValid() const
{
	CFormView::AssertValid();
}

void CTempView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CCTSMonDoc* CTempView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCTSMonDoc)));
	return (CCTSMonDoc*)m_pDocument;
}
#endif //_DEBUG
/////////////////////////////////////////////////////////////////////////////
// CTempView message handlers
void CTempView::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();	
	// TODO: Add your specialized code here and/or call the base class		
	m_pDoc = GetDocument();
	m_bRackIndexUp = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "RackIndexUp", TRUE);			
	// -----------------------------------------------------------------------
	// m_CtrlTempCombo Setting
	// -----------------------------------------------------------------------
	m_pImagelist = new CImageList;
	m_pImagelist->Create(IDB_TEMP, 19, 1, RGB(255,255,255));
	m_ctrlDisplayCombo.SetImageList(m_pImagelist);
	m_ctrlDisplayCombo.ResetContent();

	m_fMinTemp = 0.0f;
	m_fMaxTemp = 0.0f;
	int nCurIndex = 0;
	int nIndex = 0;
	
	m_ctrlDisplayCombo.AddString(TEXT_LANG[0]);//"전원부 평균"
	m_ctrlDisplayCombo.SetItemData(nIndex, EP_POWAVG);
	m_ctrlDisplayCombo.SetItemIcon(nIndex, 0);
	if(m_nSelectUnitDisplay == EP_POWAVG)	
		nCurIndex = nIndex;
	nIndex++;

	m_ctrlDisplayCombo.AddString(TEXT_LANG[1]);//"전원부 최대"
	m_ctrlDisplayCombo.SetItemData(nIndex, EP_POWMAX);
	m_ctrlDisplayCombo.SetItemIcon(nIndex, 0);
	if(m_nSelectUnitDisplay == EP_POWMAX)	
		nCurIndex = nIndex;
	nIndex++;

	m_ctrlDisplayCombo.AddString(TEXT_LANG[2]);//"전원부 최소"
	m_ctrlDisplayCombo.SetItemData(nIndex, EP_POWMIN);
	m_ctrlDisplayCombo.SetItemIcon(nIndex, 0);
	if(m_nSelectUnitDisplay == EP_POWMIN)	
		nCurIndex = nIndex;
	nIndex++;

	m_ctrlDisplayCombo.AddString(TEXT_LANG[3]);//"전원부Point 지정온도"
	m_ctrlDisplayCombo.SetItemData(nIndex, EP_POWPOINT);
	m_ctrlDisplayCombo.SetItemIcon(nIndex, 0);
	if(m_nSelectUnitDisplay == EP_POWPOINT)	
		nCurIndex = nIndex;
	nIndex++;

	m_ctrlDisplayCombo.AddString(TEXT_LANG[4]);//"JIG 평균"
	m_ctrlDisplayCombo.SetItemData(nIndex, EP_JIGAVG);
	m_ctrlDisplayCombo.SetItemIcon(nIndex, 0);
	if(m_nSelectUnitDisplay == EP_JIGAVG)	
		nCurIndex = nIndex;
	nIndex++;

	m_ctrlDisplayCombo.AddString(TEXT_LANG[5]);//"JIG 최대"
	m_ctrlDisplayCombo.SetItemData(nIndex, EP_JIGMAX);
	m_ctrlDisplayCombo.SetItemIcon(nIndex, 0);
	if(m_nSelectUnitDisplay == EP_JIGMAX)	
		nCurIndex = nIndex;
	nIndex++;

	m_ctrlDisplayCombo.AddString(TEXT_LANG[6]);//"JIG 최소"
	m_ctrlDisplayCombo.SetItemData(nIndex, EP_JIGMIN);
	m_ctrlDisplayCombo.SetItemIcon(nIndex, 0);
	if(m_nSelectUnitDisplay == EP_JIGMIN)	
		nCurIndex = nIndex;
	nIndex++;

	m_ctrlDisplayCombo.AddString(TEXT_LANG[7]);//"JIG Point 지정온도"
	m_ctrlDisplayCombo.SetItemData(nIndex, EP_JIGPOINT);
	m_ctrlDisplayCombo.SetItemIcon(nIndex, 0);
	if(m_nSelectUnitDisplay == EP_JIGPOINT)	
		nCurIndex = nIndex;
	nIndex++;	
	
	m_nSelectUnitDisplay = 0;		// Unit display index
	m_nPrevUnitDisplay = 0;			// PrevUnit display index
	m_nSelectChDisplay = -1;			// Ch display index
	m_ctrlDisplayCombo.SetCurSel(m_nSelectUnitDisplay);

	InitTempGridWnd();
	InitTempColor();
	DrawTempColor();		
	SelectUnitTemp();

	 m_CmdTarget.SetFontSize(20)
		 .SetTextColor(RGB_LABEL_BACKGROUND_STAGENAME)
		 .SetBkColor(RGB_LABEL_FONT_STAGENAME)
		 .SetFontBold(TRUE)
		 .SetFontName(TEXT_LANG[8]);//"HY헤드라인M"

	m_MaxTempIndex.SetBkColor(0);
	m_MaxTempIndex.SetTextColor(RGB(255, 255, 0));
	m_MaxTempIndex.SetFontSize(13);
	 
	m_MinTempIndex.SetBkColor(0);
	m_MinTempIndex.SetTextColor(RGB(255,255,0));
	m_MinTempIndex.SetFontSize(13);
	 
	m_MaxTempIndex.SetText("READY");
	m_MinTempIndex.SetText("READY");	 
		
	m_ctrlChNameCombo.EnableWindow(FALSE);		// Point 지정온도 or JIG 지정온도를 선택하면 TRUE
	m_bInitCombo = FALSE;	
}

VOID CTempView::InitTempColor()
{
	m_UnitLevel8.SetBkColor(0);
	m_UnitLevel8.SetTextColor(RGB(255, 255, 0));
	m_UnitLevel8.SetFontSize(FONTSIZE);
	m_UnitLevel7.SetBkColor(0);
	m_UnitLevel7.SetTextColor(RGB(255, 255, 0));
	m_UnitLevel7.SetFontSize(FONTSIZE);
	m_UnitLevel6.SetBkColor(0);
	m_UnitLevel6.SetTextColor(RGB(255, 255, 0));
	m_UnitLevel6.SetFontSize(FONTSIZE);
	m_UnitLevel5.SetBkColor(0);
	m_UnitLevel5.SetTextColor(RGB(255, 255, 0));
	m_UnitLevel5.SetFontSize(FONTSIZE);
	m_UnitLevel5.SetBkColor(0);
	m_UnitLevel5.SetTextColor(RGB(255, 255, 0));
	m_UnitLevel5.SetFontSize(FONTSIZE);
	m_UnitLevel4.SetBkColor(0);
	m_UnitLevel4.SetTextColor(RGB(255, 255, 0));
	m_UnitLevel4.SetFontSize(FONTSIZE);
	m_UnitLevel3.SetBkColor(0);
	m_UnitLevel3.SetTextColor(RGB(255, 255, 0));
	m_UnitLevel3.SetFontSize(FONTSIZE);
	m_UnitLevel2.SetBkColor(0);
	m_UnitLevel2.SetTextColor(RGB(255, 255, 0));
	m_UnitLevel2.SetFontSize(FONTSIZE);
	m_UnitLevel1.SetBkColor(0);
	m_UnitLevel1.SetTextColor(RGB(255, 255, 0));
	m_UnitLevel1.SetFontSize(FONTSIZE);
	m_JigLevel8.SetBkColor(0);
	m_JigLevel8.SetTextColor(RGB(255, 255, 0));
	m_JigLevel8.SetFontSize(FONTSIZE);
	m_JigLevel7.SetBkColor(0);
	m_JigLevel7.SetTextColor(RGB(255, 255, 0));
	m_JigLevel7.SetFontSize(FONTSIZE);
	m_JigLevel6.SetBkColor(0);
	m_JigLevel6.SetTextColor(RGB(255, 255, 0));
	m_JigLevel6.SetFontSize(FONTSIZE);
	m_JigLevel5.SetBkColor(0);
	m_JigLevel5.SetTextColor(RGB(255, 255, 0));
	m_JigLevel5.SetFontSize(FONTSIZE);
	m_JigLevel4.SetBkColor(0);
	m_JigLevel4.SetTextColor(RGB(255, 255, 0));
	m_JigLevel4.SetFontSize(FONTSIZE);
	m_JigLevel3.SetBkColor(0);
	m_JigLevel3.SetTextColor(RGB(255, 255, 0));
	m_JigLevel3.SetFontSize(FONTSIZE);
	m_JigLevel2.SetBkColor(0);
	m_JigLevel2.SetTextColor(RGB(255, 255, 0));
	m_JigLevel2.SetFontSize(FONTSIZE);
	m_JigLevel1.SetBkColor(0);
	m_JigLevel1.SetTextColor(RGB(255, 255, 0));
	m_JigLevel1.SetFontSize(FONTSIZE);
}

VOID CTempView::DrawTempColor()
{	
	CString strValue;
	
	strValue.Format("%.1f",m_pDoc->m_TempScaleConfig.m_fUnitlevel1);
	m_UnitLevel1.SetText(strValue);	
	strValue.Format("%.1f",m_pDoc->m_TempScaleConfig.m_fUnitlevel2);
	m_UnitLevel2.SetText(strValue);	
	strValue.Format("%.1f",m_pDoc->m_TempScaleConfig.m_fUnitlevel3);
	m_UnitLevel3.SetText(strValue);	
	strValue.Format("%.1f",m_pDoc->m_TempScaleConfig.m_fUnitlevel4);
	m_UnitLevel4.SetText(strValue);	
	strValue.Format("%.1f",m_pDoc->m_TempScaleConfig.m_fUnitlevel5);
	m_UnitLevel5.SetText(strValue);	
	strValue.Format("%.1f",m_pDoc->m_TempScaleConfig.m_fUnitlevel6);
	m_UnitLevel6.SetText(strValue);	
	strValue.Format("%.1f",m_pDoc->m_TempScaleConfig.m_fUnitlevel7);
	m_UnitLevel7.SetText(strValue);	
	strValue.Format("%.1f",m_pDoc->m_TempScaleConfig.m_fUnitlevel8);
	m_UnitLevel8.SetText(strValue);	
	
	strValue.Format("%.1f",m_pDoc->m_TempScaleConfig.m_fJiglevel1);
	m_JigLevel1.SetText(strValue);
	strValue.Format("%.1f",m_pDoc->m_TempScaleConfig.m_fJiglevel2);
	m_JigLevel2.SetText(strValue);
	strValue.Format("%.1f",m_pDoc->m_TempScaleConfig.m_fJiglevel3);
	m_JigLevel3.SetText(strValue);
	strValue.Format("%.1f",m_pDoc->m_TempScaleConfig.m_fJiglevel4);
	m_JigLevel4.SetText(strValue);
	strValue.Format("%.1f",m_pDoc->m_TempScaleConfig.m_fJiglevel5);
	m_JigLevel5.SetText(strValue);
	strValue.Format("%.1f",m_pDoc->m_TempScaleConfig.m_fJiglevel6);
	m_JigLevel6.SetText(strValue);
	strValue.Format("%.1f",m_pDoc->m_TempScaleConfig.m_fJiglevel7);
	m_JigLevel7.SetText(strValue);
	strValue.Format("%.1f",m_pDoc->m_TempScaleConfig.m_fJiglevel8);
	m_JigLevel8.SetText(strValue);
}

LONG CTempView::OnMovedCurrentCell(WPARAM wParam, LPARAM lParam)
{	
	ROWCOL nRow, nCol;	
//	int nIndex;
	lParam = NULL;
	nCol = LOWORD(wParam);
	nRow = HIWORD(wParam);
	if( nCol > 1 && nCol < m_wndTempGrid.GetColCount() && 
		nRow > 0 && nRow < m_wndTempGrid.GetRowCount() )
	{
		m_CmdTarget.SetText(GetModuleName(GetModuleIDRowCol(nRow, nCol)));	
	}
	return 1;
}

VOID CTempView::StartMonitoring()
{
	//현재 Window가 Active 되어 있지 않으면 사용 안함		
	if(((CMainFrame *)AfxGetMainWnd())->CompareCurrentSelectTab(TAB_INDEX_MONITORING) == FALSE)
		return;	

	int nTimer = 2000;	
	m_nDisplayTimer = SetTimer(102, nTimer, NULL);
}

VOID CTempView::StopMonitoring()
{
	if(m_nDisplayTimer == 0)	return;
	KillTimer(m_nDisplayTimer);
	m_nDisplayTimer = 0;
}

BOOL CTempView::InitTempGridWnd()
{
	m_wndTempGrid.SubclassDlgItem(IDC_TEMP_SYSTEM_GRID, this);
 	m_wndTempGrid.m_bSameRowSize = TRUE;
	m_wndTempGrid.m_bSameColSize = TRUE;
  	m_wndTempGrid.m_bCustomWidth = FALSE;
	
	m_wndTempGrid.Initialize();	
	BOOL bLock = m_wndTempGrid.LockUpdate();

	m_nTempGridRowCount = GetDocument()->m_nModulePerRack;
	
	m_nTempGridColCount = GetDocument()->GetInstalledModuleNum()/m_nTempGridRowCount;

	if( GetDocument()->GetInstalledModuleNum()%m_nTempGridRowCount != 0 )
	{
		m_nTempGridColCount = m_nTempGridColCount + 1;					// 나머지가 0이 아닐경우 Col+1
	}
	m_nTempGridColCount = m_nTempGridColCount + 2;						// Num, AvrValue Add

	m_nTempGridRowCount = m_nTempGridRowCount + 1;						// AvrValue Add
	m_wndTempGrid.SetRowCount( m_nTempGridRowCount );

	m_wndTempGrid.SetDrawingTechnique( gxDrawUsingMemDC );
	m_wndTempGrid.SetColCount( m_nTempGridColCount ); 
	m_wndTempGrid.GetParam()->EnableSelection( GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL );

	int i = 0;

	if(m_bRackIndexUp)
	{
		for( i=m_nTempGridRowCount-1; i>0; i-- )
		{
			CString strTemp;
			strTemp.Format("%d", i);
			m_wndTempGrid.SetStyleRange(CGXRange((m_nTempGridRowCount-i), 1), CGXStyle().SetValue(strTemp).SetFont(CGXFont().SetSize(11).SetBold(TRUE)));
		}
	}
	else
	{
		for( i=1; i<m_nTempGridRowCount; i++ )
		{
			CString strTemp;
			strTemp.Format("%d", i);
			m_wndTempGrid.SetStyleRange(CGXRange(i, 1), CGXStyle().SetValue(strTemp).SetFont(CGXFont().SetSize(11).SetBold(TRUE)));
		}
	}	
	
	m_wndTempGrid.SetStyleRange(CGXRange().SetCols(1),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_HEADER)
	);

	m_wndTempGrid.SetStyleRange(CGXRange().SetRows(m_nTempGridRowCount),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_HEADER).SetEnabled(FALSE).SetTextColor(RGB(255,255,255)).SetInterior(RGB(192,192,192))
	);

	m_wndTempGrid.SetStyleRange(CGXRange().SetCols(m_nTempGridColCount),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_HEADER).SetEnabled(FALSE).SetTextColor(RGB(255,255,255)).SetInterior(RGB(192,192,192))
	);
	
	m_wndTempGrid.SetStyleRange(CGXRange().SetCols(1),
  		CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(255,255,255)).SetInterior(RGB(192,192,192)));			

	m_wndTempGrid.SetStyleRange(CGXRange(0, 1), CGXStyle().SetValue("TEMP").SetFont(CGXFont().SetSize(11).SetBold(TRUE)));
	for( i=1; i<m_nTempGridColCount; i++ )
	{
		CString strTemp;
		strTemp.Format("%d", i);
		m_wndTempGrid.SetStyleRange(CGXRange(0, i+1), CGXStyle().SetValue(strTemp).SetFont(CGXFont().SetSize(11).SetBold(TRUE)));
	}
	m_wndTempGrid.SetStyleRange(CGXRange(0, m_nTempGridColCount), CGXStyle().SetValue("AVG").SetFont(CGXFont().SetSize(11).SetBold(TRUE)));
	m_wndTempGrid.SetStyleRange(CGXRange(m_nTempGridRowCount, 1), CGXStyle().SetValue("AVG").SetFont(CGXFont().SetSize(11).SetBold(TRUE)));		
	m_wndTempGrid.LockUpdate(bLock);
	m_wndTempGrid.Redraw();
	return TRUE;
}

void CTempView::OnSize(UINT nType, int cx, int cy) 
{	
	// TODO: Add your message handler code here
	if(::IsWindow(this->GetSafeHwnd()))
	{
		CFormView::ShowScrollBar(SB_HORZ,FALSE);
		CFormView::ShowScrollBar(SB_VERT,FALSE);
		
		CRect rect, ctrlRect;
		GetClientRect(&rect);
		if( rect.right > 1246 )
		{
			rect.right = 1246;
		}

		if(m_wndTempGrid.GetSafeHwnd())
		{
			m_wndTempGrid.GetWindowRect(&ctrlRect);
			ScreenToClient(&ctrlRect);
			m_wndTempGrid.MoveWindow(ctrlRect.left, ctrlRect.top, rect.right-ctrlRect.left-100, rect.bottom-ctrlRect.top, FALSE);
			m_wndTempGrid.GetClientRect(ctrlRect);
			float width = 0;
			width = (float)ctrlRect.Width()/90.0f;						
			m_wndTempGrid.Redraw();
		}
	}
	CFormView::OnSize(nType, cx, cy);	
}

VOID CTempView::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	if(nIDEvent == 102 )
	{
		SelectUnitTemp();
	}

	CFormView::OnTimer(nIDEvent);	
}

BOOL CTempView::GetChRowCol(int nIndex, ROWCOL &nRow, ROWCOL &nCol)
{	
	if(nIndex < 0)	return FALSE;

	int nTotalRowCount = m_wndTempGrid.GetRowCount()-1;
	
	if(m_bRackIndexUp)
	{
		nCol = (nIndex/nTotalRowCount)+2;						
		nRow = nTotalRowCount-(nIndex%nTotalRowCount);			
	}
	else
	{
		nCol = nIndex/nTotalRowCount+2;						
		nRow = nIndex%nTotalRowCount+1;			
	}

	if(nRow > m_wndTempGrid.GetRowCount() || nCol > m_wndTempGrid.GetColCount())	
		return FALSE;

	return TRUE;
}

INT	CTempView::GetModuleIDRowCol( ROWCOL nRow, ROWCOL nCol )
{
	int nIndex = 0;
	int nTotalRowCount = m_wndTempGrid.GetRowCount()-1;
		
	if(m_bRackIndexUp)
	{
		nIndex = nTotalRowCount-nRow + (nCol-2)*nTotalRowCount;	// 상단에서 부터 표시 		
	}
	else
	{
		nIndex = (nRow-1) + (nCol-2)*nTotalRowCount;			// 하단에서 부터 표시 
	}
	return EPGetModuleID(nIndex);
}

VOID CTempView::SelectUnitTemp()
{		
	ROWCOL nRow = 0, nCol = 0;	
	INT	r=255, g=255, b=255;
	COLORREF rgbCol = RGB(255,255,255);
	CString strWarnnigUnitNum;
	CString strTemp;
	CString strColor;
	CString	strVal;	
	strWarnnigUnitNum = _T("");

	BOOL bLock = m_wndTempGrid.LockUpdate();	
	for (int nIdx=0; nIdx < EPGetInstalledModuleNum(); nIdx++)	// Module 수만큼
	{			
		if(GetChRowCol( nIdx, nRow, nCol ) == FALSE)		
			continue;		

		int nModuleID = EPGetModuleID(nIdx);
		EP_SYSTEM_PARAM *pSysParam = ::EPGetSysParam(nModuleID);

		strVal = ProcUnitTempData( nModuleID );		

		// 20090427 kky for 경고 온도값보다 온도값과 해당 유닛번호를 경고창에 띄어준다.
		// 모든 Unit을 검사한후 결과창을 띄어준다.
		if( pSysParam->bUseTempLimit )
		{
			if( strVal != "-" && pSysParam->sWanningTemp != 0 )
			{
				strTemp.Format("%d", nModuleID);
				if( atof(strVal) >= pSysParam->sWanningTemp )
				{
					strWarnnigUnitNum += strTemp;
					strWarnnigUnitNum += " ";
				}
			}
		}

		if( strVal == "-" )
		{
			strVal.Format("-");
			rgbCol = RGB(255, 255, 255); 
		}		
		else
		{	
			if( m_nSelectUnitDisplay <= 3 )			// UnitColor
			{
				strTemp = GetUnitColor( strVal );
				AfxExtractSubString(strColor, strTemp, 0, ',');
				r = atoi( strColor );
				AfxExtractSubString(strColor, strTemp, 1, ',');
				g = atoi( strColor );
				AfxExtractSubString(strColor, strTemp, 2, ',');
				b = atoi( strColor );
				rgbCol = RGB( r, g, b );
			}
			else									// JigColor
			{
				strTemp = GetJigColor( strVal );
				AfxExtractSubString(strColor, strTemp, 0, ',');
				r = atoi( strColor );
				AfxExtractSubString(strColor, strTemp, 1, ',');
				g = atoi( strColor );
				AfxExtractSubString(strColor, strTemp, 2, ',');
				b = atoi( strColor );
				rgbCol = RGB( r, g, b );
			}			
		}				
		m_wndTempGrid.SetStyleRange(CGXRange(nRow, nCol), CGXStyle().SetValue(strVal));		
		m_wndTempGrid.SetStyleRange(CGXRange(nRow, nCol), CGXStyle().SetInterior(rgbCol));
	}
	
	if( !strWarnnigUnitNum.IsEmpty() )
	{
		if( m_bWarnningWindow == FALSE )
		{
			CWanningDlg dlg;
			dlg.m_strValue = strWarnnigUnitNum;
			dlg.m_strValue += TEXT_LANG[9];//"번"
			dlg.m_strValue += TEXT_LANG[10];//"모듈의 온도값이 경고치를 넘었습니다."

			m_bWarnningWindow = TRUE;
			
			if( dlg.DoModal() == IDOK )
			{
				m_bWarnningWindow = FALSE;
			}		
		}		
	}

	SetTempAvgVal();
	m_wndTempGrid.LockUpdate(bLock);
	m_wndTempGrid.Redraw();
}

VOID CTempView::SetTempAvgVal()
{
	// -----------------------------------------------------------------------
	// AVG, 최대, 최소 모듈 이름
	// -----------------------------------------------------------------------
	float fMaxValue = -1;
	float fMinValue = 100;
	float fSumValue = -1;
	int	nMaxIndex = -1;
	int nMinIndex = -1;
	int cnt = 0;
	UINT i, j;
	CString strTemp;

	for( i=1; i<m_wndTempGrid.GetRowCount(); i++ )
	{	
		cnt = 0;
		fSumValue = 0;

		for( j=2; j<m_wndTempGrid.GetColCount(); j++ )
		{
			strTemp = m_wndTempGrid.GetValueRowCol(i, j);
			if( strTemp != "-" && !strTemp.IsEmpty() )
			{
				fSumValue += (float)atof(strTemp);
				cnt++;
			}			
		}	
		if( cnt == 0 )
		{
			strTemp.Format("-");
		}
		else
		{
			strTemp.Format("%.1f℃", fSumValue/cnt);
		}				
		m_wndTempGrid.SetStyleRange(CGXRange(i, j), CGXStyle().SetValue(strTemp));		
	}
	
	for( j=2; j<m_wndTempGrid.GetColCount(); j++ )
	{	
		cnt = 0;
		fSumValue = 0;
		for( i=1; i<m_wndTempGrid.GetRowCount(); i++ )
		{
			strTemp = m_wndTempGrid.GetValueRowCol(i, j);			
			if( strTemp != "-" && !strTemp.IsEmpty() )
			{
				fSumValue += (float)atof(strTemp);	
				cnt++;
				if( fMaxValue < (float)atof(strTemp) )
				{
					fMaxValue = (float)atof(strTemp);
					nMaxIndex = GetModuleIDRowCol(i,j);
				}
				if( fMinValue > (float)atof(strTemp) )
				{
					fMinValue = (float)atof(strTemp);
					nMinIndex = GetModuleIDRowCol(i,j);
				}
			}			
		}		
		if( cnt == 0 )
		{
			strTemp.Format("-");
		}
		else
		{
			strTemp.Format("%.1f℃", fSumValue/cnt);
		}		
		m_wndTempGrid.SetStyleRange(CGXRange(i, j), CGXStyle().SetValue(strTemp));				
	}
	
	if( nMaxIndex == -1 )
	{
		strTemp.Format("-");
		m_MaxTempIndex.SetText(strTemp);
	}
	else
	{	
		strTemp.Format("%s   %.1f℃", GetModuleName(nMaxIndex), fMaxValue);
		m_MaxTempIndex.SetText(strTemp);		
	}	

	if( nMinIndex == -1 )
	{
		strTemp.Format("-");
		m_MinTempIndex.SetText(strTemp);	
	}
	else
	{
		strTemp.Format("%s   %.1f℃", GetModuleName(nMinIndex), fMinValue);
		m_MinTempIndex.SetText(strTemp);			
	}
}

void CTempView::OnSelchangeTempCombo() 
{
	// TODO: Add your control notification handler code here
	int nItemNo = m_ctrlDisplayCombo.GetCurSel();
	if( nItemNo < 0 )
		return;	
	m_nSelectChDisplay = 0;
	m_nSelectUnitDisplay = m_ctrlDisplayCombo.GetItemData(nItemNo);		

	if( m_nSelectUnitDisplay != m_nPrevUnitDisplay )
	{
		if( m_nSelectUnitDisplay == 3 || m_nSelectUnitDisplay == 7 )
		{
			m_ctrlChNameCombo.EnableWindow(TRUE);
		}
		else
		{
			m_bInitCombo = FALSE;						
			m_ctrlChNameCombo.EnableWindow(FALSE);
		}
	}	
	
	if( m_nSelectUnitDisplay == 3 )
	{
		if( m_nPrevUnitDisplay == 7 )
		{			
			m_bInitCombo = FALSE;						
		}			
	}
	
	if( m_nSelectUnitDisplay == 7 )
	{
		if( m_nPrevUnitDisplay == 3 )
		{			
			m_bInitCombo = FALSE;						
		}		
	}
	SelectUnitTemp();		
}

CString CTempView::ProcUnitTempData( int nModuleID )			// nIndex => ModuleID
{	
 	CCTSMonDoc *pDoc = GetDocument();
	long TempMax = 0, TempMin = 10000, TempAvg = 0, TempCh = 0;	
	CString strTemp = _T("");
	CString strValue = _T("");
	BOOL	m_bUnitSensor = FALSE;
	BOOL	m_bJigSensor = FALSE;	
	int nCnt = 0;			// 평균 구하기 위한 반복수
	int moduleID;			// 저장된 ModuleID
	int nSensorNo;			// 저장된 SensorNum
	CFormModule		*pModule;
	_MAPPING_DATA	*pMapData;
	
	pModule = pDoc->GetModuleInfo(nModuleID);
	EP_GP_DATA gpData = EPGetGroupData(nModuleID, 0);

	if( gpData.gpState.state != EP_STATE_LINE_OFF )
	{
		for( int i=0; i<MAX_USE_TEMP; i++ )				// 총 Sensor값은 16개
		{
			pMapData = pModule->GetSensorMap(0, i);	
			
			if( pMapData->nChannelNo >= 0 )
			{
				switch( m_nSelectUnitDisplay )
				{
				case EP_POWAVG:	
					if(pMapData->nChannelNo == 0)		// Unit
					{
						TempAvg += gpData.sensorData.sensorData1[i].lData;
						nCnt++;
						m_bUnitSensor = TRUE;
					}					
					break;
					
				case EP_POWMAX:	
					if(pMapData->nChannelNo == 0)		// Unit
					{
						if(TempMax < gpData.sensorData.sensorData1[i].lData)
						{
							TempMax = gpData.sensorData.sensorData1[i].lData;
						}
						m_bUnitSensor = TRUE;
					}							
					break;
					
				case EP_POWMIN:
					if(pMapData->nChannelNo == 0)		// Unit
					{
						if(TempMin > gpData.sensorData.sensorData1[i].lData)
						{
							TempMin = gpData.sensorData.sensorData1[i].lData;
						}
						m_bUnitSensor = TRUE;
					}							
					break;
					
				case EP_POWPOINT:						
					if( m_bInitCombo == FALSE )
					{
						EnableUnitChNameCombo();
					}
					else
					{
						if( m_nSelectChDisplay == -1 )
						{
							strTemp.Format("-");
							return strTemp;
						}									
									
						if( pMapData->nChannelNo == 0 )
						{
							AfxExtractSubString(strTemp,m_UnitChData.GetAt(m_nSelectChDisplay),0,',');
							moduleID = atoi(strTemp);		
							AfxExtractSubString(strTemp,m_UnitChData.GetAt(m_nSelectChDisplay),3,',');
							nSensorNo = atoi(strTemp);						
							
							if( nSensorNo == i )
							{
								m_bUnitSensor = TRUE;
								TempCh = gpData.sensorData.sensorData1[nSensorNo].lData;
							}							
						}											
					}			
					break;
					
				case EP_JIGAVG:		
					if(pMapData->nChannelNo == 1)		// JIG
					{
						TempAvg += gpData.sensorData.sensorData1[i].lData;
						nCnt++;
					}	
					m_bJigSensor = TRUE;
					break;
					
				case EP_JIGMAX:		
					if(pMapData->nChannelNo == 1)		// JIG
					{
						if(TempMax < gpData.sensorData.sensorData1[i].lData)
						{
							TempMax = gpData.sensorData.sensorData1[i].lData;
						}
						m_bJigSensor = TRUE;
					}							
					break;
					
				case EP_JIGMIN:		
					if(pMapData->nChannelNo == 1)		// JIG
					{
						if(TempMin > gpData.sensorData.sensorData1[i].lData)
						{
							TempMin = gpData.sensorData.sensorData1[i].lData;
						}
						m_bJigSensor = TRUE;
					}							
					break;
					
				case EP_JIGPOINT:	
					if( m_bInitCombo == FALSE )
					{
						EnableJigChNameCombo();
					}
					else
					{
						if( m_nSelectChDisplay == -1)
						{
							strTemp.Format("-");
							return strTemp;
						}										
						
						if( pMapData->nChannelNo == 1 )
						{
							AfxExtractSubString(strTemp,m_JigChData.GetAt(m_nSelectChDisplay),0,',');
							moduleID = atoi(strTemp);		
							AfxExtractSubString(strTemp,m_JigChData.GetAt(m_nSelectChDisplay),3,',');
							nSensorNo = atoi(strTemp);														
							if( nSensorNo == i )
							{
								m_bJigSensor = TRUE;
								TempCh = gpData.sensorData.sensorData1[nSensorNo].lData;						
							}						
						}											
					}
					break;
				}
			}			
		}
	}

	m_nPrevUnitDisplay = m_nSelectUnitDisplay;	

	if( m_nSelectUnitDisplay <= 3 )		// Unit
	{
		if( m_bUnitSensor == FALSE )
		{
			strTemp.Format("-");
			return strTemp; 	
		}	
		if(m_nSelectUnitDisplay == 0)
		{
			if(nCnt > 0)
			{
				strTemp.Format("%.1f℃", (float)TempAvg/nCnt/100.0f);			
				return strTemp;
			}
		}
		
		else if(m_nSelectUnitDisplay == 1)
		{
			strTemp.Format("%.1f℃", (float)TempMax/100.0f);
			return strTemp;
		}
		
		else if(m_nSelectUnitDisplay == 2)
		{
			strTemp.Format("%.1f℃", (float)TempMin/100.0f);
			return strTemp;
		}	
		else if(m_nSelectUnitDisplay == 3)
		{
			strTemp.Format("%.1f℃", (float)TempCh/100.0f);
			return strTemp;
		}
	}
	else if( m_nSelectUnitDisplay >= 4 )
	{
		if( m_bJigSensor == FALSE )
		{
			strTemp.Format("-");
			return strTemp; 	
		}

		if( m_nSelectUnitDisplay == 4)
		{
			if(nCnt > 0)
			{
				strTemp.Format("%.1f℃", (float)TempAvg/nCnt/100.0f);			
				return strTemp;
			}
		}
		else if( m_nSelectUnitDisplay == 5 )
		{
			strTemp.Format("%.1f℃", (float)TempMax/100.0f);
			return strTemp;
		}
		
		else if( m_nSelectUnitDisplay == 6 )
		{
			strTemp.Format("%.1f℃", (float)TempMin/100.0f);
			return strTemp;
		}	
		else if( m_nSelectUnitDisplay == 7 )
		{
			strTemp.Format("%.1f℃", (float)TempCh/100.0f);
			return strTemp;
		}
	}
	strTemp.Format("-");
	return strTemp; 
}

VOID CTempView::EnableJigChNameCombo()
{
	// -----------------------------------------------------------------------
	// Jig ChCombo Setting
	// -----------------------------------------------------------------------	
	CCTSMonDoc *pDoc = GetDocument();
	int CurrentModuleID = 0;
	CString strTemp = _T("");
	int i = 0;
	
	m_JigChData.RemoveAll();
	m_JigChData.FreeExtra();
	m_nPrevUnitDisplay = m_nSelectUnitDisplay;
	m_ctrlChNameCombo.ResetContent();

	_MAPPING_DATA	*pMapData;
	CFormModule		*pModule;	

	pModule = pDoc->GetModuleInfo(EPGetModuleID(0));
	if( pModule == NULL )
	{
		return;
	}
	
	for(i = 0; i<MAX_USE_TEMP; i++ )
	{		
		pMapData = pModule->GetSensorMap(0, i);		// SensorType => 0
		if( pMapData->nChannelNo >= 0 )
		{			
			CString strTemp;								
			if( pMapData->nChannelNo == 1 )			// Jig
			{
				// Module, ChNameID, ChName, ChNum
				strTemp.Format("%d,%d,%s,%d", 1, pMapData->nChannelNo, pMapData->szName, i );
				m_JigChData.Add(strTemp);					
			}					
		}
	}		
	m_ctrlChNameCombo.ResetContent();

	if( m_JigChData.GetSize() == 0 )
	{
		m_nSelectChDisplay = -1;
		m_bInitCombo = TRUE;
		return;
	}

	AfxExtractSubString(strTemp,m_JigChData.GetAt(0),0,',');		
	CurrentModuleID = atoi(strTemp);		// Module 1이 기준이 된다.	
	for( i=0; i<m_JigChData.GetSize(); i++)
	{
		AfxExtractSubString(strTemp,m_JigChData.GetAt(i),1,',');		
		if( atoi(strTemp) == 1 )
		{
			AfxExtractSubString(strTemp,m_JigChData.GetAt(i),2,',');		
			m_ctrlChNameCombo.AddString(strTemp);		
		}			
	}		
	m_ctrlChNameCombo.SetCurSel(m_nSelectChDisplay);		
	m_bInitCombo = TRUE;	
}

VOID CTempView::EnableUnitChNameCombo()
{
	// -----------------------------------------------------------------------
	// Unit ChCombo Setting
	// -----------------------------------------------------------------------	
	CCTSMonDoc *pDoc = GetDocument();
	int CurrentModuleID = 0;
	int i = 0;
	CString strTemp;

	m_UnitChData.RemoveAll();
	m_UnitChData.FreeExtra();		
	m_nPrevUnitDisplay = m_nSelectUnitDisplay;
	m_ctrlChNameCombo.ResetContent();
	
	_MAPPING_DATA	*pMapData;
	CFormModule		*pModule;

	pModule = pDoc->GetModuleInfo(EPGetModuleID(0));
	if( pModule == NULL )
	{
		return;
	}
	
	for(i = 0; i<MAX_USE_TEMP; i++ )
	{
		pMapData = pModule->GetSensorMap(0, i);		// SensorType => 0
		if( pMapData->nChannelNo >= 0 )
		{			
			CString strTemp;								
			if( pMapData->nChannelNo == 0 )			// Unit
			{
				// Module, ChNameID, ChName, ChNum
				strTemp.Format("%d,%d,%s,%d", 1, pMapData->nChannelNo, pMapData->szName, i );
				m_UnitChData.Add(strTemp);					
			}					
		}
	}		
	m_ctrlChNameCombo.ResetContent();

	if( m_UnitChData.GetSize() == 0 )
	{			
		m_nSelectChDisplay = -1;
		m_bInitCombo = TRUE;
		return;
	}

	AfxExtractSubString(strTemp,m_UnitChData.GetAt(0),0,',');		
	CurrentModuleID = atoi(strTemp);		// Module 1이 기준이 된다.
	if( CurrentModuleID == 1 )
	{
		for( i=0; i<m_UnitChData.GetSize(); i++)
		{			
			AfxExtractSubString(strTemp,m_UnitChData.GetAt(i),1,',');
			if( atoi(strTemp) == 0)
			{
				AfxExtractSubString(strTemp,m_UnitChData.GetAt(i),2,',');
				m_ctrlChNameCombo.AddString(strTemp);		
			}			
		}			
	}	
	m_ctrlChNameCombo.SetCurSel(m_nSelectChDisplay);		
	m_bInitCombo = TRUE;
}

void CTempView::OnSelchangeChnameCombo() 
{
	// TODO: Add your control notification handler code here
    int SelectChDisplay = m_ctrlChNameCombo.GetCurSel(); 	
	if( SelectChDisplay < 0 )
	{
		m_nSelectChDisplay = -1;
		return;
	}		
	m_nSelectChDisplay = SelectChDisplay;
	SelectUnitTemp();
}

void CTempView::OnBtnTempsetting() 
{	
	// TODO: Add your control notification handler code here		
	CTempColorScaleConfig *m_pTempScaleConfig;
	m_pTempScaleConfig = new CTempColorScaleConfig(m_pDoc);
	ASSERT(m_pTempScaleConfig);
	if( m_pTempScaleConfig == NULL )
	{
		return;
	}

	m_pTempScaleConfig->m_TempScaleConfig = m_pDoc->m_TempScaleConfig;
	
	if( m_pTempScaleConfig->DoModal() == IDOK )
	{
		delete m_pTempScaleConfig;
		m_pTempScaleConfig = NULL;
		
		DrawTempColor();

		if( m_nDisplayTimer == 102 )
		{
			KillTimer(102);
			int nTimer = 2000;
			m_nDisplayTimer = SetTimer(102, nTimer, NULL);
		}
	}
}

LONG CTempView::OnGridDoubleClick(WPARAM wParam, LPARAM lParam)
{
	ROWCOL nRow, nCol;
	lParam = NULL;

//	int nIndex;
//	int nInstallMD;
	nCol = LOWORD(wParam);
	nRow = HIWORD(wParam);
	if( nCol > 1 && nCol < m_wndTempGrid.GetColCount() && 
			nRow > 0 && nRow < m_wndTempGrid.GetRowCount() )
	{
		int nID = GetModuleIDRowCol(nRow, nCol);
		if(nID > 0)
		{
			CSensorMapDlg dlg(m_pDoc, this);
			dlg.m_nModuleID = nID;
			if(dlg.DoModal()==IDOK)
			{	
			}
		}
	}
	return 0;
}

LONG CTempView::OnGridRightClick(WPARAM wParam, LPARAM lParam )
{
	ROWCOL nRow, nCol;
	nCol = LOWORD(wParam);
	nRow = HIWORD(wParam);

	if( nCol > 1 && nCol < m_wndTempGrid.GetColCount() && 
		nRow > 0 && nRow < m_wndTempGrid.GetRowCount() )
	{
		int nModuleID;
		nModuleID = GetModuleIDRowCol(nRow,nCol);
		if(nModuleID > 0)
		{
			CDetailTempCondition dlg(m_pDoc, nModuleID );
			dlg.DoModal();
		}
	}
	return 0;
}

CString CTempView::GetUnitColor( CString strValue )
{
	CString strTemp;
	strTemp = __T("");
	
	int ColorVal;
	int nColCnt;
	int r,g,b;
	
	int nMinVal = 0;
	int nMaxVal = 0;
	int nInputVal = 0;
	int nGradeVal = 0;
	
	nMaxVal = (int)m_pDoc->m_TempScaleConfig.m_fUnitlevel8;
	nMinVal = (int)m_pDoc->m_TempScaleConfig.m_fUnitlevel1;
	
	nInputVal = atoi(strValue);	
	nInputVal = nInputVal - nMinVal;
	nMaxVal = nMaxVal - nMinVal;
	
	// nMinVal = nMinVal * 100;
	nMaxVal = nMaxVal * 100;
	nInputVal = nInputVal*100;
	nMinVal = 0;	
	
	nGradeVal = nMaxVal - nMinVal;
	nGradeVal = nGradeVal/6;
	
	nColCnt = nInputVal/nGradeVal;
	
	r=TempColScl[nColCnt].r;
	g=TempColScl[nColCnt].g;
	b=TempColScl[nColCnt].b;
	
	nInputVal = nInputVal - (nGradeVal * nColCnt);
	
	switch( nColCnt )
	{
	case 0:	
		ColorVal =  (nInputVal*128)/nGradeVal;
		g = TempColScl[nColCnt].g + ColorVal;
		break;
	case 1:
		ColorVal =  (nInputVal*128)/nGradeVal;
		g = TempColScl[nColCnt].g + ColorVal;
		break;
	case 2:
		ColorVal =  (nInputVal*128)/nGradeVal;
		b = TempColScl[nColCnt].b - ColorVal;
		break;
	case 3:
		ColorVal =  (nInputVal*128)/nGradeVal;
		r = TempColScl[nColCnt].r + ColorVal;				
		break;
	case 4:
		ColorVal =  (nInputVal*128)/nGradeVal;
		g = TempColScl[nColCnt].g - ColorVal;
		break;
	case 5:
		ColorVal =  (nInputVal*128)/nGradeVal;
		g = TempColScl[nColCnt].g - ColorVal;
		break;	
	}
	
	strTemp.Format("%d,%d,%d",r,g,b);
	return strTemp;
}

CString CTempView::GetJigColor( CString strValue )
{
	CString strTemp;
	strTemp = __T("");
	
	int ColorVal;
	int nColCnt;
	int r,g,b;
	
	int nMinVal = 0;
	int nMaxVal = 0;
	int nInputVal = 0;
	int nGradeVal = 0;
	
	nMaxVal = (int)m_pDoc->m_TempScaleConfig.m_fJiglevel8;
	nMinVal = (int)m_pDoc->m_TempScaleConfig.m_fJiglevel1;
	
	nInputVal = atoi(strValue);	
	nInputVal = nInputVal - nMinVal;
	nMaxVal = nMaxVal - nMinVal;
	
	// nMinVal = nMinVal * 100;
	nMaxVal = nMaxVal * 100;
	nInputVal = nInputVal*100;
	nMinVal = 0;	
	
	nGradeVal = nMaxVal - nMinVal;
	nGradeVal = nGradeVal/6;
	
	nColCnt = nInputVal/nGradeVal;
	
	r=TempColScl[nColCnt].r;
	g=TempColScl[nColCnt].g;
	b=TempColScl[nColCnt].b;
	
	nInputVal = nInputVal - (nGradeVal * nColCnt);
	
	switch( nColCnt )
	{
	case 0:	
		ColorVal =  (nInputVal*128)/nGradeVal;
		g = TempColScl[nColCnt].g + ColorVal;
		break;
	case 1:
		ColorVal =  (nInputVal*128)/nGradeVal;
		g = TempColScl[nColCnt].g + ColorVal;
		break;
	case 2:
		ColorVal =  (nInputVal*128)/nGradeVal;
		b = TempColScl[nColCnt].b - ColorVal;
		break;
	case 3:
		ColorVal =  (nInputVal*128)/nGradeVal;
		r = TempColScl[nColCnt].r + ColorVal;				
		break;
	case 4:
		ColorVal =  (nInputVal*128)/nGradeVal;
		g = TempColScl[nColCnt].g - ColorVal;
		break;
	case 5:
		ColorVal =  (nInputVal*128)/nGradeVal;
		g = TempColScl[nColCnt].g - ColorVal;
		break;	
	}
	
	strTemp.Format("%d,%d,%d",r,g,b);
	return strTemp;
}

void CTempView::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	// TODO: Add your message handler code here
	int i,j,y,by;
	CPen wPen;
	CDC *pDC;
	RECT gra;
	int r,g,b,dy;
	
	pDC = m_JigScale.GetDC();
	m_JigScale.GetClientRect(&gra);
	
	gra.left	+= 1;
	gra.bottom	-= 1;
	gra.right	-= 1;
	
	for(i=0;i<7;i++)
	{
		wPen.CreatePen(PS_SOLID, 1, RGB(TempColScl[i].r,TempColScl[i].g,TempColScl[i].b));
		pDC->SelectObject(&wPen);
		
		if (i == 6)
		{
			y = gra.bottom-i * gra.bottom / 6 + 1;
		}
		else
		{
			y = gra.bottom-i * gra.bottom / 6 - 1;
		}
		
		pDC->MoveTo(gra.left,y);
		pDC->LineTo(gra.right,y);
		
		wPen.DeleteObject();
		
		if (i)
		{
			dy = by-y;
			for (j=1;j<dy;j++)
			{
				r=TempColScl[i-1].r+(TempColScl[i].r-TempColScl[i-1].r)*j/dy;
				g=TempColScl[i-1].g+(TempColScl[i].g-TempColScl[i-1].g)*j/dy;
				b=TempColScl[i-1].b+(TempColScl[i].b-TempColScl[i-1].b)*j/dy;
				wPen.CreatePen(PS_SOLID, 1, RGB(r,g,b));
				pDC->SelectObject(&wPen);
				pDC->MoveTo(gra.left,by-j);
				pDC->LineTo(gra.right,by-j);
				wPen.DeleteObject();
			}
		}
		by=y;
	}
	m_JigScale.ReleaseDC(pDC);	
	

	pDC = m_UnitScale.GetDC();
	m_UnitScale.GetClientRect(&gra);
	
	gra.left	+= 1;
	gra.bottom	-= 1;
	gra.right	-= 1;
	
	for(i=0;i<7;i++)
	{
		wPen.CreatePen(PS_SOLID, 1, RGB(TempColScl[i].r,TempColScl[i].g,TempColScl[i].b));
		pDC->SelectObject(&wPen);
		
		if (i == 6)
		{
			y = gra.bottom-i * gra.bottom / 6 + 1;
		}
		else
		{
			y = gra.bottom-i * gra.bottom / 6 - 1;
		}
		
		pDC->MoveTo(gra.left,y);
		pDC->LineTo(gra.right,y);
		
		wPen.DeleteObject();
		
		if (i)
		{
			dy = by-y;
			for (j=1;j<dy;j++)
			{
				r=TempColScl[i-1].r+(TempColScl[i].r-TempColScl[i-1].r)*j/dy;
				g=TempColScl[i-1].g+(TempColScl[i].g-TempColScl[i-1].g)*j/dy;
				b=TempColScl[i-1].b+(TempColScl[i].b-TempColScl[i-1].b)*j/dy;
				wPen.CreatePen(PS_SOLID, 1, RGB(r,g,b));
				pDC->SelectObject(&wPen);
				pDC->MoveTo(gra.left,by-j);
				pDC->LineTo(gra.right,by-j);
				wPen.DeleteObject();
			}
		}
		by=y;
	}
	m_UnitScale.ReleaseDC(pDC);		
	// Do not call CFormView::OnPaint() for painting messages
}