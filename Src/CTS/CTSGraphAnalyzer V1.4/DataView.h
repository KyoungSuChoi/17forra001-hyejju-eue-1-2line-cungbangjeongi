#if !defined(AFX_DATAVIEW_H__ADD07C71_4F4E_4F11_A9EB_918693785425__INCLUDED_)
#define AFX_DATAVIEW_H__ADD07C71_4F4E_4F11_A9EB_918693785425__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DataView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDataView form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

#include "NewGridWnd.h"

//#define MAX_EXCEL_ROW	20
#include "RangeSelDlg.h"

class CDataView : public CFormView
{
protected:
	CDataView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CDataView)

// Form Data
public:
	//{{AFX_DATA(CDataView)
	enum { IDD = IDD_DATA_FORM };
	CLabel	m_ctrlCurStep;
	CComboBox	m_StepSelCombo;
	CListCtrl	m_ctrlTestInfoList;
	CTreeCtrl	m_TestTree; 

	CString *TEXT_LANG;	
	BOOL LanguageinitMonConfig();
	//}}AFX_DATA

// Attributes
public:

// Operations
public:
//	void InitStepEndGrid();
	void DisplayData(HTREEITEM hItem);
	int SaveToExcelFile(CString strFilter, CString strSaveFile);
	BOOL AddStepStartData();
	CString GetUnitString(int nItem);
	CString ValueString(double dData, int item, BOOL bUnit = FALSE);
//	void UpdateUnitSetting();
//	BOOL m_bShowChList;
//	BOOL UpdateRangeList();			// 범위를 설정하는 함수로 임시 삭제
	void DisplayCurSelCh();
	void LoadSelChData();
	CString GetDefaultExcelFileName(CString strTestPath, CString strChName);
//	void InitChListGrid();
//	BOOL ExecuteExcel(CString strFileName);
	void UpdateTestInfoList(CString strChName);
	void InitTestInfoList();
//	void InitRawDataGrid();
	void UpdateStepRawData(LONG	lTableIndex, BOOL bShowStepStartData = FALSE);
	void CopyDataToClipBoard(CListCtrl *pListCtrl);
	void UpdateStepData(CString strChPath);
//	void UpdateStepDataA(CString strChPath);
	void ListUpTree();
	void OnGridCopy();
	BOOL UpdateDataList();
	void OnAnalToGraphDataView( CString strPath );

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDataView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	BOOL m_bInitedTooltip;
	void InitTooltip();
	CToolTipCtrl m_tooltip;
	BOOL MakeNextFile(FILE *fp,  int &nFileCount, CString strFile);
	BOOL ExcelAllStepData(BOOL bSelStep = FALSE);
	CUnitTrans m_UnitTrans;
//	int	m_nRawGridWidth;

	int m_nCurrentUnitMode;
	
/*	int m_nTimeUnit;
	CString m_strVUnit;
	int m_nVDecimal ;
	CString m_strIUnit;
	int m_nIDecimal;
	CString m_strCUnit;
	int m_nCDecimal;
	CString m_strWUnit;
	int m_nWDecimal;
	CString m_strWhUnit;
	int m_nWhDecimal;
*/
	CList<LONG, LONG&> m_RangeList;
//	void UpdateChListData(CString strPath);
//	CGridCtrl	m_wndChListGrid;
	CGridCtrl	m_wndStepGrid;
//	CGridCtrl	m_wndRawDataGrid;

	CMyGridWnd	m_wndStepEndGrid;

	CImageList	*m_pImagelist;
	void InitStepListCtrl();
	virtual ~CDataView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CDataView)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSelchangedTestTree(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnGridLButtonDblClk(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnGridRButtonUp(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnTestSelButton();
	afx_msg void OnUpdateEditCopy(CCmdUI* pCmdUI);
	afx_msg void OnSelectAll();
	afx_msg void OnFilePrint();
	afx_msg void OnRclickTestTree(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnUpdateSelectAll(CCmdUI* pCmdUI);
	afx_msg void OnEditClear();
	afx_msg void OnUpdateEditClear(CCmdUI* pCmdUI);
	afx_msg void OnSchButton();
	afx_msg void OnExcelStepEnd();
	afx_msg void OnExcelSelStep();
	afx_msg void OnUpdateExcelSelStep(CCmdUI* pCmdUI);
	afx_msg void OnExcelAllStepData();
	afx_msg void OnUpdateExcelAllStepData(CCmdUI* pCmdUI);
	afx_msg void OnGraphView();
	afx_msg void OnApplyButton();
	afx_msg void OnStepStartDetail();
	afx_msg void OnUpdateStepStartDetail(CCmdUI* pCmdUI);
	afx_msg void OnUserOption();
	afx_msg void OnExcelDetailButton();
	afx_msg void OnWorkLogView();
	afx_msg void OnManagePattern();
	afx_msg void OnUpdateManagePattern(CCmdUI* pCmdUI);
	afx_msg void OnUpdateWorkLogView(CCmdUI* pCmdUI);	
	afx_msg void OnDblclkTestTree(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	afx_msg void OnGridSelectChanged(NMHDR* pNMHDR, LRESULT* pResult);

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATAVIEW_H__ADD07C71_4F4E_4F11_A9EB_918693785425__INCLUDED_)
