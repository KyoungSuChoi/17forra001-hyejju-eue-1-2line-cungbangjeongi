#pragma once

#define FMS_RESPONSE(FMS_OP_COMMAND)\
	m_vUnit[indexNum]->fnGetFMS()->fnSend_##FMS_OP_COMMAND(_rec);\

typedef struct st_FMS_ChannelCode
{
	INT nCode;
	CHAR szMessage[3];
}FMS_ChannelCode;

#ifdef _CHARGER
	#define _MIN_CC_I	100.0f
	#define _MAX_CC_I	30000.0f
	#define _MIN_CV_V	1800.0f
	// #define _MAX_CV_V	4350.0f
	#define _MAX_CV_V	5000.0f
	#define _MAX_I		30000.0f
	#define _MAX_V		5000.0f
	// #define _OVP_V		4400.0f
	#define _OVP_V		5050.0f

#elif _DCIR
	#define _MIN_CC_I	100.0f
	#define _MAX_CC_I	240000.0f
	#define _MIN_CV_V	1800.0f
	#define _MAX_CV_V	5000.0f
	#define _MAX_I		240000.0f
	#define _MAX_V		5000.0f
	#define _OVP_V		5050.0f

#elif _PRECHARGER
	#define _MIN_CC_I	100.0f
	#define _MAX_CC_I	10000.0f
	#define _MIN_CV_V	1800.0f
	#define _MAX_CV_V	5000.0f
	#define _MAX_I		10000.0f
	#define _MAX_V		5000.0f
	#define _OVP_V		5050.0f
#endif

class CFMS
{
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	CFMS(void);
	virtual ~CFMS(void);

	VOID	fnInit(CFormModule* _Module);

	INT		strLinker(CHAR *msg, VOID * _dest, INT* mesmap);
	INT		strCutter(CHAR *msg, VOID * _dest, INT* mesmap);

public:
	FMS_ERRORCODE fnReserveProc(INT64);
	FMS_ERRORCODE fnReStartSet(INT64);
	FMS_ERRORCODE fnMakeHead(st_FMSMSGHEAD&, CHAR*, UINT, CHAR _result = 0x20);

	VOID				fnSetFMSStateCode(UINT, FMS_STATE_CODE);
	FMS_STATE_CODE		fnGetFMSStateCode();

	st_FMS_PACKET* fnGetPack(){return &m_Packet;}
	//VOID fnSetPack(st_FMS_PACKET& _pack, INT _lineno)
	//{memcpy(&m_Packet, &_pack, sizeof(st_FMS_PACKET));m_LineNo = _lineno;}

	VOID fnGetFMSImpossbleCode(st_IMPOSSBLE_STATE& _sate);
	VOID fnGetFMSDCIRImpossbleCode(st_DCIR_IMPOSSBLE_STATE& _iState);
	
	CString m_strTypeSel[4];
	VOID fnLoadTypeSel();


	BOOL m_OverCCheck;  //20200411 엄륭 용량 상한 관련
	int m_bChgChkF; //20200807 엄륭
	int m_ProcessNo;

	BOOL fnRun();

	UINT64 fnGetWorkCode(){return m_WorkCode;}

	BOOL fnSendResponse(CHAR*, CHAR _resultcode = '0');
	INT fnGetLineNo();
	CHAR* fnGetTrayID();

	//////////////////////////////////////////////////////////////////////////
	CString Time_Conversion(CString str_Time);
	FMS_ERRORCODE fnMakeResult();	
	FMS_ERRORCODE fnMisSendMakeResult_CHARGE(CString, st_CHARGER_INRESEVE&);
	FMS_ERRORCODE fnMisSendMakeResult_DCIR(CString, st_DCIR_INRESEVE&);	
	//////////////////////////////////////////////////////////////////////////

	FMS_ERRORCODE fnSetOperationMode(CHAR);
	CString fnGetResultData(){return m_str_Result_Data;}

	//////////////////////////////////////////////////////////////////////////
	FMS_ERRORCODE SendSBCInitCommand();
	FMS_ERRORCODE SendGUIInitCommand();
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	FMS_ERRORCODE fnSend_E_HEAET_BEAT_P(FMS_ERRORCODE);
	FMS_ERRORCODE fnSend_E_TIME_SET_RESPONSE(FMS_ERRORCODE);
	FMS_ERRORCODE fnSend_E_ERROR_NOTICE_RESPONSE(FMS_ERRORCODE);
	FMS_ERRORCODE fnSend_E_EQ_ERROR(FMS_ERRORCODE);	

	FMS_ERRORCODE fnSend_E_CHARGER_IMPOSSBLE_RESPONSE(FMS_ERRORCODE);
	FMS_ERRORCODE fnSend_E_CHARGER_INRESEVE_RESPONSE(FMS_ERRORCODE);
	FMS_ERRORCODE fnSend_E_CHARGER_IN_RESPONSE(FMS_ERRORCODE);
	FMS_ERRORCODE fnSend_E_CHARGER_WORKEND(FMS_ERRORCODE);
	FMS_ERRORCODE fnSend_E_CHARGER_RESULT_RESPONSE(FMS_ERRORCODE);
	FMS_ERRORCODE fnSend_E_CHARGER_RESULT_RECV_RESPONSE(FMS_ERRORCODE);
	FMS_ERRORCODE fnSend_E_CHARGER_OUT_RESPONSE(FMS_ERRORCODE);	
	FMS_ERRORCODE fnSend_E_CHARGER_MODE_CHANGE_RESPONSE(FMS_ERRORCODE);
	FMS_ERRORCODE fnSend_E_CHARGER_STATE(FMS_ERRORCODE);
	//////////////////////////////////////////////////////////////////////////
	FMS_ERRORCODE fnSend_E_CHARGER_WORKEND_MisSend_CHARGER(CHAR*, st_CHARGER_INRESEVE&);
	FMS_ERRORCODE fnSend_E_CHARGER_WORKEND_MisSend_DCIR(CHAR*, st_DCIR_INRESEVE&);
	FMS_ERRORCODE fnSend_E_CHARGER_RESULT_RESPONSE_MisSend();
	FMS_ERRORCODE fnSend_E_CHARGER_RESULT_RECV_RESPONSE_MisSend(FMS_ERRORCODE);
	//////////////////////////////////////////////////////////////////////////
	BOOL fnResultDataStateUpdate(CHAR*, RESULTDATA_STATE _rState);
private:
	BOOL fnRangeCheck( STEP_VALUE_RANGE _range, CHAR* _szValue, FLOAT& _fValue, FMS_Step_Type fmsStepType = FMS_SKIP );
	BOOL fnLoadWorkInfo(INT64);
	BOOL fnTransFMSWorkInfoToCTSWorkInfo();
	BOOL fnSendConditionToModule();
	BOOL fnCheckProcAndSystemType(long lProcTypeID);
	INT fnStepValidityCheck();
	BOOL fnMakeTrayInfo(bool bNew = TRUE);

	//////////////////////////////////////////////////////////////////////////
	//Run
	BOOL FileNameCheck(char *szfileName);
	BOOL CheckFileNameValidate(CString strFileName, CString &strNewFileName, int nModuleID, int nGroupIndex = 0 );
	CString CreateResultFilePath(int nModuleID, CString strDataFolder, CString strLot, CString strTray);
	INT ShowRunInformation(int nModuleID, int nGroupIndex, BOOL bNewFileName, long lProcPK);
	BOOL SendLastTrayStateData(int nModuleID, int nGroupIndex);
	DWORD CheckEnableRun(int nModuleID);	
	//////////////////////////////////////////////////////////////////////////

	CStep* fnGetStepList(){return m_pStep;}
	//////////////////////////////////////////////////////////////////////////
#ifdef _CHARGER

	BOOL fnLoadWorkInfo_CHARGER(INT64, st_CHARGER_INRESEVE&);

#elif _DCIR

	BOOL fnLoadWorkInfo_DCIR(INT64, st_DCIR_INRESEVE&);

#elif _PRECHARGER

	BOOL fnLoadWorkInfo_CHARGER(INT64, st_CHARGER_INRESEVE&);
#endif
	//////////////////////////////////////////////////////////////////////////
	BOOL fnLoadChannelCode();
	CHAR* fnGetChannelCode(INT _code);
	VOID fnResetChannelCode();
	VOID fnWriteTemperature();
	//////////////////////////////////////////////////////////////////////////
	
private:
	COleDateTime	m_OleRunStartTime;
	COleDateTime	m_OleRunEndTime;

private:
	INT m_nModuleID;
	CFormModule* m_pModule;

private:
	UINT64 m_WorkCode;

#ifdef _CHARGER

	st_CHARGER_INRESEVE m_inreserv;

#elif _DCIR

	st_DCIR_INRESEVE m_inreserv;

#elif _PRECHARGER

	st_CHARGER_INRESEVE m_inreserv;

#endif

private:
	CTestCondition m_TestCondition;
	UINT m_totStep;
	CStep *m_pStep;
private:
	EP_RUN_CMD_INFO	m_runInfo;
private:
	UINT m_Mode;
	FMS_STATE_CODE m_FMSStateCode;

private:	
	st_FMS_PACKET m_Packet;
	st_FMS_PACKET m_RecvPacket;

private:
	CString m_strDataFolder;
	int		m_nTimeFolderInterval;
	BOOL	m_bFolderTime;
	BOOL	m_bFolderModuleName;
	BOOL	m_bFolderLot;
	BOOL	m_bFolderTray;

private:
	CString m_resultfilePathNName;
	CString m_str_Result_Data;

#ifdef _DCIR
	st_DCIR_RESULT_FILE_RESPONSE_L m_Result_Data_L;
	st_DCIR_Result_Step_Set m_total_Step[100];
	st_DCIR_RESULT_FILE_RESPONSE_R m_Result_Data_R;
#else
	st_RESULT_FILE_RESPONSE_L m_Result_Data_L;
	st_Result_Step_Set m_total_Step[100];
	st_RESULT_FILE_RESPONSE_R m_Result_Data_R;
#endif

private:
	std::vector<st_FMS_ChannelCode *> m_vChannelCode;
public:
	INT fnLoadSendMisVector();
	VOID fnResetSendMisVector();
	BOOL fnGetSendMisState(){return m_MisSendState;}
	VOID fnSetSendMisState(INT _state){m_MisSendState = _state;}
	BOOL fnSendMisProc();
	VOID fnSendMisNext(){m_SendMisIdx++;}
	BOOL fnMisSendResultDataStateUpdate();
	BOOL fnMisSendResultDataStateUpdate2();	
private:
	INT m_MisSendState;
	INT m_SendMisIdx;
	INT m_SendMisCnt;
	std::vector<st_SEND_MIS_INFO*> m_vSendMis;

#ifdef _DCIR
	st_DCIR_RESULT_FILE_RESPONSE_L m_MisSend_Result_Data_L;
	st_DCIR_Result_Step_Set m_MisSend_total_Step[100];
	st_DCIR_RESULT_FILE_RESPONSE_R m_MisSend_Result_Data_R;
#else 
	st_RESULT_FILE_RESPONSE_L m_MisSend_Result_Data_L;
	st_Result_Step_Set m_MisSend_total_Step[100];
	st_RESULT_FILE_RESPONSE_R m_MisSend_Result_Data_R;
#endif

	CString m_str_MisSendResult_Data;
public:
	//결과 파일 상태가 END
	//상태 강제 전이.....
	INT m_PreState;
public:
	//DWORD fnGetLastState(){return m_LastState;}
	//VOID fnSetLastState(DWORD _state){m_LastState = _state;}
	VOID fnSetError(){m_bError = TRUE;}
	BOOL fnGetError()
	{
		return m_bError;
	}
	VOID fnResetError(){m_bError = FALSE;}
private:
    BOOL m_bError;
	DWORD m_LastState;
public:
	VOID fnSet_E_EQ_ERROR_Dtat(CString _errCode);
private:
	st_EQ_ERROR m_ErrorData;
	CHAR m_Result_FIle_Type;
};
