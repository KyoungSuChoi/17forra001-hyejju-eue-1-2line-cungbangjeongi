// ErrorHistoryDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "ErrorHistoryDlg.h"


// CErrorHistoryDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CErrorHistoryDlg, CDialog)

CErrorHistoryDlg::CErrorHistoryDlg(CCTSMonDoc *pDoc, CWnd* pParent /*=NULL*/)
	: CDialog(CErrorHistoryDlg::IDD, pParent)
{
	m_pDoc = pDoc;
	LanguageinitMonConfig();
}

CErrorHistoryDlg::~CErrorHistoryDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

bool CErrorHistoryDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CErrorHistoryDlg"), _T("TEXT_CErrorHistoryDlg_CNT"), _T("TEXT_CErrorHistoryDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CErrorHistoryDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CErrorHistoryDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

void CErrorHistoryDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, ID_DLG_LABEL_NAME, m_Label1);
	DDX_Control(pDX, IDOK, m_BtnClose);
	DDX_Control(pDX, IDC_BTN_EXCEL_CONVERT, m_BtnExcelConvert);
}


BEGIN_MESSAGE_MAP(CErrorHistoryDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CErrorHistoryDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BTN_EXCEL_CONVERT, &CErrorHistoryDlg::OnBnClickedBtnExcelConvert)
END_MESSAGE_MAP()


// CErrorHistoryDlg 메시지 처리기입니다.
VOID CErrorHistoryDlg::InitFieldGrid()
{
	m_wndFieldGrid.SubclassDlgItem(IDC_SEARCH_FIELD_GRID, this);
	m_wndFieldGrid.Initialize();
	
	m_wndFieldGrid.m_bSameColSize  = FALSE;
	m_wndFieldGrid.m_bSameRowSize  = FALSE;	

	CRect rect;
	m_wndFieldGrid.GetParam()->EnableUndo(FALSE);

	m_wndFieldGrid.SetRowCount(0);
	m_wndFieldGrid.SetColCount(7);
	m_wndFieldGrid.SetDefaultRowHeight(24);	
		
	m_wndFieldGrid.EnableScrollBar(SB_VERT);	

	//Enable Multi Channel Select
	m_wndFieldGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	m_wndFieldGrid.SetDrawingTechnique(gxDrawUsingMemDC);	

	m_wndFieldGrid.SetColWidth(0		, 0			, 0);
	m_wndFieldGrid.SetColWidth(1		, 1			, 200);
	m_wndFieldGrid.SetColWidth(2		, 2			, 100);
	m_wndFieldGrid.SetColWidth(3		, 3			, 100);
	m_wndFieldGrid.SetColWidth(4		, 6			, 100);
	m_wndFieldGrid.SetColWidth(7		, 7			, 500);

	m_wndFieldGrid.SetStyleRange(CGXRange(0,ID_COL_DATE),	CGXStyle().SetValue("Date Time"));
	m_wndFieldGrid.SetStyleRange(CGXRange(0,ID_COL_STAGENO),CGXStyle().SetValue("Stage No"));
	m_wndFieldGrid.SetStyleRange(CGXRange(0,ID_COL_TRAYNO),	CGXStyle().SetValue("TrayNo"));
	m_wndFieldGrid.SetStyleRange(CGXRange(0,ID_COL_TYPE),	CGXStyle().SetValue("Type"));
	m_wndFieldGrid.SetStyleRange(CGXRange(0,ID_COL_MODE),	CGXStyle().SetValue("Mode"));
	m_wndFieldGrid.SetStyleRange(CGXRange(0,ID_COL_CODE),	CGXStyle().SetValue("Code"));		
	m_wndFieldGrid.SetStyleRange(CGXRange(0,ID_COL_ERRMSG),	CGXStyle().SetValue("Error Message"));
		
	m_wndFieldGrid.GetParam()->EnableUndo(TRUE);
	
	m_wndFieldGrid.SetScrollBarMode(SB_VERT, gxnEnabled | gxnEnhanced);

	//Static Ctrl/////////////////////////////////////
	m_wndFieldGrid.SetStyleRange(CGXRange().SetTable(), CGXStyle().SetFont(CGXFont().SetSize(13).SetBold(true)));
}

void CErrorHistoryDlg::InitLabel()
{
	m_Label1.SetFontSize(24)
		.SetBkColor(RGB_MIDNIGHTBLUE)
		.SetTextColor(RGB_WHITE)
		.SetFontBold(TRUE)
		.SetText("Error History");	
}

// CErrorHistoryDlg 메시지 처리기입니다.
bool CErrorHistoryDlg::LoadEmgFromProductsMDB()
{	
	ASSERT(m_pDoc);
	
	CString strTemp = _T("");
	CString strSQL = _T("");	
	
	CDaoDatabase  db;
	COleVariant data;	
	
	int nCount = 0;

	try
	{				
		db.Open(m_pDoc->m_strLogDataBaseName);		
		strSQL.Format("Select [No], [DateTime], [Stage], [TrayNo], [Type], [OperationMode], [Code], [Message], [CheckFlag] From EMG_Log order by DateTime DESC");

		CDaoRecordset rs(&db);
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
		
		while(!rs.IsEOF())
		{
			nCount++;
			m_wndFieldGrid.SetRowCount(nCount);
			
			data = rs.GetFieldValue(ID_COL_DATE);
			COleDateTime dt(data.date);
			strTemp = dt.Format("%y/%m/%d %H:%M:%S");
			m_wndFieldGrid.SetValueRange(CGXRange(nCount, ID_COL_DATE), strTemp);
		
			data = rs.GetFieldValue(ID_COL_STAGENO); 
			strTemp = data.pbVal;
			m_wndFieldGrid.SetValueRange(CGXRange(nCount, ID_COL_STAGENO), strTemp);
			
			data = rs.GetFieldValue(ID_COL_TRAYNO);
			strTemp = data.pbVal;			
			m_wndFieldGrid.SetValueRange(CGXRange(nCount, ID_COL_TRAYNO), strTemp);
			
			data = rs.GetFieldValue(ID_COL_TYPE);
			strTemp.Format("%d", data.lVal);						
			m_wndFieldGrid.SetValueRange(CGXRange(nCount, ID_COL_TYPE), strTemp);
			
			data = rs.GetFieldValue(ID_COL_MODE);
			if( data.lVal == EP_OPERATION_AUTO )
			{
				strTemp.Format("Auto");						
			}
			else
			{
				strTemp.Format("Local");			
			}
			m_wndFieldGrid.SetValueRange(CGXRange(nCount, ID_COL_MODE), strTemp);
						
			data = rs.GetFieldValue(ID_COL_CODE);
			strTemp = data.pbVal;
			m_wndFieldGrid.SetValueRange(CGXRange(nCount, ID_COL_CODE), strTemp);
			
			data = rs.GetFieldValue(ID_COL_ERRMSG);
			strTemp = data.pbVal;			
			m_wndFieldGrid.SetValueRange(CGXRange(nCount, ID_COL_ERRMSG), strTemp);			
			
			rs.MoveNext();
		}
		
		rs.Close();
	}
	catch (CDBException* e)
	{		
		AfxMessageBox(e->m_strError+e->m_strStateNativeOrigin); 
		e->Delete();
		db.Close();
		return FALSE;
	}
	db.Close();
	
	return true;	
}

BOOL CErrorHistoryDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	
	InitLabel();
	
	InitFieldGrid();
	
	CenterWindow();
	
	InitColorBtn();
	
	LoadEmgFromProductsMDB();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CErrorHistoryDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnOK();
}

void CErrorHistoryDlg::InitColorBtn()
{	
	m_BtnClose.SetFontStyle(20,1);
	m_BtnClose.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	
	m_BtnExcelConvert.SetFontStyle(17,1);
	m_BtnExcelConvert.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
}

void CErrorHistoryDlg::ClearGrid()
{
	int nRow = 0;
	nRow = m_wndFieldGrid.GetRowCount();
	
	if( nRow != 0 )
	{
		m_wndFieldGrid.RemoveRows(1, nRow);	
	}
}

void CErrorHistoryDlg::OnBnClickedBtnExcelConvert()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	
	CString strFileName;
	
	COleDateTime nowtime(COleDateTime::GetCurrentTime());
	
	strFileName.Format("ErrorHistory_%s.csv", nowtime.Format("%y%m%d%H%M%S"));

	CFileDialog pDlg(FALSE, "", strFileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, TEXT_LANG[0]);//"csv 파일(*.csv)|*.csv|"
	if(IDOK == pDlg.DoModal())
	{
		strFileName = pDlg.GetPathName();
		if(strFileName.IsEmpty())	return ;

		FILE *fp = fopen(strFileName, "wt");
		if(fp == NULL)	return ;
		
		int i = 0;
		int j = 0;

		for( i=0; i< m_wndFieldGrid.GetRowCount(); i++ )
		{
			for( j=1; j< m_wndFieldGrid.GetColCount()+1; j++ )
			{
				if( j > 1)
				{
					fprintf(fp, ",");
				}
				fprintf(fp, "%s", m_wndFieldGrid.GetValueRowCol(i, j));
			}
			fprintf(fp, "\n");
		}
		
		fclose(fp);
	}	
}
