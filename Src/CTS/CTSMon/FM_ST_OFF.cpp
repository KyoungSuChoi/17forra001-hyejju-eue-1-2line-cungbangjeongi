#include "stdafx.h"

#include "CTSMon.h"

#include "FMS.h"
#include "FM_Unit.h"

#include "FM_STATE.h"

CFM_ST_OFF::CFM_ST_OFF(FM_STATUS_ID _Eqstid, FM_STATUS_ID _stid, CFM_Unit* _unit)
:CFM_STATE(_Eqstid, _stid, _unit)
{
}


CFM_ST_OFF::~CFM_ST_OFF(void)
{
}

VOID CFM_ST_OFF::fnEnter()
{
	fnSetFMSStateCode(FMS_ST_OFF);
	//CHANGE_FNID(FNID_PROC);


	//TRACE("CFM_ST_OFF::fnEnter %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_OFF::fnProc()
{
	//TRACE("CFM_ST_OFF::fnProc %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_OFF::fnExit()
{
	//TRACE("CFM_ST_OFF::fnExit %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_OFF::fnSBCPorcess(WORD _state)
{
	if(m_Unit->fnGetModule()->GetState() == EP_STATE_LINE_OFF)
	{

	}
	else
	{
		switch(m_Unit->fnGetModule()->GetOperationMode())
		{
		case EP_OPERATION_LOCAL:
			CHANGE_STATE(EQUIP_ST_LOCAL);
			break;
		case EP_OPEARTION_MAINTENANCE:
			CHANGE_STATE(EQUIP_ST_MAINT);
			break;
		case EP_OPERATION_AUTO:
			CHANGE_STATE(EQUIP_ST_AUTO);
			break;
		}
	}

	//TRACE("CFM_ST_OFF::fnSBCPorcess %d \n", m_Unit->fnGetModuleIdx());
}

FMS_ERRORCODE CFM_ST_OFF::fnFMSPorcess(FMS_COMMAND _msgId, st_FMS_PACKET* _recvData)
{
	FMS_ERRORCODE _error = ER_NO_Process;

	switch(_msgId)
	{
	default:
		{
		}
		break;
	}
	//TRACE("CFM_ST_OFF::fnFMSPorcess %d \n", m_Unit->fnGetModuleIdx());

	return _error;
}

FMS_STATE_CODE CFM_ST_OFF::fnGetFMSStateCode()
{
	FMS_STATE_CODE code;


	return code;
}