// ColorSelDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ctsanal.h"
#include "ColorSelDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CColorSelDlg dialog

CColorSelDlg::CColorSelDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CColorSelDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CColorSelDlg)
	m_strCode = _T("");
	m_Color = RGB(255, 255, 255);
	//}}AFX_DATA_INIT
	LanguageinitMonConfig();
}

CColorSelDlg::~CColorSelDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

bool CColorSelDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CColorSelDlg"), _T("TEXT_CColorSelDlg_CNT"), _T("TEXT_CColorSelDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CColorSelDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CColorSelDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

void CColorSelDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CColorSelDlg)
//	DDX_Control(pDX, IDC_BUTTON1, m_btnColor);
	DDX_Text(pDX, IDC_EDIT1, m_strCode);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CColorSelDlg, CDialog)
	//{{AFX_MSG_MAP(CColorSelDlg)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CColorSelDlg message handlers

BOOL CColorSelDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	
	m_btnColor.AttachButton(IDC_BUTTON1, this);	

	m_btnColor.SetColor(m_Color);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CColorSelDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
	int n = m_strCode.GetLength();
	if( n > 5)
	{
		AfxMessageBox(TEXT_LANG[0]);//"등급 code 5개 문자만 입력 가능합니다."
		return;
	}
	m_Color = m_btnColor.GetColor();
	CDialog::OnOK();
}

void CColorSelDlg::OnButton1() 
{
	// TODO: Add your control notification handler code here
	
}
