#include "stdafx.h"

#include "CTSMon.h"
#include "MainFrm.h"
#include "CTSMonDoc.h"

#include "FMS.h"
#include "FM_Unit.h"
#include "FM_STATE.h"

#include "FMS_SM.h"

DWORD WINAPI FMS_MAINProcessCallback(LPVOID parameter)
{
	CFMS_SM *Owner = (CFMS_SM*) parameter;
	Owner->FMS_MAINProcessCallback();

	return 0;
}

// 1. FMS 데이터 파싱부분
VOID CFMS_SM::FMS_MAINProcessCallback(VOID)
{
	while (TRUE)
	{
		DWORD Result = WaitForSingleObject(mFMSMainThreadDestroyEvent, 100);

		if(Result == WAIT_OBJECT_0)
			return;

		if( m_bRunChk == FALSE )
		{
			m_bRunChk = TRUE;
			
			while(theApp.m_FMS_RecvQ.GetIsEmpty() == FALSE)
			{
				st_FMS_PACKET pack;
				theApp.m_FMS_RecvQ.Pop(pack);
				//////////////////////////////////////////////////////////////////////////
				//Header Check
				//Line
				if(strcmp(pack.head.Line, m_Not_Module_Fms.fnGetPack()->head.Line) != 0)
				{
					CHAR cmd[5] = {0};
					sprintf(cmd, "%04d", atoi(pack.head.Command) + 1000);
					m_Not_Module_Fms.fnSendResponse(cmd, '1');

					CFMSLog::WriteErrorLog("Line Error [%s]",cmd);
					break;
				}
				//수신자			
				if(strcmp(pack.head.Addressee, m_Not_Module_Fms.fnGetPack()->head.Sender) != 0)
				{
					CHAR cmd[5] = {0};
					sprintf(cmd, "%04d", atoi(pack.head.Command) + 1000);
					m_Not_Module_Fms.fnSendResponse(cmd, '1');

					CFMSLog::WriteErrorLog("Addressee Error [%s]",cmd);
					break;
				}
				//////////////////////////////////////////////////////////////////////////
				//PC단 처리할 내용
				//어느 상태든지 처리를 해주어야 할 내용
				switch(atoi(pack.head.Command))
				{
				case E_CHARGER_IMPOSSBLE:
					{
						st_CHARGER_IMPOSSBLE_RESPONSE impossble_response;
						memset(&impossble_response, 0x20, sizeof(st_CHARGER_IMPOSSBLE_RESPONSE));
						UINT stateIdx = 0;

						int nSize = m_vUnit.size();

						for(UINT i = 0; i < m_vUnit.size();i++)
						{
							if( fnGetDeviceID() == DEVICE_FORMATION1 )
							{
								if(i == 0 || i == 1) continue;
							}
							else if( fnGetDeviceID() == DEVICE_FORMATION2 )
							{
								if( i == 0 || i == 1 || i== 8 || i==9 )	continue;
							}
							else if( fnGetDeviceID() == DEVICE_FORMATION4 )
							{
								if( i == 31 || i == 39 || i == 47 )	continue;
							}
							else if( fnGetDeviceID() == DEVICE_FORMATION5 )
							{
								if( i == 7 || i == 15 || i == 23 || i == 31 )	continue;
							}
							else if( fnGetDeviceID() == DEVICE_CHARGER2 )
							{
								if( i == 7 || i == 15 || i == 22 || i == 23 )	continue;
							}
							else if( fnGetDeviceID() == DEVICE_CHARGER3 )
							{
								if( i == 7 || i == 15 || i == 23 || i == 31 || i == 39 || i == 47 )	continue;
							}
													
							m_vUnit[i]->fnGetFMS()->fnGetFMSImpossbleCode(
								impossble_response.EQ_State[stateIdx++]);
						
						}

						st_FMS_PACKET* pPack = m_Not_Module_Fms.fnGetPack();
						memset(pPack->data, 0x20, sizeof(pPack->data));

#ifdef _DCIR
						UINT dataLen = m_Not_Module_Fms.strLinker(pPack->data
							, &impossble_response.EQ_State, g_iDCIR_IMPOSSBLE_RESPONSE_Map);
#else
						UINT dataLen = m_Not_Module_Fms.strLinker(pPack->data
							, &impossble_response.EQ_State, g_iCHARGER_IMPOSSBLE_RESPONSE_Map);						
#endif

						pPack->dataLen = dataLen;

						m_Not_Module_Fms.fnMakeHead(pPack->head, CHARGER_IMPOSSBLE_RESPONSE, dataLen, RESULT_CODE_OK);

						theApp.m_FMS_SendQ.Push(*pPack);
					}	
					break;

				case E_HEAET_BEAT_F:
					{
						if( pack.head.ResultcCode[3] == '0' )
						{
							CHAR cmd[5] = {0};
							sprintf(cmd, "%04d", atoi(pack.head.Command) + 1000);
							m_Not_Module_Fms.fnSendResponse(cmd, '1');						
						}

						theApp.m_bHeartbeatChk = FALSE;
					}
					break;

				case E_TIME_SET:
					{
						CString pszText = pack.head.SendTime;

						UINT v_year, v_month, v_day,v_hour,v_min,v_sec;
						sscanf(pszText,"%04d%02d%02d%02d%02d%02d"
							, &v_year, &v_month, &v_day, &v_hour, &v_min, &v_sec);

						SYSTEMTIME	tnow;
						tnow.wYear = v_year;
						tnow.wMonth = v_month;
						tnow.wDay = v_day;
						tnow.wHour = v_hour;
						tnow.wMinute = v_min;
						tnow.wSecond = v_sec;
						tnow.wMilliseconds=0;

						st_TIME_SET_RESPONSE misResult;
						memset(&misResult, 0x00, sizeof(st_TIME_SET_RESPONSE));
						
						/////////////////////////////////////////////////////////////////////////
						//갯수 가져오기
						//
						m_bMisSendWorking = false;
						m_nMisWorkingUnitNum = -1;
						
						INT sendMisCnt = 0;		// total 갯수
						INT nStageCnt = 0;
						
						for (INT i = 0; i < m_vUnit.size(); i++)
						{
							nStageCnt = m_vUnit[i]->fnGetFMS()->fnLoadSendMisVector();

							if( nStageCnt != 0 )
							{
								CFMSLog::WriteLog(TEXT_LANG[0].GetBuffer(TEXT_LANG[0].GetLength()), m_vUnit[i]->fnGetModule()->GetModuleName(), nStageCnt);//"[Stage %s] 미전송 데이터 갯수 : [%d]"

								// if( m_vUnit[i]->fnGetModuleState() == EP_STATE_IDLE  || m_vUnit[i]->fnGetModuleState() == EP_STATE_LINE_OFF )
								if( m_vUnit[i]->fnGetModuleState() == EP_STATE_IDLE )
								{
									m_vUnit[i]->fnGetFMS()->fnSetSendMisState(1);		// 작업 트레이가 없는 경우 미전송 상태로 변경
									// CFMSLog::WriteLog("[Stage %d] 미전송 전송 상태로 전환", m_vUnit[i]->fnGetModuleID() );
									CFMSLog::WriteLog(TEXT_LANG[1].GetBuffer(TEXT_LANG[1].GetLength()), m_vUnit[i]->fnGetModule()->GetModuleName() );//"[Stage %s] 미전송 전송 상태로 전환"
								}
							}
							else
							{
								sendMisCnt += nStageCnt;
							}
						}						

						sprintf_s(misResult.Send_Fail_Count, "%03d", sendMisCnt);
						//////////////////////////////////////////////////////////////////////////
						st_FMS_PACKET* pPack = m_Not_Module_Fms.fnGetPack();
						memset(pPack->data, 0x20, sizeof(pPack->data));
						UINT dataLen = m_Not_Module_Fms.strLinker(pPack->data
							, &misResult, g_iTIME_SET_RESPONSE_Map);
						pPack->dataLen = dataLen;

						if (SetLocalTime(&tnow))
						{
							m_Not_Module_Fms.fnMakeHead(pPack->head
								, TIME_SET_RESPONSE, dataLen, RESULT_CODE_OK);

							theApp.m_FMS_SendQ.Push(*pPack);
						}
						else
						{
							m_Not_Module_Fms.fnMakeHead(pPack->head
								, TIME_SET_RESPONSE, dataLen, RESULT_CODE_TIME_SET_FAIL);

							theApp.m_FMS_SendQ.Push(*pPack);
						}
						//////////////////////////////////////////////////////////////////////////
					}
					break;
				case E_CHARGER_MODE_CHANGE:
					{
						st_CHARGER_MODE_CHANGE recvdata;
						ZeroMemory(&recvdata, sizeof(st_CHARGER_MODE_CHANGE));
						m_Not_Module_Fms.strCutter(pack.data, &recvdata, g_iCHARGER_MODE_CHANGE_Map);
						
						INT stageidx = fnGetStageIdx(atoi(recvdata.Equipment_Num));
						CHAR changeMode = recvdata.Equipment_State[0];

						st_RESPONS_RESERVE* pRespnse = new st_RESPONS_RESERVE;
						ZeroMemory(pRespnse, sizeof(st_RESPONS_RESERVE));

						pRespnse->fmsERCode = ER_Status_Error;	// Default 'Status Errror' 로 셋팅 후 정상일 경우 값 변경

						if(stageidx >= 0 && stageidx < m_vUnit.size())
						{
							FM_STATUS_ID ModeID = m_vUnit[stageidx]->fnGetState()->fnGetEQStateID();
							FM_STATUS_ID stateID = m_vUnit[stageidx]->fnGetState()->fnGetStateID();

							//응답 바로
							pRespnse->RRState = RRS_RECVSET;

							// 현재 모드
							switch(ModeID)
							{
							case EQUIP_ST_AUTO:
								{
									switch(changeMode)
									{
									case '0':
										pRespnse->fmsERCode = FMS_ER_NONE;
										break;
									case '1':
										{
											if(stateID != AUTO_ST_RUN && stateID != AUTO_ST_CONTACT_CHECK )
											{
												FMS_ERRORCODE ercode = m_vUnit[stageidx]->fnGetFMS()->fnSetOperationMode(changeMode);
												if(ercode == ER_Stage_Setting_Error)
												{
													m_vUnit[stageidx]->fnSendEmg(m_Papa, 255);
													m_vUnit[stageidx]->fnGetFMS()->fnSetError();
												}

												pRespnse->fmsERCode = FMS_ER_NONE;
												//pRespnse->fmsCommand = (FMS_COMMAND)atoi(pack.head.Command);
												//pRespnse->timer = 10;
												//m_vUnit[stageidx]->fnRsponse(pRespnse);											
											}
										}
										break;
									default:
										{
											pRespnse->fmsERCode = ER_Settiing_Data_error;								
										}							
									}
								}
								break;
							case EQUIP_ST_LOCAL:
								{
									switch(changeMode)
									{
									case '0':
										{
											if(m_vUnit[stageidx]->fnGetModuleTrayState() == FALSE 
												&& m_vUnit[stageidx]->fnGetModuleState() == EP_STATE_IDLE)
											{
												FMS_ERRORCODE ercode = m_vUnit[stageidx]->fnGetFMS()->fnSetOperationMode(changeMode);
												if(ercode == ER_Stage_Setting_Error)
												{
													m_vUnit[stageidx]->fnSendEmg(m_Papa, 255);
													m_vUnit[stageidx]->fnGetFMS()->fnSetError();
												}

												pRespnse->fmsERCode = FMS_ER_NONE;
												//pRespnse->fmsCommand = (FMS_COMMAND)atoi(pack.head.Command);
												//pRespnse->timer = 10;
												//m_vUnit[stageidx]->fnRsponse(pRespnse);
											}
										}
										break;
									case '1':
										{									
											pRespnse->fmsERCode = FMS_ER_NONE;
										}
										break;
									default:
										pRespnse->fmsERCode = ER_Settiing_Data_error;
									}
								}
								break;						
							}

							// TRACE("E_CHARGER_MODE_CHANGE [EQUIP_ST_OFF] %d \n", stageidx);
							m_vUnit[stageidx]->fnGetFMS()->fnSend_E_CHARGER_MODE_CHANGE_RESPONSE(pRespnse->fmsERCode);						
						}
					}
					break;
				case E_ERROR_NOTICE:
					{
						st_ERROR_NOTICE recvdata;
						ZeroMemory(&recvdata, sizeof(st_ERROR_NOTICE));
						m_Not_Module_Fms.strCutter(pack.data, &recvdata, g_iERROR_NOTICE_Map);
						
						INT stageidx = fnGetStageIdx(atoi(recvdata.Equipment_Num));

						if(stageidx >= 0 && stageidx < m_vUnit.size())
						{
							st_ERROR_NOTICE_RESPONSE data;
							memset(&data, 0x20, sizeof(st_ERROR_NOTICE_RESPONSE));

							sprintf(data.Equipment_Num, "%03d", m_vUnit[stageidx]->fnGetFMS()->fnGetLineNo());

							st_FMS_PACKET* pPack = m_vUnit[stageidx]->fnGetFMS()->fnGetPack();
							memset(pPack->data, 0x20, sizeof(pPack->data));
							UINT dataLen = m_vUnit[stageidx]->fnGetFMS()->strLinker(pPack->data
								, &data, g_iERROR_NOTICE_RESPONSE_Map);

							pPack->dataLen = dataLen;

							m_vUnit[stageidx]->fnGetFMS()->fnSetError();

							m_vUnit[stageidx]->fnGetFMS()->fnMakeHead(pPack->head
								, ERROR_NOTICE_RESPONSE, dataLen, RESULT_CODE_OK);

							theApp.m_FMS_SendQ.Push(*pPack);
						}
					}
					break;
				default:
					{
						CHAR szmachinid[4] = {0};
						memcpy(szmachinid, pack.data, 3);
						INT stageidx = fnGetStageIdx(atoi(szmachinid));

						if(stageidx >= 0 && stageidx < m_vUnit.size())
						{
							m_FMS_Q[stageidx].Push(pack);
						}
						else
						{
							//error
						}
					}

					break;
				}
			}
			
			m_bRunChk = FALSE;
		}		
	}
}

INT CFMS_SM::fnGetStageIdx( int nMachineID )
{
	int nSize = m_vUnit.size();
	
	for(UINT i = 0; i < nSize;i++)
	{
		if( m_vUnit[i]->fnGetMachineId() == nMachineID )
		{
			return i;
		}
	}

	return 0;
};

DWORD WINAPI FMS_SMProcessCallback(LPVOID parameter)
{
	CFMS_SM *Owner = (CFMS_SM*) parameter;
	Owner->FMS_SMProcessCallback();

	return 0;
}

VOID CFMS_SM::FMS_SMProcessCallback(VOID)
{
	INT indexNum = mTcpTheadIndex++;

	SetEvent(mFMS_StartProcessHandle);

	while (TRUE)
	{
		DWORD Result = WaitForSingleObject(m_vUnitProcEvent[indexNum], FMSSM_MAIN_TIMER);

		if (m_bTreadStop)
			return;

		if(Result == WAIT_OBJECT_0)
		{
		}
		else
		{
			m_vUnit[indexNum]->fnRsponseProc();

			m_vUnit[indexNum]->fnGetState()->fnSBCPorcess(0);

			if(m_FMS_Q[indexNum].GetIsEmpty())
			{
				// Cmd 전송시 Blue, Red 상태 Time Delay 체크 부분
				m_vUnit[indexNum]->fnProcessing();
			}
			
			//미전송 확인
			if(m_vUnit[indexNum]->fnGetFMS()->fnGetSendMisState())
			{				
				if( m_bMisSendWorking == false )
				{
					//미전송 프로세스 시작
					m_bMisSendWorking = true;
					m_nMisWorkingUnitNum = indexNum;					
				}
				
				if( m_nMisWorkingUnitNum == indexNum )
				{
					m_vUnit[indexNum]->fnGetFMS()->fnSendMisProc();
				}
			}
			else
			{
				if( m_nMisWorkingUnitNum == indexNum )
				{
					if( m_bMisSendWorking == true )
					{
						m_bMisSendWorking = false;
					}				
				}
			}

			while(m_FMS_Q[indexNum].GetIsEmpty() == FALSE)
			{
				st_FMS_PACKET pack;
				m_FMS_Q[indexNum].Pop(pack);

				FMS_ERRORCODE _rec = ER_END;
				switch(atoi(pack.head.Command))
				{
				case E_CHARGER_INRESEVE:
					{
						//		FM_STATUS_ID FMSstate = m_vUnit[indexNum]->fnGetState()->fnGetStateID();
						//		if(FMSstate == AUTO_ST_READY)
						//		{
						//			_rec = ER_Already_Reserve;
						//		}
					}
					break;
				case E_CHARGER_WORKEND_RESPONSE:
					{
						if(atoi(pack.head.ResultcCode) == 2)
						{
							_rec = FMS_ER_NONE;
							m_vUnit[indexNum]->fnGetFMS()->fnSetSendMisState(3);
						}
					}
					break;
				case E_CHARGER_RESULT_RECV:
					{
						st_CHARGER_RESULT_RECV data;
						memset(&data, 0x20, sizeof(st_CHARGER_RESULT_RECV));
						m_vUnit[indexNum]->fnGetFMS()->strCutter(pack.data, &data
							, g_iCHARGER_RESULT_RECV_Map);

						if(data.Notify_Mode[0] == '2')
						{
							// 1. 미전송 파일 OK
							m_vUnit[indexNum]->fnGetFMS()->fnMisSendResultDataStateUpdate();
							m_vUnit[indexNum]->fnGetFMS()->fnSendMisNext();
							m_vUnit[indexNum]->fnGetFMS()->fnSetSendMisState(1);
							_rec = FMS_ER_NONE;
						}
						else if(data.Notify_Mode[0] == '3')
						{
							// 1. 미전송 파일 NG
							m_vUnit[indexNum]->fnGetFMS()->fnSendMisNext();
							m_vUnit[indexNum]->fnGetFMS()->fnSetSendMisState(1);
							_rec = FMS_ER_NONE;
						}

						if(FMS_ER_NONE == _rec)
						{
							m_vUnit[indexNum]->fnGetFMS()->fnSend_E_CHARGER_RESULT_RECV_RESPONSE(_rec);
							continue;
						}
					}
					break;
				}
				

				if(ER_END == _rec)
				{
					_rec = m_vUnit[indexNum]->fnGetState()->fnFMSPorcess(
						(FMS_COMMAND)atoi(pack.head.Command), &pack);
				}

				if(ER_fnSendConditionToModule == _rec)
				{
					m_vUnit[indexNum]->fnSendEmg(m_Papa, 255);
					m_vUnit[indexNum]->fnGetFMS()->fnSetError();
				}

				if(ER_Run_Fail == _rec)
				{
					m_vUnit[indexNum]->fnSendEmg(m_Papa, 255);
					m_vUnit[indexNum]->fnGetFMS()->fnSetError();
				}			
				else if( ER_fnRecipeOverC_Error == _rec )
				{
					m_vUnit[indexNum]->fnSendEmg(m_Papa, 256);
					m_vUnit[indexNum]->fnGetFMS()->fnSetError();
				}


				switch(atoi(pack.head.Command))
				{
				case E_CHARGER_OUT:
					{
						FMS_RESPONSE(E_CHARGER_OUT_RESPONSE);
					}
					break;
				case E_CHARGER_INRESEVE:
					{
						FMS_RESPONSE(E_CHARGER_INRESEVE_RESPONSE);
					}
					break;
				case E_CHARGER_IN:
					{
						FMS_RESPONSE(E_CHARGER_IN_RESPONSE);
					}
					break;
				case E_CHARGER_RESULT:
					{
						FMS_RESPONSE(E_CHARGER_RESULT_RESPONSE);
					}
					break;
				case E_CHARGER_RESULT_RECV:
					{
						FMS_RESPONSE(E_CHARGER_RESULT_RECV_RESPONSE);
					}
					break;
				
				case E_CHARGER_MODE_CHANGE:
					{
						FMS_RESPONSE(E_CHARGER_MODE_CHANGE_RESPONSE);
					}
					break;
				}

				TRACE(" ID [%d] %s \n", indexNum, pack.head.Command);
			}	
		}
	}
}

CFMS_SM::CFMS_SM(void)
: m_Papa(NULL)
, m_strFMID("")
, m_strFMUnitID("") 
, m_bTreadStop(FALSE)
, mTcpTheadIndex(0)
, mFMS_StartProcessHandle(NULL)
, mFMSMainThreadHandle(NULL)
, mFMSMainThreadDestroyEvent(NULL)
{
	m_bWork = false;
	m_bMisSendWorking = false;
	m_nMisWorkingUnitNum = -1;
	m_nDeviceID = 0;
	LanguageinitMonConfig();
}

CFMS_SM::~CFMS_SM(void)
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CFMS_SM::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CFMS_SM"), _T("TEXT_CFMS_SM_CNT"), _T("TEXT_CFMS_SM_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CFMS_SM_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CFMS_SM"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}


BOOL CFMS_SM::fnBegin(HWND _papa, CPtrArray& _ArrayModule, int nDeviceID )
{
	CFMSSyncObj FMSSync;
	
	m_Papa = _papa;

	m_nDeviceID = nDeviceID;

	m_Not_Module_Fms.fnInit(NULL);
	UINT i = 0;
	
	//프로세스 스레스 정상 시작 이벤트 생성
	mFMS_StartProcessHandle = CreateEvent(NULL, FALSE, FALSE, NULL);
	if (!mFMS_StartProcessHandle)
	{
		return fnEnd();
	}

	for(i = 0; i < _ArrayModule.GetSize(); i++)
		{
		CFM_Unit* pTemp = new CFM_Unit();
		pTemp->fnInit(i, (CFormModule *)_ArrayModule.GetAt(i));
		//pTemp->fnSetPack(pack, linNo);
		//m_strFMID = pTemp->fnGetModule()->m_strModuleCode.Left(8);
		m_vUnit.push_back(pTemp);
	}
	
	//모듈만큼 프로세스 시작 이벤트 핸들 생성
	//i = 0;
	//for(i = 0; i < 5; i++)
	for(i = 0; i < _ArrayModule.GetSize(); i++)
	{
		HANDLE ProcEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
		if(ProcEvent)
			m_vUnitProcEvent.push_back(ProcEvent);
		else
			return fnEnd();
	}
	
	//i = 0;
	//for(i = 0; i < 5; i++)
	for(i = 0; i < _ArrayModule.GetSize(); i++)
	{
		//프로세스 스레드 생성
		HANDLE WorkerThread = CreateThread(NULL, 0, ::FMS_SMProcessCallback, this, 0, NULL);
		if(WorkerThread)
		{
			m_vUnitThread.push_back(WorkerThread);
			WaitForSingleObject(mFMS_StartProcessHandle, INFINITE);
		}
		else
			return fnEnd();
	}

	CloseHandle(mFMS_StartProcessHandle);
	mFMS_StartProcessHandle = NULL;

	mFMSMainThreadDestroyEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	if (!mFMSMainThreadDestroyEvent)
	{
		return fnEnd();
	}

	mFMSMainThreadHandle = CreateThread(NULL, 0, ::FMS_MAINProcessCallback, this, 0, NULL);
	if(!mFMSMainThreadHandle)
	{
		return fnEnd();
	}

	m_bWork = true;			// FMS_SM 동작을 위한 준비 OK.
	m_bRunChk = FALSE;
	return TRUE;
}
BOOL CFMS_SM::fnEnd()
{
	if (mFMSMainThreadDestroyEvent && mFMSMainThreadHandle)
	{
		SetEvent(mFMSMainThreadDestroyEvent);
		WaitForSingleObject(mFMSMainThreadHandle, INFINITE);
		CloseHandle(mFMSMainThreadDestroyEvent);
		CloseHandle(mFMSMainThreadHandle);
	}

	m_bTreadStop = TRUE;

	UINT i = 0;
	for(i = 0; i < m_vUnitThread.size(); i++)
	{
		SetEvent(m_vUnitProcEvent[i]);
		WaitForSingleObject(m_vUnitThread[i], INFINITE);
		CloseHandle(m_vUnitProcEvent[i]);
		CloseHandle(m_vUnitThread[i]);	
	}

	m_vUnitProcEvent.clear();
	m_vUnitThread.clear();

	for(i = 0; i < m_vUnit.size(); i++)
	{
		CFM_Unit* ptemp = m_vUnit[i];

		ptemp->fnUnInit();

		delete ptemp;
	}
	
	m_vUnit.clear();
	
	m_bWork = false;

	return TRUE;
}

BOOL CFMS_SM::fnSetEMG(INT moduleID, CString emgCode)
{
	if( m_bWork == false )
	{
		return FALSE;	
	}
	
	if(moduleID > 0 && moduleID <= m_vUnit.size())
	{
		INT moduleidx = moduleID - 1;
		m_vUnit[moduleidx]->fnGetFMS()->fnSet_E_EQ_ERROR_Dtat(emgCode);
	}
	else
	{
		CFMSLog::WriteErrorLog("fnSetEMG [%d]", moduleID);	
	}

	return TRUE;
}

BOOL CFMS_SM::fnRsponseSet(INT moduleID, EP_RESPONSE& _response)
{	
	if( m_bWork == false )
	{
		return FALSE;	
	}
	
	if(moduleID > 0 && moduleID <= m_vUnit.size())
	{
		INT moduleidx = moduleID - 1;
		m_vUnit[moduleidx]->fnRsponseSet(_response);	
	}
	else
	{
		CFMSLog::WriteErrorLog("fnRsponseSet [%d]", moduleID);
	}

	return TRUE;
}

FMS_STATE_CODE CFMS_SM::fnGetFMSStateForAllSystemView(INT moduleID)
{
	if( m_bWork == false )
	{
		return FMS_ST_OFF;	
	}
	
	if(moduleID > 0 && moduleID <= m_vUnit.size())
	{
		INT moduleidx = moduleID - 1;
		return m_vUnit[moduleidx]->fnGetFMSState();	
	}
	else
	{
		CFMSLog::WriteErrorLog("fnGetFMSStateForAllSystemView [%d]", moduleID);	
	}

	return FMS_ST_OFF;
}

BOOL CFMS_SM::fnClearReset(INT moduleID)
{
	if( m_bWork == false )
	{
		return FALSE;
	}
	
	if(moduleID > 0 && moduleID <= m_vUnit.size())
	{
		INT moduleidx = moduleID - 1;
		m_vUnit[moduleidx]->fnSetState(m_vUnit[moduleidx]->fnGet_EQUIP_ST_OFF());
		//m_vUnit[moduleidx]->fnGetFMS()->fnSetLastState(FMS_ST_OFF);
		m_vUnit[moduleidx]->fnGetFMS()->fnResetError();
	}
	else
	{
		CFMSLog::WriteErrorLog("fnRsponseSet [%d]", moduleID);
	}
	
	return TRUE;
}

BOOL CFMS_SM::fnClearColor(INT moduleID)
{
	if( m_bWork == false )
	{
		return FALSE;
	}
	
	BOOL _result = FALSE;
	if(moduleID > 0 && moduleID <= m_vUnit.size())
	{
		INT moduleidx = moduleID - 1;
		CFM_Unit* pUnit = m_vUnit[moduleidx];
		switch(pUnit->fnGetFMSState())
		{
		case FMS_ST_RED_READY:
			pUnit->fnGetState()->fnWaitInit();
			pUnit->fnGetState()->fnSetProcID(FNID_ENTER);
			_result = TRUE;
			break;
		case FMS_ST_TRAY_IN:
			pUnit->fnGetState()->fnWaitInit();
			pUnit->fnGetState()->fnSetProcID(FNID_ENTER);
			_result = TRUE;
		
			break;
		case FMS_ST_RED_END:
			pUnit->fnGetState()->fnWaitInit();
			pUnit->fnGetState()->fnSetFMSStateCode(FMS_ST_END);
			pUnit->fnGetState()->fnSetProcID(FNID_EXIT);
			_result = TRUE;
			break;
		case FMS_ST_BLUE_END:
			pUnit->fnGetState()->fnWaitInit();
			pUnit->fnGetState()->fnSetProcID(FNID_PROC);
			_result = TRUE;
			break;
		case FMS_ST_RED_TRAY_IN:
			pUnit->fnGetState()->fnWaitInit();
			pUnit->fnGetState()->fnSetProcID(FNID_ENTER);
			_result = TRUE;
			break;
		default:
			_result = FALSE;
			break;
		}
	}

	return _result;
}

// 1. Tray 가 입고중이고 공정이 하달된 경우 Run 명령을 전송한다.
BOOL CFMS_SM::fnTrayInStateReset(INT moduleID)
{
	if( m_bWork == false )
	{
		return FALSE;
	}

	BOOL _result = FALSE;

	if(moduleID > 0 && moduleID <= m_vUnit.size())
	{
		INT moduleidx = moduleID - 1;
		CFM_Unit* pUnit = m_vUnit[moduleidx];
		if(pUnit->fnGetState()->fnGetEQStateID() == EQUIP_ST_AUTO
			&& pUnit->fnGetState()->fnGetStateID() == AUTO_ST_TRAY_IN )
		{	
			if( pUnit->fnGetFMS()->fnRun() )
			{
				_result = TRUE;
			}
		}
	}

	return _result;
}

BOOL CFMS_SM::fnEndStateReset(INT moduleID)
{
	if( m_bWork == false )
	{
		return FALSE;
	}
	
	BOOL _result = FALSE;

	if(moduleID > 0 && moduleID <= m_vUnit.size())
	{
		INT moduleidx = moduleID - 1;
		CFM_Unit* pUnit = m_vUnit[moduleidx];
		if(pUnit->fnGetState()->fnGetEQStateID() == EQUIP_ST_AUTO
			 && pUnit->fnGetState()->fnGetStateID() == AUTO_ST_END)
		{
			pUnit->fnGetState()->fnWaitInit();
			pUnit->fnGetState()->fnSetProcID(FNID_ENTER);
			_result = TRUE;
		}
	}

	return _result;
}

//////////////////////////////////////////////////////////////////////////
// 
/*
case '2':
	{										
		if(stateID == AUTO_ST_READY || stateID == AUTO_ST_TRAY_IN)
		{
			m_vUnit[stageidx]->fnSetState(m_vUnit[stageidx]->fnGet_AUTO_ST_VACANCY());

			pRespnse->fmsERCode = m_vUnit[stageidx]->fnGetFMS()->SendSBCInitCommand();
			pRespnse->fmsERCode = m_vUnit[stageidx]->fnGetFMS()->SendGUIInitCommand();
		}
		else
		{
			pRespnse->fmsERCode = ER_Status_Error;
		}
	}
	break;
case '3':
	{
		FM_STATUS_ID stateID = m_vUnit[stageidx]->fnGetState()->fnGetStateID();
		if(stateID != AUTO_ST_RUN && stateID != AUTO_ST_CONTACT_CHECK )
		{
			m_vUnit[stageidx]->fnSetState(m_vUnit[stageidx]->fnGet_EQUIP_ST_OFF());
			//m_vUnit[stageidx]->fnGetFMS()->fnSetLastState(FMS_ST_OFF);
			m_vUnit[stageidx]->fnGetFMS()->fnResetError();

			pRespnse->fmsERCode = m_vUnit[stageidx]->fnGetFMS()->SendSBCInitCommand();
			pRespnse->fmsERCode = m_vUnit[stageidx]->fnGetFMS()->SendGUIInitCommand();

			//error reset
			//AfxGetApp()->WriteProfileInt(FMS_REG, "FMS_ERROR_State", );

			m_vUnit[stageidx]->fnGetFMS()->fnResetSendMisVector();
		}
		else
		{
			pRespnse->fmsERCode = ER_Status_Error;
		}
	}
	break;
default:
	pRespnse->fmsERCode = ER_Settiing_Data_error;
	
// m_Not_Module_Fms.fnSend_E_CHARGER_MODE_CHANGE_RESPONSE(ER_Data_Error);
*/