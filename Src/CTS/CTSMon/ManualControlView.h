#pragma once
#include "afxwin.h"
#include "CTSMonDoc.h"
#include "NormalSensorMapDlg.h"

// CManualControlView 폼 뷰입니다.

#define MAX_AUTO_SEQUENCE	100
#define MAX_WRITE_ADDR_CNT	48
#define MAX_READ_ADDR_CNT	80
#define MAX_WRITE_PORT		6
#define MAX_READ_PORT		10

#define MAX_ADDR_CNT		16

typedef struct 
{
	unsigned short ch_0				:1;		// 래치
	unsigned short ch_1				:1;		// 클립
	unsigned short ch_2				:1;		// 스토퍼#1
	unsigned short ch_3				:1;		// 스토퍼#2
	unsigned short ch_4				:1;		// 스토퍼#3
	unsigned short ch_5				:1;		// 스토퍼#4
	unsigned short ch_6				:1;		// 지그 플레이트 전진
	unsigned short ch_7				:1;		// 지그 플레이트 상승
	unsigned short ch_8				:1;		// 클렙 플레이트 전진
	unsigned short ch_9				:1;		// 지그부 팬
	unsigned short ch_10			:1;		// Buzzer
	unsigned short ch_11			:1;		// 메인 지그 전진
	unsigned short ch_12			:1;		
	unsigned short ch_13			:1;
	unsigned short ch_14			:1;
	unsigned short ch_15			:1;	
} IO_Data;

class CManualControlView : public CFormView
{
	DECLARE_DYNCREATE(CManualControlView)
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

protected:
	CManualControlView();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CManualControlView();

public:
	enum { IDD = IDD_MANUAL_CONTROL_VIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

public:	
	// Version Check
	CNormalSensorMapDlg *m_pNormalSensorMapDlg;
	int m_nCurModuleID;
	bool m_bStart;	
	CString m_strProjectName;	
	CString m_strCurdir;
	CFont	m_font;
	int		m_nLineMode;
	
	IO_Data m_SendIOData;
	IO_Data m_RecvIOData;
	unsigned short m_nRecvIOData;

	CLedButton m_stateButton[MAX_READ_ADDR_CNT];

	COLORREF m_crBackground;
	CBrush m_wndbkBrush;	// background brush

	BOOL m_bOutMask;		
	bool m_bReadySetCmd;

	unsigned short	m_nInputAddress[16];
	unsigned short	m_nOutputAddress[16];
	CString	m_strInputName[MAX_READ_ADDR_CNT];
	CString	m_strOutputName[MAX_WRITE_ADDR_CNT];
	BYTE	m_outputMask[MAX_WRITE_ADDR_CNT];
	BYTE	m_InputData[10];
	int		m_nChkInputData;

public:
	CCTSMonDoc* GetDocument();	
	void	WriteData();
	void	UpdateSendBtnEnable(bool bEnable);
	void	UpdateIoState(bool bForceUpdate = false);
	void	ClearIoState();
	void	RollBackOutState(int nPort=0);
	int		Hex2Dec(CString hexData);
	void	ParsingConfig(CString strConfig);
	void	InitValue();
	void	InitLabel();
	void	InitFont();
	void	InitColorBtn();
	bool	LoadIoConfig();	
			
	void	SetBackgroundColor(COLORREF crBackground);
	CString GetTargetModuleName();
	void	ModuleConnected(int nModuleID);
	void	ModuleDisConnected(int nModuleID);
	void	SetCurrentModule(int nModuleID, int nGroupIndex = 0 );
	void	StopMonitoring();
	void	StartMonitoring();
	bool    UpdateGroupState(int nModuleID, int nGroupIndex=0);
	
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CLabel m_LabelViewName;
	virtual void OnInitialUpdate();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	// afx_msg BOOL OnEraseBkgnd(CDC* pDC);	
	afx_msg void OnTimer(UINT_PTR nIDEvent);	
	afx_msg void OnBnClickedLocalModeBtn();
	afx_msg void OnBnClickedMaintenanceModeBtn();
	CLabel m_CmdTarget;
	CLabel m_ctrlWorkMode;
	CColorButton2 m_Btn_AutoUpdateStop;
	UINT m_nAutoIntervalTime;
	int	m_nDisplayTimer;
//	virtual BOOL PreTranslateMessage(MSG* pMsg);
	CLabel m_LabelOutput1;
	CLabel m_LabelOutput2;
	CLabel m_LabelOutput3;
	CLabel m_LabelOutput4;
	CLabel m_LabelOutput5;
	CLabel m_LabelOutput6;
	CLabel m_LabelInput1;
	CLabel m_LabelInput2;
	CLabel m_LabelInput3;
	CLabel m_LabelInput4;
	CLabel m_LabelInput5;
	CLabel m_LabelInput6;
	CLabel m_LabelInput7;
	CLabel m_LabelInput8;
	CLabel m_LabelInput9;
	CLabel m_LabelInput10;
	afx_msg void OnBnClickedRestartBtn();
	afx_msg void OnBnClickedResetBtn();
	CColorButton2 m_Btn_ReStart;
	CColorButton2 m_Btn_Reset;
//	afx_msg void OnBnClickedKillProcessButton();
//	afx_msg void OnBnClickedRebootButton();
//	afx_msg void OnBnClickedHaltButton();
	CLabel m_Label1;
	CLabel m_Label2;
	CLabel m_Label3;
	CLabel m_Label4;
	CLabel m_Label6;
	CLabel m_LabelTypeSel1;
	CLabel m_LabelTypeSel2;
	CLabel m_LabelTypeSel3;
	CLabel m_LabelTypeSel4;
	CLabel m_Label13;
	CLabel m_Label14;
	CLabel m_Label11;
	CLabel m_Label12;
	CLabel m_Label15;
	CLabel m_Label16;
	CLabel m_Label17;
	CLabel m_Label18;
	CLabel m_Label19;
	CLabel m_Label20;
	CLabel m_Label22;
	CLabel m_Label23;
	CLabel m_Label24;
	CLabel m_Label25;
	CLabel m_Label26;
	CLabel m_Label21;
	afx_msg void OnBnClickedBtnStopper1();
	afx_msg void OnBnClickedBtnStopper2();
	afx_msg void OnBnClickedBtnStopper3();
	afx_msg void OnBnClickedBtnStopper4();
	CColorButton2 m_Btn_MaintenanceMode;
	CColorButton2 m_Btn_LocalMode;
	CLabel m_Label27;
	afx_msg void OnBnClickedBtnStopperSetting();
	afx_msg void OnBnClickedBtnLatchClose();
	afx_msg void OnBnClickedBtnLatchOpen();
	afx_msg void OnBnClickedBtnClipClose();
	afx_msg void OnBnClickedBtnClipOpen();
	afx_msg void OnBnClickedBtnJigPlateForward();
	afx_msg void OnBnClickedBtnJigPlateBack();
	afx_msg void OnBnClickedBtnClipPlateUp();
	afx_msg void OnBnClickedBtnClipPlateDown();
	afx_msg void OnBnClickedBtnClipPlateForward();
	afx_msg void OnBnClickedBtnClipPlateBack();
	afx_msg void OnBnClickedBtnJigFanOn();
	afx_msg void OnBnClickedBtnJigFanOff();
	CColorButton2 m_BtnLatchClose;
	CColorButton2 m_BtnLatchOpen;
	CColorButton2 m_BtnClipClose;
	CColorButton2 m_BtnClipOpen;
	CColorButton2 m_BtnStopper1;
	CColorButton2 m_BtnStopper2;
	CColorButton2 m_BtnStopper3;
	CColorButton2 m_BtnStopper4;
	CColorButton2 m_BtnJigPlateForward;
	CColorButton2 m_BtnJigPlateback;
	CColorButton2 m_BtnClipPlateUp;
	CColorButton2 m_BtnClipPlateDown;
	CColorButton2 m_BtnClipPlateForward;
	CColorButton2 m_BtnClipPlateBack;
	CColorButton2 m_BtnJigFanOn;
	CColorButton2 m_BtnJigFanOff;
	CLabel m_LabelLeftStopper;
	CLabel m_LabelRightStopper;
	afx_msg void OnBnClickedBtnNormalSensor();
	CColorButton2 m_Btn_BuzzerOff;
	CColorButton2 m_Btn_EmergencyStop;
	afx_msg void OnBnClickedBuzzerOffBtn();
	afx_msg void OnBnClickedEmergencyStopBtn();
	CColorButton2 m_Btn_FmsReset1;
	CColorButton2 m_Btn_FmsReset2;
	CColorButton2 m_Btn_FmsEndCommandSend;
	afx_msg void OnBnClickedFmsEndCmdSendBtn();
	CLabel m_LabelLog;
	afx_msg void OnBnClickedFmsReset1Btn();
	afx_msg void OnBnClickedContinueBtn();
	CColorButton2 m_Btn_Continue;
	afx_msg void OnBnClickedFmsReset2Btn();
	afx_msg void OnBnClickedBtnTypeSelSetting();
};

#ifndef _DEBUG  // debug version in TopView.cpp
inline CCTSMonDoc* CManualControlView::GetDocument()
{ return (CCTSMonDoc*)m_pDocument; }
#endif


