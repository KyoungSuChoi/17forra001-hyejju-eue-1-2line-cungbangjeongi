// SensorMapDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ctsmon.h"
#include "SensorMapDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSensorMapDlg dialog


CSensorMapDlg::CSensorMapDlg(CCTSMonDoc *pDoc, CWnd* pParent /*=NULL*/)
	: CDialog(CSensorMapDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSensorMapDlg)
	m_bUseTempWarning = FALSE;
	m_nWaringRed = 0;
	m_nWaringYellow = 0;
	m_bUseTrayError = FALSE;
	m_sWarnTemp = 0;
	m_sDangerTemp = 0;
	m_bUseJigTemp = FALSE;
	m_nJigTargetTemp = 0;
	m_nJigTempInterval = 0;
	m_bUseJigTempWarning = FALSE;
	m_sWarnJigTemp = 0;
	m_sDangerJigTemp = 0;
	//}}AFX_DATA_INIT
	ASSERT(pDoc);
	m_nModuleID = 0;
	m_pDoc = pDoc;
	LanguageinitMonConfig();
}


CSensorMapDlg::~CSensorMapDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CSensorMapDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CSensorMapDlg"), _T("TEXT_CSensorMapDlg_CNT"), _T("TEXT_CSensorMapDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CSensorMapDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CSensorMapDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}


void CSensorMapDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSensorMapDlg)
	DDX_Control(pDX, IDOK, m_Ok_Btn);
	DDX_Control(pDX, IDCANCEL, m_Cancel_Btn);
	DDX_Control(pDX, IDC_APPLY_ALL_BUTTON, m_btnApplyAll);
	DDX_Control(pDX, IDC_MD_SEL_COMBO, m_ctrlModuleSel);
	DDX_Control(pDX, IDC_LAMP_GREEN_STATIC, m_ctrlGreenLamp);
	DDX_Control(pDX, IDC_LAMP_YELLOW_STATIC, m_ctrlYellowLamp);
	DDX_Control(pDX, IDC_LAMP_RED_STATIC, m_ctrlRedLamp);
	DDX_Check(pDX, IDC_USE_TEMP_WARNING_CHECK, m_bUseTempWarning);
	DDX_Text(pDX, IDC_LAMP_RED_EDIT, m_nWaringRed);
	DDX_Text(pDX, IDC_LAMP_YELLOW_EDIT, m_nWaringYellow);
	DDX_Check(pDX, IDC_USE_TRAY_ERROR_CHECK, m_bUseTrayError);
	DDX_Text(pDX, IDC_WARN_TEMP_EDIT, m_sWarnTemp);
	DDX_Text(pDX, IDC_DANGER_TEMP_EDIT, m_sDangerTemp);
	DDX_Check(pDX, IDC_USE_JIG_TEMP_CHECK, m_bUseJigTemp);
	DDX_Text(pDX, IDC_JIG_TARGET_TEMP_EDIT, m_nJigTargetTemp);
	DDX_Text(pDX, IDC_JIG_TEMP_INTERVAL_EDIT, m_nJigTempInterval);
	DDX_Check(pDX, IDC_USE_JIG_TEMP_WARNING_CHECK, m_bUseJigTempWarning);
	DDX_Text(pDX, IDC_WARN_JIG_TEMP_EDIT, m_sWarnJigTemp);
	DDX_Text(pDX, IDC_DANGER_JIG_TEMP_EDIT, m_sDangerJigTemp);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSensorMapDlg, CDialog)
	//{{AFX_MSG_MAP(CSensorMapDlg)
	ON_CBN_SELCHANGE(IDC_MD_SEL_COMBO, OnSelchangeMdSelCombo)
	ON_BN_CLICKED(IDC_APPLY_ALL_BUTTON, OnApplyAllButton)
	ON_BN_CLICKED(IDC_RADIO1, OnRadio1)
	ON_BN_CLICKED(IDC_RADIO2, OnRadio2)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSensorMapDlg message handlers

void CSensorMapDlg::InitMappingGrid()
{
	m_wndMapGrid.SubclassDlgItem(IDC_SENSOR_MAP_GRID, this);
	m_wndMapGrid.m_bSameRowSize = FALSE;
	m_wndMapGrid.m_bSameColSize = FALSE;
	m_wndMapGrid.m_bCustomWidth = TRUE;

	m_wndMapGrid.m_nWidth[1]	= 60;
	m_wndMapGrid.m_nWidth[2]	= 60;
	m_wndMapGrid.m_nWidth[3]	= 100;
	m_wndMapGrid.Initialize();
	BOOL bLock = m_wndMapGrid.LockUpdate();

	m_wndMapGrid.SetColCount(3);
	m_wndMapGrid.SetRowCount(EP_MAX_SENSOR_CH);
	m_wndMapGrid.SetScrollBarMode(SB_VERT, gxnEnabled | gxnEnhanced);	//Only Vertial Scroll Bar

//	m_wndMapGrid.m_bCustomColor 	= TRUE;
	m_wndMapGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);

	m_wndMapGrid.SetValueRange(CGXRange(0,1), TEXT_LANG[0]);//"센서번호"
	m_wndMapGrid.SetValueRange(CGXRange(0,2), TEXT_LANG[1]);//"모듈채널"
	m_wndMapGrid.SetValueRange(CGXRange(0,3), TEXT_LANG[2]);//"이름"

	m_wndMapGrid.SetDefaultRowHeight(18);
	m_wndMapGrid.SetStyleRange(CGXRange().SetCols(1), CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(255, 255, 255)));

	//Mapping Table display
	CString strType, strTemp;
	strType = "None\n";	
	int i = 0;
 	for(i=0; i<EP_DEFAULT_CH_PER_MD; i++)
 	{
 		strTemp.Format("%d\n", i+1);
 		strType += strTemp;
 	}
	m_wndMapGrid.SetStyleRange(CGXRange().SetCols(2),
			CGXStyle()
				.SetControl(GX_IDS_CTRL_ZEROBASED_EX)
				.SetHorizontalAlignment(DT_CENTER)
				.SetChoiceList(strType)
	);

	m_wndMapGrid.SetStyleRange(CGXRange().SetCols(3),
			CGXStyle()
				.SetControl(GX_IDS_CTRL_EDIT)
				.SetMaxLength(16)
	);

	for(i=0; i<EP_MAX_SENSOR_CH; i++)
	{
		strTemp.Format("%d\n", i+1);
		m_wndMapGrid.SetValueRange(CGXRange(i+1, 1), strTemp);
		strTemp.Format("T %d", i+1);
		m_wndMapGrid.SetValueRange(CGXRange(i+1, 3), strTemp);
	}

	((CButton*)GetDlgItem(IDC_RADIO1))->SetCheck(TRUE); 

	m_wndMapGrid.LockUpdate(bLock);
	m_wndMapGrid.Redraw();
}

BOOL CSensorMapDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	// TODO: Add extra initialization here

	m_ctrlGreenLamp.SetBkColor(RGB(0, 220, 0));
	m_ctrlYellowLamp.SetBkColor(RGB(220, 200, 0));
	m_ctrlRedLamp.SetBkColor(RGB(220, 0, 0));

	int nModID, nDefaultID = 0;
	for(int i=0; i<m_pDoc->GetInstalledModuleNum(); i++)
	{
		nModID = EPGetModuleID(i);
		m_ctrlModuleSel.AddString(::GetModuleName(nModID));
		m_ctrlModuleSel.SetItemData(i, nModID);
		if(nModID == m_nModuleID)	nDefaultID = i;
	}
	
	m_ctrlModuleSel.SetCurSel(nDefaultID);

/*	EP_SYSTEM_PARAM *lpSysParam = EPGetSysParam(m_nModuleID);
	if(lpSysParam != NULL)	
	{
		m_bUseTempWarning = lpSysParam->bUseTempLimit;
		m_lTempMax = ETC_PRECISION(lpSysParam->lMaxTemp);

		m_nWaringRed = lpSysParam->wWarningNo2;
		m_nWaringYellow = lpSysParam->wWarningNo1;
	}
*/
	InitMappingGrid();

	DisplayMapping(m_nModuleID);	
	
	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSensorMapDlg::OnSelchangeMdSelCombo() 
{
	// TODO: Add your control notification handler code here
	int nSel = m_ctrlModuleSel.GetCurSel();
	if(nSel == LB_ERR)	return;

	//이전 Module Update
	UpdateData(TRUE);
	if(ApplyToModule(m_nModuleID) == FALSE)
	{
		CString strTemp;
		strTemp.Format("%s Sensor mapping save fail!!!", ::GetModuleName(m_nModuleID));
		AfxMessageBox(strTemp);

	}
	m_nModuleID = m_ctrlModuleSel.GetItemData(nSel);
	DisplayMapping(m_nModuleID);
}

void CSensorMapDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);

	if(ApplyToModule(m_nModuleID) == FALSE)
	{
		CString strTemp;
		strTemp.Format("%s Sensor mapping save fail!!!", ::GetModuleName(m_nModuleID));
		AfxMessageBox(strTemp);
	}

	m_pDoc->SaveBfSensorMap();

	CDialog::OnOK();
}

void CSensorMapDlg::OnApplyAllButton() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

	if(MessageBox(TEXT_LANG[3], TEXT_LANG[4], MB_ICONQUESTION|MB_YESNO) == IDYES)//"현재 설정을 모든 모듈에 적용 하시겠습니까?"//"적용"
	{
		CString strTemp;
		m_pDoc->SetProgressWnd(0, m_pDoc->GetInstalledModuleNum(), "Sending sensormap data to module...");
		for(int i=1; i<= m_pDoc->GetInstalledModuleNum(); i++)
		{
			if(ApplyToModule(i) == FALSE)		//2008-12-01 ljb
			{
				strTemp.Format("%s Sensor mapping save fail!!!", ::GetModuleName(EPGetModuleID(i)));
				AfxMessageBox(strTemp);
			}
			m_pDoc->SetProgressPos(i);
		}

		m_pDoc->SaveBfSensorMap();
	}
	m_pDoc->HideProgressWnd();
	CDialog::OnOK();
}

BOOL CSensorMapDlg::ApplyToModule(int nModuleID)
{
	UpdateData(TRUE);
	CString strTemp;
	CFormModule *pModuleInfo = m_pDoc->GetModuleInfo(nModuleID);
	if(pModuleInfo == FALSE)	
		return FALSE;

	int nS = SENSOR1;
	if(((CButton*)GetDlgItem(IDC_RADIO2))->GetCheck())	
		nS = SENSOR2;

	UpdateMappingCh(nModuleID, nS);		// Mapping file 에 저장

	_MAPPING_DATA *pMapData;
	EP_SYSTEM_PARAM *lpSysParam = EPGetSysParam(nModuleID);
	if(lpSysParam != NULL)	
	{
		lpSysParam->bUseTrayError = m_bUseTrayError;
		lpSysParam->wWarningNo1 = (unsigned short)m_nWaringYellow;					// 경고 발생 갯수
		lpSysParam->wWarningNo2 = (unsigned short)m_nWaringRed;						// 수동모드전환 발생 갯수

		lpSysParam->bUseTempLimit = m_bUseTempWarning;
		lpSysParam->sWanningTemp = (unsigned short)ExpFloattoLong((float)m_sWarnTemp, EP_ETC_FLOAT);
		lpSysParam->sJigWanningTemp = (unsigned short)ExpFloattoLong((float)m_sWarnJigTemp, EP_ETC_FLOAT);

		lpSysParam->bUseJigTempLimit = m_bUseJigTempWarning;
		lpSysParam->sCutTemp = (unsigned short)ExpFloattoLong((float)m_sDangerTemp, EP_ETC_FLOAT);	
		lpSysParam->sJigCutTemp = (unsigned short)ExpFloattoLong((float)m_sDangerJigTemp, EP_ETC_FLOAT);	
	
		lpSysParam->bUseJigTargetTemp = m_bUseJigTemp;
		lpSysParam->nJigTargetTemp = m_nJigTargetTemp;
		lpSysParam->nJigTempInterval = m_nJigTempInterval;

		CDaoDatabase  db;
		CString strSQL = _T("");
		CString strData = _T("");
		CString strData2 = _T("");
		CString strData3 = _T("");

		int sysType = ((CCTSMonApp*)AfxGetApp())->GetSystemType();
		try
		{
			//Save setting to database
 			db.Open(GetDataBaseName());
			CString strSQL;

			strData.Format("%d, %d", m_sWarnTemp, m_sWarnJigTemp );

			strData2.Format("%d, %d", m_sDangerTemp, m_sDangerJigTemp );

			strData3.Format("%d %d %d", lpSysParam->wWarningNo1, lpSysParam->wWarningNo2, lpSysParam->bUseTrayError);
			
			strSQL.Format("UPDATE SystemConfig SET UseUnitLimitFlag = %d, WarnTemperature = '%s', UseJigLimitFlag = %d, DangerTemperature = '%s', Data1 = '%s', UseJigTargetTemp = %d, JigTargetTemp = %d, JigTargetTempInterval = %d WHERE ModuleID = %d", 
					m_bUseTempWarning, 					
					strData,
					m_bUseJigTempWarning,
					strData2,
					strData3,
					m_bUseJigTemp,
					m_nJigTargetTemp,
					m_nJigTempInterval,
					nModuleID);

			if(sysType != 0)
			{
				strTemp.Format(" AND ModuleType = %d", sysType);
				strSQL += strTemp;
			}				
			db.Execute(strSQL);
			db.Close();
		}
		catch(CDaoException *e)
		{
		  // Simply show an error message to the user.
			AfxMessageBox(e->m_pErrorInfo->m_strDescription);
			e->Delete();
			return FALSE;
		}
		//Send setting to module make network body
		EP_SENSOR_SET_DATA data;
		ZeroMemory(&data, sizeof(EP_SENSOR_SET_DATA));			

		if(pModuleInfo)
		{
			for(int j = 0; j<EP_MAX_SENSOR_CH; j++)
			{
				pMapData = pModuleInfo->GetSensorMap(SENSOR1, j);
				if(pMapData->nChannelNo >= 0)
				{
					data.sensorSet1.bUseSensorFlag[j] = TRUE;
				}
				pMapData = pModuleInfo->GetSensorMap(SENSOR2, j);
				if(pMapData->nChannelNo >= 0)
				{
					data.sensorSet2.bUseSensorFlag[j] = TRUE;
				}				
			}
		}

		// 1. Tray 작업 불량 경고 설정
		data.wWarningNo1 = lpSysParam->wWarningNo1;
		data.wWarningNo2 = lpSysParam->wWarningNo2;	
		data.bUseError = (unsigned char)lpSysParam->bUseTrayError;					//use CutTemp -> 수동모드전환

		// 2. 지그부 온도 설정
		data.sensorSet1.sWanningLimit = lpSysParam->sJigWanningTemp;					//Temperature Warnning
		data.sensorSet1.sCutLimit = lpSysParam->sJigCutTemp;							//Temperature CutTemp
		data.sensorSet1.bUseLimit = (unsigned char)lpSysParam->bUseJigTempLimit;		//use limit data

		// 3. 전원부 온도 설정
		data.sensorSet2.sWanningLimit = lpSysParam->sWanningTemp;
		data.sensorSet2.sCutLimit = lpSysParam->sCutTemp;
		data.sensorSet2.bUseLimit = (unsigned char)lpSysParam->bUseTempLimit;		//use limit data

		if(EPGetModuleState(nModuleID) != EP_STATE_LINE_OFF)
		{
			if(EPSendCommand(nModuleID, 1, 0, EP_CMD_SENSOR_LIMIT_SET, &data, sizeof(EP_SENSOR_SET_DATA))!= EP_ACK)
			{				
				strTemp.Format("%s :: Sensor Limit Data Send Fail.", ::GetModuleName(nModuleID));
				m_pDoc->WriteLog( strTemp );
			}
		}						
	}

	return TRUE;
}

void CSensorMapDlg::DisplayMapping(int nModuleID)
{
	CFormModule *pModuleInfo = m_pDoc->GetModuleInfo(nModuleID);
	
	//Mapping Table display
	CString strType, strTemp, strTemp1;
	int i = 0;
	strTemp = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Module Name", "Rack");	
	strType = "None\n";	
	strType += (strTemp + "\n");	
	strType += "Jig\n";
	for(i = 0; i<EPGetChInGroup(nModuleID); i++)
	{
		strTemp.Format("%d\n", i+1);
		strType += strTemp;
	}
	m_wndMapGrid.SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetChoiceList(strType));

	_MAPPING_DATA *pMapData;
	
	int nS = SENSOR1;
	if(((CButton*)GetDlgItem(IDC_RADIO2))->GetCheck())
		nS = SENSOR2; 

	for(i= 0; i<EP_MAX_SENSOR_CH; i++)
	{
		pMapData = pModuleInfo->GetSensorMap(nS, i);
		// strTemp.Format("%d", pMapData->nChannelNo+1);
		strTemp.Format("%d", pMapData->nChannelNo+1);	//ljb 온도 설정 수정 왜 2냐 0:None, 1:Module,2:Jig,3:channel(1 base)
		m_wndMapGrid.SetStyleRange(CGXRange(i+1, 2), CGXStyle().SetValue(strTemp));	
		m_wndMapGrid.SetStyleRange(CGXRange(i+1, 3), CGXStyle().SetValue(pMapData->szName));	
	}

	EP_SYSTEM_PARAM *lpSysParam = EPGetSysParam(nModuleID);
	if(lpSysParam != NULL)	
	{
		m_bUseTrayError = lpSysParam->bUseTrayError;
		m_nWaringYellow = lpSysParam->wWarningNo1;
		m_nWaringRed = lpSysParam->wWarningNo2;

		m_bUseTempWarning = lpSysParam->bUseTempLimit;	
		m_sWarnTemp = (unsigned int)ETC_PRECISION(lpSysParam->sWanningTemp);
		m_sDangerTemp = (unsigned int)ETC_PRECISION(lpSysParam->sCutTemp);

		m_bUseJigTempWarning = lpSysParam->bUseJigTempLimit;	
		m_sWarnJigTemp = (unsigned int)ETC_PRECISION(lpSysParam->sJigWanningTemp);
		m_sDangerJigTemp = (unsigned int)ETC_PRECISION(lpSysParam->sJigCutTemp);

		m_bUseJigTemp = lpSysParam->bUseJigTargetTemp;
		m_nJigTargetTemp = lpSysParam->nJigTargetTemp;
		m_nJigTempInterval = lpSysParam->nJigTempInterval;
	}	
	UpdateData(FALSE);
}

void CSensorMapDlg::OnRadio1() 
{
	// TODO: Add your control notification handler code here
	UpdateMappingCh(m_nModuleID, SENSOR2);
	DisplayMapping(m_nModuleID);	
}

void CSensorMapDlg::UpdateMappingCh(int nModuleID, int nSensor)
{
	CFormModule *pModuleInfo = m_pDoc->GetModuleInfo(nModuleID);
	if(pModuleInfo == FALSE)	
		return;

	CString strTemp;
	_MAPPING_DATA *pMapData;
	int nTemp;

	for(DWORD i=0; i<m_wndMapGrid.GetRowCount() && i<EP_MAX_SENSOR_CH; i++)
	{
		pMapData = pModuleInfo->GetSensorMap(nSensor, i);
		// nTemp = atol(m_wndMapGrid.GetValueRowCol(i+1, 2))-1;
		nTemp = atol(m_wndMapGrid.GetValueRowCol(i+1, 2))-1;
		pMapData->nChannelNo = (unsigned short)nTemp;		//-1 : Not Use, 0: Module,  1~ : 1 Base channel no
		sprintf(pMapData->szName, "%s", m_wndMapGrid.GetValueRowCol(i+1, 3));		
	}	
}

void CSensorMapDlg::OnRadio2()
{
	// TODO: Add your control notification handler code here
	UpdateMappingCh(m_nModuleID, SENSOR1);
	DisplayMapping(m_nModuleID);		
}
