#if !defined(AFX_WANNINGDLG_H__601E518F_1C2A_4B75_9AE2_DAFE52FDF2D1__INCLUDED_)
#define AFX_WANNINGDLG_H__601E518F_1C2A_4B75_9AE2_DAFE52FDF2D1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WanningDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CWanningDlg dialog

class CWanningDlg : public CDialog
{
// Construction
public:
	CWanningDlg(CWnd* pParent = NULL);   // standard constructor	

// Dialog Data
	//{{AFX_DATA(CWanningDlg)
	enum { IDD = IDD_DLG_WANNING };
	CString	m_strValue;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWanningDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CWanningDlg)
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WANNINGDLG_H__601E518F_1C2A_4B75_9AE2_DAFE52FDF2D1__INCLUDED_)
