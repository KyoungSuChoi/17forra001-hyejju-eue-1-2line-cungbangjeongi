// Step.cpp: implementation of the CStep class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Step.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CStep::CStep(int nStepType /*=0*/)
{
	ClearStepData();
	m_type = (BYTE)nStepType;
}

CStep::CStep()
{
	ClearStepData();
}

CStep::~CStep()
{

}

BOOL CStep::ClearStepData()
{
	m_type = 0;
	m_StepIndex = 0;
	m_mode = 0;
	m_bGrade = FALSE;
	m_lProcType = 0;				
	m_fVref = 0.0f;
    m_fIref = 0.0f;
	m_fTref = 0.0f;
    
	m_fEndTime = 0.0f;
    m_fEndV = 0.0f;
    m_fEndI = 0.0f;
    m_fEndC = 0.0f;
    m_fEndDV = 0.0f;
    m_fEndDI = 0.0f;
	m_fEndW = 0.0f;
	m_fEndWh = 0.0f;
	m_fEndT	= 0.0f;
	m_fStartT = 0.0f;

	m_bUseActualCapa = 0;					//현재 Step의 용량값을 기준 용량으로 사용 할지 여부 
	m_UseAutucalCapaStepNo = 0;					//비교할 기준 용량이 속한 Step번호
	m_fSocRate = 0.0f;						//비교 SOC Rate


	m_nLoopInfoGoto = 0;
	m_nLoopInfoEndVGoto	= 0;
	m_nLoopInfoEndTGoto	= 0;
	m_nLoopInfoEndCGoto	= 0;
	m_nLoopInfoCycle = 0;	
	m_nGotoStepID	= 0;

	m_fHighLimitV = 0.0f;
    m_fLowLimitV = 0.0f;
    m_fHighLimitI = 0.0f;
    m_fLowLimitI = 0.0f;
    m_fHighLimitC = 0.0f;
    m_fLowLimitC = 0.0f;
	m_fHighLimitImp = 0.0f;
    m_fLowLimitImp = 0.0f;
	m_fHighLimitTemp = 0.0f;
	m_fLowLimitTemp = 0.0f; 
	for(int i=0; i<PS_MAX_COMP_POINT; i++)
	{
		m_fCompTimeV[i] = 0.0f;			
		m_fCompHighV[i] = 0.0f;
		m_fCompLowV[i] = 0.0f;
		m_fCompTimeI[i] = 0.0f;			
		m_fCompHighI[i] = 0.0f;
		m_fCompLowI[i] = 0.0f;
	}
    m_fDeltaTimeV = 0.0f;
    m_fDeltaV = 0.0f;
    m_fDeltaTimeI = 0.0f;
    m_fDeltaI = 0.0f;

	m_fReportTemp = 0.0f;
	m_fReportV = 0.0f;
	m_fReportI = 0.0f;
	m_fReportTime = 0.0f;


	m_fCapaVoltage1		= 0.0f;	
	m_fCapaVoltage2		= 0.0f;	

	m_fDCRStartTime	= 0.0f;	
	m_fDCREndTime		=0.0f;

	m_fLCStartTime		=0.0f;	
	m_fLCEndTime		=0.0f;		

	m_lRange		=0;	
	
	m_strPatternFileName.Empty();
	m_fValueMax = 0.0f;
	m_fValueMin = 0.0f;


	m_Grading.ClearStep();
	m_StepID = 0;
	return TRUE;
}

void CStep::operator=(CStep &step) 
{
	m_type = step.m_type;
	m_StepIndex = step.m_StepIndex;
	m_mode = step.m_mode;
	m_bGrade = step.m_bGrade;
	m_lProcType = step.m_lProcType;				
	m_fVref = step.m_fVref;
    m_fIref = step.m_fIref;
    m_fTref = step.m_fTref;
	m_fStartT = step.m_fStartT;
   
	m_fEndTime = step.m_fEndTime;
    m_fEndV = step.m_fEndV;
    m_fEndI = step.m_fEndI;
    m_fEndC = step.m_fEndC;
    m_fEndDV = step.m_fEndDV;
    m_fEndDI = step.m_fEndDI;
	m_fEndW	 = step.m_fEndW;
	m_fEndWh = step.m_fEndWh;
	m_fEndT	= step.m_fEndT;

	m_nLoopInfoGoto = step.m_nLoopInfoGoto;	
	m_nLoopInfoEndVGoto = step.m_nLoopInfoEndVGoto;
	m_nLoopInfoEndTGoto = step.m_nLoopInfoEndTGoto;
	m_nLoopInfoEndCGoto = step.m_nLoopInfoEndCGoto;
	m_nGotoStepID		= step.m_nGotoStepID;

	m_nLoopInfoCycle = step.m_nLoopInfoCycle;	

	m_fHighLimitV = step.m_fHighLimitV;
    m_fLowLimitV = step.m_fLowLimitV;
    m_fHighLimitI = step.m_fHighLimitI;
    m_fLowLimitI = step.m_fLowLimitI;
    m_fHighLimitC = step.m_fHighLimitC;
    m_fLowLimitC = step.m_fLowLimitC;
	m_fHighLimitImp = step.m_fHighLimitImp;
    m_fLowLimitImp = step.m_fLowLimitImp;
	m_fHighLimitTemp = step.m_fHighLimitTemp;
	m_fLowLimitTemp = step.m_fLowLimitTemp;

	for(int i=0; i<PS_MAX_COMP_POINT; i++)
	{
		m_fCompTimeV[i] = step.m_fCompTimeV[i];			
		m_fCompHighV[i] = step.m_fCompHighV[i];
		m_fCompLowV[i] = step.m_fCompLowV[i];
		m_fCompTimeI[i] = step.m_fCompTimeI[i];			
		m_fCompHighI[i] = step.m_fCompHighI[i];
		m_fCompLowI[i] = step.m_fCompLowI[i];
	}
    m_fDeltaTimeV = step.m_fDeltaTimeV;
    m_fDeltaV = step.m_fDeltaV;
    m_fDeltaTimeI = step.m_fDeltaTimeI;
    m_fDeltaI = step.m_fDeltaI;

	m_fReportTemp = step.m_fReportTemp;
	m_fReportV	=	step.m_fReportV;
	m_fReportI	=	step.m_fReportI;
	m_fReportTime	=	step.m_fReportTime;

	m_fCapaVoltage1		=step.m_fCapaVoltage1;	
	m_fCapaVoltage2		=step.m_fCapaVoltage2;	

	m_fDCRStartTime	=step.m_fDCRStartTime;	
	m_fDCREndTime		=step.m_fDCREndTime;

	m_fLCStartTime		=step.m_fLCStartTime;	
	m_fLCEndTime		=step.m_fLCEndTime;		

	m_lRange		=step.m_lRange;	
	
	m_strPatternFileName = step.m_strPatternFileName;
	m_fValueMax = step.m_fValueMax;
	m_fValueMin = step.m_fValueMin;


	m_Grading.SetGradingData(step.m_Grading);

	m_StepID = step.m_StepID;

	GRADE_STEP data;
	m_Grading.ClearStep();
	for(i=0; i<step.m_Grading.GetGradeStepSize(); i++)
	{
		data = step.m_Grading.GetStepData(i);
		m_Grading.AddGradeStep(data.fMin, data.fMax, data.strCode);
	}
}

FILE_STEP_PARAM		CStep::GetFileStep()
{
	FILE_STEP_PARAM stepData;
	ZeroMemory(&stepData, sizeof(FILE_STEP_PARAM));

	stepData.chStepNo	= m_StepIndex;			
	stepData.nProcType	= m_lProcType;		
	stepData.chType		= m_type;
	stepData.chMode		= m_mode;
	stepData.fVref		= m_fVref;
	stepData.fIref		= m_fIref;
	stepData.fTref		= m_fTref;
	stepData.fStartT	= m_fStartT;
				
	stepData.fEndTime	= m_fEndTime;	
	stepData.fEndV		= m_fEndV;
	stepData.fEndI		= m_fEndI;
	stepData.fEndC		= m_fEndC;
	stepData.fEndDV		= m_fEndDV;		
	stepData.fEndDI		= m_fEndDI;	
	stepData.fEndW		= m_fEndW;
	stepData.fEndWh		= m_fEndWh;
	stepData.fEndT		= m_fEndT;

	stepData.fSocRate	= m_fSocRate ;						//비교 SOC Rate
	stepData.bUseActualCapa = m_bUseActualCapa	;			//현재 Step의 용량값을 기준 용량으로 사용 할지 여부 
	stepData.bUseDataStepNo = m_UseAutucalCapaStepNo ;		//비교할 기준 용량이 속한 Step번호

	stepData.nLoopInfoGoto		= m_nLoopInfoGoto;	
	stepData.nLoopInfoCycle		= m_nLoopInfoCycle;	
	stepData.nLoopInfoEndVGoto	= m_nLoopInfoEndVGoto;
	stepData.nLoopInfoEndTGoto	= m_nLoopInfoEndTGoto;
	stepData.nLoopInfoEndCGoto	= m_nLoopInfoEndCGoto;
	stepData.nGotoStepID	= m_nGotoStepID;

	stepData.fVLimitHigh	=	m_fHighLimitV;
	stepData.fVLimitLow		=	m_fLowLimitV;
	stepData.fILimitHigh	=	m_fHighLimitI;	
	stepData.fILimitLow		=	m_fLowLimitI;
	stepData.fCLimitHigh	=	m_fHighLimitC;
	stepData.fCLimitLow		=	m_fLowLimitC;
	stepData.fImpLimitHigh	=	m_fHighLimitImp;
	stepData.fImpLimitLow	=	m_fLowLimitImp;
	stepData.fHighLimitTemp = m_fHighLimitTemp;
	stepData.fLowLimitTemp = m_fLowLimitTemp;
	
	stepData.fDeltaTime		=	m_fDeltaTimeV; 	
	stepData.fDeltaTime1	=	m_fDeltaTimeI;	
	stepData.fDeltaV		=	m_fDeltaV;		
	stepData.fDeltaI		=	m_fDeltaI;

	stepData.bGrade			=	m_bGrade;
	stepData.sGrading_Val.chTotalGrade			= (BYTE)m_Grading.GetGradeStepSize();
	stepData.sGrading_Val.lGradeItem = m_Grading.m_lGradingItem;

	GRADE_STEP	grade_step;
	for(int i =0; i<m_Grading.GetGradeStepSize(); i++)
	{
		grade_step = m_Grading.GetStepData(i);
		stepData.sGrading_Val.faValue1[i]	=	grade_step.fMin;
		stepData.sGrading_Val.faValue2[i]	=	grade_step.fMax;
		stepData.sGrading_Val.aszGradeCode[i]	=	grade_step.strCode[0];
	}
	
	i=0;

	while(i<3 && i<PS_MAX_COMP_POINT)
	{
		stepData.fCompVLow[i]	=	m_fCompLowV[i];
		stepData.fCompVHigh[i]	=	m_fCompHighV[i];	
		stepData.fCompTimeV[i] =	m_fCompTimeV[i];
		stepData.fCompILow[i]	=	m_fCompLowI[i];
		stepData.fCompIHigh[i]	=	m_fCompHighI[i];
		stepData.fCompTimeI[i]	=	m_fCompTimeI[i];
		i++;
	}
	
	stepData.fReportTemp = m_fReportTemp;
	stepData.fReportV	=	m_fReportV;
	stepData.fReportI	=	m_fReportI;
	stepData.fReportTime	=	m_fReportTime;

	stepData.fCapaVoltage1 = m_fCapaVoltage1;	
	stepData.fCapaVoltage2 = m_fCapaVoltage2;	
	
	stepData.fDCRStartTime = m_fDCRStartTime;	
	stepData.fDCREndTime = m_fDCREndTime;
	
	stepData.fLCStartTime = m_fLCStartTime;	
	stepData.fLCEndTime = m_fLCEndTime;		

	stepData.lRange = m_lRange;		
	
	stepData.fPatternMinVal = m_fValueMin;
	stepData.fPatternMaxVal = m_fValueMax;
	sprintf(stepData.szSimulationFile, "%s", m_strPatternFileName);

	//m_StepID;

	return stepData;
}

//
void	CStep::SetStepData(FILE_STEP_PARAM *pStepData)
{
	ClearStepData();
	
	m_StepIndex		= pStepData->chStepNo	;
	m_lProcType		= pStepData->nProcType;	
	m_type			= pStepData->chType;		
	m_mode			= pStepData->chMode;		
	m_fVref			= pStepData->fVref;		
	m_fIref			= pStepData->fIref;	
	m_fTref			= pStepData->fTref;
	m_fStartT		= pStepData->fStartT;
				
	m_fEndTime			= pStepData->fEndTime;	
	m_fEndV				= pStepData->fEndV	;
	m_fEndI				= pStepData->fEndI	;
	m_fEndC				= pStepData->fEndC	;
	m_fEndDV			= pStepData->fEndDV	;		
	m_fEndDI			= pStepData->fEndDI	;
	m_fEndW				= pStepData->fEndW	;
	m_fEndWh			= pStepData->fEndWh	;
	m_fEndT				= pStepData->fEndT	;

	m_fSocRate			= pStepData->fSocRate;				//비교 SOC Rate
	m_bUseActualCapa	= pStepData->bUseActualCapa	;			//현재 Step의 용량값을 기준 용량으로 사용 할지 여부 
	m_UseAutucalCapaStepNo	= pStepData->bUseDataStepNo;			//비교할 기준 용량이 속한 Step번호

	m_nLoopInfoGoto		=	pStepData->nLoopInfoGoto;	
	m_nLoopInfoCycle	=	pStepData->nLoopInfoCycle;	
	m_nLoopInfoEndVGoto =	pStepData->nLoopInfoEndVGoto;
	m_nLoopInfoEndVGoto =	pStepData->nLoopInfoEndTGoto;
	m_nLoopInfoEndVGoto =	pStepData->nLoopInfoEndCGoto;
	m_nGotoStepID		=	pStepData->nGotoStepID;

	m_fHighLimitV	=	pStepData->fVLimitHigh	;
	m_fLowLimitV	=	pStepData->fVLimitLow		;
	m_fHighLimitI	=	pStepData->fILimitHigh	;	
	m_fLowLimitI	=	pStepData->fILimitLow		;	
	m_fHighLimitC	=	pStepData->fCLimitHigh	;	
	m_fLowLimitC	=	pStepData->fCLimitLow		;	
	m_fHighLimitImp	=	pStepData->fImpLimitHigh	;
	m_fLowLimitImp	=	pStepData->fImpLimitLow	;	
	m_fHighLimitTemp	=	pStepData->fHighLimitTemp	;
	m_fLowLimitTemp	=	pStepData->fLowLimitTemp	;	

	m_fDeltaTimeV	=	pStepData->fDeltaTime		; 	
	m_fDeltaTimeI	=	pStepData->fDeltaTime1	;	
	m_fDeltaV		=	pStepData->fDeltaV		;		
	m_fDeltaI		=	pStepData->fDeltaI		;

	m_bGrade = (BYTE)pStepData->bGrade;
	m_Grading.ClearStep();
	m_Grading.m_lGradingItem = pStepData->sGrading_Val.lGradeItem;

//	GRADE_STEP	grade_step;
	for(int i =0; i<pStepData->sGrading_Val.chTotalGrade; i++)
	{	
		m_Grading.AddGradeStep(pStepData->sGrading_Val.faValue1[i], pStepData->sGrading_Val.faValue2[i], pStepData->sGrading_Val.aszGradeCode[i]);
	}

	i=0;
	while(i<3 && i<PS_MAX_COMP_POINT)
	{
		m_fCompLowV[i]	=pStepData->fCompVLow[i]	;
		m_fCompHighV[i]	=pStepData->fCompVHigh[i]	;	
		m_fCompTimeV[i]	=pStepData->fCompTimeV[i] 	;
		m_fCompLowI[i]	=pStepData->fCompILow[i]	;
		m_fCompHighI[i]	=pStepData->fCompIHigh[i]	;
		m_fCompTimeI[i]	=pStepData->fCompTimeI[i]	;
		i++;
	}

	m_fReportTemp = pStepData->fReportTemp;
	m_fReportV = pStepData->fReportV;
	m_fReportI = pStepData->fReportI;
	m_fReportTime = pStepData->fReportTime;

	
	m_fCapaVoltage1	 = pStepData->fCapaVoltage1;	
	m_fCapaVoltage2	 = pStepData->fCapaVoltage2;	
	
	m_fDCRStartTime = pStepData->fDCRStartTime ;	
	m_fDCREndTime = pStepData->fDCREndTime ;
	
	m_fLCStartTime = pStepData->fLCStartTime ;	
	m_fLCEndTime = pStepData->fLCEndTime ;		

	m_lRange = pStepData->lRange;	
	
	m_fValueMax = pStepData->fPatternMaxVal;
	m_fValueMin = pStepData->fPatternMinVal;

	m_strPatternFileName = pStepData->szSimulationFile;
}
