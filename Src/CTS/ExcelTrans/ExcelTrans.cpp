// ExcelTrans.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "ExcelTrans.h"
#include "ExcelTransDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CExcelTransApp

BEGIN_MESSAGE_MAP(CExcelTransApp, CWinApp)
	//{{AFX_MSG_MAP(CExcelTransApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CExcelTransApp construction

CExcelTransApp::CExcelTransApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CExcelTransApp object

CExcelTransApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CExcelTransApp initialization

BOOL CExcelTransApp::InitInstance()
{
	AfxGetModuleState()->m_dwVersion = 0x0601;
	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	SetRegistryKey(_T("PNE CTS"));

	if( LanguageinitMonConfig() == false )
	{
		AfxMessageBox("Could not found [ ExcelTrans_Lang.ini ]");
		return false;
	}

	char szBuff[512], szBuff2[512];
	ZeroMemory(szBuff, sizeof(szBuff));
	ZeroMemory(szBuff2, sizeof(szBuff2));

	sscanf(m_lpCmdLine, "%s", szBuff);
	
	GetLongPathName(szBuff, szBuff2, 511);

	CString strFileName(szBuff2);
	CStringArray fileNaemArray;
	if(!strFileName.IsEmpty())
		fileNaemArray.Add(strFileName);

	CExcelTransDlg dlg;
	dlg.SetFileName(fileNaemArray);
	
	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

bool CExcelTransApp::LanguageinitMonConfig() 
{
	g_nLanguage = AfxGetApp()->GetProfileInt(REG_SECTION, "Language", 0);
	g_strLangPath.Format("%s\\Lang\\ExcelTrans_Lang.ini", GetProfileString(REG_SECTION ,"Path", "C:\Program Files\PNE CTS"));

	switch(g_nLanguage)
	{
	case 0:
		{
			SetThreadUILanguage( MAKELCID( MAKELANGID(LANG_KOREAN , SUBLANG_KOREAN) , SORT_DEFAULT));
			break;
		}

	case 1:
		{
			SetThreadUILanguage( MAKELCID( MAKELANGID(LANG_ENGLISH , SUBLANG_ENGLISH_US) , SORT_DEFAULT));
			break;
		}

	case 2:
		{
			SetThreadUILanguage( MAKELCID( MAKELANGID(LANG_CHINESE_SIMPLIFIED , SUBLANG_CHINESE_SIMPLIFIED) , SORT_DEFAULT));
			break;
		}
	}

	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CExcelTransApp"), _T("TEXT_CExcelTransApp_CNT"), _T("TEXT_CExcelTransApp_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt];

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CExcelTransApp_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CExcelTransApp"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Languge error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

int CExcelTransApp::ExitInstance()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}

	return CWinApp::ExitInstance();
}
