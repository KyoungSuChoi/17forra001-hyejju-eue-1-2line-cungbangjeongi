///////////////////////////////////////
//
//		ADPOWER Battery Charge/Discharge System
//		Base define file
//
///////////////////////////////////////

#ifndef _ADPOWER_BASE_DEFINE_INCLUDE_H_
#define _ADPOWER_BASE_DEFINE_INCLUDE_H_

//#define _EDLC_TEST_SYSTEM

//Formation과 같이 사용되므로 Index를 변경하면 안됨 
#define		PS_MAX_STEP					100			//Max Step 	
#define		PS_MAX_SUB_STEP				10			//Max Sub Step
#define		PS_MAX_GRADE_STEP			10			//Max Grade Step
#define		PS_MAX_STEP_GRADE			4			//Max Grade item In One Step
#define		PS_MAX_COMP_POINT			3			//Voltage Compation Point
#define		PS_MAX_DELTA_POINT			3			//Delta Value Check Point

//Step Type 정의
//Step Type은 Max 31이하로 정의 하여야 한다.
#define		PS_STEP_NONE			0x00
#define		PS_STEP_CHARGE			0x01
#define		PS_STEP_DISCHARGE		0x02
#define		PS_STEP_REST			0x03
#define		PS_STEP_OCV				0x04
#define		PS_STEP_IMPEDANCE		0x05
#define		PS_STEP_END				0x06
#define		PS_STEP_ADV_CYCLE		0x07
#define		PS_STEP_LOOP			0x08
#define		PS_STEP_LONG_REST		0x09		
#define		PS_STEP_PATTERN			0x09		//2006/03/24 사용자가 입력한 pattern data를 실행
#define		PS_STEP_BALANCE			0x0A		//20071102		선택된 Channel의 전압을 맞춘다.
#define		PS_STEP_CHECK			0x0B


//Step의 Mode
#define		PS_MODE_CCCV			0x01
#define		PS_MODE_CC				0x02
#define		PS_MODE_CV				0x03
#define		PS_MODE_DCIMP			0x04
#define		PS_MODE_ACIMP			0x05
#define		PS_MODE_CP				0x06
#define		PS_MODE_PULSE			0x07
#define		PS_MODE_CR				0x08

#define		PS_GOTO_NEXT_CYC		0x00

#define		PS_CHARGE_MODE_STRING1		"CC/CV\nCC\nCV\nCP\nCR"
#define		PS_CHARGE_MODE_STRING2		"CC/CV\nCC\nCV\nCP"
#define		PS_CHARGE_MODE_STRING3		"CC/CV\nCC\nCV"
#define		PS_IMPEDANCE_MODE_STRING	"DC\nAC"

//Range
#define		PS_CURRENT_RANGE_HIGH		0
#define		PS_CURRENT_RANGE_LOW		1
#define		PS_CURRENT_RANGE_MICRO		2
#define		PS_RANGE1					0
#define		PS_RANGE2					1
#define		PS_RANGE3					2

#define		PS_SCHEDULE_DATABASE_NAME		"Schedule.mdb"

#define PMS_SUPERVISOR				0xFFFFFFFF		//모든 권한
#define PMS_MODULE_SHUT_DOWN		0x00000001		//모듈 종료 (Power Off)
#define PMS_MODULE_CAL_UPDATE		0x00000002		//보정 파일 Update
#define PMS_GROUP_CONTROL_CMD		0x00000004		//Control Command
#define PMS_TEST_CONDITION_SEND		0x00000008		//시험 조건 전송
#define PMS_FORM_START				0x00000010		//PowerForm 시작
#define PMS_FORM_CLOSE				0x00000020		//Powerform 종료
#define PMS_USER_SETTING_CHANGE		0x00000040		//사용자 설정 변경
#define PMS_MODULE_ADD_DELETE		0x00000080		//모듈 추가 삭제 
#define PMS_EDITOR_START			0x00000100		//시험 조건 편집기 시작
#define PMS_EDITOR_EDIT				0x00000200		//시험 조건 편집 
#define PMS_TRAY_REG				0x00000400		//Tray 등록
#define PMS_MODULE_MODE_CHANGE		0x00000800

#define PS_USER_SUPER		PMS_SUPERVISOR
#define PS_USER_ADMIN		PMS_SUPERVISOR
#define PS_USER_OPERATOR	(PMS_GROUP_CONTROL_CMD|PMS_TEST_CONDITION_SEND|PMS_FORM_START|PMS_FORM_CLOSE|PMS_EDITOR_START)
#define PS_USER_GUEST		(PMS_FORM_START|PMS_EDITOR_START|PMS_FORM_CLOSE)


#define		PS_MAX_FILE_SAVE_ITEM_NUM		24
#define		PS_MAX_ITEM_NUM					36		// 33->36 2009_02_12 kky WattHour. Sum, Char. WattHour, DisChar. WattHour
#define		PS_MAX_FILE_ITEM_NUM			32

//각 data 항목의 Index
#define		PS_STATE			0x00
#define		PS_VOLTAGE			0x01
#define		PS_CURRENT			0x02
#define		PS_CAPACITY			0x03
#define		PS_IMPEDANCE		0x04
#define		PS_CODE				0x05
#define		PS_STEP_TIME		0x06
#define		PS_TOT_TIME			0x07
#define		PS_GRADE_CODE		0x08
#define		PS_STEP_NO			0x09
#define		PS_WATT				0x0A
#define		PS_WATT_HOUR		0x0B
#define		PS_TEMPERATURE		0x0C
#define		PS_PRESSURE			0x0D
#define		PS_STEP_TYPE		0x0E
#define		PS_CUR_CYCLE		0x0F
#define		PS_TOT_CYCLE		0x10
#define		PS_TEST_NAME		0x11
#define		PS_SCHEDULE_NAME	0x12
#define		PS_CHANNEL_NO		0x13
#define		PS_MODULE_NO		0x14
#define		PS_LOT_NO			0x15
#define		PS_DATA_SEQ			0x16
#define		PS_AVG_CURRENT		0x17
#define		PS_AVG_VOLTAGE		0x18
#define		PS_CAPACITY_SUM		0x19  
#define		PS_CHARGE_CAP		0x1a  
#define		PS_DISCHARGE_CAP	0x1b
#define		PS_METER_DATA		0x1c
#define		PS_START_TIME		0x1d
#define		PS_END_TIME			0x1e
#define		PS_SHARING_INFO		0x1f
#define		PS_GOTO_COUNT		0x20		// 20081202 KHS
#define		PS_WATTHOUR_SUM		0x21		// 2009_02_12 kky
#define		PS_CHAR_WATTHOUR	0x22		// 2009_02_12 kky
#define		PS_DISCHAR_WATTHOUR	0x23		// 2009_02_12 kky

//Grading 항목 정의 
#define		PS_NONE					0
#define		PS_GRADE_VOLTAGE		1	
#define		PS_GRADE_CAPACITY		2
#define		PS_GRADE_IMPEDANCE		3
#define		PS_GRADE_CURRENT		4	
#define		PS_GRADE_TIME			5
#define		PS_GRADE_FARAD			6

//Channel State
#define PS_STATE_IDLE			0x0000
#define PS_STATE_STANDBY		0x0001
#define PS_STATE_RUN			0x0002
#define PS_STATE_PAUSE			0x0003
#define PS_STATE_MAINTENANCE	0x0004

//Run sub state(Step Type)
#define PS_STATE_OCV			0x0010
#define PS_STATE_CHARGE			0x0011
#define PS_STATE_DISCHARGE		0x0012
#define PS_STATE_REST			0x0013
#define PS_STATE_IMPEDANCE		0x0014
#define PS_STATE_CHANGING		0x0015
#define PS_STATE_CHECK			0x0016

//PC Part state
#define PS_STATE_LINE_OFF		0x0020
#define PS_STATE_LINE_ON		0x0021
#define PS_STATE_EMERGENCY		0x0022
#define PS_STATE_SELF_TEST		0x0023
#define PS_STATE_FAIL			0x0024
#define PS_STATE_STOP			0x0025
#define PS_STATE_END			0x0026
#define PS_STATE_FAULT			0x0027
#define PS_STATE_NONCELL		0x0028
#define PS_STATE_READY			0x0029
#define PS_STATE_ERROR			0xFFFF

//////////////////////////////////////////////////////////////////////////

typedef struct floatData
{	
	float x; 
	float y; 
} fltPoint;

//define float point Num
#define PS_VTG_FLOAT	3
#define PS_CRT_FLOAT	3
#define PS_ETC_FLOAT	3
#define PS_VTG_EXP		1000.0
#define PS_CRT_EXP		1000.0
#define PS_ETC_EXP		1000.0

#define LONG2FLOAT(x)		(x == 0 ? 0.0f : float((double)x/PS_VTG_EXP))
#define FLOAT2LONG(x)		(x == 0.0 ? 0 : long( x < 0 ? ((double)x*PS_VTG_EXP-0.5) : ((double)x*PS_VTG_EXP+0.5)))

#define MAKE2LONG(x)		(x == 0.0 ? 0 : long( x < 0 ? (double)x-0.5 : (double)x+0.5))

#define PCTIME(x)			(x == 0 ? 0.0f : float((double)x/100.0f))			//sec 단위로 변경
#define MDTIME(x)			(x == 0.0f ? 0 : ULONG(MAKE2LONG((double)x*100.0f)))			//10m sec 단위로 변경

#define PS_EMG_NONE				0
#define PS_EMG_AC_FAIL			1
#define PS_EMG_UPS_BAT_FAIL		2
#define PS_EMG_MAIN_EMG_SWITCH	3
#define PS_EMG_SUB_EMG_SWITCH	4
#define PS_EMG_SMPS_FAIL		5
#define PS_EMG_OT_FAIL			6
#define PS_EMG_POWER_SWITCH_RUN	7

//progress ==> Scheudle.mdb의 TestType에 사용됨 
#define PS_PGS_NOWORK		0x00
#define PS_PGS_PREIMPEDANCE	0x01		//PI
#define	PS_PGS_PRECHARGE	0x02		//PC
#define PS_PGS_FORMATION	0x03		//FO
#define PS_PGS_IROCV		0x04		//IR
#define PS_PGS_FINALCHARGE	0x05		//FC
#define PS_PGS_AGING		0x06		//AG
#define	PS_PGS_GRADING		0x07		//GD
#define PS_PGS_SELECTOR		0x08		//SL
#define PS_PGS_OCV			0x09		//OV

#define PS_PGS_PREIMPEDANCE_CODE	"PI"
#define	PS_PGS_PRECHARGE_CODE		"PC"
#define PS_PGS_FORMATION_CODE		"FO"
#define PS_PGS_IROCV_CODE			"IR"
#define PS_PGS_FINALCHARGE_CODE		"FC"
#define PS_PGS_AGING_CODE			"AG"
#define	PS_PGS_GRADING_CODE			"GD"
#define PS_PGS_SELECTOR_CODE		"SL"
#define PS_PGS_OCV_CODE				"OV"

#define PS_REGISTRY_CTSMONPRO_NAME		 "SOFTWARE\\ADP CTSPro\\CTSMonPro"
#endif	//_ADPOWER_BASE_DEFINE_INCLUDE_H_


