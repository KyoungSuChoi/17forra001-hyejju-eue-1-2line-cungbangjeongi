//#include <setupapi.h>

#ifndef DEL_RELEASE
	#define DEL_RELEASE(x) { if (x) delete x; x = NULL; }
#endif

class GStr 
{
public:   
	GStr();
    ~GStr();
    	/*
	static LPTSTR str_GetLptstr(CString str)
	{//1		
		//#ifdef _UNICODE	
			//BSTR  sbstr=str.AllocSysString();
			//LPTSTR slptstr=W2T((LPWSTR)sbstr);
			//return slptstr;
		//#endif

		return (LPTSTR)(LPCTSTR)str;		
	}

	static char* str_All(CString data)
	{//2	
		#ifdef _UNICODE			
			int isize = WideCharToMultiByte(CP_ACP, 0, data, -1, 0, 0, 0, 0)+1;
			 
			char *buf=new char[isize];
			::memset(buf,0,isize);//////////////////
			int iret=WideCharToMultiByte(CP_ACP,0,data,data.GetLength(),buf,isize,NULL,NULL);
						
			return buf;
		#else
			return (LPSTR)(LPCTSTR)data;
		#endif
	}

	static int str_strToInt(CString str)
	{
		return _ttoi(str);
	}
	
	static LPCWSTR str_CharToWChar(char* mbString)
	{
		int len = 0;
		len = (int)strlen(mbString) + 1;
		wchar_t *ucString = new wchar_t[len];
		mbstowcs(ucString, mbString, len);
		return (LPCWSTR)ucString;
	}*/

	static CString str_UnicodeConvert(CString sUnicode)
	{			
		int iLen = MultiByteToWideChar(CP_ACP, 0, (LPSTR)(LPCTSTR)sUnicode, sUnicode.GetLength()*2, NULL, NULL);
		
		BSTR sResult = SysAllocStringLen(NULL, iLen);
		MultiByteToWideChar(CP_ACP, 0, (LPSTR)(LPCTSTR)sUnicode, sUnicode.GetLength()*2, sResult, iLen);
		
		CString str=(CString)sResult;
		SysFreeString(sResult); //메모리 해제.

		return str;		
	}

	/*static CString str_Decrypt(CString data)
	{//3 텍스트 복호화
		if(data.IsEmpty() || data==_T("")) return _T("");
		int len=data.GetLength();

		char *buf=new char[len];
		memcpy(buf,data,len);//1증가
    		
		data=_T("");	
		for(int i = 0; i <len; i++){// 복호화
			
			buf[i] ^= 7598;
			
			CString temp=_T("");
			temp.Format(_T("%c"),buf[i]);								   											
			data=data+temp;				
		}  
		return data;		
	}

	static CString str_Encrypt(CString data)
	{//4 문자열 암호화
		if(data.IsEmpty() || data==_T("")) return _T("");

		int len=data.GetLength();
		char *buf=new char[len];
		memcpy(buf,data,len);//1증가
		
		for(int i = 0; i < len; i++){//암호화
			buf[i]^=7598;	
		}
		return (CString)buf;
	}*/
	
	static CString str_Encrypt2(CString data)
	{//5 문자열 암호화
		if(data.IsEmpty() || data==_T("")) return _T("");
		
		int len=data.GetLength();
		BYTE *buf=str_getByte(data,len);//len 자리

		int i=0;
			
		for(i=0;i<len;i++)
		{	 		 			
			buf[i]^=211;
		}		

		data=_T("");		
		for(i=0;i<len;i++)
		{
			CString temp=_T("");
		    temp.Format(_T("%02x"),buf[i]);	
			data=data+temp;
		}		
		delete buf;
		return data;
	}

	static CString str_Decrypt2(CString data)
	{//6 문자열 암호화
		if(data.IsEmpty() || data==_T("")) return _T("");
		
		int len=data.GetLength()/2;

		CString sdata=_T("");
		int i=0;
		for(i = 0; i < len; i++)//암호화
		{
			CString temp=data.Mid((i*2),2);	
			
			BYTE byHigh = str_Code2Ascii((char)temp[0]);
			BYTE byLow =  str_Code2Ascii((char)temp[1]);
				
			char buf=(char)(byHigh*16+byLow);
			buf^=211;
 						
			temp.Format(_T("%c"),buf);			
			sdata=sdata+temp;				
		}		
		return sdata;
	}

	/*static BOOL str_ChkHangul(CString value)
	{//7 한글이 있는지 체크
		if(value==_T("")) return FALSE;

		for (int i = 0 ; i < value.GetLength(); i++)
		{
			if( ((value[i] & 0x80) == 0x80)){
				return TRUE;
			}
		}
		return FALSE;
	} 

	static BOOL str_ChkEncrypt(CString data)
	{//8 텍스트 복호화 체크
		if(data.IsEmpty() || data==_T("")) return FALSE;
		int len=data.GetLength();

		char *buf=new char[len];
		memcpy(buf,data,len+1);//1증가
    
		buf[0] ^= 7598;
    		
		data=_T("");
		data.Format(_T("%d"),data[0]);

		if(_ttoi(data)<0) return TRUE;
		else return FALSE;
	}*/
		
	static CString str_BTrim(CString str)
	{//9 공백 없애기
		str.TrimLeft();
		str.TrimRight();

		return str;
	}

	static int str_Count(CString tmpstr,CString searchstr)
	{//10 문자열에 특정 문자 갯수	
		int tcount=0;
		int dcount=-1;
		int i=0;

		for(i=0; i<tmpstr.GetLength();i++)
		{
			dcount=tmpstr.Find(searchstr,dcount+1);
			if(dcount>-1) ++tcount;
			else if(dcount==-1) break;
		}	
		return tcount;
	}

	static CString str_GetSplit(CString str,int count,char sp,BOOL bTrim=TRUE)
	{//11 문자기준으로 문자열 쪽개기
		if(str.IsEmpty()) return _T("");
		int item=str_Count(str,(CString)sp)+1;

		if(item<count) return _T("");

		CString tmp;
		AfxExtractSubString(tmp,str,count,sp);

		if(bTrim==TRUE)	return str_BTrim(tmp);

		return tmp;
	}

	static CString str_Replace( CString tmpstr, CString strVal, TCHAR searchSep )
	{
		int nCnt = 0;
		CString strTemp;
		strTemp.Format("%c", searchSep);
		nCnt = GStr::str_Count(tmpstr , strTemp );

		if( nCnt != 0 )
		{
			CString *pStr;
			pStr = new CString[nCnt+1];

			CString strRtn;

			int i = 0;
			BOOL bRtn = FALSE;
			for( i=0; i<nCnt; i++ )
			{
				bRtn = AfxExtractSubString( pStr[i], tmpstr, i, searchSep);
				strRtn += pStr[i];
				strRtn += strVal;
			}

			AfxExtractSubString( pStr[i], tmpstr, i, searchSep);
			strRtn += pStr[i];
			delete[] pStr;
			return strRtn;
		}

		return _T("");		
	}

	static CString str_BeforeSplit(CString tmpstr,int item,CString searchstr)
	{//12 문자열을 특정 문자 기준으로 앞쪽에 있는것만 가져오기
		int tcount=0;
		int dcount=-1;
		int i=0;

		for(i=0; i<tmpstr.GetLength();i++)
		{
			dcount=tmpstr.Find(searchstr,dcount+1);
			if(dcount>-1) ++tcount;
			else if(dcount==-1) break;

			if(tcount==item)
			{
			   tmpstr.Delete(dcount,tmpstr.GetLength()-dcount);
			   return tmpstr;
			}
		}	
		return _T("");
	}

	static CString str_AfterSplit(CString tmpstr,int item,CString searchstr)
	{//13 문자열을 특정 문자 기준으로 뒷쪽에 있는것만 가져오기	
		int tcount=0;
		int dcount=-1;
		int i=0;

		for(i=0; i<tmpstr.GetLength();i++)
		{
			dcount=tmpstr.Find(searchstr,dcount+1);
			if(dcount>-1) ++tcount;
			else if(dcount==-1) break;

			if(tcount==item)
			{
			   tmpstr.Delete(0,dcount+1);
			   return tmpstr;
			}
		}	
		return _T("");
	}

	static CString str_IntToStr(int num,int pt=1)
	{//14 자릿수 
		CString str=_T("");

		if(pt<2)  str.Format(_T("%d"),num);
		if(pt==2) str.Format(_T("%02d"),num);
		if(pt==3) str.Format(_T("%03d"),num);
		if(pt==4) str.Format(_T("%04d"),num);
		if(pt==5) str.Format(_T("%05d"),num);
		if(pt==6) str.Format(_T("%06d"),num);
		
		return str;
	}

	static CString str_Blank(CString sKey,int cnt,CString sValue=_T(" "))
	{//15 blank 자릿수 
		if(cnt<=sKey.GetLength()) return sKey;

		CString sBlank=_T("");
		int i=0;

		for(i=0;i<cnt-sKey.GetLength();i++) sBlank=sBlank+_T(" ");

		if(sValue!=_T("")) return sKey+sBlank+sValue;

		return sKey+sBlank;
	}

	/*static CString str_BlankA(CString sKey,int cnt,CString sValue=_T(" "))
	{//15 blank 자릿수 
		if(cnt<=sKey.GetLength()) return sKey;

		CString sBlank=_T("");
		for(int i=0;i<cnt-sKey.GetLength();i++) sBlank=sBlank+sValue;

	
		return sKey+sBlank;
	}


	static CString str_BlankDigit(CString sKey,int cnt,CString sValue=_T(" "))
	{//16 blank 자릿수 
		//if(cnt<=sKey.GetLength()) return sKey;

		CString sBlank=_T("");
		for(int i=0;i<cnt-sKey.GetLength();i++) sBlank=sBlank+sValue;

		return sKey+sBlank;		
	}

	static CString str_GetHttp(CString surl)
	{//17 http:// 붙이기
		if(surl.IsEmpty() || surl==_T("")) return surl;
		
		if(surl.Find(_T("http://"))<0 && surl.Find(_T("https://"))<0)  surl=_T("http://")+surl;

		return surl;
	}

	static TCHAR* str_DecodingUrl(const TCHAR *pszUrl, int nSize=1024)
	{//18 URL => 한글
		TCHAR *pszDecodedUrl=new TCHAR[nSize];	
		int iUrlLen=lstrlen(pszUrl); 
		
		if(!iUrlLen) return pszDecodedUrl;
					           
		#if defined(_UNICODE) || defined(UNICODE)
			int nSrcAllocSize=WideCharToMultiByte(CP_ACP,WC_COMPOSITECHECK,pszUrl,-1,0,0,0,0);
			char *pszMBSrc=(char*)calloc(nSrcAllocSize, sizeof(char));
			WideCharToMultiByte(CP_ACP,WC_COMPOSITECHECK,pszUrl,-1,pszMBSrc,nSrcAllocSize,0,0);
			const char *ptr=(const char*)pszMBSrc;
			iUrlLen=lstrlenA(pszMBSrc); // -A(안시)주의: 멀티바이트 스트링의 문자열 길이를 구한다.
		#else
			const char *pszMBSrc=pszUrl;
			const char *ptr=pszMBSrc; 
		#endif

		int nDecodedCount=0;
		while(ptr<pszMBSrc+iUrlLen-2)
		{
			if(ptr[0]=='%'&& 
			  ((ptr[1]>='0' && ptr[1]<='9') || (ptr[1]>='A' && ptr[1]<='F') || (ptr[1]>='a' && ptr[1]<='f')) &&
			  ((ptr[2]>='0' && ptr[2]<='9') || (ptr[2]>='A' && ptr[2]<='F') || (ptr[2]>='a' && ptr[2]<='f'))){
				nDecodedCount++; ptr+=3;
			} else ptr++;
		}
		int nDecodedLen=iUrlLen-(nDecodedCount<<1);
		int iAllocSize=(nDecodedLen+1)*sizeof(TCHAR);
    
		if(!pszDecodedUrl)
		{
			#if defined(_UNICODE) || defined(UNICODE)
				free(pszMBSrc);
			#endif
			return pszDecodedUrl; //iAllocSize
		}
		#if defined(_UNICODE) || defined(UNICODE)
			byte *pszMBDecoded=(byte*)calloc(iAllocSize/sizeof(TCHAR), sizeof(byte));
			byte *pDecode=pszMBDecoded;
		#else
				byte *pDecode=(byte*)pszDecodedUrl;
		#endif
		ptr=pszMBSrc;
				
		while(ptr<pszMBSrc+iUrlLen)
		{
			if(ptr[0]=='%'&& 
			  ((ptr[1]>='0' && ptr[1]<='9') || (ptr[1]>='A' && ptr[1]<='F') || (ptr[1]>='a' && ptr[1]<='f')) &&
			  ((ptr[2]>='0' && ptr[2]<='9') || (ptr[2]>='A' && ptr[2]<='F') || (ptr[2]>='a' && ptr[2]<='f'))){
				if(ptr[1]>='0' && ptr[1]<='9'){*pDecode=((byte)ptr[1]-48)<<4;}
				else if(ptr[1]>='A' && ptr[1]<='F'){*pDecode=((byte)ptr[1]-55)<<4;}
				else {*pDecode=((byte)ptr[1]-87)<<4;}

				if(ptr[2]>='0' && ptr[2]<='9'){*pDecode+=(byte)ptr[2]-48;}
				else if(ptr[2]>='A' && ptr[2]<='F'){*pDecode+=(byte)ptr[2]-55;}
				else {*pDecode+=(byte)ptr[2]-87;}       
				pDecode++; ptr+=3;
			} else {
				if(*ptr=='+'){*pDecode=' ';}
				else {*pDecode=*(byte*)ptr;}
				pDecode++; ptr++;
			}
		}		
		*pDecode=0; //널 종료
		
		#if defined(_UNICODE) || defined(UNICODE)
			MultiByteToWideChar(CP_ACP,0,(char*)pszMBDecoded,-1,pszDecodedUrl,iAllocSize);
			free(pszMBDecoded);
			free(pszMBSrc);
		#endif
		
		return pszDecodedUrl;//iAllocSize
	}

	static TCHAR* str_EncodingUrl(const TCHAR *pszUrl)
	{//19 한글=>URL
		TCHAR *pszEncodedUrl=new TCHAR[1024];
		int iUrlLen=lstrlen(pszUrl); 
		if(!iUrlLen) return 0;

		#if defined(_UNICODE) || defined(UNICODE) // 안시코드로 변환
			int iSrcAllocSize=WideCharToMultiByte(CP_ACP,WC_COMPOSITECHECK,pszUrl,-1,0,0,0,0);
			char *pszMBSrc=(char*)calloc(iSrcAllocSize, sizeof(char));
			WideCharToMultiByte(CP_ACP,WC_COMPOSITECHECK,pszUrl,-1,pszMBSrc,iSrcAllocSize,0,0);
			const char *ptr=(const char*)pszMBSrc;
			iUrlLen=lstrlenA(pszMBSrc); // -A(안시)주의
		#else
			const char *pszMBSrc=pszUrl;
			const char *ptr=pszMBSrc; 
		#endif

		// 아래 허용 특정문자셋(변경시 아래 문자셋 수정하면 됨)
		const char szPermittedChar[]="/?:@&=$,<>#%\"-_.!~*\'(){}|\\^[]";
		int iEncodedCount=0;
		while(ptr<pszMBSrc+iUrlLen){
			if(!(*ptr==' ' || (*ptr>='0' && *ptr<='9') || (*ptr>='A' && *ptr<='Z') ||
				(*ptr>='a' && *ptr<='z') || strchr(szPermittedChar, *ptr))){
				iEncodedCount++;
			}
			ptr++;
		}
		int iEncodedLen=iUrlLen+(iEncodedCount<<1);
		int iAllocSize=(iEncodedLen+1)*sizeof(TCHAR);
		if(!pszEncodedUrl){
			#if defined(_UNICODE) || defined(UNICODE)
					free(pszMBSrc);
			#endif
			return pszEncodedUrl; //iAllocSize
		}

		#if defined(_UNICODE) || defined(UNICODE)
			byte *pszMBEncoded=(byte*)calloc(iAllocSize/sizeof(TCHAR), sizeof(byte));
			byte *pEncode=pszMBEncoded;
		#else
			byte *pEncode=(byte*)pszEncodedUrl;
		#endif

		ptr=pszMBSrc;
		while(ptr<pszMBSrc+iUrlLen)
		{
			if(*ptr==' '){*pEncode='+';}
			else if((*ptr>='0' && *ptr<='9') || (*ptr>='A' && *ptr<='Z') ||
				(*ptr>='a' && *ptr<='z') || strchr(szPermittedChar, *ptr)){
				*pEncode=*ptr;
			} else {
				*pEncode='%'; ++pEncode;
				if((*(byte*)ptr>>4)>=0 && (*(byte*)ptr>>4)<=9){*pEncode=(*(byte*)ptr>>4)+48;}
				else{*pEncode=(*(byte*)ptr>>4)+55;}
				++pEncode;
				if((*(byte*)ptr&0xf)>=0 && (*(byte*)ptr&0xf)<=9){*pEncode=(*(byte*)ptr&0xf)+48;}
				else{*pEncode=(*(byte*)ptr&0xf)+55;}
			}
			++pEncode, ++ptr;
		}
		*pEncode=0; // 널종료

		#if defined(_UNICODE) || defined(UNICODE)
			MultiByteToWideChar(CP_ACP,0,(char*)pszMBEncoded,-1,pszEncodedUrl,iAllocSize);
			free(pszMBEncoded);
			free(pszMBSrc);
		#endif

		return pszEncodedUrl; //iAllocSize
	}
		
	static CString str_DecodingUtf(CString value,BOOL bflag=TRUE)
	{//20 UTF-8		  	
		TCHAR *pszDecodedUrl=str_DecodingUrl(value, value.GetLength());//Decoding  

		LPWSTR t=NULL;
		TCHAR *dest=NULL;		

		//UTF-8		
		#ifdef _UNICODE			   
			int need=MultiByteToWideChar(CP_UTF8,0,(char*)pszDecodedUrl,-1,NULL,0);
			t=(LPWSTR)malloc(need*sizeof(wchar_t));
			int num=MultiByteToWideChar(CP_UTF8,0,(char*)pszDecodedUrl,lstrlen(pszDecodedUrl)+1,t,need);
			need=WideCharToMultiByte(CP_ACP,0,(LPCWSTR)t,num,NULL,0,0,0);
            
			dest=(TCHAR *)malloc(need);
			WideCharToMultiByte(CP_ACP,0,(LPCWSTR)t,num,(char*)dest,need,0,0);				
		#else
		    int need=MultiByteToWideChar(CP_UTF8,0,pszDecodedUrl,-1,NULL,0);
			t=(LPWSTR)malloc(need*sizeof(wchar_t));
			int num=MultiByteToWideChar(CP_UTF8,0,pszDecodedUrl,strlen(pszDecodedUrl)+1,t,need);
			need=WideCharToMultiByte(CP_ACP,0,(LPCWSTR)t,num,NULL,0,0,0);
			
			dest=(TCHAR *)malloc(need);
			WideCharToMultiByte(CP_ACP,0,(LPCWSTR)t,num,dest,need,0,0);				
		#endif
		//free(t);
		
		CString str=(CString)dest;
		delete t;
		delete dest;
		delete pszDecodedUrl;
		return str;
	}

	static BOOL str_IsTextUTF8( CString strInputStream, INT iLen )
	{//22				
		DWORD cOctets;  // octets to go in this UTF-8 encoded character		
		BOOL  bAllAscii= TRUE;

		#ifdef _UNICODE			   									     
		    wchar_t chr;
			const wchar_t *lpstrInputStream=strInputStream;
		#else		
		    UCHAR chr;	
			const char *lpstrInputStream=strInputStream;	
		#endif
				
		cOctets= 0;
		for(int i=0; i < iLen; i++ )
		{
			chr= *(lpstrInputStream+i);
			
			if( (chr&0x80) != 0 ) bAllAscii= FALSE;

			if( cOctets == 0 )  {
				//
				// 7 bit ascii after 7 bit ascii is just fine.  Handle start of encoding case.
				//
				if( chr >= 0x80 ) 
				{  
					//
					// count of the leading 1 bits is the number of characters encoded
					//
					do {
						chr <<= 1;
						cOctets++;
					}
					while( (chr&0x80) != 0 );

					cOctets--;                        // count includes this character
					if( cOctets == 0 ) return FALSE;  // must start with 11xxxxxx
				}
			}
			else 
			{
				// non-leading bytes must start as 10xxxxxx
				if( (chr&0xC0) != 0x80 ) 
				{
					return FALSE;
				}
				cOctets--;                           // processed another octet in encoding
			}
		}

		//
		// End of text.  Check for consistency.
		//
		if(cOctets>0) return FALSE;// anything left over at the end is an error
		if(bAllAscii) return FALSE;// Not utf-8 if all ascii.  Forces caller to use code pages for conversion
			
		return TRUE;
	}

	static BOOL str_IsTextUnicode(LPSTR lpstrInputStream, INT iLen)
	{ 		
		INT  iResult= ~0; // turn on IS_TEXT_UNICODE_DBCS_LEADBYTE
		BOOL bUnicode;

		//#ifdef _UNICODE			   									     
		//    wchar_t *lpstrInputStream=strInputStream;
		//#else			
		//    const char *lpstrInputStream=strInputStream;	
		//#endif

		// We would like to check the possibility
		// of IS_TEXT_UNICODE_DBCS_LEADBYTE.
		//
			
		bUnicode= IsTextUnicode( lpstrInputStream, iLen, &iResult);
		
		if (bUnicode                                         &&
		   ((iResult & IS_TEXT_UNICODE_STATISTICS)    != 0 ) &&
		   ((iResult & (~IS_TEXT_UNICODE_STATISTICS)) == 0 )    )
		{ 
			CPINFO cpiInfo;
			CHAR* pch= (CHAR*)lpstrInputStream;
			INT  cb;

			//
			// If the result depends only upon statistics, check
			// to see if there is a possibility of DBCS.
			// Only do this check if the ansi code page is DBCS
			//

			GetCPInfo( CP_ACP, &cpiInfo);

			if( cpiInfo.MaxCharSize > 1 )
			{ 
				for( cb=0; cb<iLen; cb++ )
				{ 
					if( IsDBCSLeadByte(*pch++) )
					{ 
						return FALSE;
					}
				}
			}
		 }

		 return bUnicode;
	}*/
	
	static int str_Hex2IntB(CString hexStr)
	{//25 2자
		
		UINT res = 0;

		#ifdef _UNICODE			   			
			wchar_t  num[3];
			wchar_t *stop;
		#else
		   char  num[3];
		   char *stop;
		#endif	

		//Nothing happens if return 0, just server will not wakeup
		if (hexStr.GetLength()>2) return 0;

		memset(num,'\0',3);
		
		#ifdef _UNICODE			   
			wcscpy(num,hexStr);
			res = wcstol(num,&stop,16);
		#else
		    strcpy(num,hexStr);		
			res = strtol(num,&stop,16);
		#endif	
				
		if (res==LONG_MAX || res==LONG_MIN || res==0) 
		{
			return 0;
		}		
		return res;
	}
/*
	static CString str_UtfToUrl(CString value)
	{//21 Utf => url encoding
		value=str_BTrim(value);

		TCHAR *pszDecodedUrl=str_DecodingUrl(value);//Decoding 
        BOOL bflag=str_IsTextUTF8(pszDecodedUrl,lstrlen(pszDecodedUrl));
		if(bflag==TRUE)
		{//utf8이면
			value=str_DecodingUtf(value);//utf8이면
			return str_EncodingUrl(value);//urlencode
		}
		return value;//urlencode
	}

	static CString str_UrlEncode(CString& str)
	{//23
		if(str==_T("")) return _T("");

		CString sRet;
		int n = str.GetLength();
		for(int i=0; i<n; i++)
		{
			BYTE c = (BYTE)str[i];
			if (isalnum(c))
			{
				sRet+=c;
			}
			
			//else if (0x20 == c)
			//{
				//sRet+='+';
			//	sRet+=_T("%20");
			//}
			else
			{
				TCHAR tmp[6]={0};

				#ifdef _UNICODE			 
					swprintf_s(tmp, _T("%%%02X"), c);
				#else
					_stprintf(tmp, _T("%%%02X"), c);
				#endif

				sRet+=tmp;
			}
		}
		return sRet;
	}

	static int str_Hex2IntA(TCHAR *hex) 
	{//24 1자	
		char cHex = tolower(hex[0]);
		int nRet = 0;
		if ('a' == cHex) nRet = 10;
		else if ('b' == cHex) nRet = 11;
		else if ('c' == cHex) nRet = 12;
		else if ('d' == cHex) nRet = 13;
		else if ('e' == cHex) nRet = 14;
		else if ('f' == cHex) nRet = 15;
		else if ('g' == cHex) nRet = 16;
		else
		{
			#ifdef _UNICODE	
				wchar_t buf[2];
			#else
				char buf[2];
			#endif

			buf[0] = cHex;
			buf[1] = '\0';
			nRet = _ttoi(buf);
		}
		return nRet;
	}

	static int str_Hex2Char(const TCHAR* hex)
	{//26
		TCHAR buff[12];
		
		#ifdef _UNICODE			 
			swprintf_s(buff, _T("%s"), hex);					
		#else
			_stprintf(buff,_T("%s"),hex);
		#endif

		int ret = 0;
		const int len = (int)lstrlen(buff);
		for (int i=0; i<len; i++) 
		{
			TCHAR tmp[4];
			tmp[0] = buff[i];
			tmp[1] = '\0';
			int tmp_i = str_Hex2IntA(tmp);
			int rs = 1;
			for(int j=i; j<(len-1); j++)
				rs *= 16;		
			ret += (rs * tmp_i);
		}
		return ret;
	}

	static CString str_UrlDecode(CString& str)
	{//27
		CString sRet(_T(""));
		int nLen = str.GetLength();
		for(int i=0; i<nLen; i++)
		{
			TCHAR tch = (TCHAR)str[i];

			//if ('+' == tch)
			//{
			//	sRet += ' ';
			//}
			if ('%' == tch)
			{
				TCHAR tmp[4]={0};
				TCHAR hex[4]={0};
				hex[0] = str[++i];
				hex[1] = str[++i];

				#ifdef _UNICODE			 					
					swprintf_s(tmp, _T("%c"), str_Hex2Char(hex));
				#else
					_stprintf(tmp, _T("%c"), str_Hex2Char(hex));
				#endif
			
				sRet += tmp;
			}
			else
				sRet += tch;
		}
		return sRet;
	}

	static CString str_GetUrlKeyValue(CString urlinfo,CString skey)
	{//28 도메인에서 키의 값구하기-URL의 ExtraInfo에서 특정 단어가 있는 값만 구하기		
		urlinfo=str_AfterSplit(urlinfo,1,_T("?"));
		urlinfo.TrimLeft(_T("?"));
        skey.MakeLower();//소문자 기본
		
		int cnt=str_Count(urlinfo,_T("&"));
		if(cnt>0){
			for(int i=0;i<cnt+1;i++){			
			   CString header=str_GetSplit(urlinfo,i,'&');					   

			   CString key=str_GetSplit(header,0,'=');
			   CString value=str_AfterSplit(header,1,_T("="));

			   key.MakeLower();			   			   
			   if(key==skey){	
					return value;
					break;
				}
			}
		} else {
			CString key=str_GetSplit(urlinfo,0,'=');		
			CString value=str_GetSplit(urlinfo,1,'=');
			
			key.MakeLower();		
			if(key==skey) return value;		
		}
		
		return _T("");
	}	

	static char* str_DebugMsg(char *pszFormat, ...)
	{//29		
		char *buf=new char[1024];
		
		sprintf(buf,"[%lu]: ",GetCurrentThreadId());
		
		va_list arglist;
		va_start(arglist, pszFormat);
		vsprintf(&buf[strlen(buf)],pszFormat, arglist);
		va_end(arglist);
		 
		return buf;
		//OutputDebugString(buf);
		//printf("%s",buf);
	}*/

	static BOOL str_ChkNumeric(CString str)
	{//30				
		int len = str.GetLength();
		if(len > 0) 
		{
		   if(len>1)
		   {
			   if(str[0]=='-')
			   {
					str=str.Mid(1,len-1);
					len=len-1;
			   }
		   }		   

		   int ascint=0;
		   for(int i=0;i < len;i++)
		   {
			   ascint = (int)str[i];		
			   if(ascint < 48 || ascint > 57)
			   { 		  
				  return TRUE;  //char
			   }
		   }
		}
		else 
		{
			return TRUE;
		}

		return FALSE;          //numeric
	}
/*
	static CString str_IniSplit(CString IniData,CString skey1,CString skey2)
	{//31
		CString strData=_T("[")+str_AfterSplit(IniData,1,skey1);//처리 속도를 높이기위해서
		
		CString strTemp=str_BeforeSplit(strData,1,skey2);		
		if(strTemp.IsEmpty() || strTemp==_T("")) return strData;

		return strTemp;
	}

	static CString str_Keyboard(int iKey)
	{//32	
		CString skey=_T("");
		if(iKey>=112 && iKey<=123)
		{
		  skey.Format(_T("F%d"),iKey-111);
		}
		else
		{
		  char akey=iKey;
		  skey=(CString)akey;
		}	 
		return skey;				    
	}

	static CString str_GetError(int errorNo)
	{//33
		LPVOID lpMsgBuf;
		FormatMessage( 
			 FORMAT_MESSAGE_ALLOCATE_BUFFER | 
			 FORMAT_MESSAGE_FROM_SYSTEM | 
			 FORMAT_MESSAGE_IGNORE_INSERTS,
			 NULL,
			 GetLastError(),
			 MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
			 (LPTSTR) &lpMsgBuf,
			 0,
			 NULL 
			 );
			 CString strError = (LPCTSTR)lpMsgBuf;
			 LocalFree(lpMsgBuf);
		return strError;
	}

	static CString str_ChkReturn(CString str_Msg)
	{//34
		if(str_Msg.IsEmpty() || str_Msg==_T("")) return _T("");

		CString str=_T("");

		int count=str_Count(str_Msg,_T("/"));        	  
		if(count>0)
		{
			for(int i=0;i<count+1;i++)
			{
				CString temp=str_GetSplit(str_Msg,i,'/',FALSE);		 	   		   		   
				if(str==_T("")) str=temp + _T("\r\n");
				else            str= str + temp + _T("\r\n");					  		 
			}
		} 
		else str=str_Msg;	   
		
		return str;
	}

	static CString str_GetLeftStr( CString strFind, CString strOri )
	{//35
		int nIndex = 0, nFind = 0;
		while( nFind >= 0 )
		{
			nFind = strOri.Find( strFind, nIndex );
			if( nFind < 0 )
				break;
			nIndex = nFind + 1;
		}

		if( nIndex == 0 )
			return _T("");

		int nCount = nIndex;

		return strOri.Left( nCount - 1 );	
	}

	static CString str_GetPath(CString strPath,char ch='\\')
	{//36 앞쪽		
		strPath.TrimRight(ch);
		
		int nPos=strPath.ReverseFind(ch);
		if(nPos>-1)
		{
			strPath = strPath.Mid(0,nPos+1);	
		}

		return strPath;
	}
	
	static CString str_GetFileExt(CString strName)
	{//37		
		strName=str_BTrim(strName);
		
		int nPos=strName.ReverseFind('.');
		if(nPos>-1)
		{
			strName = strName.Mid(nPos+1,strName.GetLength()-(nPos+1));	
		}

		return strName;
	}*/

	static CString str_GetFile(CString strPath,char ch='\\')
	{//38 뒷쪽		
		strPath.TrimRight(ch);
		
		int nPos=strPath.ReverseFind(ch);
		if(nPos>-1)
		{
			strPath = strPath.Mid(nPos+1,strPath.GetLength()-(nPos+1));	
		}

		return strPath;
	}

	/*
	static CString str_GetMid(CString data,int nFirst,int nCount)
	{//39	
		if(data.IsEmpty() || data==_T("")) return _T("");

		CString str=data;

	    str=str.Mid(nFirst,nCount);
				
		return str_BTrim(str);
	}

	static CString str_GetGuid(GUID guid)
	{//40
		wchar_t wszCLSID[129]; 
		StringFromGUID2(guid,  wszCLSID, 128); // 문자열로 변환

		#ifdef _UNICODE			   
			return wszCLSID;
		#else
			USES_CONVERSION; 
		    return OLE2A(wszCLSID);
		#endif

		return _T("");
	}

	static CString str_GetNumFormat(long idata)
	{//41
		NUMBERFMT nFmt = { 0, 0, 3, _T("."),_T(","), 0 };
		
		CString str;
		str.Format(_T("%d"),idata);

		#ifdef _UNICODE	
			wchar_t Result[40]={0};
		#else
			char Result[40]={0};
		#endif

	    GetNumberFormat(0, 0, (LPCTSTR)str, &nFmt, Result, 40);

		return Result;
	}
	
	static CString str_CharToDecimal(UCHAR *nData,int nSize,char ch)
	{//43
		if(nSize<1) return _T("");
		if(!nData)  return _T("");

		CString sData=_T("");
		for(int i=0;i<nSize;i++)
		{
		  CString str=_T("");
		  str.Format(_T("%d%c"),nData[i],ch);

		  sData=sData+str;	  	 
		}
		return sData;
	}

	static BOOL str_ChkWeb(CString sfile)
	{//44
		CString strA=sfile.Mid(0,6);
		CString strB=sfile.Mid(0,7);

		if(strA.Find(_T("mms://"))>-1 || strB.Find(_T("http://"))>-1) return TRUE;

		return FALSE;
	}

	static BYTE* str_MacByte(CString smac)
	{//45
		BYTE *byteMacAddress=new BYTE[6];
		for(int i=0;i<6;i++)
		{
			byteMacAddress[i]=str_Hex2IntB(GStr::str_GetSplit(smac,i,'-'));
		}
		return byteMacAddress;
	}

	static BYTE* str_MacByteA(CString strMac)
	{//45
		strMac.Replace(_T("-"),_T(""));
		strMac.Replace(_T(":"),_T(""));
		strMac.Replace(_T(","),_T(""));

		BYTE *byteMacAddress=new BYTE[6];
		for(int i=0;i<6;i++)
		{		
			byteMacAddress[i]=GStr::str_Hex2IntB(strMac.Mid(i*2,2));
		}
		return byteMacAddress;
	}*/

	static BYTE* str_getByte(CString data,int len)
	{//46 변환
		BYTE *digest=new BYTE[len];
		::memset(digest,0,len);
		
		if(data.GetLength()>len) data=data.Mid(0,len);

		for(int i=0;i<data.GetLength();i++)
		{
			digest[i]=(char)data[i];
		}

		return digest;	
	}

	static BYTE str_Code2Ascii( char cData )
	{//47
		BYTE Ascii;

		//--> '0' ~ '9' => 0 ~ 9
		if( ('0' <= cData) && (cData <= '9') )
		{
		  Ascii = cData - '0';
		} //--> 'A' ~ 'F' => 10 ~ 15
		else if( ('A' <= cData) && (cData <= 'F') )
		{
		  Ascii = (cData - 'A') + 10;
		} //--> 'a' ~ 'f' => 10 ~ 15
		 else if( ('a' <= cData) && (cData <= 'f') )
		{
		  Ascii = (cData - 'a') + 10;
		}
		else 
		{
		  Ascii = 0;
		}
		return Ascii;
	}

	/*static CString str_UtfDecode(CString value)
	{//48 UTF-8	
		TCHAR *pszDecodedUrl=NULL;		
		
		LPWSTR t;
		TCHAR *dest=NULL;

		//UTF-8
		#ifdef _UNICODE	
			pszDecodedUrl=value.AllocSysString();
									
			int need=MultiByteToWideChar(CP_UTF8,0,(char*)pszDecodedUrl,-1,NULL,0);
			t=(LPWSTR)malloc(need*sizeof(wchar_t));
			int num=MultiByteToWideChar(CP_UTF8,0,(char*)pszDecodedUrl,lstrlen(pszDecodedUrl)+1,t,need);
			need=WideCharToMultiByte(CP_ACP,0,(LPCWSTR)t,num,NULL,0,0,0);
				
			dest=(TCHAR *)malloc(need);
 			WideCharToMultiByte(CP_ACP,0,(LPCWSTR)t,num,(char*)dest,need,0,0);		
		#else
		    pszDecodedUrl=(LPSTR)(LPCTSTR)value;

			int need=MultiByteToWideChar(CP_UTF8,0,pszDecodedUrl,-1,NULL,0);
			t=(LPWSTR)malloc(need*sizeof(wchar_t));
			int num=MultiByteToWideChar(CP_UTF8,0,pszDecodedUrl,strlen(pszDecodedUrl)+1,t,need);
			need=WideCharToMultiByte(CP_ACP,0,(LPCWSTR)t,num,NULL,0,0,0);
				
			dest=(TCHAR *)malloc(need);
 			WideCharToMultiByte(CP_ACP,0,(LPCWSTR)t,num,dest,need,0,0);		

		#endif			
		//free(t);

		CString str=(CString)dest;		
		delete t;
		delete dest;
		delete pszDecodedUrl;
		return str;
	}

	static CString str_IsUtf(CString value,BOOL bTrim=TRUE)
	{//49 Utf => encoding
		if(bTrim==TRUE) value=str_BTrim(value);		
						
		int nLength=value.GetLength();
        BOOL bflag=str_IsTextUTF8(value,nLength);
		if(bflag==TRUE)
		{//utf8이면
			return str_UtfDecode(value);//utf8이면			
		}
							
		return value;//urlencode
	}

	static char* str_CximageEnc(CString sfile)
	{
		int nSize=sfile.GetLength();
		if(nSize<1) return NULL;
		char *buf={0};

		#ifdef _UNICODE	
			buf=new char[nSize*2];
			memset(buf,0,sizeof(buf));
			WideCharToMultiByte(CP_ACP, 0, sfile, -1, buf, nSize*2, 0, 0);	
		#else
			buf=new char[nSize+1];
			memset(buf,0,sizeof(buf));
			strcpy(buf,sfile);
		#endif

		return buf;
	}
	
	static CString str_Random(CString strValid)
	{
		if(strValid.IsEmpty() || strValid==_T("")) strValid=_T("0123456789");
		
		srand((unsigned)time(NULL));
		int num=rand() % strValid.GetLength();//random
		
		CString skey=strValid.Mid(num, 1); 
		return skey;
	}*/


	static CString str_MutibyteToUtf8(CString sdata)
	{
		if(sdata.IsEmpty() || sdata==_T("")) return sdata;

		//mutibyte -> unicode
		wchar_t strUnicode[2048] = {0,}; 
		char    strMultibyte[2048] = {0,}; 
		strcpy(strMultibyte,sdata); 
		int nLen = MultiByteToWideChar(CP_ACP, 0, strMultibyte, strlen(strMultibyte), NULL, NULL); 
		MultiByteToWideChar(CP_ACP, 0, strMultibyte, strlen(strMultibyte), strUnicode, nLen); 

		//unicode=>utf8
		char strUtf8[2048] ={0,}; 
		nLen = WideCharToMultiByte(CP_UTF8, 0, strUnicode, lstrlenW(strUnicode), NULL, 0, NULL, NULL); 
		WideCharToMultiByte (CP_UTF8, 0, strUnicode, lstrlenW(strUnicode), strUtf8, nLen, NULL, NULL); 
	
		CString str=(CString)strUtf8;
        return str;
	}

	static CString str_Utf8ToUnicode(CString sdata)
	{
		if(sdata.IsEmpty() || sdata==_T("")) return sdata;

		wchar_t strUnicode[2048] = {0,}; 
		char    strUTF8[2048] = {0,}; 
		strcpy(strUTF8,sdata);

		int nLen = MultiByteToWideChar(CP_UTF8, 0, strUTF8, strlen(strUTF8), NULL, NULL); 
		MultiByteToWideChar(CP_UTF8, 0, strUTF8, strlen(strUTF8), strUnicode, nLen); 

		CString str=(CString)strUnicode;
        return str;
	}

} ;