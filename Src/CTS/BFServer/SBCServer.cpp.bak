#include "StdAfx.h"

#include "SBCGlobal.h"
#include "SBCLog.h"
#include "SBCCriticalSection.h"
#include "SBCSyncParent.h"
#include "SBCStaticSyncParent.h"
#include "SBCMemoryPool.h"
#include "SBCManagedBuf.h"
#include "SBCCircularQueue.h"

#include "SBCPacketBox.h"

#include "SBCIocp.h"
#include "SBCNetObj.h"

#include "SBCNetIOCP.h"
#include "SBCRawServer.h"

#include "SBCObj.h"

#include "SBCServer.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CSBCServer::CSBCServer()
{
	mConnectCnt = 0;

	mKeepThreadHandle = NULL;
	mKeepThreadDestroyEvent = NULL;

	mMainThreadHandle = NULL;
	mMainThreadDestroyEvent = NULL;
}

CSBCServer::~CSBCServer()
{

}

DWORD WINAPI KeepThreadCallback(LPVOID parameter)
{
	CSBCServer *Owner = (CSBCServer*) parameter;
	Owner->KeepThreadCallback();

	return 0; 
}

DWORD WINAPI MainThreadCallback(LPVOID parameter)
{
	CSBCServer *Owner = (CSBCServer*) parameter;
	Owner->MainThreadCallback();

	return 0;
}

//DWORD WINAPI ResetThreadCallback(LPVOID parameter)
//{
//	CDeviceServer *Owner = (CDeviceServer*) parameter;
//	Owner->ResetThreadCallback();
//
//	return 0;
//}

VOID CSBCServer::KeepThreadCallback(VOID)
{

	while (TRUE)
	{
		SetEvent(mObjClose);
		DWORD Result = WaitForSingleObject(
			mKeepThreadDestroyEvent, 100);

		if (Result == WAIT_OBJECT_0)
			return;

		if(m_lstSBCObj.size())
		{
			for (std::list<CSBCObj*>::iterator it=m_lstSBCObj.begin();it!=m_lstSBCObj.end();it++)
			{
				CSBCObj *poSBCObj = (CSBCObj*)(*it);
				poSBCObj->WriteingPacket();
			}
		}
	}
}

VOID CSBCServer::MainThreadCallback(VOID)
{
	INT cnt = 0;
	while (TRUE)
	{
		DWORD Result = WaitForSingleObject(mMainThreadDestroyEvent, 1000);

		if (Result == WAIT_OBJECT_0)
			return;

		if(m_lstSBCObj.size())
		{
			for (std::list<CSBCObj*>::iterator it=m_lstSBCObj.begin();it!=m_lstSBCObj.end();it++)
			{
				CSBCObj *poSBCObj = (CSBCObj*)(*it);
				if(poSBCObj->GetCloseCnt() > 10)
				{
					poSBCObj->ForceClose();
				}
				else
				{
					poSBCObj->SetCloseCnt();
				}
			}
		}
		if(cnt > 5)
		{
			cnt = 0;
			CSBCLog::WriteSysLog("[Qcnt: [%d]]\n", g_Que.GetCount());
		}
		cnt++;

	}
}

//VOID CDeviceServer::ResetThreadCallback(VOID)
//{
//	while (TRUE)
//	{
//		DWORD Result = WaitForSingleObject(mResetThreadDestroyEvent, 1000);
//
//		if (Result == WAIT_OBJECT_0)
//			return;
//	}
//}

BOOL CSBCServer::NetBegin(HWND mainframe)
{	
	//m_lCnt = 0;
	mPapaHwnd = mainframe;


	CSBCRawServer::Begin(_EP_CONTROL_TCPIP_PORT1);

	mKeepThreadHandle = NULL;
	mKeepThreadDestroyEvent = NULL;

	mMainThreadHandle = NULL;
	mMainThreadDestroyEvent = NULL;

	//mResetThreadHandle = NULL;
	//mResetThreadDestroyEvent = NULL;
	
	mObjClose = CreateEvent(NULL, FALSE, FALSE, NULL);
	if (!mObjClose)
	{
		CSBCRawServer::End();

		return FALSE;
	}

	mKeepThreadDestroyEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	if (!mKeepThreadDestroyEvent)
	{
		CSBCRawServer::End();

		return FALSE;
	}

	mKeepThreadHandle		= CreateThread(NULL, 0, ::KeepThreadCallback, this, 0, NULL);
	if (!mKeepThreadHandle)
	{
		CSBCRawServer::End();

		return FALSE;
	}

	mMainThreadDestroyEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	if (!mMainThreadDestroyEvent)
	{
		CSBCRawServer::End();

		return FALSE;
	}

	mMainThreadOn = TRUE;
	mMainThreadHandle		= CreateThread(NULL, 0, ::MainThreadCallback, this, 0, NULL);
	if (!mMainThreadHandle)
	{
		CSBCRawServer::End();

		return FALSE;
	}

	return TRUE;
}
BOOL CSBCServer::NetEnd(VOID)
{
	if (mKeepThreadDestroyEvent && mKeepThreadHandle)
	{
		mMainThreadOn = FALSE;
		SetEvent(mKeepThreadDestroyEvent);

		WaitForSingleObject(mKeepThreadHandle, INFINITE);

		CloseHandle(mKeepThreadDestroyEvent);
		CloseHandle(mKeepThreadHandle);
	}

	if (mMainThreadDestroyEvent && mMainThreadHandle)
	{
		SetEvent(mMainThreadDestroyEvent);

		WaitForSingleObject(mMainThreadHandle, INFINITE);

		CloseHandle(mMainThreadDestroyEvent);
		CloseHandle(mMainThreadHandle);
	}

	CloseHandle(mObjClose);

	CSBCRawServer::End();

	for (std::list<CSBCObj*>::iterator it=m_lstSBCObj.begin();it!=m_lstSBCObj.end();it++)
	{
		CSBCObj *poSBCObj = (CSBCObj*)(*it);
		if (poSBCObj->ForceClose())
		{
			//OnDisconnected(poSBCObj);
			delete poSBCObj;
		}
	}

	m_lstSBCObj.clear();

	return TRUE;
}

VOID CSBCServer::OnConnected(CSBCObj *poNetObj)
{
	CSBCLog::WriteSysLog(_T("SBCOnConnected => %d\n"), ++mConnectCnt);
	//
	poNetObj->SetNotifyWnd(mPapaHwnd);
	poNetObj->GetPeerName();
	m_lstSBCObj.push_back(poNetObj);
	poNetObj->SavePacket(EP_CMD_VER_REQUEST);
	poNetObj->SendCommand();

}

VOID CSBCServer::OnDisconnected(CSBCObj *poNetObj)
{
	CSBCLog::WriteSysLog(_T("<= SBCOnDisconnected %d\n"), --mConnectCnt);
	WaitForSingleObject(mObjClose, 1000);
	poNetObj->ModuleClosed();
	if(m_lstSBCObj.size())
	{
		m_lstSBCObj.remove(poNetObj);
		delete poNetObj;
	}
}

VOID CSBCServer::OnRead(CSBCObj *poNetObj, EP_MSG_HEADER &_hdr, BYTE *pReadBuf, DWORD dwLen)
{
	//if(_hdr.nCommand != 0x00000004
	//	&& _hdr.nCommand != 0x00000007
	//	&& _hdr.nCommand != 0x0000000C
	//	)
	//{
	CSBCLog::WriteSysLog("OnRead=====%x %d========> %d %d %d Command[0x%08X] %d [Qcnt: [%d]]\n"
			, poNetObj, dwLen
			, _hdr.nID
			, _hdr.wGroupNum
			, _hdr.wChannelNum
			, _hdr.nCommand
			, _hdr.nLength
			, g_Que.GetCount());
	//}

	//poNetObj->Write(_hdr, pReadBuf, dwLen);
	
	SBCPacketError _error = SPE_MAXERROR;
	poNetObj->ResetCloseCnt();
	//Connect Seq
	if(_hdr.nCommand == EP_CMD_VER_DATA)
	{
		_error = poNetObj->RecvEP_CMD_VER_DATA(&_hdr, pReadBuf, dwLen);
	}
	//paser
	if(poNetObj->CheckMsgHeader(&_hdr))
	{
		switch(_hdr.nCommand)
		{
		case EP_CMD_AUTO_GP_STATE_DATA://no Ack
			{
				_error = poNetObj->RecvEP_CMD_AUTO_GP_STATE_DATA(&_hdr, pReadBuf, dwLen);
			}
			break;
		case EP_CMD_AUTO_GP_DATA://no Ack
			{
				_error = poNetObj->RecvEP_CMD_AUTO_GP_DATA(&_hdr, pReadBuf, dwLen);
			}
			break;
		case EP_CMD_AUTO_GP_STEP_END_DATA://no Ack
			{
				_error = poNetObj->RecvEP_CMD_AUTO_GP_STEP_END_DATA(&_hdr, pReadBuf, dwLen);
			}
			break;
		case EP_CMD_AUTO_SENSOR_DATA://no Ack
			{
				_error = poNetObj->RecvEP_CMD_AUTO_SENSOR_DATA(&_hdr, pReadBuf, dwLen);
			}
			break;
		case EP_CMD_AUTO_EMG_DATA://no Ack
			{
				_error = poNetObj->RecvEP_CMD_AUTO_EMG_DATA(&_hdr, pReadBuf, dwLen);
			}
			break;
		}
		switch(_hdr.nCommand)
		{
		//case EP_CMD_RESPONSE:
		case EP_CMD_VER_DATA:
			break;
		case EP_CMD_AUTO_GP_STATE_DATA://no Ack
		case EP_CMD_AUTO_GP_DATA://no Ack
		case EP_CMD_AUTO_GP_STEP_END_DATA://no Ack
		case EP_CMD_AUTO_SENSOR_DATA://no Ack
		case EP_CMD_AUTO_EMG_DATA://no Ack
			break;
		case EP_CMD_RESPONSE:
			_error = poNetObj->RecvEP_CMD_RESPONSE(&_hdr, pReadBuf, dwLen);
			break;
		case EP_CMD_TAG_INFO:
			{
				_error = poNetObj->RecvEP_CMD_TAG_INFO(&_hdr, pReadBuf, dwLen);
				poNetObj->SendEP_CMD_RESPONSE(&_hdr, _error);
			}
			break;
		case EP_CMD_AUTO_CHECK_RESULT:
			{
				_error = poNetObj->RecvEP_CMD_AUTO_CHECK_RESULT(&_hdr, pReadBuf, dwLen);
				poNetObj->SendEP_CMD_RESPONSE(&_hdr, _error);
			}
			break;
		case EP_CMD_HEARTBEAT:
			{
				_error = poNetObj->SendEP_CMD_RESPONSE(&_hdr, SPE_NONE);
			}
			break;
		case EP_CMD_USER_CMD:
			{
				_error = poNetObj->RecvEP_CMD_USER_CMD(&_hdr, pReadBuf, dwLen);
				poNetObj->SendEP_CMD_RESPONSE(&_hdr, _error);
			}
			break;
		case EP_CMD_REAL_TIME_DATA:
			{
				_error = poNetObj->RecvEP_CMD_REAL_TIME_DATA(&_hdr, pReadBuf, dwLen);
				poNetObj->SendEP_CMD_RESPONSE(&_hdr, _error);
			}
			break;
		case EP_CMD_LINE_MODE_DATA:
			{
				_error = poNetObj->RecvEP_CMD_LINE_MODE_DATA(&_hdr, pReadBuf, dwLen);
			}
			break;
		case EP_CMD_IOTEST_READINFO:
			{
				_error = poNetObj->RecvEP_CMD_IOTEST_READINFO(&_hdr, pReadBuf, dwLen);
			}
			break;
		case EP_CMD_MAPPING_DATA:
			{
				_error = poNetObj->RecvEP_CMD_MAPPING_DATA(&_hdr, pReadBuf, dwLen);
			}
			break;
		case 0x0001300D:
			{
				_error = poNetObj->RecvEP_CMD_CAL_POINT_DATA(&_hdr, pReadBuf, dwLen);
			}
			break;
		case EP_CMD_CAL_RES:
			{
				_error = poNetObj->RecvEP_CMD_CAL_RES(&_hdr, pReadBuf, dwLen);
			}
			break;
		case EP_CMD_CHK_RES:
			{
				_error = poNetObj->RecvEP_CMD_CHK_RES(&_hdr, pReadBuf, dwLen);
			}
			break;
		case EP_CMD_CAL_END:
			{
				_error = poNetObj->RecvEP_CMD_CAL_END(&_hdr, pReadBuf, dwLen);
			}
			break;
		case EP_CMD_CHK_END:
			{
				_error = poNetObj->RecvEP_CMD_CHK_END(&_hdr, pReadBuf, dwLen);
			}
			break;
		//case EP_CMD_CAL_RESULT:
		//	{
		//		_error = poNetObj->RecvEP_CMD_CAL_RESULT(&_hdr, pReadBuf, dwLen);
		//		poNetObj->SendEP_CMD_RESPONSE(&_hdr, _error);
		//	}
		//	break;
		//case EP_CMD_AUTO_GP_STEP_SEC_DATA:
		//	{
		//		_error = poNetObj->RecvEP_CMD_AUTO_GP_STEP_SEC_DATA(&_hdr, pReadBuf, dwLen);
		//		poNetObj->SendEP_CMD_RESPONSE(&_hdr, _error);
		//	}
		//	break;
		//case EP_CMD_REAL_CHAMBER_DATA:
		//	{
		//		_error = poNetObj->RecvEP_CMD_REAL_CHAMBER_DATA(&_hdr, pReadBuf, dwLen);
		//		poNetObj->SendEP_CMD_RESPONSE(&_hdr, _error);
		//	}
		//	break;
		//case EP_CMD_ALL_NG:
		//	{
		//		_error = poNetObj->RecvEP_CMD_ALL_NG(&_hdr, pReadBuf, dwLen);
		//		poNetObj->SendEP_CMD_RESPONSE(&_hdr, _error);
		//	}
		//	break;
		//case EP_CMD_SWITCH:
		//	{
		//		_error = poNetObj->RecvEP_CMD_SWITCH(&_hdr, pReadBuf, dwLen);
		//		poNetObj->SendEP_CMD_RESPONSE(&_hdr, _error);
		//	}
		//	break;
		default:
			CSBCLog::WriteErrorLog("Unknown Command ID: %d Command: 0x%x, Len:%d"
				, _hdr.nID
				, _hdr.nCommand
				, _hdr.nLength);
			break;
		}
	}
	else
	{
		CSBCLog::WriteErrorLog("Unknown Command");
	}

	if(_error == SPE_NONE)
	{

	}
	else
	{
		CSBCLog::WriteErrorLog("0x%08x %s", _hdr.nCommand, g_szSBCPacketError[_error]);
	}
}

VOID CSBCServer::OnWrite(CSBCObj *poNetObj, DWORD dwLen)
{
	//TRACE("OnWrite=====%x %d========> \n", poNetObj, dwLen);
	poNetObj->ResetCloseCnt();
}

BOOL CSBCServer::SavePacket(INT nModuleID, EP_MSG_HEADER  *_hdr, BYTE *pReadBuf, DWORD dwLen)
{

	for (std::list<CSBCObj*>::iterator it=m_lstSBCObj.begin();it!=m_lstSBCObj.end();it++)
	{
		CSBCObj *poSBCObj = (CSBCObj*)(*it);
		if (poSBCObj->GetModuleID() == nModuleID)
		{
			poSBCObj->SavePacket(_hdr, pReadBuf, dwLen);
			return TRUE;
		}
	}

	return FALSE;
}

BOOL CSBCServer::SendCommand(INT nModuleID)
{
	for (std::list<CSBCObj*>::iterator it=m_lstSBCObj.begin();it!=m_lstSBCObj.end();it++)
	{
		CSBCObj *poSBCObj = (CSBCObj*)(*it);
		if (poSBCObj->GetModuleID() == nModuleID)
		{
			poSBCObj->SendCommand();
			return TRUE;
		}
	}

	return FALSE;
}

VOID CSBCServer::SetNotiHWND(INT nModuleID, HWND _hwnd, UINT _cmd)
{
	for (std::list<CSBCObj*>::iterator it=m_lstSBCObj.begin();it!=m_lstSBCObj.end();it++)
	{
		CSBCObj *poSBCObj = (CSBCObj*)(*it);
		if (poSBCObj->GetModuleID() == nModuleID)
		{
			Request_CMD *pInfo = new Request_CMD;
			pInfo->hWnd = _hwnd;
			pInfo->Cmd = _cmd;
			
			poSBCObj->SetNotifyDlgWnd(pInfo);
		}
	}
}