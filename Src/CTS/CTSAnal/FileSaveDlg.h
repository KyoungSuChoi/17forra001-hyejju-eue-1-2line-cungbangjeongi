#if !defined(AFX_FILESAVEDLG_H__C91E45A0_A381_11D4_905E_0001027DB21C__INCLUDED_)
#define AFX_FILESAVEDLG_H__C91E45A0_A381_11D4_905E_0001027DB21C__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// FileSaveDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFileSaveDlg dialog
class CFileSaveDlg : public CDialog
{
// Construction
public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig();

	CFileSaveDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CFileSaveDlg();

	CString m_strSection;
	void SaveSetting();
	void LoadSetting();
	void ChannelStateCheck(BOOL bInit=FALSE);
	void ProcStateCheck(BOOL bInit=FALSE); 

	

	CString m_strOrgFileName;
// Dialog Data
	//{{AFX_DATA(CFileSaveDlg)
	enum { IDD = IDD_FILE_SAVE_DLG };
	CStaticFilespec	m_strFileNameLabel;
	BOOL	m_bCSVfileSave;
	BOOL	m_bTxtFileSave;

	BOOL	m_bCycleSave;
	BOOL	m_bCannelSave;
	BOOL	m_bProcSave;

	BOOL	m_bChMode;
	BOOL	m_bChType;
	BOOL	m_bChTime;
	BOOL	m_bChState;
	BOOL	m_bChGrade;
	BOOL	m_bChFailure;
	BOOL	m_bChVoltage;
	BOOL	m_bChCurrent;
	BOOL	m_bChWatt;
	BOOL	m_bChCapacity;
	BOOL	m_bChImpedance;
	
	BOOL	m_bStepVoltage;
	BOOL	m_bStepCurrent;
	BOOL	m_bStepWatt;
	BOOL	m_bStepCapacity;
	BOOL	m_bStepImpedance;
	BOOL	m_bStepGrade;
	BOOL	m_bStepFailure;
	BOOL	m_bStepTime;
	
	CString m_strSaveFileName;
	CString	m_strChFileName;
	CString	m_strStepFileName;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFileSaveDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFileSaveDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnProcCheck();
	afx_msg void OnChannelCheck();
	afx_msg void OnOk();
	afx_msg void OnFolderSettingBtn();
	afx_msg void OnCycleDataCheck();
	afx_msg void OnChListFileBtn();
	afx_msg void OnStepListFileBtn();
	afx_msg void OnTxtSave();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FILESAVEDLG_H__C91E45A0_A381_11D4_905E_0001027DB21C__INCLUDED_)
