#pragma once

#define CHANGE_FNID(PROC_FN_ID)\
	m_Unit->fnGetState()->fnSetProcID(##PROC_FN_ID)

#define CHANGE_STATE(FM_STATUS_ID)\
	m_Unit->fnSetState(m_Unit->fnGet_##FM_STATUS_ID());\
	CHANGE_FNID(FNID_ENTER)

#define CHANGE_TRAY_FNID(PROC_FN_ID)\
	m_Unit->fnGetTrayState()->fnSetProcID(##PROC_FN_ID)

#define CHANGE_TRAY_STATE(FM_TRAY_STATUS_ID)\
	m_Unit->fnSetTrayState(m_Unit->fnGet_##FM_TRAY_STATUS_ID());\
	CHANGE_TRAY_FNID(FNID_ENTER)

class CFM_STATE
{
public:
	CFM_STATE(FM_STATUS_ID, FM_STATUS_ID, CFM_Unit*);
	virtual ~CFM_STATE(void);
	virtual VOID		fnInit(){TRACE("==========CFM_STATE fnInit");};
	VOID				fnWaitInit();

	VOID				fnPorcessing();
	virtual VOID		fnEnter(){TRACE("==========CFM_STATE fnRecv");};
	virtual VOID		fnProc(){TRACE("==========CFM_STATE fnProc");};
	virtual VOID		fnExit(){TRACE("==========CFM_STATE fnSend");};

	virtual VOID		fnSBCPorcess(WORD){TRACE("==========CFM_STATE fnChangeModuleState");};
	virtual FMS_ERRORCODE		fnFMSPorcess(FMS_COMMAND, st_FMS_PACKET*)
								{TRACE("==========CFM_STATE fnChangeModuleState"); return ER_END;};

	FM_STATUS_ID		fnGetEQStateID(){return m_EQSTID;}
	FM_STATUS_ID		fnGetStateID(){return m_STID;}

	CFM_Unit*			fnGetMachin(){return m_Unit;}
	CFM_STATE*			fnInitState();
	CFM_STATE*			fnCheckDown();

	VOID				fnSetProcID(PROC_FN_ID _id)
	{
		m_ProcID = _id;
	}
	PROC_FN_ID			fnGetProcID(){return m_ProcID;}

	DWORD				fnGetRetyState(){return m_RetryMap;}
	//BOOL				fnSend_MEI_EQ_STATE_EVENT();
	//MES_ERROR			fnSend_NOMAL_REPLY(MES_MSG_ID _msgid, MES_ERROR);

	VOID				fnSetWaitTimer(INT _time){m_iWaitUserTimer = _time;}

	//BOOL				fnsearchXMLSubData(TiXmlElement* pEml, TCHAR _szElementname[][32], TCHAR** _szEleValue);
	//BOOL				fnsearchXMLData_sub(TiXmlElement* pEml, TCHAR _szElementname[][32]
	//						, TCHAR** _szEleValue, TCHAR _szElementSubname[][32], DWORD subStartID, TCHAR*** _szEleValuesubData);

	//LPMES_XML_RECV		fnGetXMLRecv(){return &m_XmlRecv;}
	//VOID				fnSetXMLRecv(LPMES_XML_RECV _xmlrecv){memcpy(&m_XmlRecv, _xmlrecv, sizeof(MES_XML_RECV));}

public:
	VOID				fnSetFMSStateCode(FMS_STATE_CODE);
	FMS_STATE_CODE		fnGetFMSStateCode();

protected:
	const FM_STATUS_ID m_EQSTID;
	const FM_STATUS_ID m_STID;
	CFM_Unit* m_Unit;

	DWORD m_RetryMap;
	UINT m_nSeqNum;		// 상태별 Seq 상태

	INT m_iWaitUserTimer;
	INT m_iWaitTimer;
	INT m_iWaitCount;

private:
	PROC_FN_ID m_ProcID;

	INT m_iConstWaitTimer;
	INT m_iConstWaitCount;
};
static VOID (CFM_STATE::*CFM_STATE_mfg[])() = 
{
	&CFM_STATE::fnEnter
	, &CFM_STATE::fnProc
	, &CFM_STATE::fnExit
};
//////////////////////////////////////////////////////////////////////////
class CFM_ST_OFF :public CFM_STATE
{
public:
	CFM_ST_OFF(FM_STATUS_ID, FM_STATUS_ID, CFM_Unit*);
	virtual ~CFM_ST_OFF(void);

	VOID		fnInit(){};
	VOID		fnEnter();
	VOID		fnProc();
	VOID		fnExit();

	VOID		fnSBCPorcess(WORD);
	FMS_ERRORCODE		fnFMSPorcess(FMS_COMMAND, st_FMS_PACKET*);

	FMS_STATE_CODE		fnGetFMSStateCode();

private:
	BOOL		fnCheckModuleState();
};
//////////////////////////////////////////////////////////////////////////
class CFM_ST_AUTO :public CFM_STATE
{
public:
	CFM_ST_AUTO(FM_STATUS_ID, FM_STATUS_ID, CFM_Unit*);
	virtual ~CFM_ST_AUTO(void);

	virtual	VOID		fnInit(){};
	virtual VOID		fnEnter();
	virtual VOID		fnProc();
	virtual VOID		fnExit();

	virtual VOID		fnSBCPorcess(WORD);
	virtual FMS_ERRORCODE		fnFMSPorcess(FMS_COMMAND, st_FMS_PACKET*);

	VOID fnAlarmOn();

	VOID fnSetVacancy(CFM_STATE* _st){m_pstATVacancy = _st;}
	VOID fnSetReady(CFM_STATE* _st){m_pstATReady = _st;}
	VOID fnSetTrayIn(CFM_STATE* _st){m_pstATTrayIn = _st;}
	VOID fnSetContactCheck(CFM_STATE* _st){m_pstATContacCheck = _st;}
	VOID fnSetRun(CFM_STATE* _st){m_pstATRun = _st;}
	VOID fnSetEnd(CFM_STATE* _st){m_pstATEnd = _st;}
	VOID fnSetError(CFM_STATE* _st){m_pstATError = _st;}

	CFM_STATE* fnGetVacancy(){return m_pstATVacancy;}
	CFM_STATE* fnGetReady(){return m_pstATReady;}
	CFM_STATE* fnGetTrayIn(){return m_pstATTrayIn;}
	CFM_STATE* fnGetContactCheck(){return m_pstATContacCheck;}
	CFM_STATE* fnGetRun(){return m_pstATRun;}
	CFM_STATE* fnGetEnd(){return m_pstATEnd;}
	CFM_STATE* fnGetError(){return m_pstATError;}

private:

	CFM_STATE* m_pstATVacancy;
	CFM_STATE* m_pstATReady;
	CFM_STATE* m_pstATTrayIn;
	CFM_STATE* m_pstATContacCheck;
	CFM_STATE* m_pstATRun;
	CFM_STATE* m_pstATEnd;
	CFM_STATE* m_pstATError;
};
class CFM_ST_AT_VACANCY : public CFM_ST_AUTO
{
public:
	CFM_ST_AT_VACANCY(FM_STATUS_ID, FM_STATUS_ID, CFM_Unit*);
	virtual ~CFM_ST_AT_VACANCY(void);

	VOID		fnEnter();
	VOID		fnProc();
	VOID		fnExit();
	
	VOID		fnSetMisSeq();

	VOID		fnSBCPorcess(WORD);
	FMS_ERRORCODE		fnFMSPorcess(FMS_COMMAND, st_FMS_PACKET*);

private:
	
};

class CFM_ST_AT_READY :	public CFM_ST_AUTO
{
public:
	CFM_ST_AT_READY(FM_STATUS_ID, FM_STATUS_ID, CFM_Unit*);
	virtual ~CFM_ST_AT_READY(void);

	VOID		fnEnter();
	VOID		fnProc();
	VOID		fnExit();

	VOID		fnSBCPorcess(WORD);
	FMS_ERRORCODE		fnFMSPorcess(FMS_COMMAND, st_FMS_PACKET*);

private:
};

class CFM_ST_AT_TRAY_IN :	public CFM_ST_AUTO
{
public:
	CFM_ST_AT_TRAY_IN(FM_STATUS_ID, FM_STATUS_ID, CFM_Unit*);
	virtual ~CFM_ST_AT_TRAY_IN(void);

	VOID		fnEnter();
	VOID		fnProc();
	VOID		fnExit();

	VOID		fnSBCPorcess(WORD);
	FMS_ERRORCODE		fnFMSPorcess(FMS_COMMAND, st_FMS_PACKET*);

private:
};

class CFM_ST_AT_CONTACT_CHECK :	public CFM_ST_AUTO
{
public:
	CFM_ST_AT_CONTACT_CHECK(FM_STATUS_ID, FM_STATUS_ID, CFM_Unit*);
	virtual ~CFM_ST_AT_CONTACT_CHECK(void);

	VOID		fnEnter();
	VOID		fnProc();
	VOID		fnExit();

	VOID		fnSBCPorcess(WORD);
	FMS_ERRORCODE		fnFMSPorcess(FMS_COMMAND, st_FMS_PACKET*);

private:
};

class CFM_ST_AT_RUN : public CFM_ST_AUTO
{
public:
	CFM_ST_AT_RUN(FM_STATUS_ID, FM_STATUS_ID, CFM_Unit*);
	virtual ~CFM_ST_AT_RUN(void);

	VOID		fnEnter();
	VOID		fnProc();
	VOID		fnExit();

	VOID		fnSBCPorcess(WORD);
	FMS_ERRORCODE		fnFMSPorcess(FMS_COMMAND, st_FMS_PACKET*);

private:
};

class CFM_ST_AT_END : public CFM_ST_AUTO
{
public:
	CFM_ST_AT_END(FM_STATUS_ID, FM_STATUS_ID, CFM_Unit*);
	virtual ~CFM_ST_AT_END(void);

	VOID		fnEnter();
	VOID		fnProc();
	VOID		fnExit();
	
	VOID		fnSBCPorcess(WORD);
	FMS_ERRORCODE		fnFMSPorcess(FMS_COMMAND, st_FMS_PACKET*);

private:
};
class CFM_ST_AT_ERROR : public CFM_ST_AUTO
{
public:
	CFM_ST_AT_ERROR(FM_STATUS_ID, FM_STATUS_ID, CFM_Unit*);
	virtual ~CFM_ST_AT_ERROR(void);

	VOID		fnEnter();
	VOID		fnProc();
	VOID		fnExit();

	VOID		fnSBCPorcess(WORD);
	FMS_ERRORCODE		fnFMSPorcess(FMS_COMMAND, st_FMS_PACKET*);

private:
};
//////////////////////////////////////////////////////////////////////////
class CFM_ST_LOCAL :public CFM_STATE
{
public:
	CFM_ST_LOCAL(FM_STATUS_ID, FM_STATUS_ID, CFM_Unit*);
	virtual ~CFM_ST_LOCAL(void);

	virtual	VOID		fnInit(){};
	virtual VOID		fnEnter();
	virtual VOID		fnProc();
	virtual VOID		fnExit();

	virtual VOID		fnSBCPorcess(WORD);
	virtual FMS_ERRORCODE		fnFMSPorcess(FMS_COMMAND, st_FMS_PACKET*);

	VOID fnAlarmOn();

	VOID fnSetLocal(CFM_STATE* _st){m_pstLOLocal = _st;}
	VOID fnSetError(CFM_STATE* _st){m_pstLOError = _st;}

	CFM_STATE* fnGetLocal(){return m_pstLOLocal;}
	CFM_STATE* fnGetError(){return m_pstLOError;}
private:
	CFM_STATE* m_pstLOLocal;
	CFM_STATE* m_pstLOError;
};
class CFM_ST_LO_LOCAL : public CFM_ST_LOCAL
{
public:
	CFM_ST_LO_LOCAL(FM_STATUS_ID, FM_STATUS_ID, CFM_Unit*);
	virtual ~CFM_ST_LO_LOCAL(void);

	VOID		fnEnter();
	VOID		fnProc();
	VOID		fnExit();

	VOID		fnSBCPorcess(WORD);
	FMS_ERRORCODE		fnFMSPorcess(FMS_COMMAND, st_FMS_PACKET*);

private:
};

class CFM_ST_LO_ERROR : public CFM_ST_LOCAL
{
public:
	CFM_ST_LO_ERROR(FM_STATUS_ID, FM_STATUS_ID, CFM_Unit*);
	virtual ~CFM_ST_LO_ERROR(void);

	VOID		fnEnter();
	VOID		fnProc();
	VOID		fnExit();

	VOID		fnSBCPorcess(WORD);
	FMS_ERRORCODE		fnFMSPorcess(FMS_COMMAND, st_FMS_PACKET*);

private:
};
//////////////////////////////////////////////////////////////////////////
class CFM_ST_MAINT :public CFM_STATE
{
public:
	CFM_ST_MAINT(FM_STATUS_ID, FM_STATUS_ID, CFM_Unit*);
	virtual ~CFM_ST_MAINT(void);

	VOID		fnInit(){};
	VOID		fnEnter();
	VOID		fnProc();
	VOID		fnExit();

	VOID		fnSBCPorcess(WORD);
	FMS_ERRORCODE		fnFMSPorcess(FMS_COMMAND, st_FMS_PACKET*);

	VOID fnSetMaint(CFM_STATE* _st){m_pstMAMaint = _st;}
	VOID fnSetError(CFM_STATE* _st){m_pstMAError = _st;}

	CFM_STATE* fnGetMaint(){return m_pstMAMaint;}
	CFM_STATE* fnGetError(){return m_pstMAError;}

private:
	CFM_STATE* m_pstMAMaint;
	CFM_STATE* m_pstMAError;
};
class CFM_ST_MA_MAINT : public CFM_ST_MAINT
{
public:
	CFM_ST_MA_MAINT(FM_STATUS_ID, FM_STATUS_ID, CFM_Unit*);
	virtual ~CFM_ST_MA_MAINT(void);

	VOID		fnEnter();
	VOID		fnProc();
	VOID		fnExit();

	VOID		fnSBCPorcess(WORD);
	FMS_ERRORCODE		fnFMSPorcess(FMS_COMMAND, st_FMS_PACKET*);

private:
};

class CFM_ST_MA_ERROR : public CFM_ST_MAINT
{
public:
	CFM_ST_MA_ERROR(FM_STATUS_ID, FM_STATUS_ID, CFM_Unit*);
	virtual ~CFM_ST_MA_ERROR(void);

	VOID		fnEnter();
	VOID		fnProc();
	VOID		fnExit();

	VOID		fnSBCPorcess(WORD);
	FMS_ERRORCODE		fnFMSPorcess(FMS_COMMAND, st_FMS_PACKET*);

private:
};
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
class CFM_TRAY_STATE
{
public:
	CFM_TRAY_STATE(FM_TRAY_STATUS_ID, CFM_Unit*);
	virtual ~CFM_TRAY_STATE(void);

	VOID				fnWaitInit();

	VOID				fnPorcessing();
	virtual VOID		fnEnter(){TRACE("==========TRAY_STATE fnRecv");};
	virtual VOID		fnProc(){TRACE("==========TRAY_STATE fnProc");};
	virtual VOID		fnExit(){TRACE("==========TRAY_STATE fnSend");};

	FM_TRAY_STATUS_ID	fnGetStateID(){return m_STID;}
	CFM_Unit*			fnGetMachin(){return m_Unit;}
	CFM_TRAY_STATE*		fnInitState();
	CFM_TRAY_STATE*		fnCheckStop();
	CFM_TRAY_STATE*		fnChangeState(BOOL _state);
	BOOL				fnGetError(){return m_bERROR;}

	VOID				fnSetProcID(PROC_FN_ID _id){m_ProcID = _id;}
	PROC_FN_ID			fnGetProcID(){return m_ProcID;}

	DWORD				fnGetRetyState(){return m_RetryMap;}
	const INT			fnGetWaitTimer(){return m_iConstWaitTimer;}
	const INT			fnGetWaitCount(){return m_iConstWaitCount;}

	MES_ERROR			fnMEI_STAGESTATE_CHANGE_EVENT(CHAR* _STATE);

protected:
	const FM_TRAY_STATUS_ID m_STID;
	CFM_Unit* m_Unit;

	DWORD m_RetryMap;

	INT m_iWaitTimer;
	INT m_iWaitCount;

private:
	BOOL m_bERROR;
	PROC_FN_ID m_ProcID;

	INT m_iConstWaitTimer;
	INT m_iConstWaitCount;
};
static VOID (CFM_TRAY_STATE::*CFM_TRAY_STATE_mfg[])() = 
{
	&CFM_TRAY_STATE::fnEnter
	, &CFM_TRAY_STATE::fnProc
	, &CFM_TRAY_STATE::fnExit
};
class CTRAY_ST_READY:public CFM_TRAY_STATE
{
public:
	CTRAY_ST_READY(FM_TRAY_STATUS_ID, CFM_Unit*);
	virtual ~CTRAY_ST_READY(void);

	VOID		fnEnter();
	VOID		fnProc();
	VOID		fnExit();
private:
};
class CTRAY_ST_IN:public CFM_TRAY_STATE
{
public:
	CTRAY_ST_IN(FM_TRAY_STATUS_ID, CFM_Unit*);
	virtual ~CTRAY_ST_IN(void);

	VOID		fnEnter();
	VOID		fnProc();
	VOID		fnExit();
private:
};

class CTRAY_ST_INED:public CFM_TRAY_STATE
{
public:
	CTRAY_ST_INED(FM_TRAY_STATUS_ID, CFM_Unit*);
	virtual ~CTRAY_ST_INED(void);

	VOID		fnEnter();
	VOID		fnProc();
	VOID		fnExit();
private:
};

class CTRAY_ST_OUT:public CFM_TRAY_STATE
{
public:
	CTRAY_ST_OUT(FM_TRAY_STATUS_ID, CFM_Unit*);
	virtual ~CTRAY_ST_OUT(void);

	VOID		fnEnter();
	VOID		fnProc();
	VOID		fnExit();
private:
};

class CTRAY_ST_OUTED:public CFM_TRAY_STATE
{
public:
	CTRAY_ST_OUTED(FM_TRAY_STATUS_ID, CFM_Unit*);
	virtual ~CTRAY_ST_OUTED(void);

	VOID		fnEnter();
	VOID		fnProc();
	VOID		fnExit();
private:
};