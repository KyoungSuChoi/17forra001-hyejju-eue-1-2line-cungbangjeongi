// RegulatorDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSMon.h"
#include "RegulatorDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRegulatorDlg dialog


CRegulatorDlg::CRegulatorDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CRegulatorDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CRegulatorDlg)
	m_strFileName = _T("regulator.csv");
	//}}AFX_DATA_INIT
	LanguageinitMonConfig();
}

CRegulatorDlg::~CRegulatorDlg()
{
	if( TEXT_LANG != NULL )
	{
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CRegulatorDlg::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CRegulatorDlg"), _T("TEXT_CRegulatorDlg_CNT"), _T("TEXT_CRegulatorDlg_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CRegulatorDlg_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CRegulatorDlg"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}



void CRegulatorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRegulatorDlg)
	DDX_Text(pDX, IDC_EDIT1, m_strFileName);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CRegulatorDlg, CDialog)
	//{{AFX_MSG_MAP(CRegulatorDlg)
	ON_BN_CLICKED(IDC_DATA_REQUEST, OnDataRequest)
	ON_BN_CLICKED(IDC_FILE_NAME_EDIT, OnFileNameEdit)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRegulatorDlg message handlers

BOOL CRegulatorDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CRegulatorDlg::UpdateMDData(int nModuleID)
{
	//EP_CMD_GET_REF_IC_DATA Command 는 Version 0x3002 이상에서 지원 
/*	EP_MD_SYSTEM_DATA *lpSysData = EPGetModuleSysData(EPGetModuleIndex(nModuleID));
	if(lpSysData == NULL)	return;
	if(lpSysData->nVersion < 0x3002)
	{
		DisplayData(nModuleID);		//지원 하지 않음
		return;
	}
*/
	
	LPVOID lpData = NULL;
	int nSize =  sizeof(EP_REF_IC_DATA);
	lpData = EPSendDataCmd(nModuleID, 0, 1, EP_CMD_GET_REF_IC_DATA, nSize);

	if(lpData != NULL)
	{
		LPEP_REF_IC_DATA lpRefData = new EP_REF_IC_DATA;
		ASSERT(lpRefData);
		ZeroMemory(lpRefData, sizeof(EP_REF_IC_DATA));
		memcpy(lpRefData, lpData, sizeof(EP_REF_IC_DATA));

		DisplayData(nModuleID, lpRefData);

		delete lpRefData;
		lpRefData = NULL;
	}
}

void CRegulatorDlg::DisplayData(int nModuleID, LPEP_REF_IC_DATA lpData)
{
	FILE *fp =  NULL;
	fp = fopen(m_strFileName, "at+");

	if(fp != NULL)
	{
		if(lpData == NULL)
		{
			fprintf(fp, TEXT_LANG[0]);//"MD%d, 지원하지 않는 장비 입니다.\n"
		}
		else
		{
			for(int i = 0; i<EPGetInstalledBoard(nModuleID); i++)
			{
				//AD Value at Calibration
				fprintf(fp, "MD%d, BD%d, Cal,", nModuleID, i+1);
				fprintf(fp, "%d, %s,", lpData->refIC_Data[i].vPluse.lCalData, lpData->refIC_Data[i].vPluse.szCalTime);
				fprintf(fp, "%d, %s,", lpData->refIC_Data[i].vZero.lCalData, lpData->refIC_Data[i].vZero.szCalTime);
				fprintf(fp, "%d, %s,", lpData->refIC_Data[i].vMinus.lCalData, lpData->refIC_Data[i].vMinus.szCalTime);
				fprintf(fp, "%d, %s,", lpData->refIC_Data[i].iPluse.lCalData, lpData->refIC_Data[i].iPluse.szCalTime);
				fprintf(fp, "%d, %s,", lpData->refIC_Data[i].iZero.lCalData, lpData->refIC_Data[i].iZero.szCalTime);
				fprintf(fp, "%d, %s\n", lpData->refIC_Data[i].iMinus.lCalData, lpData->refIC_Data[i].iMinus.szCalTime);

				//Current AD value
				fprintf(fp, "MD%d, BD%d, Cur,", nModuleID, i+1);
				fprintf(fp, "%d, %s,", lpData->refIC_Data[i].vPluse.lCurData, lpData->refIC_Data[i].vPluse.szCurTime);
				fprintf(fp, "%d, %s,", lpData->refIC_Data[i].vZero.lCurData, lpData->refIC_Data[i].vZero.szCurTime);
				fprintf(fp, "%d, %s,", lpData->refIC_Data[i].vMinus.lCurData, lpData->refIC_Data[i].vMinus.szCurTime);
				fprintf(fp, "%d, %s,", lpData->refIC_Data[i].iPluse.lCurData, lpData->refIC_Data[i].iPluse.szCurTime);
				fprintf(fp, "%d, %s,", lpData->refIC_Data[i].iZero.lCurData, lpData->refIC_Data[i].iZero.szCurTime);
				fprintf(fp, "%d, %s\n", lpData->refIC_Data[i].iMinus.lCurData, lpData->refIC_Data[i].iMinus.szCurTime);

				//Max AD Value
				fprintf(fp, "MD%d, BD%d, Max,", nModuleID, i+1);
				fprintf(fp, "%d, %s,", lpData->refIC_Data[i].vPluse.lMaxData, lpData->refIC_Data[i].vPluse.szMaxTime);
				fprintf(fp, "%d, %s,", lpData->refIC_Data[i].vZero.lMaxData, lpData->refIC_Data[i].vZero.szMaxTime);
				fprintf(fp, "%d, %s,", lpData->refIC_Data[i].vMinus.lMaxData, lpData->refIC_Data[i].vMinus.szMaxTime);
				fprintf(fp, "%d, %s,", lpData->refIC_Data[i].iPluse.lMaxData, lpData->refIC_Data[i].iPluse.szMaxTime);
				fprintf(fp, "%d, %s,", lpData->refIC_Data[i].iZero.lMaxData, lpData->refIC_Data[i].iZero.szMaxTime);
				fprintf(fp, "%d, %s\n", lpData->refIC_Data[i].iMinus.lMaxData, lpData->refIC_Data[i].iMinus.szMaxTime);

				//Min AD Value 
				fprintf(fp, "MD%d, BD%d, Min,", nModuleID, i+1);
				fprintf(fp, "%d, %s,", lpData->refIC_Data[i].vPluse.lMinData, lpData->refIC_Data[i].vPluse.szMinTime);
				fprintf(fp, "%d, %s,", lpData->refIC_Data[i].vZero.lMinData, lpData->refIC_Data[i].vZero.szMinTime);
				fprintf(fp, "%d, %s,", lpData->refIC_Data[i].vMinus.lMinData, lpData->refIC_Data[i].vMinus.szMinTime);
				fprintf(fp, "%d, %s,", lpData->refIC_Data[i].iPluse.lMinData, lpData->refIC_Data[i].iPluse.szMinTime);
				fprintf(fp, "%d, %s,", lpData->refIC_Data[i].iZero.lMinData, lpData->refIC_Data[i].iZero.szMinTime);
				fprintf(fp, "%d, %s\n", lpData->refIC_Data[i].iMinus.lMinData, lpData->refIC_Data[i].iMinus.szMinTime);
			}
		}
	}
	fclose(fp);

}

void CRegulatorDlg::OnDataRequest() 
{
	// TODO: Add your control notification handler code here
	
	int nInstalledMD =  EPGetInstalledModuleNum();
	int nModuleID;
	BeginWaitCursor();

	for(int i = 0; i<nInstalledMD; i++)
	{
		nModuleID = EPGetModuleID(i);
		if(nModuleID > 0)
		{
			if(EPGetModuleState(nModuleID) != EP_STATE_LINE_OFF)
			{
				UpdateMDData(nModuleID);
			}
		}
	}
	EndWaitCursor();

	OnOK();
	
}

void CRegulatorDlg::OnFileNameEdit() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	CFileDialog pDlg(FALSE, ".csv", m_strFileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, TEXT_LANG[1]);//"csv 파일(*.csv)|*.csv|"
	if(IDOK == pDlg.DoModal())
	{
		m_strFileName = pDlg.GetPathName();
		UpdateData(FALSE);
	}
}
