// StepRecordSet.cpp : implementation file
//

#include "stdafx.h"
#include "StepRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CStepRecordSet

IMPLEMENT_DYNAMIC(CStepRecordSet, CDaoRecordset)

extern CString GetDataBaseName();

CStepRecordSet::CStepRecordSet(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CStepRecordSet)
	m_StepID = 0;
	m_ModelID = 0;
	m_TestID = 0;
	m_StepNo = 0;
	m_StepProcType = 0;
	m_StepType = 0;
	m_StepMode = 0;
	m_Vref = 0.0f;
	m_Iref = 0.0f;
	m_EndTime = 0;
	m_EndV = 0.0f;
	m_EndI = 0.0f;
	m_EndCapacity = 0.0f;
	m_End_dV = 0.0f;
	m_End_dI = 0.0f;
	m_CycleCount = 0;
	m_OverV = 0.0f;
	m_LimitV = 0.0f;
	m_OverI = 0.0f;
	m_LimitI = 0.0f;
	m_OverCapacity = 0.0f;
	m_LimitCapacity = 0.0f;
	m_OverImpedance = 0.0f;
	m_LimitImpedance = 0.0f;
	m_DeltaTime = 0;
	m_DeltaTime1 = 0;
	m_DeltaV = 0.0f;
	m_DeltaI = 0.0f;
	m_Grade = FALSE;
	m_CompTimeV1 = 0;
	m_CompTimeV2 = 0;
	m_CompTimeV3 = 0;
	m_CompVLow1 = 0.0f;
	m_CompVLow2 = 0.0f;
	m_CompVLow3 = 0.0f;
	m_CompVHigh1 = 0.0f;
	m_CompVHigh2 = 0.0f;
	m_CompVHigh3 = 0.0f;
	m_CompTimeI1 = 0;
	m_CompTimeI2 = 0;
	m_CompTimeI3 = 0;
	m_CompILow1 = 0.0f;
	m_CompILow2 = 0.0f;
	m_CompILow3 = 0.0f;
	m_CompIHigh1 = 0.0f;
	m_CompIHigh2 = 0.0f;
	m_CompIHigh3 = 0.0f;
	m_RecordTime = 0;
	m_RecordVIGetTime = 0;
	m_RecordDeltaV = 0.0f;
	m_RecordDeltaI = 0.0f;
	m_CapVLow = 0.0f;
	m_CapVHigh = 0.0f;
	m_EndCheckVLow = 0.0f;
	m_EndCheckVHigh = 0.0f;
	m_EndCheckILow = 0.0f;
	m_EndCheckIHigh = 0.0f;
	m_strGotoValue = _T("");
	m_strReportTemp = _T("");
	m_strSocEnd = _T("");
	m_strEndWatt = _T("");
	m_strTempLimit = _T("");
	m_strSimulFile = _T("");
	m_strValueLimit = _T("");
	m_strEndVGotoValue = _T("");	//m_Value7 = _T("");
	m_strEndTGotoValue = _T("");	//m_Value8 = _T("");
	m_strEndCGotoValue = _T("");	//m_Value9 = _T("");
	m_fDCIR_RegTemp = 0.0f;
	m_fDCIR_ResistanceRate = 0.0f;

	m_fCompChgCcVtg = 0.0f;
	m_ulCompChgCcTime = 0;
	m_fCompChgCcDeltaVtg = 0.0f;
	m_fCompChgCvCrt = 0.0f;
	m_ulCompChgCvtTime = 0;
	m_fCompChgCvDeltaCrt = 0.0f;
		
	m_nFields = 75;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString CStepRecordSet::GetDefaultDBName()
{
	return _T(GetDataBaseName());
}

CString CStepRecordSet::GetDefaultSQL()
{
	return _T("[Step]");
}

void CStepRecordSet::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CStepRecordSet)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Long(pFX, _T("[StepID]"), m_StepID);
	DFX_Long(pFX, _T("[ModelID]"), m_ModelID);
	DFX_Long(pFX, _T("[TestID]"), m_TestID);
	DFX_Long(pFX, _T("[StepNo]"), m_StepNo);
	DFX_Long(pFX, _T("[StepProcType]"), m_StepProcType);
	DFX_Long(pFX, _T("[StepType]"), m_StepType);
	DFX_Long(pFX, _T("[StepMode]"), m_StepMode);
	DFX_Single(pFX, _T("[Vref]"), m_Vref);
	DFX_Single(pFX, _T("[Iref]"), m_Iref);
	DFX_Long(pFX, _T("[EndTime]"), m_EndTime);
	DFX_Single(pFX, _T("[EndV]"), m_EndV);
	DFX_Single(pFX, _T("[EndI]"), m_EndI);
	DFX_Single(pFX, _T("[EndCapacity]"), m_EndCapacity);
	DFX_Single(pFX, _T("[End_dV]"), m_End_dV);
	DFX_Single(pFX, _T("[End_dI]"), m_End_dI);
	DFX_Long(pFX, _T("[CycleCount]"), m_CycleCount);
	DFX_Single(pFX, _T("[OverV]"), m_OverV);
	DFX_Single(pFX, _T("[LimitV]"), m_LimitV);
	DFX_Single(pFX, _T("[OverI]"), m_OverI);
	DFX_Single(pFX, _T("[LimitI]"), m_LimitI);
	DFX_Single(pFX, _T("[OverCapacity]"), m_OverCapacity);
	DFX_Single(pFX, _T("[LimitCapacity]"), m_LimitCapacity);
	DFX_Single(pFX, _T("[OverImpedance]"), m_OverImpedance);
	DFX_Single(pFX, _T("[LimitImpedance]"), m_LimitImpedance);
	DFX_Long(pFX, _T("[DeltaTime]"), m_DeltaTime);
	DFX_Long(pFX, _T("[DeltaTime1]"), m_DeltaTime1);
	DFX_Single(pFX, _T("[DeltaV]"), m_DeltaV);
	DFX_Single(pFX, _T("[DeltaI]"), m_DeltaI);
	DFX_Bool(pFX, _T("[Grade]"), m_Grade);
	DFX_Long(pFX, _T("[CompTimeV1]"), m_CompTimeV1);
	DFX_Long(pFX, _T("[CompTimeV2]"), m_CompTimeV2);
	DFX_Long(pFX, _T("[CompTimeV3]"), m_CompTimeV3);
	DFX_Single(pFX, _T("[CompVLow1]"), m_CompVLow1);
	DFX_Single(pFX, _T("[CompVLow2]"), m_CompVLow2);
	DFX_Single(pFX, _T("[CompVLow3]"), m_CompVLow3);
	DFX_Single(pFX, _T("[CompVHigh1]"), m_CompVHigh1);
	DFX_Single(pFX, _T("[CompVHigh2]"), m_CompVHigh2);
	DFX_Single(pFX, _T("[CompVHigh3]"), m_CompVHigh3);
	DFX_Long(pFX, _T("[CompTimeI1]"), m_CompTimeI1);
	DFX_Long(pFX, _T("[CompTimeI2]"), m_CompTimeI2);
	DFX_Long(pFX, _T("[CompTimeI3]"), m_CompTimeI3);
	DFX_Single(pFX, _T("[CompILow1]"), m_CompILow1);
	DFX_Single(pFX, _T("[CompILow2]"), m_CompILow2);
	DFX_Single(pFX, _T("[CompILow3]"), m_CompILow3);
	DFX_Single(pFX, _T("[CompIHigh1]"), m_CompIHigh1);
	DFX_Single(pFX, _T("[CompIHigh2]"), m_CompIHigh2);
	DFX_Single(pFX, _T("[CompIHigh3]"), m_CompIHigh3);
	DFX_Long(pFX, _T("[RecordTime]"), m_RecordTime);
	DFX_Single(pFX, _T("[RecordDeltaV]"), m_RecordDeltaV);
	DFX_Single(pFX, _T("[RecordDeltaI]"), m_RecordDeltaI);	
	DFX_Long(pFX, _T("[RecordVIGetTime]"), m_RecordVIGetTime);
	DFX_Single(pFX, _T("[CapVLow]"), m_CapVLow);
	DFX_Single(pFX, _T("[CapVHigh]"), m_CapVHigh);
	DFX_Single(pFX, _T("[EndCheckVLow]"), m_EndCheckVLow);
	DFX_Single(pFX, _T("[EndCheckVHigh]"), m_EndCheckVHigh);
	DFX_Single(pFX, _T("[EndCheckILow]"), m_EndCheckILow);
	DFX_Single(pFX, _T("[EndCheckIHigh]"), m_EndCheckIHigh);
	DFX_Text(pFX, _T("[Value0]"), m_strGotoValue);
	DFX_Text(pFX, _T("[Value1]"), m_strReportTemp);
	DFX_Text(pFX, _T("[Value2]"), m_strSocEnd);
	DFX_Text(pFX, _T("[Value3]"), m_strEndWatt);
	DFX_Text(pFX, _T("[Value4]"), m_strTempLimit);
	DFX_Text(pFX, _T("[Value5]"), m_strSimulFile);
	DFX_Text(pFX, _T("[Value6]"), m_strValueLimit);
	DFX_Text(pFX, _T("[Value7]"), m_strEndVGotoValue);
	DFX_Text(pFX, _T("[Value8]"), m_strEndTGotoValue);
	DFX_Text(pFX, _T("[Value9]"), m_strEndCGotoValue);
	DFX_Single(pFX, _T("[DCIR_RegTemp]"), m_fDCIR_RegTemp);
	DFX_Single(pFX, _T("[DCIR_ResistanceRate]"), m_fDCIR_ResistanceRate);
	DFX_Single(pFX, _T("[CompChgCcVtg]"), m_fCompChgCcVtg);
	DFX_Long(pFX, _T("[CompChgCcTime]"), m_ulCompChgCcTime);
	DFX_Single(pFX, _T("[CompChgCcDeltaVtg]"), m_fCompChgCcDeltaVtg);
	DFX_Single(pFX, _T("[CompChgCvCrt]"), m_fCompChgCvCrt);
	DFX_Long(pFX, _T("[CompChgCvTime]"), m_ulCompChgCvtTime);
	DFX_Single(pFX, _T("[CompChgCvDeltaCrt]"), m_fCompChgCvDeltaCrt);
	//}}AFX_FIELD_MAP
}
 
/////////////////////////////////////////////////////////////////////////////
 // CStepRecordSet diagnostics

#ifdef _DEBUG
void CStepRecordSet::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CStepRecordSet::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
